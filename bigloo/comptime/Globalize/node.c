/*===========================================================================*/
/*   (Globalize/node.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/node.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_NODE_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_NODE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_svarzf2ginfozf2_bgl
	{
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
		bool_t BgL_celledzf3zf3;
		bool_t BgL_stackablez00;
	}                      *BgL_svarzf2ginfozf2_bglt;

	typedef struct BgL_localzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		bool_t BgL_globaliza7edzf3z54;
	}                       *BgL_localzf2ginfozf2_bglt;


#endif													// BGL_GLOBALIZE_NODE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62gloz12z70zzglobaliza7e_nodeza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_nodeza7 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_funz00zzast_varz00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern BgL_globalz00_bglt
		BGl_thezd2globalzd2zzglobaliza7e_localzd2ze3globalz96(BgL_localz00_bglt);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static BgL_nodez00_bglt BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_nodez00_bglt,
		BgL_variablez00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzglobaliza7e_nodeza7(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2sequence1380za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62gloz12zd2app1384za2zzglobaliza7e_nodeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2letzd2fun1403z70zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_nodeza7(void);
	static BgL_appz00_bglt
		BGl_azd2makezd2procedurez00zzglobaliza7e_nodeza7(BgL_localz00_bglt);
	static obj_t BGl_makezd2setszd2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t,
		BgL_variablez00_bglt);
	extern obj_t BGl_za2cellza2z00zztype_cachez00;
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_nodeza7(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static BgL_appz00_bglt
		BGl_azd2procedurezd2setz12z12zzglobaliza7e_nodeza7(obj_t, obj_t, long,
		obj_t, BgL_variablez00_bglt);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	static BgL_localz00_bglt
		BGl_cellzd2variablezd2zzglobaliza7e_nodeza7(BgL_localz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2atom1369za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzglobaliza7e_nodeza7(obj_t, obj_t);
	extern obj_t BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_nodeza7(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2conditional1396za2zzglobaliza7e_nodeza7(obj_t, obj_t,
		obj_t);
	static bool_t BGl_celledzf3zf3zzglobaliza7e_nodeza7(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_nodezd2globaliza7ez12z67zzglobaliza7e_nodeza7(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2switch1401za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2globaliza7ez12z05zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62gloz121366z70zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2setzd2exzd2it1408za2zzglobaliza7e_nodeza7(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2appzd2ly1386z70zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static BgL_nodez00_bglt BGl_cellzd2formalszd2zzglobaliza7e_nodeza7(obj_t,
		BgL_nodez00_bglt);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2letzd2var1406z70zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_thezd2closurezd2zzglobaliza7e_freeza7(BgL_variablez00_bglt,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2boxzd2ref1416z70zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2extern1390za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2boxzd2setz121418z62zzglobaliza7e_nodeza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_gloza2z12zb0zzglobaliza7e_nodeza7(obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2makezd2box1414z70zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzglobaliza7e_nodeza7(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_nodeza7(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_nodeza7(void);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2kwote1371za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_nodeza7(void);
	static obj_t BGl_celledzd2bindingszd2zzglobaliza7e_nodeza7(obj_t);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2jumpzd2exzd2it1412za2zzglobaliza7e_nodeza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2funcall1388za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2cast1392za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static BgL_letzd2varzd2_bglt
		BGl_makezd2escapingzd2bindingsz00zzglobaliza7e_nodeza7(obj_t,
		BgL_nodez00_bglt, BgL_variablez00_bglt);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2setq1394za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static obj_t
		BGl_globaliza7ezd2localzd2funz12zb5zzglobaliza7e_nodeza7(BgL_localz00_bglt,
		BgL_variablez00_bglt);
	static BgL_makezd2boxzd2_bglt
		BGl_azd2makezd2cellz00zzglobaliza7e_nodeza7(BgL_nodez00_bglt,
		BgL_variablez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2sync1382za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2closure1377za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2fail1399za2zzglobaliza7e_nodeza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	static BgL_nodez00_bglt BGl_z62gloz12zd2var1374za2zzglobaliza7e_nodeza7(obj_t,
		obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00(BgL_typez00_bglt);
	static obj_t __cnst[10];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_nodezd2globaliza7ez12zd2envzb5zzglobaliza7e_nodeza7,
		BgL_bgl_za762nodeza7d2global2076z00,
		BGl_z62nodezd2globaliza7ez12z05zzglobaliza7e_nodeza7, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2047z00zzglobaliza7e_nodeza7,
		BgL_bgl_string2047za700za7za7g2077za7, "glo!1366", 8);
	      DEFINE_STRING(BGl_string2048z00zzglobaliza7e_nodeza7,
		BgL_bgl_string2048za700za7za7g2078za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2050z00zzglobaliza7e_nodeza7,
		BgL_bgl_string2050za700za7za7g2079za7, "glo!", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2046z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza7121366za7702080za7,
		BGl_z62gloz121366z70zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2049z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2atom2081za7,
		BGl_z62gloz12zd2atom1369za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2051z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2kwot2082za7,
		BGl_z62gloz12zd2kwote1371za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2052z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2var12083za7,
		BGl_z62gloz12zd2var1374za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2053z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2clos2084za7,
		BGl_z62gloz12zd2closure1377za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2054z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2sequ2085za7,
		BGl_z62gloz12zd2sequence1380za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2055z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2sync2086za7,
		BGl_z62gloz12zd2sync1382za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2056z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2app12087za7,
		BGl_z62gloz12zd2app1384za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2057z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2appza72088z00,
		BGl_z62gloz12zd2appzd2ly1386z70zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2058z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2func2089za7,
		BGl_z62gloz12zd2funcall1388za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2059z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2exte2090za7,
		BGl_z62gloz12zd2extern1390za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2060z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2cast2091za7,
		BGl_z62gloz12zd2cast1392za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2061z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2setq2092za7,
		BGl_z62gloz12zd2setq1394za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2062z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2cond2093za7,
		BGl_z62gloz12zd2conditional1396za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2063z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2fail2094za7,
		BGl_z62gloz12zd2fail1399za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2064z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2swit2095za7,
		BGl_z62gloz12zd2switch1401za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2065z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2letza72096z00,
		BGl_z62gloz12zd2letzd2fun1403z70zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2072z00zzglobaliza7e_nodeza7,
		BgL_bgl_string2072za700za7za7g2097za7,
		"Shoud not be here (because of the integration", 45);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2066z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2letza72098z00,
		BGl_z62gloz12zd2letzd2var1406z70zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2073z00zzglobaliza7e_nodeza7,
		BgL_bgl_string2073za700za7za7g2099za7, "globalize_node", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2067z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2setza72100z00,
		BGl_z62gloz12zd2setzd2exzd2it1408za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2074z00zzglobaliza7e_nodeza7,
		BgL_bgl_string2074za700za7za7g2101za7,
		"aux glo! glo!1366 procedure-set! foreign make-fx-procedure make-va-procedure done (write cell-globalize) cell-globalize ",
		120);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2068z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2jump2102za7,
		BGl_z62gloz12zd2jumpzd2exzd2it1412za2zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2069z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2make2103za7,
		BGl_z62gloz12zd2makezd2box1414z70zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2070z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2boxza72104z00,
		BGl_z62gloz12zd2boxzd2ref1416z70zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2071z00zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za7d2boxza72105z00,
		BGl_z62gloz12zd2boxzd2setz121418z62zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
		BgL_bgl_za762gloza712za770za7za7gl2106za7,
		BGl_z62gloz12z70zzglobaliza7e_nodeza7, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_nodeza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(long
		BgL_checksumz00_3820, char *BgL_fromz00_3821)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_nodeza7))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_nodeza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_nodeza7();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_nodeza7();
					BGl_cnstzd2initzd2zzglobaliza7e_nodeza7();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_nodeza7();
					BGl_genericzd2initzd2zzglobaliza7e_nodeza7();
					BGl_methodzd2initzd2zzglobaliza7e_nodeza7();
					return BGl_toplevelzd2initzd2zzglobaliza7e_nodeza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_nodeza7(void)
	{
		{	/* Globalize/node.scm 17 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "globalize_node");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_node");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_node");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"globalize_node");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "globalize_node");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"globalize_node");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "globalize_node");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_node");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_node");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_node");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzglobaliza7e_nodeza7(void)
	{
		{	/* Globalize/node.scm 17 */
			{	/* Globalize/node.scm 17 */
				obj_t BgL_cportz00_3529;

				{	/* Globalize/node.scm 17 */
					obj_t BgL_stringz00_3536;

					BgL_stringz00_3536 = BGl_string2074z00zzglobaliza7e_nodeza7;
					{	/* Globalize/node.scm 17 */
						obj_t BgL_startz00_3537;

						BgL_startz00_3537 = BINT(0L);
						{	/* Globalize/node.scm 17 */
							obj_t BgL_endz00_3538;

							BgL_endz00_3538 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3536)));
							{	/* Globalize/node.scm 17 */

								BgL_cportz00_3529 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3536, BgL_startz00_3537, BgL_endz00_3538);
				}}}}
				{
					long BgL_iz00_3530;

					BgL_iz00_3530 = 9L;
				BgL_loopz00_3531:
					if ((BgL_iz00_3530 == -1L))
						{	/* Globalize/node.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Globalize/node.scm 17 */
							{	/* Globalize/node.scm 17 */
								obj_t BgL_arg2075z00_3532;

								{	/* Globalize/node.scm 17 */

									{	/* Globalize/node.scm 17 */
										obj_t BgL_locationz00_3534;

										BgL_locationz00_3534 = BBOOL(((bool_t) 0));
										{	/* Globalize/node.scm 17 */

											BgL_arg2075z00_3532 =
												BGl_readz00zz__readerz00(BgL_cportz00_3529,
												BgL_locationz00_3534);
										}
									}
								}
								{	/* Globalize/node.scm 17 */
									int BgL_tmpz00_3851;

									BgL_tmpz00_3851 = (int) (BgL_iz00_3530);
									CNST_TABLE_SET(BgL_tmpz00_3851, BgL_arg2075z00_3532);
							}}
							{	/* Globalize/node.scm 17 */
								int BgL_auxz00_3535;

								BgL_auxz00_3535 = (int) ((BgL_iz00_3530 - 1L));
								{
									long BgL_iz00_3856;

									BgL_iz00_3856 = (long) (BgL_auxz00_3535);
									BgL_iz00_3530 = BgL_iz00_3856;
									goto BgL_loopz00_3531;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_nodeza7(void)
	{
		{	/* Globalize/node.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzglobaliza7e_nodeza7(void)
	{
		{	/* Globalize/node.scm 17 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzglobaliza7e_nodeza7(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1760;

				BgL_headz00_1760 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1761;
					obj_t BgL_tailz00_1762;

					BgL_prevz00_1761 = BgL_headz00_1760;
					BgL_tailz00_1762 = BgL_l1z00_1;
				BgL_loopz00_1763:
					if (PAIRP(BgL_tailz00_1762))
						{
							obj_t BgL_newzd2prevzd2_1765;

							BgL_newzd2prevzd2_1765 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1762), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1761, BgL_newzd2prevzd2_1765);
							{
								obj_t BgL_tailz00_3866;
								obj_t BgL_prevz00_3865;

								BgL_prevz00_3865 = BgL_newzd2prevzd2_1765;
								BgL_tailz00_3866 = CDR(BgL_tailz00_1762);
								BgL_tailz00_1762 = BgL_tailz00_3866;
								BgL_prevz00_1761 = BgL_prevz00_3865;
								goto BgL_loopz00_1763;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1760);
				}
			}
		}

	}



/* node-globalize! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_nodezd2globaliza7ez12z67zzglobaliza7e_nodeza7(BgL_nodez00_bglt
		BgL_nodez00_3, BgL_variablez00_bglt BgL_integratorz00_4,
		obj_t BgL_whatzf2byza2z50_5)
	{
		{	/* Globalize/node.scm 40 */
			{	/* Globalize/node.scm 44 */
				BgL_valuez00_bglt BgL_funz00_1768;

				BgL_funz00_1768 =
					(((BgL_variablez00_bglt) COBJECT(BgL_integratorz00_4))->BgL_valuez00);
				{	/* Globalize/node.scm 44 */
					obj_t BgL_celledz00_1769;

					{	/* Globalize/node.scm 45 */
						obj_t BgL_arg1473z00_1790;

						BgL_arg1473z00_1790 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1768)))->BgL_argsz00);
						BgL_celledz00_1769 =
							BGl_celledzd2bindingszd2zzglobaliza7e_nodeza7
							(BgL_arg1473z00_1790);
					}
					{	/* Globalize/node.scm 45 */
						obj_t BgL_whatzf2byza2z50_1770;

						BgL_whatzf2byza2z50_1770 =
							BGl_appendzd221011zd2zzglobaliza7e_nodeza7(BgL_celledz00_1769,
							BgL_whatzf2byza2z50_5);
						{	/* Globalize/node.scm 46 */

							{
								obj_t BgL_l1344z00_1772;

								BgL_l1344z00_1772 = BgL_whatzf2byza2z50_1770;
							BgL_zc3z04anonymousza31423ze3z87_1773:
								if (PAIRP(BgL_l1344z00_1772))
									{	/* Globalize/node.scm 48 */
										{	/* Globalize/node.scm 49 */
											obj_t BgL_wzd2bzd2_1775;

											BgL_wzd2bzd2_1775 = CAR(BgL_l1344z00_1772);
											{	/* Globalize/node.scm 49 */
												obj_t BgL_arg1434z00_1776;
												obj_t BgL_arg1437z00_1777;

												BgL_arg1434z00_1776 = CAR(((obj_t) BgL_wzd2bzd2_1775));
												BgL_arg1437z00_1777 = CDR(((obj_t) BgL_wzd2bzd2_1775));
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_arg1434z00_1776))))->
														BgL_fastzd2alphazd2) =
													((obj_t) BgL_arg1437z00_1777), BUNSPEC);
											}
										}
										{
											obj_t BgL_l1344z00_3884;

											BgL_l1344z00_3884 = CDR(BgL_l1344z00_1772);
											BgL_l1344z00_1772 = BgL_l1344z00_3884;
											goto BgL_zc3z04anonymousza31423ze3z87_1773;
										}
									}
								else
									{	/* Globalize/node.scm 48 */
										((bool_t) 1);
									}
							}
							{	/* Globalize/node.scm 51 */
								BgL_nodez00_bglt BgL_resz00_1780;

								BgL_resz00_1780 =
									BGl_cellzd2formalszd2zzglobaliza7e_nodeza7(BgL_celledz00_1769,
									BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_nodez00_3,
										BgL_integratorz00_4));
								{
									obj_t BgL_l1346z00_1782;

									BgL_l1346z00_1782 = BgL_whatzf2byza2z50_1770;
								BgL_zc3z04anonymousza31449ze3z87_1783:
									if (PAIRP(BgL_l1346z00_1782))
										{	/* Globalize/node.scm 53 */
											{	/* Globalize/node.scm 54 */
												obj_t BgL_wzd2bzd2_1785;

												BgL_wzd2bzd2_1785 = CAR(BgL_l1346z00_1782);
												{	/* Globalize/node.scm 54 */
													obj_t BgL_arg1453z00_1786;

													BgL_arg1453z00_1786 =
														CAR(((obj_t) BgL_wzd2bzd2_1785));
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			BgL_arg1453z00_1786))))->
															BgL_fastzd2alphazd2) =
														((obj_t) BUNSPEC), BUNSPEC);
												}
											}
											{
												obj_t BgL_l1346z00_3896;

												BgL_l1346z00_3896 = CDR(BgL_l1346z00_1782);
												BgL_l1346z00_1782 = BgL_l1346z00_3896;
												goto BgL_zc3z04anonymousza31449ze3z87_1783;
											}
										}
									else
										{	/* Globalize/node.scm 53 */
											((bool_t) 1);
										}
								}
								return BgL_resz00_1780;
							}
						}
					}
				}
			}
		}

	}



/* &node-globalize! */
	BgL_nodez00_bglt BGl_z62nodezd2globaliza7ez12z05zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3430, obj_t BgL_nodez00_3431, obj_t BgL_integratorz00_3432,
		obj_t BgL_whatzf2byza2z50_3433)
	{
		{	/* Globalize/node.scm 40 */
			return
				BGl_nodezd2globaliza7ez12z67zzglobaliza7e_nodeza7(
				((BgL_nodez00_bglt) BgL_nodez00_3431),
				((BgL_variablez00_bglt) BgL_integratorz00_3432),
				BgL_whatzf2byza2z50_3433);
		}

	}



/* celled-bindings */
	obj_t BGl_celledzd2bindingszd2zzglobaliza7e_nodeza7(obj_t BgL_formalsz00_6)
	{
		{	/* Globalize/node.scm 61 */
			{
				obj_t BgL_celledz00_1793;
				obj_t BgL_formalsz00_1794;

				BgL_celledz00_1793 = BNIL;
				BgL_formalsz00_1794 = BgL_formalsz00_6;
			BgL_zc3z04anonymousza31474ze3z87_1795:
				if (NULLP(BgL_formalsz00_1794))
					{	/* Globalize/node.scm 65 */
						return BgL_celledz00_1793;
					}
				else
					{	/* Globalize/node.scm 67 */
						bool_t BgL_test2113z00_3903;

						{	/* Globalize/node.scm 67 */
							obj_t BgL_arg1514z00_1805;

							BgL_arg1514z00_1805 = CAR(((obj_t) BgL_formalsz00_1794));
							BgL_test2113z00_3903 =
								BGl_celledzf3zf3zzglobaliza7e_nodeza7(
								((BgL_variablez00_bglt) BgL_arg1514z00_1805));
						}
						if (BgL_test2113z00_3903)
							{	/* Globalize/node.scm 70 */
								obj_t BgL_ovarz00_1799;

								BgL_ovarz00_1799 = CAR(((obj_t) BgL_formalsz00_1794));
								{	/* Globalize/node.scm 70 */
									BgL_localz00_bglt BgL_nvarz00_1800;

									BgL_nvarz00_1800 =
										BGl_cellzd2variablezd2zzglobaliza7e_nodeza7(
										((BgL_localz00_bglt) BgL_ovarz00_1799));
									{	/* Globalize/node.scm 71 */

										{	/* Globalize/node.scm 72 */
											obj_t BgL_arg1489z00_1801;
											obj_t BgL_arg1502z00_1802;

											{	/* Globalize/node.scm 72 */
												obj_t BgL_arg1509z00_1803;

												BgL_arg1509z00_1803 =
													MAKE_YOUNG_PAIR(BgL_ovarz00_1799,
													((obj_t) BgL_nvarz00_1800));
												BgL_arg1489z00_1801 =
													MAKE_YOUNG_PAIR(BgL_arg1509z00_1803,
													BgL_celledz00_1793);
											}
											BgL_arg1502z00_1802 = CDR(((obj_t) BgL_formalsz00_1794));
											{
												obj_t BgL_formalsz00_3918;
												obj_t BgL_celledz00_3917;

												BgL_celledz00_3917 = BgL_arg1489z00_1801;
												BgL_formalsz00_3918 = BgL_arg1502z00_1802;
												BgL_formalsz00_1794 = BgL_formalsz00_3918;
												BgL_celledz00_1793 = BgL_celledz00_3917;
												goto BgL_zc3z04anonymousza31474ze3z87_1795;
											}
										}
									}
								}
							}
						else
							{	/* Globalize/node.scm 68 */
								obj_t BgL_arg1513z00_1804;

								BgL_arg1513z00_1804 = CDR(((obj_t) BgL_formalsz00_1794));
								{
									obj_t BgL_formalsz00_3921;

									BgL_formalsz00_3921 = BgL_arg1513z00_1804;
									BgL_formalsz00_1794 = BgL_formalsz00_3921;
									goto BgL_zc3z04anonymousza31474ze3z87_1795;
								}
							}
					}
			}
		}

	}



/* cell-variable */
	BgL_localz00_bglt
		BGl_cellzd2variablezd2zzglobaliza7e_nodeza7(BgL_localz00_bglt
		BgL_localz00_7)
	{
		{	/* Globalize/node.scm 77 */
			{	/* Globalize/node.scm 78 */
				BgL_localz00_bglt BgL_varz00_1807;

				{	/* Globalize/node.scm 78 */
					obj_t BgL_arg1540z00_1814;

					BgL_arg1540z00_1814 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_localz00_7)))->BgL_idz00);
					BgL_varz00_1807 =
						BGl_makezd2localzd2svarz00zzast_localz00(BgL_arg1540z00_1814,
						((BgL_typez00_bglt) BGl_za2cellza2z00zztype_cachez00));
				}
				{	/* Globalize/node.scm 79 */
					obj_t BgL_vz00_2573;

					BgL_vz00_2573 = CNST_TABLE_REF(0);
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_1807)))->BgL_accessz00) =
						((obj_t) BgL_vz00_2573), BUNSPEC);
				}
				{	/* Globalize/node.scm 80 */
					bool_t BgL_arg1516z00_1808;

					BgL_arg1516z00_1808 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_localz00_7)))->BgL_userzf3zf3);
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_1807)))->BgL_userzf3zf3) =
						((bool_t) BgL_arg1516z00_1808), BUNSPEC);
				}
				{	/* Globalize/node.scm 81 */
					BgL_svarz00_bglt BgL_tmp1112z00_1809;

					BgL_tmp1112z00_1809 =
						((BgL_svarz00_bglt)
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_1807)))->BgL_valuez00));
					{	/* Globalize/node.scm 81 */
						BgL_svarzf2ginfozf2_bglt BgL_wide1114z00_1811;

						BgL_wide1114z00_1811 =
							((BgL_svarzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_svarzf2ginfozf2_bgl))));
						{	/* Globalize/node.scm 81 */
							obj_t BgL_auxz00_3940;
							BgL_objectz00_bglt BgL_tmpz00_3937;

							BgL_auxz00_3940 = ((obj_t) BgL_wide1114z00_1811);
							BgL_tmpz00_3937 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_tmp1112z00_1809));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3937, BgL_auxz00_3940);
						}
						((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_tmp1112z00_1809));
						{	/* Globalize/node.scm 81 */
							long BgL_arg1535z00_1812;

							BgL_arg1535z00_1812 =
								BGL_CLASS_NUM(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1112z00_1809)),
								BgL_arg1535z00_1812);
						}
						((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_tmp1112z00_1809));
					}
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_3951;

						{
							obj_t BgL_auxz00_3952;

							{	/* Globalize/node.scm 83 */
								BgL_objectz00_bglt BgL_tmpz00_3953;

								BgL_tmpz00_3953 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1112z00_1809));
								BgL_auxz00_3952 = BGL_OBJECT_WIDENING(BgL_tmpz00_3953);
							}
							BgL_auxz00_3951 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3952);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3951))->
								BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					}
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_3959;

						{
							obj_t BgL_auxz00_3960;

							{	/* Globalize/node.scm 82 */
								BgL_objectz00_bglt BgL_tmpz00_3961;

								BgL_tmpz00_3961 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1112z00_1809));
								BgL_auxz00_3960 = BGL_OBJECT_WIDENING(BgL_tmpz00_3961);
							}
							BgL_auxz00_3959 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3960);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3959))->
								BgL_freezd2markzd2) = ((long) -10L), BUNSPEC);
					}
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_3967;

						{
							obj_t BgL_auxz00_3968;

							{	/* Globalize/node.scm 82 */
								BgL_objectz00_bglt BgL_tmpz00_3969;

								BgL_tmpz00_3969 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1112z00_1809));
								BgL_auxz00_3968 = BGL_OBJECT_WIDENING(BgL_tmpz00_3969);
							}
							BgL_auxz00_3967 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3968);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3967))->
								BgL_markz00) = ((long) -10L), BUNSPEC);
					}
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_3975;

						{
							obj_t BgL_auxz00_3976;

							{	/* Globalize/node.scm 82 */
								BgL_objectz00_bglt BgL_tmpz00_3977;

								BgL_tmpz00_3977 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1112z00_1809));
								BgL_auxz00_3976 = BGL_OBJECT_WIDENING(BgL_tmpz00_3977);
							}
							BgL_auxz00_3975 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3976);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3975))->
								BgL_celledzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					}
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_3983;

						{
							obj_t BgL_auxz00_3984;

							{	/* Globalize/node.scm 82 */
								BgL_objectz00_bglt BgL_tmpz00_3985;

								BgL_tmpz00_3985 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1112z00_1809));
								BgL_auxz00_3984 = BGL_OBJECT_WIDENING(BgL_tmpz00_3985);
							}
							BgL_auxz00_3983 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3984);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3983))->
								BgL_stackablez00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					}
					((BgL_svarz00_bglt) BgL_tmp1112z00_1809);
				}
				return BgL_varz00_1807;
			}
		}

	}



/* cell-formals */
	BgL_nodez00_bglt BGl_cellzd2formalszd2zzglobaliza7e_nodeza7(obj_t
		BgL_celledz00_8, BgL_nodez00_bglt BgL_bodyz00_9)
	{
		{	/* Globalize/node.scm 89 */
			if (NULLP(BgL_celledz00_8))
				{	/* Globalize/node.scm 90 */
					return BgL_bodyz00_9;
				}
			else
				{	/* Globalize/node.scm 92 */
					obj_t BgL_locz00_1816;

					BgL_locz00_1816 =
						(((BgL_nodez00_bglt) COBJECT(BgL_bodyz00_9))->BgL_locz00);
					{	/* Globalize/node.scm 93 */
						BgL_letzd2varzd2_bglt BgL_new1118z00_1817;

						{	/* Globalize/node.scm 94 */
							BgL_letzd2varzd2_bglt BgL_new1116z00_1839;

							BgL_new1116z00_1839 =
								((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_letzd2varzd2_bgl))));
							{	/* Globalize/node.scm 94 */
								long BgL_arg1573z00_1840;

								BgL_arg1573z00_1840 =
									BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1116z00_1839),
									BgL_arg1573z00_1840);
							}
							{	/* Globalize/node.scm 94 */
								BgL_objectz00_bglt BgL_tmpz00_3999;

								BgL_tmpz00_3999 = ((BgL_objectz00_bglt) BgL_new1116z00_1839);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3999, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1116z00_1839);
							BgL_new1118z00_1817 = BgL_new1116z00_1839;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1118z00_1817)))->BgL_locz00) =
							((obj_t) BgL_locz00_1816), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1118z00_1817)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt) COBJECT(BgL_bodyz00_9))->
									BgL_typez00)), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1118z00_1817)))->BgL_sidezd2effectzd2) =
							((obj_t) BUNSPEC), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1118z00_1817)))->BgL_keyz00) =
							((obj_t) BINT(-1L)), BUNSPEC);
						{
							obj_t BgL_auxz00_4013;

							{	/* Globalize/node.scm 97 */
								obj_t BgL_head1350z00_1820;

								BgL_head1350z00_1820 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1348z00_1822;
									obj_t BgL_tail1351z00_1823;

									BgL_l1348z00_1822 = BgL_celledz00_8;
									BgL_tail1351z00_1823 = BgL_head1350z00_1820;
								BgL_zc3z04anonymousza31543ze3z87_1824:
									if (NULLP(BgL_l1348z00_1822))
										{	/* Globalize/node.scm 97 */
											BgL_auxz00_4013 = CDR(BgL_head1350z00_1820);
										}
									else
										{	/* Globalize/node.scm 97 */
											obj_t BgL_newtail1352z00_1826;

											{	/* Globalize/node.scm 97 */
												obj_t BgL_arg1552z00_1828;

												{	/* Globalize/node.scm 97 */
													obj_t BgL_ozd2nzd2_1829;

													BgL_ozd2nzd2_1829 = CAR(((obj_t) BgL_l1348z00_1822));
													{	/* Globalize/node.scm 98 */
														obj_t BgL_arg1553z00_1830;
														BgL_makezd2boxzd2_bglt BgL_arg1559z00_1831;

														BgL_arg1553z00_1830 =
															CDR(((obj_t) BgL_ozd2nzd2_1829));
														{	/* Globalize/node.scm 100 */
															BgL_refz00_bglt BgL_arg1561z00_1832;
															obj_t BgL_arg1564z00_1833;

															{	/* Globalize/node.scm 100 */
																BgL_refz00_bglt BgL_new1120z00_1834;

																{	/* Globalize/node.scm 103 */
																	BgL_refz00_bglt BgL_new1119z00_1836;

																	BgL_new1119z00_1836 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Globalize/node.scm 103 */
																		long BgL_arg1571z00_1837;

																		{	/* Globalize/node.scm 103 */
																			obj_t BgL_classz00_2596;

																			BgL_classz00_2596 =
																				BGl_refz00zzast_nodez00;
																			BgL_arg1571z00_1837 =
																				BGL_CLASS_NUM(BgL_classz00_2596);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1119z00_1836),
																			BgL_arg1571z00_1837);
																	}
																	{	/* Globalize/node.scm 103 */
																		BgL_objectz00_bglt BgL_tmpz00_4026;

																		BgL_tmpz00_4026 =
																			((BgL_objectz00_bglt)
																			BgL_new1119z00_1836);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4026,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1119z00_1836);
																	BgL_new1120z00_1834 = BgL_new1119z00_1836;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1120z00_1834)))->BgL_locz00) =
																	((obj_t) BgL_locz00_1816), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1120z00_1834)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						CAR(((obj_t)
																								BgL_ozd2nzd2_1829)))))->
																			BgL_typez00)), BUNSPEC);
																((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																					BgL_new1120z00_1834)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt) CAR(((obj_t)
																					BgL_ozd2nzd2_1829)))), BUNSPEC);
																BgL_arg1561z00_1832 = BgL_new1120z00_1834;
															}
															BgL_arg1564z00_1833 =
																CAR(((obj_t) BgL_ozd2nzd2_1829));
															BgL_arg1559z00_1831 =
																BGl_azd2makezd2cellz00zzglobaliza7e_nodeza7(
																((BgL_nodez00_bglt) BgL_arg1561z00_1832),
																((BgL_variablez00_bglt) BgL_arg1564z00_1833));
														}
														BgL_arg1552z00_1828 =
															MAKE_YOUNG_PAIR(BgL_arg1553z00_1830,
															((obj_t) BgL_arg1559z00_1831));
												}}
												BgL_newtail1352z00_1826 =
													MAKE_YOUNG_PAIR(BgL_arg1552z00_1828, BNIL);
											}
											SET_CDR(BgL_tail1351z00_1823, BgL_newtail1352z00_1826);
											{	/* Globalize/node.scm 97 */
												obj_t BgL_arg1546z00_1827;

												BgL_arg1546z00_1827 = CDR(((obj_t) BgL_l1348z00_1822));
												{
													obj_t BgL_tail1351z00_4055;
													obj_t BgL_l1348z00_4054;

													BgL_l1348z00_4054 = BgL_arg1546z00_1827;
													BgL_tail1351z00_4055 = BgL_newtail1352z00_1826;
													BgL_tail1351z00_1823 = BgL_tail1351z00_4055;
													BgL_l1348z00_1822 = BgL_l1348z00_4054;
													goto BgL_zc3z04anonymousza31543ze3z87_1824;
												}
											}
										}
								}
							}
							((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1118z00_1817))->
									BgL_bindingsz00) = ((obj_t) BgL_auxz00_4013), BUNSPEC);
						}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1118z00_1817))->
								BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_bodyz00_9), BUNSPEC);
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1118z00_1817))->
								BgL_removablezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
						return ((BgL_nodez00_bglt) BgL_new1118z00_1817);
					}
				}
		}

	}



/* a-make-cell */
	BgL_makezd2boxzd2_bglt
		BGl_azd2makezd2cellz00zzglobaliza7e_nodeza7(BgL_nodez00_bglt BgL_nodez00_10,
		BgL_variablez00_bglt BgL_variablez00_11)
	{
		{	/* Globalize/node.scm 110 */
			{	/* Globalize/node.scm 112 */
				obj_t BgL_vz00_2607;

				BgL_vz00_2607 = CNST_TABLE_REF(0);
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_variablez00_11))))->BgL_accessz00) =
					((obj_t) BgL_vz00_2607), BUNSPEC);
			}
			{	/* Globalize/node.scm 113 */
				BgL_valuez00_bglt BgL_arg1575z00_1842;

				BgL_arg1575z00_1842 =
					(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_11))->BgL_valuez00);
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_4065;

					{
						obj_t BgL_auxz00_4066;

						{	/* Globalize/node.scm 113 */
							BgL_objectz00_bglt BgL_tmpz00_4067;

							BgL_tmpz00_4067 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_arg1575z00_1842));
							BgL_auxz00_4066 = BGL_OBJECT_WIDENING(BgL_tmpz00_4067);
						}
						BgL_auxz00_4065 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_4066);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4065))->
							BgL_celledzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
				}
			}
			{	/* Globalize/node.scm 114 */
				BgL_makezd2boxzd2_bglt BgL_new1123z00_1843;

				{	/* Globalize/node.scm 117 */
					BgL_makezd2boxzd2_bglt BgL_new1122z00_1846;

					BgL_new1122z00_1846 =
						((BgL_makezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_makezd2boxzd2_bgl))));
					{	/* Globalize/node.scm 117 */
						long BgL_arg1585z00_1847;

						BgL_arg1585z00_1847 = BGL_CLASS_NUM(BGl_makezd2boxzd2zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1122z00_1846), BgL_arg1585z00_1847);
					}
					{	/* Globalize/node.scm 117 */
						BgL_objectz00_bglt BgL_tmpz00_4077;

						BgL_tmpz00_4077 = ((BgL_objectz00_bglt) BgL_new1122z00_1846);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4077, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1122z00_1846);
					BgL_new1123z00_1843 = BgL_new1122z00_1846;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1123z00_1843)))->BgL_locz00) =
					((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_10))->BgL_locz00)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1123z00_1843)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt)
							BGl_za2cellza2z00zztype_cachez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1123z00_1843)))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1123z00_1843)))->BgL_keyz00) =
					((obj_t) BINT(-1L)), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1123z00_1843))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_nodez00_10), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_4093;

					{	/* Globalize/node.scm 116 */
						BgL_typez00_bglt BgL_arg1576z00_1844;

						BgL_arg1576z00_1844 =
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_11))->
							BgL_typez00);
						BgL_auxz00_4093 =
							BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
							(BgL_arg1576z00_1844);
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1123z00_1843))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_4093), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_4097;

					{	/* Globalize/node.scm 119 */
						BgL_svarz00_bglt BgL_oz00_2618;

						BgL_oz00_2618 =
							((BgL_svarz00_bglt)
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_11))->
								BgL_valuez00));
						{	/* Globalize/node.scm 119 */
							bool_t BgL_tmpz00_4100;

							{
								BgL_svarzf2ginfozf2_bglt BgL_auxz00_4101;

								{
									obj_t BgL_auxz00_4102;

									{	/* Globalize/node.scm 119 */
										BgL_objectz00_bglt BgL_tmpz00_4103;

										BgL_tmpz00_4103 = ((BgL_objectz00_bglt) BgL_oz00_2618);
										BgL_auxz00_4102 = BGL_OBJECT_WIDENING(BgL_tmpz00_4103);
									}
									BgL_auxz00_4101 =
										((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_4102);
								}
								BgL_tmpz00_4100 =
									(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4101))->
									BgL_stackablez00);
							}
							BgL_auxz00_4097 = BBOOL(BgL_tmpz00_4100);
					}}
					((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1123z00_1843))->
							BgL_stackablez00) = ((obj_t) BgL_auxz00_4097), BUNSPEC);
				}
				return BgL_new1123z00_1843;
			}
		}

	}



/* celled? */
	bool_t BGl_celledzf3zf3zzglobaliza7e_nodeza7(BgL_variablez00_bglt
		BgL_variablez00_12)
	{
		{	/* Globalize/node.scm 124 */
			{	/* Globalize/node.scm 125 */
				bool_t BgL_test2116z00_4110;

				{	/* Globalize/node.scm 125 */
					obj_t BgL_classz00_2620;

					BgL_classz00_2620 = BGl_localz00zzast_varz00;
					{	/* Globalize/node.scm 125 */
						BgL_objectz00_bglt BgL_arg1807z00_2622;

						{	/* Globalize/node.scm 125 */
							obj_t BgL_tmpz00_4111;

							BgL_tmpz00_4111 = ((obj_t) BgL_variablez00_12);
							BgL_arg1807z00_2622 = (BgL_objectz00_bglt) (BgL_tmpz00_4111);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Globalize/node.scm 125 */
								long BgL_idxz00_2628;

								BgL_idxz00_2628 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2622);
								BgL_test2116z00_4110 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2628 + 2L)) == BgL_classz00_2620);
							}
						else
							{	/* Globalize/node.scm 125 */
								bool_t BgL_res2022z00_2653;

								{	/* Globalize/node.scm 125 */
									obj_t BgL_oclassz00_2636;

									{	/* Globalize/node.scm 125 */
										obj_t BgL_arg1815z00_2644;
										long BgL_arg1816z00_2645;

										BgL_arg1815z00_2644 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Globalize/node.scm 125 */
											long BgL_arg1817z00_2646;

											BgL_arg1817z00_2646 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2622);
											BgL_arg1816z00_2645 = (BgL_arg1817z00_2646 - OBJECT_TYPE);
										}
										BgL_oclassz00_2636 =
											VECTOR_REF(BgL_arg1815z00_2644, BgL_arg1816z00_2645);
									}
									{	/* Globalize/node.scm 125 */
										bool_t BgL__ortest_1115z00_2637;

										BgL__ortest_1115z00_2637 =
											(BgL_classz00_2620 == BgL_oclassz00_2636);
										if (BgL__ortest_1115z00_2637)
											{	/* Globalize/node.scm 125 */
												BgL_res2022z00_2653 = BgL__ortest_1115z00_2637;
											}
										else
											{	/* Globalize/node.scm 125 */
												long BgL_odepthz00_2638;

												{	/* Globalize/node.scm 125 */
													obj_t BgL_arg1804z00_2639;

													BgL_arg1804z00_2639 = (BgL_oclassz00_2636);
													BgL_odepthz00_2638 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2639);
												}
												if ((2L < BgL_odepthz00_2638))
													{	/* Globalize/node.scm 125 */
														obj_t BgL_arg1802z00_2641;

														{	/* Globalize/node.scm 125 */
															obj_t BgL_arg1803z00_2642;

															BgL_arg1803z00_2642 = (BgL_oclassz00_2636);
															BgL_arg1802z00_2641 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2642,
																2L);
														}
														BgL_res2022z00_2653 =
															(BgL_arg1802z00_2641 == BgL_classz00_2620);
													}
												else
													{	/* Globalize/node.scm 125 */
														BgL_res2022z00_2653 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2116z00_4110 = BgL_res2022z00_2653;
							}
					}
				}
				if (BgL_test2116z00_4110)
					{	/* Globalize/node.scm 126 */
						bool_t BgL_test2120z00_4133;

						{	/* Globalize/node.scm 126 */
							BgL_valuez00_bglt BgL_arg1594z00_1855;

							BgL_arg1594z00_1855 =
								(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_12))->
								BgL_valuez00);
							{	/* Globalize/node.scm 126 */
								obj_t BgL_classz00_2655;

								BgL_classz00_2655 = BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
								{	/* Globalize/node.scm 126 */
									BgL_objectz00_bglt BgL_arg1807z00_2657;

									{	/* Globalize/node.scm 126 */
										obj_t BgL_tmpz00_4135;

										BgL_tmpz00_4135 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1594z00_1855));
										BgL_arg1807z00_2657 =
											(BgL_objectz00_bglt) (BgL_tmpz00_4135);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Globalize/node.scm 126 */
											long BgL_idxz00_2663;

											BgL_idxz00_2663 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2657);
											BgL_test2120z00_4133 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2663 + 3L)) == BgL_classz00_2655);
										}
									else
										{	/* Globalize/node.scm 126 */
											bool_t BgL_res2023z00_2688;

											{	/* Globalize/node.scm 126 */
												obj_t BgL_oclassz00_2671;

												{	/* Globalize/node.scm 126 */
													obj_t BgL_arg1815z00_2679;
													long BgL_arg1816z00_2680;

													BgL_arg1815z00_2679 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Globalize/node.scm 126 */
														long BgL_arg1817z00_2681;

														BgL_arg1817z00_2681 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2657);
														BgL_arg1816z00_2680 =
															(BgL_arg1817z00_2681 - OBJECT_TYPE);
													}
													BgL_oclassz00_2671 =
														VECTOR_REF(BgL_arg1815z00_2679,
														BgL_arg1816z00_2680);
												}
												{	/* Globalize/node.scm 126 */
													bool_t BgL__ortest_1115z00_2672;

													BgL__ortest_1115z00_2672 =
														(BgL_classz00_2655 == BgL_oclassz00_2671);
													if (BgL__ortest_1115z00_2672)
														{	/* Globalize/node.scm 126 */
															BgL_res2023z00_2688 = BgL__ortest_1115z00_2672;
														}
													else
														{	/* Globalize/node.scm 126 */
															long BgL_odepthz00_2673;

															{	/* Globalize/node.scm 126 */
																obj_t BgL_arg1804z00_2674;

																BgL_arg1804z00_2674 = (BgL_oclassz00_2671);
																BgL_odepthz00_2673 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2674);
															}
															if ((3L < BgL_odepthz00_2673))
																{	/* Globalize/node.scm 126 */
																	obj_t BgL_arg1802z00_2676;

																	{	/* Globalize/node.scm 126 */
																		obj_t BgL_arg1803z00_2677;

																		BgL_arg1803z00_2677 = (BgL_oclassz00_2671);
																		BgL_arg1802z00_2676 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2677, 3L);
																	}
																	BgL_res2023z00_2688 =
																		(BgL_arg1802z00_2676 == BgL_classz00_2655);
																}
															else
																{	/* Globalize/node.scm 126 */
																	BgL_res2023z00_2688 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2120z00_4133 = BgL_res2023z00_2688;
										}
								}
							}
						}
						if (BgL_test2120z00_4133)
							{	/* Globalize/node.scm 127 */
								bool_t BgL__ortest_1126z00_1850;

								{	/* Globalize/node.scm 127 */
									BgL_svarz00_bglt BgL_oz00_2690;

									BgL_oz00_2690 =
										((BgL_svarz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_12))->
											BgL_valuez00));
									{
										BgL_svarzf2ginfozf2_bglt BgL_auxz00_4160;

										{
											obj_t BgL_auxz00_4161;

											{	/* Globalize/node.scm 127 */
												BgL_objectz00_bglt BgL_tmpz00_4162;

												BgL_tmpz00_4162 = ((BgL_objectz00_bglt) BgL_oz00_2690);
												BgL_auxz00_4161 = BGL_OBJECT_WIDENING(BgL_tmpz00_4162);
											}
											BgL_auxz00_4160 =
												((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_4161);
										}
										BgL__ortest_1126z00_1850 =
											(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4160))->
											BgL_celledzf3zf3);
									}
								}
								if (BgL__ortest_1126z00_1850)
									{	/* Globalize/node.scm 127 */
										return BgL__ortest_1126z00_1850;
									}
								else
									{	/* Globalize/node.scm 127 */
										if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
													(((BgL_variablez00_bglt)
															COBJECT(BgL_variablez00_12))->BgL_accessz00),
													CNST_TABLE_REF(1))))
											{	/* Globalize/node.scm 129 */
												BgL_svarz00_bglt BgL_oz00_2694;

												BgL_oz00_2694 =
													((BgL_svarz00_bglt)
													(((BgL_variablez00_bglt)
															COBJECT(BgL_variablez00_12))->BgL_valuez00));
												{
													BgL_svarzf2ginfozf2_bglt BgL_auxz00_4175;

													{
														obj_t BgL_auxz00_4176;

														{	/* Globalize/node.scm 129 */
															BgL_objectz00_bglt BgL_tmpz00_4177;

															BgL_tmpz00_4177 =
																((BgL_objectz00_bglt) BgL_oz00_2694);
															BgL_auxz00_4176 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_4177);
														}
														BgL_auxz00_4175 =
															((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_4176);
													}
													return
														(((BgL_svarzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_4175))->BgL_kapturedzf3zf3);
												}
											}
										else
											{	/* Globalize/node.scm 128 */
												return ((bool_t) 0);
											}
									}
							}
						else
							{	/* Globalize/node.scm 126 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Globalize/node.scm 125 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* glo*! */
	obj_t BGl_gloza2z12zb0zzglobaliza7e_nodeza7(obj_t BgL_nodeza2za2_59,
		obj_t BgL_integratorz00_60)
	{
		{	/* Globalize/node.scm 493 */
			{
				obj_t BgL_nodeza2za2_1857;

				BgL_nodeza2za2_1857 = BgL_nodeza2za2_59;
			BgL_zc3z04anonymousza31595ze3z87_1858:
				if (NULLP(BgL_nodeza2za2_1857))
					{	/* Globalize/node.scm 495 */
						return CNST_TABLE_REF(2);
					}
				else
					{	/* Globalize/node.scm 495 */
						{	/* Globalize/node.scm 498 */
							BgL_nodez00_bglt BgL_arg1602z00_1860;

							{	/* Globalize/node.scm 498 */
								obj_t BgL_arg1605z00_1861;

								BgL_arg1605z00_1861 = CAR(((obj_t) BgL_nodeza2za2_1857));
								BgL_arg1602z00_1860 =
									BGl_gloz12z12zzglobaliza7e_nodeza7(
									((BgL_nodez00_bglt) BgL_arg1605z00_1861),
									((BgL_variablez00_bglt) BgL_integratorz00_60));
							}
							{	/* Globalize/node.scm 498 */
								obj_t BgL_auxz00_4192;
								obj_t BgL_tmpz00_4190;

								BgL_auxz00_4192 = ((obj_t) BgL_arg1602z00_1860);
								BgL_tmpz00_4190 = ((obj_t) BgL_nodeza2za2_1857);
								SET_CAR(BgL_tmpz00_4190, BgL_auxz00_4192);
							}
						}
						{	/* Globalize/node.scm 499 */
							obj_t BgL_arg1606z00_1862;

							BgL_arg1606z00_1862 = CDR(((obj_t) BgL_nodeza2za2_1857));
							{
								obj_t BgL_nodeza2za2_4197;

								BgL_nodeza2za2_4197 = BgL_arg1606z00_1862;
								BgL_nodeza2za2_1857 = BgL_nodeza2za2_4197;
								goto BgL_zc3z04anonymousza31595ze3z87_1858;
							}
						}
					}
			}
		}

	}



/* make-escaping-bindings */
	BgL_letzd2varzd2_bglt
		BGl_makezd2escapingzd2bindingsz00zzglobaliza7e_nodeza7(obj_t
		BgL_ebindingsz00_61, BgL_nodez00_bglt BgL_nodez00_62,
		BgL_variablez00_bglt BgL_integratorz00_63)
	{
		{	/* Globalize/node.scm 504 */
			{
				obj_t BgL_ebindingsz00_1867;
				obj_t BgL_bindingsz00_1868;
				obj_t BgL_setsz00_1869;

				BgL_ebindingsz00_1867 = BgL_ebindingsz00_61;
				BgL_bindingsz00_1868 = BNIL;
				BgL_setsz00_1869 = BNIL;
			BgL_zc3z04anonymousza31607ze3z87_1870:
				if (NULLP(BgL_ebindingsz00_1867))
					{	/* Globalize/node.scm 512 */
						BgL_letzd2varzd2_bglt BgL_new1180z00_1872;

						{	/* Globalize/node.scm 513 */
							BgL_letzd2varzd2_bglt BgL_new1179z00_1879;

							BgL_new1179z00_1879 =
								((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_letzd2varzd2_bgl))));
							{	/* Globalize/node.scm 513 */
								long BgL_arg1615z00_1880;

								BgL_arg1615z00_1880 =
									BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1179z00_1879),
									BgL_arg1615z00_1880);
							}
							{	/* Globalize/node.scm 513 */
								BgL_objectz00_bglt BgL_tmpz00_4204;

								BgL_tmpz00_4204 = ((BgL_objectz00_bglt) BgL_new1179z00_1879);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4204, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1179z00_1879);
							BgL_new1180z00_1872 = BgL_new1179z00_1879;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1180z00_1872)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_62))->
									BgL_locz00)), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1180z00_1872)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT(BgL_nodez00_62))->BgL_typez00)), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1180z00_1872)))->BgL_sidezd2effectzd2) =
							((obj_t) BUNSPEC), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1180z00_1872)))->BgL_keyz00) =
							((obj_t) BINT(-1L)), BUNSPEC);
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1180z00_1872))->
								BgL_bindingsz00) = ((obj_t) BgL_bindingsz00_1868), BUNSPEC);
						{
							BgL_nodez00_bglt BgL_auxz00_4220;

							if (NULLP(BgL_setsz00_1869))
								{	/* Globalize/node.scm 516 */
									BgL_auxz00_4220 = BgL_nodez00_62;
								}
							else
								{	/* Globalize/node.scm 518 */
									BgL_sequencez00_bglt BgL_new1183z00_1874;

									{	/* Globalize/node.scm 519 */
										BgL_sequencez00_bglt BgL_new1182z00_1877;

										BgL_new1182z00_1877 =
											((BgL_sequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_sequencez00_bgl))));
										{	/* Globalize/node.scm 519 */
											long BgL_arg1613z00_1878;

											{	/* Globalize/node.scm 519 */
												obj_t BgL_classz00_2705;

												BgL_classz00_2705 = BGl_sequencez00zzast_nodez00;
												BgL_arg1613z00_1878 = BGL_CLASS_NUM(BgL_classz00_2705);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1182z00_1877),
												BgL_arg1613z00_1878);
										}
										{	/* Globalize/node.scm 519 */
											BgL_objectz00_bglt BgL_tmpz00_4227;

											BgL_tmpz00_4227 =
												((BgL_objectz00_bglt) BgL_new1182z00_1877);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4227, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1182z00_1877);
										BgL_new1183z00_1874 = BgL_new1182z00_1877;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1183z00_1874)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_62))->
												BgL_locz00)), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1183z00_1874)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_nodez00_bglt)
													COBJECT(BgL_nodez00_62))->BgL_typez00)), BUNSPEC);
									((((BgL_nodezf2effectzf2_bglt)
												COBJECT(((BgL_nodezf2effectzf2_bglt)
														BgL_new1183z00_1874)))->BgL_sidezd2effectzd2) =
										((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_nodezf2effectzf2_bglt)
												COBJECT(((BgL_nodezf2effectzf2_bglt)
														BgL_new1183z00_1874)))->BgL_keyz00) =
										((obj_t) BINT(-1L)), BUNSPEC);
									{
										obj_t BgL_auxz00_4242;

										{	/* Globalize/node.scm 521 */
											obj_t BgL_arg1611z00_1875;

											{	/* Globalize/node.scm 521 */
												obj_t BgL_list1612z00_1876;

												BgL_list1612z00_1876 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_nodez00_62), BNIL);
												BgL_arg1611z00_1875 = BgL_list1612z00_1876;
											}
											BgL_auxz00_4242 =
												BGl_appendzd221011zd2zzglobaliza7e_nodeza7
												(BgL_setsz00_1869, BgL_arg1611z00_1875);
										}
										((((BgL_sequencez00_bglt) COBJECT(BgL_new1183z00_1874))->
												BgL_nodesz00) = ((obj_t) BgL_auxz00_4242), BUNSPEC);
									}
									((((BgL_sequencez00_bglt) COBJECT(BgL_new1183z00_1874))->
											BgL_unsafez00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
									((((BgL_sequencez00_bglt) COBJECT(BgL_new1183z00_1874))->
											BgL_metaz00) = ((obj_t) BNIL), BUNSPEC);
									BgL_auxz00_4220 = ((BgL_nodez00_bglt) BgL_new1183z00_1874);
								}
							((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1180z00_1872))->
									BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_4220), BUNSPEC);
						}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1180z00_1872))->
								BgL_removablezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
						return BgL_new1180z00_1872;
					}
				else
					{	/* Globalize/node.scm 522 */
						obj_t BgL_localz00_1881;

						BgL_localz00_1881 = CAR(((obj_t) BgL_ebindingsz00_1867));
						{	/* Globalize/node.scm 522 */
							obj_t BgL_newz00_1882;

							{	/* Globalize/node.scm 523 */
								obj_t BgL_arg1651z00_1893;

								BgL_arg1651z00_1893 =
									(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_62))->BgL_locz00);
								BgL_newz00_1882 =
									BGl_thezd2closurezd2zzglobaliza7e_freeza7(
									((BgL_variablez00_bglt) BgL_localz00_1881),
									BgL_arg1651z00_1893);
							}
							{	/* Globalize/node.scm 523 */
								obj_t BgL_nsetsz00_1883;

								{	/* Globalize/node.scm 525 */
									obj_t BgL_arg1642z00_1890;
									obj_t BgL_arg1646z00_1891;

									BgL_arg1642z00_1890 =
										(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_62))->BgL_locz00);
									{	/* Globalize/node.scm 526 */
										BgL_sfunz00_bglt BgL_oz00_2716;

										BgL_oz00_2716 =
											((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_localz00_1881))))->
												BgL_valuez00));
										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4262;

											{
												obj_t BgL_auxz00_4263;

												{	/* Globalize/node.scm 526 */
													BgL_objectz00_bglt BgL_tmpz00_4264;

													BgL_tmpz00_4264 =
														((BgL_objectz00_bglt) BgL_oz00_2716);
													BgL_auxz00_4263 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4264);
												}
												BgL_auxz00_4262 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4263);
											}
											BgL_arg1646z00_1891 =
												(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4262))->
												BgL_kapturedz00);
										}
									}
									BgL_nsetsz00_1883 =
										BGl_makezd2setszd2zzglobaliza7e_nodeza7(BgL_newz00_1882,
										BgL_arg1642z00_1890, BgL_arg1646z00_1891,
										BgL_integratorz00_63);
								}
								{	/* Globalize/node.scm 524 */

									{	/* Globalize/node.scm 528 */
										obj_t BgL_arg1616z00_1884;
										obj_t BgL_arg1625z00_1885;
										obj_t BgL_arg1626z00_1886;

										BgL_arg1616z00_1884 = CDR(((obj_t) BgL_ebindingsz00_1867));
										{	/* Globalize/node.scm 529 */
											obj_t BgL_arg1627z00_1887;

											{	/* Globalize/node.scm 529 */
												BgL_appz00_bglt BgL_arg1629z00_1888;

												BgL_arg1629z00_1888 =
													BGl_azd2makezd2procedurez00zzglobaliza7e_nodeza7(
													((BgL_localz00_bglt) BgL_localz00_1881));
												BgL_arg1627z00_1887 =
													MAKE_YOUNG_PAIR(BgL_newz00_1882,
													((obj_t) BgL_arg1629z00_1888));
											}
											BgL_arg1625z00_1885 =
												MAKE_YOUNG_PAIR(BgL_arg1627z00_1887,
												BgL_bindingsz00_1868);
										}
										if (NULLP(BgL_nsetsz00_1883))
											{	/* Globalize/node.scm 531 */
												BgL_arg1626z00_1886 = BgL_setsz00_1869;
											}
										else
											{	/* Globalize/node.scm 531 */
												BgL_arg1626z00_1886 =
													MAKE_YOUNG_PAIR(BgL_nsetsz00_1883, BgL_setsz00_1869);
											}
										{
											obj_t BgL_setsz00_4282;
											obj_t BgL_bindingsz00_4281;
											obj_t BgL_ebindingsz00_4280;

											BgL_ebindingsz00_4280 = BgL_arg1616z00_1884;
											BgL_bindingsz00_4281 = BgL_arg1625z00_1885;
											BgL_setsz00_4282 = BgL_arg1626z00_1886;
											BgL_setsz00_1869 = BgL_setsz00_4282;
											BgL_bindingsz00_1868 = BgL_bindingsz00_4281;
											BgL_ebindingsz00_1867 = BgL_ebindingsz00_4280;
											goto BgL_zc3z04anonymousza31607ze3z87_1870;
										}
									}
								}
							}
						}
					}
			}
		}

	}



/* globalize-local-fun! */
	obj_t
		BGl_globaliza7ezd2localzd2funz12zb5zzglobaliza7e_nodeza7(BgL_localz00_bglt
		BgL_localz00_64, BgL_variablez00_bglt BgL_integratorz00_65)
	{
		{	/* Globalize/node.scm 538 */
			{	/* Globalize/node.scm 541 */
				BgL_valuez00_bglt BgL_funz00_1895;

				BgL_funz00_1895 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_localz00_64)))->BgL_valuez00);
				{	/* Globalize/node.scm 541 */
					obj_t BgL_obodyz00_1896;

					BgL_obodyz00_1896 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1895)))->BgL_bodyz00);
					{	/* Globalize/node.scm 542 */

						if ((((obj_t) BgL_localz00_64) == ((obj_t) BgL_integratorz00_65)))
							{	/* Globalize/node.scm 544 */
								BgL_nodez00_bglt BgL_arg1654z00_1897;

								BgL_arg1654z00_1897 =
									BGl_gloz12z12zzglobaliza7e_nodeza7(
									((BgL_nodez00_bglt) BgL_obodyz00_1896), BgL_integratorz00_65);
								return
									((((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1895)))->BgL_bodyz00) =
									((obj_t) ((obj_t) BgL_arg1654z00_1897)), BUNSPEC);
							}
						else
							{	/* Globalize/node.scm 545 */
								obj_t BgL_celledz00_1898;

								{	/* Globalize/node.scm 545 */
									obj_t BgL_arg1688z00_1918;

									BgL_arg1688z00_1918 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1895)))->BgL_argsz00);
									BgL_celledz00_1898 =
										BGl_celledzd2bindingszd2zzglobaliza7e_nodeza7
										(BgL_arg1688z00_1918);
								}
								{
									obj_t BgL_l1362z00_1900;

									BgL_l1362z00_1900 = BgL_celledz00_1898;
								BgL_zc3z04anonymousza31655ze3z87_1901:
									if (PAIRP(BgL_l1362z00_1900))
										{	/* Globalize/node.scm 546 */
											{	/* Globalize/node.scm 547 */
												obj_t BgL_wzd2bzd2_1903;

												BgL_wzd2bzd2_1903 = CAR(BgL_l1362z00_1900);
												{	/* Globalize/node.scm 547 */
													obj_t BgL_arg1661z00_1904;
													obj_t BgL_arg1663z00_1905;

													BgL_arg1661z00_1904 =
														CAR(((obj_t) BgL_wzd2bzd2_1903));
													BgL_arg1663z00_1905 =
														CDR(((obj_t) BgL_wzd2bzd2_1903));
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			BgL_arg1661z00_1904))))->
															BgL_fastzd2alphazd2) =
														((obj_t) BgL_arg1663z00_1905), BUNSPEC);
												}
											}
											{
												obj_t BgL_l1362z00_4309;

												BgL_l1362z00_4309 = CDR(BgL_l1362z00_1900);
												BgL_l1362z00_1900 = BgL_l1362z00_4309;
												goto BgL_zc3z04anonymousza31655ze3z87_1901;
											}
										}
									else
										{	/* Globalize/node.scm 546 */
											((bool_t) 1);
										}
								}
								{	/* Globalize/node.scm 549 */
									BgL_nodez00_bglt BgL_nbody1z00_1908;

									BgL_nbody1z00_1908 =
										BGl_gloz12z12zzglobaliza7e_nodeza7(
										((BgL_nodez00_bglt) BgL_obodyz00_1896),
										BgL_integratorz00_65);
									{	/* Globalize/node.scm 549 */
										BgL_nodez00_bglt BgL_nbody2z00_1909;

										BgL_nbody2z00_1909 =
											BGl_cellzd2formalszd2zzglobaliza7e_nodeza7
											(BgL_celledz00_1898, BgL_nbody1z00_1908);
										{	/* Globalize/node.scm 550 */

											{
												obj_t BgL_l1364z00_1911;

												BgL_l1364z00_1911 = BgL_celledz00_1898;
											BgL_zc3z04anonymousza31676ze3z87_1912:
												if (PAIRP(BgL_l1364z00_1911))
													{	/* Globalize/node.scm 551 */
														{	/* Globalize/node.scm 552 */
															obj_t BgL_wzd2bzd2_1914;

															BgL_wzd2bzd2_1914 = CAR(BgL_l1364z00_1911);
															{	/* Globalize/node.scm 552 */
																obj_t BgL_arg1678z00_1915;

																BgL_arg1678z00_1915 =
																	CAR(((obj_t) BgL_wzd2bzd2_1914));
																((((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						BgL_arg1678z00_1915))))->
																		BgL_fastzd2alphazd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
															}
														}
														{
															obj_t BgL_l1364z00_4322;

															BgL_l1364z00_4322 = CDR(BgL_l1364z00_1911);
															BgL_l1364z00_1911 = BgL_l1364z00_4322;
															goto BgL_zc3z04anonymousza31676ze3z87_1912;
														}
													}
												else
													{	/* Globalize/node.scm 551 */
														((bool_t) 1);
													}
											}
											return
												((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1895)))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_nbody2z00_1909)), BUNSPEC);
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* a-make-procedure */
	BgL_appz00_bglt
		BGl_azd2makezd2procedurez00zzglobaliza7e_nodeza7(BgL_localz00_bglt
		BgL_localz00_66)
	{
		{	/* Globalize/node.scm 559 */
			{	/* Globalize/node.scm 560 */
				BgL_valuez00_bglt BgL_funz00_1919;

				BgL_funz00_1919 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_localz00_66)))->BgL_valuez00);
				{	/* Globalize/node.scm 560 */
					long BgL_arityz00_1920;

					BgL_arityz00_1920 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_sfunz00_bglt) BgL_funz00_1919))))->BgL_arityz00);
					{	/* Globalize/node.scm 561 */
						obj_t BgL_kapturedz00_1921;

						{	/* Globalize/node.scm 562 */
							BgL_sfunz00_bglt BgL_oz00_2736;

							BgL_oz00_2736 =
								((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_localz00_66)))->
									BgL_valuez00));
							{
								BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4335;

								{
									obj_t BgL_auxz00_4336;

									{	/* Globalize/node.scm 562 */
										BgL_objectz00_bglt BgL_tmpz00_4337;

										BgL_tmpz00_4337 = ((BgL_objectz00_bglt) BgL_oz00_2736);
										BgL_auxz00_4336 = BGL_OBJECT_WIDENING(BgL_tmpz00_4337);
									}
									BgL_auxz00_4335 =
										((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4336);
								}
								BgL_kapturedz00_1921 =
									(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4335))->
									BgL_kapturedz00);
						}}
						{	/* Globalize/node.scm 563 */
							obj_t BgL_makezd2pzd2_1923;

							if ((BgL_arityz00_1920 < 0L))
								{	/* Globalize/node.scm 564 */
									BgL_makezd2pzd2_1923 = CNST_TABLE_REF(3);
								}
							else
								{	/* Globalize/node.scm 564 */
									BgL_makezd2pzd2_1923 = CNST_TABLE_REF(4);
								}
							{	/* Globalize/node.scm 564 */
								obj_t BgL_vz00_1924;

								BgL_vz00_1924 =
									BGl_findzd2globalzf2modulez20zzast_envz00
									(BgL_makezd2pzd2_1923, CNST_TABLE_REF(5));
								{	/* Globalize/node.scm 565 */

									{	/* Globalize/node.scm 566 */
										BgL_appz00_bglt BgL_new1185z00_1925;

										{	/* Globalize/node.scm 567 */
											BgL_appz00_bglt BgL_new1184z00_1950;

											BgL_new1184z00_1950 =
												((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_appz00_bgl))));
											{	/* Globalize/node.scm 567 */
												long BgL_arg1717z00_1951;

												BgL_arg1717z00_1951 =
													BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1184z00_1950),
													BgL_arg1717z00_1951);
											}
											{	/* Globalize/node.scm 567 */
												BgL_objectz00_bglt BgL_tmpz00_4352;

												BgL_tmpz00_4352 =
													((BgL_objectz00_bglt) BgL_new1184z00_1950);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4352, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1184z00_1950);
											BgL_new1185z00_1925 = BgL_new1184z00_1950;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1185z00_1925)))->
												BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1185z00_1925)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_variablez00_bglt)
														COBJECT(((BgL_variablez00_bglt) BgL_vz00_1924)))->
													BgL_typez00)), BUNSPEC);
										((((BgL_nodezf2effectzf2_bglt)
													COBJECT(((BgL_nodezf2effectzf2_bglt)
															BgL_new1185z00_1925)))->BgL_sidezd2effectzd2) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_nodezf2effectzf2_bglt)
													COBJECT(((BgL_nodezf2effectzf2_bglt)
															BgL_new1185z00_1925)))->BgL_keyz00) =
											((obj_t) BINT(-1L)), BUNSPEC);
										{
											BgL_varz00_bglt BgL_auxz00_4367;

											{	/* Globalize/node.scm 573 */
												BgL_refz00_bglt BgL_new1187z00_1926;

												{	/* Globalize/node.scm 574 */
													BgL_refz00_bglt BgL_new1186z00_1927;

													BgL_new1186z00_1927 =
														((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_refz00_bgl))));
													{	/* Globalize/node.scm 574 */
														long BgL_arg1689z00_1928;

														{	/* Globalize/node.scm 574 */
															obj_t BgL_classz00_2744;

															BgL_classz00_2744 = BGl_refz00zzast_nodez00;
															BgL_arg1689z00_1928 =
																BGL_CLASS_NUM(BgL_classz00_2744);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1186z00_1927),
															BgL_arg1689z00_1928);
													}
													{	/* Globalize/node.scm 574 */
														BgL_objectz00_bglt BgL_tmpz00_4372;

														BgL_tmpz00_4372 =
															((BgL_objectz00_bglt) BgL_new1186z00_1927);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4372, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1186z00_1927);
													BgL_new1187z00_1926 = BgL_new1186z00_1927;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1187z00_1926)))->
														BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1187z00_1926)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_vz00_1924)))->BgL_typez00)), BUNSPEC);
												((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																	BgL_new1187z00_1926)))->BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															BgL_vz00_1924)), BUNSPEC);
												BgL_auxz00_4367 =
													((BgL_varz00_bglt) BgL_new1187z00_1926);
											}
											((((BgL_appz00_bglt) COBJECT(BgL_new1185z00_1925))->
													BgL_funz00) =
												((BgL_varz00_bglt) BgL_auxz00_4367), BUNSPEC);
										}
										{
											obj_t BgL_auxz00_4387;

											{	/* Globalize/node.scm 577 */
												BgL_refz00_bglt BgL_arg1691z00_1929;
												BgL_literalz00_bglt BgL_arg1692z00_1930;
												BgL_literalz00_bglt BgL_arg1699z00_1931;

												{	/* Globalize/node.scm 577 */
													BgL_refz00_bglt BgL_new1190z00_1935;

													{	/* Globalize/node.scm 578 */
														BgL_refz00_bglt BgL_new1189z00_1937;

														BgL_new1189z00_1937 =
															((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_refz00_bgl))));
														{	/* Globalize/node.scm 578 */
															long BgL_arg1705z00_1938;

															{	/* Globalize/node.scm 578 */
																obj_t BgL_classz00_2749;

																BgL_classz00_2749 = BGl_refz00zzast_nodez00;
																BgL_arg1705z00_1938 =
																	BGL_CLASS_NUM(BgL_classz00_2749);
															}
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1189z00_1937),
																BgL_arg1705z00_1938);
														}
														{	/* Globalize/node.scm 578 */
															BgL_objectz00_bglt BgL_tmpz00_4392;

															BgL_tmpz00_4392 =
																((BgL_objectz00_bglt) BgL_new1189z00_1937);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4392, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1189z00_1937);
														BgL_new1190z00_1935 = BgL_new1189z00_1937;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1190z00_1935)))->
															BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
													{
														BgL_typez00_bglt BgL_auxz00_4398;

														{	/* Globalize/node.scm 579 */
															BgL_globalz00_bglt BgL_arg1703z00_1936;

															BgL_arg1703z00_1936 =
																BGl_thezd2globalzd2zzglobaliza7e_localzd2ze3globalz96
																(BgL_localz00_66);
															BgL_auxz00_4398 =
																(((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt)
																			BgL_arg1703z00_1936)))->BgL_typez00);
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1190z00_1935)))->
																BgL_typez00) =
															((BgL_typez00_bglt) BgL_auxz00_4398), BUNSPEC);
													}
													((((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_new1190z00_1935)))->
															BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BGl_thezd2globalzd2zzglobaliza7e_localzd2ze3globalz96
																(BgL_localz00_66))), BUNSPEC);
													BgL_arg1691z00_1929 = BgL_new1190z00_1935;
												}
												{	/* Globalize/node.scm 581 */
													BgL_literalz00_bglt BgL_new1192z00_1939;

													{	/* Globalize/node.scm 582 */
														BgL_literalz00_bglt BgL_new1191z00_1940;

														BgL_new1191z00_1940 =
															((BgL_literalz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_literalz00_bgl))));
														{	/* Globalize/node.scm 582 */
															long BgL_arg1708z00_1941;

															{	/* Globalize/node.scm 582 */
																obj_t BgL_classz00_2754;

																BgL_classz00_2754 = BGl_literalz00zzast_nodez00;
																BgL_arg1708z00_1941 =
																	BGL_CLASS_NUM(BgL_classz00_2754);
															}
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1191z00_1940),
																BgL_arg1708z00_1941);
														}
														{	/* Globalize/node.scm 582 */
															BgL_objectz00_bglt BgL_tmpz00_4412;

															BgL_tmpz00_4412 =
																((BgL_objectz00_bglt) BgL_new1191z00_1940);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4412, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1191z00_1940);
														BgL_new1192z00_1939 = BgL_new1191z00_1940;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1192z00_1939)))->
															BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1192z00_1939)))->BgL_typez00) =
														((BgL_typez00_bglt)
															BGl_getzd2typezd2atomz00zztype_typeofz00(BINT
																(BgL_arityz00_1920))), BUNSPEC);
													((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
																		BgL_new1192z00_1939)))->BgL_valuez00) =
														((obj_t) BINT(BgL_arityz00_1920)), BUNSPEC);
													BgL_arg1692z00_1930 = BgL_new1192z00_1939;
												}
												{	/* Globalize/node.scm 585 */
													BgL_literalz00_bglt BgL_new1194z00_1942;

													{	/* Globalize/node.scm 586 */
														BgL_literalz00_bglt BgL_new1193z00_1943;

														BgL_new1193z00_1943 =
															((BgL_literalz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_literalz00_bgl))));
														{	/* Globalize/node.scm 586 */
															long BgL_arg1709z00_1944;

															{	/* Globalize/node.scm 586 */
																obj_t BgL_classz00_2758;

																BgL_classz00_2758 = BGl_literalz00zzast_nodez00;
																BgL_arg1709z00_1944 =
																	BGL_CLASS_NUM(BgL_classz00_2758);
															}
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1193z00_1943),
																BgL_arg1709z00_1944);
														}
														{	/* Globalize/node.scm 586 */
															BgL_objectz00_bglt BgL_tmpz00_4429;

															BgL_tmpz00_4429 =
																((BgL_objectz00_bglt) BgL_new1193z00_1943);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4429, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1193z00_1943);
														BgL_new1194z00_1942 = BgL_new1193z00_1943;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1194z00_1942)))->
															BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1194z00_1942)))->BgL_typez00) =
														((BgL_typez00_bglt)
															BGl_getzd2typezd2atomz00zztype_typeofz00(BINT
																(1L))), BUNSPEC);
													((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
																		BgL_new1194z00_1942)))->BgL_valuez00) =
														((obj_t)
															BINT(bgl_list_length(BgL_kapturedz00_1921))),
														BUNSPEC);
													BgL_arg1699z00_1931 = BgL_new1194z00_1942;
												}
												{	/* Globalize/node.scm 577 */
													obj_t BgL_list1700z00_1932;

													{	/* Globalize/node.scm 577 */
														obj_t BgL_arg1701z00_1933;

														{	/* Globalize/node.scm 577 */
															obj_t BgL_arg1702z00_1934;

															BgL_arg1702z00_1934 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1699z00_1931), BNIL);
															BgL_arg1701z00_1933 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1692z00_1930),
																BgL_arg1702z00_1934);
														}
														BgL_list1700z00_1932 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1691z00_1929),
															BgL_arg1701z00_1933);
													}
													BgL_auxz00_4387 = BgL_list1700z00_1932;
											}}
											((((BgL_appz00_bglt) COBJECT(BgL_new1185z00_1925))->
													BgL_argsz00) = ((obj_t) BgL_auxz00_4387), BUNSPEC);
										}
										{
											obj_t BgL_auxz00_4450;

											if (
												((((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_funz00_1919)))->
														BgL_stackablez00) == BTRUE))
												{	/* Globalize/node.scm 570 */
													BgL_valuez00_bglt BgL_gvz00_1947;

													BgL_gvz00_1947 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt) BgL_vz00_1924))))->
														BgL_valuez00);
													{	/* Globalize/node.scm 571 */
														bool_t BgL_test2135z00_4458;

														{	/* Globalize/node.scm 571 */
															obj_t BgL_classz00_2765;

															BgL_classz00_2765 = BGl_funz00zzast_varz00;
															{	/* Globalize/node.scm 571 */
																BgL_objectz00_bglt BgL_arg1807z00_2767;

																{	/* Globalize/node.scm 571 */
																	obj_t BgL_tmpz00_4459;

																	BgL_tmpz00_4459 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_gvz00_1947));
																	BgL_arg1807z00_2767 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_4459);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Globalize/node.scm 571 */
																		long BgL_idxz00_2773;

																		BgL_idxz00_2773 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2767);
																		{	/* Globalize/node.scm 571 */
																			obj_t BgL_arg1800z00_2774;

																			{	/* Globalize/node.scm 571 */
																				long BgL_arg1801z00_2775;

																				BgL_arg1801z00_2775 =
																					(BgL_idxz00_2773 + 2L);
																				{	/* Globalize/node.scm 571 */
																					obj_t BgL_vectorz00_2777;

																					BgL_vectorz00_2777 =
																						BGl_za2inheritancesza2z00zz__objectz00;
																					BgL_arg1800z00_2774 =
																						VECTOR_REF(BgL_vectorz00_2777,
																						BgL_arg1801z00_2775);
																			}}
																			BgL_test2135z00_4458 =
																				(BgL_arg1800z00_2774 ==
																				BgL_classz00_2765);
																	}}
																else
																	{	/* Globalize/node.scm 571 */
																		bool_t BgL_res2026z00_2798;

																		{	/* Globalize/node.scm 571 */
																			obj_t BgL_oclassz00_2781;

																			{	/* Globalize/node.scm 571 */
																				obj_t BgL_arg1815z00_2789;
																				long BgL_arg1816z00_2790;

																				BgL_arg1815z00_2789 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Globalize/node.scm 571 */
																					long BgL_arg1817z00_2791;

																					BgL_arg1817z00_2791 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2767);
																					BgL_arg1816z00_2790 =
																						(BgL_arg1817z00_2791 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2781 =
																					VECTOR_REF(BgL_arg1815z00_2789,
																					BgL_arg1816z00_2790);
																			}
																			{	/* Globalize/node.scm 571 */
																				bool_t BgL__ortest_1115z00_2782;

																				BgL__ortest_1115z00_2782 =
																					(BgL_classz00_2765 ==
																					BgL_oclassz00_2781);
																				if (BgL__ortest_1115z00_2782)
																					{	/* Globalize/node.scm 571 */
																						BgL_res2026z00_2798 =
																							BgL__ortest_1115z00_2782;
																					}
																				else
																					{	/* Globalize/node.scm 571 */
																						long BgL_odepthz00_2783;

																						{	/* Globalize/node.scm 571 */
																							obj_t BgL_arg1804z00_2784;

																							BgL_arg1804z00_2784 =
																								(BgL_oclassz00_2781);
																							BgL_odepthz00_2783 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2784);
																						}
																						if ((2L < BgL_odepthz00_2783))
																							{	/* Globalize/node.scm 571 */
																								obj_t BgL_arg1802z00_2786;

																								{	/* Globalize/node.scm 571 */
																									obj_t BgL_arg1803z00_2787;

																									BgL_arg1803z00_2787 =
																										(BgL_oclassz00_2781);
																									BgL_arg1802z00_2786 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2787, 2L);
																								}
																								BgL_res2026z00_2798 =
																									(BgL_arg1802z00_2786 ==
																									BgL_classz00_2765);
																							}
																						else
																							{	/* Globalize/node.scm 571 */
																								BgL_res2026z00_2798 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2135z00_4458 = BgL_res2026z00_2798;
																	}
															}
														}
														if (BgL_test2135z00_4458)
															{	/* Globalize/node.scm 571 */
																BgL_auxz00_4450 =
																	(((BgL_funz00_bglt) COBJECT(
																			((BgL_funz00_bglt) BgL_gvz00_1947)))->
																	BgL_stackzd2allocatorzd2);
															}
														else
															{	/* Globalize/node.scm 571 */
																BgL_auxz00_4450 = BFALSE;
															}
													}
												}
											else
												{	/* Globalize/node.scm 569 */
													BgL_auxz00_4450 = BFALSE;
												}
											((((BgL_appz00_bglt) COBJECT(BgL_new1185z00_1925))->
													BgL_stackablez00) =
												((obj_t) BgL_auxz00_4450), BUNSPEC);
										}
										return BgL_new1185z00_1925;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-sets */
	obj_t BGl_makezd2setszd2zzglobaliza7e_nodeza7(obj_t BgL_newz00_67,
		obj_t BgL_locz00_68, obj_t BgL_kapturedz00_69,
		BgL_variablez00_bglt BgL_integratorz00_70)
	{
		{	/* Globalize/node.scm 593 */
			if (NULLP(BgL_kapturedz00_69))
				{	/* Globalize/node.scm 594 */
					return BNIL;
				}
			else
				{
					obj_t BgL_kapturedz00_1957;
					long BgL_indicez00_1958;
					obj_t BgL_setsz00_1959;

					{
						BgL_sequencez00_bglt BgL_auxz00_4487;

						BgL_kapturedz00_1957 = BgL_kapturedz00_69;
						BgL_indicez00_1958 = 0L;
						BgL_setsz00_1959 = BNIL;
					BgL_zc3z04anonymousza31722ze3z87_1960:
						if (NULLP(BgL_kapturedz00_1957))
							{	/* Globalize/node.scm 600 */
								BgL_sequencez00_bglt BgL_new1197z00_1962;

								{	/* Globalize/node.scm 601 */
									BgL_sequencez00_bglt BgL_new1196z00_1964;

									BgL_new1196z00_1964 =
										((BgL_sequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_sequencez00_bgl))));
									{	/* Globalize/node.scm 601 */
										long BgL_arg1733z00_1965;

										BgL_arg1733z00_1965 =
											BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1196z00_1964),
											BgL_arg1733z00_1965);
									}
									{	/* Globalize/node.scm 601 */
										BgL_objectz00_bglt BgL_tmpz00_4494;

										BgL_tmpz00_4494 =
											((BgL_objectz00_bglt) BgL_new1196z00_1964);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4494, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1196z00_1964);
									BgL_new1197z00_1962 = BgL_new1196z00_1964;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1197z00_1962)))->
										BgL_locz00) = ((obj_t) BgL_locz00_68), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1197z00_1962)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt) CAR(((obj_t)
																BgL_setsz00_1959)))))->BgL_typez00)), BUNSPEC);
								((((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt)
													BgL_new1197z00_1962)))->BgL_sidezd2effectzd2) =
									((obj_t) BUNSPEC), BUNSPEC);
								((((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt)
													BgL_new1197z00_1962)))->BgL_keyz00) =
									((obj_t) BINT(-1L)), BUNSPEC);
								((((BgL_sequencez00_bglt) COBJECT(BgL_new1197z00_1962))->
										BgL_nodesz00) =
									((obj_t) bgl_reverse_bang(BgL_setsz00_1959)), BUNSPEC);
								((((BgL_sequencez00_bglt) COBJECT(BgL_new1197z00_1962))->
										BgL_unsafez00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
								((((BgL_sequencez00_bglt) COBJECT(BgL_new1197z00_1962))->
										BgL_metaz00) = ((obj_t) BNIL), BUNSPEC);
								BgL_auxz00_4487 = BgL_new1197z00_1962;
							}
						else
							{	/* Globalize/node.scm 604 */
								obj_t BgL_arg1734z00_1966;
								long BgL_arg1735z00_1967;
								obj_t BgL_arg1736z00_1968;

								BgL_arg1734z00_1966 = CDR(((obj_t) BgL_kapturedz00_1957));
								BgL_arg1735z00_1967 = (BgL_indicez00_1958 + 1L);
								{	/* Globalize/node.scm 609 */
									BgL_appz00_bglt BgL_arg1737z00_1969;

									{	/* Globalize/node.scm 609 */
										obj_t BgL_arg1738z00_1970;

										BgL_arg1738z00_1970 = CAR(((obj_t) BgL_kapturedz00_1957));
										BgL_arg1737z00_1969 =
											BGl_azd2procedurezd2setz12z12zzglobaliza7e_nodeza7
											(BgL_locz00_68, BgL_newz00_67, BgL_indicez00_1958,
											BgL_arg1738z00_1970, BgL_integratorz00_70);
									}
									BgL_arg1736z00_1968 =
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_arg1737z00_1969), BgL_setsz00_1959);
								}
								{
									obj_t BgL_setsz00_4525;
									long BgL_indicez00_4524;
									obj_t BgL_kapturedz00_4523;

									BgL_kapturedz00_4523 = BgL_arg1734z00_1966;
									BgL_indicez00_4524 = BgL_arg1735z00_1967;
									BgL_setsz00_4525 = BgL_arg1736z00_1968;
									BgL_setsz00_1959 = BgL_setsz00_4525;
									BgL_indicez00_1958 = BgL_indicez00_4524;
									BgL_kapturedz00_1957 = BgL_kapturedz00_4523;
									goto BgL_zc3z04anonymousza31722ze3z87_1960;
								}
							}
						return ((obj_t) BgL_auxz00_4487);
					}
				}
		}

	}



/* a-procedure-set! */
	BgL_appz00_bglt BGl_azd2procedurezd2setz12z12zzglobaliza7e_nodeza7(obj_t
		BgL_locz00_71, obj_t BgL_newz00_72, long BgL_indicez00_73,
		obj_t BgL_kapturedz00_74, BgL_variablez00_bglt BgL_integratorz00_75)
	{
		{	/* Globalize/node.scm 616 */
			{
				obj_t BgL_varz00_1996;

				{	/* Globalize/node.scm 622 */
					obj_t BgL_vfz00_1973;
					obj_t BgL_vaz00_1974;

					BgL_vfz00_1973 =
						BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(6),
						CNST_TABLE_REF(5));
					BgL_varz00_1996 = BgL_kapturedz00_74;
				BgL_zc3z04anonymousza31755ze3z87_1997:
					{	/* Globalize/node.scm 618 */
						obj_t BgL_alphaz00_1998;

						BgL_alphaz00_1998 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_1996)))->
							BgL_fastzd2alphazd2);
						{	/* Globalize/node.scm 619 */
							bool_t BgL_test2141z00_4532;

							{	/* Globalize/node.scm 619 */
								obj_t BgL_classz00_2810;

								BgL_classz00_2810 = BGl_localz00zzast_varz00;
								if (BGL_OBJECTP(BgL_alphaz00_1998))
									{	/* Globalize/node.scm 619 */
										BgL_objectz00_bglt BgL_arg1807z00_2812;

										BgL_arg1807z00_2812 =
											(BgL_objectz00_bglt) (BgL_alphaz00_1998);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Globalize/node.scm 619 */
												long BgL_idxz00_2818;

												BgL_idxz00_2818 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2812);
												BgL_test2141z00_4532 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2818 + 2L)) == BgL_classz00_2810);
											}
										else
											{	/* Globalize/node.scm 619 */
												bool_t BgL_res2027z00_2843;

												{	/* Globalize/node.scm 619 */
													obj_t BgL_oclassz00_2826;

													{	/* Globalize/node.scm 619 */
														obj_t BgL_arg1815z00_2834;
														long BgL_arg1816z00_2835;

														BgL_arg1815z00_2834 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Globalize/node.scm 619 */
															long BgL_arg1817z00_2836;

															BgL_arg1817z00_2836 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2812);
															BgL_arg1816z00_2835 =
																(BgL_arg1817z00_2836 - OBJECT_TYPE);
														}
														BgL_oclassz00_2826 =
															VECTOR_REF(BgL_arg1815z00_2834,
															BgL_arg1816z00_2835);
													}
													{	/* Globalize/node.scm 619 */
														bool_t BgL__ortest_1115z00_2827;

														BgL__ortest_1115z00_2827 =
															(BgL_classz00_2810 == BgL_oclassz00_2826);
														if (BgL__ortest_1115z00_2827)
															{	/* Globalize/node.scm 619 */
																BgL_res2027z00_2843 = BgL__ortest_1115z00_2827;
															}
														else
															{	/* Globalize/node.scm 619 */
																long BgL_odepthz00_2828;

																{	/* Globalize/node.scm 619 */
																	obj_t BgL_arg1804z00_2829;

																	BgL_arg1804z00_2829 = (BgL_oclassz00_2826);
																	BgL_odepthz00_2828 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2829);
																}
																if ((2L < BgL_odepthz00_2828))
																	{	/* Globalize/node.scm 619 */
																		obj_t BgL_arg1802z00_2831;

																		{	/* Globalize/node.scm 619 */
																			obj_t BgL_arg1803z00_2832;

																			BgL_arg1803z00_2832 =
																				(BgL_oclassz00_2826);
																			BgL_arg1802z00_2831 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2832, 2L);
																		}
																		BgL_res2027z00_2843 =
																			(BgL_arg1802z00_2831 ==
																			BgL_classz00_2810);
																	}
																else
																	{	/* Globalize/node.scm 619 */
																		BgL_res2027z00_2843 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2141z00_4532 = BgL_res2027z00_2843;
											}
									}
								else
									{	/* Globalize/node.scm 619 */
										BgL_test2141z00_4532 = ((bool_t) 0);
									}
							}
							if (BgL_test2141z00_4532)
								{
									obj_t BgL_varz00_4555;

									BgL_varz00_4555 = BgL_alphaz00_1998;
									BgL_varz00_1996 = BgL_varz00_4555;
									goto BgL_zc3z04anonymousza31755ze3z87_1997;
								}
							else
								{	/* Globalize/node.scm 619 */
									BgL_vaz00_1974 = BgL_varz00_1996;
								}
						}
					}
					{	/* Globalize/node.scm 624 */
						BgL_appz00_bglt BgL_new1200z00_1975;

						{	/* Globalize/node.scm 625 */
							BgL_appz00_bglt BgL_new1198z00_1994;

							BgL_new1198z00_1994 =
								((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_appz00_bgl))));
							{	/* Globalize/node.scm 625 */
								long BgL_arg1754z00_1995;

								BgL_arg1754z00_1995 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1198z00_1994),
									BgL_arg1754z00_1995);
							}
							{	/* Globalize/node.scm 625 */
								BgL_objectz00_bglt BgL_tmpz00_4560;

								BgL_tmpz00_4560 = ((BgL_objectz00_bglt) BgL_new1198z00_1994);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4560, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1198z00_1994);
							BgL_new1200z00_1975 = BgL_new1198z00_1994;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1200z00_1975)))->BgL_locz00) =
							((obj_t) BgL_locz00_71), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1200z00_1975)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_variablez00_bglt)
										COBJECT(((BgL_variablez00_bglt) BgL_vfz00_1973)))->
									BgL_typez00)), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1200z00_1975)))->BgL_sidezd2effectzd2) =
							((obj_t) BUNSPEC), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1200z00_1975)))->BgL_keyz00) =
							((obj_t) BINT(-1L)), BUNSPEC);
						{
							BgL_varz00_bglt BgL_auxz00_4575;

							{	/* Globalize/node.scm 627 */
								BgL_refz00_bglt BgL_new1202z00_1976;

								{	/* Globalize/node.scm 628 */
									BgL_refz00_bglt BgL_new1201z00_1977;

									BgL_new1201z00_1977 =
										((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_refz00_bgl))));
									{	/* Globalize/node.scm 628 */
										long BgL_arg1739z00_1978;

										{	/* Globalize/node.scm 628 */
											obj_t BgL_classz00_2849;

											BgL_classz00_2849 = BGl_refz00zzast_nodez00;
											BgL_arg1739z00_1978 = BGL_CLASS_NUM(BgL_classz00_2849);
										}
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1201z00_1977),
											BgL_arg1739z00_1978);
									}
									{	/* Globalize/node.scm 628 */
										BgL_objectz00_bglt BgL_tmpz00_4580;

										BgL_tmpz00_4580 =
											((BgL_objectz00_bglt) BgL_new1201z00_1977);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4580, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1201z00_1977);
									BgL_new1202z00_1976 = BgL_new1201z00_1977;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1202z00_1976)))->
										BgL_locz00) = ((obj_t) BgL_locz00_71), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1202z00_1976)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_variablez00_bglt)
												COBJECT(((BgL_variablez00_bglt) BgL_vfz00_1973)))->
											BgL_typez00)), BUNSPEC);
								((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
													BgL_new1202z00_1976)))->BgL_variablez00) =
									((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
											BgL_vfz00_1973)), BUNSPEC);
								BgL_auxz00_4575 = ((BgL_varz00_bglt) BgL_new1202z00_1976);
							}
							((((BgL_appz00_bglt) COBJECT(BgL_new1200z00_1975))->BgL_funz00) =
								((BgL_varz00_bglt) BgL_auxz00_4575), BUNSPEC);
						}
						{
							obj_t BgL_auxz00_4595;

							{	/* Globalize/node.scm 631 */
								BgL_refz00_bglt BgL_arg1740z00_1979;
								BgL_literalz00_bglt BgL_arg1746z00_1980;
								BgL_refz00_bglt BgL_arg1747z00_1981;

								{	/* Globalize/node.scm 631 */
									BgL_refz00_bglt BgL_new1205z00_1985;

									{	/* Globalize/node.scm 632 */
										BgL_refz00_bglt BgL_new1204z00_1986;

										BgL_new1204z00_1986 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Globalize/node.scm 632 */
											long BgL_arg1751z00_1987;

											{	/* Globalize/node.scm 632 */
												obj_t BgL_classz00_2854;

												BgL_classz00_2854 = BGl_refz00zzast_nodez00;
												BgL_arg1751z00_1987 = BGL_CLASS_NUM(BgL_classz00_2854);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1204z00_1986),
												BgL_arg1751z00_1987);
										}
										{	/* Globalize/node.scm 632 */
											BgL_objectz00_bglt BgL_tmpz00_4600;

											BgL_tmpz00_4600 =
												((BgL_objectz00_bglt) BgL_new1204z00_1986);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4600, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1204z00_1986);
										BgL_new1205z00_1985 = BgL_new1204z00_1986;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1205z00_1985)))->
											BgL_locz00) = ((obj_t) BgL_locz00_71), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1205z00_1985)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) BgL_newz00_72)))->
												BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1205z00_1985)))->BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BgL_newz00_72)), BUNSPEC);
									BgL_arg1740z00_1979 = BgL_new1205z00_1985;
								}
								{	/* Globalize/node.scm 635 */
									BgL_literalz00_bglt BgL_new1207z00_1988;

									{	/* Globalize/node.scm 636 */
										BgL_literalz00_bglt BgL_new1206z00_1989;

										BgL_new1206z00_1989 =
											((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_literalz00_bgl))));
										{	/* Globalize/node.scm 636 */
											long BgL_arg1752z00_1990;

											{	/* Globalize/node.scm 636 */
												obj_t BgL_classz00_2859;

												BgL_classz00_2859 = BGl_literalz00zzast_nodez00;
												BgL_arg1752z00_1990 = BGL_CLASS_NUM(BgL_classz00_2859);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1206z00_1989),
												BgL_arg1752z00_1990);
										}
										{	/* Globalize/node.scm 636 */
											BgL_objectz00_bglt BgL_tmpz00_4617;

											BgL_tmpz00_4617 =
												((BgL_objectz00_bglt) BgL_new1206z00_1989);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4617, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1206z00_1989);
										BgL_new1207z00_1988 = BgL_new1206z00_1989;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1207z00_1988)))->
											BgL_locz00) = ((obj_t) BgL_locz00_71), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1207z00_1988)))->BgL_typez00) =
										((BgL_typez00_bglt)
											BGl_getzd2typezd2atomz00zztype_typeofz00(BINT
												(BgL_indicez00_73))), BUNSPEC);
									((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
														BgL_new1207z00_1988)))->BgL_valuez00) =
										((obj_t) BINT(BgL_indicez00_73)), BUNSPEC);
									BgL_arg1746z00_1980 = BgL_new1207z00_1988;
								}
								{	/* Globalize/node.scm 639 */

									{	/* Globalize/node.scm 640 */
										BgL_refz00_bglt BgL_new1209z00_1991;

										{	/* Globalize/node.scm 641 */
											BgL_refz00_bglt BgL_new1208z00_1992;

											BgL_new1208z00_1992 =
												((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_refz00_bgl))));
											{	/* Globalize/node.scm 641 */
												long BgL_arg1753z00_1993;

												{	/* Globalize/node.scm 641 */
													obj_t BgL_classz00_2863;

													BgL_classz00_2863 = BGl_refz00zzast_nodez00;
													BgL_arg1753z00_1993 =
														BGL_CLASS_NUM(BgL_classz00_2863);
												}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1208z00_1992),
													BgL_arg1753z00_1993);
											}
											{	/* Globalize/node.scm 641 */
												BgL_objectz00_bglt BgL_tmpz00_4634;

												BgL_tmpz00_4634 =
													((BgL_objectz00_bglt) BgL_new1208z00_1992);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4634, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1208z00_1992);
											BgL_new1209z00_1991 = BgL_new1208z00_1992;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1209z00_1991)))->
												BgL_locz00) = ((obj_t) BgL_locz00_71), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1209z00_1991)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_variablez00_bglt)
														COBJECT(((BgL_variablez00_bglt) BgL_vaz00_1974)))->
													BgL_typez00)), BUNSPEC);
										((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
															BgL_new1209z00_1991)))->BgL_variablez00) =
											((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
													BgL_vaz00_1974)), BUNSPEC);
										BgL_arg1747z00_1981 = BgL_new1209z00_1991;
								}}
								{	/* Globalize/node.scm 631 */
									obj_t BgL_list1748z00_1982;

									{	/* Globalize/node.scm 631 */
										obj_t BgL_arg1749z00_1983;

										{	/* Globalize/node.scm 631 */
											obj_t BgL_arg1750z00_1984;

											BgL_arg1750z00_1984 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1747z00_1981), BNIL);
											BgL_arg1749z00_1983 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg1746z00_1980), BgL_arg1750z00_1984);
										}
										BgL_list1748z00_1982 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_arg1740z00_1979), BgL_arg1749z00_1983);
									}
									BgL_auxz00_4595 = BgL_list1748z00_1982;
							}}
							((((BgL_appz00_bglt) COBJECT(BgL_new1200z00_1975))->BgL_argsz00) =
								((obj_t) BgL_auxz00_4595), BUNSPEC);
						}
						((((BgL_appz00_bglt) COBJECT(BgL_new1200z00_1975))->
								BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
						return BgL_new1200z00_1975;
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_nodeza7(void)
	{
		{	/* Globalize/node.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_nodeza7(void)
	{
		{	/* Globalize/node.scm 17 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
				BGl_proc2046z00zzglobaliza7e_nodeza7, BGl_nodez00zzast_nodez00,
				BGl_string2047z00zzglobaliza7e_nodeza7);
		}

	}



/* &glo!1366 */
	obj_t BGl_z62gloz121366z70zzglobaliza7e_nodeza7(obj_t BgL_envz00_3435,
		obj_t BgL_nodez00_3436, obj_t BgL_integratorz00_3437)
	{
		{	/* Globalize/node.scm 134 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(7),
				BGl_string2048z00zzglobaliza7e_nodeza7,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3436)));
		}

	}



/* glo! */
	BgL_nodez00_bglt BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_nodez00_bglt
		BgL_nodez00_13, BgL_variablez00_bglt BgL_integratorz00_14)
	{
		{	/* Globalize/node.scm 134 */
			{	/* Globalize/node.scm 134 */
				obj_t BgL_method1367z00_2006;

				{	/* Globalize/node.scm 134 */
					obj_t BgL_res2033z00_2899;

					{	/* Globalize/node.scm 134 */
						long BgL_objzd2classzd2numz00_2870;

						BgL_objzd2classzd2numz00_2870 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_13));
						{	/* Globalize/node.scm 134 */
							obj_t BgL_arg1811z00_2871;

							BgL_arg1811z00_2871 =
								PROCEDURE_REF(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
								(int) (1L));
							{	/* Globalize/node.scm 134 */
								int BgL_offsetz00_2874;

								BgL_offsetz00_2874 = (int) (BgL_objzd2classzd2numz00_2870);
								{	/* Globalize/node.scm 134 */
									long BgL_offsetz00_2875;

									BgL_offsetz00_2875 =
										((long) (BgL_offsetz00_2874) - OBJECT_TYPE);
									{	/* Globalize/node.scm 134 */
										long BgL_modz00_2876;

										BgL_modz00_2876 =
											(BgL_offsetz00_2875 >> (int) ((long) ((int) (4L))));
										{	/* Globalize/node.scm 134 */
											long BgL_restz00_2878;

											BgL_restz00_2878 =
												(BgL_offsetz00_2875 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Globalize/node.scm 134 */

												{	/* Globalize/node.scm 134 */
													obj_t BgL_bucketz00_2880;

													BgL_bucketz00_2880 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2871), BgL_modz00_2876);
													BgL_res2033z00_2899 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2880), BgL_restz00_2878);
					}}}}}}}}
					BgL_method1367z00_2006 = BgL_res2033z00_2899;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1367z00_2006,
						((obj_t) BgL_nodez00_13), ((obj_t) BgL_integratorz00_14)));
			}
		}

	}



/* &glo! */
	BgL_nodez00_bglt BGl_z62gloz12z70zzglobaliza7e_nodeza7(obj_t BgL_envz00_3438,
		obj_t BgL_nodez00_3439, obj_t BgL_integratorz00_3440)
	{
		{	/* Globalize/node.scm 134 */
			return
				BGl_gloz12z12zzglobaliza7e_nodeza7(
				((BgL_nodez00_bglt) BgL_nodez00_3439),
				((BgL_variablez00_bglt) BgL_integratorz00_3440));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_nodeza7(void)
	{
		{	/* Globalize/node.scm 17 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_atomz00zzast_nodez00,
				BGl_proc2049z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_kwotez00zzast_nodez00,
				BGl_proc2051z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_varz00zzast_nodez00,
				BGl_proc2052z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_closurez00zzast_nodez00,
				BGl_proc2053z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_sequencez00zzast_nodez00,
				BGl_proc2054z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_syncz00zzast_nodez00,
				BGl_proc2055z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_appz00zzast_nodez00,
				BGl_proc2056z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc2057z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_funcallz00zzast_nodez00,
				BGl_proc2058z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_externz00zzast_nodez00,
				BGl_proc2059z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_castz00zzast_nodez00,
				BGl_proc2060z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_setqz00zzast_nodez00,
				BGl_proc2061z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
				BGl_conditionalz00zzast_nodez00, BGl_proc2062z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_failz00zzast_nodez00,
				BGl_proc2063z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7, BGl_switchz00zzast_nodez00,
				BGl_proc2064z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2065z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2066z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2067z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc2068z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2069z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2070z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzglobaliza7e_nodeza7,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2071z00zzglobaliza7e_nodeza7,
				BGl_string2050z00zzglobaliza7e_nodeza7);
		}

	}



/* &glo!-box-set!1418 */
	BgL_nodez00_bglt
		BGl_z62gloz12zd2boxzd2setz121418z62zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3463, obj_t BgL_nodez00_3464, obj_t BgL_integratorz00_3465)
	{
		{	/* Globalize/node.scm 484 */
			{
				BgL_varz00_bglt BgL_auxz00_4718;

				{	/* Globalize/node.scm 486 */
					BgL_varz00_bglt BgL_arg1956z00_3543;

					BgL_arg1956z00_3543 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3464)))->BgL_varz00);
					BgL_auxz00_4718 =
						((BgL_varz00_bglt)
						BGl_gloz12z12zzglobaliza7e_nodeza7(
							((BgL_nodez00_bglt) BgL_arg1956z00_3543),
							((BgL_variablez00_bglt) BgL_integratorz00_3465)));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3464)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_4718), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4727;

				{	/* Globalize/node.scm 487 */
					BgL_nodez00_bglt BgL_arg1957z00_3544;

					BgL_arg1957z00_3544 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3464)))->BgL_valuez00);
					BgL_auxz00_4727 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1957z00_3544,
						((BgL_variablez00_bglt) BgL_integratorz00_3465));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3464)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4727), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3464));
		}

	}



/* &glo!-box-ref1416 */
	BgL_nodez00_bglt BGl_z62gloz12zd2boxzd2ref1416z70zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3466, obj_t BgL_nodez00_3467, obj_t BgL_integratorz00_3468)
	{
		{	/* Globalize/node.scm 476 */
			{
				BgL_varz00_bglt BgL_auxz00_4736;

				{	/* Globalize/node.scm 478 */
					BgL_varz00_bglt BgL_arg1955z00_3546;

					BgL_arg1955z00_3546 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3467)))->BgL_varz00);
					BgL_auxz00_4736 =
						((BgL_varz00_bglt)
						BGl_gloz12z12zzglobaliza7e_nodeza7(
							((BgL_nodez00_bglt) BgL_arg1955z00_3546),
							((BgL_variablez00_bglt) BgL_integratorz00_3468)));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3467)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_4736), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_3467));
		}

	}



/* &glo!-make-box1414 */
	BgL_nodez00_bglt BGl_z62gloz12zd2makezd2box1414z70zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3469, obj_t BgL_nodez00_3470, obj_t BgL_integratorz00_3471)
	{
		{	/* Globalize/node.scm 468 */
			{
				BgL_nodez00_bglt BgL_auxz00_4747;

				{	/* Globalize/node.scm 470 */
					BgL_nodez00_bglt BgL_arg1954z00_3548;

					BgL_arg1954z00_3548 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_3470)))->BgL_valuez00);
					BgL_auxz00_4747 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1954z00_3548,
						((BgL_variablez00_bglt) BgL_integratorz00_3471));
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_3470)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4747), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_3470));
		}

	}



/* &glo!-jump-ex-it1412 */
	BgL_nodez00_bglt
		BGl_z62gloz12zd2jumpzd2exzd2it1412za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3472, obj_t BgL_nodez00_3473, obj_t BgL_integratorz00_3474)
	{
		{	/* Globalize/node.scm 459 */
			{
				BgL_nodez00_bglt BgL_auxz00_4756;

				{	/* Globalize/node.scm 461 */
					BgL_nodez00_bglt BgL_arg1952z00_3550;

					BgL_arg1952z00_3550 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3473)))->BgL_exitz00);
					BgL_auxz00_4756 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1952z00_3550,
						((BgL_variablez00_bglt) BgL_integratorz00_3474));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3473)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_4756), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4763;

				{	/* Globalize/node.scm 462 */
					BgL_nodez00_bglt BgL_arg1953z00_3551;

					BgL_arg1953z00_3551 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3473)))->
						BgL_valuez00);
					BgL_auxz00_4763 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1953z00_3551,
						((BgL_variablez00_bglt) BgL_integratorz00_3474));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3473)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_4763), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3473));
		}

	}



/* &glo!-set-ex-it1408 */
	BgL_nodez00_bglt
		BGl_z62gloz12zd2setzd2exzd2it1408za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3475, obj_t BgL_nodez00_3476, obj_t BgL_integratorz00_3477)
	{
		{	/* Globalize/node.scm 443 */
			{	/* Globalize/node.scm 445 */
				obj_t BgL_hdlgz00_3553;

				BgL_hdlgz00_3553 =
					(((BgL_sexitz00_bglt) COBJECT(
							((BgL_sexitz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt)
													(((BgL_varz00_bglt) COBJECT(
																(((BgL_setzd2exzd2itz00_bglt) COBJECT(
																			((BgL_setzd2exzd2itz00_bglt)
																				BgL_nodez00_3476)))->BgL_varz00)))->
														BgL_variablez00)))))->BgL_valuez00))))->
					BgL_handlerz00);
				{	/* Globalize/node.scm 450 */
					bool_t BgL_test2146z00_4780;

					{	/* Globalize/node.scm 450 */
						BgL_sfunz00_bglt BgL_oz00_3554;

						BgL_oz00_3554 =
							((BgL_sfunz00_bglt)
							(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_localz00_bglt) BgL_hdlgz00_3553))))->BgL_valuez00));
						{
							BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4785;

							{
								obj_t BgL_auxz00_4786;

								{	/* Globalize/node.scm 450 */
									BgL_objectz00_bglt BgL_tmpz00_4787;

									BgL_tmpz00_4787 = ((BgL_objectz00_bglt) BgL_oz00_3554);
									BgL_auxz00_4786 = BGL_OBJECT_WIDENING(BgL_tmpz00_4787);
								}
								BgL_auxz00_4785 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4786);
							}
							BgL_test2146z00_4780 =
								(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4785))->
								BgL_gzf3zf3);
						}
					}
					if (BgL_test2146z00_4780)
						{	/* Globalize/node.scm 451 */
							BgL_valuez00_bglt BgL_arg1943z00_3555;

							BgL_arg1943z00_3555 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_localz00_bglt)
												(((BgL_varz00_bglt) COBJECT(
															(((BgL_setzd2exzd2itz00_bglt) COBJECT(
																		((BgL_setzd2exzd2itz00_bglt)
																			BgL_nodez00_3476)))->BgL_varz00)))->
													BgL_variablez00)))))->BgL_valuez00);
							((((BgL_sexitz00_bglt) COBJECT(((BgL_sexitz00_bglt)
												BgL_arg1943z00_3555)))->BgL_detachedzf3zf3) =
								((bool_t) ((bool_t) 1)), BUNSPEC);
						}
					else
						{	/* Globalize/node.scm 450 */
							BFALSE;
						}
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4800;

					{	/* Globalize/node.scm 452 */
						BgL_nodez00_bglt BgL_arg1947z00_3556;

						BgL_arg1947z00_3556 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3476)))->
							BgL_bodyz00);
						BgL_auxz00_4800 =
							BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1947z00_3556,
							((BgL_variablez00_bglt) BgL_integratorz00_3477));
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3476)))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_4800), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4807;

					{	/* Globalize/node.scm 453 */
						BgL_nodez00_bglt BgL_arg1948z00_3557;

						BgL_arg1948z00_3557 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3476)))->
							BgL_onexitz00);
						BgL_auxz00_4807 =
							BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1948z00_3557,
							((BgL_variablez00_bglt) BgL_integratorz00_3477));
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3476)))->
							BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_4807), BUNSPEC);
				}
				return
					((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3476));
			}
		}

	}



/* &glo!-let-var1406 */
	BgL_nodez00_bglt BGl_z62gloz12zd2letzd2var1406z70zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3478, obj_t BgL_nodez00_3479, obj_t BgL_integratorz00_3480)
	{
		{	/* Globalize/node.scm 418 */
			{	/* Globalize/node.scm 420 */
				obj_t BgL_g1358z00_3559;

				BgL_g1358z00_3559 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_3479)))->BgL_bindingsz00);
				{
					obj_t BgL_l1356z00_3561;

					BgL_l1356z00_3561 = BgL_g1358z00_3559;
				BgL_zc3z04anonymousza31928ze3z87_3560:
					if (PAIRP(BgL_l1356z00_3561))
						{	/* Globalize/node.scm 420 */
							{	/* Globalize/node.scm 421 */
								obj_t BgL_bindingz00_3562;

								BgL_bindingz00_3562 = CAR(BgL_l1356z00_3561);
								{	/* Globalize/node.scm 421 */
									obj_t BgL_varz00_3563;
									obj_t BgL_valz00_3564;

									BgL_varz00_3563 = CAR(((obj_t) BgL_bindingz00_3562));
									BgL_valz00_3564 = CDR(((obj_t) BgL_bindingz00_3562));
									{	/* Globalize/node.scm 423 */
										BgL_nodez00_bglt BgL_arg1930z00_3565;

										BgL_arg1930z00_3565 =
											BGl_gloz12z12zzglobaliza7e_nodeza7(
											((BgL_nodez00_bglt) BgL_valz00_3564),
											((BgL_variablez00_bglt) BgL_integratorz00_3480));
										{	/* Globalize/node.scm 423 */
											obj_t BgL_auxz00_4830;
											obj_t BgL_tmpz00_4828;

											BgL_auxz00_4830 = ((obj_t) BgL_arg1930z00_3565);
											BgL_tmpz00_4828 = ((obj_t) BgL_bindingz00_3562);
											SET_CDR(BgL_tmpz00_4828, BgL_auxz00_4830);
										}
									}
									if (BGl_celledzf3zf3zzglobaliza7e_nodeza7(
											((BgL_variablez00_bglt) BgL_varz00_3563)))
										{	/* Globalize/node.scm 425 */
											BgL_localz00_bglt BgL_nvarz00_3566;

											BgL_nvarz00_3566 =
												BGl_cellzd2variablezd2zzglobaliza7e_nodeza7(
												((BgL_localz00_bglt) BgL_varz00_3563));
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_varz00_3563))))->
													BgL_fastzd2alphazd2) =
												((obj_t) ((obj_t) BgL_nvarz00_3566)), BUNSPEC);
											{	/* Globalize/node.scm 428 */
												BgL_makezd2boxzd2_bglt BgL_arg1932z00_3567;

												{	/* Globalize/node.scm 428 */
													obj_t BgL_arg1933z00_3568;

													BgL_arg1933z00_3568 =
														CDR(((obj_t) BgL_bindingz00_3562));
													BgL_arg1932z00_3567 =
														BGl_azd2makezd2cellz00zzglobaliza7e_nodeza7(
														((BgL_nodez00_bglt) BgL_arg1933z00_3568),
														((BgL_variablez00_bglt) BgL_varz00_3563));
												}
												{	/* Globalize/node.scm 427 */
													obj_t BgL_auxz00_4849;
													obj_t BgL_tmpz00_4847;

													BgL_auxz00_4849 = ((obj_t) BgL_arg1932z00_3567);
													BgL_tmpz00_4847 = ((obj_t) BgL_bindingz00_3562);
													SET_CDR(BgL_tmpz00_4847, BgL_auxz00_4849);
												}
											}
										}
									else
										{	/* Globalize/node.scm 424 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1356z00_4852;

								BgL_l1356z00_4852 = CDR(BgL_l1356z00_3561);
								BgL_l1356z00_3561 = BgL_l1356z00_4852;
								goto BgL_zc3z04anonymousza31928ze3z87_3560;
							}
						}
					else
						{	/* Globalize/node.scm 420 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4854;

				{	/* Globalize/node.scm 430 */
					BgL_nodez00_bglt BgL_arg1935z00_3569;

					BgL_arg1935z00_3569 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_3479)))->BgL_bodyz00);
					BgL_auxz00_4854 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1935z00_3569,
						((BgL_variablez00_bglt) BgL_integratorz00_3480));
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_3479)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4854), BUNSPEC);
			}
			{	/* Globalize/node.scm 431 */
				obj_t BgL_g1361z00_3570;

				BgL_g1361z00_3570 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_3479)))->BgL_bindingsz00);
				{
					obj_t BgL_l1359z00_3572;

					BgL_l1359z00_3572 = BgL_g1361z00_3570;
				BgL_zc3z04anonymousza31936ze3z87_3571:
					if (PAIRP(BgL_l1359z00_3572))
						{	/* Globalize/node.scm 431 */
							{	/* Globalize/node.scm 432 */
								obj_t BgL_bindingz00_3573;

								BgL_bindingz00_3573 = CAR(BgL_l1359z00_3572);
								{	/* Globalize/node.scm 432 */
									obj_t BgL_varz00_3574;

									BgL_varz00_3574 = CAR(((obj_t) BgL_bindingz00_3573));
									if (BGl_celledzf3zf3zzglobaliza7e_nodeza7(
											((BgL_variablez00_bglt) BgL_varz00_3574)))
										{	/* Globalize/node.scm 434 */
											BgL_localz00_bglt BgL_nvarz00_3575;

											BgL_nvarz00_3575 =
												BGl_cellzd2variablezd2zzglobaliza7e_nodeza7(
												((BgL_localz00_bglt) BgL_varz00_3574));
											{	/* Globalize/node.scm 435 */
												obj_t BgL_arg1939z00_3576;

												BgL_arg1939z00_3576 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_varz00_3574)))->
													BgL_fastzd2alphazd2);
												{	/* Globalize/node.scm 435 */
													obj_t BgL_tmpz00_4875;

													BgL_tmpz00_4875 = ((obj_t) BgL_bindingz00_3573);
													SET_CAR(BgL_tmpz00_4875, BgL_arg1939z00_3576);
												}
											}
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_varz00_3574))))->
													BgL_fastzd2alphazd2) = ((obj_t) BUNSPEC), BUNSPEC);
										}
									else
										{	/* Globalize/node.scm 433 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1359z00_4881;

								BgL_l1359z00_4881 = CDR(BgL_l1359z00_3572);
								BgL_l1359z00_3572 = BgL_l1359z00_4881;
								goto BgL_zc3z04anonymousza31936ze3z87_3571;
							}
						}
					else
						{	/* Globalize/node.scm 431 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_3479));
		}

	}



/* &glo!-let-fun1403 */
	BgL_nodez00_bglt BGl_z62gloz12zd2letzd2fun1403z70zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3481, obj_t BgL_nodez00_3482, obj_t BgL_integratorz00_3483)
	{
		{	/* Globalize/node.scm 376 */
			{
				BgL_nodez00_bglt BgL_auxz00_4885;

				{	/* Globalize/node.scm 378 */
					BgL_nodez00_bglt BgL_arg1898z00_3578;

					BgL_arg1898z00_3578 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3482)))->BgL_bodyz00);
					BgL_auxz00_4885 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1898z00_3578,
						((BgL_variablez00_bglt) BgL_integratorz00_3483));
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3482)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4885), BUNSPEC);
			}
			{
				obj_t BgL_obindingsz00_3580;
				obj_t BgL_nbindingsz00_3581;
				obj_t BgL_ebindingsz00_3582;

				{	/* Globalize/node.scm 379 */
					obj_t BgL_arg1899z00_3593;

					BgL_arg1899z00_3593 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3482)))->BgL_localsz00);
					{
						BgL_nodezf2effectzf2_bglt BgL_auxz00_4894;

						BgL_obindingsz00_3580 = BgL_arg1899z00_3593;
						BgL_nbindingsz00_3581 = BNIL;
						BgL_ebindingsz00_3582 = BNIL;
					BgL_liipz00_3579:
						if (NULLP(BgL_obindingsz00_3580))
							{	/* Globalize/node.scm 390 */
								((((BgL_letzd2funzd2_bglt) COBJECT(
												((BgL_letzd2funzd2_bglt) BgL_nodez00_3482)))->
										BgL_localsz00) = ((obj_t) BgL_nbindingsz00_3581), BUNSPEC);
								if (NULLP(BgL_ebindingsz00_3582))
									{	/* Globalize/node.scm 392 */
										BgL_auxz00_4894 =
											((BgL_nodezf2effectzf2_bglt)
											((BgL_letzd2funzd2_bglt) BgL_nodez00_3482));
									}
								else
									{	/* Globalize/node.scm 392 */
										BgL_auxz00_4894 =
											((BgL_nodezf2effectzf2_bglt)
											BGl_makezd2escapingzd2bindingsz00zzglobaliza7e_nodeza7
											(BgL_ebindingsz00_3582,
												((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
														BgL_nodez00_3482)),
												((BgL_variablez00_bglt) BgL_integratorz00_3483)));
									}
							}
						else
							{	/* Globalize/node.scm 395 */
								bool_t BgL_test2153z00_4908;

								if (
									(CAR(
											((obj_t) BgL_obindingsz00_3580)) ==
										BgL_integratorz00_3483))
									{	/* Globalize/node.scm 395 */
										BgL_test2153z00_4908 = ((bool_t) 0);
									}
								else
									{	/* Globalize/node.scm 396 */
										BgL_localz00_bglt BgL_oz00_3583;

										BgL_oz00_3583 =
											((BgL_localz00_bglt)
											CAR(((obj_t) BgL_obindingsz00_3580)));
										{
											BgL_localzf2ginfozf2_bglt BgL_auxz00_4916;

											{
												obj_t BgL_auxz00_4917;

												{	/* Globalize/node.scm 396 */
													BgL_objectz00_bglt BgL_tmpz00_4918;

													BgL_tmpz00_4918 =
														((BgL_objectz00_bglt) BgL_oz00_3583);
													BgL_auxz00_4917 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4918);
												}
												BgL_auxz00_4916 =
													((BgL_localzf2ginfozf2_bglt) BgL_auxz00_4917);
											}
											BgL_test2153z00_4908 =
												(((BgL_localzf2ginfozf2_bglt)
													COBJECT(BgL_auxz00_4916))->BgL_escapezf3zf3);
										}
									}
								if (BgL_test2153z00_4908)
									{	/* Globalize/node.scm 397 */
										obj_t BgL_arg1912z00_3584;
										obj_t BgL_arg1913z00_3585;

										BgL_arg1912z00_3584 = CDR(((obj_t) BgL_obindingsz00_3580));
										{	/* Globalize/node.scm 399 */
											obj_t BgL_arg1914z00_3586;

											BgL_arg1914z00_3586 =
												CAR(((obj_t) BgL_obindingsz00_3580));
											BgL_arg1913z00_3585 =
												MAKE_YOUNG_PAIR(BgL_arg1914z00_3586,
												BgL_ebindingsz00_3582);
										}
										{
											obj_t BgL_ebindingsz00_4929;
											obj_t BgL_obindingsz00_4928;

											BgL_obindingsz00_4928 = BgL_arg1912z00_3584;
											BgL_ebindingsz00_4929 = BgL_arg1913z00_3585;
											BgL_ebindingsz00_3582 = BgL_ebindingsz00_4929;
											BgL_obindingsz00_3580 = BgL_obindingsz00_4928;
											goto BgL_liipz00_3579;
										}
									}
								else
									{	/* Globalize/node.scm 395 */
										if (
											(CAR(
													((obj_t) BgL_obindingsz00_3580)) ==
												BgL_integratorz00_3483))
											{	/* Globalize/node.scm 400 */
												{	/* Globalize/node.scm 404 */
													obj_t BgL_arg1918z00_3587;

													{	/* Globalize/node.scm 404 */
														obj_t BgL_arg1919z00_3588;

														BgL_arg1919z00_3588 =
															CAR(((obj_t) BgL_obindingsz00_3580));
														BgL_arg1918z00_3587 =
															BGl_shapez00zztools_shapez00(BgL_arg1919z00_3588);
													}
													BGl_internalzd2errorzd2zztools_errorz00(CNST_TABLE_REF
														(8), BGl_string2072z00zzglobaliza7e_nodeza7,
														BgL_arg1918z00_3587);
												}
												{	/* Globalize/node.scm 405 */
													obj_t BgL_arg1920z00_3589;

													BgL_arg1920z00_3589 =
														CDR(((obj_t) BgL_obindingsz00_3580));
													{
														obj_t BgL_obindingsz00_4941;

														BgL_obindingsz00_4941 = BgL_arg1920z00_3589;
														BgL_obindingsz00_3580 = BgL_obindingsz00_4941;
														goto BgL_liipz00_3579;
													}
												}
											}
										else
											{	/* Globalize/node.scm 409 */
												obj_t BgL_localz00_3590;

												BgL_localz00_3590 =
													CAR(((obj_t) BgL_obindingsz00_3580));
												BGl_globaliza7ezd2localzd2funz12zb5zzglobaliza7e_nodeza7
													(((BgL_localz00_bglt) BgL_localz00_3590),
													((BgL_variablez00_bglt) BgL_integratorz00_3483));
												{	/* Globalize/node.scm 411 */
													obj_t BgL_arg1923z00_3591;
													obj_t BgL_arg1924z00_3592;

													BgL_arg1923z00_3591 =
														CDR(((obj_t) BgL_obindingsz00_3580));
													BgL_arg1924z00_3592 =
														MAKE_YOUNG_PAIR(BgL_localz00_3590,
														BgL_nbindingsz00_3581);
													{
														obj_t BgL_nbindingsz00_4951;
														obj_t BgL_obindingsz00_4950;

														BgL_obindingsz00_4950 = BgL_arg1923z00_3591;
														BgL_nbindingsz00_4951 = BgL_arg1924z00_3592;
														BgL_nbindingsz00_3581 = BgL_nbindingsz00_4951;
														BgL_obindingsz00_3580 = BgL_obindingsz00_4950;
														goto BgL_liipz00_3579;
													}
												}
											}
									}
							}
						return ((BgL_nodez00_bglt) BgL_auxz00_4894);
					}
				}
			}
		}

	}



/* &glo!-switch1401 */
	BgL_nodez00_bglt BGl_z62gloz12zd2switch1401za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3484, obj_t BgL_nodez00_3485, obj_t BgL_integratorz00_3486)
	{
		{	/* Globalize/node.scm 365 */
			{
				BgL_nodez00_bglt BgL_auxz00_4953;

				{	/* Globalize/node.scm 367 */
					BgL_nodez00_bglt BgL_arg1891z00_3595;

					BgL_arg1891z00_3595 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_3485)))->BgL_testz00);
					BgL_auxz00_4953 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1891z00_3595,
						((BgL_variablez00_bglt) BgL_integratorz00_3486));
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_3485)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4953), BUNSPEC);
			}
			{	/* Globalize/node.scm 368 */
				obj_t BgL_g1355z00_3596;

				BgL_g1355z00_3596 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_3485)))->BgL_clausesz00);
				{
					obj_t BgL_l1353z00_3598;

					BgL_l1353z00_3598 = BgL_g1355z00_3596;
				BgL_zc3z04anonymousza31892ze3z87_3597:
					if (PAIRP(BgL_l1353z00_3598))
						{	/* Globalize/node.scm 368 */
							{	/* Globalize/node.scm 369 */
								obj_t BgL_clausez00_3599;

								BgL_clausez00_3599 = CAR(BgL_l1353z00_3598);
								{	/* Globalize/node.scm 369 */
									BgL_nodez00_bglt BgL_arg1894z00_3600;

									{	/* Globalize/node.scm 369 */
										obj_t BgL_arg1896z00_3601;

										BgL_arg1896z00_3601 = CDR(((obj_t) BgL_clausez00_3599));
										BgL_arg1894z00_3600 =
											BGl_gloz12z12zzglobaliza7e_nodeza7(
											((BgL_nodez00_bglt) BgL_arg1896z00_3601),
											((BgL_variablez00_bglt) BgL_integratorz00_3486));
									}
									{	/* Globalize/node.scm 369 */
										obj_t BgL_auxz00_4972;
										obj_t BgL_tmpz00_4970;

										BgL_auxz00_4972 = ((obj_t) BgL_arg1894z00_3600);
										BgL_tmpz00_4970 = ((obj_t) BgL_clausez00_3599);
										SET_CDR(BgL_tmpz00_4970, BgL_auxz00_4972);
									}
								}
							}
							{
								obj_t BgL_l1353z00_4975;

								BgL_l1353z00_4975 = CDR(BgL_l1353z00_3598);
								BgL_l1353z00_3598 = BgL_l1353z00_4975;
								goto BgL_zc3z04anonymousza31892ze3z87_3597;
							}
						}
					else
						{	/* Globalize/node.scm 368 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_3485));
		}

	}



/* &glo!-fail1399 */
	BgL_nodez00_bglt BGl_z62gloz12zd2fail1399za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3487, obj_t BgL_nodez00_3488, obj_t BgL_integratorz00_3489)
	{
		{	/* Globalize/node.scm 355 */
			{
				BgL_nodez00_bglt BgL_auxz00_4979;

				{	/* Globalize/node.scm 357 */
					BgL_nodez00_bglt BgL_arg1888z00_3603;

					BgL_arg1888z00_3603 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3488)))->BgL_procz00);
					BgL_auxz00_4979 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1888z00_3603,
						((BgL_variablez00_bglt) BgL_integratorz00_3489));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3488)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4979), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4986;

				{	/* Globalize/node.scm 358 */
					BgL_nodez00_bglt BgL_arg1889z00_3604;

					BgL_arg1889z00_3604 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3488)))->BgL_msgz00);
					BgL_auxz00_4986 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1889z00_3604,
						((BgL_variablez00_bglt) BgL_integratorz00_3489));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3488)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4986), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4993;

				{	/* Globalize/node.scm 359 */
					BgL_nodez00_bglt BgL_arg1890z00_3605;

					BgL_arg1890z00_3605 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3488)))->BgL_objz00);
					BgL_auxz00_4993 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1890z00_3605,
						((BgL_variablez00_bglt) BgL_integratorz00_3489));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3488)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4993), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_3488));
		}

	}



/* &glo!-conditional1396 */
	BgL_nodez00_bglt BGl_z62gloz12zd2conditional1396za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3490, obj_t BgL_nodez00_3491, obj_t BgL_integratorz00_3492)
	{
		{	/* Globalize/node.scm 345 */
			{
				BgL_nodez00_bglt BgL_auxz00_5002;

				{	/* Globalize/node.scm 347 */
					BgL_nodez00_bglt BgL_arg1884z00_3607;

					BgL_arg1884z00_3607 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3491)))->BgL_testz00);
					BgL_auxz00_5002 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1884z00_3607,
						((BgL_variablez00_bglt) BgL_integratorz00_3492));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3491)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5002), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5009;

				{	/* Globalize/node.scm 348 */
					BgL_nodez00_bglt BgL_arg1885z00_3608;

					BgL_arg1885z00_3608 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3491)))->BgL_truez00);
					BgL_auxz00_5009 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1885z00_3608,
						((BgL_variablez00_bglt) BgL_integratorz00_3492));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3491)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_5009), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5016;

				{	/* Globalize/node.scm 349 */
					BgL_nodez00_bglt BgL_arg1887z00_3609;

					BgL_arg1887z00_3609 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3491)))->BgL_falsez00);
					BgL_auxz00_5016 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1887z00_3609,
						((BgL_variablez00_bglt) BgL_integratorz00_3492));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3491)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_5016), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_3491));
		}

	}



/* &glo!-setq1394 */
	BgL_nodez00_bglt BGl_z62gloz12zd2setq1394za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3493, obj_t BgL_nodez00_3494, obj_t BgL_integratorz00_3495)
	{
		{	/* Globalize/node.scm 307 */
			{
				BgL_nodez00_bglt BgL_auxz00_5025;

				{	/* Globalize/node.scm 309 */
					BgL_nodez00_bglt BgL_arg1863z00_3611;

					BgL_arg1863z00_3611 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_3494)))->BgL_valuez00);
					BgL_auxz00_5025 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1863z00_3611,
						((BgL_variablez00_bglt) BgL_integratorz00_3495));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_3494)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_5025), BUNSPEC);
			}
			{	/* Globalize/node.scm 310 */
				BgL_variablez00_bglt BgL_varz00_3612;

				BgL_varz00_3612 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_3494)))->BgL_varz00)))->
					BgL_variablez00);
				{	/* Globalize/node.scm 310 */
					BgL_typez00_bglt BgL_vtypez00_3613;

					BgL_vtypez00_3613 =
						(((BgL_variablez00_bglt) COBJECT(BgL_varz00_3612))->BgL_typez00);
					{	/* Globalize/node.scm 311 */

						{	/* Globalize/node.scm 312 */
							obj_t BgL_g1152z00_3614;

							BgL_g1152z00_3614 =
								(((BgL_variablez00_bglt) COBJECT(BgL_varz00_3612))->
								BgL_fastzd2alphazd2);
							{
								obj_t BgL_varz00_3616;
								obj_t BgL_alphaz00_3617;

								BgL_varz00_3616 = ((obj_t) BgL_varz00_3612);
								BgL_alphaz00_3617 = BgL_g1152z00_3614;
							BgL_loopz00_3615:
								{	/* Globalize/node.scm 314 */
									bool_t BgL_test2157z00_5037;

									{	/* Globalize/node.scm 314 */
										obj_t BgL_classz00_3618;

										BgL_classz00_3618 = BGl_localz00zzast_varz00;
										if (BGL_OBJECTP(BgL_alphaz00_3617))
											{	/* Globalize/node.scm 314 */
												BgL_objectz00_bglt BgL_arg1807z00_3619;

												BgL_arg1807z00_3619 =
													(BgL_objectz00_bglt) (BgL_alphaz00_3617);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Globalize/node.scm 314 */
														long BgL_idxz00_3620;

														BgL_idxz00_3620 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3619);
														BgL_test2157z00_5037 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3620 + 2L)) == BgL_classz00_3618);
													}
												else
													{	/* Globalize/node.scm 314 */
														bool_t BgL_res2039z00_3623;

														{	/* Globalize/node.scm 314 */
															obj_t BgL_oclassz00_3624;

															{	/* Globalize/node.scm 314 */
																obj_t BgL_arg1815z00_3625;
																long BgL_arg1816z00_3626;

																BgL_arg1815z00_3625 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Globalize/node.scm 314 */
																	long BgL_arg1817z00_3627;

																	BgL_arg1817z00_3627 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3619);
																	BgL_arg1816z00_3626 =
																		(BgL_arg1817z00_3627 - OBJECT_TYPE);
																}
																BgL_oclassz00_3624 =
																	VECTOR_REF(BgL_arg1815z00_3625,
																	BgL_arg1816z00_3626);
															}
															{	/* Globalize/node.scm 314 */
																bool_t BgL__ortest_1115z00_3628;

																BgL__ortest_1115z00_3628 =
																	(BgL_classz00_3618 == BgL_oclassz00_3624);
																if (BgL__ortest_1115z00_3628)
																	{	/* Globalize/node.scm 314 */
																		BgL_res2039z00_3623 =
																			BgL__ortest_1115z00_3628;
																	}
																else
																	{	/* Globalize/node.scm 314 */
																		long BgL_odepthz00_3629;

																		{	/* Globalize/node.scm 314 */
																			obj_t BgL_arg1804z00_3630;

																			BgL_arg1804z00_3630 =
																				(BgL_oclassz00_3624);
																			BgL_odepthz00_3629 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3630);
																		}
																		if ((2L < BgL_odepthz00_3629))
																			{	/* Globalize/node.scm 314 */
																				obj_t BgL_arg1802z00_3631;

																				{	/* Globalize/node.scm 314 */
																					obj_t BgL_arg1803z00_3632;

																					BgL_arg1803z00_3632 =
																						(BgL_oclassz00_3624);
																					BgL_arg1802z00_3631 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3632, 2L);
																				}
																				BgL_res2039z00_3623 =
																					(BgL_arg1802z00_3631 ==
																					BgL_classz00_3618);
																			}
																		else
																			{	/* Globalize/node.scm 314 */
																				BgL_res2039z00_3623 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2157z00_5037 = BgL_res2039z00_3623;
													}
											}
										else
											{	/* Globalize/node.scm 314 */
												BgL_test2157z00_5037 = ((bool_t) 0);
											}
									}
									if (BgL_test2157z00_5037)
										{	/* Globalize/node.scm 314 */
											{	/* Globalize/node.scm 316 */
												BgL_varz00_bglt BgL_arg1866z00_3633;

												BgL_arg1866z00_3633 =
													(((BgL_setqz00_bglt) COBJECT(
															((BgL_setqz00_bglt) BgL_nodez00_3494)))->
													BgL_varz00);
												((((BgL_varz00_bglt) COBJECT(BgL_arg1866z00_3633))->
														BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															BgL_alphaz00_3617)), BUNSPEC);
											}
											{	/* Globalize/node.scm 317 */
												obj_t BgL_arg1868z00_3634;

												BgL_arg1868z00_3634 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_alphaz00_3617)))->
													BgL_fastzd2alphazd2);
												{
													obj_t BgL_alphaz00_5067;
													obj_t BgL_varz00_5066;

													BgL_varz00_5066 = BgL_alphaz00_3617;
													BgL_alphaz00_5067 = BgL_arg1868z00_3634;
													BgL_alphaz00_3617 = BgL_alphaz00_5067;
													BgL_varz00_3616 = BgL_varz00_5066;
													goto BgL_loopz00_3615;
												}
											}
										}
									else
										{	/* Globalize/node.scm 318 */
											BgL_variablez00_bglt BgL_varz00_3635;

											BgL_varz00_3635 =
												(((BgL_varz00_bglt) COBJECT(
														(((BgL_setqz00_bglt) COBJECT(
																	((BgL_setqz00_bglt) BgL_nodez00_3494)))->
															BgL_varz00)))->BgL_variablez00);
											{	/* Globalize/node.scm 319 */
												bool_t BgL_test2162z00_5071;

												{	/* Globalize/node.scm 319 */
													bool_t BgL_test2163z00_5072;

													{	/* Globalize/node.scm 319 */
														obj_t BgL_classz00_3636;

														BgL_classz00_3636 = BGl_localz00zzast_varz00;
														{	/* Globalize/node.scm 319 */
															BgL_objectz00_bglt BgL_arg1807z00_3637;

															{	/* Globalize/node.scm 319 */
																obj_t BgL_tmpz00_5073;

																BgL_tmpz00_5073 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_varz00_3635));
																BgL_arg1807z00_3637 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_5073);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Globalize/node.scm 319 */
																	long BgL_idxz00_3638;

																	BgL_idxz00_3638 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3637);
																	BgL_test2163z00_5072 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3638 + 2L)) ==
																		BgL_classz00_3636);
																}
															else
																{	/* Globalize/node.scm 319 */
																	bool_t BgL_res2040z00_3641;

																	{	/* Globalize/node.scm 319 */
																		obj_t BgL_oclassz00_3642;

																		{	/* Globalize/node.scm 319 */
																			obj_t BgL_arg1815z00_3643;
																			long BgL_arg1816z00_3644;

																			BgL_arg1815z00_3643 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Globalize/node.scm 319 */
																				long BgL_arg1817z00_3645;

																				BgL_arg1817z00_3645 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3637);
																				BgL_arg1816z00_3644 =
																					(BgL_arg1817z00_3645 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3642 =
																				VECTOR_REF(BgL_arg1815z00_3643,
																				BgL_arg1816z00_3644);
																		}
																		{	/* Globalize/node.scm 319 */
																			bool_t BgL__ortest_1115z00_3646;

																			BgL__ortest_1115z00_3646 =
																				(BgL_classz00_3636 ==
																				BgL_oclassz00_3642);
																			if (BgL__ortest_1115z00_3646)
																				{	/* Globalize/node.scm 319 */
																					BgL_res2040z00_3641 =
																						BgL__ortest_1115z00_3646;
																				}
																			else
																				{	/* Globalize/node.scm 319 */
																					long BgL_odepthz00_3647;

																					{	/* Globalize/node.scm 319 */
																						obj_t BgL_arg1804z00_3648;

																						BgL_arg1804z00_3648 =
																							(BgL_oclassz00_3642);
																						BgL_odepthz00_3647 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3648);
																					}
																					if ((2L < BgL_odepthz00_3647))
																						{	/* Globalize/node.scm 319 */
																							obj_t BgL_arg1802z00_3649;

																							{	/* Globalize/node.scm 319 */
																								obj_t BgL_arg1803z00_3650;

																								BgL_arg1803z00_3650 =
																									(BgL_oclassz00_3642);
																								BgL_arg1802z00_3649 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3650, 2L);
																							}
																							BgL_res2040z00_3641 =
																								(BgL_arg1802z00_3649 ==
																								BgL_classz00_3636);
																						}
																					else
																						{	/* Globalize/node.scm 319 */
																							BgL_res2040z00_3641 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2163z00_5072 = BgL_res2040z00_3641;
																}
														}
													}
													if (BgL_test2163z00_5072)
														{	/* Globalize/node.scm 319 */
															BgL_test2162z00_5071 =
																BGl_celledzf3zf3zzglobaliza7e_nodeza7
																(BgL_varz00_3635);
														}
													else
														{	/* Globalize/node.scm 319 */
															BgL_test2162z00_5071 = ((bool_t) 0);
														}
												}
												if (BgL_test2162z00_5071)
													{	/* Globalize/node.scm 320 */
														BgL_localz00_bglt BgL_azd2varzd2_3651;
														obj_t BgL_locz00_3652;

														BgL_azd2varzd2_3651 =
															BGl_makezd2localzd2svarz00zzast_localz00
															(CNST_TABLE_REF(9),
															((BgL_typez00_bglt)
																BGl_za2objza2z00zztype_cachez00));
														BgL_locz00_3652 =
															(((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_setqz00_bglt)
																			BgL_nodez00_3494))))->BgL_locz00);
														{	/* Globalize/node.scm 322 */
															obj_t BgL_vz00_3653;

															BgL_vz00_3653 = CNST_TABLE_REF(0);
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_varz00_3635))))->BgL_accessz00) =
																((obj_t) BgL_vz00_3653), BUNSPEC);
														}
														{	/* Globalize/node.scm 323 */
															bool_t BgL_arg1872z00_3654;

															BgL_arg1872z00_3654 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt) BgL_varz00_3635))))->
																BgL_userzf3zf3);
															((((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_azd2varzd2_3651)))->
																	BgL_userzf3zf3) =
																((bool_t) BgL_arg1872z00_3654), BUNSPEC);
														}
														{	/* Globalize/node.scm 324 */
															BgL_varz00_bglt BgL_arg1873z00_3655;

															BgL_arg1873z00_3655 =
																(((BgL_setqz00_bglt) COBJECT(
																		((BgL_setqz00_bglt) BgL_nodez00_3494)))->
																BgL_varz00);
															{	/* Globalize/node.scm 324 */
																BgL_typez00_bglt BgL_vz00_3656;

																BgL_vz00_3656 =
																	((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_arg1873z00_3655)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_vz00_3656), BUNSPEC);
															}
														}
														{	/* Globalize/node.scm 325 */
															BgL_svarz00_bglt BgL_tmp1153z00_3657;

															BgL_tmp1153z00_3657 =
																((BgL_svarz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_azd2varzd2_3651)))->BgL_valuez00));
															{	/* Globalize/node.scm 325 */
																BgL_svarzf2ginfozf2_bglt BgL_wide1155z00_3658;

																BgL_wide1155z00_3658 =
																	((BgL_svarzf2ginfozf2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_svarzf2ginfozf2_bgl))));
																{	/* Globalize/node.scm 325 */
																	obj_t BgL_auxz00_5124;
																	BgL_objectz00_bglt BgL_tmpz00_5121;

																	BgL_auxz00_5124 =
																		((obj_t) BgL_wide1155z00_3658);
																	BgL_tmpz00_5121 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1153z00_3657));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5121,
																		BgL_auxz00_5124);
																}
																((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1153z00_3657));
																{	/* Globalize/node.scm 325 */
																	long BgL_arg1874z00_3659;

																	BgL_arg1874z00_3659 =
																		BGL_CLASS_NUM
																		(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1153z00_3657)),
																		BgL_arg1874z00_3659);
																}
																((BgL_svarz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1153z00_3657));
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_5135;

																{
																	obj_t BgL_auxz00_5136;

																	{	/* Globalize/node.scm 326 */
																		BgL_objectz00_bglt BgL_tmpz00_5137;

																		BgL_tmpz00_5137 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1153z00_3657));
																		BgL_auxz00_5136 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5137);
																	}
																	BgL_auxz00_5135 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_5136);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_5135))->
																		BgL_kapturedzf3zf3) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_5143;

																{
																	obj_t BgL_auxz00_5144;

																	{	/* Globalize/node.scm 326 */
																		BgL_objectz00_bglt BgL_tmpz00_5145;

																		BgL_tmpz00_5145 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1153z00_3657));
																		BgL_auxz00_5144 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5145);
																	}
																	BgL_auxz00_5143 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_5144);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_5143))->
																		BgL_freezd2markzd2) =
																	((long) -10L), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_5151;

																{
																	obj_t BgL_auxz00_5152;

																	{	/* Globalize/node.scm 326 */
																		BgL_objectz00_bglt BgL_tmpz00_5153;

																		BgL_tmpz00_5153 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1153z00_3657));
																		BgL_auxz00_5152 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5153);
																	}
																	BgL_auxz00_5151 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_5152);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_5151))->BgL_markz00) =
																	((long) -10L), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_5159;

																{
																	obj_t BgL_auxz00_5160;

																	{	/* Globalize/node.scm 326 */
																		BgL_objectz00_bglt BgL_tmpz00_5161;

																		BgL_tmpz00_5161 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1153z00_3657));
																		BgL_auxz00_5160 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5161);
																	}
																	BgL_auxz00_5159 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_5160);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_5159))->
																		BgL_celledzf3zf3) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_5167;

																{
																	obj_t BgL_auxz00_5168;

																	{	/* Globalize/node.scm 326 */
																		BgL_objectz00_bglt BgL_tmpz00_5169;

																		BgL_tmpz00_5169 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1153z00_3657));
																		BgL_auxz00_5168 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5169);
																	}
																	BgL_auxz00_5167 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_5168);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_5167))->
																		BgL_stackablez00) =
																	((bool_t) ((bool_t) 1)), BUNSPEC);
															}
															((BgL_svarz00_bglt) BgL_tmp1153z00_3657);
														}
														{	/* Globalize/node.scm 327 */
															BgL_letzd2varzd2_bglt BgL_new1158z00_3660;

															{	/* Globalize/node.scm 328 */
																BgL_letzd2varzd2_bglt BgL_new1157z00_3661;

																BgL_new1157z00_3661 =
																	((BgL_letzd2varzd2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_letzd2varzd2_bgl))));
																{	/* Globalize/node.scm 328 */
																	long BgL_arg1880z00_3662;

																	BgL_arg1880z00_3662 =
																		BGL_CLASS_NUM
																		(BGl_letzd2varzd2zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			BgL_new1157z00_3661),
																		BgL_arg1880z00_3662);
																}
																{	/* Globalize/node.scm 328 */
																	BgL_objectz00_bglt BgL_tmpz00_5180;

																	BgL_tmpz00_5180 =
																		((BgL_objectz00_bglt) BgL_new1157z00_3661);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5180,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1157z00_3661);
																BgL_new1158z00_3660 = BgL_new1157z00_3661;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1158z00_3660)))->BgL_locz00) =
																((obj_t) BgL_locz00_3652), BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1158z00_3660)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BGl_za2unspecza2z00zztype_cachez00)),
																BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1158z00_3660)))->
																	BgL_sidezd2effectzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1158z00_3660)))->BgL_keyz00) =
																((obj_t) BINT(-1L)), BUNSPEC);
															{
																obj_t BgL_auxz00_5194;

																{	/* Globalize/node.scm 330 */
																	obj_t BgL_arg1875z00_3663;

																	{	/* Globalize/node.scm 330 */
																		BgL_nodez00_bglt BgL_arg1877z00_3664;

																		BgL_arg1877z00_3664 =
																			(((BgL_setqz00_bglt) COBJECT(
																					((BgL_setqz00_bglt)
																						BgL_nodez00_3494)))->BgL_valuez00);
																		BgL_arg1875z00_3663 =
																			MAKE_YOUNG_PAIR(((obj_t)
																				BgL_azd2varzd2_3651),
																			((obj_t) BgL_arg1877z00_3664));
																	}
																	{	/* Globalize/node.scm 330 */
																		obj_t BgL_list1876z00_3665;

																		BgL_list1876z00_3665 =
																			MAKE_YOUNG_PAIR(BgL_arg1875z00_3663,
																			BNIL);
																		BgL_auxz00_5194 = BgL_list1876z00_3665;
																}}
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1158z00_3660))->
																		BgL_bindingsz00) =
																	((obj_t) BgL_auxz00_5194), BUNSPEC);
															}
															{
																BgL_nodez00_bglt BgL_auxz00_5202;

																{	/* Globalize/node.scm 331 */
																	BgL_boxzd2setz12zc0_bglt BgL_new1160z00_3666;

																	{	/* Globalize/node.scm 332 */
																		BgL_boxzd2setz12zc0_bglt
																			BgL_new1159z00_3667;
																		BgL_new1159z00_3667 =
																			((BgL_boxzd2setz12zc0_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_boxzd2setz12zc0_bgl))));
																		{	/* Globalize/node.scm 332 */
																			long BgL_arg1879z00_3668;

																			{	/* Globalize/node.scm 332 */
																				obj_t BgL_classz00_3669;

																				BgL_classz00_3669 =
																					BGl_boxzd2setz12zc0zzast_nodez00;
																				BgL_arg1879z00_3668 =
																					BGL_CLASS_NUM(BgL_classz00_3669);
																			}
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1159z00_3667),
																				BgL_arg1879z00_3668);
																		}
																		{	/* Globalize/node.scm 332 */
																			BgL_objectz00_bglt BgL_tmpz00_5207;

																			BgL_tmpz00_5207 =
																				((BgL_objectz00_bglt)
																				BgL_new1159z00_3667);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5207,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1159z00_3667);
																		BgL_new1160z00_3666 = BgL_new1159z00_3667;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1160z00_3666)))->
																			BgL_locz00) =
																		((obj_t) BgL_locz00_3652), BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1160z00_3666)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2unspecza2z00zztype_cachez00)),
																		BUNSPEC);
																	((((BgL_boxzd2setz12zc0_bglt)
																				COBJECT(BgL_new1160z00_3666))->
																			BgL_varz00) =
																		((BgL_varz00_bglt) (((BgL_setqz00_bglt)
																					COBJECT(((BgL_setqz00_bglt)
																							BgL_nodez00_3494)))->BgL_varz00)),
																		BUNSPEC);
																	{
																		BgL_nodez00_bglt BgL_auxz00_5219;

																		{	/* Globalize/node.scm 336 */
																			BgL_refz00_bglt BgL_new1162z00_3670;

																			{	/* Globalize/node.scm 337 */
																				BgL_refz00_bglt BgL_new1161z00_3671;

																				BgL_new1161z00_3671 =
																					((BgL_refz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_refz00_bgl))));
																				{	/* Globalize/node.scm 337 */
																					long BgL_arg1878z00_3672;

																					{	/* Globalize/node.scm 337 */
																						obj_t BgL_classz00_3673;

																						BgL_classz00_3673 =
																							BGl_refz00zzast_nodez00;
																						BgL_arg1878z00_3672 =
																							BGL_CLASS_NUM(BgL_classz00_3673);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1161z00_3671),
																						BgL_arg1878z00_3672);
																				}
																				{	/* Globalize/node.scm 337 */
																					BgL_objectz00_bglt BgL_tmpz00_5224;

																					BgL_tmpz00_5224 =
																						((BgL_objectz00_bglt)
																						BgL_new1161z00_3671);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_5224, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1161z00_3671);
																				BgL_new1162z00_3670 =
																					BgL_new1161z00_3671;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1162z00_3670)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_3652), BUNSPEC);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_new1162z00_3670)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((
																							(BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_azd2varzd2_3651)))->
																						BgL_typez00)), BUNSPEC);
																			((((BgL_varz00_bglt)
																						COBJECT(((BgL_varz00_bglt)
																								BgL_new1162z00_3670)))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) (
																						(BgL_variablez00_bglt)
																						BgL_azd2varzd2_3651)), BUNSPEC);
																			BgL_auxz00_5219 =
																				((BgL_nodez00_bglt)
																				BgL_new1162z00_3670);
																		}
																		((((BgL_boxzd2setz12zc0_bglt)
																					COBJECT(BgL_new1160z00_3666))->
																				BgL_valuez00) =
																			((BgL_nodez00_bglt) BgL_auxz00_5219),
																			BUNSPEC);
																	}
																	((((BgL_boxzd2setz12zc0_bglt)
																				COBJECT(BgL_new1160z00_3666))->
																			BgL_vtypez00) =
																		((BgL_typez00_bglt)
																			BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
																			(BgL_vtypez00_3613)), BUNSPEC);
																	BgL_auxz00_5202 =
																		((BgL_nodez00_bglt) BgL_new1160z00_3666);
																}
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1158z00_3660))->
																		BgL_bodyz00) =
																	((BgL_nodez00_bglt) BgL_auxz00_5202),
																	BUNSPEC);
															}
															((((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_new1158z00_3660))->
																	BgL_removablezf3zf3) =
																((bool_t) ((bool_t) 1)), BUNSPEC);
															return ((BgL_nodez00_bglt) BgL_new1158z00_3660);
														}
													}
												else
													{	/* Globalize/node.scm 319 */
														return
															((BgL_nodez00_bglt)
															((BgL_setqz00_bglt) BgL_nodez00_3494));
													}
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &glo!-cast1392 */
	BgL_nodez00_bglt BGl_z62gloz12zd2cast1392za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3496, obj_t BgL_nodez00_3497, obj_t BgL_integratorz00_3498)
	{
		{	/* Globalize/node.scm 299 */
			{	/* Globalize/node.scm 301 */
				BgL_nodez00_bglt BgL_arg1862z00_3675;

				BgL_arg1862z00_3675 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_3497)))->BgL_argz00);
				BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1862z00_3675,
					((BgL_variablez00_bglt) BgL_integratorz00_3498));
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_3497));
		}

	}



/* &glo!-extern1390 */
	BgL_nodez00_bglt BGl_z62gloz12zd2extern1390za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3499, obj_t BgL_nodez00_3500, obj_t BgL_integratorz00_3501)
	{
		{	/* Globalize/node.scm 291 */
			BGl_gloza2z12zb0zzglobaliza7e_nodeza7(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_3500)))->BgL_exprza2za2),
				BgL_integratorz00_3501);
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_3500));
		}

	}



/* &glo!-funcall1388 */
	BgL_nodez00_bglt BGl_z62gloz12zd2funcall1388za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3502, obj_t BgL_nodez00_3503, obj_t BgL_integratorz00_3504)
	{
		{	/* Globalize/node.scm 282 */
			{
				BgL_nodez00_bglt BgL_auxz00_5259;

				{	/* Globalize/node.scm 284 */
					BgL_nodez00_bglt BgL_arg1858z00_3678;

					BgL_arg1858z00_3678 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3503)))->BgL_funz00);
					BgL_auxz00_5259 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1858z00_3678,
						((BgL_variablez00_bglt) BgL_integratorz00_3504));
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3503)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5259), BUNSPEC);
			}
			BGl_gloza2z12zb0zzglobaliza7e_nodeza7(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_3503)))->BgL_argsz00),
				BgL_integratorz00_3504);
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_3503));
		}

	}



/* &glo!-app-ly1386 */
	BgL_nodez00_bglt BGl_z62gloz12zd2appzd2ly1386z70zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3505, obj_t BgL_nodez00_3506, obj_t BgL_integratorz00_3507)
	{
		{	/* Globalize/node.scm 273 */
			{
				BgL_nodez00_bglt BgL_auxz00_5271;

				{	/* Globalize/node.scm 275 */
					BgL_nodez00_bglt BgL_arg1856z00_3680;

					BgL_arg1856z00_3680 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3506)))->BgL_funz00);
					BgL_auxz00_5271 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1856z00_3680,
						((BgL_variablez00_bglt) BgL_integratorz00_3507));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3506)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5271), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5278;

				{	/* Globalize/node.scm 276 */
					BgL_nodez00_bglt BgL_arg1857z00_3681;

					BgL_arg1857z00_3681 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3506)))->BgL_argz00);
					BgL_auxz00_5278 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1857z00_3681,
						((BgL_variablez00_bglt) BgL_integratorz00_3507));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3506)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5278), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_3506));
		}

	}



/* &glo!-app1384 */
	BgL_nodez00_bglt BGl_z62gloz12zd2app1384za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3508, obj_t BgL_nodez00_3509, obj_t BgL_integratorz00_3510)
	{
		{	/* Globalize/node.scm 208 */
			{	/* Globalize/node.scm 210 */
				BgL_variablez00_bglt BgL_funz00_3683;

				BgL_funz00_3683 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_3509)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Globalize/node.scm 210 */
					BgL_valuez00_bglt BgL_infoz00_3684;

					BgL_infoz00_3684 =
						(((BgL_variablez00_bglt) COBJECT(BgL_funz00_3683))->BgL_valuez00);
					{	/* Globalize/node.scm 211 */

						{	/* Globalize/node.scm 220 */
							bool_t BgL_test2167z00_5291;

							{	/* Globalize/node.scm 220 */
								bool_t BgL_test2168z00_5292;

								{	/* Globalize/node.scm 220 */
									obj_t BgL_classz00_3685;

									BgL_classz00_3685 = BGl_localz00zzast_varz00;
									{	/* Globalize/node.scm 220 */
										BgL_objectz00_bglt BgL_arg1807z00_3686;

										{	/* Globalize/node.scm 220 */
											obj_t BgL_tmpz00_5293;

											BgL_tmpz00_5293 =
												((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3683));
											BgL_arg1807z00_3686 =
												(BgL_objectz00_bglt) (BgL_tmpz00_5293);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Globalize/node.scm 220 */
												long BgL_idxz00_3687;

												BgL_idxz00_3687 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3686);
												BgL_test2168z00_5292 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3687 + 2L)) == BgL_classz00_3685);
											}
										else
											{	/* Globalize/node.scm 220 */
												bool_t BgL_res2036z00_3690;

												{	/* Globalize/node.scm 220 */
													obj_t BgL_oclassz00_3691;

													{	/* Globalize/node.scm 220 */
														obj_t BgL_arg1815z00_3692;
														long BgL_arg1816z00_3693;

														BgL_arg1815z00_3692 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Globalize/node.scm 220 */
															long BgL_arg1817z00_3694;

															BgL_arg1817z00_3694 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3686);
															BgL_arg1816z00_3693 =
																(BgL_arg1817z00_3694 - OBJECT_TYPE);
														}
														BgL_oclassz00_3691 =
															VECTOR_REF(BgL_arg1815z00_3692,
															BgL_arg1816z00_3693);
													}
													{	/* Globalize/node.scm 220 */
														bool_t BgL__ortest_1115z00_3695;

														BgL__ortest_1115z00_3695 =
															(BgL_classz00_3685 == BgL_oclassz00_3691);
														if (BgL__ortest_1115z00_3695)
															{	/* Globalize/node.scm 220 */
																BgL_res2036z00_3690 = BgL__ortest_1115z00_3695;
															}
														else
															{	/* Globalize/node.scm 220 */
																long BgL_odepthz00_3696;

																{	/* Globalize/node.scm 220 */
																	obj_t BgL_arg1804z00_3697;

																	BgL_arg1804z00_3697 = (BgL_oclassz00_3691);
																	BgL_odepthz00_3696 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3697);
																}
																if ((2L < BgL_odepthz00_3696))
																	{	/* Globalize/node.scm 220 */
																		obj_t BgL_arg1802z00_3698;

																		{	/* Globalize/node.scm 220 */
																			obj_t BgL_arg1803z00_3699;

																			BgL_arg1803z00_3699 =
																				(BgL_oclassz00_3691);
																			BgL_arg1802z00_3698 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3699, 2L);
																		}
																		BgL_res2036z00_3690 =
																			(BgL_arg1802z00_3698 ==
																			BgL_classz00_3685);
																	}
																else
																	{	/* Globalize/node.scm 220 */
																		BgL_res2036z00_3690 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2168z00_5292 = BgL_res2036z00_3690;
											}
									}
								}
								if (BgL_test2168z00_5292)
									{	/* Globalize/node.scm 221 */
										bool_t BgL_test2172z00_5316;

										if ((((obj_t) BgL_funz00_3683) == BgL_integratorz00_3510))
											{	/* Globalize/node.scm 222 */
												bool_t BgL_test2174z00_5320;

												{
													BgL_localzf2ginfozf2_bglt BgL_auxz00_5321;

													{
														obj_t BgL_auxz00_5322;

														{	/* Globalize/node.scm 222 */
															BgL_objectz00_bglt BgL_tmpz00_5323;

															BgL_tmpz00_5323 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_funz00_3683));
															BgL_auxz00_5322 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_5323);
														}
														BgL_auxz00_5321 =
															((BgL_localzf2ginfozf2_bglt) BgL_auxz00_5322);
													}
													BgL_test2174z00_5320 =
														(((BgL_localzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_5321))->BgL_escapezf3zf3);
												}
												if (BgL_test2174z00_5320)
													{	/* Globalize/node.scm 222 */
														BgL_test2172z00_5316 = ((bool_t) 0);
													}
												else
													{	/* Globalize/node.scm 222 */
														BgL_test2172z00_5316 = ((bool_t) 1);
													}
											}
										else
											{	/* Globalize/node.scm 221 */
												BgL_test2172z00_5316 = ((bool_t) 1);
											}
										if (BgL_test2172z00_5316)
											{
												BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5329;

												{
													obj_t BgL_auxz00_5330;

													{	/* Globalize/node.scm 223 */
														BgL_objectz00_bglt BgL_tmpz00_5331;

														BgL_tmpz00_5331 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_infoz00_3684));
														BgL_auxz00_5330 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5331);
													}
													BgL_auxz00_5329 =
														((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5330);
												}
												BgL_test2167z00_5291 =
													(((BgL_sfunzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_5329))->BgL_gzf3zf3);
											}
										else
											{	/* Globalize/node.scm 221 */
												BgL_test2167z00_5291 = ((bool_t) 0);
											}
									}
								else
									{	/* Globalize/node.scm 220 */
										BgL_test2167z00_5291 = ((bool_t) 0);
									}
							}
							if (BgL_test2167z00_5291)
								{	/* Globalize/node.scm 225 */
									BgL_refz00_bglt BgL_arg1820z00_3700;

									{	/* Globalize/node.scm 225 */
										BgL_refz00_bglt BgL_new1139z00_3701;

										{	/* Globalize/node.scm 226 */
											BgL_refz00_bglt BgL_new1138z00_3702;

											BgL_new1138z00_3702 =
												((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_refz00_bgl))));
											{	/* Globalize/node.scm 226 */
												long BgL_arg1822z00_3703;

												BgL_arg1822z00_3703 =
													BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1138z00_3702),
													BgL_arg1822z00_3703);
											}
											{	/* Globalize/node.scm 226 */
												BgL_objectz00_bglt BgL_tmpz00_5341;

												BgL_tmpz00_5341 =
													((BgL_objectz00_bglt) BgL_new1138z00_3702);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5341, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1138z00_3702);
											BgL_new1139z00_3701 = BgL_new1138z00_3702;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1139z00_3701)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
																	BgL_nodez00_3509))))->BgL_locz00)), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1139z00_3701)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
																	BgL_nodez00_3509))))->BgL_typez00)), BUNSPEC);
										((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
															BgL_new1139z00_3701)))->BgL_variablez00) =
											((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
													BGl_thezd2globalzd2zzglobaliza7e_localzd2ze3globalz96(
														((BgL_localz00_bglt) BgL_funz00_3683)))), BUNSPEC);
										BgL_arg1820z00_3700 = BgL_new1139z00_3701;
									}
									((((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_3509)))->BgL_funz00) =
										((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1820z00_3700)),
										BUNSPEC);
								}
							else
								{	/* Globalize/node.scm 220 */
									BFALSE;
								}
						}
						{
							obj_t BgL_nodesz00_3705;

							BgL_nodesz00_3705 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_3509)))->BgL_argsz00);
						BgL_liipz00_3704:
							if (NULLP(BgL_nodesz00_3705))
								{	/* Globalize/node.scm 232 */
									CNST_TABLE_REF(2);
								}
							else
								{	/* Globalize/node.scm 232 */
									{	/* Globalize/node.scm 235 */
										BgL_nodez00_bglt BgL_arg1831z00_3706;

										{	/* Globalize/node.scm 235 */
											obj_t BgL_arg1832z00_3707;

											BgL_arg1832z00_3707 = CAR(((obj_t) BgL_nodesz00_3705));
											BgL_arg1831z00_3706 =
												BGl_gloz12z12zzglobaliza7e_nodeza7(
												((BgL_nodez00_bglt) BgL_arg1832z00_3707),
												((BgL_variablez00_bglt) BgL_integratorz00_3510));
										}
										{	/* Globalize/node.scm 235 */
											obj_t BgL_auxz00_5373;
											obj_t BgL_tmpz00_5371;

											BgL_auxz00_5373 = ((obj_t) BgL_arg1831z00_3706);
											BgL_tmpz00_5371 = ((obj_t) BgL_nodesz00_3705);
											SET_CAR(BgL_tmpz00_5371, BgL_auxz00_5373);
										}
									}
									{	/* Globalize/node.scm 236 */
										obj_t BgL_arg1833z00_3708;

										BgL_arg1833z00_3708 = CDR(((obj_t) BgL_nodesz00_3705));
										{
											obj_t BgL_nodesz00_5378;

											BgL_nodesz00_5378 = BgL_arg1833z00_3708;
											BgL_nodesz00_3705 = BgL_nodesz00_5378;
											goto BgL_liipz00_3704;
										}
									}
								}
						}
						{	/* Globalize/node.scm 238 */
							bool_t BgL_test2176z00_5381;

							{	/* Globalize/node.scm 238 */
								bool_t BgL_test2177z00_5382;

								{	/* Globalize/node.scm 238 */
									obj_t BgL_classz00_3709;

									BgL_classz00_3709 = BGl_globalz00zzast_varz00;
									{	/* Globalize/node.scm 238 */
										BgL_objectz00_bglt BgL_arg1807z00_3710;

										{	/* Globalize/node.scm 238 */
											obj_t BgL_tmpz00_5383;

											BgL_tmpz00_5383 =
												((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3683));
											BgL_arg1807z00_3710 =
												(BgL_objectz00_bglt) (BgL_tmpz00_5383);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Globalize/node.scm 238 */
												long BgL_idxz00_3711;

												BgL_idxz00_3711 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3710);
												BgL_test2177z00_5382 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3711 + 2L)) == BgL_classz00_3709);
											}
										else
											{	/* Globalize/node.scm 238 */
												bool_t BgL_res2037z00_3714;

												{	/* Globalize/node.scm 238 */
													obj_t BgL_oclassz00_3715;

													{	/* Globalize/node.scm 238 */
														obj_t BgL_arg1815z00_3716;
														long BgL_arg1816z00_3717;

														BgL_arg1815z00_3716 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Globalize/node.scm 238 */
															long BgL_arg1817z00_3718;

															BgL_arg1817z00_3718 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3710);
															BgL_arg1816z00_3717 =
																(BgL_arg1817z00_3718 - OBJECT_TYPE);
														}
														BgL_oclassz00_3715 =
															VECTOR_REF(BgL_arg1815z00_3716,
															BgL_arg1816z00_3717);
													}
													{	/* Globalize/node.scm 238 */
														bool_t BgL__ortest_1115z00_3719;

														BgL__ortest_1115z00_3719 =
															(BgL_classz00_3709 == BgL_oclassz00_3715);
														if (BgL__ortest_1115z00_3719)
															{	/* Globalize/node.scm 238 */
																BgL_res2037z00_3714 = BgL__ortest_1115z00_3719;
															}
														else
															{	/* Globalize/node.scm 238 */
																long BgL_odepthz00_3720;

																{	/* Globalize/node.scm 238 */
																	obj_t BgL_arg1804z00_3721;

																	BgL_arg1804z00_3721 = (BgL_oclassz00_3715);
																	BgL_odepthz00_3720 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3721);
																}
																if ((2L < BgL_odepthz00_3720))
																	{	/* Globalize/node.scm 238 */
																		obj_t BgL_arg1802z00_3722;

																		{	/* Globalize/node.scm 238 */
																			obj_t BgL_arg1803z00_3723;

																			BgL_arg1803z00_3723 =
																				(BgL_oclassz00_3715);
																			BgL_arg1802z00_3722 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3723, 2L);
																		}
																		BgL_res2037z00_3714 =
																			(BgL_arg1802z00_3722 ==
																			BgL_classz00_3709);
																	}
																else
																	{	/* Globalize/node.scm 238 */
																		BgL_res2037z00_3714 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2177z00_5382 = BgL_res2037z00_3714;
											}
									}
								}
								if (BgL_test2177z00_5382)
									{	/* Globalize/node.scm 238 */
										BgL_test2176z00_5381 = ((bool_t) 1);
									}
								else
									{	/* Globalize/node.scm 239 */
										bool_t BgL_test2181z00_5406;

										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5407;

											{
												obj_t BgL_auxz00_5408;

												{	/* Globalize/node.scm 239 */
													BgL_objectz00_bglt BgL_tmpz00_5409;

													BgL_tmpz00_5409 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_infoz00_3684));
													BgL_auxz00_5408 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5409);
												}
												BgL_auxz00_5407 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5408);
											}
											BgL_test2181z00_5406 =
												(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5407))->
												BgL_gzf3zf3);
										}
										if (BgL_test2181z00_5406)
											{	/* Globalize/node.scm 239 */
												if (
													(((obj_t) BgL_funz00_3683) == BgL_integratorz00_3510))
													{
														BgL_localzf2ginfozf2_bglt BgL_auxz00_5418;

														{
															obj_t BgL_auxz00_5419;

															{	/* Globalize/node.scm 241 */
																BgL_objectz00_bglt BgL_tmpz00_5420;

																BgL_tmpz00_5420 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt) BgL_funz00_3683));
																BgL_auxz00_5419 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5420);
															}
															BgL_auxz00_5418 =
																((BgL_localzf2ginfozf2_bglt) BgL_auxz00_5419);
														}
														BgL_test2176z00_5381 =
															(((BgL_localzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_5418))->BgL_escapezf3zf3);
													}
												else
													{	/* Globalize/node.scm 240 */
														BgL_test2176z00_5381 = ((bool_t) 0);
													}
											}
										else
											{	/* Globalize/node.scm 239 */
												BgL_test2176z00_5381 = ((bool_t) 1);
											}
									}
							}
							if (BgL_test2176z00_5381)
								{	/* Globalize/node.scm 238 */
									CNST_TABLE_REF(2);
								}
							else
								{	/* Globalize/node.scm 243 */
									bool_t BgL_test2183z00_5427;

									{
										BgL_localzf2ginfozf2_bglt BgL_auxz00_5428;

										{
											obj_t BgL_auxz00_5429;

											{	/* Globalize/node.scm 243 */
												BgL_objectz00_bglt BgL_tmpz00_5430;

												BgL_tmpz00_5430 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt) BgL_funz00_3683));
												BgL_auxz00_5429 = BGL_OBJECT_WIDENING(BgL_tmpz00_5430);
											}
											BgL_auxz00_5428 =
												((BgL_localzf2ginfozf2_bglt) BgL_auxz00_5429);
										}
										BgL_test2183z00_5427 =
											(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5428))->
											BgL_escapezf3zf3);
									}
									if (BgL_test2183z00_5427)
										{
											obj_t BgL_auxz00_5436;

											{	/* Globalize/node.scm 246 */
												BgL_nodez00_bglt BgL_arg1838z00_3724;
												obj_t BgL_arg1839z00_3725;

												{	/* Globalize/node.scm 246 */
													BgL_refz00_bglt BgL_arg1840z00_3726;

													{	/* Globalize/node.scm 246 */
														BgL_refz00_bglt BgL_new1142z00_3727;

														{	/* Globalize/node.scm 249 */
															BgL_refz00_bglt BgL_new1141z00_3728;

															BgL_new1141z00_3728 =
																((BgL_refz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_refz00_bgl))));
															{	/* Globalize/node.scm 249 */
																long BgL_arg1845z00_3729;

																{	/* Globalize/node.scm 249 */
																	obj_t BgL_classz00_3730;

																	BgL_classz00_3730 = BGl_refz00zzast_nodez00;
																	BgL_arg1845z00_3729 =
																		BGL_CLASS_NUM(BgL_classz00_3730);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1141z00_3728),
																	BgL_arg1845z00_3729);
															}
															{	/* Globalize/node.scm 249 */
																BgL_objectz00_bglt BgL_tmpz00_5442;

																BgL_tmpz00_5442 =
																	((BgL_objectz00_bglt) BgL_new1141z00_3728);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5442,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1141z00_3728);
															BgL_new1142z00_3727 = BgL_new1141z00_3728;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1142z00_3727)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_appz00_bglt)
																					BgL_nodez00_3509))))->BgL_locz00)),
															BUNSPEC);
														{
															BgL_typez00_bglt BgL_auxz00_5451;

															{	/* Globalize/node.scm 248 */
																obj_t BgL_arg1842z00_3731;

																{	/* Globalize/node.scm 248 */
																	obj_t BgL_arg1843z00_3732;

																	BgL_arg1843z00_3732 =
																		(((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					((BgL_appz00_bglt)
																						BgL_nodez00_3509))))->BgL_locz00);
																	BgL_arg1842z00_3731 =
																		BGl_thezd2closurezd2zzglobaliza7e_freeza7
																		(BgL_funz00_3683, BgL_arg1843z00_3732);
																}
																BgL_auxz00_5451 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_arg1842z00_3731)))->BgL_typez00);
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1142z00_3727)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_auxz00_5451), BUNSPEC);
														}
														{
															BgL_variablez00_bglt BgL_auxz00_5460;

															{	/* Globalize/node.scm 249 */
																obj_t BgL_arg1844z00_3733;

																BgL_arg1844z00_3733 =
																	(((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				((BgL_appz00_bglt)
																					BgL_nodez00_3509))))->BgL_locz00);
																BgL_auxz00_5460 =
																	((BgL_variablez00_bglt)
																	BGl_thezd2closurezd2zzglobaliza7e_freeza7
																	(BgL_funz00_3683, BgL_arg1844z00_3733));
															}
															((((BgL_varz00_bglt) COBJECT(
																			((BgL_varz00_bglt)
																				BgL_new1142z00_3727)))->
																	BgL_variablez00) =
																((BgL_variablez00_bglt) BgL_auxz00_5460),
																BUNSPEC);
														}
														BgL_arg1840z00_3726 = BgL_new1142z00_3727;
													}
													BgL_arg1838z00_3724 =
														BGl_gloz12z12zzglobaliza7e_nodeza7(
														((BgL_nodez00_bglt) BgL_arg1840z00_3726),
														((BgL_variablez00_bglt) BgL_integratorz00_3510));
												}
												BgL_arg1839z00_3725 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_3509)))->
													BgL_argsz00);
												BgL_auxz00_5436 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1838z00_3724),
													BgL_arg1839z00_3725);
											}
											((((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_3509)))->
													BgL_argsz00) = ((obj_t) BgL_auxz00_5436), BUNSPEC);
										}
									else
										{	/* Globalize/node.scm 255 */
											obj_t BgL_g1143z00_3734;

											{
												BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5476;

												{
													obj_t BgL_auxz00_5477;

													{	/* Globalize/node.scm 256 */
														BgL_objectz00_bglt BgL_tmpz00_5478;

														BgL_tmpz00_5478 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_infoz00_3684));
														BgL_auxz00_5477 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5478);
													}
													BgL_auxz00_5476 =
														((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5477);
												}
												BgL_g1143z00_3734 =
													(((BgL_sfunzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_5476))->BgL_kapturedz00);
											}
											{
												obj_t BgL_newzd2actualszd2_3736;
												obj_t BgL_kapturedz00_3737;

												BgL_newzd2actualszd2_3736 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_3509)))->
													BgL_argsz00);
												BgL_kapturedz00_3737 = BgL_g1143z00_3734;
											BgL_loopz00_3735:
												if (NULLP(BgL_kapturedz00_3737))
													{	/* Globalize/node.scm 257 */
														((((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_nodez00_3509)))->
																BgL_argsz00) =
															((obj_t) BgL_newzd2actualszd2_3736), BUNSPEC);
													}
												else
													{	/* Globalize/node.scm 259 */
														obj_t BgL_kapz00_3738;

														BgL_kapz00_3738 =
															CAR(((obj_t) BgL_kapturedz00_3737));
														{	/* Globalize/node.scm 259 */
															obj_t BgL_alphaz00_3739;

															BgL_alphaz00_3739 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt) BgL_kapz00_3738))))->
																BgL_fastzd2alphazd2);
															{	/* Globalize/node.scm 260 */
																obj_t BgL_varz00_3740;

																{	/* Globalize/node.scm 261 */
																	bool_t BgL_test2185z00_5493;

																	{	/* Globalize/node.scm 261 */
																		obj_t BgL_classz00_3741;

																		BgL_classz00_3741 =
																			BGl_localz00zzast_varz00;
																		if (BGL_OBJECTP(BgL_alphaz00_3739))
																			{	/* Globalize/node.scm 261 */
																				BgL_objectz00_bglt BgL_arg1807z00_3742;

																				BgL_arg1807z00_3742 =
																					(BgL_objectz00_bglt)
																					(BgL_alphaz00_3739);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Globalize/node.scm 261 */
																						long BgL_idxz00_3743;

																						BgL_idxz00_3743 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_3742);
																						BgL_test2185z00_5493 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_3743 + 2L)) ==
																							BgL_classz00_3741);
																					}
																				else
																					{	/* Globalize/node.scm 261 */
																						bool_t BgL_res2038z00_3746;

																						{	/* Globalize/node.scm 261 */
																							obj_t BgL_oclassz00_3747;

																							{	/* Globalize/node.scm 261 */
																								obj_t BgL_arg1815z00_3748;
																								long BgL_arg1816z00_3749;

																								BgL_arg1815z00_3748 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Globalize/node.scm 261 */
																									long BgL_arg1817z00_3750;

																									BgL_arg1817z00_3750 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_3742);
																									BgL_arg1816z00_3749 =
																										(BgL_arg1817z00_3750 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_3747 =
																									VECTOR_REF
																									(BgL_arg1815z00_3748,
																									BgL_arg1816z00_3749);
																							}
																							{	/* Globalize/node.scm 261 */
																								bool_t BgL__ortest_1115z00_3751;

																								BgL__ortest_1115z00_3751 =
																									(BgL_classz00_3741 ==
																									BgL_oclassz00_3747);
																								if (BgL__ortest_1115z00_3751)
																									{	/* Globalize/node.scm 261 */
																										BgL_res2038z00_3746 =
																											BgL__ortest_1115z00_3751;
																									}
																								else
																									{	/* Globalize/node.scm 261 */
																										long BgL_odepthz00_3752;

																										{	/* Globalize/node.scm 261 */
																											obj_t BgL_arg1804z00_3753;

																											BgL_arg1804z00_3753 =
																												(BgL_oclassz00_3747);
																											BgL_odepthz00_3752 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_3753);
																										}
																										if (
																											(2L < BgL_odepthz00_3752))
																											{	/* Globalize/node.scm 261 */
																												obj_t
																													BgL_arg1802z00_3754;
																												{	/* Globalize/node.scm 261 */
																													obj_t
																														BgL_arg1803z00_3755;
																													BgL_arg1803z00_3755 =
																														(BgL_oclassz00_3747);
																													BgL_arg1802z00_3754 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_3755,
																														2L);
																												}
																												BgL_res2038z00_3746 =
																													(BgL_arg1802z00_3754
																													== BgL_classz00_3741);
																											}
																										else
																											{	/* Globalize/node.scm 261 */
																												BgL_res2038z00_3746 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2185z00_5493 =
																							BgL_res2038z00_3746;
																					}
																			}
																		else
																			{	/* Globalize/node.scm 261 */
																				BgL_test2185z00_5493 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test2185z00_5493)
																		{	/* Globalize/node.scm 261 */
																			BgL_varz00_3740 = BgL_alphaz00_3739;
																		}
																	else
																		{	/* Globalize/node.scm 261 */
																			BgL_varz00_3740 = BgL_kapz00_3738;
																		}
																}
																{	/* Globalize/node.scm 261 */

																	{	/* Globalize/node.scm 262 */
																		obj_t BgL_arg1849z00_3756;
																		obj_t BgL_arg1850z00_3757;

																		{	/* Globalize/node.scm 262 */
																			BgL_refz00_bglt BgL_arg1851z00_3758;

																			{	/* Globalize/node.scm 262 */
																				BgL_refz00_bglt BgL_new1145z00_3759;

																				{	/* Globalize/node.scm 265 */
																					BgL_refz00_bglt BgL_new1144z00_3760;

																					BgL_new1144z00_3760 =
																						((BgL_refz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_refz00_bgl))));
																					{	/* Globalize/node.scm 265 */
																						long BgL_arg1852z00_3761;

																						BgL_arg1852z00_3761 =
																							BGL_CLASS_NUM
																							(BGl_refz00zzast_nodez00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1144z00_3760),
																							BgL_arg1852z00_3761);
																					}
																					{	/* Globalize/node.scm 265 */
																						BgL_objectz00_bglt BgL_tmpz00_5520;

																						BgL_tmpz00_5520 =
																							((BgL_objectz00_bglt)
																							BgL_new1144z00_3760);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_5520, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1144z00_3760);
																					BgL_new1145z00_3759 =
																						BgL_new1144z00_3760;
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1145z00_3759)))->
																						BgL_locz00) =
																					((obj_t) (((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt) (
																											(BgL_appz00_bglt)
																											BgL_nodez00_3509))))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_new1145z00_3759)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) ((
																								(BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_varz00_3740)))->
																							BgL_typez00)), BUNSPEC);
																				((((BgL_varz00_bglt)
																							COBJECT(((BgL_varz00_bglt)
																									BgL_new1145z00_3759)))->
																						BgL_variablez00) =
																					((BgL_variablez00_bglt) (
																							(BgL_variablez00_bglt)
																							BgL_varz00_3740)), BUNSPEC);
																				BgL_arg1851z00_3758 =
																					BgL_new1145z00_3759;
																			}
																			BgL_arg1849z00_3756 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_arg1851z00_3758),
																				BgL_newzd2actualszd2_3736);
																		}
																		BgL_arg1850z00_3757 =
																			CDR(((obj_t) BgL_kapturedz00_3737));
																		{
																			obj_t BgL_kapturedz00_5541;
																			obj_t BgL_newzd2actualszd2_5540;

																			BgL_newzd2actualszd2_5540 =
																				BgL_arg1849z00_3756;
																			BgL_kapturedz00_5541 =
																				BgL_arg1850z00_3757;
																			BgL_kapturedz00_3737 =
																				BgL_kapturedz00_5541;
																			BgL_newzd2actualszd2_3736 =
																				BgL_newzd2actualszd2_5540;
																			goto BgL_loopz00_3735;
																		}
																	}
																}
															}
														}
													}
											}
										}
								}
						}
						return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3509));
					}
				}
			}
		}

	}



/* &glo!-sync1382 */
	BgL_nodez00_bglt BGl_z62gloz12zd2sync1382za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3511, obj_t BgL_nodez00_3512, obj_t BgL_integratorz00_3513)
	{
		{	/* Globalize/node.scm 198 */
			{
				BgL_nodez00_bglt BgL_auxz00_5546;

				{	/* Globalize/node.scm 200 */
					BgL_nodez00_bglt BgL_arg1806z00_3763;

					BgL_arg1806z00_3763 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3512)))->BgL_mutexz00);
					BgL_auxz00_5546 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1806z00_3763,
						((BgL_variablez00_bglt) BgL_integratorz00_3513));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3512)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5546), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5553;

				{	/* Globalize/node.scm 201 */
					BgL_nodez00_bglt BgL_arg1808z00_3764;

					BgL_arg1808z00_3764 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3512)))->BgL_prelockz00);
					BgL_auxz00_5553 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1808z00_3764,
						((BgL_variablez00_bglt) BgL_integratorz00_3513));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3512)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5553), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5560;

				{	/* Globalize/node.scm 202 */
					BgL_nodez00_bglt BgL_arg1812z00_3765;

					BgL_arg1812z00_3765 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3512)))->BgL_bodyz00);
					BgL_auxz00_5560 =
						BGl_gloz12z12zzglobaliza7e_nodeza7(BgL_arg1812z00_3765,
						((BgL_variablez00_bglt) BgL_integratorz00_3513));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3512)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5560), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_3512));
		}

	}



/* &glo!-sequence1380 */
	BgL_nodez00_bglt BGl_z62gloz12zd2sequence1380za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3514, obj_t BgL_nodez00_3515, obj_t BgL_integratorz00_3516)
	{
		{	/* Globalize/node.scm 190 */
			BGl_gloza2z12zb0zzglobaliza7e_nodeza7(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_3515)))->BgL_nodesz00),
				BgL_integratorz00_3516);
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_3515));
		}

	}



/* &glo!-closure1377 */
	BgL_nodez00_bglt BGl_z62gloz12zd2closure1377za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3517, obj_t BgL_nodez00_3518, obj_t BgL_integratorz00_3519)
	{
		{	/* Globalize/node.scm 179 */
			{	/* Globalize/node.scm 181 */
				BgL_refz00_bglt BgL_arg1773z00_3768;

				{	/* Globalize/node.scm 181 */
					BgL_refz00_bglt BgL_new1134z00_3769;

					{	/* Globalize/node.scm 182 */
						BgL_refz00_bglt BgL_new1133z00_3770;

						BgL_new1133z00_3770 =
							((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_refz00_bgl))));
						{	/* Globalize/node.scm 182 */
							long BgL_arg1799z00_3771;

							BgL_arg1799z00_3771 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1133z00_3770),
								BgL_arg1799z00_3771);
						}
						{	/* Globalize/node.scm 182 */
							BgL_objectz00_bglt BgL_tmpz00_5578;

							BgL_tmpz00_5578 = ((BgL_objectz00_bglt) BgL_new1133z00_3770);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5578, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1133z00_3770);
						BgL_new1134z00_3769 = BgL_new1133z00_3770;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1134z00_3769)))->BgL_locz00) =
						((obj_t) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_closurez00_bglt)
												BgL_nodez00_3518))))->BgL_locz00)), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1134z00_3769)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt)
								BGl_za2procedureza2z00zztype_cachez00)), BUNSPEC);
					{
						BgL_variablez00_bglt BgL_auxz00_5590;

						{	/* Globalize/node.scm 184 */
							BgL_variablez00_bglt BgL_arg1775z00_3772;
							obj_t BgL_arg1798z00_3773;

							BgL_arg1775z00_3772 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt)
											((BgL_closurez00_bglt) BgL_nodez00_3518))))->
								BgL_variablez00);
							BgL_arg1798z00_3773 =
								(((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_closurez00_bglt)
												BgL_nodez00_3518))))->BgL_locz00);
							BgL_auxz00_5590 =
								((BgL_variablez00_bglt)
								BGl_thezd2closurezd2zzglobaliza7e_freeza7(BgL_arg1775z00_3772,
									BgL_arg1798z00_3773));
						}
						((((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_new1134z00_3769)))->
								BgL_variablez00) =
							((BgL_variablez00_bglt) BgL_auxz00_5590), BUNSPEC);
					}
					BgL_arg1773z00_3768 = BgL_new1134z00_3769;
				}
				return
					BGl_gloz12z12zzglobaliza7e_nodeza7(
					((BgL_nodez00_bglt) BgL_arg1773z00_3768),
					((BgL_variablez00_bglt) BgL_integratorz00_3519));
			}
		}

	}



/* &glo!-var1374 */
	BgL_nodez00_bglt BGl_z62gloz12zd2var1374za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3520, obj_t BgL_nodez00_3521, obj_t BgL_integratorz00_3522)
	{
		{	/* Globalize/node.scm 151 */
			{	/* Globalize/node.scm 152 */
				BgL_variablez00_bglt BgL_variablez00_3775;

				BgL_variablez00_3775 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_3521)))->BgL_variablez00);
				{	/* Globalize/node.scm 152 */
					BgL_typez00_bglt BgL_vtypez00_3776;

					BgL_vtypez00_3776 =
						(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_3775))->
						BgL_typez00);
					{	/* Globalize/node.scm 153 */

						{
							obj_t BgL_variablez00_3778;

							BgL_variablez00_3778 = ((obj_t) BgL_variablez00_3775);
						BgL_loopz00_3777:
							{	/* Globalize/node.scm 155 */
								obj_t BgL_alphaz00_3779;

								BgL_alphaz00_3779 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_variablez00_3778)))->
									BgL_fastzd2alphazd2);
								{	/* Globalize/node.scm 157 */
									bool_t BgL_test2190z00_5609;

									{	/* Globalize/node.scm 157 */
										obj_t BgL_classz00_3780;

										BgL_classz00_3780 = BGl_localz00zzast_varz00;
										if (BGL_OBJECTP(BgL_alphaz00_3779))
											{	/* Globalize/node.scm 157 */
												BgL_objectz00_bglt BgL_arg1807z00_3781;

												BgL_arg1807z00_3781 =
													(BgL_objectz00_bglt) (BgL_alphaz00_3779);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Globalize/node.scm 157 */
														long BgL_idxz00_3782;

														BgL_idxz00_3782 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3781);
														BgL_test2190z00_5609 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3782 + 2L)) == BgL_classz00_3780);
													}
												else
													{	/* Globalize/node.scm 157 */
														bool_t BgL_res2034z00_3785;

														{	/* Globalize/node.scm 157 */
															obj_t BgL_oclassz00_3786;

															{	/* Globalize/node.scm 157 */
																obj_t BgL_arg1815z00_3787;
																long BgL_arg1816z00_3788;

																BgL_arg1815z00_3787 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Globalize/node.scm 157 */
																	long BgL_arg1817z00_3789;

																	BgL_arg1817z00_3789 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3781);
																	BgL_arg1816z00_3788 =
																		(BgL_arg1817z00_3789 - OBJECT_TYPE);
																}
																BgL_oclassz00_3786 =
																	VECTOR_REF(BgL_arg1815z00_3787,
																	BgL_arg1816z00_3788);
															}
															{	/* Globalize/node.scm 157 */
																bool_t BgL__ortest_1115z00_3790;

																BgL__ortest_1115z00_3790 =
																	(BgL_classz00_3780 == BgL_oclassz00_3786);
																if (BgL__ortest_1115z00_3790)
																	{	/* Globalize/node.scm 157 */
																		BgL_res2034z00_3785 =
																			BgL__ortest_1115z00_3790;
																	}
																else
																	{	/* Globalize/node.scm 157 */
																		long BgL_odepthz00_3791;

																		{	/* Globalize/node.scm 157 */
																			obj_t BgL_arg1804z00_3792;

																			BgL_arg1804z00_3792 =
																				(BgL_oclassz00_3786);
																			BgL_odepthz00_3791 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3792);
																		}
																		if ((2L < BgL_odepthz00_3791))
																			{	/* Globalize/node.scm 157 */
																				obj_t BgL_arg1802z00_3793;

																				{	/* Globalize/node.scm 157 */
																					obj_t BgL_arg1803z00_3794;

																					BgL_arg1803z00_3794 =
																						(BgL_oclassz00_3786);
																					BgL_arg1802z00_3793 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3794, 2L);
																				}
																				BgL_res2034z00_3785 =
																					(BgL_arg1802z00_3793 ==
																					BgL_classz00_3780);
																			}
																		else
																			{	/* Globalize/node.scm 157 */
																				BgL_res2034z00_3785 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2190z00_5609 = BgL_res2034z00_3785;
													}
											}
										else
											{	/* Globalize/node.scm 157 */
												BgL_test2190z00_5609 = ((bool_t) 0);
											}
									}
									if (BgL_test2190z00_5609)
										{	/* Globalize/node.scm 157 */
											((((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_nodez00_3521)))->
													BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_alphaz00_3779)), BUNSPEC);
											{	/* Globalize/node.scm 159 */
												BgL_typez00_bglt BgL_arg1765z00_3795;

												BgL_arg1765z00_3795 =
													BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
													(BgL_vtypez00_3776);
												((((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
																		BgL_nodez00_3521))))->BgL_typez00) =
													((BgL_typez00_bglt) BgL_arg1765z00_3795), BUNSPEC);
											}
											{
												obj_t BgL_variablez00_5639;

												BgL_variablez00_5639 = BgL_alphaz00_3779;
												BgL_variablez00_3778 = BgL_variablez00_5639;
												goto BgL_loopz00_3777;
											}
										}
									else
										{	/* Globalize/node.scm 161 */
											bool_t BgL_test2195z00_5640;

											{	/* Globalize/node.scm 161 */
												obj_t BgL_classz00_3796;

												BgL_classz00_3796 = BGl_globalz00zzast_varz00;
												if (BGL_OBJECTP(BgL_variablez00_3778))
													{	/* Globalize/node.scm 161 */
														BgL_objectz00_bglt BgL_arg1807z00_3797;

														BgL_arg1807z00_3797 =
															(BgL_objectz00_bglt) (BgL_variablez00_3778);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Globalize/node.scm 161 */
																long BgL_idxz00_3798;

																BgL_idxz00_3798 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3797);
																BgL_test2195z00_5640 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3798 + 2L)) ==
																	BgL_classz00_3796);
															}
														else
															{	/* Globalize/node.scm 161 */
																bool_t BgL_res2035z00_3801;

																{	/* Globalize/node.scm 161 */
																	obj_t BgL_oclassz00_3802;

																	{	/* Globalize/node.scm 161 */
																		obj_t BgL_arg1815z00_3803;
																		long BgL_arg1816z00_3804;

																		BgL_arg1815z00_3803 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Globalize/node.scm 161 */
																			long BgL_arg1817z00_3805;

																			BgL_arg1817z00_3805 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3797);
																			BgL_arg1816z00_3804 =
																				(BgL_arg1817z00_3805 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3802 =
																			VECTOR_REF(BgL_arg1815z00_3803,
																			BgL_arg1816z00_3804);
																	}
																	{	/* Globalize/node.scm 161 */
																		bool_t BgL__ortest_1115z00_3806;

																		BgL__ortest_1115z00_3806 =
																			(BgL_classz00_3796 == BgL_oclassz00_3802);
																		if (BgL__ortest_1115z00_3806)
																			{	/* Globalize/node.scm 161 */
																				BgL_res2035z00_3801 =
																					BgL__ortest_1115z00_3806;
																			}
																		else
																			{	/* Globalize/node.scm 161 */
																				long BgL_odepthz00_3807;

																				{	/* Globalize/node.scm 161 */
																					obj_t BgL_arg1804z00_3808;

																					BgL_arg1804z00_3808 =
																						(BgL_oclassz00_3802);
																					BgL_odepthz00_3807 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3808);
																				}
																				if ((2L < BgL_odepthz00_3807))
																					{	/* Globalize/node.scm 161 */
																						obj_t BgL_arg1802z00_3809;

																						{	/* Globalize/node.scm 161 */
																							obj_t BgL_arg1803z00_3810;

																							BgL_arg1803z00_3810 =
																								(BgL_oclassz00_3802);
																							BgL_arg1802z00_3809 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3810, 2L);
																						}
																						BgL_res2035z00_3801 =
																							(BgL_arg1802z00_3809 ==
																							BgL_classz00_3796);
																					}
																				else
																					{	/* Globalize/node.scm 161 */
																						BgL_res2035z00_3801 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2195z00_5640 = BgL_res2035z00_3801;
															}
													}
												else
													{	/* Globalize/node.scm 161 */
														BgL_test2195z00_5640 = ((bool_t) 0);
													}
											}
											if (BgL_test2195z00_5640)
												{	/* Globalize/node.scm 161 */
													return
														((BgL_nodez00_bglt)
														((BgL_varz00_bglt) BgL_nodez00_3521));
												}
											else
												{	/* Globalize/node.scm 161 */
													if (BGl_celledzf3zf3zzglobaliza7e_nodeza7(
															((BgL_variablez00_bglt) BgL_variablez00_3778)))
														{	/* Globalize/node.scm 165 */
															BgL_typez00_bglt BgL_vtypez00_3811;
															BgL_typez00_bglt BgL_ntypez00_3812;

															BgL_vtypez00_3811 =
																BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
																(BgL_vtypez00_3776);
															{	/* Globalize/node.scm 166 */
																BgL_typez00_bglt BgL_arg1771z00_3813;

																BgL_arg1771z00_3813 =
																	(((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				((BgL_varz00_bglt)
																					BgL_nodez00_3521))))->BgL_typez00);
																BgL_ntypez00_3812 =
																	BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
																	(BgL_arg1771z00_3813);
															}
															{	/* Globalize/node.scm 167 */
																BgL_typez00_bglt BgL_vz00_3814;

																BgL_vz00_3814 =
																	((BgL_typez00_bglt)
																	BGl_za2cellza2z00zztype_cachez00);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_varz00_bglt)
																						BgL_nodez00_3521))))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_vz00_3814), BUNSPEC);
															}
															{	/* Globalize/node.scm 168 */
																BgL_boxzd2refzd2_bglt BgL_new1130z00_3815;

																{	/* Globalize/node.scm 169 */
																	BgL_boxzd2refzd2_bglt BgL_new1129z00_3816;

																	BgL_new1129z00_3816 =
																		((BgL_boxzd2refzd2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_boxzd2refzd2_bgl))));
																	{	/* Globalize/node.scm 169 */
																		long BgL_arg1770z00_3817;

																		BgL_arg1770z00_3817 =
																			BGL_CLASS_NUM
																			(BGl_boxzd2refzd2zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1129z00_3816),
																			BgL_arg1770z00_3817);
																	}
																	{	/* Globalize/node.scm 169 */
																		BgL_objectz00_bglt BgL_tmpz00_5681;

																		BgL_tmpz00_5681 =
																			((BgL_objectz00_bglt)
																			BgL_new1129z00_3816);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5681,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1129z00_3816);
																	BgL_new1130z00_3815 = BgL_new1129z00_3816;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1130z00_3815)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_varz00_bglt)
																							BgL_nodez00_3521))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1130z00_3815)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_ntypez00_3812),
																	BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1130z00_3815)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1130z00_3815)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																((((BgL_boxzd2refzd2_bglt)
																			COBJECT(BgL_new1130z00_3815))->
																		BgL_varz00) =
																	((BgL_varz00_bglt) ((BgL_varz00_bglt)
																			BgL_nodez00_3521)), BUNSPEC);
																((((BgL_boxzd2refzd2_bglt)
																			COBJECT(BgL_new1130z00_3815))->
																		BgL_vtypez00) =
																	((BgL_typez00_bglt) BgL_vtypez00_3811),
																	BUNSPEC);
																return ((BgL_nodez00_bglt) BgL_new1130z00_3815);
															}
														}
													else
														{	/* Globalize/node.scm 163 */
															return
																((BgL_nodez00_bglt)
																((BgL_varz00_bglt) BgL_nodez00_3521));
														}
												}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &glo!-kwote1371 */
	BgL_nodez00_bglt BGl_z62gloz12zd2kwote1371za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3523, obj_t BgL_nodez00_3524, obj_t BgL_integratorz00_3525)
	{
		{	/* Globalize/node.scm 145 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_3524));
		}

	}



/* &glo!-atom1369 */
	BgL_nodez00_bglt BGl_z62gloz12zd2atom1369za2zzglobaliza7e_nodeza7(obj_t
		BgL_envz00_3526, obj_t BgL_nodez00_3527, obj_t BgL_integratorz00_3528)
	{
		{	/* Globalize/node.scm 139 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_3527));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_nodeza7(void)
	{
		{	/* Globalize/node.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(244215788L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96
				(115338080L, BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
			return BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2073z00zzglobaliza7e_nodeza7));
		}

	}

#ifdef __cplusplus
}
#endif
