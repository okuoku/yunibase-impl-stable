/*===========================================================================*/
/*   (Globalize/globalize.scm)                                               */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/globalize.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_GLOBALIZE_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_GLOBALIZE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_globalzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		obj_t BgL_globalzd2closurezd2;
	}                        *BgL_globalzf2ginfozf2_bglt;


#endif													// BGL_GLOBALIZE_GLOBALIZE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern BgL_globalz00_bglt
		BGl_localzd2ze3globalz31zzglobaliza7e_localzd2ze3globalz96
		(BgL_localz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_globaliza7ez00 =
		BUNSPEC;
	extern BgL_globalz00_bglt
		BGl_globalzd2closurezd2zzglobaliza7e_globalzd2closurez75(BgL_globalz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_setzd2kapturedz12zc0zzglobaliza7e_kaptureza7(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzglobaliza7e_globaliza7ez00(void);
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_globaliza7ez00(void);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_globaliza7ez00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t
		BGl_setzd2globaliza7edzd2newzd2bodiesz12z67zzglobaliza7e_newzd2bodyz75
		(BgL_globalz00_bglt, obj_t);
	static bool_t BGl_verbzd2globaliza7ationz75zzglobaliza7e_globaliza7ez00(void);
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_globaliza7ez00(void);
	extern BgL_nodez00_bglt
		BGl_nodezd2globaliza7ez12z67zzglobaliza7e_nodeza7(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	extern bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	extern obj_t
		BGl_setzd2integrationz12zc0zzglobaliza7e_integrationza7(BgL_globalz00_bglt,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2verboseza2z00zzengine_paramz00;
	extern bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globaliza7ez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_checkzd2sharingzd2(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_newzd2bodyz75(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_integrationza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_gnza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_kaptureza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DEF obj_t BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00 = BUNSPEC;
	extern obj_t BGl_localz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_globaliza7ez12zb5zzglobaliza7e_globaliza7ez00(BgL_globalz00_bglt);
	static obj_t
		BGl_libraryzd2moduleszd2initz00zzglobaliza7e_globaliza7ez00(void);
	static obj_t
		BGl_importedzd2moduleszd2initz00zzglobaliza7e_globaliza7ez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_globaliza7ez00(void);
	static obj_t BGl_z62globaliza7ez12zd7zzglobaliza7e_globaliza7ez00(obj_t,
		obj_t);
	extern obj_t BGl_Gnz12z12zzglobaliza7e_gnza7(obj_t, BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2G0za2z00zzglobaliza7e_globaliza7ez00 = BUNSPEC;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2G1za2z00zzglobaliza7e_globaliza7ez00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1722z00zzglobaliza7e_globaliza7ez00,
		BgL_bgl_string1722za700za7za7g1729za7, ": ", 2);
	      DEFINE_STRING(BGl_string1723z00zzglobaliza7e_globaliza7ez00,
		BgL_bgl_string1723za700za7za7g1730za7, "        ", 8);
	      DEFINE_STRING(BGl_string1724z00zzglobaliza7e_globaliza7ez00,
		BgL_bgl_string1724za700za7za7g1731za7, " ==>", 4);
	      DEFINE_STRING(BGl_string1725z00zzglobaliza7e_globaliza7ez00,
		BgL_bgl_string1725za700za7za7g1732za7, "           ", 11);
	      DEFINE_STRING(BGl_string1726z00zzglobaliza7e_globaliza7ez00,
		BgL_bgl_string1726za700za7za7g1733za7, " --> ", 5);
	      DEFINE_STRING(BGl_string1727z00zzglobaliza7e_globaliza7ez00,
		BgL_bgl_string1727za700za7za7g1734za7, " -->", 4);
	      DEFINE_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00,
		BgL_bgl_string1728za700za7za7g1735za7, "globalize_globalize", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globaliza7ez12zd2envz67zzglobaliza7e_globaliza7ez00,
		BgL_bgl_za762globaliza7a7eza711736za7,
		BGl_z62globaliza7ez12zd7zzglobaliza7e_globaliza7ez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_globaliza7ez00));
		     ADD_ROOT((void *) (&BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00));
		     ADD_ROOT((void *) (&BGl_za2G0za2z00zzglobaliza7e_globaliza7ez00));
		     ADD_ROOT((void *) (&BGl_za2G1za2z00zzglobaliza7e_globaliza7ez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globaliza7ez00(long
		BgL_checksumz00_2183, char *BgL_fromz00_2184)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_globaliza7ez00))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_globaliza7ez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_globaliza7ez00();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_globaliza7ez00();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_globaliza7ez00();
					return BGl_toplevelzd2initzd2zzglobaliza7e_globaliza7ez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_globaliza7ez00(void)
	{
		{	/* Globalize/globalize.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "globalize_globalize");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"globalize_globalize");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_globalize");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_globalize");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_globalize");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_globalize");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_globalize");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_globaliza7ez00(void)
	{
		{	/* Globalize/globalize.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzglobaliza7e_globaliza7ez00(void)
	{
		{	/* Globalize/globalize.scm 15 */
			BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00 = BNIL;
			BGl_za2G0za2z00zzglobaliza7e_globaliza7ez00 = BNIL;
			return (BGl_za2G1za2z00zzglobaliza7e_globaliza7ez00 = BNIL, BUNSPEC);
		}

	}



/* globalize! */
	BGL_EXPORTED_DEF obj_t
		BGl_globaliza7ez12zb5zzglobaliza7e_globaliza7ez00(BgL_globalz00_bglt
		BgL_globalz00_3)
	{
		{	/* Globalize/globalize.scm 50 */
			{	/* Globalize/globalize.scm 57 */
				bool_t BgL_test1738z00_2200;

				if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
					{	/* Globalize/globalize.scm 57 */
						BgL_test1738z00_2200 =
							(3L <= (long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
					}
				else
					{	/* Globalize/globalize.scm 57 */
						BgL_test1738z00_2200 =
							BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(3L),
							BGl_za2verboseza2z00zzengine_paramz00);
					}
				if (BgL_test1738z00_2200)
					{	/* Globalize/globalize.scm 57 */
						obj_t BgL_arg1284z00_1699;

						BgL_arg1284z00_1699 =
							BGl_shapez00zztools_shapez00(((obj_t) BgL_globalz00_3));
						{	/* Globalize/globalize.scm 57 */
							obj_t BgL_list1285z00_1700;

							{	/* Globalize/globalize.scm 57 */
								obj_t BgL_arg1304z00_1701;

								{	/* Globalize/globalize.scm 57 */
									obj_t BgL_arg1305z00_1702;

									{	/* Globalize/globalize.scm 57 */
										obj_t BgL_arg1306z00_1703;

										BgL_arg1306z00_1703 =
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
										BgL_arg1305z00_1702 =
											MAKE_YOUNG_PAIR
											(BGl_string1722z00zzglobaliza7e_globaliza7ez00,
											BgL_arg1306z00_1703);
									}
									BgL_arg1304z00_1701 =
										MAKE_YOUNG_PAIR(BgL_arg1284z00_1699, BgL_arg1305z00_1702);
								}
								BgL_list1285z00_1700 =
									MAKE_YOUNG_PAIR(BGl_string1723z00zzglobaliza7e_globaliza7ez00,
									BgL_arg1304z00_1701);
							}
							BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list1285z00_1700);
					}}
				else
					{	/* Globalize/globalize.scm 57 */
						BUNSPEC;
					}
			}
			{	/* Globalize/globalize.scm 58 */
				BgL_valuez00_bglt BgL_funz00_1705;

				BgL_funz00_1705 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_globalz00_3)))->BgL_valuez00);
				BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00 = BNIL;
				BGl_za2G0za2z00zzglobaliza7e_globaliza7ez00 = BNIL;
				BGl_za2G1za2z00zzglobaliza7e_globaliza7ez00 = BNIL;
				{	/* Globalize/globalize.scm 62 */
					obj_t BgL_arg1307z00_1706;
					obj_t BgL_arg1308z00_1707;

					BgL_arg1307z00_1706 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1705)))->BgL_argsz00);
					BgL_arg1308z00_1707 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1705)))->BgL_bodyz00);
					BGl_Gnz12z12zzglobaliza7e_gnza7(BgL_arg1307z00_1706,
						((BgL_nodez00_bglt) BgL_arg1308z00_1707),
						((BgL_variablez00_bglt) BgL_globalz00_3), BNIL);
				}
				BGl_setzd2integrationz12zc0zzglobaliza7e_integrationza7(BgL_globalz00_3,
					BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00,
					BGl_za2G0za2z00zzglobaliza7e_globaliza7ez00,
					BGl_za2G1za2z00zzglobaliza7e_globaliza7ez00);
				{	/* Globalize/globalize.scm 73 */
					obj_t BgL_gz00_1708;

					{
						obj_t BgL_g1z00_1734;
						obj_t BgL_gz00_1735;

						BgL_g1z00_1734 = BGl_za2G1za2z00zzglobaliza7e_globaliza7ez00;
						BgL_gz00_1735 = BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00;
					BgL_zc3z04anonymousza31328ze3z87_1736:
						if (NULLP(BgL_g1z00_1734))
							{	/* Globalize/globalize.scm 76 */
								BgL_gz00_1708 = BgL_gz00_1735;
							}
						else
							{	/* Globalize/globalize.scm 78 */
								bool_t BgL_test1742z00_2228;

								{	/* Globalize/globalize.scm 78 */
									obj_t BgL_arg1343z00_1746;

									{	/* Globalize/globalize.scm 78 */
										BgL_sfunz00_bglt BgL_oz00_2006;

										BgL_oz00_2006 =
											((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt)
																CAR(
																	((obj_t) BgL_g1z00_1734))))))->BgL_valuez00));
										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2235;

											{
												obj_t BgL_auxz00_2236;

												{	/* Globalize/globalize.scm 78 */
													BgL_objectz00_bglt BgL_tmpz00_2237;

													BgL_tmpz00_2237 =
														((BgL_objectz00_bglt) BgL_oz00_2006);
													BgL_auxz00_2236 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2237);
												}
												BgL_auxz00_2235 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2236);
											}
											BgL_arg1343z00_1746 =
												(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2235))->
												BgL_integratorz00);
										}
									}
									{	/* Globalize/globalize.scm 78 */
										obj_t BgL_classz00_2008;

										BgL_classz00_2008 = BGl_variablez00zzast_varz00;
										if (BGL_OBJECTP(BgL_arg1343z00_1746))
											{	/* Globalize/globalize.scm 78 */
												BgL_objectz00_bglt BgL_arg1807z00_2010;

												BgL_arg1807z00_2010 =
													(BgL_objectz00_bglt) (BgL_arg1343z00_1746);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Globalize/globalize.scm 78 */
														long BgL_idxz00_2016;

														BgL_idxz00_2016 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2010);
														BgL_test1742z00_2228 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2016 + 1L)) == BgL_classz00_2008);
													}
												else
													{	/* Globalize/globalize.scm 78 */
														bool_t BgL_res1715z00_2041;

														{	/* Globalize/globalize.scm 78 */
															obj_t BgL_oclassz00_2024;

															{	/* Globalize/globalize.scm 78 */
																obj_t BgL_arg1815z00_2032;
																long BgL_arg1816z00_2033;

																BgL_arg1815z00_2032 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Globalize/globalize.scm 78 */
																	long BgL_arg1817z00_2034;

																	BgL_arg1817z00_2034 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2010);
																	BgL_arg1816z00_2033 =
																		(BgL_arg1817z00_2034 - OBJECT_TYPE);
																}
																BgL_oclassz00_2024 =
																	VECTOR_REF(BgL_arg1815z00_2032,
																	BgL_arg1816z00_2033);
															}
															{	/* Globalize/globalize.scm 78 */
																bool_t BgL__ortest_1115z00_2025;

																BgL__ortest_1115z00_2025 =
																	(BgL_classz00_2008 == BgL_oclassz00_2024);
																if (BgL__ortest_1115z00_2025)
																	{	/* Globalize/globalize.scm 78 */
																		BgL_res1715z00_2041 =
																			BgL__ortest_1115z00_2025;
																	}
																else
																	{	/* Globalize/globalize.scm 78 */
																		long BgL_odepthz00_2026;

																		{	/* Globalize/globalize.scm 78 */
																			obj_t BgL_arg1804z00_2027;

																			BgL_arg1804z00_2027 =
																				(BgL_oclassz00_2024);
																			BgL_odepthz00_2026 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2027);
																		}
																		if ((1L < BgL_odepthz00_2026))
																			{	/* Globalize/globalize.scm 78 */
																				obj_t BgL_arg1802z00_2029;

																				{	/* Globalize/globalize.scm 78 */
																					obj_t BgL_arg1803z00_2030;

																					BgL_arg1803z00_2030 =
																						(BgL_oclassz00_2024);
																					BgL_arg1802z00_2029 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2030, 1L);
																				}
																				BgL_res1715z00_2041 =
																					(BgL_arg1802z00_2029 ==
																					BgL_classz00_2008);
																			}
																		else
																			{	/* Globalize/globalize.scm 78 */
																				BgL_res1715z00_2041 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1742z00_2228 = BgL_res1715z00_2041;
													}
											}
										else
											{	/* Globalize/globalize.scm 78 */
												BgL_test1742z00_2228 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test1742z00_2228)
									{	/* Globalize/globalize.scm 79 */
										obj_t BgL_arg1335z00_1742;

										BgL_arg1335z00_1742 = CDR(((obj_t) BgL_g1z00_1734));
										{
											obj_t BgL_g1z00_2266;

											BgL_g1z00_2266 = BgL_arg1335z00_1742;
											BgL_g1z00_1734 = BgL_g1z00_2266;
											goto BgL_zc3z04anonymousza31328ze3z87_1736;
										}
									}
								else
									{	/* Globalize/globalize.scm 81 */
										obj_t BgL_arg1339z00_1743;
										obj_t BgL_arg1340z00_1744;

										BgL_arg1339z00_1743 = CDR(((obj_t) BgL_g1z00_1734));
										{	/* Globalize/globalize.scm 81 */
											obj_t BgL_arg1342z00_1745;

											BgL_arg1342z00_1745 = CAR(((obj_t) BgL_g1z00_1734));
											BgL_arg1340z00_1744 =
												MAKE_YOUNG_PAIR(BgL_arg1342z00_1745, BgL_gz00_1735);
										}
										{
											obj_t BgL_gz00_2273;
											obj_t BgL_g1z00_2272;

											BgL_g1z00_2272 = BgL_arg1339z00_1743;
											BgL_gz00_2273 = BgL_arg1340z00_1744;
											BgL_gz00_1735 = BgL_gz00_2273;
											BgL_g1z00_1734 = BgL_g1z00_2272;
											goto BgL_zc3z04anonymousza31328ze3z87_1736;
										}
									}
							}
					}
					BGl_verbzd2globaliza7ationz75zzglobaliza7e_globaliza7ez00();
					BGl_setzd2globaliza7edzd2newzd2bodiesz12z67zzglobaliza7e_newzd2bodyz75
						(BgL_globalz00_3, BgL_gz00_1708);
					BGl_setzd2kapturedz12zc0zzglobaliza7e_kaptureza7(BgL_gz00_1708);
					{	/* Globalize/globalize.scm 91 */
						obj_t BgL_g1112z00_1709;

						{	/* Globalize/globalize.scm 92 */
							bool_t BgL_test1748z00_2277;

							{
								BgL_globalzf2ginfozf2_bglt BgL_auxz00_2278;

								{
									obj_t BgL_auxz00_2279;

									{	/* Globalize/globalize.scm 92 */
										BgL_objectz00_bglt BgL_tmpz00_2280;

										BgL_tmpz00_2280 =
											((BgL_objectz00_bglt)
											((BgL_globalz00_bglt) BgL_globalz00_3));
										BgL_auxz00_2279 = BGL_OBJECT_WIDENING(BgL_tmpz00_2280);
									}
									BgL_auxz00_2278 =
										((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_2279);
								}
								BgL_test1748z00_2277 =
									(((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2278))->
									BgL_escapezf3zf3);
							}
							if (BgL_test1748z00_2277)
								{	/* Globalize/globalize.scm 93 */
									BgL_globalz00_bglt BgL_cloz00_1723;

									{	/* Globalize/globalize.scm 94 */
										obj_t BgL_arg1325z00_1730;

										BgL_arg1325z00_1730 =
											(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														(((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_funz00_1705)))->
															BgL_bodyz00))))->BgL_locz00);
										BgL_cloz00_1723 =
											BGl_globalzd2closurezd2zzglobaliza7e_globalzd2closurez75
											(BgL_globalz00_3, BgL_arg1325z00_1730);
									}
									{	/* Globalize/globalize.scm 95 */
										bool_t BgL_test1750z00_2291;

										if (BGl_globalzd2optionalzf3z21zzast_varz00(
												((obj_t) BgL_globalz00_3)))
											{	/* Globalize/globalize.scm 95 */
												BgL_test1750z00_2291 = ((bool_t) 1);
											}
										else
											{	/* Globalize/globalize.scm 95 */
												BgL_test1750z00_2291 =
													BGl_globalzd2keyzf3z21zzast_varz00(
													((obj_t) BgL_globalz00_3));
											}
										if (BgL_test1750z00_2291)
											{	/* Globalize/globalize.scm 101 */
												obj_t BgL_list1321z00_1726;

												BgL_list1321z00_1726 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_globalz00_3), BNIL);
												BgL_g1112z00_1709 = BgL_list1321z00_1726;
											}
										else
											{	/* Globalize/globalize.scm 102 */
												obj_t BgL_list1322z00_1727;

												{	/* Globalize/globalize.scm 102 */
													obj_t BgL_arg1323z00_1728;

													BgL_arg1323z00_1728 =
														MAKE_YOUNG_PAIR(((obj_t) BgL_globalz00_3), BNIL);
													BgL_list1322z00_1727 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_cloz00_1723), BgL_arg1323z00_1728);
												}
												BgL_g1112z00_1709 = BgL_list1322z00_1727;
											}
									}
								}
							else
								{	/* Globalize/globalize.scm 103 */
									obj_t BgL_list1327z00_1732;

									BgL_list1327z00_1732 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_globalz00_3), BNIL);
									BgL_g1112z00_1709 = BgL_list1327z00_1732;
								}
						}
						{
							obj_t BgL_gz00_1711;
							obj_t BgL_newzd2gzd2_1712;

							BgL_gz00_1711 = BgL_gz00_1708;
							BgL_newzd2gzd2_1712 = BgL_g1112z00_1709;
						BgL_zc3z04anonymousza31309ze3z87_1713:
							if (NULLP(BgL_gz00_1711))
								{	/* Globalize/globalize.scm 106 */
									BgL_nodez00_bglt BgL_bodyz00_1715;

									{	/* Globalize/globalize.scm 106 */
										obj_t BgL_arg1312z00_1716;

										BgL_arg1312z00_1716 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_1705)))->BgL_bodyz00);
										BgL_bodyz00_1715 =
											BGl_nodezd2globaliza7ez12z67zzglobaliza7e_nodeza7(
											((BgL_nodez00_bglt) BgL_arg1312z00_1716),
											((BgL_variablez00_bglt) BgL_globalz00_3), BNIL);
									}
									((((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_1705)))->BgL_bodyz00) =
										((obj_t) ((obj_t) BgL_bodyz00_1715)), BUNSPEC);
									return BgL_newzd2gzd2_1712;
								}
							else
								{	/* Globalize/globalize.scm 110 */
									obj_t BgL_arg1314z00_1717;
									obj_t BgL_arg1315z00_1718;

									BgL_arg1314z00_1717 = CDR(((obj_t) BgL_gz00_1711));
									{	/* Globalize/globalize.scm 110 */
										BgL_globalz00_bglt BgL_arg1316z00_1719;

										{	/* Globalize/globalize.scm 110 */
											obj_t BgL_arg1317z00_1720;

											BgL_arg1317z00_1720 = CAR(((obj_t) BgL_gz00_1711));
											BgL_arg1316z00_1719 =
												BGl_localzd2ze3globalz31zzglobaliza7e_localzd2ze3globalz96
												(((BgL_localz00_bglt) BgL_arg1317z00_1720));
										}
										BgL_arg1315z00_1718 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_arg1316z00_1719), BgL_newzd2gzd2_1712);
									}
									{
										obj_t BgL_newzd2gzd2_2324;
										obj_t BgL_gz00_2323;

										BgL_gz00_2323 = BgL_arg1314z00_1717;
										BgL_newzd2gzd2_2324 = BgL_arg1315z00_1718;
										BgL_newzd2gzd2_1712 = BgL_newzd2gzd2_2324;
										BgL_gz00_1711 = BgL_gz00_2323;
										goto BgL_zc3z04anonymousza31309ze3z87_1713;
									}
								}
						}
					}
				}
			}
		}

	}



/* &globalize! */
	obj_t BGl_z62globaliza7ez12zd7zzglobaliza7e_globaliza7ez00(obj_t
		BgL_envz00_2181, obj_t BgL_globalz00_2182)
	{
		{	/* Globalize/globalize.scm 50 */
			return
				BGl_globaliza7ez12zb5zzglobaliza7e_globaliza7ez00(
				((BgL_globalz00_bglt) BgL_globalz00_2182));
		}

	}



/* verb-globalization */
	bool_t BGl_verbzd2globaliza7ationz75zzglobaliza7e_globaliza7ez00(void)
	{
		{	/* Globalize/globalize.scm 115 */
			{
				obj_t BgL_l1252z00_1751;

				BgL_l1252z00_1751 = BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00;
			BgL_zc3z04anonymousza31349ze3z87_1752:
				if (PAIRP(BgL_l1252z00_1751))
					{	/* Globalize/globalize.scm 116 */
						{	/* Globalize/globalize.scm 117 */
							obj_t BgL_localz00_1754;

							BgL_localz00_1754 = CAR(BgL_l1252z00_1751);
							{	/* Globalize/globalize.scm 117 */
								bool_t BgL_test1754z00_2330;

								if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
									{	/* Globalize/globalize.scm 117 */
										BgL_test1754z00_2330 =
											(3L <=
											(long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
									}
								else
									{	/* Globalize/globalize.scm 117 */
										BgL_test1754z00_2330 =
											BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(3L),
											BGl_za2verboseza2z00zzengine_paramz00);
									}
								if (BgL_test1754z00_2330)
									{	/* Globalize/globalize.scm 117 */
										bool_t BgL_test1757z00_2337;

										if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
											{	/* Globalize/globalize.scm 117 */
												BgL_test1757z00_2337 =
													(3L <=
													(long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
											}
										else
											{	/* Globalize/globalize.scm 117 */
												BgL_test1757z00_2337 =
													BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(3L),
													BGl_za2verboseza2z00zzengine_paramz00);
											}
										if (BgL_test1757z00_2337)
											{	/* Globalize/globalize.scm 117 */
												obj_t BgL_arg1361z00_1761;

												BgL_arg1361z00_1761 =
													BGl_shapez00zztools_shapez00(BgL_localz00_1754);
												{	/* Globalize/globalize.scm 117 */
													obj_t BgL_list1362z00_1762;

													{	/* Globalize/globalize.scm 117 */
														obj_t BgL_arg1364z00_1763;

														{	/* Globalize/globalize.scm 117 */
															obj_t BgL_arg1367z00_1764;

															{	/* Globalize/globalize.scm 117 */
																obj_t BgL_arg1370z00_1765;

																BgL_arg1370z00_1765 =
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
																	BNIL);
																BgL_arg1367z00_1764 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1724z00zzglobaliza7e_globaliza7ez00,
																	BgL_arg1370z00_1765);
															}
															BgL_arg1364z00_1763 =
																MAKE_YOUNG_PAIR(BgL_arg1361z00_1761,
																BgL_arg1367z00_1764);
														}
														BgL_list1362z00_1762 =
															MAKE_YOUNG_PAIR
															(BGl_string1725z00zzglobaliza7e_globaliza7ez00,
															BgL_arg1364z00_1763);
													}
													BGl_verbosez00zztools_speekz00(BINT(3L),
														BgL_list1362z00_1762);
											}}
										else
											{	/* Globalize/globalize.scm 117 */
												BUNSPEC;
											}
									}
								else
									{	/* Globalize/globalize.scm 117 */
										BFALSE;
									}
							}
						}
						{
							obj_t BgL_l1252z00_2352;

							BgL_l1252z00_2352 = CDR(BgL_l1252z00_1751);
							BgL_l1252z00_1751 = BgL_l1252z00_2352;
							goto BgL_zc3z04anonymousza31349ze3z87_1752;
						}
					}
				else
					{	/* Globalize/globalize.scm 116 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1255z00_1771;

				BgL_l1255z00_1771 = BGl_za2G1za2z00zzglobaliza7e_globaliza7ez00;
			BgL_zc3z04anonymousza31372ze3z87_1772:
				if (PAIRP(BgL_l1255z00_1771))
					{	/* Globalize/globalize.scm 119 */
						{	/* Globalize/globalize.scm 120 */
							obj_t BgL_localz00_1774;

							BgL_localz00_1774 = CAR(BgL_l1255z00_1771);
							{	/* Globalize/globalize.scm 120 */
								BgL_valuez00_bglt BgL_sfunzf2ginfozf2_1775;

								BgL_sfunzf2ginfozf2_1775 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt) BgL_localz00_1774))))->
									BgL_valuez00);
								{	/* Globalize/globalize.scm 121 */
									bool_t BgL_test1760z00_2360;

									{	/* Globalize/globalize.scm 121 */
										obj_t BgL_arg1489z00_1807;

										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2361;

											{
												obj_t BgL_auxz00_2362;

												{	/* Globalize/globalize.scm 121 */
													BgL_objectz00_bglt BgL_tmpz00_2363;

													BgL_tmpz00_2363 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_sfunzf2ginfozf2_1775));
													BgL_auxz00_2362 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2363);
												}
												BgL_auxz00_2361 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2362);
											}
											BgL_arg1489z00_1807 =
												(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2361))->
												BgL_integratorz00);
										}
										{	/* Globalize/globalize.scm 121 */
											obj_t BgL_classz00_2066;

											BgL_classz00_2066 = BGl_localz00zzast_varz00;
											if (BGL_OBJECTP(BgL_arg1489z00_1807))
												{	/* Globalize/globalize.scm 121 */
													BgL_objectz00_bglt BgL_arg1807z00_2068;

													BgL_arg1807z00_2068 =
														(BgL_objectz00_bglt) (BgL_arg1489z00_1807);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Globalize/globalize.scm 121 */
															long BgL_idxz00_2074;

															BgL_idxz00_2074 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2068);
															BgL_test1760z00_2360 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2074 + 2L)) == BgL_classz00_2066);
														}
													else
														{	/* Globalize/globalize.scm 121 */
															bool_t BgL_res1719z00_2099;

															{	/* Globalize/globalize.scm 121 */
																obj_t BgL_oclassz00_2082;

																{	/* Globalize/globalize.scm 121 */
																	obj_t BgL_arg1815z00_2090;
																	long BgL_arg1816z00_2091;

																	BgL_arg1815z00_2090 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Globalize/globalize.scm 121 */
																		long BgL_arg1817z00_2092;

																		BgL_arg1817z00_2092 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2068);
																		BgL_arg1816z00_2091 =
																			(BgL_arg1817z00_2092 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2082 =
																		VECTOR_REF(BgL_arg1815z00_2090,
																		BgL_arg1816z00_2091);
																}
																{	/* Globalize/globalize.scm 121 */
																	bool_t BgL__ortest_1115z00_2083;

																	BgL__ortest_1115z00_2083 =
																		(BgL_classz00_2066 == BgL_oclassz00_2082);
																	if (BgL__ortest_1115z00_2083)
																		{	/* Globalize/globalize.scm 121 */
																			BgL_res1719z00_2099 =
																				BgL__ortest_1115z00_2083;
																		}
																	else
																		{	/* Globalize/globalize.scm 121 */
																			long BgL_odepthz00_2084;

																			{	/* Globalize/globalize.scm 121 */
																				obj_t BgL_arg1804z00_2085;

																				BgL_arg1804z00_2085 =
																					(BgL_oclassz00_2082);
																				BgL_odepthz00_2084 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2085);
																			}
																			if ((2L < BgL_odepthz00_2084))
																				{	/* Globalize/globalize.scm 121 */
																					obj_t BgL_arg1802z00_2087;

																					{	/* Globalize/globalize.scm 121 */
																						obj_t BgL_arg1803z00_2088;

																						BgL_arg1803z00_2088 =
																							(BgL_oclassz00_2082);
																						BgL_arg1802z00_2087 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2088, 2L);
																					}
																					BgL_res1719z00_2099 =
																						(BgL_arg1802z00_2087 ==
																						BgL_classz00_2066);
																				}
																			else
																				{	/* Globalize/globalize.scm 121 */
																					BgL_res1719z00_2099 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1760z00_2360 = BgL_res1719z00_2099;
														}
												}
											else
												{	/* Globalize/globalize.scm 121 */
													BgL_test1760z00_2360 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test1760z00_2360)
										{	/* Globalize/globalize.scm 122 */
											bool_t BgL_test1766z00_2391;

											if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
												{	/* Globalize/globalize.scm 122 */
													BgL_test1766z00_2391 =
														(3L <=
														(long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
												}
											else
												{	/* Globalize/globalize.scm 122 */
													BgL_test1766z00_2391 =
														BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(3L),
														BGl_za2verboseza2z00zzengine_paramz00);
												}
											if (BgL_test1766z00_2391)
												{	/* Globalize/globalize.scm 122 */
													bool_t BgL_test1768z00_2398;

													if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
														{	/* Globalize/globalize.scm 122 */
															BgL_test1768z00_2398 =
																(3L <=
																(long)
																CINT(BGl_za2verboseza2z00zzengine_paramz00));
														}
													else
														{	/* Globalize/globalize.scm 122 */
															BgL_test1768z00_2398 =
																BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(3L),
																BGl_za2verboseza2z00zzengine_paramz00);
														}
													if (BgL_test1768z00_2398)
														{	/* Globalize/globalize.scm 123 */
															obj_t BgL_arg1408z00_1784;
															obj_t BgL_arg1410z00_1785;

															BgL_arg1408z00_1784 =
																BGl_shapez00zztools_shapez00(BgL_localz00_1774);
															{	/* Globalize/globalize.scm 125 */
																obj_t BgL_arg1448z00_1791;

																{
																	BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2406;

																	{
																		obj_t BgL_auxz00_2407;

																		{	/* Globalize/globalize.scm 125 */
																			BgL_objectz00_bglt BgL_tmpz00_2408;

																			BgL_tmpz00_2408 =
																				((BgL_objectz00_bglt)
																				((BgL_sfunz00_bglt)
																					BgL_sfunzf2ginfozf2_1775));
																			BgL_auxz00_2407 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2408);
																		}
																		BgL_auxz00_2406 =
																			((BgL_sfunzf2ginfozf2_bglt)
																			BgL_auxz00_2407);
																	}
																	BgL_arg1448z00_1791 =
																		(((BgL_sfunzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_2406))->
																		BgL_integratorz00);
																}
																BgL_arg1410z00_1785 =
																	BGl_shapez00zztools_shapez00
																	(BgL_arg1448z00_1791);
															}
															{	/* Globalize/globalize.scm 122 */
																obj_t BgL_list1411z00_1786;

																{	/* Globalize/globalize.scm 122 */
																	obj_t BgL_arg1421z00_1787;

																	{	/* Globalize/globalize.scm 122 */
																		obj_t BgL_arg1422z00_1788;

																		{	/* Globalize/globalize.scm 122 */
																			obj_t BgL_arg1434z00_1789;

																			{	/* Globalize/globalize.scm 122 */
																				obj_t BgL_arg1437z00_1790;

																				BgL_arg1437z00_1790 =
																					MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																							10)), BNIL);
																				BgL_arg1434z00_1789 =
																					MAKE_YOUNG_PAIR(BgL_arg1410z00_1785,
																					BgL_arg1437z00_1790);
																			}
																			BgL_arg1422z00_1788 =
																				MAKE_YOUNG_PAIR
																				(BGl_string1726z00zzglobaliza7e_globaliza7ez00,
																				BgL_arg1434z00_1789);
																		}
																		BgL_arg1421z00_1787 =
																			MAKE_YOUNG_PAIR(BgL_arg1408z00_1784,
																			BgL_arg1422z00_1788);
																	}
																	BgL_list1411z00_1786 =
																		MAKE_YOUNG_PAIR
																		(BGl_string1725z00zzglobaliza7e_globaliza7ez00,
																		BgL_arg1421z00_1787);
																}
																BGl_verbosez00zztools_speekz00(BINT(3L),
																	BgL_list1411z00_1786);
														}}
													else
														{	/* Globalize/globalize.scm 122 */
															BUNSPEC;
														}
												}
											else
												{	/* Globalize/globalize.scm 122 */
													BFALSE;
												}
										}
									else
										{	/* Globalize/globalize.scm 127 */
											bool_t BgL_test1770z00_2423;

											if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
												{	/* Globalize/globalize.scm 127 */
													BgL_test1770z00_2423 =
														(3L <=
														(long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
												}
											else
												{	/* Globalize/globalize.scm 127 */
													BgL_test1770z00_2423 =
														BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(3L),
														BGl_za2verboseza2z00zzengine_paramz00);
												}
											if (BgL_test1770z00_2423)
												{	/* Globalize/globalize.scm 127 */
													bool_t BgL_test1772z00_2430;

													if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
														{	/* Globalize/globalize.scm 127 */
															BgL_test1772z00_2430 =
																(3L <=
																(long)
																CINT(BGl_za2verboseza2z00zzengine_paramz00));
														}
													else
														{	/* Globalize/globalize.scm 127 */
															BgL_test1772z00_2430 =
																BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(3L),
																BGl_za2verboseza2z00zzengine_paramz00);
														}
													if (BgL_test1772z00_2430)
														{	/* Globalize/globalize.scm 127 */
															obj_t BgL_arg1453z00_1800;

															BgL_arg1453z00_1800 =
																BGl_shapez00zztools_shapez00(BgL_localz00_1774);
															{	/* Globalize/globalize.scm 127 */
																obj_t BgL_list1454z00_1801;

																{	/* Globalize/globalize.scm 127 */
																	obj_t BgL_arg1472z00_1802;

																	{	/* Globalize/globalize.scm 127 */
																		obj_t BgL_arg1473z00_1803;

																		{	/* Globalize/globalize.scm 127 */
																			obj_t BgL_arg1485z00_1804;

																			BgL_arg1485z00_1804 =
																				MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																						10)), BNIL);
																			BgL_arg1473z00_1803 =
																				MAKE_YOUNG_PAIR
																				(BGl_string1727z00zzglobaliza7e_globaliza7ez00,
																				BgL_arg1485z00_1804);
																		}
																		BgL_arg1472z00_1802 =
																			MAKE_YOUNG_PAIR(BgL_arg1453z00_1800,
																			BgL_arg1473z00_1803);
																	}
																	BgL_list1454z00_1801 =
																		MAKE_YOUNG_PAIR
																		(BGl_string1725z00zzglobaliza7e_globaliza7ez00,
																		BgL_arg1472z00_1802);
																}
																BGl_verbosez00zztools_speekz00(BINT(3L),
																	BgL_list1454z00_1801);
														}}
													else
														{	/* Globalize/globalize.scm 127 */
															BUNSPEC;
														}
												}
											else
												{	/* Globalize/globalize.scm 127 */
													BFALSE;
												}
										}
								}
							}
						}
						{
							obj_t BgL_l1255z00_2445;

							BgL_l1255z00_2445 = CDR(BgL_l1255z00_1771);
							BgL_l1255z00_1771 = BgL_l1255z00_2445;
							goto BgL_zc3z04anonymousza31372ze3z87_1772;
						}
					}
				else
					{	/* Globalize/globalize.scm 119 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_globaliza7ez00(void)
	{
		{	/* Globalize/globalize.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_globaliza7ez00(void)
	{
		{	/* Globalize/globalize.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_globaliza7ez00(void)
	{
		{	/* Globalize/globalize.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_globaliza7ez00(void)
	{
		{	/* Globalize/globalize.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(2706117L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(244215788L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_kaptureza7(459831036L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_gnza7(13532668L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_integrationza7(395399079L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_newzd2bodyz75(420014038L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96
				(115338080L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75
				(62221965L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
			return BGl_modulezd2initializa7ationz75zzast_checkzd2sharingzd2(88839753L,
				BSTRING_TO_STRING(BGl_string1728z00zzglobaliza7e_globaliza7ez00));
		}

	}

#ifdef __cplusplus
}
#endif
