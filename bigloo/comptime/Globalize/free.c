/*===========================================================================*/
/*   (Globalize/free.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/free.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_FREE_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_FREE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_svarzf2ginfozf2_bgl
	{
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
		bool_t BgL_celledzf3zf3;
		bool_t BgL_stackablez00;
	}                      *BgL_svarzf2ginfozf2_bglt;

	typedef struct BgL_sexitzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
	}                       *BgL_sexitzf2ginfozf2_bglt;

	typedef struct BgL_localzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		bool_t BgL_globaliza7edzf3z54;
	}                       *BgL_localzf2ginfozf2_bglt;


#endif													// BGL_GLOBALIZE_FREE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62nodezd2freezd2conditiona1324z62zzglobaliza7e_freeza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_freeza7 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62nodezd2freezd2letzd2fun1330zb0zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t
		BGl_z62nodezd2freezd2makezd2box1338zb0zzglobaliza7e_freeza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzglobaliza7e_freeza7(void);
	static obj_t
		BGl_z62nodezd2freezd2boxzd2setz121342za2zzglobaliza7e_freeza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_nodezd2freeza2z70zzglobaliza7e_freeza7(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_za2integratorza2z00zzglobaliza7e_freeza7 = BUNSPEC;
	static obj_t BGl_z62nodezd2freezb0zzglobaliza7e_freeza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_freeza7(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_freeza7(void);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2freezd2varsz00zzglobaliza7e_freeza7(BgL_nodez00_bglt,
		BgL_localz00_bglt);
	static obj_t BGl_z62thezd2globalzd2closurez62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_nodezd2freezd2zzglobaliza7e_freeza7(BgL_nodez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_freeza7(void);
	static obj_t
		BGl_z62nodezd2freezd2jumpzd2exzd2it1336z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62freezd2fromzb0zzglobaliza7e_freeza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2freezd2sequence1308z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(BgL_localz00_bglt,
		BgL_localz00_bglt);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62nodezd2freezd2var1304z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_markzd2variablez12zc0zzglobaliza7e_freeza7(BgL_localz00_bglt);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2freezd2funcall1316z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_thezd2localzd2closurez00zzglobaliza7e_freeza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_freezd2fromzd2zzglobaliza7e_freeza7(obj_t,
		BgL_localz00_bglt);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2freezd2letzd2var1332zb0zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static long BGl_za2roundza2z00zzglobaliza7e_freeza7 = 0L;
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2freezd2closure1306z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62nodezd2freezd2setzd2exzd2it1334z62zzglobaliza7e_freeza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2freezd2boxzd2ref1340zb0zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_z62nodezd2freezd2app1312z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62nodezd2freezd2appzd2ly1314zb0zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_freezd2variablezf3z21zzglobaliza7e_freeza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_thezd2closurezd2zzglobaliza7e_freeza7(BgL_variablez00_bglt, obj_t);
	static obj_t BGl_z62nodezd2free1301zb0zzglobaliza7e_freeza7(obj_t, obj_t,
		obj_t);
	extern BgL_globalz00_bglt
		BGl_makezd2globalzd2closurez00zzglobaliza7e_globalzd2closurez75
		(BgL_globalz00_bglt);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzglobaliza7e_freeza7(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_freeza7(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	static obj_t
		BGl_internalzd2getzd2freezd2varsz12zc0zzglobaliza7e_freeza7
		(BgL_nodez00_bglt, BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_freeza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_freeza7(void);
	static obj_t BGl_z62nodezd2freezd2extern1318z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2freezd2cast1320z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2freezd2switch1328z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2freezd2setq1322z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2freezd2sync1310z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2freezd2fail1326z62zzglobaliza7e_freeza7(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62getzd2freezd2varsz62zzglobaliza7e_freeza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62thezd2closurezb0zzglobaliza7e_freeza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t __cnst[6];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2freezd2varszd2envzd2zzglobaliza7e_freeza7,
		BgL_bgl_za762getza7d2freeza7d22008za7,
		BGl_z62getzd2freezd2varsz62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1980z00zzglobaliza7e_freeza7,
		BgL_bgl_string1980za700za7za7g2009za7, "Unknown variable type (~a)", 26);
	      DEFINE_STRING(BGl_string1981z00zzglobaliza7e_freeza7,
		BgL_bgl_string1981za700za7za7g2010za7, "free-variable?", 14);
	      DEFINE_STRING(BGl_string1983z00zzglobaliza7e_freeza7,
		BgL_bgl_string1983za700za7za7g2011za7, "node-free1301", 13);
	      DEFINE_STRING(BGl_string1985z00zzglobaliza7e_freeza7,
		BgL_bgl_string1985za700za7za7g2012za7, "node-free", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1982z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2free132013z00,
		BGl_z62nodezd2free1301zb0zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1984z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2014za7,
		BGl_z62nodezd2freezd2var1304z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1986z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2015za7,
		BGl_z62nodezd2freezd2closure1306z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1987z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2016za7,
		BGl_z62nodezd2freezd2sequence1308z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1988z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2017za7,
		BGl_z62nodezd2freezd2sync1310z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1989z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2018za7,
		BGl_z62nodezd2freezd2app1312z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1990z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2019za7,
		BGl_z62nodezd2freezd2appzd2ly1314zb0zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1991z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2020za7,
		BGl_z62nodezd2freezd2funcall1316z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1992z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2021za7,
		BGl_z62nodezd2freezd2extern1318z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1993z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2022za7,
		BGl_z62nodezd2freezd2cast1320z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1994z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2023za7,
		BGl_z62nodezd2freezd2setq1322z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1995z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2024za7,
		BGl_z62nodezd2freezd2conditiona1324z62zzglobaliza7e_freeza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1996z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2025za7,
		BGl_z62nodezd2freezd2fail1326z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1997z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2026za7,
		BGl_z62nodezd2freezd2switch1328z62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1998z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2027za7,
		BGl_z62nodezd2freezd2letzd2fun1330zb0zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1999z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2028za7,
		BGl_z62nodezd2freezd2letzd2var1332zb0zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_thezd2globalzd2closurezd2envzd2zzglobaliza7e_freeza7,
		BgL_bgl_za762theza7d2globalza72029za7,
		BGl_z62thezd2globalzd2closurez62zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_freezd2fromzd2envz00zzglobaliza7e_freeza7,
		BgL_bgl_za762freeza7d2fromza7b2030za7,
		BGl_z62freezd2fromzb0zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2005z00zzglobaliza7e_freeza7,
		BgL_bgl_string2005za700za7za7g2031za7, "globalize_free", 14);
	      DEFINE_STRING(BGl_string2006z00zzglobaliza7e_freeza7,
		BgL_bgl_string2006za700za7za7g2032za7,
		"sfun sgfun -env::procedure value make-fx-procedure make-va-procedure ",
		69);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7b2033za7,
		BGl_z62nodezd2freezb0zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2000z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2034za7,
		BGl_z62nodezd2freezd2setzd2exzd2it1334z62zzglobaliza7e_freeza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2001z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2035za7,
		BGl_z62nodezd2freezd2jumpzd2exzd2it1336z62zzglobaliza7e_freeza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2002z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2036za7,
		BGl_z62nodezd2freezd2makezd2box1338zb0zzglobaliza7e_freeza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2003z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2037za7,
		BGl_z62nodezd2freezd2boxzd2ref1340zb0zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2004z00zzglobaliza7e_freeza7,
		BgL_bgl_za762nodeza7d2freeza7d2038za7,
		BGl_z62nodezd2freezd2boxzd2setz121342za2zzglobaliza7e_freeza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_thezd2closurezd2envz00zzglobaliza7e_freeza7,
		BgL_bgl_za762theza7d2closure2039z00,
		BGl_z62thezd2closurezb0zzglobaliza7e_freeza7, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_freeza7));
		     ADD_ROOT((void *) (&BGl_za2integratorza2z00zzglobaliza7e_freeza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(long
		BgL_checksumz00_3647, char *BgL_fromz00_3648)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_freeza7))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_freeza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_freeza7();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_freeza7();
					BGl_cnstzd2initzd2zzglobaliza7e_freeza7();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_freeza7();
					BGl_genericzd2initzd2zzglobaliza7e_freeza7();
					BGl_methodzd2initzd2zzglobaliza7e_freeza7();
					return BGl_toplevelzd2initzd2zzglobaliza7e_freeza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_freeza7(void)
	{
		{	/* Globalize/free.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "globalize_free");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"globalize_free");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"globalize_free");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_free");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"globalize_free");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "globalize_free");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "globalize_free");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"globalize_free");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_free");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_free");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_free");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_free");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzglobaliza7e_freeza7(void)
	{
		{	/* Globalize/free.scm 15 */
			{	/* Globalize/free.scm 15 */
				obj_t BgL_cportz00_3486;

				{	/* Globalize/free.scm 15 */
					obj_t BgL_stringz00_3493;

					BgL_stringz00_3493 = BGl_string2006z00zzglobaliza7e_freeza7;
					{	/* Globalize/free.scm 15 */
						obj_t BgL_startz00_3494;

						BgL_startz00_3494 = BINT(0L);
						{	/* Globalize/free.scm 15 */
							obj_t BgL_endz00_3495;

							BgL_endz00_3495 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3493)));
							{	/* Globalize/free.scm 15 */

								BgL_cportz00_3486 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3493, BgL_startz00_3494, BgL_endz00_3495);
				}}}}
				{
					long BgL_iz00_3487;

					BgL_iz00_3487 = 5L;
				BgL_loopz00_3488:
					if ((BgL_iz00_3487 == -1L))
						{	/* Globalize/free.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Globalize/free.scm 15 */
							{	/* Globalize/free.scm 15 */
								obj_t BgL_arg2007z00_3489;

								{	/* Globalize/free.scm 15 */

									{	/* Globalize/free.scm 15 */
										obj_t BgL_locationz00_3491;

										BgL_locationz00_3491 = BBOOL(((bool_t) 0));
										{	/* Globalize/free.scm 15 */

											BgL_arg2007z00_3489 =
												BGl_readz00zz__readerz00(BgL_cportz00_3486,
												BgL_locationz00_3491);
										}
									}
								}
								{	/* Globalize/free.scm 15 */
									int BgL_tmpz00_3680;

									BgL_tmpz00_3680 = (int) (BgL_iz00_3487);
									CNST_TABLE_SET(BgL_tmpz00_3680, BgL_arg2007z00_3489);
							}}
							{	/* Globalize/free.scm 15 */
								int BgL_auxz00_3492;

								BgL_auxz00_3492 = (int) ((BgL_iz00_3487 - 1L));
								{
									long BgL_iz00_3685;

									BgL_iz00_3685 = (long) (BgL_auxz00_3492);
									BgL_iz00_3487 = BgL_iz00_3685;
									goto BgL_loopz00_3488;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_freeza7(void)
	{
		{	/* Globalize/free.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzglobaliza7e_freeza7(void)
	{
		{	/* Globalize/free.scm 15 */
			BGl_za2roundza2z00zzglobaliza7e_freeza7 = 0L;
			BGl_za2integratorza2z00zzglobaliza7e_freeza7 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* mark-variable! */
	obj_t BGl_markzd2variablez12zc0zzglobaliza7e_freeza7(BgL_localz00_bglt
		BgL_localz00_3)
	{
		{	/* Globalize/free.scm 44 */
			{	/* Globalize/free.scm 45 */
				BgL_valuez00_bglt BgL_infoz00_1755;

				BgL_infoz00_1755 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_localz00_3)))->BgL_valuez00);
				{	/* Globalize/free.scm 47 */
					bool_t BgL_test2042z00_3690;

					{	/* Globalize/free.scm 47 */
						obj_t BgL_classz00_2354;

						BgL_classz00_2354 = BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
						{	/* Globalize/free.scm 47 */
							BgL_objectz00_bglt BgL_arg1807z00_2356;

							{	/* Globalize/free.scm 47 */
								obj_t BgL_tmpz00_3691;

								BgL_tmpz00_3691 =
									((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1755));
								BgL_arg1807z00_2356 = (BgL_objectz00_bglt) (BgL_tmpz00_3691);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Globalize/free.scm 47 */
									long BgL_idxz00_2362;

									BgL_idxz00_2362 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2356);
									BgL_test2042z00_3690 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2362 + 3L)) == BgL_classz00_2354);
								}
							else
								{	/* Globalize/free.scm 47 */
									bool_t BgL_res1951z00_2387;

									{	/* Globalize/free.scm 47 */
										obj_t BgL_oclassz00_2370;

										{	/* Globalize/free.scm 47 */
											obj_t BgL_arg1815z00_2378;
											long BgL_arg1816z00_2379;

											BgL_arg1815z00_2378 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Globalize/free.scm 47 */
												long BgL_arg1817z00_2380;

												BgL_arg1817z00_2380 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2356);
												BgL_arg1816z00_2379 =
													(BgL_arg1817z00_2380 - OBJECT_TYPE);
											}
											BgL_oclassz00_2370 =
												VECTOR_REF(BgL_arg1815z00_2378, BgL_arg1816z00_2379);
										}
										{	/* Globalize/free.scm 47 */
											bool_t BgL__ortest_1115z00_2371;

											BgL__ortest_1115z00_2371 =
												(BgL_classz00_2354 == BgL_oclassz00_2370);
											if (BgL__ortest_1115z00_2371)
												{	/* Globalize/free.scm 47 */
													BgL_res1951z00_2387 = BgL__ortest_1115z00_2371;
												}
											else
												{	/* Globalize/free.scm 47 */
													long BgL_odepthz00_2372;

													{	/* Globalize/free.scm 47 */
														obj_t BgL_arg1804z00_2373;

														BgL_arg1804z00_2373 = (BgL_oclassz00_2370);
														BgL_odepthz00_2372 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2373);
													}
													if ((3L < BgL_odepthz00_2372))
														{	/* Globalize/free.scm 47 */
															obj_t BgL_arg1802z00_2375;

															{	/* Globalize/free.scm 47 */
																obj_t BgL_arg1803z00_2376;

																BgL_arg1803z00_2376 = (BgL_oclassz00_2370);
																BgL_arg1802z00_2375 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2376,
																	3L);
															}
															BgL_res1951z00_2387 =
																(BgL_arg1802z00_2375 == BgL_classz00_2354);
														}
													else
														{	/* Globalize/free.scm 47 */
															BgL_res1951z00_2387 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2042z00_3690 = BgL_res1951z00_2387;
								}
						}
					}
					if (BgL_test2042z00_3690)
						{	/* Globalize/free.scm 48 */
							long BgL_vz00_2389;

							BgL_vz00_2389 = BGl_za2roundza2z00zzglobaliza7e_freeza7;
							{
								BgL_svarzf2ginfozf2_bglt BgL_auxz00_3714;

								{
									obj_t BgL_auxz00_3715;

									{	/* Globalize/free.scm 48 */
										BgL_objectz00_bglt BgL_tmpz00_3716;

										BgL_tmpz00_3716 =
											((BgL_objectz00_bglt)
											((BgL_svarz00_bglt) BgL_infoz00_1755));
										BgL_auxz00_3715 = BGL_OBJECT_WIDENING(BgL_tmpz00_3716);
									}
									BgL_auxz00_3714 =
										((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3715);
								}
								return
									((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3714))->
										BgL_freezd2markzd2) = ((long) BgL_vz00_2389), BUNSPEC);
						}}
					else
						{	/* Globalize/free.scm 49 */
							bool_t BgL_test2046z00_3722;

							{	/* Globalize/free.scm 49 */
								obj_t BgL_classz00_2391;

								BgL_classz00_2391 = BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7;
								{	/* Globalize/free.scm 49 */
									BgL_objectz00_bglt BgL_arg1807z00_2393;

									{	/* Globalize/free.scm 49 */
										obj_t BgL_tmpz00_3723;

										BgL_tmpz00_3723 =
											((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1755));
										BgL_arg1807z00_2393 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3723);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Globalize/free.scm 49 */
											long BgL_idxz00_2399;

											BgL_idxz00_2399 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2393);
											BgL_test2046z00_3722 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2399 + 4L)) == BgL_classz00_2391);
										}
									else
										{	/* Globalize/free.scm 49 */
											bool_t BgL_res1952z00_2424;

											{	/* Globalize/free.scm 49 */
												obj_t BgL_oclassz00_2407;

												{	/* Globalize/free.scm 49 */
													obj_t BgL_arg1815z00_2415;
													long BgL_arg1816z00_2416;

													BgL_arg1815z00_2415 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Globalize/free.scm 49 */
														long BgL_arg1817z00_2417;

														BgL_arg1817z00_2417 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2393);
														BgL_arg1816z00_2416 =
															(BgL_arg1817z00_2417 - OBJECT_TYPE);
													}
													BgL_oclassz00_2407 =
														VECTOR_REF(BgL_arg1815z00_2415,
														BgL_arg1816z00_2416);
												}
												{	/* Globalize/free.scm 49 */
													bool_t BgL__ortest_1115z00_2408;

													BgL__ortest_1115z00_2408 =
														(BgL_classz00_2391 == BgL_oclassz00_2407);
													if (BgL__ortest_1115z00_2408)
														{	/* Globalize/free.scm 49 */
															BgL_res1952z00_2424 = BgL__ortest_1115z00_2408;
														}
													else
														{	/* Globalize/free.scm 49 */
															long BgL_odepthz00_2409;

															{	/* Globalize/free.scm 49 */
																obj_t BgL_arg1804z00_2410;

																BgL_arg1804z00_2410 = (BgL_oclassz00_2407);
																BgL_odepthz00_2409 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2410);
															}
															if ((4L < BgL_odepthz00_2409))
																{	/* Globalize/free.scm 49 */
																	obj_t BgL_arg1802z00_2412;

																	{	/* Globalize/free.scm 49 */
																		obj_t BgL_arg1803z00_2413;

																		BgL_arg1803z00_2413 = (BgL_oclassz00_2407);
																		BgL_arg1802z00_2412 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2413, 4L);
																	}
																	BgL_res1952z00_2424 =
																		(BgL_arg1802z00_2412 == BgL_classz00_2391);
																}
															else
																{	/* Globalize/free.scm 49 */
																	BgL_res1952z00_2424 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2046z00_3722 = BgL_res1952z00_2424;
										}
								}
							}
							if (BgL_test2046z00_3722)
								{	/* Globalize/free.scm 50 */
									obj_t BgL_vz00_2426;

									BgL_vz00_2426 = BINT(BGl_za2roundza2z00zzglobaliza7e_freeza7);
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3747;

										{
											obj_t BgL_auxz00_3748;

											{	/* Globalize/free.scm 50 */
												BgL_objectz00_bglt BgL_tmpz00_3749;

												BgL_tmpz00_3749 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_infoz00_1755));
												BgL_auxz00_3748 = BGL_OBJECT_WIDENING(BgL_tmpz00_3749);
											}
											BgL_auxz00_3747 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3748);
										}
										return
											((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3747))->
												BgL_freezd2markzd2) = ((obj_t) BgL_vz00_2426), BUNSPEC);
									}
								}
							else
								{	/* Globalize/free.scm 51 */
									bool_t BgL_test2050z00_3755;

									{	/* Globalize/free.scm 51 */
										obj_t BgL_classz00_2428;

										BgL_classz00_2428 =
											BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7;
										{	/* Globalize/free.scm 51 */
											BgL_objectz00_bglt BgL_arg1807z00_2430;

											{	/* Globalize/free.scm 51 */
												obj_t BgL_tmpz00_3756;

												BgL_tmpz00_3756 =
													((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1755));
												BgL_arg1807z00_2430 =
													(BgL_objectz00_bglt) (BgL_tmpz00_3756);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Globalize/free.scm 51 */
													long BgL_idxz00_2436;

													BgL_idxz00_2436 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2430);
													BgL_test2050z00_3755 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2436 + 3L)) == BgL_classz00_2428);
												}
											else
												{	/* Globalize/free.scm 51 */
													bool_t BgL_res1953z00_2461;

													{	/* Globalize/free.scm 51 */
														obj_t BgL_oclassz00_2444;

														{	/* Globalize/free.scm 51 */
															obj_t BgL_arg1815z00_2452;
															long BgL_arg1816z00_2453;

															BgL_arg1815z00_2452 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Globalize/free.scm 51 */
																long BgL_arg1817z00_2454;

																BgL_arg1817z00_2454 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2430);
																BgL_arg1816z00_2453 =
																	(BgL_arg1817z00_2454 - OBJECT_TYPE);
															}
															BgL_oclassz00_2444 =
																VECTOR_REF(BgL_arg1815z00_2452,
																BgL_arg1816z00_2453);
														}
														{	/* Globalize/free.scm 51 */
															bool_t BgL__ortest_1115z00_2445;

															BgL__ortest_1115z00_2445 =
																(BgL_classz00_2428 == BgL_oclassz00_2444);
															if (BgL__ortest_1115z00_2445)
																{	/* Globalize/free.scm 51 */
																	BgL_res1953z00_2461 =
																		BgL__ortest_1115z00_2445;
																}
															else
																{	/* Globalize/free.scm 51 */
																	long BgL_odepthz00_2446;

																	{	/* Globalize/free.scm 51 */
																		obj_t BgL_arg1804z00_2447;

																		BgL_arg1804z00_2447 = (BgL_oclassz00_2444);
																		BgL_odepthz00_2446 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2447);
																	}
																	if ((3L < BgL_odepthz00_2446))
																		{	/* Globalize/free.scm 51 */
																			obj_t BgL_arg1802z00_2449;

																			{	/* Globalize/free.scm 51 */
																				obj_t BgL_arg1803z00_2450;

																				BgL_arg1803z00_2450 =
																					(BgL_oclassz00_2444);
																				BgL_arg1802z00_2449 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2450, 3L);
																			}
																			BgL_res1953z00_2461 =
																				(BgL_arg1802z00_2449 ==
																				BgL_classz00_2428);
																		}
																	else
																		{	/* Globalize/free.scm 51 */
																			BgL_res1953z00_2461 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2050z00_3755 = BgL_res1953z00_2461;
												}
										}
									}
									if (BgL_test2050z00_3755)
										{	/* Globalize/free.scm 52 */
											long BgL_vz00_2463;

											BgL_vz00_2463 = BGl_za2roundza2z00zzglobaliza7e_freeza7;
											{
												BgL_sexitzf2ginfozf2_bglt BgL_auxz00_3779;

												{
													obj_t BgL_auxz00_3780;

													{	/* Globalize/free.scm 52 */
														BgL_objectz00_bglt BgL_tmpz00_3781;

														BgL_tmpz00_3781 =
															((BgL_objectz00_bglt)
															((BgL_sexitz00_bglt) BgL_infoz00_1755));
														BgL_auxz00_3780 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3781);
													}
													BgL_auxz00_3779 =
														((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_3780);
												}
												return
													((((BgL_sexitzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_3779))->BgL_freezd2markzd2) =
													((long) BgL_vz00_2463), BUNSPEC);
										}}
									else
										{	/* Globalize/free.scm 51 */
											return BFALSE;
										}
								}
						}
				}
			}
		}

	}



/* bind-variable! */
	obj_t BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(BgL_localz00_bglt
		BgL_localz00_4, BgL_localz00_bglt BgL_integratorz00_5)
	{
		{	/* Globalize/free.scm 57 */
			{	/* Globalize/free.scm 58 */
				BgL_valuez00_bglt BgL_finfoz00_1759;

				BgL_finfoz00_1759 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_integratorz00_5)))->BgL_valuez00);
				{	/* Globalize/free.scm 59 */
					obj_t BgL_arg1361z00_1760;

					{	/* Globalize/free.scm 59 */
						obj_t BgL_arg1364z00_1761;

						{
							BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3789;

							{
								obj_t BgL_auxz00_3790;

								{	/* Globalize/free.scm 59 */
									BgL_objectz00_bglt BgL_tmpz00_3791;

									BgL_tmpz00_3791 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_finfoz00_1759));
									BgL_auxz00_3790 = BGL_OBJECT_WIDENING(BgL_tmpz00_3791);
								}
								BgL_auxz00_3789 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3790);
							}
							BgL_arg1364z00_1761 =
								(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3789))->
								BgL_boundz00);
						}
						BgL_arg1361z00_1760 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_localz00_4), BgL_arg1364z00_1761);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3799;

						{
							obj_t BgL_auxz00_3800;

							{	/* Globalize/free.scm 59 */
								BgL_objectz00_bglt BgL_tmpz00_3801;

								BgL_tmpz00_3801 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_finfoz00_1759));
								BgL_auxz00_3800 = BGL_OBJECT_WIDENING(BgL_tmpz00_3801);
							}
							BgL_auxz00_3799 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3800);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3799))->
								BgL_boundz00) = ((obj_t) BgL_arg1361z00_1760), BUNSPEC);
					}
				}
				return BGl_markzd2variablez12zc0zzglobaliza7e_freeza7(BgL_localz00_4);
			}
		}

	}



/* free-variable? */
	obj_t BGl_freezd2variablezf3z21zzglobaliza7e_freeza7(obj_t BgL_localz00_6)
	{
		{	/* Globalize/free.scm 65 */
			{	/* Globalize/free.scm 66 */
				bool_t BgL_test2054z00_3808;

				{	/* Globalize/free.scm 66 */
					obj_t BgL_classz00_2470;

					BgL_classz00_2470 = BGl_localz00zzast_varz00;
					if (BGL_OBJECTP(BgL_localz00_6))
						{	/* Globalize/free.scm 66 */
							BgL_objectz00_bglt BgL_arg1807z00_2472;

							BgL_arg1807z00_2472 = (BgL_objectz00_bglt) (BgL_localz00_6);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Globalize/free.scm 66 */
									long BgL_idxz00_2478;

									BgL_idxz00_2478 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2472);
									BgL_test2054z00_3808 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2478 + 2L)) == BgL_classz00_2470);
								}
							else
								{	/* Globalize/free.scm 66 */
									bool_t BgL_res1954z00_2503;

									{	/* Globalize/free.scm 66 */
										obj_t BgL_oclassz00_2486;

										{	/* Globalize/free.scm 66 */
											obj_t BgL_arg1815z00_2494;
											long BgL_arg1816z00_2495;

											BgL_arg1815z00_2494 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Globalize/free.scm 66 */
												long BgL_arg1817z00_2496;

												BgL_arg1817z00_2496 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2472);
												BgL_arg1816z00_2495 =
													(BgL_arg1817z00_2496 - OBJECT_TYPE);
											}
											BgL_oclassz00_2486 =
												VECTOR_REF(BgL_arg1815z00_2494, BgL_arg1816z00_2495);
										}
										{	/* Globalize/free.scm 66 */
											bool_t BgL__ortest_1115z00_2487;

											BgL__ortest_1115z00_2487 =
												(BgL_classz00_2470 == BgL_oclassz00_2486);
											if (BgL__ortest_1115z00_2487)
												{	/* Globalize/free.scm 66 */
													BgL_res1954z00_2503 = BgL__ortest_1115z00_2487;
												}
											else
												{	/* Globalize/free.scm 66 */
													long BgL_odepthz00_2488;

													{	/* Globalize/free.scm 66 */
														obj_t BgL_arg1804z00_2489;

														BgL_arg1804z00_2489 = (BgL_oclassz00_2486);
														BgL_odepthz00_2488 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2489);
													}
													if ((2L < BgL_odepthz00_2488))
														{	/* Globalize/free.scm 66 */
															obj_t BgL_arg1802z00_2491;

															{	/* Globalize/free.scm 66 */
																obj_t BgL_arg1803z00_2492;

																BgL_arg1803z00_2492 = (BgL_oclassz00_2486);
																BgL_arg1802z00_2491 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2492,
																	2L);
															}
															BgL_res1954z00_2503 =
																(BgL_arg1802z00_2491 == BgL_classz00_2470);
														}
													else
														{	/* Globalize/free.scm 66 */
															BgL_res1954z00_2503 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2054z00_3808 = BgL_res1954z00_2503;
								}
						}
					else
						{	/* Globalize/free.scm 66 */
							BgL_test2054z00_3808 = ((bool_t) 0);
						}
				}
				if (BgL_test2054z00_3808)
					{	/* Globalize/free.scm 67 */
						BgL_valuez00_bglt BgL_infoz00_1763;

						BgL_infoz00_1763 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_localz00_6))))->BgL_valuez00);
						{	/* Globalize/free.scm 69 */
							bool_t BgL_test2059z00_3834;

							{	/* Globalize/free.scm 69 */
								obj_t BgL_classz00_2505;

								BgL_classz00_2505 = BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
								{	/* Globalize/free.scm 69 */
									BgL_objectz00_bglt BgL_arg1807z00_2507;

									{	/* Globalize/free.scm 69 */
										obj_t BgL_tmpz00_3835;

										BgL_tmpz00_3835 =
											((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1763));
										BgL_arg1807z00_2507 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3835);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Globalize/free.scm 69 */
											long BgL_idxz00_2513;

											BgL_idxz00_2513 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2507);
											BgL_test2059z00_3834 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2513 + 3L)) == BgL_classz00_2505);
										}
									else
										{	/* Globalize/free.scm 69 */
											bool_t BgL_res1955z00_2538;

											{	/* Globalize/free.scm 69 */
												obj_t BgL_oclassz00_2521;

												{	/* Globalize/free.scm 69 */
													obj_t BgL_arg1815z00_2529;
													long BgL_arg1816z00_2530;

													BgL_arg1815z00_2529 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Globalize/free.scm 69 */
														long BgL_arg1817z00_2531;

														BgL_arg1817z00_2531 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2507);
														BgL_arg1816z00_2530 =
															(BgL_arg1817z00_2531 - OBJECT_TYPE);
													}
													BgL_oclassz00_2521 =
														VECTOR_REF(BgL_arg1815z00_2529,
														BgL_arg1816z00_2530);
												}
												{	/* Globalize/free.scm 69 */
													bool_t BgL__ortest_1115z00_2522;

													BgL__ortest_1115z00_2522 =
														(BgL_classz00_2505 == BgL_oclassz00_2521);
													if (BgL__ortest_1115z00_2522)
														{	/* Globalize/free.scm 69 */
															BgL_res1955z00_2538 = BgL__ortest_1115z00_2522;
														}
													else
														{	/* Globalize/free.scm 69 */
															long BgL_odepthz00_2523;

															{	/* Globalize/free.scm 69 */
																obj_t BgL_arg1804z00_2524;

																BgL_arg1804z00_2524 = (BgL_oclassz00_2521);
																BgL_odepthz00_2523 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2524);
															}
															if ((3L < BgL_odepthz00_2523))
																{	/* Globalize/free.scm 69 */
																	obj_t BgL_arg1802z00_2526;

																	{	/* Globalize/free.scm 69 */
																		obj_t BgL_arg1803z00_2527;

																		BgL_arg1803z00_2527 = (BgL_oclassz00_2521);
																		BgL_arg1802z00_2526 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2527, 3L);
																	}
																	BgL_res1955z00_2538 =
																		(BgL_arg1802z00_2526 == BgL_classz00_2505);
																}
															else
																{	/* Globalize/free.scm 69 */
																	BgL_res1955z00_2538 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2059z00_3834 = BgL_res1955z00_2538;
										}
								}
							}
							if (BgL_test2059z00_3834)
								{	/* Globalize/free.scm 70 */
									bool_t BgL_test2063z00_3858;

									{	/* Globalize/free.scm 70 */
										long BgL_arg1370z00_1767;

										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3859;

											{
												obj_t BgL_auxz00_3860;

												{	/* Globalize/free.scm 70 */
													BgL_objectz00_bglt BgL_tmpz00_3861;

													BgL_tmpz00_3861 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_infoz00_1763));
													BgL_auxz00_3860 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3861);
												}
												BgL_auxz00_3859 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3860);
											}
											BgL_arg1370z00_1767 =
												(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3859))->
												BgL_freezd2markzd2);
										}
										BgL_test2063z00_3858 =
											(BgL_arg1370z00_1767 ==
											BGl_za2roundza2z00zzglobaliza7e_freeza7);
									}
									if (BgL_test2063z00_3858)
										{	/* Globalize/free.scm 70 */
											return BFALSE;
										}
									else
										{	/* Globalize/free.scm 70 */
											return BTRUE;
										}
								}
							else
								{	/* Globalize/free.scm 71 */
									bool_t BgL_test2064z00_3868;

									{	/* Globalize/free.scm 71 */
										obj_t BgL_classz00_2541;

										BgL_classz00_2541 =
											BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7;
										{	/* Globalize/free.scm 71 */
											BgL_objectz00_bglt BgL_arg1807z00_2543;

											{	/* Globalize/free.scm 71 */
												obj_t BgL_tmpz00_3869;

												BgL_tmpz00_3869 =
													((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1763));
												BgL_arg1807z00_2543 =
													(BgL_objectz00_bglt) (BgL_tmpz00_3869);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Globalize/free.scm 71 */
													long BgL_idxz00_2549;

													BgL_idxz00_2549 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2543);
													BgL_test2064z00_3868 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2549 + 4L)) == BgL_classz00_2541);
												}
											else
												{	/* Globalize/free.scm 71 */
													bool_t BgL_res1956z00_2574;

													{	/* Globalize/free.scm 71 */
														obj_t BgL_oclassz00_2557;

														{	/* Globalize/free.scm 71 */
															obj_t BgL_arg1815z00_2565;
															long BgL_arg1816z00_2566;

															BgL_arg1815z00_2565 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Globalize/free.scm 71 */
																long BgL_arg1817z00_2567;

																BgL_arg1817z00_2567 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2543);
																BgL_arg1816z00_2566 =
																	(BgL_arg1817z00_2567 - OBJECT_TYPE);
															}
															BgL_oclassz00_2557 =
																VECTOR_REF(BgL_arg1815z00_2565,
																BgL_arg1816z00_2566);
														}
														{	/* Globalize/free.scm 71 */
															bool_t BgL__ortest_1115z00_2558;

															BgL__ortest_1115z00_2558 =
																(BgL_classz00_2541 == BgL_oclassz00_2557);
															if (BgL__ortest_1115z00_2558)
																{	/* Globalize/free.scm 71 */
																	BgL_res1956z00_2574 =
																		BgL__ortest_1115z00_2558;
																}
															else
																{	/* Globalize/free.scm 71 */
																	long BgL_odepthz00_2559;

																	{	/* Globalize/free.scm 71 */
																		obj_t BgL_arg1804z00_2560;

																		BgL_arg1804z00_2560 = (BgL_oclassz00_2557);
																		BgL_odepthz00_2559 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2560);
																	}
																	if ((4L < BgL_odepthz00_2559))
																		{	/* Globalize/free.scm 71 */
																			obj_t BgL_arg1802z00_2562;

																			{	/* Globalize/free.scm 71 */
																				obj_t BgL_arg1803z00_2563;

																				BgL_arg1803z00_2563 =
																					(BgL_oclassz00_2557);
																				BgL_arg1802z00_2562 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2563, 4L);
																			}
																			BgL_res1956z00_2574 =
																				(BgL_arg1802z00_2562 ==
																				BgL_classz00_2541);
																		}
																	else
																		{	/* Globalize/free.scm 71 */
																			BgL_res1956z00_2574 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2064z00_3868 = BgL_res1956z00_2574;
												}
										}
									}
									if (BgL_test2064z00_3868)
										{	/* Globalize/free.scm 72 */
											bool_t BgL_test2068z00_3892;

											{	/* Globalize/free.scm 72 */
												obj_t BgL_arg1408z00_1771;

												{
													BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3893;

													{
														obj_t BgL_auxz00_3894;

														{	/* Globalize/free.scm 72 */
															BgL_objectz00_bglt BgL_tmpz00_3895;

															BgL_tmpz00_3895 =
																((BgL_objectz00_bglt)
																((BgL_sfunz00_bglt) BgL_infoz00_1763));
															BgL_auxz00_3894 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3895);
														}
														BgL_auxz00_3893 =
															((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3894);
													}
													BgL_arg1408z00_1771 =
														(((BgL_sfunzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_3893))->BgL_freezd2markzd2);
												}
												BgL_test2068z00_3892 =
													(BgL_arg1408z00_1771 ==
													BINT(BGl_za2roundza2z00zzglobaliza7e_freeza7));
											}
											if (BgL_test2068z00_3892)
												{	/* Globalize/free.scm 72 */
													return BFALSE;
												}
											else
												{	/* Globalize/free.scm 72 */
													return BTRUE;
												}
										}
									else
										{	/* Globalize/free.scm 73 */
											bool_t BgL_test2069z00_3903;

											{	/* Globalize/free.scm 73 */
												obj_t BgL_classz00_2577;

												BgL_classz00_2577 =
													BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7;
												{	/* Globalize/free.scm 73 */
													BgL_objectz00_bglt BgL_arg1807z00_2579;

													{	/* Globalize/free.scm 73 */
														obj_t BgL_tmpz00_3904;

														BgL_tmpz00_3904 =
															((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1763));
														BgL_arg1807z00_2579 =
															(BgL_objectz00_bglt) (BgL_tmpz00_3904);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Globalize/free.scm 73 */
															long BgL_idxz00_2585;

															BgL_idxz00_2585 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2579);
															BgL_test2069z00_3903 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2585 + 3L)) == BgL_classz00_2577);
														}
													else
														{	/* Globalize/free.scm 73 */
															bool_t BgL_res1957z00_2610;

															{	/* Globalize/free.scm 73 */
																obj_t BgL_oclassz00_2593;

																{	/* Globalize/free.scm 73 */
																	obj_t BgL_arg1815z00_2601;
																	long BgL_arg1816z00_2602;

																	BgL_arg1815z00_2601 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Globalize/free.scm 73 */
																		long BgL_arg1817z00_2603;

																		BgL_arg1817z00_2603 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2579);
																		BgL_arg1816z00_2602 =
																			(BgL_arg1817z00_2603 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2593 =
																		VECTOR_REF(BgL_arg1815z00_2601,
																		BgL_arg1816z00_2602);
																}
																{	/* Globalize/free.scm 73 */
																	bool_t BgL__ortest_1115z00_2594;

																	BgL__ortest_1115z00_2594 =
																		(BgL_classz00_2577 == BgL_oclassz00_2593);
																	if (BgL__ortest_1115z00_2594)
																		{	/* Globalize/free.scm 73 */
																			BgL_res1957z00_2610 =
																				BgL__ortest_1115z00_2594;
																		}
																	else
																		{	/* Globalize/free.scm 73 */
																			long BgL_odepthz00_2595;

																			{	/* Globalize/free.scm 73 */
																				obj_t BgL_arg1804z00_2596;

																				BgL_arg1804z00_2596 =
																					(BgL_oclassz00_2593);
																				BgL_odepthz00_2595 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2596);
																			}
																			if ((3L < BgL_odepthz00_2595))
																				{	/* Globalize/free.scm 73 */
																					obj_t BgL_arg1802z00_2598;

																					{	/* Globalize/free.scm 73 */
																						obj_t BgL_arg1803z00_2599;

																						BgL_arg1803z00_2599 =
																							(BgL_oclassz00_2593);
																						BgL_arg1802z00_2598 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2599, 3L);
																					}
																					BgL_res1957z00_2610 =
																						(BgL_arg1802z00_2598 ==
																						BgL_classz00_2577);
																				}
																			else
																				{	/* Globalize/free.scm 73 */
																					BgL_res1957z00_2610 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2069z00_3903 = BgL_res1957z00_2610;
														}
												}
											}
											if (BgL_test2069z00_3903)
												{	/* Globalize/free.scm 74 */
													bool_t BgL_test2073z00_3927;

													{	/* Globalize/free.scm 74 */
														long BgL_arg1434z00_1775;

														{
															BgL_sexitzf2ginfozf2_bglt BgL_auxz00_3928;

															{
																obj_t BgL_auxz00_3929;

																{	/* Globalize/free.scm 74 */
																	BgL_objectz00_bglt BgL_tmpz00_3930;

																	BgL_tmpz00_3930 =
																		((BgL_objectz00_bglt)
																		((BgL_sexitz00_bglt) BgL_infoz00_1763));
																	BgL_auxz00_3929 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_3930);
																}
																BgL_auxz00_3928 =
																	((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_3929);
															}
															BgL_arg1434z00_1775 =
																(((BgL_sexitzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_3928))->
																BgL_freezd2markzd2);
														}
														BgL_test2073z00_3927 =
															(BgL_arg1434z00_1775 ==
															BGl_za2roundza2z00zzglobaliza7e_freeza7);
													}
													if (BgL_test2073z00_3927)
														{	/* Globalize/free.scm 74 */
															return BFALSE;
														}
													else
														{	/* Globalize/free.scm 74 */
															return BTRUE;
														}
												}
											else
												{	/* Globalize/free.scm 77 */
													obj_t BgL_arg1437z00_1776;

													{	/* Globalize/free.scm 77 */
														obj_t BgL_arg1448z00_1777;

														BgL_arg1448z00_1777 =
															BGl_shapez00zztools_shapez00(BgL_localz00_6);
														{	/* Globalize/free.scm 77 */
															obj_t BgL_list1449z00_1778;

															BgL_list1449z00_1778 =
																MAKE_YOUNG_PAIR(BgL_arg1448z00_1777, BNIL);
															BgL_arg1437z00_1776 =
																BGl_formatz00zz__r4_output_6_10_3z00
																(BGl_string1980z00zzglobaliza7e_freeza7,
																BgL_list1449z00_1778);
														}
													}
													return
														BGl_errorz00zz__errorz00
														(BGl_string1981z00zzglobaliza7e_freeza7,
														BgL_arg1437z00_1776, BgL_localz00_6);
												}
										}
								}
						}
					}
				else
					{	/* Globalize/free.scm 66 */
						return BFALSE;
					}
			}
		}

	}



/* get-free-vars */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2freezd2varsz00zzglobaliza7e_freeza7(BgL_nodez00_bglt
		BgL_nodez00_7, BgL_localz00_bglt BgL_integratorz00_8)
	{
		{	/* Globalize/free.scm 86 */
			{	/* Globalize/free.scm 89 */
				obj_t BgL_freez00_1779;

				{	/* Globalize/free.scm 89 */
					BgL_sfunz00_bglt BgL_oz00_2614;

					BgL_oz00_2614 =
						((BgL_sfunz00_bglt)
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_integratorz00_8)))->
							BgL_valuez00));
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3944;

						{
							obj_t BgL_auxz00_3945;

							{	/* Globalize/free.scm 89 */
								BgL_objectz00_bglt BgL_tmpz00_3946;

								BgL_tmpz00_3946 = ((BgL_objectz00_bglt) BgL_oz00_2614);
								BgL_auxz00_3945 = BGL_OBJECT_WIDENING(BgL_tmpz00_3946);
							}
							BgL_auxz00_3944 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3945);
						}
						BgL_freez00_1779 =
							(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3944))->
							BgL_freez00);
					}
				}
				{	/* Globalize/free.scm 90 */
					bool_t BgL_test2074z00_3951;

					if (NULLP(BgL_freez00_1779))
						{	/* Globalize/free.scm 90 */
							BgL_test2074z00_3951 = ((bool_t) 1);
						}
					else
						{	/* Globalize/free.scm 90 */
							BgL_test2074z00_3951 = PAIRP(BgL_freez00_1779);
						}
					if (BgL_test2074z00_3951)
						{	/* Globalize/free.scm 90 */
							return BgL_freez00_1779;
						}
					else
						{	/* Globalize/free.scm 92 */
							obj_t BgL_freez00_1782;

							BgL_freez00_1782 =
								BGl_internalzd2getzd2freezd2varsz12zc0zzglobaliza7e_freeza7
								(BgL_nodez00_7, BgL_integratorz00_8);
							{	/* Globalize/free.scm 93 */
								BgL_valuez00_bglt BgL_arg1453z00_1783;

								BgL_arg1453z00_1783 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_integratorz00_8)))->
									BgL_valuez00);
								{
									BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3958;

									{
										obj_t BgL_auxz00_3959;

										{	/* Globalize/free.scm 93 */
											BgL_objectz00_bglt BgL_tmpz00_3960;

											BgL_tmpz00_3960 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_arg1453z00_1783));
											BgL_auxz00_3959 = BGL_OBJECT_WIDENING(BgL_tmpz00_3960);
										}
										BgL_auxz00_3958 =
											((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3959);
									}
									((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3958))->
											BgL_freez00) = ((obj_t) BgL_freez00_1782), BUNSPEC);
								}
							}
							return BgL_freez00_1782;
						}
				}
			}
		}

	}



/* &get-free-vars */
	obj_t BGl_z62getzd2freezd2varsz62zzglobaliza7e_freeza7(obj_t BgL_envz00_3387,
		obj_t BgL_nodez00_3388, obj_t BgL_integratorz00_3389)
	{
		{	/* Globalize/free.scm 86 */
			return
				BGl_getzd2freezd2varsz00zzglobaliza7e_freeza7(
				((BgL_nodez00_bglt) BgL_nodez00_3388),
				((BgL_localz00_bglt) BgL_integratorz00_3389));
		}

	}



/* internal-get-free-vars! */
	obj_t
		BGl_internalzd2getzd2freezd2varsz12zc0zzglobaliza7e_freeza7(BgL_nodez00_bglt
		BgL_nodez00_9, BgL_localz00_bglt BgL_integratorz00_10)
	{
		{	/* Globalize/free.scm 107 */
			BGl_za2roundza2z00zzglobaliza7e_freeza7 =
				(BGl_za2roundza2z00zzglobaliza7e_freeza7 + 1L);
			BGl_za2integratorza2z00zzglobaliza7e_freeza7 =
				((obj_t) BgL_integratorz00_10);
			BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(BgL_integratorz00_10,
				BgL_integratorz00_10);
			{	/* Globalize/free.scm 114 */
				obj_t BgL_arg1472z00_1786;

				{	/* Globalize/free.scm 338 */
					bool_t BgL_test2076z00_3972;

					{	/* Globalize/free.scm 338 */
						obj_t BgL_classz00_2622;

						BgL_classz00_2622 = BGl_globalz00zzast_varz00;
						{	/* Globalize/free.scm 338 */
							BgL_objectz00_bglt BgL_arg1807z00_2624;

							{	/* Globalize/free.scm 338 */
								obj_t BgL_tmpz00_3973;

								BgL_tmpz00_3973 =
									((obj_t) ((BgL_variablez00_bglt) BgL_integratorz00_10));
								BgL_arg1807z00_2624 = (BgL_objectz00_bglt) (BgL_tmpz00_3973);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Globalize/free.scm 338 */
									long BgL_idxz00_2630;

									BgL_idxz00_2630 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2624);
									BgL_test2076z00_3972 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2630 + 2L)) == BgL_classz00_2622);
								}
							else
								{	/* Globalize/free.scm 338 */
									bool_t BgL_res1958z00_2655;

									{	/* Globalize/free.scm 338 */
										obj_t BgL_oclassz00_2638;

										{	/* Globalize/free.scm 338 */
											obj_t BgL_arg1815z00_2646;
											long BgL_arg1816z00_2647;

											BgL_arg1815z00_2646 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Globalize/free.scm 338 */
												long BgL_arg1817z00_2648;

												BgL_arg1817z00_2648 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2624);
												BgL_arg1816z00_2647 =
													(BgL_arg1817z00_2648 - OBJECT_TYPE);
											}
											BgL_oclassz00_2638 =
												VECTOR_REF(BgL_arg1815z00_2646, BgL_arg1816z00_2647);
										}
										{	/* Globalize/free.scm 338 */
											bool_t BgL__ortest_1115z00_2639;

											BgL__ortest_1115z00_2639 =
												(BgL_classz00_2622 == BgL_oclassz00_2638);
											if (BgL__ortest_1115z00_2639)
												{	/* Globalize/free.scm 338 */
													BgL_res1958z00_2655 = BgL__ortest_1115z00_2639;
												}
											else
												{	/* Globalize/free.scm 338 */
													long BgL_odepthz00_2640;

													{	/* Globalize/free.scm 338 */
														obj_t BgL_arg1804z00_2641;

														BgL_arg1804z00_2641 = (BgL_oclassz00_2638);
														BgL_odepthz00_2640 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2641);
													}
													if ((2L < BgL_odepthz00_2640))
														{	/* Globalize/free.scm 338 */
															obj_t BgL_arg1802z00_2643;

															{	/* Globalize/free.scm 338 */
																obj_t BgL_arg1803z00_2644;

																BgL_arg1803z00_2644 = (BgL_oclassz00_2638);
																BgL_arg1802z00_2643 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2644,
																	2L);
															}
															BgL_res1958z00_2655 =
																(BgL_arg1802z00_2643 == BgL_classz00_2622);
														}
													else
														{	/* Globalize/free.scm 338 */
															BgL_res1958z00_2655 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2076z00_3972 = BgL_res1958z00_2655;
								}
						}
					}
					if (BgL_test2076z00_3972)
						{	/* Globalize/free.scm 338 */
							BgL_arg1472z00_1786 =
								BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7(
								((BgL_globalz00_bglt)
									((BgL_variablez00_bglt) BgL_integratorz00_10)), BFALSE);
						}
					else
						{	/* Globalize/free.scm 338 */
							BgL_arg1472z00_1786 =
								BGl_thezd2localzd2closurez00zzglobaliza7e_freeza7(
								((obj_t)
									((BgL_variablez00_bglt) BgL_integratorz00_10)), BFALSE);
						}
				}
				BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(
					((BgL_localz00_bglt) BgL_arg1472z00_1786), BgL_integratorz00_10);
			}
			{	/* Globalize/free.scm 115 */
				obj_t BgL_g1286z00_1787;

				BgL_g1286z00_1787 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_integratorz00_10)))->
									BgL_valuez00))))->BgL_argsz00);
				{
					obj_t BgL_l1284z00_1789;

					BgL_l1284z00_1789 = BgL_g1286z00_1787;
				BgL_zc3z04anonymousza31473ze3z87_1790:
					if (PAIRP(BgL_l1284z00_1789))
						{	/* Globalize/free.scm 116 */
							{	/* Globalize/free.scm 115 */
								obj_t BgL_lz00_1792;

								BgL_lz00_1792 = CAR(BgL_l1284z00_1789);
								BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(
									((BgL_localz00_bglt) BgL_lz00_1792), BgL_integratorz00_10);
							}
							{
								obj_t BgL_l1284z00_4013;

								BgL_l1284z00_4013 = CDR(BgL_l1284z00_1789);
								BgL_l1284z00_1789 = BgL_l1284z00_4013;
								goto BgL_zc3z04anonymousza31473ze3z87_1790;
							}
						}
					else
						{	/* Globalize/free.scm 116 */
							((bool_t) 1);
						}
				}
			}
			return BGl_nodezd2freezd2zzglobaliza7e_freeza7(BgL_nodez00_9, BNIL);
		}

	}



/* node-free* */
	obj_t BGl_nodezd2freeza2z70zzglobaliza7e_freeza7(obj_t BgL_nodeza2za2_53,
		obj_t BgL_freez00_54)
	{
		{	/* Globalize/free.scm 326 */
			{
				obj_t BgL_nodeza2za2_1797;
				obj_t BgL_freez00_1798;

				BgL_nodeza2za2_1797 = BgL_nodeza2za2_53;
				BgL_freez00_1798 = BgL_freez00_54;
			BgL_zc3z04anonymousza31490ze3z87_1799:
				if (NULLP(BgL_nodeza2za2_1797))
					{	/* Globalize/free.scm 329 */
						return BgL_freez00_1798;
					}
				else
					{	/* Globalize/free.scm 331 */
						obj_t BgL_arg1502z00_1801;
						obj_t BgL_arg1509z00_1802;

						BgL_arg1502z00_1801 = CDR(((obj_t) BgL_nodeza2za2_1797));
						{	/* Globalize/free.scm 332 */
							obj_t BgL_arg1513z00_1803;

							BgL_arg1513z00_1803 = CAR(((obj_t) BgL_nodeza2za2_1797));
							BgL_arg1509z00_1802 =
								BGl_nodezd2freezd2zzglobaliza7e_freeza7(
								((BgL_nodez00_bglt) BgL_arg1513z00_1803), BgL_freez00_1798);
						}
						{
							obj_t BgL_freez00_4025;
							obj_t BgL_nodeza2za2_4024;

							BgL_nodeza2za2_4024 = BgL_arg1502z00_1801;
							BgL_freez00_4025 = BgL_arg1509z00_1802;
							BgL_freez00_1798 = BgL_freez00_4025;
							BgL_nodeza2za2_1797 = BgL_nodeza2za2_4024;
							goto BgL_zc3z04anonymousza31490ze3z87_1799;
						}
					}
			}
		}

	}



/* the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_thezd2closurezd2zzglobaliza7e_freeza7(BgL_variablez00_bglt
		BgL_variablez00_55, obj_t BgL_locz00_56)
	{
		{	/* Globalize/free.scm 337 */
			{	/* Globalize/free.scm 338 */
				bool_t BgL_test2082z00_4026;

				{	/* Globalize/free.scm 338 */
					obj_t BgL_classz00_2663;

					BgL_classz00_2663 = BGl_globalz00zzast_varz00;
					{	/* Globalize/free.scm 338 */
						BgL_objectz00_bglt BgL_arg1807z00_2665;

						{	/* Globalize/free.scm 338 */
							obj_t BgL_tmpz00_4027;

							BgL_tmpz00_4027 = ((obj_t) BgL_variablez00_55);
							BgL_arg1807z00_2665 = (BgL_objectz00_bglt) (BgL_tmpz00_4027);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Globalize/free.scm 338 */
								long BgL_idxz00_2671;

								BgL_idxz00_2671 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2665);
								BgL_test2082z00_4026 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2671 + 2L)) == BgL_classz00_2663);
							}
						else
							{	/* Globalize/free.scm 338 */
								bool_t BgL_res1959z00_2696;

								{	/* Globalize/free.scm 338 */
									obj_t BgL_oclassz00_2679;

									{	/* Globalize/free.scm 338 */
										obj_t BgL_arg1815z00_2687;
										long BgL_arg1816z00_2688;

										BgL_arg1815z00_2687 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Globalize/free.scm 338 */
											long BgL_arg1817z00_2689;

											BgL_arg1817z00_2689 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2665);
											BgL_arg1816z00_2688 = (BgL_arg1817z00_2689 - OBJECT_TYPE);
										}
										BgL_oclassz00_2679 =
											VECTOR_REF(BgL_arg1815z00_2687, BgL_arg1816z00_2688);
									}
									{	/* Globalize/free.scm 338 */
										bool_t BgL__ortest_1115z00_2680;

										BgL__ortest_1115z00_2680 =
											(BgL_classz00_2663 == BgL_oclassz00_2679);
										if (BgL__ortest_1115z00_2680)
											{	/* Globalize/free.scm 338 */
												BgL_res1959z00_2696 = BgL__ortest_1115z00_2680;
											}
										else
											{	/* Globalize/free.scm 338 */
												long BgL_odepthz00_2681;

												{	/* Globalize/free.scm 338 */
													obj_t BgL_arg1804z00_2682;

													BgL_arg1804z00_2682 = (BgL_oclassz00_2679);
													BgL_odepthz00_2681 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2682);
												}
												if ((2L < BgL_odepthz00_2681))
													{	/* Globalize/free.scm 338 */
														obj_t BgL_arg1802z00_2684;

														{	/* Globalize/free.scm 338 */
															obj_t BgL_arg1803z00_2685;

															BgL_arg1803z00_2685 = (BgL_oclassz00_2679);
															BgL_arg1802z00_2684 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2685,
																2L);
														}
														BgL_res1959z00_2696 =
															(BgL_arg1802z00_2684 == BgL_classz00_2663);
													}
												else
													{	/* Globalize/free.scm 338 */
														BgL_res1959z00_2696 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2082z00_4026 = BgL_res1959z00_2696;
							}
					}
				}
				if (BgL_test2082z00_4026)
					{	/* Globalize/free.scm 338 */
						return
							BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7(
							((BgL_globalz00_bglt) BgL_variablez00_55), BgL_locz00_56);
					}
				else
					{	/* Globalize/free.scm 338 */
						return
							BGl_thezd2localzd2closurez00zzglobaliza7e_freeza7(
							((obj_t) BgL_variablez00_55), BgL_locz00_56);
					}
			}
		}

	}



/* &the-closure */
	obj_t BGl_z62thezd2closurezb0zzglobaliza7e_freeza7(obj_t BgL_envz00_3390,
		obj_t BgL_variablez00_3391, obj_t BgL_locz00_3392)
	{
		{	/* Globalize/free.scm 337 */
			return
				BGl_thezd2closurezd2zzglobaliza7e_freeza7(
				((BgL_variablez00_bglt) BgL_variablez00_3391), BgL_locz00_3392);
		}

	}



/* the-global-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7(BgL_globalz00_bglt
		BgL_globalz00_57, obj_t BgL_locz00_58)
	{
		{	/* Globalize/free.scm 345 */
			{
				BgL_globalz00_bglt BgL_gz00_1855;

				{	/* Globalize/free.scm 354 */
					obj_t BgL_closurez00_1807;

					BgL_closurez00_1807 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_globalz00_57)))->
										BgL_valuez00))))->BgL_thezd2closurezd2);
					{	/* Globalize/free.scm 356 */
						bool_t BgL_test2086z00_4059;

						{	/* Globalize/free.scm 356 */
							obj_t BgL_classz00_2733;

							BgL_classz00_2733 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_closurez00_1807))
								{	/* Globalize/free.scm 356 */
									BgL_objectz00_bglt BgL_arg1807z00_2735;

									BgL_arg1807z00_2735 =
										(BgL_objectz00_bglt) (BgL_closurez00_1807);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Globalize/free.scm 356 */
											long BgL_idxz00_2741;

											BgL_idxz00_2741 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2735);
											BgL_test2086z00_4059 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2741 + 2L)) == BgL_classz00_2733);
										}
									else
										{	/* Globalize/free.scm 356 */
											bool_t BgL_res1961z00_2766;

											{	/* Globalize/free.scm 356 */
												obj_t BgL_oclassz00_2749;

												{	/* Globalize/free.scm 356 */
													obj_t BgL_arg1815z00_2757;
													long BgL_arg1816z00_2758;

													BgL_arg1815z00_2757 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Globalize/free.scm 356 */
														long BgL_arg1817z00_2759;

														BgL_arg1817z00_2759 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2735);
														BgL_arg1816z00_2758 =
															(BgL_arg1817z00_2759 - OBJECT_TYPE);
													}
													BgL_oclassz00_2749 =
														VECTOR_REF(BgL_arg1815z00_2757,
														BgL_arg1816z00_2758);
												}
												{	/* Globalize/free.scm 356 */
													bool_t BgL__ortest_1115z00_2750;

													BgL__ortest_1115z00_2750 =
														(BgL_classz00_2733 == BgL_oclassz00_2749);
													if (BgL__ortest_1115z00_2750)
														{	/* Globalize/free.scm 356 */
															BgL_res1961z00_2766 = BgL__ortest_1115z00_2750;
														}
													else
														{	/* Globalize/free.scm 356 */
															long BgL_odepthz00_2751;

															{	/* Globalize/free.scm 356 */
																obj_t BgL_arg1804z00_2752;

																BgL_arg1804z00_2752 = (BgL_oclassz00_2749);
																BgL_odepthz00_2751 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2752);
															}
															if ((2L < BgL_odepthz00_2751))
																{	/* Globalize/free.scm 356 */
																	obj_t BgL_arg1802z00_2754;

																	{	/* Globalize/free.scm 356 */
																		obj_t BgL_arg1803z00_2755;

																		BgL_arg1803z00_2755 = (BgL_oclassz00_2749);
																		BgL_arg1802z00_2754 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2755, 2L);
																	}
																	BgL_res1961z00_2766 =
																		(BgL_arg1802z00_2754 == BgL_classz00_2733);
																}
															else
																{	/* Globalize/free.scm 356 */
																	BgL_res1961z00_2766 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2086z00_4059 = BgL_res1961z00_2766;
										}
								}
							else
								{	/* Globalize/free.scm 356 */
									BgL_test2086z00_4059 = ((bool_t) 0);
								}
						}
						if (BgL_test2086z00_4059)
							{	/* Globalize/free.scm 356 */
								return BgL_closurez00_1807;
							}
						else
							{	/* Globalize/free.scm 358 */
								obj_t BgL_g1132z00_1809;

								BgL_gz00_1855 = BgL_globalz00_57;
								if (CBOOL(
										(((BgL_globalz00_bglt) COBJECT(BgL_gz00_1855))->
											BgL_aliasz00)))
									{	/* Globalize/free.scm 350 */
										obj_t BgL_agz00_1859;

										{	/* Globalize/free.scm 350 */
											obj_t BgL_arg1625z00_1861;
											obj_t BgL_arg1626z00_1862;

											BgL_arg1625z00_1861 =
												(((BgL_globalz00_bglt) COBJECT(BgL_gz00_1855))->
												BgL_aliasz00);
											BgL_arg1626z00_1862 =
												(((BgL_globalz00_bglt) COBJECT(BgL_gz00_1855))->
												BgL_modulez00);
											{	/* Globalize/free.scm 350 */
												obj_t BgL_list1627z00_1863;

												BgL_list1627z00_1863 =
													MAKE_YOUNG_PAIR(BgL_arg1626z00_1862, BNIL);
												BgL_agz00_1859 =
													BGl_findzd2globalzd2zzast_envz00(BgL_arg1625z00_1861,
													BgL_list1627z00_1863);
											}
										}
										{	/* Globalize/free.scm 351 */
											bool_t BgL_test2092z00_4089;

											{	/* Globalize/free.scm 351 */
												obj_t BgL_classz00_2697;

												BgL_classz00_2697 = BGl_globalz00zzast_varz00;
												if (BGL_OBJECTP(BgL_agz00_1859))
													{	/* Globalize/free.scm 351 */
														BgL_objectz00_bglt BgL_arg1807z00_2699;

														BgL_arg1807z00_2699 =
															(BgL_objectz00_bglt) (BgL_agz00_1859);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Globalize/free.scm 351 */
																long BgL_idxz00_2705;

																BgL_idxz00_2705 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2699);
																BgL_test2092z00_4089 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2705 + 2L)) ==
																	BgL_classz00_2697);
															}
														else
															{	/* Globalize/free.scm 351 */
																bool_t BgL_res1960z00_2730;

																{	/* Globalize/free.scm 351 */
																	obj_t BgL_oclassz00_2713;

																	{	/* Globalize/free.scm 351 */
																		obj_t BgL_arg1815z00_2721;
																		long BgL_arg1816z00_2722;

																		BgL_arg1815z00_2721 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Globalize/free.scm 351 */
																			long BgL_arg1817z00_2723;

																			BgL_arg1817z00_2723 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2699);
																			BgL_arg1816z00_2722 =
																				(BgL_arg1817z00_2723 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2713 =
																			VECTOR_REF(BgL_arg1815z00_2721,
																			BgL_arg1816z00_2722);
																	}
																	{	/* Globalize/free.scm 351 */
																		bool_t BgL__ortest_1115z00_2714;

																		BgL__ortest_1115z00_2714 =
																			(BgL_classz00_2697 == BgL_oclassz00_2713);
																		if (BgL__ortest_1115z00_2714)
																			{	/* Globalize/free.scm 351 */
																				BgL_res1960z00_2730 =
																					BgL__ortest_1115z00_2714;
																			}
																		else
																			{	/* Globalize/free.scm 351 */
																				long BgL_odepthz00_2715;

																				{	/* Globalize/free.scm 351 */
																					obj_t BgL_arg1804z00_2716;

																					BgL_arg1804z00_2716 =
																						(BgL_oclassz00_2713);
																					BgL_odepthz00_2715 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2716);
																				}
																				if ((2L < BgL_odepthz00_2715))
																					{	/* Globalize/free.scm 351 */
																						obj_t BgL_arg1802z00_2718;

																						{	/* Globalize/free.scm 351 */
																							obj_t BgL_arg1803z00_2719;

																							BgL_arg1803z00_2719 =
																								(BgL_oclassz00_2713);
																							BgL_arg1802z00_2718 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2719, 2L);
																						}
																						BgL_res1960z00_2730 =
																							(BgL_arg1802z00_2718 ==
																							BgL_classz00_2697);
																					}
																				else
																					{	/* Globalize/free.scm 351 */
																						BgL_res1960z00_2730 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2092z00_4089 = BgL_res1960z00_2730;
															}
													}
												else
													{	/* Globalize/free.scm 351 */
														BgL_test2092z00_4089 = ((bool_t) 0);
													}
											}
											if (BgL_test2092z00_4089)
												{	/* Globalize/free.scm 351 */
													BgL_g1132z00_1809 =
														BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7(
														((BgL_globalz00_bglt) BgL_agz00_1859),
														BgL_locz00_58);
												}
											else
												{	/* Globalize/free.scm 351 */
													BgL_g1132z00_1809 = BFALSE;
												}
										}
									}
								else
									{	/* Globalize/free.scm 349 */
										BgL_g1132z00_1809 = BFALSE;
									}
								if (CBOOL(BgL_g1132z00_1809))
									{	/* Globalize/free.scm 358 */
										{	/* Globalize/free.scm 361 */
											BgL_valuez00_bglt BgL_arg1516z00_1812;

											BgL_arg1516z00_1812 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_globalz00_57)))->
												BgL_valuez00);
											((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																BgL_arg1516z00_1812)))->BgL_thezd2closurezd2) =
												((obj_t) BgL_g1132z00_1809), BUNSPEC);
										}
										return BgL_g1132z00_1809;
									}
								else
									{	/* Globalize/free.scm 364 */
										BgL_globalz00_bglt BgL_glocloz00_1813;

										BgL_glocloz00_1813 =
											BGl_makezd2globalzd2closurez00zzglobaliza7e_globalzd2closurez75
											(BgL_globalz00_57);
										{	/* Globalize/free.scm 364 */
											long BgL_arityz00_1814;

											BgL_arityz00_1814 =
												(((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			BgL_globalz00_57)))->BgL_valuez00))))->
												BgL_arityz00);
											{	/* Globalize/free.scm 365 */
												obj_t BgL_makezd2clozd2_1815;

												if ((BgL_arityz00_1814 < 0L))
													{	/* Globalize/free.scm 366 */
														BgL_makezd2clozd2_1815 = CNST_TABLE_REF(0);
													}
												else
													{	/* Globalize/free.scm 366 */
														BgL_makezd2clozd2_1815 = CNST_TABLE_REF(1);
													}
												{	/* Globalize/free.scm 366 */
													BgL_nodez00_bglt BgL_nodez00_1816;

													{	/* Globalize/free.scm 370 */
														obj_t BgL_arg1589z00_1836;

														{	/* Globalize/free.scm 370 */
															obj_t BgL_arg1591z00_1837;

															{	/* Globalize/free.scm 370 */
																BgL_refz00_bglt BgL_arg1593z00_1838;
																obj_t BgL_arg1594z00_1839;

																{	/* Globalize/free.scm 370 */
																	BgL_refz00_bglt BgL_new1135z00_1840;

																	{	/* Globalize/free.scm 373 */
																		BgL_refz00_bglt BgL_new1134z00_1841;

																		BgL_new1134z00_1841 =
																			((BgL_refz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_refz00_bgl))));
																		{	/* Globalize/free.scm 373 */
																			long BgL_arg1595z00_1842;

																			BgL_arg1595z00_1842 =
																				BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1134z00_1841),
																				BgL_arg1595z00_1842);
																		}
																		{	/* Globalize/free.scm 373 */
																			BgL_objectz00_bglt BgL_tmpz00_4133;

																			BgL_tmpz00_4133 =
																				((BgL_objectz00_bglt)
																				BgL_new1134z00_1841);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4133,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1134z00_1841);
																		BgL_new1135z00_1840 = BgL_new1134z00_1841;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1135z00_1840)))->
																			BgL_locz00) =
																		((obj_t) BgL_locz00_58), BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1135z00_1840)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00)),
																		BUNSPEC);
																	((((BgL_varz00_bglt)
																				COBJECT(((BgL_varz00_bglt)
																						BgL_new1135z00_1840)))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_glocloz00_1813)), BUNSPEC);
																	BgL_arg1593z00_1838 = BgL_new1135z00_1840;
																}
																{	/* Globalize/free.scm 374 */
																	BgL_literalz00_bglt BgL_arg1602z00_1843;
																	obj_t BgL_arg1605z00_1844;

																	{	/* Globalize/free.scm 374 */
																		BgL_literalz00_bglt BgL_new1137z00_1845;

																		{	/* Globalize/free.scm 377 */
																			BgL_literalz00_bglt BgL_new1136z00_1846;

																			BgL_new1136z00_1846 =
																				((BgL_literalz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_literalz00_bgl))));
																			{	/* Globalize/free.scm 377 */
																				long BgL_arg1606z00_1847;

																				BgL_arg1606z00_1847 =
																					BGL_CLASS_NUM
																					(BGl_literalz00zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1136z00_1846),
																					BgL_arg1606z00_1847);
																			}
																			{	/* Globalize/free.scm 377 */
																				BgL_objectz00_bglt BgL_tmpz00_4149;

																				BgL_tmpz00_4149 =
																					((BgL_objectz00_bglt)
																					BgL_new1136z00_1846);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4149,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1136z00_1846);
																			BgL_new1137z00_1845 = BgL_new1136z00_1846;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1137z00_1845)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_58), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1137z00_1845)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2_za2z00zztype_cachez00)),
																			BUNSPEC);
																		((((BgL_atomz00_bglt)
																					COBJECT(((BgL_atomz00_bglt)
																							BgL_new1137z00_1845)))->
																				BgL_valuez00) =
																			((obj_t) BINT(BgL_arityz00_1814)),
																			BUNSPEC);
																		BgL_arg1602z00_1843 = BgL_new1137z00_1845;
																	}
																	{	/* Globalize/free.scm 378 */
																		BgL_literalz00_bglt BgL_arg1609z00_1848;

																		{	/* Globalize/free.scm 378 */
																			BgL_literalz00_bglt BgL_new1139z00_1849;

																			{	/* Globalize/free.scm 381 */
																				BgL_literalz00_bglt BgL_new1138z00_1850;

																				BgL_new1138z00_1850 =
																					((BgL_literalz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_literalz00_bgl))));
																				{	/* Globalize/free.scm 381 */
																					long BgL_arg1611z00_1851;

																					BgL_arg1611z00_1851 =
																						BGL_CLASS_NUM
																						(BGl_literalz00zzast_nodez00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1138z00_1850),
																						BgL_arg1611z00_1851);
																				}
																				{	/* Globalize/free.scm 381 */
																					BgL_objectz00_bglt BgL_tmpz00_4165;

																					BgL_tmpz00_4165 =
																						((BgL_objectz00_bglt)
																						BgL_new1138z00_1850);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_4165, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1138z00_1850);
																				BgL_new1139z00_1849 =
																					BgL_new1138z00_1850;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1139z00_1849)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_58), BUNSPEC);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_new1139z00_1849)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2_za2z00zztype_cachez00)),
																				BUNSPEC);
																			((((BgL_atomz00_bglt)
																						COBJECT(((BgL_atomz00_bglt)
																								BgL_new1139z00_1849)))->
																					BgL_valuez00) =
																				((obj_t) BINT(0L)), BUNSPEC);
																			BgL_arg1609z00_1848 = BgL_new1139z00_1849;
																		}
																		BgL_arg1605z00_1844 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_arg1609z00_1848), BNIL);
																	}
																	BgL_arg1594z00_1839 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1602z00_1843),
																		BgL_arg1605z00_1844);
																}
																BgL_arg1591z00_1837 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1593z00_1838),
																	BgL_arg1594z00_1839);
															}
															BgL_arg1589z00_1836 =
																MAKE_YOUNG_PAIR(BgL_makezd2clozd2_1815,
																BgL_arg1591z00_1837);
														}
														BgL_nodez00_1816 =
															BGl_sexpzd2ze3nodez31zzast_sexpz00
															(BgL_arg1589z00_1836, BNIL, BgL_locz00_58,
															CNST_TABLE_REF(2));
													}
													{	/* Globalize/free.scm 369 */
														BgL_globalz00_bglt BgL_closurez00_1817;

														{	/* Globalize/free.scm 386 */
															obj_t BgL_arg1552z00_1822;
															obj_t BgL_arg1553z00_1823;
															obj_t BgL_arg1559z00_1824;

															{	/* Globalize/free.scm 386 */
																obj_t BgL_arg1561z00_1825;

																{	/* Globalize/free.scm 386 */
																	obj_t BgL_arg1564z00_1826;
																	obj_t BgL_arg1565z00_1827;

																	{	/* Globalize/free.scm 386 */
																		obj_t BgL_arg1571z00_1828;

																		if (CBOOL(
																				(((BgL_globalz00_bglt)
																						COBJECT(BgL_globalz00_57))->
																					BgL_aliasz00)))
																			{	/* Globalize/free.scm 386 */
																				BgL_arg1571z00_1828 =
																					(((BgL_globalz00_bglt)
																						COBJECT(BgL_globalz00_57))->
																					BgL_aliasz00);
																			}
																		else
																			{	/* Globalize/free.scm 386 */
																				BgL_arg1571z00_1828 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								BgL_globalz00_57)))->BgL_idz00);
																			}
																		{	/* Globalize/free.scm 385 */
																			obj_t BgL_arg1455z00_2788;

																			BgL_arg1455z00_2788 =
																				SYMBOL_TO_STRING(
																				((obj_t) BgL_arg1571z00_1828));
																			BgL_arg1564z00_1826 =
																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																				(BgL_arg1455z00_2788);
																		}
																	}
																	{	/* Globalize/free.scm 385 */
																		obj_t BgL_symbolz00_2789;

																		BgL_symbolz00_2789 = CNST_TABLE_REF(3);
																		{	/* Globalize/free.scm 385 */
																			obj_t BgL_arg1455z00_2790;

																			BgL_arg1455z00_2790 =
																				SYMBOL_TO_STRING(BgL_symbolz00_2789);
																			BgL_arg1565z00_1827 =
																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																				(BgL_arg1455z00_2790);
																		}
																	}
																	BgL_arg1561z00_1825 =
																		string_append(BgL_arg1564z00_1826,
																		BgL_arg1565z00_1827);
																}
																BgL_arg1552z00_1822 =
																	bstring_to_symbol(BgL_arg1561z00_1825);
															}
															BgL_arg1553z00_1823 =
																(((BgL_globalz00_bglt)
																	COBJECT(BgL_globalz00_57))->BgL_modulez00);
															{	/* Globalize/free.scm 392 */
																bool_t BgL_test2100z00_4201;

																{	/* Globalize/free.scm 392 */
																	BgL_valuez00_bglt BgL_arg1585z00_1835;

																	BgL_arg1585z00_1835 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					BgL_globalz00_57)))->BgL_valuez00);
																	{	/* Globalize/free.scm 392 */
																		obj_t BgL_classz00_2794;

																		BgL_classz00_2794 = BGl_sfunz00zzast_varz00;
																		{	/* Globalize/free.scm 392 */
																			BgL_objectz00_bglt BgL_arg1807z00_2796;

																			{	/* Globalize/free.scm 392 */
																				obj_t BgL_tmpz00_4204;

																				BgL_tmpz00_4204 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_arg1585z00_1835));
																				BgL_arg1807z00_2796 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_4204);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Globalize/free.scm 392 */
																					long BgL_idxz00_2802;

																					BgL_idxz00_2802 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2796);
																					BgL_test2100z00_4201 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2802 + 3L)) ==
																						BgL_classz00_2794);
																				}
																			else
																				{	/* Globalize/free.scm 392 */
																					bool_t BgL_res1962z00_2827;

																					{	/* Globalize/free.scm 392 */
																						obj_t BgL_oclassz00_2810;

																						{	/* Globalize/free.scm 392 */
																							obj_t BgL_arg1815z00_2818;
																							long BgL_arg1816z00_2819;

																							BgL_arg1815z00_2818 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Globalize/free.scm 392 */
																								long BgL_arg1817z00_2820;

																								BgL_arg1817z00_2820 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2796);
																								BgL_arg1816z00_2819 =
																									(BgL_arg1817z00_2820 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2810 =
																								VECTOR_REF(BgL_arg1815z00_2818,
																								BgL_arg1816z00_2819);
																						}
																						{	/* Globalize/free.scm 392 */
																							bool_t BgL__ortest_1115z00_2811;

																							BgL__ortest_1115z00_2811 =
																								(BgL_classz00_2794 ==
																								BgL_oclassz00_2810);
																							if (BgL__ortest_1115z00_2811)
																								{	/* Globalize/free.scm 392 */
																									BgL_res1962z00_2827 =
																										BgL__ortest_1115z00_2811;
																								}
																							else
																								{	/* Globalize/free.scm 392 */
																									long BgL_odepthz00_2812;

																									{	/* Globalize/free.scm 392 */
																										obj_t BgL_arg1804z00_2813;

																										BgL_arg1804z00_2813 =
																											(BgL_oclassz00_2810);
																										BgL_odepthz00_2812 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2813);
																									}
																									if ((3L < BgL_odepthz00_2812))
																										{	/* Globalize/free.scm 392 */
																											obj_t BgL_arg1802z00_2815;

																											{	/* Globalize/free.scm 392 */
																												obj_t
																													BgL_arg1803z00_2816;
																												BgL_arg1803z00_2816 =
																													(BgL_oclassz00_2810);
																												BgL_arg1802z00_2815 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2816,
																													3L);
																											}
																											BgL_res1962z00_2827 =
																												(BgL_arg1802z00_2815 ==
																												BgL_classz00_2794);
																										}
																									else
																										{	/* Globalize/free.scm 392 */
																											BgL_res1962z00_2827 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2100z00_4201 =
																						BgL_res1962z00_2827;
																				}
																		}
																	}
																}
																if (BgL_test2100z00_4201)
																	{	/* Globalize/free.scm 392 */
																		if (
																			((((BgL_sfunz00_bglt) COBJECT(
																							((BgL_sfunz00_bglt)
																								(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt)
																												BgL_globalz00_57)))->
																									BgL_valuez00))))->
																					BgL_classz00) == CNST_TABLE_REF(4)))
																			{	/* Globalize/free.scm 393 */
																				BgL_arg1559z00_1824 = CNST_TABLE_REF(4);
																			}
																		else
																			{	/* Globalize/free.scm 393 */
																				BgL_arg1559z00_1824 = CNST_TABLE_REF(5);
																			}
																	}
																else
																	{	/* Globalize/free.scm 392 */
																		BgL_arg1559z00_1824 = CNST_TABLE_REF(5);
																	}
															}
															BgL_closurez00_1817 =
																BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2
																(BgL_arg1552z00_1822, BgL_arg1553z00_1823,
																((obj_t) BgL_nodez00_1816), BgL_arg1559z00_1824,
																BgL_locz00_58);
														}
														{	/* Globalize/free.scm 385 */

															((((BgL_globalz00_bglt)
																		COBJECT(BgL_closurez00_1817))->
																	BgL_libraryz00) =
																((obj_t) (((BgL_globalz00_bglt)
																			COBJECT(BgL_globalz00_57))->
																		BgL_libraryz00)), BUNSPEC);
															((((BgL_globalz00_bglt)
																		COBJECT(BgL_closurez00_1817))->
																	BgL_importz00) =
																((obj_t) (((BgL_globalz00_bglt)
																			COBJECT(BgL_globalz00_57))->
																		BgL_importz00)), BUNSPEC);
															{	/* Globalize/free.scm 402 */
																BgL_valuez00_bglt BgL_arg1544z00_1820;

																BgL_arg1544z00_1820 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_globalz00_57)))->BgL_valuez00);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_arg1544z00_1820)))->
																		BgL_thezd2closurezd2) =
																	((obj_t) ((obj_t) BgL_closurez00_1817)),
																	BUNSPEC);
															}
															{	/* Globalize/free.scm 406 */
																BgL_valuez00_bglt BgL_arg1546z00_1821;

																BgL_arg1546z00_1821 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_glocloz00_1813)))->BgL_valuez00);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_arg1546z00_1821)))->
																		BgL_thezd2closurezd2) =
																	((obj_t) ((obj_t) BgL_closurez00_1817)),
																	BUNSPEC);
															}
															return ((obj_t) BgL_closurez00_1817);
														}
													}
												}
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* &the-global-closure */
	obj_t BGl_z62thezd2globalzd2closurez62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3393, obj_t BgL_globalz00_3394, obj_t BgL_locz00_3395)
	{
		{	/* Globalize/free.scm 345 */
			return
				BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7(
				((BgL_globalz00_bglt) BgL_globalz00_3394), BgL_locz00_3395);
		}

	}



/* the-local-closure */
	obj_t BGl_thezd2localzd2closurez00zzglobaliza7e_freeza7(obj_t BgL_localz00_59,
		obj_t BgL_locz00_60)
	{
		{	/* Globalize/free.scm 416 */
			{	/* Globalize/free.scm 417 */
				BgL_valuez00_bglt BgL_infoz00_1865;

				BgL_infoz00_1865 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_localz00_bglt) BgL_localz00_59))))->BgL_valuez00);
				{	/* Globalize/free.scm 418 */
					bool_t BgL_test2105z00_4259;

					{	/* Globalize/free.scm 418 */
						obj_t BgL_arg1654z00_1881;

						BgL_arg1654z00_1881 =
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt)
										((BgL_sfunz00_bglt) BgL_infoz00_1865))))->
							BgL_thezd2closurezd2);
						{	/* Globalize/free.scm 418 */
							obj_t BgL_classz00_2841;

							BgL_classz00_2841 = BGl_localz00zzast_varz00;
							if (BGL_OBJECTP(BgL_arg1654z00_1881))
								{	/* Globalize/free.scm 418 */
									BgL_objectz00_bglt BgL_arg1807z00_2843;

									BgL_arg1807z00_2843 =
										(BgL_objectz00_bglt) (BgL_arg1654z00_1881);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Globalize/free.scm 418 */
											long BgL_idxz00_2849;

											BgL_idxz00_2849 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2843);
											BgL_test2105z00_4259 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2849 + 2L)) == BgL_classz00_2841);
										}
									else
										{	/* Globalize/free.scm 418 */
											bool_t BgL_res1963z00_2874;

											{	/* Globalize/free.scm 418 */
												obj_t BgL_oclassz00_2857;

												{	/* Globalize/free.scm 418 */
													obj_t BgL_arg1815z00_2865;
													long BgL_arg1816z00_2866;

													BgL_arg1815z00_2865 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Globalize/free.scm 418 */
														long BgL_arg1817z00_2867;

														BgL_arg1817z00_2867 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2843);
														BgL_arg1816z00_2866 =
															(BgL_arg1817z00_2867 - OBJECT_TYPE);
													}
													BgL_oclassz00_2857 =
														VECTOR_REF(BgL_arg1815z00_2865,
														BgL_arg1816z00_2866);
												}
												{	/* Globalize/free.scm 418 */
													bool_t BgL__ortest_1115z00_2858;

													BgL__ortest_1115z00_2858 =
														(BgL_classz00_2841 == BgL_oclassz00_2857);
													if (BgL__ortest_1115z00_2858)
														{	/* Globalize/free.scm 418 */
															BgL_res1963z00_2874 = BgL__ortest_1115z00_2858;
														}
													else
														{	/* Globalize/free.scm 418 */
															long BgL_odepthz00_2859;

															{	/* Globalize/free.scm 418 */
																obj_t BgL_arg1804z00_2860;

																BgL_arg1804z00_2860 = (BgL_oclassz00_2857);
																BgL_odepthz00_2859 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2860);
															}
															if ((2L < BgL_odepthz00_2859))
																{	/* Globalize/free.scm 418 */
																	obj_t BgL_arg1802z00_2862;

																	{	/* Globalize/free.scm 418 */
																		obj_t BgL_arg1803z00_2863;

																		BgL_arg1803z00_2863 = (BgL_oclassz00_2857);
																		BgL_arg1802z00_2862 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2863, 2L);
																	}
																	BgL_res1963z00_2874 =
																		(BgL_arg1802z00_2862 == BgL_classz00_2841);
																}
															else
																{	/* Globalize/free.scm 418 */
																	BgL_res1963z00_2874 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2105z00_4259 = BgL_res1963z00_2874;
										}
								}
							else
								{	/* Globalize/free.scm 418 */
									BgL_test2105z00_4259 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2105z00_4259)
						{	/* Globalize/free.scm 418 */
							return
								(((BgL_funz00_bglt) COBJECT(
										((BgL_funz00_bglt)
											((BgL_sfunz00_bglt) BgL_infoz00_1865))))->
								BgL_thezd2closurezd2);
						}
					else
						{	/* Globalize/free.scm 420 */
							BgL_localz00_bglt BgL_closurez00_1868;

							{	/* Globalize/free.scm 420 */
								obj_t BgL_arg1651z00_1880;

								BgL_arg1651z00_1880 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt) BgL_localz00_59))))->BgL_idz00);
								BgL_closurez00_1868 =
									BGl_makezd2localzd2svarz00zzast_localz00(BgL_arg1651z00_1880,
									((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00));
							}
							{	/* Globalize/free.scm 421 */
								bool_t BgL_arg1642z00_1869;

								BgL_arg1642z00_1869 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt) BgL_localz00_59))))->
									BgL_userzf3zf3);
								((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
													BgL_closurez00_1868)))->BgL_userzf3zf3) =
									((bool_t) BgL_arg1642z00_1869), BUNSPEC);
							}
							{	/* Globalize/free.scm 422 */
								BgL_svarz00_bglt BgL_tmp1140z00_1870;

								BgL_tmp1140z00_1870 =
									((BgL_svarz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_closurez00_1868)))->
										BgL_valuez00));
								{	/* Globalize/free.scm 422 */
									BgL_svarzf2ginfozf2_bglt BgL_wide1142z00_1872;

									BgL_wide1142z00_1872 =
										((BgL_svarzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_svarzf2ginfozf2_bgl))));
									{	/* Globalize/free.scm 422 */
										obj_t BgL_auxz00_4305;
										BgL_objectz00_bglt BgL_tmpz00_4302;

										BgL_auxz00_4305 = ((obj_t) BgL_wide1142z00_1872);
										BgL_tmpz00_4302 =
											((BgL_objectz00_bglt)
											((BgL_svarz00_bglt) BgL_tmp1140z00_1870));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4302, BgL_auxz00_4305);
									}
									((BgL_objectz00_bglt)
										((BgL_svarz00_bglt) BgL_tmp1140z00_1870));
									{	/* Globalize/free.scm 422 */
										long BgL_arg1646z00_1873;

										BgL_arg1646z00_1873 =
											BGL_CLASS_NUM(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1140z00_1870)),
											BgL_arg1646z00_1873);
									}
									((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_tmp1140z00_1870));
								}
								{
									BgL_svarzf2ginfozf2_bglt BgL_auxz00_4316;

									{
										obj_t BgL_auxz00_4317;

										{	/* Globalize/free.scm 422 */
											BgL_objectz00_bglt BgL_tmpz00_4318;

											BgL_tmpz00_4318 =
												((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1140z00_1870));
											BgL_auxz00_4317 = BGL_OBJECT_WIDENING(BgL_tmpz00_4318);
										}
										BgL_auxz00_4316 =
											((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_4317);
									}
									((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4316))->
											BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
								}
								{
									BgL_svarzf2ginfozf2_bglt BgL_auxz00_4324;

									{
										obj_t BgL_auxz00_4325;

										{	/* Globalize/free.scm 422 */
											BgL_objectz00_bglt BgL_tmpz00_4326;

											BgL_tmpz00_4326 =
												((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1140z00_1870));
											BgL_auxz00_4325 = BGL_OBJECT_WIDENING(BgL_tmpz00_4326);
										}
										BgL_auxz00_4324 =
											((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_4325);
									}
									((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4324))->
											BgL_freezd2markzd2) = ((long) -10L), BUNSPEC);
								}
								{
									BgL_svarzf2ginfozf2_bglt BgL_auxz00_4332;

									{
										obj_t BgL_auxz00_4333;

										{	/* Globalize/free.scm 422 */
											BgL_objectz00_bglt BgL_tmpz00_4334;

											BgL_tmpz00_4334 =
												((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1140z00_1870));
											BgL_auxz00_4333 = BGL_OBJECT_WIDENING(BgL_tmpz00_4334);
										}
										BgL_auxz00_4332 =
											((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_4333);
									}
									((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4332))->
											BgL_markz00) = ((long) -10L), BUNSPEC);
								}
								{
									BgL_svarzf2ginfozf2_bglt BgL_auxz00_4340;

									{
										obj_t BgL_auxz00_4341;

										{	/* Globalize/free.scm 422 */
											BgL_objectz00_bglt BgL_tmpz00_4342;

											BgL_tmpz00_4342 =
												((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1140z00_1870));
											BgL_auxz00_4341 = BGL_OBJECT_WIDENING(BgL_tmpz00_4342);
										}
										BgL_auxz00_4340 =
											((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_4341);
									}
									((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4340))->
											BgL_celledzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
								}
								{
									BgL_svarzf2ginfozf2_bglt BgL_auxz00_4348;

									{
										obj_t BgL_auxz00_4349;

										{	/* Globalize/free.scm 422 */
											BgL_objectz00_bglt BgL_tmpz00_4350;

											BgL_tmpz00_4350 =
												((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1140z00_1870));
											BgL_auxz00_4349 = BGL_OBJECT_WIDENING(BgL_tmpz00_4350);
										}
										BgL_auxz00_4348 =
											((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_4349);
									}
									((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4348))->
											BgL_stackablez00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
								}
								((BgL_svarz00_bglt) BgL_tmp1140z00_1870);
							}
							{	/* Globalize/free.scm 423 */
								BgL_localzf2ginfozf2_bglt BgL_wide1146z00_1877;

								BgL_wide1146z00_1877 =
									((BgL_localzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localzf2ginfozf2_bgl))));
								{	/* Globalize/free.scm 423 */
									obj_t BgL_auxz00_4361;
									BgL_objectz00_bglt BgL_tmpz00_4358;

									BgL_auxz00_4361 = ((obj_t) BgL_wide1146z00_1877);
									BgL_tmpz00_4358 =
										((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_closurez00_1868));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4358, BgL_auxz00_4361);
								}
								((BgL_objectz00_bglt)
									((BgL_localz00_bglt) BgL_closurez00_1868));
								{	/* Globalize/free.scm 423 */
									long BgL_arg1650z00_1878;

									BgL_arg1650z00_1878 =
										BGL_CLASS_NUM(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_closurez00_1868)),
										BgL_arg1650z00_1878);
								}
								((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_closurez00_1868));
							}
							{
								BgL_localzf2ginfozf2_bglt BgL_auxz00_4372;

								{
									obj_t BgL_auxz00_4373;

									{	/* Globalize/free.scm 423 */
										BgL_objectz00_bglt BgL_tmpz00_4374;

										BgL_tmpz00_4374 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_closurez00_1868));
										BgL_auxz00_4373 = BGL_OBJECT_WIDENING(BgL_tmpz00_4374);
									}
									BgL_auxz00_4372 =
										((BgL_localzf2ginfozf2_bglt) BgL_auxz00_4373);
								}
								((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4372))->
										BgL_escapezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							{
								BgL_localzf2ginfozf2_bglt BgL_auxz00_4380;

								{
									obj_t BgL_auxz00_4381;

									{	/* Globalize/free.scm 423 */
										BgL_objectz00_bglt BgL_tmpz00_4382;

										BgL_tmpz00_4382 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_closurez00_1868));
										BgL_auxz00_4381 = BGL_OBJECT_WIDENING(BgL_tmpz00_4382);
									}
									BgL_auxz00_4380 =
										((BgL_localzf2ginfozf2_bglt) BgL_auxz00_4381);
								}
								((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4380))->
										BgL_globaliza7edzf3z54) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							((BgL_localz00_bglt) BgL_closurez00_1868);
							((((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt)
												((BgL_sfunz00_bglt) BgL_infoz00_1865))))->
									BgL_thezd2closurezd2) =
								((obj_t) ((obj_t) BgL_closurez00_1868)), BUNSPEC);
							return ((obj_t) BgL_closurez00_1868);
						}
				}
			}
		}

	}



/* free-from */
	BGL_EXPORTED_DEF obj_t BGl_freezd2fromzd2zzglobaliza7e_freeza7(obj_t
		BgL_setsz00_61, BgL_localz00_bglt BgL_integratorz00_62)
	{
		{	/* Globalize/free.scm 430 */
			BGl_za2roundza2z00zzglobaliza7e_freeza7 =
				(BGl_za2roundza2z00zzglobaliza7e_freeza7 + 1L);
			{	/* Globalize/free.scm 433 */
				BgL_valuez00_bglt BgL_finfoz00_1882;

				BgL_finfoz00_1882 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_integratorz00_62)))->BgL_valuez00);
				{	/* Globalize/free.scm 440 */
					obj_t BgL_g1295z00_1883;

					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4397;

						{
							obj_t BgL_auxz00_4398;

							{	/* Globalize/free.scm 440 */
								BgL_objectz00_bglt BgL_tmpz00_4399;

								BgL_tmpz00_4399 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_finfoz00_1882));
								BgL_auxz00_4398 = BGL_OBJECT_WIDENING(BgL_tmpz00_4399);
							}
							BgL_auxz00_4397 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4398);
						}
						BgL_g1295z00_1883 =
							(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4397))->
							BgL_boundz00);
					}
					{
						obj_t BgL_l1293z00_1885;

						BgL_l1293z00_1885 = BgL_g1295z00_1883;
					BgL_zc3z04anonymousza31655ze3z87_1886:
						if (PAIRP(BgL_l1293z00_1885))
							{	/* Globalize/free.scm 440 */
								{	/* Globalize/free.scm 440 */
									obj_t BgL_arg1661z00_1888;

									BgL_arg1661z00_1888 = CAR(BgL_l1293z00_1885);
									BGl_markzd2variablez12zc0zzglobaliza7e_freeza7(
										((BgL_localz00_bglt) BgL_arg1661z00_1888));
								}
								{
									obj_t BgL_l1293z00_4410;

									BgL_l1293z00_4410 = CDR(BgL_l1293z00_1885);
									BgL_l1293z00_1885 = BgL_l1293z00_4410;
									goto BgL_zc3z04anonymousza31655ze3z87_1886;
								}
							}
						else
							{	/* Globalize/free.scm 440 */
								((bool_t) 1);
							}
					}
				}
			}
			if (NULLP(BgL_setsz00_61))
				{	/* Globalize/free.scm 442 */
					return BNIL;
				}
			else
				{	/* Globalize/free.scm 442 */
					obj_t BgL_head1298z00_1893;

					BgL_head1298z00_1893 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1296z00_1895;
						obj_t BgL_tail1299z00_1896;

						BgL_l1296z00_1895 = BgL_setsz00_61;
						BgL_tail1299z00_1896 = BgL_head1298z00_1893;
					BgL_zc3z04anonymousza31665ze3z87_1897:
						if (NULLP(BgL_l1296z00_1895))
							{	/* Globalize/free.scm 442 */
								return CDR(BgL_head1298z00_1893);
							}
						else
							{	/* Globalize/free.scm 442 */
								obj_t BgL_newtail1300z00_1899;

								{	/* Globalize/free.scm 442 */
									obj_t BgL_arg1678z00_1901;

									{	/* Globalize/free.scm 442 */
										obj_t BgL_setz00_1902;

										BgL_setz00_1902 = CAR(((obj_t) BgL_l1296z00_1895));
										{
											obj_t BgL_setz00_1905;
											obj_t BgL_resz00_1906;

											BgL_setz00_1905 = BgL_setz00_1902;
											BgL_resz00_1906 = BNIL;
										BgL_zc3z04anonymousza31679ze3z87_1907:
											if (NULLP(BgL_setz00_1905))
												{	/* Globalize/free.scm 446 */
													BgL_arg1678z00_1901 = BgL_resz00_1906;
												}
											else
												{	/* Globalize/free.scm 448 */
													bool_t BgL_test2114z00_4422;

													{	/* Globalize/free.scm 448 */
														obj_t BgL_arg1701z00_1915;

														BgL_arg1701z00_1915 =
															CAR(((obj_t) BgL_setz00_1905));
														BgL_test2114z00_4422 =
															CBOOL
															(BGl_freezd2variablezf3z21zzglobaliza7e_freeza7
															(BgL_arg1701z00_1915));
													}
													if (BgL_test2114z00_4422)
														{	/* Globalize/free.scm 449 */
															obj_t BgL_arg1691z00_1911;
															obj_t BgL_arg1692z00_1912;

															BgL_arg1691z00_1911 =
																CDR(((obj_t) BgL_setz00_1905));
															{	/* Globalize/free.scm 449 */
																obj_t BgL_arg1699z00_1913;

																BgL_arg1699z00_1913 =
																	CAR(((obj_t) BgL_setz00_1905));
																BgL_arg1692z00_1912 =
																	MAKE_YOUNG_PAIR(BgL_arg1699z00_1913,
																	BgL_resz00_1906);
															}
															{
																obj_t BgL_resz00_4433;
																obj_t BgL_setz00_4432;

																BgL_setz00_4432 = BgL_arg1691z00_1911;
																BgL_resz00_4433 = BgL_arg1692z00_1912;
																BgL_resz00_1906 = BgL_resz00_4433;
																BgL_setz00_1905 = BgL_setz00_4432;
																goto BgL_zc3z04anonymousza31679ze3z87_1907;
															}
														}
													else
														{	/* Globalize/free.scm 451 */
															obj_t BgL_arg1700z00_1914;

															BgL_arg1700z00_1914 =
																CDR(((obj_t) BgL_setz00_1905));
															{
																obj_t BgL_setz00_4436;

																BgL_setz00_4436 = BgL_arg1700z00_1914;
																BgL_setz00_1905 = BgL_setz00_4436;
																goto BgL_zc3z04anonymousza31679ze3z87_1907;
															}
														}
												}
										}
									}
									BgL_newtail1300z00_1899 =
										MAKE_YOUNG_PAIR(BgL_arg1678z00_1901, BNIL);
								}
								SET_CDR(BgL_tail1299z00_1896, BgL_newtail1300z00_1899);
								{	/* Globalize/free.scm 442 */
									obj_t BgL_arg1675z00_1900;

									BgL_arg1675z00_1900 = CDR(((obj_t) BgL_l1296z00_1895));
									{
										obj_t BgL_tail1299z00_4442;
										obj_t BgL_l1296z00_4441;

										BgL_l1296z00_4441 = BgL_arg1675z00_1900;
										BgL_tail1299z00_4442 = BgL_newtail1300z00_1899;
										BgL_tail1299z00_1896 = BgL_tail1299z00_4442;
										BgL_l1296z00_1895 = BgL_l1296z00_4441;
										goto BgL_zc3z04anonymousza31665ze3z87_1897;
									}
								}
							}
					}
				}
		}

	}



/* &free-from */
	obj_t BGl_z62freezd2fromzb0zzglobaliza7e_freeza7(obj_t BgL_envz00_3396,
		obj_t BgL_setsz00_3397, obj_t BgL_integratorz00_3398)
	{
		{	/* Globalize/free.scm 430 */
			return
				BGl_freezd2fromzd2zzglobaliza7e_freeza7(BgL_setsz00_3397,
				((BgL_localz00_bglt) BgL_integratorz00_3398));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_freeza7(void)
	{
		{	/* Globalize/free.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_freeza7(void)
	{
		{	/* Globalize/free.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_proc1982z00zzglobaliza7e_freeza7, BGl_nodez00zzast_nodez00,
				BGl_string1983z00zzglobaliza7e_freeza7);
		}

	}



/* &node-free1301 */
	obj_t BGl_z62nodezd2free1301zb0zzglobaliza7e_freeza7(obj_t BgL_envz00_3400,
		obj_t BgL_nodez00_3401, obj_t BgL_freez00_3402)
	{
		{	/* Globalize/free.scm 123 */
			return BgL_freez00_3402;
		}

	}



/* node-free */
	obj_t BGl_nodezd2freezd2zzglobaliza7e_freeza7(BgL_nodez00_bglt BgL_nodez00_11,
		obj_t BgL_freez00_12)
	{
		{	/* Globalize/free.scm 123 */
			{	/* Globalize/free.scm 123 */
				obj_t BgL_method1302z00_1923;

				{	/* Globalize/free.scm 123 */
					obj_t BgL_res1968z00_2941;

					{	/* Globalize/free.scm 123 */
						long BgL_objzd2classzd2numz00_2912;

						BgL_objzd2classzd2numz00_2912 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_11));
						{	/* Globalize/free.scm 123 */
							obj_t BgL_arg1811z00_2913;

							BgL_arg1811z00_2913 =
								PROCEDURE_REF(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
								(int) (1L));
							{	/* Globalize/free.scm 123 */
								int BgL_offsetz00_2916;

								BgL_offsetz00_2916 = (int) (BgL_objzd2classzd2numz00_2912);
								{	/* Globalize/free.scm 123 */
									long BgL_offsetz00_2917;

									BgL_offsetz00_2917 =
										((long) (BgL_offsetz00_2916) - OBJECT_TYPE);
									{	/* Globalize/free.scm 123 */
										long BgL_modz00_2918;

										BgL_modz00_2918 =
											(BgL_offsetz00_2917 >> (int) ((long) ((int) (4L))));
										{	/* Globalize/free.scm 123 */
											long BgL_restz00_2920;

											BgL_restz00_2920 =
												(BgL_offsetz00_2917 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Globalize/free.scm 123 */

												{	/* Globalize/free.scm 123 */
													obj_t BgL_bucketz00_2922;

													BgL_bucketz00_2922 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2913), BgL_modz00_2918);
													BgL_res1968z00_2941 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2922), BgL_restz00_2920);
					}}}}}}}}
					BgL_method1302z00_1923 = BgL_res1968z00_2941;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1302z00_1923,
					((obj_t) BgL_nodez00_11), BgL_freez00_12);
			}
		}

	}



/* &node-free */
	obj_t BGl_z62nodezd2freezb0zzglobaliza7e_freeza7(obj_t BgL_envz00_3403,
		obj_t BgL_nodez00_3404, obj_t BgL_freez00_3405)
	{
		{	/* Globalize/free.scm 123 */
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				((BgL_nodez00_bglt) BgL_nodez00_3404), BgL_freez00_3405);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_freeza7(void)
	{
		{	/* Globalize/free.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7, BGl_varz00zzast_nodez00,
				BGl_proc1984z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_closurez00zzast_nodez00, BGl_proc1986z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_sequencez00zzast_nodez00, BGl_proc1987z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_syncz00zzast_nodez00, BGl_proc1988z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7, BGl_appz00zzast_nodez00,
				BGl_proc1989z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1990z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_funcallz00zzast_nodez00, BGl_proc1991z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_externz00zzast_nodez00, BGl_proc1992z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_castz00zzast_nodez00, BGl_proc1993z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_setqz00zzast_nodez00, BGl_proc1994z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_conditionalz00zzast_nodez00, BGl_proc1995z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_failz00zzast_nodez00, BGl_proc1996z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_switchz00zzast_nodez00, BGl_proc1997z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1998z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1999z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2000z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc2001z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2002z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2003z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzglobaliza7e_freeza7,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2004z00zzglobaliza7e_freeza7,
				BGl_string1985z00zzglobaliza7e_freeza7);
		}

	}



/* &node-free-box-set!1342 */
	obj_t BGl_z62nodezd2freezd2boxzd2setz121342za2zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3426, obj_t BgL_nodez00_3427, obj_t BgL_freez00_3428)
	{
		{	/* Globalize/free.scm 319 */
			{	/* Globalize/free.scm 321 */
				BgL_varz00_bglt BgL_arg1874z00_3503;
				obj_t BgL_arg1875z00_3504;

				BgL_arg1874z00_3503 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3427)))->BgL_varz00);
				BgL_arg1875z00_3504 =
					BGl_nodezd2freezd2zzglobaliza7e_freeza7(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3427)))->BgL_valuez00),
					BgL_freez00_3428);
				return BGl_nodezd2freezd2zzglobaliza7e_freeza7(((BgL_nodez00_bglt)
						BgL_arg1874z00_3503), BgL_arg1875z00_3504);
			}
		}

	}



/* &node-free-box-ref1340 */
	obj_t BGl_z62nodezd2freezd2boxzd2ref1340zb0zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3429, obj_t BgL_nodez00_3430, obj_t BgL_freez00_3431)
	{
		{	/* Globalize/free.scm 312 */
			{	/* Globalize/free.scm 314 */
				BgL_varz00_bglt BgL_arg1873z00_3506;

				BgL_arg1873z00_3506 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_3430)))->BgL_varz00);
				return
					BGl_nodezd2freezd2zzglobaliza7e_freeza7(
					((BgL_nodez00_bglt) BgL_arg1873z00_3506), BgL_freez00_3431);
			}
		}

	}



/* &node-free-make-box1338 */
	obj_t BGl_z62nodezd2freezd2makezd2box1338zb0zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3432, obj_t BgL_nodez00_3433, obj_t BgL_freez00_3434)
	{
		{	/* Globalize/free.scm 305 */
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_3433)))->BgL_valuez00),
				BgL_freez00_3434);
		}

	}



/* &node-free-jump-ex-it1336 */
	obj_t BGl_z62nodezd2freezd2jumpzd2exzd2it1336z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3435, obj_t BgL_nodez00_3436, obj_t BgL_freez00_3437)
	{
		{	/* Globalize/free.scm 298 */
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3436)))->BgL_exitz00),
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3436)))->
						BgL_valuez00), BgL_freez00_3437));
		}

	}



/* &node-free-set-ex-it1334 */
	obj_t BGl_z62nodezd2freezd2setzd2exzd2it1334z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3438, obj_t BgL_nodez00_3439, obj_t BgL_freez00_3440)
	{
		{	/* Globalize/free.scm 290 */
			{	/* Globalize/free.scm 292 */
				BgL_variablez00_bglt BgL_arg1860z00_3510;

				BgL_arg1860z00_3510 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3439)))->
								BgL_varz00)))->BgL_variablez00);
				BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(((BgL_localz00_bglt)
						BgL_arg1860z00_3510),
					((BgL_localz00_bglt) BGl_za2integratorza2z00zzglobaliza7e_freeza7));
			}
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3439)))->BgL_bodyz00),
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3439)))->
						BgL_onexitz00), BgL_freez00_3440));
		}

	}



/* &node-free-let-var1332 */
	obj_t BGl_z62nodezd2freezd2letzd2var1332zb0zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3441, obj_t BgL_nodez00_3442, obj_t BgL_freez00_3443)
	{
		{	/* Globalize/free.scm 276 */
			{
				obj_t BgL_bindingsz00_3513;
				obj_t BgL_freez00_3514;

				BgL_bindingsz00_3513 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_3442)))->BgL_bindingsz00);
				BgL_freez00_3514 = BgL_freez00_3443;
			BgL_loopz00_3512:
				if (NULLP(BgL_bindingsz00_3513))
					{	/* Globalize/free.scm 280 */
						return
							BGl_nodezd2freezd2zzglobaliza7e_freeza7(
							(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_3442)))->BgL_bodyz00),
							BgL_freez00_3514);
					}
				else
					{	/* Globalize/free.scm 280 */
						{	/* Globalize/free.scm 283 */
							obj_t BgL_arg1853z00_3515;

							{	/* Globalize/free.scm 283 */
								obj_t BgL_pairz00_3516;

								BgL_pairz00_3516 = CAR(((obj_t) BgL_bindingsz00_3513));
								BgL_arg1853z00_3515 = CAR(BgL_pairz00_3516);
							}
							BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(
								((BgL_localz00_bglt) BgL_arg1853z00_3515),
								((BgL_localz00_bglt)
									BGl_za2integratorza2z00zzglobaliza7e_freeza7));
						}
						{	/* Globalize/free.scm 284 */
							obj_t BgL_arg1856z00_3517;
							obj_t BgL_arg1857z00_3518;

							BgL_arg1856z00_3517 = CDR(((obj_t) BgL_bindingsz00_3513));
							{	/* Globalize/free.scm 285 */
								obj_t BgL_arg1858z00_3519;

								{	/* Globalize/free.scm 285 */
									obj_t BgL_pairz00_3520;

									BgL_pairz00_3520 = CAR(((obj_t) BgL_bindingsz00_3513));
									BgL_arg1858z00_3519 = CDR(BgL_pairz00_3520);
								}
								BgL_arg1857z00_3518 =
									BGl_nodezd2freezd2zzglobaliza7e_freeza7(
									((BgL_nodez00_bglt) BgL_arg1858z00_3519), BgL_freez00_3514);
							}
							{
								obj_t BgL_freez00_4550;
								obj_t BgL_bindingsz00_4549;

								BgL_bindingsz00_4549 = BgL_arg1856z00_3517;
								BgL_freez00_4550 = BgL_arg1857z00_3518;
								BgL_freez00_3514 = BgL_freez00_4550;
								BgL_bindingsz00_3513 = BgL_bindingsz00_4549;
								goto BgL_loopz00_3512;
							}
						}
					}
			}
		}

	}



/* &node-free-let-fun1330 */
	obj_t BGl_z62nodezd2freezd2letzd2fun1330zb0zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3444, obj_t BgL_nodez00_3445, obj_t BgL_freez00_3446)
	{
		{	/* Globalize/free.scm 251 */
			{	/* Globalize/free.scm 253 */
				obj_t BgL_g1289z00_3522;

				BgL_g1289z00_3522 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_3445)))->BgL_localsz00);
				{
					obj_t BgL_l1287z00_3524;

					BgL_l1287z00_3524 = BgL_g1289z00_3522;
				BgL_zc3z04anonymousza31832ze3z87_3523:
					if (PAIRP(BgL_l1287z00_3524))
						{	/* Globalize/free.scm 253 */
							{	/* Globalize/free.scm 255 */
								obj_t BgL_fz00_3525;

								BgL_fz00_3525 = CAR(BgL_l1287z00_3524);
								BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(
									((BgL_localz00_bglt) BgL_fz00_3525),
									((BgL_localz00_bglt)
										BGl_za2integratorza2z00zzglobaliza7e_freeza7));
								{	/* Globalize/free.scm 257 */
									bool_t BgL_test2117z00_4561;

									{
										BgL_localzf2ginfozf2_bglt BgL_auxz00_4562;

										{
											obj_t BgL_auxz00_4563;

											{	/* Globalize/free.scm 257 */
												BgL_objectz00_bglt BgL_tmpz00_4564;

												BgL_tmpz00_4564 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt) BgL_fz00_3525));
												BgL_auxz00_4563 = BGL_OBJECT_WIDENING(BgL_tmpz00_4564);
											}
											BgL_auxz00_4562 =
												((BgL_localzf2ginfozf2_bglt) BgL_auxz00_4563);
										}
										BgL_test2117z00_4561 =
											(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4562))->
											BgL_escapezf3zf3);
									}
									if (BgL_test2117z00_4561)
										{	/* Globalize/free.scm 258 */
											obj_t BgL_arg1835z00_3526;

											{	/* Globalize/free.scm 258 */
												obj_t BgL_arg1836z00_3527;

												BgL_arg1836z00_3527 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_letzd2funzd2_bglt) BgL_nodez00_3445))))->
													BgL_locz00);
												BgL_arg1835z00_3526 =
													BGl_thezd2localzd2closurez00zzglobaliza7e_freeza7
													(BgL_fz00_3525, BgL_arg1836z00_3527);
											}
											BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(
												((BgL_localz00_bglt) BgL_arg1835z00_3526),
												((BgL_localz00_bglt)
													BGl_za2integratorza2z00zzglobaliza7e_freeza7));
										}
									else
										{	/* Globalize/free.scm 257 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1287z00_4577;

								BgL_l1287z00_4577 = CDR(BgL_l1287z00_3524);
								BgL_l1287z00_3524 = BgL_l1287z00_4577;
								goto BgL_zc3z04anonymousza31832ze3z87_3523;
							}
						}
					else
						{	/* Globalize/free.scm 253 */
							((bool_t) 1);
						}
				}
			}
			{
				obj_t BgL_lclsz00_3529;
				obj_t BgL_freez00_3530;

				BgL_lclsz00_3529 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_3445)))->BgL_localsz00);
				BgL_freez00_3530 = BgL_freez00_3446;
			BgL_liipz00_3528:
				if (NULLP(BgL_lclsz00_3529))
					{	/* Globalize/free.scm 263 */
						return
							BGl_nodezd2freezd2zzglobaliza7e_freeza7(
							(((BgL_letzd2funzd2_bglt) COBJECT(
										((BgL_letzd2funzd2_bglt) BgL_nodez00_3445)))->BgL_bodyz00),
							BgL_freez00_3530);
					}
				else
					{	/* Globalize/free.scm 265 */
						obj_t BgL_localz00_3531;

						BgL_localz00_3531 = CAR(((obj_t) BgL_lclsz00_3529));
						{	/* Globalize/free.scm 265 */
							BgL_valuez00_bglt BgL_funz00_3532;

							BgL_funz00_3532 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_localz00_bglt) BgL_localz00_3531))))->BgL_valuez00);
							{	/* Globalize/free.scm 266 */

								{	/* Globalize/free.scm 267 */
									obj_t BgL_g1292z00_3533;

									BgL_g1292z00_3533 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_3532)))->BgL_argsz00);
									{
										obj_t BgL_l1290z00_3535;

										BgL_l1290z00_3535 = BgL_g1292z00_3533;
									BgL_zc3z04anonymousza31843ze3z87_3534:
										if (PAIRP(BgL_l1290z00_3535))
											{	/* Globalize/free.scm 269 */
												{	/* Globalize/free.scm 268 */
													obj_t BgL_lz00_3536;

													BgL_lz00_3536 = CAR(BgL_l1290z00_3535);
													BGl_bindzd2variablez12zc0zzglobaliza7e_freeza7(
														((BgL_localz00_bglt) BgL_lz00_3536),
														((BgL_localz00_bglt)
															BGl_za2integratorza2z00zzglobaliza7e_freeza7));
												}
												{
													obj_t BgL_l1290z00_4597;

													BgL_l1290z00_4597 = CDR(BgL_l1290z00_3535);
													BgL_l1290z00_3535 = BgL_l1290z00_4597;
													goto BgL_zc3z04anonymousza31843ze3z87_3534;
												}
											}
										else
											{	/* Globalize/free.scm 269 */
												((bool_t) 1);
											}
									}
								}
								{	/* Globalize/free.scm 270 */
									obj_t BgL_arg1846z00_3537;
									obj_t BgL_arg1847z00_3538;

									BgL_arg1846z00_3537 = CDR(((obj_t) BgL_lclsz00_3529));
									{	/* Globalize/free.scm 271 */
										obj_t BgL_arg1848z00_3539;

										BgL_arg1848z00_3539 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_3532)))->BgL_bodyz00);
										BgL_arg1847z00_3538 =
											BGl_nodezd2freezd2zzglobaliza7e_freeza7(
											((BgL_nodez00_bglt) BgL_arg1848z00_3539),
											BgL_freez00_3530);
									}
									{
										obj_t BgL_freez00_4606;
										obj_t BgL_lclsz00_4605;

										BgL_lclsz00_4605 = BgL_arg1846z00_3537;
										BgL_freez00_4606 = BgL_arg1847z00_3538;
										BgL_freez00_3530 = BgL_freez00_4606;
										BgL_lclsz00_3529 = BgL_lclsz00_4605;
										goto BgL_liipz00_3528;
									}
								}
							}
						}
					}
			}
		}

	}



/* &node-free-switch1328 */
	obj_t BGl_z62nodezd2freezd2switch1328z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3447, obj_t BgL_nodez00_3448, obj_t BgL_freez00_3449)
	{
		{	/* Globalize/free.scm 240 */
			{
				obj_t BgL_clausesz00_3542;
				obj_t BgL_freez00_3543;

				BgL_clausesz00_3542 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_3448)))->BgL_clausesz00);
				BgL_freez00_3543 = BgL_freez00_3449;
			BgL_loopz00_3541:
				if (NULLP(BgL_clausesz00_3542))
					{	/* Globalize/free.scm 244 */
						return
							BGl_nodezd2freezd2zzglobaliza7e_freeza7(
							(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_3448)))->BgL_testz00),
							BgL_freez00_3543);
					}
				else
					{	/* Globalize/free.scm 246 */
						obj_t BgL_arg1820z00_3544;
						obj_t BgL_arg1822z00_3545;

						BgL_arg1820z00_3544 = CDR(((obj_t) BgL_clausesz00_3542));
						{	/* Globalize/free.scm 246 */
							obj_t BgL_arg1823z00_3546;

							{	/* Globalize/free.scm 246 */
								obj_t BgL_pairz00_3547;

								BgL_pairz00_3547 = CAR(((obj_t) BgL_clausesz00_3542));
								BgL_arg1823z00_3546 = CDR(BgL_pairz00_3547);
							}
							BgL_arg1822z00_3545 =
								BGl_nodezd2freezd2zzglobaliza7e_freeza7(
								((BgL_nodez00_bglt) BgL_arg1823z00_3546), BgL_freez00_3543);
						}
						{
							obj_t BgL_freez00_4622;
							obj_t BgL_clausesz00_4621;

							BgL_clausesz00_4621 = BgL_arg1820z00_3544;
							BgL_freez00_4622 = BgL_arg1822z00_3545;
							BgL_freez00_3543 = BgL_freez00_4622;
							BgL_clausesz00_3542 = BgL_clausesz00_4621;
							goto BgL_loopz00_3541;
						}
					}
			}
		}

	}



/* &node-free-fail1326 */
	obj_t BGl_z62nodezd2freezd2fail1326z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3450, obj_t BgL_nodez00_3451, obj_t BgL_freez00_3452)
	{
		{	/* Globalize/free.scm 233 */
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_3451)))->BgL_procz00),
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3451)))->BgL_msgz00),
					BGl_nodezd2freezd2zzglobaliza7e_freeza7(
						(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3451)))->BgL_objz00),
						BgL_freez00_3452)));
		}

	}



/* &node-free-conditiona1324 */
	obj_t BGl_z62nodezd2freezd2conditiona1324z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3453, obj_t BgL_nodez00_3454, obj_t BgL_freez00_3455)
	{
		{	/* Globalize/free.scm 226 */
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_3454)))->BgL_testz00),
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3454)))->BgL_truez00),
					BGl_nodezd2freezd2zzglobaliza7e_freeza7(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3454)))->BgL_falsez00),
						BgL_freez00_3455)));
		}

	}



/* &node-free-setq1322 */
	obj_t BGl_z62nodezd2freezd2setq1322z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3456, obj_t BgL_nodez00_3457, obj_t BgL_freez00_3458)
	{
		{	/* Globalize/free.scm 219 */
			{	/* Globalize/free.scm 221 */
				BgL_varz00_bglt BgL_arg1755z00_3551;
				obj_t BgL_arg1761z00_3552;

				BgL_arg1755z00_3551 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_3457)))->BgL_varz00);
				BgL_arg1761z00_3552 =
					BGl_nodezd2freezd2zzglobaliza7e_freeza7(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_3457)))->BgL_valuez00),
					BgL_freez00_3458);
				return BGl_nodezd2freezd2zzglobaliza7e_freeza7(((BgL_nodez00_bglt)
						BgL_arg1755z00_3551), BgL_arg1761z00_3552);
			}
		}

	}



/* &node-free-cast1320 */
	obj_t BGl_z62nodezd2freezd2cast1320z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3459, obj_t BgL_nodez00_3460, obj_t BgL_freez00_3461)
	{
		{	/* Globalize/free.scm 212 */
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_3460)))->BgL_argz00),
				BgL_freez00_3461);
		}

	}



/* &node-free-extern1318 */
	obj_t BGl_z62nodezd2freezd2extern1318z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3462, obj_t BgL_nodez00_3463, obj_t BgL_freez00_3464)
	{
		{	/* Globalize/free.scm 205 */
			return
				BGl_nodezd2freeza2z70zzglobaliza7e_freeza7(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_3463)))->BgL_exprza2za2),
				BgL_freez00_3464);
		}

	}



/* &node-free-funcall1316 */
	obj_t BGl_z62nodezd2freezd2funcall1316z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3465, obj_t BgL_nodez00_3466, obj_t BgL_freez00_3467)
	{
		{	/* Globalize/free.scm 198 */
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_3466)))->BgL_funz00),
				BGl_nodezd2freeza2z70zzglobaliza7e_freeza7(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3466)))->BgL_argsz00),
					BgL_freez00_3467));
		}

	}



/* &node-free-app-ly1314 */
	obj_t BGl_z62nodezd2freezd2appzd2ly1314zb0zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3468, obj_t BgL_nodez00_3469, obj_t BgL_freez00_3470)
	{
		{	/* Globalize/free.scm 191 */
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_3469)))->BgL_funz00),
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3469)))->BgL_argz00),
					BgL_freez00_3470));
		}

	}



/* &node-free-app1312 */
	obj_t BGl_z62nodezd2freezd2app1312z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3471, obj_t BgL_nodez00_3472, obj_t BgL_freez00_3473)
	{
		{	/* Globalize/free.scm 175 */
			{	/* Globalize/free.scm 177 */
				obj_t BgL_freez00_3558;

				{	/* Globalize/free.scm 177 */
					BgL_variablez00_bglt BgL_varz00_3559;

					BgL_varz00_3559 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_3472)))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Globalize/free.scm 179 */
						bool_t BgL_test2121z00_4671;

						{	/* Globalize/free.scm 179 */
							obj_t BgL_classz00_3560;

							BgL_classz00_3560 = BGl_globalz00zzast_varz00;
							{	/* Globalize/free.scm 179 */
								BgL_objectz00_bglt BgL_arg1807z00_3561;

								{	/* Globalize/free.scm 179 */
									obj_t BgL_tmpz00_4672;

									BgL_tmpz00_4672 =
										((obj_t) ((BgL_objectz00_bglt) BgL_varz00_3559));
									BgL_arg1807z00_3561 = (BgL_objectz00_bglt) (BgL_tmpz00_4672);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Globalize/free.scm 179 */
										long BgL_idxz00_3562;

										BgL_idxz00_3562 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3561);
										BgL_test2121z00_4671 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3562 + 2L)) == BgL_classz00_3560);
									}
								else
									{	/* Globalize/free.scm 179 */
										bool_t BgL_res1972z00_3565;

										{	/* Globalize/free.scm 179 */
											obj_t BgL_oclassz00_3566;

											{	/* Globalize/free.scm 179 */
												obj_t BgL_arg1815z00_3567;
												long BgL_arg1816z00_3568;

												BgL_arg1815z00_3567 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Globalize/free.scm 179 */
													long BgL_arg1817z00_3569;

													BgL_arg1817z00_3569 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3561);
													BgL_arg1816z00_3568 =
														(BgL_arg1817z00_3569 - OBJECT_TYPE);
												}
												BgL_oclassz00_3566 =
													VECTOR_REF(BgL_arg1815z00_3567, BgL_arg1816z00_3568);
											}
											{	/* Globalize/free.scm 179 */
												bool_t BgL__ortest_1115z00_3570;

												BgL__ortest_1115z00_3570 =
													(BgL_classz00_3560 == BgL_oclassz00_3566);
												if (BgL__ortest_1115z00_3570)
													{	/* Globalize/free.scm 179 */
														BgL_res1972z00_3565 = BgL__ortest_1115z00_3570;
													}
												else
													{	/* Globalize/free.scm 179 */
														long BgL_odepthz00_3571;

														{	/* Globalize/free.scm 179 */
															obj_t BgL_arg1804z00_3572;

															BgL_arg1804z00_3572 = (BgL_oclassz00_3566);
															BgL_odepthz00_3571 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3572);
														}
														if ((2L < BgL_odepthz00_3571))
															{	/* Globalize/free.scm 179 */
																obj_t BgL_arg1802z00_3573;

																{	/* Globalize/free.scm 179 */
																	obj_t BgL_arg1803z00_3574;

																	BgL_arg1803z00_3574 = (BgL_oclassz00_3566);
																	BgL_arg1802z00_3573 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3574,
																		2L);
																}
																BgL_res1972z00_3565 =
																	(BgL_arg1802z00_3573 == BgL_classz00_3560);
															}
														else
															{	/* Globalize/free.scm 179 */
																BgL_res1972z00_3565 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2121z00_4671 = BgL_res1972z00_3565;
									}
							}
						}
						if (BgL_test2121z00_4671)
							{	/* Globalize/free.scm 179 */
								BgL_freez00_3558 = BgL_freez00_3473;
							}
						else
							{	/* Globalize/free.scm 181 */
								bool_t BgL_test2125z00_4695;

								{	/* Globalize/free.scm 181 */
									bool_t BgL_test2126z00_4696;

									{
										BgL_localzf2ginfozf2_bglt BgL_auxz00_4697;

										{
											obj_t BgL_auxz00_4698;

											{	/* Globalize/free.scm 181 */
												BgL_objectz00_bglt BgL_tmpz00_4699;

												BgL_tmpz00_4699 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt) BgL_varz00_3559));
												BgL_auxz00_4698 = BGL_OBJECT_WIDENING(BgL_tmpz00_4699);
											}
											BgL_auxz00_4697 =
												((BgL_localzf2ginfozf2_bglt) BgL_auxz00_4698);
										}
										BgL_test2126z00_4696 =
											(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4697))->
											BgL_escapezf3zf3);
									}
									if (BgL_test2126z00_4696)
										{	/* Globalize/free.scm 181 */
											BgL_test2125z00_4695 =
												CBOOL(BGl_freezd2variablezf3z21zzglobaliza7e_freeza7(
													((obj_t) BgL_varz00_3559)));
										}
									else
										{	/* Globalize/free.scm 181 */
											BgL_test2125z00_4695 = ((bool_t) 0);
										}
								}
								if (BgL_test2125z00_4695)
									{	/* Globalize/free.scm 181 */
										BGl_markzd2variablez12zc0zzglobaliza7e_freeza7(
											((BgL_localz00_bglt) BgL_varz00_3559));
										{	/* Globalize/free.scm 183 */
											obj_t BgL_arg1739z00_3575;

											{	/* Globalize/free.scm 183 */
												obj_t BgL_arg1740z00_3576;

												BgL_arg1740z00_3576 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_3472))))->
													BgL_locz00);
												{	/* Globalize/free.scm 338 */
													bool_t BgL_test2127z00_4713;

													{	/* Globalize/free.scm 338 */
														obj_t BgL_classz00_3577;

														BgL_classz00_3577 = BGl_globalz00zzast_varz00;
														{	/* Globalize/free.scm 338 */
															BgL_objectz00_bglt BgL_arg1807z00_3578;

															{	/* Globalize/free.scm 338 */
																obj_t BgL_tmpz00_4714;

																BgL_tmpz00_4714 = ((obj_t) BgL_varz00_3559);
																BgL_arg1807z00_3578 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4714);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Globalize/free.scm 338 */
																	long BgL_idxz00_3579;

																	BgL_idxz00_3579 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3578);
																	BgL_test2127z00_4713 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3579 + 2L)) ==
																		BgL_classz00_3577);
																}
															else
																{	/* Globalize/free.scm 338 */
																	bool_t BgL_res1973z00_3582;

																	{	/* Globalize/free.scm 338 */
																		obj_t BgL_oclassz00_3583;

																		{	/* Globalize/free.scm 338 */
																			obj_t BgL_arg1815z00_3584;
																			long BgL_arg1816z00_3585;

																			BgL_arg1815z00_3584 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Globalize/free.scm 338 */
																				long BgL_arg1817z00_3586;

																				BgL_arg1817z00_3586 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3578);
																				BgL_arg1816z00_3585 =
																					(BgL_arg1817z00_3586 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3583 =
																				VECTOR_REF(BgL_arg1815z00_3584,
																				BgL_arg1816z00_3585);
																		}
																		{	/* Globalize/free.scm 338 */
																			bool_t BgL__ortest_1115z00_3587;

																			BgL__ortest_1115z00_3587 =
																				(BgL_classz00_3577 ==
																				BgL_oclassz00_3583);
																			if (BgL__ortest_1115z00_3587)
																				{	/* Globalize/free.scm 338 */
																					BgL_res1973z00_3582 =
																						BgL__ortest_1115z00_3587;
																				}
																			else
																				{	/* Globalize/free.scm 338 */
																					long BgL_odepthz00_3588;

																					{	/* Globalize/free.scm 338 */
																						obj_t BgL_arg1804z00_3589;

																						BgL_arg1804z00_3589 =
																							(BgL_oclassz00_3583);
																						BgL_odepthz00_3588 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3589);
																					}
																					if ((2L < BgL_odepthz00_3588))
																						{	/* Globalize/free.scm 338 */
																							obj_t BgL_arg1802z00_3590;

																							{	/* Globalize/free.scm 338 */
																								obj_t BgL_arg1803z00_3591;

																								BgL_arg1803z00_3591 =
																									(BgL_oclassz00_3583);
																								BgL_arg1802z00_3590 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3591, 2L);
																							}
																							BgL_res1973z00_3582 =
																								(BgL_arg1802z00_3590 ==
																								BgL_classz00_3577);
																						}
																					else
																						{	/* Globalize/free.scm 338 */
																							BgL_res1973z00_3582 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2127z00_4713 = BgL_res1973z00_3582;
																}
														}
													}
													if (BgL_test2127z00_4713)
														{	/* Globalize/free.scm 338 */
															BgL_arg1739z00_3575 =
																BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7
																(((BgL_globalz00_bglt) BgL_varz00_3559),
																BgL_arg1740z00_3576);
														}
													else
														{	/* Globalize/free.scm 338 */
															BgL_arg1739z00_3575 =
																BGl_thezd2localzd2closurez00zzglobaliza7e_freeza7
																(((obj_t) BgL_varz00_3559),
																BgL_arg1740z00_3576);
														}
												}
											}
											BgL_freez00_3558 =
												MAKE_YOUNG_PAIR(BgL_arg1739z00_3575, BgL_freez00_3473);
										}
									}
								else
									{	/* Globalize/free.scm 181 */
										BgL_freez00_3558 = BgL_freez00_3473;
									}
							}
					}
				}
				return
					BGl_nodezd2freeza2z70zzglobaliza7e_freeza7(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_3472)))->BgL_argsz00),
					BgL_freez00_3558);
			}
		}

	}



/* &node-free-sync1310 */
	obj_t BGl_z62nodezd2freezd2sync1310z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3474, obj_t BgL_nodez00_3475, obj_t BgL_freez00_3476)
	{
		{	/* Globalize/free.scm 168 */
			return
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_3475)))->BgL_bodyz00),
				BGl_nodezd2freezd2zzglobaliza7e_freeza7(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3475)))->BgL_prelockz00),
					BGl_nodezd2freezd2zzglobaliza7e_freeza7(
						(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3475)))->BgL_mutexz00),
						BgL_freez00_3476)));
		}

	}



/* &node-free-sequence1308 */
	obj_t BGl_z62nodezd2freezd2sequence1308z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3477, obj_t BgL_nodez00_3478, obj_t BgL_freez00_3479)
	{
		{	/* Globalize/free.scm 161 */
			return
				BGl_nodezd2freeza2z70zzglobaliza7e_freeza7(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_3478)))->BgL_nodesz00),
				BgL_freez00_3479);
		}

	}



/* &node-free-closure1306 */
	obj_t BGl_z62nodezd2freezd2closure1306z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3480, obj_t BgL_nodez00_3481, obj_t BgL_freez00_3482)
	{
		{	/* Globalize/free.scm 146 */
			{	/* Globalize/free.scm 148 */
				obj_t BgL_varz00_3595;

				{	/* Globalize/free.scm 148 */
					BgL_variablez00_bglt BgL_arg1717z00_3596;

					BgL_arg1717z00_3596 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt)
									((BgL_closurez00_bglt) BgL_nodez00_3481))))->BgL_variablez00);
					{	/* Globalize/free.scm 338 */
						bool_t BgL_test2131z00_4759;

						{	/* Globalize/free.scm 338 */
							obj_t BgL_classz00_3597;

							BgL_classz00_3597 = BGl_globalz00zzast_varz00;
							{	/* Globalize/free.scm 338 */
								BgL_objectz00_bglt BgL_arg1807z00_3598;

								{	/* Globalize/free.scm 338 */
									obj_t BgL_tmpz00_4760;

									BgL_tmpz00_4760 = ((obj_t) BgL_arg1717z00_3596);
									BgL_arg1807z00_3598 = (BgL_objectz00_bglt) (BgL_tmpz00_4760);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Globalize/free.scm 338 */
										long BgL_idxz00_3599;

										BgL_idxz00_3599 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3598);
										BgL_test2131z00_4759 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3599 + 2L)) == BgL_classz00_3597);
									}
								else
									{	/* Globalize/free.scm 338 */
										bool_t BgL_res1970z00_3602;

										{	/* Globalize/free.scm 338 */
											obj_t BgL_oclassz00_3603;

											{	/* Globalize/free.scm 338 */
												obj_t BgL_arg1815z00_3604;
												long BgL_arg1816z00_3605;

												BgL_arg1815z00_3604 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Globalize/free.scm 338 */
													long BgL_arg1817z00_3606;

													BgL_arg1817z00_3606 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3598);
													BgL_arg1816z00_3605 =
														(BgL_arg1817z00_3606 - OBJECT_TYPE);
												}
												BgL_oclassz00_3603 =
													VECTOR_REF(BgL_arg1815z00_3604, BgL_arg1816z00_3605);
											}
											{	/* Globalize/free.scm 338 */
												bool_t BgL__ortest_1115z00_3607;

												BgL__ortest_1115z00_3607 =
													(BgL_classz00_3597 == BgL_oclassz00_3603);
												if (BgL__ortest_1115z00_3607)
													{	/* Globalize/free.scm 338 */
														BgL_res1970z00_3602 = BgL__ortest_1115z00_3607;
													}
												else
													{	/* Globalize/free.scm 338 */
														long BgL_odepthz00_3608;

														{	/* Globalize/free.scm 338 */
															obj_t BgL_arg1804z00_3609;

															BgL_arg1804z00_3609 = (BgL_oclassz00_3603);
															BgL_odepthz00_3608 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3609);
														}
														if ((2L < BgL_odepthz00_3608))
															{	/* Globalize/free.scm 338 */
																obj_t BgL_arg1802z00_3610;

																{	/* Globalize/free.scm 338 */
																	obj_t BgL_arg1803z00_3611;

																	BgL_arg1803z00_3611 = (BgL_oclassz00_3603);
																	BgL_arg1802z00_3610 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3611,
																		2L);
																}
																BgL_res1970z00_3602 =
																	(BgL_arg1802z00_3610 == BgL_classz00_3597);
															}
														else
															{	/* Globalize/free.scm 338 */
																BgL_res1970z00_3602 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2131z00_4759 = BgL_res1970z00_3602;
									}
							}
						}
						if (BgL_test2131z00_4759)
							{	/* Globalize/free.scm 338 */
								BgL_varz00_3595 =
									BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7(
									((BgL_globalz00_bglt) BgL_arg1717z00_3596), BFALSE);
							}
						else
							{	/* Globalize/free.scm 338 */
								BgL_varz00_3595 =
									BGl_thezd2localzd2closurez00zzglobaliza7e_freeza7(
									((obj_t) BgL_arg1717z00_3596), BFALSE);
							}
					}
				}
				{	/* Globalize/free.scm 150 */
					bool_t BgL_test2135z00_4786;

					{	/* Globalize/free.scm 150 */
						obj_t BgL_classz00_3612;

						BgL_classz00_3612 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_varz00_3595))
							{	/* Globalize/free.scm 150 */
								BgL_objectz00_bglt BgL_arg1807z00_3613;

								BgL_arg1807z00_3613 = (BgL_objectz00_bglt) (BgL_varz00_3595);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Globalize/free.scm 150 */
										long BgL_idxz00_3614;

										BgL_idxz00_3614 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3613);
										BgL_test2135z00_4786 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3614 + 2L)) == BgL_classz00_3612);
									}
								else
									{	/* Globalize/free.scm 150 */
										bool_t BgL_res1971z00_3617;

										{	/* Globalize/free.scm 150 */
											obj_t BgL_oclassz00_3618;

											{	/* Globalize/free.scm 150 */
												obj_t BgL_arg1815z00_3619;
												long BgL_arg1816z00_3620;

												BgL_arg1815z00_3619 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Globalize/free.scm 150 */
													long BgL_arg1817z00_3621;

													BgL_arg1817z00_3621 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3613);
													BgL_arg1816z00_3620 =
														(BgL_arg1817z00_3621 - OBJECT_TYPE);
												}
												BgL_oclassz00_3618 =
													VECTOR_REF(BgL_arg1815z00_3619, BgL_arg1816z00_3620);
											}
											{	/* Globalize/free.scm 150 */
												bool_t BgL__ortest_1115z00_3622;

												BgL__ortest_1115z00_3622 =
													(BgL_classz00_3612 == BgL_oclassz00_3618);
												if (BgL__ortest_1115z00_3622)
													{	/* Globalize/free.scm 150 */
														BgL_res1971z00_3617 = BgL__ortest_1115z00_3622;
													}
												else
													{	/* Globalize/free.scm 150 */
														long BgL_odepthz00_3623;

														{	/* Globalize/free.scm 150 */
															obj_t BgL_arg1804z00_3624;

															BgL_arg1804z00_3624 = (BgL_oclassz00_3618);
															BgL_odepthz00_3623 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3624);
														}
														if ((2L < BgL_odepthz00_3623))
															{	/* Globalize/free.scm 150 */
																obj_t BgL_arg1802z00_3625;

																{	/* Globalize/free.scm 150 */
																	obj_t BgL_arg1803z00_3626;

																	BgL_arg1803z00_3626 = (BgL_oclassz00_3618);
																	BgL_arg1802z00_3625 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3626,
																		2L);
																}
																BgL_res1971z00_3617 =
																	(BgL_arg1802z00_3625 == BgL_classz00_3612);
															}
														else
															{	/* Globalize/free.scm 150 */
																BgL_res1971z00_3617 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2135z00_4786 = BgL_res1971z00_3617;
									}
							}
						else
							{	/* Globalize/free.scm 150 */
								BgL_test2135z00_4786 = ((bool_t) 0);
							}
					}
					if (BgL_test2135z00_4786)
						{	/* Globalize/free.scm 150 */
							return BgL_freez00_3482;
						}
					else
						{	/* Globalize/free.scm 150 */
							if (CBOOL(BGl_freezd2variablezf3z21zzglobaliza7e_freeza7
									(BgL_varz00_3595)))
								{	/* Globalize/free.scm 152 */
									BGl_markzd2variablez12zc0zzglobaliza7e_freeza7(
										((BgL_localz00_bglt) BgL_varz00_3595));
									return MAKE_YOUNG_PAIR(BgL_varz00_3595, BgL_freez00_3482);
								}
							else
								{	/* Globalize/free.scm 152 */
									return BgL_freez00_3482;
								}
						}
				}
			}
		}

	}



/* &node-free-var1304 */
	obj_t BGl_z62nodezd2freezd2var1304z62zzglobaliza7e_freeza7(obj_t
		BgL_envz00_3483, obj_t BgL_nodez00_3484, obj_t BgL_freez00_3485)
	{
		{	/* Globalize/free.scm 129 */
			{	/* Globalize/free.scm 135 */
				bool_t BgL_test2141z00_4815;

				{	/* Globalize/free.scm 135 */
					BgL_variablez00_bglt BgL_arg1711z00_3628;

					BgL_arg1711z00_3628 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_nodez00_3484)))->BgL_variablez00);
					{	/* Globalize/free.scm 135 */
						obj_t BgL_classz00_3629;

						BgL_classz00_3629 = BGl_globalz00zzast_varz00;
						{	/* Globalize/free.scm 135 */
							BgL_objectz00_bglt BgL_arg1807z00_3630;

							{	/* Globalize/free.scm 135 */
								obj_t BgL_tmpz00_4818;

								BgL_tmpz00_4818 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1711z00_3628));
								BgL_arg1807z00_3630 = (BgL_objectz00_bglt) (BgL_tmpz00_4818);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Globalize/free.scm 135 */
									long BgL_idxz00_3631;

									BgL_idxz00_3631 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3630);
									BgL_test2141z00_4815 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3631 + 2L)) == BgL_classz00_3629);
								}
							else
								{	/* Globalize/free.scm 135 */
									bool_t BgL_res1969z00_3634;

									{	/* Globalize/free.scm 135 */
										obj_t BgL_oclassz00_3635;

										{	/* Globalize/free.scm 135 */
											obj_t BgL_arg1815z00_3636;
											long BgL_arg1816z00_3637;

											BgL_arg1815z00_3636 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Globalize/free.scm 135 */
												long BgL_arg1817z00_3638;

												BgL_arg1817z00_3638 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3630);
												BgL_arg1816z00_3637 =
													(BgL_arg1817z00_3638 - OBJECT_TYPE);
											}
											BgL_oclassz00_3635 =
												VECTOR_REF(BgL_arg1815z00_3636, BgL_arg1816z00_3637);
										}
										{	/* Globalize/free.scm 135 */
											bool_t BgL__ortest_1115z00_3639;

											BgL__ortest_1115z00_3639 =
												(BgL_classz00_3629 == BgL_oclassz00_3635);
											if (BgL__ortest_1115z00_3639)
												{	/* Globalize/free.scm 135 */
													BgL_res1969z00_3634 = BgL__ortest_1115z00_3639;
												}
											else
												{	/* Globalize/free.scm 135 */
													long BgL_odepthz00_3640;

													{	/* Globalize/free.scm 135 */
														obj_t BgL_arg1804z00_3641;

														BgL_arg1804z00_3641 = (BgL_oclassz00_3635);
														BgL_odepthz00_3640 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3641);
													}
													if ((2L < BgL_odepthz00_3640))
														{	/* Globalize/free.scm 135 */
															obj_t BgL_arg1802z00_3642;

															{	/* Globalize/free.scm 135 */
																obj_t BgL_arg1803z00_3643;

																BgL_arg1803z00_3643 = (BgL_oclassz00_3635);
																BgL_arg1802z00_3642 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3643,
																	2L);
															}
															BgL_res1969z00_3634 =
																(BgL_arg1802z00_3642 == BgL_classz00_3629);
														}
													else
														{	/* Globalize/free.scm 135 */
															BgL_res1969z00_3634 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2141z00_4815 = BgL_res1969z00_3634;
								}
						}
					}
				}
				if (BgL_test2141z00_4815)
					{	/* Globalize/free.scm 135 */
						return BgL_freez00_3485;
					}
				else
					{	/* Globalize/free.scm 137 */
						bool_t BgL_test2145z00_4841;

						{	/* Globalize/free.scm 137 */
							BgL_variablez00_bglt BgL_arg1710z00_3644;

							BgL_arg1710z00_3644 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_3484)))->BgL_variablez00);
							BgL_test2145z00_4841 =
								CBOOL(BGl_freezd2variablezf3z21zzglobaliza7e_freeza7(
									((obj_t) BgL_arg1710z00_3644)));
						}
						if (BgL_test2145z00_4841)
							{	/* Globalize/free.scm 137 */
								{	/* Globalize/free.scm 138 */
									BgL_variablez00_bglt BgL_arg1708z00_3645;

									BgL_arg1708z00_3645 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_3484)))->
										BgL_variablez00);
									BGl_markzd2variablez12zc0zzglobaliza7e_freeza7((
											(BgL_localz00_bglt) BgL_arg1708z00_3645));
								}
								{	/* Globalize/free.scm 139 */
									BgL_variablez00_bglt BgL_arg1709z00_3646;

									BgL_arg1709z00_3646 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_3484)))->
										BgL_variablez00);
									return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1709z00_3646),
										BgL_freez00_3485);
								}
							}
						else
							{	/* Globalize/free.scm 137 */
								return BgL_freez00_3485;
							}
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_freeza7(void)
	{
		{	/* Globalize/free.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(2706117L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
			return
				BGl_modulezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75
				(62221965L, BSTRING_TO_STRING(BGl_string2005z00zzglobaliza7e_freeza7));
		}

	}

#ifdef __cplusplus
}
#endif
