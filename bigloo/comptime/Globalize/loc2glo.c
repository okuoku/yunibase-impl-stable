/*===========================================================================*/
/*   (Globalize/loc2glo.scm)                                                 */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/loc2glo.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_GLOBALIZE_LOCALzd2ze3GLOBALz31_TYPE_DEFINITIONS
#define BGL_BgL_GLOBALIZE_LOCALzd2ze3GLOBALz31_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_svarzf2ginfozf2_bgl
	{
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
		bool_t BgL_celledzf3zf3;
		bool_t BgL_stackablez00;
	}                      *BgL_svarzf2ginfozf2_bglt;

	typedef struct BgL_localzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		bool_t BgL_globaliza7edzf3z54;
	}                       *BgL_localzf2ginfozf2_bglt;

	typedef struct BgL_globalzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		obj_t BgL_globalzd2closurezd2;
	}                        *BgL_globalzf2ginfozf2_bglt;


#endif													// BGL_BgL_GLOBALIZE_LOCALzd2ze3GLOBALz31_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_localzd2ze3globalz31zzglobaliza7e_localzd2ze3globalz96
		(BgL_localz00_bglt);
	static BgL_nodez00_bglt
		BGl_makezd2escapingzd2bodyz00zzglobaliza7e_localzd2ze3globalz96
		(BgL_localz00_bglt, BgL_globalz00_bglt, obj_t, obj_t, BgL_localz00_bglt,
		BgL_nodez00_bglt);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	static obj_t
		BGl_requirezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96 =
		BUNSPEC;
	extern obj_t BGl_zb2zd2arityz60zztools_argsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_thezd2globalzd2zzglobaliza7e_localzd2ze3globalz96(BgL_localz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static BgL_globalz00_bglt
		BGl_fixzd2nonzd2escapingzd2definitionzd2zzglobaliza7e_localzd2ze3globalz96
		(BgL_globalz00_bglt, BgL_localz00_bglt, obj_t, obj_t, obj_t);
	static BgL_globalz00_bglt
		BGl_z62thezd2globalzb0zzglobaliza7e_localzd2ze3globalz96(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_localzd2ze3globalz96(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_za2cellza2z00zztype_cachez00;
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_localzd2ze3globalz96(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzglobaliza7e_localzd2ze3globalz96(obj_t,
		obj_t);
	extern obj_t BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_localzd2ze3globalz96(void);
	extern BgL_nodez00_bglt
		BGl_nodezd2globaliza7ez12z67zzglobaliza7e_nodeza7(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00;
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_methodz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_thezd2closurezd2zzglobaliza7e_freeza7(BgL_variablez00_bglt,
		obj_t);
	static BgL_globalz00_bglt
		BGl_z62localzd2ze3globalz53zzglobaliza7e_localzd2ze3globalz96(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzglobaliza7e_localzd2ze3globalz96(void);
	static obj_t
		BGl_libraryzd2moduleszd2initz00zzglobaliza7e_localzd2ze3globalz96(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zzglobaliza7e_localzd2ze3globalz96(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_localzd2ze3globalz96(void);
	static BgL_globalz00_bglt
		BGl_fixzd2escapingzd2definitionz00zzglobaliza7e_localzd2ze3globalz96
		(BgL_globalz00_bglt, BgL_localz00_bglt, obj_t, obj_t, obj_t);
	extern obj_t BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t BGl_defaultzd2typezd2zzglobaliza7e_localzd2ze3globalz96(void);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern bool_t
		BGl_localzd2iszd2methodzf3zf3zzobject_methodz00(BgL_localz00_bglt);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t __cnst[9];


	   
		 
		DEFINE_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96,
		BgL_bgl_string1924za700za7za7g1927za7, "globalize_local->global", 23);
	      DEFINE_STRING(BGl_string1925z00zzglobaliza7e_localzd2ze3globalz96,
		BgL_bgl_string1925za700za7za7g1928za7,
		"now sfun static & cell-globalize write value procedure-ref env ", 63);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2ze3globalzd2envze3zzglobaliza7e_localzd2ze3globalz96,
		BgL_bgl_za762localza7d2za7e3gl1929za7,
		BGl_z62localzd2ze3globalz53zzglobaliza7e_localzd2ze3globalz96, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_thezd2globalzd2envz00zzglobaliza7e_localzd2ze3globalz96,
		BgL_bgl_za762theza7d2globalza71930za7,
		BGl_z62thezd2globalzb0zzglobaliza7e_localzd2ze3globalz96, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*)
			(&BGl_requirezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96(long
		BgL_checksumz00_2745, char *BgL_fromz00_2746)
	{
		{
			if (CBOOL
				(BGl_requirezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_localzd2ze3globalz96 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_localzd2ze3globalz96();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_localzd2ze3globalz96();
					BGl_cnstzd2initzd2zzglobaliza7e_localzd2ze3globalz96();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_localzd2ze3globalz96();
					return BGl_methodzd2initzd2zzglobaliza7e_localzd2ze3globalz96();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_localzd2ze3globalz96(void)
	{
		{	/* Globalize/loc2glo.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L,
				"globalize_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"globalize_local->global");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L,
				"globalize_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"globalize_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"globalize_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_local->global");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzglobaliza7e_localzd2ze3globalz96(void)
	{
		{	/* Globalize/loc2glo.scm 15 */
			{	/* Globalize/loc2glo.scm 15 */
				obj_t BgL_cportz00_2734;

				{	/* Globalize/loc2glo.scm 15 */
					obj_t BgL_stringz00_2741;

					BgL_stringz00_2741 =
						BGl_string1925z00zzglobaliza7e_localzd2ze3globalz96;
					{	/* Globalize/loc2glo.scm 15 */
						obj_t BgL_startz00_2742;

						BgL_startz00_2742 = BINT(0L);
						{	/* Globalize/loc2glo.scm 15 */
							obj_t BgL_endz00_2743;

							BgL_endz00_2743 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2741)));
							{	/* Globalize/loc2glo.scm 15 */

								BgL_cportz00_2734 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2741, BgL_startz00_2742, BgL_endz00_2743);
				}}}}
				{
					long BgL_iz00_2735;

					BgL_iz00_2735 = 8L;
				BgL_loopz00_2736:
					if ((BgL_iz00_2735 == -1L))
						{	/* Globalize/loc2glo.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Globalize/loc2glo.scm 15 */
							{	/* Globalize/loc2glo.scm 15 */
								obj_t BgL_arg1926z00_2737;

								{	/* Globalize/loc2glo.scm 15 */

									{	/* Globalize/loc2glo.scm 15 */
										obj_t BgL_locationz00_2739;

										BgL_locationz00_2739 = BBOOL(((bool_t) 0));
										{	/* Globalize/loc2glo.scm 15 */

											BgL_arg1926z00_2737 =
												BGl_readz00zz__readerz00(BgL_cportz00_2734,
												BgL_locationz00_2739);
										}
									}
								}
								{	/* Globalize/loc2glo.scm 15 */
									int BgL_tmpz00_2773;

									BgL_tmpz00_2773 = (int) (BgL_iz00_2735);
									CNST_TABLE_SET(BgL_tmpz00_2773, BgL_arg1926z00_2737);
							}}
							{	/* Globalize/loc2glo.scm 15 */
								int BgL_auxz00_2740;

								BgL_auxz00_2740 = (int) ((BgL_iz00_2735 - 1L));
								{
									long BgL_iz00_2778;

									BgL_iz00_2778 = (long) (BgL_auxz00_2740);
									BgL_iz00_2735 = BgL_iz00_2778;
									goto BgL_loopz00_2736;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_localzd2ze3globalz96(void)
	{
		{	/* Globalize/loc2glo.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzglobaliza7e_localzd2ze3globalz96(obj_t
		BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1812;

				BgL_headz00_1812 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1813;
					obj_t BgL_tailz00_1814;

					BgL_prevz00_1813 = BgL_headz00_1812;
					BgL_tailz00_1814 = BgL_l1z00_1;
				BgL_loopz00_1815:
					if (PAIRP(BgL_tailz00_1814))
						{
							obj_t BgL_newzd2prevzd2_1817;

							BgL_newzd2prevzd2_1817 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1814), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1813, BgL_newzd2prevzd2_1817);
							{
								obj_t BgL_tailz00_2788;
								obj_t BgL_prevz00_2787;

								BgL_prevz00_2787 = BgL_newzd2prevzd2_1817;
								BgL_tailz00_2788 = CDR(BgL_tailz00_1814);
								BgL_tailz00_1814 = BgL_tailz00_2788;
								BgL_prevz00_1813 = BgL_prevz00_2787;
								goto BgL_loopz00_1815;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1812);
				}
			}
		}

	}



/* default-type */
	obj_t BGl_defaultzd2typezd2zzglobaliza7e_localzd2ze3globalz96(void)
	{
		{	/* Globalize/loc2glo.scm 40 */
			{	/* Globalize/loc2glo.scm 41 */
				bool_t BgL_test1934z00_2791;

				if (CBOOL
					(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
					{	/* Globalize/loc2glo.scm 41 */
						BgL_test1934z00_2791 =
							((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L);
					}
				else
					{	/* Globalize/loc2glo.scm 41 */
						BgL_test1934z00_2791 = ((bool_t) 0);
					}
				if (BgL_test1934z00_2791)
					{	/* Globalize/loc2glo.scm 41 */
						return BGl_za2_za2z00zztype_cachez00;
					}
				else
					{	/* Globalize/loc2glo.scm 41 */
						return BGl_za2objza2z00zztype_cachez00;
					}
			}
		}

	}



/* local->global */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_localzd2ze3globalz31zzglobaliza7e_localzd2ze3globalz96(BgL_localz00_bglt
		BgL_localz00_3)
	{
		{	/* Globalize/loc2glo.scm 46 */
			{	/* Globalize/loc2glo.scm 50 */
				BgL_globalz00_bglt BgL_globalz00_1821;

				BgL_globalz00_1821 =
					BGl_thezd2globalzd2zzglobaliza7e_localzd2ze3globalz96(BgL_localz00_3);
				{	/* Globalize/loc2glo.scm 50 */
					obj_t BgL_argsz00_1822;

					BgL_argsz00_1822 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_localz00_3)))->
										BgL_valuez00))))->BgL_argsz00);
					{	/* Globalize/loc2glo.scm 51 */
						BgL_valuez00_bglt BgL_infoz00_1823;

						BgL_infoz00_1823 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_localz00_3)))->BgL_valuez00);
						{	/* Globalize/loc2glo.scm 52 */
							obj_t BgL_newzd2bodyzd2_1824;

							{
								BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2803;

								{
									obj_t BgL_auxz00_2804;

									{	/* Globalize/loc2glo.scm 53 */
										BgL_objectz00_bglt BgL_tmpz00_2805;

										BgL_tmpz00_2805 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt) BgL_infoz00_1823));
										BgL_auxz00_2804 = BGL_OBJECT_WIDENING(BgL_tmpz00_2805);
									}
									BgL_auxz00_2803 =
										((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2804);
								}
								BgL_newzd2bodyzd2_1824 =
									(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2803))->
									BgL_newzd2bodyzd2);
							}
							{	/* Globalize/loc2glo.scm 53 */
								obj_t BgL_kapturedz00_1825;

								{
									BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2811;

									{
										obj_t BgL_auxz00_2812;

										{	/* Globalize/loc2glo.scm 54 */
											BgL_objectz00_bglt BgL_tmpz00_2813;

											BgL_tmpz00_2813 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_infoz00_1823));
											BgL_auxz00_2812 = BGL_OBJECT_WIDENING(BgL_tmpz00_2813);
										}
										BgL_auxz00_2811 =
											((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2812);
									}
									BgL_kapturedz00_1825 =
										(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2811))->
										BgL_kapturedz00);
								}
								{	/* Globalize/loc2glo.scm 54 */

									{	/* Globalize/loc2glo.scm 57 */
										bool_t BgL_test1936z00_2819;

										{
											BgL_localzf2ginfozf2_bglt BgL_auxz00_2820;

											{
												obj_t BgL_auxz00_2821;

												{	/* Globalize/loc2glo.scm 57 */
													BgL_objectz00_bglt BgL_tmpz00_2822;

													BgL_tmpz00_2822 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_localz00_3));
													BgL_auxz00_2821 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2822);
												}
												BgL_auxz00_2820 =
													((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2821);
											}
											BgL_test1936z00_2819 =
												(((BgL_localzf2ginfozf2_bglt)
													COBJECT(BgL_auxz00_2820))->BgL_escapezf3zf3);
										}
										if (BgL_test1936z00_2819)
											{	/* Globalize/loc2glo.scm 57 */
												BGl_fixzd2escapingzd2definitionz00zzglobaliza7e_localzd2ze3globalz96
													(BgL_globalz00_1821, BgL_localz00_3, BgL_argsz00_1822,
													BgL_kapturedz00_1825, BgL_newzd2bodyzd2_1824);
											}
										else
											{	/* Globalize/loc2glo.scm 57 */
												BGl_fixzd2nonzd2escapingzd2definitionzd2zzglobaliza7e_localzd2ze3globalz96
													(BgL_globalz00_1821, BgL_localz00_3, BgL_argsz00_1822,
													BgL_kapturedz00_1825, BgL_newzd2bodyzd2_1824);
											}
									}
									return BgL_globalz00_1821;
								}
							}
						}
					}
				}
			}
		}

	}



/* &local->global */
	BgL_globalz00_bglt
		BGl_z62localzd2ze3globalz53zzglobaliza7e_localzd2ze3globalz96(obj_t
		BgL_envz00_2730, obj_t BgL_localz00_2731)
	{
		{	/* Globalize/loc2glo.scm 46 */
			return
				BGl_localzd2ze3globalz31zzglobaliza7e_localzd2ze3globalz96(
				((BgL_localz00_bglt) BgL_localz00_2731));
		}

	}



/* fix-escaping-definition */
	BgL_globalz00_bglt
		BGl_fixzd2escapingzd2definitionz00zzglobaliza7e_localzd2ze3globalz96
		(BgL_globalz00_bglt BgL_globalz00_5, BgL_localz00_bglt BgL_localz00_6,
		obj_t BgL_argsz00_7, obj_t BgL_kapturedz00_8, obj_t BgL_bodyz00_9)
	{
		{	/* Globalize/loc2glo.scm 84 */
			{	/* Globalize/loc2glo.scm 85 */
				BgL_localz00_bglt BgL_envz00_1828;

				BgL_envz00_1828 =
					BGl_makezd2localzd2svarz00zzast_localz00(CNST_TABLE_REF(0),
					((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00));
				{	/* Globalize/loc2glo.scm 85 */
					obj_t BgL_newzd2freezd2_1829;

					if (NULLP(BgL_kapturedz00_8))
						{	/* Globalize/loc2glo.scm 86 */
							BgL_newzd2freezd2_1829 = BNIL;
						}
					else
						{	/* Globalize/loc2glo.scm 86 */
							obj_t BgL_head1332z00_1961;

							BgL_head1332z00_1961 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1330z00_1963;
								obj_t BgL_tail1333z00_1964;

								BgL_l1330z00_1963 = BgL_kapturedz00_8;
								BgL_tail1333z00_1964 = BgL_head1332z00_1961;
							BgL_zc3z04anonymousza31627ze3z87_1965:
								if (NULLP(BgL_l1330z00_1963))
									{	/* Globalize/loc2glo.scm 86 */
										BgL_newzd2freezd2_1829 = CDR(BgL_head1332z00_1961);
									}
								else
									{	/* Globalize/loc2glo.scm 86 */
										obj_t BgL_newtail1334z00_1967;

										{	/* Globalize/loc2glo.scm 86 */
											BgL_localz00_bglt BgL_arg1630z00_1969;

											{	/* Globalize/loc2glo.scm 86 */
												obj_t BgL_oldz00_1970;

												BgL_oldz00_1970 = CAR(((obj_t) BgL_l1330z00_1963));
												{	/* Globalize/loc2glo.scm 87 */
													BgL_localz00_bglt BgL_newz00_1971;

													{	/* Globalize/loc2glo.scm 88 */
														obj_t BgL_arg1654z00_1984;
														obj_t BgL_arg1661z00_1985;

														BgL_arg1654z00_1984 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_oldz00_1970))))->
															BgL_idz00);
														BgL_arg1661z00_1985 =
															BGl_defaultzd2typezd2zzglobaliza7e_localzd2ze3globalz96
															();
														BgL_newz00_1971 =
															BGl_makezd2localzd2svarz00zzast_localz00
															(BgL_arg1654z00_1984,
															((BgL_typez00_bglt) BgL_arg1661z00_1985));
													}
													{	/* Globalize/loc2glo.scm 90 */
														bool_t BgL_arg1642z00_1972;

														BgL_arg1642z00_1972 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_oldz00_1970))))->
															BgL_userzf3zf3);
														((((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt)
																			BgL_newz00_1971)))->BgL_userzf3zf3) =
															((bool_t) BgL_arg1642z00_1972), BUNSPEC);
													}
													{	/* Globalize/loc2glo.scm 91 */
														BgL_localzf2ginfozf2_bglt BgL_wide1115z00_1975;

														BgL_wide1115z00_1975 =
															((BgL_localzf2ginfozf2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_localzf2ginfozf2_bgl))));
														{	/* Globalize/loc2glo.scm 91 */
															obj_t BgL_auxz00_2858;
															BgL_objectz00_bglt BgL_tmpz00_2855;

															BgL_auxz00_2858 = ((obj_t) BgL_wide1115z00_1975);
															BgL_tmpz00_2855 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_newz00_1971));
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2855,
																BgL_auxz00_2858);
														}
														((BgL_objectz00_bglt)
															((BgL_localz00_bglt) BgL_newz00_1971));
														{	/* Globalize/loc2glo.scm 91 */
															long BgL_arg1646z00_1976;

															BgL_arg1646z00_1976 =
																BGL_CLASS_NUM
																(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																		(BgL_localz00_bglt) BgL_newz00_1971)),
																BgL_arg1646z00_1976);
														}
														((BgL_localz00_bglt)
															((BgL_localz00_bglt) BgL_newz00_1971));
													}
													{
														BgL_localzf2ginfozf2_bglt BgL_auxz00_2869;

														{
															obj_t BgL_auxz00_2870;

															{	/* Globalize/loc2glo.scm 91 */
																BgL_objectz00_bglt BgL_tmpz00_2871;

																BgL_tmpz00_2871 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt) BgL_newz00_1971));
																BgL_auxz00_2870 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2871);
															}
															BgL_auxz00_2869 =
																((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2870);
														}
														((((BgL_localzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_2869))->BgL_escapezf3zf3) =
															((bool_t) ((bool_t) 0)), BUNSPEC);
													}
													{
														BgL_localzf2ginfozf2_bglt BgL_auxz00_2877;

														{
															obj_t BgL_auxz00_2878;

															{	/* Globalize/loc2glo.scm 91 */
																BgL_objectz00_bglt BgL_tmpz00_2879;

																BgL_tmpz00_2879 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt) BgL_newz00_1971));
																BgL_auxz00_2878 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2879);
															}
															BgL_auxz00_2877 =
																((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2878);
														}
														((((BgL_localzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_2877))->
																BgL_globaliza7edzf3z54) =
															((bool_t) ((bool_t) 0)), BUNSPEC);
													}
													((BgL_localz00_bglt) BgL_newz00_1971);
													{	/* Globalize/loc2glo.scm 92 */
														BgL_svarz00_bglt BgL_tmp1117z00_1978;

														BgL_tmp1117z00_1978 =
															((BgL_svarz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_newz00_1971)))->
																BgL_valuez00));
														{	/* Globalize/loc2glo.scm 92 */
															BgL_svarzf2ginfozf2_bglt BgL_wide1119z00_1980;

															BgL_wide1119z00_1980 =
																((BgL_svarzf2ginfozf2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_svarzf2ginfozf2_bgl))));
															{	/* Globalize/loc2glo.scm 92 */
																obj_t BgL_auxz00_2893;
																BgL_objectz00_bglt BgL_tmpz00_2890;

																BgL_auxz00_2893 =
																	((obj_t) BgL_wide1119z00_1980);
																BgL_tmpz00_2890 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1117z00_1978));
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2890,
																	BgL_auxz00_2893);
															}
															((BgL_objectz00_bglt)
																((BgL_svarz00_bglt) BgL_tmp1117z00_1978));
															{	/* Globalize/loc2glo.scm 92 */
																long BgL_arg1650z00_1981;

																BgL_arg1650z00_1981 =
																	BGL_CLASS_NUM
																	(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																			(BgL_svarz00_bglt) BgL_tmp1117z00_1978)),
																	BgL_arg1650z00_1981);
															}
															((BgL_svarz00_bglt)
																((BgL_svarz00_bglt) BgL_tmp1117z00_1978));
														}
														{
															BgL_svarzf2ginfozf2_bglt BgL_auxz00_2904;

															{
																obj_t BgL_auxz00_2905;

																{	/* Globalize/loc2glo.scm 93 */
																	BgL_objectz00_bglt BgL_tmpz00_2906;

																	BgL_tmpz00_2906 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1117z00_1978));
																	BgL_auxz00_2905 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2906);
																}
																BgL_auxz00_2904 =
																	((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_2905);
															}
															((((BgL_svarzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2904))->
																	BgL_kapturedzf3zf3) =
																((bool_t) ((bool_t) 1)), BUNSPEC);
														}
														{
															BgL_svarzf2ginfozf2_bglt BgL_auxz00_2912;

															{
																obj_t BgL_auxz00_2913;

																{	/* Globalize/loc2glo.scm 93 */
																	BgL_objectz00_bglt BgL_tmpz00_2914;

																	BgL_tmpz00_2914 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1117z00_1978));
																	BgL_auxz00_2913 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2914);
																}
																BgL_auxz00_2912 =
																	((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_2913);
															}
															((((BgL_svarzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2912))->
																	BgL_freezd2markzd2) = ((long) -10L), BUNSPEC);
														}
														{
															BgL_svarzf2ginfozf2_bglt BgL_auxz00_2920;

															{
																obj_t BgL_auxz00_2921;

																{	/* Globalize/loc2glo.scm 93 */
																	BgL_objectz00_bglt BgL_tmpz00_2922;

																	BgL_tmpz00_2922 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1117z00_1978));
																	BgL_auxz00_2921 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2922);
																}
																BgL_auxz00_2920 =
																	((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_2921);
															}
															((((BgL_svarzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2920))->BgL_markz00) =
																((long) -10L), BUNSPEC);
														}
														{
															BgL_svarzf2ginfozf2_bglt BgL_auxz00_2928;

															{
																obj_t BgL_auxz00_2929;

																{	/* Globalize/loc2glo.scm 93 */
																	BgL_objectz00_bglt BgL_tmpz00_2930;

																	BgL_tmpz00_2930 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1117z00_1978));
																	BgL_auxz00_2929 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2930);
																}
																BgL_auxz00_2928 =
																	((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_2929);
															}
															((((BgL_svarzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2928))->
																	BgL_celledzf3zf3) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
														}
														{
															BgL_svarzf2ginfozf2_bglt BgL_auxz00_2936;

															{
																obj_t BgL_auxz00_2937;

																{	/* Globalize/loc2glo.scm 93 */
																	BgL_objectz00_bglt BgL_tmpz00_2938;

																	BgL_tmpz00_2938 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1117z00_1978));
																	BgL_auxz00_2937 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2938);
																}
																BgL_auxz00_2936 =
																	((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_2937);
															}
															((((BgL_svarzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2936))->
																	BgL_stackablez00) =
																((bool_t) ((bool_t) 1)), BUNSPEC);
														}
														((BgL_svarz00_bglt) BgL_tmp1117z00_1978);
													}
													{	/* Globalize/loc2glo.scm 94 */
														obj_t BgL_arg1651z00_1983;

														BgL_arg1651z00_1983 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_oldz00_1970))))->
															BgL_accessz00);
														((((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt)
																			BgL_newz00_1971)))->BgL_accessz00) =
															((obj_t) BgL_arg1651z00_1983), BUNSPEC);
													}
													BgL_arg1630z00_1969 = BgL_newz00_1971;
											}}
											BgL_newtail1334z00_1967 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1630z00_1969), BNIL);
										}
										SET_CDR(BgL_tail1333z00_1964, BgL_newtail1334z00_1967);
										{	/* Globalize/loc2glo.scm 86 */
											obj_t BgL_arg1629z00_1968;

											BgL_arg1629z00_1968 = CDR(((obj_t) BgL_l1330z00_1963));
											{
												obj_t BgL_tail1333z00_2956;
												obj_t BgL_l1330z00_2955;

												BgL_l1330z00_2955 = BgL_arg1629z00_1968;
												BgL_tail1333z00_2956 = BgL_newtail1334z00_1967;
												BgL_tail1333z00_1964 = BgL_tail1333z00_2956;
												BgL_l1330z00_1963 = BgL_l1330z00_2955;
												goto BgL_zc3z04anonymousza31627ze3z87_1965;
											}
										}
									}
							}
						}
					{	/* Globalize/loc2glo.scm 86 */
						obj_t BgL_newzd2argszd2_1830;

						if (NULLP(BgL_argsz00_7))
							{	/* Globalize/loc2glo.scm 97 */
								BgL_newzd2argszd2_1830 = BNIL;
							}
						else
							{	/* Globalize/loc2glo.scm 97 */
								obj_t BgL_head1337z00_1932;

								BgL_head1337z00_1932 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1335z00_1934;
									obj_t BgL_tail1338z00_1935;

									BgL_l1335z00_1934 = BgL_argsz00_7;
									BgL_tail1338z00_1935 = BgL_head1337z00_1932;
								BgL_zc3z04anonymousza31596ze3z87_1936:
									if (NULLP(BgL_l1335z00_1934))
										{	/* Globalize/loc2glo.scm 97 */
											BgL_newzd2argszd2_1830 = CDR(BgL_head1337z00_1932);
										}
									else
										{	/* Globalize/loc2glo.scm 97 */
											obj_t BgL_newtail1339z00_1938;

											{	/* Globalize/loc2glo.scm 97 */
												BgL_localz00_bglt BgL_arg1605z00_1940;

												{	/* Globalize/loc2glo.scm 97 */
													obj_t BgL_oldz00_1941;

													BgL_oldz00_1941 = CAR(((obj_t) BgL_l1335z00_1934));
													{	/* Globalize/loc2glo.scm 98 */
														BgL_localz00_bglt BgL_newz00_1942;

														{	/* Globalize/loc2glo.scm 99 */
															obj_t BgL_arg1616z00_1956;
															obj_t BgL_arg1625z00_1957;

															BgL_arg1616z00_1956 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt) BgL_oldz00_1941))))->
																BgL_idz00);
															BgL_arg1625z00_1957 =
																BGl_defaultzd2typezd2zzglobaliza7e_localzd2ze3globalz96
																();
															BgL_newz00_1942 =
																BGl_makezd2localzd2svarz00zzast_localz00
																(BgL_arg1616z00_1956,
																((BgL_typez00_bglt) BgL_arg1625z00_1957));
														}
														{	/* Globalize/loc2glo.scm 101 */
															bool_t BgL_arg1606z00_1943;

															BgL_arg1606z00_1943 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt) BgL_oldz00_1941))))->
																BgL_userzf3zf3);
															((((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_newz00_1942)))->BgL_userzf3zf3) =
																((bool_t) BgL_arg1606z00_1943), BUNSPEC);
														}
														{	/* Globalize/loc2glo.scm 102 */
															BgL_localzf2ginfozf2_bglt BgL_wide1123z00_1946;

															BgL_wide1123z00_1946 =
																((BgL_localzf2ginfozf2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_localzf2ginfozf2_bgl))));
															{	/* Globalize/loc2glo.scm 102 */
																obj_t BgL_auxz00_2980;
																BgL_objectz00_bglt BgL_tmpz00_2977;

																BgL_auxz00_2980 =
																	((obj_t) BgL_wide1123z00_1946);
																BgL_tmpz00_2977 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt) BgL_newz00_1942));
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2977,
																	BgL_auxz00_2980);
															}
															((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_newz00_1942));
															{	/* Globalize/loc2glo.scm 102 */
																long BgL_arg1609z00_1947;

																BgL_arg1609z00_1947 =
																	BGL_CLASS_NUM
																	(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																			(BgL_localz00_bglt) BgL_newz00_1942)),
																	BgL_arg1609z00_1947);
															}
															((BgL_localz00_bglt)
																((BgL_localz00_bglt) BgL_newz00_1942));
														}
														{
															BgL_localzf2ginfozf2_bglt BgL_auxz00_2991;

															{
																obj_t BgL_auxz00_2992;

																{	/* Globalize/loc2glo.scm 102 */
																	BgL_objectz00_bglt BgL_tmpz00_2993;

																	BgL_tmpz00_2993 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt) BgL_newz00_1942));
																	BgL_auxz00_2992 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2993);
																}
																BgL_auxz00_2991 =
																	((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2992);
															}
															((((BgL_localzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2991))->
																	BgL_escapezf3zf3) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
														}
														{
															BgL_localzf2ginfozf2_bglt BgL_auxz00_2999;

															{
																obj_t BgL_auxz00_3000;

																{	/* Globalize/loc2glo.scm 102 */
																	BgL_objectz00_bglt BgL_tmpz00_3001;

																	BgL_tmpz00_3001 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt) BgL_newz00_1942));
																	BgL_auxz00_3000 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_3001);
																}
																BgL_auxz00_2999 =
																	((BgL_localzf2ginfozf2_bglt) BgL_auxz00_3000);
															}
															((((BgL_localzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2999))->
																	BgL_globaliza7edzf3z54) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
														}
														((BgL_localz00_bglt) BgL_newz00_1942);
														{	/* Globalize/loc2glo.scm 103 */
															BgL_svarz00_bglt BgL_tmp1125z00_1949;

															BgL_tmp1125z00_1949 =
																((BgL_svarz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_newz00_1942)))->BgL_valuez00));
															{	/* Globalize/loc2glo.scm 103 */
																BgL_svarzf2ginfozf2_bglt BgL_wide1127z00_1951;

																BgL_wide1127z00_1951 =
																	((BgL_svarzf2ginfozf2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_svarzf2ginfozf2_bgl))));
																{	/* Globalize/loc2glo.scm 103 */
																	obj_t BgL_auxz00_3015;
																	BgL_objectz00_bglt BgL_tmpz00_3012;

																	BgL_auxz00_3015 =
																		((obj_t) BgL_wide1127z00_1951);
																	BgL_tmpz00_3012 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1125z00_1949));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3012,
																		BgL_auxz00_3015);
																}
																((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1125z00_1949));
																{	/* Globalize/loc2glo.scm 103 */
																	long BgL_arg1611z00_1952;

																	BgL_arg1611z00_1952 =
																		BGL_CLASS_NUM
																		(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1125z00_1949)),
																		BgL_arg1611z00_1952);
																}
																((BgL_svarz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1125z00_1949));
															}
															{
																bool_t BgL_auxz00_3033;
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3026;

																{	/* Globalize/loc2glo.scm 104 */
																	BgL_svarz00_bglt BgL_oz00_2394;

																	BgL_oz00_2394 =
																		((BgL_svarz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_oldz00_1941))))->
																			BgL_valuez00));
																	{
																		BgL_svarzf2ginfozf2_bglt BgL_auxz00_3038;

																		{
																			obj_t BgL_auxz00_3039;

																			{	/* Globalize/loc2glo.scm 104 */
																				BgL_objectz00_bglt BgL_tmpz00_3040;

																				BgL_tmpz00_3040 =
																					((BgL_objectz00_bglt) BgL_oz00_2394);
																				BgL_auxz00_3039 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_3040);
																			}
																			BgL_auxz00_3038 =
																				((BgL_svarzf2ginfozf2_bglt)
																				BgL_auxz00_3039);
																		}
																		BgL_auxz00_3033 =
																			(((BgL_svarzf2ginfozf2_bglt)
																				COBJECT(BgL_auxz00_3038))->
																			BgL_kapturedzf3zf3);
																}}
																{
																	obj_t BgL_auxz00_3027;

																	{	/* Globalize/loc2glo.scm 104 */
																		BgL_objectz00_bglt BgL_tmpz00_3028;

																		BgL_tmpz00_3028 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1125z00_1949));
																		BgL_auxz00_3027 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3028);
																	}
																	BgL_auxz00_3026 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3027);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3026))->
																		BgL_kapturedzf3zf3) =
																	((bool_t) BgL_auxz00_3033), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3046;

																{
																	obj_t BgL_auxz00_3047;

																	{	/* Globalize/loc2glo.scm 104 */
																		BgL_objectz00_bglt BgL_tmpz00_3048;

																		BgL_tmpz00_3048 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1125z00_1949));
																		BgL_auxz00_3047 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3048);
																	}
																	BgL_auxz00_3046 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3047);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3046))->
																		BgL_freezd2markzd2) =
																	((long) -10L), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3054;

																{
																	obj_t BgL_auxz00_3055;

																	{	/* Globalize/loc2glo.scm 104 */
																		BgL_objectz00_bglt BgL_tmpz00_3056;

																		BgL_tmpz00_3056 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1125z00_1949));
																		BgL_auxz00_3055 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3056);
																	}
																	BgL_auxz00_3054 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3055);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3054))->BgL_markz00) =
																	((long) -10L), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3062;

																{
																	obj_t BgL_auxz00_3063;

																	{	/* Globalize/loc2glo.scm 104 */
																		BgL_objectz00_bglt BgL_tmpz00_3064;

																		BgL_tmpz00_3064 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1125z00_1949));
																		BgL_auxz00_3063 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3064);
																	}
																	BgL_auxz00_3062 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3063);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3062))->
																		BgL_celledzf3zf3) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3070;

																{
																	obj_t BgL_auxz00_3071;

																	{	/* Globalize/loc2glo.scm 104 */
																		BgL_objectz00_bglt BgL_tmpz00_3072;

																		BgL_tmpz00_3072 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1125z00_1949));
																		BgL_auxz00_3071 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3072);
																	}
																	BgL_auxz00_3070 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3071);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3070))->
																		BgL_stackablez00) =
																	((bool_t) ((bool_t) 1)), BUNSPEC);
															}
															((BgL_svarz00_bglt) BgL_tmp1125z00_1949);
														}
														{	/* Globalize/loc2glo.scm 106 */
															obj_t BgL_arg1615z00_1955;

															BgL_arg1615z00_1955 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt) BgL_oldz00_1941))))->
																BgL_accessz00);
															((((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_newz00_1942)))->BgL_accessz00) =
																((obj_t) BgL_arg1615z00_1955), BUNSPEC);
														}
														BgL_arg1605z00_1940 = BgL_newz00_1942;
												}}
												BgL_newtail1339z00_1938 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1605z00_1940), BNIL);
											}
											SET_CDR(BgL_tail1338z00_1935, BgL_newtail1339z00_1938);
											{	/* Globalize/loc2glo.scm 97 */
												obj_t BgL_arg1602z00_1939;

												BgL_arg1602z00_1939 = CDR(((obj_t) BgL_l1335z00_1934));
												{
													obj_t BgL_tail1338z00_3090;
													obj_t BgL_l1335z00_3089;

													BgL_l1335z00_3089 = BgL_arg1602z00_1939;
													BgL_tail1338z00_3090 = BgL_newtail1339z00_1938;
													BgL_tail1338z00_1935 = BgL_tail1338z00_3090;
													BgL_l1335z00_1934 = BgL_l1335z00_3089;
													goto BgL_zc3z04anonymousza31596ze3z87_1936;
												}
											}
										}
								}
							}
						{	/* Globalize/loc2glo.scm 97 */
							BgL_valuez00_bglt BgL_oldzd2funzd2_1831;

							BgL_oldzd2funzd2_1831 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_localz00_6)))->BgL_valuez00);
							{	/* Globalize/loc2glo.scm 109 */
								BgL_sfunz00_bglt BgL_newzd2funzd2_1832;

								{	/* Globalize/loc2glo.scm 110 */
									BgL_sfunz00_bglt BgL_new1130z00_1915;

									{	/* Globalize/loc2glo.scm 111 */
										BgL_sfunz00_bglt BgL_new1144z00_1928;

										BgL_new1144z00_1928 =
											((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_sfunz00_bgl))));
										{	/* Globalize/loc2glo.scm 111 */
											long BgL_arg1594z00_1929;

											BgL_arg1594z00_1929 =
												BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1144z00_1928),
												BgL_arg1594z00_1929);
										}
										{	/* Globalize/loc2glo.scm 111 */
											BgL_objectz00_bglt BgL_tmpz00_3097;

											BgL_tmpz00_3097 =
												((BgL_objectz00_bglt) BgL_new1144z00_1928);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3097, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1144z00_1928);
										BgL_new1130z00_1915 = BgL_new1144z00_1928;
									}
									{
										long BgL_auxz00_3101;

										{	/* Globalize/loc2glo.scm 111 */
											long BgL_arg1593z00_1916;

											BgL_arg1593z00_1916 =
												(((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt)
															((BgL_sfunz00_bglt) BgL_oldzd2funzd2_1831))))->
												BgL_arityz00);
											BgL_auxz00_3101 =
												(long)
												CINT(BGl_zb2zd2arityz60zztools_argsz00(BINT
													(BgL_arg1593z00_1916), BINT(1L)));
										}
										((((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt) BgL_new1130z00_1915)))->
												BgL_arityz00) = ((long) BgL_auxz00_3101), BUNSPEC);
									}
									((((BgL_funz00_bglt) COBJECT(
													((BgL_funz00_bglt) BgL_new1130z00_1915)))->
											BgL_sidezd2effectzd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1831)))->BgL_sidezd2effectzd2)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1130z00_1915)))->BgL_predicatezd2ofzd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1831)))->BgL_predicatezd2ofzd2)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1130z00_1915)))->BgL_stackzd2allocatorzd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1831)))->
												BgL_stackzd2allocatorzd2)), BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1130z00_1915)))->BgL_topzf3zf3) =
										((bool_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1831)))->BgL_topzf3zf3)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1130z00_1915)))->BgL_thezd2closurezd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1831)))->BgL_thezd2closurezd2)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1130z00_1915)))->BgL_effectz00) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1831)))->BgL_effectz00)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1130z00_1915)))->BgL_failsafez00) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1831)))->BgL_failsafez00)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1130z00_1915)))->BgL_argszd2noescapezd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1831)))->
												BgL_argszd2noescapezd2)), BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1130z00_1915)))->BgL_argszd2retescapezd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1831)))->
												BgL_argszd2retescapezd2)), BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_propertyz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->BgL_propertyz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_argsz00) =
										((obj_t) MAKE_YOUNG_PAIR(((obj_t) BgL_envz00_1828),
												BgL_newzd2argszd2_1830)), BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_argszd2namezd2) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->BgL_argszd2namezd2)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_bodyz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->BgL_bodyz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_classz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->BgL_classz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_dssslzd2keywordszd2) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->
												BgL_dssslzd2keywordszd2)), BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_locz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->BgL_locz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_optionalsz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->BgL_optionalsz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_keysz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->BgL_keysz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_thezd2closurezd2globalz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->
												BgL_thezd2closurezd2globalz00)), BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_strengthz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->BgL_strengthz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1130z00_1915))->
											BgL_stackablez00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1831))))->BgL_stackablez00)),
										BUNSPEC);
									BgL_newzd2funzd2_1832 = BgL_new1130z00_1915;
								}
								{	/* Globalize/loc2glo.scm 110 */

									{
										BgL_globalzf2ginfozf2_bglt BgL_auxz00_3194;

										{
											obj_t BgL_auxz00_3195;

											{	/* Globalize/loc2glo.scm 114 */
												BgL_objectz00_bglt BgL_tmpz00_3196;

												BgL_tmpz00_3196 =
													((BgL_objectz00_bglt)
													((BgL_globalz00_bglt) BgL_globalz00_5));
												BgL_auxz00_3195 = BGL_OBJECT_WIDENING(BgL_tmpz00_3196);
											}
											BgL_auxz00_3194 =
												((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_3195);
										}
										((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3194))->
												BgL_escapezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
									}
									{	/* Globalize/loc2glo.scm 115 */
										BgL_sfunzf2ginfozf2_bglt BgL_wide1147z00_1835;

										BgL_wide1147z00_1835 =
											((BgL_sfunzf2ginfozf2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_sfunzf2ginfozf2_bgl))));
										{	/* Globalize/loc2glo.scm 115 */
											obj_t BgL_auxz00_3206;
											BgL_objectz00_bglt BgL_tmpz00_3203;

											BgL_auxz00_3206 = ((obj_t) BgL_wide1147z00_1835);
											BgL_tmpz00_3203 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3203, BgL_auxz00_3206);
										}
										((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
										{	/* Globalize/loc2glo.scm 115 */
											long BgL_arg1410z00_1836;

											BgL_arg1410z00_1836 =
												BGL_CLASS_NUM
												(BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7);
											BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
														(BgL_sfunz00_bglt) BgL_newzd2funzd2_1832)),
												BgL_arg1410z00_1836);
										}
										((BgL_sfunz00_bglt)
											((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3217;

										{
											obj_t BgL_auxz00_3218;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3219;

												BgL_tmpz00_3219 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3218 = BGL_OBJECT_WIDENING(BgL_tmpz00_3219);
											}
											BgL_auxz00_3217 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3218);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3217))->
												BgL_gzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3225;

										{
											obj_t BgL_auxz00_3226;

											{	/* Globalize/ginfo.scm 24 */
												BgL_objectz00_bglt BgL_tmpz00_3227;

												BgL_tmpz00_3227 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3226 = BGL_OBJECT_WIDENING(BgL_tmpz00_3227);
											}
											BgL_auxz00_3225 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3226);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3225))->
												BgL_cfromz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3233;

										{
											obj_t BgL_auxz00_3234;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3235;

												BgL_tmpz00_3235 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3234 = BGL_OBJECT_WIDENING(BgL_tmpz00_3235);
											}
											BgL_auxz00_3233 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3234);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3233))->
												BgL_cfromza2za2) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3241;

										{
											obj_t BgL_auxz00_3242;

											{	/* Globalize/ginfo.scm 28 */
												BgL_objectz00_bglt BgL_tmpz00_3243;

												BgL_tmpz00_3243 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3242 = BGL_OBJECT_WIDENING(BgL_tmpz00_3243);
											}
											BgL_auxz00_3241 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3242);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3241))->
												BgL_ctoz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3249;

										{
											obj_t BgL_auxz00_3250;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3251;

												BgL_tmpz00_3251 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3250 = BGL_OBJECT_WIDENING(BgL_tmpz00_3251);
											}
											BgL_auxz00_3249 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3250);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3249))->
												BgL_ctoza2za2) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3257;

										{
											obj_t BgL_auxz00_3258;

											{	/* Globalize/ginfo.scm 32 */
												BgL_objectz00_bglt BgL_tmpz00_3259;

												BgL_tmpz00_3259 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3258 = BGL_OBJECT_WIDENING(BgL_tmpz00_3259);
											}
											BgL_auxz00_3257 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3258);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3257))->
												BgL_efunctionsz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3265;

										{
											obj_t BgL_auxz00_3266;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3267;

												BgL_tmpz00_3267 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3266 = BGL_OBJECT_WIDENING(BgL_tmpz00_3267);
											}
											BgL_auxz00_3265 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3266);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3265))->
												BgL_integratorz00) = ((obj_t) BUNSPEC), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3273;

										{
											obj_t BgL_auxz00_3274;

											{	/* Globalize/ginfo.scm 36 */
												BgL_objectz00_bglt BgL_tmpz00_3275;

												BgL_tmpz00_3275 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3274 = BGL_OBJECT_WIDENING(BgL_tmpz00_3275);
											}
											BgL_auxz00_3273 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3274);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3273))->
												BgL_imarkz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3281;

										{
											obj_t BgL_auxz00_3282;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3283;

												BgL_tmpz00_3283 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3282 = BGL_OBJECT_WIDENING(BgL_tmpz00_3283);
											}
											BgL_auxz00_3281 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3282);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3281))->
												BgL_ownerz00) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3289;

										{
											obj_t BgL_auxz00_3290;

											{	/* Globalize/ginfo.scm 40 */
												BgL_objectz00_bglt BgL_tmpz00_3291;

												BgL_tmpz00_3291 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3290 = BGL_OBJECT_WIDENING(BgL_tmpz00_3291);
											}
											BgL_auxz00_3289 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3290);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3289))->
												BgL_integratedz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3297;

										{
											obj_t BgL_auxz00_3298;

											{	/* Globalize/ginfo.scm 42 */
												BgL_objectz00_bglt BgL_tmpz00_3299;

												BgL_tmpz00_3299 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3298 = BGL_OBJECT_WIDENING(BgL_tmpz00_3299);
											}
											BgL_auxz00_3297 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3298);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3297))->
												BgL_pluggedzd2inzd2) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3305;

										{
											obj_t BgL_auxz00_3306;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3307;

												BgL_tmpz00_3307 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3306 = BGL_OBJECT_WIDENING(BgL_tmpz00_3307);
											}
											BgL_auxz00_3305 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3306);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3305))->
												BgL_markz00) = ((long) -10L), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3313;

										{
											obj_t BgL_auxz00_3314;

											{	/* Globalize/ginfo.scm 46 */
												BgL_objectz00_bglt BgL_tmpz00_3315;

												BgL_tmpz00_3315 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3314 = BGL_OBJECT_WIDENING(BgL_tmpz00_3315);
											}
											BgL_auxz00_3313 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3314);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3313))->
												BgL_freezd2markzd2) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3321;

										{
											obj_t BgL_auxz00_3322;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3323;

												BgL_tmpz00_3323 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3322 = BGL_OBJECT_WIDENING(BgL_tmpz00_3323);
											}
											BgL_auxz00_3321 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3322);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3321))->
												BgL_thezd2globalzd2) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3329;

										{
											obj_t BgL_auxz00_3330;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3331;

												BgL_tmpz00_3331 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3330 = BGL_OBJECT_WIDENING(BgL_tmpz00_3331);
											}
											BgL_auxz00_3329 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3330);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3329))->
												BgL_kapturedz00) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3337;

										{
											obj_t BgL_auxz00_3338;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3339;

												BgL_tmpz00_3339 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3338 = BGL_OBJECT_WIDENING(BgL_tmpz00_3339);
											}
											BgL_auxz00_3337 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3338);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3337))->
												BgL_newzd2bodyzd2) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3345;

										{
											obj_t BgL_auxz00_3346;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3347;

												BgL_tmpz00_3347 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3346 = BGL_OBJECT_WIDENING(BgL_tmpz00_3347);
											}
											BgL_auxz00_3345 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3346);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3345))->
												BgL_bmarkz00) = ((long) -10L), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3353;

										{
											obj_t BgL_auxz00_3354;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3355;

												BgL_tmpz00_3355 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3354 = BGL_OBJECT_WIDENING(BgL_tmpz00_3355);
											}
											BgL_auxz00_3353 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3354);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3353))->
												BgL_umarkz00) = ((long) -10L), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3361;

										{
											obj_t BgL_auxz00_3362;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3363;

												BgL_tmpz00_3363 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3362 = BGL_OBJECT_WIDENING(BgL_tmpz00_3363);
											}
											BgL_auxz00_3361 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3362);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3361))->
												BgL_freez00) = ((obj_t) BUNSPEC), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3369;

										{
											obj_t BgL_auxz00_3370;

											{	/* Globalize/ginfo.scm 60 */
												BgL_objectz00_bglt BgL_tmpz00_3371;

												BgL_tmpz00_3371 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832));
												BgL_auxz00_3370 = BGL_OBJECT_WIDENING(BgL_tmpz00_3371);
											}
											BgL_auxz00_3369 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3370);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3369))->
												BgL_boundz00) = ((obj_t) BNIL), BUNSPEC);
									}
									((BgL_sfunz00_bglt) BgL_newzd2funzd2_1832);
									{	/* Globalize/loc2glo.scm 117 */
										BgL_svarz00_bglt BgL_tmp1149z00_1838;

										BgL_tmp1149z00_1838 =
											((BgL_svarz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_envz00_1828)))->
												BgL_valuez00));
										{	/* Globalize/loc2glo.scm 117 */
											BgL_svarzf2ginfozf2_bglt BgL_wide1151z00_1840;

											BgL_wide1151z00_1840 =
												((BgL_svarzf2ginfozf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_svarzf2ginfozf2_bgl))));
											{	/* Globalize/loc2glo.scm 117 */
												obj_t BgL_auxz00_3385;
												BgL_objectz00_bglt BgL_tmpz00_3382;

												BgL_auxz00_3385 = ((obj_t) BgL_wide1151z00_1840);
												BgL_tmpz00_3382 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1149z00_1838));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3382,
													BgL_auxz00_3385);
											}
											((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1149z00_1838));
											{	/* Globalize/loc2glo.scm 117 */
												long BgL_arg1421z00_1841;

												BgL_arg1421z00_1841 =
													BGL_CLASS_NUM
													(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_svarz00_bglt) BgL_tmp1149z00_1838)),
													BgL_arg1421z00_1841);
											}
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1149z00_1838));
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3396;

											{
												obj_t BgL_auxz00_3397;

												{	/* Globalize/loc2glo.scm 117 */
													BgL_objectz00_bglt BgL_tmpz00_3398;

													BgL_tmpz00_3398 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_1838));
													BgL_auxz00_3397 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3398);
												}
												BgL_auxz00_3396 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3397);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3396))->
													BgL_kapturedzf3zf3) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3404;

											{
												obj_t BgL_auxz00_3405;

												{	/* Globalize/loc2glo.scm 117 */
													BgL_objectz00_bglt BgL_tmpz00_3406;

													BgL_tmpz00_3406 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_1838));
													BgL_auxz00_3405 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3406);
												}
												BgL_auxz00_3404 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3405);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3404))->
													BgL_freezd2markzd2) = ((long) -10L), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3412;

											{
												obj_t BgL_auxz00_3413;

												{	/* Globalize/loc2glo.scm 117 */
													BgL_objectz00_bglt BgL_tmpz00_3414;

													BgL_tmpz00_3414 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_1838));
													BgL_auxz00_3413 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3414);
												}
												BgL_auxz00_3412 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3413);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3412))->
													BgL_markz00) = ((long) -10L), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3420;

											{
												obj_t BgL_auxz00_3421;

												{	/* Globalize/loc2glo.scm 117 */
													BgL_objectz00_bglt BgL_tmpz00_3422;

													BgL_tmpz00_3422 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_1838));
													BgL_auxz00_3421 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3422);
												}
												BgL_auxz00_3420 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3421);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3420))->
													BgL_celledzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3428;

											{
												obj_t BgL_auxz00_3429;

												{	/* Globalize/loc2glo.scm 117 */
													BgL_objectz00_bglt BgL_tmpz00_3430;

													BgL_tmpz00_3430 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_1838));
													BgL_auxz00_3429 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3430);
												}
												BgL_auxz00_3428 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3429);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3428))->
													BgL_stackablez00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
										}
										((BgL_svarz00_bglt) BgL_tmp1149z00_1838);
									}
									{	/* Globalize/loc2glo.scm 118 */
										obj_t BgL_arg1422z00_1843;

										{	/* Globalize/loc2glo.scm 118 */
											BgL_typez00_bglt BgL_arg1434z00_1844;

											BgL_arg1434z00_1844 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_localz00_6)))->
												BgL_typez00);
											if (CBOOL
												(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
												{	/* Globalize/loc2glo.scm 70 */
													BgL_arg1422z00_1843 = ((obj_t) BgL_arg1434z00_1844);
												}
											else
												{	/* Globalize/loc2glo.scm 70 */
													BgL_arg1422z00_1843 = BGl_za2objza2z00zztype_cachez00;
												}
										}
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_globalz00_5)))->
												BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BgL_arg1422z00_1843)), BUNSPEC);
									}
									((((BgL_sfunz00_bglt) COBJECT(BgL_newzd2funzd2_1832))->
											BgL_thezd2closurezd2globalz00) =
										((obj_t) ((obj_t) BgL_localz00_6)), BUNSPEC);
									{	/* Globalize/loc2glo.scm 121 */
										obj_t BgL_nargsz00_1845;

										BgL_nargsz00_1845 =
											CDR(
											(((BgL_sfunz00_bglt) COBJECT(BgL_newzd2funzd2_1832))->
												BgL_argsz00));
										{	/* Globalize/loc2glo.scm 122 */
											bool_t BgL_test1942z00_3449;

											if (BGl_localzd2iszd2methodzf3zf3zzobject_methodz00
												(BgL_localz00_6))
												{	/* Globalize/loc2glo.scm 123 */
													obj_t BgL_arg1544z00_1884;

													BgL_arg1544z00_1884 =
														BGl_thezd2backendzd2zzbackend_backendz00();
													BgL_test1942z00_3449 =
														(((BgL_backendz00_bglt) COBJECT(
																((BgL_backendz00_bglt) BgL_arg1544z00_1884)))->
														BgL_typedzd2funcallzd2);
												}
											else
												{	/* Globalize/loc2glo.scm 122 */
													BgL_test1942z00_3449 = ((bool_t) 0);
												}
											if (BgL_test1942z00_3449)
												{	/* Globalize/loc2glo.scm 122 */
													{	/* Globalize/loc2glo.scm 128 */
														obj_t BgL_arg1448z00_1849;
														BgL_typez00_bglt BgL_arg1453z00_1850;

														BgL_arg1448z00_1849 =
															CAR(((obj_t) BgL_nargsz00_1845));
														BgL_arg1453z00_1850 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_argsz00_7))))))->
															BgL_typez00);
														((((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt) (
																				(BgL_localz00_bglt)
																				BgL_arg1448z00_1849))))->BgL_typez00) =
															((BgL_typez00_bglt) BgL_arg1453z00_1850),
															BUNSPEC);
													}
													{	/* Globalize/loc2glo.scm 129 */
														obj_t BgL_g1343z00_1852;
														obj_t BgL_g1344z00_1853;

														BgL_g1343z00_1852 =
															CDR(((obj_t) BgL_nargsz00_1845));
														BgL_g1344z00_1853 = CDR(((obj_t) BgL_argsz00_7));
														{
															obj_t BgL_ll1340z00_1855;
															obj_t BgL_ll1341z00_1856;

															BgL_ll1340z00_1855 = BgL_g1343z00_1852;
															BgL_ll1341z00_1856 = BgL_g1344z00_1853;
														BgL_zc3z04anonymousza31455ze3z87_1857:
															if (NULLP(BgL_ll1340z00_1855))
																{	/* Globalize/loc2glo.scm 134 */
																	((bool_t) 1);
																}
															else
																{	/* Globalize/loc2glo.scm 134 */
																	{	/* Globalize/loc2glo.scm 130 */
																		obj_t BgL_nz00_1859;
																		obj_t BgL_oz00_1860;

																		BgL_nz00_1859 =
																			CAR(((obj_t) BgL_ll1340z00_1855));
																		BgL_oz00_1860 =
																			CAR(((obj_t) BgL_ll1341z00_1856));
																		{	/* Globalize/loc2glo.scm 130 */
																			bool_t BgL_test1945z00_3475;

																			{	/* Globalize/loc2glo.scm 130 */
																				bool_t BgL_test1946z00_3476;

																				{	/* Globalize/loc2glo.scm 130 */
																					BgL_typez00_bglt BgL_arg1514z00_1872;

																					BgL_arg1514z00_1872 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_localz00_bglt)
																										BgL_oz00_1860))))->
																						BgL_typez00);
																					BgL_test1946z00_3476 =
																						BGl_bigloozd2typezf3z21zztype_typez00
																						(BgL_arg1514z00_1872);
																				}
																				if (BgL_test1946z00_3476)
																					{	/* Globalize/loc2glo.scm 131 */
																						bool_t BgL_test1947z00_3481;

																						{	/* Globalize/loc2glo.scm 131 */
																							BgL_typez00_bglt
																								BgL_arg1513z00_1871;
																							BgL_arg1513z00_1871 =
																								(((BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt) (
																												(BgL_localz00_bglt)
																												BgL_oz00_1860))))->
																								BgL_typez00);
																							BgL_test1947z00_3481 =
																								(((obj_t) BgL_arg1513z00_1871)
																								==
																								BGl_za2_za2z00zztype_cachez00);
																						}
																						if (BgL_test1947z00_3481)
																							{	/* Globalize/loc2glo.scm 131 */
																								BgL_test1945z00_3475 =
																									((bool_t) 0);
																							}
																						else
																							{	/* Globalize/loc2glo.scm 131 */
																								BgL_test1945z00_3475 =
																									((bool_t) 1);
																							}
																					}
																				else
																					{	/* Globalize/loc2glo.scm 130 */
																						BgL_test1945z00_3475 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test1945z00_3475)
																				{	/* Globalize/loc2glo.scm 132 */
																					BgL_typez00_bglt BgL_arg1509z00_1868;

																					BgL_arg1509z00_1868 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_localz00_bglt)
																										BgL_oz00_1860))))->
																						BgL_typez00);
																					((((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										((BgL_localz00_bglt)
																											BgL_nz00_1859))))->
																							BgL_typez00) =
																						((BgL_typez00_bglt)
																							BgL_arg1509z00_1868), BUNSPEC);
																				}
																			else
																				{	/* Globalize/loc2glo.scm 133 */
																					BgL_typez00_bglt BgL_vz00_2469;

																					BgL_vz00_2469 =
																						((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00);
																					((((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										((BgL_localz00_bglt)
																											BgL_nz00_1859))))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) BgL_vz00_2469),
																						BUNSPEC);
																				}
																		}
																	}
																	{	/* Globalize/loc2glo.scm 134 */
																		obj_t BgL_arg1516z00_1873;
																		obj_t BgL_arg1535z00_1874;

																		BgL_arg1516z00_1873 =
																			CDR(((obj_t) BgL_ll1340z00_1855));
																		BgL_arg1535z00_1874 =
																			CDR(((obj_t) BgL_ll1341z00_1856));
																		{
																			obj_t BgL_ll1341z00_3502;
																			obj_t BgL_ll1340z00_3501;

																			BgL_ll1340z00_3501 = BgL_arg1516z00_1873;
																			BgL_ll1341z00_3502 = BgL_arg1535z00_1874;
																			BgL_ll1341z00_1856 = BgL_ll1341z00_3502;
																			BgL_ll1340z00_1855 = BgL_ll1340z00_3501;
																			goto
																				BgL_zc3z04anonymousza31455ze3z87_1857;
																		}
																	}
																}
														}
													}
												}
											else
												{	/* Globalize/loc2glo.scm 122 */
													if (CBOOL
														(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
														{	/* Globalize/loc2glo.scm 135 */
															((bool_t) 0);
														}
													else
														{
															obj_t BgL_l1345z00_1877;

															BgL_l1345z00_1877 = BgL_nargsz00_1845;
														BgL_zc3z04anonymousza31536ze3z87_1878:
															if (PAIRP(BgL_l1345z00_1877))
																{	/* Globalize/loc2glo.scm 136 */
																	{	/* Globalize/loc2glo.scm 137 */
																		obj_t BgL_lz00_1880;

																		BgL_lz00_1880 = CAR(BgL_l1345z00_1877);
																		{	/* Globalize/loc2glo.scm 137 */
																			BgL_typez00_bglt BgL_vz00_2474;

																			BgL_vz00_2474 =
																				((BgL_typez00_bglt)
																				BGl_za2objza2z00zztype_cachez00);
																			((((BgL_variablez00_bglt)
																						COBJECT(((BgL_variablez00_bglt) (
																									(BgL_localz00_bglt)
																									BgL_lz00_1880))))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) BgL_vz00_2474),
																				BUNSPEC);
																		}
																	}
																	{
																		obj_t BgL_l1345z00_3512;

																		BgL_l1345z00_3512 = CDR(BgL_l1345z00_1877);
																		BgL_l1345z00_1877 = BgL_l1345z00_3512;
																		goto BgL_zc3z04anonymousza31536ze3z87_1878;
																	}
																}
															else
																{	/* Globalize/loc2glo.scm 136 */
																	((bool_t) 1);
																}
														}
												}
										}
									}
									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_5)))->
											BgL_valuez00) =
										((BgL_valuez00_bglt) ((BgL_valuez00_bglt)
												BgL_newzd2funzd2_1832)), BUNSPEC);
									{	/* Globalize/loc2glo.scm 145 */
										BgL_nodez00_bglt BgL_arg1552z00_1886;

										{	/* Globalize/loc2glo.scm 145 */
											BgL_nodez00_bglt BgL_arg1553z00_1887;

											{	/* Globalize/loc2glo.scm 145 */
												obj_t BgL_arg1559z00_1888;

												{	/* Globalize/loc2glo.scm 145 */
													obj_t BgL_arg1561z00_1889;
													obj_t BgL_arg1564z00_1890;

													{	/* Globalize/loc2glo.scm 145 */
														obj_t BgL_arg1565z00_1891;

														BgL_arg1565z00_1891 =
															BGl_thezd2closurezd2zzglobaliza7e_freeza7(
															((BgL_variablez00_bglt) BgL_localz00_6), BFALSE);
														BgL_arg1561z00_1889 =
															MAKE_YOUNG_PAIR(BgL_arg1565z00_1891,
															((obj_t) BgL_envz00_1828));
													}
													if (NULLP(BgL_kapturedz00_8))
														{	/* Globalize/loc2glo.scm 146 */
															BgL_arg1564z00_1890 = BNIL;
														}
													else
														{	/* Globalize/loc2glo.scm 146 */
															obj_t BgL_head1349z00_1895;

															{	/* Globalize/loc2glo.scm 146 */
																obj_t BgL_arg1585z00_1911;

																{	/* Globalize/loc2glo.scm 146 */
																	obj_t BgL_arg1589z00_1912;
																	obj_t BgL_arg1591z00_1913;

																	BgL_arg1589z00_1912 =
																		CAR(((obj_t) BgL_kapturedz00_8));
																	BgL_arg1591z00_1913 =
																		CAR(((obj_t) BgL_newzd2freezd2_1829));
																	BgL_arg1585z00_1911 =
																		MAKE_YOUNG_PAIR(BgL_arg1589z00_1912,
																		BgL_arg1591z00_1913);
																}
																BgL_head1349z00_1895 =
																	MAKE_YOUNG_PAIR(BgL_arg1585z00_1911, BNIL);
															}
															{	/* Globalize/loc2glo.scm 146 */
																obj_t BgL_g1353z00_1896;
																obj_t BgL_g1354z00_1897;

																BgL_g1353z00_1896 =
																	CDR(((obj_t) BgL_kapturedz00_8));
																BgL_g1354z00_1897 =
																	CDR(((obj_t) BgL_newzd2freezd2_1829));
																{
																	obj_t BgL_ll1347z00_1899;
																	obj_t BgL_ll1348z00_1900;
																	obj_t BgL_tail1350z00_1901;

																	BgL_ll1347z00_1899 = BgL_g1353z00_1896;
																	BgL_ll1348z00_1900 = BgL_g1354z00_1897;
																	BgL_tail1350z00_1901 = BgL_head1349z00_1895;
																BgL_zc3z04anonymousza31567ze3z87_1902:
																	if (NULLP(BgL_ll1347z00_1899))
																		{	/* Globalize/loc2glo.scm 146 */
																			BgL_arg1564z00_1890 =
																				BgL_head1349z00_1895;
																		}
																	else
																		{	/* Globalize/loc2glo.scm 146 */
																			obj_t BgL_newtail1351z00_1904;

																			{	/* Globalize/loc2glo.scm 146 */
																				obj_t BgL_arg1575z00_1907;

																				{	/* Globalize/loc2glo.scm 146 */
																					obj_t BgL_arg1576z00_1908;
																					obj_t BgL_arg1584z00_1909;

																					BgL_arg1576z00_1908 =
																						CAR(((obj_t) BgL_ll1347z00_1899));
																					BgL_arg1584z00_1909 =
																						CAR(((obj_t) BgL_ll1348z00_1900));
																					BgL_arg1575z00_1907 =
																						MAKE_YOUNG_PAIR(BgL_arg1576z00_1908,
																						BgL_arg1584z00_1909);
																				}
																				BgL_newtail1351z00_1904 =
																					MAKE_YOUNG_PAIR(BgL_arg1575z00_1907,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1350z00_1901,
																				BgL_newtail1351z00_1904);
																			{	/* Globalize/loc2glo.scm 146 */
																				obj_t BgL_arg1571z00_1905;
																				obj_t BgL_arg1573z00_1906;

																				BgL_arg1571z00_1905 =
																					CDR(((obj_t) BgL_ll1347z00_1899));
																				BgL_arg1573z00_1906 =
																					CDR(((obj_t) BgL_ll1348z00_1900));
																				{
																					obj_t BgL_tail1350z00_3548;
																					obj_t BgL_ll1348z00_3547;
																					obj_t BgL_ll1347z00_3546;

																					BgL_ll1347z00_3546 =
																						BgL_arg1571z00_1905;
																					BgL_ll1348z00_3547 =
																						BgL_arg1573z00_1906;
																					BgL_tail1350z00_3548 =
																						BgL_newtail1351z00_1904;
																					BgL_tail1350z00_1901 =
																						BgL_tail1350z00_3548;
																					BgL_ll1348z00_1900 =
																						BgL_ll1348z00_3547;
																					BgL_ll1347z00_1899 =
																						BgL_ll1347z00_3546;
																					goto
																						BgL_zc3z04anonymousza31567ze3z87_1902;
																				}
																			}
																		}
																}
															}
														}
													BgL_arg1559z00_1888 =
														MAKE_YOUNG_PAIR(BgL_arg1561z00_1889,
														BgL_arg1564z00_1890);
												}
												BgL_arg1553z00_1887 =
													BGl_nodezd2globaliza7ez12z67zzglobaliza7e_nodeza7(
													((BgL_nodez00_bglt) BgL_bodyz00_9),
													((BgL_variablez00_bglt) BgL_localz00_6),
													BgL_arg1559z00_1888);
											}
											BgL_arg1552z00_1886 =
												BGl_makezd2escapingzd2bodyz00zzglobaliza7e_localzd2ze3globalz96
												(BgL_localz00_6, BgL_globalz00_5,
												BgL_newzd2argszd2_1830, BgL_newzd2freezd2_1829,
												BgL_envz00_1828, BgL_arg1553z00_1887);
										}
										((((BgL_sfunz00_bglt) COBJECT(BgL_newzd2funzd2_1832))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_arg1552z00_1886)), BUNSPEC);
									}
									return BgL_globalz00_5;
								}
							}
						}
					}
				}
			}
		}

	}



/* make-escaping-body */
	BgL_nodez00_bglt
		BGl_makezd2escapingzd2bodyz00zzglobaliza7e_localzd2ze3globalz96
		(BgL_localz00_bglt BgL_localz00_10, BgL_globalz00_bglt BgL_globalz00_11,
		obj_t BgL_argsz00_12, obj_t BgL_kapturedz00_13,
		BgL_localz00_bglt BgL_envz00_14, BgL_nodez00_bglt BgL_bodyz00_15)
	{
		{	/* Globalize/loc2glo.scm 152 */
			{	/* Globalize/loc2glo.scm 153 */
				obj_t BgL_stackz00_1987;
				obj_t BgL_locz00_1988;

				{	/* Globalize/loc2glo.scm 153 */
					obj_t BgL_list1735z00_2041;

					BgL_list1735z00_2041 = MAKE_YOUNG_PAIR(((obj_t) BgL_envz00_14), BNIL);
					BgL_stackz00_1987 = BgL_list1735z00_2041;
				}
				BgL_locz00_1988 =
					(((BgL_nodez00_bglt) COBJECT(BgL_bodyz00_15))->BgL_locz00);
				{	/* Globalize/loc2glo.scm 155 */
					BgL_valuez00_bglt BgL_arg1663z00_1989;

					BgL_arg1663z00_1989 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_localz00_10)))->BgL_valuez00);
					((((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_arg1663z00_1989)))->BgL_bodyz00) =
						((obj_t) ((obj_t) BgL_bodyz00_15)), BUNSPEC);
				}
				{	/* Globalize/loc2glo.scm 156 */
					BgL_letzd2varzd2_bglt BgL_new1154z00_1990;

					{	/* Globalize/loc2glo.scm 157 */
						BgL_letzd2varzd2_bglt BgL_new1153z00_2039;

						BgL_new1153z00_2039 =
							((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_letzd2varzd2_bgl))));
						{	/* Globalize/loc2glo.scm 157 */
							long BgL_arg1734z00_2040;

							BgL_arg1734z00_2040 =
								BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1153z00_2039),
								BgL_arg1734z00_2040);
						}
						{	/* Globalize/loc2glo.scm 157 */
							BgL_objectz00_bglt BgL_tmpz00_3568;

							BgL_tmpz00_3568 = ((BgL_objectz00_bglt) BgL_new1153z00_2039);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3568, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1153z00_2039);
						BgL_new1154z00_1990 = BgL_new1153z00_2039;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1154z00_1990)))->BgL_locz00) =
						((obj_t) BgL_locz00_1988), BUNSPEC);
					{
						BgL_typez00_bglt BgL_auxz00_3574;

						{	/* Globalize/loc2glo.scm 158 */
							BgL_typez00_bglt BgL_arg1675z00_1991;

							BgL_arg1675z00_1991 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_localz00_10)))->BgL_typez00);
							BgL_auxz00_3574 =
								BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg1675z00_1991,
								((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00));
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1154z00_1990)))->BgL_typez00) =
							((BgL_typez00_bglt) BgL_auxz00_3574), BUNSPEC);
					}
					((((BgL_nodezf2effectzf2_bglt) COBJECT(
									((BgL_nodezf2effectzf2_bglt) BgL_new1154z00_1990)))->
							BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1154z00_1990)))->BgL_keyz00) =
						((obj_t) BINT(-1L)), BUNSPEC);
					{
						obj_t BgL_auxz00_3586;

						{
							obj_t BgL_kapturedz00_1994;
							long BgL_numz00_1995;
							obj_t BgL_resz00_1996;

							BgL_kapturedz00_1994 = BgL_kapturedz00_13;
							BgL_numz00_1995 = 0L;
							BgL_resz00_1996 = BNIL;
						BgL_zc3z04anonymousza31676ze3z87_1997:
							if (NULLP(BgL_kapturedz00_1994))
								{	/* Globalize/loc2glo.scm 162 */
									BgL_auxz00_3586 = bgl_reverse_bang(BgL_resz00_1996);
								}
							else
								{	/* Globalize/loc2glo.scm 164 */
									obj_t BgL_arg1678z00_1999;
									long BgL_arg1681z00_2000;
									obj_t BgL_arg1688z00_2001;

									BgL_arg1678z00_1999 = CDR(((obj_t) BgL_kapturedz00_1994));
									BgL_arg1681z00_2000 = (BgL_numz00_1995 + 1L);
									{	/* Globalize/loc2glo.scm 166 */
										obj_t BgL_arg1689z00_2002;

										{	/* Globalize/loc2glo.scm 166 */
											obj_t BgL_arg1691z00_2003;
											BgL_nodez00_bglt BgL_arg1692z00_2004;

											BgL_arg1691z00_2003 = CAR(((obj_t) BgL_kapturedz00_1994));
											{	/* Globalize/loc2glo.scm 168 */
												obj_t BgL_arg1699z00_2005;

												{	/* Globalize/loc2glo.scm 168 */
													obj_t BgL_arg1700z00_2006;

													{	/* Globalize/loc2glo.scm 168 */
														obj_t BgL_arg1701z00_2007;

														BgL_arg1701z00_2007 =
															MAKE_YOUNG_PAIR(BINT(BgL_numz00_1995), BNIL);
														BgL_arg1700z00_2006 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
															BgL_arg1701z00_2007);
													}
													BgL_arg1699z00_2005 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
														BgL_arg1700z00_2006);
												}
												BgL_arg1692z00_2004 =
													BGl_sexpzd2ze3nodez31zzast_sexpz00
													(BgL_arg1699z00_2005, BgL_stackz00_1987,
													BgL_locz00_1988, CNST_TABLE_REF(2));
											}
											BgL_arg1689z00_2002 =
												MAKE_YOUNG_PAIR(BgL_arg1691z00_2003,
												((obj_t) BgL_arg1692z00_2004));
										}
										BgL_arg1688z00_2001 =
											MAKE_YOUNG_PAIR(BgL_arg1689z00_2002, BgL_resz00_1996);
									}
									{
										obj_t BgL_resz00_3608;
										long BgL_numz00_3607;
										obj_t BgL_kapturedz00_3606;

										BgL_kapturedz00_3606 = BgL_arg1678z00_1999;
										BgL_numz00_3607 = BgL_arg1681z00_2000;
										BgL_resz00_3608 = BgL_arg1688z00_2001;
										BgL_resz00_1996 = BgL_resz00_3608;
										BgL_numz00_1995 = BgL_numz00_3607;
										BgL_kapturedz00_1994 = BgL_kapturedz00_3606;
										goto BgL_zc3z04anonymousza31676ze3z87_1997;
									}
								}
						}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1154z00_1990))->
								BgL_bindingsz00) = ((obj_t) BgL_auxz00_3586), BUNSPEC);
					}
					{
						BgL_nodez00_bglt BgL_auxz00_3610;

						{	/* Globalize/loc2glo.scm 173 */
							BgL_letzd2funzd2_bglt BgL_new1157z00_2009;

							{	/* Globalize/loc2glo.scm 174 */
								BgL_letzd2funzd2_bglt BgL_new1156z00_2037;

								BgL_new1156z00_2037 =
									((BgL_letzd2funzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_letzd2funzd2_bgl))));
								{	/* Globalize/loc2glo.scm 174 */
									long BgL_arg1733z00_2038;

									{	/* Globalize/loc2glo.scm 174 */
										obj_t BgL_classz00_2500;

										BgL_classz00_2500 = BGl_letzd2funzd2zzast_nodez00;
										BgL_arg1733z00_2038 = BGL_CLASS_NUM(BgL_classz00_2500);
									}
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1156z00_2037),
										BgL_arg1733z00_2038);
								}
								{	/* Globalize/loc2glo.scm 174 */
									BgL_objectz00_bglt BgL_tmpz00_3615;

									BgL_tmpz00_3615 = ((BgL_objectz00_bglt) BgL_new1156z00_2037);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3615, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1156z00_2037);
								BgL_new1157z00_2009 = BgL_new1156z00_2037;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1157z00_2009)))->BgL_locz00) =
								((obj_t) BgL_locz00_1988), BUNSPEC);
							{
								BgL_typez00_bglt BgL_auxz00_3621;

								{	/* Globalize/loc2glo.scm 175 */
									BgL_typez00_bglt BgL_arg1702z00_2010;

									BgL_arg1702z00_2010 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_localz00_10)))->
										BgL_typez00);
									BgL_auxz00_3621 =
										BGl_strictzd2nodezd2typez00zzast_nodez00
										(BgL_arg1702z00_2010,
										((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00));
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1157z00_2009)))->
										BgL_typez00) =
									((BgL_typez00_bglt) BgL_auxz00_3621), BUNSPEC);
							}
							((((BgL_nodezf2effectzf2_bglt) COBJECT(
											((BgL_nodezf2effectzf2_bglt) BgL_new1157z00_2009)))->
									BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
							((((BgL_nodezf2effectzf2_bglt)
										COBJECT(((BgL_nodezf2effectzf2_bglt)
												BgL_new1157z00_2009)))->BgL_keyz00) =
								((obj_t) BINT(-1L)), BUNSPEC);
							{
								obj_t BgL_auxz00_3633;

								{	/* Globalize/loc2glo.scm 176 */
									obj_t BgL_list1703z00_2011;

									BgL_list1703z00_2011 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_localz00_10), BNIL);
									BgL_auxz00_3633 = BgL_list1703z00_2011;
								}
								((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1157z00_2009))->
										BgL_localsz00) = ((obj_t) BgL_auxz00_3633), BUNSPEC);
							}
							{
								BgL_nodez00_bglt BgL_auxz00_3637;

								{	/* Globalize/loc2glo.scm 177 */
									BgL_appz00_bglt BgL_new1159z00_2012;

									{	/* Globalize/loc2glo.scm 178 */
										BgL_appz00_bglt BgL_new1158z00_2035;

										BgL_new1158z00_2035 =
											((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_appz00_bgl))));
										{	/* Globalize/loc2glo.scm 178 */
											long BgL_arg1724z00_2036;

											{	/* Globalize/loc2glo.scm 178 */
												obj_t BgL_classz00_2506;

												BgL_classz00_2506 = BGl_appz00zzast_nodez00;
												BgL_arg1724z00_2036 = BGL_CLASS_NUM(BgL_classz00_2506);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1158z00_2035),
												BgL_arg1724z00_2036);
										}
										{	/* Globalize/loc2glo.scm 178 */
											BgL_objectz00_bglt BgL_tmpz00_3642;

											BgL_tmpz00_3642 =
												((BgL_objectz00_bglt) BgL_new1158z00_2035);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3642, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1158z00_2035);
										BgL_new1159z00_2012 = BgL_new1158z00_2035;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1159z00_2012)))->
											BgL_locz00) = ((obj_t) BgL_locz00_1988), BUNSPEC);
									{
										BgL_typez00_bglt BgL_auxz00_3648;

										{	/* Globalize/loc2glo.scm 179 */
											BgL_typez00_bglt BgL_arg1705z00_2013;

											BgL_arg1705z00_2013 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_localz00_10)))->
												BgL_typez00);
											BgL_auxz00_3648 =
												BGl_strictzd2nodezd2typez00zzast_nodez00
												(BgL_arg1705z00_2013,
												((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00));
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1159z00_2012)))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_auxz00_3648), BUNSPEC);
									}
									((((BgL_nodezf2effectzf2_bglt) COBJECT(
													((BgL_nodezf2effectzf2_bglt) BgL_new1159z00_2012)))->
											BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_nodezf2effectzf2_bglt)
												COBJECT(((BgL_nodezf2effectzf2_bglt)
														BgL_new1159z00_2012)))->BgL_keyz00) =
										((obj_t) BINT(-1L)), BUNSPEC);
									{
										BgL_varz00_bglt BgL_auxz00_3660;

										{	/* Globalize/loc2glo.scm 180 */
											BgL_refz00_bglt BgL_new1161z00_2014;

											{	/* Globalize/loc2glo.scm 181 */
												BgL_refz00_bglt BgL_new1160z00_2016;

												BgL_new1160z00_2016 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Globalize/loc2glo.scm 181 */
													long BgL_arg1709z00_2017;

													{	/* Globalize/loc2glo.scm 181 */
														obj_t BgL_classz00_2511;

														BgL_classz00_2511 = BGl_refz00zzast_nodez00;
														BgL_arg1709z00_2017 =
															BGL_CLASS_NUM(BgL_classz00_2511);
													}
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1160z00_2016),
														BgL_arg1709z00_2017);
												}
												{	/* Globalize/loc2glo.scm 181 */
													BgL_objectz00_bglt BgL_tmpz00_3665;

													BgL_tmpz00_3665 =
														((BgL_objectz00_bglt) BgL_new1160z00_2016);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3665, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1160z00_2016);
												BgL_new1161z00_2014 = BgL_new1160z00_2016;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1161z00_2014)))->
													BgL_locz00) = ((obj_t) BgL_locz00_1988), BUNSPEC);
											{
												BgL_typez00_bglt BgL_auxz00_3671;

												{	/* Globalize/loc2glo.scm 183 */
													BgL_typez00_bglt BgL_arg1708z00_2015;

													BgL_arg1708z00_2015 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_localz00_10)))->
														BgL_typez00);
													BgL_auxz00_3671 =
														BGl_strictzd2nodezd2typez00zzast_nodez00
														(BgL_arg1708z00_2015,
														((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00));
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1161z00_2014)))->
														BgL_typez00) =
													((BgL_typez00_bglt) BgL_auxz00_3671), BUNSPEC);
											}
											((((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_new1161z00_2014)))->
													BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_localz00_10)), BUNSPEC);
											BgL_auxz00_3660 = ((BgL_varz00_bglt) BgL_new1161z00_2014);
										}
										((((BgL_appz00_bglt) COBJECT(BgL_new1159z00_2012))->
												BgL_funz00) =
											((BgL_varz00_bglt) BgL_auxz00_3660), BUNSPEC);
									}
									{
										obj_t BgL_auxz00_3683;

										if (NULLP(BgL_argsz00_12))
											{	/* Globalize/loc2glo.scm 185 */
												BgL_auxz00_3683 = BNIL;
											}
										else
											{	/* Globalize/loc2glo.scm 185 */
												obj_t BgL_head1357z00_2020;

												BgL_head1357z00_2020 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1355z00_2022;
													obj_t BgL_tail1358z00_2023;

													BgL_l1355z00_2022 = BgL_argsz00_12;
													BgL_tail1358z00_2023 = BgL_head1357z00_2020;
												BgL_zc3z04anonymousza31712ze3z87_2024:
													if (NULLP(BgL_l1355z00_2022))
														{	/* Globalize/loc2glo.scm 185 */
															BgL_auxz00_3683 = CDR(BgL_head1357z00_2020);
														}
													else
														{	/* Globalize/loc2glo.scm 185 */
															obj_t BgL_newtail1359z00_2026;

															{	/* Globalize/loc2glo.scm 185 */
																BgL_refz00_bglt BgL_arg1718z00_2028;

																{	/* Globalize/loc2glo.scm 185 */
																	obj_t BgL_vz00_2029;

																	BgL_vz00_2029 =
																		CAR(((obj_t) BgL_l1355z00_2022));
																	{	/* Globalize/loc2glo.scm 186 */
																		BgL_refz00_bglt BgL_new1163z00_2030;

																		{	/* Globalize/loc2glo.scm 190 */
																			BgL_refz00_bglt BgL_new1162z00_2032;

																			BgL_new1162z00_2032 =
																				((BgL_refz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_refz00_bgl))));
																			{	/* Globalize/loc2glo.scm 190 */
																				long BgL_arg1722z00_2033;

																				{	/* Globalize/loc2glo.scm 190 */
																					obj_t BgL_classz00_2518;

																					BgL_classz00_2518 =
																						BGl_refz00zzast_nodez00;
																					BgL_arg1722z00_2033 =
																						BGL_CLASS_NUM(BgL_classz00_2518);
																				}
																				BGL_OBJECT_CLASS_NUM_SET(
																					((BgL_objectz00_bglt)
																						BgL_new1162z00_2032),
																					BgL_arg1722z00_2033);
																			}
																			{	/* Globalize/loc2glo.scm 190 */
																				BgL_objectz00_bglt BgL_tmpz00_3696;

																				BgL_tmpz00_3696 =
																					((BgL_objectz00_bglt)
																					BgL_new1162z00_2032);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3696,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1162z00_2032);
																			BgL_new1163z00_2030 = BgL_new1162z00_2032;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1163z00_2030)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_1988), BUNSPEC);
																		{
																			BgL_typez00_bglt BgL_auxz00_3702;

																			{	/* Globalize/loc2glo.scm 189 */
																				BgL_typez00_bglt BgL_arg1720z00_2031;

																				BgL_arg1720z00_2031 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								BgL_vz00_2029)))->BgL_typez00);
																				BgL_auxz00_3702 =
																					BGl_strictzd2nodezd2typez00zzast_nodez00
																					(BgL_arg1720z00_2031,
																					((BgL_typez00_bglt)
																						BGl_za2_za2z00zztype_cachez00));
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1163z00_2030)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) BgL_auxz00_3702),
																				BUNSPEC);
																		}
																		((((BgL_varz00_bglt) COBJECT(
																						((BgL_varz00_bglt)
																							BgL_new1163z00_2030)))->
																				BgL_variablez00) =
																			((BgL_variablez00_bglt) (
																					(BgL_variablez00_bglt)
																					BgL_vz00_2029)), BUNSPEC);
																		BgL_arg1718z00_2028 = BgL_new1163z00_2030;
																}}
																BgL_newtail1359z00_2026 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1718z00_2028), BNIL);
															}
															SET_CDR(BgL_tail1358z00_2023,
																BgL_newtail1359z00_2026);
															{	/* Globalize/loc2glo.scm 185 */
																obj_t BgL_arg1717z00_2027;

																BgL_arg1717z00_2027 =
																	CDR(((obj_t) BgL_l1355z00_2022));
																{
																	obj_t BgL_tail1358z00_3718;
																	obj_t BgL_l1355z00_3717;

																	BgL_l1355z00_3717 = BgL_arg1717z00_2027;
																	BgL_tail1358z00_3718 =
																		BgL_newtail1359z00_2026;
																	BgL_tail1358z00_2023 = BgL_tail1358z00_3718;
																	BgL_l1355z00_2022 = BgL_l1355z00_3717;
																	goto BgL_zc3z04anonymousza31712ze3z87_2024;
																}
															}
														}
												}
											}
										((((BgL_appz00_bglt) COBJECT(BgL_new1159z00_2012))->
												BgL_argsz00) = ((obj_t) BgL_auxz00_3683), BUNSPEC);
									}
									((((BgL_appz00_bglt) COBJECT(BgL_new1159z00_2012))->
											BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
									BgL_auxz00_3637 = ((BgL_nodez00_bglt) BgL_new1159z00_2012);
								}
								((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1157z00_2009))->
										BgL_bodyz00) =
									((BgL_nodez00_bglt) BgL_auxz00_3637), BUNSPEC);
							}
							BgL_auxz00_3610 = ((BgL_nodez00_bglt) BgL_new1157z00_2009);
						}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1154z00_1990))->
								BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_3610), BUNSPEC);
					}
					((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1154z00_1990))->
							BgL_removablezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					return ((BgL_nodez00_bglt) BgL_new1154z00_1990);
				}
			}
		}

	}



/* fix-non-escaping-definition */
	BgL_globalz00_bglt
		BGl_fixzd2nonzd2escapingzd2definitionzd2zzglobaliza7e_localzd2ze3globalz96
		(BgL_globalz00_bglt BgL_globalz00_16, BgL_localz00_bglt BgL_localz00_17,
		obj_t BgL_argsz00_18, obj_t BgL_kapturedz00_19, obj_t BgL_bodyz00_20)
	{
		{	/* Globalize/loc2glo.scm 196 */
			{	/* Globalize/loc2glo.scm 197 */
				obj_t BgL_addzd2argszd2_2042;

				if (NULLP(BgL_kapturedz00_19))
					{	/* Globalize/loc2glo.scm 197 */
						BgL_addzd2argszd2_2042 = BNIL;
					}
				else
					{	/* Globalize/loc2glo.scm 197 */
						obj_t BgL_head1362z00_2092;

						BgL_head1362z00_2092 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1360z00_2094;
							obj_t BgL_tail1363z00_2095;

							BgL_l1360z00_2094 = BgL_kapturedz00_19;
							BgL_tail1363z00_2095 = BgL_head1362z00_2092;
						BgL_zc3z04anonymousza31764ze3z87_2096:
							if (NULLP(BgL_l1360z00_2094))
								{	/* Globalize/loc2glo.scm 197 */
									BgL_addzd2argszd2_2042 = CDR(BgL_head1362z00_2092);
								}
							else
								{	/* Globalize/loc2glo.scm 197 */
									obj_t BgL_newtail1364z00_2098;

									{	/* Globalize/loc2glo.scm 197 */
										BgL_localz00_bglt BgL_arg1770z00_2100;

										{	/* Globalize/loc2glo.scm 197 */
											obj_t BgL_oldz00_2101;

											BgL_oldz00_2101 = CAR(((obj_t) BgL_l1360z00_2094));
											{	/* Globalize/loc2glo.scm 200 */
												BgL_localz00_bglt BgL_newz00_2102;

												{	/* Globalize/loc2glo.scm 201 */
													obj_t BgL_arg1798z00_2115;
													BgL_typez00_bglt BgL_arg1799z00_2116;

													BgL_arg1798z00_2115 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_oldz00_2101))))->
														BgL_idz00);
													BgL_arg1799z00_2116 =
														(((BgL_variablez00_bglt)
															COBJECT(((BgL_variablez00_bglt) (
																		(BgL_localz00_bglt) BgL_oldz00_2101))))->
														BgL_typez00);
													BgL_newz00_2102 =
														BGl_makezd2localzd2svarz00zzast_localz00
														(BgL_arg1798z00_2115, BgL_arg1799z00_2116);
												}
												{	/* Globalize/loc2glo.scm 203 */
													bool_t BgL_arg1771z00_2103;

													BgL_arg1771z00_2103 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_oldz00_2101))))->
														BgL_userzf3zf3);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_newz00_2102)))->BgL_userzf3zf3) =
														((bool_t) BgL_arg1771z00_2103), BUNSPEC);
												}
												{	/* Globalize/loc2glo.scm 204 */
													BgL_localzf2ginfozf2_bglt BgL_wide1166z00_2106;

													BgL_wide1166z00_2106 =
														((BgL_localzf2ginfozf2_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_localzf2ginfozf2_bgl))));
													{	/* Globalize/loc2glo.scm 204 */
														obj_t BgL_auxz00_3751;
														BgL_objectz00_bglt BgL_tmpz00_3748;

														BgL_auxz00_3751 = ((obj_t) BgL_wide1166z00_2106);
														BgL_tmpz00_3748 =
															((BgL_objectz00_bglt)
															((BgL_localz00_bglt) BgL_newz00_2102));
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3748,
															BgL_auxz00_3751);
													}
													((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_newz00_2102));
													{	/* Globalize/loc2glo.scm 204 */
														long BgL_arg1773z00_2107;

														BgL_arg1773z00_2107 =
															BGL_CLASS_NUM
															(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
														BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																	(BgL_localz00_bglt) BgL_newz00_2102)),
															BgL_arg1773z00_2107);
													}
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_newz00_2102));
												}
												{
													BgL_localzf2ginfozf2_bglt BgL_auxz00_3762;

													{
														obj_t BgL_auxz00_3763;

														{	/* Globalize/loc2glo.scm 204 */
															BgL_objectz00_bglt BgL_tmpz00_3764;

															BgL_tmpz00_3764 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_newz00_2102));
															BgL_auxz00_3763 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3764);
														}
														BgL_auxz00_3762 =
															((BgL_localzf2ginfozf2_bglt) BgL_auxz00_3763);
													}
													((((BgL_localzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_3762))->BgL_escapezf3zf3) =
														((bool_t) ((bool_t) 0)), BUNSPEC);
												}
												{
													BgL_localzf2ginfozf2_bglt BgL_auxz00_3770;

													{
														obj_t BgL_auxz00_3771;

														{	/* Globalize/loc2glo.scm 204 */
															BgL_objectz00_bglt BgL_tmpz00_3772;

															BgL_tmpz00_3772 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_newz00_2102));
															BgL_auxz00_3771 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3772);
														}
														BgL_auxz00_3770 =
															((BgL_localzf2ginfozf2_bglt) BgL_auxz00_3771);
													}
													((((BgL_localzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_3770))->
															BgL_globaliza7edzf3z54) =
														((bool_t) ((bool_t) 0)), BUNSPEC);
												}
												((BgL_localz00_bglt) BgL_newz00_2102);
												{	/* Globalize/loc2glo.scm 205 */
													BgL_svarz00_bglt BgL_tmp1168z00_2109;

													BgL_tmp1168z00_2109 =
														((BgL_svarz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt) BgL_newz00_2102)))->
															BgL_valuez00));
													{	/* Globalize/loc2glo.scm 205 */
														BgL_svarzf2ginfozf2_bglt BgL_wide1170z00_2111;

														BgL_wide1170z00_2111 =
															((BgL_svarzf2ginfozf2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_svarzf2ginfozf2_bgl))));
														{	/* Globalize/loc2glo.scm 205 */
															obj_t BgL_auxz00_3786;
															BgL_objectz00_bglt BgL_tmpz00_3783;

															BgL_auxz00_3786 = ((obj_t) BgL_wide1170z00_2111);
															BgL_tmpz00_3783 =
																((BgL_objectz00_bglt)
																((BgL_svarz00_bglt) BgL_tmp1168z00_2109));
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3783,
																BgL_auxz00_3786);
														}
														((BgL_objectz00_bglt)
															((BgL_svarz00_bglt) BgL_tmp1168z00_2109));
														{	/* Globalize/loc2glo.scm 205 */
															long BgL_arg1775z00_2112;

															BgL_arg1775z00_2112 =
																BGL_CLASS_NUM
																(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																		(BgL_svarz00_bglt) BgL_tmp1168z00_2109)),
																BgL_arg1775z00_2112);
														}
														((BgL_svarz00_bglt)
															((BgL_svarz00_bglt) BgL_tmp1168z00_2109));
													}
													{
														BgL_svarzf2ginfozf2_bglt BgL_auxz00_3797;

														{
															obj_t BgL_auxz00_3798;

															{	/* Globalize/loc2glo.scm 206 */
																BgL_objectz00_bglt BgL_tmpz00_3799;

																BgL_tmpz00_3799 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1168z00_2109));
																BgL_auxz00_3798 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3799);
															}
															BgL_auxz00_3797 =
																((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3798);
														}
														((((BgL_svarzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_3797))->
																BgL_kapturedzf3zf3) =
															((bool_t) ((bool_t) 1)), BUNSPEC);
													}
													{
														BgL_svarzf2ginfozf2_bglt BgL_auxz00_3805;

														{
															obj_t BgL_auxz00_3806;

															{	/* Globalize/loc2glo.scm 206 */
																BgL_objectz00_bglt BgL_tmpz00_3807;

																BgL_tmpz00_3807 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1168z00_2109));
																BgL_auxz00_3806 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3807);
															}
															BgL_auxz00_3805 =
																((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3806);
														}
														((((BgL_svarzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_3805))->
																BgL_freezd2markzd2) = ((long) -10L), BUNSPEC);
													}
													{
														BgL_svarzf2ginfozf2_bglt BgL_auxz00_3813;

														{
															obj_t BgL_auxz00_3814;

															{	/* Globalize/loc2glo.scm 206 */
																BgL_objectz00_bglt BgL_tmpz00_3815;

																BgL_tmpz00_3815 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1168z00_2109));
																BgL_auxz00_3814 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3815);
															}
															BgL_auxz00_3813 =
																((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3814);
														}
														((((BgL_svarzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_3813))->BgL_markz00) =
															((long) -10L), BUNSPEC);
													}
													{
														BgL_svarzf2ginfozf2_bglt BgL_auxz00_3821;

														{
															obj_t BgL_auxz00_3822;

															{	/* Globalize/loc2glo.scm 206 */
																BgL_objectz00_bglt BgL_tmpz00_3823;

																BgL_tmpz00_3823 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1168z00_2109));
																BgL_auxz00_3822 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3823);
															}
															BgL_auxz00_3821 =
																((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3822);
														}
														((((BgL_svarzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_3821))->BgL_celledzf3zf3) =
															((bool_t) ((bool_t) 0)), BUNSPEC);
													}
													{
														BgL_svarzf2ginfozf2_bglt BgL_auxz00_3829;

														{
															obj_t BgL_auxz00_3830;

															{	/* Globalize/loc2glo.scm 206 */
																BgL_objectz00_bglt BgL_tmpz00_3831;

																BgL_tmpz00_3831 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1168z00_2109));
																BgL_auxz00_3830 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3831);
															}
															BgL_auxz00_3829 =
																((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3830);
														}
														((((BgL_svarzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_3829))->BgL_stackablez00) =
															((bool_t) ((bool_t) 1)), BUNSPEC);
													}
													((BgL_svarz00_bglt) BgL_tmp1168z00_2109);
												}
												{	/* Globalize/loc2glo.scm 208 */
													obj_t BgL_casezd2valuezd2_2114;

													BgL_casezd2valuezd2_2114 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_oldz00_2101))))->
														BgL_accessz00);
													if ((BgL_casezd2valuezd2_2114 == CNST_TABLE_REF(3)))
														{	/* Globalize/loc2glo.scm 208 */
															{	/* Globalize/loc2glo.scm 211 */
																BgL_typez00_bglt BgL_vz00_2550;

																BgL_vz00_2550 =
																	((BgL_typez00_bglt)
																	BGl_za2cellza2z00zztype_cachez00);
																((((BgL_variablez00_bglt)
																			COBJECT(((BgL_variablez00_bglt)
																					BgL_newz00_2102)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_vz00_2550), BUNSPEC);
															}
															{	/* Globalize/loc2glo.scm 212 */
																obj_t BgL_vz00_2552;

																BgL_vz00_2552 = CNST_TABLE_REF(4);
																((((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					BgL_newz00_2102)))->BgL_accessz00) =
																	((obj_t) BgL_vz00_2552), BUNSPEC);
															}
														}
													else
														{	/* Globalize/loc2glo.scm 208 */
															if (
																(BgL_casezd2valuezd2_2114 == CNST_TABLE_REF(4)))
																{	/* Globalize/loc2glo.scm 208 */
																	{	/* Globalize/loc2glo.scm 215 */
																		BgL_typez00_bglt BgL_vz00_2554;

																		BgL_vz00_2554 =
																			((BgL_typez00_bglt)
																			BGl_za2cellza2z00zztype_cachez00);
																		((((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt)
																							BgL_newz00_2102)))->BgL_typez00) =
																			((BgL_typez00_bglt) BgL_vz00_2554),
																			BUNSPEC);
																	}
																	{	/* Globalize/loc2glo.scm 216 */
																		obj_t BgL_vz00_2556;

																		BgL_vz00_2556 = CNST_TABLE_REF(4);
																		((((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							BgL_newz00_2102)))->
																				BgL_accessz00) =
																			((obj_t) BgL_vz00_2556), BUNSPEC);
																	}
																}
															else
																{	/* Globalize/loc2glo.scm 208 */
																	BUNSPEC;
																}
														}
												}
												BgL_arg1770z00_2100 = BgL_newz00_2102;
											}
										}
										BgL_newtail1364z00_2098 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg1770z00_2100), BNIL);
									}
									SET_CDR(BgL_tail1363z00_2095, BgL_newtail1364z00_2098);
									{	/* Globalize/loc2glo.scm 197 */
										obj_t BgL_arg1767z00_2099;

										BgL_arg1767z00_2099 = CDR(((obj_t) BgL_l1360z00_2094));
										{
											obj_t BgL_tail1363z00_3865;
											obj_t BgL_l1360z00_3864;

											BgL_l1360z00_3864 = BgL_arg1767z00_2099;
											BgL_tail1363z00_3865 = BgL_newtail1364z00_2098;
											BgL_tail1363z00_2095 = BgL_tail1363z00_3865;
											BgL_l1360z00_2094 = BgL_l1360z00_3864;
											goto BgL_zc3z04anonymousza31764ze3z87_2096;
										}
									}
								}
						}
					}
				{	/* Globalize/loc2glo.scm 197 */
					BgL_valuez00_bglt BgL_oldzd2funzd2_2043;

					BgL_oldzd2funzd2_2043 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_localz00_17)))->BgL_valuez00);
					{	/* Globalize/loc2glo.scm 220 */
						BgL_sfunz00_bglt BgL_newzd2funzd2_2044;

						{	/* Globalize/loc2glo.scm 221 */
							BgL_sfunz00_bglt BgL_new1172z00_2073;

							{	/* Globalize/loc2glo.scm 222 */
								BgL_sfunz00_bglt BgL_new1186z00_2088;

								BgL_new1186z00_2088 =
									((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sfunz00_bgl))));
								{	/* Globalize/loc2glo.scm 222 */
									long BgL_arg1762z00_2089;

									BgL_arg1762z00_2089 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1186z00_2088),
										BgL_arg1762z00_2089);
								}
								{	/* Globalize/loc2glo.scm 222 */
									BgL_objectz00_bglt BgL_tmpz00_3872;

									BgL_tmpz00_3872 = ((BgL_objectz00_bglt) BgL_new1186z00_2088);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3872, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1186z00_2088);
								BgL_new1172z00_2073 = BgL_new1186z00_2088;
							}
							{
								long BgL_auxz00_3876;

								{	/* Globalize/loc2glo.scm 222 */
									long BgL_arg1754z00_2074;
									long BgL_arg1755z00_2075;

									BgL_arg1754z00_2074 =
										(((BgL_funz00_bglt) COBJECT(
												((BgL_funz00_bglt)
													((BgL_sfunz00_bglt) BgL_oldzd2funzd2_2043))))->
										BgL_arityz00);
									BgL_arg1755z00_2075 = bgl_list_length(BgL_kapturedz00_19);
									BgL_auxz00_3876 =
										(long)
										CINT(BGl_zb2zd2arityz60zztools_argsz00(BINT
											(BgL_arg1754z00_2074), BINT(BgL_arg1755z00_2075)));
								}
								((((BgL_funz00_bglt) COBJECT(
												((BgL_funz00_bglt) BgL_new1172z00_2073)))->
										BgL_arityz00) = ((long) BgL_auxz00_3876), BUNSPEC);
							}
							((((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt) BgL_new1172z00_2073)))->
									BgL_sidezd2effectzd2) =
								((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
													BgL_oldzd2funzd2_2043)))->BgL_sidezd2effectzd2)),
								BUNSPEC);
							((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_new1172z00_2073)))->BgL_predicatezd2ofzd2) =
								((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
													BgL_oldzd2funzd2_2043)))->BgL_predicatezd2ofzd2)),
								BUNSPEC);
							((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_new1172z00_2073)))->BgL_stackzd2allocatorzd2) =
								((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
													BgL_oldzd2funzd2_2043)))->BgL_stackzd2allocatorzd2)),
								BUNSPEC);
							((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_new1172z00_2073)))->BgL_topzf3zf3) =
								((bool_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
													BgL_oldzd2funzd2_2043)))->BgL_topzf3zf3)), BUNSPEC);
							((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_new1172z00_2073)))->BgL_thezd2closurezd2) =
								((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
													BgL_oldzd2funzd2_2043)))->BgL_thezd2closurezd2)),
								BUNSPEC);
							((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_new1172z00_2073)))->BgL_effectz00) =
								((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
													BgL_oldzd2funzd2_2043)))->BgL_effectz00)), BUNSPEC);
							((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_new1172z00_2073)))->BgL_failsafez00) =
								((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
													BgL_oldzd2funzd2_2043)))->BgL_failsafez00)), BUNSPEC);
							((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_new1172z00_2073)))->BgL_argszd2noescapezd2) =
								((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
													BgL_oldzd2funzd2_2043)))->BgL_argszd2noescapezd2)),
								BUNSPEC);
							((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_new1172z00_2073)))->BgL_argszd2retescapezd2) =
								((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
													BgL_oldzd2funzd2_2043)))->BgL_argszd2retescapezd2)),
								BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_propertyz00) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->BgL_propertyz00)),
								BUNSPEC);
							{
								obj_t BgL_auxz00_3927;

								{	/* Globalize/loc2glo.scm 223 */
									obj_t BgL_arg1761z00_2077;

									BgL_arg1761z00_2077 = bgl_reverse(BgL_addzd2argszd2_2042);
									BgL_auxz00_3927 =
										BGl_appendzd221011zd2zzglobaliza7e_localzd2ze3globalz96
										(BgL_arg1761z00_2077, BgL_argsz00_18);
								}
								((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
										BgL_argsz00) = ((obj_t) BgL_auxz00_3927), BUNSPEC);
							}
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_argszd2namezd2) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->BgL_argszd2namezd2)),
								BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_bodyz00) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->BgL_bodyz00)), BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_classz00) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->BgL_classz00)), BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_dssslzd2keywordszd2) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->
										BgL_dssslzd2keywordszd2)), BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->BgL_locz00) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->BgL_locz00)), BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_optionalsz00) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->BgL_optionalsz00)),
								BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_keysz00) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->BgL_keysz00)), BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_thezd2closurezd2globalz00) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->
										BgL_thezd2closurezd2globalz00)), BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_strengthz00) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->BgL_strengthz00)),
								BUNSPEC);
							((((BgL_sfunz00_bglt) COBJECT(BgL_new1172z00_2073))->
									BgL_stackablez00) =
								((obj_t) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
														BgL_oldzd2funzd2_2043))))->BgL_stackablez00)),
								BUNSPEC);
							BgL_newzd2funzd2_2044 = BgL_new1172z00_2073;
						}
						{	/* Globalize/loc2glo.scm 221 */

							{
								BgL_localzf2ginfozf2_bglt BgL_auxz00_3971;

								{
									obj_t BgL_auxz00_3972;

									{	/* Globalize/loc2glo.scm 224 */
										BgL_objectz00_bglt BgL_tmpz00_3973;

										BgL_tmpz00_3973 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_localz00_17));
										BgL_auxz00_3972 = BGL_OBJECT_WIDENING(BgL_tmpz00_3973);
									}
									BgL_auxz00_3971 =
										((BgL_localzf2ginfozf2_bglt) BgL_auxz00_3972);
								}
								((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3971))->
										BgL_escapezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							{
								BgL_globalzf2ginfozf2_bglt BgL_auxz00_3979;

								{
									obj_t BgL_auxz00_3980;

									{	/* Globalize/loc2glo.scm 225 */
										BgL_objectz00_bglt BgL_tmpz00_3981;

										BgL_tmpz00_3981 =
											((BgL_objectz00_bglt)
											((BgL_globalz00_bglt) BgL_globalz00_16));
										BgL_auxz00_3980 = BGL_OBJECT_WIDENING(BgL_tmpz00_3981);
									}
									BgL_auxz00_3979 =
										((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_3980);
								}
								((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3979))->
										BgL_escapezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
							}
							{	/* Globalize/loc2glo.scm 226 */
								BgL_typez00_bglt BgL_arg1736z00_2045;

								BgL_arg1736z00_2045 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_localz00_17)))->BgL_typez00);
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_globalz00_16)))->
										BgL_typez00) =
									((BgL_typez00_bglt) BgL_arg1736z00_2045), BUNSPEC);
							}
							{	/* Globalize/loc2glo.scm 229 */
								BgL_nodez00_bglt BgL_arg1737z00_2046;

								{	/* Globalize/loc2glo.scm 229 */
									obj_t BgL_arg1738z00_2047;
									obj_t BgL_arg1739z00_2048;

									{	/* Globalize/loc2glo.scm 228 */
										BgL_sfunz00_bglt BgL_oz00_2575;

										BgL_oz00_2575 =
											((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_localz00_17)))->
												BgL_valuez00));
										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3994;

											{
												obj_t BgL_auxz00_3995;

												{	/* Globalize/loc2glo.scm 228 */
													BgL_objectz00_bglt BgL_tmpz00_3996;

													BgL_tmpz00_3996 =
														((BgL_objectz00_bglt) BgL_oz00_2575);
													BgL_auxz00_3995 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3996);
												}
												BgL_auxz00_3994 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3995);
											}
											BgL_arg1738z00_2047 =
												(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3994))->
												BgL_newzd2bodyzd2);
									}}
									if (NULLP(BgL_kapturedz00_19))
										{	/* Globalize/loc2glo.scm 231 */
											BgL_arg1739z00_2048 = BNIL;
										}
									else
										{	/* Globalize/loc2glo.scm 231 */
											obj_t BgL_head1367z00_2053;

											{	/* Globalize/loc2glo.scm 231 */
												obj_t BgL_arg1751z00_2069;

												{	/* Globalize/loc2glo.scm 231 */
													obj_t BgL_arg1752z00_2070;
													obj_t BgL_arg1753z00_2071;

													BgL_arg1752z00_2070 =
														CAR(((obj_t) BgL_kapturedz00_19));
													BgL_arg1753z00_2071 =
														CAR(((obj_t) BgL_addzd2argszd2_2042));
													BgL_arg1751z00_2069 =
														MAKE_YOUNG_PAIR(BgL_arg1752z00_2070,
														BgL_arg1753z00_2071);
												}
												BgL_head1367z00_2053 =
													MAKE_YOUNG_PAIR(BgL_arg1751z00_2069, BNIL);
											}
											{	/* Globalize/loc2glo.scm 231 */
												obj_t BgL_g1371z00_2054;
												obj_t BgL_g1372z00_2055;

												BgL_g1371z00_2054 = CDR(((obj_t) BgL_kapturedz00_19));
												BgL_g1372z00_2055 =
													CDR(((obj_t) BgL_addzd2argszd2_2042));
												{
													obj_t BgL_ll1365z00_2057;
													obj_t BgL_ll1366z00_2058;
													obj_t BgL_tail1368z00_2059;

													BgL_ll1365z00_2057 = BgL_g1371z00_2054;
													BgL_ll1366z00_2058 = BgL_g1372z00_2055;
													BgL_tail1368z00_2059 = BgL_head1367z00_2053;
												BgL_zc3z04anonymousza31742ze3z87_2060:
													if (NULLP(BgL_ll1365z00_2057))
														{	/* Globalize/loc2glo.scm 231 */
															BgL_arg1739z00_2048 = BgL_head1367z00_2053;
														}
													else
														{	/* Globalize/loc2glo.scm 231 */
															obj_t BgL_newtail1369z00_2062;

															{	/* Globalize/loc2glo.scm 231 */
																obj_t BgL_arg1748z00_2065;

																{	/* Globalize/loc2glo.scm 231 */
																	obj_t BgL_arg1749z00_2066;
																	obj_t BgL_arg1750z00_2067;

																	BgL_arg1749z00_2066 =
																		CAR(((obj_t) BgL_ll1365z00_2057));
																	BgL_arg1750z00_2067 =
																		CAR(((obj_t) BgL_ll1366z00_2058));
																	BgL_arg1748z00_2065 =
																		MAKE_YOUNG_PAIR(BgL_arg1749z00_2066,
																		BgL_arg1750z00_2067);
																}
																BgL_newtail1369z00_2062 =
																	MAKE_YOUNG_PAIR(BgL_arg1748z00_2065, BNIL);
															}
															SET_CDR(BgL_tail1368z00_2059,
																BgL_newtail1369z00_2062);
															{	/* Globalize/loc2glo.scm 231 */
																obj_t BgL_arg1746z00_2063;
																obj_t BgL_arg1747z00_2064;

																BgL_arg1746z00_2063 =
																	CDR(((obj_t) BgL_ll1365z00_2057));
																BgL_arg1747z00_2064 =
																	CDR(((obj_t) BgL_ll1366z00_2058));
																{
																	obj_t BgL_tail1368z00_4028;
																	obj_t BgL_ll1366z00_4027;
																	obj_t BgL_ll1365z00_4026;

																	BgL_ll1365z00_4026 = BgL_arg1746z00_2063;
																	BgL_ll1366z00_4027 = BgL_arg1747z00_2064;
																	BgL_tail1368z00_4028 =
																		BgL_newtail1369z00_2062;
																	BgL_tail1368z00_2059 = BgL_tail1368z00_4028;
																	BgL_ll1366z00_2058 = BgL_ll1366z00_4027;
																	BgL_ll1365z00_2057 = BgL_ll1365z00_4026;
																	goto BgL_zc3z04anonymousza31742ze3z87_2060;
																}
															}
														}
												}
											}
										}
									BgL_arg1737z00_2046 =
										BGl_nodezd2globaliza7ez12z67zzglobaliza7e_nodeza7(
										((BgL_nodez00_bglt) BgL_arg1738z00_2047),
										((BgL_variablez00_bglt) BgL_localz00_17),
										BgL_arg1739z00_2048);
								}
								((((BgL_sfunz00_bglt) COBJECT(BgL_newzd2funzd2_2044))->
										BgL_bodyz00) =
									((obj_t) ((obj_t) BgL_arg1737z00_2046)), BUNSPEC);
							}
							((((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_16)))->
									BgL_valuez00) =
								((BgL_valuez00_bglt) ((BgL_valuez00_bglt)
										BgL_newzd2funzd2_2044)), BUNSPEC);
							return BgL_globalz00_16;
						}
					}
				}
			}
		}

	}



/* the-global */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_thezd2globalzd2zzglobaliza7e_localzd2ze3globalz96(BgL_localz00_bglt
		BgL_localz00_21)
	{
		{	/* Globalize/loc2glo.scm 241 */
			{	/* Globalize/loc2glo.scm 242 */
				BgL_valuez00_bglt BgL_valuez00_2118;

				BgL_valuez00_2118 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_localz00_21)))->BgL_valuez00);
				{	/* Globalize/loc2glo.scm 243 */
					bool_t BgL_test1961z00_4039;

					{	/* Globalize/loc2glo.scm 243 */
						obj_t BgL_arg1839z00_2142;

						{
							BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4040;

							{
								obj_t BgL_auxz00_4041;

								{	/* Globalize/loc2glo.scm 243 */
									BgL_objectz00_bglt BgL_tmpz00_4042;

									BgL_tmpz00_4042 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_valuez00_2118));
									BgL_auxz00_4041 = BGL_OBJECT_WIDENING(BgL_tmpz00_4042);
								}
								BgL_auxz00_4040 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4041);
							}
							BgL_arg1839z00_2142 =
								(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4040))->
								BgL_thezd2globalzd2);
						}
						{	/* Globalize/loc2glo.scm 243 */
							obj_t BgL_classz00_2592;

							BgL_classz00_2592 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_arg1839z00_2142))
								{	/* Globalize/loc2glo.scm 243 */
									BgL_objectz00_bglt BgL_arg1807z00_2594;

									BgL_arg1807z00_2594 =
										(BgL_objectz00_bglt) (BgL_arg1839z00_2142);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Globalize/loc2glo.scm 243 */
											long BgL_idxz00_2600;

											BgL_idxz00_2600 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2594);
											BgL_test1961z00_4039 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2600 + 2L)) == BgL_classz00_2592);
										}
									else
										{	/* Globalize/loc2glo.scm 243 */
											bool_t BgL_res1921z00_2625;

											{	/* Globalize/loc2glo.scm 243 */
												obj_t BgL_oclassz00_2608;

												{	/* Globalize/loc2glo.scm 243 */
													obj_t BgL_arg1815z00_2616;
													long BgL_arg1816z00_2617;

													BgL_arg1815z00_2616 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Globalize/loc2glo.scm 243 */
														long BgL_arg1817z00_2618;

														BgL_arg1817z00_2618 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2594);
														BgL_arg1816z00_2617 =
															(BgL_arg1817z00_2618 - OBJECT_TYPE);
													}
													BgL_oclassz00_2608 =
														VECTOR_REF(BgL_arg1815z00_2616,
														BgL_arg1816z00_2617);
												}
												{	/* Globalize/loc2glo.scm 243 */
													bool_t BgL__ortest_1115z00_2609;

													BgL__ortest_1115z00_2609 =
														(BgL_classz00_2592 == BgL_oclassz00_2608);
													if (BgL__ortest_1115z00_2609)
														{	/* Globalize/loc2glo.scm 243 */
															BgL_res1921z00_2625 = BgL__ortest_1115z00_2609;
														}
													else
														{	/* Globalize/loc2glo.scm 243 */
															long BgL_odepthz00_2610;

															{	/* Globalize/loc2glo.scm 243 */
																obj_t BgL_arg1804z00_2611;

																BgL_arg1804z00_2611 = (BgL_oclassz00_2608);
																BgL_odepthz00_2610 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2611);
															}
															if ((2L < BgL_odepthz00_2610))
																{	/* Globalize/loc2glo.scm 243 */
																	obj_t BgL_arg1802z00_2613;

																	{	/* Globalize/loc2glo.scm 243 */
																		obj_t BgL_arg1803z00_2614;

																		BgL_arg1803z00_2614 = (BgL_oclassz00_2608);
																		BgL_arg1802z00_2613 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2614, 2L);
																	}
																	BgL_res1921z00_2625 =
																		(BgL_arg1802z00_2613 == BgL_classz00_2592);
																}
															else
																{	/* Globalize/loc2glo.scm 243 */
																	BgL_res1921z00_2625 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1961z00_4039 = BgL_res1921z00_2625;
										}
								}
							else
								{	/* Globalize/loc2glo.scm 243 */
									BgL_test1961z00_4039 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test1961z00_4039)
						{
							obj_t BgL_auxz00_4070;

							{
								BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4071;

								{
									obj_t BgL_auxz00_4072;

									{	/* Globalize/loc2glo.scm 244 */
										BgL_objectz00_bglt BgL_tmpz00_4073;

										BgL_tmpz00_4073 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt) BgL_valuez00_2118));
										BgL_auxz00_4072 = BGL_OBJECT_WIDENING(BgL_tmpz00_4073);
									}
									BgL_auxz00_4071 =
										((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4072);
								}
								BgL_auxz00_4070 =
									(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4071))->
									BgL_thezd2globalzd2);
							}
							return ((BgL_globalz00_bglt) BgL_auxz00_4070);
						}
					else
						{	/* Globalize/loc2glo.scm 245 */
							obj_t BgL_lidz00_2121;

							{	/* Globalize/loc2glo.scm 245 */
								obj_t BgL_arg1835z00_2138;

								{	/* Globalize/loc2glo.scm 245 */
									obj_t BgL_arg1836z00_2139;
									obj_t BgL_arg1837z00_2140;

									{	/* Globalize/loc2glo.scm 245 */
										obj_t BgL_symbolz00_2628;

										BgL_symbolz00_2628 = CNST_TABLE_REF(5);
										{	/* Globalize/loc2glo.scm 245 */
											obj_t BgL_arg1455z00_2629;

											BgL_arg1455z00_2629 =
												SYMBOL_TO_STRING(BgL_symbolz00_2628);
											BgL_arg1836z00_2139 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2629);
										}
									}
									{	/* Globalize/loc2glo.scm 245 */
										obj_t BgL_arg1838z00_2141;

										BgL_arg1838z00_2141 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_localz00_21)))->
											BgL_idz00);
										{	/* Globalize/loc2glo.scm 245 */
											obj_t BgL_arg1455z00_2632;

											BgL_arg1455z00_2632 =
												SYMBOL_TO_STRING(BgL_arg1838z00_2141);
											BgL_arg1837z00_2140 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2632);
										}
									}
									BgL_arg1835z00_2138 =
										string_append(BgL_arg1836z00_2139, BgL_arg1837z00_2140);
								}
								BgL_lidz00_2121 = bstring_to_symbol(BgL_arg1835z00_2138);
							}
							{	/* Globalize/loc2glo.scm 245 */
								obj_t BgL_idz00_2122;

								{	/* Globalize/loc2glo.scm 246 */
									bool_t BgL_test1966z00_4089;

									{	/* Globalize/loc2glo.scm 246 */
										obj_t BgL_arg1834z00_2137;

										BgL_arg1834z00_2137 =
											BGl_findzd2globalzf2modulez20zzast_envz00(BgL_lidz00_2121,
											BGl_za2moduleza2z00zzmodule_modulez00);
										{	/* Globalize/loc2glo.scm 246 */
											obj_t BgL_classz00_2634;

											BgL_classz00_2634 = BGl_globalz00zzast_varz00;
											if (BGL_OBJECTP(BgL_arg1834z00_2137))
												{	/* Globalize/loc2glo.scm 246 */
													BgL_objectz00_bglt BgL_arg1807z00_2636;

													BgL_arg1807z00_2636 =
														(BgL_objectz00_bglt) (BgL_arg1834z00_2137);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Globalize/loc2glo.scm 246 */
															long BgL_idxz00_2642;

															BgL_idxz00_2642 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2636);
															BgL_test1966z00_4089 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2642 + 2L)) == BgL_classz00_2634);
														}
													else
														{	/* Globalize/loc2glo.scm 246 */
															bool_t BgL_res1922z00_2667;

															{	/* Globalize/loc2glo.scm 246 */
																obj_t BgL_oclassz00_2650;

																{	/* Globalize/loc2glo.scm 246 */
																	obj_t BgL_arg1815z00_2658;
																	long BgL_arg1816z00_2659;

																	BgL_arg1815z00_2658 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Globalize/loc2glo.scm 246 */
																		long BgL_arg1817z00_2660;

																		BgL_arg1817z00_2660 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2636);
																		BgL_arg1816z00_2659 =
																			(BgL_arg1817z00_2660 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2650 =
																		VECTOR_REF(BgL_arg1815z00_2658,
																		BgL_arg1816z00_2659);
																}
																{	/* Globalize/loc2glo.scm 246 */
																	bool_t BgL__ortest_1115z00_2651;

																	BgL__ortest_1115z00_2651 =
																		(BgL_classz00_2634 == BgL_oclassz00_2650);
																	if (BgL__ortest_1115z00_2651)
																		{	/* Globalize/loc2glo.scm 246 */
																			BgL_res1922z00_2667 =
																				BgL__ortest_1115z00_2651;
																		}
																	else
																		{	/* Globalize/loc2glo.scm 246 */
																			long BgL_odepthz00_2652;

																			{	/* Globalize/loc2glo.scm 246 */
																				obj_t BgL_arg1804z00_2653;

																				BgL_arg1804z00_2653 =
																					(BgL_oclassz00_2650);
																				BgL_odepthz00_2652 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2653);
																			}
																			if ((2L < BgL_odepthz00_2652))
																				{	/* Globalize/loc2glo.scm 246 */
																					obj_t BgL_arg1802z00_2655;

																					{	/* Globalize/loc2glo.scm 246 */
																						obj_t BgL_arg1803z00_2656;

																						BgL_arg1803z00_2656 =
																							(BgL_oclassz00_2650);
																						BgL_arg1802z00_2655 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2656, 2L);
																					}
																					BgL_res1922z00_2667 =
																						(BgL_arg1802z00_2655 ==
																						BgL_classz00_2634);
																				}
																			else
																				{	/* Globalize/loc2glo.scm 246 */
																					BgL_res1922z00_2667 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1966z00_4089 = BgL_res1922z00_2667;
														}
												}
											else
												{	/* Globalize/loc2glo.scm 246 */
													BgL_test1966z00_4089 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test1966z00_4089)
										{	/* Globalize/loc2glo.scm 246 */
											BgL_idz00_2122 =
												BGl_gensymz00zz__r4_symbols_6_4z00(BgL_lidz00_2121);
										}
									else
										{	/* Globalize/loc2glo.scm 246 */
											BgL_idz00_2122 = BgL_lidz00_2121;
										}
								}
								{	/* Globalize/loc2glo.scm 246 */
									BgL_globalz00_bglt BgL_globalz00_2123;

									BgL_globalz00_2123 =
										BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2
										(BgL_idz00_2122, BNIL, BNIL,
										BGl_za2moduleza2z00zzmodule_modulez00, CNST_TABLE_REF(6),
										CNST_TABLE_REF(7), CNST_TABLE_REF(8), BUNSPEC);
									{	/* Globalize/loc2glo.scm 249 */

										{	/* Globalize/loc2glo.scm 262 */
											BgL_valuez00_bglt BgL_arg1808z00_2124;
											obj_t BgL_arg1812z00_2125;

											BgL_arg1808z00_2124 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_globalz00_2123)))->
												BgL_valuez00);
											BgL_arg1812z00_2125 =
												(((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
															BgL_valuez00_2118)))->BgL_locz00);
											((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
																BgL_arg1808z00_2124)))->BgL_locz00) =
												((obj_t) BgL_arg1812z00_2125), BUNSPEC);
										}
										{	/* Globalize/loc2glo.scm 264 */
											BgL_typez00_bglt BgL_arg1820z00_2126;

											BgL_arg1820z00_2126 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_localz00_21)))->
												BgL_typez00);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_globalz00_2123)))->BgL_typez00) =
												((BgL_typez00_bglt) BgL_arg1820z00_2126), BUNSPEC);
										}
										if (
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_localz00_21)))->
												BgL_userzf3zf3))
											{	/* Globalize/loc2glo.scm 266 */
												BFALSE;
											}
										else
											{	/* Globalize/loc2glo.scm 266 */
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_globalz00_2123)))->
														BgL_userzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
											}
										{	/* Globalize/loc2glo.scm 268 */
											BgL_globalzf2ginfozf2_bglt BgL_wide1189z00_2130;

											BgL_wide1189z00_2130 =
												((BgL_globalzf2ginfozf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_globalzf2ginfozf2_bgl))));
											{	/* Globalize/loc2glo.scm 268 */
												obj_t BgL_auxz00_4137;
												BgL_objectz00_bglt BgL_tmpz00_4134;

												BgL_auxz00_4137 = ((obj_t) BgL_wide1189z00_2130);
												BgL_tmpz00_4134 =
													((BgL_objectz00_bglt)
													((BgL_globalz00_bglt) BgL_globalz00_2123));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4134,
													BgL_auxz00_4137);
											}
											((BgL_objectz00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_2123));
											{	/* Globalize/loc2glo.scm 268 */
												long BgL_arg1822z00_2131;

												BgL_arg1822z00_2131 =
													BGL_CLASS_NUM
													(BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_globalz00_bglt) BgL_globalz00_2123)),
													BgL_arg1822z00_2131);
											}
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_2123));
										}
										{
											BgL_globalzf2ginfozf2_bglt BgL_auxz00_4148;

											{
												obj_t BgL_auxz00_4149;

												{	/* Globalize/loc2glo.scm 268 */
													BgL_objectz00_bglt BgL_tmpz00_4150;

													BgL_tmpz00_4150 =
														((BgL_objectz00_bglt)
														((BgL_globalz00_bglt) BgL_globalz00_2123));
													BgL_auxz00_4149 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4150);
												}
												BgL_auxz00_4148 =
													((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_4149);
											}
											((((BgL_globalzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_4148))->BgL_escapezf3zf3) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
										}
										{
											BgL_globalzf2ginfozf2_bglt BgL_auxz00_4156;

											{
												obj_t BgL_auxz00_4157;

												{	/* Globalize/loc2glo.scm 268 */
													BgL_objectz00_bglt BgL_tmpz00_4158;

													BgL_tmpz00_4158 =
														((BgL_objectz00_bglt)
														((BgL_globalz00_bglt) BgL_globalz00_2123));
													BgL_auxz00_4157 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4158);
												}
												BgL_auxz00_4156 =
													((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_4157);
											}
											((((BgL_globalzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_4156))->
													BgL_globalzd2closurezd2) = ((obj_t) BFALSE), BUNSPEC);
										}
										((BgL_globalz00_bglt) BgL_globalz00_2123);
										{	/* Globalize/loc2glo.scm 269 */
											BgL_valuez00_bglt BgL_arg1823z00_2133;
											obj_t BgL_arg1831z00_2134;

											BgL_arg1823z00_2133 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_globalz00_2123)))->
												BgL_valuez00);
											BgL_arg1831z00_2134 =
												(((BgL_funz00_bglt)
													COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
																BgL_valuez00_2118))))->BgL_sidezd2effectzd2);
											((((BgL_funz00_bglt)
														COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
																	BgL_arg1823z00_2133))))->
													BgL_sidezd2effectzd2) =
												((obj_t) BgL_arg1831z00_2134), BUNSPEC);
										}
										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4173;

											{
												obj_t BgL_auxz00_4174;

												{	/* Globalize/loc2glo.scm 271 */
													BgL_objectz00_bglt BgL_tmpz00_4175;

													BgL_tmpz00_4175 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_valuez00_2118));
													BgL_auxz00_4174 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4175);
												}
												BgL_auxz00_4173 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4174);
											}
											((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4173))->
													BgL_thezd2globalzd2) =
												((obj_t) ((obj_t) BgL_globalz00_2123)), BUNSPEC);
										}
										return BgL_globalz00_2123;
									}
								}
							}
						}
				}
			}
		}

	}



/* &the-global */
	BgL_globalz00_bglt
		BGl_z62thezd2globalzb0zzglobaliza7e_localzd2ze3globalz96(obj_t
		BgL_envz00_2732, obj_t BgL_localz00_2733)
	{
		{	/* Globalize/loc2glo.scm 241 */
			return
				BGl_thezd2globalzd2zzglobaliza7e_localzd2ze3globalz96(
				((BgL_localz00_bglt) BgL_localz00_2733));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_localzd2ze3globalz96(void)
	{
		{	/* Globalize/loc2glo.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_localzd2ze3globalz96(void)
	{
		{	/* Globalize/loc2glo.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_localzd2ze3globalz96(void)
	{
		{	/* Globalize/loc2glo.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_localzd2ze3globalz96(void)
	{
		{	/* Globalize/loc2glo.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzobject_methodz00(493120707L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(2706117L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
			return
				BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(244215788L,
				BSTRING_TO_STRING(BGl_string1924z00zzglobaliza7e_localzd2ze3globalz96));
		}

	}

#ifdef __cplusplus
}
#endif
