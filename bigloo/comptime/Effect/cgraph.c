/*===========================================================================*/
/*   (Effect/cgraph.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Effect/cgraph.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EFFECT_CGRAPH_TYPE_DEFINITIONS
#define BGL_EFFECT_CGRAPH_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_localzf2fromzf2_bgl
	{
		obj_t BgL_fromz00;
	}                      *BgL_localzf2fromzf2_bglt;

	typedef struct BgL_globalzf2fromzf2_bgl
	{
		obj_t BgL_fromz00;
	}                       *BgL_globalzf2fromzf2_bglt;


#endif													// BGL_EFFECT_CGRAPH_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2aliasz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t BGl_z62globalzf2fromzd2modulez42zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2occurrencez42zzeffect_cgraphz00(obj_t,
		obj_t);
	static obj_t BGl_z62localzf2fromzd2accessz42zzeffect_cgraphz00(obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t
		BGl_z62globalzf2fromzd2fastzd2alphazd2setz12z50zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2evalzf3zd2setz12z13zzeffect_cgraphz00
		(BgL_globalz00_bglt, bool_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static BgL_typez00_bglt
		BGl_z62globalzf2fromzd2typez42zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t
		BGl_z62callzd2graphz12zd2setzd2exzd2i1356z70zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62globalzf2fromzd2occurrencezd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzeffect_cgraphz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2fromz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t BGl_z62callzd2graphz12zd2conditio1346z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2removablez20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t
		BGl_z62callzd2graphz12zd2boxzd2setz121364zb0zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2modulezd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_z62getzd2varzf2allz42zzeffect_cgraphz00(obj_t);
	static obj_t
		BGl_z62callzd2graphz12zd2makezd2box1360za2zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62callzd2graphz12zd2appzd2ly1336za2zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_localzf2fromzd2valuez20zzeffect_cgraphz00(BgL_localz00_bglt);
	static obj_t
		BGl_z62globalzf2fromzd2pragmazd2setz12z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_localzf2fromzd2userzf3zd3zzeffect_cgraphz00(BgL_localz00_bglt);
	static obj_t BGl_z62globalzf2fromzd2initz42zzeffect_cgraphz00(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62globalzf2fromzd2importz42zzeffect_cgraphz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2jvmzd2typezd2namez20zzeffect_cgraphz00
		(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62localzf2fromzd2removablezd2setz12z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(BgL_variablez00_bglt);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t
		BGl_z62globalzf2fromzd2userzf3zd2setz12z71zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62localzf2fromzd2occurrencewz42zzeffect_cgraphz00(obj_t,
		obj_t);
	static obj_t BGl_z62callzd2graphz12zd2funcall1338z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2varzf2sidezd2effectzf2zzeffect_cgraphz00(void);
	static obj_t BGl_z62resetzd2effectzd2tablesz12z70zzeffect_cgraphz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzeffect_cgraphz00(void);
	static obj_t
		BGl_z62callzd2graphz12zd2jumpzd2exzd21358z70zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2pragmaz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t
		BGl_z62globalzf2fromzd2jvmzd2typezd2namezd2setz12z82zzeffect_cgraphz00
		(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2fromzd2accesszd2setz12z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2accessz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t
		BGl_z62callzd2graphz12zd2letzd2var1354za2zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzf2fromzf3z63zzeffect_cgraphz00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzeffect_cgraphz00(void);
	static obj_t BGl_z62callzd2graphz12za2zzeffect_cgraphz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2removablez42zzeffect_cgraphz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_resetzd2effectzd2tablesz12z12zzeffect_cgraphz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2accesszd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzeffect_cgraphz00(void);
	static obj_t BGl_z62globalzf2fromzd2srczd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	static obj_t BGl_z62callzd2graphz12zd2extern1340z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2removablezd2setz12ze0zzeffect_cgraphz00
		(BgL_localz00_bglt, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62globalzf2fromzd2fromz42zzeffect_cgraphz00(obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2aliaszd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_z62globalzf2fromzd2fastzd2alphaz90zzeffect_cgraphz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzf2fromzd2evalzf3zd3zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t BGl_z62callzd2graphz12zd2switch1350z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62callzd2graphz12zd2boxzd2ref1362za2zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62savezd2callz12zd2globalzf2fr1371z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2removablezd2setz12ze0zzeffect_cgraphz00
		(BgL_globalz00_bglt, obj_t);
	static BgL_valuez00_bglt
		BGl_z62localzf2fromzd2valuez42zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62localzf2fromzd2userzf3zb1zzeffect_cgraphz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_globalzf2fromzd2occurrencewz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2srczd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		obj_t);
	static bool_t BGl_callzd2graphza2z12z62zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62localzf2fromzd2keyz42zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62callzd2graphz12zd2sequence1330z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_localzf2fromzf3z01zzeffect_cgraphz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62localzf2fromzd2namezd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2fromzd2removablezd2setz12z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzeffect_cgraphz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2jvmzd2typezd2namezd2setz12ze0zzeffect_cgraphz00
		(BgL_globalz00_bglt, obj_t);
	static obj_t
		BGl_z62globalzf2fromzd2evalzf3zd2setz12z71zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2srcz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t BGl_z62globalzf2fromzd2srcz42zzeffect_cgraphz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2valuezd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt,
		BgL_valuez00_bglt);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t
		BGl_z62savezd2callz12zd2localzf2fro1377z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzf2fromzd2aliasz42zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2idz42zzeffect_cgraphz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2namezd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt,
		obj_t);
	static obj_t BGl_z62callzd2graphz12zd2app1334z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2fromzd2occurrencewzd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_localzf2fromzd2occurrencez20zzeffect_cgraphz00(BgL_localz00_bglt);
	static obj_t BGl_z62savezd2callz12zd2local1374z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62localzf2fromzd2idz42zzeffect_cgraphz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2importzd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_globalzf2fromzd2valuez20zzeffect_cgraphz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzf2fromzd2userzf3zd3zzeffect_cgraphz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2modulez20zzeffect_cgraphz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzf2fromzd2evaluablezf3zd3zzeffect_cgraphz00(BgL_globalz00_bglt);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_makezd2globalzf2fromz20zzeffect_cgraphz00(obj_t, obj_t,
		BgL_typez00_bglt, BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long,
		bool_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_localzf2fromzd2keyz20zzeffect_cgraphz00(BgL_localz00_bglt);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62localzf2fromzd2valuezd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2namezd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2namez20zzeffect_cgraphz00(BgL_localz00_bglt);
	static obj_t BGl_z62localzf2fromzd2namez42zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62localzf2fromzd2accesszd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2userzf3zd2setz12z13zzeffect_cgraphz00(BgL_localz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2importz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t BGl_z62localzf2fromzd2occurrencez42zzeffect_cgraphz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2occurrencewzd2setz12ze0zzeffect_cgraphz00
		(BgL_localz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2fastzd2alphazd2setz12z32zzeffect_cgraphz00
		(BgL_localz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2occurrencezd2setz12ze0zzeffect_cgraphz00
		(BgL_localz00_bglt, long);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t
		BGl_z62globalzf2fromzd2modulezd2setz12z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzf2fromzd2aliaszd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31541ze3ze5zzeffect_cgraphz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2idz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t BGl_z62localzf2fromzf3z63zzeffect_cgraphz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2pragmazd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzeffect_cgraphz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_localz00_bglt
		BGl_z62localzf2fromzd2nilz42zzeffect_cgraphz00(obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_z62callzd2graphz121327za2zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t
		BGl_z62localzf2fromzd2fastzd2alphazd2setz12z50zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2namezd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62localzf2fromzd2occurrencezd2setz12z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2evaluablezf3zd2setz12z13zzeffect_cgraphz00
		(BgL_globalz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2accessz20zzeffect_cgraphz00(BgL_localz00_bglt);
	static obj_t BGl_z62globalzf2fromzd2evalzf3zb1zzeffect_cgraphz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62globalzf2fromzd2libraryzd2setz12z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2valuezd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		BgL_valuez00_bglt);
	static obj_t
		BGl_z62localzf2fromzd2userzf3zd2setz12z71zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2fastzd2alphazf2zzeffect_cgraphz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2namez20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static BgL_globalz00_bglt
		BGl_z62makezd2globalzf2fromz42zzeffect_cgraphz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2accesszd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_localzf2fromzf2zzeffect_cgraphz00 = BUNSPEC;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzeffect_cgraphz00(void);
	static obj_t BGl_savezd2callz12zc0zzeffect_cgraphz00(BgL_variablez00_bglt,
		BgL_variablez00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzeffect_cgraphz00(void);
	static obj_t BGl_z62localzf2fromzd2typezd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_localzf2fromzd2typez20zzeffect_cgraphz00(BgL_localz00_bglt);
	static BgL_typez00_bglt BGl_z62localzf2fromzd2typez42zzeffect_cgraphz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_globalzf2fromzd2occurrencez20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzeffect_cgraphz00(void);
	static obj_t BGl_z62localzf2fromzd2fromzd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2initzd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzeffect_cgraphz00(void);
	static BgL_localz00_bglt BGl_z62lambda1515z62zzeffect_cgraphz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2pragmaz42zzeffect_cgraphz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_globalzf2fromzf3z01zzeffect_cgraphz00(obj_t);
	static obj_t BGl_z62lambda1607z62zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1608z62zzeffect_cgraphz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2typezd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62globalzf2fromzd2accessz42zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2libraryz42zzeffect_cgraphz00(obj_t,
		obj_t);
	static obj_t BGl_z62savezd2callz12za2zzeffect_cgraphz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_globalzf2fromzd2nilz20zzeffect_cgraphz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2fromzd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt,
		obj_t);
	static BgL_globalz00_bglt
		BGl_z62globalzf2fromzd2nilz42zzeffect_cgraphz00(obj_t);
	static BgL_localz00_bglt BGl_z62lambda1536z62zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2evaluablezf3zb1zzeffect_cgraphz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2fastzd2alphazd2setz12z32zzeffect_cgraphz00
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_localzf2fromzd2nilz20zzeffect_cgraphz00(void);
	static obj_t BGl_z62localzf2fromzd2fastzd2alphaz90zzeffect_cgraphz00(obj_t,
		obj_t);
	static obj_t BGl_z62callzd2graphz12zd2cast1342z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2occurrencezd2setz12ze0zzeffect_cgraphz00
		(BgL_globalz00_bglt, long);
	static obj_t BGl_z62globalzf2fromzd2occurrencewz42zzeffect_cgraphz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2idz20zzeffect_cgraphz00(BgL_localz00_bglt);
	static obj_t BGl_z62savezd2callz12zd2global1369z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2callzd2graphz12z12zzeffect_cgraphz00(BgL_variablez00_bglt);
	static BgL_localz00_bglt BGl_z62lambda1542z62zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t
		BGl_z62callzd2graphz12zd2letzd2fun1352za2zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62funzd2callzd2graphz12z70zzeffect_cgraphz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2userzf3zd2setz12z13zzeffect_cgraphz00
		(BgL_globalz00_bglt, bool_t);
	static obj_t
		BGl_z62localzf2fromzd2occurrencewzd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_EXPORTED_DECL long
		BGl_localzf2fromzd2occurrencewz20zzeffect_cgraphz00(BgL_localz00_bglt);
	extern obj_t BGl_valuez00zzast_varz00;
	BGL_EXPORTED_DECL obj_t BGl_getzd2varzf2allz20zzeffect_cgraphz00(void);
	static BgL_valuez00_bglt
		BGl_z62globalzf2fromzd2valuez42zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2userzf3zb1zzeffect_cgraphz00(obj_t,
		obj_t);
	static obj_t BGl_za2varzf2allza2zf2zzeffect_cgraphz00 = BUNSPEC;
	static obj_t BGl_z62globalzf2fromzd2initzd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62callzd2graphz12zd2setq1344z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2typezd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_globalzf2fromzd2typez20zzeffect_cgraphz00(BgL_globalz00_bglt);
	extern obj_t BGl_variablez00zzast_varz00;
	static obj_t BGl_z62globalzf2fromzd2namez42zzeffect_cgraphz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2fromzd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31590ze3ze5zzeffect_cgraphz00(obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62callzd2graphz12zd2sync1332z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1562z62zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1563z62zzeffect_cgraphz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62callzd2graphz12zd2fail1348z70zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62getzd2varzf2sidezd2effectz90zzeffect_cgraphz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2fromz20zzeffect_cgraphz00(BgL_localz00_bglt);
	static obj_t
		BGl_z62globalzf2fromzd2evaluablezf3zd2setz12z71zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_globalzf2fromzf2zzeffect_cgraphz00 = BUNSPEC;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62localzf2fromzd2fromz42zzeffect_cgraphz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2initz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2localzf2fromz20zzeffect_cgraphz00(obj_t, obj_t, BgL_typez00_bglt,
		BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long, bool_t, long, obj_t);
	static BgL_globalz00_bglt BGl_z62lambda1577z62zzeffect_cgraphz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62localzf2fromzd2removablez42zzeffect_cgraphz00(obj_t,
		obj_t);
	static BgL_localz00_bglt
		BGl_z62makezd2localzf2fromz42zzeffect_cgraphz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62savezd2callz121365za2zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static BgL_globalz00_bglt BGl_z62lambda1586z62zzeffect_cgraphz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62globalzf2fromzd2importzd2setz12z82zzeffect_cgraphz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_nodez00_bglt,
		BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2fastzd2alphazf2zzeffect_cgraphz00(BgL_globalz00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2libraryzd2setz12ze0zzeffect_cgraphz00
		(BgL_globalz00_bglt, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static BgL_globalz00_bglt BGl_z62lambda1591z62zzeffect_cgraphz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2fromzd2removablez20zzeffect_cgraphz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2occurrencewzd2setz12ze0zzeffect_cgraphz00
		(BgL_globalz00_bglt, long);
	static obj_t BGl_z62globalzf2fromzd2valuezd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2typezd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2fromzd2jvmzd2typezd2namez42zzeffect_cgraphz00(obj_t, obj_t);
	static obj_t BGl_z62globalzf2fromzd2fromzd2setz12z82zzeffect_cgraphz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2fromzd2libraryz20zzeffect_cgraphz00(BgL_globalz00_bglt);
	static obj_t __cnst[10];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funzd2callzd2graphz12zd2envzc0zzeffect_cgraphz00,
		BgL_bgl_za762funza7d2callza7d21943za7,
		BGl_z62funzd2callzd2graphz12z70zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2accesszd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71944za7,
		BGl_z62localzf2fromzd2accesszd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2valuezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71945za7,
		BGl_z62localzf2fromzd2valuez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzf2fromzf3zd2envzd3zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1946z00,
		BGl_z62globalzf2fromzf3z63zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2namezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71947za7,
		BGl_z62localzf2fromzd2namezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2fastzd2alphazd2envz20zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71948za7,
		BGl_z62localzf2fromzd2fastzd2alphaz90zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2occurrencewzd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71949za7,
		BGl_z62localzf2fromzd2occurrencewzd2setz12z82zzeffect_cgraphz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2importzd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1950z00,
		BGl_z62globalzf2fromzd2importzd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2occurrencewzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71951za7,
		BGl_z62localzf2fromzd2occurrencewz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2valuezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1952z00,
		BGl_z62globalzf2fromzd2valuez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2modulezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1953z00,
		BGl_z62globalzf2fromzd2modulezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2fastzd2alphazd2envz20zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1954z00,
		BGl_z62globalzf2fromzd2fastzd2alphaz90zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2userzf3zd2envz01zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71955za7,
		BGl_z62localzf2fromzd2userzf3zb1zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2typezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1956z00,
		BGl_z62globalzf2fromzd2typez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2nilzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1957z00,
		BGl_z62globalzf2fromzd2nilz42zzeffect_cgraphz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2evalzf3zd2envz01zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1958z00,
		BGl_z62globalzf2fromzd2evalzf3zb1zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2accesszd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71959za7,
		BGl_z62localzf2fromzd2accessz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2removablezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1960z00,
		BGl_z62globalzf2fromzd2removablezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2pragmazd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1961z00,
		BGl_z62globalzf2fromzd2pragmaz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2fastzd2alphazd2setz12zd2envze0zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1962z00,
		BGl_z62globalzf2fromzd2fastzd2alphazd2setz12z50zzeffect_cgraphz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2evalzf3zd2setz12zd2envzc1zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1963z00,
		BGl_z62globalzf2fromzd2evalzf3zd2setz12z71zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2occurrencezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1964z00,
		BGl_z62globalzf2fromzd2occurrencezd2setz12z82zzeffect_cgraphz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2accesszd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1965z00,
		BGl_z62globalzf2fromzd2accesszd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2fastzd2alphazd2setz12zd2envze0zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71966za7,
		BGl_z62localzf2fromzd2fastzd2alphazd2setz12z50zzeffect_cgraphz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2typezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71967za7,
		BGl_z62localzf2fromzd2typez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2occurrencezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza71968za7,
		BGl_z62localzf2fromzd2occurrencezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2userzf3zd2envz01zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1969z00,
		BGl_z62globalzf2fromzd2userzf3zb1zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2modulezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1970z00,
		BGl_z62globalzf2fromzd2modulez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2idzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1971z00,
		BGl_z62globalzf2fromzd2idz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2occurrencewzd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1972z00,
		BGl_z62globalzf2fromzd2occurrencewzd2setz12z82zzeffect_cgraphz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2globalzf2fromzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762makeza7d2global1973z00,
		BGl_z62makezd2globalzf2fromz42zzeffect_cgraphz00, 0L, BUNSPEC, 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2aliaszd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1974z00,
		BGl_z62globalzf2fromzd2aliasz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2occurrencewzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1975z00,
		BGl_z62globalzf2fromzd2occurrencewz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1900z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1542za7621976z00, BGl_z62lambda1542z62zzeffect_cgraphz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1901z00zzeffect_cgraphz00,
		BgL_bgl_za762za7c3za704anonymo1977za7,
		BGl_z62zc3z04anonymousza31541ze3ze5zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1902z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1536za7621978z00, BGl_z62lambda1536z62zzeffect_cgraphz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1903z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1515za7621979z00, BGl_z62lambda1515z62zzeffect_cgraphz00,
		0L, BUNSPEC, 14);
	      DEFINE_STRING(BGl_string1910z00zzeffect_cgraphz00,
		BgL_bgl_string1910za700za7za7e1980za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1904z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1608za7621981z00, BGl_z62lambda1608z62zzeffect_cgraphz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1905z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1607za7621982z00, BGl_z62lambda1607z62zzeffect_cgraphz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1912z00zzeffect_cgraphz00,
		BgL_bgl_string1912za700za7za7e1983za7, "call-graph!1327", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1906z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1591za7621984z00, BGl_z62lambda1591z62zzeffect_cgraphz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1907z00zzeffect_cgraphz00,
		BgL_bgl_za762za7c3za704anonymo1985za7,
		BGl_z62zc3z04anonymousza31590ze3ze5zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1914z00zzeffect_cgraphz00,
		BgL_bgl_string1914za700za7za7e1986za7, "save-call!1365", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1908z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1586za7621987z00, BGl_z62lambda1586z62zzeffect_cgraphz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1915z00zzeffect_cgraphz00,
		BgL_bgl_string1915za700za7za7e1988za7, "No method for this object", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1909z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1577za7621989z00, BGl_z62lambda1577z62zzeffect_cgraphz00,
		0L, BUNSPEC, 21);
	      DEFINE_STRING(BGl_string1917z00zzeffect_cgraphz00,
		BgL_bgl_string1917za700za7za7e1990za7, "call-graph!", 11);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza71991za7,
		BGl_z62callzd2graphz12za2zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1911z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza71992za7,
		BGl_z62callzd2graphz121327za2zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1913z00zzeffect_cgraphz00,
		BgL_bgl_za762saveza7d2callza711993za7,
		BGl_z62savezd2callz121365za2zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1916z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza71994za7,
		BGl_z62callzd2graphz12zd2sequence1330z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1918z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza71995za7,
		BGl_z62callzd2graphz12zd2sync1332z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1919z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza71996za7,
		BGl_z62callzd2graphz12zd2app1334z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2typezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1997z00,
		BGl_z62globalzf2fromzd2typezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2srczd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from1998z00,
		BGl_z62globalzf2fromzd2srcz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1920z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza71999za7,
		BGl_z62callzd2graphz12zd2appzd2ly1336za2zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1921z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72000za7,
		BGl_z62callzd2graphz12zd2funcall1338z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2jvmzd2typezd2namezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2001z00,
		BGl_z62globalzf2fromzd2jvmzd2typezd2namez42zzeffect_cgraphz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1922z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72002za7,
		BGl_z62callzd2graphz12zd2extern1340z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1923z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72003za7,
		BGl_z62callzd2graphz12zd2cast1342z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1924z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72004za7,
		BGl_z62callzd2graphz12zd2setq1344z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1925z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72005za7,
		BGl_z62callzd2graphz12zd2conditio1346z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1926z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72006za7,
		BGl_z62callzd2graphz12zd2fail1348z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1927z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72007za7,
		BGl_z62callzd2graphz12zd2switch1350z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1928z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72008za7,
		BGl_z62callzd2graphz12zd2letzd2fun1352za2zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2removablezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72009za7,
		BGl_z62localzf2fromzd2removablezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1929z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72010za7,
		BGl_z62callzd2graphz12zd2letzd2var1354za2zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1936z00zzeffect_cgraphz00,
		BgL_bgl_string1936za700za7za7e2011za7, "save-call!", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1930z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72012za7,
		BGl_z62callzd2graphz12zd2setzd2exzd2i1356z70zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1931z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72013za7,
		BGl_z62callzd2graphz12zd2jumpzd2exzd21358z70zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1932z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72014za7,
		BGl_z62callzd2graphz12zd2makezd2box1360za2zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1933z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72015za7,
		BGl_z62callzd2graphz12zd2boxzd2ref1362za2zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1940z00zzeffect_cgraphz00,
		BgL_bgl_string1940za700za7za7e2016za7, "effect_cgraph", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1934z00zzeffect_cgraphz00,
		BgL_bgl_za762callza7d2graphza72017za7,
		BGl_z62callzd2graphz12zd2boxzd2setz121364zb0zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1941z00zzeffect_cgraphz00,
		BgL_bgl_string1941za700za7za7e2018za7,
		"import done save-call!1365 _ global/from effect_cgraph local/from obj from nothing ",
		83);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1935z00zzeffect_cgraphz00,
		BgL_bgl_za762saveza7d2callza712019za7,
		BGl_z62savezd2callz12zd2global1369z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1937z00zzeffect_cgraphz00,
		BgL_bgl_za762saveza7d2callza712020za7,
		BGl_z62savezd2callz12zd2globalzf2fr1371z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1938z00zzeffect_cgraphz00,
		BgL_bgl_za762saveza7d2callza712021za7,
		BGl_z62savezd2callz12zd2local1374z70zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1939z00zzeffect_cgraphz00,
		BgL_bgl_za762saveza7d2callza712022za7,
		BGl_z62savezd2callz12zd2localzf2fro1377z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2initzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2023z00,
		BGl_z62globalzf2fromzd2initz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2nilzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72024za7,
		BGl_z62localzf2fromzd2nilz42zzeffect_cgraphz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2accesszd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2025z00,
		BGl_z62globalzf2fromzd2accessz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2varzf2allzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762getza7d2varza7f2a2026za7,
		BGl_z62getzd2varzf2allz42zzeffect_cgraphz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2initzd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2027z00,
		BGl_z62globalzf2fromzd2initzd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2evaluablezf3zd2envz01zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2028z00,
		BGl_z62globalzf2fromzd2evaluablezf3zb1zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2valuezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72029za7,
		BGl_z62localzf2fromzd2valuezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2typezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72030za7,
		BGl_z62localzf2fromzd2typezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2varzf2sidezd2effectzd2envz20zzeffect_cgraphz00,
		BgL_bgl_za762getza7d2varza7f2s2031za7,
		BGl_z62getzd2varzf2sidezd2effectz90zzeffect_cgraphz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1898z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1563za7622032z00, BGl_z62lambda1563z62zzeffect_cgraphz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2valuezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2033z00,
		BGl_z62globalzf2fromzd2valuezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1899z00zzeffect_cgraphz00,
		BgL_bgl_za762lambda1562za7622034z00, BGl_z62lambda1562z62zzeffect_cgraphz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2srczd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2035z00,
		BGl_z62globalzf2fromzd2srczd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2fromzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2036z00,
		BGl_z62globalzf2fromzd2fromz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2fromzd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2037z00,
		BGl_z62globalzf2fromzd2fromzd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2libraryzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2038z00,
		BGl_z62globalzf2fromzd2libraryz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2fromzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72039za7,
		BGl_z62localzf2fromzd2fromz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2idzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72040za7,
		BGl_z62localzf2fromzd2idz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_savezd2callz12zd2envz12zzeffect_cgraphz00,
		BgL_bgl_za762saveza7d2callza712041za7,
		BGl_z62savezd2callz12za2zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2removablezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2042z00,
		BGl_z62globalzf2fromzd2removablez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2aliaszd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2043z00,
		BGl_z62globalzf2fromzd2aliaszd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2fromzd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72044za7,
		BGl_z62localzf2fromzd2fromzd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2userzf3zd2setz12zd2envzc1zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72045za7,
		BGl_z62localzf2fromzd2userzf3zd2setz12z71zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2namezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2046z00,
		BGl_z62globalzf2fromzd2namez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2occurrencezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72047za7,
		BGl_z62localzf2fromzd2occurrencez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2userzf3zd2setz12zd2envzc1zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2048z00,
		BGl_z62globalzf2fromzd2userzf3zd2setz12z71zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_resetzd2effectzd2tablesz12zd2envzc0zzeffect_cgraphz00,
		BgL_bgl_za762resetza7d2effec2049z00,
		BGl_z62resetzd2effectzd2tablesz12z70zzeffect_cgraphz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2occurrencezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2050z00,
		BGl_z62globalzf2fromzd2occurrencez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzf2fromzf3zd2envzd3zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72051za7,
		BGl_z62localzf2fromzf3z63zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2libraryzd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2052z00,
		BGl_z62globalzf2fromzd2libraryzd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2pragmazd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2053z00,
		BGl_z62globalzf2fromzd2pragmazd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2removablezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72054za7,
		BGl_z62localzf2fromzd2removablez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2keyzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72055za7,
		BGl_z62localzf2fromzd2keyz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2jvmzd2typezd2namezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2056z00,
		BGl_z62globalzf2fromzd2jvmzd2typezd2namezd2setz12z82zzeffect_cgraphz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2namezd2setz12zd2envz32zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2057z00,
		BGl_z62globalzf2fromzd2namezd2setz12z82zzeffect_cgraphz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2fromzd2namezd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762localza7f2fromza72058za7,
		BGl_z62localzf2fromzd2namez42zzeffect_cgraphz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2evaluablezf3zd2setz12zd2envzc1zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2059z00,
		BGl_z62globalzf2fromzd2evaluablezf3zd2setz12z71zzeffect_cgraphz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2localzf2fromzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762makeza7d2localza72060za7,
		BGl_z62makezd2localzf2fromz42zzeffect_cgraphz00, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2fromzd2importzd2envzf2zzeffect_cgraphz00,
		BgL_bgl_za762globalza7f2from2061z00,
		BGl_z62globalzf2fromzd2importz42zzeffect_cgraphz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzeffect_cgraphz00));
		     ADD_ROOT((void
				*) (&BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00));
		     ADD_ROOT((void *) (&BGl_localzf2fromzf2zzeffect_cgraphz00));
		     ADD_ROOT((void *) (&BGl_za2varzf2allza2zf2zzeffect_cgraphz00));
		     ADD_ROOT((void *) (&BGl_globalzf2fromzf2zzeffect_cgraphz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzeffect_cgraphz00(long
		BgL_checksumz00_3267, char *BgL_fromz00_3268)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzeffect_cgraphz00))
				{
					BGl_requirezd2initializa7ationz75zzeffect_cgraphz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzeffect_cgraphz00();
					BGl_libraryzd2moduleszd2initz00zzeffect_cgraphz00();
					BGl_cnstzd2initzd2zzeffect_cgraphz00();
					BGl_importedzd2moduleszd2initz00zzeffect_cgraphz00();
					BGl_objectzd2initzd2zzeffect_cgraphz00();
					BGl_genericzd2initzd2zzeffect_cgraphz00();
					BGl_methodzd2initzd2zzeffect_cgraphz00();
					return BGl_toplevelzd2initzd2zzeffect_cgraphz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "effect_cgraph");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"effect_cgraph");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "effect_cgraph");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"effect_cgraph");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "effect_cgraph");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"effect_cgraph");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "effect_cgraph");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"effect_cgraph");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"effect_cgraph");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"effect_cgraph");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 15 */
			{	/* Effect/cgraph.scm 15 */
				obj_t BgL_cportz00_3011;

				{	/* Effect/cgraph.scm 15 */
					obj_t BgL_stringz00_3018;

					BgL_stringz00_3018 = BGl_string1941z00zzeffect_cgraphz00;
					{	/* Effect/cgraph.scm 15 */
						obj_t BgL_startz00_3019;

						BgL_startz00_3019 = BINT(0L);
						{	/* Effect/cgraph.scm 15 */
							obj_t BgL_endz00_3020;

							BgL_endz00_3020 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3018)));
							{	/* Effect/cgraph.scm 15 */

								BgL_cportz00_3011 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3018, BgL_startz00_3019, BgL_endz00_3020);
				}}}}
				{
					long BgL_iz00_3012;

					BgL_iz00_3012 = 9L;
				BgL_loopz00_3013:
					if ((BgL_iz00_3012 == -1L))
						{	/* Effect/cgraph.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Effect/cgraph.scm 15 */
							{	/* Effect/cgraph.scm 15 */
								obj_t BgL_arg1942z00_3014;

								{	/* Effect/cgraph.scm 15 */

									{	/* Effect/cgraph.scm 15 */
										obj_t BgL_locationz00_3016;

										BgL_locationz00_3016 = BBOOL(((bool_t) 0));
										{	/* Effect/cgraph.scm 15 */

											BgL_arg1942z00_3014 =
												BGl_readz00zz__readerz00(BgL_cportz00_3011,
												BgL_locationz00_3016);
										}
									}
								}
								{	/* Effect/cgraph.scm 15 */
									int BgL_tmpz00_3299;

									BgL_tmpz00_3299 = (int) (BgL_iz00_3012);
									CNST_TABLE_SET(BgL_tmpz00_3299, BgL_arg1942z00_3014);
							}}
							{	/* Effect/cgraph.scm 15 */
								int BgL_auxz00_3017;

								BgL_auxz00_3017 = (int) ((BgL_iz00_3012 - 1L));
								{
									long BgL_iz00_3304;

									BgL_iz00_3304 = (long) (BgL_auxz00_3017);
									BgL_iz00_3012 = BgL_iz00_3304;
									goto BgL_loopz00_3013;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 15 */
			BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00 = BNIL;
			return (BGl_za2varzf2allza2zf2zzeffect_cgraphz00 = BNIL, BUNSPEC);
		}

	}



/* make-local/from */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2localzf2fromz20zzeffect_cgraphz00(obj_t BgL_id1163z00_3,
		obj_t BgL_name1164z00_4, BgL_typez00_bglt BgL_type1165z00_5,
		BgL_valuez00_bglt BgL_value1166z00_6, obj_t BgL_access1167z00_7,
		obj_t BgL_fastzd2alpha1168zd2_8, obj_t BgL_removable1169z00_9,
		long BgL_occurrence1170z00_10, long BgL_occurrencew1171z00_11,
		bool_t BgL_userzf31172zf3_12, long BgL_key1173z00_13,
		obj_t BgL_from1174z00_14)
	{
		{	/* Effect/cgraph.sch 91 */
			{	/* Effect/cgraph.sch 91 */
				BgL_localz00_bglt BgL_new1157z00_3022;

				{	/* Effect/cgraph.sch 91 */
					BgL_localz00_bglt BgL_tmp1155z00_3023;
					BgL_localzf2fromzf2_bglt BgL_wide1156z00_3024;

					{
						BgL_localz00_bglt BgL_auxz00_3307;

						{	/* Effect/cgraph.sch 91 */
							BgL_localz00_bglt BgL_new1154z00_3025;

							BgL_new1154z00_3025 =
								((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_localz00_bgl))));
							{	/* Effect/cgraph.sch 91 */
								long BgL_arg1422z00_3026;

								BgL_arg1422z00_3026 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1154z00_3025),
									BgL_arg1422z00_3026);
							}
							{	/* Effect/cgraph.sch 91 */
								BgL_objectz00_bglt BgL_tmpz00_3312;

								BgL_tmpz00_3312 = ((BgL_objectz00_bglt) BgL_new1154z00_3025);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3312, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1154z00_3025);
							BgL_auxz00_3307 = BgL_new1154z00_3025;
						}
						BgL_tmp1155z00_3023 = ((BgL_localz00_bglt) BgL_auxz00_3307);
					}
					BgL_wide1156z00_3024 =
						((BgL_localzf2fromzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_localzf2fromzf2_bgl))));
					{	/* Effect/cgraph.sch 91 */
						obj_t BgL_auxz00_3320;
						BgL_objectz00_bglt BgL_tmpz00_3318;

						BgL_auxz00_3320 = ((obj_t) BgL_wide1156z00_3024);
						BgL_tmpz00_3318 = ((BgL_objectz00_bglt) BgL_tmp1155z00_3023);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3318, BgL_auxz00_3320);
					}
					((BgL_objectz00_bglt) BgL_tmp1155z00_3023);
					{	/* Effect/cgraph.sch 91 */
						long BgL_arg1421z00_3027;

						BgL_arg1421z00_3027 =
							BGL_CLASS_NUM(BGl_localzf2fromzf2zzeffect_cgraphz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1155z00_3023), BgL_arg1421z00_3027);
					}
					BgL_new1157z00_3022 = ((BgL_localz00_bglt) BgL_tmp1155z00_3023);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1157z00_3022)))->BgL_idz00) =
					((obj_t) BgL_id1163z00_3), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1157z00_3022)))->BgL_namez00) =
					((obj_t) BgL_name1164z00_4), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1157z00_3022)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1165z00_5), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1157z00_3022)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1166z00_6), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1157z00_3022)))->BgL_accessz00) =
					((obj_t) BgL_access1167z00_7), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1157z00_3022)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1168zd2_8), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1157z00_3022)))->BgL_removablez00) =
					((obj_t) BgL_removable1169z00_9), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1157z00_3022)))->BgL_occurrencez00) =
					((long) BgL_occurrence1170z00_10), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1157z00_3022)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1171z00_11), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1157z00_3022)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31172zf3_12), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1157z00_3022)))->BgL_keyz00) =
					((long) BgL_key1173z00_13), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1157z00_3022)))->BgL_valzd2noescapezd2) =
					((obj_t) BTRUE), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1157z00_3022)))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2fromzf2_bglt BgL_auxz00_3354;

					{
						obj_t BgL_auxz00_3355;

						{	/* Effect/cgraph.sch 91 */
							BgL_objectz00_bglt BgL_tmpz00_3356;

							BgL_tmpz00_3356 = ((BgL_objectz00_bglt) BgL_new1157z00_3022);
							BgL_auxz00_3355 = BGL_OBJECT_WIDENING(BgL_tmpz00_3356);
						}
						BgL_auxz00_3354 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_3355);
					}
					((((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_3354))->
							BgL_fromz00) = ((obj_t) BgL_from1174z00_14), BUNSPEC);
				}
				return BgL_new1157z00_3022;
			}
		}

	}



/* &make-local/from */
	BgL_localz00_bglt BGl_z62makezd2localzf2fromz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2636, obj_t BgL_id1163z00_2637, obj_t BgL_name1164z00_2638,
		obj_t BgL_type1165z00_2639, obj_t BgL_value1166z00_2640,
		obj_t BgL_access1167z00_2641, obj_t BgL_fastzd2alpha1168zd2_2642,
		obj_t BgL_removable1169z00_2643, obj_t BgL_occurrence1170z00_2644,
		obj_t BgL_occurrencew1171z00_2645, obj_t BgL_userzf31172zf3_2646,
		obj_t BgL_key1173z00_2647, obj_t BgL_from1174z00_2648)
	{
		{	/* Effect/cgraph.sch 91 */
			return
				BGl_makezd2localzf2fromz20zzeffect_cgraphz00(BgL_id1163z00_2637,
				BgL_name1164z00_2638, ((BgL_typez00_bglt) BgL_type1165z00_2639),
				((BgL_valuez00_bglt) BgL_value1166z00_2640), BgL_access1167z00_2641,
				BgL_fastzd2alpha1168zd2_2642, BgL_removable1169z00_2643,
				(long) CINT(BgL_occurrence1170z00_2644),
				(long) CINT(BgL_occurrencew1171z00_2645),
				CBOOL(BgL_userzf31172zf3_2646), (long) CINT(BgL_key1173z00_2647),
				BgL_from1174z00_2648);
		}

	}



/* local/from? */
	BGL_EXPORTED_DEF bool_t BGl_localzf2fromzf3z01zzeffect_cgraphz00(obj_t
		BgL_objz00_15)
	{
		{	/* Effect/cgraph.sch 92 */
			{	/* Effect/cgraph.sch 92 */
				obj_t BgL_classz00_3028;

				BgL_classz00_3028 = BGl_localzf2fromzf2zzeffect_cgraphz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* Effect/cgraph.sch 92 */
						BgL_objectz00_bglt BgL_arg1807z00_3029;

						BgL_arg1807z00_3029 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Effect/cgraph.sch 92 */
								long BgL_idxz00_3030;

								BgL_idxz00_3030 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3029);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3030 + 3L)) == BgL_classz00_3028);
							}
						else
							{	/* Effect/cgraph.sch 92 */
								bool_t BgL_res1878z00_3033;

								{	/* Effect/cgraph.sch 92 */
									obj_t BgL_oclassz00_3034;

									{	/* Effect/cgraph.sch 92 */
										obj_t BgL_arg1815z00_3035;
										long BgL_arg1816z00_3036;

										BgL_arg1815z00_3035 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Effect/cgraph.sch 92 */
											long BgL_arg1817z00_3037;

											BgL_arg1817z00_3037 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3029);
											BgL_arg1816z00_3036 = (BgL_arg1817z00_3037 - OBJECT_TYPE);
										}
										BgL_oclassz00_3034 =
											VECTOR_REF(BgL_arg1815z00_3035, BgL_arg1816z00_3036);
									}
									{	/* Effect/cgraph.sch 92 */
										bool_t BgL__ortest_1115z00_3038;

										BgL__ortest_1115z00_3038 =
											(BgL_classz00_3028 == BgL_oclassz00_3034);
										if (BgL__ortest_1115z00_3038)
											{	/* Effect/cgraph.sch 92 */
												BgL_res1878z00_3033 = BgL__ortest_1115z00_3038;
											}
										else
											{	/* Effect/cgraph.sch 92 */
												long BgL_odepthz00_3039;

												{	/* Effect/cgraph.sch 92 */
													obj_t BgL_arg1804z00_3040;

													BgL_arg1804z00_3040 = (BgL_oclassz00_3034);
													BgL_odepthz00_3039 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3040);
												}
												if ((3L < BgL_odepthz00_3039))
													{	/* Effect/cgraph.sch 92 */
														obj_t BgL_arg1802z00_3041;

														{	/* Effect/cgraph.sch 92 */
															obj_t BgL_arg1803z00_3042;

															BgL_arg1803z00_3042 = (BgL_oclassz00_3034);
															BgL_arg1802z00_3041 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3042,
																3L);
														}
														BgL_res1878z00_3033 =
															(BgL_arg1802z00_3041 == BgL_classz00_3028);
													}
												else
													{	/* Effect/cgraph.sch 92 */
														BgL_res1878z00_3033 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1878z00_3033;
							}
					}
				else
					{	/* Effect/cgraph.sch 92 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &local/from? */
	obj_t BGl_z62localzf2fromzf3z63zzeffect_cgraphz00(obj_t BgL_envz00_2649,
		obj_t BgL_objz00_2650)
	{
		{	/* Effect/cgraph.sch 92 */
			return BBOOL(BGl_localzf2fromzf3z01zzeffect_cgraphz00(BgL_objz00_2650));
		}

	}



/* local/from-nil */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_localzf2fromzd2nilz20zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.sch 93 */
			{	/* Effect/cgraph.sch 93 */
				obj_t BgL_classz00_2122;

				BgL_classz00_2122 = BGl_localzf2fromzf2zzeffect_cgraphz00;
				{	/* Effect/cgraph.sch 93 */
					obj_t BgL__ortest_1117z00_2123;

					BgL__ortest_1117z00_2123 = BGL_CLASS_NIL(BgL_classz00_2122);
					if (CBOOL(BgL__ortest_1117z00_2123))
						{	/* Effect/cgraph.sch 93 */
							return ((BgL_localz00_bglt) BgL__ortest_1117z00_2123);
						}
					else
						{	/* Effect/cgraph.sch 93 */
							return
								((BgL_localz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2122));
						}
				}
			}
		}

	}



/* &local/from-nil */
	BgL_localz00_bglt BGl_z62localzf2fromzd2nilz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2651)
	{
		{	/* Effect/cgraph.sch 93 */
			return BGl_localzf2fromzd2nilz20zzeffect_cgraphz00();
		}

	}



/* local/from-from */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2fromz20zzeffect_cgraphz00(BgL_localz00_bglt BgL_oz00_16)
	{
		{	/* Effect/cgraph.sch 94 */
			{
				BgL_localzf2fromzf2_bglt BgL_auxz00_3399;

				{
					obj_t BgL_auxz00_3400;

					{	/* Effect/cgraph.sch 94 */
						BgL_objectz00_bglt BgL_tmpz00_3401;

						BgL_tmpz00_3401 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_3400 = BGL_OBJECT_WIDENING(BgL_tmpz00_3401);
					}
					BgL_auxz00_3399 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_3400);
				}
				return
					(((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_3399))->BgL_fromz00);
			}
		}

	}



/* &local/from-from */
	obj_t BGl_z62localzf2fromzd2fromz42zzeffect_cgraphz00(obj_t BgL_envz00_2652,
		obj_t BgL_oz00_2653)
	{
		{	/* Effect/cgraph.sch 94 */
			return
				BGl_localzf2fromzd2fromz20zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2653));
		}

	}



/* local/from-from-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2fromzd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* Effect/cgraph.sch 95 */
			{
				BgL_localzf2fromzf2_bglt BgL_auxz00_3408;

				{
					obj_t BgL_auxz00_3409;

					{	/* Effect/cgraph.sch 95 */
						BgL_objectz00_bglt BgL_tmpz00_3410;

						BgL_tmpz00_3410 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_3409 = BGL_OBJECT_WIDENING(BgL_tmpz00_3410);
					}
					BgL_auxz00_3408 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_3409);
				}
				return
					((((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_3408))->
						BgL_fromz00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &local/from-from-set! */
	obj_t BGl_z62localzf2fromzd2fromzd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2654, obj_t BgL_oz00_2655, obj_t BgL_vz00_2656)
	{
		{	/* Effect/cgraph.sch 95 */
			return
				BGl_localzf2fromzd2fromzd2setz12ze0zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2655), BgL_vz00_2656);
		}

	}



/* local/from-key */
	BGL_EXPORTED_DEF long
		BGl_localzf2fromzd2keyz20zzeffect_cgraphz00(BgL_localz00_bglt BgL_oz00_19)
	{
		{	/* Effect/cgraph.sch 96 */
			return
				(((BgL_localz00_bglt) COBJECT(
						((BgL_localz00_bglt) BgL_oz00_19)))->BgL_keyz00);
		}

	}



/* &local/from-key */
	obj_t BGl_z62localzf2fromzd2keyz42zzeffect_cgraphz00(obj_t BgL_envz00_2657,
		obj_t BgL_oz00_2658)
	{
		{	/* Effect/cgraph.sch 96 */
			return
				BINT(BGl_localzf2fromzd2keyz20zzeffect_cgraphz00(
					((BgL_localz00_bglt) BgL_oz00_2658)));
		}

	}



/* local/from-user? */
	BGL_EXPORTED_DEF bool_t
		BGl_localzf2fromzd2userzf3zd3zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_22)
	{
		{	/* Effect/cgraph.sch 98 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_22)))->BgL_userzf3zf3);
		}

	}



/* &local/from-user? */
	obj_t BGl_z62localzf2fromzd2userzf3zb1zzeffect_cgraphz00(obj_t
		BgL_envz00_2659, obj_t BgL_oz00_2660)
	{
		{	/* Effect/cgraph.sch 98 */
			return
				BBOOL(BGl_localzf2fromzd2userzf3zd3zzeffect_cgraphz00(
					((BgL_localz00_bglt) BgL_oz00_2660)));
		}

	}



/* local/from-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2userzf3zd2setz12z13zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_23, bool_t BgL_vz00_24)
	{
		{	/* Effect/cgraph.sch 99 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_23)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_24), BUNSPEC);
		}

	}



/* &local/from-user?-set! */
	obj_t BGl_z62localzf2fromzd2userzf3zd2setz12z71zzeffect_cgraphz00(obj_t
		BgL_envz00_2661, obj_t BgL_oz00_2662, obj_t BgL_vz00_2663)
	{
		{	/* Effect/cgraph.sch 99 */
			return
				BGl_localzf2fromzd2userzf3zd2setz12z13zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2662), CBOOL(BgL_vz00_2663));
		}

	}



/* local/from-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_localzf2fromzd2occurrencewz20zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_25)
	{
		{	/* Effect/cgraph.sch 100 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_25)))->BgL_occurrencewz00);
		}

	}



/* &local/from-occurrencew */
	obj_t BGl_z62localzf2fromzd2occurrencewz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2664, obj_t BgL_oz00_2665)
	{
		{	/* Effect/cgraph.sch 100 */
			return
				BINT(BGl_localzf2fromzd2occurrencewz20zzeffect_cgraphz00(
					((BgL_localz00_bglt) BgL_oz00_2665)));
		}

	}



/* local/from-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2occurrencewzd2setz12ze0zzeffect_cgraphz00
		(BgL_localz00_bglt BgL_oz00_26, long BgL_vz00_27)
	{
		{	/* Effect/cgraph.sch 101 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_26)))->BgL_occurrencewz00) =
				((long) BgL_vz00_27), BUNSPEC);
		}

	}



/* &local/from-occurrencew-set! */
	obj_t BGl_z62localzf2fromzd2occurrencewzd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2666, obj_t BgL_oz00_2667, obj_t BgL_vz00_2668)
	{
		{	/* Effect/cgraph.sch 101 */
			return
				BGl_localzf2fromzd2occurrencewzd2setz12ze0zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2667), (long) CINT(BgL_vz00_2668));
		}

	}



/* local/from-occurrence */
	BGL_EXPORTED_DEF long
		BGl_localzf2fromzd2occurrencez20zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_28)
	{
		{	/* Effect/cgraph.sch 102 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_28)))->BgL_occurrencez00);
		}

	}



/* &local/from-occurrence */
	obj_t BGl_z62localzf2fromzd2occurrencez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2669, obj_t BgL_oz00_2670)
	{
		{	/* Effect/cgraph.sch 102 */
			return
				BINT(BGl_localzf2fromzd2occurrencez20zzeffect_cgraphz00(
					((BgL_localz00_bglt) BgL_oz00_2670)));
		}

	}



/* local/from-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2occurrencezd2setz12ze0zzeffect_cgraphz00
		(BgL_localz00_bglt BgL_oz00_29, long BgL_vz00_30)
	{
		{	/* Effect/cgraph.sch 103 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_29)))->BgL_occurrencez00) =
				((long) BgL_vz00_30), BUNSPEC);
		}

	}



/* &local/from-occurrence-set! */
	obj_t BGl_z62localzf2fromzd2occurrencezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2671, obj_t BgL_oz00_2672, obj_t BgL_vz00_2673)
	{
		{	/* Effect/cgraph.sch 103 */
			return
				BGl_localzf2fromzd2occurrencezd2setz12ze0zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2672), (long) CINT(BgL_vz00_2673));
		}

	}



/* local/from-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2removablez20zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_31)
	{
		{	/* Effect/cgraph.sch 104 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_31)))->BgL_removablez00);
		}

	}



/* &local/from-removable */
	obj_t BGl_z62localzf2fromzd2removablez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2674, obj_t BgL_oz00_2675)
	{
		{	/* Effect/cgraph.sch 104 */
			return
				BGl_localzf2fromzd2removablez20zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2675));
		}

	}



/* local/from-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2removablezd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_32, obj_t BgL_vz00_33)
	{
		{	/* Effect/cgraph.sch 105 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_32)))->BgL_removablez00) =
				((obj_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &local/from-removable-set! */
	obj_t BGl_z62localzf2fromzd2removablezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2676, obj_t BgL_oz00_2677, obj_t BgL_vz00_2678)
	{
		{	/* Effect/cgraph.sch 105 */
			return
				BGl_localzf2fromzd2removablezd2setz12ze0zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2677), BgL_vz00_2678);
		}

	}



/* local/from-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2fastzd2alphazf2zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_34)
	{
		{	/* Effect/cgraph.sch 106 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_34)))->BgL_fastzd2alphazd2);
		}

	}



/* &local/from-fast-alpha */
	obj_t BGl_z62localzf2fromzd2fastzd2alphaz90zzeffect_cgraphz00(obj_t
		BgL_envz00_2679, obj_t BgL_oz00_2680)
	{
		{	/* Effect/cgraph.sch 106 */
			return
				BGl_localzf2fromzd2fastzd2alphazf2zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2680));
		}

	}



/* local/from-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2fastzd2alphazd2setz12z32zzeffect_cgraphz00
		(BgL_localz00_bglt BgL_oz00_35, obj_t BgL_vz00_36)
	{
		{	/* Effect/cgraph.sch 107 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_35)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &local/from-fast-alpha-set! */
	obj_t BGl_z62localzf2fromzd2fastzd2alphazd2setz12z50zzeffect_cgraphz00(obj_t
		BgL_envz00_2681, obj_t BgL_oz00_2682, obj_t BgL_vz00_2683)
	{
		{	/* Effect/cgraph.sch 107 */
			return
				BGl_localzf2fromzd2fastzd2alphazd2setz12z32zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2682), BgL_vz00_2683);
		}

	}



/* local/from-access */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2accessz20zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_37)
	{
		{	/* Effect/cgraph.sch 108 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_37)))->BgL_accessz00);
		}

	}



/* &local/from-access */
	obj_t BGl_z62localzf2fromzd2accessz42zzeffect_cgraphz00(obj_t BgL_envz00_2684,
		obj_t BgL_oz00_2685)
	{
		{	/* Effect/cgraph.sch 108 */
			return
				BGl_localzf2fromzd2accessz20zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2685));
		}

	}



/* local/from-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2accesszd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_38, obj_t BgL_vz00_39)
	{
		{	/* Effect/cgraph.sch 109 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_38)))->BgL_accessz00) =
				((obj_t) BgL_vz00_39), BUNSPEC);
		}

	}



/* &local/from-access-set! */
	obj_t BGl_z62localzf2fromzd2accesszd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2686, obj_t BgL_oz00_2687, obj_t BgL_vz00_2688)
	{
		{	/* Effect/cgraph.sch 109 */
			return
				BGl_localzf2fromzd2accesszd2setz12ze0zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2687), BgL_vz00_2688);
		}

	}



/* local/from-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_localzf2fromzd2valuez20zzeffect_cgraphz00(BgL_localz00_bglt BgL_oz00_40)
	{
		{	/* Effect/cgraph.sch 110 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_40)))->BgL_valuez00);
		}

	}



/* &local/from-value */
	BgL_valuez00_bglt BGl_z62localzf2fromzd2valuez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2689, obj_t BgL_oz00_2690)
	{
		{	/* Effect/cgraph.sch 110 */
			return
				BGl_localzf2fromzd2valuez20zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2690));
		}

	}



/* local/from-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2valuezd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_41, BgL_valuez00_bglt BgL_vz00_42)
	{
		{	/* Effect/cgraph.sch 111 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_41)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_42), BUNSPEC);
		}

	}



/* &local/from-value-set! */
	obj_t BGl_z62localzf2fromzd2valuezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2691, obj_t BgL_oz00_2692, obj_t BgL_vz00_2693)
	{
		{	/* Effect/cgraph.sch 111 */
			return
				BGl_localzf2fromzd2valuezd2setz12ze0zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2692),
				((BgL_valuez00_bglt) BgL_vz00_2693));
		}

	}



/* local/from-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_localzf2fromzd2typez20zzeffect_cgraphz00(BgL_localz00_bglt BgL_oz00_43)
	{
		{	/* Effect/cgraph.sch 112 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_43)))->BgL_typez00);
		}

	}



/* &local/from-type */
	BgL_typez00_bglt BGl_z62localzf2fromzd2typez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2694, obj_t BgL_oz00_2695)
	{
		{	/* Effect/cgraph.sch 112 */
			return
				BGl_localzf2fromzd2typez20zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2695));
		}

	}



/* local/from-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2typezd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_44, BgL_typez00_bglt BgL_vz00_45)
	{
		{	/* Effect/cgraph.sch 113 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_44)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_45), BUNSPEC);
		}

	}



/* &local/from-type-set! */
	obj_t BGl_z62localzf2fromzd2typezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2696, obj_t BgL_oz00_2697, obj_t BgL_vz00_2698)
	{
		{	/* Effect/cgraph.sch 113 */
			return
				BGl_localzf2fromzd2typezd2setz12ze0zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2697),
				((BgL_typez00_bglt) BgL_vz00_2698));
		}

	}



/* local/from-name */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2namez20zzeffect_cgraphz00(BgL_localz00_bglt BgL_oz00_46)
	{
		{	/* Effect/cgraph.sch 114 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_46)))->BgL_namez00);
		}

	}



/* &local/from-name */
	obj_t BGl_z62localzf2fromzd2namez42zzeffect_cgraphz00(obj_t BgL_envz00_2699,
		obj_t BgL_oz00_2700)
	{
		{	/* Effect/cgraph.sch 114 */
			return
				BGl_localzf2fromzd2namez20zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2700));
		}

	}



/* local/from-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2namezd2setz12ze0zzeffect_cgraphz00(BgL_localz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* Effect/cgraph.sch 115 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_47)))->BgL_namez00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &local/from-name-set! */
	obj_t BGl_z62localzf2fromzd2namezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2701, obj_t BgL_oz00_2702, obj_t BgL_vz00_2703)
	{
		{	/* Effect/cgraph.sch 115 */
			return
				BGl_localzf2fromzd2namezd2setz12ze0zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2702), BgL_vz00_2703);
		}

	}



/* local/from-id */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2fromzd2idz20zzeffect_cgraphz00(BgL_localz00_bglt BgL_oz00_49)
	{
		{	/* Effect/cgraph.sch 116 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_49)))->BgL_idz00);
		}

	}



/* &local/from-id */
	obj_t BGl_z62localzf2fromzd2idz42zzeffect_cgraphz00(obj_t BgL_envz00_2704,
		obj_t BgL_oz00_2705)
	{
		{	/* Effect/cgraph.sch 116 */
			return
				BGl_localzf2fromzd2idz20zzeffect_cgraphz00(
				((BgL_localz00_bglt) BgL_oz00_2705));
		}

	}



/* make-global/from */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_makezd2globalzf2fromz20zzeffect_cgraphz00(obj_t BgL_id1141z00_52,
		obj_t BgL_name1142z00_53, BgL_typez00_bglt BgL_type1143z00_54,
		BgL_valuez00_bglt BgL_value1144z00_55, obj_t BgL_access1145z00_56,
		obj_t BgL_fastzd2alpha1146zd2_57, obj_t BgL_removable1147z00_58,
		long BgL_occurrence1148z00_59, long BgL_occurrencew1149z00_60,
		bool_t BgL_userzf31150zf3_61, obj_t BgL_module1151z00_62,
		obj_t BgL_import1152z00_63, bool_t BgL_evaluablezf31153zf3_64,
		bool_t BgL_evalzf31154zf3_65, obj_t BgL_library1155z00_66,
		obj_t BgL_pragma1156z00_67, obj_t BgL_src1157z00_68,
		obj_t BgL_jvmzd2typezd2name1158z00_69, obj_t BgL_init1159z00_70,
		obj_t BgL_alias1160z00_71, obj_t BgL_from1161z00_72)
	{
		{	/* Effect/cgraph.sch 120 */
			{	/* Effect/cgraph.sch 120 */
				BgL_globalz00_bglt BgL_new1161z00_3043;

				{	/* Effect/cgraph.sch 120 */
					BgL_globalz00_bglt BgL_tmp1159z00_3044;
					BgL_globalzf2fromzf2_bglt BgL_wide1160z00_3045;

					{
						BgL_globalz00_bglt BgL_auxz00_3506;

						{	/* Effect/cgraph.sch 120 */
							BgL_globalz00_bglt BgL_new1158z00_3046;

							BgL_new1158z00_3046 =
								((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_globalz00_bgl))));
							{	/* Effect/cgraph.sch 120 */
								long BgL_arg1437z00_3047;

								BgL_arg1437z00_3047 = BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1158z00_3046),
									BgL_arg1437z00_3047);
							}
							{	/* Effect/cgraph.sch 120 */
								BgL_objectz00_bglt BgL_tmpz00_3511;

								BgL_tmpz00_3511 = ((BgL_objectz00_bglt) BgL_new1158z00_3046);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3511, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1158z00_3046);
							BgL_auxz00_3506 = BgL_new1158z00_3046;
						}
						BgL_tmp1159z00_3044 = ((BgL_globalz00_bglt) BgL_auxz00_3506);
					}
					BgL_wide1160z00_3045 =
						((BgL_globalzf2fromzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_globalzf2fromzf2_bgl))));
					{	/* Effect/cgraph.sch 120 */
						obj_t BgL_auxz00_3519;
						BgL_objectz00_bglt BgL_tmpz00_3517;

						BgL_auxz00_3519 = ((obj_t) BgL_wide1160z00_3045);
						BgL_tmpz00_3517 = ((BgL_objectz00_bglt) BgL_tmp1159z00_3044);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3517, BgL_auxz00_3519);
					}
					((BgL_objectz00_bglt) BgL_tmp1159z00_3044);
					{	/* Effect/cgraph.sch 120 */
						long BgL_arg1434z00_3048;

						BgL_arg1434z00_3048 =
							BGL_CLASS_NUM(BGl_globalzf2fromzf2zzeffect_cgraphz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1159z00_3044), BgL_arg1434z00_3048);
					}
					BgL_new1161z00_3043 = ((BgL_globalz00_bglt) BgL_tmp1159z00_3044);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1161z00_3043)))->BgL_idz00) =
					((obj_t) BgL_id1141z00_52), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1161z00_3043)))->BgL_namez00) =
					((obj_t) BgL_name1142z00_53), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1161z00_3043)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1143z00_54), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1161z00_3043)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1144z00_55), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1161z00_3043)))->BgL_accessz00) =
					((obj_t) BgL_access1145z00_56), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1161z00_3043)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1146zd2_57), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1161z00_3043)))->BgL_removablez00) =
					((obj_t) BgL_removable1147z00_58), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1161z00_3043)))->BgL_occurrencez00) =
					((long) BgL_occurrence1148z00_59), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1161z00_3043)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1149z00_60), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1161z00_3043)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31150zf3_61), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_modulez00) =
					((obj_t) BgL_module1151z00_62), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_importz00) =
					((obj_t) BgL_import1152z00_63), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_evaluablezf3zf3) =
					((bool_t) BgL_evaluablezf31153zf3_64), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_evalzf3zf3) =
					((bool_t) BgL_evalzf31154zf3_65), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_libraryz00) =
					((obj_t) BgL_library1155z00_66), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_pragmaz00) =
					((obj_t) BgL_pragma1156z00_67), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_srcz00) =
					((obj_t) BgL_src1157z00_68), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_jvmzd2typezd2namez00) =
					((obj_t) BgL_jvmzd2typezd2name1158z00_69), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_initz00) =
					((obj_t) BgL_init1159z00_70), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1161z00_3043)))->BgL_aliasz00) =
					((obj_t) BgL_alias1160z00_71), BUNSPEC);
				{
					BgL_globalzf2fromzf2_bglt BgL_auxz00_3567;

					{
						obj_t BgL_auxz00_3568;

						{	/* Effect/cgraph.sch 120 */
							BgL_objectz00_bglt BgL_tmpz00_3569;

							BgL_tmpz00_3569 = ((BgL_objectz00_bglt) BgL_new1161z00_3043);
							BgL_auxz00_3568 = BGL_OBJECT_WIDENING(BgL_tmpz00_3569);
						}
						BgL_auxz00_3567 = ((BgL_globalzf2fromzf2_bglt) BgL_auxz00_3568);
					}
					((((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_3567))->
							BgL_fromz00) = ((obj_t) BgL_from1161z00_72), BUNSPEC);
				}
				return BgL_new1161z00_3043;
			}
		}

	}



/* &make-global/from */
	BgL_globalz00_bglt BGl_z62makezd2globalzf2fromz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2706, obj_t BgL_id1141z00_2707, obj_t BgL_name1142z00_2708,
		obj_t BgL_type1143z00_2709, obj_t BgL_value1144z00_2710,
		obj_t BgL_access1145z00_2711, obj_t BgL_fastzd2alpha1146zd2_2712,
		obj_t BgL_removable1147z00_2713, obj_t BgL_occurrence1148z00_2714,
		obj_t BgL_occurrencew1149z00_2715, obj_t BgL_userzf31150zf3_2716,
		obj_t BgL_module1151z00_2717, obj_t BgL_import1152z00_2718,
		obj_t BgL_evaluablezf31153zf3_2719, obj_t BgL_evalzf31154zf3_2720,
		obj_t BgL_library1155z00_2721, obj_t BgL_pragma1156z00_2722,
		obj_t BgL_src1157z00_2723, obj_t BgL_jvmzd2typezd2name1158z00_2724,
		obj_t BgL_init1159z00_2725, obj_t BgL_alias1160z00_2726,
		obj_t BgL_from1161z00_2727)
	{
		{	/* Effect/cgraph.sch 120 */
			return
				BGl_makezd2globalzf2fromz20zzeffect_cgraphz00(BgL_id1141z00_2707,
				BgL_name1142z00_2708, ((BgL_typez00_bglt) BgL_type1143z00_2709),
				((BgL_valuez00_bglt) BgL_value1144z00_2710), BgL_access1145z00_2711,
				BgL_fastzd2alpha1146zd2_2712, BgL_removable1147z00_2713,
				(long) CINT(BgL_occurrence1148z00_2714),
				(long) CINT(BgL_occurrencew1149z00_2715),
				CBOOL(BgL_userzf31150zf3_2716), BgL_module1151z00_2717,
				BgL_import1152z00_2718, CBOOL(BgL_evaluablezf31153zf3_2719),
				CBOOL(BgL_evalzf31154zf3_2720), BgL_library1155z00_2721,
				BgL_pragma1156z00_2722, BgL_src1157z00_2723,
				BgL_jvmzd2typezd2name1158z00_2724, BgL_init1159z00_2725,
				BgL_alias1160z00_2726, BgL_from1161z00_2727);
		}

	}



/* global/from? */
	BGL_EXPORTED_DEF bool_t BGl_globalzf2fromzf3z01zzeffect_cgraphz00(obj_t
		BgL_objz00_73)
	{
		{	/* Effect/cgraph.sch 121 */
			{	/* Effect/cgraph.sch 121 */
				obj_t BgL_classz00_3049;

				BgL_classz00_3049 = BGl_globalzf2fromzf2zzeffect_cgraphz00;
				if (BGL_OBJECTP(BgL_objz00_73))
					{	/* Effect/cgraph.sch 121 */
						BgL_objectz00_bglt BgL_arg1807z00_3050;

						BgL_arg1807z00_3050 = (BgL_objectz00_bglt) (BgL_objz00_73);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Effect/cgraph.sch 121 */
								long BgL_idxz00_3051;

								BgL_idxz00_3051 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3050);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3051 + 3L)) == BgL_classz00_3049);
							}
						else
							{	/* Effect/cgraph.sch 121 */
								bool_t BgL_res1879z00_3054;

								{	/* Effect/cgraph.sch 121 */
									obj_t BgL_oclassz00_3055;

									{	/* Effect/cgraph.sch 121 */
										obj_t BgL_arg1815z00_3056;
										long BgL_arg1816z00_3057;

										BgL_arg1815z00_3056 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Effect/cgraph.sch 121 */
											long BgL_arg1817z00_3058;

											BgL_arg1817z00_3058 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3050);
											BgL_arg1816z00_3057 = (BgL_arg1817z00_3058 - OBJECT_TYPE);
										}
										BgL_oclassz00_3055 =
											VECTOR_REF(BgL_arg1815z00_3056, BgL_arg1816z00_3057);
									}
									{	/* Effect/cgraph.sch 121 */
										bool_t BgL__ortest_1115z00_3059;

										BgL__ortest_1115z00_3059 =
											(BgL_classz00_3049 == BgL_oclassz00_3055);
										if (BgL__ortest_1115z00_3059)
											{	/* Effect/cgraph.sch 121 */
												BgL_res1879z00_3054 = BgL__ortest_1115z00_3059;
											}
										else
											{	/* Effect/cgraph.sch 121 */
												long BgL_odepthz00_3060;

												{	/* Effect/cgraph.sch 121 */
													obj_t BgL_arg1804z00_3061;

													BgL_arg1804z00_3061 = (BgL_oclassz00_3055);
													BgL_odepthz00_3060 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3061);
												}
												if ((3L < BgL_odepthz00_3060))
													{	/* Effect/cgraph.sch 121 */
														obj_t BgL_arg1802z00_3062;

														{	/* Effect/cgraph.sch 121 */
															obj_t BgL_arg1803z00_3063;

															BgL_arg1803z00_3063 = (BgL_oclassz00_3055);
															BgL_arg1802z00_3062 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3063,
																3L);
														}
														BgL_res1879z00_3054 =
															(BgL_arg1802z00_3062 == BgL_classz00_3049);
													}
												else
													{	/* Effect/cgraph.sch 121 */
														BgL_res1879z00_3054 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1879z00_3054;
							}
					}
				else
					{	/* Effect/cgraph.sch 121 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &global/from? */
	obj_t BGl_z62globalzf2fromzf3z63zzeffect_cgraphz00(obj_t BgL_envz00_2728,
		obj_t BgL_objz00_2729)
	{
		{	/* Effect/cgraph.sch 121 */
			return BBOOL(BGl_globalzf2fromzf3z01zzeffect_cgraphz00(BgL_objz00_2729));
		}

	}



/* global/from-nil */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_globalzf2fromzd2nilz20zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.sch 122 */
			{	/* Effect/cgraph.sch 122 */
				obj_t BgL_classz00_2175;

				BgL_classz00_2175 = BGl_globalzf2fromzf2zzeffect_cgraphz00;
				{	/* Effect/cgraph.sch 122 */
					obj_t BgL__ortest_1117z00_2176;

					BgL__ortest_1117z00_2176 = BGL_CLASS_NIL(BgL_classz00_2175);
					if (CBOOL(BgL__ortest_1117z00_2176))
						{	/* Effect/cgraph.sch 122 */
							return ((BgL_globalz00_bglt) BgL__ortest_1117z00_2176);
						}
					else
						{	/* Effect/cgraph.sch 122 */
							return
								((BgL_globalz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2175));
						}
				}
			}
		}

	}



/* &global/from-nil */
	BgL_globalz00_bglt BGl_z62globalzf2fromzd2nilz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2730)
	{
		{	/* Effect/cgraph.sch 122 */
			return BGl_globalzf2fromzd2nilz20zzeffect_cgraphz00();
		}

	}



/* global/from-from */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2fromz20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_74)
	{
		{	/* Effect/cgraph.sch 123 */
			{
				BgL_globalzf2fromzf2_bglt BgL_auxz00_3613;

				{
					obj_t BgL_auxz00_3614;

					{	/* Effect/cgraph.sch 123 */
						BgL_objectz00_bglt BgL_tmpz00_3615;

						BgL_tmpz00_3615 = ((BgL_objectz00_bglt) BgL_oz00_74);
						BgL_auxz00_3614 = BGL_OBJECT_WIDENING(BgL_tmpz00_3615);
					}
					BgL_auxz00_3613 = ((BgL_globalzf2fromzf2_bglt) BgL_auxz00_3614);
				}
				return
					(((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_3613))->BgL_fromz00);
			}
		}

	}



/* &global/from-from */
	obj_t BGl_z62globalzf2fromzd2fromz42zzeffect_cgraphz00(obj_t BgL_envz00_2731,
		obj_t BgL_oz00_2732)
	{
		{	/* Effect/cgraph.sch 123 */
			return
				BGl_globalzf2fromzd2fromz20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2732));
		}

	}



/* global/from-from-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2fromzd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_75, obj_t BgL_vz00_76)
	{
		{	/* Effect/cgraph.sch 124 */
			{
				BgL_globalzf2fromzf2_bglt BgL_auxz00_3622;

				{
					obj_t BgL_auxz00_3623;

					{	/* Effect/cgraph.sch 124 */
						BgL_objectz00_bglt BgL_tmpz00_3624;

						BgL_tmpz00_3624 = ((BgL_objectz00_bglt) BgL_oz00_75);
						BgL_auxz00_3623 = BGL_OBJECT_WIDENING(BgL_tmpz00_3624);
					}
					BgL_auxz00_3622 = ((BgL_globalzf2fromzf2_bglt) BgL_auxz00_3623);
				}
				return
					((((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_3622))->
						BgL_fromz00) = ((obj_t) BgL_vz00_76), BUNSPEC);
			}
		}

	}



/* &global/from-from-set! */
	obj_t BGl_z62globalzf2fromzd2fromzd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2733, obj_t BgL_oz00_2734, obj_t BgL_vz00_2735)
	{
		{	/* Effect/cgraph.sch 124 */
			return
				BGl_globalzf2fromzd2fromzd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2734), BgL_vz00_2735);
		}

	}



/* global/from-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2aliasz20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_77)
	{
		{	/* Effect/cgraph.sch 125 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_77)))->BgL_aliasz00);
		}

	}



/* &global/from-alias */
	obj_t BGl_z62globalzf2fromzd2aliasz42zzeffect_cgraphz00(obj_t BgL_envz00_2736,
		obj_t BgL_oz00_2737)
	{
		{	/* Effect/cgraph.sch 125 */
			return
				BGl_globalzf2fromzd2aliasz20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2737));
		}

	}



/* global/from-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2aliaszd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_78, obj_t BgL_vz00_79)
	{
		{	/* Effect/cgraph.sch 126 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_78)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_79), BUNSPEC);
		}

	}



/* &global/from-alias-set! */
	obj_t BGl_z62globalzf2fromzd2aliaszd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2738, obj_t BgL_oz00_2739, obj_t BgL_vz00_2740)
	{
		{	/* Effect/cgraph.sch 126 */
			return
				BGl_globalzf2fromzd2aliaszd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2739), BgL_vz00_2740);
		}

	}



/* global/from-init */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2initz20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_80)
	{
		{	/* Effect/cgraph.sch 127 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_80)))->BgL_initz00);
		}

	}



/* &global/from-init */
	obj_t BGl_z62globalzf2fromzd2initz42zzeffect_cgraphz00(obj_t BgL_envz00_2741,
		obj_t BgL_oz00_2742)
	{
		{	/* Effect/cgraph.sch 127 */
			return
				BGl_globalzf2fromzd2initz20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2742));
		}

	}



/* global/from-init-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2initzd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_81, obj_t BgL_vz00_82)
	{
		{	/* Effect/cgraph.sch 128 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_81)))->BgL_initz00) =
				((obj_t) BgL_vz00_82), BUNSPEC);
		}

	}



/* &global/from-init-set! */
	obj_t BGl_z62globalzf2fromzd2initzd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2743, obj_t BgL_oz00_2744, obj_t BgL_vz00_2745)
	{
		{	/* Effect/cgraph.sch 128 */
			return
				BGl_globalzf2fromzd2initzd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2744), BgL_vz00_2745);
		}

	}



/* global/from-jvm-type-name */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2jvmzd2typezd2namez20zzeffect_cgraphz00
		(BgL_globalz00_bglt BgL_oz00_83)
	{
		{	/* Effect/cgraph.sch 129 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_83)))->BgL_jvmzd2typezd2namez00);
		}

	}



/* &global/from-jvm-type-name */
	obj_t BGl_z62globalzf2fromzd2jvmzd2typezd2namez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2746, obj_t BgL_oz00_2747)
	{
		{	/* Effect/cgraph.sch 129 */
			return
				BGl_globalzf2fromzd2jvmzd2typezd2namez20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2747));
		}

	}



/* global/from-jvm-type-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2jvmzd2typezd2namezd2setz12ze0zzeffect_cgraphz00
		(BgL_globalz00_bglt BgL_oz00_84, obj_t BgL_vz00_85)
	{
		{	/* Effect/cgraph.sch 130 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_84)))->BgL_jvmzd2typezd2namez00) =
				((obj_t) BgL_vz00_85), BUNSPEC);
		}

	}



/* &global/from-jvm-type-name-set! */
	obj_t
		BGl_z62globalzf2fromzd2jvmzd2typezd2namezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2748, obj_t BgL_oz00_2749, obj_t BgL_vz00_2750)
	{
		{	/* Effect/cgraph.sch 130 */
			return
				BGl_globalzf2fromzd2jvmzd2typezd2namezd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2749), BgL_vz00_2750);
		}

	}



/* global/from-src */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2srcz20zzeffect_cgraphz00(BgL_globalz00_bglt BgL_oz00_86)
	{
		{	/* Effect/cgraph.sch 131 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_86)))->BgL_srcz00);
		}

	}



/* &global/from-src */
	obj_t BGl_z62globalzf2fromzd2srcz42zzeffect_cgraphz00(obj_t BgL_envz00_2751,
		obj_t BgL_oz00_2752)
	{
		{	/* Effect/cgraph.sch 131 */
			return
				BGl_globalzf2fromzd2srcz20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2752));
		}

	}



/* global/from-src-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2srczd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_87, obj_t BgL_vz00_88)
	{
		{	/* Effect/cgraph.sch 132 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_87)))->BgL_srcz00) =
				((obj_t) BgL_vz00_88), BUNSPEC);
		}

	}



/* &global/from-src-set! */
	obj_t BGl_z62globalzf2fromzd2srczd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2753, obj_t BgL_oz00_2754, obj_t BgL_vz00_2755)
	{
		{	/* Effect/cgraph.sch 132 */
			return
				BGl_globalzf2fromzd2srczd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2754), BgL_vz00_2755);
		}

	}



/* global/from-pragma */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2pragmaz20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_89)
	{
		{	/* Effect/cgraph.sch 133 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_89)))->BgL_pragmaz00);
		}

	}



/* &global/from-pragma */
	obj_t BGl_z62globalzf2fromzd2pragmaz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2756, obj_t BgL_oz00_2757)
	{
		{	/* Effect/cgraph.sch 133 */
			return
				BGl_globalzf2fromzd2pragmaz20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2757));
		}

	}



/* global/from-pragma-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2pragmazd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_90, obj_t BgL_vz00_91)
	{
		{	/* Effect/cgraph.sch 134 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_90)))->BgL_pragmaz00) =
				((obj_t) BgL_vz00_91), BUNSPEC);
		}

	}



/* &global/from-pragma-set! */
	obj_t BGl_z62globalzf2fromzd2pragmazd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2758, obj_t BgL_oz00_2759, obj_t BgL_vz00_2760)
	{
		{	/* Effect/cgraph.sch 134 */
			return
				BGl_globalzf2fromzd2pragmazd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2759), BgL_vz00_2760);
		}

	}



/* global/from-library */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2libraryz20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_92)
	{
		{	/* Effect/cgraph.sch 135 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_92)))->BgL_libraryz00);
		}

	}



/* &global/from-library */
	obj_t BGl_z62globalzf2fromzd2libraryz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2761, obj_t BgL_oz00_2762)
	{
		{	/* Effect/cgraph.sch 135 */
			return
				BGl_globalzf2fromzd2libraryz20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2762));
		}

	}



/* global/from-library-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2libraryzd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_93, obj_t BgL_vz00_94)
	{
		{	/* Effect/cgraph.sch 136 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_93)))->BgL_libraryz00) =
				((obj_t) BgL_vz00_94), BUNSPEC);
		}

	}



/* &global/from-library-set! */
	obj_t BGl_z62globalzf2fromzd2libraryzd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2763, obj_t BgL_oz00_2764, obj_t BgL_vz00_2765)
	{
		{	/* Effect/cgraph.sch 136 */
			return
				BGl_globalzf2fromzd2libraryzd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2764), BgL_vz00_2765);
		}

	}



/* global/from-eval? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzf2fromzd2evalzf3zd3zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_95)
	{
		{	/* Effect/cgraph.sch 137 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_95)))->BgL_evalzf3zf3);
		}

	}



/* &global/from-eval? */
	obj_t BGl_z62globalzf2fromzd2evalzf3zb1zzeffect_cgraphz00(obj_t
		BgL_envz00_2766, obj_t BgL_oz00_2767)
	{
		{	/* Effect/cgraph.sch 137 */
			return
				BBOOL(BGl_globalzf2fromzd2evalzf3zd3zzeffect_cgraphz00(
					((BgL_globalz00_bglt) BgL_oz00_2767)));
		}

	}



/* global/from-eval?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2evalzf3zd2setz12z13zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_96, bool_t BgL_vz00_97)
	{
		{	/* Effect/cgraph.sch 138 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_96)))->BgL_evalzf3zf3) =
				((bool_t) BgL_vz00_97), BUNSPEC);
		}

	}



/* &global/from-eval?-set! */
	obj_t BGl_z62globalzf2fromzd2evalzf3zd2setz12z71zzeffect_cgraphz00(obj_t
		BgL_envz00_2768, obj_t BgL_oz00_2769, obj_t BgL_vz00_2770)
	{
		{	/* Effect/cgraph.sch 138 */
			return
				BGl_globalzf2fromzd2evalzf3zd2setz12z13zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2769), CBOOL(BgL_vz00_2770));
		}

	}



/* global/from-evaluable? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzf2fromzd2evaluablezf3zd3zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_98)
	{
		{	/* Effect/cgraph.sch 139 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_98)))->BgL_evaluablezf3zf3);
		}

	}



/* &global/from-evaluable? */
	obj_t BGl_z62globalzf2fromzd2evaluablezf3zb1zzeffect_cgraphz00(obj_t
		BgL_envz00_2771, obj_t BgL_oz00_2772)
	{
		{	/* Effect/cgraph.sch 139 */
			return
				BBOOL(BGl_globalzf2fromzd2evaluablezf3zd3zzeffect_cgraphz00(
					((BgL_globalz00_bglt) BgL_oz00_2772)));
		}

	}



/* global/from-evaluable?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2evaluablezf3zd2setz12z13zzeffect_cgraphz00
		(BgL_globalz00_bglt BgL_oz00_99, bool_t BgL_vz00_100)
	{
		{	/* Effect/cgraph.sch 140 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_99)))->BgL_evaluablezf3zf3) =
				((bool_t) BgL_vz00_100), BUNSPEC);
		}

	}



/* &global/from-evaluable?-set! */
	obj_t BGl_z62globalzf2fromzd2evaluablezf3zd2setz12z71zzeffect_cgraphz00(obj_t
		BgL_envz00_2773, obj_t BgL_oz00_2774, obj_t BgL_vz00_2775)
	{
		{	/* Effect/cgraph.sch 140 */
			return
				BGl_globalzf2fromzd2evaluablezf3zd2setz12z13zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2774), CBOOL(BgL_vz00_2775));
		}

	}



/* global/from-import */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2importz20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_101)
	{
		{	/* Effect/cgraph.sch 141 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_101)))->BgL_importz00);
		}

	}



/* &global/from-import */
	obj_t BGl_z62globalzf2fromzd2importz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2776, obj_t BgL_oz00_2777)
	{
		{	/* Effect/cgraph.sch 141 */
			return
				BGl_globalzf2fromzd2importz20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2777));
		}

	}



/* global/from-import-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2importzd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_102, obj_t BgL_vz00_103)
	{
		{	/* Effect/cgraph.sch 142 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_102)))->BgL_importz00) =
				((obj_t) BgL_vz00_103), BUNSPEC);
		}

	}



/* &global/from-import-set! */
	obj_t BGl_z62globalzf2fromzd2importzd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2778, obj_t BgL_oz00_2779, obj_t BgL_vz00_2780)
	{
		{	/* Effect/cgraph.sch 142 */
			return
				BGl_globalzf2fromzd2importzd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2779), BgL_vz00_2780);
		}

	}



/* global/from-module */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2modulez20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_104)
	{
		{	/* Effect/cgraph.sch 143 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_104)))->BgL_modulez00);
		}

	}



/* &global/from-module */
	obj_t BGl_z62globalzf2fromzd2modulez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2781, obj_t BgL_oz00_2782)
	{
		{	/* Effect/cgraph.sch 143 */
			return
				BGl_globalzf2fromzd2modulez20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2782));
		}

	}



/* global/from-module-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2modulezd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_105, obj_t BgL_vz00_106)
	{
		{	/* Effect/cgraph.sch 144 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_105)))->BgL_modulez00) =
				((obj_t) BgL_vz00_106), BUNSPEC);
		}

	}



/* &global/from-module-set! */
	obj_t BGl_z62globalzf2fromzd2modulezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2783, obj_t BgL_oz00_2784, obj_t BgL_vz00_2785)
	{
		{	/* Effect/cgraph.sch 144 */
			return
				BGl_globalzf2fromzd2modulezd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2784), BgL_vz00_2785);
		}

	}



/* global/from-user? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzf2fromzd2userzf3zd3zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_107)
	{
		{	/* Effect/cgraph.sch 145 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_107)))->BgL_userzf3zf3);
		}

	}



/* &global/from-user? */
	obj_t BGl_z62globalzf2fromzd2userzf3zb1zzeffect_cgraphz00(obj_t
		BgL_envz00_2786, obj_t BgL_oz00_2787)
	{
		{	/* Effect/cgraph.sch 145 */
			return
				BBOOL(BGl_globalzf2fromzd2userzf3zd3zzeffect_cgraphz00(
					((BgL_globalz00_bglt) BgL_oz00_2787)));
		}

	}



/* global/from-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2userzf3zd2setz12z13zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_108, bool_t BgL_vz00_109)
	{
		{	/* Effect/cgraph.sch 146 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_108)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_109), BUNSPEC);
		}

	}



/* &global/from-user?-set! */
	obj_t BGl_z62globalzf2fromzd2userzf3zd2setz12z71zzeffect_cgraphz00(obj_t
		BgL_envz00_2788, obj_t BgL_oz00_2789, obj_t BgL_vz00_2790)
	{
		{	/* Effect/cgraph.sch 146 */
			return
				BGl_globalzf2fromzd2userzf3zd2setz12z13zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2789), CBOOL(BgL_vz00_2790));
		}

	}



/* global/from-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_globalzf2fromzd2occurrencewz20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_110)
	{
		{	/* Effect/cgraph.sch 147 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_110)))->BgL_occurrencewz00);
		}

	}



/* &global/from-occurrencew */
	obj_t BGl_z62globalzf2fromzd2occurrencewz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2791, obj_t BgL_oz00_2792)
	{
		{	/* Effect/cgraph.sch 147 */
			return
				BINT(BGl_globalzf2fromzd2occurrencewz20zzeffect_cgraphz00(
					((BgL_globalz00_bglt) BgL_oz00_2792)));
		}

	}



/* global/from-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2occurrencewzd2setz12ze0zzeffect_cgraphz00
		(BgL_globalz00_bglt BgL_oz00_111, long BgL_vz00_112)
	{
		{	/* Effect/cgraph.sch 148 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_111)))->BgL_occurrencewz00) =
				((long) BgL_vz00_112), BUNSPEC);
		}

	}



/* &global/from-occurrencew-set! */
	obj_t BGl_z62globalzf2fromzd2occurrencewzd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2793, obj_t BgL_oz00_2794, obj_t BgL_vz00_2795)
	{
		{	/* Effect/cgraph.sch 148 */
			return
				BGl_globalzf2fromzd2occurrencewzd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2794), (long) CINT(BgL_vz00_2795));
		}

	}



/* global/from-occurrence */
	BGL_EXPORTED_DEF long
		BGl_globalzf2fromzd2occurrencez20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_113)
	{
		{	/* Effect/cgraph.sch 149 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_113)))->BgL_occurrencez00);
		}

	}



/* &global/from-occurrence */
	obj_t BGl_z62globalzf2fromzd2occurrencez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2796, obj_t BgL_oz00_2797)
	{
		{	/* Effect/cgraph.sch 149 */
			return
				BINT(BGl_globalzf2fromzd2occurrencez20zzeffect_cgraphz00(
					((BgL_globalz00_bglt) BgL_oz00_2797)));
		}

	}



/* global/from-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2occurrencezd2setz12ze0zzeffect_cgraphz00
		(BgL_globalz00_bglt BgL_oz00_114, long BgL_vz00_115)
	{
		{	/* Effect/cgraph.sch 150 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_114)))->BgL_occurrencez00) =
				((long) BgL_vz00_115), BUNSPEC);
		}

	}



/* &global/from-occurrence-set! */
	obj_t BGl_z62globalzf2fromzd2occurrencezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2798, obj_t BgL_oz00_2799, obj_t BgL_vz00_2800)
	{
		{	/* Effect/cgraph.sch 150 */
			return
				BGl_globalzf2fromzd2occurrencezd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2799), (long) CINT(BgL_vz00_2800));
		}

	}



/* global/from-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2removablez20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_116)
	{
		{	/* Effect/cgraph.sch 151 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_116)))->BgL_removablez00);
		}

	}



/* &global/from-removable */
	obj_t BGl_z62globalzf2fromzd2removablez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2801, obj_t BgL_oz00_2802)
	{
		{	/* Effect/cgraph.sch 151 */
			return
				BGl_globalzf2fromzd2removablez20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2802));
		}

	}



/* global/from-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2removablezd2setz12ze0zzeffect_cgraphz00
		(BgL_globalz00_bglt BgL_oz00_117, obj_t BgL_vz00_118)
	{
		{	/* Effect/cgraph.sch 152 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_117)))->BgL_removablez00) =
				((obj_t) BgL_vz00_118), BUNSPEC);
		}

	}



/* &global/from-removable-set! */
	obj_t BGl_z62globalzf2fromzd2removablezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2803, obj_t BgL_oz00_2804, obj_t BgL_vz00_2805)
	{
		{	/* Effect/cgraph.sch 152 */
			return
				BGl_globalzf2fromzd2removablezd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2804), BgL_vz00_2805);
		}

	}



/* global/from-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2fastzd2alphazf2zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_119)
	{
		{	/* Effect/cgraph.sch 153 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_119)))->BgL_fastzd2alphazd2);
		}

	}



/* &global/from-fast-alpha */
	obj_t BGl_z62globalzf2fromzd2fastzd2alphaz90zzeffect_cgraphz00(obj_t
		BgL_envz00_2806, obj_t BgL_oz00_2807)
	{
		{	/* Effect/cgraph.sch 153 */
			return
				BGl_globalzf2fromzd2fastzd2alphazf2zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2807));
		}

	}



/* global/from-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2fastzd2alphazd2setz12z32zzeffect_cgraphz00
		(BgL_globalz00_bglt BgL_oz00_120, obj_t BgL_vz00_121)
	{
		{	/* Effect/cgraph.sch 154 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_120)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_121), BUNSPEC);
		}

	}



/* &global/from-fast-alpha-set! */
	obj_t BGl_z62globalzf2fromzd2fastzd2alphazd2setz12z50zzeffect_cgraphz00(obj_t
		BgL_envz00_2808, obj_t BgL_oz00_2809, obj_t BgL_vz00_2810)
	{
		{	/* Effect/cgraph.sch 154 */
			return
				BGl_globalzf2fromzd2fastzd2alphazd2setz12z32zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2809), BgL_vz00_2810);
		}

	}



/* global/from-access */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2accessz20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_122)
	{
		{	/* Effect/cgraph.sch 155 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_122)))->BgL_accessz00);
		}

	}



/* &global/from-access */
	obj_t BGl_z62globalzf2fromzd2accessz42zzeffect_cgraphz00(obj_t
		BgL_envz00_2811, obj_t BgL_oz00_2812)
	{
		{	/* Effect/cgraph.sch 155 */
			return
				BGl_globalzf2fromzd2accessz20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2812));
		}

	}



/* global/from-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2accesszd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_123, obj_t BgL_vz00_124)
	{
		{	/* Effect/cgraph.sch 156 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_123)))->BgL_accessz00) =
				((obj_t) BgL_vz00_124), BUNSPEC);
		}

	}



/* &global/from-access-set! */
	obj_t BGl_z62globalzf2fromzd2accesszd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2813, obj_t BgL_oz00_2814, obj_t BgL_vz00_2815)
	{
		{	/* Effect/cgraph.sch 156 */
			return
				BGl_globalzf2fromzd2accesszd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2814), BgL_vz00_2815);
		}

	}



/* global/from-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_globalzf2fromzd2valuez20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_125)
	{
		{	/* Effect/cgraph.sch 157 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_125)))->BgL_valuez00);
		}

	}



/* &global/from-value */
	BgL_valuez00_bglt BGl_z62globalzf2fromzd2valuez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2816, obj_t BgL_oz00_2817)
	{
		{	/* Effect/cgraph.sch 157 */
			return
				BGl_globalzf2fromzd2valuez20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2817));
		}

	}



/* global/from-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2valuezd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_126, BgL_valuez00_bglt BgL_vz00_127)
	{
		{	/* Effect/cgraph.sch 158 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_126)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_127), BUNSPEC);
		}

	}



/* &global/from-value-set! */
	obj_t BGl_z62globalzf2fromzd2valuezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2818, obj_t BgL_oz00_2819, obj_t BgL_vz00_2820)
	{
		{	/* Effect/cgraph.sch 158 */
			return
				BGl_globalzf2fromzd2valuezd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2819),
				((BgL_valuez00_bglt) BgL_vz00_2820));
		}

	}



/* global/from-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_globalzf2fromzd2typez20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_128)
	{
		{	/* Effect/cgraph.sch 159 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_128)))->BgL_typez00);
		}

	}



/* &global/from-type */
	BgL_typez00_bglt BGl_z62globalzf2fromzd2typez42zzeffect_cgraphz00(obj_t
		BgL_envz00_2821, obj_t BgL_oz00_2822)
	{
		{	/* Effect/cgraph.sch 159 */
			return
				BGl_globalzf2fromzd2typez20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2822));
		}

	}



/* global/from-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2typezd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_129, BgL_typez00_bglt BgL_vz00_130)
	{
		{	/* Effect/cgraph.sch 160 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_129)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_130), BUNSPEC);
		}

	}



/* &global/from-type-set! */
	obj_t BGl_z62globalzf2fromzd2typezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2823, obj_t BgL_oz00_2824, obj_t BgL_vz00_2825)
	{
		{	/* Effect/cgraph.sch 160 */
			return
				BGl_globalzf2fromzd2typezd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2824),
				((BgL_typez00_bglt) BgL_vz00_2825));
		}

	}



/* global/from-name */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2namez20zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_131)
	{
		{	/* Effect/cgraph.sch 161 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_131)))->BgL_namez00);
		}

	}



/* &global/from-name */
	obj_t BGl_z62globalzf2fromzd2namez42zzeffect_cgraphz00(obj_t BgL_envz00_2826,
		obj_t BgL_oz00_2827)
	{
		{	/* Effect/cgraph.sch 161 */
			return
				BGl_globalzf2fromzd2namez20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2827));
		}

	}



/* global/from-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2namezd2setz12ze0zzeffect_cgraphz00(BgL_globalz00_bglt
		BgL_oz00_132, obj_t BgL_vz00_133)
	{
		{	/* Effect/cgraph.sch 162 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_132)))->BgL_namez00) =
				((obj_t) BgL_vz00_133), BUNSPEC);
		}

	}



/* &global/from-name-set! */
	obj_t BGl_z62globalzf2fromzd2namezd2setz12z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2828, obj_t BgL_oz00_2829, obj_t BgL_vz00_2830)
	{
		{	/* Effect/cgraph.sch 162 */
			return
				BGl_globalzf2fromzd2namezd2setz12ze0zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2829), BgL_vz00_2830);
		}

	}



/* global/from-id */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2fromzd2idz20zzeffect_cgraphz00(BgL_globalz00_bglt BgL_oz00_134)
	{
		{	/* Effect/cgraph.sch 163 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_134)))->BgL_idz00);
		}

	}



/* &global/from-id */
	obj_t BGl_z62globalzf2fromzd2idz42zzeffect_cgraphz00(obj_t BgL_envz00_2831,
		obj_t BgL_oz00_2832)
	{
		{	/* Effect/cgraph.sch 163 */
			return
				BGl_globalzf2fromzd2idz20zzeffect_cgraphz00(
				((BgL_globalz00_bglt) BgL_oz00_2832));
		}

	}



/* fun-call-graph! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2callzd2graphz12z12zzeffect_cgraphz00(BgL_variablez00_bglt
		BgL_variablez00_137)
	{
		{	/* Effect/cgraph.scm 31 */
			BGl_za2varzf2allza2zf2zzeffect_cgraphz00 =
				MAKE_YOUNG_PAIR(
				((obj_t) BgL_variablez00_137),
				BGl_za2varzf2allza2zf2zzeffect_cgraphz00);
			{	/* Effect/cgraph.scm 33 */
				obj_t BgL_arg1448z00_1550;

				BgL_arg1448z00_1550 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_137))->
									BgL_valuez00))))->BgL_bodyz00);
				return BGl_callzd2graphz12zc0zzeffect_cgraphz00(((BgL_nodez00_bglt)
						BgL_arg1448z00_1550), BgL_variablez00_137);
			}
		}

	}



/* &fun-call-graph! */
	obj_t BGl_z62funzd2callzd2graphz12z70zzeffect_cgraphz00(obj_t BgL_envz00_2833,
		obj_t BgL_variablez00_2834)
	{
		{	/* Effect/cgraph.scm 31 */
			return
				BGl_funzd2callzd2graphz12z12zzeffect_cgraphz00(
				((BgL_variablez00_bglt) BgL_variablez00_2834));
		}

	}



/* call-graph*! */
	bool_t BGl_callzd2graphza2z12z62zzeffect_cgraphz00(obj_t BgL_nodeza2za2_176,
		obj_t BgL_ownerz00_177)
	{
		{	/* Effect/cgraph.scm 206 */
			{
				obj_t BgL_l1321z00_1553;

				BgL_l1321z00_1553 = BgL_nodeza2za2_176;
			BgL_zc3z04anonymousza31454ze3z87_1554:
				if (PAIRP(BgL_l1321z00_1553))
					{	/* Effect/cgraph.scm 207 */
						{	/* Effect/cgraph.scm 207 */
							obj_t BgL_nodez00_1556;

							BgL_nodez00_1556 = CAR(BgL_l1321z00_1553);
							BGl_callzd2graphz12zc0zzeffect_cgraphz00(
								((BgL_nodez00_bglt) BgL_nodez00_1556),
								((BgL_variablez00_bglt) BgL_ownerz00_177));
						}
						{
							obj_t BgL_l1321z00_3814;

							BgL_l1321z00_3814 = CDR(BgL_l1321z00_1553);
							BgL_l1321z00_1553 = BgL_l1321z00_3814;
							goto BgL_zc3z04anonymousza31454ze3z87_1554;
						}
					}
				else
					{	/* Effect/cgraph.scm 207 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* get-var/side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2varzf2sidezd2effectzf2zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 251 */
			return BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00;
		}

	}



/* &get-var/side-effect */
	obj_t BGl_z62getzd2varzf2sidezd2effectz90zzeffect_cgraphz00(obj_t
		BgL_envz00_2835)
	{
		{	/* Effect/cgraph.scm 251 */
			return BGl_getzd2varzf2sidezd2effectzf2zzeffect_cgraphz00();
		}

	}



/* get-var/all */
	BGL_EXPORTED_DEF obj_t BGl_getzd2varzf2allz20zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 262 */
			return BGl_za2varzf2allza2zf2zzeffect_cgraphz00;
		}

	}



/* &get-var/all */
	obj_t BGl_z62getzd2varzf2allz42zzeffect_cgraphz00(obj_t BgL_envz00_2836)
	{
		{	/* Effect/cgraph.scm 262 */
			return BGl_getzd2varzf2allz20zzeffect_cgraphz00();
		}

	}



/* reset-effect-tables! */
	BGL_EXPORTED_DEF obj_t
		BGl_resetzd2effectzd2tablesz12z12zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 273 */
			BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00 = BNIL;
			return (BGl_za2varzf2allza2zf2zzeffect_cgraphz00 = BNIL, BUNSPEC);
		}

	}



/* &reset-effect-tables! */
	obj_t BGl_z62resetzd2effectzd2tablesz12z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2837)
	{
		{	/* Effect/cgraph.scm 273 */
			return BGl_resetzd2effectzd2tablesz12z12zzeffect_cgraphz00();
		}

	}



/* mark-side-effect! */
	obj_t BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(BgL_variablez00_bglt
		BgL_vz00_188)
	{
		{	/* Effect/cgraph.scm 280 */
			{	/* Effect/cgraph.scm 283 */
				BgL_valuez00_bglt BgL_funz00_1559;

				BgL_funz00_1559 =
					(((BgL_variablez00_bglt) COBJECT(BgL_vz00_188))->BgL_valuez00);
				if (CBOOL(
						(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_funz00_1559)))->BgL_sidezd2effectzd2)))
					{	/* Effect/cgraph.scm 285 */
						if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
									((obj_t) BgL_vz00_188),
									BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00)))
							{	/* Effect/cgraph.scm 288 */
								return CNST_TABLE_REF(0);
							}
						else
							{	/* Effect/cgraph.scm 288 */
								return (BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00 =
									MAKE_YOUNG_PAIR(
										((obj_t) BgL_vz00_188),
										BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00),
									BUNSPEC);
							}
					}
				else
					{	/* Effect/cgraph.scm 285 */
						BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00 =
							MAKE_YOUNG_PAIR(
							((obj_t) BgL_vz00_188),
							BGl_za2varzf2sidezd2effectza2z20zzeffect_cgraphz00);
						return ((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
											BgL_funz00_1559)))->BgL_sidezd2effectzd2) =
							((obj_t) BTRUE), BUNSPEC);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 15 */
			{	/* Effect/cgraph.scm 21 */
				obj_t BgL_arg1513z00_1566;
				obj_t BgL_arg1514z00_1567;

				{	/* Effect/cgraph.scm 21 */
					obj_t BgL_v1323z00_1602;

					BgL_v1323z00_1602 = create_vector(1L);
					{	/* Effect/cgraph.scm 21 */
						obj_t BgL_arg1553z00_1603;

						BgL_arg1553z00_1603 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc1899z00zzeffect_cgraphz00,
							BGl_proc1898z00zzeffect_cgraphz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(2));
						VECTOR_SET(BgL_v1323z00_1602, 0L, BgL_arg1553z00_1603);
					}
					BgL_arg1513z00_1566 = BgL_v1323z00_1602;
				}
				{	/* Effect/cgraph.scm 21 */
					obj_t BgL_v1324z00_1613;

					BgL_v1324z00_1613 = create_vector(0L);
					BgL_arg1514z00_1567 = BgL_v1324z00_1613;
				}
				BGl_localzf2fromzf2zzeffect_cgraphz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(3),
					CNST_TABLE_REF(4), BGl_localz00zzast_varz00, 19676L,
					BGl_proc1903z00zzeffect_cgraphz00, BGl_proc1902z00zzeffect_cgraphz00,
					BFALSE, BGl_proc1901z00zzeffect_cgraphz00,
					BGl_proc1900z00zzeffect_cgraphz00, BgL_arg1513z00_1566,
					BgL_arg1514z00_1567);
			}
			{	/* Effect/cgraph.scm 22 */
				obj_t BgL_arg1575z00_1622;
				obj_t BgL_arg1576z00_1623;

				{	/* Effect/cgraph.scm 22 */
					obj_t BgL_v1325z00_1665;

					BgL_v1325z00_1665 = create_vector(1L);
					{	/* Effect/cgraph.scm 22 */
						obj_t BgL_arg1602z00_1666;

						BgL_arg1602z00_1666 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc1905z00zzeffect_cgraphz00,
							BGl_proc1904z00zzeffect_cgraphz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(2));
						VECTOR_SET(BgL_v1325z00_1665, 0L, BgL_arg1602z00_1666);
					}
					BgL_arg1575z00_1622 = BgL_v1325z00_1665;
				}
				{	/* Effect/cgraph.scm 22 */
					obj_t BgL_v1326z00_1676;

					BgL_v1326z00_1676 = create_vector(0L);
					BgL_arg1576z00_1623 = BgL_v1326z00_1676;
				}
				return (BGl_globalzf2fromzf2zzeffect_cgraphz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(5),
						CNST_TABLE_REF(4), BGl_globalz00zzast_varz00, 6288L,
						BGl_proc1909z00zzeffect_cgraphz00,
						BGl_proc1908z00zzeffect_cgraphz00, BFALSE,
						BGl_proc1907z00zzeffect_cgraphz00,
						BGl_proc1906z00zzeffect_cgraphz00, BgL_arg1575z00_1622,
						BgL_arg1576z00_1623), BUNSPEC);
			}
		}

	}



/* &lambda1591 */
	BgL_globalz00_bglt BGl_z62lambda1591z62zzeffect_cgraphz00(obj_t
		BgL_envz00_2850, obj_t BgL_o1148z00_2851)
	{
		{	/* Effect/cgraph.scm 22 */
			{	/* Effect/cgraph.scm 22 */
				long BgL_arg1593z00_3065;

				{	/* Effect/cgraph.scm 22 */
					obj_t BgL_arg1594z00_3066;

					{	/* Effect/cgraph.scm 22 */
						obj_t BgL_arg1595z00_3067;

						{	/* Effect/cgraph.scm 22 */
							obj_t BgL_arg1815z00_3068;
							long BgL_arg1816z00_3069;

							BgL_arg1815z00_3068 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Effect/cgraph.scm 22 */
								long BgL_arg1817z00_3070;

								BgL_arg1817z00_3070 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_globalz00_bglt) BgL_o1148z00_2851)));
								BgL_arg1816z00_3069 = (BgL_arg1817z00_3070 - OBJECT_TYPE);
							}
							BgL_arg1595z00_3067 =
								VECTOR_REF(BgL_arg1815z00_3068, BgL_arg1816z00_3069);
						}
						BgL_arg1594z00_3066 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1595z00_3067);
					}
					{	/* Effect/cgraph.scm 22 */
						obj_t BgL_tmpz00_3860;

						BgL_tmpz00_3860 = ((obj_t) BgL_arg1594z00_3066);
						BgL_arg1593z00_3065 = BGL_CLASS_NUM(BgL_tmpz00_3860);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_globalz00_bglt) BgL_o1148z00_2851)), BgL_arg1593z00_3065);
			}
			{	/* Effect/cgraph.scm 22 */
				BgL_objectz00_bglt BgL_tmpz00_3866;

				BgL_tmpz00_3866 =
					((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_o1148z00_2851));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3866, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_o1148z00_2851));
			return ((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1148z00_2851));
		}

	}



/* &<@anonymous:1590> */
	obj_t BGl_z62zc3z04anonymousza31590ze3ze5zzeffect_cgraphz00(obj_t
		BgL_envz00_2852, obj_t BgL_new1147z00_2853)
	{
		{	/* Effect/cgraph.scm 22 */
			{
				BgL_globalz00_bglt BgL_auxz00_3874;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1147z00_2853))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(6)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3882;

					{	/* Effect/cgraph.scm 22 */
						obj_t BgL_classz00_3072;

						BgL_classz00_3072 = BGl_typez00zztype_typez00;
						{	/* Effect/cgraph.scm 22 */
							obj_t BgL__ortest_1117z00_3073;

							BgL__ortest_1117z00_3073 = BGL_CLASS_NIL(BgL_classz00_3072);
							if (CBOOL(BgL__ortest_1117z00_3073))
								{	/* Effect/cgraph.scm 22 */
									BgL_auxz00_3882 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3073);
								}
							else
								{	/* Effect/cgraph.scm 22 */
									BgL_auxz00_3882 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3072));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1147z00_2853))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_3882), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_3892;

					{	/* Effect/cgraph.scm 22 */
						obj_t BgL_classz00_3074;

						BgL_classz00_3074 = BGl_valuez00zzast_varz00;
						{	/* Effect/cgraph.scm 22 */
							obj_t BgL__ortest_1117z00_3075;

							BgL__ortest_1117z00_3075 = BGL_CLASS_NIL(BgL_classz00_3074);
							if (CBOOL(BgL__ortest_1117z00_3075))
								{	/* Effect/cgraph.scm 22 */
									BgL_auxz00_3892 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_3075);
								}
							else
								{	/* Effect/cgraph.scm 22 */
									BgL_auxz00_3892 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3074));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1147z00_2853))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_3892), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1147z00_2853))))->
						BgL_accessz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_modulez00) =
					((obj_t) CNST_TABLE_REF(6)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_importz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_evaluablezf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_evalzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_libraryz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_pragmaz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_srcz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_jvmzd2typezd2namez00) =
					((obj_t) BGl_string1910z00zzeffect_cgraphz00), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_initz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1147z00_2853))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_globalzf2fromzf2_bglt BgL_auxz00_3951;

					{
						obj_t BgL_auxz00_3952;

						{	/* Effect/cgraph.scm 22 */
							BgL_objectz00_bglt BgL_tmpz00_3953;

							BgL_tmpz00_3953 =
								((BgL_objectz00_bglt)
								((BgL_globalz00_bglt) BgL_new1147z00_2853));
							BgL_auxz00_3952 = BGL_OBJECT_WIDENING(BgL_tmpz00_3953);
						}
						BgL_auxz00_3951 = ((BgL_globalzf2fromzf2_bglt) BgL_auxz00_3952);
					}
					((((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_3951))->
							BgL_fromz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_3874 = ((BgL_globalz00_bglt) BgL_new1147z00_2853);
				return ((obj_t) BgL_auxz00_3874);
			}
		}

	}



/* &lambda1586 */
	BgL_globalz00_bglt BGl_z62lambda1586z62zzeffect_cgraphz00(obj_t
		BgL_envz00_2854, obj_t BgL_o1144z00_2855)
	{
		{	/* Effect/cgraph.scm 22 */
			{	/* Effect/cgraph.scm 22 */
				BgL_globalzf2fromzf2_bglt BgL_wide1146z00_3077;

				BgL_wide1146z00_3077 =
					((BgL_globalzf2fromzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_globalzf2fromzf2_bgl))));
				{	/* Effect/cgraph.scm 22 */
					obj_t BgL_auxz00_3966;
					BgL_objectz00_bglt BgL_tmpz00_3962;

					BgL_auxz00_3966 = ((obj_t) BgL_wide1146z00_3077);
					BgL_tmpz00_3962 =
						((BgL_objectz00_bglt)
						((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1144z00_2855)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3962, BgL_auxz00_3966);
				}
				((BgL_objectz00_bglt)
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1144z00_2855)));
				{	/* Effect/cgraph.scm 22 */
					long BgL_arg1589z00_3078;

					BgL_arg1589z00_3078 =
						BGL_CLASS_NUM(BGl_globalzf2fromzf2zzeffect_cgraphz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_globalz00_bglt)
								((BgL_globalz00_bglt) BgL_o1144z00_2855))),
						BgL_arg1589z00_3078);
				}
				return
					((BgL_globalz00_bglt)
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1144z00_2855)));
			}
		}

	}



/* &lambda1577 */
	BgL_globalz00_bglt BGl_z62lambda1577z62zzeffect_cgraphz00(obj_t
		BgL_envz00_2856, obj_t BgL_id1123z00_2857, obj_t BgL_name1124z00_2858,
		obj_t BgL_type1125z00_2859, obj_t BgL_value1126z00_2860,
		obj_t BgL_access1127z00_2861, obj_t BgL_fastzd2alpha1128zd2_2862,
		obj_t BgL_removable1129z00_2863, obj_t BgL_occurrence1130z00_2864,
		obj_t BgL_occurrencew1131z00_2865, obj_t BgL_userzf31132zf3_2866,
		obj_t BgL_module1133z00_2867, obj_t BgL_import1134z00_2868,
		obj_t BgL_evaluablezf31135zf3_2869, obj_t BgL_evalzf31136zf3_2870,
		obj_t BgL_library1137z00_2871, obj_t BgL_pragma1138z00_2872,
		obj_t BgL_src1139z00_2873, obj_t BgL_jvmzd2typezd2name1140z00_2874,
		obj_t BgL_init1141z00_2875, obj_t BgL_alias1142z00_2876,
		obj_t BgL_from1143z00_2877)
	{
		{	/* Effect/cgraph.scm 22 */
			{	/* Effect/cgraph.scm 22 */
				long BgL_occurrence1130z00_3082;
				long BgL_occurrencew1131z00_3083;
				bool_t BgL_userzf31132zf3_3084;
				bool_t BgL_evaluablezf31135zf3_3086;
				bool_t BgL_evalzf31136zf3_3087;

				BgL_occurrence1130z00_3082 = (long) CINT(BgL_occurrence1130z00_2864);
				BgL_occurrencew1131z00_3083 = (long) CINT(BgL_occurrencew1131z00_2865);
				BgL_userzf31132zf3_3084 = CBOOL(BgL_userzf31132zf3_2866);
				BgL_evaluablezf31135zf3_3086 = CBOOL(BgL_evaluablezf31135zf3_2869);
				BgL_evalzf31136zf3_3087 = CBOOL(BgL_evalzf31136zf3_2870);
				{	/* Effect/cgraph.scm 22 */
					BgL_globalz00_bglt BgL_new1195z00_3089;

					{	/* Effect/cgraph.scm 22 */
						BgL_globalz00_bglt BgL_tmp1193z00_3090;
						BgL_globalzf2fromzf2_bglt BgL_wide1194z00_3091;

						{
							BgL_globalz00_bglt BgL_auxz00_3985;

							{	/* Effect/cgraph.scm 22 */
								BgL_globalz00_bglt BgL_new1192z00_3092;

								BgL_new1192z00_3092 =
									((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_globalz00_bgl))));
								{	/* Effect/cgraph.scm 22 */
									long BgL_arg1585z00_3093;

									BgL_arg1585z00_3093 =
										BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1192z00_3092),
										BgL_arg1585z00_3093);
								}
								{	/* Effect/cgraph.scm 22 */
									BgL_objectz00_bglt BgL_tmpz00_3990;

									BgL_tmpz00_3990 = ((BgL_objectz00_bglt) BgL_new1192z00_3092);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3990, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1192z00_3092);
								BgL_auxz00_3985 = BgL_new1192z00_3092;
							}
							BgL_tmp1193z00_3090 = ((BgL_globalz00_bglt) BgL_auxz00_3985);
						}
						BgL_wide1194z00_3091 =
							((BgL_globalzf2fromzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_globalzf2fromzf2_bgl))));
						{	/* Effect/cgraph.scm 22 */
							obj_t BgL_auxz00_3998;
							BgL_objectz00_bglt BgL_tmpz00_3996;

							BgL_auxz00_3998 = ((obj_t) BgL_wide1194z00_3091);
							BgL_tmpz00_3996 = ((BgL_objectz00_bglt) BgL_tmp1193z00_3090);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3996, BgL_auxz00_3998);
						}
						((BgL_objectz00_bglt) BgL_tmp1193z00_3090);
						{	/* Effect/cgraph.scm 22 */
							long BgL_arg1584z00_3094;

							BgL_arg1584z00_3094 =
								BGL_CLASS_NUM(BGl_globalzf2fromzf2zzeffect_cgraphz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1193z00_3090),
								BgL_arg1584z00_3094);
						}
						BgL_new1195z00_3089 = ((BgL_globalz00_bglt) BgL_tmp1193z00_3090);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1195z00_3089)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1123z00_2857)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_3089)))->BgL_namez00) =
						((obj_t) BgL_name1124z00_2858), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_3089)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1125z00_2859)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_3089)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1126z00_2860)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_3089)))->BgL_accessz00) =
						((obj_t) BgL_access1127z00_2861), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_3089)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1128zd2_2862), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_3089)))->BgL_removablez00) =
						((obj_t) BgL_removable1129z00_2863), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_3089)))->BgL_occurrencez00) =
						((long) BgL_occurrence1130z00_3082), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_3089)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1131z00_3083), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_3089)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31132zf3_3084), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_modulez00) =
						((obj_t) ((obj_t) BgL_module1133z00_2867)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_importz00) =
						((obj_t) BgL_import1134z00_2868), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_evaluablezf3zf3) =
						((bool_t) BgL_evaluablezf31135zf3_3086), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_evalzf3zf3) =
						((bool_t) BgL_evalzf31136zf3_3087), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_libraryz00) =
						((obj_t) BgL_library1137z00_2871), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_pragmaz00) =
						((obj_t) BgL_pragma1138z00_2872), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_srcz00) =
						((obj_t) BgL_src1139z00_2873), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_jvmzd2typezd2namez00) =
						((obj_t) ((obj_t) BgL_jvmzd2typezd2name1140z00_2874)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_initz00) =
						((obj_t) BgL_init1141z00_2875), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1195z00_3089)))->BgL_aliasz00) =
						((obj_t) BgL_alias1142z00_2876), BUNSPEC);
					{
						BgL_globalzf2fromzf2_bglt BgL_auxz00_4051;

						{
							obj_t BgL_auxz00_4052;

							{	/* Effect/cgraph.scm 22 */
								BgL_objectz00_bglt BgL_tmpz00_4053;

								BgL_tmpz00_4053 = ((BgL_objectz00_bglt) BgL_new1195z00_3089);
								BgL_auxz00_4052 = BGL_OBJECT_WIDENING(BgL_tmpz00_4053);
							}
							BgL_auxz00_4051 = ((BgL_globalzf2fromzf2_bglt) BgL_auxz00_4052);
						}
						((((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_4051))->
								BgL_fromz00) = ((obj_t) BgL_from1143z00_2877), BUNSPEC);
					}
					return BgL_new1195z00_3089;
				}
			}
		}

	}



/* &lambda1608 */
	obj_t BGl_z62lambda1608z62zzeffect_cgraphz00(obj_t BgL_envz00_2878,
		obj_t BgL_oz00_2879, obj_t BgL_vz00_2880)
	{
		{	/* Effect/cgraph.scm 22 */
			{
				BgL_globalzf2fromzf2_bglt BgL_auxz00_4058;

				{
					obj_t BgL_auxz00_4059;

					{	/* Effect/cgraph.scm 22 */
						BgL_objectz00_bglt BgL_tmpz00_4060;

						BgL_tmpz00_4060 =
							((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_2879));
						BgL_auxz00_4059 = BGL_OBJECT_WIDENING(BgL_tmpz00_4060);
					}
					BgL_auxz00_4058 = ((BgL_globalzf2fromzf2_bglt) BgL_auxz00_4059);
				}
				return
					((((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_4058))->
						BgL_fromz00) = ((obj_t) BgL_vz00_2880), BUNSPEC);
			}
		}

	}



/* &lambda1607 */
	obj_t BGl_z62lambda1607z62zzeffect_cgraphz00(obj_t BgL_envz00_2881,
		obj_t BgL_oz00_2882)
	{
		{	/* Effect/cgraph.scm 22 */
			{
				BgL_globalzf2fromzf2_bglt BgL_auxz00_4066;

				{
					obj_t BgL_auxz00_4067;

					{	/* Effect/cgraph.scm 22 */
						BgL_objectz00_bglt BgL_tmpz00_4068;

						BgL_tmpz00_4068 =
							((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_2882));
						BgL_auxz00_4067 = BGL_OBJECT_WIDENING(BgL_tmpz00_4068);
					}
					BgL_auxz00_4066 = ((BgL_globalzf2fromzf2_bglt) BgL_auxz00_4067);
				}
				return
					(((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_4066))->BgL_fromz00);
			}
		}

	}



/* &lambda1542 */
	BgL_localz00_bglt BGl_z62lambda1542z62zzeffect_cgraphz00(obj_t
		BgL_envz00_2883, obj_t BgL_o1121z00_2884)
	{
		{	/* Effect/cgraph.scm 21 */
			{	/* Effect/cgraph.scm 21 */
				long BgL_arg1544z00_3098;

				{	/* Effect/cgraph.scm 21 */
					obj_t BgL_arg1546z00_3099;

					{	/* Effect/cgraph.scm 21 */
						obj_t BgL_arg1552z00_3100;

						{	/* Effect/cgraph.scm 21 */
							obj_t BgL_arg1815z00_3101;
							long BgL_arg1816z00_3102;

							BgL_arg1815z00_3101 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Effect/cgraph.scm 21 */
								long BgL_arg1817z00_3103;

								BgL_arg1817z00_3103 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1121z00_2884)));
								BgL_arg1816z00_3102 = (BgL_arg1817z00_3103 - OBJECT_TYPE);
							}
							BgL_arg1552z00_3100 =
								VECTOR_REF(BgL_arg1815z00_3101, BgL_arg1816z00_3102);
						}
						BgL_arg1546z00_3099 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1552z00_3100);
					}
					{	/* Effect/cgraph.scm 21 */
						obj_t BgL_tmpz00_4081;

						BgL_tmpz00_4081 = ((obj_t) BgL_arg1546z00_3099);
						BgL_arg1544z00_3098 = BGL_CLASS_NUM(BgL_tmpz00_4081);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1121z00_2884)), BgL_arg1544z00_3098);
			}
			{	/* Effect/cgraph.scm 21 */
				BgL_objectz00_bglt BgL_tmpz00_4087;

				BgL_tmpz00_4087 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1121z00_2884));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4087, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1121z00_2884));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1121z00_2884));
		}

	}



/* &<@anonymous:1541> */
	obj_t BGl_z62zc3z04anonymousza31541ze3ze5zzeffect_cgraphz00(obj_t
		BgL_envz00_2885, obj_t BgL_new1120z00_2886)
	{
		{	/* Effect/cgraph.scm 21 */
			{
				BgL_localz00_bglt BgL_auxz00_4095;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1120z00_2886))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(6)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2886))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_4103;

					{	/* Effect/cgraph.scm 21 */
						obj_t BgL_classz00_3105;

						BgL_classz00_3105 = BGl_typez00zztype_typez00;
						{	/* Effect/cgraph.scm 21 */
							obj_t BgL__ortest_1117z00_3106;

							BgL__ortest_1117z00_3106 = BGL_CLASS_NIL(BgL_classz00_3105);
							if (CBOOL(BgL__ortest_1117z00_3106))
								{	/* Effect/cgraph.scm 21 */
									BgL_auxz00_4103 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3106);
								}
							else
								{	/* Effect/cgraph.scm 21 */
									BgL_auxz00_4103 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3105));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1120z00_2886))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_4103), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_4113;

					{	/* Effect/cgraph.scm 21 */
						obj_t BgL_classz00_3107;

						BgL_classz00_3107 = BGl_valuez00zzast_varz00;
						{	/* Effect/cgraph.scm 21 */
							obj_t BgL__ortest_1117z00_3108;

							BgL__ortest_1117z00_3108 = BGL_CLASS_NIL(BgL_classz00_3107);
							if (CBOOL(BgL__ortest_1117z00_3108))
								{	/* Effect/cgraph.scm 21 */
									BgL_auxz00_4113 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_3108);
								}
							else
								{	/* Effect/cgraph.scm 21 */
									BgL_auxz00_4113 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3107));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1120z00_2886))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_4113), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1120z00_2886))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2886))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2886))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2886))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2886))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2886))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2886))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2886))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2886))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2fromzf2_bglt BgL_auxz00_4150;

					{
						obj_t BgL_auxz00_4151;

						{	/* Effect/cgraph.scm 21 */
							BgL_objectz00_bglt BgL_tmpz00_4152;

							BgL_tmpz00_4152 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1120z00_2886));
							BgL_auxz00_4151 = BGL_OBJECT_WIDENING(BgL_tmpz00_4152);
						}
						BgL_auxz00_4150 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_4151);
					}
					((((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_4150))->
							BgL_fromz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_4095 = ((BgL_localz00_bglt) BgL_new1120z00_2886);
				return ((obj_t) BgL_auxz00_4095);
			}
		}

	}



/* &lambda1536 */
	BgL_localz00_bglt BGl_z62lambda1536z62zzeffect_cgraphz00(obj_t
		BgL_envz00_2887, obj_t BgL_o1117z00_2888)
	{
		{	/* Effect/cgraph.scm 21 */
			{	/* Effect/cgraph.scm 21 */
				BgL_localzf2fromzf2_bglt BgL_wide1119z00_3110;

				BgL_wide1119z00_3110 =
					((BgL_localzf2fromzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2fromzf2_bgl))));
				{	/* Effect/cgraph.scm 21 */
					obj_t BgL_auxz00_4165;
					BgL_objectz00_bglt BgL_tmpz00_4161;

					BgL_auxz00_4165 = ((obj_t) BgL_wide1119z00_3110);
					BgL_tmpz00_4161 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1117z00_2888)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4161, BgL_auxz00_4165);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1117z00_2888)));
				{	/* Effect/cgraph.scm 21 */
					long BgL_arg1540z00_3111;

					BgL_arg1540z00_3111 =
						BGL_CLASS_NUM(BGl_localzf2fromzf2zzeffect_cgraphz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1117z00_2888))), BgL_arg1540z00_3111);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1117z00_2888)));
			}
		}

	}



/* &lambda1515 */
	BgL_localz00_bglt BGl_z62lambda1515z62zzeffect_cgraphz00(obj_t
		BgL_envz00_2889, obj_t BgL_id1103z00_2890, obj_t BgL_name1104z00_2891,
		obj_t BgL_type1105z00_2892, obj_t BgL_value1106z00_2893,
		obj_t BgL_access1107z00_2894, obj_t BgL_fastzd2alpha1108zd2_2895,
		obj_t BgL_removable1109z00_2896, obj_t BgL_occurrence1110z00_2897,
		obj_t BgL_occurrencew1111z00_2898, obj_t BgL_userzf31112zf3_2899,
		obj_t BgL_key1113z00_2900, obj_t BgL_valzd2noescape1114zd2_2901,
		obj_t BgL_volatile1115z00_2902, obj_t BgL_from1116z00_2903)
	{
		{	/* Effect/cgraph.scm 21 */
			{	/* Effect/cgraph.scm 21 */
				long BgL_occurrence1110z00_3115;
				long BgL_occurrencew1111z00_3116;
				bool_t BgL_userzf31112zf3_3117;
				long BgL_key1113z00_3118;
				bool_t BgL_volatile1115z00_3119;

				BgL_occurrence1110z00_3115 = (long) CINT(BgL_occurrence1110z00_2897);
				BgL_occurrencew1111z00_3116 = (long) CINT(BgL_occurrencew1111z00_2898);
				BgL_userzf31112zf3_3117 = CBOOL(BgL_userzf31112zf3_2899);
				BgL_key1113z00_3118 = (long) CINT(BgL_key1113z00_2900);
				BgL_volatile1115z00_3119 = CBOOL(BgL_volatile1115z00_2902);
				{	/* Effect/cgraph.scm 21 */
					BgL_localz00_bglt BgL_new1190z00_3120;

					{	/* Effect/cgraph.scm 21 */
						BgL_localz00_bglt BgL_tmp1188z00_3121;
						BgL_localzf2fromzf2_bglt BgL_wide1189z00_3122;

						{
							BgL_localz00_bglt BgL_auxz00_4184;

							{	/* Effect/cgraph.scm 21 */
								BgL_localz00_bglt BgL_new1187z00_3123;

								BgL_new1187z00_3123 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Effect/cgraph.scm 21 */
									long BgL_arg1535z00_3124;

									BgL_arg1535z00_3124 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1187z00_3123),
										BgL_arg1535z00_3124);
								}
								{	/* Effect/cgraph.scm 21 */
									BgL_objectz00_bglt BgL_tmpz00_4189;

									BgL_tmpz00_4189 = ((BgL_objectz00_bglt) BgL_new1187z00_3123);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4189, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1187z00_3123);
								BgL_auxz00_4184 = BgL_new1187z00_3123;
							}
							BgL_tmp1188z00_3121 = ((BgL_localz00_bglt) BgL_auxz00_4184);
						}
						BgL_wide1189z00_3122 =
							((BgL_localzf2fromzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2fromzf2_bgl))));
						{	/* Effect/cgraph.scm 21 */
							obj_t BgL_auxz00_4197;
							BgL_objectz00_bglt BgL_tmpz00_4195;

							BgL_auxz00_4197 = ((obj_t) BgL_wide1189z00_3122);
							BgL_tmpz00_4195 = ((BgL_objectz00_bglt) BgL_tmp1188z00_3121);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4195, BgL_auxz00_4197);
						}
						((BgL_objectz00_bglt) BgL_tmp1188z00_3121);
						{	/* Effect/cgraph.scm 21 */
							long BgL_arg1516z00_3125;

							BgL_arg1516z00_3125 =
								BGL_CLASS_NUM(BGl_localzf2fromzf2zzeffect_cgraphz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1188z00_3121),
								BgL_arg1516z00_3125);
						}
						BgL_new1190z00_3120 = ((BgL_localz00_bglt) BgL_tmp1188z00_3121);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1190z00_3120)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1103z00_2890)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1190z00_3120)))->BgL_namez00) =
						((obj_t) BgL_name1104z00_2891), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1190z00_3120)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1105z00_2892)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1190z00_3120)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1106z00_2893)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1190z00_3120)))->BgL_accessz00) =
						((obj_t) BgL_access1107z00_2894), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1190z00_3120)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1108zd2_2895), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1190z00_3120)))->BgL_removablez00) =
						((obj_t) BgL_removable1109z00_2896), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1190z00_3120)))->BgL_occurrencez00) =
						((long) BgL_occurrence1110z00_3115), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1190z00_3120)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1111z00_3116), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1190z00_3120)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31112zf3_3117), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1190z00_3120)))->BgL_keyz00) =
						((long) BgL_key1113z00_3118), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1190z00_3120)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1114zd2_2901), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1190z00_3120)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1115z00_3119), BUNSPEC);
					{
						BgL_localzf2fromzf2_bglt BgL_auxz00_4234;

						{
							obj_t BgL_auxz00_4235;

							{	/* Effect/cgraph.scm 21 */
								BgL_objectz00_bglt BgL_tmpz00_4236;

								BgL_tmpz00_4236 = ((BgL_objectz00_bglt) BgL_new1190z00_3120);
								BgL_auxz00_4235 = BGL_OBJECT_WIDENING(BgL_tmpz00_4236);
							}
							BgL_auxz00_4234 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_4235);
						}
						((((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_4234))->
								BgL_fromz00) = ((obj_t) BgL_from1116z00_2903), BUNSPEC);
					}
					return BgL_new1190z00_3120;
				}
			}
		}

	}



/* &lambda1563 */
	obj_t BGl_z62lambda1563z62zzeffect_cgraphz00(obj_t BgL_envz00_2904,
		obj_t BgL_oz00_2905, obj_t BgL_vz00_2906)
	{
		{	/* Effect/cgraph.scm 21 */
			{
				BgL_localzf2fromzf2_bglt BgL_auxz00_4241;

				{
					obj_t BgL_auxz00_4242;

					{	/* Effect/cgraph.scm 21 */
						BgL_objectz00_bglt BgL_tmpz00_4243;

						BgL_tmpz00_4243 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_2905));
						BgL_auxz00_4242 = BGL_OBJECT_WIDENING(BgL_tmpz00_4243);
					}
					BgL_auxz00_4241 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_4242);
				}
				return
					((((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_4241))->
						BgL_fromz00) = ((obj_t) BgL_vz00_2906), BUNSPEC);
			}
		}

	}



/* &lambda1562 */
	obj_t BGl_z62lambda1562z62zzeffect_cgraphz00(obj_t BgL_envz00_2907,
		obj_t BgL_oz00_2908)
	{
		{	/* Effect/cgraph.scm 21 */
			{
				BgL_localzf2fromzf2_bglt BgL_auxz00_4249;

				{
					obj_t BgL_auxz00_4250;

					{	/* Effect/cgraph.scm 21 */
						BgL_objectz00_bglt BgL_tmpz00_4251;

						BgL_tmpz00_4251 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_2908));
						BgL_auxz00_4250 = BGL_OBJECT_WIDENING(BgL_tmpz00_4251);
					}
					BgL_auxz00_4249 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_4250);
				}
				return
					(((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_4249))->BgL_fromz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_proc1911z00zzeffect_cgraphz00, BGl_nodez00zzast_nodez00,
				BGl_string1912z00zzeffect_cgraphz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_savezd2callz12zd2envz12zzeffect_cgraphz00,
				BGl_proc1913z00zzeffect_cgraphz00, BGl_variablez00zzast_varz00,
				BGl_string1914z00zzeffect_cgraphz00);
		}

	}



/* &save-call!1365 */
	obj_t BGl_z62savezd2callz121365za2zzeffect_cgraphz00(obj_t BgL_envz00_2911,
		obj_t BgL_calleez00_2912, obj_t BgL_ownerz00_2913)
	{
		{	/* Effect/cgraph.scm 212 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(7),
				BGl_string1915z00zzeffect_cgraphz00,
				((obj_t) ((BgL_variablez00_bglt) BgL_calleez00_2912)));
		}

	}



/* &call-graph!1327 */
	obj_t BGl_z62callzd2graphz121327za2zzeffect_cgraphz00(obj_t BgL_envz00_2914,
		obj_t BgL_nodez00_2915, obj_t BgL_ownerz00_2916)
	{
		{	/* Effect/cgraph.scm 38 */
			return CNST_TABLE_REF(8);
		}

	}



/* call-graph! */
	obj_t BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_nodez00_bglt
		BgL_nodez00_138, BgL_variablez00_bglt BgL_ownerz00_139)
	{
		{	/* Effect/cgraph.scm 38 */
			{	/* Effect/cgraph.scm 38 */
				obj_t BgL_method1328z00_1691;

				{	/* Effect/cgraph.scm 38 */
					obj_t BgL_res1884z00_2290;

					{	/* Effect/cgraph.scm 38 */
						long BgL_objzd2classzd2numz00_2261;

						BgL_objzd2classzd2numz00_2261 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_138));
						{	/* Effect/cgraph.scm 38 */
							obj_t BgL_arg1811z00_2262;

							BgL_arg1811z00_2262 =
								PROCEDURE_REF(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
								(int) (1L));
							{	/* Effect/cgraph.scm 38 */
								int BgL_offsetz00_2265;

								BgL_offsetz00_2265 = (int) (BgL_objzd2classzd2numz00_2261);
								{	/* Effect/cgraph.scm 38 */
									long BgL_offsetz00_2266;

									BgL_offsetz00_2266 =
										((long) (BgL_offsetz00_2265) - OBJECT_TYPE);
									{	/* Effect/cgraph.scm 38 */
										long BgL_modz00_2267;

										BgL_modz00_2267 =
											(BgL_offsetz00_2266 >> (int) ((long) ((int) (4L))));
										{	/* Effect/cgraph.scm 38 */
											long BgL_restz00_2269;

											BgL_restz00_2269 =
												(BgL_offsetz00_2266 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Effect/cgraph.scm 38 */

												{	/* Effect/cgraph.scm 38 */
													obj_t BgL_bucketz00_2271;

													BgL_bucketz00_2271 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2262), BgL_modz00_2267);
													BgL_res1884z00_2290 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2271), BgL_restz00_2269);
					}}}}}}}}
					BgL_method1328z00_1691 = BgL_res1884z00_2290;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1328z00_1691,
					((obj_t) BgL_nodez00_138), ((obj_t) BgL_ownerz00_139));
			}
		}

	}



/* &call-graph! */
	obj_t BGl_z62callzd2graphz12za2zzeffect_cgraphz00(obj_t BgL_envz00_2917,
		obj_t BgL_nodez00_2918, obj_t BgL_ownerz00_2919)
	{
		{	/* Effect/cgraph.scm 38 */
			return
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(
				((BgL_nodez00_bglt) BgL_nodez00_2918),
				((BgL_variablez00_bglt) BgL_ownerz00_2919));
		}

	}



/* save-call! */
	obj_t BGl_savezd2callz12zc0zzeffect_cgraphz00(BgL_variablez00_bglt
		BgL_calleez00_178, BgL_variablez00_bglt BgL_ownerz00_179)
	{
		{	/* Effect/cgraph.scm 212 */
			{	/* Effect/cgraph.scm 212 */
				obj_t BgL_method1367z00_1692;

				{	/* Effect/cgraph.scm 212 */
					obj_t BgL_res1889z00_2321;

					{	/* Effect/cgraph.scm 212 */
						long BgL_objzd2classzd2numz00_2292;

						BgL_objzd2classzd2numz00_2292 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_calleez00_178));
						{	/* Effect/cgraph.scm 212 */
							obj_t BgL_arg1811z00_2293;

							BgL_arg1811z00_2293 =
								PROCEDURE_REF(BGl_savezd2callz12zd2envz12zzeffect_cgraphz00,
								(int) (1L));
							{	/* Effect/cgraph.scm 212 */
								int BgL_offsetz00_2296;

								BgL_offsetz00_2296 = (int) (BgL_objzd2classzd2numz00_2292);
								{	/* Effect/cgraph.scm 212 */
									long BgL_offsetz00_2297;

									BgL_offsetz00_2297 =
										((long) (BgL_offsetz00_2296) - OBJECT_TYPE);
									{	/* Effect/cgraph.scm 212 */
										long BgL_modz00_2298;

										BgL_modz00_2298 =
											(BgL_offsetz00_2297 >> (int) ((long) ((int) (4L))));
										{	/* Effect/cgraph.scm 212 */
											long BgL_restz00_2300;

											BgL_restz00_2300 =
												(BgL_offsetz00_2297 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Effect/cgraph.scm 212 */

												{	/* Effect/cgraph.scm 212 */
													obj_t BgL_bucketz00_2302;

													BgL_bucketz00_2302 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2293), BgL_modz00_2298);
													BgL_res1889z00_2321 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2302), BgL_restz00_2300);
					}}}}}}}}
					BgL_method1367z00_1692 = BgL_res1889z00_2321;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1367z00_1692,
					((obj_t) BgL_calleez00_178), ((obj_t) BgL_ownerz00_179));
			}
		}

	}



/* &save-call! */
	obj_t BGl_z62savezd2callz12za2zzeffect_cgraphz00(obj_t BgL_envz00_2920,
		obj_t BgL_calleez00_2921, obj_t BgL_ownerz00_2922)
	{
		{	/* Effect/cgraph.scm 212 */
			return
				BGl_savezd2callz12zc0zzeffect_cgraphz00(
				((BgL_variablez00_bglt) BgL_calleez00_2921),
				((BgL_variablez00_bglt) BgL_ownerz00_2922));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1916z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_syncz00zzast_nodez00, BGl_proc1918z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_appz00zzast_nodez00, BGl_proc1919z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1920z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1921z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_externz00zzast_nodez00, BGl_proc1922z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_castz00zzast_nodez00, BGl_proc1923z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_setqz00zzast_nodez00, BGl_proc1924z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1925z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_failz00zzast_nodez00, BGl_proc1926z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_switchz00zzast_nodez00, BGl_proc1927z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1928z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1929z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1930z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1931z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1932z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1933z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callzd2graphz12zd2envz12zzeffect_cgraphz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1934z00zzeffect_cgraphz00,
				BGl_string1917z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_savezd2callz12zd2envz12zzeffect_cgraphz00,
				BGl_globalz00zzast_varz00, BGl_proc1935z00zzeffect_cgraphz00,
				BGl_string1936z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_savezd2callz12zd2envz12zzeffect_cgraphz00,
				BGl_globalzf2fromzf2zzeffect_cgraphz00,
				BGl_proc1937z00zzeffect_cgraphz00, BGl_string1936z00zzeffect_cgraphz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_savezd2callz12zd2envz12zzeffect_cgraphz00,
				BGl_localz00zzast_varz00, BGl_proc1938z00zzeffect_cgraphz00,
				BGl_string1936z00zzeffect_cgraphz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_savezd2callz12zd2envz12zzeffect_cgraphz00,
				BGl_localzf2fromzf2zzeffect_cgraphz00,
				BGl_proc1939z00zzeffect_cgraphz00, BGl_string1936z00zzeffect_cgraphz00);
		}

	}



/* &save-call!-local/fro1377 */
	obj_t BGl_z62savezd2callz12zd2localzf2fro1377z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2945, obj_t BgL_calleez00_2946, obj_t BgL_ownerz00_2947)
	{
		{	/* Effect/cgraph.scm 243 */
			{	/* Effect/cgraph.scm 245 */
				bool_t BgL_test2081z00_4356;

				{	/* Effect/cgraph.scm 245 */
					obj_t BgL_arg1770z00_3133;

					{
						BgL_localzf2fromzf2_bglt BgL_auxz00_4357;

						{
							obj_t BgL_auxz00_4358;

							{	/* Effect/cgraph.scm 245 */
								BgL_objectz00_bglt BgL_tmpz00_4359;

								BgL_tmpz00_4359 =
									((BgL_objectz00_bglt)
									((BgL_localz00_bglt) BgL_calleez00_2946));
								BgL_auxz00_4358 = BGL_OBJECT_WIDENING(BgL_tmpz00_4359);
							}
							BgL_auxz00_4357 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_4358);
						}
						BgL_arg1770z00_3133 =
							(((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_4357))->
							BgL_fromz00);
					}
					BgL_test2081z00_4356 =
						CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_ownerz00_2947,
							BgL_arg1770z00_3133));
				}
				if (BgL_test2081z00_4356)
					{	/* Effect/cgraph.scm 245 */
						return BFALSE;
					}
				else
					{
						obj_t BgL_auxz00_4374;
						BgL_localzf2fromzf2_bglt BgL_auxz00_4367;

						{	/* Effect/cgraph.scm 246 */
							obj_t BgL_arg1767z00_3134;

							{
								BgL_localzf2fromzf2_bglt BgL_auxz00_4375;

								{
									obj_t BgL_auxz00_4376;

									{	/* Effect/cgraph.scm 246 */
										BgL_objectz00_bglt BgL_tmpz00_4377;

										BgL_tmpz00_4377 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_calleez00_2946));
										BgL_auxz00_4376 = BGL_OBJECT_WIDENING(BgL_tmpz00_4377);
									}
									BgL_auxz00_4375 =
										((BgL_localzf2fromzf2_bglt) BgL_auxz00_4376);
								}
								BgL_arg1767z00_3134 =
									(((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_4375))->
									BgL_fromz00);
							}
							BgL_auxz00_4374 =
								MAKE_YOUNG_PAIR(BgL_ownerz00_2947, BgL_arg1767z00_3134);
						}
						{
							obj_t BgL_auxz00_4368;

							{	/* Effect/cgraph.scm 246 */
								BgL_objectz00_bglt BgL_tmpz00_4369;

								BgL_tmpz00_4369 =
									((BgL_objectz00_bglt)
									((BgL_localz00_bglt) BgL_calleez00_2946));
								BgL_auxz00_4368 = BGL_OBJECT_WIDENING(BgL_tmpz00_4369);
							}
							BgL_auxz00_4367 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_4368);
						}
						return
							((((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_4367))->
								BgL_fromz00) = ((obj_t) BgL_auxz00_4374), BUNSPEC);
					}
			}
		}

	}



/* &save-call!-local1374 */
	obj_t BGl_z62savezd2callz12zd2local1374z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2948, obj_t BgL_calleez00_2949, obj_t BgL_ownerz00_2950)
	{
		{	/* Effect/cgraph.scm 237 */
			{	/* Effect/cgraph.scm 238 */
				BgL_localz00_bglt BgL_arg1761z00_3136;

				{	/* Effect/cgraph.scm 238 */
					BgL_localzf2fromzf2_bglt BgL_wide1184z00_3137;

					BgL_wide1184z00_3137 =
						((BgL_localzf2fromzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_localzf2fromzf2_bgl))));
					{	/* Effect/cgraph.scm 238 */
						obj_t BgL_auxz00_4390;
						BgL_objectz00_bglt BgL_tmpz00_4386;

						BgL_auxz00_4390 = ((obj_t) BgL_wide1184z00_3137);
						BgL_tmpz00_4386 =
							((BgL_objectz00_bglt)
							((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_calleez00_2949)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4386, BgL_auxz00_4390);
					}
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_calleez00_2949)));
					{	/* Effect/cgraph.scm 238 */
						long BgL_arg1762z00_3138;

						BgL_arg1762z00_3138 =
							BGL_CLASS_NUM(BGl_localzf2fromzf2zzeffect_cgraphz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_localz00_bglt)
									((BgL_localz00_bglt) BgL_calleez00_2949))),
							BgL_arg1762z00_3138);
					}
					((BgL_localz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_calleez00_2949)));
				}
				{
					BgL_localzf2fromzf2_bglt BgL_auxz00_4404;

					{
						obj_t BgL_auxz00_4405;

						{	/* Effect/cgraph.scm 238 */
							BgL_objectz00_bglt BgL_tmpz00_4406;

							BgL_tmpz00_4406 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_calleez00_2949)));
							BgL_auxz00_4405 = BGL_OBJECT_WIDENING(BgL_tmpz00_4406);
						}
						BgL_auxz00_4404 = ((BgL_localzf2fromzf2_bglt) BgL_auxz00_4405);
					}
					((((BgL_localzf2fromzf2_bglt) COBJECT(BgL_auxz00_4404))->
							BgL_fromz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_arg1761z00_3136 =
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_calleez00_2949));
				return
					BGl_savezd2callz12zc0zzeffect_cgraphz00(
					((BgL_variablez00_bglt) BgL_arg1761z00_3136),
					((BgL_variablez00_bglt) BgL_ownerz00_2950));
			}
		}

	}



/* &save-call!-global/fr1371 */
	obj_t BGl_z62savezd2callz12zd2globalzf2fr1371z82zzeffect_cgraphz00(obj_t
		BgL_envz00_2951, obj_t BgL_calleez00_2952, obj_t BgL_ownerz00_2953)
	{
		{	/* Effect/cgraph.scm 229 */
			{	/* Effect/cgraph.scm 231 */
				bool_t BgL_test2082z00_4418;

				{	/* Effect/cgraph.scm 231 */
					obj_t BgL_arg1755z00_3140;

					{
						BgL_globalzf2fromzf2_bglt BgL_auxz00_4419;

						{
							obj_t BgL_auxz00_4420;

							{	/* Effect/cgraph.scm 231 */
								BgL_objectz00_bglt BgL_tmpz00_4421;

								BgL_tmpz00_4421 =
									((BgL_objectz00_bglt)
									((BgL_globalz00_bglt) BgL_calleez00_2952));
								BgL_auxz00_4420 = BGL_OBJECT_WIDENING(BgL_tmpz00_4421);
							}
							BgL_auxz00_4419 = ((BgL_globalzf2fromzf2_bglt) BgL_auxz00_4420);
						}
						BgL_arg1755z00_3140 =
							(((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_4419))->
							BgL_fromz00);
					}
					BgL_test2082z00_4418 =
						CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_ownerz00_2953,
							BgL_arg1755z00_3140));
				}
				if (BgL_test2082z00_4418)
					{	/* Effect/cgraph.scm 231 */
						return BFALSE;
					}
				else
					{
						obj_t BgL_auxz00_4436;
						BgL_globalzf2fromzf2_bglt BgL_auxz00_4429;

						{	/* Effect/cgraph.scm 232 */
							obj_t BgL_arg1754z00_3141;

							{
								BgL_globalzf2fromzf2_bglt BgL_auxz00_4437;

								{
									obj_t BgL_auxz00_4438;

									{	/* Effect/cgraph.scm 232 */
										BgL_objectz00_bglt BgL_tmpz00_4439;

										BgL_tmpz00_4439 =
											((BgL_objectz00_bglt)
											((BgL_globalz00_bglt) BgL_calleez00_2952));
										BgL_auxz00_4438 = BGL_OBJECT_WIDENING(BgL_tmpz00_4439);
									}
									BgL_auxz00_4437 =
										((BgL_globalzf2fromzf2_bglt) BgL_auxz00_4438);
								}
								BgL_arg1754z00_3141 =
									(((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_4437))->
									BgL_fromz00);
							}
							BgL_auxz00_4436 =
								MAKE_YOUNG_PAIR(BgL_ownerz00_2953, BgL_arg1754z00_3141);
						}
						{
							obj_t BgL_auxz00_4430;

							{	/* Effect/cgraph.scm 232 */
								BgL_objectz00_bglt BgL_tmpz00_4431;

								BgL_tmpz00_4431 =
									((BgL_objectz00_bglt)
									((BgL_globalz00_bglt) BgL_calleez00_2952));
								BgL_auxz00_4430 = BGL_OBJECT_WIDENING(BgL_tmpz00_4431);
							}
							BgL_auxz00_4429 = ((BgL_globalzf2fromzf2_bglt) BgL_auxz00_4430);
						}
						return
							((((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_4429))->
								BgL_fromz00) = ((obj_t) BgL_auxz00_4436), BUNSPEC);
					}
			}
		}

	}



/* &save-call!-global1369 */
	obj_t BGl_z62savezd2callz12zd2global1369z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2954, obj_t BgL_calleez00_2955, obj_t BgL_ownerz00_2956)
	{
		{	/* Effect/cgraph.scm 217 */
			{	/* Effect/cgraph.scm 218 */
				BgL_valuez00_bglt BgL_funz00_3143;

				BgL_funz00_3143 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_calleez00_2955))))->BgL_valuez00);
				{	/* Effect/cgraph.scm 219 */
					bool_t BgL_test2083z00_4450;

					{	/* Effect/cgraph.scm 219 */
						bool_t BgL_test2084z00_4451;

						{	/* Effect/cgraph.scm 219 */
							obj_t BgL_classz00_3144;

							BgL_classz00_3144 = BGl_cfunz00zzast_varz00;
							{	/* Effect/cgraph.scm 219 */
								BgL_objectz00_bglt BgL_arg1807z00_3145;

								{	/* Effect/cgraph.scm 219 */
									obj_t BgL_tmpz00_4452;

									BgL_tmpz00_4452 =
										((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3143));
									BgL_arg1807z00_3145 = (BgL_objectz00_bglt) (BgL_tmpz00_4452);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Effect/cgraph.scm 219 */
										long BgL_idxz00_3146;

										BgL_idxz00_3146 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3145);
										BgL_test2084z00_4451 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3146 + 3L)) == BgL_classz00_3144);
									}
								else
									{	/* Effect/cgraph.scm 219 */
										bool_t BgL_res1892z00_3149;

										{	/* Effect/cgraph.scm 219 */
											obj_t BgL_oclassz00_3150;

											{	/* Effect/cgraph.scm 219 */
												obj_t BgL_arg1815z00_3151;
												long BgL_arg1816z00_3152;

												BgL_arg1815z00_3151 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Effect/cgraph.scm 219 */
													long BgL_arg1817z00_3153;

													BgL_arg1817z00_3153 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3145);
													BgL_arg1816z00_3152 =
														(BgL_arg1817z00_3153 - OBJECT_TYPE);
												}
												BgL_oclassz00_3150 =
													VECTOR_REF(BgL_arg1815z00_3151, BgL_arg1816z00_3152);
											}
											{	/* Effect/cgraph.scm 219 */
												bool_t BgL__ortest_1115z00_3154;

												BgL__ortest_1115z00_3154 =
													(BgL_classz00_3144 == BgL_oclassz00_3150);
												if (BgL__ortest_1115z00_3154)
													{	/* Effect/cgraph.scm 219 */
														BgL_res1892z00_3149 = BgL__ortest_1115z00_3154;
													}
												else
													{	/* Effect/cgraph.scm 219 */
														long BgL_odepthz00_3155;

														{	/* Effect/cgraph.scm 219 */
															obj_t BgL_arg1804z00_3156;

															BgL_arg1804z00_3156 = (BgL_oclassz00_3150);
															BgL_odepthz00_3155 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3156);
														}
														if ((3L < BgL_odepthz00_3155))
															{	/* Effect/cgraph.scm 219 */
																obj_t BgL_arg1802z00_3157;

																{	/* Effect/cgraph.scm 219 */
																	obj_t BgL_arg1803z00_3158;

																	BgL_arg1803z00_3158 = (BgL_oclassz00_3150);
																	BgL_arg1802z00_3157 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3158,
																		3L);
																}
																BgL_res1892z00_3149 =
																	(BgL_arg1802z00_3157 == BgL_classz00_3144);
															}
														else
															{	/* Effect/cgraph.scm 219 */
																BgL_res1892z00_3149 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2084z00_4451 = BgL_res1892z00_3149;
									}
							}
						}
						if (BgL_test2084z00_4451)
							{	/* Effect/cgraph.scm 219 */
								BgL_test2083z00_4450 = ((bool_t) 1);
							}
						else
							{	/* Effect/cgraph.scm 220 */
								bool_t BgL_test2088z00_4475;

								{	/* Effect/cgraph.scm 220 */
									obj_t BgL_classz00_3159;

									BgL_classz00_3159 = BGl_sfunz00zzast_varz00;
									{	/* Effect/cgraph.scm 220 */
										BgL_objectz00_bglt BgL_arg1807z00_3160;

										{	/* Effect/cgraph.scm 220 */
											obj_t BgL_tmpz00_4476;

											BgL_tmpz00_4476 =
												((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3143));
											BgL_arg1807z00_3160 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4476);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Effect/cgraph.scm 220 */
												long BgL_idxz00_3161;

												BgL_idxz00_3161 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3160);
												BgL_test2088z00_4475 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3161 + 3L)) == BgL_classz00_3159);
											}
										else
											{	/* Effect/cgraph.scm 220 */
												bool_t BgL_res1893z00_3164;

												{	/* Effect/cgraph.scm 220 */
													obj_t BgL_oclassz00_3165;

													{	/* Effect/cgraph.scm 220 */
														obj_t BgL_arg1815z00_3166;
														long BgL_arg1816z00_3167;

														BgL_arg1815z00_3166 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Effect/cgraph.scm 220 */
															long BgL_arg1817z00_3168;

															BgL_arg1817z00_3168 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3160);
															BgL_arg1816z00_3167 =
																(BgL_arg1817z00_3168 - OBJECT_TYPE);
														}
														BgL_oclassz00_3165 =
															VECTOR_REF(BgL_arg1815z00_3166,
															BgL_arg1816z00_3167);
													}
													{	/* Effect/cgraph.scm 220 */
														bool_t BgL__ortest_1115z00_3169;

														BgL__ortest_1115z00_3169 =
															(BgL_classz00_3159 == BgL_oclassz00_3165);
														if (BgL__ortest_1115z00_3169)
															{	/* Effect/cgraph.scm 220 */
																BgL_res1893z00_3164 = BgL__ortest_1115z00_3169;
															}
														else
															{	/* Effect/cgraph.scm 220 */
																long BgL_odepthz00_3170;

																{	/* Effect/cgraph.scm 220 */
																	obj_t BgL_arg1804z00_3171;

																	BgL_arg1804z00_3171 = (BgL_oclassz00_3165);
																	BgL_odepthz00_3170 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3171);
																}
																if ((3L < BgL_odepthz00_3170))
																	{	/* Effect/cgraph.scm 220 */
																		obj_t BgL_arg1802z00_3172;

																		{	/* Effect/cgraph.scm 220 */
																			obj_t BgL_arg1803z00_3173;

																			BgL_arg1803z00_3173 =
																				(BgL_oclassz00_3165);
																			BgL_arg1802z00_3172 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3173, 3L);
																		}
																		BgL_res1893z00_3164 =
																			(BgL_arg1802z00_3172 ==
																			BgL_classz00_3159);
																	}
																else
																	{	/* Effect/cgraph.scm 220 */
																		BgL_res1893z00_3164 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2088z00_4475 = BgL_res1893z00_3164;
											}
									}
								}
								if (BgL_test2088z00_4475)
									{	/* Effect/cgraph.scm 220 */
										BgL_test2083z00_4450 =
											(
											(((BgL_globalz00_bglt) COBJECT(
														((BgL_globalz00_bglt) BgL_calleez00_2955)))->
												BgL_importz00) == CNST_TABLE_REF(9));
									}
								else
									{	/* Effect/cgraph.scm 220 */
										BgL_test2083z00_4450 = ((bool_t) 0);
									}
							}
					}
					if (BgL_test2083z00_4450)
						{	/* Effect/cgraph.scm 219 */
							if (CBOOL(
									(((BgL_funz00_bglt) COBJECT(
												((BgL_funz00_bglt) BgL_funz00_3143)))->
										BgL_sidezd2effectzd2)))
								{	/* Effect/cgraph.scm 222 */
									return
										BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(
										((BgL_variablez00_bglt) BgL_ownerz00_2956));
								}
							else
								{	/* Effect/cgraph.scm 222 */
									return BFALSE;
								}
						}
					else
						{	/* Effect/cgraph.scm 224 */
							BgL_globalz00_bglt BgL_arg1749z00_3174;

							{	/* Effect/cgraph.scm 224 */
								BgL_globalzf2fromzf2_bglt BgL_wide1179z00_3175;

								BgL_wide1179z00_3175 =
									((BgL_globalzf2fromzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_globalzf2fromzf2_bgl))));
								{	/* Effect/cgraph.scm 224 */
									obj_t BgL_auxz00_4514;
									BgL_objectz00_bglt BgL_tmpz00_4510;

									BgL_auxz00_4514 = ((obj_t) BgL_wide1179z00_3175);
									BgL_tmpz00_4510 =
										((BgL_objectz00_bglt)
										((BgL_globalz00_bglt)
											((BgL_globalz00_bglt) BgL_calleez00_2955)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4510, BgL_auxz00_4514);
								}
								((BgL_objectz00_bglt)
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_calleez00_2955)));
								{	/* Effect/cgraph.scm 224 */
									long BgL_arg1750z00_3176;

									BgL_arg1750z00_3176 =
										BGL_CLASS_NUM(BGl_globalzf2fromzf2zzeffect_cgraphz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_calleez00_2955))),
										BgL_arg1750z00_3176);
								}
								((BgL_globalz00_bglt)
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_calleez00_2955)));
							}
							{
								BgL_globalzf2fromzf2_bglt BgL_auxz00_4528;

								{
									obj_t BgL_auxz00_4529;

									{	/* Effect/cgraph.scm 224 */
										BgL_objectz00_bglt BgL_tmpz00_4530;

										BgL_tmpz00_4530 =
											((BgL_objectz00_bglt)
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_calleez00_2955)));
										BgL_auxz00_4529 = BGL_OBJECT_WIDENING(BgL_tmpz00_4530);
									}
									BgL_auxz00_4528 =
										((BgL_globalzf2fromzf2_bglt) BgL_auxz00_4529);
								}
								((((BgL_globalzf2fromzf2_bglt) COBJECT(BgL_auxz00_4528))->
										BgL_fromz00) = ((obj_t) BNIL), BUNSPEC);
							}
							BgL_arg1749z00_3174 =
								((BgL_globalz00_bglt)
								((BgL_globalz00_bglt) BgL_calleez00_2955));
							return
								BGl_savezd2callz12zc0zzeffect_cgraphz00(
								((BgL_variablez00_bglt) BgL_arg1749z00_3174),
								((BgL_variablez00_bglt) BgL_ownerz00_2956));
						}
				}
			}
		}

	}



/* &call-graph!-box-set!1364 */
	obj_t BGl_z62callzd2graphz12zd2boxzd2setz121364zb0zzeffect_cgraphz00(obj_t
		BgL_envz00_2957, obj_t BgL_nodez00_2958, obj_t BgL_ownerz00_2959)
	{
		{	/* Effect/cgraph.scm 197 */
			BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(
				((BgL_variablez00_bglt) BgL_ownerz00_2959));
			{	/* Effect/cgraph.scm 200 */
				BgL_varz00_bglt BgL_arg1739z00_3178;

				BgL_arg1739z00_3178 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2958)))->BgL_varz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(
					((BgL_nodez00_bglt) BgL_arg1739z00_3178),
					((BgL_variablez00_bglt) BgL_ownerz00_2959));
			}
			{	/* Effect/cgraph.scm 201 */
				BgL_nodez00_bglt BgL_arg1740z00_3179;

				BgL_arg1740z00_3179 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2958)))->BgL_valuez00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1740z00_3179,
					((BgL_variablez00_bglt) BgL_ownerz00_2959));
			}
		}

	}



/* &call-graph!-box-ref1362 */
	obj_t BGl_z62callzd2graphz12zd2boxzd2ref1362za2zzeffect_cgraphz00(obj_t
		BgL_envz00_2960, obj_t BgL_nodez00_2961, obj_t BgL_ownerz00_2962)
	{
		{	/* Effect/cgraph.scm 189 */
			{	/* Effect/cgraph.scm 190 */
				BgL_varz00_bglt BgL_arg1738z00_3181;

				BgL_arg1738z00_3181 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2961)))->BgL_varz00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(
					((BgL_nodez00_bglt) BgL_arg1738z00_3181),
					((BgL_variablez00_bglt) BgL_ownerz00_2962));
			}
		}

	}



/* &call-graph!-make-box1360 */
	obj_t BGl_z62callzd2graphz12zd2makezd2box1360za2zzeffect_cgraphz00(obj_t
		BgL_envz00_2963, obj_t BgL_nodez00_2964, obj_t BgL_ownerz00_2965)
	{
		{	/* Effect/cgraph.scm 183 */
			{	/* Effect/cgraph.scm 184 */
				BgL_nodez00_bglt BgL_arg1737z00_3183;

				BgL_arg1737z00_3183 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2964)))->BgL_valuez00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1737z00_3183,
					((BgL_variablez00_bglt) BgL_ownerz00_2965));
			}
		}

	}



/* &call-graph!-jump-ex-1358 */
	obj_t BGl_z62callzd2graphz12zd2jumpzd2exzd21358z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2966, obj_t BgL_nodez00_2967, obj_t BgL_ownerz00_2968)
	{
		{	/* Effect/cgraph.scm 174 */
			BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(
				((BgL_variablez00_bglt) BgL_ownerz00_2968));
			{	/* Effect/cgraph.scm 177 */
				BgL_nodez00_bglt BgL_arg1735z00_3185;

				BgL_arg1735z00_3185 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2967)))->BgL_exitz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1735z00_3185,
					((BgL_variablez00_bglt) BgL_ownerz00_2968));
			}
			{	/* Effect/cgraph.scm 178 */
				BgL_nodez00_bglt BgL_arg1736z00_3186;

				BgL_arg1736z00_3186 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2967)))->BgL_valuez00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1736z00_3186,
					((BgL_variablez00_bglt) BgL_ownerz00_2968));
			}
		}

	}



/* &call-graph!-set-ex-i1356 */
	obj_t BGl_z62callzd2graphz12zd2setzd2exzd2i1356z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2969, obj_t BgL_nodez00_2970, obj_t BgL_ownerz00_2971)
	{
		{	/* Effect/cgraph.scm 165 */
			BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(
				((BgL_variablez00_bglt) BgL_ownerz00_2971));
			{	/* Effect/cgraph.scm 168 */
				BgL_nodez00_bglt BgL_arg1733z00_3188;

				BgL_arg1733z00_3188 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2970)))->BgL_onexitz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1733z00_3188,
					((BgL_variablez00_bglt) BgL_ownerz00_2971));
			}
			{	/* Effect/cgraph.scm 169 */
				BgL_nodez00_bglt BgL_arg1734z00_3189;

				BgL_arg1734z00_3189 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2970)))->BgL_bodyz00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1734z00_3189,
					((BgL_variablez00_bglt) BgL_ownerz00_2971));
			}
		}

	}



/* &call-graph!-let-var1354 */
	obj_t BGl_z62callzd2graphz12zd2letzd2var1354za2zzeffect_cgraphz00(obj_t
		BgL_envz00_2972, obj_t BgL_nodez00_2973, obj_t BgL_ownerz00_2974)
	{
		{	/* Effect/cgraph.scm 155 */
			{	/* Effect/cgraph.scm 156 */
				bool_t BgL_tmpz00_4582;

				{	/* Effect/cgraph.scm 157 */
					BgL_nodez00_bglt BgL_arg1718z00_3191;

					BgL_arg1718z00_3191 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2973)))->BgL_bodyz00);
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1718z00_3191,
						((BgL_variablez00_bglt) BgL_ownerz00_2974));
				}
				{	/* Effect/cgraph.scm 158 */
					obj_t BgL_g1320z00_3192;

					BgL_g1320z00_3192 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2973)))->BgL_bindingsz00);
					{
						obj_t BgL_l1318z00_3194;

						BgL_l1318z00_3194 = BgL_g1320z00_3192;
					BgL_zc3z04anonymousza31719ze3z87_3193:
						if (PAIRP(BgL_l1318z00_3194))
							{	/* Effect/cgraph.scm 158 */
								{	/* Effect/cgraph.scm 159 */
									obj_t BgL_bindingz00_3195;

									BgL_bindingz00_3195 = CAR(BgL_l1318z00_3194);
									{	/* Effect/cgraph.scm 159 */
										obj_t BgL_arg1722z00_3196;

										BgL_arg1722z00_3196 = CDR(((obj_t) BgL_bindingz00_3195));
										BGl_callzd2graphz12zc0zzeffect_cgraphz00(
											((BgL_nodez00_bglt) BgL_arg1722z00_3196),
											((BgL_variablez00_bglt) BgL_ownerz00_2974));
									}
								}
								{
									obj_t BgL_l1318z00_4597;

									BgL_l1318z00_4597 = CDR(BgL_l1318z00_3194);
									BgL_l1318z00_3194 = BgL_l1318z00_4597;
									goto BgL_zc3z04anonymousza31719ze3z87_3193;
								}
							}
						else
							{	/* Effect/cgraph.scm 158 */
								BgL_tmpz00_4582 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_4582);
			}
		}

	}



/* &call-graph!-let-fun1352 */
	obj_t BGl_z62callzd2graphz12zd2letzd2fun1352za2zzeffect_cgraphz00(obj_t
		BgL_envz00_2975, obj_t BgL_nodez00_2976, obj_t BgL_ownerz00_2977)
	{
		{	/* Effect/cgraph.scm 145 */
			{	/* Effect/cgraph.scm 147 */
				obj_t BgL_g1317z00_3198;

				BgL_g1317z00_3198 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2976)))->BgL_localsz00);
				{
					obj_t BgL_l1315z00_3200;

					BgL_l1315z00_3200 = BgL_g1317z00_3198;
				BgL_zc3z04anonymousza31710ze3z87_3199:
					if (PAIRP(BgL_l1315z00_3200))
						{	/* Effect/cgraph.scm 147 */
							{	/* Effect/cgraph.scm 148 */
								obj_t BgL_localz00_3201;

								BgL_localz00_3201 = CAR(BgL_l1315z00_3200);
								BGl_funzd2callzd2graphz12z12zzeffect_cgraphz00(
									((BgL_variablez00_bglt) BgL_localz00_3201));
							}
							{
								obj_t BgL_l1315z00_4607;

								BgL_l1315z00_4607 = CDR(BgL_l1315z00_3200);
								BgL_l1315z00_3200 = BgL_l1315z00_4607;
								goto BgL_zc3z04anonymousza31710ze3z87_3199;
							}
						}
					else
						{	/* Effect/cgraph.scm 147 */
							((bool_t) 1);
						}
				}
			}
			{	/* Effect/cgraph.scm 150 */
				BgL_nodez00_bglt BgL_arg1717z00_3202;

				BgL_arg1717z00_3202 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2976)))->BgL_bodyz00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1717z00_3202,
					((BgL_variablez00_bglt) BgL_ownerz00_2977));
			}
		}

	}



/* &call-graph!-switch1350 */
	obj_t BGl_z62callzd2graphz12zd2switch1350z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2978, obj_t BgL_nodez00_2979, obj_t BgL_ownerz00_2980)
	{
		{	/* Effect/cgraph.scm 135 */
			{	/* Effect/cgraph.scm 136 */
				bool_t BgL_tmpz00_4613;

				{	/* Effect/cgraph.scm 137 */
					BgL_nodez00_bglt BgL_arg1705z00_3204;

					BgL_arg1705z00_3204 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2979)))->BgL_testz00);
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1705z00_3204,
						((BgL_variablez00_bglt) BgL_ownerz00_2980));
				}
				{	/* Effect/cgraph.scm 138 */
					obj_t BgL_g1314z00_3205;

					BgL_g1314z00_3205 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2979)))->BgL_clausesz00);
					{
						obj_t BgL_l1312z00_3207;

						BgL_l1312z00_3207 = BgL_g1314z00_3205;
					BgL_zc3z04anonymousza31706ze3z87_3206:
						if (PAIRP(BgL_l1312z00_3207))
							{	/* Effect/cgraph.scm 138 */
								{	/* Effect/cgraph.scm 139 */
									obj_t BgL_clausez00_3208;

									BgL_clausez00_3208 = CAR(BgL_l1312z00_3207);
									{	/* Effect/cgraph.scm 139 */
										obj_t BgL_arg1708z00_3209;

										BgL_arg1708z00_3209 = CDR(((obj_t) BgL_clausez00_3208));
										BGl_callzd2graphz12zc0zzeffect_cgraphz00(
											((BgL_nodez00_bglt) BgL_arg1708z00_3209),
											((BgL_variablez00_bglt) BgL_ownerz00_2980));
									}
								}
								{
									obj_t BgL_l1312z00_4628;

									BgL_l1312z00_4628 = CDR(BgL_l1312z00_3207);
									BgL_l1312z00_3207 = BgL_l1312z00_4628;
									goto BgL_zc3z04anonymousza31706ze3z87_3206;
								}
							}
						else
							{	/* Effect/cgraph.scm 138 */
								BgL_tmpz00_4613 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_4613);
			}
		}

	}



/* &call-graph!-fail1348 */
	obj_t BGl_z62callzd2graphz12zd2fail1348z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2981, obj_t BgL_nodez00_2982, obj_t BgL_ownerz00_2983)
	{
		{	/* Effect/cgraph.scm 125 */
			BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(
				((BgL_variablez00_bglt) BgL_ownerz00_2983));
			{	/* Effect/cgraph.scm 128 */
				BgL_nodez00_bglt BgL_arg1701z00_3211;

				BgL_arg1701z00_3211 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2982)))->BgL_procz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1701z00_3211,
					((BgL_variablez00_bglt) BgL_ownerz00_2983));
			}
			{	/* Effect/cgraph.scm 129 */
				BgL_nodez00_bglt BgL_arg1702z00_3212;

				BgL_arg1702z00_3212 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2982)))->BgL_msgz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1702z00_3212,
					((BgL_variablez00_bglt) BgL_ownerz00_2983));
			}
			{	/* Effect/cgraph.scm 130 */
				BgL_nodez00_bglt BgL_arg1703z00_3213;

				BgL_arg1703z00_3213 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2982)))->BgL_objz00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1703z00_3213,
					((BgL_variablez00_bglt) BgL_ownerz00_2983));
			}
		}

	}



/* &call-graph!-conditio1346 */
	obj_t BGl_z62callzd2graphz12zd2conditio1346z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2984, obj_t BgL_nodez00_2985, obj_t BgL_ownerz00_2986)
	{
		{	/* Effect/cgraph.scm 116 */
			{	/* Effect/cgraph.scm 118 */
				BgL_nodez00_bglt BgL_arg1692z00_3215;

				BgL_arg1692z00_3215 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2985)))->BgL_testz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1692z00_3215,
					((BgL_variablez00_bglt) BgL_ownerz00_2986));
			}
			{	/* Effect/cgraph.scm 119 */
				BgL_nodez00_bglt BgL_arg1699z00_3216;

				BgL_arg1699z00_3216 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2985)))->BgL_truez00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1699z00_3216,
					((BgL_variablez00_bglt) BgL_ownerz00_2986));
			}
			{	/* Effect/cgraph.scm 120 */
				BgL_nodez00_bglt BgL_arg1700z00_3217;

				BgL_arg1700z00_3217 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2985)))->BgL_falsez00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1700z00_3217,
					((BgL_variablez00_bglt) BgL_ownerz00_2986));
			}
		}

	}



/* &call-graph!-setq1344 */
	obj_t BGl_z62callzd2graphz12zd2setq1344z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2987, obj_t BgL_nodez00_2988, obj_t BgL_ownerz00_2989)
	{
		{	/* Effect/cgraph.scm 106 */
			{	/* Effect/cgraph.scm 108 */
				bool_t BgL_test2096z00_4657;

				{	/* Effect/cgraph.scm 108 */
					bool_t BgL_test2097z00_4658;

					{	/* Effect/cgraph.scm 108 */
						obj_t BgL_classz00_3219;

						BgL_classz00_3219 = BGl_localz00zzast_varz00;
						if (BGL_OBJECTP(BgL_ownerz00_2989))
							{	/* Effect/cgraph.scm 108 */
								BgL_objectz00_bglt BgL_arg1807z00_3220;

								BgL_arg1807z00_3220 = (BgL_objectz00_bglt) (BgL_ownerz00_2989);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Effect/cgraph.scm 108 */
										long BgL_idxz00_3221;

										BgL_idxz00_3221 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3220);
										BgL_test2097z00_4658 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3221 + 2L)) == BgL_classz00_3219);
									}
								else
									{	/* Effect/cgraph.scm 108 */
										bool_t BgL_res1890z00_3224;

										{	/* Effect/cgraph.scm 108 */
											obj_t BgL_oclassz00_3225;

											{	/* Effect/cgraph.scm 108 */
												obj_t BgL_arg1815z00_3226;
												long BgL_arg1816z00_3227;

												BgL_arg1815z00_3226 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Effect/cgraph.scm 108 */
													long BgL_arg1817z00_3228;

													BgL_arg1817z00_3228 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3220);
													BgL_arg1816z00_3227 =
														(BgL_arg1817z00_3228 - OBJECT_TYPE);
												}
												BgL_oclassz00_3225 =
													VECTOR_REF(BgL_arg1815z00_3226, BgL_arg1816z00_3227);
											}
											{	/* Effect/cgraph.scm 108 */
												bool_t BgL__ortest_1115z00_3229;

												BgL__ortest_1115z00_3229 =
													(BgL_classz00_3219 == BgL_oclassz00_3225);
												if (BgL__ortest_1115z00_3229)
													{	/* Effect/cgraph.scm 108 */
														BgL_res1890z00_3224 = BgL__ortest_1115z00_3229;
													}
												else
													{	/* Effect/cgraph.scm 108 */
														long BgL_odepthz00_3230;

														{	/* Effect/cgraph.scm 108 */
															obj_t BgL_arg1804z00_3231;

															BgL_arg1804z00_3231 = (BgL_oclassz00_3225);
															BgL_odepthz00_3230 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3231);
														}
														if ((2L < BgL_odepthz00_3230))
															{	/* Effect/cgraph.scm 108 */
																obj_t BgL_arg1802z00_3232;

																{	/* Effect/cgraph.scm 108 */
																	obj_t BgL_arg1803z00_3233;

																	BgL_arg1803z00_3233 = (BgL_oclassz00_3225);
																	BgL_arg1802z00_3232 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3233,
																		2L);
																}
																BgL_res1890z00_3224 =
																	(BgL_arg1802z00_3232 == BgL_classz00_3219);
															}
														else
															{	/* Effect/cgraph.scm 108 */
																BgL_res1890z00_3224 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2097z00_4658 = BgL_res1890z00_3224;
									}
							}
						else
							{	/* Effect/cgraph.scm 108 */
								BgL_test2097z00_4658 = ((bool_t) 0);
							}
					}
					if (BgL_test2097z00_4658)
						{	/* Effect/cgraph.scm 108 */
							BgL_test2096z00_4657 = ((bool_t) 1);
						}
					else
						{	/* Effect/cgraph.scm 108 */
							BgL_variablez00_bglt BgL_arg1681z00_3234;

							BgL_arg1681z00_3234 =
								(((BgL_varz00_bglt) COBJECT(
										(((BgL_setqz00_bglt) COBJECT(
													((BgL_setqz00_bglt) BgL_nodez00_2988)))->
											BgL_varz00)))->BgL_variablez00);
							{	/* Effect/cgraph.scm 108 */
								obj_t BgL_classz00_3235;

								BgL_classz00_3235 = BGl_globalz00zzast_varz00;
								{	/* Effect/cgraph.scm 108 */
									BgL_objectz00_bglt BgL_arg1807z00_3236;

									{	/* Effect/cgraph.scm 108 */
										obj_t BgL_tmpz00_4684;

										BgL_tmpz00_4684 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1681z00_3234));
										BgL_arg1807z00_3236 =
											(BgL_objectz00_bglt) (BgL_tmpz00_4684);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Effect/cgraph.scm 108 */
											long BgL_idxz00_3237;

											BgL_idxz00_3237 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3236);
											BgL_test2096z00_4657 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3237 + 2L)) == BgL_classz00_3235);
										}
									else
										{	/* Effect/cgraph.scm 108 */
											bool_t BgL_res1891z00_3240;

											{	/* Effect/cgraph.scm 108 */
												obj_t BgL_oclassz00_3241;

												{	/* Effect/cgraph.scm 108 */
													obj_t BgL_arg1815z00_3242;
													long BgL_arg1816z00_3243;

													BgL_arg1815z00_3242 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Effect/cgraph.scm 108 */
														long BgL_arg1817z00_3244;

														BgL_arg1817z00_3244 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3236);
														BgL_arg1816z00_3243 =
															(BgL_arg1817z00_3244 - OBJECT_TYPE);
													}
													BgL_oclassz00_3241 =
														VECTOR_REF(BgL_arg1815z00_3242,
														BgL_arg1816z00_3243);
												}
												{	/* Effect/cgraph.scm 108 */
													bool_t BgL__ortest_1115z00_3245;

													BgL__ortest_1115z00_3245 =
														(BgL_classz00_3235 == BgL_oclassz00_3241);
													if (BgL__ortest_1115z00_3245)
														{	/* Effect/cgraph.scm 108 */
															BgL_res1891z00_3240 = BgL__ortest_1115z00_3245;
														}
													else
														{	/* Effect/cgraph.scm 108 */
															long BgL_odepthz00_3246;

															{	/* Effect/cgraph.scm 108 */
																obj_t BgL_arg1804z00_3247;

																BgL_arg1804z00_3247 = (BgL_oclassz00_3241);
																BgL_odepthz00_3246 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3247);
															}
															if ((2L < BgL_odepthz00_3246))
																{	/* Effect/cgraph.scm 108 */
																	obj_t BgL_arg1802z00_3248;

																	{	/* Effect/cgraph.scm 108 */
																		obj_t BgL_arg1803z00_3249;

																		BgL_arg1803z00_3249 = (BgL_oclassz00_3241);
																		BgL_arg1802z00_3248 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3249, 2L);
																	}
																	BgL_res1891z00_3240 =
																		(BgL_arg1802z00_3248 == BgL_classz00_3235);
																}
															else
																{	/* Effect/cgraph.scm 108 */
																	BgL_res1891z00_3240 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2096z00_4657 = BgL_res1891z00_3240;
										}
								}
							}
						}
				}
				if (BgL_test2096z00_4657)
					{	/* Effect/cgraph.scm 108 */
						BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(
							((BgL_variablez00_bglt) BgL_ownerz00_2989));
					}
				else
					{	/* Effect/cgraph.scm 108 */
						BFALSE;
					}
			}
			{	/* Effect/cgraph.scm 110 */
				BgL_varz00_bglt BgL_arg1689z00_3250;

				BgL_arg1689z00_3250 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2988)))->BgL_varz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(
					((BgL_nodez00_bglt) BgL_arg1689z00_3250),
					((BgL_variablez00_bglt) BgL_ownerz00_2989));
			}
			{	/* Effect/cgraph.scm 111 */
				BgL_nodez00_bglt BgL_arg1691z00_3251;

				BgL_arg1691z00_3251 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2988)))->BgL_valuez00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1691z00_3251,
					((BgL_variablez00_bglt) BgL_ownerz00_2989));
			}
		}

	}



/* &call-graph!-cast1342 */
	obj_t BGl_z62callzd2graphz12zd2cast1342z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2990, obj_t BgL_nodez00_2991, obj_t BgL_ownerz00_2992)
	{
		{	/* Effect/cgraph.scm 95 */
			{	/* Effect/cgraph.scm 97 */
				BgL_nodez00_bglt BgL_arg1661z00_3253;

				BgL_arg1661z00_3253 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2991)))->BgL_argz00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1661z00_3253,
					((BgL_variablez00_bglt) BgL_ownerz00_2992));
			}
		}

	}



/* &call-graph!-extern1340 */
	obj_t BGl_z62callzd2graphz12zd2extern1340z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2993, obj_t BgL_nodez00_2994, obj_t BgL_ownerz00_2995)
	{
		{	/* Effect/cgraph.scm 86 */
			{	/* Effect/cgraph.scm 87 */
				bool_t BgL_tmpz00_4722;

				if (CBOOL(
						(((BgL_nodezf2effectzf2_bglt) COBJECT(
									((BgL_nodezf2effectzf2_bglt)
										((BgL_externz00_bglt) BgL_nodez00_2994))))->
							BgL_sidezd2effectzd2)))
					{	/* Effect/cgraph.scm 88 */
						BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(
							((BgL_variablez00_bglt) BgL_ownerz00_2995));
					}
				else
					{	/* Effect/cgraph.scm 88 */
						BFALSE;
					}
				BgL_tmpz00_4722 =
					BGl_callzd2graphza2z12z62zzeffect_cgraphz00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2994)))->BgL_exprza2za2),
					BgL_ownerz00_2995);
				return BBOOL(BgL_tmpz00_4722);
			}
		}

	}



/* &call-graph!-funcall1338 */
	obj_t BGl_z62callzd2graphz12zd2funcall1338z70zzeffect_cgraphz00(obj_t
		BgL_envz00_2996, obj_t BgL_nodez00_2997, obj_t BgL_ownerz00_2998)
	{
		{	/* Effect/cgraph.scm 77 */
			{	/* Effect/cgraph.scm 78 */
				bool_t BgL_tmpz00_4734;

				BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(
					((BgL_variablez00_bglt) BgL_ownerz00_2998));
				{	/* Effect/cgraph.scm 80 */
					BgL_nodez00_bglt BgL_arg1646z00_3256;

					BgL_arg1646z00_3256 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2997)))->BgL_funz00);
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1646z00_3256,
						((BgL_variablez00_bglt) BgL_ownerz00_2998));
				}
				BgL_tmpz00_4734 =
					BGl_callzd2graphza2z12z62zzeffect_cgraphz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2997)))->BgL_argsz00),
					BgL_ownerz00_2998);
				return BBOOL(BgL_tmpz00_4734);
			}
		}

	}



/* &call-graph!-app-ly1336 */
	obj_t BGl_z62callzd2graphz12zd2appzd2ly1336za2zzeffect_cgraphz00(obj_t
		BgL_envz00_2999, obj_t BgL_nodez00_3000, obj_t BgL_ownerz00_3001)
	{
		{	/* Effect/cgraph.scm 68 */
			BGl_markzd2sidezd2effectz12z12zzeffect_cgraphz00(
				((BgL_variablez00_bglt) BgL_ownerz00_3001));
			{	/* Effect/cgraph.scm 71 */
				BgL_nodez00_bglt BgL_arg1630z00_3258;

				BgL_arg1630z00_3258 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_3000)))->BgL_funz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1630z00_3258,
					((BgL_variablez00_bglt) BgL_ownerz00_3001));
			}
			{	/* Effect/cgraph.scm 72 */
				BgL_nodez00_bglt BgL_arg1642z00_3259;

				BgL_arg1642z00_3259 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_3000)))->BgL_argz00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1642z00_3259,
					((BgL_variablez00_bglt) BgL_ownerz00_3001));
			}
		}

	}



/* &call-graph!-app1334 */
	obj_t BGl_z62callzd2graphz12zd2app1334z70zzeffect_cgraphz00(obj_t
		BgL_envz00_3002, obj_t BgL_nodez00_3003, obj_t BgL_ownerz00_3004)
	{
		{	/* Effect/cgraph.scm 58 */
			{	/* Effect/cgraph.scm 59 */
				bool_t BgL_tmpz00_4755;

				{	/* Effect/cgraph.scm 62 */
					BgL_variablez00_bglt BgL_arg1626z00_3261;

					BgL_arg1626z00_3261 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_3003)))->BgL_funz00)))->
						BgL_variablez00);
					BGl_savezd2callz12zc0zzeffect_cgraphz00(BgL_arg1626z00_3261,
						((BgL_variablez00_bglt) BgL_ownerz00_3004));
				}
				BgL_tmpz00_4755 =
					BGl_callzd2graphza2z12z62zzeffect_cgraphz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_3003)))->BgL_argsz00),
					BgL_ownerz00_3004);
				return BBOOL(BgL_tmpz00_4755);
			}
		}

	}



/* &call-graph!-sync1332 */
	obj_t BGl_z62callzd2graphz12zd2sync1332z70zzeffect_cgraphz00(obj_t
		BgL_envz00_3005, obj_t BgL_nodez00_3006, obj_t BgL_ownerz00_3007)
	{
		{	/* Effect/cgraph.scm 50 */
			{	/* Effect/cgraph.scm 51 */
				BgL_nodez00_bglt BgL_arg1615z00_3263;

				BgL_arg1615z00_3263 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_3006)))->BgL_mutexz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1615z00_3263,
					((BgL_variablez00_bglt) BgL_ownerz00_3007));
			}
			{	/* Effect/cgraph.scm 52 */
				BgL_nodez00_bglt BgL_arg1616z00_3264;

				BgL_arg1616z00_3264 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_3006)))->BgL_prelockz00);
				BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1616z00_3264,
					((BgL_variablez00_bglt) BgL_ownerz00_3007));
			}
			{	/* Effect/cgraph.scm 53 */
				BgL_nodez00_bglt BgL_arg1625z00_3265;

				BgL_arg1625z00_3265 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_3006)))->BgL_bodyz00);
				return
					BGl_callzd2graphz12zc0zzeffect_cgraphz00(BgL_arg1625z00_3265,
					((BgL_variablez00_bglt) BgL_ownerz00_3007));
			}
		}

	}



/* &call-graph!-sequence1330 */
	obj_t BGl_z62callzd2graphz12zd2sequence1330z70zzeffect_cgraphz00(obj_t
		BgL_envz00_3008, obj_t BgL_nodez00_3009, obj_t BgL_ownerz00_3010)
	{
		{	/* Effect/cgraph.scm 44 */
			return
				BBOOL(BGl_callzd2graphza2z12z62zzeffect_cgraphz00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_3009)))->BgL_nodesz00),
					BgL_ownerz00_3010));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzeffect_cgraphz00(void)
	{
		{	/* Effect/cgraph.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1940z00zzeffect_cgraphz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1940z00zzeffect_cgraphz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1940z00zzeffect_cgraphz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1940z00zzeffect_cgraphz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1940z00zzeffect_cgraphz00));
			return
				BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1940z00zzeffect_cgraphz00));
		}

	}

#ifdef __cplusplus
}
#endif
