/*===========================================================================*/
/*   (Effect/spread.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Effect/spread.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EFFECT_SPREAD_TYPE_DEFINITIONS
#define BGL_EFFECT_SPREAD_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_EFFECT_SPREAD_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzeffect_spreadz00 = BUNSPEC;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62spreadzd2sidezd2effectz121248z70zzeffect_spreadz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzeffect_spreadz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzeffect_spreadz00(void);
	static obj_t BGl_objectzd2initzd2zzeffect_spreadz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzeffect_spreadz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21251za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21253za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21255za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21257za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21259za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21261za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21263za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21265za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21267za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21269za2zzeffect_spreadz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21271za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21273za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21275za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21277za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21279za2zzeffect_spreadz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzeffect_spreadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21281za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21283za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21285za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21287za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21289za2zzeffect_spreadz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21291za2zzeffect_spreadz00(obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2sidezd2effectz12zd21293za2zzeffect_spreadz00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62spreadzd2sidezd2effectz12z70zzeffect_spreadz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzeffect_spreadz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzeffect_spreadz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzeffect_spreadz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzeffect_spreadz00(void);
	extern obj_t BGl_retblockz00zzast_nodez00;
	static bool_t BGl_spreadzd2sidezd2effectza2z12zb0zzeffect_spreadz00(obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_EXPORTED_DECL bool_t
		BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(BgL_nodez00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_patchz00zzast_nodez00;
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1706z00zzeffect_spreadz00,
		BgL_bgl_string1706za700za7za7e1734za7, "spread-side-effect!1248", 23);
	      DEFINE_STRING(BGl_string1708z00zzeffect_spreadz00,
		BgL_bgl_string1708za700za7za7e1735za7, "spread-side-effect!", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1705z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1736z00,
		BGl_z62spreadzd2sidezd2effectz121248z70zzeffect_spreadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1707z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1737z00,
		BGl_z62spreadzd2sidezd2effectz12zd21251za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1709z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1738z00,
		BGl_z62spreadzd2sidezd2effectz12zd21253za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1710z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1739z00,
		BGl_z62spreadzd2sidezd2effectz12zd21255za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1711z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1740z00,
		BGl_z62spreadzd2sidezd2effectz12zd21257za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1712z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1741z00,
		BGl_z62spreadzd2sidezd2effectz12zd21259za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1713z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1742z00,
		BGl_z62spreadzd2sidezd2effectz12zd21261za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1714z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1743z00,
		BGl_z62spreadzd2sidezd2effectz12zd21263za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1715z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1744z00,
		BGl_z62spreadzd2sidezd2effectz12zd21265za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1716z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1745z00,
		BGl_z62spreadzd2sidezd2effectz12zd21267za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1717z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1746z00,
		BGl_z62spreadzd2sidezd2effectz12zd21269za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1718z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1747z00,
		BGl_z62spreadzd2sidezd2effectz12zd21271za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1719z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1748z00,
		BGl_z62spreadzd2sidezd2effectz12zd21273za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1720z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1749z00,
		BGl_z62spreadzd2sidezd2effectz12zd21275za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1721z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1750z00,
		BGl_z62spreadzd2sidezd2effectz12zd21277za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1722z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1751z00,
		BGl_z62spreadzd2sidezd2effectz12zd21279za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1723z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1752z00,
		BGl_z62spreadzd2sidezd2effectz12zd21281za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1730z00zzeffect_spreadz00,
		BgL_bgl_string1730za700za7za7e1753za7, "effect_spread", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1724z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1754z00,
		BGl_z62spreadzd2sidezd2effectz12zd21283za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1731z00zzeffect_spreadz00,
		BgL_bgl_string1731za700za7za7e1755za7, "read ", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1725z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1756z00,
		BGl_z62spreadzd2sidezd2effectz12zd21285za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1726z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1757z00,
		BGl_z62spreadzd2sidezd2effectz12zd21287za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1727z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1758z00,
		BGl_z62spreadzd2sidezd2effectz12zd21289za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1728z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1759z00,
		BGl_z62spreadzd2sidezd2effectz12zd21291za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1729z00zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1760z00,
		BGl_z62spreadzd2sidezd2effectz12zd21293za2zzeffect_spreadz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
		BgL_bgl_za762spreadza7d2side1761z00,
		BGl_z62spreadzd2sidezd2effectz12z70zzeffect_spreadz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzeffect_spreadz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzeffect_spreadz00(long
		BgL_checksumz00_1958, char *BgL_fromz00_1959)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzeffect_spreadz00))
				{
					BGl_requirezd2initializa7ationz75zzeffect_spreadz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzeffect_spreadz00();
					BGl_libraryzd2moduleszd2initz00zzeffect_spreadz00();
					BGl_cnstzd2initzd2zzeffect_spreadz00();
					BGl_importedzd2moduleszd2initz00zzeffect_spreadz00();
					BGl_genericzd2initzd2zzeffect_spreadz00();
					BGl_methodzd2initzd2zzeffect_spreadz00();
					return BGl_toplevelzd2initzd2zzeffect_spreadz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzeffect_spreadz00(void)
	{
		{	/* Effect/spread.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "effect_spread");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "effect_spread");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"effect_spread");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"effect_spread");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"effect_spread");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "effect_spread");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"effect_spread");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"effect_spread");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzeffect_spreadz00(void)
	{
		{	/* Effect/spread.scm 15 */
			{	/* Effect/spread.scm 15 */
				obj_t BgL_cportz00_1885;

				{	/* Effect/spread.scm 15 */
					obj_t BgL_stringz00_1892;

					BgL_stringz00_1892 = BGl_string1731z00zzeffect_spreadz00;
					{	/* Effect/spread.scm 15 */
						obj_t BgL_startz00_1893;

						BgL_startz00_1893 = BINT(0L);
						{	/* Effect/spread.scm 15 */
							obj_t BgL_endz00_1894;

							BgL_endz00_1894 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1892)));
							{	/* Effect/spread.scm 15 */

								BgL_cportz00_1885 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1892, BgL_startz00_1893, BgL_endz00_1894);
				}}}}
				{
					long BgL_iz00_1886;

					BgL_iz00_1886 = 0L;
				BgL_loopz00_1887:
					if ((BgL_iz00_1886 == -1L))
						{	/* Effect/spread.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Effect/spread.scm 15 */
							{	/* Effect/spread.scm 15 */
								obj_t BgL_arg1733z00_1888;

								{	/* Effect/spread.scm 15 */

									{	/* Effect/spread.scm 15 */
										obj_t BgL_locationz00_1890;

										BgL_locationz00_1890 = BBOOL(((bool_t) 0));
										{	/* Effect/spread.scm 15 */

											BgL_arg1733z00_1888 =
												BGl_readz00zz__readerz00(BgL_cportz00_1885,
												BgL_locationz00_1890);
										}
									}
								}
								{	/* Effect/spread.scm 15 */
									int BgL_tmpz00_1987;

									BgL_tmpz00_1987 = (int) (BgL_iz00_1886);
									CNST_TABLE_SET(BgL_tmpz00_1987, BgL_arg1733z00_1888);
							}}
							{	/* Effect/spread.scm 15 */
								int BgL_auxz00_1891;

								BgL_auxz00_1891 = (int) ((BgL_iz00_1886 - 1L));
								{
									long BgL_iz00_1992;

									BgL_iz00_1992 = (long) (BgL_auxz00_1891);
									BgL_iz00_1886 = BgL_iz00_1992;
									goto BgL_loopz00_1887;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzeffect_spreadz00(void)
	{
		{	/* Effect/spread.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzeffect_spreadz00(void)
	{
		{	/* Effect/spread.scm 15 */
			return BUNSPEC;
		}

	}



/* spread-side-effect*! */
	bool_t BGl_spreadzd2sidezd2effectza2z12zb0zzeffect_spreadz00(obj_t
		BgL_nodeza2za2_26)
	{
		{	/* Effect/spread.scm 233 */
			{
				obj_t BgL_nodeza2za2_1377;
				bool_t BgL_resz00_1378;

				BgL_nodeza2za2_1377 = BgL_nodeza2za2_26;
				BgL_resz00_1378 = ((bool_t) 0);
			BgL_zc3z04anonymousza31306ze3z87_1379:
				if (NULLP(BgL_nodeza2za2_1377))
					{	/* Effect/spread.scm 236 */
						return BgL_resz00_1378;
					}
				else
					{	/* Effect/spread.scm 238 */
						obj_t BgL_arg1308z00_1381;
						bool_t BgL_arg1310z00_1382;

						BgL_arg1308z00_1381 = CDR(((obj_t) BgL_nodeza2za2_1377));
						{	/* Effect/spread.scm 238 */
							bool_t BgL__ortest_1130z00_1383;

							{	/* Effect/spread.scm 238 */
								obj_t BgL_arg1311z00_1384;

								BgL_arg1311z00_1384 = CAR(((obj_t) BgL_nodeza2za2_1377));
								BgL__ortest_1130z00_1383 =
									BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
									((BgL_nodez00_bglt) BgL_arg1311z00_1384));
							}
							if (BgL__ortest_1130z00_1383)
								{	/* Effect/spread.scm 238 */
									BgL_arg1310z00_1382 = BgL__ortest_1130z00_1383;
								}
							else
								{	/* Effect/spread.scm 238 */
									BgL_arg1310z00_1382 = BgL_resz00_1378;
								}
						}
						{
							bool_t BgL_resz00_2005;
							obj_t BgL_nodeza2za2_2004;

							BgL_nodeza2za2_2004 = BgL_arg1308z00_1381;
							BgL_resz00_2005 = BgL_arg1310z00_1382;
							BgL_resz00_1378 = BgL_resz00_2005;
							BgL_nodeza2za2_1377 = BgL_nodeza2za2_2004;
							goto BgL_zc3z04anonymousza31306ze3z87_1379;
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzeffect_spreadz00(void)
	{
		{	/* Effect/spread.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzeffect_spreadz00(void)
	{
		{	/* Effect/spread.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_proc1705z00zzeffect_spreadz00, BGl_nodez00zzast_nodez00,
				BGl_string1706z00zzeffect_spreadz00);
		}

	}



/* &spread-side-effect!1248 */
	obj_t BGl_z62spreadzd2sidezd2effectz121248z70zzeffect_spreadz00(obj_t
		BgL_envz00_1815, obj_t BgL_nodez00_1816)
	{
		{	/* Effect/spread.scm 23 */
			return BBOOL(((bool_t) 0));
		}

	}



/* spread-side-effect! */
	BGL_EXPORTED_DEF bool_t
		BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(BgL_nodez00_bglt
		BgL_nodez00_3)
	{
		{	/* Effect/spread.scm 23 */
			{	/* Effect/spread.scm 23 */
				obj_t BgL_method1249z00_1390;

				{	/* Effect/spread.scm 23 */
					obj_t BgL_res1704z00_1779;

					{	/* Effect/spread.scm 23 */
						long BgL_objzd2classzd2numz00_1750;

						BgL_objzd2classzd2numz00_1750 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_3));
						{	/* Effect/spread.scm 23 */
							obj_t BgL_arg1811z00_1751;

							BgL_arg1811z00_1751 =
								PROCEDURE_REF
								(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
								(int) (1L));
							{	/* Effect/spread.scm 23 */
								int BgL_offsetz00_1754;

								BgL_offsetz00_1754 = (int) (BgL_objzd2classzd2numz00_1750);
								{	/* Effect/spread.scm 23 */
									long BgL_offsetz00_1755;

									BgL_offsetz00_1755 =
										((long) (BgL_offsetz00_1754) - OBJECT_TYPE);
									{	/* Effect/spread.scm 23 */
										long BgL_modz00_1756;

										BgL_modz00_1756 =
											(BgL_offsetz00_1755 >> (int) ((long) ((int) (4L))));
										{	/* Effect/spread.scm 23 */
											long BgL_restz00_1758;

											BgL_restz00_1758 =
												(BgL_offsetz00_1755 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Effect/spread.scm 23 */

												{	/* Effect/spread.scm 23 */
													obj_t BgL_bucketz00_1760;

													BgL_bucketz00_1760 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1751), BgL_modz00_1756);
													BgL_res1704z00_1779 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1760), BgL_restz00_1758);
					}}}}}}}}
					BgL_method1249z00_1390 = BgL_res1704z00_1779;
				}
				return
					CBOOL(BGL_PROCEDURE_CALL1(BgL_method1249z00_1390,
						((obj_t) BgL_nodez00_3)));
			}
		}

	}



/* &spread-side-effect! */
	obj_t BGl_z62spreadzd2sidezd2effectz12z70zzeffect_spreadz00(obj_t
		BgL_envz00_1817, obj_t BgL_nodez00_1818)
	{
		{	/* Effect/spread.scm 23 */
			return
				BBOOL(BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					((BgL_nodez00_bglt) BgL_nodez00_1818)));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzeffect_spreadz00(void)
	{
		{	/* Effect/spread.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_varz00zzast_nodez00, BGl_proc1707z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_patchz00zzast_nodez00, BGl_proc1709z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1710z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_syncz00zzast_nodez00, BGl_proc1711z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_appz00zzast_nodez00, BGl_proc1712z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1713z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1714z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_externz00zzast_nodez00, BGl_proc1715z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_castz00zzast_nodez00, BGl_proc1716z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_setqz00zzast_nodez00, BGl_proc1717z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1718z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_failz00zzast_nodez00, BGl_proc1719z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_switchz00zzast_nodez00, BGl_proc1720z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1721z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1722z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1723z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1724z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1725z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1726z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_returnz00zzast_nodez00, BGl_proc1727z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_retblockz00zzast_nodez00, BGl_proc1728z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2sidezd2effectz12zd2envzc0zzeffect_spreadz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1729z00zzeffect_spreadz00,
				BGl_string1708z00zzeffect_spreadz00);
		}

	}



/* &spread-side-effect!-1293 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21293za2zzeffect_spreadz00(obj_t
		BgL_envz00_1841, obj_t BgL_nodez00_1842)
	{
		{	/* Effect/spread.scm 226 */
			{	/* Effect/spread.scm 227 */
				bool_t BgL_tmpz00_2064;

				{	/* Effect/spread.scm 228 */
					BgL_varz00_bglt BgL_arg1502z00_1898;

					BgL_arg1502z00_1898 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_1842)))->BgL_varz00);
					BgL_tmpz00_2064 =
						BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
						((BgL_nodez00_bglt) BgL_arg1502z00_1898));
				}
				return BBOOL(BgL_tmpz00_2064);
			}
		}

	}



/* &spread-side-effect!-1291 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21291za2zzeffect_spreadz00(obj_t
		BgL_envz00_1843, obj_t BgL_nodez00_1844)
	{
		{	/* Effect/spread.scm 215 */
			return
				BBOOL(BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_1844)))->BgL_bodyz00)));
		}

	}



/* &spread-side-effect!-1289 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21289za2zzeffect_spreadz00(obj_t
		BgL_envz00_1845, obj_t BgL_nodez00_1846)
	{
		{	/* Effect/spread.scm 207 */
			{	/* Effect/spread.scm 208 */
				bool_t BgL_tmpz00_2074;

				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_1846)))->BgL_valuez00));
				BgL_tmpz00_2074 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_2074);
			}
		}

	}



/* &spread-side-effect!-1287 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21287za2zzeffect_spreadz00(obj_t
		BgL_envz00_1847, obj_t BgL_nodez00_1848)
	{
		{	/* Effect/spread.scm 199 */
			{	/* Effect/spread.scm 200 */
				bool_t BgL_tmpz00_2079;

				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_1848)))->BgL_valuez00));
				BgL_tmpz00_2079 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_2079);
			}
		}

	}



/* &spread-side-effect!-1285 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21285za2zzeffect_spreadz00(obj_t
		BgL_envz00_1849, obj_t BgL_nodez00_1850)
	{
		{	/* Effect/spread.scm 190 */
			{	/* Effect/spread.scm 191 */
				bool_t BgL_tmpz00_2084;

				{	/* Effect/spread.scm 192 */
					bool_t BgL_resz00_1903;

					BgL_resz00_1903 =
						BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
						(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_1850)))->BgL_valuez00));
					((((BgL_nodezf2effectzf2_bglt) COBJECT(
									((BgL_nodezf2effectzf2_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_nodez00_1850))))->
							BgL_sidezd2effectzd2) =
						((obj_t) BBOOL(BgL_resz00_1903)), BUNSPEC);
					BgL_tmpz00_2084 = BgL_resz00_1903;
				}
				return BBOOL(BgL_tmpz00_2084);
			}
		}

	}



/* &spread-side-effect!-1283 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21283za2zzeffect_spreadz00(obj_t
		BgL_envz00_1851, obj_t BgL_nodez00_1852)
	{
		{	/* Effect/spread.scm 181 */
			{	/* Effect/spread.scm 182 */
				bool_t BgL_tmpz00_2093;

				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1852)))->
						BgL_exitz00));
				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(((
							(BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
									BgL_nodez00_1852)))->BgL_valuez00));
				BgL_tmpz00_2093 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_2093);
			}
		}

	}



/* &spread-side-effect!-1281 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21281za2zzeffect_spreadz00(obj_t
		BgL_envz00_1853, obj_t BgL_nodez00_1854)
	{
		{	/* Effect/spread.scm 173 */
			{	/* Effect/spread.scm 174 */
				bool_t BgL_tmpz00_2101;

				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1854)))->
						BgL_onexitz00));
				BgL_tmpz00_2101 =
					BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(((
							(BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
									BgL_nodez00_1854)))->BgL_bodyz00));
				return BBOOL(BgL_tmpz00_2101);
			}
		}

	}



/* &spread-side-effect!-1279 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21279za2zzeffect_spreadz00(obj_t
		BgL_envz00_1855, obj_t BgL_nodez00_1856)
	{
		{	/* Effect/spread.scm 159 */
			{	/* Effect/spread.scm 160 */
				bool_t BgL_tmpz00_2109;

				{	/* Effect/spread.scm 161 */
					bool_t BgL_g1121z00_1907;

					BgL_g1121z00_1907 =
						BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
						(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_1856)))->BgL_bodyz00));
					{
						obj_t BgL_bdgsz00_1909;
						bool_t BgL_resz00_1910;

						BgL_bdgsz00_1909 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_1856)))->
							BgL_bindingsz00);
						BgL_resz00_1910 = BgL_g1121z00_1907;
					BgL_loopz00_1908:
						if (NULLP(BgL_bdgsz00_1909))
							{	/* Effect/spread.scm 163 */
								((((BgL_nodezf2effectzf2_bglt) COBJECT(
												((BgL_nodezf2effectzf2_bglt)
													((BgL_letzd2varzd2_bglt) BgL_nodez00_1856))))->
										BgL_sidezd2effectzd2) =
									((obj_t) BBOOL(BgL_resz00_1910)), BUNSPEC);
								BgL_tmpz00_2109 = BgL_resz00_1910;
							}
						else
							{	/* Effect/spread.scm 167 */
								obj_t BgL_arg1408z00_1911;
								bool_t BgL_arg1410z00_1912;

								BgL_arg1408z00_1911 = CDR(((obj_t) BgL_bdgsz00_1909));
								{	/* Effect/spread.scm 168 */
									bool_t BgL__ortest_1122z00_1913;

									{	/* Effect/spread.scm 168 */
										obj_t BgL_arg1421z00_1914;

										{	/* Effect/spread.scm 168 */
											obj_t BgL_pairz00_1915;

											BgL_pairz00_1915 = CAR(((obj_t) BgL_bdgsz00_1909));
											BgL_arg1421z00_1914 = CDR(BgL_pairz00_1915);
										}
										BgL__ortest_1122z00_1913 =
											BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
											((BgL_nodez00_bglt) BgL_arg1421z00_1914));
									}
									if (BgL__ortest_1122z00_1913)
										{	/* Effect/spread.scm 168 */
											BgL_arg1410z00_1912 = BgL__ortest_1122z00_1913;
										}
									else
										{	/* Effect/spread.scm 168 */
											BgL_arg1410z00_1912 = BgL_resz00_1910;
										}
								}
								{
									bool_t BgL_resz00_2128;
									obj_t BgL_bdgsz00_2127;

									BgL_bdgsz00_2127 = BgL_arg1408z00_1911;
									BgL_resz00_2128 = BgL_arg1410z00_1912;
									BgL_resz00_1910 = BgL_resz00_2128;
									BgL_bdgsz00_1909 = BgL_bdgsz00_2127;
									goto BgL_loopz00_1908;
								}
							}
					}
				}
				return BBOOL(BgL_tmpz00_2109);
			}
		}

	}



/* &spread-side-effect!-1277 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21277za2zzeffect_spreadz00(obj_t
		BgL_envz00_1857, obj_t BgL_nodez00_1858)
	{
		{	/* Effect/spread.scm 147 */
			{	/* Effect/spread.scm 148 */
				bool_t BgL_tmpz00_2132;

				{	/* Effect/spread.scm 149 */
					obj_t BgL_g1247z00_1917;

					BgL_g1247z00_1917 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_1858)))->BgL_localsz00);
					{
						obj_t BgL_l1245z00_1919;

						BgL_l1245z00_1919 = BgL_g1247z00_1917;
					BgL_zc3z04anonymousza31372ze3z87_1918:
						if (PAIRP(BgL_l1245z00_1919))
							{	/* Effect/spread.scm 149 */
								{	/* Effect/spread.scm 150 */
									obj_t BgL_localz00_1920;

									BgL_localz00_1920 = CAR(BgL_l1245z00_1919);
									{	/* Effect/spread.scm 150 */
										obj_t BgL_arg1375z00_1921;

										BgL_arg1375z00_1921 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_localz00_1920))))->
															BgL_valuez00))))->BgL_bodyz00);
										BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00((
												(BgL_nodez00_bglt) BgL_arg1375z00_1921));
									}
								}
								{
									obj_t BgL_l1245z00_2145;

									BgL_l1245z00_2145 = CDR(BgL_l1245z00_1919);
									BgL_l1245z00_1919 = BgL_l1245z00_2145;
									goto BgL_zc3z04anonymousza31372ze3z87_1918;
								}
							}
						else
							{	/* Effect/spread.scm 149 */
								((bool_t) 1);
							}
					}
				}
				{	/* Effect/spread.scm 152 */
					bool_t BgL_resz00_1922;

					BgL_resz00_1922 =
						BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
						(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_1858)))->BgL_bodyz00));
					((((BgL_nodezf2effectzf2_bglt) COBJECT(
									((BgL_nodezf2effectzf2_bglt)
										((BgL_letzd2funzd2_bglt) BgL_nodez00_1858))))->
							BgL_sidezd2effectzd2) =
						((obj_t) BBOOL(BgL_resz00_1922)), BUNSPEC);
					BgL_tmpz00_2132 = BgL_resz00_1922;
				}
				return BBOOL(BgL_tmpz00_2132);
			}
		}

	}



/* &spread-side-effect!-1275 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21275za2zzeffect_spreadz00(obj_t
		BgL_envz00_1859, obj_t BgL_nodez00_1860)
	{
		{	/* Effect/spread.scm 133 */
			{	/* Effect/spread.scm 134 */
				bool_t BgL_tmpz00_2155;

				{	/* Effect/spread.scm 135 */
					bool_t BgL_g1117z00_1924;

					BgL_g1117z00_1924 =
						BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
						(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_1860)))->BgL_testz00));
					{
						obj_t BgL_clausesz00_1926;
						bool_t BgL_resz00_1927;

						BgL_clausesz00_1926 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_1860)))->BgL_clausesz00);
						BgL_resz00_1927 = BgL_g1117z00_1924;
					BgL_loopz00_1925:
						if (NULLP(BgL_clausesz00_1926))
							{	/* Effect/spread.scm 137 */
								((((BgL_nodezf2effectzf2_bglt) COBJECT(
												((BgL_nodezf2effectzf2_bglt)
													((BgL_switchz00_bglt) BgL_nodez00_1860))))->
										BgL_sidezd2effectzd2) =
									((obj_t) BBOOL(BgL_resz00_1927)), BUNSPEC);
								BgL_tmpz00_2155 = BgL_resz00_1927;
							}
						else
							{	/* Effect/spread.scm 141 */
								obj_t BgL_arg1361z00_1928;
								bool_t BgL_arg1364z00_1929;

								BgL_arg1361z00_1928 = CDR(((obj_t) BgL_clausesz00_1926));
								{	/* Effect/spread.scm 142 */
									bool_t BgL__ortest_1118z00_1930;

									{	/* Effect/spread.scm 142 */
										obj_t BgL_arg1367z00_1931;

										{	/* Effect/spread.scm 142 */
											obj_t BgL_pairz00_1932;

											BgL_pairz00_1932 = CAR(((obj_t) BgL_clausesz00_1926));
											BgL_arg1367z00_1931 = CDR(BgL_pairz00_1932);
										}
										BgL__ortest_1118z00_1930 =
											BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
											((BgL_nodez00_bglt) BgL_arg1367z00_1931));
									}
									if (BgL__ortest_1118z00_1930)
										{	/* Effect/spread.scm 142 */
											BgL_arg1364z00_1929 = BgL__ortest_1118z00_1930;
										}
									else
										{	/* Effect/spread.scm 142 */
											BgL_arg1364z00_1929 = BgL_resz00_1927;
										}
								}
								{
									bool_t BgL_resz00_2174;
									obj_t BgL_clausesz00_2173;

									BgL_clausesz00_2173 = BgL_arg1361z00_1928;
									BgL_resz00_2174 = BgL_arg1364z00_1929;
									BgL_resz00_1927 = BgL_resz00_2174;
									BgL_clausesz00_1926 = BgL_clausesz00_2173;
									goto BgL_loopz00_1925;
								}
							}
					}
				}
				return BBOOL(BgL_tmpz00_2155);
			}
		}

	}



/* &spread-side-effect!-1273 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21273za2zzeffect_spreadz00(obj_t
		BgL_envz00_1861, obj_t BgL_nodez00_1862)
	{
		{	/* Effect/spread.scm 123 */
			{	/* Effect/spread.scm 124 */
				bool_t BgL_tmpz00_2178;

				{	/* Effect/spread.scm 125 */
					bool_t BgL_reszd2proczd2_1934;

					BgL_reszd2proczd2_1934 =
						BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
						(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_1862)))->BgL_procz00));
					{	/* Effect/spread.scm 125 */
						bool_t BgL_reszd2msgzd2_1935;

						BgL_reszd2msgzd2_1935 =
							BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
							(((BgL_failz00_bglt) COBJECT(
										((BgL_failz00_bglt) BgL_nodez00_1862)))->BgL_msgz00));
						{	/* Effect/spread.scm 126 */
							bool_t BgL_reszd2objzd2_1936;

							BgL_reszd2objzd2_1936 =
								BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
								(((BgL_failz00_bglt) COBJECT(
											((BgL_failz00_bglt) BgL_nodez00_1862)))->BgL_objz00));
							BgL_tmpz00_2178 = ((bool_t) 1);
						}
					}
				}
				return BBOOL(BgL_tmpz00_2178);
			}
		}

	}



/* &spread-side-effect!-1271 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21271za2zzeffect_spreadz00(obj_t
		BgL_envz00_1863, obj_t BgL_nodez00_1864)
	{
		{	/* Effect/spread.scm 111 */
			{	/* Effect/spread.scm 112 */
				bool_t BgL_tmpz00_2189;

				{	/* Effect/spread.scm 113 */
					bool_t BgL_reszd2testzd2_1938;

					BgL_reszd2testzd2_1938 =
						BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_1864)))->BgL_testz00));
					{	/* Effect/spread.scm 113 */
						bool_t BgL_reszd2truezd2_1939;

						BgL_reszd2truezd2_1939 =
							BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_1864)))->
								BgL_truez00));
						{	/* Effect/spread.scm 114 */
							bool_t BgL_reszd2falsezd2_1940;

							BgL_reszd2falsezd2_1940 =
								BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
								(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_1864)))->
									BgL_falsez00));
							{	/* Effect/spread.scm 115 */
								bool_t BgL_resz00_1941;

								if (BgL_reszd2testzd2_1938)
									{	/* Effect/spread.scm 116 */
										BgL_resz00_1941 = BgL_reszd2testzd2_1938;
									}
								else
									{	/* Effect/spread.scm 116 */
										if (BgL_reszd2truezd2_1939)
											{	/* Effect/spread.scm 116 */
												BgL_resz00_1941 = BgL_reszd2truezd2_1939;
											}
										else
											{	/* Effect/spread.scm 116 */
												BgL_resz00_1941 = BgL_reszd2falsezd2_1940;
											}
									}
								{	/* Effect/spread.scm 116 */

									((((BgL_nodezf2effectzf2_bglt) COBJECT(
													((BgL_nodezf2effectzf2_bglt)
														((BgL_conditionalz00_bglt) BgL_nodez00_1864))))->
											BgL_sidezd2effectzd2) =
										((obj_t) BBOOL(BgL_resz00_1941)), BUNSPEC);
									BgL_tmpz00_2189 = BgL_resz00_1941;
								}
							}
						}
					}
				}
				return BBOOL(BgL_tmpz00_2189);
			}
		}

	}



/* &spread-side-effect!-1269 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21269za2zzeffect_spreadz00(obj_t
		BgL_envz00_1865, obj_t BgL_nodez00_1866)
	{
		{	/* Effect/spread.scm 103 */
			{	/* Effect/spread.scm 104 */
				bool_t BgL_tmpz00_2206;

				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_1866)))->BgL_valuez00));
				BgL_tmpz00_2206 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_2206);
			}
		}

	}



/* &spread-side-effect!-1267 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21267za2zzeffect_spreadz00(obj_t
		BgL_envz00_1867, obj_t BgL_nodez00_1868)
	{
		{	/* Effect/spread.scm 96 */
			return
				BBOOL(BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_1868)))->BgL_argz00)));
		}

	}



/* &spread-side-effect!-1265 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21265za2zzeffect_spreadz00(obj_t
		BgL_envz00_1869, obj_t BgL_nodez00_1870)
	{
		{	/* Effect/spread.scm 87 */
			{	/* Effect/spread.scm 89 */
				obj_t BgL_resz00_1945;

				{	/* Effect/spread.scm 89 */
					bool_t BgL__ortest_1109z00_1946;

					BgL__ortest_1109z00_1946 =
						BGl_spreadzd2sidezd2effectza2z12zb0zzeffect_spreadz00(
						(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_nodez00_1870)))->BgL_exprza2za2));
					if (BgL__ortest_1109z00_1946)
						{	/* Effect/spread.scm 89 */
							BgL_resz00_1945 = BBOOL(BgL__ortest_1109z00_1946);
						}
					else
						{	/* Effect/spread.scm 89 */
							BgL_resz00_1945 =
								(((BgL_nodezf2effectzf2_bglt) COBJECT(
										((BgL_nodezf2effectzf2_bglt)
											((BgL_externz00_bglt) BgL_nodez00_1870))))->
								BgL_sidezd2effectzd2);
						}
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_externz00_bglt) BgL_nodez00_1870))))->
						BgL_sidezd2effectzd2) = ((obj_t) BgL_resz00_1945), BUNSPEC);
				return BgL_resz00_1945;
			}
		}

	}



/* &spread-side-effect!-1263 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21263za2zzeffect_spreadz00(obj_t
		BgL_envz00_1871, obj_t BgL_nodez00_1872)
	{
		{	/* Effect/spread.scm 78 */
			{	/* Effect/spread.scm 79 */
				bool_t BgL_tmpz00_2226;

				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_1872)))->BgL_funz00));
				BGl_spreadzd2sidezd2effectza2z12zb0zzeffect_spreadz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_1872)))->BgL_argsz00));
				BgL_tmpz00_2226 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_2226);
			}
		}

	}



/* &spread-side-effect!-1261 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21261za2zzeffect_spreadz00(obj_t
		BgL_envz00_1873, obj_t BgL_nodez00_1874)
	{
		{	/* Effect/spread.scm 69 */
			{	/* Effect/spread.scm 70 */
				bool_t BgL_tmpz00_2234;

				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_1874)))->BgL_funz00));
				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_1874)))->BgL_argz00));
				BgL_tmpz00_2234 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_2234);
			}
		}

	}



/* &spread-side-effect!-1259 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21259za2zzeffect_spreadz00(obj_t
		BgL_envz00_1875, obj_t BgL_nodez00_1876)
	{
		{	/* Effect/spread.scm 59 */
			{	/* Effect/spread.scm 61 */
				obj_t BgL_resz00_1950;

				{	/* Effect/spread.scm 61 */
					bool_t BgL__ortest_1105z00_1951;

					BgL__ortest_1105z00_1951 =
						BGl_spreadzd2sidezd2effectza2z12zb0zzeffect_spreadz00(
						(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_1876)))->BgL_argsz00));
					if (BgL__ortest_1105z00_1951)
						{	/* Effect/spread.scm 61 */
							BgL_resz00_1950 = BBOOL(BgL__ortest_1105z00_1951);
						}
					else
						{	/* Effect/spread.scm 61 */
							BgL_resz00_1950 =
								(((BgL_funz00_bglt) COBJECT(
										((BgL_funz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														(((BgL_varz00_bglt) COBJECT(
																	(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_nodez00_1876)))->
																		BgL_funz00)))->BgL_variablez00)))->
												BgL_valuez00))))->BgL_sidezd2effectzd2);
						}
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_appz00_bglt) BgL_nodez00_1876))))->
						BgL_sidezd2effectzd2) = ((obj_t) BgL_resz00_1950), BUNSPEC);
				return BgL_resz00_1950;
			}
		}

	}



/* &spread-side-effect!-1257 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21257za2zzeffect_spreadz00(obj_t
		BgL_envz00_1877, obj_t BgL_nodez00_1878)
	{
		{	/* Effect/spread.scm 50 */
			{	/* Effect/spread.scm 51 */
				bool_t BgL_tmpz00_2256;

				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_1878)))->BgL_mutexz00));
				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_1878)))->BgL_prelockz00));
				BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_1878)))->BgL_bodyz00));
				BgL_tmpz00_2256 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_2256);
			}
		}

	}



/* &spread-side-effect!-1255 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21255za2zzeffect_spreadz00(obj_t
		BgL_envz00_1879, obj_t BgL_nodez00_1880)
	{
		{	/* Effect/spread.scm 42 */
			{	/* Effect/spread.scm 43 */
				bool_t BgL_tmpz00_2267;

				{	/* Effect/spread.scm 43 */
					bool_t BgL_resz00_1954;

					BgL_resz00_1954 =
						BGl_spreadzd2sidezd2effectza2z12zb0zzeffect_spreadz00(
						(((BgL_sequencez00_bglt) COBJECT(
									((BgL_sequencez00_bglt) BgL_nodez00_1880)))->BgL_nodesz00));
					((((BgL_nodezf2effectzf2_bglt) COBJECT(
									((BgL_nodezf2effectzf2_bglt)
										((BgL_sequencez00_bglt) BgL_nodez00_1880))))->
							BgL_sidezd2effectzd2) =
						((obj_t) BBOOL(BgL_resz00_1954)), BUNSPEC);
					BgL_tmpz00_2267 = BgL_resz00_1954;
				}
				return BBOOL(BgL_tmpz00_2267);
			}
		}

	}



/* &spread-side-effect!-1253 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21253za2zzeffect_spreadz00(obj_t
		BgL_envz00_1881, obj_t BgL_nodez00_1882)
	{
		{	/* Effect/spread.scm 35 */
			{	/* Effect/spread.scm 36 */
				bool_t BgL_tmpz00_2276;

				{	/* Effect/spread.scm 36 */
					obj_t BgL_arg1319z00_1956;

					BgL_arg1319z00_1956 =
						(((BgL_atomz00_bglt) COBJECT(
								((BgL_atomz00_bglt)
									((BgL_patchz00_bglt) BgL_nodez00_1882))))->BgL_valuez00);
					BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(
						((BgL_nodez00_bglt) BgL_arg1319z00_1956));
				}
				BgL_tmpz00_2276 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_2276);
			}
		}

	}



/* &spread-side-effect!-1251 */
	obj_t BGl_z62spreadzd2sidezd2effectz12zd21251za2zzeffect_spreadz00(obj_t
		BgL_envz00_1883, obj_t BgL_nodez00_1884)
	{
		{	/* Effect/spread.scm 29 */
			{	/* Effect/spread.scm 30 */
				bool_t BgL_tmpz00_2283;

				if (
					((((BgL_variablez00_bglt) COBJECT(
									(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_1884)))->
										BgL_variablez00)))->BgL_accessz00) == CNST_TABLE_REF(0)))
					{	/* Effect/spread.scm 30 */
						BgL_tmpz00_2283 = ((bool_t) 0);
					}
				else
					{	/* Effect/spread.scm 30 */
						BgL_tmpz00_2283 = ((bool_t) 1);
					}
				return BBOOL(BgL_tmpz00_2283);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzeffect_spreadz00(void)
	{
		{	/* Effect/spread.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1730z00zzeffect_spreadz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1730z00zzeffect_spreadz00));
			return
				BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1730z00zzeffect_spreadz00));
		}

	}

#ifdef __cplusplus
}
#endif
