/*===========================================================================*/
/*   (Effect/walk.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Effect/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EFFECT_WALK_TYPE_DEFINITIONS
#define BGL_EFFECT_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_feffectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_readz00;
		obj_t BgL_writez00;
	}                 *BgL_feffectz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_localzf2fromzf2_bgl
	{
		obj_t BgL_fromz00;
	}                      *BgL_localzf2fromzf2_bglt;

	typedef struct BgL_globalzf2fromzf2_bgl
	{
		obj_t BgL_fromz00;
	}                       *BgL_globalzf2fromzf2_bglt;


#endif													// BGL_EFFECT_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzeffect_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_getzd2varzf2sidezd2effectzf2zzeffect_cgraphz00(void);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzeffect_walkz00(void);
	static obj_t BGl_objectzd2initzd2zzeffect_walkz00(void);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_effectzd2walkz12zc0zzeffect_walkz00(obj_t,
		bool_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzeffect_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_z62zc3z04anonymousza31422ze3ze5zzeffect_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_writezd2effectzd2zzeffect_walkz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_feffectz12z12zzeffect_feffectz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzeffect_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_feffectz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_spreadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_cgraphz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_localzf2fromzf2zzeffect_cgraphz00;
	extern obj_t BGl_feffectz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzeffect_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzeffect_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzeffect_walkz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzeffect_walkz00(void);
	static obj_t BGl_z62effectzd2walkz12za2zzeffect_walkz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
	static bool_t BGl_iteratezd2tozd2fixzd2pointz12zc0zzeffect_walkz00(obj_t);
	extern obj_t
		BGl_funzd2callzd2graphz12z12zzeffect_cgraphz00(BgL_variablez00_bglt);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_getzd2varzf2allz20zzeffect_cgraphz00(void);
	static obj_t BGl_z62writezd2effectzb0zzeffect_walkz00(obj_t, obj_t);
	extern obj_t BGl_globalzf2fromzf2zzeffect_cgraphz00;
	extern bool_t
		BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(BgL_nodez00_bglt);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_writezd2effectzd2envz00zzeffect_walkz00,
		BgL_bgl_za762writeza7d2effec1771z00,
		BGl_z62writezd2effectzb0zzeffect_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1747z00zzeffect_walkz00,
		BgL_bgl_string1747za700za7za7e1772za7, "Effect", 6);
	      DEFINE_STRING(BGl_string1748z00zzeffect_walkz00,
		BgL_bgl_string1748za700za7za7e1773za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1749z00zzeffect_walkz00,
		BgL_bgl_string1749za700za7za7e1774za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1750z00zzeffect_walkz00,
		BgL_bgl_string1750za700za7za7e1775za7, " error", 6);
	      DEFINE_STRING(BGl_string1751z00zzeffect_walkz00,
		BgL_bgl_string1751za700za7za7e1776za7, "s", 1);
	      DEFINE_STRING(BGl_string1752z00zzeffect_walkz00,
		BgL_bgl_string1752za700za7za7e1777za7, "", 0);
	      DEFINE_STRING(BGl_string1753z00zzeffect_walkz00,
		BgL_bgl_string1753za700za7za7e1778za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1754z00zzeffect_walkz00,
		BgL_bgl_string1754za700za7za7e1779za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1755z00zzeffect_walkz00,
		BgL_bgl_string1755za700za7za7e1780za7, " (effect", 8);
	      DEFINE_STRING(BGl_string1756z00zzeffect_walkz00,
		BgL_bgl_string1756za700za7za7e1781za7, "   (", 4);
	      DEFINE_STRING(BGl_string1757z00zzeffect_walkz00,
		BgL_bgl_string1757za700za7za7e1782za7, ")", 1);
	      DEFINE_STRING(BGl_string1758z00zzeffect_walkz00,
		BgL_bgl_string1758za700za7za7e1783za7, " (read ", 7);
	      DEFINE_STRING(BGl_string1759z00zzeffect_walkz00,
		BgL_bgl_string1759za700za7za7e1784za7, " (write ", 8);
	      DEFINE_STRING(BGl_string1760z00zzeffect_walkz00,
		BgL_bgl_string1760za700za7za7e1785za7, "))", 2);
	      DEFINE_STRING(BGl_string1761z00zzeffect_walkz00,
		BgL_bgl_string1761za700za7za7e1786za7, ".sch", 4);
	      DEFINE_STRING(BGl_string1762z00zzeffect_walkz00,
		BgL_bgl_string1762za700za7za7e1787za7, "(directives\n (pragma\n", 21);
	      DEFINE_STRING(BGl_string1763z00zzeffect_walkz00,
		BgL_bgl_string1763za700za7za7e1788za7, " ))\n", 4);
	      DEFINE_STRING(BGl_string1764z00zzeffect_walkz00,
		BgL_bgl_string1764za700za7za7e1789za7, "write-effect", 12);
	      DEFINE_STRING(BGl_string1765z00zzeffect_walkz00,
		BgL_bgl_string1765za700za7za7e1790za7, "Can't open output file", 22);
	      DEFINE_STRING(BGl_string1766z00zzeffect_walkz00,
		BgL_bgl_string1766za700za7za7e1791za7, "effect_walk", 11);
	      DEFINE_STRING(BGl_string1767z00zzeffect_walkz00,
		BgL_bgl_string1767za700za7za7e1792za7,
		"top (reset-effect-tables!) pass-started ", 40);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_effectzd2walkz12zd2envz12zzeffect_walkz00,
		BgL_bgl_za762effectza7d2walk1793z00,
		BGl_z62effectzd2walkz12za2zzeffect_walkz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_resetzd2effectzd2tablesz12zd2envzc0zzeffect_cgraphz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzeffect_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzeffect_walkz00(long
		BgL_checksumz00_2100, char *BgL_fromz00_2101)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzeffect_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzeffect_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzeffect_walkz00();
					BGl_libraryzd2moduleszd2initz00zzeffect_walkz00();
					BGl_cnstzd2initzd2zzeffect_walkz00();
					BGl_importedzd2moduleszd2initz00zzeffect_walkz00();
					return BGl_methodzd2initzd2zzeffect_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzeffect_walkz00(void)
	{
		{	/* Effect/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"effect_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "effect_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"effect_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "effect_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "effect_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "effect_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "effect_walk");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "effect_walk");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "effect_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "effect_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"effect_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "effect_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"effect_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzeffect_walkz00(void)
	{
		{	/* Effect/walk.scm 15 */
			{	/* Effect/walk.scm 15 */
				obj_t BgL_cportz00_2088;

				{	/* Effect/walk.scm 15 */
					obj_t BgL_stringz00_2095;

					BgL_stringz00_2095 = BGl_string1767z00zzeffect_walkz00;
					{	/* Effect/walk.scm 15 */
						obj_t BgL_startz00_2096;

						BgL_startz00_2096 = BINT(0L);
						{	/* Effect/walk.scm 15 */
							obj_t BgL_endz00_2097;

							BgL_endz00_2097 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2095)));
							{	/* Effect/walk.scm 15 */

								BgL_cportz00_2088 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2095, BgL_startz00_2096, BgL_endz00_2097);
				}}}}
				{
					long BgL_iz00_2089;

					BgL_iz00_2089 = 2L;
				BgL_loopz00_2090:
					if ((BgL_iz00_2089 == -1L))
						{	/* Effect/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Effect/walk.scm 15 */
							{	/* Effect/walk.scm 15 */
								obj_t BgL_arg1770z00_2091;

								{	/* Effect/walk.scm 15 */

									{	/* Effect/walk.scm 15 */
										obj_t BgL_locationz00_2093;

										BgL_locationz00_2093 = BBOOL(((bool_t) 0));
										{	/* Effect/walk.scm 15 */

											BgL_arg1770z00_2091 =
												BGl_readz00zz__readerz00(BgL_cportz00_2088,
												BgL_locationz00_2093);
										}
									}
								}
								{	/* Effect/walk.scm 15 */
									int BgL_tmpz00_2132;

									BgL_tmpz00_2132 = (int) (BgL_iz00_2089);
									CNST_TABLE_SET(BgL_tmpz00_2132, BgL_arg1770z00_2091);
							}}
							{	/* Effect/walk.scm 15 */
								int BgL_auxz00_2094;

								BgL_auxz00_2094 = (int) ((BgL_iz00_2089 - 1L));
								{
									long BgL_iz00_2137;

									BgL_iz00_2137 = (long) (BgL_auxz00_2094);
									BgL_iz00_2089 = BgL_iz00_2137;
									goto BgL_loopz00_2090;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzeffect_walkz00(void)
	{
		{	/* Effect/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* effect-walk! */
	BGL_EXPORTED_DEF obj_t BGl_effectzd2walkz12zc0zzeffect_walkz00(obj_t
		BgL_globalsz00_3, bool_t BgL_feffectz00_4)
	{
		{	/* Effect/walk.scm 33 */
			{	/* Effect/walk.scm 34 */
				obj_t BgL_list1273z00_1484;

				{	/* Effect/walk.scm 34 */
					obj_t BgL_arg1284z00_1485;

					{	/* Effect/walk.scm 34 */
						obj_t BgL_arg1304z00_1486;

						BgL_arg1304z00_1486 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1284z00_1485 =
							MAKE_YOUNG_PAIR(BGl_string1747z00zzeffect_walkz00,
							BgL_arg1304z00_1486);
					}
					BgL_list1273z00_1484 =
						MAKE_YOUNG_PAIR(BGl_string1748z00zzeffect_walkz00,
						BgL_arg1284z00_1485);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1273z00_1484);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1747z00zzeffect_walkz00;
			{	/* Effect/walk.scm 34 */
				obj_t BgL_g1112z00_1487;

				BgL_g1112z00_1487 = BNIL;
				{
					obj_t BgL_hooksz00_1490;
					obj_t BgL_hnamesz00_1491;

					BgL_hooksz00_1490 = BgL_g1112z00_1487;
					BgL_hnamesz00_1491 = BNIL;
				BgL_zc3z04anonymousza31305ze3z87_1492:
					if (NULLP(BgL_hooksz00_1490))
						{	/* Effect/walk.scm 34 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Effect/walk.scm 34 */
							bool_t BgL_test1797z00_2150;

							{	/* Effect/walk.scm 34 */
								obj_t BgL_fun1313z00_1499;

								BgL_fun1313z00_1499 = CAR(((obj_t) BgL_hooksz00_1490));
								BgL_test1797z00_2150 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1313z00_1499));
							}
							if (BgL_test1797z00_2150)
								{	/* Effect/walk.scm 34 */
									obj_t BgL_arg1310z00_1496;
									obj_t BgL_arg1311z00_1497;

									BgL_arg1310z00_1496 = CDR(((obj_t) BgL_hooksz00_1490));
									BgL_arg1311z00_1497 = CDR(((obj_t) BgL_hnamesz00_1491));
									{
										obj_t BgL_hnamesz00_2162;
										obj_t BgL_hooksz00_2161;

										BgL_hooksz00_2161 = BgL_arg1310z00_1496;
										BgL_hnamesz00_2162 = BgL_arg1311z00_1497;
										BgL_hnamesz00_1491 = BgL_hnamesz00_2162;
										BgL_hooksz00_1490 = BgL_hooksz00_2161;
										goto BgL_zc3z04anonymousza31305ze3z87_1492;
									}
								}
							else
								{	/* Effect/walk.scm 34 */
									obj_t BgL_arg1312z00_1498;

									BgL_arg1312z00_1498 = CAR(((obj_t) BgL_hnamesz00_1491));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1747z00zzeffect_walkz00,
										BGl_string1749z00zzeffect_walkz00, BgL_arg1312z00_1498);
								}
						}
				}
			}
			{
				obj_t BgL_l1245z00_1503;

				BgL_l1245z00_1503 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31315ze3z87_1504:
				if (PAIRP(BgL_l1245z00_1503))
					{	/* Effect/walk.scm 37 */
						{	/* Effect/walk.scm 38 */
							obj_t BgL_globalz00_1506;

							BgL_globalz00_1506 = CAR(BgL_l1245z00_1503);
							BGl_funzd2callzd2graphz12z12zzeffect_cgraphz00(
								((BgL_variablez00_bglt) BgL_globalz00_1506));
						}
						{
							obj_t BgL_l1245z00_2171;

							BgL_l1245z00_2171 = CDR(BgL_l1245z00_1503);
							BgL_l1245z00_1503 = BgL_l1245z00_2171;
							goto BgL_zc3z04anonymousza31315ze3z87_1504;
						}
					}
				else
					{	/* Effect/walk.scm 37 */
						((bool_t) 1);
					}
			}
			BGl_iteratezd2tozd2fixzd2pointz12zc0zzeffect_walkz00
				(BGl_getzd2varzf2sidezd2effectzf2zzeffect_cgraphz00());
			{	/* Effect/walk.scm 46 */
				obj_t BgL_g1249z00_1510;

				BgL_g1249z00_1510 = BGl_getzd2varzf2allz20zzeffect_cgraphz00();
				{
					obj_t BgL_l1247z00_1512;

					BgL_l1247z00_1512 = BgL_g1249z00_1510;
				BgL_zc3z04anonymousza31319ze3z87_1513:
					if (PAIRP(BgL_l1247z00_1512))
						{	/* Effect/walk.scm 50 */
							{	/* Effect/walk.scm 47 */
								obj_t BgL_varz00_1515;

								BgL_varz00_1515 = CAR(BgL_l1247z00_1512);
								{	/* Effect/walk.scm 47 */
									BgL_valuez00_bglt BgL_funz00_1516;

									BgL_funz00_1516 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_varz00_1515)))->
										BgL_valuez00);
									if (((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_funz00_1516)))->BgL_sidezd2effectzd2) ==
											BUNSPEC))
										{	/* Effect/walk.scm 48 */
											((((BgL_funz00_bglt) COBJECT(
															((BgL_funz00_bglt) BgL_funz00_1516)))->
													BgL_sidezd2effectzd2) = ((obj_t) BFALSE), BUNSPEC);
										}
									else
										{	/* Effect/walk.scm 48 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1247z00_2187;

								BgL_l1247z00_2187 = CDR(BgL_l1247z00_1512);
								BgL_l1247z00_1512 = BgL_l1247z00_2187;
								goto BgL_zc3z04anonymousza31319ze3z87_1513;
							}
						}
					else
						{	/* Effect/walk.scm 50 */
							((bool_t) 1);
						}
				}
			}
			{
				obj_t BgL_l1250z00_1523;

				BgL_l1250z00_1523 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31326ze3z87_1524:
				if (PAIRP(BgL_l1250z00_1523))
					{	/* Effect/walk.scm 53 */
						{	/* Effect/walk.scm 54 */
							obj_t BgL_globalz00_1526;

							BgL_globalz00_1526 = CAR(BgL_l1250z00_1523);
							{	/* Effect/walk.scm 54 */
								obj_t BgL_arg1328z00_1527;

								BgL_arg1328z00_1527 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_globalz00_1526))))->
													BgL_valuez00))))->BgL_bodyz00);
								BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00((
										(BgL_nodez00_bglt) BgL_arg1328z00_1527));
							}
						}
						{
							obj_t BgL_l1250z00_2199;

							BgL_l1250z00_2199 = CDR(BgL_l1250z00_1523);
							BgL_l1250z00_1523 = BgL_l1250z00_2199;
							goto BgL_zc3z04anonymousza31326ze3z87_1524;
						}
					}
				else
					{	/* Effect/walk.scm 53 */
						((bool_t) 1);
					}
			}
			if (BgL_feffectz00_4)
				{	/* Effect/walk.scm 59 */
					BGl_feffectz12z12zzeffect_feffectz00(BgL_globalsz00_3);
				}
			else
				{	/* Effect/walk.scm 59 */
					BFALSE;
				}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Effect/walk.scm 62 */
					{	/* Effect/walk.scm 62 */
						obj_t BgL_port1252z00_1533;

						{	/* Effect/walk.scm 62 */
							obj_t BgL_tmpz00_2206;

							BgL_tmpz00_2206 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1252z00_1533 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2206);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1252z00_1533);
						bgl_display_string(BGl_string1750z00zzeffect_walkz00,
							BgL_port1252z00_1533);
						{	/* Effect/walk.scm 62 */
							obj_t BgL_arg1333z00_1534;

							{	/* Effect/walk.scm 62 */
								bool_t BgL_test1809z00_2211;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Effect/walk.scm 62 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Effect/walk.scm 62 */
												BgL_test1809z00_2211 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Effect/walk.scm 62 */
												BgL_test1809z00_2211 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Effect/walk.scm 62 */
										BgL_test1809z00_2211 = ((bool_t) 0);
									}
								if (BgL_test1809z00_2211)
									{	/* Effect/walk.scm 62 */
										BgL_arg1333z00_1534 = BGl_string1751z00zzeffect_walkz00;
									}
								else
									{	/* Effect/walk.scm 62 */
										BgL_arg1333z00_1534 = BGl_string1752z00zzeffect_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1333z00_1534, BgL_port1252z00_1533);
						}
						bgl_display_string(BGl_string1753z00zzeffect_walkz00,
							BgL_port1252z00_1533);
						bgl_display_char(((unsigned char) 10), BgL_port1252z00_1533);
					}
					{	/* Effect/walk.scm 62 */
						obj_t BgL_list1336z00_1538;

						BgL_list1336z00_1538 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						return BGl_exitz00zz__errorz00(BgL_list1336z00_1538);
					}
				}
			else
				{	/* Effect/walk.scm 62 */
					obj_t BgL_g1114z00_1539;
					obj_t BgL_g1115z00_1540;

					{	/* Effect/walk.scm 62 */
						obj_t BgL_list1348z00_1553;

						BgL_list1348z00_1553 =
							MAKE_YOUNG_PAIR
							(BGl_resetzd2effectzd2tablesz12zd2envzc0zzeffect_cgraphz00, BNIL);
						BgL_g1114z00_1539 = BgL_list1348z00_1553;
					}
					BgL_g1115z00_1540 = CNST_TABLE_REF(1);
					{
						obj_t BgL_hooksz00_1542;
						obj_t BgL_hnamesz00_1543;

						BgL_hooksz00_1542 = BgL_g1114z00_1539;
						BgL_hnamesz00_1543 = BgL_g1115z00_1540;
					BgL_zc3z04anonymousza31337ze3z87_1544:
						if (NULLP(BgL_hooksz00_1542))
							{	/* Effect/walk.scm 62 */
								return BgL_globalsz00_3;
							}
						else
							{	/* Effect/walk.scm 62 */
								bool_t BgL_test1813z00_2230;

								{	/* Effect/walk.scm 62 */
									obj_t BgL_fun1347z00_1551;

									BgL_fun1347z00_1551 = CAR(((obj_t) BgL_hooksz00_1542));
									BgL_test1813z00_2230 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1347z00_1551));
								}
								if (BgL_test1813z00_2230)
									{	/* Effect/walk.scm 62 */
										obj_t BgL_arg1342z00_1548;
										obj_t BgL_arg1343z00_1549;

										BgL_arg1342z00_1548 = CDR(((obj_t) BgL_hooksz00_1542));
										BgL_arg1343z00_1549 = CDR(((obj_t) BgL_hnamesz00_1543));
										{
											obj_t BgL_hnamesz00_2242;
											obj_t BgL_hooksz00_2241;

											BgL_hooksz00_2241 = BgL_arg1342z00_1548;
											BgL_hnamesz00_2242 = BgL_arg1343z00_1549;
											BgL_hnamesz00_1543 = BgL_hnamesz00_2242;
											BgL_hooksz00_1542 = BgL_hooksz00_2241;
											goto BgL_zc3z04anonymousza31337ze3z87_1544;
										}
									}
								else
									{	/* Effect/walk.scm 62 */
										obj_t BgL_arg1346z00_1550;

										BgL_arg1346z00_1550 = CAR(((obj_t) BgL_hnamesz00_1543));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string1754z00zzeffect_walkz00, BgL_arg1346z00_1550);
									}
							}
					}
				}
		}

	}



/* &effect-walk! */
	obj_t BGl_z62effectzd2walkz12za2zzeffect_walkz00(obj_t BgL_envz00_2079,
		obj_t BgL_globalsz00_2080, obj_t BgL_feffectz00_2081)
	{
		{	/* Effect/walk.scm 33 */
			return
				BGl_effectzd2walkz12zc0zzeffect_walkz00(BgL_globalsz00_2080,
				CBOOL(BgL_feffectz00_2081));
		}

	}



/* iterate-to-fix-point! */
	bool_t BGl_iteratezd2tozd2fixzd2pointz12zc0zzeffect_walkz00(obj_t BgL_wz00_5)
	{
		{	/* Effect/walk.scm 67 */
			if (PAIRP(BgL_wz00_5))
				{
					obj_t BgL_l1253z00_1556;

					BgL_l1253z00_1556 = BgL_wz00_5;
				BgL_zc3z04anonymousza31351ze3z87_1557:
					if (PAIRP(BgL_l1253z00_1556))
						{	/* Effect/walk.scm 69 */
							{	/* Effect/walk.scm 71 */
								obj_t BgL_varz00_1559;

								BgL_varz00_1559 = CAR(BgL_l1253z00_1556);
								{	/* Effect/walk.scm 71 */
									BgL_valuez00_bglt BgL_funz00_1560;

									BgL_funz00_1560 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_varz00_1559)))->
										BgL_valuez00);
									if (((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_funz00_1560)))->BgL_sidezd2effectzd2) ==
											BTRUE))
										{	/* Effect/walk.scm 72 */
											((bool_t) 0);
										}
									else
										{	/* Effect/walk.scm 72 */
											((((BgL_funz00_bglt) COBJECT(
															((BgL_funz00_bglt) BgL_funz00_1560)))->
													BgL_sidezd2effectzd2) = ((obj_t) BTRUE), BUNSPEC);
											{	/* Effect/walk.scm 75 */
												bool_t BgL_test1817z00_2261;

												{	/* Effect/walk.scm 75 */
													obj_t BgL_classz00_1840;

													BgL_classz00_1840 =
														BGl_localzf2fromzf2zzeffect_cgraphz00;
													if (BGL_OBJECTP(BgL_varz00_1559))
														{	/* Effect/walk.scm 75 */
															BgL_objectz00_bglt BgL_arg1807z00_1842;

															BgL_arg1807z00_1842 =
																(BgL_objectz00_bglt) (BgL_varz00_1559);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Effect/walk.scm 75 */
																	long BgL_idxz00_1848;

																	BgL_idxz00_1848 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_1842);
																	BgL_test1817z00_2261 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_1848 + 3L)) ==
																		BgL_classz00_1840);
																}
															else
																{	/* Effect/walk.scm 75 */
																	bool_t BgL_res1741z00_1873;

																	{	/* Effect/walk.scm 75 */
																		obj_t BgL_oclassz00_1856;

																		{	/* Effect/walk.scm 75 */
																			obj_t BgL_arg1815z00_1864;
																			long BgL_arg1816z00_1865;

																			BgL_arg1815z00_1864 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Effect/walk.scm 75 */
																				long BgL_arg1817z00_1866;

																				BgL_arg1817z00_1866 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_1842);
																				BgL_arg1816z00_1865 =
																					(BgL_arg1817z00_1866 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_1856 =
																				VECTOR_REF(BgL_arg1815z00_1864,
																				BgL_arg1816z00_1865);
																		}
																		{	/* Effect/walk.scm 75 */
																			bool_t BgL__ortest_1115z00_1857;

																			BgL__ortest_1115z00_1857 =
																				(BgL_classz00_1840 ==
																				BgL_oclassz00_1856);
																			if (BgL__ortest_1115z00_1857)
																				{	/* Effect/walk.scm 75 */
																					BgL_res1741z00_1873 =
																						BgL__ortest_1115z00_1857;
																				}
																			else
																				{	/* Effect/walk.scm 75 */
																					long BgL_odepthz00_1858;

																					{	/* Effect/walk.scm 75 */
																						obj_t BgL_arg1804z00_1859;

																						BgL_arg1804z00_1859 =
																							(BgL_oclassz00_1856);
																						BgL_odepthz00_1858 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_1859);
																					}
																					if ((3L < BgL_odepthz00_1858))
																						{	/* Effect/walk.scm 75 */
																							obj_t BgL_arg1802z00_1861;

																							{	/* Effect/walk.scm 75 */
																								obj_t BgL_arg1803z00_1862;

																								BgL_arg1803z00_1862 =
																									(BgL_oclassz00_1856);
																								BgL_arg1802z00_1861 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_1862, 3L);
																							}
																							BgL_res1741z00_1873 =
																								(BgL_arg1802z00_1861 ==
																								BgL_classz00_1840);
																						}
																					else
																						{	/* Effect/walk.scm 75 */
																							BgL_res1741z00_1873 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1817z00_2261 = BgL_res1741z00_1873;
																}
														}
													else
														{	/* Effect/walk.scm 75 */
															BgL_test1817z00_2261 = ((bool_t) 0);
														}
												}
												if (BgL_test1817z00_2261)
													{	/* Effect/walk.scm 76 */
														obj_t BgL_arg1364z00_1564;

														{
															BgL_localzf2fromzf2_bglt BgL_auxz00_2284;

															{
																obj_t BgL_auxz00_2285;

																{	/* Effect/walk.scm 76 */
																	BgL_objectz00_bglt BgL_tmpz00_2286;

																	BgL_tmpz00_2286 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt) BgL_varz00_1559));
																	BgL_auxz00_2285 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2286);
																}
																BgL_auxz00_2284 =
																	((BgL_localzf2fromzf2_bglt) BgL_auxz00_2285);
															}
															BgL_arg1364z00_1564 =
																(((BgL_localzf2fromzf2_bglt)
																	COBJECT(BgL_auxz00_2284))->BgL_fromz00);
														}
														BGl_iteratezd2tozd2fixzd2pointz12zc0zzeffect_walkz00
															(BgL_arg1364z00_1564);
													}
												else
													{	/* Effect/walk.scm 77 */
														bool_t BgL_test1824z00_2293;

														{	/* Effect/walk.scm 77 */
															obj_t BgL_classz00_1876;

															BgL_classz00_1876 =
																BGl_globalzf2fromzf2zzeffect_cgraphz00;
															if (BGL_OBJECTP(BgL_varz00_1559))
																{	/* Effect/walk.scm 77 */
																	BgL_objectz00_bglt BgL_arg1807z00_1878;

																	BgL_arg1807z00_1878 =
																		(BgL_objectz00_bglt) (BgL_varz00_1559);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Effect/walk.scm 77 */
																			long BgL_idxz00_1884;

																			BgL_idxz00_1884 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_1878);
																			BgL_test1824z00_2293 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_1884 + 3L)) ==
																				BgL_classz00_1876);
																		}
																	else
																		{	/* Effect/walk.scm 77 */
																			bool_t BgL_res1742z00_1909;

																			{	/* Effect/walk.scm 77 */
																				obj_t BgL_oclassz00_1892;

																				{	/* Effect/walk.scm 77 */
																					obj_t BgL_arg1815z00_1900;
																					long BgL_arg1816z00_1901;

																					BgL_arg1815z00_1900 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Effect/walk.scm 77 */
																						long BgL_arg1817z00_1902;

																						BgL_arg1817z00_1902 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_1878);
																						BgL_arg1816z00_1901 =
																							(BgL_arg1817z00_1902 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_1892 =
																						VECTOR_REF(BgL_arg1815z00_1900,
																						BgL_arg1816z00_1901);
																				}
																				{	/* Effect/walk.scm 77 */
																					bool_t BgL__ortest_1115z00_1893;

																					BgL__ortest_1115z00_1893 =
																						(BgL_classz00_1876 ==
																						BgL_oclassz00_1892);
																					if (BgL__ortest_1115z00_1893)
																						{	/* Effect/walk.scm 77 */
																							BgL_res1742z00_1909 =
																								BgL__ortest_1115z00_1893;
																						}
																					else
																						{	/* Effect/walk.scm 77 */
																							long BgL_odepthz00_1894;

																							{	/* Effect/walk.scm 77 */
																								obj_t BgL_arg1804z00_1895;

																								BgL_arg1804z00_1895 =
																									(BgL_oclassz00_1892);
																								BgL_odepthz00_1894 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_1895);
																							}
																							if ((3L < BgL_odepthz00_1894))
																								{	/* Effect/walk.scm 77 */
																									obj_t BgL_arg1802z00_1897;

																									{	/* Effect/walk.scm 77 */
																										obj_t BgL_arg1803z00_1898;

																										BgL_arg1803z00_1898 =
																											(BgL_oclassz00_1892);
																										BgL_arg1802z00_1897 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_1898, 3L);
																									}
																									BgL_res1742z00_1909 =
																										(BgL_arg1802z00_1897 ==
																										BgL_classz00_1876);
																								}
																							else
																								{	/* Effect/walk.scm 77 */
																									BgL_res1742z00_1909 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1824z00_2293 =
																				BgL_res1742z00_1909;
																		}
																}
															else
																{	/* Effect/walk.scm 77 */
																	BgL_test1824z00_2293 = ((bool_t) 0);
																}
														}
														if (BgL_test1824z00_2293)
															{	/* Effect/walk.scm 78 */
																obj_t BgL_arg1367z00_1566;

																{
																	BgL_globalzf2fromzf2_bglt BgL_auxz00_2316;

																	{
																		obj_t BgL_auxz00_2317;

																		{	/* Effect/walk.scm 78 */
																			BgL_objectz00_bglt BgL_tmpz00_2318;

																			BgL_tmpz00_2318 =
																				((BgL_objectz00_bglt)
																				((BgL_globalz00_bglt) BgL_varz00_1559));
																			BgL_auxz00_2317 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2318);
																		}
																		BgL_auxz00_2316 =
																			((BgL_globalzf2fromzf2_bglt)
																			BgL_auxz00_2317);
																	}
																	BgL_arg1367z00_1566 =
																		(((BgL_globalzf2fromzf2_bglt)
																			COBJECT(BgL_auxz00_2316))->BgL_fromz00);
																}
																BGl_iteratezd2tozd2fixzd2pointz12zc0zzeffect_walkz00
																	(BgL_arg1367z00_1566);
															}
														else
															{	/* Effect/walk.scm 77 */
																((bool_t) 0);
															}
													}
											}
										}
								}
							}
							{
								obj_t BgL_l1253z00_2325;

								BgL_l1253z00_2325 = CDR(BgL_l1253z00_1556);
								BgL_l1253z00_1556 = BgL_l1253z00_2325;
								goto BgL_zc3z04anonymousza31351ze3z87_1557;
							}
						}
					else
						{	/* Effect/walk.scm 69 */
							return ((bool_t) 1);
						}
				}
			else
				{	/* Effect/walk.scm 68 */
					return ((bool_t) 0);
				}
		}

	}



/* write-effect */
	BGL_EXPORTED_DEF obj_t BGl_writezd2effectzd2zzeffect_walkz00(obj_t
		BgL_globalsz00_6)
	{
		{	/* Effect/walk.scm 84 */
			{
				obj_t BgL_gz00_1602;

				{	/* Effect/walk.scm 95 */
					obj_t BgL_onamez00_1571;

					if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
						{	/* Effect/walk.scm 95 */
							BgL_onamez00_1571 = BGl_za2destza2z00zzengine_paramz00;
						}
					else
						{	/* Effect/walk.scm 97 */
							bool_t BgL_test1831z00_2329;

							if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
								{	/* Effect/walk.scm 97 */
									obj_t BgL_tmpz00_2332;

									BgL_tmpz00_2332 =
										CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
									BgL_test1831z00_2329 = STRINGP(BgL_tmpz00_2332);
								}
							else
								{	/* Effect/walk.scm 97 */
									BgL_test1831z00_2329 = ((bool_t) 0);
								}
							if (BgL_test1831z00_2329)
								{	/* Effect/walk.scm 97 */
									BgL_onamez00_1571 =
										string_append(BGl_prefixz00zz__osz00(CAR
											(BGl_za2srczd2filesza2zd2zzengine_paramz00)),
										BGl_string1761z00zzeffect_walkz00);
								}
							else
								{	/* Effect/walk.scm 97 */
									BgL_onamez00_1571 = BFALSE;
								}
						}
					{	/* Effect/walk.scm 95 */
						obj_t BgL_portz00_1572;

						if (STRINGP(BgL_onamez00_1571))
							{	/* Effect/walk.scm 101 */

								BgL_portz00_1572 =
									BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00
									(BgL_onamez00_1571, BTRUE);
							}
						else
							{	/* Effect/walk.scm 102 */
								obj_t BgL_tmpz00_2341;

								BgL_tmpz00_2341 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_portz00_1572 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2341);
							}
						{	/* Effect/walk.scm 100 */

							if (OUTPUT_PORTP(BgL_portz00_1572))
								{	/* Effect/walk.scm 105 */
									obj_t BgL_exitd1117z00_1574;

									BgL_exitd1117z00_1574 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Effect/walk.scm 110 */
										obj_t BgL_zc3z04anonymousza31422ze3z87_2083;

										BgL_zc3z04anonymousza31422ze3z87_2083 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31422ze3ze5zzeffect_walkz00,
											(int) (0L), (int) (1L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31422ze3z87_2083,
											(int) (0L), BgL_portz00_1572);
										{	/* Effect/walk.scm 105 */
											obj_t BgL_arg1828z00_1960;

											{	/* Effect/walk.scm 105 */
												obj_t BgL_arg1829z00_1961;

												BgL_arg1829z00_1961 =
													BGL_EXITD_PROTECT(BgL_exitd1117z00_1574);
												BgL_arg1828z00_1960 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31422ze3z87_2083,
													BgL_arg1829z00_1961);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1117z00_1574,
												BgL_arg1828z00_1960);
											BUNSPEC;
										}
										{	/* Effect/walk.scm 107 */
											obj_t BgL_tmp1119z00_1576;

											{	/* Effect/walk.scm 107 */
												obj_t BgL_arg1375z00_1577;

												{	/* Effect/walk.scm 107 */
													obj_t BgL_tmpz00_2355;

													BgL_tmpz00_2355 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_arg1375z00_1577 =
														BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2355);
												}
												bgl_display_string(BGl_string1762z00zzeffect_walkz00,
													BgL_arg1375z00_1577);
											}
											{
												obj_t BgL_l1256z00_1579;

												BgL_l1256z00_1579 = BgL_globalsz00_6;
											BgL_zc3z04anonymousza31376ze3z87_1580:
												if (PAIRP(BgL_l1256z00_1579))
													{	/* Effect/walk.scm 108 */
														BgL_gz00_1602 = CAR(BgL_l1256z00_1579);
														{	/* Effect/walk.scm 86 */
															obj_t BgL_fez00_1604;

															BgL_fez00_1604 =
																(((BgL_funz00_bglt) COBJECT(
																		((BgL_funz00_bglt)
																			(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							BgL_gz00_1602)))->
																				BgL_valuez00))))->BgL_effectz00);
															{	/* Effect/walk.scm 87 */
																bool_t BgL_test1836z00_2365;

																{	/* Effect/walk.scm 87 */
																	obj_t BgL_classz00_1915;

																	BgL_classz00_1915 =
																		BGl_feffectz00zzast_varz00;
																	if (BGL_OBJECTP(BgL_fez00_1604))
																		{	/* Effect/walk.scm 87 */
																			BgL_objectz00_bglt BgL_arg1807z00_1917;

																			BgL_arg1807z00_1917 =
																				(BgL_objectz00_bglt) (BgL_fez00_1604);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Effect/walk.scm 87 */
																					long BgL_idxz00_1923;

																					BgL_idxz00_1923 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_1917);
																					BgL_test1836z00_2365 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_1923 + 1L)) ==
																						BgL_classz00_1915);
																				}
																			else
																				{	/* Effect/walk.scm 87 */
																					bool_t BgL_res1743z00_1948;

																					{	/* Effect/walk.scm 87 */
																						obj_t BgL_oclassz00_1931;

																						{	/* Effect/walk.scm 87 */
																							obj_t BgL_arg1815z00_1939;
																							long BgL_arg1816z00_1940;

																							BgL_arg1815z00_1939 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Effect/walk.scm 87 */
																								long BgL_arg1817z00_1941;

																								BgL_arg1817z00_1941 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_1917);
																								BgL_arg1816z00_1940 =
																									(BgL_arg1817z00_1941 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_1931 =
																								VECTOR_REF(BgL_arg1815z00_1939,
																								BgL_arg1816z00_1940);
																						}
																						{	/* Effect/walk.scm 87 */
																							bool_t BgL__ortest_1115z00_1932;

																							BgL__ortest_1115z00_1932 =
																								(BgL_classz00_1915 ==
																								BgL_oclassz00_1931);
																							if (BgL__ortest_1115z00_1932)
																								{	/* Effect/walk.scm 87 */
																									BgL_res1743z00_1948 =
																										BgL__ortest_1115z00_1932;
																								}
																							else
																								{	/* Effect/walk.scm 87 */
																									long BgL_odepthz00_1933;

																									{	/* Effect/walk.scm 87 */
																										obj_t BgL_arg1804z00_1934;

																										BgL_arg1804z00_1934 =
																											(BgL_oclassz00_1931);
																										BgL_odepthz00_1933 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_1934);
																									}
																									if ((1L < BgL_odepthz00_1933))
																										{	/* Effect/walk.scm 87 */
																											obj_t BgL_arg1802z00_1936;

																											{	/* Effect/walk.scm 87 */
																												obj_t
																													BgL_arg1803z00_1937;
																												BgL_arg1803z00_1937 =
																													(BgL_oclassz00_1931);
																												BgL_arg1802z00_1936 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_1937,
																													1L);
																											}
																											BgL_res1743z00_1948 =
																												(BgL_arg1802z00_1936 ==
																												BgL_classz00_1915);
																										}
																									else
																										{	/* Effect/walk.scm 87 */
																											BgL_res1743z00_1948 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1836z00_2365 =
																						BgL_res1743z00_1948;
																				}
																		}
																	else
																		{	/* Effect/walk.scm 87 */
																			BgL_test1836z00_2365 = ((bool_t) 0);
																		}
																}
																if (BgL_test1836z00_2365)
																	{	/* Effect/walk.scm 89 */
																		bool_t BgL_test1841z00_2388;

																		if (
																			((((BgL_feffectz00_bglt) COBJECT(
																							((BgL_feffectz00_bglt)
																								BgL_fez00_1604)))->
																					BgL_readz00) == CNST_TABLE_REF(2)))
																			{	/* Effect/walk.scm 89 */
																				BgL_test1841z00_2388 =
																					(
																					(((BgL_feffectz00_bglt) COBJECT(
																								((BgL_feffectz00_bglt)
																									BgL_fez00_1604)))->
																						BgL_writez00) == CNST_TABLE_REF(2));
																			}
																		else
																			{	/* Effect/walk.scm 89 */
																				BgL_test1841z00_2388 = ((bool_t) 0);
																			}
																		if (BgL_test1841z00_2388)
																			{	/* Effect/walk.scm 89 */
																				BFALSE;
																			}
																		else
																			{	/* Effect/walk.scm 89 */
																				{	/* Effect/walk.scm 91 */
																					obj_t BgL_arg1509z00_1612;

																					BgL_arg1509z00_1612 =
																						BGl_shapez00zztools_shapez00
																						(BgL_gz00_1602);
																					{	/* Effect/walk.scm 91 */
																						obj_t BgL_list1510z00_1613;

																						{	/* Effect/walk.scm 91 */
																							obj_t BgL_arg1513z00_1614;

																							{	/* Effect/walk.scm 91 */
																								obj_t BgL_arg1514z00_1615;

																								BgL_arg1514z00_1615 =
																									MAKE_YOUNG_PAIR
																									(BGl_string1755z00zzeffect_walkz00,
																									BNIL);
																								BgL_arg1513z00_1614 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1509z00_1612,
																									BgL_arg1514z00_1615);
																							}
																							BgL_list1510z00_1613 =
																								MAKE_YOUNG_PAIR
																								(BGl_string1756z00zzeffect_walkz00,
																								BgL_arg1513z00_1614);
																						}
																						BGl_displayza2za2zz__r4_output_6_10_3z00
																							(BgL_list1510z00_1613);
																					}
																				}
																				if (NULLP(
																						(((BgL_feffectz00_bglt) COBJECT(
																									((BgL_feffectz00_bglt)
																										BgL_fez00_1604)))->
																							BgL_readz00)))
																					{	/* Effect/walk.scm 92 */
																						BFALSE;
																					}
																				else
																					{	/* Effect/walk.scm 92 */
																						obj_t BgL_arg1535z00_1618;

																						BgL_arg1535z00_1618 =
																							(((BgL_feffectz00_bglt) COBJECT(
																									((BgL_feffectz00_bglt)
																										BgL_fez00_1604)))->
																							BgL_readz00);
																						{	/* Effect/walk.scm 92 */
																							obj_t BgL_list1536z00_1619;

																							{	/* Effect/walk.scm 92 */
																								obj_t BgL_arg1540z00_1620;

																								{	/* Effect/walk.scm 92 */
																									obj_t BgL_arg1544z00_1621;

																									BgL_arg1544z00_1621 =
																										MAKE_YOUNG_PAIR
																										(BGl_string1757z00zzeffect_walkz00,
																										BNIL);
																									BgL_arg1540z00_1620 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1535z00_1618,
																										BgL_arg1544z00_1621);
																								}
																								BgL_list1536z00_1619 =
																									MAKE_YOUNG_PAIR
																									(BGl_string1758z00zzeffect_walkz00,
																									BgL_arg1540z00_1620);
																							}
																							BGl_displayza2za2zz__r4_output_6_10_3z00
																								(BgL_list1536z00_1619);
																						}
																					}
																				if (NULLP(
																						(((BgL_feffectz00_bglt) COBJECT(
																									((BgL_feffectz00_bglt)
																										BgL_fez00_1604)))->
																							BgL_writez00)))
																					{	/* Effect/walk.scm 93 */
																						BFALSE;
																					}
																				else
																					{	/* Effect/walk.scm 93 */
																						obj_t BgL_arg1559z00_1625;

																						BgL_arg1559z00_1625 =
																							(((BgL_feffectz00_bglt) COBJECT(
																									((BgL_feffectz00_bglt)
																										BgL_fez00_1604)))->
																							BgL_writez00);
																						{	/* Effect/walk.scm 93 */
																							obj_t BgL_list1560z00_1626;

																							{	/* Effect/walk.scm 93 */
																								obj_t BgL_arg1561z00_1627;

																								{	/* Effect/walk.scm 93 */
																									obj_t BgL_arg1564z00_1628;

																									BgL_arg1564z00_1628 =
																										MAKE_YOUNG_PAIR
																										(BGl_string1757z00zzeffect_walkz00,
																										BNIL);
																									BgL_arg1561z00_1627 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1559z00_1625,
																										BgL_arg1564z00_1628);
																								}
																								BgL_list1560z00_1626 =
																									MAKE_YOUNG_PAIR
																									(BGl_string1759z00zzeffect_walkz00,
																									BgL_arg1561z00_1627);
																							}
																							BGl_displayza2za2zz__r4_output_6_10_3z00
																								(BgL_list1560z00_1626);
																						}
																					}
																				{	/* Effect/walk.scm 94 */
																					obj_t BgL_port1255z00_1630;

																					{	/* Effect/walk.scm 94 */
																						obj_t BgL_tmpz00_2423;

																						BgL_tmpz00_2423 =
																							BGL_CURRENT_DYNAMIC_ENV();
																						BgL_port1255z00_1630 =
																							BGL_ENV_CURRENT_OUTPUT_PORT
																							(BgL_tmpz00_2423);
																					}
																					bgl_display_string
																						(BGl_string1760z00zzeffect_walkz00,
																						BgL_port1255z00_1630);
																					bgl_display_char(((unsigned char) 10),
																						BgL_port1255z00_1630);
																	}}}
																else
																	{	/* Effect/walk.scm 87 */
																		BFALSE;
																	}
															}
														}
														{
															obj_t BgL_l1256z00_2429;

															BgL_l1256z00_2429 = CDR(BgL_l1256z00_1579);
															BgL_l1256z00_1579 = BgL_l1256z00_2429;
															goto BgL_zc3z04anonymousza31376ze3z87_1580;
														}
													}
												else
													{	/* Effect/walk.scm 108 */
														((bool_t) 1);
													}
											}
											{	/* Effect/walk.scm 109 */
												obj_t BgL_arg1421z00_1585;

												{	/* Effect/walk.scm 109 */
													obj_t BgL_tmpz00_2431;

													BgL_tmpz00_2431 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_arg1421z00_1585 =
														BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2431);
												}
												BgL_tmp1119z00_1576 =
													bgl_display_string(BGl_string1763z00zzeffect_walkz00,
													BgL_arg1421z00_1585);
											}
											{	/* Effect/walk.scm 105 */
												bool_t BgL_test1845z00_2435;

												{	/* Effect/walk.scm 105 */
													obj_t BgL_arg1827z00_1971;

													BgL_arg1827z00_1971 =
														BGL_EXITD_PROTECT(BgL_exitd1117z00_1574);
													BgL_test1845z00_2435 = PAIRP(BgL_arg1827z00_1971);
												}
												if (BgL_test1845z00_2435)
													{	/* Effect/walk.scm 105 */
														obj_t BgL_arg1825z00_1972;

														{	/* Effect/walk.scm 105 */
															obj_t BgL_arg1826z00_1973;

															BgL_arg1826z00_1973 =
																BGL_EXITD_PROTECT(BgL_exitd1117z00_1574);
															BgL_arg1825z00_1972 =
																CDR(((obj_t) BgL_arg1826z00_1973));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1117z00_1574,
															BgL_arg1825z00_1972);
														BUNSPEC;
													}
												else
													{	/* Effect/walk.scm 105 */
														BFALSE;
													}
											}
											BGl_z62zc3z04anonymousza31422ze3ze5zzeffect_walkz00
												(BgL_zc3z04anonymousza31422ze3z87_2083);
											return BgL_tmp1119z00_1576;
										}
									}
								}
							else
								{	/* Effect/walk.scm 103 */
									return
										BGl_errorz00zz__errorz00(BGl_string1764z00zzeffect_walkz00,
										BGl_string1765z00zzeffect_walkz00, BgL_onamez00_1571);
								}
						}
					}
				}
			}
		}

	}



/* &write-effect */
	obj_t BGl_z62writezd2effectzb0zzeffect_walkz00(obj_t BgL_envz00_2084,
		obj_t BgL_globalsz00_2085)
	{
		{	/* Effect/walk.scm 84 */
			return BGl_writezd2effectzd2zzeffect_walkz00(BgL_globalsz00_2085);
		}

	}



/* &<@anonymous:1422> */
	obj_t BGl_z62zc3z04anonymousza31422ze3ze5zzeffect_walkz00(obj_t
		BgL_envz00_2086)
	{
		{	/* Effect/walk.scm 105 */
			{	/* Effect/walk.scm 110 */
				obj_t BgL_portz00_2087;

				BgL_portz00_2087 = PROCEDURE_REF(BgL_envz00_2086, (int) (0L));
				{	/* Effect/walk.scm 110 */
					bool_t BgL_test1846z00_2447;

					{	/* Effect/walk.scm 110 */
						obj_t BgL_arg1437z00_2099;

						{	/* Effect/walk.scm 110 */
							obj_t BgL_tmpz00_2448;

							BgL_tmpz00_2448 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1437z00_2099 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2448);
						}
						BgL_test1846z00_2447 = (BgL_portz00_2087 == BgL_arg1437z00_2099);
					}
					if (BgL_test1846z00_2447)
						{	/* Effect/walk.scm 110 */
							return BFALSE;
						}
					else
						{	/* Effect/walk.scm 110 */
							return bgl_close_output_port(((obj_t) BgL_portz00_2087));
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzeffect_walkz00(void)
	{
		{	/* Effect/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzeffect_walkz00(void)
	{
		{	/* Effect/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzeffect_walkz00(void)
	{
		{	/* Effect/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzeffect_walkz00(void)
	{
		{	/* Effect/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zzeffect_cgraphz00(271385047L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zzeffect_spreadz00(348216764L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			BGl_modulezd2initializa7ationz75zzeffect_feffectz00(516374368L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1766z00zzeffect_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
