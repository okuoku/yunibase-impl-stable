/*===========================================================================*/
/*   (Effect/effect.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Effect/effect.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EFFECT_EFFECT_TYPE_DEFINITIONS
#define BGL_EFFECT_EFFECT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;


#endif													// BGL_EFFECT_EFFECT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62sidezd2effectzf3zd2node1226z91zzeffect_effectz00(obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	extern obj_t BGl_nodezf2effectzf2zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzeffect_effectz00 = BUNSPEC;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62sidezd2effectzf3zd2patch1228z91zzeffect_effectz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzeffect_effectz00(void);
	static obj_t BGl_z62sidezd2effectzf3z43zzeffect_effectz00(obj_t, obj_t);
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzeffect_effectz00(void);
	static obj_t BGl_objectzd2initzd2zzeffect_effectz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	static obj_t
		BGl_z62sidezd2effectzf3zd2nodezf2ef1232z63zzeffect_effectz00(obj_t, obj_t);
	static obj_t BGl_z62sidezd2effectzf3zd2setq1236z91zzeffect_effectz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62sidezd2effectzf3zd2jumpzd2ex1242z43zzeffect_effectz00(obj_t, obj_t);
	static obj_t BGl_z62sidezd2effectzf3zd2cast1250z91zzeffect_effectz00(obj_t,
		obj_t);
	static obj_t BGl_z62sidezd2effectzf3zd2fail1238z91zzeffect_effectz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzeffect_effectz00(void);
	static obj_t BGl_z62sidezd2effectzf3zd2sync1252z91zzeffect_effectz00(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62sidezd2effectzf3zd2var1230z91zzeffect_effectz00(obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzeffect_effectz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzeffect_effectz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzeffect_effectz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzeffect_effectz00(void);
	static obj_t
		BGl_z62sidezd2effectzf3zd2appzd2ly1246z43zzeffect_effectz00(obj_t, obj_t);
	static obj_t
		BGl_z62sidezd2effectzf3zd2setzd2exzd21240z91zzeffect_effectz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	extern obj_t BGl_retblockz00zzast_nodez00;
	static obj_t BGl_z62sidezd2effectzf31223z43zzeffect_effectz00(obj_t, obj_t);
	static obj_t BGl_z62sidezd2effectzf3zd2return1256z91zzeffect_effectz00(obj_t,
		obj_t);
	static obj_t BGl_z62sidezd2effectzf3zd2retbloc1254z91zzeffect_effectz00(obj_t,
		obj_t);
	static obj_t BGl_z62sidezd2effectzf3zd2app1234z91zzeffect_effectz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62sidezd2effectzf3zd2boxzd2set1244z43zzeffect_effectz00(obj_t, obj_t);
	static obj_t BGl_z62sidezd2effectzf3zd2funcall1248z91zzeffect_effectz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_patchz00zzast_nodez00;
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1523z00zzeffect_effectz00,
		BgL_bgl_string1523za700za7za7e1545za7, "side-effect?1223", 16);
	      DEFINE_STRING(BGl_string1524z00zzeffect_effectz00,
		BgL_bgl_string1524za700za7za7e1546za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1526z00zzeffect_effectz00,
		BgL_bgl_string1526za700za7za7e1547za7, "side-effect?", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1522z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1548z00,
		BGl_z62sidezd2effectzf31223z43zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1525z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1549z00,
		BGl_z62sidezd2effectzf3zd2node1226z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1527z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1550z00,
		BGl_z62sidezd2effectzf3zd2patch1228z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1528z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1551z00,
		BGl_z62sidezd2effectzf3zd2var1230z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1529z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1552z00,
		BGl_z62sidezd2effectzf3zd2nodezf2ef1232z63zzeffect_effectz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1530z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1553z00,
		BGl_z62sidezd2effectzf3zd2app1234z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1531z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1554z00,
		BGl_z62sidezd2effectzf3zd2setq1236z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1532z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1555z00,
		BGl_z62sidezd2effectzf3zd2fail1238z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1533z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1556z00,
		BGl_z62sidezd2effectzf3zd2setzd2exzd21240z91zzeffect_effectz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1534z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1557z00,
		BGl_z62sidezd2effectzf3zd2jumpzd2ex1242z43zzeffect_effectz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1535z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1558z00,
		BGl_z62sidezd2effectzf3zd2boxzd2set1244z43zzeffect_effectz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1542z00zzeffect_effectz00,
		BgL_bgl_string1542za700za7za7e1559za7, "effect_effect", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1536z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1560z00,
		BGl_z62sidezd2effectzf3zd2appzd2ly1246z43zzeffect_effectz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1543z00zzeffect_effectz00,
		BgL_bgl_string1543za700za7za7e1561za7, "read side-effect?1223 ", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1537z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1562z00,
		BGl_z62sidezd2effectzf3zd2funcall1248z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1538z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1563z00,
		BGl_z62sidezd2effectzf3zd2cast1250z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1539z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1564z00,
		BGl_z62sidezd2effectzf3zd2sync1252z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1540z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1565z00,
		BGl_z62sidezd2effectzf3zd2retbloc1254z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1541z00zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1566z00,
		BGl_z62sidezd2effectzf3zd2return1256z91zzeffect_effectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
		BgL_bgl_za762sideza7d2effect1567z00,
		BGl_z62sidezd2effectzf3z43zzeffect_effectz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzeffect_effectz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long
		BgL_checksumz00_1740, char *BgL_fromz00_1741)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzeffect_effectz00))
				{
					BGl_requirezd2initializa7ationz75zzeffect_effectz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzeffect_effectz00();
					BGl_libraryzd2moduleszd2initz00zzeffect_effectz00();
					BGl_cnstzd2initzd2zzeffect_effectz00();
					BGl_importedzd2moduleszd2initz00zzeffect_effectz00();
					BGl_genericzd2initzd2zzeffect_effectz00();
					BGl_methodzd2initzd2zzeffect_effectz00();
					return BGl_toplevelzd2initzd2zzeffect_effectz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzeffect_effectz00(void)
	{
		{	/* Effect/effect.scm 19 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "effect_effect");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "effect_effect");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"effect_effect");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "effect_effect");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"effect_effect");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L,
				"effect_effect");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"effect_effect");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "effect_effect");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"effect_effect");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzeffect_effectz00(void)
	{
		{	/* Effect/effect.scm 19 */
			{	/* Effect/effect.scm 19 */
				obj_t BgL_cportz00_1708;

				{	/* Effect/effect.scm 19 */
					obj_t BgL_stringz00_1715;

					BgL_stringz00_1715 = BGl_string1543z00zzeffect_effectz00;
					{	/* Effect/effect.scm 19 */
						obj_t BgL_startz00_1716;

						BgL_startz00_1716 = BINT(0L);
						{	/* Effect/effect.scm 19 */
							obj_t BgL_endz00_1717;

							BgL_endz00_1717 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1715)));
							{	/* Effect/effect.scm 19 */

								BgL_cportz00_1708 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1715, BgL_startz00_1716, BgL_endz00_1717);
				}}}}
				{
					long BgL_iz00_1709;

					BgL_iz00_1709 = 1L;
				BgL_loopz00_1710:
					if ((BgL_iz00_1709 == -1L))
						{	/* Effect/effect.scm 19 */
							return BUNSPEC;
						}
					else
						{	/* Effect/effect.scm 19 */
							{	/* Effect/effect.scm 19 */
								obj_t BgL_arg1544z00_1711;

								{	/* Effect/effect.scm 19 */

									{	/* Effect/effect.scm 19 */
										obj_t BgL_locationz00_1713;

										BgL_locationz00_1713 = BBOOL(((bool_t) 0));
										{	/* Effect/effect.scm 19 */

											BgL_arg1544z00_1711 =
												BGl_readz00zz__readerz00(BgL_cportz00_1708,
												BgL_locationz00_1713);
										}
									}
								}
								{	/* Effect/effect.scm 19 */
									int BgL_tmpz00_1770;

									BgL_tmpz00_1770 = (int) (BgL_iz00_1709);
									CNST_TABLE_SET(BgL_tmpz00_1770, BgL_arg1544z00_1711);
							}}
							{	/* Effect/effect.scm 19 */
								int BgL_auxz00_1714;

								BgL_auxz00_1714 = (int) ((BgL_iz00_1709 - 1L));
								{
									long BgL_iz00_1775;

									BgL_iz00_1775 = (long) (BgL_auxz00_1714);
									BgL_iz00_1709 = BgL_iz00_1775;
									goto BgL_loopz00_1710;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzeffect_effectz00(void)
	{
		{	/* Effect/effect.scm 19 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzeffect_effectz00(void)
	{
		{	/* Effect/effect.scm 19 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzeffect_effectz00(void)
	{
		{	/* Effect/effect.scm 19 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzeffect_effectz00(void)
	{
		{	/* Effect/effect.scm 19 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_proc1522z00zzeffect_effectz00, BGl_nodez00zzast_nodez00,
				BGl_string1523z00zzeffect_effectz00);
		}

	}



/* &side-effect?1223 */
	obj_t BGl_z62sidezd2effectzf31223z43zzeffect_effectz00(obj_t BgL_envz00_1656,
		obj_t BgL_nodez00_1657)
	{
		{	/* Effect/effect.scm 26 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string1524z00zzeffect_effectz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_1657)));
		}

	}



/* side-effect? */
	BGL_EXPORTED_DEF bool_t
		BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt BgL_nodez00_3)
	{
		{	/* Effect/effect.scm 26 */
			{	/* Effect/effect.scm 26 */
				obj_t BgL_method1224z00_1373;

				{	/* Effect/effect.scm 26 */
					obj_t BgL_res1521z00_1644;

					{	/* Effect/effect.scm 26 */
						long BgL_objzd2classzd2numz00_1615;

						BgL_objzd2classzd2numz00_1615 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_3));
						{	/* Effect/effect.scm 26 */
							obj_t BgL_arg1811z00_1616;

							BgL_arg1811z00_1616 =
								PROCEDURE_REF(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
								(int) (1L));
							{	/* Effect/effect.scm 26 */
								int BgL_offsetz00_1619;

								BgL_offsetz00_1619 = (int) (BgL_objzd2classzd2numz00_1615);
								{	/* Effect/effect.scm 26 */
									long BgL_offsetz00_1620;

									BgL_offsetz00_1620 =
										((long) (BgL_offsetz00_1619) - OBJECT_TYPE);
									{	/* Effect/effect.scm 26 */
										long BgL_modz00_1621;

										BgL_modz00_1621 =
											(BgL_offsetz00_1620 >> (int) ((long) ((int) (4L))));
										{	/* Effect/effect.scm 26 */
											long BgL_restz00_1623;

											BgL_restz00_1623 =
												(BgL_offsetz00_1620 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Effect/effect.scm 26 */

												{	/* Effect/effect.scm 26 */
													obj_t BgL_bucketz00_1625;

													BgL_bucketz00_1625 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1616), BgL_modz00_1621);
													BgL_res1521z00_1644 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1625), BgL_restz00_1623);
					}}}}}}}}
					BgL_method1224z00_1373 = BgL_res1521z00_1644;
				}
				return
					CBOOL(BGL_PROCEDURE_CALL1(BgL_method1224z00_1373,
						((obj_t) BgL_nodez00_3)));
			}
		}

	}



/* &side-effect? */
	obj_t BGl_z62sidezd2effectzf3z43zzeffect_effectz00(obj_t BgL_envz00_1658,
		obj_t BgL_nodez00_1659)
	{
		{	/* Effect/effect.scm 26 */
			return
				BBOOL(BGl_sidezd2effectzf3z21zzeffect_effectz00(
					((BgL_nodez00_bglt) BgL_nodez00_1659)));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzeffect_effectz00(void)
	{
		{	/* Effect/effect.scm 19 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_nodez00zzast_nodez00, BGl_proc1525z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_patchz00zzast_nodez00, BGl_proc1527z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_varz00zzast_nodez00, BGl_proc1528z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_nodezf2effectzf2zzast_nodez00, BGl_proc1529z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_appz00zzast_nodez00, BGl_proc1530z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_setqz00zzast_nodez00, BGl_proc1531z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_failz00zzast_nodez00, BGl_proc1532z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1533z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1534z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1535z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1536z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1537z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_castz00zzast_nodez00, BGl_proc1538z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_syncz00zzast_nodez00, BGl_proc1539z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_retblockz00zzast_nodez00, BGl_proc1540z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
				BGl_returnz00zzast_nodez00, BGl_proc1541z00zzeffect_effectz00,
				BGl_string1526z00zzeffect_effectz00);
		}

	}



/* &side-effect?-return1256 */
	obj_t BGl_z62sidezd2effectzf3zd2return1256z91zzeffect_effectz00(obj_t
		BgL_envz00_1676, obj_t BgL_nodez00_1677)
	{
		{	/* Effect/effect.scm 129 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-retbloc1254 */
	obj_t BGl_z62sidezd2effectzf3zd2retbloc1254z91zzeffect_effectz00(obj_t
		BgL_envz00_1678, obj_t BgL_nodez00_1679)
	{
		{	/* Effect/effect.scm 122 */
			return
				BBOOL(BGl_sidezd2effectzf3z21zzeffect_effectz00(
					(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_1679)))->BgL_bodyz00)));
		}

	}



/* &side-effect?-sync1252 */
	obj_t BGl_z62sidezd2effectzf3zd2sync1252z91zzeffect_effectz00(obj_t
		BgL_envz00_1680, obj_t BgL_nodez00_1681)
	{
		{	/* Effect/effect.scm 116 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-cast1250 */
	obj_t BGl_z62sidezd2effectzf3zd2cast1250z91zzeffect_effectz00(obj_t
		BgL_envz00_1682, obj_t BgL_nodez00_1683)
	{
		{	/* Effect/effect.scm 109 */
			return
				BBOOL(BGl_sidezd2effectzf3z21zzeffect_effectz00(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_1683)))->BgL_argz00)));
		}

	}



/* &side-effect?-funcall1248 */
	obj_t BGl_z62sidezd2effectzf3zd2funcall1248z91zzeffect_effectz00(obj_t
		BgL_envz00_1684, obj_t BgL_nodez00_1685)
	{
		{	/* Effect/effect.scm 103 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-app-ly1246 */
	obj_t BGl_z62sidezd2effectzf3zd2appzd2ly1246z43zzeffect_effectz00(obj_t
		BgL_envz00_1686, obj_t BgL_nodez00_1687)
	{
		{	/* Effect/effect.scm 97 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-box-set1244 */
	obj_t BGl_z62sidezd2effectzf3zd2boxzd2set1244z43zzeffect_effectz00(obj_t
		BgL_envz00_1688, obj_t BgL_nodez00_1689)
	{
		{	/* Effect/effect.scm 91 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-jump-ex1242 */
	obj_t BGl_z62sidezd2effectzf3zd2jumpzd2ex1242z43zzeffect_effectz00(obj_t
		BgL_envz00_1690, obj_t BgL_nodez00_1691)
	{
		{	/* Effect/effect.scm 85 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-set-ex-1240 */
	obj_t BGl_z62sidezd2effectzf3zd2setzd2exzd21240z91zzeffect_effectz00(obj_t
		BgL_envz00_1692, obj_t BgL_nodez00_1693)
	{
		{	/* Effect/effect.scm 79 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-fail1238 */
	obj_t BGl_z62sidezd2effectzf3zd2fail1238z91zzeffect_effectz00(obj_t
		BgL_envz00_1694, obj_t BgL_nodez00_1695)
	{
		{	/* Effect/effect.scm 73 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-setq1236 */
	obj_t BGl_z62sidezd2effectzf3zd2setq1236z91zzeffect_effectz00(obj_t
		BgL_envz00_1696, obj_t BgL_nodez00_1697)
	{
		{	/* Effect/effect.scm 67 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-app1234 */
	obj_t BGl_z62sidezd2effectzf3zd2app1234z91zzeffect_effectz00(obj_t
		BgL_envz00_1698, obj_t BgL_nodez00_1699)
	{
		{	/* Effect/effect.scm 58 */
			{

				{	/* Effect/effect.scm 59 */
					obj_t BgL__ortest_1102z00_1734;

					{	/* Effect/effect.scm 58 */
						obj_t BgL_nextzd2method1233zd2_1733;

						BgL_nextzd2method1233zd2_1733 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_1699)),
							BGl_sidezd2effectzf3zd2envzf3zzeffect_effectz00,
							BGl_appz00zzast_nodez00);
						BgL__ortest_1102z00_1734 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1233zd2_1733,
							((obj_t) ((BgL_appz00_bglt) BgL_nodez00_1699)));
					}
					if (CBOOL(BgL__ortest_1102z00_1734))
						{	/* Effect/effect.scm 59 */
							return BgL__ortest_1102z00_1734;
						}
					else
						{	/* Effect/effect.scm 59 */
							return
								BBOOL(
								((((BgL_funz00_bglt) COBJECT(
												((BgL_funz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																(((BgL_varz00_bglt) COBJECT(
																			(((BgL_appz00_bglt) COBJECT(
																						((BgL_appz00_bglt)
																							BgL_nodez00_1699)))->
																				BgL_funz00)))->BgL_variablez00)))->
														BgL_valuez00))))->BgL_sidezd2effectzd2) == BTRUE));
						}
				}
			}
		}

	}



/* &side-effect?-node/ef1232 */
	obj_t BGl_z62sidezd2effectzf3zd2nodezf2ef1232z63zzeffect_effectz00(obj_t
		BgL_envz00_1700, obj_t BgL_nodez00_1701)
	{
		{	/* Effect/effect.scm 49 */
			{	/* Effect/effect.scm 50 */
				obj_t BgL_effectz00_1736;

				BgL_effectz00_1736 =
					(((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_nodez00_1701)))->
					BgL_sidezd2effectzd2);
				if (BOOLEANP(BgL_effectz00_1736))
					{	/* Effect/effect.scm 51 */
						return BgL_effectz00_1736;
					}
				else
					{	/* Effect/effect.scm 51 */
						return BTRUE;
					}
			}
		}

	}



/* &side-effect?-var1230 */
	obj_t BGl_z62sidezd2effectzf3zd2var1230z91zzeffect_effectz00(obj_t
		BgL_envz00_1702, obj_t BgL_nodez00_1703)
	{
		{	/* Effect/effect.scm 43 */
			{	/* Effect/effect.scm 44 */
				bool_t BgL_tmpz00_1873;

				if (
					((((BgL_variablez00_bglt) COBJECT(
									(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_1703)))->
										BgL_variablez00)))->BgL_accessz00) == CNST_TABLE_REF(1)))
					{	/* Effect/effect.scm 44 */
						BgL_tmpz00_1873 = ((bool_t) 0);
					}
				else
					{	/* Effect/effect.scm 44 */
						BgL_tmpz00_1873 = ((bool_t) 1);
					}
				return BBOOL(BgL_tmpz00_1873);
			}
		}

	}



/* &side-effect?-patch1228 */
	obj_t BGl_z62sidezd2effectzf3zd2patch1228z91zzeffect_effectz00(obj_t
		BgL_envz00_1704, obj_t BgL_nodez00_1705)
	{
		{	/* Effect/effect.scm 37 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &side-effect?-node1226 */
	obj_t BGl_z62sidezd2effectzf3zd2node1226z91zzeffect_effectz00(obj_t
		BgL_envz00_1706, obj_t BgL_nodez00_1707)
	{
		{	/* Effect/effect.scm 31 */
			return BBOOL(((bool_t) 0));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzeffect_effectz00(void)
	{
		{	/* Effect/effect.scm 19 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1542z00zzeffect_effectz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1542z00zzeffect_effectz00));
			return
				BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1542z00zzeffect_effectz00));
		}

	}

#ifdef __cplusplus
}
#endif
