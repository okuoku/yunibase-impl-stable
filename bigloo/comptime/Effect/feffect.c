/*===========================================================================*/
/*   (Effect/feffect.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Effect/feffect.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EFFECT_FEFFECT_TYPE_DEFINITIONS
#define BGL_EFFECT_FEFFECT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_feffectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_readz00;
		obj_t BgL_writez00;
	}                 *BgL_feffectz00_bglt;

	typedef struct BgL_localzf2fromzf2_bgl
	{
		obj_t BgL_fromz00;
	}                      *BgL_localzf2fromzf2_bglt;

	typedef struct BgL_globalzf2fromzf2_bgl
	{
		obj_t BgL_fromz00;
	}                       *BgL_globalzf2fromzf2_bglt;


#endif													// BGL_EFFECT_FEFFECT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static BgL_feffectz00_bglt BGl_funzd2effectz12zc0zzeffect_feffectz00(obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzeffect_feffectz00 = BUNSPEC;
	static BgL_feffectz00_bglt
		BGl_za2effectzd2writezd2memza2z00zzeffect_feffectz00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static BgL_feffectz00_bglt BGl_za2effectzd2topza2zd2zzeffect_feffectz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2sequenc1319z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzeffect_feffectz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2atom1291z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62feffectz12z70zzeffect_feffectz00(obj_t, obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2makezd2bo1311za2zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_mergez12ze70zf5zzeffect_feffectz00(obj_t, obj_t, obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12za2zzeffect_feffectz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzeffect_feffectz00(void);
	static obj_t BGl_objectzd2initzd2zzeffect_feffectz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static bool_t BGl_feffectzd2fixzd2pointz12z12zzeffect_feffectz00(obj_t);
	BGL_IMPORT obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2cast1317z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bodyzd2effectz121288za2zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzeffect_feffectz00(void);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2jumpzd2ex1309za2zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2fail1305z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static BgL_feffectz00_bglt BGl_z62parsezd2effectzb0zzeffect_feffectz00(obj_t,
		obj_t);
	static BgL_feffectz00_bglt
		BGl_funzd2effectzd2modulez12z12zzeffect_feffectz00(BgL_globalz00_bglt);
	static BgL_feffectz00_bglt
		BGl_dozd2funzd2effectz12z12zzeffect_feffectz00(BgL_variablez00_bglt);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_feffectz12z12zzeffect_feffectz00(obj_t);
	static obj_t BGl_mergezd2callerz12ze70z27zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2sync1321z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_bodyzd2effectza2z12z62zzeffect_feffectz00(obj_t, obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2appzd2ly1325za2zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2switch1303z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2setq1299z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static BgL_feffectz00_bglt
		BGl_za2effectzd2readzd2memza2z00zzeffect_feffectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzeffect_feffectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_cgraphz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2letzd2fun1331za2zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2app1327z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31631ze3ze5zzeffect_feffectz00(obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2var1297z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2extern1329z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localzf2fromzf2zzeffect_cgraphz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	extern obj_t BGl_feffectz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzeffect_feffectz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzeffect_feffectz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzeffect_feffectz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzeffect_feffectz00(void);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2kwote1295z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62objectzd2displayzd2feffe1287z62zzeffect_feffectz00(obj_t,
		obj_t, obj_t);
	static BgL_feffectz00_bglt
		BGl_bodyzd2effectz12zc0zzeffect_feffectz00(BgL_nodez00_bglt,
		BgL_feffectz00_bglt);
	static BgL_feffectz00_bglt
		BGl_funzd2effectzd2alienz12z12zzeffect_feffectz00(BgL_globalz00_bglt);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2patch1293z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2setzd2exzd21307z70zzeffect_feffectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static bool_t BGl_mergezd2effectsz12zc0zzeffect_feffectz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2conditi1301z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_globalzf2fromzf2zzeffect_cgraphz00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2boxzd2set1315za2zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_printfz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static BgL_feffectz00_bglt
		BGl_funzd2effectzd2globalz12z12zzeffect_feffectz00(BgL_globalz00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2funcall1323z70zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_EXPORTED_DECL BgL_feffectz00_bglt
		BGl_parsezd2effectzd2zzeffect_feffectz00(obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2boxzd2ref1313za2zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	static BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2letzd2var1333za2zzeffect_feffectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_patchz00zzast_nodez00;
	static obj_t __cnst[6];


	BGL_IMPORT obj_t BGl_objectzd2displayzd2envz00zz__objectz00;
	   
		 
		DEFINE_STRING(BGl_string1962z00zzeffect_feffectz00,
		BgL_bgl_string1962za700za7za7e1996za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1963z00zzeffect_feffectz00,
		BgL_bgl_string1963za700za7za7e1997za7, "Illegal `effect' pragma", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_parsezd2effectzd2envz00zzeffect_feffectz00,
		BgL_bgl_za762parseza7d2effec1998z00,
		BGl_z62parsezd2effectzb0zzeffect_feffectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1965z00zzeffect_feffectz00,
		BgL_bgl_string1965za700za7za7e1999za7, "body-effect!1288", 16);
	      DEFINE_STRING(BGl_string1966z00zzeffect_feffectz00,
		BgL_bgl_string1966za700za7za7e2000za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1968z00zzeffect_feffectz00,
		BgL_bgl_string1968za700za7za7e2001za7, "object-display", 14);
	      DEFINE_STRING(BGl_string1970z00zzeffect_feffectz00,
		BgL_bgl_string1970za700za7za7e2002za7, "body-effect!", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1964z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2003z00,
		BGl_z62bodyzd2effectz121288za2zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1967z00zzeffect_feffectz00,
		BgL_bgl_za762objectza7d2disp2004z00, va_generic_entry,
		BGl_z62objectzd2displayzd2feffe1287z62zzeffect_feffectz00, BUNSPEC, -2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1969z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2005z00,
		BGl_z62bodyzd2effectz12zd2atom1291z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1971z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2006z00,
		BGl_z62bodyzd2effectz12zd2patch1293z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1972z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2007z00,
		BGl_z62bodyzd2effectz12zd2kwote1295z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1973z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2008z00,
		BGl_z62bodyzd2effectz12zd2var1297z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1974z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2009z00,
		BGl_z62bodyzd2effectz12zd2setq1299z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1975z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2010z00,
		BGl_z62bodyzd2effectz12zd2conditi1301z70zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1976z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2011z00,
		BGl_z62bodyzd2effectz12zd2switch1303z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1977z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2012z00,
		BGl_z62bodyzd2effectz12zd2fail1305z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1978z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2013z00,
		BGl_z62bodyzd2effectz12zd2setzd2exzd21307z70zzeffect_feffectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1979z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2014z00,
		BGl_z62bodyzd2effectz12zd2jumpzd2ex1309za2zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1980z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2015z00,
		BGl_z62bodyzd2effectz12zd2makezd2bo1311za2zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1981z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2016z00,
		BGl_z62bodyzd2effectz12zd2boxzd2ref1313za2zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1982z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2017z00,
		BGl_z62bodyzd2effectz12zd2boxzd2set1315za2zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1983z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2018z00,
		BGl_z62bodyzd2effectz12zd2cast1317z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1984z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2019z00,
		BGl_z62bodyzd2effectz12zd2sequenc1319z70zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1985z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2020z00,
		BGl_z62bodyzd2effectz12zd2sync1321z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1992z00zzeffect_feffectz00,
		BgL_bgl_string1992za700za7za7e2021za7, "#|feffect: ~a ~a|", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1986z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2022z00,
		BGl_z62bodyzd2effectz12zd2funcall1323z70zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1993z00zzeffect_feffectz00,
		BgL_bgl_string1993za700za7za7e2023za7, "effect_feffect", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1987z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2024z00,
		BGl_z62bodyzd2effectz12zd2appzd2ly1325za2zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1994z00zzeffect_feffectz00,
		BgL_bgl_string1994za700za7za7e2025za7,
		"body-effect!1288 (static export) write read (memory) top ", 57);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1988z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2026z00,
		BGl_z62bodyzd2effectz12zd2app1327z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1989z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2027z00,
		BGl_z62bodyzd2effectz12zd2extern1329z70zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1990z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2028z00,
		BGl_z62bodyzd2effectz12zd2letzd2fun1331za2zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1991z00zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2029z00,
		BGl_z62bodyzd2effectz12zd2letzd2var1333za2zzeffect_feffectz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
		BgL_bgl_za762bodyza7d2effect2030z00,
		BGl_z62bodyzd2effectz12za2zzeffect_feffectz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_feffectz12zd2envzc0zzeffect_feffectz00,
		BgL_bgl_za762feffectza712za7702031za7,
		BGl_z62feffectz12z70zzeffect_feffectz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzeffect_feffectz00));
		     ADD_ROOT((void
				*) (&BGl_za2effectzd2writezd2memza2z00zzeffect_feffectz00));
		     ADD_ROOT((void *) (&BGl_za2effectzd2topza2zd2zzeffect_feffectz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2effectzd2readzd2memza2z00zzeffect_feffectz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzeffect_feffectz00(long
		BgL_checksumz00_3043, char *BgL_fromz00_3044)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzeffect_feffectz00))
				{
					BGl_requirezd2initializa7ationz75zzeffect_feffectz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzeffect_feffectz00();
					BGl_libraryzd2moduleszd2initz00zzeffect_feffectz00();
					BGl_cnstzd2initzd2zzeffect_feffectz00();
					BGl_importedzd2moduleszd2initz00zzeffect_feffectz00();
					BGl_genericzd2initzd2zzeffect_feffectz00();
					BGl_methodzd2initzd2zzeffect_feffectz00();
					return BGl_toplevelzd2initzd2zzeffect_feffectz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzeffect_feffectz00(void)
	{
		{	/* Effect/feffect.scm 16 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"effect_feffect");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "effect_feffect");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"effect_feffect");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "effect_feffect");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"effect_feffect");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "effect_feffect");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"effect_feffect");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"effect_feffect");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "effect_feffect");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"effect_feffect");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"effect_feffect");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"effect_feffect");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzeffect_feffectz00(void)
	{
		{	/* Effect/feffect.scm 16 */
			{	/* Effect/feffect.scm 16 */
				obj_t BgL_cportz00_2915;

				{	/* Effect/feffect.scm 16 */
					obj_t BgL_stringz00_2922;

					BgL_stringz00_2922 = BGl_string1994z00zzeffect_feffectz00;
					{	/* Effect/feffect.scm 16 */
						obj_t BgL_startz00_2923;

						BgL_startz00_2923 = BINT(0L);
						{	/* Effect/feffect.scm 16 */
							obj_t BgL_endz00_2924;

							BgL_endz00_2924 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2922)));
							{	/* Effect/feffect.scm 16 */

								BgL_cportz00_2915 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2922, BgL_startz00_2923, BgL_endz00_2924);
				}}}}
				{
					long BgL_iz00_2916;

					BgL_iz00_2916 = 5L;
				BgL_loopz00_2917:
					if ((BgL_iz00_2916 == -1L))
						{	/* Effect/feffect.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Effect/feffect.scm 16 */
							{	/* Effect/feffect.scm 16 */
								obj_t BgL_arg1995z00_2918;

								{	/* Effect/feffect.scm 16 */

									{	/* Effect/feffect.scm 16 */
										obj_t BgL_locationz00_2920;

										BgL_locationz00_2920 = BBOOL(((bool_t) 0));
										{	/* Effect/feffect.scm 16 */

											BgL_arg1995z00_2918 =
												BGl_readz00zz__readerz00(BgL_cportz00_2915,
												BgL_locationz00_2920);
										}
									}
								}
								{	/* Effect/feffect.scm 16 */
									int BgL_tmpz00_3076;

									BgL_tmpz00_3076 = (int) (BgL_iz00_2916);
									CNST_TABLE_SET(BgL_tmpz00_3076, BgL_arg1995z00_2918);
							}}
							{	/* Effect/feffect.scm 16 */
								int BgL_auxz00_2921;

								BgL_auxz00_2921 = (int) ((BgL_iz00_2916 - 1L));
								{
									long BgL_iz00_3081;

									BgL_iz00_3081 = (long) (BgL_auxz00_2921);
									BgL_iz00_2916 = BgL_iz00_3081;
									goto BgL_loopz00_2917;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzeffect_feffectz00(void)
	{
		{	/* Effect/feffect.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzeffect_feffectz00(void)
	{
		{	/* Effect/feffect.scm 16 */
			{	/* Effect/feffect.scm 72 */
				BgL_feffectz00_bglt BgL_new1111z00_1533;

				{	/* Effect/feffect.scm 72 */
					BgL_feffectz00_bglt BgL_new1110z00_1534;

					BgL_new1110z00_1534 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Effect/feffect.scm 72 */
						long BgL_arg1335z00_1535;

						BgL_arg1335z00_1535 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1110z00_1534), BgL_arg1335z00_1535);
					}
					BgL_new1111z00_1533 = BgL_new1110z00_1534;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1111z00_1533))->BgL_readz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1111z00_1533))->BgL_writez00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				BGl_za2effectzd2topza2zd2zzeffect_feffectz00 = BgL_new1111z00_1533;
			}
			{	/* Effect/feffect.scm 73 */
				BgL_feffectz00_bglt BgL_new1114z00_1536;

				{	/* Effect/feffect.scm 73 */
					BgL_feffectz00_bglt BgL_new1113z00_1537;

					BgL_new1113z00_1537 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Effect/feffect.scm 73 */
						long BgL_arg1339z00_1538;

						BgL_arg1339z00_1538 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1113z00_1537), BgL_arg1339z00_1538);
					}
					BgL_new1114z00_1536 = BgL_new1113z00_1537;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1114z00_1536))->BgL_readz00) =
					((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1114z00_1536))->BgL_writez00) =
					((obj_t) BNIL), BUNSPEC);
				BGl_za2effectzd2readzd2memza2z00zzeffect_feffectz00 =
					BgL_new1114z00_1536;
			}
			{	/* Effect/feffect.scm 74 */
				BgL_feffectz00_bglt BgL_new1116z00_1539;

				{	/* Ast/var.scm 179 */
					BgL_feffectz00_bglt BgL_new1115z00_1540;

					BgL_new1115z00_1540 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Ast/var.scm 179 */
						long BgL_arg1340z00_1541;

						BgL_arg1340z00_1541 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1115z00_1540), BgL_arg1340z00_1541);
					}
					BgL_new1116z00_1539 = BgL_new1115z00_1540;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1116z00_1539))->BgL_readz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1116z00_1539))->BgL_writez00) =
					((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
				BGl_za2effectzd2writezd2memza2z00zzeffect_feffectz00 =
					BgL_new1116z00_1539;
			}
			return BUNSPEC;
		}

	}



/* feffect! */
	BGL_EXPORTED_DEF obj_t BGl_feffectz12z12zzeffect_feffectz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Effect/feffect.scm 32 */
			{
				obj_t BgL_l1259z00_1551;

				BgL_l1259z00_1551 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31344ze3z87_1552:
				if (PAIRP(BgL_l1259z00_1551))
					{	/* Effect/feffect.scm 33 */
						{	/* Effect/feffect.scm 33 */
							obj_t BgL_arg1346z00_1554;

							BgL_arg1346z00_1554 = CAR(BgL_l1259z00_1551);
							BGl_funzd2effectzd2modulez12z12zzeffect_feffectz00(
								((BgL_globalz00_bglt) BgL_arg1346z00_1554));
						}
						{
							obj_t BgL_l1259z00_3111;

							BgL_l1259z00_3111 = CDR(BgL_l1259z00_1551);
							BgL_l1259z00_1551 = BgL_l1259z00_3111;
							goto BgL_zc3z04anonymousza31344ze3z87_1552;
						}
					}
				else
					{	/* Effect/feffect.scm 33 */
						((bool_t) 1);
					}
			}
			return
				BBOOL(BGl_feffectzd2fixzd2pointz12z12zzeffect_feffectz00
				(BgL_globalsz00_3));
		}

	}



/* &feffect! */
	obj_t BGl_z62feffectz12z70zzeffect_feffectz00(obj_t BgL_envz00_2794,
		obj_t BgL_globalsz00_2795)
	{
		{	/* Effect/feffect.scm 32 */
			return BGl_feffectz12z12zzeffect_feffectz00(BgL_globalsz00_2795);
		}

	}



/* feffect-fix-point! */
	bool_t BGl_feffectzd2fixzd2pointz12z12zzeffect_feffectz00(obj_t
		BgL_globalsz00_4)
	{
		{	/* Effect/feffect.scm 39 */
			{	/* Effect/feffect.scm 40 */
				struct bgl_cell BgL_box2035_2912z00;
				obj_t BgL_changedz00_2912;

				BgL_changedz00_2912 = MAKE_CELL_STACK(BTRUE, BgL_box2035_2912z00);
				{
					obj_t BgL_varz00_1569;

					{

					BgL_zc3z04anonymousza31349ze3z87_1560:
						if (CBOOL(((obj_t) ((obj_t) CELL_REF(BgL_changedz00_2912)))))
							{	/* Effect/feffect.scm 54 */
								{	/* Effect/feffect.scm 56 */
									obj_t BgL_auxz00_2913;

									BgL_auxz00_2913 = BFALSE;
									CELL_SET(BgL_changedz00_2912, BgL_auxz00_2913);
								}
								{
									obj_t BgL_l1267z00_1562;

									BgL_l1267z00_1562 = BgL_globalsz00_4;
								BgL_zc3z04anonymousza31350ze3z87_1563:
									if (PAIRP(BgL_l1267z00_1562))
										{	/* Effect/feffect.scm 57 */
											BgL_varz00_1569 = CAR(BgL_l1267z00_1562);
											{	/* Effect/feffect.scm 47 */

												{	/* Effect/feffect.scm 49 */
													bool_t BgL_test2038z00_3121;

													{	/* Effect/feffect.scm 49 */
														obj_t BgL_classz00_2151;

														BgL_classz00_2151 =
															BGl_localzf2fromzf2zzeffect_cgraphz00;
														if (BGL_OBJECTP(BgL_varz00_1569))
															{	/* Effect/feffect.scm 49 */
																BgL_objectz00_bglt BgL_arg1807z00_2153;

																BgL_arg1807z00_2153 =
																	(BgL_objectz00_bglt) (BgL_varz00_1569);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Effect/feffect.scm 49 */
																		long BgL_idxz00_2159;

																		BgL_idxz00_2159 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2153);
																		BgL_test2038z00_3121 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2159 + 3L)) ==
																			BgL_classz00_2151);
																	}
																else
																	{	/* Effect/feffect.scm 49 */
																		bool_t BgL_res1884z00_2184;

																		{	/* Effect/feffect.scm 49 */
																			obj_t BgL_oclassz00_2167;

																			{	/* Effect/feffect.scm 49 */
																				obj_t BgL_arg1815z00_2175;
																				long BgL_arg1816z00_2176;

																				BgL_arg1815z00_2175 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Effect/feffect.scm 49 */
																					long BgL_arg1817z00_2177;

																					BgL_arg1817z00_2177 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2153);
																					BgL_arg1816z00_2176 =
																						(BgL_arg1817z00_2177 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2167 =
																					VECTOR_REF(BgL_arg1815z00_2175,
																					BgL_arg1816z00_2176);
																			}
																			{	/* Effect/feffect.scm 49 */
																				bool_t BgL__ortest_1115z00_2168;

																				BgL__ortest_1115z00_2168 =
																					(BgL_classz00_2151 ==
																					BgL_oclassz00_2167);
																				if (BgL__ortest_1115z00_2168)
																					{	/* Effect/feffect.scm 49 */
																						BgL_res1884z00_2184 =
																							BgL__ortest_1115z00_2168;
																					}
																				else
																					{	/* Effect/feffect.scm 49 */
																						long BgL_odepthz00_2169;

																						{	/* Effect/feffect.scm 49 */
																							obj_t BgL_arg1804z00_2170;

																							BgL_arg1804z00_2170 =
																								(BgL_oclassz00_2167);
																							BgL_odepthz00_2169 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2170);
																						}
																						if ((3L < BgL_odepthz00_2169))
																							{	/* Effect/feffect.scm 49 */
																								obj_t BgL_arg1802z00_2172;

																								{	/* Effect/feffect.scm 49 */
																									obj_t BgL_arg1803z00_2173;

																									BgL_arg1803z00_2173 =
																										(BgL_oclassz00_2167);
																									BgL_arg1802z00_2172 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2173, 3L);
																								}
																								BgL_res1884z00_2184 =
																									(BgL_arg1802z00_2172 ==
																									BgL_classz00_2151);
																							}
																						else
																							{	/* Effect/feffect.scm 49 */
																								BgL_res1884z00_2184 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2038z00_3121 = BgL_res1884z00_2184;
																	}
															}
														else
															{	/* Effect/feffect.scm 49 */
																BgL_test2038z00_3121 = ((bool_t) 0);
															}
													}
													if (BgL_test2038z00_3121)
														{	/* Effect/feffect.scm 50 */
															obj_t BgL_g1263z00_1574;

															{
																BgL_localzf2fromzf2_bglt BgL_auxz00_3144;

																{
																	obj_t BgL_auxz00_3145;

																	{	/* Effect/feffect.scm 50 */
																		BgL_objectz00_bglt BgL_tmpz00_3146;

																		BgL_tmpz00_3146 =
																			((BgL_objectz00_bglt)
																			((BgL_localz00_bglt) BgL_varz00_1569));
																		BgL_auxz00_3145 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3146);
																	}
																	BgL_auxz00_3144 =
																		((BgL_localzf2fromzf2_bglt)
																		BgL_auxz00_3145);
																}
																BgL_g1263z00_1574 =
																	(((BgL_localzf2fromzf2_bglt)
																		COBJECT(BgL_auxz00_3144))->BgL_fromz00);
															}
															{
																obj_t BgL_l1261z00_1576;

																BgL_l1261z00_1576 = BgL_g1263z00_1574;
															BgL_zc3z04anonymousza31367ze3z87_1577:
																if (PAIRP(BgL_l1261z00_1576))
																	{	/* Effect/feffect.scm 50 */
																		BGl_mergezd2callerz12ze70z27zzeffect_feffectz00
																			(BgL_changedz00_2912, BgL_varz00_1569,
																			CAR(BgL_l1261z00_1576));
																		{
																			obj_t BgL_l1261z00_3156;

																			BgL_l1261z00_3156 =
																				CDR(BgL_l1261z00_1576);
																			BgL_l1261z00_1576 = BgL_l1261z00_3156;
																			goto
																				BgL_zc3z04anonymousza31367ze3z87_1577;
																		}
																	}
																else
																	{	/* Effect/feffect.scm 50 */
																		((bool_t) 1);
																	}
															}
														}
													else
														{	/* Effect/feffect.scm 51 */
															bool_t BgL_test2044z00_3158;

															{	/* Effect/feffect.scm 51 */
																obj_t BgL_classz00_2189;

																BgL_classz00_2189 =
																	BGl_globalzf2fromzf2zzeffect_cgraphz00;
																if (BGL_OBJECTP(BgL_varz00_1569))
																	{	/* Effect/feffect.scm 51 */
																		BgL_objectz00_bglt BgL_arg1807z00_2191;

																		BgL_arg1807z00_2191 =
																			(BgL_objectz00_bglt) (BgL_varz00_1569);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Effect/feffect.scm 51 */
																				long BgL_idxz00_2197;

																				BgL_idxz00_2197 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2191);
																				BgL_test2044z00_3158 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2197 + 3L)) ==
																					BgL_classz00_2189);
																			}
																		else
																			{	/* Effect/feffect.scm 51 */
																				bool_t BgL_res1885z00_2222;

																				{	/* Effect/feffect.scm 51 */
																					obj_t BgL_oclassz00_2205;

																					{	/* Effect/feffect.scm 51 */
																						obj_t BgL_arg1815z00_2213;
																						long BgL_arg1816z00_2214;

																						BgL_arg1815z00_2213 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Effect/feffect.scm 51 */
																							long BgL_arg1817z00_2215;

																							BgL_arg1817z00_2215 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2191);
																							BgL_arg1816z00_2214 =
																								(BgL_arg1817z00_2215 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2205 =
																							VECTOR_REF(BgL_arg1815z00_2213,
																							BgL_arg1816z00_2214);
																					}
																					{	/* Effect/feffect.scm 51 */
																						bool_t BgL__ortest_1115z00_2206;

																						BgL__ortest_1115z00_2206 =
																							(BgL_classz00_2189 ==
																							BgL_oclassz00_2205);
																						if (BgL__ortest_1115z00_2206)
																							{	/* Effect/feffect.scm 51 */
																								BgL_res1885z00_2222 =
																									BgL__ortest_1115z00_2206;
																							}
																						else
																							{	/* Effect/feffect.scm 51 */
																								long BgL_odepthz00_2207;

																								{	/* Effect/feffect.scm 51 */
																									obj_t BgL_arg1804z00_2208;

																									BgL_arg1804z00_2208 =
																										(BgL_oclassz00_2205);
																									BgL_odepthz00_2207 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2208);
																								}
																								if ((3L < BgL_odepthz00_2207))
																									{	/* Effect/feffect.scm 51 */
																										obj_t BgL_arg1802z00_2210;

																										{	/* Effect/feffect.scm 51 */
																											obj_t BgL_arg1803z00_2211;

																											BgL_arg1803z00_2211 =
																												(BgL_oclassz00_2205);
																											BgL_arg1802z00_2210 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2211,
																												3L);
																										}
																										BgL_res1885z00_2222 =
																											(BgL_arg1802z00_2210 ==
																											BgL_classz00_2189);
																									}
																								else
																									{	/* Effect/feffect.scm 51 */
																										BgL_res1885z00_2222 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2044z00_3158 =
																					BgL_res1885z00_2222;
																			}
																	}
																else
																	{	/* Effect/feffect.scm 51 */
																		BgL_test2044z00_3158 = ((bool_t) 0);
																	}
															}
															if (BgL_test2044z00_3158)
																{	/* Effect/feffect.scm 52 */
																	obj_t BgL_g1266z00_1583;

																	{
																		BgL_globalzf2fromzf2_bglt BgL_auxz00_3181;

																		{
																			obj_t BgL_auxz00_3182;

																			{	/* Effect/feffect.scm 52 */
																				BgL_objectz00_bglt BgL_tmpz00_3183;

																				BgL_tmpz00_3183 =
																					((BgL_objectz00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_varz00_1569));
																				BgL_auxz00_3182 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_3183);
																			}
																			BgL_auxz00_3181 =
																				((BgL_globalzf2fromzf2_bglt)
																				BgL_auxz00_3182);
																		}
																		BgL_g1266z00_1583 =
																			(((BgL_globalzf2fromzf2_bglt)
																				COBJECT(BgL_auxz00_3181))->BgL_fromz00);
																	}
																	{
																		obj_t BgL_l1264z00_1585;

																		BgL_l1264z00_1585 = BgL_g1266z00_1583;
																	BgL_zc3z04anonymousza31373ze3z87_1586:
																		if (PAIRP(BgL_l1264z00_1585))
																			{	/* Effect/feffect.scm 52 */
																				BGl_mergezd2callerz12ze70z27zzeffect_feffectz00
																					(BgL_changedz00_2912, BgL_varz00_1569,
																					CAR(BgL_l1264z00_1585));
																				{
																					obj_t BgL_l1264z00_3193;

																					BgL_l1264z00_3193 =
																						CDR(BgL_l1264z00_1585);
																					BgL_l1264z00_1585 = BgL_l1264z00_3193;
																					goto
																						BgL_zc3z04anonymousza31373ze3z87_1586;
																				}
																			}
																		else
																			{	/* Effect/feffect.scm 52 */
																				((bool_t) 1);
																			}
																	}
																}
															else
																{	/* Effect/feffect.scm 51 */
																	((bool_t) 0);
																}
														}
												}
											}
											{
												obj_t BgL_l1267z00_3196;

												BgL_l1267z00_3196 = CDR(BgL_l1267z00_1562);
												BgL_l1267z00_1562 = BgL_l1267z00_3196;
												goto BgL_zc3z04anonymousza31350ze3z87_1563;
											}
										}
									else
										{	/* Effect/feffect.scm 57 */
											((bool_t) 1);
										}
								}
								goto BgL_zc3z04anonymousza31349ze3z87_1560;
							}
						else
							{	/* Effect/feffect.scm 54 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* merge-caller!~0 */
	obj_t BGl_mergezd2callerz12ze70z27zzeffect_feffectz00(obj_t
		BgL_changedz00_2910, obj_t BgL_varz00_2909, obj_t BgL_fz00_1591)
	{
		{	/* Effect/feffect.scm 46 */
			{	/* Effect/feffect.scm 43 */
				obj_t BgL_efz00_1593;
				obj_t BgL_etz00_1594;

				BgL_efz00_1593 =
					(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_fz00_1591)))->
									BgL_valuez00))))->BgL_effectz00);
				BgL_etz00_1594 =
					(((BgL_funz00_bglt)
						COBJECT(((BgL_funz00_bglt) (((BgL_variablez00_bglt)
										COBJECT(((BgL_variablez00_bglt) BgL_varz00_2909)))->
									BgL_valuez00))))->BgL_effectz00);
				{	/* Effect/feffect.scm 45 */
					bool_t BgL_test2050z00_3206;

					{	/* Effect/feffect.scm 45 */
						obj_t BgL_objz00_2115;

						BgL_objz00_2115 = BgL_efz00_1593;
						{	/* Effect/feffect.scm 45 */
							obj_t BgL_classz00_2116;

							BgL_classz00_2116 = BGl_feffectz00zzast_varz00;
							if (BGL_OBJECTP(BgL_objz00_2115))
								{	/* Effect/feffect.scm 45 */
									BgL_objectz00_bglt BgL_arg1807z00_2118;

									BgL_arg1807z00_2118 = (BgL_objectz00_bglt) (BgL_objz00_2115);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Effect/feffect.scm 45 */
											long BgL_idxz00_2124;

											BgL_idxz00_2124 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2118);
											BgL_test2050z00_3206 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2124 + 1L)) == BgL_classz00_2116);
										}
									else
										{	/* Effect/feffect.scm 45 */
											bool_t BgL_res1883z00_2149;

											{	/* Effect/feffect.scm 45 */
												obj_t BgL_oclassz00_2132;

												{	/* Effect/feffect.scm 45 */
													obj_t BgL_arg1815z00_2140;
													long BgL_arg1816z00_2141;

													BgL_arg1815z00_2140 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Effect/feffect.scm 45 */
														long BgL_arg1817z00_2142;

														BgL_arg1817z00_2142 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2118);
														BgL_arg1816z00_2141 =
															(BgL_arg1817z00_2142 - OBJECT_TYPE);
													}
													BgL_oclassz00_2132 =
														VECTOR_REF(BgL_arg1815z00_2140,
														BgL_arg1816z00_2141);
												}
												{	/* Effect/feffect.scm 45 */
													bool_t BgL__ortest_1115z00_2133;

													BgL__ortest_1115z00_2133 =
														(BgL_classz00_2116 == BgL_oclassz00_2132);
													if (BgL__ortest_1115z00_2133)
														{	/* Effect/feffect.scm 45 */
															BgL_res1883z00_2149 = BgL__ortest_1115z00_2133;
														}
													else
														{	/* Effect/feffect.scm 45 */
															long BgL_odepthz00_2134;

															{	/* Effect/feffect.scm 45 */
																obj_t BgL_arg1804z00_2135;

																BgL_arg1804z00_2135 = (BgL_oclassz00_2132);
																BgL_odepthz00_2134 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2135);
															}
															if ((1L < BgL_odepthz00_2134))
																{	/* Effect/feffect.scm 45 */
																	obj_t BgL_arg1802z00_2137;

																	{	/* Effect/feffect.scm 45 */
																		obj_t BgL_arg1803z00_2138;

																		BgL_arg1803z00_2138 = (BgL_oclassz00_2132);
																		BgL_arg1802z00_2137 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2138, 1L);
																	}
																	BgL_res1883z00_2149 =
																		(BgL_arg1802z00_2137 == BgL_classz00_2116);
																}
															else
																{	/* Effect/feffect.scm 45 */
																	BgL_res1883z00_2149 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2050z00_3206 = BgL_res1883z00_2149;
										}
								}
							else
								{	/* Effect/feffect.scm 45 */
									BgL_test2050z00_3206 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2050z00_3206)
						{	/* Effect/feffect.scm 45 */
							BFALSE;
						}
					else
						{	/* Effect/feffect.scm 45 */
							BgL_efz00_1593 =
								((obj_t) BGl_za2effectzd2topza2zd2zzeffect_feffectz00);
						}
				}
				{	/* Effect/feffect.scm 46 */
					obj_t BgL_auxz00_2911;

					{	/* Effect/feffect.scm 46 */
						bool_t BgL__ortest_1108z00_1596;

						BgL__ortest_1108z00_1596 =
							BGl_mergezd2effectsz12zc0zzeffect_feffectz00(BgL_etz00_1594,
							BgL_efz00_1593);
						if (BgL__ortest_1108z00_1596)
							{	/* Effect/feffect.scm 46 */
								BgL_auxz00_2911 = BBOOL(BgL__ortest_1108z00_1596);
							}
						else
							{	/* Effect/feffect.scm 46 */
								BgL_auxz00_2911 =
									((obj_t) ((obj_t) CELL_REF(BgL_changedz00_2910)));
							}
					}
					return CELL_SET(BgL_changedz00_2910, BgL_auxz00_2911);
				}
			}
		}

	}



/* parse-effect */
	BGL_EXPORTED_DEF BgL_feffectz00_bglt
		BGl_parsezd2effectzd2zzeffect_feffectz00(obj_t BgL_propz00_7)
	{
		{	/* Effect/feffect.scm 79 */
			{	/* Effect/feffect.scm 80 */
				BgL_feffectz00_bglt BgL_ez00_1601;

				{	/* Effect/feffect.scm 80 */
					BgL_feffectz00_bglt BgL_new1119z00_1636;

					{	/* Ast/var.scm 179 */
						BgL_feffectz00_bglt BgL_new1118z00_1637;

						BgL_new1118z00_1637 =
							((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_feffectz00_bgl))));
						{	/* Ast/var.scm 179 */
							long BgL_arg1516z00_1638;

							BgL_arg1516z00_1638 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1118z00_1637),
								BgL_arg1516z00_1638);
						}
						BgL_new1119z00_1636 = BgL_new1118z00_1637;
					}
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1119z00_1636))->BgL_readz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1119z00_1636))->
							BgL_writez00) = ((obj_t) BNIL), BUNSPEC);
					BgL_ez00_1601 = BgL_new1119z00_1636;
				}
				{	/* Effect/feffect.scm 81 */
					obj_t BgL_g1120z00_1602;

					BgL_g1120z00_1602 = CDR(((obj_t) BgL_propz00_7));
					{
						obj_t BgL_vz00_1604;

						{
							obj_t BgL_auxz00_3242;

							BgL_vz00_1604 = BgL_g1120z00_1602;
						BgL_zc3z04anonymousza31423ze3z87_1605:
							if (NULLP(BgL_vz00_1604))
								{	/* Effect/feffect.scm 82 */
									BgL_auxz00_3242 = ((obj_t) BgL_ez00_1601);
								}
							else
								{

									{	/* Effect/feffect.scm 84 */
										obj_t BgL_ezd2362zd2_1612;

										BgL_ezd2362zd2_1612 = CAR(((obj_t) BgL_vz00_1604));
										if (PAIRP(BgL_ezd2362zd2_1612))
											{	/* Effect/feffect.scm 84 */
												obj_t BgL_cdrzd2367zd2_1614;

												BgL_cdrzd2367zd2_1614 = CDR(BgL_ezd2362zd2_1612);
												if ((CAR(BgL_ezd2362zd2_1612) == CNST_TABLE_REF(2)))
													{	/* Effect/feffect.scm 84 */
														if (PAIRP(BgL_cdrzd2367zd2_1614))
															{	/* Effect/feffect.scm 84 */
																if (NULLP(CDR(BgL_cdrzd2367zd2_1614)))
																	{	/* Effect/feffect.scm 84 */
																		obj_t BgL_arg1448z00_1620;

																		BgL_arg1448z00_1620 =
																			CAR(BgL_cdrzd2367zd2_1614);
																		((((BgL_feffectz00_bglt)
																					COBJECT(BgL_ez00_1601))->
																				BgL_readz00) =
																			((obj_t) BgL_arg1448z00_1620), BUNSPEC);
																		{	/* Effect/feffect.scm 87 */
																			obj_t BgL_arg1513z00_2244;

																			BgL_arg1513z00_2244 =
																				CDR(((obj_t) BgL_vz00_1604));
																			{
																				obj_t BgL_vz00_3264;

																				BgL_vz00_3264 = BgL_arg1513z00_2244;
																				BgL_vz00_1604 = BgL_vz00_3264;
																				goto
																					BgL_zc3z04anonymousza31423ze3z87_1605;
																			}
																		}
																	}
																else
																	{	/* Effect/feffect.scm 84 */
																	BgL_tagzd2361zd2_1611:
																		BgL_auxz00_3242 =
																			BGl_userzd2errorzd2zztools_errorz00
																			(BGl_string1962z00zzeffect_feffectz00,
																			BGl_string1963z00zzeffect_feffectz00,
																			BgL_vz00_1604, BNIL);
																	}
															}
														else
															{	/* Effect/feffect.scm 84 */
																goto BgL_tagzd2361zd2_1611;
															}
													}
												else
													{	/* Effect/feffect.scm 84 */
														if ((CAR(BgL_ezd2362zd2_1612) == CNST_TABLE_REF(3)))
															{	/* Effect/feffect.scm 84 */
																if (PAIRP(BgL_cdrzd2367zd2_1614))
																	{	/* Effect/feffect.scm 84 */
																		if (NULLP(CDR(BgL_cdrzd2367zd2_1614)))
																			{	/* Effect/feffect.scm 84 */
																				obj_t BgL_arg1485z00_1628;

																				BgL_arg1485z00_1628 =
																					CAR(BgL_cdrzd2367zd2_1614);
																				((((BgL_feffectz00_bglt)
																							COBJECT(BgL_ez00_1601))->
																						BgL_writez00) =
																					((obj_t) BgL_arg1485z00_1628),
																					BUNSPEC);
																				{	/* Effect/feffect.scm 90 */
																					obj_t BgL_arg1514z00_2251;

																					BgL_arg1514z00_2251 =
																						CDR(((obj_t) BgL_vz00_1604));
																					{
																						obj_t BgL_vz00_3279;

																						BgL_vz00_3279 = BgL_arg1514z00_2251;
																						BgL_vz00_1604 = BgL_vz00_3279;
																						goto
																							BgL_zc3z04anonymousza31423ze3z87_1605;
																					}
																				}
																			}
																		else
																			{	/* Effect/feffect.scm 84 */
																				goto BgL_tagzd2361zd2_1611;
																			}
																	}
																else
																	{	/* Effect/feffect.scm 84 */
																		goto BgL_tagzd2361zd2_1611;
																	}
															}
														else
															{	/* Effect/feffect.scm 84 */
																goto BgL_tagzd2361zd2_1611;
															}
													}
											}
										else
											{	/* Effect/feffect.scm 84 */
												goto BgL_tagzd2361zd2_1611;
											}
									}
								}
							return ((BgL_feffectz00_bglt) BgL_auxz00_3242);
						}
					}
				}
			}
		}

	}



/* &parse-effect */
	BgL_feffectz00_bglt BGl_z62parsezd2effectzb0zzeffect_feffectz00(obj_t
		BgL_envz00_2796, obj_t BgL_propz00_2797)
	{
		{	/* Effect/feffect.scm 79 */
			return BGl_parsezd2effectzd2zzeffect_feffectz00(BgL_propz00_2797);
		}

	}



/* merge-effects! */
	bool_t BGl_mergezd2effectsz12zc0zzeffect_feffectz00(obj_t BgL_f1z00_8,
		obj_t BgL_f2z00_9)
	{
		{	/* Effect/feffect.scm 100 */
			{	/* Effect/feffect.scm 100 */
				struct bgl_cell BgL_box2064_2907z00;
				obj_t BgL_changedz00_2907;

				BgL_changedz00_2907 = MAKE_CELL_STACK(BFALSE, BgL_box2064_2907z00);
				{	/* Effect/feffect.scm 136 */
					obj_t BgL_arg1535z00_1642;

					{	/* Effect/feffect.scm 136 */
						obj_t BgL_arg1540z00_1643;
						obj_t BgL_arg1544z00_1644;

						BgL_arg1540z00_1643 =
							(((BgL_feffectz00_bglt) COBJECT(
									((BgL_feffectz00_bglt) BgL_f1z00_8)))->BgL_readz00);
						BgL_arg1544z00_1644 =
							(((BgL_feffectz00_bglt) COBJECT(
									((BgL_feffectz00_bglt) BgL_f2z00_9)))->BgL_readz00);
						BgL_arg1535z00_1642 =
							BGl_mergez12ze70zf5zzeffect_feffectz00(BgL_changedz00_2907,
							BgL_arg1540z00_1643, BgL_arg1544z00_1644);
					}
					((((BgL_feffectz00_bglt) COBJECT(
									((BgL_feffectz00_bglt) BgL_f1z00_8)))->BgL_readz00) =
						((obj_t) BgL_arg1535z00_1642), BUNSPEC);
				}
				{	/* Effect/feffect.scm 137 */
					obj_t BgL_arg1546z00_1645;

					{	/* Effect/feffect.scm 137 */
						obj_t BgL_arg1552z00_1646;
						obj_t BgL_arg1553z00_1647;

						BgL_arg1552z00_1646 =
							(((BgL_feffectz00_bglt) COBJECT(
									((BgL_feffectz00_bglt) BgL_f1z00_8)))->BgL_writez00);
						BgL_arg1553z00_1647 =
							(((BgL_feffectz00_bglt) COBJECT(
									((BgL_feffectz00_bglt) BgL_f2z00_9)))->BgL_writez00);
						BgL_arg1546z00_1645 =
							BGl_mergez12ze70zf5zzeffect_feffectz00(BgL_changedz00_2907,
							BgL_arg1552z00_1646, BgL_arg1553z00_1647);
					}
					((((BgL_feffectz00_bglt) COBJECT(
									((BgL_feffectz00_bglt) BgL_f1z00_8)))->BgL_writez00) =
						((obj_t) BgL_arg1546z00_1645), BUNSPEC);
				}
				return CBOOL(((obj_t) ((obj_t) CELL_REF(BgL_changedz00_2907))));
			}
		}

	}



/* merge!~0 */
	obj_t BGl_mergez12ze70zf5zzeffect_feffectz00(obj_t BgL_changedz00_2903,
		obj_t BgL_e1z00_1678, obj_t BgL_e2z00_1679)
	{
		{	/* Effect/feffect.scm 135 */
			{
				obj_t BgL_l1z00_1648;
				obj_t BgL_l2z00_1649;

				if ((BgL_e1z00_1678 == CNST_TABLE_REF(0)))
					{	/* Effect/feffect.scm 127 */
						return BgL_e1z00_1678;
					}
				else
					{	/* Effect/feffect.scm 127 */
						if (NULLP(BgL_e2z00_1679))
							{	/* Effect/feffect.scm 129 */
								return BgL_e1z00_1678;
							}
						else
							{	/* Effect/feffect.scm 129 */
								if ((BgL_e2z00_1679 == CNST_TABLE_REF(0)))
									{	/* Effect/feffect.scm 131 */
										{	/* Effect/feffect.scm 132 */
											obj_t BgL_auxz00_2904;

											BgL_auxz00_2904 = BTRUE;
											CELL_SET(BgL_changedz00_2903, BgL_auxz00_2904);
										}
										return CNST_TABLE_REF(0);
									}
								else
									{	/* Effect/feffect.scm 131 */
										BgL_l1z00_1648 = BgL_e1z00_1678;
										BgL_l2z00_1649 = BgL_e2z00_1679;
										if (NULLP(BgL_l1z00_1648))
											{	/* Effect/feffect.scm 104 */
												if (PAIRP(BgL_l2z00_1649))
													{	/* Effect/feffect.scm 106 */
														obj_t BgL_auxz00_2905;

														BgL_auxz00_2905 = BTRUE;
														CELL_SET(BgL_changedz00_2903, BgL_auxz00_2905);
													}
												else
													{	/* Effect/feffect.scm 105 */
														BFALSE;
													}
												return BgL_l2z00_1649;
											}
										else
											{	/* Effect/feffect.scm 104 */
												if (NULLP(BgL_l2z00_1649))
													{	/* Effect/feffect.scm 108 */
														return BgL_l1z00_1648;
													}
												else
													{	/* Effect/feffect.scm 111 */
														obj_t BgL_kz00_1654;

														{	/* Effect/feffect.scm 111 */

															{	/* Effect/feffect.scm 111 */

																BgL_kz00_1654 =
																	BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
															}
														}
														{
															obj_t BgL_l1269z00_1656;

															BgL_l1269z00_1656 = BgL_l1z00_1648;
														BgL_zc3z04anonymousza31558ze3z87_1657:
															if (PAIRP(BgL_l1269z00_1656))
																{	/* Effect/feffect.scm 112 */
																	BGl_putpropz12z12zz__r4_symbols_6_4z00(CAR
																		(BgL_l1269z00_1656), BgL_kz00_1654, BTRUE);
																	{
																		obj_t BgL_l1269z00_3318;

																		BgL_l1269z00_3318 = CDR(BgL_l1269z00_1656);
																		BgL_l1269z00_1656 = BgL_l1269z00_3318;
																		goto BgL_zc3z04anonymousza31558ze3z87_1657;
																	}
																}
															else
																{	/* Effect/feffect.scm 112 */
																	((bool_t) 1);
																}
														}
														{
															obj_t BgL_l1271z00_1663;

															BgL_l1271z00_1663 = BgL_l2z00_1649;
														BgL_zc3z04anonymousza31562ze3z87_1664:
															if (PAIRP(BgL_l1271z00_1663))
																{	/* Effect/feffect.scm 115 */
																	{	/* Effect/feffect.scm 116 */
																		obj_t BgL_sz00_1666;

																		BgL_sz00_1666 = CAR(BgL_l1271z00_1663);
																		if (CBOOL
																			(BGl_getpropz00zz__r4_symbols_6_4z00
																				(BgL_sz00_1666, BgL_kz00_1654)))
																			{	/* Effect/feffect.scm 116 */
																				BFALSE;
																			}
																		else
																			{	/* Effect/feffect.scm 116 */
																				{	/* Effect/feffect.scm 118 */
																					obj_t BgL_auxz00_2906;

																					BgL_auxz00_2906 = BTRUE;
																					CELL_SET(BgL_changedz00_2903,
																						BgL_auxz00_2906);
																				}
																				BgL_l1z00_1648 =
																					MAKE_YOUNG_PAIR(BgL_sz00_1666,
																					BgL_l1z00_1648);
																			}
																	}
																	{
																		obj_t BgL_l1271z00_3327;

																		BgL_l1271z00_3327 = CDR(BgL_l1271z00_1663);
																		BgL_l1271z00_1663 = BgL_l1271z00_3327;
																		goto BgL_zc3z04anonymousza31562ze3z87_1664;
																	}
																}
															else
																{	/* Effect/feffect.scm 115 */
																	((bool_t) 1);
																}
														}
														{
															obj_t BgL_l1273z00_1671;

															BgL_l1273z00_1671 = BgL_l1z00_1648;
														BgL_zc3z04anonymousza31566ze3z87_1672:
															if (PAIRP(BgL_l1273z00_1671))
																{	/* Effect/feffect.scm 121 */
																	BGl_rempropz12z12zz__r4_symbols_6_4z00(CAR
																		(BgL_l1273z00_1671), BgL_kz00_1654);
																	{
																		obj_t BgL_l1273z00_3333;

																		BgL_l1273z00_3333 = CDR(BgL_l1273z00_1671);
																		BgL_l1273z00_1671 = BgL_l1273z00_3333;
																		goto BgL_zc3z04anonymousza31566ze3z87_1672;
																	}
																}
															else
																{	/* Effect/feffect.scm 121 */
																	((bool_t) 1);
																}
														}
														return BgL_l1z00_1648;
													}
											}
									}
							}
					}
			}
		}

	}



/* fun-effect! */
	BgL_feffectz00_bglt BGl_funzd2effectz12zc0zzeffect_feffectz00(obj_t
		BgL_vz00_10)
	{
		{	/* Effect/feffect.scm 143 */
			{	/* Effect/feffect.scm 144 */
				bool_t BgL_test2075z00_3335;

				{	/* Effect/feffect.scm 144 */
					obj_t BgL_classz00_2267;

					BgL_classz00_2267 = BGl_globalz00zzast_varz00;
					if (BGL_OBJECTP(BgL_vz00_10))
						{	/* Effect/feffect.scm 144 */
							BgL_objectz00_bglt BgL_arg1807z00_2269;

							BgL_arg1807z00_2269 = (BgL_objectz00_bglt) (BgL_vz00_10);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Effect/feffect.scm 144 */
									long BgL_idxz00_2275;

									BgL_idxz00_2275 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2269);
									BgL_test2075z00_3335 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2275 + 2L)) == BgL_classz00_2267);
								}
							else
								{	/* Effect/feffect.scm 144 */
									bool_t BgL_res1886z00_2300;

									{	/* Effect/feffect.scm 144 */
										obj_t BgL_oclassz00_2283;

										{	/* Effect/feffect.scm 144 */
											obj_t BgL_arg1815z00_2291;
											long BgL_arg1816z00_2292;

											BgL_arg1815z00_2291 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Effect/feffect.scm 144 */
												long BgL_arg1817z00_2293;

												BgL_arg1817z00_2293 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2269);
												BgL_arg1816z00_2292 =
													(BgL_arg1817z00_2293 - OBJECT_TYPE);
											}
											BgL_oclassz00_2283 =
												VECTOR_REF(BgL_arg1815z00_2291, BgL_arg1816z00_2292);
										}
										{	/* Effect/feffect.scm 144 */
											bool_t BgL__ortest_1115z00_2284;

											BgL__ortest_1115z00_2284 =
												(BgL_classz00_2267 == BgL_oclassz00_2283);
											if (BgL__ortest_1115z00_2284)
												{	/* Effect/feffect.scm 144 */
													BgL_res1886z00_2300 = BgL__ortest_1115z00_2284;
												}
											else
												{	/* Effect/feffect.scm 144 */
													long BgL_odepthz00_2285;

													{	/* Effect/feffect.scm 144 */
														obj_t BgL_arg1804z00_2286;

														BgL_arg1804z00_2286 = (BgL_oclassz00_2283);
														BgL_odepthz00_2285 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2286);
													}
													if ((2L < BgL_odepthz00_2285))
														{	/* Effect/feffect.scm 144 */
															obj_t BgL_arg1802z00_2288;

															{	/* Effect/feffect.scm 144 */
																obj_t BgL_arg1803z00_2289;

																BgL_arg1803z00_2289 = (BgL_oclassz00_2283);
																BgL_arg1802z00_2288 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2289,
																	2L);
															}
															BgL_res1886z00_2300 =
																(BgL_arg1802z00_2288 == BgL_classz00_2267);
														}
													else
														{	/* Effect/feffect.scm 144 */
															BgL_res1886z00_2300 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2075z00_3335 = BgL_res1886z00_2300;
								}
						}
					else
						{	/* Effect/feffect.scm 144 */
							BgL_test2075z00_3335 = ((bool_t) 0);
						}
				}
				if (BgL_test2075z00_3335)
					{	/* Effect/feffect.scm 144 */
						return
							BGl_funzd2effectzd2globalz12z12zzeffect_feffectz00(
							((BgL_globalz00_bglt) BgL_vz00_10));
					}
				else
					{	/* Effect/feffect.scm 144 */
						return
							BGl_dozd2funzd2effectz12z12zzeffect_feffectz00(
							((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_vz00_10)));
					}
			}
		}

	}



/* fun-effect-global! */
	BgL_feffectz00_bglt
		BGl_funzd2effectzd2globalz12z12zzeffect_feffectz00(BgL_globalz00_bglt
		BgL_vz00_11)
	{
		{	/* Effect/feffect.scm 151 */
			{	/* Effect/feffect.scm 152 */
				bool_t BgL_test2080z00_3363;

				if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
							(((BgL_globalz00_bglt) COBJECT(BgL_vz00_11))->BgL_importz00),
							CNST_TABLE_REF(4))))
					{	/* Effect/feffect.scm 153 */
						bool_t BgL_test2082z00_3369;

						{	/* Effect/feffect.scm 153 */
							BgL_valuez00_bglt BgL_arg1593z00_1694;

							BgL_arg1593z00_1694 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_vz00_11)))->BgL_valuez00);
							{	/* Effect/feffect.scm 153 */
								obj_t BgL_classz00_2304;

								BgL_classz00_2304 = BGl_sfunz00zzast_varz00;
								{	/* Effect/feffect.scm 153 */
									BgL_objectz00_bglt BgL_arg1807z00_2306;

									{	/* Effect/feffect.scm 153 */
										obj_t BgL_tmpz00_3372;

										BgL_tmpz00_3372 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1593z00_1694));
										BgL_arg1807z00_2306 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3372);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Effect/feffect.scm 153 */
											long BgL_idxz00_2312;

											BgL_idxz00_2312 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2306);
											BgL_test2082z00_3369 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2312 + 3L)) == BgL_classz00_2304);
										}
									else
										{	/* Effect/feffect.scm 153 */
											bool_t BgL_res1887z00_2337;

											{	/* Effect/feffect.scm 153 */
												obj_t BgL_oclassz00_2320;

												{	/* Effect/feffect.scm 153 */
													obj_t BgL_arg1815z00_2328;
													long BgL_arg1816z00_2329;

													BgL_arg1815z00_2328 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Effect/feffect.scm 153 */
														long BgL_arg1817z00_2330;

														BgL_arg1817z00_2330 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2306);
														BgL_arg1816z00_2329 =
															(BgL_arg1817z00_2330 - OBJECT_TYPE);
													}
													BgL_oclassz00_2320 =
														VECTOR_REF(BgL_arg1815z00_2328,
														BgL_arg1816z00_2329);
												}
												{	/* Effect/feffect.scm 153 */
													bool_t BgL__ortest_1115z00_2321;

													BgL__ortest_1115z00_2321 =
														(BgL_classz00_2304 == BgL_oclassz00_2320);
													if (BgL__ortest_1115z00_2321)
														{	/* Effect/feffect.scm 153 */
															BgL_res1887z00_2337 = BgL__ortest_1115z00_2321;
														}
													else
														{	/* Effect/feffect.scm 153 */
															long BgL_odepthz00_2322;

															{	/* Effect/feffect.scm 153 */
																obj_t BgL_arg1804z00_2323;

																BgL_arg1804z00_2323 = (BgL_oclassz00_2320);
																BgL_odepthz00_2322 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2323);
															}
															if ((3L < BgL_odepthz00_2322))
																{	/* Effect/feffect.scm 153 */
																	obj_t BgL_arg1802z00_2325;

																	{	/* Effect/feffect.scm 153 */
																		obj_t BgL_arg1803z00_2326;

																		BgL_arg1803z00_2326 = (BgL_oclassz00_2320);
																		BgL_arg1802z00_2325 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2326, 3L);
																	}
																	BgL_res1887z00_2337 =
																		(BgL_arg1802z00_2325 == BgL_classz00_2304);
																}
															else
																{	/* Effect/feffect.scm 153 */
																	BgL_res1887z00_2337 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2082z00_3369 = BgL_res1887z00_2337;
										}
								}
							}
						}
						if (BgL_test2082z00_3369)
							{	/* Effect/feffect.scm 153 */
								BgL_test2080z00_3363 = ((bool_t) 0);
							}
						else
							{	/* Effect/feffect.scm 153 */
								BgL_test2080z00_3363 = ((bool_t) 1);
							}
					}
				else
					{	/* Effect/feffect.scm 152 */
						BgL_test2080z00_3363 = ((bool_t) 1);
					}
				if (BgL_test2080z00_3363)
					{	/* Effect/feffect.scm 152 */
						BGL_TAIL return
							BGl_funzd2effectzd2alienz12z12zzeffect_feffectz00(BgL_vz00_11);
					}
				else
					{	/* Effect/feffect.scm 152 */
						BGL_TAIL return
							BGl_funzd2effectzd2modulez12z12zzeffect_feffectz00(BgL_vz00_11);
					}
			}
		}

	}



/* fun-effect-alien! */
	BgL_feffectz00_bglt
		BGl_funzd2effectzd2alienz12z12zzeffect_feffectz00(BgL_globalz00_bglt
		BgL_gz00_12)
	{
		{	/* Effect/feffect.scm 162 */
			{	/* Effect/feffect.scm 163 */
				BgL_funz00_bglt BgL_i1121z00_1696;

				BgL_i1121z00_1696 =
					((BgL_funz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_gz00_12)))->BgL_valuez00));
				{	/* Effect/feffect.scm 164 */
					bool_t BgL_test2086z00_3400;

					{	/* Effect/feffect.scm 164 */
						obj_t BgL_arg1602z00_1699;

						BgL_arg1602z00_1699 =
							(((BgL_funz00_bglt) COBJECT(BgL_i1121z00_1696))->BgL_effectz00);
						{	/* Effect/feffect.scm 164 */
							obj_t BgL_classz00_2339;

							BgL_classz00_2339 = BGl_feffectz00zzast_varz00;
							if (BGL_OBJECTP(BgL_arg1602z00_1699))
								{	/* Effect/feffect.scm 164 */
									BgL_objectz00_bglt BgL_arg1807z00_2341;

									BgL_arg1807z00_2341 =
										(BgL_objectz00_bglt) (BgL_arg1602z00_1699);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Effect/feffect.scm 164 */
											long BgL_idxz00_2347;

											BgL_idxz00_2347 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2341);
											BgL_test2086z00_3400 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2347 + 1L)) == BgL_classz00_2339);
										}
									else
										{	/* Effect/feffect.scm 164 */
											bool_t BgL_res1888z00_2372;

											{	/* Effect/feffect.scm 164 */
												obj_t BgL_oclassz00_2355;

												{	/* Effect/feffect.scm 164 */
													obj_t BgL_arg1815z00_2363;
													long BgL_arg1816z00_2364;

													BgL_arg1815z00_2363 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Effect/feffect.scm 164 */
														long BgL_arg1817z00_2365;

														BgL_arg1817z00_2365 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2341);
														BgL_arg1816z00_2364 =
															(BgL_arg1817z00_2365 - OBJECT_TYPE);
													}
													BgL_oclassz00_2355 =
														VECTOR_REF(BgL_arg1815z00_2363,
														BgL_arg1816z00_2364);
												}
												{	/* Effect/feffect.scm 164 */
													bool_t BgL__ortest_1115z00_2356;

													BgL__ortest_1115z00_2356 =
														(BgL_classz00_2339 == BgL_oclassz00_2355);
													if (BgL__ortest_1115z00_2356)
														{	/* Effect/feffect.scm 164 */
															BgL_res1888z00_2372 = BgL__ortest_1115z00_2356;
														}
													else
														{	/* Effect/feffect.scm 164 */
															long BgL_odepthz00_2357;

															{	/* Effect/feffect.scm 164 */
																obj_t BgL_arg1804z00_2358;

																BgL_arg1804z00_2358 = (BgL_oclassz00_2355);
																BgL_odepthz00_2357 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2358);
															}
															if ((1L < BgL_odepthz00_2357))
																{	/* Effect/feffect.scm 164 */
																	obj_t BgL_arg1802z00_2360;

																	{	/* Effect/feffect.scm 164 */
																		obj_t BgL_arg1803z00_2361;

																		BgL_arg1803z00_2361 = (BgL_oclassz00_2355);
																		BgL_arg1802z00_2360 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2361, 1L);
																	}
																	BgL_res1888z00_2372 =
																		(BgL_arg1802z00_2360 == BgL_classz00_2339);
																}
															else
																{	/* Effect/feffect.scm 164 */
																	BgL_res1888z00_2372 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2086z00_3400 = BgL_res1888z00_2372;
										}
								}
							else
								{	/* Effect/feffect.scm 164 */
									BgL_test2086z00_3400 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2086z00_3400)
						{	/* Effect/feffect.scm 164 */
							BFALSE;
						}
					else
						{	/* Effect/feffect.scm 164 */
							((((BgL_funz00_bglt) COBJECT(BgL_i1121z00_1696))->BgL_effectz00) =
								((obj_t) ((obj_t)
										BGl_za2effectzd2topza2zd2zzeffect_feffectz00)), BUNSPEC);
						}
				}
				return
					((BgL_feffectz00_bglt)
					(((BgL_funz00_bglt) COBJECT(BgL_i1121z00_1696))->BgL_effectz00));
			}
		}

	}



/* fun-effect-module! */
	BgL_feffectz00_bglt
		BGl_funzd2effectzd2modulez12z12zzeffect_feffectz00(BgL_globalz00_bglt
		BgL_vz00_13)
	{
		{	/* Effect/feffect.scm 171 */
			{	/* Effect/feffect.scm 172 */
				BgL_valuez00_bglt BgL_funz00_1700;

				BgL_funz00_1700 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_vz00_13)))->BgL_valuez00);
				{	/* Effect/feffect.scm 172 */
					obj_t BgL_efz00_1701;

					BgL_efz00_1701 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_funz00_1700)))->BgL_effectz00);
					{	/* Effect/feffect.scm 173 */

						{	/* Effect/feffect.scm 176 */
							bool_t BgL_test2091z00_3432;

							{	/* Effect/feffect.scm 176 */
								obj_t BgL_classz00_2375;

								BgL_classz00_2375 = BGl_feffectz00zzast_varz00;
								{	/* Effect/feffect.scm 176 */
									BgL_objectz00_bglt BgL_arg1807z00_2377;

									{	/* Effect/feffect.scm 176 */
										obj_t BgL_tmpz00_3433;

										BgL_tmpz00_3433 =
											((obj_t) ((BgL_objectz00_bglt) BgL_funz00_1700));
										BgL_arg1807z00_2377 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3433);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Effect/feffect.scm 176 */
											long BgL_idxz00_2383;

											BgL_idxz00_2383 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2377);
											BgL_test2091z00_3432 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2383 + 1L)) == BgL_classz00_2375);
										}
									else
										{	/* Effect/feffect.scm 176 */
											bool_t BgL_res1889z00_2408;

											{	/* Effect/feffect.scm 176 */
												obj_t BgL_oclassz00_2391;

												{	/* Effect/feffect.scm 176 */
													obj_t BgL_arg1815z00_2399;
													long BgL_arg1816z00_2400;

													BgL_arg1815z00_2399 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Effect/feffect.scm 176 */
														long BgL_arg1817z00_2401;

														BgL_arg1817z00_2401 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2377);
														BgL_arg1816z00_2400 =
															(BgL_arg1817z00_2401 - OBJECT_TYPE);
													}
													BgL_oclassz00_2391 =
														VECTOR_REF(BgL_arg1815z00_2399,
														BgL_arg1816z00_2400);
												}
												{	/* Effect/feffect.scm 176 */
													bool_t BgL__ortest_1115z00_2392;

													BgL__ortest_1115z00_2392 =
														(BgL_classz00_2375 == BgL_oclassz00_2391);
													if (BgL__ortest_1115z00_2392)
														{	/* Effect/feffect.scm 176 */
															BgL_res1889z00_2408 = BgL__ortest_1115z00_2392;
														}
													else
														{	/* Effect/feffect.scm 176 */
															long BgL_odepthz00_2393;

															{	/* Effect/feffect.scm 176 */
																obj_t BgL_arg1804z00_2394;

																BgL_arg1804z00_2394 = (BgL_oclassz00_2391);
																BgL_odepthz00_2393 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2394);
															}
															if ((1L < BgL_odepthz00_2393))
																{	/* Effect/feffect.scm 176 */
																	obj_t BgL_arg1802z00_2396;

																	{	/* Effect/feffect.scm 176 */
																		obj_t BgL_arg1803z00_2397;

																		BgL_arg1803z00_2397 = (BgL_oclassz00_2391);
																		BgL_arg1802z00_2396 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2397, 1L);
																	}
																	BgL_res1889z00_2408 =
																		(BgL_arg1802z00_2396 == BgL_classz00_2375);
																}
															else
																{	/* Effect/feffect.scm 176 */
																	BgL_res1889z00_2408 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2091z00_3432 = BgL_res1889z00_2408;
										}
								}
							}
							if (BgL_test2091z00_3432)
								{	/* Effect/feffect.scm 176 */
									return ((BgL_feffectz00_bglt) BgL_efz00_1701);
								}
							else
								{	/* Effect/feffect.scm 176 */
									return
										BGl_dozd2funzd2effectz12z12zzeffect_feffectz00(
										((BgL_variablez00_bglt) BgL_vz00_13));
								}
						}
					}
				}
			}
		}

	}



/* do-fun-effect! */
	BgL_feffectz00_bglt
		BGl_dozd2funzd2effectz12z12zzeffect_feffectz00(BgL_variablez00_bglt
		BgL_vz00_15)
	{
		{	/* Effect/feffect.scm 195 */
			{	/* Effect/feffect.scm 196 */
				BgL_sfunz00_bglt BgL_i1122z00_1703;

				BgL_i1122z00_1703 =
					((BgL_sfunz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(BgL_vz00_15))->BgL_valuez00));
				{	/* Effect/feffect.scm 197 */
					bool_t BgL_test2095z00_3461;

					{	/* Effect/feffect.scm 197 */
						obj_t BgL_arg1615z00_1711;

						BgL_arg1615z00_1711 =
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_i1122z00_1703)))->BgL_effectz00);
						{	/* Effect/feffect.scm 197 */
							obj_t BgL_classz00_2410;

							BgL_classz00_2410 = BGl_feffectz00zzast_varz00;
							if (BGL_OBJECTP(BgL_arg1615z00_1711))
								{	/* Effect/feffect.scm 197 */
									BgL_objectz00_bglt BgL_arg1807z00_2412;

									BgL_arg1807z00_2412 =
										(BgL_objectz00_bglt) (BgL_arg1615z00_1711);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Effect/feffect.scm 197 */
											long BgL_idxz00_2418;

											BgL_idxz00_2418 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2412);
											BgL_test2095z00_3461 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2418 + 1L)) == BgL_classz00_2410);
										}
									else
										{	/* Effect/feffect.scm 197 */
											bool_t BgL_res1890z00_2443;

											{	/* Effect/feffect.scm 197 */
												obj_t BgL_oclassz00_2426;

												{	/* Effect/feffect.scm 197 */
													obj_t BgL_arg1815z00_2434;
													long BgL_arg1816z00_2435;

													BgL_arg1815z00_2434 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Effect/feffect.scm 197 */
														long BgL_arg1817z00_2436;

														BgL_arg1817z00_2436 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2412);
														BgL_arg1816z00_2435 =
															(BgL_arg1817z00_2436 - OBJECT_TYPE);
													}
													BgL_oclassz00_2426 =
														VECTOR_REF(BgL_arg1815z00_2434,
														BgL_arg1816z00_2435);
												}
												{	/* Effect/feffect.scm 197 */
													bool_t BgL__ortest_1115z00_2427;

													BgL__ortest_1115z00_2427 =
														(BgL_classz00_2410 == BgL_oclassz00_2426);
													if (BgL__ortest_1115z00_2427)
														{	/* Effect/feffect.scm 197 */
															BgL_res1890z00_2443 = BgL__ortest_1115z00_2427;
														}
													else
														{	/* Effect/feffect.scm 197 */
															long BgL_odepthz00_2428;

															{	/* Effect/feffect.scm 197 */
																obj_t BgL_arg1804z00_2429;

																BgL_arg1804z00_2429 = (BgL_oclassz00_2426);
																BgL_odepthz00_2428 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2429);
															}
															if ((1L < BgL_odepthz00_2428))
																{	/* Effect/feffect.scm 197 */
																	obj_t BgL_arg1802z00_2431;

																	{	/* Effect/feffect.scm 197 */
																		obj_t BgL_arg1803z00_2432;

																		BgL_arg1803z00_2432 = (BgL_oclassz00_2426);
																		BgL_arg1802z00_2431 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2432, 1L);
																	}
																	BgL_res1890z00_2443 =
																		(BgL_arg1802z00_2431 == BgL_classz00_2410);
																}
															else
																{	/* Effect/feffect.scm 197 */
																	BgL_res1890z00_2443 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2095z00_3461 = BgL_res1890z00_2443;
										}
								}
							else
								{	/* Effect/feffect.scm 197 */
									BgL_test2095z00_3461 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2095z00_3461)
						{	/* Effect/feffect.scm 197 */
							return
								((BgL_feffectz00_bglt)
								(((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt) BgL_i1122z00_1703)))->BgL_effectz00));
						}
					else
						{	/* Effect/feffect.scm 197 */
							{
								obj_t BgL_auxz00_3489;

								{	/* Effect/feffect.scm 200 */
									BgL_feffectz00_bglt BgL_new1124z00_1706;

									{	/* Ast/var.scm 180 */
										BgL_feffectz00_bglt BgL_new1123z00_1707;

										BgL_new1123z00_1707 =
											((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_feffectz00_bgl))));
										{	/* Ast/var.scm 180 */
											long BgL_arg1609z00_1708;

											{	/* Ast/var.scm 180 */
												obj_t BgL_classz00_2444;

												BgL_classz00_2444 = BGl_feffectz00zzast_varz00;
												BgL_arg1609z00_1708 = BGL_CLASS_NUM(BgL_classz00_2444);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1123z00_1707),
												BgL_arg1609z00_1708);
										}
										BgL_new1124z00_1706 = BgL_new1123z00_1707;
									}
									((((BgL_feffectz00_bglt) COBJECT(BgL_new1124z00_1706))->
											BgL_readz00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_feffectz00_bglt) COBJECT(BgL_new1124z00_1706))->
											BgL_writez00) = ((obj_t) BNIL), BUNSPEC);
									BgL_auxz00_3489 = ((obj_t) BgL_new1124z00_1706);
								}
								((((BgL_funz00_bglt) COBJECT(
												((BgL_funz00_bglt) BgL_i1122z00_1703)))->
										BgL_effectz00) = ((obj_t) BgL_auxz00_3489), BUNSPEC);
							}
							{	/* Effect/feffect.scm 201 */
								obj_t BgL_arg1611z00_1709;
								obj_t BgL_arg1613z00_1710;

								BgL_arg1611z00_1709 =
									(((BgL_sfunz00_bglt) COBJECT(BgL_i1122z00_1703))->
									BgL_bodyz00);
								BgL_arg1613z00_1710 =
									(((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_i1122z00_1703)))->BgL_effectz00);
								return
									BGl_bodyzd2effectz12zc0zzeffect_feffectz00(((BgL_nodez00_bglt)
										BgL_arg1611z00_1709),
									((BgL_feffectz00_bglt) BgL_arg1613z00_1710));
							}
						}
				}
			}
		}

	}



/* body-effect*! */
	obj_t BGl_bodyzd2effectza2z12z62zzeffect_feffectz00(obj_t BgL_nodeza2za2_62,
		obj_t BgL_effectz00_63)
	{
		{	/* Effect/feffect.scm 390 */
			{
				obj_t BgL_l1284z00_1713;

				BgL_l1284z00_1713 = BgL_nodeza2za2_62;
			BgL_zc3z04anonymousza31616ze3z87_1714:
				if (PAIRP(BgL_l1284z00_1713))
					{	/* Effect/feffect.scm 391 */
						{	/* Effect/feffect.scm 391 */
							obj_t BgL_nz00_1716;

							BgL_nz00_1716 = CAR(BgL_l1284z00_1713);
							BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
								((BgL_nodez00_bglt) BgL_nz00_1716),
								((BgL_feffectz00_bglt) BgL_effectz00_63));
						}
						{
							obj_t BgL_l1284z00_3511;

							BgL_l1284z00_3511 = CDR(BgL_l1284z00_1713);
							BgL_l1284z00_1713 = BgL_l1284z00_3511;
							goto BgL_zc3z04anonymousza31616ze3z87_1714;
						}
					}
				else
					{	/* Effect/feffect.scm 391 */
						((bool_t) 1);
					}
			}
			return BgL_effectz00_63;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzeffect_feffectz00(void)
	{
		{	/* Effect/feffect.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzeffect_feffectz00(void)
	{
		{	/* Effect/feffect.scm 16 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_proc1964z00zzeffect_feffectz00, BGl_nodez00zzast_nodez00,
				BGl_string1965z00zzeffect_feffectz00);
		}

	}



/* &body-effect!1288 */
	obj_t BGl_z62bodyzd2effectz121288za2zzeffect_feffectz00(obj_t BgL_envz00_2799,
		obj_t BgL_nodez00_2800, obj_t BgL_effectz00_2801)
	{
		{	/* Effect/feffect.scm 210 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(5),
				BGl_string1966z00zzeffect_feffectz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2800)));
		}

	}



/* body-effect! */
	BgL_feffectz00_bglt
		BGl_bodyzd2effectz12zc0zzeffect_feffectz00(BgL_nodez00_bglt BgL_nodez00_16,
		BgL_feffectz00_bglt BgL_effectz00_17)
	{
		{	/* Effect/feffect.scm 210 */
			{	/* Effect/feffect.scm 210 */
				obj_t BgL_method1289z00_1724;

				{	/* Effect/feffect.scm 210 */
					obj_t BgL_res1895z00_2479;

					{	/* Effect/feffect.scm 210 */
						long BgL_objzd2classzd2numz00_2450;

						BgL_objzd2classzd2numz00_2450 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_16));
						{	/* Effect/feffect.scm 210 */
							obj_t BgL_arg1811z00_2451;

							BgL_arg1811z00_2451 =
								PROCEDURE_REF(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
								(int) (1L));
							{	/* Effect/feffect.scm 210 */
								int BgL_offsetz00_2454;

								BgL_offsetz00_2454 = (int) (BgL_objzd2classzd2numz00_2450);
								{	/* Effect/feffect.scm 210 */
									long BgL_offsetz00_2455;

									BgL_offsetz00_2455 =
										((long) (BgL_offsetz00_2454) - OBJECT_TYPE);
									{	/* Effect/feffect.scm 210 */
										long BgL_modz00_2456;

										BgL_modz00_2456 =
											(BgL_offsetz00_2455 >> (int) ((long) ((int) (4L))));
										{	/* Effect/feffect.scm 210 */
											long BgL_restz00_2458;

											BgL_restz00_2458 =
												(BgL_offsetz00_2455 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Effect/feffect.scm 210 */

												{	/* Effect/feffect.scm 210 */
													obj_t BgL_bucketz00_2460;

													BgL_bucketz00_2460 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2451), BgL_modz00_2456);
													BgL_res1895z00_2479 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2460), BgL_restz00_2458);
					}}}}}}}}
					BgL_method1289z00_1724 = BgL_res1895z00_2479;
				}
				return
					((BgL_feffectz00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1289z00_1724,
						((obj_t) BgL_nodez00_16), ((obj_t) BgL_effectz00_17)));
			}
		}

	}



/* &body-effect! */
	BgL_feffectz00_bglt BGl_z62bodyzd2effectz12za2zzeffect_feffectz00(obj_t
		BgL_envz00_2802, obj_t BgL_nodez00_2803, obj_t BgL_effectz00_2804)
	{
		{	/* Effect/feffect.scm 210 */
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				((BgL_nodez00_bglt) BgL_nodez00_2803),
				((BgL_feffectz00_bglt) BgL_effectz00_2804));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzeffect_feffectz00(void)
	{
		{	/* Effect/feffect.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_objectzd2displayzd2envz00zz__objectz00, BGl_feffectz00zzast_varz00,
				BGl_proc1967z00zzeffect_feffectz00,
				BGl_string1968z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_atomz00zzast_nodez00, BGl_proc1969z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_patchz00zzast_nodez00, BGl_proc1971z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_kwotez00zzast_nodez00, BGl_proc1972z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_varz00zzast_nodez00, BGl_proc1973z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_setqz00zzast_nodez00, BGl_proc1974z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1975z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_switchz00zzast_nodez00, BGl_proc1976z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_failz00zzast_nodez00, BGl_proc1977z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1978z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1979z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1980z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1981z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1982z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_castz00zzast_nodez00, BGl_proc1983z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1984z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_syncz00zzast_nodez00, BGl_proc1985z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1986z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1987z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_appz00zzast_nodez00, BGl_proc1988z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_externz00zzast_nodez00, BGl_proc1989z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1990z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bodyzd2effectz12zd2envz12zzeffect_feffectz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1991z00zzeffect_feffectz00,
				BGl_string1970z00zzeffect_feffectz00);
		}

	}



/* &body-effect!-let-var1333 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2letzd2var1333za2zzeffect_feffectz00(obj_t
		BgL_envz00_2829, obj_t BgL_nodez00_2830, obj_t BgL_effectz00_2831)
	{
		{	/* Effect/feffect.scm 380 */
			{	/* Effect/feffect.scm 382 */
				obj_t BgL_g1283z00_2930;

				BgL_g1283z00_2930 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2830)))->BgL_bindingsz00);
				{
					obj_t BgL_l1281z00_2932;

					BgL_l1281z00_2932 = BgL_g1283z00_2930;
				BgL_zc3z04anonymousza31762ze3z87_2931:
					if (PAIRP(BgL_l1281z00_2932))
						{	/* Effect/feffect.scm 382 */
							{	/* Effect/feffect.scm 383 */
								obj_t BgL_bindingz00_2933;

								BgL_bindingz00_2933 = CAR(BgL_l1281z00_2932);
								{	/* Effect/feffect.scm 383 */
									obj_t BgL_arg1765z00_2934;

									BgL_arg1765z00_2934 = CDR(((obj_t) BgL_bindingz00_2933));
									BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
										((BgL_nodez00_bglt) BgL_arg1765z00_2934),
										((BgL_feffectz00_bglt) BgL_effectz00_2831));
								}
							}
							{
								obj_t BgL_l1281z00_3587;

								BgL_l1281z00_3587 = CDR(BgL_l1281z00_2932);
								BgL_l1281z00_2932 = BgL_l1281z00_3587;
								goto BgL_zc3z04anonymousza31762ze3z87_2931;
							}
						}
					else
						{	/* Effect/feffect.scm 382 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2830)))->BgL_bodyz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2831));
		}

	}



/* &body-effect!-let-fun1331 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2letzd2fun1331za2zzeffect_feffectz00(obj_t
		BgL_envz00_2832, obj_t BgL_nodez00_2833, obj_t BgL_effectz00_2834)
	{
		{	/* Effect/feffect.scm 372 */
			{	/* Effect/feffect.scm 374 */
				obj_t BgL_g1280z00_2937;

				BgL_g1280z00_2937 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2833)))->BgL_localsz00);
				{
					obj_t BgL_l1278z00_2939;

					BgL_l1278z00_2939 = BgL_g1280z00_2937;
				BgL_zc3z04anonymousza31752ze3z87_2938:
					if (PAIRP(BgL_l1278z00_2939))
						{	/* Effect/feffect.scm 374 */
							BGl_funzd2effectz12zc0zzeffect_feffectz00(CAR(BgL_l1278z00_2939));
							{
								obj_t BgL_l1278z00_3599;

								BgL_l1278z00_3599 = CDR(BgL_l1278z00_2939);
								BgL_l1278z00_2939 = BgL_l1278z00_3599;
								goto BgL_zc3z04anonymousza31752ze3z87_2938;
							}
						}
					else
						{	/* Effect/feffect.scm 374 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2833)))->BgL_bodyz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2834));
		}

	}



/* &body-effect!-extern1329 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2extern1329z70zzeffect_feffectz00(obj_t
		BgL_envz00_2835, obj_t BgL_nodez00_2836, obj_t BgL_effectz00_2837)
	{
		{	/* Effect/feffect.scm 362 */
			{	/* Effect/feffect.scm 363 */
				obj_t BgL_efz00_2941;

				{	/* Effect/feffect.scm 363 */
					bool_t BgL_test2103z00_3605;

					{	/* Effect/feffect.scm 363 */
						obj_t BgL_arg1751z00_2942;

						BgL_arg1751z00_2942 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_nodez00_2836)))->BgL_effectz00);
						{	/* Effect/feffect.scm 363 */
							obj_t BgL_classz00_2943;

							BgL_classz00_2943 = BGl_feffectz00zzast_varz00;
							if (BGL_OBJECTP(BgL_arg1751z00_2942))
								{	/* Effect/feffect.scm 363 */
									BgL_objectz00_bglt BgL_arg1807z00_2944;

									BgL_arg1807z00_2944 =
										(BgL_objectz00_bglt) (BgL_arg1751z00_2942);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Effect/feffect.scm 363 */
											long BgL_idxz00_2945;

											BgL_idxz00_2945 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2944);
											BgL_test2103z00_3605 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2945 + 1L)) == BgL_classz00_2943);
										}
									else
										{	/* Effect/feffect.scm 363 */
											bool_t BgL_res1898z00_2948;

											{	/* Effect/feffect.scm 363 */
												obj_t BgL_oclassz00_2949;

												{	/* Effect/feffect.scm 363 */
													obj_t BgL_arg1815z00_2950;
													long BgL_arg1816z00_2951;

													BgL_arg1815z00_2950 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Effect/feffect.scm 363 */
														long BgL_arg1817z00_2952;

														BgL_arg1817z00_2952 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2944);
														BgL_arg1816z00_2951 =
															(BgL_arg1817z00_2952 - OBJECT_TYPE);
													}
													BgL_oclassz00_2949 =
														VECTOR_REF(BgL_arg1815z00_2950,
														BgL_arg1816z00_2951);
												}
												{	/* Effect/feffect.scm 363 */
													bool_t BgL__ortest_1115z00_2953;

													BgL__ortest_1115z00_2953 =
														(BgL_classz00_2943 == BgL_oclassz00_2949);
													if (BgL__ortest_1115z00_2953)
														{	/* Effect/feffect.scm 363 */
															BgL_res1898z00_2948 = BgL__ortest_1115z00_2953;
														}
													else
														{	/* Effect/feffect.scm 363 */
															long BgL_odepthz00_2954;

															{	/* Effect/feffect.scm 363 */
																obj_t BgL_arg1804z00_2955;

																BgL_arg1804z00_2955 = (BgL_oclassz00_2949);
																BgL_odepthz00_2954 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2955);
															}
															if ((1L < BgL_odepthz00_2954))
																{	/* Effect/feffect.scm 363 */
																	obj_t BgL_arg1802z00_2956;

																	{	/* Effect/feffect.scm 363 */
																		obj_t BgL_arg1803z00_2957;

																		BgL_arg1803z00_2957 = (BgL_oclassz00_2949);
																		BgL_arg1802z00_2956 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2957, 1L);
																	}
																	BgL_res1898z00_2948 =
																		(BgL_arg1802z00_2956 == BgL_classz00_2943);
																}
															else
																{	/* Effect/feffect.scm 363 */
																	BgL_res1898z00_2948 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2103z00_3605 = BgL_res1898z00_2948;
										}
								}
							else
								{	/* Effect/feffect.scm 363 */
									BgL_test2103z00_3605 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2103z00_3605)
						{	/* Effect/feffect.scm 363 */
							BgL_efz00_2941 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt) BgL_nodez00_2836)))->BgL_effectz00);
						}
					else
						{	/* Effect/feffect.scm 363 */
							BgL_efz00_2941 =
								((obj_t) BGl_za2effectzd2topza2zd2zzeffect_feffectz00);
						}
				}
				BGl_mergezd2effectsz12zc0zzeffect_feffectz00(BgL_effectz00_2837,
					BgL_efz00_2941);
				return ((BgL_feffectz00_bglt) BgL_effectz00_2837);
			}
		}

	}



/* &body-effect!-app1327 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2app1327z70zzeffect_feffectz00(obj_t
		BgL_envz00_2838, obj_t BgL_nodez00_2839, obj_t BgL_effectz00_2840)
	{
		{	/* Effect/feffect.scm 352 */
			BGl_bodyzd2effectza2z12z62zzeffect_feffectz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2839)))->BgL_argsz00),
				BgL_effectz00_2840);
			{	/* Effect/feffect.scm 355 */
				BgL_feffectz00_bglt BgL_efz00_2959;

				{	/* Effect/feffect.scm 355 */
					BgL_variablez00_bglt BgL_arg1746z00_2960;

					BgL_arg1746z00_2960 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_2839)))->BgL_funz00)))->
						BgL_variablez00);
					BgL_efz00_2959 =
						BGl_funzd2effectz12zc0zzeffect_feffectz00(((obj_t)
							BgL_arg1746z00_2960));
				}
				BGl_mergezd2effectsz12zc0zzeffect_feffectz00(BgL_effectz00_2840,
					((obj_t) BgL_efz00_2959));
			}
			return ((BgL_feffectz00_bglt) BgL_effectz00_2840);
		}

	}



/* &body-effect!-app-ly1325 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2appzd2ly1325za2zzeffect_feffectz00(obj_t
		BgL_envz00_2841, obj_t BgL_nodez00_2842, obj_t BgL_effectz00_2843)
	{
		{	/* Effect/feffect.scm 345 */
			BGl_mergezd2effectsz12zc0zzeffect_feffectz00(
				((obj_t)
					((BgL_feffectz00_bglt) BgL_effectz00_2843)),
				((obj_t) BGl_za2effectzd2topza2zd2zzeffect_feffectz00));
			return ((BgL_feffectz00_bglt) BgL_effectz00_2843);
		}

	}



/* &body-effect!-funcall1323 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2funcall1323z70zzeffect_feffectz00(obj_t
		BgL_envz00_2844, obj_t BgL_nodez00_2845, obj_t BgL_effectz00_2846)
	{
		{	/* Effect/feffect.scm 338 */
			BGl_mergezd2effectsz12zc0zzeffect_feffectz00(
				((obj_t)
					((BgL_feffectz00_bglt) BgL_effectz00_2846)),
				((obj_t) BGl_za2effectzd2topza2zd2zzeffect_feffectz00));
			return ((BgL_feffectz00_bglt) BgL_effectz00_2846);
		}

	}



/* &body-effect!-sync1321 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2sync1321z70zzeffect_feffectz00(obj_t
		BgL_envz00_2847, obj_t BgL_nodez00_2848, obj_t BgL_effectz00_2849)
	{
		{	/* Effect/feffect.scm 330 */
			BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2848)))->BgL_mutexz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2849));
			BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2848)))->BgL_prelockz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2849));
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2848)))->BgL_bodyz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2849));
		}

	}



/* &body-effect!-sequenc1319 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2sequenc1319z70zzeffect_feffectz00(obj_t
		BgL_envz00_2850, obj_t BgL_nodez00_2851, obj_t BgL_effectz00_2852)
	{
		{	/* Effect/feffect.scm 324 */
			{	/* Effect/feffect.scm 325 */
				obj_t BgL_arg1736z00_2969;

				BgL_arg1736z00_2969 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2851)))->BgL_nodesz00);
				return
					((BgL_feffectz00_bglt)
					BGl_bodyzd2effectza2z12z62zzeffect_feffectz00(BgL_arg1736z00_2969,
						((obj_t) ((BgL_feffectz00_bglt) BgL_effectz00_2852))));
			}
		}

	}



/* &body-effect!-cast1317 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2cast1317z70zzeffect_feffectz00(obj_t
		BgL_envz00_2853, obj_t BgL_nodez00_2854, obj_t BgL_effectz00_2855)
	{
		{	/* Effect/feffect.scm 317 */
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2854)))->BgL_argz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2855));
		}

	}



/* &body-effect!-box-set1315 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2boxzd2set1315za2zzeffect_feffectz00(obj_t
		BgL_envz00_2856, obj_t BgL_nodez00_2857, obj_t BgL_effectz00_2858)
	{
		{	/* Effect/feffect.scm 309 */
			{	/* Effect/feffect.scm 311 */
				BgL_varz00_bglt BgL_arg1733z00_2974;

				BgL_arg1733z00_2974 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2857)))->BgL_varz00);
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
					((BgL_nodez00_bglt) BgL_arg1733z00_2974),
					((BgL_feffectz00_bglt) BgL_effectz00_2858));
			}
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2857)))->BgL_valuez00),
				((BgL_feffectz00_bglt) BgL_effectz00_2858));
		}

	}



/* &body-effect!-box-ref1313 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2boxzd2ref1313za2zzeffect_feffectz00(obj_t
		BgL_envz00_2859, obj_t BgL_nodez00_2860, obj_t BgL_effectz00_2861)
	{
		{	/* Effect/feffect.scm 303 */
			{	/* Effect/feffect.scm 304 */
				BgL_varz00_bglt BgL_arg1724z00_2977;

				BgL_arg1724z00_2977 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2860)))->BgL_varz00);
				return
					BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
					((BgL_nodez00_bglt) BgL_arg1724z00_2977),
					((BgL_feffectz00_bglt) BgL_effectz00_2861));
			}
		}

	}



/* &body-effect!-make-bo1311 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2makezd2bo1311za2zzeffect_feffectz00(obj_t
		BgL_envz00_2862, obj_t BgL_nodez00_2863, obj_t BgL_effectz00_2864)
	{
		{	/* Effect/feffect.scm 297 */
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2863)))->BgL_valuez00),
				((BgL_feffectz00_bglt) BgL_effectz00_2864));
		}

	}



/* &body-effect!-jump-ex1309 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2jumpzd2ex1309za2zzeffect_feffectz00(obj_t
		BgL_envz00_2865, obj_t BgL_nodez00_2866, obj_t BgL_effectz00_2867)
	{
		{	/* Effect/feffect.scm 289 */
			BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2866)))->BgL_exitz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2867));
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2866)))->BgL_valuez00),
				((BgL_feffectz00_bglt) BgL_effectz00_2867));
		}

	}



/* &body-effect!-set-ex-1307 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2setzd2exzd21307z70zzeffect_feffectz00(obj_t
		BgL_envz00_2868, obj_t BgL_nodez00_2869, obj_t BgL_effectz00_2870)
	{
		{	/* Effect/feffect.scm 281 */
			BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2869)))->BgL_onexitz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2870));
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2869)))->BgL_bodyz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2870));
		}

	}



/* &body-effect!-fail1305 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2fail1305z70zzeffect_feffectz00(obj_t
		BgL_envz00_2871, obj_t BgL_nodez00_2872, obj_t BgL_effectz00_2873)
	{
		{	/* Effect/feffect.scm 272 */
			BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2872)))->BgL_procz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2873));
			BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2872)))->BgL_msgz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2873));
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2872)))->BgL_objz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2873));
		}

	}



/* &body-effect!-switch1303 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2switch1303z70zzeffect_feffectz00(obj_t
		BgL_envz00_2874, obj_t BgL_nodez00_2875, obj_t BgL_effectz00_2876)
	{
		{	/* Effect/feffect.scm 261 */
			BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2875)))->BgL_testz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2876));
			{	/* Effect/feffect.scm 264 */
				obj_t BgL_g1277z00_2988;

				BgL_g1277z00_2988 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2875)))->BgL_clausesz00);
				{
					obj_t BgL_l1275z00_2990;

					BgL_l1275z00_2990 = BgL_g1277z00_2988;
				BgL_zc3z04anonymousza31702ze3z87_2989:
					if (PAIRP(BgL_l1275z00_2990))
						{	/* Effect/feffect.scm 264 */
							{	/* Effect/feffect.scm 265 */
								obj_t BgL_clausez00_2991;

								BgL_clausez00_2991 = CAR(BgL_l1275z00_2990);
								{	/* Effect/feffect.scm 265 */
									obj_t BgL_arg1705z00_2992;

									BgL_arg1705z00_2992 = CDR(((obj_t) BgL_clausez00_2991));
									BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
										((BgL_nodez00_bglt) BgL_arg1705z00_2992),
										((BgL_feffectz00_bglt) BgL_effectz00_2876));
								}
							}
							{
								obj_t BgL_l1275z00_3738;

								BgL_l1275z00_3738 = CDR(BgL_l1275z00_2990);
								BgL_l1275z00_2990 = BgL_l1275z00_3738;
								goto BgL_zc3z04anonymousza31702ze3z87_2989;
							}
						}
					else
						{	/* Effect/feffect.scm 264 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_feffectz00_bglt) BgL_effectz00_2876);
		}

	}



/* &body-effect!-conditi1301 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2conditi1301z70zzeffect_feffectz00(obj_t
		BgL_envz00_2877, obj_t BgL_nodez00_2878, obj_t BgL_effectz00_2879)
	{
		{	/* Effect/feffect.scm 252 */
			BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2878)))->BgL_testz00),
				((BgL_feffectz00_bglt) BgL_effectz00_2879));
			BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2878)))->BgL_truez00),
				((BgL_feffectz00_bglt) BgL_effectz00_2879));
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2878)))->BgL_falsez00),
				((BgL_feffectz00_bglt) BgL_effectz00_2879));
		}

	}



/* &body-effect!-setq1299 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2setq1299z70zzeffect_feffectz00(obj_t
		BgL_envz00_2880, obj_t BgL_nodez00_2881, obj_t BgL_effectz00_2882)
	{
		{	/* Effect/feffect.scm 243 */
			{	/* Effect/feffect.scm 245 */
				bool_t BgL_test2109z00_3753;

				{	/* Effect/feffect.scm 245 */
					BgL_variablez00_bglt BgL_arg1688z00_2997;

					BgL_arg1688z00_2997 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_setqz00_bglt) COBJECT(
											((BgL_setqz00_bglt) BgL_nodez00_2881)))->BgL_varz00)))->
						BgL_variablez00);
					{	/* Effect/feffect.scm 245 */
						obj_t BgL_classz00_2998;

						BgL_classz00_2998 = BGl_globalz00zzast_varz00;
						{	/* Effect/feffect.scm 245 */
							BgL_objectz00_bglt BgL_arg1807z00_2999;

							{	/* Effect/feffect.scm 245 */
								obj_t BgL_tmpz00_3757;

								BgL_tmpz00_3757 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1688z00_2997));
								BgL_arg1807z00_2999 = (BgL_objectz00_bglt) (BgL_tmpz00_3757);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Effect/feffect.scm 245 */
									long BgL_idxz00_3000;

									BgL_idxz00_3000 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2999);
									BgL_test2109z00_3753 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3000 + 2L)) == BgL_classz00_2998);
								}
							else
								{	/* Effect/feffect.scm 245 */
									bool_t BgL_res1897z00_3003;

									{	/* Effect/feffect.scm 245 */
										obj_t BgL_oclassz00_3004;

										{	/* Effect/feffect.scm 245 */
											obj_t BgL_arg1815z00_3005;
											long BgL_arg1816z00_3006;

											BgL_arg1815z00_3005 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Effect/feffect.scm 245 */
												long BgL_arg1817z00_3007;

												BgL_arg1817z00_3007 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2999);
												BgL_arg1816z00_3006 =
													(BgL_arg1817z00_3007 - OBJECT_TYPE);
											}
											BgL_oclassz00_3004 =
												VECTOR_REF(BgL_arg1815z00_3005, BgL_arg1816z00_3006);
										}
										{	/* Effect/feffect.scm 245 */
											bool_t BgL__ortest_1115z00_3008;

											BgL__ortest_1115z00_3008 =
												(BgL_classz00_2998 == BgL_oclassz00_3004);
											if (BgL__ortest_1115z00_3008)
												{	/* Effect/feffect.scm 245 */
													BgL_res1897z00_3003 = BgL__ortest_1115z00_3008;
												}
											else
												{	/* Effect/feffect.scm 245 */
													long BgL_odepthz00_3009;

													{	/* Effect/feffect.scm 245 */
														obj_t BgL_arg1804z00_3010;

														BgL_arg1804z00_3010 = (BgL_oclassz00_3004);
														BgL_odepthz00_3009 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3010);
													}
													if ((2L < BgL_odepthz00_3009))
														{	/* Effect/feffect.scm 245 */
															obj_t BgL_arg1802z00_3011;

															{	/* Effect/feffect.scm 245 */
																obj_t BgL_arg1803z00_3012;

																BgL_arg1803z00_3012 = (BgL_oclassz00_3004);
																BgL_arg1802z00_3011 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3012,
																	2L);
															}
															BgL_res1897z00_3003 =
																(BgL_arg1802z00_3011 == BgL_classz00_2998);
														}
													else
														{	/* Effect/feffect.scm 245 */
															BgL_res1897z00_3003 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2109z00_3753 = BgL_res1897z00_3003;
								}
						}
					}
				}
				if (BgL_test2109z00_3753)
					{	/* Effect/feffect.scm 245 */
						BGl_mergezd2effectsz12zc0zzeffect_feffectz00(
							((obj_t)
								((BgL_feffectz00_bglt) BgL_effectz00_2882)),
							((obj_t) BGl_za2effectzd2writezd2memza2z00zzeffect_feffectz00));
					}
				else
					{	/* Effect/feffect.scm 245 */
						((bool_t) 0);
					}
			}
			return
				BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2881)))->BgL_valuez00),
				((BgL_feffectz00_bglt) BgL_effectz00_2882));
		}

	}



/* &body-effect!-var1297 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2var1297z70zzeffect_feffectz00(obj_t
		BgL_envz00_2883, obj_t BgL_nodez00_2884, obj_t BgL_effectz00_2885)
	{
		{	/* Effect/feffect.scm 235 */
			{	/* Effect/feffect.scm 236 */
				bool_t BgL_test2113z00_3788;

				{	/* Effect/feffect.scm 236 */
					BgL_variablez00_bglt BgL_arg1675z00_3015;

					BgL_arg1675z00_3015 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_nodez00_2884)))->BgL_variablez00);
					{	/* Effect/feffect.scm 236 */
						obj_t BgL_classz00_3016;

						BgL_classz00_3016 = BGl_globalz00zzast_varz00;
						{	/* Effect/feffect.scm 236 */
							BgL_objectz00_bglt BgL_arg1807z00_3017;

							{	/* Effect/feffect.scm 236 */
								obj_t BgL_tmpz00_3791;

								BgL_tmpz00_3791 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1675z00_3015));
								BgL_arg1807z00_3017 = (BgL_objectz00_bglt) (BgL_tmpz00_3791);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Effect/feffect.scm 236 */
									long BgL_idxz00_3018;

									BgL_idxz00_3018 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3017);
									BgL_test2113z00_3788 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3018 + 2L)) == BgL_classz00_3016);
								}
							else
								{	/* Effect/feffect.scm 236 */
									bool_t BgL_res1896z00_3021;

									{	/* Effect/feffect.scm 236 */
										obj_t BgL_oclassz00_3022;

										{	/* Effect/feffect.scm 236 */
											obj_t BgL_arg1815z00_3023;
											long BgL_arg1816z00_3024;

											BgL_arg1815z00_3023 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Effect/feffect.scm 236 */
												long BgL_arg1817z00_3025;

												BgL_arg1817z00_3025 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3017);
												BgL_arg1816z00_3024 =
													(BgL_arg1817z00_3025 - OBJECT_TYPE);
											}
											BgL_oclassz00_3022 =
												VECTOR_REF(BgL_arg1815z00_3023, BgL_arg1816z00_3024);
										}
										{	/* Effect/feffect.scm 236 */
											bool_t BgL__ortest_1115z00_3026;

											BgL__ortest_1115z00_3026 =
												(BgL_classz00_3016 == BgL_oclassz00_3022);
											if (BgL__ortest_1115z00_3026)
												{	/* Effect/feffect.scm 236 */
													BgL_res1896z00_3021 = BgL__ortest_1115z00_3026;
												}
											else
												{	/* Effect/feffect.scm 236 */
													long BgL_odepthz00_3027;

													{	/* Effect/feffect.scm 236 */
														obj_t BgL_arg1804z00_3028;

														BgL_arg1804z00_3028 = (BgL_oclassz00_3022);
														BgL_odepthz00_3027 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3028);
													}
													if ((2L < BgL_odepthz00_3027))
														{	/* Effect/feffect.scm 236 */
															obj_t BgL_arg1802z00_3029;

															{	/* Effect/feffect.scm 236 */
																obj_t BgL_arg1803z00_3030;

																BgL_arg1803z00_3030 = (BgL_oclassz00_3022);
																BgL_arg1802z00_3029 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3030,
																	2L);
															}
															BgL_res1896z00_3021 =
																(BgL_arg1802z00_3029 == BgL_classz00_3016);
														}
													else
														{	/* Effect/feffect.scm 236 */
															BgL_res1896z00_3021 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2113z00_3788 = BgL_res1896z00_3021;
								}
						}
					}
				}
				if (BgL_test2113z00_3788)
					{	/* Effect/feffect.scm 236 */
						BGl_mergezd2effectsz12zc0zzeffect_feffectz00(
							((obj_t)
								((BgL_feffectz00_bglt) BgL_effectz00_2885)),
							((obj_t) BGl_za2effectzd2readzd2memza2z00zzeffect_feffectz00));
					}
				else
					{	/* Effect/feffect.scm 236 */
						((bool_t) 0);
					}
			}
			return ((BgL_feffectz00_bglt) BgL_effectz00_2885);
		}

	}



/* &body-effect!-kwote1295 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2kwote1295z70zzeffect_feffectz00(obj_t
		BgL_envz00_2886, obj_t BgL_nodez00_2887, obj_t BgL_effectz00_2888)
	{
		{	/* Effect/feffect.scm 229 */
			return ((BgL_feffectz00_bglt) BgL_effectz00_2888);
		}

	}



/* &body-effect!-patch1293 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2patch1293z70zzeffect_feffectz00(obj_t
		BgL_envz00_2889, obj_t BgL_nodez00_2890, obj_t BgL_effectz00_2891)
	{
		{	/* Effect/feffect.scm 221 */
			BGl_mergezd2effectsz12zc0zzeffect_feffectz00(
				((obj_t)
					((BgL_feffectz00_bglt) BgL_effectz00_2891)),
				((obj_t) BGl_za2effectzd2writezd2memza2z00zzeffect_feffectz00));
			{	/* Effect/feffect.scm 224 */
				obj_t BgL_arg1654z00_3034;

				BgL_arg1654z00_3034 =
					(((BgL_atomz00_bglt) COBJECT(
							((BgL_atomz00_bglt)
								((BgL_patchz00_bglt) BgL_nodez00_2890))))->BgL_valuez00);
				return
					BGl_bodyzd2effectz12zc0zzeffect_feffectz00(
					((BgL_nodez00_bglt) BgL_arg1654z00_3034),
					((BgL_feffectz00_bglt) BgL_effectz00_2891));
			}
		}

	}



/* &body-effect!-atom1291 */
	BgL_feffectz00_bglt
		BGl_z62bodyzd2effectz12zd2atom1291z70zzeffect_feffectz00(obj_t
		BgL_envz00_2892, obj_t BgL_nodez00_2893, obj_t BgL_effectz00_2894)
	{
		{	/* Effect/feffect.scm 215 */
			return ((BgL_feffectz00_bglt) BgL_effectz00_2894);
		}

	}



/* &object-display-feffe1287 */
	obj_t BGl_z62objectzd2displayzd2feffe1287z62zzeffect_feffectz00(obj_t
		BgL_envz00_2895, obj_t BgL_fz00_2896, obj_t BgL_pz00_2897)
	{
		{	/* Effect/feffect.scm 63 */
			{	/* Effect/feffect.scm 64 */
				obj_t BgL_arg1627z00_3037;

				if (PAIRP(BgL_pz00_2897))
					{	/* Effect/feffect.scm 64 */
						BgL_arg1627z00_3037 = CAR(BgL_pz00_2897);
					}
				else
					{	/* Effect/feffect.scm 64 */
						obj_t BgL_tmpz00_3834;

						BgL_tmpz00_3834 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1627z00_3037 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3834);
					}
				{	/* Effect/feffect.scm 66 */
					obj_t BgL_zc3z04anonymousza31631ze3z87_3038;

					BgL_zc3z04anonymousza31631ze3z87_3038 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31631ze3ze5zzeffect_feffectz00, (int) (0L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31631ze3z87_3038, (int) (0L),
						((obj_t) ((BgL_feffectz00_bglt) BgL_fz00_2896)));
					return
						BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
						(BgL_arg1627z00_3037, BgL_zc3z04anonymousza31631ze3z87_3038);
				}
			}
		}

	}



/* &<@anonymous:1631> */
	obj_t BGl_z62zc3z04anonymousza31631ze3ze5zzeffect_feffectz00(obj_t
		BgL_envz00_2898)
	{
		{	/* Effect/feffect.scm 65 */
			{	/* Effect/feffect.scm 66 */
				BgL_feffectz00_bglt BgL_fz00_2899;

				BgL_fz00_2899 =
					((BgL_feffectz00_bglt) PROCEDURE_REF(BgL_envz00_2898, (int) (0L)));
				{	/* Effect/feffect.scm 67 */
					obj_t BgL_arg1642z00_3039;
					obj_t BgL_arg1646z00_3040;

					BgL_arg1642z00_3039 =
						(((BgL_feffectz00_bglt) COBJECT(BgL_fz00_2899))->BgL_readz00);
					BgL_arg1646z00_3040 =
						(((BgL_feffectz00_bglt) COBJECT(BgL_fz00_2899))->BgL_writez00);
					{	/* Effect/feffect.scm 67 */
						obj_t BgL_list1647z00_3041;

						{	/* Effect/feffect.scm 67 */
							obj_t BgL_arg1650z00_3042;

							BgL_arg1650z00_3042 = MAKE_YOUNG_PAIR(BgL_arg1646z00_3040, BNIL);
							BgL_list1647z00_3041 =
								MAKE_YOUNG_PAIR(BgL_arg1642z00_3039, BgL_arg1650z00_3042);
						}
						return
							BGl_printfz00zz__r4_output_6_10_3z00
							(BGl_string1992z00zzeffect_feffectz00, BgL_list1647z00_3041);
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzeffect_feffectz00(void)
	{
		{	/* Effect/feffect.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1993z00zzeffect_feffectz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1993z00zzeffect_feffectz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1993z00zzeffect_feffectz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1993z00zzeffect_feffectz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1993z00zzeffect_feffectz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1993z00zzeffect_feffectz00));
			return
				BGl_modulezd2initializa7ationz75zzeffect_cgraphz00(271385047L,
				BSTRING_TO_STRING(BGl_string1993z00zzeffect_feffectz00));
		}

	}

#ifdef __cplusplus
}
#endif
