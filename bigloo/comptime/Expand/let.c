/*===========================================================================*/
/*   (Expand/let.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/let.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_LET_TYPE_DEFINITIONS
#define BGL_EXPAND_LET_TYPE_DEFINITIONS
#endif													// BGL_EXPAND_LET_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62zc3z04anonymousza31846ze3ze5zzexpand_letz00(obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_letz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2letreczb0zzexpand_letz00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzexpand_letz00(void);
	extern obj_t BGl_epairifyz00zztools_miscz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_letz00(void);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_letz00(void);
	static obj_t BGl_z62expandzd2labelszb0zzexpand_letz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_letz00(void);
	extern obj_t BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2letreczd2zzexpand_letz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2letza2z70zzexpand_letz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
	extern obj_t BGl_replacez12z12zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzexpand_letz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_withzd2lexicalzd2zzexpand_epsz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2labelszd2zzexpand_letz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_letz00(void);
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_letz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_letz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_letz00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2letzd2zzexpand_letz00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2letza2z12zzexpand_letz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31813ze3ze5zzexpand_letz00(obj_t);
	static obj_t BGl_z62expandzd2letzb0zzexpand_letz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31662ze3ze5zzexpand_letz00(obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31590ze3ze5zzexpand_letz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31738ze3ze5zzexpand_letz00(obj_t);
	BGL_IMPORT bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	BGL_IMPORT obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t __cnst[6];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2labelszd2envz00zzexpand_letz00,
		BgL_bgl_za762expandza7d2labe2016z00,
		BGl_z62expandzd2labelszb0zzexpand_letz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2004z00zzexpand_letz00,
		BgL_bgl_string2004za700za7za7e2017za7, "Illegal `let*' form", 19);
	      DEFINE_STRING(BGl_string2005z00zzexpand_letz00,
		BgL_bgl_string2005za700za7za7e2018za7, "Illegal `named let' binding", 27);
	      DEFINE_STRING(BGl_string2007z00zzexpand_letz00,
		BgL_bgl_string2007za700za7za7e2019za7, "Illegal `let' binding", 21);
	      DEFINE_STRING(BGl_string2008z00zzexpand_letz00,
		BgL_bgl_string2008za700za7za7e2020za7, "Illegal `let' form", 18);
	      DEFINE_STRING(BGl_string2009z00zzexpand_letz00,
		BgL_bgl_string2009za700za7za7e2021za7, "Illegal `letrec' binding", 24);
	      DEFINE_STRING(BGl_string2010z00zzexpand_letz00,
		BgL_bgl_string2010za700za7za7e2022za7, "Illegal `letrec' form", 21);
	      DEFINE_STRING(BGl_string2011z00zzexpand_letz00,
		BgL_bgl_string2011za700za7za7e2023za7, "Illegal `labels' binding", 24);
	      DEFINE_STRING(BGl_string2012z00zzexpand_letz00,
		BgL_bgl_string2012za700za7za7e2024za7, "Illegal `labels' form", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2006z00zzexpand_letz00,
		BgL_bgl_za762za7c3za704anonymo2025za7,
		BGl_z62zc3z04anonymousza31590ze3ze5zzexpand_letz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2013z00zzexpand_letz00,
		BgL_bgl_string2013za700za7za7e2026za7, "expand_let", 10);
	      DEFINE_STRING(BGl_string2014z00zzexpand_letz00,
		BgL_bgl_string2014za700za7za7e2027za7, "labels _ letrec lambda let* let ",
		32);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2letreczd2envz00zzexpand_letz00,
		BgL_bgl_za762expandza7d2letr2028z00,
		BGl_z62expandzd2letreczb0zzexpand_letz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2letzd2envz00zzexpand_letz00,
		BgL_bgl_za762expandza7d2letza72029za7,
		BGl_z62expandzd2letzb0zzexpand_letz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2letza2zd2envza2zzexpand_letz00,
		BgL_bgl_za762expandza7d2letza72030za7,
		BGl_z62expandzd2letza2z12zzexpand_letz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzexpand_letz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzexpand_letz00(long
		BgL_checksumz00_2398, char *BgL_fromz00_2399)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_letz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_letz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_letz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_letz00();
					BGl_cnstzd2initzd2zzexpand_letz00();
					BGl_importedzd2moduleszd2initz00zzexpand_letz00();
					return BGl_toplevelzd2initzd2zzexpand_letz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_letz00(void)
	{
		{	/* Expand/let.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_let");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"expand_let");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_let");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "expand_let");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_let");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "expand_let");
			BGl_modulezd2initializa7ationz75zz__dssslz00(0L, "expand_let");
			BGl_modulezd2initializa7ationz75zz__prognz00(0L, "expand_let");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "expand_let");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "expand_let");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_letz00(void)
	{
		{	/* Expand/let.scm 15 */
			{	/* Expand/let.scm 15 */
				obj_t BgL_cportz00_2348;

				{	/* Expand/let.scm 15 */
					obj_t BgL_stringz00_2355;

					BgL_stringz00_2355 = BGl_string2014z00zzexpand_letz00;
					{	/* Expand/let.scm 15 */
						obj_t BgL_startz00_2356;

						BgL_startz00_2356 = BINT(0L);
						{	/* Expand/let.scm 15 */
							obj_t BgL_endz00_2357;

							BgL_endz00_2357 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2355)));
							{	/* Expand/let.scm 15 */

								BgL_cportz00_2348 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2355, BgL_startz00_2356, BgL_endz00_2357);
				}}}}
				{
					long BgL_iz00_2349;

					BgL_iz00_2349 = 5L;
				BgL_loopz00_2350:
					if ((BgL_iz00_2349 == -1L))
						{	/* Expand/let.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/let.scm 15 */
							{	/* Expand/let.scm 15 */
								obj_t BgL_arg2015z00_2351;

								{	/* Expand/let.scm 15 */

									{	/* Expand/let.scm 15 */
										obj_t BgL_locationz00_2353;

										BgL_locationz00_2353 = BBOOL(((bool_t) 0));
										{	/* Expand/let.scm 15 */

											BgL_arg2015z00_2351 =
												BGl_readz00zz__readerz00(BgL_cportz00_2348,
												BgL_locationz00_2353);
										}
									}
								}
								{	/* Expand/let.scm 15 */
									int BgL_tmpz00_2427;

									BgL_tmpz00_2427 = (int) (BgL_iz00_2349);
									CNST_TABLE_SET(BgL_tmpz00_2427, BgL_arg2015z00_2351);
							}}
							{	/* Expand/let.scm 15 */
								int BgL_auxz00_2354;

								BgL_auxz00_2354 = (int) ((BgL_iz00_2349 - 1L));
								{
									long BgL_iz00_2432;

									BgL_iz00_2432 = (long) (BgL_auxz00_2354);
									BgL_iz00_2349 = BgL_iz00_2432;
									goto BgL_loopz00_2350;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_letz00(void)
	{
		{	/* Expand/let.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzexpand_letz00(void)
	{
		{	/* Expand/let.scm 15 */
			return BUNSPEC;
		}

	}



/* expand-let* */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2letza2z70zzexpand_letz00(obj_t
		BgL_xz00_17, obj_t BgL_ez00_18)
	{
		{	/* Expand/let.scm 32 */
			{	/* Expand/let.scm 33 */
				obj_t BgL_oldzd2internalzd2_1386;

				BgL_oldzd2internalzd2_1386 =
					BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
				BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 = BTRUE;
				{	/* Expand/let.scm 35 */
					obj_t BgL_ez00_1387;

					BgL_ez00_1387 =
						BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(BgL_ez00_18);
					{	/* Expand/let.scm 35 */
						obj_t BgL_resz00_1388;

						{
							obj_t BgL_bindingsz00_1391;
							obj_t BgL_bodyz00_1392;
							obj_t BgL_bodyz00_1389;

							if (PAIRP(BgL_xz00_17))
								{	/* Expand/let.scm 36 */
									obj_t BgL_cdrzd2109zd2_1397;

									BgL_cdrzd2109zd2_1397 = CDR(((obj_t) BgL_xz00_17));
									if (PAIRP(BgL_cdrzd2109zd2_1397))
										{	/* Expand/let.scm 36 */
											obj_t BgL_cdrzd2112zd2_1399;

											BgL_cdrzd2112zd2_1399 = CDR(BgL_cdrzd2109zd2_1397);
											if (NULLP(CAR(BgL_cdrzd2109zd2_1397)))
												{	/* Expand/let.scm 36 */
													if (NULLP(BgL_cdrzd2112zd2_1399))
														{	/* Expand/let.scm 36 */
														BgL_tagzd2103zd2_1394:
															BgL_resz00_1388 =
																BGl_errorz00zz__errorz00(BFALSE,
																BGl_string2004z00zzexpand_letz00, BgL_xz00_17);
														}
													else
														{	/* Expand/let.scm 36 */
															BgL_bodyz00_1389 = BgL_cdrzd2112zd2_1399;
															{	/* Expand/let.scm 38 */
																obj_t BgL_arg1323z00_1409;

																{	/* Expand/let.scm 38 */
																	obj_t BgL_arg1325z00_1410;

																	{	/* Expand/let.scm 38 */
																		obj_t BgL_arg1326z00_1411;

																		{	/* Expand/let.scm 38 */
																			obj_t BgL_arg1327z00_1412;

																			{	/* Expand/let.scm 38 */
																				obj_t BgL_arg1328z00_1413;

																				BgL_arg1328z00_1413 =
																					BGl_expandzd2prognzd2zz__prognz00
																					(BgL_bodyz00_1389);
																				BgL_arg1327z00_1412 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_1387,
																					BgL_arg1328z00_1413, BgL_ez00_1387);
																			}
																			BgL_arg1326z00_1411 =
																				MAKE_YOUNG_PAIR(BgL_arg1327z00_1412,
																				BNIL);
																		}
																		BgL_arg1325z00_1410 =
																			MAKE_YOUNG_PAIR(BNIL,
																			BgL_arg1326z00_1411);
																	}
																	BgL_arg1323z00_1409 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																		BgL_arg1325z00_1410);
																}
																BgL_resz00_1388 =
																	BGl_replacez12z12zztools_miscz00(BgL_xz00_17,
																	BgL_arg1323z00_1409);
															}
														}
												}
											else
												{	/* Expand/let.scm 36 */
													obj_t BgL_carzd2127zd2_1404;
													obj_t BgL_cdrzd2128zd2_1405;

													BgL_carzd2127zd2_1404 =
														CAR(((obj_t) BgL_cdrzd2109zd2_1397));
													BgL_cdrzd2128zd2_1405 =
														CDR(((obj_t) BgL_cdrzd2109zd2_1397));
													if (PAIRP(BgL_carzd2127zd2_1404))
														{	/* Expand/let.scm 36 */
															if (NULLP(BgL_cdrzd2128zd2_1405))
																{	/* Expand/let.scm 36 */
																	goto BgL_tagzd2103zd2_1394;
																}
															else
																{	/* Expand/let.scm 36 */
																	BgL_bindingsz00_1391 = BgL_carzd2127zd2_1404;
																	BgL_bodyz00_1392 = BgL_cdrzd2128zd2_1405;
																	{	/* Expand/let.scm 40 */
																		obj_t BgL_slz00_1414;
																		obj_t BgL_bz00_1415;

																		{	/* Expand/let.scm 41 */
																			obj_t BgL_arg1333z00_1419;
																			obj_t BgL_arg1335z00_1420;

																			BgL_arg1333z00_1419 =
																				MAKE_YOUNG_PAIR(CDR
																				(BgL_bindingsz00_1391),
																				BgL_bodyz00_1392);
																			BgL_arg1335z00_1420 =
																				BGl_findzd2locationzd2zztools_locationz00
																				(CAR(BgL_bindingsz00_1391));
																			{	/* Expand/let.scm 40 */
																				obj_t BgL_res2001z00_2134;

																				{	/* Expand/let.scm 40 */
																					obj_t BgL_obj1z00_2133;

																					BgL_obj1z00_2133 = CNST_TABLE_REF(1);
																					BgL_res2001z00_2134 =
																						MAKE_YOUNG_EPAIR(BgL_obj1z00_2133,
																						BgL_arg1333z00_1419,
																						BgL_arg1335z00_1420);
																				}
																				BgL_slz00_1414 = BgL_res2001z00_2134;
																			}
																		}
																		{	/* Expand/let.scm 43 */
																			obj_t BgL_arg1342z00_1423;

																			BgL_arg1342z00_1423 =
																				MAKE_YOUNG_PAIR(CAR
																				(BgL_bindingsz00_1391), BNIL);
																			BgL_bz00_1415 =
																				BGl_epairifyz00zztools_miscz00
																				(BgL_arg1342z00_1423,
																				BgL_bindingsz00_1391);
																		}
																		{	/* Expand/let.scm 44 */
																			obj_t BgL_arg1329z00_1416;

																			{	/* Expand/let.scm 44 */
																				obj_t BgL_arg1331z00_1417;

																				{	/* Expand/let.scm 44 */
																					obj_t BgL_arg1332z00_1418;

																					BgL_arg1332z00_1418 =
																						MAKE_YOUNG_PAIR(BgL_slz00_1414,
																						BNIL);
																					BgL_arg1331z00_1417 =
																						MAKE_YOUNG_PAIR(BgL_bz00_1415,
																						BgL_arg1332z00_1418);
																				}
																				BgL_arg1329z00_1416 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																					BgL_arg1331z00_1417);
																			}
																			BgL_resz00_1388 =
																				BGL_PROCEDURE_CALL2(BgL_ez00_1387,
																				BgL_arg1329z00_1416, BgL_ez00_1387);
																		}
																	}
																}
														}
													else
														{	/* Expand/let.scm 36 */
															goto BgL_tagzd2103zd2_1394;
														}
												}
										}
									else
										{	/* Expand/let.scm 36 */
											goto BgL_tagzd2103zd2_1394;
										}
								}
							else
								{	/* Expand/let.scm 36 */
									goto BgL_tagzd2103zd2_1394;
								}
						}
						{	/* Expand/let.scm 36 */

							BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 =
								BgL_oldzd2internalzd2_1386;
							return BGl_replacez12z12zztools_miscz00(BgL_xz00_17,
								BgL_resz00_1388);
						}
					}
				}
			}
		}

	}



/* &expand-let* */
	obj_t BGl_z62expandzd2letza2z12zzexpand_letz00(obj_t BgL_envz00_2309,
		obj_t BgL_xz00_2310, obj_t BgL_ez00_2311)
	{
		{	/* Expand/let.scm 32 */
			return
				BGl_expandzd2letza2z70zzexpand_letz00(BgL_xz00_2310, BgL_ez00_2311);
		}

	}



/* expand-let */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2letzd2zzexpand_letz00(obj_t BgL_xz00_19,
		obj_t BgL_ez00_20)
	{
		{	/* Expand/let.scm 53 */
			{
				obj_t BgL_ez00_1592;
				obj_t BgL_bindingsz00_1593;
				obj_t BgL_bodyz00_1594;
				obj_t BgL_ez00_1497;
				obj_t BgL_loopz00_1498;
				obj_t BgL_bindingsz00_1499;
				obj_t BgL_bodyz00_1500;

				{	/* Expand/let.scm 83 */
					obj_t BgL_oldzd2internalzd2_1427;

					BgL_oldzd2internalzd2_1427 =
						BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
					BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 = BTRUE;
					{	/* Expand/let.scm 85 */
						obj_t BgL_ez00_1428;

						BgL_ez00_1428 =
							BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(BgL_ez00_20);
						{	/* Expand/let.scm 85 */
							obj_t BgL_resz00_1429;

							{
								obj_t BgL_bodyz00_1430;

								if (PAIRP(BgL_xz00_19))
									{	/* Expand/let.scm 86 */
										obj_t BgL_cdrzd2175zd2_1442;

										BgL_cdrzd2175zd2_1442 = CDR(((obj_t) BgL_xz00_19));
										if (PAIRP(BgL_cdrzd2175zd2_1442))
											{	/* Expand/let.scm 86 */
												obj_t BgL_cdrzd2178zd2_1444;

												BgL_cdrzd2178zd2_1444 = CDR(BgL_cdrzd2175zd2_1442);
												if (NULLP(CAR(BgL_cdrzd2175zd2_1442)))
													{	/* Expand/let.scm 86 */
														if (NULLP(BgL_cdrzd2178zd2_1444))
															{	/* Expand/let.scm 86 */
																obj_t BgL_carzd2192zd2_1449;
																obj_t BgL_cdrzd2193zd2_1450;

																BgL_carzd2192zd2_1449 =
																	CAR(((obj_t) BgL_cdrzd2175zd2_1442));
																BgL_cdrzd2193zd2_1450 =
																	CDR(((obj_t) BgL_cdrzd2175zd2_1442));
																if (SYMBOLP(BgL_carzd2192zd2_1449))
																	{	/* Expand/let.scm 86 */
																		if (PAIRP(BgL_cdrzd2193zd2_1450))
																			{	/* Expand/let.scm 86 */
																				obj_t BgL_carzd2199zd2_1453;
																				obj_t BgL_cdrzd2200zd2_1454;

																				BgL_carzd2199zd2_1453 =
																					CAR(BgL_cdrzd2193zd2_1450);
																				BgL_cdrzd2200zd2_1454 =
																					CDR(BgL_cdrzd2193zd2_1450);
																				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2199zd2_1453))
																					{	/* Expand/let.scm 86 */
																						if (NULLP(BgL_cdrzd2200zd2_1454))
																							{	/* Expand/let.scm 86 */
																								obj_t BgL_cdrzd2211zd2_1457;

																								BgL_cdrzd2211zd2_1457 =
																									CDR(((obj_t) BgL_xz00_19));
																								{	/* Expand/let.scm 86 */
																									obj_t BgL_carzd2214zd2_1458;

																									BgL_carzd2214zd2_1458 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd2211zd2_1457));
																									if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2214zd2_1458))
																										{	/* Expand/let.scm 86 */
																											obj_t BgL_arg1361z00_1460;

																											BgL_arg1361z00_1460 =
																												CDR(
																												((obj_t)
																													BgL_cdrzd2211zd2_1457));
																											BgL_ez00_1592 =
																												BgL_ez00_1428;
																											BgL_bindingsz00_1593 =
																												BgL_carzd2214zd2_1458;
																											BgL_bodyz00_1594 =
																												BgL_arg1361z00_1460;
																										BgL_zc3z04anonymousza31617ze3z87_1595:
																											{	/* Expand/let.scm 72 */
																												obj_t BgL_varsz00_1596;

																												if (NULLP
																													(BgL_bindingsz00_1593))
																													{	/* Expand/let.scm 72 */
																														BgL_varsz00_1596 =
																															BNIL;
																													}
																												else
																													{	/* Expand/let.scm 72 */
																														obj_t
																															BgL_head1239z00_1624;
																														BgL_head1239z00_1624
																															=
																															MAKE_YOUNG_PAIR
																															(BNIL, BNIL);
																														{
																															obj_t
																																BgL_l1237z00_1626;
																															obj_t
																																BgL_tail1240z00_1627;
																															BgL_l1237z00_1626
																																=
																																BgL_bindingsz00_1593;
																															BgL_tail1240z00_1627
																																=
																																BgL_head1239z00_1624;
																														BgL_zc3z04anonymousza31665ze3z87_1628:
																															if (NULLP
																																(BgL_l1237z00_1626))
																																{	/* Expand/let.scm 72 */
																																	BgL_varsz00_1596
																																		=
																																		CDR
																																		(BgL_head1239z00_1624);
																																}
																															else
																																{	/* Expand/let.scm 72 */
																																	obj_t
																																		BgL_newtail1241z00_1630;
																																	{	/* Expand/let.scm 72 */
																																		obj_t
																																			BgL_arg1678z00_1632;
																																		{	/* Expand/let.scm 72 */
																																			obj_t
																																				BgL_xz00_1633;
																																			BgL_xz00_1633
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_l1237z00_1626));
																																			{
																																				obj_t
																																					BgL_valz00_1634;
																																				if (PAIRP(BgL_xz00_1633))
																																					{	/* Expand/let.scm 73 */
																																						obj_t
																																							BgL_cdrzd2160zd2_1640;
																																						BgL_cdrzd2160zd2_1640
																																							=
																																							CDR
																																							(((obj_t) BgL_xz00_1633));
																																						if (PAIRP(BgL_cdrzd2160zd2_1640))
																																							{	/* Expand/let.scm 73 */
																																								if (NULLP(CDR(BgL_cdrzd2160zd2_1640)))
																																									{	/* Expand/let.scm 73 */
																																										BgL_valz00_1634
																																											=
																																											CAR
																																											(BgL_cdrzd2160zd2_1640);
																																										{	/* Expand/let.scm 74 */
																																											obj_t
																																												BgL_arg1702z00_1649;
																																											obj_t
																																												BgL_arg1703z00_1650;
																																											BgL_arg1702z00_1649
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_xz00_1633));
																																											BgL_arg1703z00_1650
																																												=
																																												BGL_PROCEDURE_CALL2
																																												(BgL_ez00_1592,
																																												BgL_valz00_1634,
																																												BgL_ez00_1592);
																																											{	/* Expand/let.scm 74 */
																																												obj_t
																																													BgL_tmpz00_2547;
																																												BgL_tmpz00_2547
																																													=
																																													(
																																													(obj_t)
																																													BgL_arg1702z00_1649);
																																												SET_CAR
																																													(BgL_tmpz00_2547,
																																													BgL_arg1703z00_1650);
																																											}
																																										}
																																										BgL_arg1678z00_1632
																																											=
																																											BgL_xz00_1633;
																																									}
																																								else
																																									{	/* Expand/let.scm 73 */
																																									BgL_tagzd2155zd2_1637:
																																										BgL_arg1678z00_1632 = BGl_errorz00zz__errorz00(BFALSE, BGl_string2007z00zzexpand_letz00, BgL_xz00_1633);
																																									}
																																							}
																																						else
																																							{	/* Expand/let.scm 73 */
																																								goto
																																									BgL_tagzd2155zd2_1637;
																																							}
																																					}
																																				else
																																					{	/* Expand/let.scm 73 */
																																						if (SYMBOLP(BgL_xz00_1633))
																																							{	/* Expand/let.scm 73 */
																																								{	/* Expand/let.scm 75 */
																																									obj_t
																																										BgL_list1704z00_1651;
																																									{	/* Expand/let.scm 75 */
																																										obj_t
																																											BgL_arg1705z00_1652;
																																										BgL_arg1705z00_1652
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BUNSPEC,
																																											BNIL);
																																										BgL_list1704z00_1651
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_xz00_1633,
																																											BgL_arg1705z00_1652);
																																									}
																																									BgL_arg1678z00_1632
																																										=
																																										BgL_list1704z00_1651;
																																								}
																																							}
																																						else
																																							{	/* Expand/let.scm 73 */
																																								goto
																																									BgL_tagzd2155zd2_1637;
																																							}
																																					}
																																			}
																																		}
																																		BgL_newtail1241z00_1630
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1678z00_1632,
																																			BNIL);
																																	}
																																	SET_CDR
																																		(BgL_tail1240z00_1627,
																																		BgL_newtail1241z00_1630);
																																	{	/* Expand/let.scm 72 */
																																		obj_t
																																			BgL_arg1675z00_1631;
																																		BgL_arg1675z00_1631
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_l1237z00_1626));
																																		{
																																			obj_t
																																				BgL_tail1240z00_2561;
																																			obj_t
																																				BgL_l1237z00_2560;
																																			BgL_l1237z00_2560
																																				=
																																				BgL_arg1675z00_1631;
																																			BgL_tail1240z00_2561
																																				=
																																				BgL_newtail1241z00_1630;
																																			BgL_tail1240z00_1627
																																				=
																																				BgL_tail1240z00_2561;
																																			BgL_l1237z00_1626
																																				=
																																				BgL_l1237z00_2560;
																																			goto
																																				BgL_zc3z04anonymousza31665ze3z87_1628;
																																		}
																																	}
																																}
																														}
																													}
																												{	/* Expand/let.scm 79 */
																													obj_t
																														BgL_arg1625z00_1597;
																													{	/* Expand/let.scm 79 */
																														obj_t
																															BgL_arg1626z00_1598;
																														{	/* Expand/let.scm 79 */
																															obj_t
																																BgL_arg1627z00_1599;
																															{	/* Expand/let.scm 79 */
																																obj_t
																																	BgL_arg1629z00_1600;
																																obj_t
																																	BgL_arg1630z00_1601;
																																if (NULLP
																																	(BgL_varsz00_1596))
																																	{	/* Expand/let.scm 79 */
																																		BgL_arg1629z00_1600
																																			= BNIL;
																																	}
																																else
																																	{	/* Expand/let.scm 79 */
																																		obj_t
																																			BgL_head1244z00_1605;
																																		{	/* Expand/let.scm 79 */
																																			obj_t
																																				BgL_arg1654z00_1617;
																																			{	/* Expand/let.scm 79 */
																																				obj_t
																																					BgL_pairz00_2181;
																																				BgL_pairz00_2181
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_varsz00_1596));
																																				BgL_arg1654z00_1617
																																					=
																																					CAR
																																					(BgL_pairz00_2181);
																																			}
																																			BgL_head1244z00_1605
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1654z00_1617,
																																				BNIL);
																																		}
																																		{	/* Expand/let.scm 79 */
																																			obj_t
																																				BgL_g1247z00_1606;
																																			BgL_g1247z00_1606
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_varsz00_1596));
																																			{
																																				obj_t
																																					BgL_l1242z00_1608;
																																				obj_t
																																					BgL_tail1245z00_1609;
																																				BgL_l1242z00_1608
																																					=
																																					BgL_g1247z00_1606;
																																				BgL_tail1245z00_1609
																																					=
																																					BgL_head1244z00_1605;
																																			BgL_zc3z04anonymousza31644ze3z87_1610:
																																				if (NULLP(BgL_l1242z00_1608))
																																					{	/* Expand/let.scm 79 */
																																						BgL_arg1629z00_1600
																																							=
																																							BgL_head1244z00_1605;
																																					}
																																				else
																																					{	/* Expand/let.scm 79 */
																																						obj_t
																																							BgL_newtail1246z00_1612;
																																						{	/* Expand/let.scm 79 */
																																							obj_t
																																								BgL_arg1650z00_1614;
																																							{	/* Expand/let.scm 79 */
																																								obj_t
																																									BgL_pairz00_2184;
																																								BgL_pairz00_2184
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_l1242z00_1608));
																																								BgL_arg1650z00_1614
																																									=
																																									CAR
																																									(BgL_pairz00_2184);
																																							}
																																							BgL_newtail1246z00_1612
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1650z00_1614,
																																								BNIL);
																																						}
																																						SET_CDR
																																							(BgL_tail1245z00_1609,
																																							BgL_newtail1246z00_1612);
																																						{	/* Expand/let.scm 79 */
																																							obj_t
																																								BgL_arg1646z00_1613;
																																							BgL_arg1646z00_1613
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_l1242z00_1608));
																																							{
																																								obj_t
																																									BgL_tail1245z00_2580;
																																								obj_t
																																									BgL_l1242z00_2579;
																																								BgL_l1242z00_2579
																																									=
																																									BgL_arg1646z00_1613;
																																								BgL_tail1245z00_2580
																																									=
																																									BgL_newtail1246z00_1612;
																																								BgL_tail1245z00_1609
																																									=
																																									BgL_tail1245z00_2580;
																																								BgL_l1242z00_1608
																																									=
																																									BgL_l1242z00_2579;
																																								goto
																																									BgL_zc3z04anonymousza31644ze3z87_1610;
																																							}
																																						}
																																					}
																																			}
																																		}
																																	}
																																BgL_arg1630z00_1601
																																	=
																																	BGl_findzd2locationzd2zztools_locationz00
																																	(BgL_xz00_19);
																																{	/* Expand/let.scm 82 */
																																	obj_t
																																		BgL_zc3z04anonymousza31662ze3z87_2312;
																																	BgL_zc3z04anonymousza31662ze3z87_2312
																																		=
																																		MAKE_FX_PROCEDURE
																																		(BGl_z62zc3z04anonymousza31662ze3ze5zzexpand_letz00,
																																		(int) (0L),
																																		(int) (2L));
																																	PROCEDURE_SET
																																		(BgL_zc3z04anonymousza31662ze3z87_2312,
																																		(int) (0L),
																																		BgL_bodyz00_1594);
																																	PROCEDURE_SET
																																		(BgL_zc3z04anonymousza31662ze3z87_2312,
																																		(int) (1L),
																																		BgL_ez00_1592);
																																	BgL_arg1627z00_1599
																																		=
																																		BGl_withzd2lexicalzd2zzexpand_epsz00
																																		(BgL_arg1629z00_1600,
																																		CNST_TABLE_REF
																																		(4),
																																		BgL_arg1630z00_1601,
																																		BgL_zc3z04anonymousza31662ze3z87_2312);
																															}}
																															BgL_arg1626z00_1598
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1627z00_1599,
																																BNIL);
																														}
																														BgL_arg1625z00_1597
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_varsz00_1596,
																															BgL_arg1626z00_1598);
																													}
																													BgL_resz00_1429 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(0),
																														BgL_arg1625z00_1597);
																										}}}
																									else
																										{	/* Expand/let.scm 86 */
																										BgL_tagzd2168zd2_1439:
																											BgL_resz00_1429 =
																												BGl_errorz00zz__errorz00
																												(BFALSE,
																												BGl_string2008z00zzexpand_letz00,
																												BgL_xz00_19);
																										}
																								}
																							}
																						else
																							{	/* Expand/let.scm 86 */
																								BgL_ez00_1497 = BgL_ez00_1428;
																								BgL_loopz00_1498 =
																									BgL_carzd2192zd2_1449;
																								BgL_bindingsz00_1499 =
																									BgL_carzd2199zd2_1453;
																								BgL_bodyz00_1500 =
																									BgL_cdrzd2200zd2_1454;
																							BgL_zc3z04anonymousza31449ze3z87_1501:
																								{	/* Expand/let.scm 55 */
																									obj_t BgL_varsz00_1502;

																									if (NULLP
																										(BgL_bindingsz00_1499))
																										{	/* Expand/let.scm 55 */
																											BgL_varsz00_1502 = BNIL;
																										}
																									else
																										{	/* Expand/let.scm 55 */
																											obj_t
																												BgL_head1222z00_1566;
																											BgL_head1222z00_1566 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											{
																												obj_t BgL_l1220z00_1568;
																												obj_t
																													BgL_tail1223z00_1569;
																												BgL_l1220z00_1568 =
																													BgL_bindingsz00_1499;
																												BgL_tail1223z00_1569 =
																													BgL_head1222z00_1566;
																											BgL_zc3z04anonymousza31595ze3z87_1570:
																												if (NULLP
																													(BgL_l1220z00_1568))
																													{	/* Expand/let.scm 55 */
																														BgL_varsz00_1502 =
																															CDR
																															(BgL_head1222z00_1566);
																													}
																												else
																													{	/* Expand/let.scm 55 */
																														obj_t
																															BgL_newtail1224z00_1572;
																														{	/* Expand/let.scm 55 */
																															obj_t
																																BgL_arg1605z00_1574;
																															{	/* Expand/let.scm 55 */
																																obj_t
																																	BgL_xz00_1575;
																																BgL_xz00_1575 =
																																	CAR(((obj_t)
																																		BgL_l1220z00_1568));
																																{
																																	obj_t
																																		BgL_valz00_1576;
																																	if (PAIRP
																																		(BgL_xz00_1575))
																																		{	/* Expand/let.scm 56 */
																																			obj_t
																																				BgL_cdrzd2148zd2_1581;
																																			BgL_cdrzd2148zd2_1581
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_xz00_1575));
																																			if (PAIRP
																																				(BgL_cdrzd2148zd2_1581))
																																				{	/* Expand/let.scm 56 */
																																					if (NULLP(CDR(BgL_cdrzd2148zd2_1581)))
																																						{	/* Expand/let.scm 56 */
																																							BgL_valz00_1576
																																								=
																																								CAR
																																								(BgL_cdrzd2148zd2_1581);
																																							if (PAIRP(BgL_valz00_1576))
																																								{	/* Expand/let.scm 59 */
																																									obj_t
																																										BgL_arg1615z00_1588;
																																									{	/* Expand/let.scm 59 */

																																										{	/* Expand/let.scm 59 */

																																											BgL_arg1615z00_1588
																																												=
																																												BGl_gensymz00zz__r4_symbols_6_4z00
																																												(BFALSE);
																																										}
																																									}
																																									BgL_arg1605z00_1574
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BTRUE,
																																										BgL_arg1615z00_1588);
																																								}
																																							else
																																								{	/* Expand/let.scm 60 */
																																									obj_t
																																										BgL_arg1616z00_1590;
																																									{	/* Expand/let.scm 60 */
																																										obj_t
																																											BgL_pairz00_2147;
																																										BgL_pairz00_2147
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_xz00_1575));
																																										BgL_arg1616z00_1590
																																											=
																																											CAR
																																											(BgL_pairz00_2147);
																																									}
																																									BgL_arg1605z00_1574
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BFALSE,
																																										BgL_arg1616z00_1590);
																																								}
																																						}
																																					else
																																						{	/* Expand/let.scm 56 */
																																						BgL_tagzd2143zd2_1578:
																																							BgL_arg1605z00_1574 = BGl_errorz00zz__errorz00(BFALSE, BGl_string2005z00zzexpand_letz00, BgL_xz00_1575);
																																						}
																																				}
																																			else
																																				{	/* Expand/let.scm 56 */
																																					goto
																																						BgL_tagzd2143zd2_1578;
																																				}
																																		}
																																	else
																																		{	/* Expand/let.scm 56 */
																																			goto
																																				BgL_tagzd2143zd2_1578;
																																		}
																																}
																															}
																															BgL_newtail1224z00_1572
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1605z00_1574,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_tail1223z00_1569,
																															BgL_newtail1224z00_1572);
																														{	/* Expand/let.scm 55 */
																															obj_t
																																BgL_arg1602z00_1573;
																															BgL_arg1602z00_1573
																																=
																																CDR(((obj_t)
																																	BgL_l1220z00_1568));
																															{
																																obj_t
																																	BgL_tail1223z00_2628;
																																obj_t
																																	BgL_l1220z00_2627;
																																BgL_l1220z00_2627
																																	=
																																	BgL_arg1602z00_1573;
																																BgL_tail1223z00_2628
																																	=
																																	BgL_newtail1224z00_1572;
																																BgL_tail1223z00_1569
																																	=
																																	BgL_tail1223z00_2628;
																																BgL_l1220z00_1568
																																	=
																																	BgL_l1220z00_2627;
																																goto
																																	BgL_zc3z04anonymousza31595ze3z87_1570;
																															}
																														}
																													}
																											}
																										}
																									{	/* Expand/let.scm 55 */
																										obj_t BgL_auxz00_1503;

																										{	/* Expand/let.scm 64 */
																											obj_t
																												BgL_list1586z00_1555;
																											{	/* Expand/let.scm 64 */
																												obj_t
																													BgL_arg1589z00_1556;
																												BgL_arg1589z00_1556 =
																													MAKE_YOUNG_PAIR
																													(BgL_bindingsz00_1499,
																													BNIL);
																												BgL_list1586z00_1555 =
																													MAKE_YOUNG_PAIR
																													(BgL_varsz00_1502,
																													BgL_arg1589z00_1556);
																											}
																											BgL_auxz00_1503 =
																												BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																												(BGl_proc2006z00zzexpand_letz00,
																												BgL_list1586z00_1555);
																										}
																										{	/* Expand/let.scm 64 */
																											obj_t BgL_recz00_1504;

																											{	/* Expand/let.scm 67 */
																												obj_t
																													BgL_arg1472z00_1509;
																												{	/* Expand/let.scm 67 */
																													obj_t
																														BgL_arg1473z00_1510;
																													obj_t
																														BgL_arg1485z00_1511;
																													{	/* Expand/let.scm 67 */
																														obj_t
																															BgL_arg1489z00_1512;
																														{	/* Expand/let.scm 67 */
																															obj_t
																																BgL_arg1502z00_1513;
																															{	/* Expand/let.scm 67 */
																																obj_t
																																	BgL_arg1509z00_1514;
																																{	/* Expand/let.scm 67 */
																																	obj_t
																																		BgL_arg1513z00_1515;
																																	{	/* Expand/let.scm 67 */
																																		obj_t
																																			BgL_arg1514z00_1516;
																																		obj_t
																																			BgL_arg1516z00_1517;
																																		if (NULLP
																																			(BgL_bindingsz00_1499))
																																			{	/* Expand/let.scm 67 */
																																				BgL_arg1514z00_1516
																																					=
																																					BNIL;
																																			}
																																		else
																																			{	/* Expand/let.scm 67 */
																																				obj_t
																																					BgL_head1227z00_1520;
																																				{	/* Expand/let.scm 67 */
																																					obj_t
																																						BgL_arg1546z00_1532;
																																					{	/* Expand/let.scm 67 */
																																						obj_t
																																							BgL_pairz00_2157;
																																						BgL_pairz00_2157
																																							=
																																							CAR
																																							(((obj_t) BgL_bindingsz00_1499));
																																						BgL_arg1546z00_1532
																																							=
																																							CAR
																																							(BgL_pairz00_2157);
																																					}
																																					BgL_head1227z00_1520
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1546z00_1532,
																																						BNIL);
																																				}
																																				{	/* Expand/let.scm 67 */
																																					obj_t
																																						BgL_g1230z00_1521;
																																					BgL_g1230z00_1521
																																						=
																																						CDR(
																																						((obj_t) BgL_bindingsz00_1499));
																																					{
																																						obj_t
																																							BgL_l1225z00_1523;
																																						obj_t
																																							BgL_tail1228z00_1524;
																																						BgL_l1225z00_1523
																																							=
																																							BgL_g1230z00_1521;
																																						BgL_tail1228z00_1524
																																							=
																																							BgL_head1227z00_1520;
																																					BgL_zc3z04anonymousza31518ze3z87_1525:
																																						if (NULLP(BgL_l1225z00_1523))
																																							{	/* Expand/let.scm 67 */
																																								BgL_arg1514z00_1516
																																									=
																																									BgL_head1227z00_1520;
																																							}
																																						else
																																							{	/* Expand/let.scm 67 */
																																								obj_t
																																									BgL_newtail1229z00_1527;
																																								{	/* Expand/let.scm 67 */
																																									obj_t
																																										BgL_arg1540z00_1529;
																																									{	/* Expand/let.scm 67 */
																																										obj_t
																																											BgL_pairz00_2160;
																																										BgL_pairz00_2160
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_l1225z00_1523));
																																										BgL_arg1540z00_1529
																																											=
																																											CAR
																																											(BgL_pairz00_2160);
																																									}
																																									BgL_newtail1229z00_1527
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1540z00_1529,
																																										BNIL);
																																								}
																																								SET_CDR
																																									(BgL_tail1228z00_1524,
																																									BgL_newtail1229z00_1527);
																																								{	/* Expand/let.scm 67 */
																																									obj_t
																																										BgL_arg1535z00_1528;
																																									BgL_arg1535z00_1528
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_l1225z00_1523));
																																									{
																																										obj_t
																																											BgL_tail1228z00_2650;
																																										obj_t
																																											BgL_l1225z00_2649;
																																										BgL_l1225z00_2649
																																											=
																																											BgL_arg1535z00_1528;
																																										BgL_tail1228z00_2650
																																											=
																																											BgL_newtail1229z00_1527;
																																										BgL_tail1228z00_1524
																																											=
																																											BgL_tail1228z00_2650;
																																										BgL_l1225z00_1523
																																											=
																																											BgL_l1225z00_2649;
																																										goto
																																											BgL_zc3z04anonymousza31518ze3z87_1525;
																																									}
																																								}
																																							}
																																					}
																																				}
																																			}
																																		BgL_arg1516z00_1517
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_expandzd2prognzd2zz__prognz00
																																			(BgL_bodyz00_1500),
																																			BNIL);
																																		BgL_arg1513z00_1515
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1514z00_1516,
																																			BgL_arg1516z00_1517);
																																	}
																																	BgL_arg1509z00_1514
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(2),
																																		BgL_arg1513z00_1515);
																																}
																																BgL_arg1502z00_1513
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1509z00_1514,
																																	BNIL);
																															}
																															BgL_arg1489z00_1512
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_loopz00_1498,
																																BgL_arg1502z00_1513);
																														}
																														BgL_arg1473z00_1510
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1489z00_1512,
																															BNIL);
																													}
																													{	/* Expand/let.scm 68 */
																														obj_t
																															BgL_arg1559z00_1535;
																														{	/* Expand/let.scm 68 */
																															obj_t
																																BgL_arg1561z00_1536;
																															{	/* Expand/let.scm 68 */
																																obj_t
																																	BgL_arg1564z00_1537;
																																if (NULLP
																																	(BgL_varsz00_1502))
																																	{	/* Expand/let.scm 68 */
																																		BgL_arg1564z00_1537
																																			= BNIL;
																																	}
																																else
																																	{	/* Expand/let.scm 68 */
																																		obj_t
																																			BgL_head1233z00_1540;
																																		{	/* Expand/let.scm 68 */
																																			obj_t
																																				BgL_arg1576z00_1552;
																																			{	/* Expand/let.scm 68 */
																																				obj_t
																																					BgL_pairz00_2164;
																																				BgL_pairz00_2164
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_varsz00_1502));
																																				BgL_arg1576z00_1552
																																					=
																																					CDR
																																					(BgL_pairz00_2164);
																																			}
																																			BgL_head1233z00_1540
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1576z00_1552,
																																				BNIL);
																																		}
																																		{	/* Expand/let.scm 68 */
																																			obj_t
																																				BgL_g1236z00_1541;
																																			BgL_g1236z00_1541
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_varsz00_1502));
																																			{
																																				obj_t
																																					BgL_l1231z00_1543;
																																				obj_t
																																					BgL_tail1234z00_1544;
																																				BgL_l1231z00_1543
																																					=
																																					BgL_g1236z00_1541;
																																				BgL_tail1234z00_1544
																																					=
																																					BgL_head1233z00_1540;
																																			BgL_zc3z04anonymousza31566ze3z87_1545:
																																				if (NULLP(BgL_l1231z00_1543))
																																					{	/* Expand/let.scm 68 */
																																						BgL_arg1564z00_1537
																																							=
																																							BgL_head1233z00_1540;
																																					}
																																				else
																																					{	/* Expand/let.scm 68 */
																																						obj_t
																																							BgL_newtail1235z00_1547;
																																						{	/* Expand/let.scm 68 */
																																							obj_t
																																								BgL_arg1573z00_1549;
																																							{	/* Expand/let.scm 68 */
																																								obj_t
																																									BgL_pairz00_2167;
																																								BgL_pairz00_2167
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_l1231z00_1543));
																																								BgL_arg1573z00_1549
																																									=
																																									CDR
																																									(BgL_pairz00_2167);
																																							}
																																							BgL_newtail1235z00_1547
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1573z00_1549,
																																								BNIL);
																																						}
																																						SET_CDR
																																							(BgL_tail1234z00_1544,
																																							BgL_newtail1235z00_1547);
																																						{	/* Expand/let.scm 68 */
																																							obj_t
																																								BgL_arg1571z00_1548;
																																							BgL_arg1571z00_1548
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_l1231z00_1543));
																																							{
																																								obj_t
																																									BgL_tail1234z00_2677;
																																								obj_t
																																									BgL_l1231z00_2676;
																																								BgL_l1231z00_2676
																																									=
																																									BgL_arg1571z00_1548;
																																								BgL_tail1234z00_2677
																																									=
																																									BgL_newtail1235z00_1547;
																																								BgL_tail1234z00_1544
																																									=
																																									BgL_tail1234z00_2677;
																																								BgL_l1231z00_1543
																																									=
																																									BgL_l1231z00_2676;
																																								goto
																																									BgL_zc3z04anonymousza31566ze3z87_1545;
																																							}
																																						}
																																					}
																																			}
																																		}
																																	}
																																BgL_arg1561z00_1536
																																	=
																																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																	(BgL_arg1564z00_1537,
																																	BNIL);
																															}
																															BgL_arg1559z00_1535
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_loopz00_1498,
																																BgL_arg1561z00_1536);
																														}
																														BgL_arg1485z00_1511
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1559z00_1535,
																															BNIL);
																													}
																													BgL_arg1472z00_1509 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1473z00_1510,
																														BgL_arg1485z00_1511);
																												}
																												BgL_recz00_1504 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(3),
																													BgL_arg1472z00_1509);
																											}
																											{	/* Expand/let.scm 67 */
																												obj_t BgL_expz00_1505;

																												if (NULLP
																													(BgL_auxz00_1503))
																													{	/* Expand/let.scm 69 */
																														BgL_expz00_1505 =
																															BgL_recz00_1504;
																													}
																												else
																													{	/* Expand/let.scm 69 */
																														obj_t
																															BgL_arg1453z00_1507;
																														{	/* Expand/let.scm 69 */
																															obj_t
																																BgL_arg1454z00_1508;
																															BgL_arg1454z00_1508
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_recz00_1504,
																																BNIL);
																															BgL_arg1453z00_1507
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_auxz00_1503,
																																BgL_arg1454z00_1508);
																														}
																														BgL_expz00_1505 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(0),
																															BgL_arg1453z00_1507);
																													}
																												{	/* Expand/let.scm 69 */

																													BgL_resz00_1429 =
																														BGL_PROCEDURE_CALL2
																														(BgL_ez00_1497,
																														BgL_expz00_1505,
																														BgL_ez00_1497);
																												}
																											}
																										}
																									}
																								}
																							}
																					}
																				else
																					{	/* Expand/let.scm 86 */
																						obj_t BgL_cdrzd2226zd2_1461;

																						BgL_cdrzd2226zd2_1461 =
																							CDR(((obj_t) BgL_xz00_19));
																						{	/* Expand/let.scm 86 */
																							obj_t BgL_carzd2229zd2_1462;

																							BgL_carzd2229zd2_1462 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd2226zd2_1461));
																							if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2229zd2_1462))
																								{	/* Expand/let.scm 86 */
																									obj_t BgL_arg1364z00_1464;

																									BgL_arg1364z00_1464 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd2226zd2_1461));
																									{
																										obj_t BgL_bodyz00_2705;
																										obj_t BgL_bindingsz00_2704;
																										obj_t BgL_ez00_2703;

																										BgL_ez00_2703 =
																											BgL_ez00_1428;
																										BgL_bindingsz00_2704 =
																											BgL_carzd2229zd2_1462;
																										BgL_bodyz00_2705 =
																											BgL_arg1364z00_1464;
																										BgL_bodyz00_1594 =
																											BgL_bodyz00_2705;
																										BgL_bindingsz00_1593 =
																											BgL_bindingsz00_2704;
																										BgL_ez00_1592 =
																											BgL_ez00_2703;
																										goto
																											BgL_zc3z04anonymousza31617ze3z87_1595;
																									}
																								}
																							else
																								{	/* Expand/let.scm 86 */
																									goto BgL_tagzd2168zd2_1439;
																								}
																						}
																					}
																			}
																		else
																			{	/* Expand/let.scm 86 */
																				goto BgL_tagzd2168zd2_1439;
																			}
																	}
																else
																	{	/* Expand/let.scm 86 */
																		goto BgL_tagzd2168zd2_1439;
																	}
															}
														else
															{	/* Expand/let.scm 86 */
																BgL_bodyz00_1430 = BgL_cdrzd2178zd2_1444;
																{	/* Expand/let.scm 89 */
																	obj_t BgL_arg1422z00_1493;

																	{	/* Expand/let.scm 89 */
																		obj_t BgL_arg1434z00_1494;

																		{	/* Expand/let.scm 89 */
																			obj_t BgL_arg1437z00_1495;

																			{	/* Expand/let.scm 89 */
																				obj_t BgL_arg1448z00_1496;

																				BgL_arg1448z00_1496 =
																					BGl_expandzd2prognzd2zz__prognz00
																					(BgL_bodyz00_1430);
																				BgL_arg1437z00_1495 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_1428,
																					BgL_arg1448z00_1496, BgL_ez00_1428);
																			}
																			BgL_arg1434z00_1494 =
																				MAKE_YOUNG_PAIR(BgL_arg1437z00_1495,
																				BNIL);
																		}
																		BgL_arg1422z00_1493 =
																			MAKE_YOUNG_PAIR(BNIL,
																			BgL_arg1434z00_1494);
																	}
																	BgL_resz00_1429 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																		BgL_arg1422z00_1493);
																}
															}
													}
												else
													{	/* Expand/let.scm 86 */
														obj_t BgL_carzd2255zd2_1466;
														obj_t BgL_cdrzd2256zd2_1467;

														BgL_carzd2255zd2_1466 =
															CAR(((obj_t) BgL_cdrzd2175zd2_1442));
														BgL_cdrzd2256zd2_1467 =
															CDR(((obj_t) BgL_cdrzd2175zd2_1442));
														if (SYMBOLP(BgL_carzd2255zd2_1466))
															{	/* Expand/let.scm 86 */
																if (PAIRP(BgL_cdrzd2256zd2_1467))
																	{	/* Expand/let.scm 86 */
																		obj_t BgL_carzd2262zd2_1470;
																		obj_t BgL_cdrzd2263zd2_1471;

																		BgL_carzd2262zd2_1470 =
																			CAR(BgL_cdrzd2256zd2_1467);
																		BgL_cdrzd2263zd2_1471 =
																			CDR(BgL_cdrzd2256zd2_1467);
																		if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2262zd2_1470))
																			{	/* Expand/let.scm 86 */
																				if (NULLP(BgL_cdrzd2263zd2_1471))
																					{	/* Expand/let.scm 86 */
																						obj_t BgL_cdrzd2275zd2_1474;

																						BgL_cdrzd2275zd2_1474 =
																							CDR(((obj_t) BgL_xz00_19));
																						{	/* Expand/let.scm 86 */
																							obj_t BgL_carzd2279zd2_1475;

																							BgL_carzd2279zd2_1475 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd2275zd2_1474));
																							if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2279zd2_1475))
																								{	/* Expand/let.scm 86 */
																									obj_t BgL_arg1375z00_1477;

																									BgL_arg1375z00_1477 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd2275zd2_1474));
																									{
																										obj_t BgL_bodyz00_2740;
																										obj_t BgL_bindingsz00_2739;
																										obj_t BgL_ez00_2738;

																										BgL_ez00_2738 =
																											BgL_ez00_1428;
																										BgL_bindingsz00_2739 =
																											BgL_carzd2279zd2_1475;
																										BgL_bodyz00_2740 =
																											BgL_arg1375z00_1477;
																										BgL_bodyz00_1594 =
																											BgL_bodyz00_2740;
																										BgL_bindingsz00_1593 =
																											BgL_bindingsz00_2739;
																										BgL_ez00_1592 =
																											BgL_ez00_2738;
																										goto
																											BgL_zc3z04anonymousza31617ze3z87_1595;
																									}
																								}
																							else
																								{	/* Expand/let.scm 86 */
																									goto BgL_tagzd2168zd2_1439;
																								}
																						}
																					}
																				else
																					{
																						obj_t BgL_bodyz00_2744;
																						obj_t BgL_bindingsz00_2743;
																						obj_t BgL_loopz00_2742;
																						obj_t BgL_ez00_2741;

																						BgL_ez00_2741 = BgL_ez00_1428;
																						BgL_loopz00_2742 =
																							BgL_carzd2255zd2_1466;
																						BgL_bindingsz00_2743 =
																							BgL_carzd2262zd2_1470;
																						BgL_bodyz00_2744 =
																							BgL_cdrzd2263zd2_1471;
																						BgL_bodyz00_1500 = BgL_bodyz00_2744;
																						BgL_bindingsz00_1499 =
																							BgL_bindingsz00_2743;
																						BgL_loopz00_1498 = BgL_loopz00_2742;
																						BgL_ez00_1497 = BgL_ez00_2741;
																						goto
																							BgL_zc3z04anonymousza31449ze3z87_1501;
																					}
																			}
																		else
																			{	/* Expand/let.scm 86 */
																				obj_t BgL_cdrzd2294zd2_1478;

																				BgL_cdrzd2294zd2_1478 =
																					CDR(((obj_t) BgL_xz00_19));
																				{	/* Expand/let.scm 86 */
																					obj_t BgL_carzd2298zd2_1479;

																					BgL_carzd2298zd2_1479 =
																						CAR(
																						((obj_t) BgL_cdrzd2294zd2_1478));
																					if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2298zd2_1479))
																						{	/* Expand/let.scm 86 */
																							obj_t BgL_arg1408z00_1481;

																							BgL_arg1408z00_1481 =
																								CDR(
																								((obj_t)
																									BgL_cdrzd2294zd2_1478));
																							{
																								obj_t BgL_bodyz00_2755;
																								obj_t BgL_bindingsz00_2754;
																								obj_t BgL_ez00_2753;

																								BgL_ez00_2753 = BgL_ez00_1428;
																								BgL_bindingsz00_2754 =
																									BgL_carzd2298zd2_1479;
																								BgL_bodyz00_2755 =
																									BgL_arg1408z00_1481;
																								BgL_bodyz00_1594 =
																									BgL_bodyz00_2755;
																								BgL_bindingsz00_1593 =
																									BgL_bindingsz00_2754;
																								BgL_ez00_1592 = BgL_ez00_2753;
																								goto
																									BgL_zc3z04anonymousza31617ze3z87_1595;
																							}
																						}
																					else
																						{	/* Expand/let.scm 86 */
																							goto BgL_tagzd2168zd2_1439;
																						}
																				}
																			}
																	}
																else
																	{	/* Expand/let.scm 86 */
																		obj_t BgL_carzd2317zd2_1483;
																		obj_t BgL_cdrzd2318zd2_1484;

																		BgL_carzd2317zd2_1483 =
																			CAR(((obj_t) BgL_cdrzd2175zd2_1442));
																		BgL_cdrzd2318zd2_1484 =
																			CDR(((obj_t) BgL_cdrzd2175zd2_1442));
																		if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2317zd2_1483))
																			{	/* Expand/let.scm 86 */
																				if (NULLP(BgL_cdrzd2318zd2_1484))
																					{	/* Expand/let.scm 86 */
																						goto BgL_tagzd2168zd2_1439;
																					}
																				else
																					{
																						obj_t BgL_bodyz00_2766;
																						obj_t BgL_bindingsz00_2765;
																						obj_t BgL_ez00_2764;

																						BgL_ez00_2764 = BgL_ez00_1428;
																						BgL_bindingsz00_2765 =
																							BgL_carzd2317zd2_1483;
																						BgL_bodyz00_2766 =
																							BgL_cdrzd2318zd2_1484;
																						BgL_bodyz00_1594 = BgL_bodyz00_2766;
																						BgL_bindingsz00_1593 =
																							BgL_bindingsz00_2765;
																						BgL_ez00_1592 = BgL_ez00_2764;
																						goto
																							BgL_zc3z04anonymousza31617ze3z87_1595;
																					}
																			}
																		else
																			{	/* Expand/let.scm 86 */
																				goto BgL_tagzd2168zd2_1439;
																			}
																	}
															}
														else
															{	/* Expand/let.scm 86 */
																obj_t BgL_carzd2334zd2_1488;
																obj_t BgL_cdrzd2335zd2_1489;

																BgL_carzd2334zd2_1488 =
																	CAR(((obj_t) BgL_cdrzd2175zd2_1442));
																BgL_cdrzd2335zd2_1489 =
																	CDR(((obj_t) BgL_cdrzd2175zd2_1442));
																if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
																	(BgL_carzd2334zd2_1488))
																	{	/* Expand/let.scm 86 */
																		if (NULLP(BgL_cdrzd2335zd2_1489))
																			{	/* Expand/let.scm 86 */
																				goto BgL_tagzd2168zd2_1439;
																			}
																		else
																			{
																				obj_t BgL_bodyz00_2777;
																				obj_t BgL_bindingsz00_2776;
																				obj_t BgL_ez00_2775;

																				BgL_ez00_2775 = BgL_ez00_1428;
																				BgL_bindingsz00_2776 =
																					BgL_carzd2334zd2_1488;
																				BgL_bodyz00_2777 =
																					BgL_cdrzd2335zd2_1489;
																				BgL_bodyz00_1594 = BgL_bodyz00_2777;
																				BgL_bindingsz00_1593 =
																					BgL_bindingsz00_2776;
																				BgL_ez00_1592 = BgL_ez00_2775;
																				goto
																					BgL_zc3z04anonymousza31617ze3z87_1595;
																			}
																	}
																else
																	{	/* Expand/let.scm 86 */
																		goto BgL_tagzd2168zd2_1439;
																	}
															}
													}
											}
										else
											{	/* Expand/let.scm 86 */
												goto BgL_tagzd2168zd2_1439;
											}
									}
								else
									{	/* Expand/let.scm 86 */
										goto BgL_tagzd2168zd2_1439;
									}
							}
							{	/* Expand/let.scm 86 */

								BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 =
									BgL_oldzd2internalzd2_1427;
								return BGl_replacez12z12zztools_miscz00(BgL_xz00_19,
									BgL_resz00_1429);
							}
						}
					}
				}
			}
		}

	}



/* &expand-let */
	obj_t BGl_z62expandzd2letzb0zzexpand_letz00(obj_t BgL_envz00_2314,
		obj_t BgL_xz00_2315, obj_t BgL_ez00_2316)
	{
		{	/* Expand/let.scm 53 */
			return BGl_expandzd2letzd2zzexpand_letz00(BgL_xz00_2315, BgL_ez00_2316);
		}

	}



/* &<@anonymous:1662> */
	obj_t BGl_z62zc3z04anonymousza31662ze3ze5zzexpand_letz00(obj_t
		BgL_envz00_2317)
	{
		{	/* Expand/let.scm 82 */
			{	/* Expand/let.scm 82 */
				obj_t BgL_bodyz00_2318;
				obj_t BgL_ez00_2319;

				BgL_bodyz00_2318 = PROCEDURE_REF(BgL_envz00_2317, (int) (0L));
				BgL_ez00_2319 = ((obj_t) PROCEDURE_REF(BgL_envz00_2317, (int) (1L)));
				{	/* Expand/let.scm 82 */
					obj_t BgL_arg1663z00_2359;

					BgL_arg1663z00_2359 =
						BGl_expandzd2prognzd2zz__prognz00(BgL_bodyz00_2318);
					return
						BGL_PROCEDURE_CALL2(BgL_ez00_2319, BgL_arg1663z00_2359,
						BgL_ez00_2319);
				}
			}
		}

	}



/* &<@anonymous:1590> */
	obj_t BGl_z62zc3z04anonymousza31590ze3ze5zzexpand_letz00(obj_t
		BgL_envz00_2320, obj_t BgL_xz00_2321, obj_t BgL_yz00_2322)
	{
		{	/* Expand/let.scm 64 */
			{	/* Expand/let.scm 65 */
				obj_t BgL__andtest_1102z00_2360;

				BgL__andtest_1102z00_2360 = CAR(((obj_t) BgL_xz00_2321));
				if (CBOOL(BgL__andtest_1102z00_2360))
					{	/* Expand/let.scm 65 */
						obj_t BgL_arg1591z00_2361;
						obj_t BgL_arg1593z00_2362;

						BgL_arg1591z00_2361 = CDR(((obj_t) BgL_xz00_2321));
						BgL_arg1593z00_2362 = CDR(((obj_t) BgL_yz00_2322));
						return MAKE_YOUNG_PAIR(BgL_arg1591z00_2361, BgL_arg1593z00_2362);
					}
				else
					{	/* Expand/let.scm 65 */
						return BFALSE;
					}
			}
		}

	}



/* expand-letrec */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2letreczd2zzexpand_letz00(obj_t
		BgL_xz00_21, obj_t BgL_ez00_22)
	{
		{	/* Expand/let.scm 104 */
			{
				obj_t BgL_ez00_1680;
				obj_t BgL_bdgsz00_1681;
				obj_t BgL_bodyz00_1682;

				{	/* Expand/let.scm 119 */
					obj_t BgL_oldzd2internalzd2_1657;

					BgL_oldzd2internalzd2_1657 =
						BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
					BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 = BTRUE;
					{	/* Expand/let.scm 121 */
						obj_t BgL_ez00_1658;

						BgL_ez00_1658 =
							BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(BgL_ez00_22);
						{	/* Expand/let.scm 121 */
							obj_t BgL_resz00_1659;

							{

								if (PAIRP(BgL_xz00_21))
									{	/* Expand/let.scm 122 */
										obj_t BgL_cdrzd2381zd2_1668;

										BgL_cdrzd2381zd2_1668 = CDR(((obj_t) BgL_xz00_21));
										if (PAIRP(BgL_cdrzd2381zd2_1668))
											{	/* Expand/let.scm 122 */
												obj_t BgL_cdrzd2384zd2_1670;

												BgL_cdrzd2384zd2_1670 = CDR(BgL_cdrzd2381zd2_1668);
												if (NULLP(CAR(BgL_cdrzd2381zd2_1668)))
													{	/* Expand/let.scm 122 */
														if (NULLP(BgL_cdrzd2384zd2_1670))
															{	/* Expand/let.scm 122 */
															BgL_tagzd2375zd2_1665:
																BgL_resz00_1659 =
																	BGl_errorz00zz__errorz00(BFALSE,
																	BGl_string2010z00zzexpand_letz00,
																	BgL_xz00_21);
															}
														else
															{	/* Expand/let.scm 122 */
																{	/* Expand/let.scm 124 */
																	obj_t BgL_objz00_2242;

																	BgL_objz00_2242 = CNST_TABLE_REF(0);
																	SET_CAR(BgL_xz00_21, BgL_objz00_2242);
																}
																BgL_resz00_1659 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_1658,
																	BgL_xz00_21, BgL_ez00_1658);
															}
													}
												else
													{	/* Expand/let.scm 122 */
														obj_t BgL_carzd2399zd2_1675;
														obj_t BgL_cdrzd2400zd2_1676;

														BgL_carzd2399zd2_1675 =
															CAR(((obj_t) BgL_cdrzd2381zd2_1668));
														BgL_cdrzd2400zd2_1676 =
															CDR(((obj_t) BgL_cdrzd2381zd2_1668));
														if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
															(BgL_carzd2399zd2_1675))
															{	/* Expand/let.scm 122 */
																if (NULLP(BgL_cdrzd2400zd2_1676))
																	{	/* Expand/let.scm 122 */
																		goto BgL_tagzd2375zd2_1665;
																	}
																else
																	{	/* Expand/let.scm 122 */
																		BgL_ez00_1680 = BgL_ez00_1658;
																		BgL_bdgsz00_1681 = BgL_carzd2399zd2_1675;
																		BgL_bodyz00_1682 = BgL_cdrzd2400zd2_1676;
																		{	/* Expand/let.scm 107 */
																			obj_t BgL_varsz00_1684;

																			if (NULLP(BgL_bdgsz00_1681))
																				{	/* Expand/let.scm 107 */
																					BgL_varsz00_1684 = BNIL;
																				}
																			else
																				{	/* Expand/let.scm 107 */
																					obj_t BgL_head1250z00_1713;

																					BgL_head1250z00_1713 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1248z00_1715;
																						obj_t BgL_tail1251z00_1716;

																						BgL_l1248z00_1715 =
																							BgL_bdgsz00_1681;
																						BgL_tail1251z00_1716 =
																							BgL_head1250z00_1713;
																					BgL_zc3z04anonymousza31751ze3z87_1717:
																						if (NULLP
																							(BgL_l1248z00_1715))
																							{	/* Expand/let.scm 107 */
																								BgL_varsz00_1684 =
																									CDR(BgL_head1250z00_1713);
																							}
																						else
																							{	/* Expand/let.scm 107 */
																								obj_t BgL_newtail1252z00_1719;

																								{	/* Expand/let.scm 107 */
																									obj_t BgL_arg1754z00_1721;

																									{	/* Expand/let.scm 107 */
																										obj_t BgL_xz00_1722;

																										BgL_xz00_1722 =
																											CAR(
																											((obj_t)
																												BgL_l1248z00_1715));
																										{
																											obj_t BgL_valz00_1723;

																											if (PAIRP(BgL_xz00_1722))
																												{	/* Expand/let.scm 108 */
																													obj_t
																														BgL_cdrzd2368zd2_1729;
																													BgL_cdrzd2368zd2_1729
																														=
																														CDR(((obj_t)
																															BgL_xz00_1722));
																													if (PAIRP
																														(BgL_cdrzd2368zd2_1729))
																														{	/* Expand/let.scm 108 */
																															if (NULLP(CDR
																																	(BgL_cdrzd2368zd2_1729)))
																																{	/* Expand/let.scm 108 */
																																	BgL_valz00_1723
																																		=
																																		CAR
																																		(BgL_cdrzd2368zd2_1729);
																																	{	/* Expand/let.scm 109 */
																																		obj_t
																																			BgL_arg1770z00_1738;
																																		obj_t
																																			BgL_arg1771z00_1739;
																																		BgL_arg1770z00_1738
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_1722));
																																		BgL_arg1771z00_1739
																																			=
																																			BGL_PROCEDURE_CALL2
																																			(BgL_ez00_1680,
																																			BgL_valz00_1723,
																																			BgL_ez00_1680);
																																		{	/* Expand/let.scm 109 */
																																			obj_t
																																				BgL_tmpz00_2853;
																																			BgL_tmpz00_2853
																																				=
																																				((obj_t)
																																				BgL_arg1770z00_1738);
																																			SET_CAR
																																				(BgL_tmpz00_2853,
																																				BgL_arg1771z00_1739);
																																		}
																																	}
																																	BgL_arg1754z00_1721
																																		=
																																		BgL_xz00_1722;
																																}
																															else
																																{	/* Expand/let.scm 108 */
																																BgL_tagzd2363zd2_1726:
																																	BgL_arg1754z00_1721 = BGl_errorz00zz__errorz00(BFALSE, BGl_string2009z00zzexpand_letz00, BgL_xz00_1722);
																																}
																														}
																													else
																														{	/* Expand/let.scm 108 */
																															goto
																																BgL_tagzd2363zd2_1726;
																														}
																												}
																											else
																												{	/* Expand/let.scm 108 */
																													if (SYMBOLP
																														(BgL_xz00_1722))
																														{	/* Expand/let.scm 108 */
																															{	/* Expand/let.scm 110 */
																																obj_t
																																	BgL_list1772z00_1740;
																																{	/* Expand/let.scm 110 */
																																	obj_t
																																		BgL_arg1773z00_1741;
																																	BgL_arg1773z00_1741
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BUNSPEC,
																																		BNIL);
																																	BgL_list1772z00_1740
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_xz00_1722,
																																		BgL_arg1773z00_1741);
																																}
																																BgL_arg1754z00_1721
																																	=
																																	BgL_list1772z00_1740;
																															}
																														}
																													else
																														{	/* Expand/let.scm 108 */
																															goto
																																BgL_tagzd2363zd2_1726;
																														}
																												}
																										}
																									}
																									BgL_newtail1252z00_1719 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1754z00_1721, BNIL);
																								}
																								SET_CDR(BgL_tail1251z00_1716,
																									BgL_newtail1252z00_1719);
																								{	/* Expand/let.scm 107 */
																									obj_t BgL_arg1753z00_1720;

																									BgL_arg1753z00_1720 =
																										CDR(
																										((obj_t)
																											BgL_l1248z00_1715));
																									{
																										obj_t BgL_tail1251z00_2867;
																										obj_t BgL_l1248z00_2866;

																										BgL_l1248z00_2866 =
																											BgL_arg1753z00_1720;
																										BgL_tail1251z00_2867 =
																											BgL_newtail1252z00_1719;
																										BgL_tail1251z00_1716 =
																											BgL_tail1251z00_2867;
																										BgL_l1248z00_1715 =
																											BgL_l1248z00_2866;
																										goto
																											BgL_zc3z04anonymousza31751ze3z87_1717;
																									}
																								}
																							}
																					}
																				}
																			{	/* Expand/let.scm 113 */
																				obj_t BgL_arg1720z00_1685;
																				obj_t BgL_arg1722z00_1686;

																				if (NULLP(BgL_varsz00_1684))
																					{	/* Expand/let.scm 113 */
																						BgL_arg1720z00_1685 = BNIL;
																					}
																				else
																					{	/* Expand/let.scm 113 */
																						obj_t BgL_head1255z00_1690;

																						{	/* Expand/let.scm 113 */
																							obj_t BgL_arg1736z00_1702;

																							{	/* Expand/let.scm 113 */
																								obj_t BgL_pairz00_2229;

																								BgL_pairz00_2229 =
																									CAR(
																									((obj_t) BgL_varsz00_1684));
																								BgL_arg1736z00_1702 =
																									CAR(BgL_pairz00_2229);
																							}
																							BgL_head1255z00_1690 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1736z00_1702, BNIL);
																						}
																						{	/* Expand/let.scm 113 */
																							obj_t BgL_g1258z00_1691;

																							BgL_g1258z00_1691 =
																								CDR(((obj_t) BgL_varsz00_1684));
																							{
																								obj_t BgL_l1253z00_1693;
																								obj_t BgL_tail1256z00_1694;

																								BgL_l1253z00_1693 =
																									BgL_g1258z00_1691;
																								BgL_tail1256z00_1694 =
																									BgL_head1255z00_1690;
																							BgL_zc3z04anonymousza31726ze3z87_1695:
																								if (NULLP
																									(BgL_l1253z00_1693))
																									{	/* Expand/let.scm 113 */
																										BgL_arg1720z00_1685 =
																											BgL_head1255z00_1690;
																									}
																								else
																									{	/* Expand/let.scm 113 */
																										obj_t
																											BgL_newtail1257z00_1697;
																										{	/* Expand/let.scm 113 */
																											obj_t BgL_arg1734z00_1699;

																											{	/* Expand/let.scm 113 */
																												obj_t BgL_pairz00_2232;

																												BgL_pairz00_2232 =
																													CAR(
																													((obj_t)
																														BgL_l1253z00_1693));
																												BgL_arg1734z00_1699 =
																													CAR(BgL_pairz00_2232);
																											}
																											BgL_newtail1257z00_1697 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1734z00_1699,
																												BNIL);
																										}
																										SET_CDR
																											(BgL_tail1256z00_1694,
																											BgL_newtail1257z00_1697);
																										{	/* Expand/let.scm 113 */
																											obj_t BgL_arg1733z00_1698;

																											BgL_arg1733z00_1698 =
																												CDR(
																												((obj_t)
																													BgL_l1253z00_1693));
																											{
																												obj_t
																													BgL_tail1256z00_2886;
																												obj_t BgL_l1253z00_2885;

																												BgL_l1253z00_2885 =
																													BgL_arg1733z00_1698;
																												BgL_tail1256z00_2886 =
																													BgL_newtail1257z00_1697;
																												BgL_tail1256z00_1694 =
																													BgL_tail1256z00_2886;
																												BgL_l1253z00_1693 =
																													BgL_l1253z00_2885;
																												goto
																													BgL_zc3z04anonymousza31726ze3z87_1695;
																											}
																										}
																									}
																							}
																						}
																					}
																				BgL_arg1722z00_1686 =
																					BGl_findzd2locationzd2zztools_locationz00
																					(BgL_xz00_21);
																				{	/* Expand/let.scm 117 */
																					obj_t
																						BgL_zc3z04anonymousza31738ze3z87_2323;
																					BgL_zc3z04anonymousza31738ze3z87_2323
																						=
																						MAKE_FX_PROCEDURE
																						(BGl_z62zc3z04anonymousza31738ze3ze5zzexpand_letz00,
																						(int) (0L), (int) (4L));
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31738ze3z87_2323,
																						(int) (0L), BgL_xz00_21);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31738ze3z87_2323,
																						(int) (1L), BgL_bodyz00_1682);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31738ze3z87_2323,
																						(int) (2L), BgL_ez00_1680);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31738ze3z87_2323,
																						(int) (3L), BgL_varsz00_1684);
																					BgL_resz00_1659 =
																						BGl_withzd2lexicalzd2zzexpand_epsz00
																						(BgL_arg1720z00_1685,
																						CNST_TABLE_REF(4),
																						BgL_arg1722z00_1686,
																						BgL_zc3z04anonymousza31738ze3z87_2323);
															}}}}}
														else
															{	/* Expand/let.scm 122 */
																goto BgL_tagzd2375zd2_1665;
															}
													}
											}
										else
											{	/* Expand/let.scm 122 */
												goto BgL_tagzd2375zd2_1665;
											}
									}
								else
									{	/* Expand/let.scm 122 */
										goto BgL_tagzd2375zd2_1665;
									}
							}
							{	/* Expand/let.scm 122 */

								BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 =
									BgL_oldzd2internalzd2_1657;
								return BGl_replacez12z12zztools_miscz00(BgL_xz00_21,
									BgL_resz00_1659);
							}
						}
					}
				}
			}
		}

	}



/* &expand-letrec */
	obj_t BGl_z62expandzd2letreczb0zzexpand_letz00(obj_t BgL_envz00_2324,
		obj_t BgL_xz00_2325, obj_t BgL_ez00_2326)
	{
		{	/* Expand/let.scm 104 */
			return
				BGl_expandzd2letreczd2zzexpand_letz00(BgL_xz00_2325, BgL_ez00_2326);
		}

	}



/* &<@anonymous:1738> */
	obj_t BGl_z62zc3z04anonymousza31738ze3ze5zzexpand_letz00(obj_t
		BgL_envz00_2327)
	{
		{	/* Expand/let.scm 116 */
			{	/* Expand/let.scm 117 */
				obj_t BgL_xz00_2328;
				obj_t BgL_bodyz00_2329;
				obj_t BgL_ez00_2330;
				obj_t BgL_varsz00_2331;

				BgL_xz00_2328 = PROCEDURE_REF(BgL_envz00_2327, (int) (0L));
				BgL_bodyz00_2329 = PROCEDURE_REF(BgL_envz00_2327, (int) (1L));
				BgL_ez00_2330 = ((obj_t) PROCEDURE_REF(BgL_envz00_2327, (int) (2L)));
				BgL_varsz00_2331 = PROCEDURE_REF(BgL_envz00_2327, (int) (3L));
				{	/* Expand/let.scm 117 */
					obj_t BgL_arg1739z00_2363;
					obj_t BgL_arg1740z00_2364;

					BgL_arg1739z00_2363 = CAR(((obj_t) BgL_xz00_2328));
					{	/* Expand/let.scm 117 */
						obj_t BgL_arg1746z00_2365;

						{	/* Expand/let.scm 117 */
							obj_t BgL_arg1747z00_2366;

							{	/* Expand/let.scm 117 */
								obj_t BgL_arg1748z00_2367;

								BgL_arg1748z00_2367 =
									BGl_expandzd2prognzd2zz__prognz00(BgL_bodyz00_2329);
								BgL_arg1747z00_2366 =
									BGL_PROCEDURE_CALL2(BgL_ez00_2330, BgL_arg1748z00_2367,
									BgL_ez00_2330);
							}
							BgL_arg1746z00_2365 = MAKE_YOUNG_PAIR(BgL_arg1747z00_2366, BNIL);
						}
						BgL_arg1740z00_2364 =
							MAKE_YOUNG_PAIR(BgL_varsz00_2331, BgL_arg1746z00_2365);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1739z00_2363, BgL_arg1740z00_2364);
				}
			}
		}

	}



/* expand-labels */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2labelszd2zzexpand_letz00(obj_t
		BgL_xz00_23, obj_t BgL_ez00_24)
	{
		{	/* Expand/let.scm 136 */
			{
				obj_t BgL_ez00_1768;
				obj_t BgL_bindingsz00_1769;
				obj_t BgL_bodyz00_1770;

				{	/* Expand/let.scm 171 */
					obj_t BgL_oldzd2internalzd2_1745;

					BgL_oldzd2internalzd2_1745 =
						BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
					BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 = BTRUE;
					{	/* Expand/let.scm 173 */
						obj_t BgL_ez00_1746;

						BgL_ez00_1746 =
							BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(BgL_ez00_24);
						{	/* Expand/let.scm 173 */
							obj_t BgL_resz00_1747;

							{

								if (PAIRP(BgL_xz00_23))
									{	/* Expand/let.scm 174 */
										obj_t BgL_cdrzd2569zd2_1756;

										BgL_cdrzd2569zd2_1756 = CDR(((obj_t) BgL_xz00_23));
										if (PAIRP(BgL_cdrzd2569zd2_1756))
											{	/* Expand/let.scm 174 */
												obj_t BgL_cdrzd2572zd2_1758;

												BgL_cdrzd2572zd2_1758 = CDR(BgL_cdrzd2569zd2_1756);
												if (NULLP(CAR(BgL_cdrzd2569zd2_1756)))
													{	/* Expand/let.scm 174 */
														if (NULLP(BgL_cdrzd2572zd2_1758))
															{	/* Expand/let.scm 174 */
															BgL_tagzd2563zd2_1753:
																BgL_resz00_1747 =
																	BGl_errorz00zz__errorz00(BFALSE,
																	BGl_string2012z00zzexpand_letz00,
																	BgL_xz00_23);
															}
														else
															{	/* Expand/let.scm 174 */
																{	/* Expand/let.scm 176 */
																	obj_t BgL_objz00_2305;

																	BgL_objz00_2305 = CNST_TABLE_REF(0);
																	SET_CAR(BgL_xz00_23, BgL_objz00_2305);
																}
																BgL_resz00_1747 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_1746,
																	BgL_xz00_23, BgL_ez00_1746);
															}
													}
												else
													{	/* Expand/let.scm 174 */
														obj_t BgL_carzd2587zd2_1763;
														obj_t BgL_cdrzd2588zd2_1764;

														BgL_carzd2587zd2_1763 =
															CAR(((obj_t) BgL_cdrzd2569zd2_1756));
														BgL_cdrzd2588zd2_1764 =
															CDR(((obj_t) BgL_cdrzd2569zd2_1756));
														if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
															(BgL_carzd2587zd2_1763))
															{	/* Expand/let.scm 174 */
																if (NULLP(BgL_cdrzd2588zd2_1764))
																	{	/* Expand/let.scm 174 */
																		goto BgL_tagzd2563zd2_1753;
																	}
																else
																	{	/* Expand/let.scm 174 */
																		BgL_ez00_1768 = BgL_ez00_1746;
																		BgL_bindingsz00_1769 =
																			BgL_carzd2587zd2_1763;
																		BgL_bodyz00_1770 = BgL_cdrzd2588zd2_1764;
																		{	/* Expand/let.scm 138 */
																			obj_t BgL_varsz00_1772;

																			if (NULLP(BgL_bindingsz00_1769))
																				{	/* Expand/let.scm 138 */
																					BgL_varsz00_1772 = BNIL;
																				}
																			else
																				{	/* Expand/let.scm 138 */
																					obj_t BgL_head1261z00_1820;

																					BgL_head1261z00_1820 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1259z00_1822;
																						obj_t BgL_tail1262z00_1823;

																						BgL_l1259z00_1822 =
																							BgL_bindingsz00_1769;
																						BgL_tail1262z00_1823 =
																							BgL_head1261z00_1820;
																					BgL_zc3z04anonymousza31851ze3z87_1824:
																						if (NULLP
																							(BgL_l1259z00_1822))
																							{	/* Expand/let.scm 138 */
																								BgL_varsz00_1772 =
																									CDR(BgL_head1261z00_1820);
																							}
																						else
																							{	/* Expand/let.scm 138 */
																								obj_t BgL_newtail1263z00_1826;

																								{	/* Expand/let.scm 138 */
																									obj_t BgL_arg1854z00_1828;

																									{	/* Expand/let.scm 138 */
																										obj_t BgL_xz00_1829;

																										BgL_xz00_1829 =
																											CAR(
																											((obj_t)
																												BgL_l1259z00_1822));
																										{

																											if (PAIRP(BgL_xz00_1829))
																												{	/* Expand/let.scm 139 */
																													obj_t
																														BgL_cdrzd2427zd2_1841;
																													BgL_cdrzd2427zd2_1841
																														=
																														CDR(((obj_t)
																															BgL_xz00_1829));
																													{	/* Expand/let.scm 139 */
																														obj_t
																															BgL_idz00_1842;
																														BgL_idz00_1842 =
																															CAR(((obj_t)
																																BgL_xz00_1829));
																														if (PAIRP
																															(BgL_cdrzd2427zd2_1841))
																															{	/* Expand/let.scm 139 */
																																if (NULLP(CAR
																																		(BgL_cdrzd2427zd2_1841)))
																																	{	/* Expand/let.scm 139 */
																																		BgL_arg1854z00_1828
																																			=
																																			BgL_idz00_1842;
																																	}
																																else
																																	{	/* Expand/let.scm 139 */
																																		bool_t
																																			BgL_test2104z00_2971;
																																		{	/* Expand/let.scm 139 */
																																			obj_t
																																				BgL_tmpz00_2972;
																																			BgL_tmpz00_2972
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2427zd2_1841));
																																			BgL_test2104z00_2971
																																				=
																																				SYMBOLP
																																				(BgL_tmpz00_2972);
																																		}
																																		if (BgL_test2104z00_2971)
																																			{	/* Expand/let.scm 139 */
																																				BgL_arg1854z00_1828
																																					=
																																					BgL_idz00_1842;
																																			}
																																		else
																																			{	/* Expand/let.scm 139 */
																																				obj_t
																																					BgL_idz00_1850;
																																				BgL_idz00_1850
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_1829));
																																				{
																																					obj_t
																																						BgL_gzd2454zd2_1909;
																																					obj_t
																																						BgL_gzd2449zd2_1855;
																																					{	/* Expand/let.scm 139 */
																																						obj_t
																																							BgL_arg1862z00_1853;
																																						{	/* Expand/let.scm 139 */
																																							obj_t
																																								BgL_pairz00_2288;
																																							BgL_pairz00_2288
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_xz00_1829));
																																							BgL_arg1862z00_1853
																																								=
																																								CAR
																																								(BgL_pairz00_2288);
																																						}
																																						BgL_gzd2449zd2_1855
																																							=
																																							BgL_arg1862z00_1853;
																																						if (PAIRP(BgL_gzd2449zd2_1855))
																																							{	/* Expand/let.scm 139 */
																																								obj_t
																																									BgL_carzd2451zd2_1858;
																																								BgL_carzd2451zd2_1858
																																									=
																																									CAR
																																									(BgL_gzd2449zd2_1855);
																																								{

																																									if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(BgL_carzd2451zd2_1858))
																																										{	/* Expand/let.scm 139 */
																																										BgL_kapzd2453zd2_1859:
																																											{	/* Expand/let.scm 139 */
																																												obj_t
																																													BgL_arg1884z00_1885;
																																												BgL_arg1884z00_1885
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_gzd2449zd2_1855));
																																												BgL_gzd2454zd2_1909
																																													=
																																													BgL_arg1884z00_1885;
																																											BgL_zc3z04anonymousza31903ze3z87_1910:
																																												if (NULLP(BgL_gzd2454zd2_1909))
																																													{	/* Expand/let.scm 139 */
																																														BgL_arg1854z00_1828
																																															=
																																															BgL_idz00_1850;
																																													}
																																												else
																																													{	/* Expand/let.scm 139 */
																																														if (PAIRP(BgL_gzd2454zd2_1909))
																																															{	/* Expand/let.scm 139 */
																																																obj_t
																																																	BgL_carzd2456zd2_1913;
																																																BgL_carzd2456zd2_1913
																																																	=
																																																	CAR
																																																	(BgL_gzd2454zd2_1909);
																																																{

																																																	if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(BgL_carzd2456zd2_1913))
																																																		{	/* Expand/let.scm 139 */
																																																		BgL_kapzd2458zd2_1914:
																																																			{	/* Expand/let.scm 139 */
																																																				obj_t
																																																					BgL_arg1927z00_1940;
																																																				BgL_arg1927z00_1940
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_gzd2454zd2_1909));
																																																				{
																																																					obj_t
																																																						BgL_gzd2454zd2_2997;
																																																					BgL_gzd2454zd2_2997
																																																						=
																																																						BgL_arg1927z00_1940;
																																																					BgL_gzd2454zd2_1909
																																																						=
																																																						BgL_gzd2454zd2_2997;
																																																					goto
																																																						BgL_zc3z04anonymousza31903ze3z87_1910;
																																																				}
																																																			}
																																																		}
																																																	else
																																																		{	/* Expand/let.scm 139 */
																																																			if (SYMBOLP(BgL_carzd2456zd2_1913))
																																																				{	/* Expand/let.scm 139 */
																																																					goto
																																																						BgL_kapzd2458zd2_1914;
																																																				}
																																																			else
																																																				{	/* Expand/let.scm 139 */
																																																					obj_t
																																																						BgL_idz00_1917;
																																																					BgL_idz00_1917
																																																						=
																																																						CAR
																																																						(
																																																						((obj_t) BgL_xz00_1829));
																																																					{
																																																						obj_t
																																																							BgL_gzd2472zd2_1930;
																																																						obj_t
																																																							BgL_gzd2468zd2_1922;
																																																						{	/* Expand/let.scm 139 */
																																																							obj_t
																																																								BgL_arg1910z00_1920;
																																																							{	/* Expand/let.scm 139 */
																																																								obj_t
																																																									BgL_pairz00_2279;
																																																								BgL_pairz00_2279
																																																									=
																																																									CDR
																																																									(
																																																									((obj_t) BgL_xz00_1829));
																																																								BgL_arg1910z00_1920
																																																									=
																																																									CAR
																																																									(BgL_pairz00_2279);
																																																							}
																																																							BgL_gzd2468zd2_1922
																																																								=
																																																								BgL_arg1910z00_1920;
																																																							if (SYMBOLP(BgL_gzd2468zd2_1922))
																																																								{	/* Expand/let.scm 139 */
																																																									BgL_arg1854z00_1828
																																																										=
																																																										BgL_idz00_1917;
																																																								}
																																																							else
																																																								{	/* Expand/let.scm 139 */
																																																									if (PAIRP(BgL_gzd2468zd2_1922))
																																																										{	/* Expand/let.scm 139 */
																																																											bool_t
																																																												BgL_test2113z00_3009;
																																																											{	/* Expand/let.scm 139 */
																																																												obj_t
																																																													BgL_tmpz00_3010;
																																																												BgL_tmpz00_3010
																																																													=
																																																													CAR
																																																													(BgL_gzd2468zd2_1922);
																																																												BgL_test2113z00_3009
																																																													=
																																																													SYMBOLP
																																																													(BgL_tmpz00_3010);
																																																											}
																																																											if (BgL_test2113z00_3009)
																																																												{	/* Expand/let.scm 139 */
																																																													BgL_gzd2472zd2_1930
																																																														=
																																																														CDR
																																																														(BgL_gzd2468zd2_1922);
																																																												BgL_zc3z04anonymousza31920ze3z87_1931:
																																																													if (SYMBOLP(BgL_gzd2472zd2_1930))
																																																														{	/* Expand/let.scm 139 */
																																																															BgL_arg1854z00_1828
																																																																=
																																																																BgL_idz00_1917;
																																																														}
																																																													else
																																																														{	/* Expand/let.scm 139 */
																																																															if (PAIRP(BgL_gzd2472zd2_1930))
																																																																{	/* Expand/let.scm 139 */
																																																																	bool_t
																																																																		BgL_test2116z00_3017;
																																																																	{	/* Expand/let.scm 139 */
																																																																		obj_t
																																																																			BgL_tmpz00_3018;
																																																																		BgL_tmpz00_3018
																																																																			=
																																																																			CAR
																																																																			(BgL_gzd2472zd2_1930);
																																																																		BgL_test2116z00_3017
																																																																			=
																																																																			SYMBOLP
																																																																			(BgL_tmpz00_3018);
																																																																	}
																																																																	if (BgL_test2116z00_3017)
																																																																		{
																																																																			obj_t
																																																																				BgL_gzd2472zd2_3021;
																																																																			BgL_gzd2472zd2_3021
																																																																				=
																																																																				CDR
																																																																				(BgL_gzd2472zd2_1930);
																																																																			BgL_gzd2472zd2_1930
																																																																				=
																																																																				BgL_gzd2472zd2_3021;
																																																																			goto
																																																																				BgL_zc3z04anonymousza31920ze3z87_1931;
																																																																		}
																																																																	else
																																																																		{	/* Expand/let.scm 139 */
																																																																		BgL_tagzd2418zd2_1838:
																																																																			BgL_arg1854z00_1828 = BGl_errorz00zz__errorz00(BFALSE, BGl_string2011z00zzexpand_letz00, BgL_xz00_1829);
																																																																		}
																																																																}
																																																															else
																																																																{	/* Expand/let.scm 139 */
																																																																	goto
																																																																		BgL_tagzd2418zd2_1838;
																																																																}
																																																														}
																																																												}
																																																											else
																																																												{	/* Expand/let.scm 139 */
																																																													goto
																																																														BgL_tagzd2418zd2_1838;
																																																												}
																																																										}
																																																									else
																																																										{	/* Expand/let.scm 139 */
																																																											goto
																																																												BgL_tagzd2418zd2_1838;
																																																										}
																																																								}
																																																						}
																																																					}
																																																				}
																																																		}
																																																}
																																															}
																																														else
																																															{	/* Expand/let.scm 139 */
																																																obj_t
																																																	BgL_idz00_1941;
																																																BgL_idz00_1941
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_xz00_1829));
																																																{
																																																	obj_t
																																																		BgL_gzd2489zd2_1954;
																																																	obj_t
																																																		BgL_gzd2485zd2_1946;
																																																	{	/* Expand/let.scm 139 */
																																																		obj_t
																																																			BgL_arg1928z00_1944;
																																																		{	/* Expand/let.scm 139 */
																																																			obj_t
																																																				BgL_pairz00_2286;
																																																			BgL_pairz00_2286
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_xz00_1829));
																																																			BgL_arg1928z00_1944
																																																				=
																																																				CAR
																																																				(BgL_pairz00_2286);
																																																		}
																																																		BgL_gzd2485zd2_1946
																																																			=
																																																			BgL_arg1928z00_1944;
																																																		if (SYMBOLP(BgL_gzd2485zd2_1946))
																																																			{	/* Expand/let.scm 139 */
																																																				BgL_arg1854z00_1828
																																																					=
																																																					BgL_idz00_1941;
																																																			}
																																																		else
																																																			{	/* Expand/let.scm 139 */
																																																				if (PAIRP(BgL_gzd2485zd2_1946))
																																																					{	/* Expand/let.scm 139 */
																																																						bool_t
																																																							BgL_test2119z00_3034;
																																																						{	/* Expand/let.scm 139 */
																																																							obj_t
																																																								BgL_tmpz00_3035;
																																																							BgL_tmpz00_3035
																																																								=
																																																								CAR
																																																								(BgL_gzd2485zd2_1946);
																																																							BgL_test2119z00_3034
																																																								=
																																																								SYMBOLP
																																																								(BgL_tmpz00_3035);
																																																						}
																																																						if (BgL_test2119z00_3034)
																																																							{	/* Expand/let.scm 139 */
																																																								BgL_gzd2489zd2_1954
																																																									=
																																																									CDR
																																																									(BgL_gzd2485zd2_1946);
																																																							BgL_zc3z04anonymousza31937ze3z87_1955:
																																																								if (SYMBOLP(BgL_gzd2489zd2_1954))
																																																									{	/* Expand/let.scm 139 */
																																																										BgL_arg1854z00_1828
																																																											=
																																																											BgL_idz00_1941;
																																																									}
																																																								else
																																																									{	/* Expand/let.scm 139 */
																																																										if (PAIRP(BgL_gzd2489zd2_1954))
																																																											{	/* Expand/let.scm 139 */
																																																												bool_t
																																																													BgL_test2122z00_3042;
																																																												{	/* Expand/let.scm 139 */
																																																													obj_t
																																																														BgL_tmpz00_3043;
																																																													BgL_tmpz00_3043
																																																														=
																																																														CAR
																																																														(BgL_gzd2489zd2_1954);
																																																													BgL_test2122z00_3042
																																																														=
																																																														SYMBOLP
																																																														(BgL_tmpz00_3043);
																																																												}
																																																												if (BgL_test2122z00_3042)
																																																													{
																																																														obj_t
																																																															BgL_gzd2489zd2_3046;
																																																														BgL_gzd2489zd2_3046
																																																															=
																																																															CDR
																																																															(BgL_gzd2489zd2_1954);
																																																														BgL_gzd2489zd2_1954
																																																															=
																																																															BgL_gzd2489zd2_3046;
																																																														goto
																																																															BgL_zc3z04anonymousza31937ze3z87_1955;
																																																													}
																																																												else
																																																													{	/* Expand/let.scm 139 */
																																																														goto
																																																															BgL_tagzd2418zd2_1838;
																																																													}
																																																											}
																																																										else
																																																											{	/* Expand/let.scm 139 */
																																																												goto
																																																													BgL_tagzd2418zd2_1838;
																																																											}
																																																									}
																																																							}
																																																						else
																																																							{	/* Expand/let.scm 139 */
																																																								goto
																																																									BgL_tagzd2418zd2_1838;
																																																							}
																																																					}
																																																				else
																																																					{	/* Expand/let.scm 139 */
																																																						goto
																																																							BgL_tagzd2418zd2_1838;
																																																					}
																																																			}
																																																	}
																																																}
																																															}
																																													}
																																											}
																																										}
																																									else
																																										{	/* Expand/let.scm 139 */
																																											if (SYMBOLP(BgL_carzd2451zd2_1858))
																																												{	/* Expand/let.scm 139 */
																																													goto
																																														BgL_kapzd2453zd2_1859;
																																												}
																																											else
																																												{	/* Expand/let.scm 139 */
																																													obj_t
																																														BgL_idz00_1862;
																																													BgL_idz00_1862
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_xz00_1829));
																																													{
																																														obj_t
																																															BgL_gzd2506zd2_1875;
																																														obj_t
																																															BgL_gzd2502zd2_1867;
																																														{	/* Expand/let.scm 139 */
																																															obj_t
																																																BgL_arg1868z00_1865;
																																															{	/* Expand/let.scm 139 */
																																																obj_t
																																																	BgL_pairz00_2263;
																																																BgL_pairz00_2263
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_xz00_1829));
																																																BgL_arg1868z00_1865
																																																	=
																																																	CAR
																																																	(BgL_pairz00_2263);
																																															}
																																															BgL_gzd2502zd2_1867
																																																=
																																																BgL_arg1868z00_1865;
																																															if (SYMBOLP(BgL_gzd2502zd2_1867))
																																																{	/* Expand/let.scm 139 */
																																																	BgL_arg1854z00_1828
																																																		=
																																																		BgL_idz00_1862;
																																																}
																																															else
																																																{	/* Expand/let.scm 139 */
																																																	if (PAIRP(BgL_gzd2502zd2_1867))
																																																		{	/* Expand/let.scm 139 */
																																																			bool_t
																																																				BgL_test2126z00_3060;
																																																			{	/* Expand/let.scm 139 */
																																																				obj_t
																																																					BgL_tmpz00_3061;
																																																				BgL_tmpz00_3061
																																																					=
																																																					CAR
																																																					(BgL_gzd2502zd2_1867);
																																																				BgL_test2126z00_3060
																																																					=
																																																					SYMBOLP
																																																					(BgL_tmpz00_3061);
																																																			}
																																																			if (BgL_test2126z00_3060)
																																																				{	/* Expand/let.scm 139 */
																																																					BgL_gzd2506zd2_1875
																																																						=
																																																						CDR
																																																						(BgL_gzd2502zd2_1867);
																																																				BgL_zc3z04anonymousza31877ze3z87_1876:
																																																					if (SYMBOLP(BgL_gzd2506zd2_1875))
																																																						{	/* Expand/let.scm 139 */
																																																							BgL_arg1854z00_1828
																																																								=
																																																								BgL_idz00_1862;
																																																						}
																																																					else
																																																						{	/* Expand/let.scm 139 */
																																																							if (PAIRP(BgL_gzd2506zd2_1875))
																																																								{	/* Expand/let.scm 139 */
																																																									bool_t
																																																										BgL_test2129z00_3068;
																																																									{	/* Expand/let.scm 139 */
																																																										obj_t
																																																											BgL_tmpz00_3069;
																																																										BgL_tmpz00_3069
																																																											=
																																																											CAR
																																																											(BgL_gzd2506zd2_1875);
																																																										BgL_test2129z00_3068
																																																											=
																																																											SYMBOLP
																																																											(BgL_tmpz00_3069);
																																																									}
																																																									if (BgL_test2129z00_3068)
																																																										{
																																																											obj_t
																																																												BgL_gzd2506zd2_3072;
																																																											BgL_gzd2506zd2_3072
																																																												=
																																																												CDR
																																																												(BgL_gzd2506zd2_1875);
																																																											BgL_gzd2506zd2_1875
																																																												=
																																																												BgL_gzd2506zd2_3072;
																																																											goto
																																																												BgL_zc3z04anonymousza31877ze3z87_1876;
																																																										}
																																																									else
																																																										{	/* Expand/let.scm 139 */
																																																											goto
																																																												BgL_tagzd2418zd2_1838;
																																																										}
																																																								}
																																																							else
																																																								{	/* Expand/let.scm 139 */
																																																									goto
																																																										BgL_tagzd2418zd2_1838;
																																																								}
																																																						}
																																																				}
																																																			else
																																																				{	/* Expand/let.scm 139 */
																																																					goto
																																																						BgL_tagzd2418zd2_1838;
																																																				}
																																																		}
																																																	else
																																																		{	/* Expand/let.scm 139 */
																																																			goto
																																																				BgL_tagzd2418zd2_1838;
																																																		}
																																																}
																																														}
																																													}
																																												}
																																										}
																																								}
																																							}
																																						else
																																							{	/* Expand/let.scm 139 */
																																								obj_t
																																									BgL_idz00_1886;
																																								BgL_idz00_1886
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_1829));
																																								{
																																									obj_t
																																										BgL_gzd2523zd2_1899;
																																									obj_t
																																										BgL_gzd2519zd2_1891;
																																									{	/* Expand/let.scm 139 */
																																										obj_t
																																											BgL_arg1885z00_1889;
																																										{	/* Expand/let.scm 139 */
																																											obj_t
																																												BgL_pairz00_2270;
																																											BgL_pairz00_2270
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_xz00_1829));
																																											BgL_arg1885z00_1889
																																												=
																																												CAR
																																												(BgL_pairz00_2270);
																																										}
																																										BgL_gzd2519zd2_1891
																																											=
																																											BgL_arg1885z00_1889;
																																										if (SYMBOLP(BgL_gzd2519zd2_1891))
																																											{	/* Expand/let.scm 139 */
																																												BgL_arg1854z00_1828
																																													=
																																													BgL_idz00_1886;
																																											}
																																										else
																																											{	/* Expand/let.scm 139 */
																																												if (PAIRP(BgL_gzd2519zd2_1891))
																																													{	/* Expand/let.scm 139 */
																																														bool_t
																																															BgL_test2132z00_3084;
																																														{	/* Expand/let.scm 139 */
																																															obj_t
																																																BgL_tmpz00_3085;
																																															BgL_tmpz00_3085
																																																=
																																																CAR
																																																(BgL_gzd2519zd2_1891);
																																															BgL_test2132z00_3084
																																																=
																																																SYMBOLP
																																																(BgL_tmpz00_3085);
																																														}
																																														if (BgL_test2132z00_3084)
																																															{	/* Expand/let.scm 139 */
																																																BgL_gzd2523zd2_1899
																																																	=
																																																	CDR
																																																	(BgL_gzd2519zd2_1891);
																																															BgL_zc3z04anonymousza31895ze3z87_1900:
																																																if (SYMBOLP(BgL_gzd2523zd2_1899))
																																																	{	/* Expand/let.scm 139 */
																																																		BgL_arg1854z00_1828
																																																			=
																																																			BgL_idz00_1886;
																																																	}
																																																else
																																																	{	/* Expand/let.scm 139 */
																																																		if (PAIRP(BgL_gzd2523zd2_1899))
																																																			{	/* Expand/let.scm 139 */
																																																				bool_t
																																																					BgL_test2135z00_3092;
																																																				{	/* Expand/let.scm 139 */
																																																					obj_t
																																																						BgL_tmpz00_3093;
																																																					BgL_tmpz00_3093
																																																						=
																																																						CAR
																																																						(BgL_gzd2523zd2_1899);
																																																					BgL_test2135z00_3092
																																																						=
																																																						SYMBOLP
																																																						(BgL_tmpz00_3093);
																																																				}
																																																				if (BgL_test2135z00_3092)
																																																					{
																																																						obj_t
																																																							BgL_gzd2523zd2_3096;
																																																						BgL_gzd2523zd2_3096
																																																							=
																																																							CDR
																																																							(BgL_gzd2523zd2_1899);
																																																						BgL_gzd2523zd2_1899
																																																							=
																																																							BgL_gzd2523zd2_3096;
																																																						goto
																																																							BgL_zc3z04anonymousza31895ze3z87_1900;
																																																					}
																																																				else
																																																					{	/* Expand/let.scm 139 */
																																																						goto
																																																							BgL_tagzd2418zd2_1838;
																																																					}
																																																			}
																																																		else
																																																			{	/* Expand/let.scm 139 */
																																																				goto
																																																					BgL_tagzd2418zd2_1838;
																																																			}
																																																	}
																																															}
																																														else
																																															{	/* Expand/let.scm 139 */
																																																goto
																																																	BgL_tagzd2418zd2_1838;
																																															}
																																													}
																																												else
																																													{	/* Expand/let.scm 139 */
																																														goto
																																															BgL_tagzd2418zd2_1838;
																																													}
																																											}
																																									}
																																								}
																																							}
																																					}
																																				}
																																			}
																																	}
																															}
																														else
																															{	/* Expand/let.scm 139 */
																																goto
																																	BgL_tagzd2418zd2_1838;
																															}
																													}
																												}
																											else
																												{	/* Expand/let.scm 139 */
																													goto
																														BgL_tagzd2418zd2_1838;
																												}
																										}
																									}
																									BgL_newtail1263z00_1826 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1854z00_1828, BNIL);
																								}
																								SET_CDR(BgL_tail1262z00_1823,
																									BgL_newtail1263z00_1826);
																								{	/* Expand/let.scm 138 */
																									obj_t BgL_arg1853z00_1827;

																									BgL_arg1853z00_1827 =
																										CDR(
																										((obj_t)
																											BgL_l1259z00_1822));
																									{
																										obj_t BgL_tail1262z00_3104;
																										obj_t BgL_l1259z00_3103;

																										BgL_l1259z00_3103 =
																											BgL_arg1853z00_1827;
																										BgL_tail1262z00_3104 =
																											BgL_newtail1263z00_1826;
																										BgL_tail1262z00_1823 =
																											BgL_tail1262z00_3104;
																										BgL_l1259z00_1822 =
																											BgL_l1259z00_3103;
																										goto
																											BgL_zc3z04anonymousza31851ze3z87_1824;
																									}
																								}
																							}
																					}
																				}
																			{	/* Expand/let.scm 154 */
																				obj_t BgL_arg1808z00_1773;

																				BgL_arg1808z00_1773 =
																					BGl_findzd2locationzd2zztools_locationz00
																					(BgL_xz00_23);
																				{	/* Expand/let.scm 156 */
																					obj_t
																						BgL_zc3z04anonymousza31813ze3z87_2333;
																					BgL_zc3z04anonymousza31813ze3z87_2333
																						=
																						MAKE_FX_PROCEDURE
																						(BGl_z62zc3z04anonymousza31813ze3ze5zzexpand_letz00,
																						(int) (0L), (int) (3L));
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31813ze3z87_2333,
																						(int) (0L), BgL_ez00_1768);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31813ze3z87_2333,
																						(int) (1L), BgL_bindingsz00_1769);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31813ze3z87_2333,
																						(int) (2L), BgL_bodyz00_1770);
																					BgL_resz00_1747 =
																						BGl_withzd2lexicalzd2zzexpand_epsz00
																						(BgL_varsz00_1772,
																						CNST_TABLE_REF(4),
																						BgL_arg1808z00_1773,
																						BgL_zc3z04anonymousza31813ze3z87_2333);
															}}}}}
														else
															{	/* Expand/let.scm 174 */
																goto BgL_tagzd2563zd2_1753;
															}
													}
											}
										else
											{	/* Expand/let.scm 174 */
												goto BgL_tagzd2563zd2_1753;
											}
									}
								else
									{	/* Expand/let.scm 174 */
										goto BgL_tagzd2563zd2_1753;
									}
							}
							{	/* Expand/let.scm 174 */

								BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 =
									BgL_oldzd2internalzd2_1745;
								return BGl_replacez12z12zztools_miscz00(BgL_xz00_23,
									BgL_resz00_1747);
							}
						}
					}
				}
			}
		}

	}



/* &expand-labels */
	obj_t BGl_z62expandzd2labelszb0zzexpand_letz00(obj_t BgL_envz00_2334,
		obj_t BgL_xz00_2335, obj_t BgL_ez00_2336)
	{
		{	/* Expand/let.scm 136 */
			return
				BGl_expandzd2labelszd2zzexpand_letz00(BgL_xz00_2335, BgL_ez00_2336);
		}

	}



/* &<@anonymous:1813> */
	obj_t BGl_z62zc3z04anonymousza31813ze3ze5zzexpand_letz00(obj_t
		BgL_envz00_2337)
	{
		{	/* Expand/let.scm 155 */
			{	/* Expand/let.scm 156 */
				obj_t BgL_ez00_2338;
				obj_t BgL_bindingsz00_2339;
				obj_t BgL_bodyz00_2340;

				BgL_ez00_2338 = ((obj_t) PROCEDURE_REF(BgL_envz00_2337, (int) (0L)));
				BgL_bindingsz00_2339 = PROCEDURE_REF(BgL_envz00_2337, (int) (1L));
				BgL_bodyz00_2340 = PROCEDURE_REF(BgL_envz00_2337, (int) (2L));
				{	/* Expand/let.scm 156 */
					obj_t BgL_newz00_2368;

					if (NULLP(BgL_bindingsz00_2339))
						{	/* Expand/let.scm 156 */
							BgL_newz00_2368 = BNIL;
						}
					else
						{	/* Expand/let.scm 156 */
							obj_t BgL_head1266z00_2369;

							BgL_head1266z00_2369 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1264z00_2371;
								obj_t BgL_tail1267z00_2372;

								BgL_l1264z00_2371 = BgL_bindingsz00_2339;
								BgL_tail1267z00_2372 = BgL_head1266z00_2369;
							BgL_zc3z04anonymousza31833ze3z87_2370:
								if (NULLP(BgL_l1264z00_2371))
									{	/* Expand/let.scm 156 */
										BgL_newz00_2368 = CDR(BgL_head1266z00_2369);
									}
								else
									{	/* Expand/let.scm 156 */
										obj_t BgL_newtail1268z00_2373;

										{	/* Expand/let.scm 156 */
											obj_t BgL_arg1836z00_2374;

											{	/* Expand/let.scm 156 */
												obj_t BgL_xz00_2375;

												BgL_xz00_2375 = CAR(((obj_t) BgL_l1264z00_2371));
												{
													obj_t BgL_namez00_2378;
													obj_t BgL_argsz00_2379;
													obj_t BgL_lbodyz00_2380;

													if (PAIRP(BgL_xz00_2375))
														{	/* Expand/let.scm 157 */
															obj_t BgL_cdrzd2552zd2_2384;

															BgL_cdrzd2552zd2_2384 =
																CDR(((obj_t) BgL_xz00_2375));
															if (PAIRP(BgL_cdrzd2552zd2_2384))
																{	/* Expand/let.scm 157 */
																	obj_t BgL_cdrzd2557zd2_2385;

																	BgL_cdrzd2557zd2_2385 =
																		CDR(BgL_cdrzd2552zd2_2384);
																	if (NULLP(BgL_cdrzd2557zd2_2385))
																		{	/* Expand/let.scm 157 */
																		BgL_tagzd2543zd2_2377:
																			BgL_arg1836z00_2374 =
																				BGl_errorz00zz__errorz00(BFALSE,
																				BGl_string2011z00zzexpand_letz00,
																				BgL_xz00_2375);
																		}
																	else
																		{	/* Expand/let.scm 157 */
																			obj_t BgL_arg1840z00_2386;
																			obj_t BgL_arg1842z00_2387;

																			BgL_arg1840z00_2386 =
																				CAR(((obj_t) BgL_xz00_2375));
																			BgL_arg1842z00_2387 =
																				CAR(BgL_cdrzd2552zd2_2384);
																			BgL_namez00_2378 = BgL_arg1840z00_2386;
																			BgL_argsz00_2379 = BgL_arg1842z00_2387;
																			BgL_lbodyz00_2380 = BgL_cdrzd2557zd2_2385;
																			{	/* Expand/let.scm 160 */
																				obj_t BgL_arg1843z00_2381;
																				obj_t BgL_arg1844z00_2382;

																				BgL_arg1843z00_2381 =
																					BGl_argsza2zd2ze3argszd2listz41zztools_argsz00
																					(BgL_argsz00_2379);
																				BgL_arg1844z00_2382 =
																					BGl_findzd2locationzd2zztools_locationz00
																					(BgL_xz00_2375);
																				{	/* Expand/let.scm 164 */
																					obj_t
																						BgL_zc3z04anonymousza31846ze3z87_2383;
																					BgL_zc3z04anonymousza31846ze3z87_2383
																						=
																						MAKE_FX_PROCEDURE
																						(BGl_z62zc3z04anonymousza31846ze3ze5zzexpand_letz00,
																						(int) (0L), (int) (5L));
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31846ze3z87_2383,
																						(int) (0L), BgL_lbodyz00_2380);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31846ze3z87_2383,
																						(int) (1L), BgL_ez00_2338);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31846ze3z87_2383,
																						(int) (2L), BgL_argsz00_2379);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31846ze3z87_2383,
																						(int) (3L), BgL_namez00_2378);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31846ze3z87_2383,
																						(int) (4L), BgL_xz00_2375);
																					BgL_arg1836z00_2374 =
																						BGl_withzd2lexicalzd2zzexpand_epsz00
																						(BgL_arg1843z00_2381,
																						CNST_TABLE_REF(4),
																						BgL_arg1844z00_2382,
																						BgL_zc3z04anonymousza31846ze3z87_2383);
																}}}}
															else
																{	/* Expand/let.scm 157 */
																	goto BgL_tagzd2543zd2_2377;
																}
														}
													else
														{	/* Expand/let.scm 157 */
															goto BgL_tagzd2543zd2_2377;
														}
												}
											}
											BgL_newtail1268z00_2373 =
												MAKE_YOUNG_PAIR(BgL_arg1836z00_2374, BNIL);
										}
										SET_CDR(BgL_tail1267z00_2372, BgL_newtail1268z00_2373);
										{	/* Expand/let.scm 156 */
											obj_t BgL_arg1835z00_2388;

											BgL_arg1835z00_2388 = CDR(((obj_t) BgL_l1264z00_2371));
											{
												obj_t BgL_tail1267z00_3169;
												obj_t BgL_l1264z00_3168;

												BgL_l1264z00_3168 = BgL_arg1835z00_2388;
												BgL_tail1267z00_3169 = BgL_newtail1268z00_2373;
												BgL_tail1267z00_2372 = BgL_tail1267z00_3169;
												BgL_l1264z00_2371 = BgL_l1264z00_3168;
												goto BgL_zc3z04anonymousza31833ze3z87_2370;
											}
										}
									}
							}
						}
					{	/* Expand/let.scm 170 */
						obj_t BgL_arg1820z00_2389;

						{	/* Expand/let.scm 170 */
							obj_t BgL_arg1822z00_2390;

							{	/* Expand/let.scm 170 */
								obj_t BgL_arg1823z00_2391;

								{	/* Expand/let.scm 170 */
									obj_t BgL_arg1831z00_2392;

									BgL_arg1831z00_2392 =
										BGl_expandzd2prognzd2zz__prognz00(BgL_bodyz00_2340);
									BgL_arg1823z00_2391 =
										BGL_PROCEDURE_CALL2(BgL_ez00_2338, BgL_arg1831z00_2392,
										BgL_ez00_2338);
								}
								BgL_arg1822z00_2390 =
									MAKE_YOUNG_PAIR(BgL_arg1823z00_2391, BNIL);
							}
							BgL_arg1820z00_2389 =
								MAKE_YOUNG_PAIR(BgL_newz00_2368, BgL_arg1822z00_2390);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1820z00_2389);
					}
				}
			}
		}

	}



/* &<@anonymous:1846> */
	obj_t BGl_z62zc3z04anonymousza31846ze3ze5zzexpand_letz00(obj_t
		BgL_envz00_2341)
	{
		{	/* Expand/let.scm 163 */
			{	/* Expand/let.scm 164 */
				obj_t BgL_lbodyz00_2342;
				obj_t BgL_ez00_2343;
				obj_t BgL_argsz00_2344;
				obj_t BgL_namez00_2345;
				obj_t BgL_xz00_2346;

				BgL_lbodyz00_2342 = PROCEDURE_REF(BgL_envz00_2341, (int) (0L));
				BgL_ez00_2343 = ((obj_t) PROCEDURE_REF(BgL_envz00_2341, (int) (1L)));
				BgL_argsz00_2344 = PROCEDURE_REF(BgL_envz00_2341, (int) (2L));
				BgL_namez00_2345 = PROCEDURE_REF(BgL_envz00_2341, (int) (3L));
				BgL_xz00_2346 = PROCEDURE_REF(BgL_envz00_2341, (int) (4L));
				{	/* Expand/let.scm 164 */
					obj_t BgL_bodyz00_2393;

					{	/* Expand/let.scm 164 */
						obj_t BgL_arg1849z00_2394;

						BgL_arg1849z00_2394 =
							BGl_expandzd2prognzd2zz__prognz00(BgL_lbodyz00_2342);
						BgL_bodyz00_2393 =
							BGL_PROCEDURE_CALL2(BgL_ez00_2343, BgL_arg1849z00_2394,
							BgL_ez00_2343);
					}
					{	/* Expand/let.scm 164 */
						obj_t BgL_bz00_2395;

						{	/* Expand/let.scm 165 */
							obj_t BgL_arg1847z00_2396;

							{	/* Expand/let.scm 165 */
								obj_t BgL_arg1848z00_2397;

								BgL_arg1848z00_2397 = MAKE_YOUNG_PAIR(BgL_bodyz00_2393, BNIL);
								BgL_arg1847z00_2396 =
									MAKE_YOUNG_PAIR(BgL_argsz00_2344, BgL_arg1848z00_2397);
							}
							BgL_bz00_2395 =
								MAKE_YOUNG_PAIR(BgL_namez00_2345, BgL_arg1847z00_2396);
						}
						{	/* Expand/let.scm 165 */

							return
								BGl_epairifyz00zztools_miscz00(BgL_bz00_2395, BgL_xz00_2346);
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_letz00(void)
	{
		{	/* Expand/let.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_letz00(void)
	{
		{	/* Expand/let.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_letz00(void)
	{
		{	/* Expand/let.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_letz00(void)
	{
		{	/* Expand/let.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2013z00zzexpand_letz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2013z00zzexpand_letz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2013z00zzexpand_letz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string2013z00zzexpand_letz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string2013z00zzexpand_letz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2013z00zzexpand_letz00));
			BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(223654864L,
				BSTRING_TO_STRING(BGl_string2013z00zzexpand_letz00));
			BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string2013z00zzexpand_letz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2013z00zzexpand_letz00));
		}

	}

#ifdef __cplusplus
}
#endif
