/*===========================================================================*/
/*   (Expand/assert.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/assert.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_ASSERT_TYPE_DEFINITIONS
#define BGL_EXPAND_ASSERT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_EXPAND_ASSERT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_expandzd2assertzd2zzexpand_assertz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_assertz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzexpand_assertz00(void);
	static obj_t BGl_genericzd2initzd2zzexpand_assertz00(void);
	static obj_t BGl_objectzd2initzd2zzexpand_assertz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_assertz00(void);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_makezd2onezd2assertz00zzexpand_assertz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_replacez12z12zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzexpand_assertz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static obj_t BGl_z62expandzd2assertzb0zzexpand_assertz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_assertz00(void);
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_assertz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_assertz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_assertz00(void);
	extern obj_t BGl_za2bmemzd2profilingza2zd2zzengine_paramz00;
	static obj_t BGl_dupz00zzexpand_assertz00(obj_t);
	BGL_IMPORT obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t __cnst[9];


	   
		 
		DEFINE_STRING(BGl_string1196z00zzexpand_assertz00,
		BgL_bgl_string1196za700za7za7e1200za7, "Illegal `assert' form", 21);
	      DEFINE_STRING(BGl_string1197z00zzexpand_assertz00,
		BgL_bgl_string1197za700za7za7e1201za7, "expand_assert", 13);
	      DEFINE_STRING(BGl_string1198z00zzexpand_assertz00,
		BgL_bgl_string1198za700za7za7e1202za7,
		"if notify-assert-fail begin location define-primop! quote check bdb assert ",
		75);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2assertzd2envz00zzexpand_assertz00,
		BgL_bgl_za762expandza7d2asse1203z00,
		BGl_z62expandzd2assertzb0zzexpand_assertz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzexpand_assertz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzexpand_assertz00(long
		BgL_checksumz00_300, char *BgL_fromz00_301)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_assertz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_assertz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_assertz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_assertz00();
					BGl_cnstzd2initzd2zzexpand_assertz00();
					BGl_importedzd2moduleszd2initz00zzexpand_assertz00();
					return BGl_toplevelzd2initzd2zzexpand_assertz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_assertz00(void)
	{
		{	/* Expand/assert.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_assert");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_assert");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"expand_assert");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_assert");
			BGl_modulezd2initializa7ationz75zz__prognz00(0L, "expand_assert");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"expand_assert");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_assert");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "expand_assert");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "expand_assert");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_assertz00(void)
	{
		{	/* Expand/assert.scm 15 */
			{	/* Expand/assert.scm 15 */
				obj_t BgL_cportz00_289;

				{	/* Expand/assert.scm 15 */
					obj_t BgL_stringz00_296;

					BgL_stringz00_296 = BGl_string1198z00zzexpand_assertz00;
					{	/* Expand/assert.scm 15 */
						obj_t BgL_startz00_297;

						BgL_startz00_297 = BINT(0L);
						{	/* Expand/assert.scm 15 */
							obj_t BgL_endz00_298;

							BgL_endz00_298 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_296)));
							{	/* Expand/assert.scm 15 */

								BgL_cportz00_289 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_296, BgL_startz00_297, BgL_endz00_298);
				}}}}
				{
					long BgL_iz00_290;

					BgL_iz00_290 = 8L;
				BgL_loopz00_291:
					if ((BgL_iz00_290 == -1L))
						{	/* Expand/assert.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/assert.scm 15 */
							{	/* Expand/assert.scm 15 */
								obj_t BgL_arg1199z00_292;

								{	/* Expand/assert.scm 15 */

									{	/* Expand/assert.scm 15 */
										obj_t BgL_locationz00_294;

										BgL_locationz00_294 = BBOOL(((bool_t) 0));
										{	/* Expand/assert.scm 15 */

											BgL_arg1199z00_292 =
												BGl_readz00zz__readerz00(BgL_cportz00_289,
												BgL_locationz00_294);
										}
									}
								}
								{	/* Expand/assert.scm 15 */
									int BgL_tmpz00_328;

									BgL_tmpz00_328 = (int) (BgL_iz00_290);
									CNST_TABLE_SET(BgL_tmpz00_328, BgL_arg1199z00_292);
							}}
							{	/* Expand/assert.scm 15 */
								int BgL_auxz00_295;

								BgL_auxz00_295 = (int) ((BgL_iz00_290 - 1L));
								{
									long BgL_iz00_333;

									BgL_iz00_333 = (long) (BgL_auxz00_295);
									BgL_iz00_290 = BgL_iz00_333;
									goto BgL_loopz00_291;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_assertz00(void)
	{
		{	/* Expand/assert.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzexpand_assertz00(void)
	{
		{	/* Expand/assert.scm 15 */
			return BUNSPEC;
		}

	}



/* expand-assert */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2assertzd2zzexpand_assertz00(obj_t
		BgL_xz00_17, obj_t BgL_ez00_18)
	{
		{	/* Expand/assert.scm 26 */
			{
				obj_t BgL_varsz00_144;
				obj_t BgL_predz00_145;
				obj_t BgL_varsz00_141;
				obj_t BgL_bodyz00_142;

				if (PAIRP(BgL_xz00_17))
					{	/* Expand/assert.scm 27 */
						obj_t BgL_cdrzd2111zd2_150;

						BgL_cdrzd2111zd2_150 = CDR(((obj_t) BgL_xz00_17));
						if (PAIRP(BgL_cdrzd2111zd2_150))
							{	/* Expand/assert.scm 27 */
								obj_t BgL_cdrzd2115zd2_152;

								BgL_cdrzd2115zd2_152 = CDR(BgL_cdrzd2111zd2_150);
								if ((CAR(BgL_cdrzd2111zd2_150) == CNST_TABLE_REF(2)))
									{	/* Expand/assert.scm 27 */
										if (PAIRP(BgL_cdrzd2115zd2_152))
											{	/* Expand/assert.scm 27 */
												obj_t BgL_carzd2118zd2_156;

												BgL_carzd2118zd2_156 = CAR(BgL_cdrzd2115zd2_152);
												if (PAIRP(BgL_carzd2118zd2_156))
													{	/* Expand/assert.scm 27 */
														BgL_varsz00_141 = BgL_carzd2118zd2_156;
														BgL_bodyz00_142 = CDR(BgL_cdrzd2115zd2_152);
														{	/* Expand/assert.scm 30 */
															obj_t BgL_newz00_172;

															{	/* Expand/assert.scm 30 */
																obj_t BgL_arg1083z00_173;

																BgL_arg1083z00_173 =
																	MAKE_YOUNG_PAIR(BgL_varsz00_141,
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_bodyz00_142, BNIL));
																BgL_newz00_172 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																	BgL_arg1083z00_173);
															}
															BGl_replacez12z12zztools_miscz00(BgL_xz00_17,
																BgL_newz00_172);
															return BGL_PROCEDURE_CALL2(BgL_ez00_18,
																BgL_xz00_17, BgL_ez00_18);
														}
													}
												else
													{	/* Expand/assert.scm 27 */
														obj_t BgL_carzd2131zd2_160;

														BgL_carzd2131zd2_160 =
															CAR(((obj_t) BgL_cdrzd2111zd2_150));
														if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
															(BgL_carzd2131zd2_160))
															{	/* Expand/assert.scm 27 */
																obj_t BgL_arg1076z00_162;

																BgL_arg1076z00_162 =
																	CDR(((obj_t) BgL_cdrzd2111zd2_150));
																BgL_varsz00_144 = BgL_carzd2131zd2_160;
																BgL_predz00_145 = BgL_arg1076z00_162;
															BgL_tagzd2102zd2_146:
																{	/* Expand/assert.scm 34 */
																	bool_t BgL_test1212z00_369;

																	{	/* Expand/assert.scm 34 */
																		bool_t BgL_test1213z00_370;

																		if (INTEGERP
																			(BGl_za2compilerzd2debugza2zd2zzengine_paramz00))
																			{	/* Expand/assert.scm 34 */
																				if (
																					((long)
																						CINT
																						(BGl_za2compilerzd2debugza2zd2zzengine_paramz00)
																						>= 1L))
																					{	/* Expand/assert.scm 35 */
																						if (CBOOL
																							(BGl_za2bmemzd2profilingza2zd2zzengine_paramz00))
																							{	/* Expand/assert.scm 36 */
																								BgL_test1213z00_370 =
																									((bool_t) 0);
																							}
																						else
																							{	/* Expand/assert.scm 36 */
																								BgL_test1213z00_370 =
																									((bool_t) 1);
																							}
																					}
																				else
																					{	/* Expand/assert.scm 35 */
																						BgL_test1213z00_370 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Expand/assert.scm 34 */
																				BgL_test1213z00_370 = ((bool_t) 0);
																			}
																		if (BgL_test1213z00_370)
																			{	/* Expand/assert.scm 34 */
																				BgL_test1212z00_369 = ((bool_t) 1);
																			}
																		else
																			{	/* Expand/assert.scm 37 */
																				obj_t BgL__andtest_1048z00_187;

																				{	/* Expand/assert.scm 37 */
																					obj_t BgL_arg1092z00_189;

																					{	/* Expand/assert.scm 37 */
																						obj_t BgL_arg1097z00_190;

																						BgL_arg1097z00_190 =
																							BGl_thezd2backendzd2zzbackend_backendz00
																							();
																						BgL_arg1092z00_189 =
																							(((BgL_backendz00_bglt)
																								COBJECT(((BgL_backendz00_bglt)
																										BgL_arg1097z00_190)))->
																							BgL_debugzd2supportzd2);
																					}
																					BgL__andtest_1048z00_187 =
																						BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																						(CNST_TABLE_REF(1),
																						BgL_arg1092z00_189);
																				}
																				if (CBOOL(BgL__andtest_1048z00_187))
																					{	/* Expand/assert.scm 37 */
																						if (INTEGERP
																							(BGl_za2bdbzd2debugza2zd2zzengine_paramz00))
																							{	/* Expand/assert.scm 38 */
																								BgL_test1212z00_369 =
																									(
																									(long)
																									CINT
																									(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
																									>= 1L);
																							}
																						else
																							{	/* Expand/assert.scm 38 */
																								BgL_test1212z00_369 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Expand/assert.scm 37 */
																						BgL_test1212z00_369 = ((bool_t) 0);
																					}
																			}
																	}
																	if (BgL_test1212z00_369)
																		{	/* Expand/assert.scm 34 */
																			return
																				BGl_replacez12z12zztools_miscz00
																				(BgL_xz00_17,
																				BGl_makezd2onezd2assertz00zzexpand_assertz00
																				(BgL_ez00_18, BgL_xz00_17,
																					BgL_varsz00_144, BgL_predz00_145));
																		}
																	else
																		{	/* Expand/assert.scm 34 */
																			return BUNSPEC;
																		}
																}
															}
														else
															{	/* Expand/assert.scm 27 */
															BgL_tagzd2103zd2_147:
																return
																	BGl_errorz00zz__errorz00(BFALSE,
																	BGl_string1196z00zzexpand_assertz00,
																	BgL_xz00_17);
															}
													}
											}
										else
											{	/* Expand/assert.scm 27 */
												obj_t BgL_carzd2144zd2_164;

												BgL_carzd2144zd2_164 =
													CAR(((obj_t) BgL_cdrzd2111zd2_150));
												if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
													(BgL_carzd2144zd2_164))
													{	/* Expand/assert.scm 27 */
														obj_t BgL_arg1078z00_166;

														BgL_arg1078z00_166 =
															CDR(((obj_t) BgL_cdrzd2111zd2_150));
														{
															obj_t BgL_predz00_399;
															obj_t BgL_varsz00_398;

															BgL_varsz00_398 = BgL_carzd2144zd2_164;
															BgL_predz00_399 = BgL_arg1078z00_166;
															BgL_predz00_145 = BgL_predz00_399;
															BgL_varsz00_144 = BgL_varsz00_398;
															goto BgL_tagzd2102zd2_146;
														}
													}
												else
													{	/* Expand/assert.scm 27 */
														goto BgL_tagzd2103zd2_147;
													}
											}
									}
								else
									{	/* Expand/assert.scm 27 */
										obj_t BgL_carzd2157zd2_168;

										BgL_carzd2157zd2_168 = CAR(((obj_t) BgL_cdrzd2111zd2_150));
										if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
											(BgL_carzd2157zd2_168))
											{	/* Expand/assert.scm 27 */
												obj_t BgL_arg1080z00_170;

												BgL_arg1080z00_170 =
													CDR(((obj_t) BgL_cdrzd2111zd2_150));
												{
													obj_t BgL_predz00_407;
													obj_t BgL_varsz00_406;

													BgL_varsz00_406 = BgL_carzd2157zd2_168;
													BgL_predz00_407 = BgL_arg1080z00_170;
													BgL_predz00_145 = BgL_predz00_407;
													BgL_varsz00_144 = BgL_varsz00_406;
													goto BgL_tagzd2102zd2_146;
												}
											}
										else
											{	/* Expand/assert.scm 27 */
												goto BgL_tagzd2103zd2_147;
											}
									}
							}
						else
							{	/* Expand/assert.scm 27 */
								goto BgL_tagzd2103zd2_147;
							}
					}
				else
					{	/* Expand/assert.scm 27 */
						goto BgL_tagzd2103zd2_147;
					}
			}
		}

	}



/* &expand-assert */
	obj_t BGl_z62expandzd2assertzb0zzexpand_assertz00(obj_t BgL_envz00_285,
		obj_t BgL_xz00_286, obj_t BgL_ez00_287)
	{
		{	/* Expand/assert.scm 26 */
			return
				BGl_expandzd2assertzd2zzexpand_assertz00(BgL_xz00_286, BgL_ez00_287);
		}

	}



/* make-one-assert */
	obj_t BGl_makezd2onezd2assertz00zzexpand_assertz00(obj_t BgL_ez00_19,
		obj_t BgL_expz00_20, obj_t BgL_varsz00_21, obj_t BgL_predz00_22)
	{
		{	/* Expand/assert.scm 49 */
			{	/* Expand/assert.scm 50 */
				obj_t BgL_predz00_193;

				if (NULLP(BgL_predz00_22))
					{	/* Expand/assert.scm 51 */
						obj_t BgL_list1185z00_239;

						BgL_list1185z00_239 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
						BgL_predz00_193 = BgL_list1185z00_239;
					}
				else
					{	/* Expand/assert.scm 50 */
						BgL_predz00_193 = BgL_predz00_22;
					}
				{	/* Expand/assert.scm 50 */
					obj_t BgL_oldzd2predzd2_194;

					BgL_oldzd2predzd2_194 = BGl_dupz00zzexpand_assertz00(BgL_predz00_193);
					{	/* Expand/assert.scm 53 */

						{	/* Expand/assert.scm 54 */
							obj_t BgL_arg1102z00_195;

							{	/* Expand/assert.scm 54 */
								obj_t BgL_arg1103z00_196;
								obj_t BgL_arg1104z00_197;

								{	/* Expand/assert.scm 54 */
									obj_t BgL_arg1114z00_198;

									BgL_arg1114z00_198 =
										BGl_expandzd2prognzd2zz__prognz00(BgL_predz00_193);
									BgL_arg1103z00_196 =
										BGL_PROCEDURE_CALL2(BgL_ez00_19, BgL_arg1114z00_198,
										BgL_ez00_19);
								}
								{	/* Expand/assert.scm 58 */
									obj_t BgL_arg1115z00_199;

									{	/* Expand/assert.scm 58 */
										obj_t BgL_arg1122z00_200;

										{	/* Expand/assert.scm 58 */
											obj_t BgL_arg1123z00_201;

											{	/* Expand/assert.scm 58 */
												obj_t BgL_arg1125z00_202;
												obj_t BgL_arg1126z00_203;

												{
													obj_t BgL_varsz00_206;
													obj_t BgL_defsz00_207;

													BgL_varsz00_206 = BgL_varsz00_21;
													BgL_defsz00_207 = BNIL;
												BgL_zc3z04anonymousza31127ze3z87_208:
													if (NULLP(BgL_varsz00_206))
														{	/* Expand/assert.scm 60 */
															BgL_arg1125z00_202 = BgL_defsz00_207;
														}
													else
														{	/* Expand/assert.scm 62 */
															obj_t BgL_arg1129z00_210;
															obj_t BgL_arg1131z00_211;

															BgL_arg1129z00_210 =
																CDR(((obj_t) BgL_varsz00_206));
															{	/* Expand/assert.scm 63 */
																obj_t BgL_arg1132z00_212;

																{	/* Expand/assert.scm 63 */
																	obj_t BgL_arg1137z00_213;

																	{	/* Expand/assert.scm 63 */
																		obj_t BgL_arg1138z00_214;
																		obj_t BgL_arg1140z00_215;

																		{	/* Expand/assert.scm 63 */
																			obj_t BgL_arg1141z00_216;

																			{	/* Expand/assert.scm 63 */
																				obj_t BgL_arg1142z00_217;

																				BgL_arg1142z00_217 =
																					CAR(((obj_t) BgL_varsz00_206));
																				BgL_arg1141z00_216 =
																					MAKE_YOUNG_PAIR(BgL_arg1142z00_217,
																					BNIL);
																			}
																			BgL_arg1138z00_214 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																				BgL_arg1141z00_216);
																		}
																		{	/* Expand/assert.scm 63 */
																			obj_t BgL_arg1143z00_218;

																			BgL_arg1143z00_218 =
																				CAR(((obj_t) BgL_varsz00_206));
																			BgL_arg1140z00_215 =
																				MAKE_YOUNG_PAIR(BgL_arg1143z00_218,
																				BNIL);
																		}
																		BgL_arg1137z00_213 =
																			MAKE_YOUNG_PAIR(BgL_arg1138z00_214,
																			BgL_arg1140z00_215);
																	}
																	BgL_arg1132z00_212 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																		BgL_arg1137z00_213);
																}
																BgL_arg1131z00_211 =
																	MAKE_YOUNG_PAIR(BgL_arg1132z00_212,
																	BgL_defsz00_207);
															}
															{
																obj_t BgL_defsz00_436;
																obj_t BgL_varsz00_435;

																BgL_varsz00_435 = BgL_arg1129z00_210;
																BgL_defsz00_436 = BgL_arg1131z00_211;
																BgL_defsz00_207 = BgL_defsz00_436;
																BgL_varsz00_206 = BgL_varsz00_435;
																goto BgL_zc3z04anonymousza31127ze3z87_208;
															}
														}
												}
												{	/* Expand/assert.scm 65 */
													obj_t BgL_arg1145z00_220;

													{	/* Expand/assert.scm 65 */
														obj_t BgL_locz00_221;

														{	/* Expand/assert.scm 65 */
															obj_t BgL_locz00_236;

															BgL_locz00_236 =
																BGl_findzd2locationzd2zztools_locationz00
																(BgL_expz00_20);
															{	/* Expand/assert.scm 66 */
																bool_t BgL_test1223z00_438;

																if (STRUCTP(BgL_locz00_236))
																	{	/* Expand/assert.scm 66 */
																		BgL_test1223z00_438 =
																			(STRUCT_KEY(BgL_locz00_236) ==
																			CNST_TABLE_REF(5));
																	}
																else
																	{	/* Expand/assert.scm 66 */
																		BgL_test1223z00_438 = ((bool_t) 0);
																	}
																if (BgL_test1223z00_438)
																	{	/* Expand/assert.scm 66 */
																		BgL_locz00_221 = BgL_locz00_236;
																	}
																else
																	{	/* Expand/assert.scm 66 */
																		BgL_locz00_221 =
																			BGl_findzd2locationzd2zztools_locationz00
																			(BgL_predz00_193);
																	}
															}
														}
														{	/* Expand/assert.scm 69 */
															obj_t BgL_arg1148z00_222;

															{	/* Expand/assert.scm 69 */
																obj_t BgL_arg1149z00_223;
																obj_t BgL_arg1152z00_224;

																{	/* Expand/assert.scm 69 */
																	obj_t BgL_arg1153z00_225;

																	BgL_arg1153z00_225 =
																		MAKE_YOUNG_PAIR(BgL_varsz00_21, BNIL);
																	BgL_arg1149z00_223 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																		BgL_arg1153z00_225);
																}
																{	/* Expand/assert.scm 70 */
																	obj_t BgL_arg1154z00_226;
																	obj_t BgL_arg1157z00_227;

																	{	/* Expand/assert.scm 70 */
																		obj_t BgL_arg1158z00_228;

																		{	/* Expand/assert.scm 70 */
																			obj_t BgL_arg1162z00_229;

																			BgL_arg1162z00_229 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																				BgL_oldzd2predzd2_194);
																			BgL_arg1158z00_228 =
																				MAKE_YOUNG_PAIR(BgL_arg1162z00_229,
																				BNIL);
																		}
																		BgL_arg1154z00_226 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																			BgL_arg1158z00_228);
																	}
																	{	/* Expand/assert.scm 71 */
																		obj_t BgL_arg1164z00_230;

																		{	/* Expand/assert.scm 71 */
																			bool_t BgL_test1225z00_453;

																			if (STRUCTP(BgL_locz00_221))
																				{	/* Expand/assert.scm 71 */
																					BgL_test1225z00_453 =
																						(STRUCT_KEY(BgL_locz00_221) ==
																						CNST_TABLE_REF(5));
																				}
																			else
																				{	/* Expand/assert.scm 71 */
																					BgL_test1225z00_453 = ((bool_t) 0);
																				}
																			if (BgL_test1225z00_453)
																				{	/* Expand/assert.scm 72 */
																					obj_t BgL_arg1166z00_232;

																					{	/* Expand/assert.scm 72 */
																						obj_t BgL_arg1171z00_233;

																						BgL_arg1171z00_233 =
																							MAKE_YOUNG_PAIR
																							(BGl_locationzd2fullzd2fnamez00zztools_locationz00
																							(BgL_locz00_221),
																							STRUCT_REF(BgL_locz00_221,
																								(int) (1L)));
																						BgL_arg1166z00_232 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1171z00_233, BNIL);
																					}
																					BgL_arg1164z00_230 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																						BgL_arg1166z00_232);
																				}
																			else
																				{	/* Expand/assert.scm 71 */
																					BgL_arg1164z00_230 = BFALSE;
																				}
																		}
																		BgL_arg1157z00_227 =
																			MAKE_YOUNG_PAIR(BgL_arg1164z00_230, BNIL);
																	}
																	BgL_arg1152z00_224 =
																		MAKE_YOUNG_PAIR(BgL_arg1154z00_226,
																		BgL_arg1157z00_227);
																}
																BgL_arg1148z00_222 =
																	MAKE_YOUNG_PAIR(BgL_arg1149z00_223,
																	BgL_arg1152z00_224);
															}
															BgL_arg1145z00_220 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																BgL_arg1148z00_222);
														}
													}
													BgL_arg1126z00_203 =
														MAKE_YOUNG_PAIR(BgL_arg1145z00_220, BNIL);
												}
												BgL_arg1123z00_201 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1125z00_202, BgL_arg1126z00_203);
											}
											BgL_arg1122z00_200 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg1123z00_201);
										}
										BgL_arg1115z00_199 =
											MAKE_YOUNG_PAIR(BgL_arg1122z00_200, BNIL);
									}
									BgL_arg1104z00_197 =
										MAKE_YOUNG_PAIR(BUNSPEC, BgL_arg1115z00_199);
								}
								BgL_arg1102z00_195 =
									MAKE_YOUNG_PAIR(BgL_arg1103z00_196, BgL_arg1104z00_197);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BgL_arg1102z00_195);
						}
					}
				}
			}
		}

	}



/* dup */
	obj_t BGl_dupz00zzexpand_assertz00(obj_t BgL_predz00_23)
	{
		{	/* Expand/assert.scm 79 */
			if (PAIRP(BgL_predz00_23))
				{	/* Expand/assert.scm 81 */
					return
						MAKE_YOUNG_PAIR(BGl_dupz00zzexpand_assertz00(CAR(BgL_predz00_23)),
						BGl_dupz00zzexpand_assertz00(CDR(BgL_predz00_23)));
				}
			else
				{	/* Expand/assert.scm 81 */
					return BgL_predz00_23;
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_assertz00(void)
	{
		{	/* Expand/assert.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_assertz00(void)
	{
		{	/* Expand/assert.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_assertz00(void)
	{
		{	/* Expand/assert.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_assertz00(void)
	{
		{	/* Expand/assert.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1197z00zzexpand_assertz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1197z00zzexpand_assertz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1197z00zzexpand_assertz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1197z00zzexpand_assertz00));
		}

	}

#ifdef __cplusplus
}
#endif
