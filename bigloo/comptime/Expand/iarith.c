/*===========================================================================*/
/*   (Expand/iarith.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/iarith.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_IARITHMETIQUE_TYPE_DEFINITIONS
#define BGL_EXPAND_IARITHMETIQUE_TYPE_DEFINITIONS
#endif													// BGL_EXPAND_IARITHMETIQUE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2bitzd2rshs32z62zzexpand_iarithmetiquez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_iarithmetiquez00 =
		BUNSPEC;
	static obj_t BGl_z62expandzd2bitzd2lshz62zzexpand_iarithmetiquez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62expandzd2minfxzb0zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2izc3zd3zc2zzexpand_iarithmetiquez00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2bitzd2rshu32z62zzexpand_iarithmetiquez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_iarithmetiquez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2ize3zd3ze2zzexpand_iarithmetiquez00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2izc3zd3za0zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_iarithmetiquez00(void);
	static obj_t BGl_z62expandzd2bitzd2lshu32z62zzexpand_iarithmetiquez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62expandzd2iza2z12zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2izb2z02zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_bignum_mul(obj_t, obj_t);
	static obj_t BGl_z62expandzd2izd2z62zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2izf2z42zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2bitzd2ors32z00zzexpand_iarithmetiquez00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2izc3z73zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2izd3z63zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2ize3z53zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2bitzd2lshz00zzexpand_iarithmetiquez00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2ize3zd3z80zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62expandzd2bitzd2urshu32z62zzexpand_iarithmetiquez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2iza2z70zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2izb2z60zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_iarithmetiquez00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2izd2z00zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2izf2z20zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2izc3z11zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2izd3z01zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2ize3z31zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2bitzd2ors32z62zzexpand_iarithmetiquez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static obj_t BGl_z62expandzd2bitzd2rshz62zzexpand_iarithmetiquez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2maxfxzd2zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2za2fxz70zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	extern obj_t BGl_replacez12z12zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzexpand_iarithmetiquez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2eqzf3z21zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2zb2fxz60zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2za2fxz12zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2bitzd2rshz00zzexpand_iarithmetiquez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2bitzd2urshz00zzexpand_iarithmetiquez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2bitzd2rshs32z00zzexpand_iarithmetiquez00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2bitzd2urshz62zzexpand_iarithmetiquez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62expandzd2eqzf3z43zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2minfxzd2zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_iarithmetiquez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_iarithmetiquez00(void);
	static obj_t BGl_z62expandzd2zb2fxz02zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_iarithmetiquez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_iarithmetiquez00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2zd2fxz00zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	extern obj_t BGl_epairifyzd2reczd2zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2bitzd2urshu32z00zzexpand_iarithmetiquez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2bitzd2rshu32z00zzexpand_iarithmetiquez00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2zd2fxz62zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2bitzd2lshu32z00zzexpand_iarithmetiquez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2zf2fxz20zzexpand_iarithmetiquez00(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2zf2fxz42zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2maxfxzb0zzexpand_iarithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t __cnst[26];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2bitzd2lshu32zd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2bitza73071za7,
		BGl_z62expandzd2bitzd2lshu32z62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2zf2fxzd2envzf2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2za7f2f3072za7,
		BGl_z62expandzd2zf2fxz42zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3051z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3051za700za7za7e3073za7,
		"Incorrect number of arguments for `eq?'", 39);
	      DEFINE_STRING(BGl_string3052z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3052za700za7za7e3074za7, "/", 1);
	      DEFINE_STRING(BGl_string3053z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3053za700za7za7e3075za7,
		"Turning \"(/ ...)\" into \"(/fx ...)\" which may result in precision penalty",
		72);
	      DEFINE_STRING(BGl_string3054z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3054za700za7za7e3076za7,
		"\" which may result in precision penalty", 39);
	      DEFINE_STRING(BGl_string3055z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3055za700za7za7e3077za7, ")\" into \"", 9);
	      DEFINE_STRING(BGl_string3056z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3056za700za7za7e3078za7, " ", 1);
	      DEFINE_STRING(BGl_string3057z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3057za700za7za7e3079za7, "Turning \"(/ ", 12);
	      DEFINE_STRING(BGl_string3058z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3058za700za7za7e3080za7,
		"Turning \"(/ ... ...)\" into \"(/fx ... ...)\" which may result in precision penalty",
		80);
	      DEFINE_STRING(BGl_string3059z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3059za700za7za7e3081za7,
		"Turning \"(/ ... ... ...)\" into \"(/fx ... (* ... ...))\" which may result in precision penalty",
		92);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2za2fxzd2envza2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2za7a2f3082za7,
		BGl_z62expandzd2za2fxz12zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3060z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3060za700za7za7e3083za7,
		"Incorrect number of arguments for `+fx'", 39);
	      DEFINE_STRING(BGl_string3061z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3061za700za7za7e3084za7,
		"Incorrect number of arguments for `-fx'", 39);
	      DEFINE_STRING(BGl_string3062z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3062za700za7za7e3085za7,
		"Incorrect number of arguments for `*fx'", 39);
	      DEFINE_STRING(BGl_string3063z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3063za700za7za7e3086za7,
		"Incorrect number of arguments for `/fx'", 39);
	      DEFINE_STRING(BGl_string3064z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3064za700za7za7e3087za7,
		"Incorrect number of arguments for `maxfx'", 41);
	      DEFINE_STRING(BGl_string3065z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3065za700za7za7e3088za7,
		"Incorrect number of arguments for `minfx'", 41);
	      DEFINE_STRING(BGl_string3066z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3066za700za7za7e3089za7, "expand_iarithmetique", 20);
	      DEFINE_STRING(BGl_string3067z00zzexpand_iarithmetiquez00,
		BgL_bgl_string3067za700za7za7e3090za7,
		"minfx maxfx let if b a >= >=fx <= <=fx > >fx < <fx and = =fx /fx * *fx -fx negfx + +fx quote c-eq? ",
		99);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2bitzd2lshzd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2bitza73091za7,
		BGl_z62expandzd2bitzd2lshz62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2ize3zd3zd2envz30zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2iza7e33092za7,
		BGl_z62expandzd2ize3zd3z80zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2iza2zd2envza2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2iza7a23093za7,
		BGl_z62expandzd2iza2z12zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2zb2fxzd2envzb2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2za7b2f3094za7,
		BGl_z62expandzd2zb2fxz02zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2bitzd2rshzd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2bitza73095za7,
		BGl_z62expandzd2bitzd2rshz62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2bitzd2urshu32zd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2bitza73096za7,
		BGl_z62expandzd2bitzd2urshu32z62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2bitzd2rshs32zd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2bitza73097za7,
		BGl_z62expandzd2bitzd2rshs32z62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2izb2zd2envzb2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2iza7b23098za7,
		BGl_z62expandzd2izb2z02zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2bitzd2ors32zd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2bitza73099za7,
		BGl_z62expandzd2bitzd2ors32z62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2minfxzd2envz00zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2minf3100z00,
		BGl_z62expandzd2minfxzb0zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2izc3zd2envzc3zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2iza7c33101za7,
		BGl_z62expandzd2izc3z73zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2maxfxzd2envz00zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2maxf3102z00,
		BGl_z62expandzd2maxfxzb0zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2izd2zd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2iza7d23103za7,
		BGl_z62expandzd2izd2z62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2izd3zd2envzd3zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2iza7d33104za7,
		BGl_z62expandzd2izd3z63zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2zd2fxzd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2za7d2f3105za7,
		BGl_z62expandzd2zd2fxz62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2bitzd2rshu32zd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2bitza73106za7,
		BGl_z62expandzd2bitzd2rshu32z62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2eqzf3zd2envzf3zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2eqza7f3107za7,
		BGl_z62expandzd2eqzf3z43zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2ize3zd2envze3zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2iza7e33108za7,
		BGl_z62expandzd2ize3z53zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2izf2zd2envzf2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2iza7f23109za7,
		BGl_z62expandzd2izf2z42zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2izc3zd3zd2envz10zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2iza7c33110za7,
		BGl_z62expandzd2izc3zd3za0zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2bitzd2urshzd2envzd2zzexpand_iarithmetiquez00,
		BgL_bgl_za762expandza7d2bitza73111za7,
		BGl_z62expandzd2bitzd2urshz62zzexpand_iarithmetiquez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzexpand_iarithmetiquez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzexpand_iarithmetiquez00(long
		BgL_checksumz00_3126, char *BgL_fromz00_3127)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_iarithmetiquez00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_iarithmetiquez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_iarithmetiquez00();
					BGl_libraryzd2moduleszd2initz00zzexpand_iarithmetiquez00();
					BGl_cnstzd2initzd2zzexpand_iarithmetiquez00();
					BGl_importedzd2moduleszd2initz00zzexpand_iarithmetiquez00();
					return BGl_methodzd2initzd2zzexpand_iarithmetiquez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_iarithmetiquez00(void)
	{
		{	/* Expand/iarith.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"expand_iarithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_iarithmetique");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_iarithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"expand_iarithmetique");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_iarithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"expand_iarithmetique");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "expand_iarithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"expand_iarithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"expand_iarithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_iarithmetique");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_iarithmetiquez00(void)
	{
		{	/* Expand/iarith.scm 15 */
			{	/* Expand/iarith.scm 15 */
				obj_t BgL_cportz00_3115;

				{	/* Expand/iarith.scm 15 */
					obj_t BgL_stringz00_3122;

					BgL_stringz00_3122 = BGl_string3067z00zzexpand_iarithmetiquez00;
					{	/* Expand/iarith.scm 15 */
						obj_t BgL_startz00_3123;

						BgL_startz00_3123 = BINT(0L);
						{	/* Expand/iarith.scm 15 */
							obj_t BgL_endz00_3124;

							BgL_endz00_3124 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3122)));
							{	/* Expand/iarith.scm 15 */

								BgL_cportz00_3115 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3122, BgL_startz00_3123, BgL_endz00_3124);
				}}}}
				{
					long BgL_iz00_3116;

					BgL_iz00_3116 = 25L;
				BgL_loopz00_3117:
					if ((BgL_iz00_3116 == -1L))
						{	/* Expand/iarith.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/iarith.scm 15 */
							{	/* Expand/iarith.scm 15 */
								obj_t BgL_arg3070z00_3118;

								{	/* Expand/iarith.scm 15 */

									{	/* Expand/iarith.scm 15 */
										obj_t BgL_locationz00_3120;

										BgL_locationz00_3120 = BBOOL(((bool_t) 0));
										{	/* Expand/iarith.scm 15 */

											BgL_arg3070z00_3118 =
												BGl_readz00zz__readerz00(BgL_cportz00_3115,
												BgL_locationz00_3120);
										}
									}
								}
								{	/* Expand/iarith.scm 15 */
									int BgL_tmpz00_3155;

									BgL_tmpz00_3155 = (int) (BgL_iz00_3116);
									CNST_TABLE_SET(BgL_tmpz00_3155, BgL_arg3070z00_3118);
							}}
							{	/* Expand/iarith.scm 15 */
								int BgL_auxz00_3121;

								BgL_auxz00_3121 = (int) ((BgL_iz00_3116 - 1L));
								{
									long BgL_iz00_3160;

									BgL_iz00_3160 = (long) (BgL_auxz00_3121);
									BgL_iz00_3116 = BgL_iz00_3160;
									goto BgL_loopz00_3117;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_iarithmetiquez00(void)
	{
		{	/* Expand/iarith.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* expand-eq? */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2eqzf3z21zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_5, obj_t BgL_ez00_6)
	{
		{	/* Expand/iarith.scm 52 */
			{
				obj_t BgL_xz00_68;
				obj_t BgL_yz00_69;

				if (PAIRP(BgL_xz00_5))
					{	/* Expand/iarith.scm 53 */
						obj_t BgL_cdrzd2112zd2_74;

						BgL_cdrzd2112zd2_74 = CDR(((obj_t) BgL_xz00_5));
						if (PAIRP(BgL_cdrzd2112zd2_74))
							{	/* Expand/iarith.scm 53 */
								obj_t BgL_carzd2114zd2_76;
								obj_t BgL_cdrzd2115zd2_77;

								BgL_carzd2114zd2_76 = CAR(BgL_cdrzd2112zd2_74);
								BgL_cdrzd2115zd2_77 = CDR(BgL_cdrzd2112zd2_74);
								{	/* Expand/iarith.scm 53 */
									bool_t BgL_test3116z00_3171;

									{	/* Expand/iarith.scm 53 */
										bool_t BgL__ortest_1012z00_1003;

										BgL__ortest_1012z00_1003 = INTEGERP(BgL_carzd2114zd2_76);
										if (BgL__ortest_1012z00_1003)
											{	/* Expand/iarith.scm 53 */
												BgL_test3116z00_3171 = BgL__ortest_1012z00_1003;
											}
										else
											{	/* Expand/iarith.scm 53 */
												BgL_test3116z00_3171 = CHARP(BgL_carzd2114zd2_76);
											}
									}
									if (BgL_test3116z00_3171)
										{	/* Expand/iarith.scm 53 */
											if (PAIRP(BgL_cdrzd2115zd2_77))
												{	/* Expand/iarith.scm 53 */
													if ((BgL_carzd2114zd2_76 == CAR(BgL_cdrzd2115zd2_77)))
														{	/* Expand/iarith.scm 53 */
															if (NULLP(CDR(BgL_cdrzd2115zd2_77)))
																{	/* Expand/iarith.scm 53 */
																	return BTRUE;
																}
															else
																{	/* Expand/iarith.scm 53 */
																BgL_tagzd2106zd2_71:
																	return
																		BGl_errorz00zz__errorz00(BFALSE,
																		BGl_string3051z00zzexpand_iarithmetiquez00,
																		BgL_xz00_5);
																}
														}
													else
														{	/* Expand/iarith.scm 53 */
															obj_t BgL_carzd2141zd2_89;
															obj_t BgL_cdrzd2142zd2_90;

															BgL_carzd2141zd2_89 =
																CAR(((obj_t) BgL_cdrzd2112zd2_74));
															BgL_cdrzd2142zd2_90 =
																CDR(((obj_t) BgL_cdrzd2112zd2_74));
															if (PAIRP(BgL_carzd2141zd2_89))
																{	/* Expand/iarith.scm 53 */
																	obj_t BgL_cdrzd2145zd2_92;

																	BgL_cdrzd2145zd2_92 =
																		CDR(BgL_carzd2141zd2_89);
																	if (
																		(CAR(BgL_carzd2141zd2_89) ==
																			CNST_TABLE_REF(1)))
																		{	/* Expand/iarith.scm 53 */
																			if (PAIRP(BgL_cdrzd2145zd2_92))
																				{	/* Expand/iarith.scm 53 */
																					obj_t BgL_yz00_96;

																					BgL_yz00_96 =
																						CAR(BgL_cdrzd2145zd2_92);
																					if (NULLP(CDR(BgL_cdrzd2145zd2_92)))
																						{	/* Expand/iarith.scm 53 */
																							obj_t BgL_carzd2151zd2_99;

																							BgL_carzd2151zd2_99 =
																								CAR(
																								((obj_t) BgL_cdrzd2142zd2_90));
																							if (PAIRP(BgL_carzd2151zd2_99))
																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_cdrzd2155zd2_101;

																									BgL_cdrzd2155zd2_101 =
																										CDR(BgL_carzd2151zd2_99);
																									if (
																										(CAR(BgL_carzd2151zd2_99) ==
																											CNST_TABLE_REF(1)))
																										{	/* Expand/iarith.scm 53 */
																											if (PAIRP
																												(BgL_cdrzd2155zd2_101))
																												{	/* Expand/iarith.scm 53 */
																													if (
																														(BgL_yz00_96 ==
																															CAR
																															(BgL_cdrzd2155zd2_101)))
																														{	/* Expand/iarith.scm 53 */
																															if (NULLP(CDR
																																	(BgL_cdrzd2155zd2_101)))
																																{	/* Expand/iarith.scm 53 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd2142zd2_90))))
																																		{	/* Expand/iarith.scm 53 */
																																			return
																																				BTRUE;
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			goto
																																				BgL_tagzd2106zd2_71;
																																		}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_cdrzd2173zd2_112;
																																	BgL_cdrzd2173zd2_112
																																		=
																																		CDR(((obj_t)
																																			BgL_xz00_5));
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_cdrzd2175zd2_113;
																																		BgL_cdrzd2175zd2_113
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd2173zd2_112));
																																		{	/* Expand/iarith.scm 53 */
																																			bool_t
																																				BgL_test3131z00_3226;
																																			{	/* Expand/iarith.scm 53 */
																																				obj_t
																																					BgL_tmpz00_3227;
																																				BgL_tmpz00_3227
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd2173zd2_112));
																																				BgL_test3131z00_3226
																																					=
																																					CHARP
																																					(BgL_tmpz00_3227);
																																			}
																																			if (BgL_test3131z00_3226)
																																				{	/* Expand/iarith.scm 53 */
																																					bool_t
																																						BgL_test3132z00_3231;
																																					{	/* Expand/iarith.scm 53 */
																																						obj_t
																																							BgL_tmpz00_3232;
																																						BgL_tmpz00_3232
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd2175zd2_113));
																																						BgL_test3132z00_3231
																																							=
																																							CHARP
																																							(BgL_tmpz00_3232);
																																					}
																																					if (BgL_test3132z00_3231)
																																						{	/* Expand/iarith.scm 53 */
																																							if (NULLP(CDR(((obj_t) BgL_cdrzd2175zd2_113))))
																																								{	/* Expand/iarith.scm 53 */
																																									return
																																										BFALSE;
																																								}
																																							else
																																								{	/* Expand/iarith.scm 53 */
																																									goto
																																										BgL_tagzd2106zd2_71;
																																								}
																																						}
																																					else
																																						{	/* Expand/iarith.scm 53 */
																																							if (NULLP(CDR(((obj_t) BgL_cdrzd2175zd2_113))))
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_arg1182z00_125;
																																									obj_t
																																										BgL_arg1183z00_126;
																																									BgL_arg1182z00_125
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2173zd2_112));
																																									BgL_arg1183z00_126
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2175zd2_113));
																																									BgL_xz00_68
																																										=
																																										BgL_arg1182z00_125;
																																									BgL_yz00_69
																																										=
																																										BgL_arg1183z00_126;
																																								BgL_tagzd2105zd2_70:
																																									{	/* Expand/iarith.scm 63 */
																																										obj_t
																																											BgL_arg2294z00_1004;
																																										{	/* Expand/iarith.scm 63 */
																																											obj_t
																																												BgL_arg2295z00_1005;
																																											obj_t
																																												BgL_arg2296z00_1006;
																																											BgL_arg2295z00_1005
																																												=
																																												BGL_PROCEDURE_CALL2
																																												(BgL_ez00_6,
																																												BgL_xz00_68,
																																												BgL_ez00_6);
																																											{	/* Expand/iarith.scm 63 */
																																												obj_t
																																													BgL_arg2297z00_1007;
																																												BgL_arg2297z00_1007
																																													=
																																													BGL_PROCEDURE_CALL2
																																													(BgL_ez00_6,
																																													BgL_yz00_69,
																																													BgL_ez00_6);
																																												BgL_arg2296z00_1006
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg2297z00_1007,
																																													BNIL);
																																											}
																																											BgL_arg2294z00_1004
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg2295z00_1005,
																																												BgL_arg2296z00_1006);
																																										}
																																										return
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(0),
																																											BgL_arg2294z00_1004);
																																									}
																																								}
																																							else
																																								{	/* Expand/iarith.scm 53 */
																																									goto
																																										BgL_tagzd2106zd2_71;
																																								}
																																						}
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd2175zd2_113))))
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_arg1191z00_133;
																																							obj_t
																																								BgL_arg1193z00_134;
																																							BgL_arg1191z00_133
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2173zd2_112));
																																							BgL_arg1193z00_134
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2175zd2_113));
																																							{
																																								obj_t
																																									BgL_yz00_3271;
																																								obj_t
																																									BgL_xz00_3270;
																																								BgL_xz00_3270
																																									=
																																									BgL_arg1191z00_133;
																																								BgL_yz00_3271
																																									=
																																									BgL_arg1193z00_134;
																																								BgL_yz00_69
																																									=
																																									BgL_yz00_3271;
																																								BgL_xz00_68
																																									=
																																									BgL_xz00_3270;
																																								goto
																																									BgL_tagzd2105zd2_70;
																																							}
																																						}
																																					else
																																						{	/* Expand/iarith.scm 53 */
																																							goto
																																								BgL_tagzd2106zd2_71;
																																						}
																																				}
																																		}
																																	}
																																}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_cdrzd2215zd2_138;
																															BgL_cdrzd2215zd2_138
																																=
																																CDR(((obj_t)
																																	BgL_xz00_5));
																															{	/* Expand/iarith.scm 53 */
																																obj_t
																																	BgL_cdrzd2217zd2_139;
																																BgL_cdrzd2217zd2_139
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd2215zd2_138));
																																{	/* Expand/iarith.scm 53 */
																																	bool_t
																																		BgL_test3136z00_3276;
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_tmpz00_3277;
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_pairz00_2126;
																																			{	/* Expand/iarith.scm 53 */
																																				obj_t
																																					BgL_pairz00_2125;
																																				BgL_pairz00_2125
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd2215zd2_138));
																																				BgL_pairz00_2126
																																					=
																																					CDR
																																					(BgL_pairz00_2125);
																																			}
																																			BgL_tmpz00_3277
																																				=
																																				CAR
																																				(BgL_pairz00_2126);
																																		}
																																		BgL_test3136z00_3276
																																			=
																																			SYMBOLP
																																			(BgL_tmpz00_3277);
																																	}
																																	if (BgL_test3136z00_3276)
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_cdrzd2225zd2_144;
																																			{	/* Expand/iarith.scm 53 */
																																				obj_t
																																					BgL_pairz00_2128;
																																				BgL_pairz00_2128
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd2217zd2_139));
																																				BgL_cdrzd2225zd2_144
																																					=
																																					CDR
																																					(BgL_pairz00_2128);
																																			}
																																			{	/* Expand/iarith.scm 53 */
																																				bool_t
																																					BgL_test3137z00_3286;
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_tmpz00_3287;
																																					BgL_tmpz00_3287
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2225zd2_144));
																																					BgL_test3137z00_3286
																																						=
																																						SYMBOLP
																																						(BgL_tmpz00_3287);
																																				}
																																				if (BgL_test3137z00_3286)
																																					{	/* Expand/iarith.scm 53 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd2225zd2_144))))
																																							{	/* Expand/iarith.scm 53 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd2217zd2_139))))
																																									{	/* Expand/iarith.scm 53 */
																																										return
																																											BFALSE;
																																									}
																																								else
																																									{	/* Expand/iarith.scm 53 */
																																										goto
																																											BgL_tagzd2106zd2_71;
																																									}
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								bool_t
																																									BgL_test3140z00_3299;
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_tmpz00_3300;
																																									BgL_tmpz00_3300
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2215zd2_138));
																																									BgL_test3140z00_3299
																																										=
																																										CHARP
																																										(BgL_tmpz00_3300);
																																								}
																																								if (BgL_test3140z00_3299)
																																									{	/* Expand/iarith.scm 53 */
																																										bool_t
																																											BgL_test3141z00_3304;
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_tmpz00_3305;
																																											BgL_tmpz00_3305
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd2217zd2_139));
																																											BgL_test3141z00_3304
																																												=
																																												CHARP
																																												(BgL_tmpz00_3305);
																																										}
																																										if (BgL_test3141z00_3304)
																																											{	/* Expand/iarith.scm 53 */
																																												if (NULLP(CDR(((obj_t) BgL_cdrzd2217zd2_139))))
																																													{	/* Expand/iarith.scm 53 */
																																														return
																																															BFALSE;
																																													}
																																												else
																																													{	/* Expand/iarith.scm 53 */
																																														goto
																																															BgL_tagzd2106zd2_71;
																																													}
																																											}
																																										else
																																											{	/* Expand/iarith.scm 53 */
																																												obj_t
																																													BgL_cdrzd2251zd2_161;
																																												BgL_cdrzd2251zd2_161
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_xz00_5));
																																												{	/* Expand/iarith.scm 53 */
																																													obj_t
																																														BgL_cdrzd2255zd2_162;
																																													BgL_cdrzd2255zd2_162
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd2251zd2_161));
																																													if (NULLP(CDR(((obj_t) BgL_cdrzd2255zd2_162))))
																																														{	/* Expand/iarith.scm 53 */
																																															obj_t
																																																BgL_arg1223z00_165;
																																															obj_t
																																																BgL_arg1225z00_166;
																																															BgL_arg1223z00_165
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_cdrzd2251zd2_161));
																																															BgL_arg1225z00_166
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_cdrzd2255zd2_162));
																																															{
																																																obj_t
																																																	BgL_yz00_3326;
																																																obj_t
																																																	BgL_xz00_3325;
																																																BgL_xz00_3325
																																																	=
																																																	BgL_arg1223z00_165;
																																																BgL_yz00_3326
																																																	=
																																																	BgL_arg1225z00_166;
																																																BgL_yz00_69
																																																	=
																																																	BgL_yz00_3326;
																																																BgL_xz00_68
																																																	=
																																																	BgL_xz00_3325;
																																																goto
																																																	BgL_tagzd2105zd2_70;
																																															}
																																														}
																																													else
																																														{	/* Expand/iarith.scm 53 */
																																															goto
																																																BgL_tagzd2106zd2_71;
																																														}
																																												}
																																											}
																																									}
																																								else
																																									{	/* Expand/iarith.scm 53 */
																																										obj_t
																																											BgL_cdrzd2266zd2_169;
																																										BgL_cdrzd2266zd2_169
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_xz00_5));
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_cdrzd2270zd2_170;
																																											BgL_cdrzd2270zd2_170
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd2266zd2_169));
																																											if (NULLP(CDR(((obj_t) BgL_cdrzd2270zd2_170))))
																																												{	/* Expand/iarith.scm 53 */
																																													obj_t
																																														BgL_arg1230z00_173;
																																													obj_t
																																														BgL_arg1231z00_174;
																																													BgL_arg1230z00_173
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd2266zd2_169));
																																													BgL_arg1231z00_174
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd2270zd2_170));
																																													{
																																														obj_t
																																															BgL_yz00_3340;
																																														obj_t
																																															BgL_xz00_3339;
																																														BgL_xz00_3339
																																															=
																																															BgL_arg1230z00_173;
																																														BgL_yz00_3340
																																															=
																																															BgL_arg1231z00_174;
																																														BgL_yz00_69
																																															=
																																															BgL_yz00_3340;
																																														BgL_xz00_68
																																															=
																																															BgL_xz00_3339;
																																														goto
																																															BgL_tagzd2105zd2_70;
																																													}
																																												}
																																											else
																																												{	/* Expand/iarith.scm 53 */
																																													goto
																																														BgL_tagzd2106zd2_71;
																																												}
																																										}
																																									}
																																							}
																																					}
																																				else
																																					{	/* Expand/iarith.scm 53 */
																																						bool_t
																																							BgL_test3145z00_3341;
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_tmpz00_3342;
																																							BgL_tmpz00_3342
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2215zd2_138));
																																							BgL_test3145z00_3341
																																								=
																																								CHARP
																																								(BgL_tmpz00_3342);
																																						}
																																						if (BgL_test3145z00_3341)
																																							{	/* Expand/iarith.scm 53 */
																																								bool_t
																																									BgL_test3146z00_3346;
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_tmpz00_3347;
																																									BgL_tmpz00_3347
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2217zd2_139));
																																									BgL_test3146z00_3346
																																										=
																																										CHARP
																																										(BgL_tmpz00_3347);
																																								}
																																								if (BgL_test3146z00_3346)
																																									{	/* Expand/iarith.scm 53 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd2217zd2_139))))
																																											{	/* Expand/iarith.scm 53 */
																																												return
																																													BFALSE;
																																											}
																																										else
																																											{	/* Expand/iarith.scm 53 */
																																												goto
																																													BgL_tagzd2106zd2_71;
																																											}
																																									}
																																								else
																																									{	/* Expand/iarith.scm 53 */
																																										obj_t
																																											BgL_cdrzd2293zd2_187;
																																										BgL_cdrzd2293zd2_187
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_xz00_5));
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_cdrzd2297zd2_188;
																																											BgL_cdrzd2297zd2_188
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd2293zd2_187));
																																											if (NULLP(CDR(((obj_t) BgL_cdrzd2297zd2_188))))
																																												{	/* Expand/iarith.scm 53 */
																																													obj_t
																																														BgL_arg1252z00_191;
																																													obj_t
																																														BgL_arg1268z00_192;
																																													BgL_arg1252z00_191
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd2293zd2_187));
																																													BgL_arg1268z00_192
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd2297zd2_188));
																																													{
																																														obj_t
																																															BgL_yz00_3368;
																																														obj_t
																																															BgL_xz00_3367;
																																														BgL_xz00_3367
																																															=
																																															BgL_arg1252z00_191;
																																														BgL_yz00_3368
																																															=
																																															BgL_arg1268z00_192;
																																														BgL_yz00_69
																																															=
																																															BgL_yz00_3368;
																																														BgL_xz00_68
																																															=
																																															BgL_xz00_3367;
																																														goto
																																															BgL_tagzd2105zd2_70;
																																													}
																																												}
																																											else
																																												{	/* Expand/iarith.scm 53 */
																																													goto
																																														BgL_tagzd2106zd2_71;
																																												}
																																										}
																																									}
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								obj_t
																																									BgL_cdrzd2308zd2_195;
																																								BgL_cdrzd2308zd2_195
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_xz00_5));
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_cdrzd2312zd2_196;
																																									BgL_cdrzd2312zd2_196
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd2308zd2_195));
																																									if (NULLP(CDR(((obj_t) BgL_cdrzd2312zd2_196))))
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_arg1306z00_199;
																																											obj_t
																																												BgL_arg1307z00_200;
																																											BgL_arg1306z00_199
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd2308zd2_195));
																																											BgL_arg1307z00_200
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd2312zd2_196));
																																											{
																																												obj_t
																																													BgL_yz00_3382;
																																												obj_t
																																													BgL_xz00_3381;
																																												BgL_xz00_3381
																																													=
																																													BgL_arg1306z00_199;
																																												BgL_yz00_3382
																																													=
																																													BgL_arg1307z00_200;
																																												BgL_yz00_69
																																													=
																																													BgL_yz00_3382;
																																												BgL_xz00_68
																																													=
																																													BgL_xz00_3381;
																																												goto
																																													BgL_tagzd2105zd2_70;
																																											}
																																										}
																																									else
																																										{	/* Expand/iarith.scm 53 */
																																											goto
																																												BgL_tagzd2106zd2_71;
																																										}
																																								}
																																							}
																																					}
																																			}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			bool_t
																																				BgL_test3150z00_3383;
																																			{	/* Expand/iarith.scm 53 */
																																				obj_t
																																					BgL_tmpz00_3384;
																																				BgL_tmpz00_3384
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd2215zd2_138));
																																				BgL_test3150z00_3383
																																					=
																																					CHARP
																																					(BgL_tmpz00_3384);
																																			}
																																			if (BgL_test3150z00_3383)
																																				{	/* Expand/iarith.scm 53 */
																																					bool_t
																																						BgL_test3151z00_3388;
																																					{	/* Expand/iarith.scm 53 */
																																						obj_t
																																							BgL_tmpz00_3389;
																																						BgL_tmpz00_3389
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd2217zd2_139));
																																						BgL_test3151z00_3388
																																							=
																																							CHARP
																																							(BgL_tmpz00_3389);
																																					}
																																					if (BgL_test3151z00_3388)
																																						{	/* Expand/iarith.scm 53 */
																																							if (NULLP(CDR(((obj_t) BgL_cdrzd2217zd2_139))))
																																								{	/* Expand/iarith.scm 53 */
																																									return
																																										BFALSE;
																																								}
																																							else
																																								{	/* Expand/iarith.scm 53 */
																																									goto
																																										BgL_tagzd2106zd2_71;
																																								}
																																						}
																																					else
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_cdrzd2335zd2_214;
																																							BgL_cdrzd2335zd2_214
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_xz00_5));
																																							{	/* Expand/iarith.scm 53 */
																																								obj_t
																																									BgL_cdrzd2339zd2_215;
																																								BgL_cdrzd2339zd2_215
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_cdrzd2335zd2_214));
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd2339zd2_215))))
																																									{	/* Expand/iarith.scm 53 */
																																										obj_t
																																											BgL_arg1323z00_218;
																																										obj_t
																																											BgL_arg1325z00_219;
																																										BgL_arg1323z00_218
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd2335zd2_214));
																																										BgL_arg1325z00_219
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd2339zd2_215));
																																										{
																																											obj_t
																																												BgL_yz00_3410;
																																											obj_t
																																												BgL_xz00_3409;
																																											BgL_xz00_3409
																																												=
																																												BgL_arg1323z00_218;
																																											BgL_yz00_3410
																																												=
																																												BgL_arg1325z00_219;
																																											BgL_yz00_69
																																												=
																																												BgL_yz00_3410;
																																											BgL_xz00_68
																																												=
																																												BgL_xz00_3409;
																																											goto
																																												BgL_tagzd2105zd2_70;
																																										}
																																									}
																																								else
																																									{	/* Expand/iarith.scm 53 */
																																										goto
																																											BgL_tagzd2106zd2_71;
																																									}
																																							}
																																						}
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_cdrzd2350zd2_222;
																																					BgL_cdrzd2350zd2_222
																																						=
																																						CDR(
																																						((obj_t) BgL_xz00_5));
																																					{	/* Expand/iarith.scm 53 */
																																						obj_t
																																							BgL_cdrzd2354zd2_223;
																																						BgL_cdrzd2354zd2_223
																																							=
																																							CDR
																																							(((obj_t) BgL_cdrzd2350zd2_222));
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd2354zd2_223))))
																																							{	/* Expand/iarith.scm 53 */
																																								obj_t
																																									BgL_arg1331z00_226;
																																								obj_t
																																									BgL_arg1332z00_227;
																																								BgL_arg1331z00_226
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd2350zd2_222));
																																								BgL_arg1332z00_227
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd2354zd2_223));
																																								{
																																									obj_t
																																										BgL_yz00_3424;
																																									obj_t
																																										BgL_xz00_3423;
																																									BgL_xz00_3423
																																										=
																																										BgL_arg1331z00_226;
																																									BgL_yz00_3424
																																										=
																																										BgL_arg1332z00_227;
																																									BgL_yz00_69
																																										=
																																										BgL_yz00_3424;
																																									BgL_xz00_68
																																										=
																																										BgL_xz00_3423;
																																									goto
																																										BgL_tagzd2105zd2_70;
																																								}
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								goto
																																									BgL_tagzd2106zd2_71;
																																							}
																																					}
																																				}
																																		}
																																}
																															}
																														}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_cdrzd2365zd2_234;
																													BgL_cdrzd2365zd2_234 =
																														CDR(((obj_t)
																															BgL_xz00_5));
																													{	/* Expand/iarith.scm 53 */
																														obj_t
																															BgL_cdrzd2367zd2_235;
																														BgL_cdrzd2367zd2_235
																															=
																															CDR(((obj_t)
																																BgL_cdrzd2365zd2_234));
																														{	/* Expand/iarith.scm 53 */
																															bool_t
																																BgL_test3155z00_3429;
																															{	/* Expand/iarith.scm 53 */
																																obj_t
																																	BgL_tmpz00_3430;
																																BgL_tmpz00_3430
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd2365zd2_234));
																																BgL_test3155z00_3429
																																	=
																																	CHARP
																																	(BgL_tmpz00_3430);
																															}
																															if (BgL_test3155z00_3429)
																																{	/* Expand/iarith.scm 53 */
																																	bool_t
																																		BgL_test3156z00_3434;
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_tmpz00_3435;
																																		BgL_tmpz00_3435
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd2367zd2_235));
																																		BgL_test3156z00_3434
																																			=
																																			CHARP
																																			(BgL_tmpz00_3435);
																																	}
																																	if (BgL_test3156z00_3434)
																																		{	/* Expand/iarith.scm 53 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd2367zd2_235))))
																																				{	/* Expand/iarith.scm 53 */
																																					return
																																						BFALSE;
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					goto
																																						BgL_tagzd2106zd2_71;
																																				}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd2367zd2_235))))
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_arg1367z00_247;
																																					obj_t
																																						BgL_arg1370z00_248;
																																					BgL_arg1367z00_247
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2365zd2_234));
																																					BgL_arg1370z00_248
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2367zd2_235));
																																					{
																																						obj_t
																																							BgL_yz00_3452;
																																						obj_t
																																							BgL_xz00_3451;
																																						BgL_xz00_3451
																																							=
																																							BgL_arg1367z00_247;
																																						BgL_yz00_3452
																																							=
																																							BgL_arg1370z00_248;
																																						BgL_yz00_69
																																							=
																																							BgL_yz00_3452;
																																						BgL_xz00_68
																																							=
																																							BgL_xz00_3451;
																																						goto
																																							BgL_tagzd2105zd2_70;
																																					}
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					goto
																																						BgL_tagzd2106zd2_71;
																																				}
																																		}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd2367zd2_235))))
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_arg1408z00_255;
																																			obj_t
																																				BgL_arg1410z00_256;
																																			BgL_arg1408z00_255
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2365zd2_234));
																																			BgL_arg1410z00_256
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2367zd2_235));
																																			{
																																				obj_t
																																					BgL_yz00_3462;
																																				obj_t
																																					BgL_xz00_3461;
																																				BgL_xz00_3461
																																					=
																																					BgL_arg1408z00_255;
																																				BgL_yz00_3462
																																					=
																																					BgL_arg1410z00_256;
																																				BgL_yz00_69
																																					=
																																					BgL_yz00_3462;
																																				BgL_xz00_68
																																					=
																																					BgL_xz00_3461;
																																				goto
																																					BgL_tagzd2105zd2_70;
																																			}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			goto
																																				BgL_tagzd2106zd2_71;
																																		}
																																}
																														}
																													}
																												}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											obj_t
																												BgL_cdrzd2409zd2_259;
																											BgL_cdrzd2409zd2_259 =
																												CDR(((obj_t)
																													BgL_xz00_5));
																											{	/* Expand/iarith.scm 53 */
																												obj_t
																													BgL_cdrzd2411zd2_260;
																												BgL_cdrzd2411zd2_260 =
																													CDR(((obj_t)
																														BgL_cdrzd2409zd2_259));
																												{	/* Expand/iarith.scm 53 */
																													bool_t
																														BgL_test3160z00_3467;
																													{	/* Expand/iarith.scm 53 */
																														obj_t
																															BgL_tmpz00_3468;
																														BgL_tmpz00_3468 =
																															CAR(((obj_t)
																																BgL_cdrzd2409zd2_259));
																														BgL_test3160z00_3467
																															=
																															CHARP
																															(BgL_tmpz00_3468);
																													}
																													if (BgL_test3160z00_3467)
																														{	/* Expand/iarith.scm 53 */
																															bool_t
																																BgL_test3161z00_3472;
																															{	/* Expand/iarith.scm 53 */
																																obj_t
																																	BgL_tmpz00_3473;
																																BgL_tmpz00_3473
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd2411zd2_260));
																																BgL_test3161z00_3472
																																	=
																																	CHARP
																																	(BgL_tmpz00_3473);
																															}
																															if (BgL_test3161z00_3472)
																																{	/* Expand/iarith.scm 53 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd2411zd2_260))))
																																		{	/* Expand/iarith.scm 53 */
																																			return
																																				BFALSE;
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			goto
																																				BgL_tagzd2106zd2_71;
																																		}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd2411zd2_260))))
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_arg1472z00_272;
																																			obj_t
																																				BgL_arg1473z00_273;
																																			BgL_arg1472z00_272
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2409zd2_259));
																																			BgL_arg1473z00_273
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2411zd2_260));
																																			{
																																				obj_t
																																					BgL_yz00_3490;
																																				obj_t
																																					BgL_xz00_3489;
																																				BgL_xz00_3489
																																					=
																																					BgL_arg1472z00_272;
																																				BgL_yz00_3490
																																					=
																																					BgL_arg1473z00_273;
																																				BgL_yz00_69
																																					=
																																					BgL_yz00_3490;
																																				BgL_xz00_68
																																					=
																																					BgL_xz00_3489;
																																				goto
																																					BgL_tagzd2105zd2_70;
																																			}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			goto
																																				BgL_tagzd2106zd2_71;
																																		}
																																}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd2411zd2_260))))
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_arg1509z00_280;
																																	obj_t
																																		BgL_arg1513z00_281;
																																	BgL_arg1509z00_280
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2409zd2_259));
																																	BgL_arg1513z00_281
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2411zd2_260));
																																	{
																																		obj_t
																																			BgL_yz00_3500;
																																		obj_t
																																			BgL_xz00_3499;
																																		BgL_xz00_3499
																																			=
																																			BgL_arg1509z00_280;
																																		BgL_yz00_3500
																																			=
																																			BgL_arg1513z00_281;
																																		BgL_yz00_69
																																			=
																																			BgL_yz00_3500;
																																		BgL_xz00_68
																																			=
																																			BgL_xz00_3499;
																																		goto
																																			BgL_tagzd2105zd2_70;
																																	}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	goto
																																		BgL_tagzd2106zd2_71;
																																}
																														}
																												}
																											}
																										}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_cdrzd2453zd2_285;

																									BgL_cdrzd2453zd2_285 =
																										CDR(((obj_t) BgL_xz00_5));
																									{	/* Expand/iarith.scm 53 */
																										obj_t BgL_cdrzd2455zd2_286;

																										BgL_cdrzd2455zd2_286 =
																											CDR(
																											((obj_t)
																												BgL_cdrzd2453zd2_285));
																										{	/* Expand/iarith.scm 53 */
																											bool_t
																												BgL_test3165z00_3505;
																											{	/* Expand/iarith.scm 53 */
																												obj_t BgL_tmpz00_3506;

																												BgL_tmpz00_3506 =
																													CAR(
																													((obj_t)
																														BgL_cdrzd2453zd2_285));
																												BgL_test3165z00_3505 =
																													CHARP
																													(BgL_tmpz00_3506);
																											}
																											if (BgL_test3165z00_3505)
																												{	/* Expand/iarith.scm 53 */
																													bool_t
																														BgL_test3166z00_3510;
																													{	/* Expand/iarith.scm 53 */
																														obj_t
																															BgL_tmpz00_3511;
																														BgL_tmpz00_3511 =
																															CAR(((obj_t)
																																BgL_cdrzd2455zd2_286));
																														BgL_test3166z00_3510
																															=
																															CHARP
																															(BgL_tmpz00_3511);
																													}
																													if (BgL_test3166z00_3510)
																														{	/* Expand/iarith.scm 53 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd2455zd2_286))))
																																{	/* Expand/iarith.scm 53 */
																																	return BFALSE;
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	goto
																																		BgL_tagzd2106zd2_71;
																																}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd2455zd2_286))))
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_arg1564z00_298;
																																	obj_t
																																		BgL_arg1565z00_299;
																																	BgL_arg1564z00_298
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2453zd2_285));
																																	BgL_arg1565z00_299
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2455zd2_286));
																																	{
																																		obj_t
																																			BgL_yz00_3528;
																																		obj_t
																																			BgL_xz00_3527;
																																		BgL_xz00_3527
																																			=
																																			BgL_arg1564z00_298;
																																		BgL_yz00_3528
																																			=
																																			BgL_arg1565z00_299;
																																		BgL_yz00_69
																																			=
																																			BgL_yz00_3528;
																																		BgL_xz00_68
																																			=
																																			BgL_xz00_3527;
																																		goto
																																			BgL_tagzd2105zd2_70;
																																	}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	goto
																																		BgL_tagzd2106zd2_71;
																																}
																														}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd2455zd2_286))))
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_arg1584z00_306;
																															obj_t
																																BgL_arg1585z00_307;
																															BgL_arg1584z00_306
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2453zd2_285));
																															BgL_arg1585z00_307
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2455zd2_286));
																															{
																																obj_t
																																	BgL_yz00_3538;
																																obj_t
																																	BgL_xz00_3537;
																																BgL_xz00_3537 =
																																	BgL_arg1584z00_306;
																																BgL_yz00_3538 =
																																	BgL_arg1585z00_307;
																																BgL_yz00_69 =
																																	BgL_yz00_3538;
																																BgL_xz00_68 =
																																	BgL_xz00_3537;
																																goto
																																	BgL_tagzd2105zd2_70;
																															}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															goto
																																BgL_tagzd2106zd2_71;
																														}
																												}
																										}
																									}
																								}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							obj_t BgL_cdrzd2497zd2_310;

																							BgL_cdrzd2497zd2_310 =
																								CDR(((obj_t) BgL_xz00_5));
																							{	/* Expand/iarith.scm 53 */
																								obj_t BgL_cdrzd2499zd2_311;

																								BgL_cdrzd2499zd2_311 =
																									CDR(
																									((obj_t)
																										BgL_cdrzd2497zd2_310));
																								{	/* Expand/iarith.scm 53 */
																									bool_t BgL_test3170z00_3543;

																									{	/* Expand/iarith.scm 53 */
																										obj_t BgL_tmpz00_3544;

																										BgL_tmpz00_3544 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd2497zd2_310));
																										BgL_test3170z00_3543 =
																											CHARP(BgL_tmpz00_3544);
																									}
																									if (BgL_test3170z00_3543)
																										{	/* Expand/iarith.scm 53 */
																											bool_t
																												BgL_test3171z00_3548;
																											{	/* Expand/iarith.scm 53 */
																												obj_t BgL_tmpz00_3549;

																												BgL_tmpz00_3549 =
																													CAR(
																													((obj_t)
																														BgL_cdrzd2499zd2_311));
																												BgL_test3171z00_3548 =
																													CHARP
																													(BgL_tmpz00_3549);
																											}
																											if (BgL_test3171z00_3548)
																												{	/* Expand/iarith.scm 53 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd2499zd2_311))))
																														{	/* Expand/iarith.scm 53 */
																															return BFALSE;
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															goto
																																BgL_tagzd2106zd2_71;
																														}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd2499zd2_311))))
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_arg1609z00_323;
																															obj_t
																																BgL_arg1611z00_324;
																															BgL_arg1609z00_323
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2497zd2_310));
																															BgL_arg1611z00_324
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2499zd2_311));
																															{
																																obj_t
																																	BgL_yz00_3566;
																																obj_t
																																	BgL_xz00_3565;
																																BgL_xz00_3565 =
																																	BgL_arg1609z00_323;
																																BgL_yz00_3566 =
																																	BgL_arg1611z00_324;
																																BgL_yz00_69 =
																																	BgL_yz00_3566;
																																BgL_xz00_68 =
																																	BgL_xz00_3565;
																																goto
																																	BgL_tagzd2105zd2_70;
																															}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															goto
																																BgL_tagzd2106zd2_71;
																														}
																												}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd2499zd2_311))))
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_arg1625z00_331;
																													obj_t
																														BgL_arg1626z00_332;
																													BgL_arg1625z00_331 =
																														CAR(((obj_t)
																															BgL_cdrzd2497zd2_310));
																													BgL_arg1626z00_332 =
																														CAR(((obj_t)
																															BgL_cdrzd2499zd2_311));
																													{
																														obj_t BgL_yz00_3576;
																														obj_t BgL_xz00_3575;

																														BgL_xz00_3575 =
																															BgL_arg1625z00_331;
																														BgL_yz00_3576 =
																															BgL_arg1626z00_332;
																														BgL_yz00_69 =
																															BgL_yz00_3576;
																														BgL_xz00_68 =
																															BgL_xz00_3575;
																														goto
																															BgL_tagzd2105zd2_70;
																													}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													goto
																														BgL_tagzd2106zd2_71;
																												}
																										}
																								}
																							}
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 53 */
																					obj_t BgL_cdrzd2541zd2_336;

																					BgL_cdrzd2541zd2_336 =
																						CDR(((obj_t) BgL_xz00_5));
																					{	/* Expand/iarith.scm 53 */
																						obj_t BgL_cdrzd2543zd2_337;

																						BgL_cdrzd2543zd2_337 =
																							CDR(
																							((obj_t) BgL_cdrzd2541zd2_336));
																						{	/* Expand/iarith.scm 53 */
																							bool_t BgL_test3175z00_3581;

																							{	/* Expand/iarith.scm 53 */
																								obj_t BgL_tmpz00_3582;

																								BgL_tmpz00_3582 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2541zd2_336));
																								BgL_test3175z00_3581 =
																									CHARP(BgL_tmpz00_3582);
																							}
																							if (BgL_test3175z00_3581)
																								{	/* Expand/iarith.scm 53 */
																									bool_t BgL_test3176z00_3586;

																									{	/* Expand/iarith.scm 53 */
																										obj_t BgL_tmpz00_3587;

																										BgL_tmpz00_3587 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd2543zd2_337));
																										BgL_test3176z00_3586 =
																											CHARP(BgL_tmpz00_3587);
																									}
																									if (BgL_test3176z00_3586)
																										{	/* Expand/iarith.scm 53 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd2543zd2_337))))
																												{	/* Expand/iarith.scm 53 */
																													return BFALSE;
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													goto
																														BgL_tagzd2106zd2_71;
																												}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd2543zd2_337))))
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_arg1675z00_349;
																													obj_t
																														BgL_arg1678z00_350;
																													BgL_arg1675z00_349 =
																														CAR(((obj_t)
																															BgL_cdrzd2541zd2_336));
																													BgL_arg1678z00_350 =
																														CAR(((obj_t)
																															BgL_cdrzd2543zd2_337));
																													{
																														obj_t BgL_yz00_3604;
																														obj_t BgL_xz00_3603;

																														BgL_xz00_3603 =
																															BgL_arg1675z00_349;
																														BgL_yz00_3604 =
																															BgL_arg1678z00_350;
																														BgL_yz00_69 =
																															BgL_yz00_3604;
																														BgL_xz00_68 =
																															BgL_xz00_3603;
																														goto
																															BgL_tagzd2105zd2_70;
																													}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													goto
																														BgL_tagzd2106zd2_71;
																												}
																										}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd2543zd2_337))))
																										{	/* Expand/iarith.scm 53 */
																											obj_t BgL_arg1691z00_357;
																											obj_t BgL_arg1692z00_358;

																											BgL_arg1691z00_357 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd2541zd2_336));
																											BgL_arg1692z00_358 =
																												CAR(((obj_t)
																													BgL_cdrzd2543zd2_337));
																											{
																												obj_t BgL_yz00_3614;
																												obj_t BgL_xz00_3613;

																												BgL_xz00_3613 =
																													BgL_arg1691z00_357;
																												BgL_yz00_3614 =
																													BgL_arg1692z00_358;
																												BgL_yz00_69 =
																													BgL_yz00_3614;
																												BgL_xz00_68 =
																													BgL_xz00_3613;
																												goto
																													BgL_tagzd2105zd2_70;
																											}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											goto BgL_tagzd2106zd2_71;
																										}
																								}
																						}
																					}
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 53 */
																			obj_t BgL_cdrzd2585zd2_361;

																			BgL_cdrzd2585zd2_361 =
																				CDR(((obj_t) BgL_xz00_5));
																			{	/* Expand/iarith.scm 53 */
																				obj_t BgL_cdrzd2587zd2_362;

																				BgL_cdrzd2587zd2_362 =
																					CDR(((obj_t) BgL_cdrzd2585zd2_361));
																				{	/* Expand/iarith.scm 53 */
																					bool_t BgL_test3180z00_3619;

																					{	/* Expand/iarith.scm 53 */
																						obj_t BgL_tmpz00_3620;

																						BgL_tmpz00_3620 =
																							CAR(
																							((obj_t) BgL_cdrzd2585zd2_361));
																						BgL_test3180z00_3619 =
																							CHARP(BgL_tmpz00_3620);
																					}
																					if (BgL_test3180z00_3619)
																						{	/* Expand/iarith.scm 53 */
																							bool_t BgL_test3181z00_3624;

																							{	/* Expand/iarith.scm 53 */
																								obj_t BgL_tmpz00_3625;

																								BgL_tmpz00_3625 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2587zd2_362));
																								BgL_test3181z00_3624 =
																									CHARP(BgL_tmpz00_3625);
																							}
																							if (BgL_test3181z00_3624)
																								{	/* Expand/iarith.scm 53 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd2587zd2_362))))
																										{	/* Expand/iarith.scm 53 */
																											return BFALSE;
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											goto BgL_tagzd2106zd2_71;
																										}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd2587zd2_362))))
																										{	/* Expand/iarith.scm 53 */
																											obj_t BgL_arg1714z00_374;
																											obj_t BgL_arg1717z00_375;

																											BgL_arg1714z00_374 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd2585zd2_361));
																											BgL_arg1717z00_375 =
																												CAR(((obj_t)
																													BgL_cdrzd2587zd2_362));
																											{
																												obj_t BgL_yz00_3642;
																												obj_t BgL_xz00_3641;

																												BgL_xz00_3641 =
																													BgL_arg1714z00_374;
																												BgL_yz00_3642 =
																													BgL_arg1717z00_375;
																												BgL_yz00_69 =
																													BgL_yz00_3642;
																												BgL_xz00_68 =
																													BgL_xz00_3641;
																												goto
																													BgL_tagzd2105zd2_70;
																											}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											goto BgL_tagzd2106zd2_71;
																										}
																								}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd2587zd2_362))))
																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_arg1724z00_382;
																									obj_t BgL_arg1733z00_383;

																									BgL_arg1724z00_382 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd2585zd2_361));
																									BgL_arg1733z00_383 =
																										CAR(((obj_t)
																											BgL_cdrzd2587zd2_362));
																									{
																										obj_t BgL_yz00_3652;
																										obj_t BgL_xz00_3651;

																										BgL_xz00_3651 =
																											BgL_arg1724z00_382;
																										BgL_yz00_3652 =
																											BgL_arg1733z00_383;
																										BgL_yz00_69 = BgL_yz00_3652;
																										BgL_xz00_68 = BgL_xz00_3651;
																										goto BgL_tagzd2105zd2_70;
																									}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									goto BgL_tagzd2106zd2_71;
																								}
																						}
																				}
																			}
																		}
																}
															else
																{	/* Expand/iarith.scm 53 */
																	obj_t BgL_carzd2628zd2_388;
																	obj_t BgL_cdrzd2629zd2_389;

																	BgL_carzd2628zd2_388 =
																		CAR(((obj_t) BgL_cdrzd2112zd2_74));
																	BgL_cdrzd2629zd2_389 =
																		CDR(((obj_t) BgL_cdrzd2112zd2_74));
																	if (PAIRP(BgL_carzd2628zd2_388))
																		{	/* Expand/iarith.scm 53 */
																			obj_t BgL_cdrzd2631zd2_391;

																			BgL_cdrzd2631zd2_391 =
																				CDR(BgL_carzd2628zd2_388);
																			if (
																				(CAR(BgL_carzd2628zd2_388) ==
																					CNST_TABLE_REF(1)))
																				{	/* Expand/iarith.scm 53 */
																					if (PAIRP(BgL_cdrzd2631zd2_391))
																						{	/* Expand/iarith.scm 53 */
																							bool_t BgL_test3188z00_3666;

																							{	/* Expand/iarith.scm 53 */
																								obj_t BgL_tmpz00_3667;

																								BgL_tmpz00_3667 =
																									CAR(BgL_cdrzd2631zd2_391);
																								BgL_test3188z00_3666 =
																									SYMBOLP(BgL_tmpz00_3667);
																							}
																							if (BgL_test3188z00_3666)
																								{	/* Expand/iarith.scm 53 */
																									if (NULLP(CDR
																											(BgL_cdrzd2631zd2_391)))
																										{	/* Expand/iarith.scm 53 */
																											obj_t
																												BgL_carzd2634zd2_399;
																											BgL_carzd2634zd2_399 =
																												CAR(((obj_t)
																													BgL_cdrzd2629zd2_389));
																											if (PAIRP
																												(BgL_carzd2634zd2_399))
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_cdrzd2637zd2_401;
																													BgL_cdrzd2637zd2_401 =
																														CDR
																														(BgL_carzd2634zd2_399);
																													if ((CAR
																															(BgL_carzd2634zd2_399)
																															==
																															CNST_TABLE_REF
																															(1)))
																														{	/* Expand/iarith.scm 53 */
																															if (PAIRP
																																(BgL_cdrzd2637zd2_401))
																																{	/* Expand/iarith.scm 53 */
																																	bool_t
																																		BgL_test3193z00_3684;
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_tmpz00_3685;
																																		BgL_tmpz00_3685
																																			=
																																			CAR
																																			(BgL_cdrzd2637zd2_401);
																																		BgL_test3193z00_3684
																																			=
																																			SYMBOLP
																																			(BgL_tmpz00_3685);
																																	}
																																	if (BgL_test3193z00_3684)
																																		{	/* Expand/iarith.scm 53 */
																																			if (NULLP
																																				(CDR
																																					(BgL_cdrzd2637zd2_401)))
																																				{	/* Expand/iarith.scm 53 */
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd2629zd2_389))))
																																						{	/* Expand/iarith.scm 53 */
																																							return
																																								BFALSE;
																																						}
																																					else
																																						{	/* Expand/iarith.scm 53 */
																																							goto
																																								BgL_tagzd2106zd2_71;
																																						}
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_cdrzd2651zd2_413;
																																					BgL_cdrzd2651zd2_413
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd2112zd2_74));
																																					{	/* Expand/iarith.scm 53 */
																																						bool_t
																																							BgL_test3196z00_3697;
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_tmpz00_3698;
																																							BgL_tmpz00_3698
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2112zd2_74));
																																							BgL_test3196z00_3697
																																								=
																																								CHARP
																																								(BgL_tmpz00_3698);
																																						}
																																						if (BgL_test3196z00_3697)
																																							{	/* Expand/iarith.scm 53 */
																																								bool_t
																																									BgL_test3197z00_3702;
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_tmpz00_3703;
																																									BgL_tmpz00_3703
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2651zd2_413));
																																									BgL_test3197z00_3702
																																										=
																																										CHARP
																																										(BgL_tmpz00_3703);
																																								}
																																								if (BgL_test3197z00_3702)
																																									{	/* Expand/iarith.scm 53 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd2651zd2_413))))
																																											{	/* Expand/iarith.scm 53 */
																																												return
																																													BFALSE;
																																											}
																																										else
																																											{	/* Expand/iarith.scm 53 */
																																												goto
																																													BgL_tagzd2106zd2_71;
																																											}
																																									}
																																								else
																																									{	/* Expand/iarith.scm 53 */
																																										obj_t
																																											BgL_cdrzd2663zd2_421;
																																										BgL_cdrzd2663zd2_421
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_xz00_5));
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_cdrzd2667zd2_422;
																																											BgL_cdrzd2667zd2_422
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd2663zd2_421));
																																											if (NULLP(CDR(((obj_t) BgL_cdrzd2667zd2_422))))
																																												{	/* Expand/iarith.scm 53 */
																																													obj_t
																																														BgL_arg1805z00_425;
																																													obj_t
																																														BgL_arg1806z00_426;
																																													BgL_arg1805z00_425
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd2663zd2_421));
																																													BgL_arg1806z00_426
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd2667zd2_422));
																																													{
																																														obj_t
																																															BgL_yz00_3724;
																																														obj_t
																																															BgL_xz00_3723;
																																														BgL_xz00_3723
																																															=
																																															BgL_arg1805z00_425;
																																														BgL_yz00_3724
																																															=
																																															BgL_arg1806z00_426;
																																														BgL_yz00_69
																																															=
																																															BgL_yz00_3724;
																																														BgL_xz00_68
																																															=
																																															BgL_xz00_3723;
																																														goto
																																															BgL_tagzd2105zd2_70;
																																													}
																																												}
																																											else
																																												{	/* Expand/iarith.scm 53 */
																																													goto
																																														BgL_tagzd2106zd2_71;
																																												}
																																										}
																																									}
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								obj_t
																																									BgL_cdrzd2678zd2_429;
																																								BgL_cdrzd2678zd2_429
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_xz00_5));
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_cdrzd2682zd2_430;
																																									BgL_cdrzd2682zd2_430
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd2678zd2_429));
																																									if (NULLP(CDR(((obj_t) BgL_cdrzd2682zd2_430))))
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_arg1822z00_433;
																																											obj_t
																																												BgL_arg1823z00_434;
																																											BgL_arg1822z00_433
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd2678zd2_429));
																																											BgL_arg1823z00_434
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd2682zd2_430));
																																											{
																																												obj_t
																																													BgL_yz00_3738;
																																												obj_t
																																													BgL_xz00_3737;
																																												BgL_xz00_3737
																																													=
																																													BgL_arg1822z00_433;
																																												BgL_yz00_3738
																																													=
																																													BgL_arg1823z00_434;
																																												BgL_yz00_69
																																													=
																																													BgL_yz00_3738;
																																												BgL_xz00_68
																																													=
																																													BgL_xz00_3737;
																																												goto
																																													BgL_tagzd2105zd2_70;
																																											}
																																										}
																																									else
																																										{	/* Expand/iarith.scm 53 */
																																											goto
																																												BgL_tagzd2106zd2_71;
																																										}
																																								}
																																							}
																																					}
																																				}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_cdrzd2693zd2_439;
																																			BgL_cdrzd2693zd2_439
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd2112zd2_74));
																																			{	/* Expand/iarith.scm 53 */
																																				bool_t
																																					BgL_test3201z00_3741;
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_tmpz00_3742;
																																					BgL_tmpz00_3742
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2112zd2_74));
																																					BgL_test3201z00_3741
																																						=
																																						CHARP
																																						(BgL_tmpz00_3742);
																																				}
																																				if (BgL_test3201z00_3741)
																																					{	/* Expand/iarith.scm 53 */
																																						bool_t
																																							BgL_test3202z00_3746;
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_tmpz00_3747;
																																							BgL_tmpz00_3747
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2693zd2_439));
																																							BgL_test3202z00_3746
																																								=
																																								CHARP
																																								(BgL_tmpz00_3747);
																																						}
																																						if (BgL_test3202z00_3746)
																																							{	/* Expand/iarith.scm 53 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd2693zd2_439))))
																																									{	/* Expand/iarith.scm 53 */
																																										return
																																											BFALSE;
																																									}
																																								else
																																									{	/* Expand/iarith.scm 53 */
																																										goto
																																											BgL_tagzd2106zd2_71;
																																									}
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								obj_t
																																									BgL_cdrzd2705zd2_447;
																																								BgL_cdrzd2705zd2_447
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_xz00_5));
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_cdrzd2709zd2_448;
																																									BgL_cdrzd2709zd2_448
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd2705zd2_447));
																																									if (NULLP(CDR(((obj_t) BgL_cdrzd2709zd2_448))))
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_arg1844z00_451;
																																											obj_t
																																												BgL_arg1845z00_452;
																																											BgL_arg1844z00_451
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd2705zd2_447));
																																											BgL_arg1845z00_452
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd2709zd2_448));
																																											{
																																												obj_t
																																													BgL_yz00_3768;
																																												obj_t
																																													BgL_xz00_3767;
																																												BgL_xz00_3767
																																													=
																																													BgL_arg1844z00_451;
																																												BgL_yz00_3768
																																													=
																																													BgL_arg1845z00_452;
																																												BgL_yz00_69
																																													=
																																													BgL_yz00_3768;
																																												BgL_xz00_68
																																													=
																																													BgL_xz00_3767;
																																												goto
																																													BgL_tagzd2105zd2_70;
																																											}
																																										}
																																									else
																																										{	/* Expand/iarith.scm 53 */
																																											goto
																																												BgL_tagzd2106zd2_71;
																																										}
																																								}
																																							}
																																					}
																																				else
																																					{	/* Expand/iarith.scm 53 */
																																						obj_t
																																							BgL_cdrzd2720zd2_455;
																																						BgL_cdrzd2720zd2_455
																																							=
																																							CDR
																																							(((obj_t) BgL_xz00_5));
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_cdrzd2724zd2_456;
																																							BgL_cdrzd2724zd2_456
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd2720zd2_455));
																																							if (NULLP(CDR(((obj_t) BgL_cdrzd2724zd2_456))))
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_arg1850z00_459;
																																									obj_t
																																										BgL_arg1851z00_460;
																																									BgL_arg1850z00_459
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2720zd2_455));
																																									BgL_arg1851z00_460
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2724zd2_456));
																																									{
																																										obj_t
																																											BgL_yz00_3782;
																																										obj_t
																																											BgL_xz00_3781;
																																										BgL_xz00_3781
																																											=
																																											BgL_arg1850z00_459;
																																										BgL_yz00_3782
																																											=
																																											BgL_arg1851z00_460;
																																										BgL_yz00_69
																																											=
																																											BgL_yz00_3782;
																																										BgL_xz00_68
																																											=
																																											BgL_xz00_3781;
																																										goto
																																											BgL_tagzd2105zd2_70;
																																									}
																																								}
																																							else
																																								{	/* Expand/iarith.scm 53 */
																																									goto
																																										BgL_tagzd2106zd2_71;
																																								}
																																						}
																																					}
																																			}
																																		}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_cdrzd2735zd2_465;
																																	BgL_cdrzd2735zd2_465
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd2112zd2_74));
																																	{	/* Expand/iarith.scm 53 */
																																		bool_t
																																			BgL_test3206z00_3785;
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_tmpz00_3786;
																																			BgL_tmpz00_3786
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2112zd2_74));
																																			BgL_test3206z00_3785
																																				=
																																				CHARP
																																				(BgL_tmpz00_3786);
																																		}
																																		if (BgL_test3206z00_3785)
																																			{	/* Expand/iarith.scm 53 */
																																				bool_t
																																					BgL_test3207z00_3790;
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_tmpz00_3791;
																																					BgL_tmpz00_3791
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2735zd2_465));
																																					BgL_test3207z00_3790
																																						=
																																						CHARP
																																						(BgL_tmpz00_3791);
																																				}
																																				if (BgL_test3207z00_3790)
																																					{	/* Expand/iarith.scm 53 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd2735zd2_465))))
																																							{	/* Expand/iarith.scm 53 */
																																								return
																																									BFALSE;
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								goto
																																									BgL_tagzd2106zd2_71;
																																							}
																																					}
																																				else
																																					{	/* Expand/iarith.scm 53 */
																																						obj_t
																																							BgL_cdrzd2747zd2_473;
																																						BgL_cdrzd2747zd2_473
																																							=
																																							CDR
																																							(((obj_t) BgL_xz00_5));
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_cdrzd2751zd2_474;
																																							BgL_cdrzd2751zd2_474
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd2747zd2_473));
																																							if (NULLP(CDR(((obj_t) BgL_cdrzd2751zd2_474))))
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_arg1866z00_477;
																																									obj_t
																																										BgL_arg1868z00_478;
																																									BgL_arg1866z00_477
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2747zd2_473));
																																									BgL_arg1868z00_478
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2751zd2_474));
																																									{
																																										obj_t
																																											BgL_yz00_3812;
																																										obj_t
																																											BgL_xz00_3811;
																																										BgL_xz00_3811
																																											=
																																											BgL_arg1866z00_477;
																																										BgL_yz00_3812
																																											=
																																											BgL_arg1868z00_478;
																																										BgL_yz00_69
																																											=
																																											BgL_yz00_3812;
																																										BgL_xz00_68
																																											=
																																											BgL_xz00_3811;
																																										goto
																																											BgL_tagzd2105zd2_70;
																																									}
																																								}
																																							else
																																								{	/* Expand/iarith.scm 53 */
																																									goto
																																										BgL_tagzd2106zd2_71;
																																								}
																																						}
																																					}
																																			}
																																		else
																																			{	/* Expand/iarith.scm 53 */
																																				obj_t
																																					BgL_cdrzd2762zd2_481;
																																				BgL_cdrzd2762zd2_481
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_xz00_5));
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_cdrzd2766zd2_482;
																																					BgL_cdrzd2766zd2_482
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd2762zd2_481));
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd2766zd2_482))))
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_arg1874z00_485;
																																							obj_t
																																								BgL_arg1875z00_486;
																																							BgL_arg1874z00_485
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2762zd2_481));
																																							BgL_arg1875z00_486
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2766zd2_482));
																																							{
																																								obj_t
																																									BgL_yz00_3826;
																																								obj_t
																																									BgL_xz00_3825;
																																								BgL_xz00_3825
																																									=
																																									BgL_arg1874z00_485;
																																								BgL_yz00_3826
																																									=
																																									BgL_arg1875z00_486;
																																								BgL_yz00_69
																																									=
																																									BgL_yz00_3826;
																																								BgL_xz00_68
																																									=
																																									BgL_xz00_3825;
																																								goto
																																									BgL_tagzd2105zd2_70;
																																							}
																																						}
																																					else
																																						{	/* Expand/iarith.scm 53 */
																																							goto
																																								BgL_tagzd2106zd2_71;
																																						}
																																				}
																																			}
																																	}
																																}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_cdrzd2777zd2_490;
																															BgL_cdrzd2777zd2_490
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd2112zd2_74));
																															{	/* Expand/iarith.scm 53 */
																																bool_t
																																	BgL_test3211z00_3829;
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_tmpz00_3830;
																																	BgL_tmpz00_3830
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2112zd2_74));
																																	BgL_test3211z00_3829
																																		=
																																		CHARP
																																		(BgL_tmpz00_3830);
																																}
																																if (BgL_test3211z00_3829)
																																	{	/* Expand/iarith.scm 53 */
																																		bool_t
																																			BgL_test3212z00_3834;
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_tmpz00_3835;
																																			BgL_tmpz00_3835
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2777zd2_490));
																																			BgL_test3212z00_3834
																																				=
																																				CHARP
																																				(BgL_tmpz00_3835);
																																		}
																																		if (BgL_test3212z00_3834)
																																			{	/* Expand/iarith.scm 53 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd2777zd2_490))))
																																					{	/* Expand/iarith.scm 53 */
																																						return
																																							BFALSE;
																																					}
																																				else
																																					{	/* Expand/iarith.scm 53 */
																																						goto
																																							BgL_tagzd2106zd2_71;
																																					}
																																			}
																																		else
																																			{	/* Expand/iarith.scm 53 */
																																				obj_t
																																					BgL_cdrzd2789zd2_498;
																																				BgL_cdrzd2789zd2_498
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_xz00_5));
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_cdrzd2793zd2_499;
																																					BgL_cdrzd2793zd2_499
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd2789zd2_498));
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd2793zd2_499))))
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_arg1887z00_502;
																																							obj_t
																																								BgL_arg1888z00_503;
																																							BgL_arg1887z00_502
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2789zd2_498));
																																							BgL_arg1888z00_503
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2793zd2_499));
																																							{
																																								obj_t
																																									BgL_yz00_3856;
																																								obj_t
																																									BgL_xz00_3855;
																																								BgL_xz00_3855
																																									=
																																									BgL_arg1887z00_502;
																																								BgL_yz00_3856
																																									=
																																									BgL_arg1888z00_503;
																																								BgL_yz00_69
																																									=
																																									BgL_yz00_3856;
																																								BgL_xz00_68
																																									=
																																									BgL_xz00_3855;
																																								goto
																																									BgL_tagzd2105zd2_70;
																																							}
																																						}
																																					else
																																						{	/* Expand/iarith.scm 53 */
																																							goto
																																								BgL_tagzd2106zd2_71;
																																						}
																																				}
																																			}
																																	}
																																else
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_cdrzd2804zd2_506;
																																		BgL_cdrzd2804zd2_506
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_5));
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_cdrzd2808zd2_507;
																																			BgL_cdrzd2808zd2_507
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd2804zd2_506));
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd2808zd2_507))))
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_arg1893z00_510;
																																					obj_t
																																						BgL_arg1894z00_511;
																																					BgL_arg1893z00_510
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2804zd2_506));
																																					BgL_arg1894z00_511
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2808zd2_507));
																																					{
																																						obj_t
																																							BgL_yz00_3870;
																																						obj_t
																																							BgL_xz00_3869;
																																						BgL_xz00_3869
																																							=
																																							BgL_arg1893z00_510;
																																						BgL_yz00_3870
																																							=
																																							BgL_arg1894z00_511;
																																						BgL_yz00_69
																																							=
																																							BgL_yz00_3870;
																																						BgL_xz00_68
																																							=
																																							BgL_xz00_3869;
																																						goto
																																							BgL_tagzd2105zd2_70;
																																					}
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					goto
																																						BgL_tagzd2106zd2_71;
																																				}
																																		}
																																	}
																															}
																														}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_cdrzd2819zd2_516;
																													BgL_cdrzd2819zd2_516 =
																														CDR(((obj_t)
																															BgL_cdrzd2112zd2_74));
																													{	/* Expand/iarith.scm 53 */
																														bool_t
																															BgL_test3216z00_3873;
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_tmpz00_3874;
																															BgL_tmpz00_3874 =
																																CAR(((obj_t)
																																	BgL_cdrzd2112zd2_74));
																															BgL_test3216z00_3873
																																=
																																CHARP
																																(BgL_tmpz00_3874);
																														}
																														if (BgL_test3216z00_3873)
																															{	/* Expand/iarith.scm 53 */
																																bool_t
																																	BgL_test3217z00_3878;
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_tmpz00_3879;
																																	BgL_tmpz00_3879
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2819zd2_516));
																																	BgL_test3217z00_3878
																																		=
																																		CHARP
																																		(BgL_tmpz00_3879);
																																}
																																if (BgL_test3217z00_3878)
																																	{	/* Expand/iarith.scm 53 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd2819zd2_516))))
																																			{	/* Expand/iarith.scm 53 */
																																				return
																																					BFALSE;
																																			}
																																		else
																																			{	/* Expand/iarith.scm 53 */
																																				goto
																																					BgL_tagzd2106zd2_71;
																																			}
																																	}
																																else
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_cdrzd2831zd2_524;
																																		BgL_cdrzd2831zd2_524
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_5));
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_cdrzd2835zd2_525;
																																			BgL_cdrzd2835zd2_525
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd2831zd2_524));
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd2835zd2_525))))
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_arg1912z00_528;
																																					obj_t
																																						BgL_arg1913z00_529;
																																					BgL_arg1912z00_528
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2831zd2_524));
																																					BgL_arg1913z00_529
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2835zd2_525));
																																					{
																																						obj_t
																																							BgL_yz00_3900;
																																						obj_t
																																							BgL_xz00_3899;
																																						BgL_xz00_3899
																																							=
																																							BgL_arg1912z00_528;
																																						BgL_yz00_3900
																																							=
																																							BgL_arg1913z00_529;
																																						BgL_yz00_69
																																							=
																																							BgL_yz00_3900;
																																						BgL_xz00_68
																																							=
																																							BgL_xz00_3899;
																																						goto
																																							BgL_tagzd2105zd2_70;
																																					}
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					goto
																																						BgL_tagzd2106zd2_71;
																																				}
																																		}
																																	}
																															}
																														else
																															{	/* Expand/iarith.scm 53 */
																																obj_t
																																	BgL_cdrzd2846zd2_532;
																																BgL_cdrzd2846zd2_532
																																	=
																																	CDR(((obj_t)
																																		BgL_xz00_5));
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_cdrzd2850zd2_533;
																																	BgL_cdrzd2850zd2_533
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd2846zd2_532));
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd2850zd2_533))))
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_arg1919z00_536;
																																			obj_t
																																				BgL_arg1920z00_537;
																																			BgL_arg1919z00_536
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2846zd2_532));
																																			BgL_arg1920z00_537
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2850zd2_533));
																																			{
																																				obj_t
																																					BgL_yz00_3914;
																																				obj_t
																																					BgL_xz00_3913;
																																				BgL_xz00_3913
																																					=
																																					BgL_arg1919z00_536;
																																				BgL_yz00_3914
																																					=
																																					BgL_arg1920z00_537;
																																				BgL_yz00_69
																																					=
																																					BgL_yz00_3914;
																																				BgL_xz00_68
																																					=
																																					BgL_xz00_3913;
																																				goto
																																					BgL_tagzd2105zd2_70;
																																			}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			goto
																																				BgL_tagzd2106zd2_71;
																																		}
																																}
																															}
																													}
																												}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											obj_t
																												BgL_cdrzd2861zd2_541;
																											BgL_cdrzd2861zd2_541 =
																												CDR(((obj_t)
																													BgL_cdrzd2112zd2_74));
																											{	/* Expand/iarith.scm 53 */
																												bool_t
																													BgL_test3221z00_3917;
																												{	/* Expand/iarith.scm 53 */
																													obj_t BgL_tmpz00_3918;

																													BgL_tmpz00_3918 =
																														CAR(
																														((obj_t)
																															BgL_cdrzd2112zd2_74));
																													BgL_test3221z00_3917 =
																														CHARP
																														(BgL_tmpz00_3918);
																												}
																												if (BgL_test3221z00_3917)
																													{	/* Expand/iarith.scm 53 */
																														bool_t
																															BgL_test3222z00_3922;
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_tmpz00_3923;
																															BgL_tmpz00_3923 =
																																CAR(((obj_t)
																																	BgL_cdrzd2861zd2_541));
																															BgL_test3222z00_3922
																																=
																																CHARP
																																(BgL_tmpz00_3923);
																														}
																														if (BgL_test3222z00_3922)
																															{	/* Expand/iarith.scm 53 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd2861zd2_541))))
																																	{	/* Expand/iarith.scm 53 */
																																		return
																																			BFALSE;
																																	}
																																else
																																	{	/* Expand/iarith.scm 53 */
																																		goto
																																			BgL_tagzd2106zd2_71;
																																	}
																															}
																														else
																															{	/* Expand/iarith.scm 53 */
																																obj_t
																																	BgL_cdrzd2873zd2_549;
																																BgL_cdrzd2873zd2_549
																																	=
																																	CDR(((obj_t)
																																		BgL_xz00_5));
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_cdrzd2877zd2_550;
																																	BgL_cdrzd2877zd2_550
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd2873zd2_549));
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd2877zd2_550))))
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_arg1934z00_553;
																																			obj_t
																																				BgL_arg1935z00_554;
																																			BgL_arg1934z00_553
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2873zd2_549));
																																			BgL_arg1935z00_554
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2877zd2_550));
																																			{
																																				obj_t
																																					BgL_yz00_3944;
																																				obj_t
																																					BgL_xz00_3943;
																																				BgL_xz00_3943
																																					=
																																					BgL_arg1934z00_553;
																																				BgL_yz00_3944
																																					=
																																					BgL_arg1935z00_554;
																																				BgL_yz00_69
																																					=
																																					BgL_yz00_3944;
																																				BgL_xz00_68
																																					=
																																					BgL_xz00_3943;
																																				goto
																																					BgL_tagzd2105zd2_70;
																																			}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			goto
																																				BgL_tagzd2106zd2_71;
																																		}
																																}
																															}
																													}
																												else
																													{	/* Expand/iarith.scm 53 */
																														obj_t
																															BgL_cdrzd2888zd2_557;
																														BgL_cdrzd2888zd2_557
																															=
																															CDR(((obj_t)
																																BgL_xz00_5));
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_cdrzd2892zd2_558;
																															BgL_cdrzd2892zd2_558
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd2888zd2_557));
																															if (NULLP(CDR((
																																			(obj_t)
																																			BgL_cdrzd2892zd2_558))))
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_arg1940z00_561;
																																	obj_t
																																		BgL_arg1941z00_562;
																																	BgL_arg1940z00_561
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2888zd2_557));
																																	BgL_arg1941z00_562
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2892zd2_558));
																																	{
																																		obj_t
																																			BgL_yz00_3958;
																																		obj_t
																																			BgL_xz00_3957;
																																		BgL_xz00_3957
																																			=
																																			BgL_arg1940z00_561;
																																		BgL_yz00_3958
																																			=
																																			BgL_arg1941z00_562;
																																		BgL_yz00_69
																																			=
																																			BgL_yz00_3958;
																																		BgL_xz00_68
																																			=
																																			BgL_xz00_3957;
																																		goto
																																			BgL_tagzd2105zd2_70;
																																	}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	goto
																																		BgL_tagzd2106zd2_71;
																																}
																														}
																													}
																											}
																										}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_cdrzd2903zd2_567;

																									BgL_cdrzd2903zd2_567 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd2112zd2_74));
																									{	/* Expand/iarith.scm 53 */
																										bool_t BgL_test3226z00_3961;

																										{	/* Expand/iarith.scm 53 */
																											obj_t BgL_tmpz00_3962;

																											BgL_tmpz00_3962 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd2112zd2_74));
																											BgL_test3226z00_3961 =
																												CHARP(BgL_tmpz00_3962);
																										}
																										if (BgL_test3226z00_3961)
																											{	/* Expand/iarith.scm 53 */
																												bool_t
																													BgL_test3227z00_3966;
																												{	/* Expand/iarith.scm 53 */
																													obj_t BgL_tmpz00_3967;

																													BgL_tmpz00_3967 =
																														CAR(
																														((obj_t)
																															BgL_cdrzd2903zd2_567));
																													BgL_test3227z00_3966 =
																														CHARP
																														(BgL_tmpz00_3967);
																												}
																												if (BgL_test3227z00_3966)
																													{	/* Expand/iarith.scm 53 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_cdrzd2903zd2_567))))
																															{	/* Expand/iarith.scm 53 */
																																return BFALSE;
																															}
																														else
																															{	/* Expand/iarith.scm 53 */
																																goto
																																	BgL_tagzd2106zd2_71;
																															}
																													}
																												else
																													{	/* Expand/iarith.scm 53 */
																														obj_t
																															BgL_cdrzd2915zd2_575;
																														BgL_cdrzd2915zd2_575
																															=
																															CDR(((obj_t)
																																BgL_xz00_5));
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_cdrzd2919zd2_576;
																															BgL_cdrzd2919zd2_576
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd2915zd2_575));
																															if (NULLP(CDR((
																																			(obj_t)
																																			BgL_cdrzd2919zd2_576))))
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_arg1954z00_579;
																																	obj_t
																																		BgL_arg1955z00_580;
																																	BgL_arg1954z00_579
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2915zd2_575));
																																	BgL_arg1955z00_580
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2919zd2_576));
																																	{
																																		obj_t
																																			BgL_yz00_3988;
																																		obj_t
																																			BgL_xz00_3987;
																																		BgL_xz00_3987
																																			=
																																			BgL_arg1954z00_579;
																																		BgL_yz00_3988
																																			=
																																			BgL_arg1955z00_580;
																																		BgL_yz00_69
																																			=
																																			BgL_yz00_3988;
																																		BgL_xz00_68
																																			=
																																			BgL_xz00_3987;
																																		goto
																																			BgL_tagzd2105zd2_70;
																																	}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	goto
																																		BgL_tagzd2106zd2_71;
																																}
																														}
																													}
																											}
																										else
																											{	/* Expand/iarith.scm 53 */
																												obj_t
																													BgL_cdrzd2930zd2_583;
																												BgL_cdrzd2930zd2_583 =
																													CDR(((obj_t)
																														BgL_xz00_5));
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_cdrzd2934zd2_584;
																													BgL_cdrzd2934zd2_584 =
																														CDR(((obj_t)
																															BgL_cdrzd2930zd2_583));
																													if (NULLP(CDR(((obj_t)
																																	BgL_cdrzd2934zd2_584))))
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_arg1960z00_587;
																															obj_t
																																BgL_arg1961z00_588;
																															BgL_arg1960z00_587
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2930zd2_583));
																															BgL_arg1961z00_588
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2934zd2_584));
																															{
																																obj_t
																																	BgL_yz00_4002;
																																obj_t
																																	BgL_xz00_4001;
																																BgL_xz00_4001 =
																																	BgL_arg1960z00_587;
																																BgL_yz00_4002 =
																																	BgL_arg1961z00_588;
																																BgL_yz00_69 =
																																	BgL_yz00_4002;
																																BgL_xz00_68 =
																																	BgL_xz00_4001;
																																goto
																																	BgL_tagzd2105zd2_70;
																															}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															goto
																																BgL_tagzd2106zd2_71;
																														}
																												}
																											}
																									}
																								}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							obj_t BgL_cdrzd2945zd2_593;

																							BgL_cdrzd2945zd2_593 =
																								CDR(
																								((obj_t) BgL_cdrzd2112zd2_74));
																							{	/* Expand/iarith.scm 53 */
																								bool_t BgL_test3231z00_4005;

																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_tmpz00_4006;

																									BgL_tmpz00_4006 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd2112zd2_74));
																									BgL_test3231z00_4005 =
																										CHARP(BgL_tmpz00_4006);
																								}
																								if (BgL_test3231z00_4005)
																									{	/* Expand/iarith.scm 53 */
																										bool_t BgL_test3232z00_4010;

																										{	/* Expand/iarith.scm 53 */
																											obj_t BgL_tmpz00_4011;

																											BgL_tmpz00_4011 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd2945zd2_593));
																											BgL_test3232z00_4010 =
																												CHARP(BgL_tmpz00_4011);
																										}
																										if (BgL_test3232z00_4010)
																											{	/* Expand/iarith.scm 53 */
																												if (NULLP(CDR(
																															((obj_t)
																																BgL_cdrzd2945zd2_593))))
																													{	/* Expand/iarith.scm 53 */
																														return BFALSE;
																													}
																												else
																													{	/* Expand/iarith.scm 53 */
																														goto
																															BgL_tagzd2106zd2_71;
																													}
																											}
																										else
																											{	/* Expand/iarith.scm 53 */
																												obj_t
																													BgL_cdrzd2957zd2_601;
																												BgL_cdrzd2957zd2_601 =
																													CDR(((obj_t)
																														BgL_xz00_5));
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_cdrzd2961zd2_602;
																													BgL_cdrzd2961zd2_602 =
																														CDR(((obj_t)
																															BgL_cdrzd2957zd2_601));
																													if (NULLP(CDR(((obj_t)
																																	BgL_cdrzd2961zd2_602))))
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_arg1974z00_605;
																															obj_t
																																BgL_arg1975z00_606;
																															BgL_arg1974z00_605
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2957zd2_601));
																															BgL_arg1975z00_606
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2961zd2_602));
																															{
																																obj_t
																																	BgL_yz00_4032;
																																obj_t
																																	BgL_xz00_4031;
																																BgL_xz00_4031 =
																																	BgL_arg1974z00_605;
																																BgL_yz00_4032 =
																																	BgL_arg1975z00_606;
																																BgL_yz00_69 =
																																	BgL_yz00_4032;
																																BgL_xz00_68 =
																																	BgL_xz00_4031;
																																goto
																																	BgL_tagzd2105zd2_70;
																															}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															goto
																																BgL_tagzd2106zd2_71;
																														}
																												}
																											}
																									}
																								else
																									{	/* Expand/iarith.scm 53 */
																										obj_t BgL_cdrzd2972zd2_609;

																										BgL_cdrzd2972zd2_609 =
																											CDR(((obj_t) BgL_xz00_5));
																										{	/* Expand/iarith.scm 53 */
																											obj_t
																												BgL_cdrzd2976zd2_610;
																											BgL_cdrzd2976zd2_610 =
																												CDR(((obj_t)
																													BgL_cdrzd2972zd2_609));
																											if (NULLP(CDR(((obj_t)
																															BgL_cdrzd2976zd2_610))))
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_arg1980z00_613;
																													obj_t
																														BgL_arg1981z00_614;
																													BgL_arg1980z00_613 =
																														CAR(((obj_t)
																															BgL_cdrzd2972zd2_609));
																													BgL_arg1981z00_614 =
																														CAR(((obj_t)
																															BgL_cdrzd2976zd2_610));
																													{
																														obj_t BgL_yz00_4046;
																														obj_t BgL_xz00_4045;

																														BgL_xz00_4045 =
																															BgL_arg1980z00_613;
																														BgL_yz00_4046 =
																															BgL_arg1981z00_614;
																														BgL_yz00_69 =
																															BgL_yz00_4046;
																														BgL_xz00_68 =
																															BgL_xz00_4045;
																														goto
																															BgL_tagzd2105zd2_70;
																													}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													goto
																														BgL_tagzd2106zd2_71;
																												}
																										}
																									}
																							}
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 53 */
																					obj_t BgL_cdrzd2987zd2_618;

																					BgL_cdrzd2987zd2_618 =
																						CDR(((obj_t) BgL_cdrzd2112zd2_74));
																					{	/* Expand/iarith.scm 53 */
																						bool_t BgL_test3236z00_4049;

																						{	/* Expand/iarith.scm 53 */
																							obj_t BgL_tmpz00_4050;

																							BgL_tmpz00_4050 =
																								CAR(
																								((obj_t) BgL_cdrzd2112zd2_74));
																							BgL_test3236z00_4049 =
																								CHARP(BgL_tmpz00_4050);
																						}
																						if (BgL_test3236z00_4049)
																							{	/* Expand/iarith.scm 53 */
																								bool_t BgL_test3237z00_4054;

																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_tmpz00_4055;

																									BgL_tmpz00_4055 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd2987zd2_618));
																									BgL_test3237z00_4054 =
																										CHARP(BgL_tmpz00_4055);
																								}
																								if (BgL_test3237z00_4054)
																									{	/* Expand/iarith.scm 53 */
																										if (NULLP(CDR(
																													((obj_t)
																														BgL_cdrzd2987zd2_618))))
																											{	/* Expand/iarith.scm 53 */
																												return BFALSE;
																											}
																										else
																											{	/* Expand/iarith.scm 53 */
																												goto
																													BgL_tagzd2106zd2_71;
																											}
																									}
																								else
																									{	/* Expand/iarith.scm 53 */
																										obj_t BgL_cdrzd2999zd2_626;

																										BgL_cdrzd2999zd2_626 =
																											CDR(((obj_t) BgL_xz00_5));
																										{	/* Expand/iarith.scm 53 */
																											obj_t
																												BgL_cdrzd21003zd2_627;
																											BgL_cdrzd21003zd2_627 =
																												CDR(((obj_t)
																													BgL_cdrzd2999zd2_626));
																											if (NULLP(CDR(((obj_t)
																															BgL_cdrzd21003zd2_627))))
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_arg1993z00_630;
																													obj_t
																														BgL_arg1994z00_631;
																													BgL_arg1993z00_630 =
																														CAR(((obj_t)
																															BgL_cdrzd2999zd2_626));
																													BgL_arg1994z00_631 =
																														CAR(((obj_t)
																															BgL_cdrzd21003zd2_627));
																													{
																														obj_t BgL_yz00_4076;
																														obj_t BgL_xz00_4075;

																														BgL_xz00_4075 =
																															BgL_arg1993z00_630;
																														BgL_yz00_4076 =
																															BgL_arg1994z00_631;
																														BgL_yz00_69 =
																															BgL_yz00_4076;
																														BgL_xz00_68 =
																															BgL_xz00_4075;
																														goto
																															BgL_tagzd2105zd2_70;
																													}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													goto
																														BgL_tagzd2106zd2_71;
																												}
																										}
																									}
																							}
																						else
																							{	/* Expand/iarith.scm 53 */
																								obj_t BgL_cdrzd21014zd2_634;

																								BgL_cdrzd21014zd2_634 =
																									CDR(((obj_t) BgL_xz00_5));
																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_cdrzd21018zd2_635;

																									BgL_cdrzd21018zd2_635 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd21014zd2_634));
																									if (NULLP(CDR(((obj_t)
																													BgL_cdrzd21018zd2_635))))
																										{	/* Expand/iarith.scm 53 */
																											obj_t BgL_arg1999z00_638;
																											obj_t BgL_arg2000z00_639;

																											BgL_arg1999z00_638 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd21014zd2_634));
																											BgL_arg2000z00_639 =
																												CAR(((obj_t)
																													BgL_cdrzd21018zd2_635));
																											{
																												obj_t BgL_yz00_4090;
																												obj_t BgL_xz00_4089;

																												BgL_xz00_4089 =
																													BgL_arg1999z00_638;
																												BgL_yz00_4090 =
																													BgL_arg2000z00_639;
																												BgL_yz00_69 =
																													BgL_yz00_4090;
																												BgL_xz00_68 =
																													BgL_xz00_4089;
																												goto
																													BgL_tagzd2105zd2_70;
																											}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											goto BgL_tagzd2106zd2_71;
																										}
																								}
																							}
																					}
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 53 */
																			bool_t BgL_test3241z00_4091;

																			{	/* Expand/iarith.scm 53 */
																				obj_t BgL_tmpz00_4092;

																				BgL_tmpz00_4092 =
																					CAR(((obj_t) BgL_cdrzd2112zd2_74));
																				BgL_test3241z00_4091 =
																					CHARP(BgL_tmpz00_4092);
																			}
																			if (BgL_test3241z00_4091)
																				{	/* Expand/iarith.scm 53 */
																					bool_t BgL_test3242z00_4096;

																					{	/* Expand/iarith.scm 53 */
																						obj_t BgL_tmpz00_4097;

																						BgL_tmpz00_4097 =
																							CAR(
																							((obj_t) BgL_cdrzd2629zd2_389));
																						BgL_test3242z00_4096 =
																							CHARP(BgL_tmpz00_4097);
																					}
																					if (BgL_test3242z00_4096)
																						{	/* Expand/iarith.scm 53 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd2629zd2_389))))
																								{	/* Expand/iarith.scm 53 */
																									return BFALSE;
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									goto BgL_tagzd2106zd2_71;
																								}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							obj_t BgL_cdrzd21043zd2_652;

																							BgL_cdrzd21043zd2_652 =
																								CDR(((obj_t) BgL_xz00_5));
																							{	/* Expand/iarith.scm 53 */
																								obj_t BgL_cdrzd21048zd2_653;

																								BgL_cdrzd21048zd2_653 =
																									CDR(
																									((obj_t)
																										BgL_cdrzd21043zd2_652));
																								if (NULLP(CDR(((obj_t)
																												BgL_cdrzd21048zd2_653))))
																									{	/* Expand/iarith.scm 53 */
																										obj_t BgL_arg2013z00_656;
																										obj_t BgL_arg2014z00_657;

																										BgL_arg2013z00_656 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd21043zd2_652));
																										BgL_arg2014z00_657 =
																											CAR(((obj_t)
																												BgL_cdrzd21048zd2_653));
																										{
																											obj_t BgL_yz00_4118;
																											obj_t BgL_xz00_4117;

																											BgL_xz00_4117 =
																												BgL_arg2013z00_656;
																											BgL_yz00_4118 =
																												BgL_arg2014z00_657;
																											BgL_yz00_69 =
																												BgL_yz00_4118;
																											BgL_xz00_68 =
																												BgL_xz00_4117;
																											goto BgL_tagzd2105zd2_70;
																										}
																									}
																								else
																									{	/* Expand/iarith.scm 53 */
																										goto BgL_tagzd2106zd2_71;
																									}
																							}
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 53 */
																					obj_t BgL_cdrzd21061zd2_660;

																					BgL_cdrzd21061zd2_660 =
																						CDR(((obj_t) BgL_xz00_5));
																					{	/* Expand/iarith.scm 53 */
																						obj_t BgL_cdrzd21066zd2_661;

																						BgL_cdrzd21066zd2_661 =
																							CDR(
																							((obj_t) BgL_cdrzd21061zd2_660));
																						if (NULLP(CDR(
																									((obj_t)
																										BgL_cdrzd21066zd2_661))))
																							{	/* Expand/iarith.scm 53 */
																								obj_t BgL_arg2019z00_664;
																								obj_t BgL_arg2020z00_665;

																								BgL_arg2019z00_664 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd21061zd2_660));
																								BgL_arg2020z00_665 =
																									CAR(((obj_t)
																										BgL_cdrzd21066zd2_661));
																								{
																									obj_t BgL_yz00_4132;
																									obj_t BgL_xz00_4131;

																									BgL_xz00_4131 =
																										BgL_arg2019z00_664;
																									BgL_yz00_4132 =
																										BgL_arg2020z00_665;
																									BgL_yz00_69 = BgL_yz00_4132;
																									BgL_xz00_68 = BgL_xz00_4131;
																									goto BgL_tagzd2105zd2_70;
																								}
																							}
																						else
																							{	/* Expand/iarith.scm 53 */
																								goto BgL_tagzd2106zd2_71;
																							}
																					}
																				}
																		}
																}
														}
												}
											else
												{	/* Expand/iarith.scm 53 */
													goto BgL_tagzd2106zd2_71;
												}
										}
									else
										{	/* Expand/iarith.scm 53 */
											obj_t BgL_carzd21091zd2_670;
											obj_t BgL_cdrzd21092zd2_671;

											BgL_carzd21091zd2_670 =
												CAR(((obj_t) BgL_cdrzd2112zd2_74));
											BgL_cdrzd21092zd2_671 =
												CDR(((obj_t) BgL_cdrzd2112zd2_74));
											if (PAIRP(BgL_carzd21091zd2_670))
												{	/* Expand/iarith.scm 53 */
													obj_t BgL_cdrzd21095zd2_673;

													BgL_cdrzd21095zd2_673 = CDR(BgL_carzd21091zd2_670);
													if ((CAR(BgL_carzd21091zd2_670) == CNST_TABLE_REF(1)))
														{	/* Expand/iarith.scm 53 */
															if (PAIRP(BgL_cdrzd21095zd2_673))
																{	/* Expand/iarith.scm 53 */
																	obj_t BgL_yz00_677;

																	BgL_yz00_677 = CAR(BgL_cdrzd21095zd2_673);
																	if (NULLP(CDR(BgL_cdrzd21095zd2_673)))
																		{	/* Expand/iarith.scm 53 */
																			if (PAIRP(BgL_cdrzd21092zd2_671))
																				{	/* Expand/iarith.scm 53 */
																					obj_t BgL_carzd21101zd2_681;

																					BgL_carzd21101zd2_681 =
																						CAR(BgL_cdrzd21092zd2_671);
																					if (PAIRP(BgL_carzd21101zd2_681))
																						{	/* Expand/iarith.scm 53 */
																							obj_t BgL_cdrzd21105zd2_683;

																							BgL_cdrzd21105zd2_683 =
																								CDR(BgL_carzd21101zd2_681);
																							if (
																								(CAR(BgL_carzd21101zd2_681) ==
																									CNST_TABLE_REF(1)))
																								{	/* Expand/iarith.scm 53 */
																									if (PAIRP
																										(BgL_cdrzd21105zd2_683))
																										{	/* Expand/iarith.scm 53 */
																											if (
																												(BgL_yz00_677 ==
																													CAR
																													(BgL_cdrzd21105zd2_683)))
																												{	/* Expand/iarith.scm 53 */
																													if (NULLP(CDR
																															(BgL_cdrzd21105zd2_683)))
																														{	/* Expand/iarith.scm 53 */
																															if (NULLP(CDR
																																	(BgL_cdrzd21092zd2_671)))
																																{	/* Expand/iarith.scm 53 */
																																	return BTRUE;
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	goto
																																		BgL_tagzd2106zd2_71;
																																}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_cdrzd21123zd2_694;
																															BgL_cdrzd21123zd2_694
																																=
																																CDR(((obj_t)
																																	BgL_xz00_5));
																															{	/* Expand/iarith.scm 53 */
																																obj_t
																																	BgL_cdrzd21125zd2_695;
																																BgL_cdrzd21125zd2_695
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd21123zd2_694));
																																{	/* Expand/iarith.scm 53 */
																																	bool_t
																																		BgL_test3257z00_4175;
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_tmpz00_4176;
																																		BgL_tmpz00_4176
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd21123zd2_694));
																																		BgL_test3257z00_4175
																																			=
																																			CHARP
																																			(BgL_tmpz00_4176);
																																	}
																																	if (BgL_test3257z00_4175)
																																		{	/* Expand/iarith.scm 53 */
																																			bool_t
																																				BgL_test3258z00_4180;
																																			{	/* Expand/iarith.scm 53 */
																																				obj_t
																																					BgL_tmpz00_4181;
																																				BgL_tmpz00_4181
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd21125zd2_695));
																																				BgL_test3258z00_4180
																																					=
																																					CHARP
																																					(BgL_tmpz00_4181);
																																			}
																																			if (BgL_test3258z00_4180)
																																				{	/* Expand/iarith.scm 53 */
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd21125zd2_695))))
																																						{	/* Expand/iarith.scm 53 */
																																							return
																																								BFALSE;
																																						}
																																					else
																																						{	/* Expand/iarith.scm 53 */
																																							goto
																																								BgL_tagzd2106zd2_71;
																																						}
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd21125zd2_695))))
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_arg2055z00_707;
																																							obj_t
																																								BgL_arg2056z00_708;
																																							BgL_arg2055z00_707
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21123zd2_694));
																																							BgL_arg2056z00_708
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21125zd2_695));
																																							{
																																								obj_t
																																									BgL_yz00_4198;
																																								obj_t
																																									BgL_xz00_4197;
																																								BgL_xz00_4197
																																									=
																																									BgL_arg2055z00_707;
																																								BgL_yz00_4198
																																									=
																																									BgL_arg2056z00_708;
																																								BgL_yz00_69
																																									=
																																									BgL_yz00_4198;
																																								BgL_xz00_68
																																									=
																																									BgL_xz00_4197;
																																								goto
																																									BgL_tagzd2105zd2_70;
																																							}
																																						}
																																					else
																																						{	/* Expand/iarith.scm 53 */
																																							goto
																																								BgL_tagzd2106zd2_71;
																																						}
																																				}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd21125zd2_695))))
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_arg2061z00_715;
																																					obj_t
																																						BgL_arg2062z00_716;
																																					BgL_arg2061z00_715
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21123zd2_694));
																																					BgL_arg2062z00_716
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21125zd2_695));
																																					{
																																						obj_t
																																							BgL_yz00_4208;
																																						obj_t
																																							BgL_xz00_4207;
																																						BgL_xz00_4207
																																							=
																																							BgL_arg2061z00_715;
																																						BgL_yz00_4208
																																							=
																																							BgL_arg2062z00_716;
																																						BgL_yz00_69
																																							=
																																							BgL_yz00_4208;
																																						BgL_xz00_68
																																							=
																																							BgL_xz00_4207;
																																						goto
																																							BgL_tagzd2105zd2_70;
																																					}
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					goto
																																						BgL_tagzd2106zd2_71;
																																				}
																																		}
																																}
																															}
																														}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_cdrzd21165zd2_720;
																													BgL_cdrzd21165zd2_720
																														=
																														CDR(((obj_t)
																															BgL_xz00_5));
																													{	/* Expand/iarith.scm 53 */
																														obj_t
																															BgL_cdrzd21167zd2_721;
																														BgL_cdrzd21167zd2_721
																															=
																															CDR(((obj_t)
																																BgL_cdrzd21165zd2_720));
																														{	/* Expand/iarith.scm 53 */
																															bool_t
																																BgL_test3262z00_4213;
																															{	/* Expand/iarith.scm 53 */
																																obj_t
																																	BgL_tmpz00_4214;
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_pairz00_2462;
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_pairz00_2461;
																																		BgL_pairz00_2461
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd21165zd2_720));
																																		BgL_pairz00_2462
																																			=
																																			CDR
																																			(BgL_pairz00_2461);
																																	}
																																	BgL_tmpz00_4214
																																		=
																																		CAR
																																		(BgL_pairz00_2462);
																																}
																																BgL_test3262z00_4213
																																	=
																																	SYMBOLP
																																	(BgL_tmpz00_4214);
																															}
																															if (BgL_test3262z00_4213)
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_cdrzd21175zd2_726;
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_pairz00_2464;
																																		BgL_pairz00_2464
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd21167zd2_721));
																																		BgL_cdrzd21175zd2_726
																																			=
																																			CDR
																																			(BgL_pairz00_2464);
																																	}
																																	{	/* Expand/iarith.scm 53 */
																																		bool_t
																																			BgL_test3263z00_4223;
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_tmpz00_4224;
																																			BgL_tmpz00_4224
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd21175zd2_726));
																																			BgL_test3263z00_4223
																																				=
																																				SYMBOLP
																																				(BgL_tmpz00_4224);
																																		}
																																		if (BgL_test3263z00_4223)
																																			{	/* Expand/iarith.scm 53 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd21175zd2_726))))
																																					{	/* Expand/iarith.scm 53 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd21167zd2_721))))
																																							{	/* Expand/iarith.scm 53 */
																																								return
																																									BFALSE;
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								goto
																																									BgL_tagzd2106zd2_71;
																																							}
																																					}
																																				else
																																					{	/* Expand/iarith.scm 53 */
																																						bool_t
																																							BgL_test3266z00_4236;
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_tmpz00_4237;
																																							BgL_tmpz00_4237
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21165zd2_720));
																																							BgL_test3266z00_4236
																																								=
																																								CHARP
																																								(BgL_tmpz00_4237);
																																						}
																																						if (BgL_test3266z00_4236)
																																							{	/* Expand/iarith.scm 53 */
																																								bool_t
																																									BgL_test3267z00_4241;
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_tmpz00_4242;
																																									BgL_tmpz00_4242
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd21167zd2_721));
																																									BgL_test3267z00_4241
																																										=
																																										CHARP
																																										(BgL_tmpz00_4242);
																																								}
																																								if (BgL_test3267z00_4241)
																																									{	/* Expand/iarith.scm 53 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd21167zd2_721))))
																																											{	/* Expand/iarith.scm 53 */
																																												return
																																													BFALSE;
																																											}
																																										else
																																											{	/* Expand/iarith.scm 53 */
																																												goto
																																													BgL_tagzd2106zd2_71;
																																											}
																																									}
																																								else
																																									{	/* Expand/iarith.scm 53 */
																																										obj_t
																																											BgL_cdrzd21201zd2_743;
																																										BgL_cdrzd21201zd2_743
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_xz00_5));
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_cdrzd21205zd2_744;
																																											BgL_cdrzd21205zd2_744
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd21201zd2_743));
																																											if (NULLP(CDR(((obj_t) BgL_cdrzd21205zd2_744))))
																																												{	/* Expand/iarith.scm 53 */
																																													obj_t
																																														BgL_arg2089z00_747;
																																													obj_t
																																														BgL_arg2090z00_748;
																																													BgL_arg2089z00_747
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd21201zd2_743));
																																													BgL_arg2090z00_748
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd21205zd2_744));
																																													{
																																														obj_t
																																															BgL_yz00_4263;
																																														obj_t
																																															BgL_xz00_4262;
																																														BgL_xz00_4262
																																															=
																																															BgL_arg2089z00_747;
																																														BgL_yz00_4263
																																															=
																																															BgL_arg2090z00_748;
																																														BgL_yz00_69
																																															=
																																															BgL_yz00_4263;
																																														BgL_xz00_68
																																															=
																																															BgL_xz00_4262;
																																														goto
																																															BgL_tagzd2105zd2_70;
																																													}
																																												}
																																											else
																																												{	/* Expand/iarith.scm 53 */
																																													goto
																																														BgL_tagzd2106zd2_71;
																																												}
																																										}
																																									}
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								obj_t
																																									BgL_cdrzd21216zd2_751;
																																								BgL_cdrzd21216zd2_751
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_xz00_5));
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_cdrzd21220zd2_752;
																																									BgL_cdrzd21220zd2_752
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd21216zd2_751));
																																									if (NULLP(CDR(((obj_t) BgL_cdrzd21220zd2_752))))
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_arg2096z00_755;
																																											obj_t
																																												BgL_arg2097z00_756;
																																											BgL_arg2096z00_755
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd21216zd2_751));
																																											BgL_arg2097z00_756
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd21220zd2_752));
																																											{
																																												obj_t
																																													BgL_yz00_4277;
																																												obj_t
																																													BgL_xz00_4276;
																																												BgL_xz00_4276
																																													=
																																													BgL_arg2096z00_755;
																																												BgL_yz00_4277
																																													=
																																													BgL_arg2097z00_756;
																																												BgL_yz00_69
																																													=
																																													BgL_yz00_4277;
																																												BgL_xz00_68
																																													=
																																													BgL_xz00_4276;
																																												goto
																																													BgL_tagzd2105zd2_70;
																																											}
																																										}
																																									else
																																										{	/* Expand/iarith.scm 53 */
																																											goto
																																												BgL_tagzd2106zd2_71;
																																										}
																																								}
																																							}
																																					}
																																			}
																																		else
																																			{	/* Expand/iarith.scm 53 */
																																				bool_t
																																					BgL_test3271z00_4278;
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_tmpz00_4279;
																																					BgL_tmpz00_4279
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21165zd2_720));
																																					BgL_test3271z00_4278
																																						=
																																						CHARP
																																						(BgL_tmpz00_4279);
																																				}
																																				if (BgL_test3271z00_4278)
																																					{	/* Expand/iarith.scm 53 */
																																						bool_t
																																							BgL_test3272z00_4283;
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_tmpz00_4284;
																																							BgL_tmpz00_4284
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21167zd2_721));
																																							BgL_test3272z00_4283
																																								=
																																								CHARP
																																								(BgL_tmpz00_4284);
																																						}
																																						if (BgL_test3272z00_4283)
																																							{	/* Expand/iarith.scm 53 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd21167zd2_721))))
																																									{	/* Expand/iarith.scm 53 */
																																										return
																																											BFALSE;
																																									}
																																								else
																																									{	/* Expand/iarith.scm 53 */
																																										goto
																																											BgL_tagzd2106zd2_71;
																																									}
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								obj_t
																																									BgL_cdrzd21243zd2_769;
																																								BgL_cdrzd21243zd2_769
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_xz00_5));
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_cdrzd21247zd2_770;
																																									BgL_cdrzd21247zd2_770
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd21243zd2_769));
																																									if (NULLP(CDR(((obj_t) BgL_cdrzd21247zd2_770))))
																																										{	/* Expand/iarith.scm 53 */
																																											obj_t
																																												BgL_arg2110z00_773;
																																											obj_t
																																												BgL_arg2111z00_774;
																																											BgL_arg2110z00_773
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd21243zd2_769));
																																											BgL_arg2111z00_774
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd21247zd2_770));
																																											{
																																												obj_t
																																													BgL_yz00_4305;
																																												obj_t
																																													BgL_xz00_4304;
																																												BgL_xz00_4304
																																													=
																																													BgL_arg2110z00_773;
																																												BgL_yz00_4305
																																													=
																																													BgL_arg2111z00_774;
																																												BgL_yz00_69
																																													=
																																													BgL_yz00_4305;
																																												BgL_xz00_68
																																													=
																																													BgL_xz00_4304;
																																												goto
																																													BgL_tagzd2105zd2_70;
																																											}
																																										}
																																									else
																																										{	/* Expand/iarith.scm 53 */
																																											goto
																																												BgL_tagzd2106zd2_71;
																																										}
																																								}
																																							}
																																					}
																																				else
																																					{	/* Expand/iarith.scm 53 */
																																						obj_t
																																							BgL_cdrzd21258zd2_777;
																																						BgL_cdrzd21258zd2_777
																																							=
																																							CDR
																																							(((obj_t) BgL_xz00_5));
																																						{	/* Expand/iarith.scm 53 */
																																							obj_t
																																								BgL_cdrzd21262zd2_778;
																																							BgL_cdrzd21262zd2_778
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd21258zd2_777));
																																							if (NULLP(CDR(((obj_t) BgL_cdrzd21262zd2_778))))
																																								{	/* Expand/iarith.scm 53 */
																																									obj_t
																																										BgL_arg2116z00_781;
																																									obj_t
																																										BgL_arg2117z00_782;
																																									BgL_arg2116z00_781
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd21258zd2_777));
																																									BgL_arg2117z00_782
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd21262zd2_778));
																																									{
																																										obj_t
																																											BgL_yz00_4319;
																																										obj_t
																																											BgL_xz00_4318;
																																										BgL_xz00_4318
																																											=
																																											BgL_arg2116z00_781;
																																										BgL_yz00_4319
																																											=
																																											BgL_arg2117z00_782;
																																										BgL_yz00_69
																																											=
																																											BgL_yz00_4319;
																																										BgL_xz00_68
																																											=
																																											BgL_xz00_4318;
																																										goto
																																											BgL_tagzd2105zd2_70;
																																									}
																																								}
																																							else
																																								{	/* Expand/iarith.scm 53 */
																																									goto
																																										BgL_tagzd2106zd2_71;
																																								}
																																						}
																																					}
																																			}
																																	}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	bool_t
																																		BgL_test3276z00_4320;
																																	{	/* Expand/iarith.scm 53 */
																																		obj_t
																																			BgL_tmpz00_4321;
																																		BgL_tmpz00_4321
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd21165zd2_720));
																																		BgL_test3276z00_4320
																																			=
																																			CHARP
																																			(BgL_tmpz00_4321);
																																	}
																																	if (BgL_test3276z00_4320)
																																		{	/* Expand/iarith.scm 53 */
																																			bool_t
																																				BgL_test3277z00_4325;
																																			{	/* Expand/iarith.scm 53 */
																																				obj_t
																																					BgL_tmpz00_4326;
																																				BgL_tmpz00_4326
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd21167zd2_721));
																																				BgL_test3277z00_4325
																																					=
																																					CHARP
																																					(BgL_tmpz00_4326);
																																			}
																																			if (BgL_test3277z00_4325)
																																				{	/* Expand/iarith.scm 53 */
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd21167zd2_721))))
																																						{	/* Expand/iarith.scm 53 */
																																							return
																																								BFALSE;
																																						}
																																					else
																																						{	/* Expand/iarith.scm 53 */
																																							goto
																																								BgL_tagzd2106zd2_71;
																																						}
																																				}
																																			else
																																				{	/* Expand/iarith.scm 53 */
																																					obj_t
																																						BgL_cdrzd21285zd2_796;
																																					BgL_cdrzd21285zd2_796
																																						=
																																						CDR(
																																						((obj_t) BgL_xz00_5));
																																					{	/* Expand/iarith.scm 53 */
																																						obj_t
																																							BgL_cdrzd21289zd2_797;
																																						BgL_cdrzd21289zd2_797
																																							=
																																							CDR
																																							(((obj_t) BgL_cdrzd21285zd2_796));
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd21289zd2_797))))
																																							{	/* Expand/iarith.scm 53 */
																																								obj_t
																																									BgL_arg2132z00_800;
																																								obj_t
																																									BgL_arg2133z00_801;
																																								BgL_arg2132z00_800
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd21285zd2_796));
																																								BgL_arg2133z00_801
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd21289zd2_797));
																																								{
																																									obj_t
																																										BgL_yz00_4347;
																																									obj_t
																																										BgL_xz00_4346;
																																									BgL_xz00_4346
																																										=
																																										BgL_arg2132z00_800;
																																									BgL_yz00_4347
																																										=
																																										BgL_arg2133z00_801;
																																									BgL_yz00_69
																																										=
																																										BgL_yz00_4347;
																																									BgL_xz00_68
																																										=
																																										BgL_xz00_4346;
																																									goto
																																										BgL_tagzd2105zd2_70;
																																								}
																																							}
																																						else
																																							{	/* Expand/iarith.scm 53 */
																																								goto
																																									BgL_tagzd2106zd2_71;
																																							}
																																					}
																																				}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_cdrzd21300zd2_804;
																																			BgL_cdrzd21300zd2_804
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_xz00_5));
																																			{	/* Expand/iarith.scm 53 */
																																				obj_t
																																					BgL_cdrzd21304zd2_805;
																																				BgL_cdrzd21304zd2_805
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd21300zd2_804));
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd21304zd2_805))))
																																					{	/* Expand/iarith.scm 53 */
																																						obj_t
																																							BgL_arg2138z00_808;
																																						obj_t
																																							BgL_arg2139z00_809;
																																						BgL_arg2138z00_808
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd21300zd2_804));
																																						BgL_arg2139z00_809
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd21304zd2_805));
																																						{
																																							obj_t
																																								BgL_yz00_4361;
																																							obj_t
																																								BgL_xz00_4360;
																																							BgL_xz00_4360
																																								=
																																								BgL_arg2138z00_808;
																																							BgL_yz00_4361
																																								=
																																								BgL_arg2139z00_809;
																																							BgL_yz00_69
																																								=
																																								BgL_yz00_4361;
																																							BgL_xz00_68
																																								=
																																								BgL_xz00_4360;
																																							goto
																																								BgL_tagzd2105zd2_70;
																																						}
																																					}
																																				else
																																					{	/* Expand/iarith.scm 53 */
																																						goto
																																							BgL_tagzd2106zd2_71;
																																					}
																																			}
																																		}
																																}
																														}
																													}
																												}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											obj_t
																												BgL_cdrzd21315zd2_816;
																											BgL_cdrzd21315zd2_816 =
																												CDR(((obj_t)
																													BgL_xz00_5));
																											{	/* Expand/iarith.scm 53 */
																												obj_t
																													BgL_cdrzd21317zd2_817;
																												BgL_cdrzd21317zd2_817 =
																													CDR(((obj_t)
																														BgL_cdrzd21315zd2_816));
																												{	/* Expand/iarith.scm 53 */
																													bool_t
																														BgL_test3281z00_4366;
																													{	/* Expand/iarith.scm 53 */
																														obj_t
																															BgL_tmpz00_4367;
																														BgL_tmpz00_4367 =
																															CAR(((obj_t)
																																BgL_cdrzd21315zd2_816));
																														BgL_test3281z00_4366
																															=
																															CHARP
																															(BgL_tmpz00_4367);
																													}
																													if (BgL_test3281z00_4366)
																														{	/* Expand/iarith.scm 53 */
																															bool_t
																																BgL_test3282z00_4371;
																															{	/* Expand/iarith.scm 53 */
																																obj_t
																																	BgL_tmpz00_4372;
																																BgL_tmpz00_4372
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd21317zd2_817));
																																BgL_test3282z00_4371
																																	=
																																	CHARP
																																	(BgL_tmpz00_4372);
																															}
																															if (BgL_test3282z00_4371)
																																{	/* Expand/iarith.scm 53 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd21317zd2_817))))
																																		{	/* Expand/iarith.scm 53 */
																																			return
																																				BFALSE;
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			goto
																																				BgL_tagzd2106zd2_71;
																																		}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd21317zd2_817))))
																																		{	/* Expand/iarith.scm 53 */
																																			obj_t
																																				BgL_arg2157z00_829;
																																			obj_t
																																				BgL_arg2158z00_830;
																																			BgL_arg2157z00_829
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd21315zd2_816));
																																			BgL_arg2158z00_830
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd21317zd2_817));
																																			{
																																				obj_t
																																					BgL_yz00_4389;
																																				obj_t
																																					BgL_xz00_4388;
																																				BgL_xz00_4388
																																					=
																																					BgL_arg2157z00_829;
																																				BgL_yz00_4389
																																					=
																																					BgL_arg2158z00_830;
																																				BgL_yz00_69
																																					=
																																					BgL_yz00_4389;
																																				BgL_xz00_68
																																					=
																																					BgL_xz00_4388;
																																				goto
																																					BgL_tagzd2105zd2_70;
																																			}
																																		}
																																	else
																																		{	/* Expand/iarith.scm 53 */
																																			goto
																																				BgL_tagzd2106zd2_71;
																																		}
																																}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd21317zd2_817))))
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_arg2163z00_837;
																																	obj_t
																																		BgL_arg2164z00_838;
																																	BgL_arg2163z00_837
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21315zd2_816));
																																	BgL_arg2164z00_838
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21317zd2_817));
																																	{
																																		obj_t
																																			BgL_yz00_4399;
																																		obj_t
																																			BgL_xz00_4398;
																																		BgL_xz00_4398
																																			=
																																			BgL_arg2163z00_837;
																																		BgL_yz00_4399
																																			=
																																			BgL_arg2164z00_838;
																																		BgL_yz00_69
																																			=
																																			BgL_yz00_4399;
																																		BgL_xz00_68
																																			=
																																			BgL_xz00_4398;
																																		goto
																																			BgL_tagzd2105zd2_70;
																																	}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	goto
																																		BgL_tagzd2106zd2_71;
																																}
																														}
																												}
																											}
																										}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_cdrzd21359zd2_841;

																									BgL_cdrzd21359zd2_841 =
																										CDR(((obj_t) BgL_xz00_5));
																									{	/* Expand/iarith.scm 53 */
																										obj_t BgL_cdrzd21361zd2_842;

																										BgL_cdrzd21361zd2_842 =
																											CDR(
																											((obj_t)
																												BgL_cdrzd21359zd2_841));
																										{	/* Expand/iarith.scm 53 */
																											bool_t
																												BgL_test3286z00_4404;
																											{	/* Expand/iarith.scm 53 */
																												obj_t BgL_tmpz00_4405;

																												BgL_tmpz00_4405 =
																													CAR(
																													((obj_t)
																														BgL_cdrzd21359zd2_841));
																												BgL_test3286z00_4404 =
																													CHARP
																													(BgL_tmpz00_4405);
																											}
																											if (BgL_test3286z00_4404)
																												{	/* Expand/iarith.scm 53 */
																													bool_t
																														BgL_test3287z00_4409;
																													{	/* Expand/iarith.scm 53 */
																														obj_t
																															BgL_tmpz00_4410;
																														BgL_tmpz00_4410 =
																															CAR(((obj_t)
																																BgL_cdrzd21361zd2_842));
																														BgL_test3287z00_4409
																															=
																															CHARP
																															(BgL_tmpz00_4410);
																													}
																													if (BgL_test3287z00_4409)
																														{	/* Expand/iarith.scm 53 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd21361zd2_842))))
																																{	/* Expand/iarith.scm 53 */
																																	return BFALSE;
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	goto
																																		BgL_tagzd2106zd2_71;
																																}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd21361zd2_842))))
																																{	/* Expand/iarith.scm 53 */
																																	obj_t
																																		BgL_arg2176z00_854;
																																	obj_t
																																		BgL_arg2177z00_855;
																																	BgL_arg2176z00_854
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21359zd2_841));
																																	BgL_arg2177z00_855
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21361zd2_842));
																																	{
																																		obj_t
																																			BgL_yz00_4427;
																																		obj_t
																																			BgL_xz00_4426;
																																		BgL_xz00_4426
																																			=
																																			BgL_arg2176z00_854;
																																		BgL_yz00_4427
																																			=
																																			BgL_arg2177z00_855;
																																		BgL_yz00_69
																																			=
																																			BgL_yz00_4427;
																																		BgL_xz00_68
																																			=
																																			BgL_xz00_4426;
																																		goto
																																			BgL_tagzd2105zd2_70;
																																	}
																																}
																															else
																																{	/* Expand/iarith.scm 53 */
																																	goto
																																		BgL_tagzd2106zd2_71;
																																}
																														}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd21361zd2_842))))
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_arg2182z00_862;
																															obj_t
																																BgL_arg2183z00_863;
																															BgL_arg2182z00_862
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21359zd2_841));
																															BgL_arg2183z00_863
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21361zd2_842));
																															{
																																obj_t
																																	BgL_yz00_4437;
																																obj_t
																																	BgL_xz00_4436;
																																BgL_xz00_4436 =
																																	BgL_arg2182z00_862;
																																BgL_yz00_4437 =
																																	BgL_arg2183z00_863;
																																BgL_yz00_69 =
																																	BgL_yz00_4437;
																																BgL_xz00_68 =
																																	BgL_xz00_4436;
																																goto
																																	BgL_tagzd2105zd2_70;
																															}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															goto
																																BgL_tagzd2106zd2_71;
																														}
																												}
																										}
																									}
																								}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							obj_t BgL_cdrzd21403zd2_867;

																							BgL_cdrzd21403zd2_867 =
																								CDR(((obj_t) BgL_xz00_5));
																							{	/* Expand/iarith.scm 53 */
																								obj_t BgL_cdrzd21405zd2_868;

																								BgL_cdrzd21405zd2_868 =
																									CDR(
																									((obj_t)
																										BgL_cdrzd21403zd2_867));
																								{	/* Expand/iarith.scm 53 */
																									bool_t BgL_test3291z00_4442;

																									{	/* Expand/iarith.scm 53 */
																										obj_t BgL_tmpz00_4443;

																										BgL_tmpz00_4443 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd21403zd2_867));
																										BgL_test3291z00_4442 =
																											CHARP(BgL_tmpz00_4443);
																									}
																									if (BgL_test3291z00_4442)
																										{	/* Expand/iarith.scm 53 */
																											bool_t
																												BgL_test3292z00_4447;
																											{	/* Expand/iarith.scm 53 */
																												obj_t BgL_tmpz00_4448;

																												BgL_tmpz00_4448 =
																													CAR(
																													((obj_t)
																														BgL_cdrzd21405zd2_868));
																												BgL_test3292z00_4447 =
																													CHARP
																													(BgL_tmpz00_4448);
																											}
																											if (BgL_test3292z00_4447)
																												{	/* Expand/iarith.scm 53 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd21405zd2_868))))
																														{	/* Expand/iarith.scm 53 */
																															return BFALSE;
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															goto
																																BgL_tagzd2106zd2_71;
																														}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd21405zd2_868))))
																														{	/* Expand/iarith.scm 53 */
																															obj_t
																																BgL_arg2196z00_880;
																															obj_t
																																BgL_arg2197z00_881;
																															BgL_arg2196z00_880
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21403zd2_867));
																															BgL_arg2197z00_881
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21405zd2_868));
																															{
																																obj_t
																																	BgL_yz00_4465;
																																obj_t
																																	BgL_xz00_4464;
																																BgL_xz00_4464 =
																																	BgL_arg2196z00_880;
																																BgL_yz00_4465 =
																																	BgL_arg2197z00_881;
																																BgL_yz00_69 =
																																	BgL_yz00_4465;
																																BgL_xz00_68 =
																																	BgL_xz00_4464;
																																goto
																																	BgL_tagzd2105zd2_70;
																															}
																														}
																													else
																														{	/* Expand/iarith.scm 53 */
																															goto
																																BgL_tagzd2106zd2_71;
																														}
																												}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd21405zd2_868))))
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_arg2202z00_888;
																													obj_t
																														BgL_arg2203z00_889;
																													BgL_arg2202z00_888 =
																														CAR(((obj_t)
																															BgL_cdrzd21403zd2_867));
																													BgL_arg2203z00_889 =
																														CAR(((obj_t)
																															BgL_cdrzd21405zd2_868));
																													{
																														obj_t BgL_yz00_4475;
																														obj_t BgL_xz00_4474;

																														BgL_xz00_4474 =
																															BgL_arg2202z00_888;
																														BgL_yz00_4475 =
																															BgL_arg2203z00_889;
																														BgL_yz00_69 =
																															BgL_yz00_4475;
																														BgL_xz00_68 =
																															BgL_xz00_4474;
																														goto
																															BgL_tagzd2105zd2_70;
																													}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													goto
																														BgL_tagzd2106zd2_71;
																												}
																										}
																								}
																							}
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 53 */
																					goto BgL_tagzd2106zd2_71;
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 53 */
																			obj_t BgL_cdrzd21455zd2_892;

																			BgL_cdrzd21455zd2_892 =
																				CDR(((obj_t) BgL_xz00_5));
																			{	/* Expand/iarith.scm 53 */
																				obj_t BgL_cdrzd21457zd2_893;

																				BgL_cdrzd21457zd2_893 =
																					CDR(((obj_t) BgL_cdrzd21455zd2_892));
																				{	/* Expand/iarith.scm 53 */
																					bool_t BgL_test3296z00_4480;

																					{	/* Expand/iarith.scm 53 */
																						obj_t BgL_tmpz00_4481;

																						BgL_tmpz00_4481 =
																							CAR(
																							((obj_t) BgL_cdrzd21455zd2_892));
																						BgL_test3296z00_4480 =
																							CHARP(BgL_tmpz00_4481);
																					}
																					if (BgL_test3296z00_4480)
																						{	/* Expand/iarith.scm 53 */
																							if (PAIRP(BgL_cdrzd21457zd2_893))
																								{	/* Expand/iarith.scm 53 */
																									bool_t BgL_test3298z00_4487;

																									{	/* Expand/iarith.scm 53 */
																										obj_t BgL_tmpz00_4488;

																										BgL_tmpz00_4488 =
																											CAR
																											(BgL_cdrzd21457zd2_893);
																										BgL_test3298z00_4487 =
																											CHARP(BgL_tmpz00_4488);
																									}
																									if (BgL_test3298z00_4487)
																										{	/* Expand/iarith.scm 53 */
																											if (NULLP(CDR
																													(BgL_cdrzd21457zd2_893)))
																												{	/* Expand/iarith.scm 53 */
																													return BFALSE;
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													goto
																														BgL_tagzd2106zd2_71;
																												}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd21457zd2_893))))
																												{	/* Expand/iarith.scm 53 */
																													obj_t
																														BgL_arg2216z00_906;
																													obj_t
																														BgL_arg2217z00_907;
																													BgL_arg2216z00_906 =
																														CAR(((obj_t)
																															BgL_cdrzd21455zd2_892));
																													BgL_arg2217z00_907 =
																														CAR(((obj_t)
																															BgL_cdrzd21457zd2_893));
																													{
																														obj_t BgL_yz00_4503;
																														obj_t BgL_xz00_4502;

																														BgL_xz00_4502 =
																															BgL_arg2216z00_906;
																														BgL_yz00_4503 =
																															BgL_arg2217z00_907;
																														BgL_yz00_69 =
																															BgL_yz00_4503;
																														BgL_xz00_68 =
																															BgL_xz00_4502;
																														goto
																															BgL_tagzd2105zd2_70;
																													}
																												}
																											else
																												{	/* Expand/iarith.scm 53 */
																													goto
																														BgL_tagzd2106zd2_71;
																												}
																										}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									goto BgL_tagzd2106zd2_71;
																								}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							if (PAIRP(BgL_cdrzd21457zd2_893))
																								{	/* Expand/iarith.scm 53 */
																									if (NULLP(CDR
																											(BgL_cdrzd21457zd2_893)))
																										{	/* Expand/iarith.scm 53 */
																											obj_t BgL_arg2223z00_915;
																											obj_t BgL_arg2224z00_916;

																											BgL_arg2223z00_915 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd21455zd2_892));
																											BgL_arg2224z00_916 =
																												CAR
																												(BgL_cdrzd21457zd2_893);
																											{
																												obj_t BgL_yz00_4513;
																												obj_t BgL_xz00_4512;

																												BgL_xz00_4512 =
																													BgL_arg2223z00_915;
																												BgL_yz00_4513 =
																													BgL_arg2224z00_916;
																												BgL_yz00_69 =
																													BgL_yz00_4513;
																												BgL_xz00_68 =
																													BgL_xz00_4512;
																												goto
																													BgL_tagzd2105zd2_70;
																											}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											goto BgL_tagzd2106zd2_71;
																										}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									goto BgL_tagzd2106zd2_71;
																								}
																						}
																				}
																			}
																		}
																}
															else
																{	/* Expand/iarith.scm 53 */
																	obj_t BgL_cdrzd21503zd2_920;

																	BgL_cdrzd21503zd2_920 =
																		CDR(((obj_t) BgL_xz00_5));
																	{	/* Expand/iarith.scm 53 */
																		obj_t BgL_cdrzd21505zd2_921;

																		BgL_cdrzd21505zd2_921 =
																			CDR(((obj_t) BgL_cdrzd21503zd2_920));
																		{	/* Expand/iarith.scm 53 */
																			bool_t BgL_test3303z00_4518;

																			{	/* Expand/iarith.scm 53 */
																				obj_t BgL_tmpz00_4519;

																				BgL_tmpz00_4519 =
																					CAR(((obj_t) BgL_cdrzd21503zd2_920));
																				BgL_test3303z00_4518 =
																					CHARP(BgL_tmpz00_4519);
																			}
																			if (BgL_test3303z00_4518)
																				{	/* Expand/iarith.scm 53 */
																					if (PAIRP(BgL_cdrzd21505zd2_921))
																						{	/* Expand/iarith.scm 53 */
																							bool_t BgL_test3305z00_4525;

																							{	/* Expand/iarith.scm 53 */
																								obj_t BgL_tmpz00_4526;

																								BgL_tmpz00_4526 =
																									CAR(BgL_cdrzd21505zd2_921);
																								BgL_test3305z00_4525 =
																									CHARP(BgL_tmpz00_4526);
																							}
																							if (BgL_test3305z00_4525)
																								{	/* Expand/iarith.scm 53 */
																									if (NULLP(CDR
																											(BgL_cdrzd21505zd2_921)))
																										{	/* Expand/iarith.scm 53 */
																											return BFALSE;
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											goto BgL_tagzd2106zd2_71;
																										}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd21505zd2_921))))
																										{	/* Expand/iarith.scm 53 */
																											obj_t BgL_arg2238z00_934;
																											obj_t BgL_arg2239z00_935;

																											BgL_arg2238z00_934 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd21503zd2_920));
																											BgL_arg2239z00_935 =
																												CAR(((obj_t)
																													BgL_cdrzd21505zd2_921));
																											{
																												obj_t BgL_yz00_4541;
																												obj_t BgL_xz00_4540;

																												BgL_xz00_4540 =
																													BgL_arg2238z00_934;
																												BgL_yz00_4541 =
																													BgL_arg2239z00_935;
																												BgL_yz00_69 =
																													BgL_yz00_4541;
																												BgL_xz00_68 =
																													BgL_xz00_4540;
																												goto
																													BgL_tagzd2105zd2_70;
																											}
																										}
																									else
																										{	/* Expand/iarith.scm 53 */
																											goto BgL_tagzd2106zd2_71;
																										}
																								}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							goto BgL_tagzd2106zd2_71;
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 53 */
																					if (PAIRP(BgL_cdrzd21505zd2_921))
																						{	/* Expand/iarith.scm 53 */
																							if (NULLP(CDR
																									(BgL_cdrzd21505zd2_921)))
																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_arg2245z00_943;
																									obj_t BgL_arg2246z00_944;

																									BgL_arg2245z00_943 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd21503zd2_920));
																									BgL_arg2246z00_944 =
																										CAR(BgL_cdrzd21505zd2_921);
																									{
																										obj_t BgL_yz00_4551;
																										obj_t BgL_xz00_4550;

																										BgL_xz00_4550 =
																											BgL_arg2245z00_943;
																										BgL_yz00_4551 =
																											BgL_arg2246z00_944;
																										BgL_yz00_69 = BgL_yz00_4551;
																										BgL_xz00_68 = BgL_xz00_4550;
																										goto BgL_tagzd2105zd2_70;
																									}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									goto BgL_tagzd2106zd2_71;
																								}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							goto BgL_tagzd2106zd2_71;
																						}
																				}
																		}
																	}
																}
														}
													else
														{	/* Expand/iarith.scm 53 */
															obj_t BgL_cdrzd21551zd2_947;

															BgL_cdrzd21551zd2_947 = CDR(((obj_t) BgL_xz00_5));
															{	/* Expand/iarith.scm 53 */
																obj_t BgL_cdrzd21553zd2_948;

																BgL_cdrzd21553zd2_948 =
																	CDR(((obj_t) BgL_cdrzd21551zd2_947));
																{	/* Expand/iarith.scm 53 */
																	bool_t BgL_test3310z00_4556;

																	{	/* Expand/iarith.scm 53 */
																		obj_t BgL_tmpz00_4557;

																		BgL_tmpz00_4557 =
																			CAR(((obj_t) BgL_cdrzd21551zd2_947));
																		BgL_test3310z00_4556 =
																			CHARP(BgL_tmpz00_4557);
																	}
																	if (BgL_test3310z00_4556)
																		{	/* Expand/iarith.scm 53 */
																			if (PAIRP(BgL_cdrzd21553zd2_948))
																				{	/* Expand/iarith.scm 53 */
																					bool_t BgL_test3312z00_4563;

																					{	/* Expand/iarith.scm 53 */
																						obj_t BgL_tmpz00_4564;

																						BgL_tmpz00_4564 =
																							CAR(BgL_cdrzd21553zd2_948);
																						BgL_test3312z00_4563 =
																							CHARP(BgL_tmpz00_4564);
																					}
																					if (BgL_test3312z00_4563)
																						{	/* Expand/iarith.scm 53 */
																							if (NULLP(CDR
																									(BgL_cdrzd21553zd2_948)))
																								{	/* Expand/iarith.scm 53 */
																									return BFALSE;
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									goto BgL_tagzd2106zd2_71;
																								}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd21553zd2_948))))
																								{	/* Expand/iarith.scm 53 */
																									obj_t BgL_arg2259z00_961;
																									obj_t BgL_arg2260z00_962;

																									BgL_arg2259z00_961 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd21551zd2_947));
																									BgL_arg2260z00_962 =
																										CAR(((obj_t)
																											BgL_cdrzd21553zd2_948));
																									{
																										obj_t BgL_yz00_4579;
																										obj_t BgL_xz00_4578;

																										BgL_xz00_4578 =
																											BgL_arg2259z00_961;
																										BgL_yz00_4579 =
																											BgL_arg2260z00_962;
																										BgL_yz00_69 = BgL_yz00_4579;
																										BgL_xz00_68 = BgL_xz00_4578;
																										goto BgL_tagzd2105zd2_70;
																									}
																								}
																							else
																								{	/* Expand/iarith.scm 53 */
																									goto BgL_tagzd2106zd2_71;
																								}
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 53 */
																					goto BgL_tagzd2106zd2_71;
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 53 */
																			if (PAIRP(BgL_cdrzd21553zd2_948))
																				{	/* Expand/iarith.scm 53 */
																					if (NULLP(CDR(BgL_cdrzd21553zd2_948)))
																						{	/* Expand/iarith.scm 53 */
																							obj_t BgL_arg2266z00_970;
																							obj_t BgL_arg2267z00_971;

																							BgL_arg2266z00_970 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd21551zd2_947));
																							BgL_arg2267z00_971 =
																								CAR(BgL_cdrzd21553zd2_948);
																							{
																								obj_t BgL_yz00_4589;
																								obj_t BgL_xz00_4588;

																								BgL_xz00_4588 =
																									BgL_arg2266z00_970;
																								BgL_yz00_4589 =
																									BgL_arg2267z00_971;
																								BgL_yz00_69 = BgL_yz00_4589;
																								BgL_xz00_68 = BgL_xz00_4588;
																								goto BgL_tagzd2105zd2_70;
																							}
																						}
																					else
																						{	/* Expand/iarith.scm 53 */
																							goto BgL_tagzd2106zd2_71;
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 53 */
																					goto BgL_tagzd2106zd2_71;
																				}
																		}
																}
															}
														}
												}
											else
												{	/* Expand/iarith.scm 53 */
													obj_t BgL_cdrzd21601zd2_976;

													BgL_cdrzd21601zd2_976 =
														CDR(((obj_t) BgL_cdrzd2112zd2_74));
													{	/* Expand/iarith.scm 53 */
														bool_t BgL_test3317z00_4592;

														{	/* Expand/iarith.scm 53 */
															obj_t BgL_tmpz00_4593;

															BgL_tmpz00_4593 =
																CAR(((obj_t) BgL_cdrzd2112zd2_74));
															BgL_test3317z00_4592 = CHARP(BgL_tmpz00_4593);
														}
														if (BgL_test3317z00_4592)
															{	/* Expand/iarith.scm 53 */
																if (PAIRP(BgL_cdrzd21601zd2_976))
																	{	/* Expand/iarith.scm 53 */
																		bool_t BgL_test3319z00_4599;

																		{	/* Expand/iarith.scm 53 */
																			obj_t BgL_tmpz00_4600;

																			BgL_tmpz00_4600 =
																				CAR(BgL_cdrzd21601zd2_976);
																			BgL_test3319z00_4599 =
																				CHARP(BgL_tmpz00_4600);
																		}
																		if (BgL_test3319z00_4599)
																			{	/* Expand/iarith.scm 53 */
																				if (NULLP(CDR(BgL_cdrzd21601zd2_976)))
																					{	/* Expand/iarith.scm 53 */
																						return BFALSE;
																					}
																				else
																					{	/* Expand/iarith.scm 53 */
																						goto BgL_tagzd2106zd2_71;
																					}
																			}
																		else
																			{	/* Expand/iarith.scm 53 */
																				if (NULLP(CDR(
																							((obj_t) BgL_cdrzd21601zd2_976))))
																					{	/* Expand/iarith.scm 53 */
																						obj_t BgL_arg2282z00_989;
																						obj_t BgL_arg2283z00_990;

																						BgL_arg2282z00_989 =
																							CAR(
																							((obj_t) BgL_cdrzd2112zd2_74));
																						BgL_arg2283z00_990 =
																							CAR(
																							((obj_t) BgL_cdrzd21601zd2_976));
																						{
																							obj_t BgL_yz00_4615;
																							obj_t BgL_xz00_4614;

																							BgL_xz00_4614 =
																								BgL_arg2282z00_989;
																							BgL_yz00_4615 =
																								BgL_arg2283z00_990;
																							BgL_yz00_69 = BgL_yz00_4615;
																							BgL_xz00_68 = BgL_xz00_4614;
																							goto BgL_tagzd2105zd2_70;
																						}
																					}
																				else
																					{	/* Expand/iarith.scm 53 */
																						goto BgL_tagzd2106zd2_71;
																					}
																			}
																	}
																else
																	{	/* Expand/iarith.scm 53 */
																		goto BgL_tagzd2106zd2_71;
																	}
															}
														else
															{	/* Expand/iarith.scm 53 */
																if (PAIRP(BgL_cdrzd21601zd2_976))
																	{	/* Expand/iarith.scm 53 */
																		if (NULLP(CDR(BgL_cdrzd21601zd2_976)))
																			{	/* Expand/iarith.scm 53 */
																				obj_t BgL_arg2290z00_998;
																				obj_t BgL_arg2291z00_999;

																				BgL_arg2290z00_998 =
																					CAR(((obj_t) BgL_cdrzd2112zd2_74));
																				BgL_arg2291z00_999 =
																					CAR(BgL_cdrzd21601zd2_976);
																				{
																					obj_t BgL_yz00_4625;
																					obj_t BgL_xz00_4624;

																					BgL_xz00_4624 = BgL_arg2290z00_998;
																					BgL_yz00_4625 = BgL_arg2291z00_999;
																					BgL_yz00_69 = BgL_yz00_4625;
																					BgL_xz00_68 = BgL_xz00_4624;
																					goto BgL_tagzd2105zd2_70;
																				}
																			}
																		else
																			{	/* Expand/iarith.scm 53 */
																				goto BgL_tagzd2106zd2_71;
																			}
																	}
																else
																	{	/* Expand/iarith.scm 53 */
																		goto BgL_tagzd2106zd2_71;
																	}
															}
													}
												}
										}
								}
							}
						else
							{	/* Expand/iarith.scm 53 */
								goto BgL_tagzd2106zd2_71;
							}
					}
				else
					{	/* Expand/iarith.scm 53 */
						goto BgL_tagzd2106zd2_71;
					}
			}
		}

	}



/* &expand-eq? */
	obj_t BGl_z62expandzd2eqzf3z43zzexpand_iarithmetiquez00(obj_t BgL_envz00_3043,
		obj_t BgL_xz00_3044, obj_t BgL_ez00_3045)
	{
		{	/* Expand/iarith.scm 52 */
			return
				BGl_expandzd2eqzf3z21zzexpand_iarithmetiquez00(BgL_xz00_3044,
				BgL_ez00_3045);
		}

	}



/* expand-i+ */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2izb2z60zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_7, obj_t BgL_ez00_8)
	{
		{	/* Expand/iarith.scm 70 */
			{
				obj_t BgL_xz00_1014;
				obj_t BgL_yz00_1015;
				obj_t BgL_xz00_1011;
				obj_t BgL_yz00_1012;

				if (PAIRP(BgL_xz00_7))
					{	/* Expand/iarith.scm 71 */
						if (NULLP(CDR(((obj_t) BgL_xz00_7))))
							{	/* Expand/iarith.scm 71 */
								return BINT(0L);
							}
						else
							{	/* Expand/iarith.scm 71 */
								obj_t BgL_cdrzd21677zd2_1021;

								BgL_cdrzd21677zd2_1021 = CDR(((obj_t) BgL_xz00_7));
								if (PAIRP(BgL_cdrzd21677zd2_1021))
									{	/* Expand/iarith.scm 71 */
										if (NULLP(CDR(BgL_cdrzd21677zd2_1021)))
											{	/* Expand/iarith.scm 71 */
												obj_t BgL_arg2304z00_1025;

												BgL_arg2304z00_1025 = CAR(BgL_cdrzd21677zd2_1021);
												return
													BGL_PROCEDURE_CALL2(BgL_ez00_8, BgL_arg2304z00_1025,
													BgL_ez00_8);
											}
										else
											{	/* Expand/iarith.scm 71 */
												obj_t BgL_cdrzd21693zd2_1027;

												BgL_cdrzd21693zd2_1027 =
													CDR(((obj_t) BgL_cdrzd21677zd2_1021));
												if (PAIRP(BgL_cdrzd21693zd2_1027))
													{	/* Expand/iarith.scm 71 */
														if (NULLP(CDR(BgL_cdrzd21693zd2_1027)))
															{	/* Expand/iarith.scm 71 */
																obj_t BgL_arg2308z00_1031;
																obj_t BgL_arg2309z00_1032;

																BgL_arg2308z00_1031 =
																	CAR(((obj_t) BgL_cdrzd21677zd2_1021));
																BgL_arg2309z00_1032 =
																	CAR(BgL_cdrzd21693zd2_1027);
																BgL_xz00_1011 = BgL_arg2308z00_1031;
																BgL_yz00_1012 = BgL_arg2309z00_1032;
																{	/* Expand/iarith.scm 78 */
																	bool_t BgL_test3330z00_4657;

																	if (INTEGERP(BgL_xz00_1011))
																		{	/* Expand/iarith.scm 78 */
																			BgL_test3330z00_4657 =
																				INTEGERP(BgL_yz00_1012);
																		}
																	else
																		{	/* Expand/iarith.scm 78 */
																			BgL_test3330z00_4657 = ((bool_t) 0);
																		}
																	if (BgL_test3330z00_4657)
																		{	/* Expand/iarith.scm 78 */
																			return
																				ADDFX(BgL_xz00_1011, BgL_yz00_1012);
																		}
																	else
																		{	/* Expand/iarith.scm 81 */
																			obj_t BgL_arg2319z00_1044;

																			{	/* Expand/iarith.scm 81 */
																				obj_t BgL_arg2320z00_1045;

																				{	/* Expand/iarith.scm 81 */
																					obj_t BgL_arg2321z00_1046;

																					BgL_arg2321z00_1046 =
																						MAKE_YOUNG_PAIR(BgL_yz00_1012,
																						BNIL);
																					BgL_arg2320z00_1045 =
																						MAKE_YOUNG_PAIR(BgL_xz00_1011,
																						BgL_arg2321z00_1046);
																				}
																				BgL_arg2319z00_1044 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																					BgL_arg2320z00_1045);
																			}
																			return
																				BGL_PROCEDURE_CALL2(BgL_ez00_8,
																				BgL_arg2319z00_1044, BgL_ez00_8);
																		}
																}
															}
														else
															{	/* Expand/iarith.scm 71 */
																obj_t BgL_arg2310z00_1034;
																obj_t BgL_arg2311z00_1035;

																BgL_arg2310z00_1034 =
																	CAR(((obj_t) BgL_cdrzd21677zd2_1021));
																BgL_arg2311z00_1035 =
																	CDR(((obj_t) BgL_cdrzd21677zd2_1021));
																BgL_xz00_1014 = BgL_arg2310z00_1034;
																BgL_yz00_1015 = BgL_arg2311z00_1035;
															BgL_tagzd21667zd2_1016:
																{	/* Expand/iarith.scm 83 */
																	obj_t BgL_arg2323z00_1048;

																	{	/* Expand/iarith.scm 83 */
																		obj_t BgL_arg2324z00_1049;

																		{	/* Expand/iarith.scm 83 */
																			obj_t BgL_arg2325z00_1050;

																			{	/* Expand/iarith.scm 83 */
																				obj_t BgL_arg2326z00_1051;

																				{	/* Expand/iarith.scm 83 */
																					obj_t BgL_arg2327z00_1052;

																					BgL_arg2327z00_1052 =
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_yz00_1015, BNIL);
																					BgL_arg2326z00_1051 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																						BgL_arg2327z00_1052);
																				}
																				BgL_arg2325z00_1050 =
																					MAKE_YOUNG_PAIR(BgL_arg2326z00_1051,
																					BNIL);
																			}
																			BgL_arg2324z00_1049 =
																				MAKE_YOUNG_PAIR(BgL_xz00_1014,
																				BgL_arg2325z00_1050);
																		}
																		BgL_arg2323z00_1048 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																			BgL_arg2324z00_1049);
																	}
																	return
																		BGL_PROCEDURE_CALL2(BgL_ez00_8,
																		BgL_arg2323z00_1048, BgL_ez00_8);
																}
															}
													}
												else
													{	/* Expand/iarith.scm 71 */
														obj_t BgL_arg2313z00_1038;
														obj_t BgL_arg2314z00_1039;

														BgL_arg2313z00_1038 =
															CAR(((obj_t) BgL_cdrzd21677zd2_1021));
														BgL_arg2314z00_1039 =
															CDR(((obj_t) BgL_cdrzd21677zd2_1021));
														{
															obj_t BgL_yz00_4692;
															obj_t BgL_xz00_4691;

															BgL_xz00_4691 = BgL_arg2313z00_1038;
															BgL_yz00_4692 = BgL_arg2314z00_1039;
															BgL_yz00_1015 = BgL_yz00_4692;
															BgL_xz00_1014 = BgL_xz00_4691;
															goto BgL_tagzd21667zd2_1016;
														}
													}
											}
									}
								else
									{	/* Expand/iarith.scm 71 */
										return BFALSE;
									}
							}
					}
				else
					{	/* Expand/iarith.scm 71 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-i+ */
	obj_t BGl_z62expandzd2izb2z02zzexpand_iarithmetiquez00(obj_t BgL_envz00_3046,
		obj_t BgL_xz00_3047, obj_t BgL_ez00_3048)
	{
		{	/* Expand/iarith.scm 70 */
			return
				BGl_expandzd2izb2z60zzexpand_iarithmetiquez00(BgL_xz00_3047,
				BgL_ez00_3048);
		}

	}



/* expand-i- */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2izd2z00zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_9, obj_t BgL_ez00_10)
	{
		{	/* Expand/iarith.scm 88 */
			{
				obj_t BgL_xz00_1058;
				obj_t BgL_yz00_1059;
				obj_t BgL_xz00_1055;
				obj_t BgL_yz00_1056;
				obj_t BgL_xz00_1053;

				if (PAIRP(BgL_xz00_9))
					{	/* Expand/iarith.scm 89 */
						obj_t BgL_cdrzd21757zd2_1063;

						BgL_cdrzd21757zd2_1063 = CDR(((obj_t) BgL_xz00_9));
						if (PAIRP(BgL_cdrzd21757zd2_1063))
							{	/* Expand/iarith.scm 89 */
								if (NULLP(CDR(BgL_cdrzd21757zd2_1063)))
									{	/* Expand/iarith.scm 89 */
										BgL_xz00_1053 = CAR(BgL_cdrzd21757zd2_1063);
										if (INTEGERP(BgL_xz00_1053))
											{	/* Expand/iarith.scm 92 */
												return BINT(NEG((long) CINT(BgL_xz00_1053)));
											}
										else
											{	/* Expand/iarith.scm 95 */
												obj_t BgL_arg2348z00_1084;

												{	/* Expand/iarith.scm 95 */
													obj_t BgL_arg2349z00_1085;

													BgL_arg2349z00_1085 =
														BGL_PROCEDURE_CALL2(BgL_ez00_10, BgL_xz00_1053,
														BgL_ez00_10);
													BgL_arg2348z00_1084 =
														MAKE_YOUNG_PAIR(BgL_arg2349z00_1085, BNIL);
												}
												return
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
													BgL_arg2348z00_1084);
											}
									}
								else
									{	/* Expand/iarith.scm 89 */
										obj_t BgL_cdrzd21773zd2_1069;

										BgL_cdrzd21773zd2_1069 =
											CDR(((obj_t) BgL_cdrzd21757zd2_1063));
										if (PAIRP(BgL_cdrzd21773zd2_1069))
											{	/* Expand/iarith.scm 89 */
												if (NULLP(CDR(BgL_cdrzd21773zd2_1069)))
													{	/* Expand/iarith.scm 89 */
														obj_t BgL_arg2337z00_1073;
														obj_t BgL_arg2338z00_1074;

														BgL_arg2337z00_1073 =
															CAR(((obj_t) BgL_cdrzd21757zd2_1063));
														BgL_arg2338z00_1074 = CAR(BgL_cdrzd21773zd2_1069);
														BgL_xz00_1055 = BgL_arg2337z00_1073;
														BgL_yz00_1056 = BgL_arg2338z00_1074;
														{	/* Expand/iarith.scm 98 */
															bool_t BgL_test3338z00_4727;

															if (INTEGERP(BgL_xz00_1055))
																{	/* Expand/iarith.scm 98 */
																	BgL_test3338z00_4727 =
																		INTEGERP(BgL_yz00_1056);
																}
															else
																{	/* Expand/iarith.scm 98 */
																	BgL_test3338z00_4727 = ((bool_t) 0);
																}
															if (BgL_test3338z00_4727)
																{	/* Expand/iarith.scm 98 */
																	return SUBFX(BgL_xz00_1055, BgL_yz00_1056);
																}
															else
																{	/* Expand/iarith.scm 101 */
																	obj_t BgL_arg2352z00_1088;

																	{	/* Expand/iarith.scm 101 */
																		obj_t BgL_arg2353z00_1089;

																		{	/* Expand/iarith.scm 101 */
																			obj_t BgL_arg2354z00_1090;

																			BgL_arg2354z00_1090 =
																				MAKE_YOUNG_PAIR(BgL_yz00_1056, BNIL);
																			BgL_arg2353z00_1089 =
																				MAKE_YOUNG_PAIR(BgL_xz00_1055,
																				BgL_arg2354z00_1090);
																		}
																		BgL_arg2352z00_1088 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																			BgL_arg2353z00_1089);
																	}
																	return
																		BGL_PROCEDURE_CALL2(BgL_ez00_10,
																		BgL_arg2352z00_1088, BgL_ez00_10);
																}
														}
													}
												else
													{	/* Expand/iarith.scm 89 */
														obj_t BgL_arg2339z00_1076;
														obj_t BgL_arg2340z00_1077;

														BgL_arg2339z00_1076 =
															CAR(((obj_t) BgL_cdrzd21757zd2_1063));
														BgL_arg2340z00_1077 =
															CDR(((obj_t) BgL_cdrzd21757zd2_1063));
														BgL_xz00_1058 = BgL_arg2339z00_1076;
														BgL_yz00_1059 = BgL_arg2340z00_1077;
													BgL_tagzd21751zd2_1060:
														{	/* Expand/iarith.scm 103 */
															obj_t BgL_arg2355z00_1092;

															{	/* Expand/iarith.scm 103 */
																obj_t BgL_arg2356z00_1093;

																{	/* Expand/iarith.scm 103 */
																	obj_t BgL_arg2357z00_1094;

																	{	/* Expand/iarith.scm 103 */
																		obj_t BgL_arg2358z00_1095;

																		{	/* Expand/iarith.scm 103 */
																			obj_t BgL_arg2361z00_1096;

																			BgL_arg2361z00_1096 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_yz00_1059, BNIL);
																			BgL_arg2358z00_1095 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																				BgL_arg2361z00_1096);
																		}
																		BgL_arg2357z00_1094 =
																			MAKE_YOUNG_PAIR(BgL_arg2358z00_1095,
																			BNIL);
																	}
																	BgL_arg2356z00_1093 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1058,
																		BgL_arg2357z00_1094);
																}
																BgL_arg2355z00_1092 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																	BgL_arg2356z00_1093);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_10,
																BgL_arg2355z00_1092, BgL_ez00_10);
														}
													}
											}
										else
											{	/* Expand/iarith.scm 89 */
												obj_t BgL_arg2342z00_1080;
												obj_t BgL_arg2345z00_1081;

												BgL_arg2342z00_1080 =
													CAR(((obj_t) BgL_cdrzd21757zd2_1063));
												BgL_arg2345z00_1081 =
													CDR(((obj_t) BgL_cdrzd21757zd2_1063));
												{
													obj_t BgL_yz00_4762;
													obj_t BgL_xz00_4761;

													BgL_xz00_4761 = BgL_arg2342z00_1080;
													BgL_yz00_4762 = BgL_arg2345z00_1081;
													BgL_yz00_1059 = BgL_yz00_4762;
													BgL_xz00_1058 = BgL_xz00_4761;
													goto BgL_tagzd21751zd2_1060;
												}
											}
									}
							}
						else
							{	/* Expand/iarith.scm 89 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/iarith.scm 89 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-i- */
	obj_t BGl_z62expandzd2izd2z62zzexpand_iarithmetiquez00(obj_t BgL_envz00_3049,
		obj_t BgL_xz00_3050, obj_t BgL_ez00_3051)
	{
		{	/* Expand/iarith.scm 88 */
			return
				BGl_expandzd2izd2z00zzexpand_iarithmetiquez00(BgL_xz00_3050,
				BgL_ez00_3051);
		}

	}



/* expand-i* */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2iza2z70zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_11, obj_t BgL_ez00_12)
	{
		{	/* Expand/iarith.scm 108 */
			{
				obj_t BgL_xz00_1103;
				obj_t BgL_yz00_1104;
				obj_t BgL_xz00_1100;
				obj_t BgL_yz00_1101;

				if (PAIRP(BgL_xz00_11))
					{	/* Expand/iarith.scm 109 */
						if (NULLP(CDR(((obj_t) BgL_xz00_11))))
							{	/* Expand/iarith.scm 109 */
								return BINT(1L);
							}
						else
							{	/* Expand/iarith.scm 109 */
								obj_t BgL_cdrzd21839zd2_1110;

								BgL_cdrzd21839zd2_1110 = CDR(((obj_t) BgL_xz00_11));
								if (PAIRP(BgL_cdrzd21839zd2_1110))
									{	/* Expand/iarith.scm 109 */
										if (NULLP(CDR(BgL_cdrzd21839zd2_1110)))
											{	/* Expand/iarith.scm 109 */
												obj_t BgL_arg2368z00_1114;

												BgL_arg2368z00_1114 = CAR(BgL_cdrzd21839zd2_1110);
												return
													BGL_PROCEDURE_CALL2(BgL_ez00_12, BgL_arg2368z00_1114,
													BgL_ez00_12);
											}
										else
											{	/* Expand/iarith.scm 109 */
												obj_t BgL_cdrzd21855zd2_1116;

												BgL_cdrzd21855zd2_1116 =
													CDR(((obj_t) BgL_cdrzd21839zd2_1110));
												if (PAIRP(BgL_cdrzd21855zd2_1116))
													{	/* Expand/iarith.scm 109 */
														if (NULLP(CDR(BgL_cdrzd21855zd2_1116)))
															{	/* Expand/iarith.scm 109 */
																obj_t BgL_arg2373z00_1120;
																obj_t BgL_arg2374z00_1121;

																BgL_arg2373z00_1120 =
																	CAR(((obj_t) BgL_cdrzd21839zd2_1110));
																BgL_arg2374z00_1121 =
																	CAR(BgL_cdrzd21855zd2_1116);
																BgL_xz00_1100 = BgL_arg2373z00_1120;
																BgL_yz00_1101 = BgL_arg2374z00_1121;
																{	/* Expand/iarith.scm 116 */
																	bool_t BgL_test3346z00_4794;

																	if (INTEGERP(BgL_xz00_1100))
																		{	/* Expand/iarith.scm 116 */
																			BgL_test3346z00_4794 =
																				INTEGERP(BgL_yz00_1101);
																		}
																	else
																		{	/* Expand/iarith.scm 116 */
																			BgL_test3346z00_4794 = ((bool_t) 0);
																		}
																	if (BgL_test3346z00_4794)
																		{	/* Expand/iarith.scm 116 */
																			return
																				BINT(
																				((long) CINT(BgL_xz00_1100) *
																					(long) CINT(BgL_yz00_1101)));
																		}
																	else
																		{	/* Expand/iarith.scm 119 */
																			obj_t BgL_arg2384z00_1133;

																			{	/* Expand/iarith.scm 119 */
																				obj_t BgL_arg2385z00_1134;

																				{	/* Expand/iarith.scm 119 */
																					obj_t BgL_arg2386z00_1135;

																					BgL_arg2386z00_1135 =
																						MAKE_YOUNG_PAIR(BgL_yz00_1101,
																						BNIL);
																					BgL_arg2385z00_1134 =
																						MAKE_YOUNG_PAIR(BgL_xz00_1100,
																						BgL_arg2386z00_1135);
																				}
																				BgL_arg2384z00_1133 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																					BgL_arg2385z00_1134);
																			}
																			return
																				BGL_PROCEDURE_CALL2(BgL_ez00_12,
																				BgL_arg2384z00_1133, BgL_ez00_12);
																		}
																}
															}
														else
															{	/* Expand/iarith.scm 109 */
																obj_t BgL_arg2375z00_1123;
																obj_t BgL_arg2376z00_1124;

																BgL_arg2375z00_1123 =
																	CAR(((obj_t) BgL_cdrzd21839zd2_1110));
																BgL_arg2376z00_1124 =
																	CDR(((obj_t) BgL_cdrzd21839zd2_1110));
																BgL_xz00_1103 = BgL_arg2375z00_1123;
																BgL_yz00_1104 = BgL_arg2376z00_1124;
															BgL_tagzd21829zd2_1105:
																{	/* Expand/iarith.scm 121 */
																	obj_t BgL_arg2387z00_1137;

																	{	/* Expand/iarith.scm 121 */
																		obj_t BgL_arg2388z00_1138;

																		{	/* Expand/iarith.scm 121 */
																			obj_t BgL_arg2389z00_1139;

																			{	/* Expand/iarith.scm 121 */
																				obj_t BgL_arg2390z00_1140;

																				{	/* Expand/iarith.scm 121 */
																					obj_t BgL_arg2391z00_1141;

																					BgL_arg2391z00_1141 =
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_yz00_1104, BNIL);
																					BgL_arg2390z00_1140 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																						BgL_arg2391z00_1141);
																				}
																				BgL_arg2389z00_1139 =
																					MAKE_YOUNG_PAIR(BgL_arg2390z00_1140,
																					BNIL);
																			}
																			BgL_arg2388z00_1138 =
																				MAKE_YOUNG_PAIR(BgL_xz00_1103,
																				BgL_arg2389z00_1139);
																		}
																		BgL_arg2387z00_1137 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																			BgL_arg2388z00_1138);
																	}
																	return
																		BGL_PROCEDURE_CALL2(BgL_ez00_12,
																		BgL_arg2387z00_1137, BgL_ez00_12);
																}
															}
													}
												else
													{	/* Expand/iarith.scm 109 */
														obj_t BgL_arg2378z00_1127;
														obj_t BgL_arg2379z00_1128;

														BgL_arg2378z00_1127 =
															CAR(((obj_t) BgL_cdrzd21839zd2_1110));
														BgL_arg2379z00_1128 =
															CDR(((obj_t) BgL_cdrzd21839zd2_1110));
														{
															obj_t BgL_yz00_4832;
															obj_t BgL_xz00_4831;

															BgL_xz00_4831 = BgL_arg2378z00_1127;
															BgL_yz00_4832 = BgL_arg2379z00_1128;
															BgL_yz00_1104 = BgL_yz00_4832;
															BgL_xz00_1103 = BgL_xz00_4831;
															goto BgL_tagzd21829zd2_1105;
														}
													}
											}
									}
								else
									{	/* Expand/iarith.scm 109 */
										return BFALSE;
									}
							}
					}
				else
					{	/* Expand/iarith.scm 109 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-i* */
	obj_t BGl_z62expandzd2iza2z12zzexpand_iarithmetiquez00(obj_t BgL_envz00_3052,
		obj_t BgL_xz00_3053, obj_t BgL_ez00_3054)
	{
		{	/* Expand/iarith.scm 108 */
			return
				BGl_expandzd2iza2z70zzexpand_iarithmetiquez00(BgL_xz00_3053,
				BgL_ez00_3054);
		}

	}



/* expand-i/ */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2izf2z20zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_13, obj_t BgL_ez00_14)
	{
		{	/* Expand/iarith.scm 126 */
			{
				obj_t BgL_az00_1147;
				obj_t BgL_yz00_1148;
				obj_t BgL_az00_1144;
				obj_t BgL_yz00_1145;
				obj_t BgL_az00_1142;

				if (PAIRP(BgL_xz00_13))
					{	/* Expand/iarith.scm 127 */
						obj_t BgL_cdrzd21919zd2_1152;

						BgL_cdrzd21919zd2_1152 = CDR(((obj_t) BgL_xz00_13));
						if (PAIRP(BgL_cdrzd21919zd2_1152))
							{	/* Expand/iarith.scm 127 */
								if (NULLP(CDR(BgL_cdrzd21919zd2_1152)))
									{	/* Expand/iarith.scm 127 */
										BgL_az00_1142 = CAR(BgL_cdrzd21919zd2_1152);
										BGl_userzd2warningzd2zztools_errorz00
											(BGl_string3052z00zzexpand_iarithmetiquez00,
											BGl_string3053z00zzexpand_iarithmetiquez00, BgL_xz00_13);
										{	/* Expand/iarith.scm 132 */
											obj_t BgL_arg2411z00_1172;

											{	/* Expand/iarith.scm 132 */
												obj_t BgL_arg2412z00_1173;

												{	/* Expand/iarith.scm 132 */
													obj_t BgL_arg2414z00_1174;

													BgL_arg2414z00_1174 =
														BGL_PROCEDURE_CALL2(BgL_ez00_14, BgL_az00_1142,
														BgL_ez00_14);
													BgL_arg2412z00_1173 =
														MAKE_YOUNG_PAIR(BgL_arg2414z00_1174, BNIL);
												}
												BgL_arg2411z00_1172 =
													MAKE_YOUNG_PAIR(BINT(1L), BgL_arg2412z00_1173);
											}
											return
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BgL_arg2411z00_1172);
										}
									}
								else
									{	/* Expand/iarith.scm 127 */
										obj_t BgL_cdrzd21935zd2_1158;

										BgL_cdrzd21935zd2_1158 =
											CDR(((obj_t) BgL_cdrzd21919zd2_1152));
										if (PAIRP(BgL_cdrzd21935zd2_1158))
											{	/* Expand/iarith.scm 127 */
												if (NULLP(CDR(BgL_cdrzd21935zd2_1158)))
													{	/* Expand/iarith.scm 127 */
														obj_t BgL_arg2401z00_1162;
														obj_t BgL_arg2402z00_1163;

														BgL_arg2401z00_1162 =
															CAR(((obj_t) BgL_cdrzd21919zd2_1152));
														BgL_arg2402z00_1163 = CAR(BgL_cdrzd21935zd2_1158);
														BgL_az00_1144 = BgL_arg2401z00_1162;
														BgL_yz00_1145 = BgL_arg2402z00_1163;
														{	/* Expand/iarith.scm 135 */
															bool_t BgL_test3353z00_4865;

															if (INTEGERP(BgL_az00_1144))
																{	/* Expand/iarith.scm 135 */
																	if (INTEGERP(BgL_yz00_1145))
																		{	/* Expand/iarith.scm 135 */
																			if (((long) CINT(BgL_yz00_1145) == 0L))
																				{	/* Expand/iarith.scm 135 */
																					BgL_test3353z00_4865 = ((bool_t) 0);
																				}
																			else
																				{	/* Expand/iarith.scm 135 */
																					BgL_test3353z00_4865 = ((bool_t) 1);
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 135 */
																			BgL_test3353z00_4865 = ((bool_t) 0);
																		}
																}
															else
																{	/* Expand/iarith.scm 135 */
																	BgL_test3353z00_4865 = ((bool_t) 0);
																}
															if (BgL_test3353z00_4865)
																{	/* Expand/iarith.scm 135 */
																	{	/* Expand/iarith.scm 138 */
																		obj_t BgL_arg2419z00_1179;

																		{	/* Expand/iarith.scm 138 */
																			obj_t BgL_arg2420z00_1180;
																			obj_t BgL_arg2421z00_1181;
																			obj_t BgL_arg2423z00_1182;

																			{	/* Expand/iarith.scm 138 */

																				BgL_arg2420z00_1180 =
																					BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																					(BgL_az00_1144, BINT(10L));
																			}
																			{	/* Expand/iarith.scm 140 */

																				BgL_arg2421z00_1181 =
																					BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																					(BgL_yz00_1145, BINT(10L));
																			}
																			{	/* Expand/iarith.scm 142 */

																				BgL_arg2423z00_1182 =
																					BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																					(BINT(((long) CINT(BgL_az00_1144) /
																							(long) CINT(BgL_yz00_1145))),
																					BINT(10L));
																			}
																			{	/* Expand/iarith.scm 137 */
																				obj_t BgL_list2424z00_1183;

																				{	/* Expand/iarith.scm 137 */
																					obj_t BgL_arg2425z00_1184;

																					{	/* Expand/iarith.scm 137 */
																						obj_t BgL_arg2426z00_1185;

																						{	/* Expand/iarith.scm 137 */
																							obj_t BgL_arg2428z00_1186;

																							{	/* Expand/iarith.scm 137 */
																								obj_t BgL_arg2429z00_1187;

																								{	/* Expand/iarith.scm 137 */
																									obj_t BgL_arg2430z00_1188;

																									{	/* Expand/iarith.scm 137 */
																										obj_t BgL_arg2431z00_1189;

																										BgL_arg2431z00_1189 =
																											MAKE_YOUNG_PAIR
																											(BGl_string3054z00zzexpand_iarithmetiquez00,
																											BNIL);
																										BgL_arg2430z00_1188 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2423z00_1182,
																											BgL_arg2431z00_1189);
																									}
																									BgL_arg2429z00_1187 =
																										MAKE_YOUNG_PAIR
																										(BGl_string3055z00zzexpand_iarithmetiquez00,
																										BgL_arg2430z00_1188);
																								}
																								BgL_arg2428z00_1186 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2421z00_1181,
																									BgL_arg2429z00_1187);
																							}
																							BgL_arg2426z00_1185 =
																								MAKE_YOUNG_PAIR
																								(BGl_string3056z00zzexpand_iarithmetiquez00,
																								BgL_arg2428z00_1186);
																						}
																						BgL_arg2425z00_1184 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2420z00_1180,
																							BgL_arg2426z00_1185);
																					}
																					BgL_list2424z00_1183 =
																						MAKE_YOUNG_PAIR
																						(BGl_string3057z00zzexpand_iarithmetiquez00,
																						BgL_arg2425z00_1184);
																				}
																				BgL_arg2419z00_1179 =
																					BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																					(BgL_list2424z00_1183);
																		}}
																		BGl_userzd2warningzd2zztools_errorz00
																			(BGl_string3052z00zzexpand_iarithmetiquez00,
																			BgL_arg2419z00_1179, BgL_xz00_13);
																	}
																	return
																		BINT(
																		((long) CINT(BgL_az00_1144) /
																			(long) CINT(BgL_yz00_1145)));
																}
															else
																{	/* Expand/iarith.scm 135 */
																	BGl_userzd2warningzd2zztools_errorz00
																		(BGl_string3052z00zzexpand_iarithmetiquez00,
																		BGl_string3058z00zzexpand_iarithmetiquez00,
																		BgL_xz00_13);
																	{	/* Expand/iarith.scm 150 */
																		obj_t BgL_arg2434z00_1197;

																		{	/* Expand/iarith.scm 150 */
																			obj_t BgL_arg2435z00_1198;

																			{	/* Expand/iarith.scm 150 */
																				obj_t BgL_arg2437z00_1199;

																				BgL_arg2437z00_1199 =
																					MAKE_YOUNG_PAIR(BgL_yz00_1145, BNIL);
																				BgL_arg2435z00_1198 =
																					MAKE_YOUNG_PAIR(BgL_az00_1144,
																					BgL_arg2437z00_1199);
																			}
																			BgL_arg2434z00_1197 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																				BgL_arg2435z00_1198);
																		}
																		return
																			BGL_PROCEDURE_CALL2(BgL_ez00_14,
																			BgL_arg2434z00_1197, BgL_ez00_14);
																	}
																}
														}
													}
												else
													{	/* Expand/iarith.scm 127 */
														obj_t BgL_arg2403z00_1165;
														obj_t BgL_arg2404z00_1166;

														BgL_arg2403z00_1165 =
															CAR(((obj_t) BgL_cdrzd21919zd2_1152));
														BgL_arg2404z00_1166 =
															CDR(((obj_t) BgL_cdrzd21919zd2_1152));
														BgL_az00_1147 = BgL_arg2403z00_1165;
														BgL_yz00_1148 = BgL_arg2404z00_1166;
													BgL_tagzd21913zd2_1149:
														BGl_userzd2warningzd2zztools_errorz00
															(BGl_string3052z00zzexpand_iarithmetiquez00,
															BGl_string3059z00zzexpand_iarithmetiquez00,
															BgL_xz00_13);
														{	/* Expand/iarith.scm 155 */
															obj_t BgL_arg2438z00_1203;

															{	/* Expand/iarith.scm 155 */
																obj_t BgL_arg2439z00_1204;

																{	/* Expand/iarith.scm 155 */
																	obj_t BgL_arg2442z00_1205;

																	{	/* Expand/iarith.scm 155 */
																		obj_t BgL_arg2443z00_1206;

																		{	/* Expand/iarith.scm 155 */
																			obj_t BgL_arg2444z00_1207;

																			BgL_arg2444z00_1207 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_yz00_1148, BNIL);
																			BgL_arg2443z00_1206 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																				BgL_arg2444z00_1207);
																		}
																		BgL_arg2442z00_1205 =
																			MAKE_YOUNG_PAIR(BgL_arg2443z00_1206,
																			BNIL);
																	}
																	BgL_arg2439z00_1204 =
																		MAKE_YOUNG_PAIR(BgL_az00_1147,
																		BgL_arg2442z00_1205);
																}
																BgL_arg2438z00_1203 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																	BgL_arg2439z00_1204);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_14,
																BgL_arg2438z00_1203, BgL_ez00_14);
														}
													}
											}
										else
											{	/* Expand/iarith.scm 127 */
												obj_t BgL_arg2407z00_1169;
												obj_t BgL_arg2408z00_1170;

												BgL_arg2407z00_1169 =
													CAR(((obj_t) BgL_cdrzd21919zd2_1152));
												BgL_arg2408z00_1170 =
													CDR(((obj_t) BgL_cdrzd21919zd2_1152));
												{
													obj_t BgL_yz00_4928;
													obj_t BgL_az00_4927;

													BgL_az00_4927 = BgL_arg2407z00_1169;
													BgL_yz00_4928 = BgL_arg2408z00_1170;
													BgL_yz00_1148 = BgL_yz00_4928;
													BgL_az00_1147 = BgL_az00_4927;
													goto BgL_tagzd21913zd2_1149;
												}
											}
									}
							}
						else
							{	/* Expand/iarith.scm 127 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/iarith.scm 127 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-i/ */
	obj_t BGl_z62expandzd2izf2z42zzexpand_iarithmetiquez00(obj_t BgL_envz00_3055,
		obj_t BgL_xz00_3056, obj_t BgL_ez00_3057)
	{
		{	/* Expand/iarith.scm 126 */
			return
				BGl_expandzd2izf2z20zzexpand_iarithmetiquez00(BgL_xz00_3056,
				BgL_ez00_3057);
		}

	}



/* expand-i= */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2izd3z01zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_15, obj_t BgL_ez00_16)
	{
		{	/* Expand/iarith.scm 160 */
			{
				obj_t BgL_xz00_1211;
				obj_t BgL_yz00_1212;
				obj_t BgL_xz00_1208;
				obj_t BgL_yz00_1209;

				if (PAIRP(BgL_xz00_15))
					{	/* Expand/iarith.scm 161 */
						obj_t BgL_cdrzd21996zd2_1216;

						BgL_cdrzd21996zd2_1216 = CDR(((obj_t) BgL_xz00_15));
						if (PAIRP(BgL_cdrzd21996zd2_1216))
							{	/* Expand/iarith.scm 161 */
								obj_t BgL_cdrzd22000zd2_1218;

								BgL_cdrzd22000zd2_1218 = CDR(BgL_cdrzd21996zd2_1216);
								if (PAIRP(BgL_cdrzd22000zd2_1218))
									{	/* Expand/iarith.scm 161 */
										if (NULLP(CDR(BgL_cdrzd22000zd2_1218)))
											{	/* Expand/iarith.scm 161 */
												BgL_xz00_1208 = CAR(BgL_cdrzd21996zd2_1216);
												BgL_yz00_1209 = CAR(BgL_cdrzd22000zd2_1218);
												{	/* Expand/iarith.scm 164 */
													bool_t BgL_test3361z00_4942;

													if (INTEGERP(BgL_xz00_1208))
														{	/* Expand/iarith.scm 164 */
															BgL_test3361z00_4942 = INTEGERP(BgL_yz00_1209);
														}
													else
														{	/* Expand/iarith.scm 164 */
															BgL_test3361z00_4942 = ((bool_t) 0);
														}
													if (BgL_test3361z00_4942)
														{	/* Expand/iarith.scm 164 */
															return
																BBOOL(
																((long) CINT(BgL_xz00_1208) ==
																	(long) CINT(BgL_yz00_1209)));
														}
													else
														{	/* Expand/iarith.scm 167 */
															obj_t BgL_arg2461z00_1233;

															{	/* Expand/iarith.scm 167 */
																obj_t BgL_arg2462z00_1234;

																{	/* Expand/iarith.scm 167 */
																	obj_t BgL_arg2463z00_1235;

																	BgL_arg2463z00_1235 =
																		MAKE_YOUNG_PAIR(BgL_yz00_1209, BNIL);
																	BgL_arg2462z00_1234 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1208,
																		BgL_arg2463z00_1235);
																}
																BgL_arg2461z00_1233 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																	BgL_arg2462z00_1234);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_16,
																BgL_arg2461z00_1233, BgL_ez00_16);
														}
												}
											}
										else
											{	/* Expand/iarith.scm 161 */
												obj_t BgL_arg2453z00_1225;
												obj_t BgL_arg2455z00_1226;

												BgL_arg2453z00_1225 =
													CAR(((obj_t) BgL_cdrzd21996zd2_1216));
												BgL_arg2455z00_1226 =
													CDR(((obj_t) BgL_cdrzd21996zd2_1216));
												BgL_xz00_1211 = BgL_arg2453z00_1225;
												BgL_yz00_1212 = BgL_arg2455z00_1226;
											BgL_tagzd21989zd2_1213:
												{	/* Expand/iarith.scm 169 */
													obj_t BgL_arg2465z00_1237;

													{	/* Expand/iarith.scm 169 */
														obj_t BgL_arg2466z00_1238;

														{	/* Expand/iarith.scm 169 */
															obj_t BgL_arg2469z00_1239;
															obj_t BgL_arg2470z00_1240;

															{	/* Expand/iarith.scm 169 */
																obj_t BgL_arg2471z00_1241;

																{	/* Expand/iarith.scm 169 */
																	obj_t BgL_arg2473z00_1242;

																	{	/* Expand/iarith.scm 169 */
																		obj_t BgL_arg2474z00_1243;

																		BgL_arg2474z00_1243 =
																			CAR(((obj_t) BgL_yz00_1212));
																		BgL_arg2473z00_1242 =
																			MAKE_YOUNG_PAIR(BgL_arg2474z00_1243,
																			BNIL);
																	}
																	BgL_arg2471z00_1241 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1211,
																		BgL_arg2473z00_1242);
																}
																BgL_arg2469z00_1239 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																	BgL_arg2471z00_1241);
															}
															{	/* Expand/iarith.scm 169 */
																obj_t BgL_arg2475z00_1244;

																{	/* Expand/iarith.scm 169 */
																	obj_t BgL_arg2476z00_1245;

																	BgL_arg2476z00_1245 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_1212, BNIL);
																	BgL_arg2475z00_1244 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																		BgL_arg2476z00_1245);
																}
																BgL_arg2470z00_1240 =
																	MAKE_YOUNG_PAIR(BgL_arg2475z00_1244, BNIL);
															}
															BgL_arg2466z00_1238 =
																MAKE_YOUNG_PAIR(BgL_arg2469z00_1239,
																BgL_arg2470z00_1240);
														}
														BgL_arg2465z00_1237 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
															BgL_arg2466z00_1238);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_16,
														BgL_arg2465z00_1237, BgL_ez00_16);
												}
											}
									}
								else
									{	/* Expand/iarith.scm 161 */
										obj_t BgL_arg2457z00_1229;
										obj_t BgL_arg2458z00_1230;

										BgL_arg2457z00_1229 = CAR(((obj_t) BgL_cdrzd21996zd2_1216));
										BgL_arg2458z00_1230 = CDR(((obj_t) BgL_cdrzd21996zd2_1216));
										{
											obj_t BgL_yz00_4988;
											obj_t BgL_xz00_4987;

											BgL_xz00_4987 = BgL_arg2457z00_1229;
											BgL_yz00_4988 = BgL_arg2458z00_1230;
											BgL_yz00_1212 = BgL_yz00_4988;
											BgL_xz00_1211 = BgL_xz00_4987;
											goto BgL_tagzd21989zd2_1213;
										}
									}
							}
						else
							{	/* Expand/iarith.scm 161 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/iarith.scm 161 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-i= */
	obj_t BGl_z62expandzd2izd3z63zzexpand_iarithmetiquez00(obj_t BgL_envz00_3058,
		obj_t BgL_xz00_3059, obj_t BgL_ez00_3060)
	{
		{	/* Expand/iarith.scm 160 */
			return
				BGl_expandzd2izd3z01zzexpand_iarithmetiquez00(BgL_xz00_3059,
				BgL_ez00_3060);
		}

	}



/* expand-i< */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2izc3z11zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_17, obj_t BgL_ez00_18)
	{
		{	/* Expand/iarith.scm 174 */
			{
				obj_t BgL_xz00_1249;
				obj_t BgL_yz00_1250;
				obj_t BgL_xz00_1246;
				obj_t BgL_yz00_1247;

				if (PAIRP(BgL_xz00_17))
					{	/* Expand/iarith.scm 175 */
						obj_t BgL_cdrzd22052zd2_1254;

						BgL_cdrzd22052zd2_1254 = CDR(((obj_t) BgL_xz00_17));
						if (PAIRP(BgL_cdrzd22052zd2_1254))
							{	/* Expand/iarith.scm 175 */
								obj_t BgL_cdrzd22056zd2_1256;

								BgL_cdrzd22056zd2_1256 = CDR(BgL_cdrzd22052zd2_1254);
								if (PAIRP(BgL_cdrzd22056zd2_1256))
									{	/* Expand/iarith.scm 175 */
										if (NULLP(CDR(BgL_cdrzd22056zd2_1256)))
											{	/* Expand/iarith.scm 175 */
												BgL_xz00_1246 = CAR(BgL_cdrzd22052zd2_1254);
												BgL_yz00_1247 = CAR(BgL_cdrzd22056zd2_1256);
												{	/* Expand/iarith.scm 178 */
													bool_t BgL_test3367z00_5002;

													if (INTEGERP(BgL_xz00_1246))
														{	/* Expand/iarith.scm 178 */
															BgL_test3367z00_5002 = INTEGERP(BgL_yz00_1247);
														}
													else
														{	/* Expand/iarith.scm 178 */
															BgL_test3367z00_5002 = ((bool_t) 0);
														}
													if (BgL_test3367z00_5002)
														{	/* Expand/iarith.scm 178 */
															return
																BBOOL(
																((long) CINT(BgL_xz00_1246) <
																	(long) CINT(BgL_yz00_1247)));
														}
													else
														{	/* Expand/iarith.scm 181 */
															obj_t BgL_arg2493z00_1271;

															{	/* Expand/iarith.scm 181 */
																obj_t BgL_arg2495z00_1272;

																{	/* Expand/iarith.scm 181 */
																	obj_t BgL_arg2497z00_1273;

																	BgL_arg2497z00_1273 =
																		MAKE_YOUNG_PAIR(BgL_yz00_1247, BNIL);
																	BgL_arg2495z00_1272 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1246,
																		BgL_arg2497z00_1273);
																}
																BgL_arg2493z00_1271 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																	BgL_arg2495z00_1272);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_18,
																BgL_arg2493z00_1271, BgL_ez00_18);
														}
												}
											}
										else
											{	/* Expand/iarith.scm 175 */
												obj_t BgL_arg2484z00_1263;
												obj_t BgL_arg2486z00_1264;

												BgL_arg2484z00_1263 =
													CAR(((obj_t) BgL_cdrzd22052zd2_1254));
												BgL_arg2486z00_1264 =
													CDR(((obj_t) BgL_cdrzd22052zd2_1254));
												BgL_xz00_1249 = BgL_arg2484z00_1263;
												BgL_yz00_1250 = BgL_arg2486z00_1264;
											BgL_tagzd22045zd2_1251:
												{	/* Expand/iarith.scm 183 */
													obj_t BgL_arg2500z00_1275;

													{	/* Expand/iarith.scm 183 */
														obj_t BgL_arg2501z00_1276;

														{	/* Expand/iarith.scm 183 */
															obj_t BgL_arg2502z00_1277;
															obj_t BgL_arg2503z00_1278;

															{	/* Expand/iarith.scm 183 */
																obj_t BgL_arg2505z00_1279;

																{	/* Expand/iarith.scm 183 */
																	obj_t BgL_arg2506z00_1280;

																	{	/* Expand/iarith.scm 183 */
																		obj_t BgL_arg2508z00_1281;

																		BgL_arg2508z00_1281 =
																			CAR(((obj_t) BgL_yz00_1250));
																		BgL_arg2506z00_1280 =
																			MAKE_YOUNG_PAIR(BgL_arg2508z00_1281,
																			BNIL);
																	}
																	BgL_arg2505z00_1279 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1249,
																		BgL_arg2506z00_1280);
																}
																BgL_arg2502z00_1277 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																	BgL_arg2505z00_1279);
															}
															{	/* Expand/iarith.scm 183 */
																obj_t BgL_arg2509z00_1282;

																{	/* Expand/iarith.scm 183 */
																	obj_t BgL_arg2510z00_1283;

																	BgL_arg2510z00_1283 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_1250, BNIL);
																	BgL_arg2509z00_1282 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																		BgL_arg2510z00_1283);
																}
																BgL_arg2503z00_1278 =
																	MAKE_YOUNG_PAIR(BgL_arg2509z00_1282, BNIL);
															}
															BgL_arg2501z00_1276 =
																MAKE_YOUNG_PAIR(BgL_arg2502z00_1277,
																BgL_arg2503z00_1278);
														}
														BgL_arg2500z00_1275 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
															BgL_arg2501z00_1276);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_18,
														BgL_arg2500z00_1275, BgL_ez00_18);
												}
											}
									}
								else
									{	/* Expand/iarith.scm 175 */
										obj_t BgL_arg2488z00_1267;
										obj_t BgL_arg2490z00_1268;

										BgL_arg2488z00_1267 = CAR(((obj_t) BgL_cdrzd22052zd2_1254));
										BgL_arg2490z00_1268 = CDR(((obj_t) BgL_cdrzd22052zd2_1254));
										{
											obj_t BgL_yz00_5048;
											obj_t BgL_xz00_5047;

											BgL_xz00_5047 = BgL_arg2488z00_1267;
											BgL_yz00_5048 = BgL_arg2490z00_1268;
											BgL_yz00_1250 = BgL_yz00_5048;
											BgL_xz00_1249 = BgL_xz00_5047;
											goto BgL_tagzd22045zd2_1251;
										}
									}
							}
						else
							{	/* Expand/iarith.scm 175 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/iarith.scm 175 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-i< */
	obj_t BGl_z62expandzd2izc3z73zzexpand_iarithmetiquez00(obj_t BgL_envz00_3061,
		obj_t BgL_xz00_3062, obj_t BgL_ez00_3063)
	{
		{	/* Expand/iarith.scm 174 */
			return
				BGl_expandzd2izc3z11zzexpand_iarithmetiquez00(BgL_xz00_3062,
				BgL_ez00_3063);
		}

	}



/* expand-i> */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2ize3z31zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_19, obj_t BgL_ez00_20)
	{
		{	/* Expand/iarith.scm 188 */
			{
				obj_t BgL_xz00_1287;
				obj_t BgL_yz00_1288;
				obj_t BgL_xz00_1284;
				obj_t BgL_yz00_1285;

				if (PAIRP(BgL_xz00_19))
					{	/* Expand/iarith.scm 189 */
						obj_t BgL_cdrzd22108zd2_1292;

						BgL_cdrzd22108zd2_1292 = CDR(((obj_t) BgL_xz00_19));
						if (PAIRP(BgL_cdrzd22108zd2_1292))
							{	/* Expand/iarith.scm 189 */
								obj_t BgL_cdrzd22112zd2_1294;

								BgL_cdrzd22112zd2_1294 = CDR(BgL_cdrzd22108zd2_1292);
								if (PAIRP(BgL_cdrzd22112zd2_1294))
									{	/* Expand/iarith.scm 189 */
										if (NULLP(CDR(BgL_cdrzd22112zd2_1294)))
											{	/* Expand/iarith.scm 189 */
												BgL_xz00_1284 = CAR(BgL_cdrzd22108zd2_1292);
												BgL_yz00_1285 = CAR(BgL_cdrzd22112zd2_1294);
												{	/* Expand/iarith.scm 192 */
													bool_t BgL_test3373z00_5062;

													if (INTEGERP(BgL_xz00_1284))
														{	/* Expand/iarith.scm 192 */
															BgL_test3373z00_5062 = INTEGERP(BgL_yz00_1285);
														}
													else
														{	/* Expand/iarith.scm 192 */
															BgL_test3373z00_5062 = ((bool_t) 0);
														}
													if (BgL_test3373z00_5062)
														{	/* Expand/iarith.scm 192 */
															return
																BBOOL(
																((long) CINT(BgL_xz00_1284) >
																	(long) CINT(BgL_yz00_1285)));
														}
													else
														{	/* Expand/iarith.scm 195 */
															obj_t BgL_arg2529z00_1309;

															{	/* Expand/iarith.scm 195 */
																obj_t BgL_arg2534z00_1310;

																{	/* Expand/iarith.scm 195 */
																	obj_t BgL_arg2536z00_1311;

																	BgL_arg2536z00_1311 =
																		MAKE_YOUNG_PAIR(BgL_yz00_1285, BNIL);
																	BgL_arg2534z00_1310 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1284,
																		BgL_arg2536z00_1311);
																}
																BgL_arg2529z00_1309 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																	BgL_arg2534z00_1310);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_20,
																BgL_arg2529z00_1309, BgL_ez00_20);
														}
												}
											}
										else
											{	/* Expand/iarith.scm 189 */
												obj_t BgL_arg2519z00_1301;
												obj_t BgL_arg2521z00_1302;

												BgL_arg2519z00_1301 =
													CAR(((obj_t) BgL_cdrzd22108zd2_1292));
												BgL_arg2521z00_1302 =
													CDR(((obj_t) BgL_cdrzd22108zd2_1292));
												BgL_xz00_1287 = BgL_arg2519z00_1301;
												BgL_yz00_1288 = BgL_arg2521z00_1302;
											BgL_tagzd22101zd2_1289:
												{	/* Expand/iarith.scm 197 */
													obj_t BgL_arg2537z00_1313;

													{	/* Expand/iarith.scm 197 */
														obj_t BgL_arg2538z00_1314;

														{	/* Expand/iarith.scm 197 */
															obj_t BgL_arg2539z00_1315;
															obj_t BgL_arg2540z00_1316;

															{	/* Expand/iarith.scm 197 */
																obj_t BgL_arg2542z00_1317;

																{	/* Expand/iarith.scm 197 */
																	obj_t BgL_arg2543z00_1318;

																	{	/* Expand/iarith.scm 197 */
																		obj_t BgL_arg2545z00_1319;

																		BgL_arg2545z00_1319 =
																			CAR(((obj_t) BgL_yz00_1288));
																		BgL_arg2543z00_1318 =
																			MAKE_YOUNG_PAIR(BgL_arg2545z00_1319,
																			BNIL);
																	}
																	BgL_arg2542z00_1317 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1287,
																		BgL_arg2543z00_1318);
																}
																BgL_arg2539z00_1315 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																	BgL_arg2542z00_1317);
															}
															{	/* Expand/iarith.scm 197 */
																obj_t BgL_arg2546z00_1320;

																{	/* Expand/iarith.scm 197 */
																	obj_t BgL_arg2548z00_1321;

																	BgL_arg2548z00_1321 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_1288, BNIL);
																	BgL_arg2546z00_1320 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																		BgL_arg2548z00_1321);
																}
																BgL_arg2540z00_1316 =
																	MAKE_YOUNG_PAIR(BgL_arg2546z00_1320, BNIL);
															}
															BgL_arg2538z00_1314 =
																MAKE_YOUNG_PAIR(BgL_arg2539z00_1315,
																BgL_arg2540z00_1316);
														}
														BgL_arg2537z00_1313 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
															BgL_arg2538z00_1314);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_20,
														BgL_arg2537z00_1313, BgL_ez00_20);
												}
											}
									}
								else
									{	/* Expand/iarith.scm 189 */
										obj_t BgL_arg2525z00_1305;
										obj_t BgL_arg2526z00_1306;

										BgL_arg2525z00_1305 = CAR(((obj_t) BgL_cdrzd22108zd2_1292));
										BgL_arg2526z00_1306 = CDR(((obj_t) BgL_cdrzd22108zd2_1292));
										{
											obj_t BgL_yz00_5108;
											obj_t BgL_xz00_5107;

											BgL_xz00_5107 = BgL_arg2525z00_1305;
											BgL_yz00_5108 = BgL_arg2526z00_1306;
											BgL_yz00_1288 = BgL_yz00_5108;
											BgL_xz00_1287 = BgL_xz00_5107;
											goto BgL_tagzd22101zd2_1289;
										}
									}
							}
						else
							{	/* Expand/iarith.scm 189 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/iarith.scm 189 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-i> */
	obj_t BGl_z62expandzd2ize3z53zzexpand_iarithmetiquez00(obj_t BgL_envz00_3064,
		obj_t BgL_xz00_3065, obj_t BgL_ez00_3066)
	{
		{	/* Expand/iarith.scm 188 */
			return
				BGl_expandzd2ize3z31zzexpand_iarithmetiquez00(BgL_xz00_3065,
				BgL_ez00_3066);
		}

	}



/* expand-i<= */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2izc3zd3zc2zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_21, obj_t BgL_ez00_22)
	{
		{	/* Expand/iarith.scm 202 */
			{
				obj_t BgL_xz00_1325;
				obj_t BgL_yz00_1326;
				obj_t BgL_xz00_1322;
				obj_t BgL_yz00_1323;

				if (PAIRP(BgL_xz00_21))
					{	/* Expand/iarith.scm 203 */
						obj_t BgL_cdrzd22164zd2_1330;

						BgL_cdrzd22164zd2_1330 = CDR(((obj_t) BgL_xz00_21));
						if (PAIRP(BgL_cdrzd22164zd2_1330))
							{	/* Expand/iarith.scm 203 */
								obj_t BgL_cdrzd22168zd2_1332;

								BgL_cdrzd22168zd2_1332 = CDR(BgL_cdrzd22164zd2_1330);
								if (PAIRP(BgL_cdrzd22168zd2_1332))
									{	/* Expand/iarith.scm 203 */
										if (NULLP(CDR(BgL_cdrzd22168zd2_1332)))
											{	/* Expand/iarith.scm 203 */
												BgL_xz00_1322 = CAR(BgL_cdrzd22164zd2_1330);
												BgL_yz00_1323 = CAR(BgL_cdrzd22168zd2_1332);
												{	/* Expand/iarith.scm 206 */
													bool_t BgL_test3379z00_5122;

													if (INTEGERP(BgL_xz00_1322))
														{	/* Expand/iarith.scm 206 */
															BgL_test3379z00_5122 = INTEGERP(BgL_yz00_1323);
														}
													else
														{	/* Expand/iarith.scm 206 */
															BgL_test3379z00_5122 = ((bool_t) 0);
														}
													if (BgL_test3379z00_5122)
														{	/* Expand/iarith.scm 206 */
															return
																BBOOL(
																((long) CINT(BgL_xz00_1322) <=
																	(long) CINT(BgL_yz00_1323)));
														}
													else
														{	/* Expand/iarith.scm 209 */
															obj_t BgL_arg2565z00_1347;

															{	/* Expand/iarith.scm 209 */
																obj_t BgL_arg2566z00_1348;

																{	/* Expand/iarith.scm 209 */
																	obj_t BgL_arg2567z00_1349;

																	BgL_arg2567z00_1349 =
																		MAKE_YOUNG_PAIR(BgL_yz00_1323, BNIL);
																	BgL_arg2566z00_1348 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1322,
																		BgL_arg2567z00_1349);
																}
																BgL_arg2565z00_1347 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																	BgL_arg2566z00_1348);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_22,
																BgL_arg2565z00_1347, BgL_ez00_22);
														}
												}
											}
										else
											{	/* Expand/iarith.scm 203 */
												obj_t BgL_arg2556z00_1339;
												obj_t BgL_arg2557z00_1340;

												BgL_arg2556z00_1339 =
													CAR(((obj_t) BgL_cdrzd22164zd2_1330));
												BgL_arg2557z00_1340 =
													CDR(((obj_t) BgL_cdrzd22164zd2_1330));
												BgL_xz00_1325 = BgL_arg2556z00_1339;
												BgL_yz00_1326 = BgL_arg2557z00_1340;
											BgL_tagzd22157zd2_1327:
												{	/* Expand/iarith.scm 211 */
													obj_t BgL_arg2568z00_1351;

													{	/* Expand/iarith.scm 211 */
														obj_t BgL_arg2572z00_1352;

														{	/* Expand/iarith.scm 211 */
															obj_t BgL_arg2578z00_1353;
															obj_t BgL_arg2579z00_1354;

															{	/* Expand/iarith.scm 211 */
																obj_t BgL_arg2584z00_1355;

																{	/* Expand/iarith.scm 211 */
																	obj_t BgL_arg2585z00_1356;

																	{	/* Expand/iarith.scm 211 */
																		obj_t BgL_arg2586z00_1357;

																		BgL_arg2586z00_1357 =
																			CAR(((obj_t) BgL_yz00_1326));
																		BgL_arg2585z00_1356 =
																			MAKE_YOUNG_PAIR(BgL_arg2586z00_1357,
																			BNIL);
																	}
																	BgL_arg2584z00_1355 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1325,
																		BgL_arg2585z00_1356);
																}
																BgL_arg2578z00_1353 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																	BgL_arg2584z00_1355);
															}
															{	/* Expand/iarith.scm 211 */
																obj_t BgL_arg2587z00_1358;

																{	/* Expand/iarith.scm 211 */
																	obj_t BgL_arg2589z00_1359;

																	BgL_arg2589z00_1359 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_1326, BNIL);
																	BgL_arg2587z00_1358 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(17),
																		BgL_arg2589z00_1359);
																}
																BgL_arg2579z00_1354 =
																	MAKE_YOUNG_PAIR(BgL_arg2587z00_1358, BNIL);
															}
															BgL_arg2572z00_1352 =
																MAKE_YOUNG_PAIR(BgL_arg2578z00_1353,
																BgL_arg2579z00_1354);
														}
														BgL_arg2568z00_1351 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
															BgL_arg2572z00_1352);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_22,
														BgL_arg2568z00_1351, BgL_ez00_22);
												}
											}
									}
								else
									{	/* Expand/iarith.scm 203 */
										obj_t BgL_arg2561z00_1343;
										obj_t BgL_arg2562z00_1344;

										BgL_arg2561z00_1343 = CAR(((obj_t) BgL_cdrzd22164zd2_1330));
										BgL_arg2562z00_1344 = CDR(((obj_t) BgL_cdrzd22164zd2_1330));
										{
											obj_t BgL_yz00_5168;
											obj_t BgL_xz00_5167;

											BgL_xz00_5167 = BgL_arg2561z00_1343;
											BgL_yz00_5168 = BgL_arg2562z00_1344;
											BgL_yz00_1326 = BgL_yz00_5168;
											BgL_xz00_1325 = BgL_xz00_5167;
											goto BgL_tagzd22157zd2_1327;
										}
									}
							}
						else
							{	/* Expand/iarith.scm 203 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/iarith.scm 203 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-i<= */
	obj_t BGl_z62expandzd2izc3zd3za0zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3067, obj_t BgL_xz00_3068, obj_t BgL_ez00_3069)
	{
		{	/* Expand/iarith.scm 202 */
			return
				BGl_expandzd2izc3zd3zc2zzexpand_iarithmetiquez00(BgL_xz00_3068,
				BgL_ez00_3069);
		}

	}



/* expand-i>= */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2ize3zd3ze2zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_23, obj_t BgL_ez00_24)
	{
		{	/* Expand/iarith.scm 216 */
			{
				obj_t BgL_xz00_1363;
				obj_t BgL_yz00_1364;
				obj_t BgL_xz00_1360;
				obj_t BgL_yz00_1361;

				if (PAIRP(BgL_xz00_23))
					{	/* Expand/iarith.scm 217 */
						obj_t BgL_cdrzd22220zd2_1368;

						BgL_cdrzd22220zd2_1368 = CDR(((obj_t) BgL_xz00_23));
						if (PAIRP(BgL_cdrzd22220zd2_1368))
							{	/* Expand/iarith.scm 217 */
								obj_t BgL_cdrzd22224zd2_1370;

								BgL_cdrzd22224zd2_1370 = CDR(BgL_cdrzd22220zd2_1368);
								if (PAIRP(BgL_cdrzd22224zd2_1370))
									{	/* Expand/iarith.scm 217 */
										if (NULLP(CDR(BgL_cdrzd22224zd2_1370)))
											{	/* Expand/iarith.scm 217 */
												BgL_xz00_1360 = CAR(BgL_cdrzd22220zd2_1368);
												BgL_yz00_1361 = CAR(BgL_cdrzd22224zd2_1370);
												{	/* Expand/iarith.scm 220 */
													bool_t BgL_test3385z00_5182;

													if (INTEGERP(BgL_xz00_1360))
														{	/* Expand/iarith.scm 220 */
															BgL_test3385z00_5182 = INTEGERP(BgL_yz00_1361);
														}
													else
														{	/* Expand/iarith.scm 220 */
															BgL_test3385z00_5182 = ((bool_t) 0);
														}
													if (BgL_test3385z00_5182)
														{	/* Expand/iarith.scm 220 */
															return
																BBOOL(
																((long) CINT(BgL_xz00_1360) >=
																	(long) CINT(BgL_yz00_1361)));
														}
													else
														{	/* Expand/iarith.scm 223 */
															obj_t BgL_arg2608z00_1385;

															{	/* Expand/iarith.scm 223 */
																obj_t BgL_arg2609z00_1386;

																{	/* Expand/iarith.scm 223 */
																	obj_t BgL_arg2610z00_1387;

																	BgL_arg2610z00_1387 =
																		MAKE_YOUNG_PAIR(BgL_yz00_1361, BNIL);
																	BgL_arg2609z00_1386 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1360,
																		BgL_arg2610z00_1387);
																}
																BgL_arg2608z00_1385 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																	BgL_arg2609z00_1386);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_24,
																BgL_arg2608z00_1385, BgL_ez00_24);
														}
												}
											}
										else
											{	/* Expand/iarith.scm 217 */
												obj_t BgL_arg2599z00_1377;
												obj_t BgL_arg2600z00_1378;

												BgL_arg2599z00_1377 =
													CAR(((obj_t) BgL_cdrzd22220zd2_1368));
												BgL_arg2600z00_1378 =
													CDR(((obj_t) BgL_cdrzd22220zd2_1368));
												BgL_xz00_1363 = BgL_arg2599z00_1377;
												BgL_yz00_1364 = BgL_arg2600z00_1378;
											BgL_tagzd22213zd2_1365:
												{	/* Expand/iarith.scm 225 */
													obj_t BgL_arg2611z00_1389;

													{	/* Expand/iarith.scm 225 */
														obj_t BgL_arg2612z00_1390;

														{	/* Expand/iarith.scm 225 */
															obj_t BgL_arg2613z00_1391;
															obj_t BgL_arg2614z00_1392;

															{	/* Expand/iarith.scm 225 */
																obj_t BgL_arg2615z00_1393;

																{	/* Expand/iarith.scm 225 */
																	obj_t BgL_arg2617z00_1394;

																	{	/* Expand/iarith.scm 225 */
																		obj_t BgL_arg2618z00_1395;

																		BgL_arg2618z00_1395 =
																			CAR(((obj_t) BgL_yz00_1364));
																		BgL_arg2617z00_1394 =
																			MAKE_YOUNG_PAIR(BgL_arg2618z00_1395,
																			BNIL);
																	}
																	BgL_arg2615z00_1393 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1363,
																		BgL_arg2617z00_1394);
																}
																BgL_arg2613z00_1391 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																	BgL_arg2615z00_1393);
															}
															{	/* Expand/iarith.scm 225 */
																obj_t BgL_arg2619z00_1396;

																{	/* Expand/iarith.scm 225 */
																	obj_t BgL_arg2620z00_1397;

																	BgL_arg2620z00_1397 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_1364, BNIL);
																	BgL_arg2619z00_1396 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																		BgL_arg2620z00_1397);
																}
																BgL_arg2614z00_1392 =
																	MAKE_YOUNG_PAIR(BgL_arg2619z00_1396, BNIL);
															}
															BgL_arg2612z00_1390 =
																MAKE_YOUNG_PAIR(BgL_arg2613z00_1391,
																BgL_arg2614z00_1392);
														}
														BgL_arg2611z00_1389 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
															BgL_arg2612z00_1390);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_24,
														BgL_arg2611z00_1389, BgL_ez00_24);
												}
											}
									}
								else
									{	/* Expand/iarith.scm 217 */
										obj_t BgL_arg2604z00_1381;
										obj_t BgL_arg2605z00_1382;

										BgL_arg2604z00_1381 = CAR(((obj_t) BgL_cdrzd22220zd2_1368));
										BgL_arg2605z00_1382 = CDR(((obj_t) BgL_cdrzd22220zd2_1368));
										{
											obj_t BgL_yz00_5228;
											obj_t BgL_xz00_5227;

											BgL_xz00_5227 = BgL_arg2604z00_1381;
											BgL_yz00_5228 = BgL_arg2605z00_1382;
											BgL_yz00_1364 = BgL_yz00_5228;
											BgL_xz00_1363 = BgL_xz00_5227;
											goto BgL_tagzd22213zd2_1365;
										}
									}
							}
						else
							{	/* Expand/iarith.scm 217 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/iarith.scm 217 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-i>= */
	obj_t BGl_z62expandzd2ize3zd3z80zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3070, obj_t BgL_xz00_3071, obj_t BgL_ez00_3072)
	{
		{	/* Expand/iarith.scm 216 */
			return
				BGl_expandzd2ize3zd3ze2zzexpand_iarithmetiquez00(BgL_xz00_3071,
				BgL_ez00_3072);
		}

	}



/* expand-+fx */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2zb2fxz60zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_25, obj_t BgL_ez00_26)
	{
		{	/* Expand/iarith.scm 230 */
			{
				obj_t BgL_az00_1398;
				obj_t BgL_bz00_1399;

				if (PAIRP(BgL_xz00_25))
					{	/* Expand/iarith.scm 231 */
						obj_t BgL_cdrzd22276zd2_1404;

						BgL_cdrzd22276zd2_1404 = CDR(((obj_t) BgL_xz00_25));
						if (PAIRP(BgL_cdrzd22276zd2_1404))
							{	/* Expand/iarith.scm 231 */
								obj_t BgL_cdrzd22280zd2_1406;

								BgL_cdrzd22280zd2_1406 = CDR(BgL_cdrzd22276zd2_1404);
								if (PAIRP(BgL_cdrzd22280zd2_1406))
									{	/* Expand/iarith.scm 231 */
										if (NULLP(CDR(BgL_cdrzd22280zd2_1406)))
											{	/* Expand/iarith.scm 231 */
												BgL_az00_1398 = CAR(BgL_cdrzd22276zd2_1404);
												BgL_bz00_1399 = CAR(BgL_cdrzd22280zd2_1406);
												{	/* Expand/iarith.scm 234 */
													bool_t BgL_test3391z00_5242;

													if (INTEGERP(BgL_az00_1398))
														{	/* Expand/iarith.scm 234 */
															BgL_test3391z00_5242 = INTEGERP(BgL_bz00_1399);
														}
													else
														{	/* Expand/iarith.scm 234 */
															BgL_test3391z00_5242 = ((bool_t) 0);
														}
													if (BgL_test3391z00_5242)
														{	/* Expand/iarith.scm 234 */
															return ADDFX(BgL_az00_1398, BgL_bz00_1399);
														}
													else
														{	/* Expand/iarith.scm 234 */
															{	/* Expand/iarith.scm 237 */
																obj_t BgL_arg2631z00_1415;
																obj_t BgL_arg2632z00_1416;

																BgL_arg2631z00_1415 =
																	CDR(((obj_t) BgL_xz00_25));
																BgL_arg2632z00_1416 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_26,
																	BgL_az00_1398, BgL_ez00_26);
																{	/* Expand/iarith.scm 237 */
																	obj_t BgL_tmpz00_5254;

																	BgL_tmpz00_5254 =
																		((obj_t) BgL_arg2631z00_1415);
																	SET_CAR(BgL_tmpz00_5254, BgL_arg2632z00_1416);
																}
															}
															{	/* Expand/iarith.scm 238 */
																obj_t BgL_arg2633z00_1417;
																obj_t BgL_arg2635z00_1418;

																{	/* Expand/iarith.scm 238 */
																	obj_t BgL_pairz00_2765;

																	BgL_pairz00_2765 = CDR(((obj_t) BgL_xz00_25));
																	BgL_arg2633z00_1417 = CDR(BgL_pairz00_2765);
																}
																BgL_arg2635z00_1418 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_26,
																	BgL_bz00_1399, BgL_ez00_26);
																{	/* Expand/iarith.scm 238 */
																	obj_t BgL_tmpz00_5265;

																	BgL_tmpz00_5265 =
																		((obj_t) BgL_arg2633z00_1417);
																	SET_CAR(BgL_tmpz00_5265, BgL_arg2635z00_1418);
																}
															}
															return BgL_xz00_25;
														}
												}
											}
										else
											{	/* Expand/iarith.scm 231 */
											BgL_tagzd22269zd2_1401:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string3060z00zzexpand_iarithmetiquez00,
													BgL_xz00_25);
											}
									}
								else
									{	/* Expand/iarith.scm 231 */
										goto BgL_tagzd22269zd2_1401;
									}
							}
						else
							{	/* Expand/iarith.scm 231 */
								goto BgL_tagzd22269zd2_1401;
							}
					}
				else
					{	/* Expand/iarith.scm 231 */
						goto BgL_tagzd22269zd2_1401;
					}
			}
		}

	}



/* &expand-+fx */
	obj_t BGl_z62expandzd2zb2fxz02zzexpand_iarithmetiquez00(obj_t BgL_envz00_3073,
		obj_t BgL_xz00_3074, obj_t BgL_ez00_3075)
	{
		{	/* Expand/iarith.scm 230 */
			return
				BGl_expandzd2zb2fxz60zzexpand_iarithmetiquez00(BgL_xz00_3074,
				BgL_ez00_3075);
		}

	}



/* expand--fx */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2zd2fxz00zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_27, obj_t BgL_ez00_28)
	{
		{	/* Expand/iarith.scm 246 */
			{
				obj_t BgL_az00_1420;
				obj_t BgL_bz00_1421;

				if (PAIRP(BgL_xz00_27))
					{	/* Expand/iarith.scm 247 */
						obj_t BgL_cdrzd22294zd2_1426;

						BgL_cdrzd22294zd2_1426 = CDR(((obj_t) BgL_xz00_27));
						if (PAIRP(BgL_cdrzd22294zd2_1426))
							{	/* Expand/iarith.scm 247 */
								obj_t BgL_cdrzd22298zd2_1428;

								BgL_cdrzd22298zd2_1428 = CDR(BgL_cdrzd22294zd2_1426);
								if (PAIRP(BgL_cdrzd22298zd2_1428))
									{	/* Expand/iarith.scm 247 */
										if (NULLP(CDR(BgL_cdrzd22298zd2_1428)))
											{	/* Expand/iarith.scm 247 */
												BgL_az00_1420 = CAR(BgL_cdrzd22294zd2_1426);
												BgL_bz00_1421 = CAR(BgL_cdrzd22298zd2_1428);
												{	/* Expand/iarith.scm 250 */
													bool_t BgL_test3397z00_5284;

													if (INTEGERP(BgL_az00_1420))
														{	/* Expand/iarith.scm 250 */
															BgL_test3397z00_5284 = INTEGERP(BgL_bz00_1421);
														}
													else
														{	/* Expand/iarith.scm 250 */
															BgL_test3397z00_5284 = ((bool_t) 0);
														}
													if (BgL_test3397z00_5284)
														{	/* Expand/iarith.scm 250 */
															return SUBFX(BgL_az00_1420, BgL_bz00_1421);
														}
													else
														{	/* Expand/iarith.scm 250 */
															{	/* Expand/iarith.scm 253 */
																obj_t BgL_arg2647z00_1437;
																obj_t BgL_arg2648z00_1438;

																BgL_arg2647z00_1437 =
																	CDR(((obj_t) BgL_xz00_27));
																BgL_arg2648z00_1438 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_28,
																	BgL_az00_1420, BgL_ez00_28);
																{	/* Expand/iarith.scm 253 */
																	obj_t BgL_tmpz00_5296;

																	BgL_tmpz00_5296 =
																		((obj_t) BgL_arg2647z00_1437);
																	SET_CAR(BgL_tmpz00_5296, BgL_arg2648z00_1438);
																}
															}
															{	/* Expand/iarith.scm 254 */
																obj_t BgL_arg2649z00_1439;
																obj_t BgL_arg2650z00_1440;

																{	/* Expand/iarith.scm 254 */
																	obj_t BgL_pairz00_2779;

																	BgL_pairz00_2779 = CDR(((obj_t) BgL_xz00_27));
																	BgL_arg2649z00_1439 = CDR(BgL_pairz00_2779);
																}
																BgL_arg2650z00_1440 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_28,
																	BgL_bz00_1421, BgL_ez00_28);
																{	/* Expand/iarith.scm 254 */
																	obj_t BgL_tmpz00_5307;

																	BgL_tmpz00_5307 =
																		((obj_t) BgL_arg2649z00_1439);
																	SET_CAR(BgL_tmpz00_5307, BgL_arg2650z00_1440);
																}
															}
															return BgL_xz00_27;
														}
												}
											}
										else
											{	/* Expand/iarith.scm 247 */
											BgL_tagzd22287zd2_1423:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string3061z00zzexpand_iarithmetiquez00,
													BgL_xz00_27);
											}
									}
								else
									{	/* Expand/iarith.scm 247 */
										goto BgL_tagzd22287zd2_1423;
									}
							}
						else
							{	/* Expand/iarith.scm 247 */
								goto BgL_tagzd22287zd2_1423;
							}
					}
				else
					{	/* Expand/iarith.scm 247 */
						goto BgL_tagzd22287zd2_1423;
					}
			}
		}

	}



/* &expand--fx */
	obj_t BGl_z62expandzd2zd2fxz62zzexpand_iarithmetiquez00(obj_t BgL_envz00_3076,
		obj_t BgL_xz00_3077, obj_t BgL_ez00_3078)
	{
		{	/* Expand/iarith.scm 246 */
			return
				BGl_expandzd2zd2fxz00zzexpand_iarithmetiquez00(BgL_xz00_3077,
				BgL_ez00_3078);
		}

	}



/* expand-*fx */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2za2fxz70zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_29, obj_t BgL_ez00_30)
	{
		{	/* Expand/iarith.scm 262 */
			{
				obj_t BgL_az00_1442;
				obj_t BgL_bz00_1443;

				if (PAIRP(BgL_xz00_29))
					{	/* Expand/iarith.scm 263 */
						obj_t BgL_cdrzd22312zd2_1448;

						BgL_cdrzd22312zd2_1448 = CDR(((obj_t) BgL_xz00_29));
						if (PAIRP(BgL_cdrzd22312zd2_1448))
							{	/* Expand/iarith.scm 263 */
								obj_t BgL_cdrzd22316zd2_1450;

								BgL_cdrzd22316zd2_1450 = CDR(BgL_cdrzd22312zd2_1448);
								if (PAIRP(BgL_cdrzd22316zd2_1450))
									{	/* Expand/iarith.scm 263 */
										if (NULLP(CDR(BgL_cdrzd22316zd2_1450)))
											{	/* Expand/iarith.scm 263 */
												BgL_az00_1442 = CAR(BgL_cdrzd22312zd2_1448);
												BgL_bz00_1443 = CAR(BgL_cdrzd22316zd2_1450);
												{	/* Expand/iarith.scm 266 */
													bool_t BgL_test3403z00_5326;

													if (INTEGERP(BgL_az00_1442))
														{	/* Expand/iarith.scm 266 */
															BgL_test3403z00_5326 = INTEGERP(BgL_bz00_1443);
														}
													else
														{	/* Expand/iarith.scm 266 */
															BgL_test3403z00_5326 = ((bool_t) 0);
														}
													if (BgL_test3403z00_5326)
														{	/* Expand/iarith.scm 267 */
															obj_t BgL_rz00_1459;

															{	/* Expand/iarith.scm 267 */
																obj_t BgL_tmpz00_2788;

																BgL_tmpz00_2788 = BINT(0L);
																{	/* Expand/iarith.scm 267 */
																	bool_t BgL_test3405z00_5331;

																	{	/* Expand/iarith.scm 267 */
																		long BgL_tmpz00_5332;

																		BgL_tmpz00_5332 =
																			(long) CINT(BgL_bz00_1443);
																		BgL_test3405z00_5331 =
																			BGL_MULFX_OV(BgL_az00_1442,
																			BgL_tmpz00_5332, BgL_tmpz00_2788);
																	}
																	if (BgL_test3405z00_5331)
																		{	/* Expand/iarith.scm 267 */
																			BgL_rz00_1459 =
																				bgl_bignum_mul(bgl_long_to_bignum(
																					(long) CINT(BgL_az00_1442)),
																				bgl_long_to_bignum(
																					(long) CINT(BgL_bz00_1443)));
																		}
																	else
																		{	/* Expand/iarith.scm 267 */
																			BgL_rz00_1459 = BgL_tmpz00_2788;
																		}
																}
															}
															{	/* Expand/iarith.scm 268 */
																bool_t BgL_test3406z00_5340;

																if (INTEGERP(BgL_rz00_1459))
																	{	/* Expand/iarith.scm 268 */
																		BgL_test3406z00_5340 =
																			(
																			(long) CINT(BgL_rz00_1459) <
																			(1L << (int) (29L)));
																	}
																else
																	{	/* Expand/iarith.scm 268 */
																		BgL_test3406z00_5340 = ((bool_t) 0);
																	}
																if (BgL_test3406z00_5340)
																	{	/* Expand/iarith.scm 268 */
																		return BgL_rz00_1459;
																	}
																else
																	{	/* Expand/iarith.scm 271 */
																		obj_t BgL_arg2664z00_1463;

																		{	/* Expand/iarith.scm 271 */
																			obj_t BgL_arg2665z00_1464;

																			{	/* Expand/iarith.scm 271 */
																				obj_t BgL_arg2666z00_1465;
																				obj_t BgL_arg2667z00_1466;

																				BgL_arg2666z00_1465 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_30,
																					BgL_az00_1442, BgL_ez00_30);
																				{	/* Expand/iarith.scm 271 */
																					obj_t BgL_arg2669z00_1467;

																					BgL_arg2669z00_1467 =
																						BGL_PROCEDURE_CALL2(BgL_ez00_30,
																						BgL_bz00_1443, BgL_ez00_30);
																					BgL_arg2667z00_1466 =
																						MAKE_YOUNG_PAIR(BgL_arg2669z00_1467,
																						BNIL);
																				}
																				BgL_arg2665z00_1464 =
																					MAKE_YOUNG_PAIR(BgL_arg2666z00_1465,
																					BgL_arg2667z00_1466);
																			}
																			BgL_arg2664z00_1463 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																				BgL_arg2665z00_1464);
																		}
																		return
																			BGl_replacez12z12zztools_miscz00
																			(BgL_xz00_29,
																			BGl_epairifyzd2reczd2zztools_miscz00
																			(BgL_arg2664z00_1463, BgL_xz00_29));
																	}
															}
														}
													else
														{	/* Expand/iarith.scm 266 */
															{	/* Expand/iarith.scm 273 */
																obj_t BgL_arg2672z00_1471;
																obj_t BgL_arg2673z00_1472;

																BgL_arg2672z00_1471 =
																	CDR(((obj_t) BgL_xz00_29));
																BgL_arg2673z00_1472 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_30,
																	BgL_az00_1442, BgL_ez00_30);
																{	/* Expand/iarith.scm 273 */
																	obj_t BgL_tmpz00_5370;

																	BgL_tmpz00_5370 =
																		((obj_t) BgL_arg2672z00_1471);
																	SET_CAR(BgL_tmpz00_5370, BgL_arg2673z00_1472);
																}
															}
															{	/* Expand/iarith.scm 274 */
																obj_t BgL_arg2674z00_1473;
																obj_t BgL_arg2675z00_1474;

																{	/* Expand/iarith.scm 274 */
																	obj_t BgL_pairz00_2804;

																	BgL_pairz00_2804 = CDR(((obj_t) BgL_xz00_29));
																	BgL_arg2674z00_1473 = CDR(BgL_pairz00_2804);
																}
																BgL_arg2675z00_1474 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_30,
																	BgL_bz00_1443, BgL_ez00_30);
																{	/* Expand/iarith.scm 274 */
																	obj_t BgL_tmpz00_5381;

																	BgL_tmpz00_5381 =
																		((obj_t) BgL_arg2674z00_1473);
																	SET_CAR(BgL_tmpz00_5381, BgL_arg2675z00_1474);
																}
															}
															return BgL_xz00_29;
														}
												}
											}
										else
											{	/* Expand/iarith.scm 263 */
											BgL_tagzd22305zd2_1445:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string3062z00zzexpand_iarithmetiquez00,
													BgL_xz00_29);
											}
									}
								else
									{	/* Expand/iarith.scm 263 */
										goto BgL_tagzd22305zd2_1445;
									}
							}
						else
							{	/* Expand/iarith.scm 263 */
								goto BgL_tagzd22305zd2_1445;
							}
					}
				else
					{	/* Expand/iarith.scm 263 */
						goto BgL_tagzd22305zd2_1445;
					}
			}
		}

	}



/* &expand-*fx */
	obj_t BGl_z62expandzd2za2fxz12zzexpand_iarithmetiquez00(obj_t BgL_envz00_3079,
		obj_t BgL_xz00_3080, obj_t BgL_ez00_3081)
	{
		{	/* Expand/iarith.scm 262 */
			return
				BGl_expandzd2za2fxz70zzexpand_iarithmetiquez00(BgL_xz00_3080,
				BgL_ez00_3081);
		}

	}



/* expand-/fx */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2zf2fxz20zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_31, obj_t BgL_ez00_32)
	{
		{	/* Expand/iarith.scm 282 */
			{
				obj_t BgL_xz00_1476;
				obj_t BgL_yz00_1477;

				if (PAIRP(BgL_xz00_31))
					{	/* Expand/iarith.scm 283 */
						obj_t BgL_cdrzd22330zd2_1482;

						BgL_cdrzd22330zd2_1482 = CDR(((obj_t) BgL_xz00_31));
						if (PAIRP(BgL_cdrzd22330zd2_1482))
							{	/* Expand/iarith.scm 283 */
								obj_t BgL_cdrzd22334zd2_1484;

								BgL_cdrzd22334zd2_1484 = CDR(BgL_cdrzd22330zd2_1482);
								if (PAIRP(BgL_cdrzd22334zd2_1484))
									{	/* Expand/iarith.scm 283 */
										if (NULLP(CDR(BgL_cdrzd22334zd2_1484)))
											{	/* Expand/iarith.scm 283 */
												BgL_xz00_1476 = CAR(BgL_cdrzd22330zd2_1482);
												BgL_yz00_1477 = CAR(BgL_cdrzd22334zd2_1484);
												{	/* Expand/iarith.scm 286 */
													bool_t BgL_test3412z00_5400;

													if (INTEGERP(BgL_xz00_1476))
														{	/* Expand/iarith.scm 286 */
															if (INTEGERP(BgL_yz00_1477))
																{	/* Expand/iarith.scm 286 */
																	if (((long) CINT(BgL_yz00_1477) == 0L))
																		{	/* Expand/iarith.scm 286 */
																			BgL_test3412z00_5400 = ((bool_t) 0);
																		}
																	else
																		{	/* Expand/iarith.scm 286 */
																			BgL_test3412z00_5400 = ((bool_t) 1);
																		}
																}
															else
																{	/* Expand/iarith.scm 286 */
																	BgL_test3412z00_5400 = ((bool_t) 0);
																}
														}
													else
														{	/* Expand/iarith.scm 286 */
															BgL_test3412z00_5400 = ((bool_t) 0);
														}
													if (BgL_test3412z00_5400)
														{	/* Expand/iarith.scm 286 */
															return
																BINT(
																((long) CINT(BgL_xz00_1476) /
																	(long) CINT(BgL_yz00_1477)));
														}
													else
														{	/* Expand/iarith.scm 289 */
															obj_t BgL_arg2690z00_1495;

															{	/* Expand/iarith.scm 289 */
																obj_t BgL_arg2691z00_1496;
																obj_t BgL_arg2692z00_1497;

																BgL_arg2691z00_1496 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_32,
																	BgL_xz00_1476, BgL_ez00_32);
																{	/* Expand/iarith.scm 289 */
																	obj_t BgL_arg2693z00_1498;

																	BgL_arg2693z00_1498 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_32,
																		BgL_yz00_1477, BgL_ez00_32);
																	BgL_arg2692z00_1497 =
																		MAKE_YOUNG_PAIR(BgL_arg2693z00_1498, BNIL);
																}
																BgL_arg2690z00_1495 =
																	MAKE_YOUNG_PAIR(BgL_arg2691z00_1496,
																	BgL_arg2692z00_1497);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																BgL_arg2690z00_1495);
														}
												}
											}
										else
											{	/* Expand/iarith.scm 283 */
											BgL_tagzd22323zd2_1479:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string3063z00zzexpand_iarithmetiquez00,
													BgL_xz00_31);
											}
									}
								else
									{	/* Expand/iarith.scm 283 */
										goto BgL_tagzd22323zd2_1479;
									}
							}
						else
							{	/* Expand/iarith.scm 283 */
								goto BgL_tagzd22323zd2_1479;
							}
					}
				else
					{	/* Expand/iarith.scm 283 */
						goto BgL_tagzd22323zd2_1479;
					}
			}
		}

	}



/* &expand-/fx */
	obj_t BGl_z62expandzd2zf2fxz42zzexpand_iarithmetiquez00(obj_t BgL_envz00_3082,
		obj_t BgL_xz00_3083, obj_t BgL_ez00_3084)
	{
		{	/* Expand/iarith.scm 282 */
			return
				BGl_expandzd2zf2fxz20zzexpand_iarithmetiquez00(BgL_xz00_3083,
				BgL_ez00_3084);
		}

	}



/* expand-maxfx */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2maxfxzd2zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_33, obj_t BgL_ez00_34)
	{
		{	/* Expand/iarith.scm 296 */
			{
				obj_t BgL_az00_1507;
				obj_t BgL_bz00_1508;
				obj_t BgL_az00_1504;
				obj_t BgL_bz00_1505;

				if (PAIRP(BgL_xz00_33))
					{	/* Expand/iarith.scm 297 */
						obj_t BgL_cdrzd22350zd2_1513;

						BgL_cdrzd22350zd2_1513 = CDR(((obj_t) BgL_xz00_33));
						if (PAIRP(BgL_cdrzd22350zd2_1513))
							{	/* Expand/iarith.scm 297 */
								if (NULLP(CDR(BgL_cdrzd22350zd2_1513)))
									{	/* Expand/iarith.scm 297 */
										obj_t BgL_arg2698z00_1517;

										BgL_arg2698z00_1517 = CAR(BgL_cdrzd22350zd2_1513);
										return
											BGL_PROCEDURE_CALL2(BgL_ez00_34, BgL_arg2698z00_1517,
											BgL_ez00_34);
									}
								else
									{	/* Expand/iarith.scm 297 */
										obj_t BgL_cdrzd22366zd2_1519;

										BgL_cdrzd22366zd2_1519 =
											CDR(((obj_t) BgL_cdrzd22350zd2_1513));
										if (PAIRP(BgL_cdrzd22366zd2_1519))
											{	/* Expand/iarith.scm 297 */
												if (NULLP(CDR(BgL_cdrzd22366zd2_1519)))
													{	/* Expand/iarith.scm 297 */
														obj_t BgL_arg2702z00_1523;
														obj_t BgL_arg2703z00_1524;

														BgL_arg2702z00_1523 =
															CAR(((obj_t) BgL_cdrzd22350zd2_1513));
														BgL_arg2703z00_1524 = CAR(BgL_cdrzd22366zd2_1519);
														BgL_az00_1504 = BgL_arg2702z00_1523;
														BgL_bz00_1505 = BgL_arg2703z00_1524;
														{	/* Expand/iarith.scm 302 */
															bool_t BgL_test3421z00_5455;

															if (INTEGERP(BgL_az00_1504))
																{	/* Expand/iarith.scm 302 */
																	BgL_test3421z00_5455 =
																		INTEGERP(BgL_bz00_1505);
																}
															else
																{	/* Expand/iarith.scm 302 */
																	BgL_test3421z00_5455 = ((bool_t) 0);
																}
															if (BgL_test3421z00_5455)
																{	/* Expand/iarith.scm 302 */
																	if (
																		((long) CINT(BgL_az00_1504) >
																			(long) CINT(BgL_bz00_1505)))
																		{	/* Expand/iarith.scm 303 */
																			return BgL_az00_1504;
																		}
																	else
																		{	/* Expand/iarith.scm 303 */
																			return BgL_bz00_1505;
																		}
																}
															else
																{	/* Expand/iarith.scm 306 */
																	obj_t BgL_arg2714z00_1538;

																	{	/* Expand/iarith.scm 306 */
																		obj_t BgL_arg2715z00_1539;

																		{	/* Expand/iarith.scm 306 */
																			obj_t BgL_arg2716z00_1540;
																			obj_t BgL_arg2717z00_1541;

																			{	/* Expand/iarith.scm 306 */
																				obj_t BgL_arg2719z00_1542;
																				obj_t BgL_arg2721z00_1543;

																				{	/* Expand/iarith.scm 306 */
																					obj_t BgL_arg2722z00_1544;

																					{	/* Expand/iarith.scm 306 */
																						obj_t BgL_arg2723z00_1545;

																						BgL_arg2723z00_1545 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_34,
																							BgL_az00_1504, BgL_ez00_34);
																						BgL_arg2722z00_1544 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2723z00_1545, BNIL);
																					}
																					BgL_arg2719z00_1542 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
																						BgL_arg2722z00_1544);
																				}
																				{	/* Expand/iarith.scm 307 */
																					obj_t BgL_arg2724z00_1546;

																					{	/* Expand/iarith.scm 307 */
																						obj_t BgL_arg2725z00_1547;

																						{	/* Expand/iarith.scm 307 */
																							obj_t BgL_arg2726z00_1548;

																							BgL_arg2726z00_1548 =
																								BGL_PROCEDURE_CALL2(BgL_ez00_34,
																								BgL_bz00_1505, BgL_ez00_34);
																							BgL_arg2725z00_1547 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2726z00_1548, BNIL);
																						}
																						BgL_arg2724z00_1546 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(21), BgL_arg2725z00_1547);
																					}
																					BgL_arg2721z00_1543 =
																						MAKE_YOUNG_PAIR(BgL_arg2724z00_1546,
																						BNIL);
																				}
																				BgL_arg2716z00_1540 =
																					MAKE_YOUNG_PAIR(BgL_arg2719z00_1542,
																					BgL_arg2721z00_1543);
																			}
																			{	/* Expand/iarith.scm 308 */
																				obj_t BgL_arg2727z00_1549;

																				{	/* Expand/iarith.scm 308 */
																					obj_t BgL_arg2728z00_1550;

																					{	/* Expand/iarith.scm 308 */
																						obj_t BgL_arg2729z00_1551;
																						obj_t BgL_arg2731z00_1552;

																						{	/* Expand/iarith.scm 308 */
																							obj_t BgL_arg2733z00_1553;

																							{	/* Expand/iarith.scm 308 */
																								obj_t BgL_arg2734z00_1554;

																								BgL_arg2734z00_1554 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(21), BNIL);
																								BgL_arg2733z00_1553 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(20), BgL_arg2734z00_1554);
																							}
																							BgL_arg2729z00_1551 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(14), BgL_arg2733z00_1553);
																						}
																						{	/* Expand/iarith.scm 308 */
																							obj_t BgL_arg2736z00_1555;

																							BgL_arg2736z00_1555 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(21), BNIL);
																							BgL_arg2731z00_1552 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(20), BgL_arg2736z00_1555);
																						}
																						BgL_arg2728z00_1550 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2729z00_1551,
																							BgL_arg2731z00_1552);
																					}
																					BgL_arg2727z00_1549 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
																						BgL_arg2728z00_1550);
																				}
																				BgL_arg2717z00_1541 =
																					MAKE_YOUNG_PAIR(BgL_arg2727z00_1549,
																					BNIL);
																			}
																			BgL_arg2715z00_1539 =
																				MAKE_YOUNG_PAIR(BgL_arg2716z00_1540,
																				BgL_arg2717z00_1541);
																		}
																		BgL_arg2714z00_1538 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(23),
																			BgL_arg2715z00_1539);
																	}
																	return
																		BGl_replacez12z12zztools_miscz00
																		(BgL_xz00_33,
																		BGl_epairifyzd2reczd2zztools_miscz00
																		(BgL_arg2714z00_1538, BgL_xz00_33));
																}
														}
													}
												else
													{	/* Expand/iarith.scm 297 */
														obj_t BgL_arg2704z00_1526;
														obj_t BgL_arg2705z00_1527;

														BgL_arg2704z00_1526 =
															CAR(((obj_t) BgL_cdrzd22350zd2_1513));
														BgL_arg2705z00_1527 =
															CDR(((obj_t) BgL_cdrzd22350zd2_1513));
														BgL_az00_1507 = BgL_arg2704z00_1526;
														BgL_bz00_1508 = BgL_arg2705z00_1527;
													BgL_tagzd22342zd2_1509:
														{	/* Expand/iarith.scm 310 */
															obj_t BgL_arg2737z00_1557;

															{	/* Expand/iarith.scm 310 */
																obj_t BgL_arg2738z00_1558;

																{	/* Expand/iarith.scm 310 */
																	obj_t BgL_arg2739z00_1559;

																	{	/* Expand/iarith.scm 310 */
																		obj_t BgL_arg2740z00_1560;

																		{	/* Expand/iarith.scm 310 */
																			obj_t BgL_arg2741z00_1561;

																			{	/* Expand/iarith.scm 310 */
																				obj_t BgL_arg2742z00_1562;

																				BgL_arg2742z00_1562 =
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_bz00_1508, BNIL);
																				BgL_arg2741z00_1561 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
																					BgL_arg2742z00_1562);
																			}
																			BgL_arg2740z00_1560 =
																				MAKE_YOUNG_PAIR(BgL_arg2741z00_1561,
																				BNIL);
																		}
																		BgL_arg2739z00_1559 =
																			MAKE_YOUNG_PAIR(BgL_az00_1507,
																			BgL_arg2740z00_1560);
																	}
																	BgL_arg2738z00_1558 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
																		BgL_arg2739z00_1559);
																}
																BgL_arg2737z00_1557 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_34,
																	BgL_arg2738z00_1558, BgL_ez00_34);
															}
															return
																BGl_replacez12z12zztools_miscz00(BgL_xz00_33,
																BGl_epairifyzd2reczd2zztools_miscz00
																(BgL_arg2737z00_1557, BgL_xz00_33));
														}
													}
											}
										else
											{	/* Expand/iarith.scm 297 */
												obj_t BgL_arg2707z00_1530;
												obj_t BgL_arg2708z00_1531;

												BgL_arg2707z00_1530 =
													CAR(((obj_t) BgL_cdrzd22350zd2_1513));
												BgL_arg2708z00_1531 =
													CDR(((obj_t) BgL_cdrzd22350zd2_1513));
												{
													obj_t BgL_bz00_5523;
													obj_t BgL_az00_5522;

													BgL_az00_5522 = BgL_arg2707z00_1530;
													BgL_bz00_5523 = BgL_arg2708z00_1531;
													BgL_bz00_1508 = BgL_bz00_5523;
													BgL_az00_1507 = BgL_az00_5522;
													goto BgL_tagzd22342zd2_1509;
												}
											}
									}
							}
						else
							{	/* Expand/iarith.scm 297 */
							BgL_tagzd22343zd2_1510:
								return
									BGl_errorz00zz__errorz00(BFALSE,
									BGl_string3064z00zzexpand_iarithmetiquez00, BgL_xz00_33);
							}
					}
				else
					{	/* Expand/iarith.scm 297 */
						goto BgL_tagzd22343zd2_1510;
					}
			}
		}

	}



/* &expand-maxfx */
	obj_t BGl_z62expandzd2maxfxzb0zzexpand_iarithmetiquez00(obj_t BgL_envz00_3085,
		obj_t BgL_xz00_3086, obj_t BgL_ez00_3087)
	{
		{	/* Expand/iarith.scm 296 */
			return
				BGl_expandzd2maxfxzd2zzexpand_iarithmetiquez00(BgL_xz00_3086,
				BgL_ez00_3087);
		}

	}



/* expand-minfx */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2minfxzd2zzexpand_iarithmetiquez00(obj_t
		BgL_xz00_35, obj_t BgL_ez00_36)
	{
		{	/* Expand/iarith.scm 317 */
			{
				obj_t BgL_az00_1568;
				obj_t BgL_bz00_1569;
				obj_t BgL_az00_1565;
				obj_t BgL_bz00_1566;

				if (PAIRP(BgL_xz00_35))
					{	/* Expand/iarith.scm 318 */
						obj_t BgL_cdrzd22423zd2_1574;

						BgL_cdrzd22423zd2_1574 = CDR(((obj_t) BgL_xz00_35));
						if (PAIRP(BgL_cdrzd22423zd2_1574))
							{	/* Expand/iarith.scm 318 */
								if (NULLP(CDR(BgL_cdrzd22423zd2_1574)))
									{	/* Expand/iarith.scm 318 */
										obj_t BgL_arg2748z00_1578;

										BgL_arg2748z00_1578 = CAR(BgL_cdrzd22423zd2_1574);
										return
											BGL_PROCEDURE_CALL2(BgL_ez00_36, BgL_arg2748z00_1578,
											BgL_ez00_36);
									}
								else
									{	/* Expand/iarith.scm 318 */
										obj_t BgL_cdrzd22439zd2_1580;

										BgL_cdrzd22439zd2_1580 =
											CDR(((obj_t) BgL_cdrzd22423zd2_1574));
										if (PAIRP(BgL_cdrzd22439zd2_1580))
											{	/* Expand/iarith.scm 318 */
												if (NULLP(CDR(BgL_cdrzd22439zd2_1580)))
													{	/* Expand/iarith.scm 318 */
														obj_t BgL_arg2753z00_1584;
														obj_t BgL_arg2754z00_1585;

														BgL_arg2753z00_1584 =
															CAR(((obj_t) BgL_cdrzd22423zd2_1574));
														BgL_arg2754z00_1585 = CAR(BgL_cdrzd22439zd2_1580);
														BgL_az00_1565 = BgL_arg2753z00_1584;
														BgL_bz00_1566 = BgL_arg2754z00_1585;
														{	/* Expand/iarith.scm 323 */
															bool_t BgL_test3429z00_5551;

															if (INTEGERP(BgL_az00_1565))
																{	/* Expand/iarith.scm 323 */
																	BgL_test3429z00_5551 =
																		INTEGERP(BgL_bz00_1566);
																}
															else
																{	/* Expand/iarith.scm 323 */
																	BgL_test3429z00_5551 = ((bool_t) 0);
																}
															if (BgL_test3429z00_5551)
																{	/* Expand/iarith.scm 323 */
																	if (
																		((long) CINT(BgL_az00_1565) <
																			(long) CINT(BgL_bz00_1566)))
																		{	/* Expand/iarith.scm 324 */
																			return BgL_az00_1565;
																		}
																	else
																		{	/* Expand/iarith.scm 324 */
																			return BgL_bz00_1566;
																		}
																}
															else
																{	/* Expand/iarith.scm 327 */
																	obj_t BgL_arg2764z00_1599;

																	{	/* Expand/iarith.scm 327 */
																		obj_t BgL_arg2765z00_1600;

																		{	/* Expand/iarith.scm 327 */
																			obj_t BgL_arg2766z00_1601;
																			obj_t BgL_arg2767z00_1602;

																			{	/* Expand/iarith.scm 327 */
																				obj_t BgL_arg2771z00_1603;
																				obj_t BgL_arg2772z00_1604;

																				{	/* Expand/iarith.scm 327 */
																					obj_t BgL_arg2773z00_1605;

																					{	/* Expand/iarith.scm 327 */
																						obj_t BgL_arg2774z00_1606;

																						BgL_arg2774z00_1606 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_36,
																							BgL_az00_1565, BgL_ez00_36);
																						BgL_arg2773z00_1605 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2774z00_1606, BNIL);
																					}
																					BgL_arg2771z00_1603 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
																						BgL_arg2773z00_1605);
																				}
																				{	/* Expand/iarith.scm 328 */
																					obj_t BgL_arg2776z00_1607;

																					{	/* Expand/iarith.scm 328 */
																						obj_t BgL_arg2777z00_1608;

																						{	/* Expand/iarith.scm 328 */
																							obj_t BgL_arg2778z00_1609;

																							BgL_arg2778z00_1609 =
																								BGL_PROCEDURE_CALL2(BgL_ez00_36,
																								BgL_bz00_1566, BgL_ez00_36);
																							BgL_arg2777z00_1608 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2778z00_1609, BNIL);
																						}
																						BgL_arg2776z00_1607 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(21), BgL_arg2777z00_1608);
																					}
																					BgL_arg2772z00_1604 =
																						MAKE_YOUNG_PAIR(BgL_arg2776z00_1607,
																						BNIL);
																				}
																				BgL_arg2766z00_1601 =
																					MAKE_YOUNG_PAIR(BgL_arg2771z00_1603,
																					BgL_arg2772z00_1604);
																			}
																			{	/* Expand/iarith.scm 329 */
																				obj_t BgL_arg2780z00_1610;

																				{	/* Expand/iarith.scm 329 */
																					obj_t BgL_arg2781z00_1611;

																					{	/* Expand/iarith.scm 329 */
																						obj_t BgL_arg2783z00_1612;
																						obj_t BgL_arg2784z00_1613;

																						{	/* Expand/iarith.scm 329 */
																							obj_t BgL_arg2786z00_1614;

																							{	/* Expand/iarith.scm 329 */
																								obj_t BgL_arg2787z00_1615;

																								BgL_arg2787z00_1615 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(21), BNIL);
																								BgL_arg2786z00_1614 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(20), BgL_arg2787z00_1615);
																							}
																							BgL_arg2783z00_1612 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(12), BgL_arg2786z00_1614);
																						}
																						{	/* Expand/iarith.scm 329 */
																							obj_t BgL_arg2789z00_1616;

																							BgL_arg2789z00_1616 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(21), BNIL);
																							BgL_arg2784z00_1613 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(20), BgL_arg2789z00_1616);
																						}
																						BgL_arg2781z00_1611 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2783z00_1612,
																							BgL_arg2784z00_1613);
																					}
																					BgL_arg2780z00_1610 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
																						BgL_arg2781z00_1611);
																				}
																				BgL_arg2767z00_1602 =
																					MAKE_YOUNG_PAIR(BgL_arg2780z00_1610,
																					BNIL);
																			}
																			BgL_arg2765z00_1600 =
																				MAKE_YOUNG_PAIR(BgL_arg2766z00_1601,
																				BgL_arg2767z00_1602);
																		}
																		BgL_arg2764z00_1599 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(23),
																			BgL_arg2765z00_1600);
																	}
																	return
																		BGl_replacez12z12zztools_miscz00
																		(BgL_xz00_35,
																		BGl_epairifyzd2reczd2zztools_miscz00
																		(BgL_arg2764z00_1599, BgL_xz00_35));
																}
														}
													}
												else
													{	/* Expand/iarith.scm 318 */
														obj_t BgL_arg2755z00_1587;
														obj_t BgL_arg2756z00_1588;

														BgL_arg2755z00_1587 =
															CAR(((obj_t) BgL_cdrzd22423zd2_1574));
														BgL_arg2756z00_1588 =
															CDR(((obj_t) BgL_cdrzd22423zd2_1574));
														BgL_az00_1568 = BgL_arg2755z00_1587;
														BgL_bz00_1569 = BgL_arg2756z00_1588;
													BgL_tagzd22415zd2_1570:
														{	/* Expand/iarith.scm 331 */
															obj_t BgL_arg2793z00_1618;

															{	/* Expand/iarith.scm 331 */
																obj_t BgL_arg2794z00_1619;

																{	/* Expand/iarith.scm 331 */
																	obj_t BgL_arg2799z00_1620;

																	{	/* Expand/iarith.scm 331 */
																		obj_t BgL_arg2800z00_1621;

																		{	/* Expand/iarith.scm 331 */
																			obj_t BgL_arg2804z00_1622;

																			{	/* Expand/iarith.scm 331 */
																				obj_t BgL_arg2805z00_1623;

																				BgL_arg2805z00_1623 =
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_bz00_1569, BNIL);
																				BgL_arg2804z00_1622 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(25),
																					BgL_arg2805z00_1623);
																			}
																			BgL_arg2800z00_1621 =
																				MAKE_YOUNG_PAIR(BgL_arg2804z00_1622,
																				BNIL);
																		}
																		BgL_arg2799z00_1620 =
																			MAKE_YOUNG_PAIR(BgL_az00_1568,
																			BgL_arg2800z00_1621);
																	}
																	BgL_arg2794z00_1619 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(25),
																		BgL_arg2799z00_1620);
																}
																BgL_arg2793z00_1618 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_36,
																	BgL_arg2794z00_1619, BgL_ez00_36);
															}
															return
																BGl_replacez12z12zztools_miscz00(BgL_xz00_35,
																BGl_epairifyzd2reczd2zztools_miscz00
																(BgL_arg2793z00_1618, BgL_xz00_35));
														}
													}
											}
										else
											{	/* Expand/iarith.scm 318 */
												obj_t BgL_arg2758z00_1591;
												obj_t BgL_arg2759z00_1592;

												BgL_arg2758z00_1591 =
													CAR(((obj_t) BgL_cdrzd22423zd2_1574));
												BgL_arg2759z00_1592 =
													CDR(((obj_t) BgL_cdrzd22423zd2_1574));
												{
													obj_t BgL_bz00_5619;
													obj_t BgL_az00_5618;

													BgL_az00_5618 = BgL_arg2758z00_1591;
													BgL_bz00_5619 = BgL_arg2759z00_1592;
													BgL_bz00_1569 = BgL_bz00_5619;
													BgL_az00_1568 = BgL_az00_5618;
													goto BgL_tagzd22415zd2_1570;
												}
											}
									}
							}
						else
							{	/* Expand/iarith.scm 318 */
							BgL_tagzd22416zd2_1571:
								return
									BGl_errorz00zz__errorz00(BFALSE,
									BGl_string3065z00zzexpand_iarithmetiquez00, BgL_xz00_35);
							}
					}
				else
					{	/* Expand/iarith.scm 318 */
						goto BgL_tagzd22416zd2_1571;
					}
			}
		}

	}



/* &expand-minfx */
	obj_t BGl_z62expandzd2minfxzb0zzexpand_iarithmetiquez00(obj_t BgL_envz00_3088,
		obj_t BgL_xz00_3089, obj_t BgL_ez00_3090)
	{
		{	/* Expand/iarith.scm 317 */
			return
				BGl_expandzd2minfxzd2zzexpand_iarithmetiquez00(BgL_xz00_3089,
				BgL_ez00_3090);
		}

	}



/* expand-bit-lsh */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2bitzd2lshz00zzexpand_iarithmetiquez00(obj_t BgL_xz00_37,
		obj_t BgL_ez00_38)
	{
		{	/* Expand/iarith.scm 353 */
			{
				obj_t BgL_nz00_1624;
				obj_t BgL_shiftz00_1625;

				if (PAIRP(BgL_xz00_37))
					{	/* Expand/iarith.scm 353 */
						obj_t BgL_cdrzd22511zd2_1630;

						BgL_cdrzd22511zd2_1630 = CDR(((obj_t) BgL_xz00_37));
						if (PAIRP(BgL_cdrzd22511zd2_1630))
							{	/* Expand/iarith.scm 353 */
								obj_t BgL_carzd22514zd2_1632;
								obj_t BgL_cdrzd22515zd2_1633;

								BgL_carzd22514zd2_1632 = CAR(BgL_cdrzd22511zd2_1630);
								BgL_cdrzd22515zd2_1633 = CDR(BgL_cdrzd22511zd2_1630);
								if (INTEGERP(BgL_carzd22514zd2_1632))
									{	/* Expand/iarith.scm 353 */
										if (PAIRP(BgL_cdrzd22515zd2_1633))
											{	/* Expand/iarith.scm 353 */
												obj_t BgL_carzd22519zd2_1636;

												BgL_carzd22519zd2_1636 = CAR(BgL_cdrzd22515zd2_1633);
												if (INTEGERP(BgL_carzd22519zd2_1636))
													{	/* Expand/iarith.scm 353 */
														if (NULLP(CDR(BgL_cdrzd22515zd2_1633)))
															{	/* Expand/iarith.scm 353 */
																BgL_nz00_1624 = BgL_carzd22514zd2_1632;
																BgL_shiftz00_1625 = BgL_carzd22519zd2_1636;
																{	/* Expand/iarith.scm 353 */
																	bool_t BgL_test3438z00_5640;

																	if (((long) CINT(BgL_shiftz00_1625) <= 31L))
																		{	/* Expand/iarith.scm 353 */
																			if (((long) CINT(BgL_nz00_1624) >= 0L))
																				{	/* Expand/iarith.scm 353 */
																					bool_t BgL_test3441z00_5647;

																					{	/* Expand/iarith.scm 353 */

																						BgL_test3441z00_5647 =
																							(
																							((long) CINT(BgL_nz00_1624) <<
																								(int) (
																									(long)
																									CINT(BgL_shiftz00_1625))) >=
																							0L);
																					}
																					if (BgL_test3441z00_5647)
																						{	/* Expand/iarith.scm 353 */

																							BgL_test3438z00_5640 =
																								(
																								((long) CINT(BgL_nz00_1624) <<
																									(int) (
																										(long)
																										CINT(BgL_shiftz00_1625))) <=
																								268435456L);
																						}
																					else
																						{	/* Expand/iarith.scm 353 */
																							BgL_test3438z00_5640 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 353 */
																					BgL_test3438z00_5640 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 353 */
																			BgL_test3438z00_5640 = ((bool_t) 0);
																		}
																	if (BgL_test3438z00_5640)
																		{	/* Expand/iarith.scm 353 */
																			return
																				BINT(
																				((long) CINT(BgL_nz00_1624) <<
																					(int) (
																						(long) CINT(BgL_shiftz00_1625))));
																		}
																	else
																		{	/* Expand/iarith.scm 353 */
																			obj_t BgL_arg2823z00_1654;

																			if (NULLP(BgL_xz00_37))
																				{	/* Expand/iarith.scm 353 */
																					BgL_arg2823z00_1654 = BNIL;
																				}
																			else
																				{	/* Expand/iarith.scm 353 */
																					obj_t BgL_head1022z00_1657;

																					BgL_head1022z00_1657 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1020z00_1659;
																						obj_t BgL_tail1023z00_1660;

																						BgL_l1020z00_1659 = BgL_xz00_37;
																						BgL_tail1023z00_1660 =
																							BgL_head1022z00_1657;
																					BgL_zc3z04anonymousza32825ze3z87_1661:
																						if (NULLP
																							(BgL_l1020z00_1659))
																							{	/* Expand/iarith.scm 353 */
																								BgL_arg2823z00_1654 =
																									CDR(BgL_head1022z00_1657);
																							}
																						else
																							{	/* Expand/iarith.scm 353 */
																								obj_t BgL_newtail1024z00_1663;

																								{	/* Expand/iarith.scm 353 */
																									obj_t BgL_arg2828z00_1665;

																									{	/* Expand/iarith.scm 353 */
																										obj_t BgL_xz00_1666;

																										BgL_xz00_1666 =
																											CAR(
																											((obj_t)
																												BgL_l1020z00_1659));
																										BgL_arg2828z00_1665 =
																											BGL_PROCEDURE_CALL2
																											(BgL_ez00_38,
																											BgL_xz00_1666,
																											BgL_ez00_38);
																									}
																									BgL_newtail1024z00_1663 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2828z00_1665, BNIL);
																								}
																								SET_CDR(BgL_tail1023z00_1660,
																									BgL_newtail1024z00_1663);
																								{	/* Expand/iarith.scm 353 */
																									obj_t BgL_arg2827z00_1664;

																									BgL_arg2827z00_1664 =
																										CDR(
																										((obj_t)
																											BgL_l1020z00_1659));
																									{
																										obj_t BgL_tail1023z00_5681;
																										obj_t BgL_l1020z00_5680;

																										BgL_l1020z00_5680 =
																											BgL_arg2827z00_1664;
																										BgL_tail1023z00_5681 =
																											BgL_newtail1024z00_1663;
																										BgL_tail1023z00_1660 =
																											BgL_tail1023z00_5681;
																										BgL_l1020z00_1659 =
																											BgL_l1020z00_5680;
																										goto
																											BgL_zc3z04anonymousza32825ze3z87_1661;
																									}
																								}
																							}
																					}
																				}
																			return
																				BGl_replacez12z12zztools_miscz00
																				(BgL_xz00_37,
																				BGl_epairifyzd2reczd2zztools_miscz00
																				(BgL_arg2823z00_1654, BgL_xz00_37));
																		}
																}
															}
														else
															{	/* Expand/iarith.scm 353 */
															BgL_tagzd22504zd2_1627:
																{	/* Expand/iarith.scm 353 */
																	obj_t BgL_arg2829z00_1677;

																	if (NULLP(BgL_xz00_37))
																		{	/* Expand/iarith.scm 353 */
																			BgL_arg2829z00_1677 = BNIL;
																		}
																	else
																		{	/* Expand/iarith.scm 353 */
																			obj_t BgL_head1027z00_1680;

																			BgL_head1027z00_1680 =
																				MAKE_YOUNG_PAIR(BNIL, BNIL);
																			{
																				obj_t BgL_l1025z00_1682;
																				obj_t BgL_tail1028z00_1683;

																				BgL_l1025z00_1682 = BgL_xz00_37;
																				BgL_tail1028z00_1683 =
																					BgL_head1027z00_1680;
																			BgL_zc3z04anonymousza32831ze3z87_1684:
																				if (NULLP(BgL_l1025z00_1682))
																					{	/* Expand/iarith.scm 353 */
																						BgL_arg2829z00_1677 =
																							CDR(BgL_head1027z00_1680);
																					}
																				else
																					{	/* Expand/iarith.scm 353 */
																						obj_t BgL_newtail1029z00_1686;

																						{	/* Expand/iarith.scm 353 */
																							obj_t BgL_arg2834z00_1688;

																							{	/* Expand/iarith.scm 353 */
																								obj_t BgL_xz00_1689;

																								BgL_xz00_1689 =
																									CAR(
																									((obj_t) BgL_l1025z00_1682));
																								BgL_arg2834z00_1688 =
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_38, BgL_xz00_1689,
																									BgL_ez00_38);
																							}
																							BgL_newtail1029z00_1686 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2834z00_1688, BNIL);
																						}
																						SET_CDR(BgL_tail1028z00_1683,
																							BgL_newtail1029z00_1686);
																						{	/* Expand/iarith.scm 353 */
																							obj_t BgL_arg2833z00_1687;

																							BgL_arg2833z00_1687 =
																								CDR(
																								((obj_t) BgL_l1025z00_1682));
																							{
																								obj_t BgL_tail1028z00_5702;
																								obj_t BgL_l1025z00_5701;

																								BgL_l1025z00_5701 =
																									BgL_arg2833z00_1687;
																								BgL_tail1028z00_5702 =
																									BgL_newtail1029z00_1686;
																								BgL_tail1028z00_1683 =
																									BgL_tail1028z00_5702;
																								BgL_l1025z00_1682 =
																									BgL_l1025z00_5701;
																								goto
																									BgL_zc3z04anonymousza32831ze3z87_1684;
																							}
																						}
																					}
																			}
																		}
																	return
																		BGl_replacez12z12zztools_miscz00
																		(BgL_xz00_37,
																		BGl_epairifyzd2reczd2zztools_miscz00
																		(BgL_arg2829z00_1677, BgL_xz00_37));
																}
															}
													}
												else
													{	/* Expand/iarith.scm 353 */
														goto BgL_tagzd22504zd2_1627;
													}
											}
										else
											{	/* Expand/iarith.scm 353 */
												goto BgL_tagzd22504zd2_1627;
											}
									}
								else
									{	/* Expand/iarith.scm 353 */
										goto BgL_tagzd22504zd2_1627;
									}
							}
						else
							{	/* Expand/iarith.scm 353 */
								goto BgL_tagzd22504zd2_1627;
							}
					}
				else
					{	/* Expand/iarith.scm 353 */
						goto BgL_tagzd22504zd2_1627;
					}
			}
		}

	}



/* &expand-bit-lsh */
	obj_t BGl_z62expandzd2bitzd2lshz62zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3091, obj_t BgL_xz00_3092, obj_t BgL_ez00_3093)
	{
		{	/* Expand/iarith.scm 353 */
			return
				BGl_expandzd2bitzd2lshz00zzexpand_iarithmetiquez00(BgL_xz00_3092,
				BgL_ez00_3093);
		}

	}



/* expand-bit-lshu32 */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2bitzd2lshu32z00zzexpand_iarithmetiquez00(obj_t BgL_xz00_39,
		obj_t BgL_ez00_40)
	{
		{	/* Expand/iarith.scm 354 */
			{
				obj_t BgL_nz00_1691;
				obj_t BgL_shiftz00_1692;

				if (PAIRP(BgL_xz00_39))
					{	/* Expand/iarith.scm 354 */
						obj_t BgL_cdrzd22531zd2_1697;

						BgL_cdrzd22531zd2_1697 = CDR(((obj_t) BgL_xz00_39));
						if (PAIRP(BgL_cdrzd22531zd2_1697))
							{	/* Expand/iarith.scm 354 */
								obj_t BgL_carzd22534zd2_1699;
								obj_t BgL_cdrzd22535zd2_1700;

								BgL_carzd22534zd2_1699 = CAR(BgL_cdrzd22531zd2_1697);
								BgL_cdrzd22535zd2_1700 = CDR(BgL_cdrzd22531zd2_1697);
								if (BGL_UINT32P(BgL_carzd22534zd2_1699))
									{	/* Expand/iarith.scm 354 */
										if (PAIRP(BgL_cdrzd22535zd2_1700))
											{	/* Expand/iarith.scm 354 */
												obj_t BgL_carzd22539zd2_1703;

												BgL_carzd22539zd2_1703 = CAR(BgL_cdrzd22535zd2_1700);
												if (INTEGERP(BgL_carzd22539zd2_1703))
													{	/* Expand/iarith.scm 354 */
														if (NULLP(CDR(BgL_cdrzd22535zd2_1700)))
															{	/* Expand/iarith.scm 354 */
																BgL_nz00_1691 = BgL_carzd22534zd2_1699;
																BgL_shiftz00_1692 = BgL_carzd22539zd2_1703;
																{	/* Expand/iarith.scm 354 */
																	bool_t BgL_test3452z00_5724;

																	if (((long) CINT(BgL_shiftz00_1692) <= 31L))
																		{	/* Expand/iarith.scm 354 */
																			if (BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																				(BgL_nz00_1691, BINT(0L)))
																				{	/* Expand/iarith.scm 354 */
																					bool_t BgL_test3455z00_5731;

																					{	/* Expand/iarith.scm 354 */
																						uint32_t BgL_a1030z00_1741;

																						{	/* Expand/iarith.scm 354 */
																							uint32_t BgL_xz00_2884;
																							long BgL_yz00_2885;

																							BgL_xz00_2884 =
																								BGL_BUINT32_TO_UINT32
																								(BgL_nz00_1691);
																							BgL_yz00_2885 =
																								(long) CINT(BgL_shiftz00_1692);
																							BgL_a1030z00_1741 =
																								(BgL_xz00_2884 <<
																								(int) (BgL_yz00_2885));
																						}
																						{	/* Expand/iarith.scm 354 */

																							BgL_test3455z00_5731 =
																								BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																								(BGL_UINT32_TO_BUINT32
																								(BgL_a1030z00_1741), BINT(0L));
																					}}
																					if (BgL_test3455z00_5731)
																						{	/* Expand/iarith.scm 354 */
																							uint32_t BgL_a1032z00_1738;

																							{	/* Expand/iarith.scm 354 */
																								uint32_t BgL_xz00_2888;
																								long BgL_yz00_2889;

																								BgL_xz00_2888 =
																									BGL_BUINT32_TO_UINT32
																									(BgL_nz00_1691);
																								BgL_yz00_2889 =
																									(long)
																									CINT(BgL_shiftz00_1692);
																								BgL_a1032z00_1738 =
																									(BgL_xz00_2888 <<
																									(int) (BgL_yz00_2889));
																							}
																							{	/* Expand/iarith.scm 354 */

																								BgL_test3452z00_5724 =
																									BGl_2zc3zd3z10zz__r4_numbers_6_5z00
																									(BGL_UINT32_TO_BUINT32
																									(BgL_a1032z00_1738),
																									BINT(268435456L));
																						}}
																					else
																						{	/* Expand/iarith.scm 354 */
																							BgL_test3452z00_5724 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 354 */
																					BgL_test3452z00_5724 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 354 */
																			BgL_test3452z00_5724 = ((bool_t) 0);
																		}
																	if (BgL_test3452z00_5724)
																		{	/* Expand/iarith.scm 354 */
																			uint32_t BgL_xz00_2892;
																			long BgL_yz00_2893;

																			BgL_xz00_2892 =
																				BGL_BUINT32_TO_UINT32(BgL_nz00_1691);
																			BgL_yz00_2893 =
																				(long) CINT(BgL_shiftz00_1692);
																			{	/* Expand/iarith.scm 354 */
																				uint32_t BgL_tmpz00_5748;

																				BgL_tmpz00_5748 =
																					(BgL_xz00_2892 <<
																					(int) (BgL_yz00_2893));
																				return
																					BGL_UINT32_TO_BUINT32
																					(BgL_tmpz00_5748);
																			}
																		}
																	else
																		{	/* Expand/iarith.scm 354 */
																			obj_t BgL_arg2856z00_1721;

																			if (NULLP(BgL_xz00_39))
																				{	/* Expand/iarith.scm 354 */
																					BgL_arg2856z00_1721 = BNIL;
																				}
																			else
																				{	/* Expand/iarith.scm 354 */
																					obj_t BgL_head1036z00_1724;

																					BgL_head1036z00_1724 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1034z00_1726;
																						obj_t BgL_tail1037z00_1727;

																						BgL_l1034z00_1726 = BgL_xz00_39;
																						BgL_tail1037z00_1727 =
																							BgL_head1036z00_1724;
																					BgL_zc3z04anonymousza32858ze3z87_1728:
																						if (NULLP
																							(BgL_l1034z00_1726))
																							{	/* Expand/iarith.scm 354 */
																								BgL_arg2856z00_1721 =
																									CDR(BgL_head1036z00_1724);
																							}
																						else
																							{	/* Expand/iarith.scm 354 */
																								obj_t BgL_newtail1038z00_1730;

																								{	/* Expand/iarith.scm 354 */
																									obj_t BgL_arg2870z00_1732;

																									{	/* Expand/iarith.scm 354 */
																										obj_t BgL_xz00_1733;

																										BgL_xz00_1733 =
																											CAR(
																											((obj_t)
																												BgL_l1034z00_1726));
																										BgL_arg2870z00_1732 =
																											BGL_PROCEDURE_CALL2
																											(BgL_ez00_40,
																											BgL_xz00_1733,
																											BgL_ez00_40);
																									}
																									BgL_newtail1038z00_1730 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2870z00_1732, BNIL);
																								}
																								SET_CDR(BgL_tail1037z00_1727,
																									BgL_newtail1038z00_1730);
																								{	/* Expand/iarith.scm 354 */
																									obj_t BgL_arg2864z00_1731;

																									BgL_arg2864z00_1731 =
																										CDR(
																										((obj_t)
																											BgL_l1034z00_1726));
																									{
																										obj_t BgL_tail1037z00_5770;
																										obj_t BgL_l1034z00_5769;

																										BgL_l1034z00_5769 =
																											BgL_arg2864z00_1731;
																										BgL_tail1037z00_5770 =
																											BgL_newtail1038z00_1730;
																										BgL_tail1037z00_1727 =
																											BgL_tail1037z00_5770;
																										BgL_l1034z00_1726 =
																											BgL_l1034z00_5769;
																										goto
																											BgL_zc3z04anonymousza32858ze3z87_1728;
																									}
																								}
																							}
																					}
																				}
																			return
																				BGl_replacez12z12zztools_miscz00
																				(BgL_xz00_39,
																				BGl_epairifyzd2reczd2zztools_miscz00
																				(BgL_arg2856z00_1721, BgL_xz00_39));
																		}
																}
															}
														else
															{	/* Expand/iarith.scm 354 */
															BgL_tagzd22524zd2_1694:
																{	/* Expand/iarith.scm 354 */
																	obj_t BgL_arg2871z00_1744;

																	if (NULLP(BgL_xz00_39))
																		{	/* Expand/iarith.scm 354 */
																			BgL_arg2871z00_1744 = BNIL;
																		}
																	else
																		{	/* Expand/iarith.scm 354 */
																			obj_t BgL_head1041z00_1747;

																			BgL_head1041z00_1747 =
																				MAKE_YOUNG_PAIR(BNIL, BNIL);
																			{
																				obj_t BgL_l1039z00_1749;
																				obj_t BgL_tail1042z00_1750;

																				BgL_l1039z00_1749 = BgL_xz00_39;
																				BgL_tail1042z00_1750 =
																					BgL_head1041z00_1747;
																			BgL_zc3z04anonymousza32873ze3z87_1751:
																				if (NULLP(BgL_l1039z00_1749))
																					{	/* Expand/iarith.scm 354 */
																						BgL_arg2871z00_1744 =
																							CDR(BgL_head1041z00_1747);
																					}
																				else
																					{	/* Expand/iarith.scm 354 */
																						obj_t BgL_newtail1043z00_1753;

																						{	/* Expand/iarith.scm 354 */
																							obj_t BgL_arg2876z00_1755;

																							{	/* Expand/iarith.scm 354 */
																								obj_t BgL_xz00_1756;

																								BgL_xz00_1756 =
																									CAR(
																									((obj_t) BgL_l1039z00_1749));
																								BgL_arg2876z00_1755 =
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_40, BgL_xz00_1756,
																									BgL_ez00_40);
																							}
																							BgL_newtail1043z00_1753 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2876z00_1755, BNIL);
																						}
																						SET_CDR(BgL_tail1042z00_1750,
																							BgL_newtail1043z00_1753);
																						{	/* Expand/iarith.scm 354 */
																							obj_t BgL_arg2875z00_1754;

																							BgL_arg2875z00_1754 =
																								CDR(
																								((obj_t) BgL_l1039z00_1749));
																							{
																								obj_t BgL_tail1042z00_5791;
																								obj_t BgL_l1039z00_5790;

																								BgL_l1039z00_5790 =
																									BgL_arg2875z00_1754;
																								BgL_tail1042z00_5791 =
																									BgL_newtail1043z00_1753;
																								BgL_tail1042z00_1750 =
																									BgL_tail1042z00_5791;
																								BgL_l1039z00_1749 =
																									BgL_l1039z00_5790;
																								goto
																									BgL_zc3z04anonymousza32873ze3z87_1751;
																							}
																						}
																					}
																			}
																		}
																	return
																		BGl_replacez12z12zztools_miscz00
																		(BgL_xz00_39,
																		BGl_epairifyzd2reczd2zztools_miscz00
																		(BgL_arg2871z00_1744, BgL_xz00_39));
																}
															}
													}
												else
													{	/* Expand/iarith.scm 354 */
														goto BgL_tagzd22524zd2_1694;
													}
											}
										else
											{	/* Expand/iarith.scm 354 */
												goto BgL_tagzd22524zd2_1694;
											}
									}
								else
									{	/* Expand/iarith.scm 354 */
										goto BgL_tagzd22524zd2_1694;
									}
							}
						else
							{	/* Expand/iarith.scm 354 */
								goto BgL_tagzd22524zd2_1694;
							}
					}
				else
					{	/* Expand/iarith.scm 354 */
						goto BgL_tagzd22524zd2_1694;
					}
			}
		}

	}



/* &expand-bit-lshu32 */
	obj_t BGl_z62expandzd2bitzd2lshu32z62zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3094, obj_t BgL_xz00_3095, obj_t BgL_ez00_3096)
	{
		{	/* Expand/iarith.scm 354 */
			return
				BGl_expandzd2bitzd2lshu32z00zzexpand_iarithmetiquez00(BgL_xz00_3095,
				BgL_ez00_3096);
		}

	}



/* expand-bit-rsh */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2bitzd2rshz00zzexpand_iarithmetiquez00(obj_t BgL_xz00_41,
		obj_t BgL_ez00_42)
	{
		{	/* Expand/iarith.scm 356 */
			{
				obj_t BgL_nz00_1758;
				obj_t BgL_shiftz00_1759;

				if (PAIRP(BgL_xz00_41))
					{	/* Expand/iarith.scm 356 */
						obj_t BgL_cdrzd22551zd2_1764;

						BgL_cdrzd22551zd2_1764 = CDR(((obj_t) BgL_xz00_41));
						if (PAIRP(BgL_cdrzd22551zd2_1764))
							{	/* Expand/iarith.scm 356 */
								obj_t BgL_carzd22554zd2_1766;
								obj_t BgL_cdrzd22555zd2_1767;

								BgL_carzd22554zd2_1766 = CAR(BgL_cdrzd22551zd2_1764);
								BgL_cdrzd22555zd2_1767 = CDR(BgL_cdrzd22551zd2_1764);
								if (INTEGERP(BgL_carzd22554zd2_1766))
									{	/* Expand/iarith.scm 356 */
										if (PAIRP(BgL_cdrzd22555zd2_1767))
											{	/* Expand/iarith.scm 356 */
												obj_t BgL_carzd22559zd2_1770;

												BgL_carzd22559zd2_1770 = CAR(BgL_cdrzd22555zd2_1767);
												if (INTEGERP(BgL_carzd22559zd2_1770))
													{	/* Expand/iarith.scm 356 */
														if (NULLP(CDR(BgL_cdrzd22555zd2_1767)))
															{	/* Expand/iarith.scm 356 */
																BgL_nz00_1758 = BgL_carzd22554zd2_1766;
																BgL_shiftz00_1759 = BgL_carzd22559zd2_1770;
																{	/* Expand/iarith.scm 356 */
																	bool_t BgL_test3466z00_5813;

																	if (((long) CINT(BgL_shiftz00_1759) <= 31L))
																		{	/* Expand/iarith.scm 356 */
																			if (((long) CINT(BgL_nz00_1758) >= 0L))
																				{	/* Expand/iarith.scm 356 */
																					bool_t BgL_test3469z00_5820;

																					{	/* Expand/iarith.scm 356 */

																						BgL_test3469z00_5820 =
																							(
																							((long) CINT(BgL_nz00_1758) >>
																								(int) (
																									(long)
																									CINT(BgL_shiftz00_1759))) >=
																							0L);
																					}
																					if (BgL_test3469z00_5820)
																						{	/* Expand/iarith.scm 356 */

																							BgL_test3466z00_5813 =
																								(
																								((long) CINT(BgL_nz00_1758) >>
																									(int) (
																										(long)
																										CINT(BgL_shiftz00_1759))) <=
																								268435456L);
																						}
																					else
																						{	/* Expand/iarith.scm 356 */
																							BgL_test3466z00_5813 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 356 */
																					BgL_test3466z00_5813 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 356 */
																			BgL_test3466z00_5813 = ((bool_t) 0);
																		}
																	if (BgL_test3466z00_5813)
																		{	/* Expand/iarith.scm 356 */
																			return
																				BINT(
																				((long) CINT(BgL_nz00_1758) >>
																					(int) (
																						(long) CINT(BgL_shiftz00_1759))));
																		}
																	else
																		{	/* Expand/iarith.scm 356 */
																			obj_t BgL_arg2894z00_1788;

																			if (NULLP(BgL_xz00_41))
																				{	/* Expand/iarith.scm 356 */
																					BgL_arg2894z00_1788 = BNIL;
																				}
																			else
																				{	/* Expand/iarith.scm 356 */
																					obj_t BgL_head1050z00_1791;

																					BgL_head1050z00_1791 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1048z00_1793;
																						obj_t BgL_tail1051z00_1794;

																						BgL_l1048z00_1793 = BgL_xz00_41;
																						BgL_tail1051z00_1794 =
																							BgL_head1050z00_1791;
																					BgL_zc3z04anonymousza32896ze3z87_1795:
																						if (NULLP
																							(BgL_l1048z00_1793))
																							{	/* Expand/iarith.scm 356 */
																								BgL_arg2894z00_1788 =
																									CDR(BgL_head1050z00_1791);
																							}
																						else
																							{	/* Expand/iarith.scm 356 */
																								obj_t BgL_newtail1052z00_1797;

																								{	/* Expand/iarith.scm 356 */
																									obj_t BgL_arg2899z00_1799;

																									{	/* Expand/iarith.scm 356 */
																										obj_t BgL_xz00_1800;

																										BgL_xz00_1800 =
																											CAR(
																											((obj_t)
																												BgL_l1048z00_1793));
																										BgL_arg2899z00_1799 =
																											BGL_PROCEDURE_CALL2
																											(BgL_ez00_42,
																											BgL_xz00_1800,
																											BgL_ez00_42);
																									}
																									BgL_newtail1052z00_1797 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2899z00_1799, BNIL);
																								}
																								SET_CDR(BgL_tail1051z00_1794,
																									BgL_newtail1052z00_1797);
																								{	/* Expand/iarith.scm 356 */
																									obj_t BgL_arg2898z00_1798;

																									BgL_arg2898z00_1798 =
																										CDR(
																										((obj_t)
																											BgL_l1048z00_1793));
																									{
																										obj_t BgL_tail1051z00_5854;
																										obj_t BgL_l1048z00_5853;

																										BgL_l1048z00_5853 =
																											BgL_arg2898z00_1798;
																										BgL_tail1051z00_5854 =
																											BgL_newtail1052z00_1797;
																										BgL_tail1051z00_1794 =
																											BgL_tail1051z00_5854;
																										BgL_l1048z00_1793 =
																											BgL_l1048z00_5853;
																										goto
																											BgL_zc3z04anonymousza32896ze3z87_1795;
																									}
																								}
																							}
																					}
																				}
																			return
																				BGl_replacez12z12zztools_miscz00
																				(BgL_xz00_41,
																				BGl_epairifyzd2reczd2zztools_miscz00
																				(BgL_arg2894z00_1788, BgL_xz00_41));
																		}
																}
															}
														else
															{	/* Expand/iarith.scm 356 */
															BgL_tagzd22544zd2_1761:
																{	/* Expand/iarith.scm 356 */
																	obj_t BgL_arg2903z00_1811;

																	if (NULLP(BgL_xz00_41))
																		{	/* Expand/iarith.scm 356 */
																			BgL_arg2903z00_1811 = BNIL;
																		}
																	else
																		{	/* Expand/iarith.scm 356 */
																			obj_t BgL_head1055z00_1814;

																			BgL_head1055z00_1814 =
																				MAKE_YOUNG_PAIR(BNIL, BNIL);
																			{
																				obj_t BgL_l1053z00_1816;
																				obj_t BgL_tail1056z00_1817;

																				BgL_l1053z00_1816 = BgL_xz00_41;
																				BgL_tail1056z00_1817 =
																					BgL_head1055z00_1814;
																			BgL_zc3z04anonymousza32905ze3z87_1818:
																				if (NULLP(BgL_l1053z00_1816))
																					{	/* Expand/iarith.scm 356 */
																						BgL_arg2903z00_1811 =
																							CDR(BgL_head1055z00_1814);
																					}
																				else
																					{	/* Expand/iarith.scm 356 */
																						obj_t BgL_newtail1057z00_1820;

																						{	/* Expand/iarith.scm 356 */
																							obj_t BgL_arg2908z00_1822;

																							{	/* Expand/iarith.scm 356 */
																								obj_t BgL_xz00_1823;

																								BgL_xz00_1823 =
																									CAR(
																									((obj_t) BgL_l1053z00_1816));
																								BgL_arg2908z00_1822 =
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_42, BgL_xz00_1823,
																									BgL_ez00_42);
																							}
																							BgL_newtail1057z00_1820 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2908z00_1822, BNIL);
																						}
																						SET_CDR(BgL_tail1056z00_1817,
																							BgL_newtail1057z00_1820);
																						{	/* Expand/iarith.scm 356 */
																							obj_t BgL_arg2907z00_1821;

																							BgL_arg2907z00_1821 =
																								CDR(
																								((obj_t) BgL_l1053z00_1816));
																							{
																								obj_t BgL_tail1056z00_5875;
																								obj_t BgL_l1053z00_5874;

																								BgL_l1053z00_5874 =
																									BgL_arg2907z00_1821;
																								BgL_tail1056z00_5875 =
																									BgL_newtail1057z00_1820;
																								BgL_tail1056z00_1817 =
																									BgL_tail1056z00_5875;
																								BgL_l1053z00_1816 =
																									BgL_l1053z00_5874;
																								goto
																									BgL_zc3z04anonymousza32905ze3z87_1818;
																							}
																						}
																					}
																			}
																		}
																	return
																		BGl_replacez12z12zztools_miscz00
																		(BgL_xz00_41,
																		BGl_epairifyzd2reczd2zztools_miscz00
																		(BgL_arg2903z00_1811, BgL_xz00_41));
																}
															}
													}
												else
													{	/* Expand/iarith.scm 356 */
														goto BgL_tagzd22544zd2_1761;
													}
											}
										else
											{	/* Expand/iarith.scm 356 */
												goto BgL_tagzd22544zd2_1761;
											}
									}
								else
									{	/* Expand/iarith.scm 356 */
										goto BgL_tagzd22544zd2_1761;
									}
							}
						else
							{	/* Expand/iarith.scm 356 */
								goto BgL_tagzd22544zd2_1761;
							}
					}
				else
					{	/* Expand/iarith.scm 356 */
						goto BgL_tagzd22544zd2_1761;
					}
			}
		}

	}



/* &expand-bit-rsh */
	obj_t BGl_z62expandzd2bitzd2rshz62zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3097, obj_t BgL_xz00_3098, obj_t BgL_ez00_3099)
	{
		{	/* Expand/iarith.scm 356 */
			return
				BGl_expandzd2bitzd2rshz00zzexpand_iarithmetiquez00(BgL_xz00_3098,
				BgL_ez00_3099);
		}

	}



/* expand-bit-rshs32 */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2bitzd2rshs32z00zzexpand_iarithmetiquez00(obj_t BgL_xz00_43,
		obj_t BgL_ez00_44)
	{
		{	/* Expand/iarith.scm 357 */
			{
				obj_t BgL_nz00_1825;
				obj_t BgL_shiftz00_1826;

				if (PAIRP(BgL_xz00_43))
					{	/* Expand/iarith.scm 357 */
						obj_t BgL_cdrzd22571zd2_1831;

						BgL_cdrzd22571zd2_1831 = CDR(((obj_t) BgL_xz00_43));
						if (PAIRP(BgL_cdrzd22571zd2_1831))
							{	/* Expand/iarith.scm 357 */
								obj_t BgL_carzd22574zd2_1833;
								obj_t BgL_cdrzd22575zd2_1834;

								BgL_carzd22574zd2_1833 = CAR(BgL_cdrzd22571zd2_1831);
								BgL_cdrzd22575zd2_1834 = CDR(BgL_cdrzd22571zd2_1831);
								if (BGL_INT32P(BgL_carzd22574zd2_1833))
									{	/* Expand/iarith.scm 357 */
										if (PAIRP(BgL_cdrzd22575zd2_1834))
											{	/* Expand/iarith.scm 357 */
												obj_t BgL_carzd22579zd2_1837;

												BgL_carzd22579zd2_1837 = CAR(BgL_cdrzd22575zd2_1834);
												if (INTEGERP(BgL_carzd22579zd2_1837))
													{	/* Expand/iarith.scm 357 */
														if (NULLP(CDR(BgL_cdrzd22575zd2_1834)))
															{	/* Expand/iarith.scm 357 */
																BgL_nz00_1825 = BgL_carzd22574zd2_1833;
																BgL_shiftz00_1826 = BgL_carzd22579zd2_1837;
																{	/* Expand/iarith.scm 357 */
																	bool_t BgL_test3480z00_5897;

																	if (((long) CINT(BgL_shiftz00_1826) <= 31L))
																		{	/* Expand/iarith.scm 357 */
																			if (BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																				(BgL_nz00_1825, BINT(0L)))
																				{	/* Expand/iarith.scm 357 */
																					bool_t BgL_test3483z00_5904;

																					{	/* Expand/iarith.scm 357 */
																						int32_t BgL_a1058z00_1875;

																						{	/* Expand/iarith.scm 357 */
																							int32_t BgL_xz00_2938;
																							long BgL_yz00_2939;

																							BgL_xz00_2938 =
																								BGL_BINT32_TO_INT32
																								(BgL_nz00_1825);
																							BgL_yz00_2939 =
																								(long) CINT(BgL_shiftz00_1826);
																							BgL_a1058z00_1875 =
																								(BgL_xz00_2938 >>
																								(int) (BgL_yz00_2939));
																						}
																						{	/* Expand/iarith.scm 357 */

																							BgL_test3483z00_5904 =
																								BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																								(BGL_INT32_TO_BINT32
																								(BgL_a1058z00_1875), BINT(0L));
																					}}
																					if (BgL_test3483z00_5904)
																						{	/* Expand/iarith.scm 357 */
																							int32_t BgL_a1060z00_1872;

																							{	/* Expand/iarith.scm 357 */
																								int32_t BgL_xz00_2942;
																								long BgL_yz00_2943;

																								BgL_xz00_2942 =
																									BGL_BINT32_TO_INT32
																									(BgL_nz00_1825);
																								BgL_yz00_2943 =
																									(long)
																									CINT(BgL_shiftz00_1826);
																								BgL_a1060z00_1872 =
																									(BgL_xz00_2942 >>
																									(int) (BgL_yz00_2943));
																							}
																							{	/* Expand/iarith.scm 357 */

																								BgL_test3480z00_5897 =
																									BGl_2zc3zd3z10zz__r4_numbers_6_5z00
																									(BGL_INT32_TO_BINT32
																									(BgL_a1060z00_1872),
																									BINT(268435456L));
																						}}
																					else
																						{	/* Expand/iarith.scm 357 */
																							BgL_test3480z00_5897 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 357 */
																					BgL_test3480z00_5897 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 357 */
																			BgL_test3480z00_5897 = ((bool_t) 0);
																		}
																	if (BgL_test3480z00_5897)
																		{	/* Expand/iarith.scm 357 */
																			int32_t BgL_xz00_2946;
																			long BgL_yz00_2947;

																			BgL_xz00_2946 =
																				BGL_BINT32_TO_INT32(BgL_nz00_1825);
																			BgL_yz00_2947 =
																				(long) CINT(BgL_shiftz00_1826);
																			{	/* Expand/iarith.scm 357 */
																				int32_t BgL_tmpz00_5921;

																				BgL_tmpz00_5921 =
																					(BgL_xz00_2946 >>
																					(int) (BgL_yz00_2947));
																				return
																					BGL_INT32_TO_BINT32(BgL_tmpz00_5921);
																			}
																		}
																	else
																		{	/* Expand/iarith.scm 357 */
																			obj_t BgL_arg2923z00_1855;

																			if (NULLP(BgL_xz00_43))
																				{	/* Expand/iarith.scm 357 */
																					BgL_arg2923z00_1855 = BNIL;
																				}
																			else
																				{	/* Expand/iarith.scm 357 */
																					obj_t BgL_head1064z00_1858;

																					BgL_head1064z00_1858 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1062z00_1860;
																						obj_t BgL_tail1065z00_1861;

																						BgL_l1062z00_1860 = BgL_xz00_43;
																						BgL_tail1065z00_1861 =
																							BgL_head1064z00_1858;
																					BgL_zc3z04anonymousza32925ze3z87_1862:
																						if (NULLP
																							(BgL_l1062z00_1860))
																							{	/* Expand/iarith.scm 357 */
																								BgL_arg2923z00_1855 =
																									CDR(BgL_head1064z00_1858);
																							}
																						else
																							{	/* Expand/iarith.scm 357 */
																								obj_t BgL_newtail1066z00_1864;

																								{	/* Expand/iarith.scm 357 */
																									obj_t BgL_arg2928z00_1866;

																									{	/* Expand/iarith.scm 357 */
																										obj_t BgL_xz00_1867;

																										BgL_xz00_1867 =
																											CAR(
																											((obj_t)
																												BgL_l1062z00_1860));
																										BgL_arg2928z00_1866 =
																											BGL_PROCEDURE_CALL2
																											(BgL_ez00_44,
																											BgL_xz00_1867,
																											BgL_ez00_44);
																									}
																									BgL_newtail1066z00_1864 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2928z00_1866, BNIL);
																								}
																								SET_CDR(BgL_tail1065z00_1861,
																									BgL_newtail1066z00_1864);
																								{	/* Expand/iarith.scm 357 */
																									obj_t BgL_arg2927z00_1865;

																									BgL_arg2927z00_1865 =
																										CDR(
																										((obj_t)
																											BgL_l1062z00_1860));
																									{
																										obj_t BgL_tail1065z00_5943;
																										obj_t BgL_l1062z00_5942;

																										BgL_l1062z00_5942 =
																											BgL_arg2927z00_1865;
																										BgL_tail1065z00_5943 =
																											BgL_newtail1066z00_1864;
																										BgL_tail1065z00_1861 =
																											BgL_tail1065z00_5943;
																										BgL_l1062z00_1860 =
																											BgL_l1062z00_5942;
																										goto
																											BgL_zc3z04anonymousza32925ze3z87_1862;
																									}
																								}
																							}
																					}
																				}
																			return
																				BGl_replacez12z12zztools_miscz00
																				(BgL_xz00_43,
																				BGl_epairifyzd2reczd2zztools_miscz00
																				(BgL_arg2923z00_1855, BgL_xz00_43));
																		}
																}
															}
														else
															{	/* Expand/iarith.scm 357 */
															BgL_tagzd22564zd2_1828:
																{	/* Expand/iarith.scm 357 */
																	obj_t BgL_arg2929z00_1878;

																	if (NULLP(BgL_xz00_43))
																		{	/* Expand/iarith.scm 357 */
																			BgL_arg2929z00_1878 = BNIL;
																		}
																	else
																		{	/* Expand/iarith.scm 357 */
																			obj_t BgL_head1069z00_1881;

																			BgL_head1069z00_1881 =
																				MAKE_YOUNG_PAIR(BNIL, BNIL);
																			{
																				obj_t BgL_l1067z00_1883;
																				obj_t BgL_tail1070z00_1884;

																				BgL_l1067z00_1883 = BgL_xz00_43;
																				BgL_tail1070z00_1884 =
																					BgL_head1069z00_1881;
																			BgL_zc3z04anonymousza32931ze3z87_1885:
																				if (NULLP(BgL_l1067z00_1883))
																					{	/* Expand/iarith.scm 357 */
																						BgL_arg2929z00_1878 =
																							CDR(BgL_head1069z00_1881);
																					}
																				else
																					{	/* Expand/iarith.scm 357 */
																						obj_t BgL_newtail1071z00_1887;

																						{	/* Expand/iarith.scm 357 */
																							obj_t BgL_arg2934z00_1889;

																							{	/* Expand/iarith.scm 357 */
																								obj_t BgL_xz00_1890;

																								BgL_xz00_1890 =
																									CAR(
																									((obj_t) BgL_l1067z00_1883));
																								BgL_arg2934z00_1889 =
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_44, BgL_xz00_1890,
																									BgL_ez00_44);
																							}
																							BgL_newtail1071z00_1887 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2934z00_1889, BNIL);
																						}
																						SET_CDR(BgL_tail1070z00_1884,
																							BgL_newtail1071z00_1887);
																						{	/* Expand/iarith.scm 357 */
																							obj_t BgL_arg2933z00_1888;

																							BgL_arg2933z00_1888 =
																								CDR(
																								((obj_t) BgL_l1067z00_1883));
																							{
																								obj_t BgL_tail1070z00_5964;
																								obj_t BgL_l1067z00_5963;

																								BgL_l1067z00_5963 =
																									BgL_arg2933z00_1888;
																								BgL_tail1070z00_5964 =
																									BgL_newtail1071z00_1887;
																								BgL_tail1070z00_1884 =
																									BgL_tail1070z00_5964;
																								BgL_l1067z00_1883 =
																									BgL_l1067z00_5963;
																								goto
																									BgL_zc3z04anonymousza32931ze3z87_1885;
																							}
																						}
																					}
																			}
																		}
																	return
																		BGl_replacez12z12zztools_miscz00
																		(BgL_xz00_43,
																		BGl_epairifyzd2reczd2zztools_miscz00
																		(BgL_arg2929z00_1878, BgL_xz00_43));
																}
															}
													}
												else
													{	/* Expand/iarith.scm 357 */
														goto BgL_tagzd22564zd2_1828;
													}
											}
										else
											{	/* Expand/iarith.scm 357 */
												goto BgL_tagzd22564zd2_1828;
											}
									}
								else
									{	/* Expand/iarith.scm 357 */
										goto BgL_tagzd22564zd2_1828;
									}
							}
						else
							{	/* Expand/iarith.scm 357 */
								goto BgL_tagzd22564zd2_1828;
							}
					}
				else
					{	/* Expand/iarith.scm 357 */
						goto BgL_tagzd22564zd2_1828;
					}
			}
		}

	}



/* &expand-bit-rshs32 */
	obj_t BGl_z62expandzd2bitzd2rshs32z62zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3100, obj_t BgL_xz00_3101, obj_t BgL_ez00_3102)
	{
		{	/* Expand/iarith.scm 357 */
			return
				BGl_expandzd2bitzd2rshs32z00zzexpand_iarithmetiquez00(BgL_xz00_3101,
				BgL_ez00_3102);
		}

	}



/* expand-bit-rshu32 */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2bitzd2rshu32z00zzexpand_iarithmetiquez00(obj_t BgL_xz00_45,
		obj_t BgL_ez00_46)
	{
		{	/* Expand/iarith.scm 358 */
			{
				obj_t BgL_nz00_1892;
				obj_t BgL_shiftz00_1893;

				if (PAIRP(BgL_xz00_45))
					{	/* Expand/iarith.scm 358 */
						obj_t BgL_cdrzd22591zd2_1898;

						BgL_cdrzd22591zd2_1898 = CDR(((obj_t) BgL_xz00_45));
						if (PAIRP(BgL_cdrzd22591zd2_1898))
							{	/* Expand/iarith.scm 358 */
								obj_t BgL_carzd22594zd2_1900;
								obj_t BgL_cdrzd22595zd2_1901;

								BgL_carzd22594zd2_1900 = CAR(BgL_cdrzd22591zd2_1898);
								BgL_cdrzd22595zd2_1901 = CDR(BgL_cdrzd22591zd2_1898);
								if (BGL_UINT32P(BgL_carzd22594zd2_1900))
									{	/* Expand/iarith.scm 358 */
										if (PAIRP(BgL_cdrzd22595zd2_1901))
											{	/* Expand/iarith.scm 358 */
												obj_t BgL_carzd22599zd2_1904;

												BgL_carzd22599zd2_1904 = CAR(BgL_cdrzd22595zd2_1901);
												if (INTEGERP(BgL_carzd22599zd2_1904))
													{	/* Expand/iarith.scm 358 */
														if (NULLP(CDR(BgL_cdrzd22595zd2_1901)))
															{	/* Expand/iarith.scm 358 */
																BgL_nz00_1892 = BgL_carzd22594zd2_1900;
																BgL_shiftz00_1893 = BgL_carzd22599zd2_1904;
																{	/* Expand/iarith.scm 358 */
																	bool_t BgL_test3494z00_5986;

																	if (((long) CINT(BgL_shiftz00_1893) <= 31L))
																		{	/* Expand/iarith.scm 358 */
																			if (BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																				(BgL_nz00_1892, BINT(0L)))
																				{	/* Expand/iarith.scm 358 */
																					bool_t BgL_test3497z00_5993;

																					{	/* Expand/iarith.scm 358 */
																						uint32_t BgL_a1072z00_1942;

																						{	/* Expand/iarith.scm 358 */
																							uint32_t BgL_xz00_2965;
																							long BgL_yz00_2966;

																							BgL_xz00_2965 =
																								BGL_BUINT32_TO_UINT32
																								(BgL_nz00_1892);
																							BgL_yz00_2966 =
																								(long) CINT(BgL_shiftz00_1893);
																							BgL_a1072z00_1942 =
																								(BgL_xz00_2965 >>
																								(int) (BgL_yz00_2966));
																						}
																						{	/* Expand/iarith.scm 358 */

																							BgL_test3497z00_5993 =
																								BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																								(BGL_UINT32_TO_BUINT32
																								(BgL_a1072z00_1942), BINT(0L));
																					}}
																					if (BgL_test3497z00_5993)
																						{	/* Expand/iarith.scm 358 */
																							uint32_t BgL_a1074z00_1939;

																							{	/* Expand/iarith.scm 358 */
																								uint32_t BgL_xz00_2969;
																								long BgL_yz00_2970;

																								BgL_xz00_2969 =
																									BGL_BUINT32_TO_UINT32
																									(BgL_nz00_1892);
																								BgL_yz00_2970 =
																									(long)
																									CINT(BgL_shiftz00_1893);
																								BgL_a1074z00_1939 =
																									(BgL_xz00_2969 >>
																									(int) (BgL_yz00_2970));
																							}
																							{	/* Expand/iarith.scm 358 */

																								BgL_test3494z00_5986 =
																									BGl_2zc3zd3z10zz__r4_numbers_6_5z00
																									(BGL_UINT32_TO_BUINT32
																									(BgL_a1074z00_1939),
																									BINT(268435456L));
																						}}
																					else
																						{	/* Expand/iarith.scm 358 */
																							BgL_test3494z00_5986 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 358 */
																					BgL_test3494z00_5986 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 358 */
																			BgL_test3494z00_5986 = ((bool_t) 0);
																		}
																	if (BgL_test3494z00_5986)
																		{	/* Expand/iarith.scm 358 */
																			uint32_t BgL_xz00_2973;
																			long BgL_yz00_2974;

																			BgL_xz00_2973 =
																				BGL_BUINT32_TO_UINT32(BgL_nz00_1892);
																			BgL_yz00_2974 =
																				(long) CINT(BgL_shiftz00_1893);
																			{	/* Expand/iarith.scm 358 */
																				uint32_t BgL_tmpz00_6010;

																				BgL_tmpz00_6010 =
																					(BgL_xz00_2973 >>
																					(int) (BgL_yz00_2974));
																				return
																					BGL_UINT32_TO_BUINT32
																					(BgL_tmpz00_6010);
																			}
																		}
																	else
																		{	/* Expand/iarith.scm 358 */
																			obj_t BgL_arg2954z00_1922;

																			if (NULLP(BgL_xz00_45))
																				{	/* Expand/iarith.scm 358 */
																					BgL_arg2954z00_1922 = BNIL;
																				}
																			else
																				{	/* Expand/iarith.scm 358 */
																					obj_t BgL_head1078z00_1925;

																					BgL_head1078z00_1925 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1076z00_1927;
																						obj_t BgL_tail1079z00_1928;

																						BgL_l1076z00_1927 = BgL_xz00_45;
																						BgL_tail1079z00_1928 =
																							BgL_head1078z00_1925;
																					BgL_zc3z04anonymousza32956ze3z87_1929:
																						if (NULLP
																							(BgL_l1076z00_1927))
																							{	/* Expand/iarith.scm 358 */
																								BgL_arg2954z00_1922 =
																									CDR(BgL_head1078z00_1925);
																							}
																						else
																							{	/* Expand/iarith.scm 358 */
																								obj_t BgL_newtail1080z00_1931;

																								{	/* Expand/iarith.scm 358 */
																									obj_t BgL_arg2967z00_1933;

																									{	/* Expand/iarith.scm 358 */
																										obj_t BgL_xz00_1934;

																										BgL_xz00_1934 =
																											CAR(
																											((obj_t)
																												BgL_l1076z00_1927));
																										BgL_arg2967z00_1933 =
																											BGL_PROCEDURE_CALL2
																											(BgL_ez00_46,
																											BgL_xz00_1934,
																											BgL_ez00_46);
																									}
																									BgL_newtail1080z00_1931 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2967z00_1933, BNIL);
																								}
																								SET_CDR(BgL_tail1079z00_1928,
																									BgL_newtail1080z00_1931);
																								{	/* Expand/iarith.scm 358 */
																									obj_t BgL_arg2966z00_1932;

																									BgL_arg2966z00_1932 =
																										CDR(
																										((obj_t)
																											BgL_l1076z00_1927));
																									{
																										obj_t BgL_tail1079z00_6032;
																										obj_t BgL_l1076z00_6031;

																										BgL_l1076z00_6031 =
																											BgL_arg2966z00_1932;
																										BgL_tail1079z00_6032 =
																											BgL_newtail1080z00_1931;
																										BgL_tail1079z00_1928 =
																											BgL_tail1079z00_6032;
																										BgL_l1076z00_1927 =
																											BgL_l1076z00_6031;
																										goto
																											BgL_zc3z04anonymousza32956ze3z87_1929;
																									}
																								}
																							}
																					}
																				}
																			return
																				BGl_replacez12z12zztools_miscz00
																				(BgL_xz00_45,
																				BGl_epairifyzd2reczd2zztools_miscz00
																				(BgL_arg2954z00_1922, BgL_xz00_45));
																		}
																}
															}
														else
															{	/* Expand/iarith.scm 358 */
															BgL_tagzd22584zd2_1895:
																{	/* Expand/iarith.scm 358 */
																	obj_t BgL_arg2968z00_1945;

																	if (NULLP(BgL_xz00_45))
																		{	/* Expand/iarith.scm 358 */
																			BgL_arg2968z00_1945 = BNIL;
																		}
																	else
																		{	/* Expand/iarith.scm 358 */
																			obj_t BgL_head1083z00_1948;

																			BgL_head1083z00_1948 =
																				MAKE_YOUNG_PAIR(BNIL, BNIL);
																			{
																				obj_t BgL_l1081z00_1950;
																				obj_t BgL_tail1084z00_1951;

																				BgL_l1081z00_1950 = BgL_xz00_45;
																				BgL_tail1084z00_1951 =
																					BgL_head1083z00_1948;
																			BgL_zc3z04anonymousza32970ze3z87_1952:
																				if (NULLP(BgL_l1081z00_1950))
																					{	/* Expand/iarith.scm 358 */
																						BgL_arg2968z00_1945 =
																							CDR(BgL_head1083z00_1948);
																					}
																				else
																					{	/* Expand/iarith.scm 358 */
																						obj_t BgL_newtail1085z00_1954;

																						{	/* Expand/iarith.scm 358 */
																							obj_t BgL_arg2973z00_1956;

																							{	/* Expand/iarith.scm 358 */
																								obj_t BgL_xz00_1957;

																								BgL_xz00_1957 =
																									CAR(
																									((obj_t) BgL_l1081z00_1950));
																								BgL_arg2973z00_1956 =
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_46, BgL_xz00_1957,
																									BgL_ez00_46);
																							}
																							BgL_newtail1085z00_1954 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2973z00_1956, BNIL);
																						}
																						SET_CDR(BgL_tail1084z00_1951,
																							BgL_newtail1085z00_1954);
																						{	/* Expand/iarith.scm 358 */
																							obj_t BgL_arg2972z00_1955;

																							BgL_arg2972z00_1955 =
																								CDR(
																								((obj_t) BgL_l1081z00_1950));
																							{
																								obj_t BgL_tail1084z00_6053;
																								obj_t BgL_l1081z00_6052;

																								BgL_l1081z00_6052 =
																									BgL_arg2972z00_1955;
																								BgL_tail1084z00_6053 =
																									BgL_newtail1085z00_1954;
																								BgL_tail1084z00_1951 =
																									BgL_tail1084z00_6053;
																								BgL_l1081z00_1950 =
																									BgL_l1081z00_6052;
																								goto
																									BgL_zc3z04anonymousza32970ze3z87_1952;
																							}
																						}
																					}
																			}
																		}
																	return
																		BGl_replacez12z12zztools_miscz00
																		(BgL_xz00_45,
																		BGl_epairifyzd2reczd2zztools_miscz00
																		(BgL_arg2968z00_1945, BgL_xz00_45));
																}
															}
													}
												else
													{	/* Expand/iarith.scm 358 */
														goto BgL_tagzd22584zd2_1895;
													}
											}
										else
											{	/* Expand/iarith.scm 358 */
												goto BgL_tagzd22584zd2_1895;
											}
									}
								else
									{	/* Expand/iarith.scm 358 */
										goto BgL_tagzd22584zd2_1895;
									}
							}
						else
							{	/* Expand/iarith.scm 358 */
								goto BgL_tagzd22584zd2_1895;
							}
					}
				else
					{	/* Expand/iarith.scm 358 */
						goto BgL_tagzd22584zd2_1895;
					}
			}
		}

	}



/* &expand-bit-rshu32 */
	obj_t BGl_z62expandzd2bitzd2rshu32z62zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3103, obj_t BgL_xz00_3104, obj_t BgL_ez00_3105)
	{
		{	/* Expand/iarith.scm 358 */
			return
				BGl_expandzd2bitzd2rshu32z00zzexpand_iarithmetiquez00(BgL_xz00_3104,
				BgL_ez00_3105);
		}

	}



/* expand-bit-ursh */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2bitzd2urshz00zzexpand_iarithmetiquez00(obj_t BgL_xz00_47,
		obj_t BgL_ez00_48)
	{
		{	/* Expand/iarith.scm 360 */
			if (NULLP(BgL_xz00_47))
				{	/* Expand/iarith.scm 362 */
					return BNIL;
				}
			else
				{	/* Expand/iarith.scm 362 */
					obj_t BgL_head1088z00_1961;

					BgL_head1088z00_1961 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1086z00_1963;
						obj_t BgL_tail1089z00_1964;

						BgL_l1086z00_1963 = BgL_xz00_47;
						BgL_tail1089z00_1964 = BgL_head1088z00_1961;
					BgL_zc3z04anonymousza32975ze3z87_1965:
						if (NULLP(BgL_l1086z00_1963))
							{	/* Expand/iarith.scm 362 */
								return CDR(BgL_head1088z00_1961);
							}
						else
							{	/* Expand/iarith.scm 362 */
								obj_t BgL_newtail1090z00_1967;

								{	/* Expand/iarith.scm 362 */
									obj_t BgL_arg2978z00_1969;

									{	/* Expand/iarith.scm 362 */
										obj_t BgL_xz00_1970;

										BgL_xz00_1970 = CAR(((obj_t) BgL_l1086z00_1963));
										BgL_arg2978z00_1969 =
											BGL_PROCEDURE_CALL2(BgL_ez00_48, BgL_xz00_1970,
											BgL_ez00_48);
									}
									BgL_newtail1090z00_1967 =
										MAKE_YOUNG_PAIR(BgL_arg2978z00_1969, BNIL);
								}
								SET_CDR(BgL_tail1089z00_1964, BgL_newtail1090z00_1967);
								{	/* Expand/iarith.scm 362 */
									obj_t BgL_arg2977z00_1968;

									BgL_arg2977z00_1968 = CDR(((obj_t) BgL_l1086z00_1963));
									{
										obj_t BgL_tail1089z00_6075;
										obj_t BgL_l1086z00_6074;

										BgL_l1086z00_6074 = BgL_arg2977z00_1968;
										BgL_tail1089z00_6075 = BgL_newtail1090z00_1967;
										BgL_tail1089z00_1964 = BgL_tail1089z00_6075;
										BgL_l1086z00_1963 = BgL_l1086z00_6074;
										goto BgL_zc3z04anonymousza32975ze3z87_1965;
									}
								}
							}
					}
				}
		}

	}



/* &expand-bit-ursh */
	obj_t BGl_z62expandzd2bitzd2urshz62zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3106, obj_t BgL_xz00_3107, obj_t BgL_ez00_3108)
	{
		{	/* Expand/iarith.scm 360 */
			return
				BGl_expandzd2bitzd2urshz00zzexpand_iarithmetiquez00(BgL_xz00_3107,
				BgL_ez00_3108);
		}

	}



/* expand-bit-urshu32 */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2bitzd2urshu32z00zzexpand_iarithmetiquez00(obj_t BgL_xz00_49,
		obj_t BgL_ez00_50)
	{
		{	/* Expand/iarith.scm 363 */
			{
				obj_t BgL_nz00_1972;
				obj_t BgL_shiftz00_1973;

				if (PAIRP(BgL_xz00_49))
					{	/* Expand/iarith.scm 363 */
						obj_t BgL_cdrzd22611zd2_1978;

						BgL_cdrzd22611zd2_1978 = CDR(((obj_t) BgL_xz00_49));
						if (PAIRP(BgL_cdrzd22611zd2_1978))
							{	/* Expand/iarith.scm 363 */
								obj_t BgL_carzd22614zd2_1980;
								obj_t BgL_cdrzd22615zd2_1981;

								BgL_carzd22614zd2_1980 = CAR(BgL_cdrzd22611zd2_1978);
								BgL_cdrzd22615zd2_1981 = CDR(BgL_cdrzd22611zd2_1978);
								if (BGL_UINT32P(BgL_carzd22614zd2_1980))
									{	/* Expand/iarith.scm 363 */
										if (PAIRP(BgL_cdrzd22615zd2_1981))
											{	/* Expand/iarith.scm 363 */
												obj_t BgL_carzd22619zd2_1984;

												BgL_carzd22619zd2_1984 = CAR(BgL_cdrzd22615zd2_1981);
												if (INTEGERP(BgL_carzd22619zd2_1984))
													{	/* Expand/iarith.scm 363 */
														if (NULLP(CDR(BgL_cdrzd22615zd2_1981)))
															{	/* Expand/iarith.scm 363 */
																BgL_nz00_1972 = BgL_carzd22614zd2_1980;
																BgL_shiftz00_1973 = BgL_carzd22619zd2_1984;
																{	/* Expand/iarith.scm 363 */
																	bool_t BgL_test3510z00_6095;

																	if (((long) CINT(BgL_shiftz00_1973) <= 31L))
																		{	/* Expand/iarith.scm 363 */
																			if (BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																				(BgL_nz00_1972, BINT(0L)))
																				{	/* Expand/iarith.scm 363 */
																					bool_t BgL_test3513z00_6102;

																					{	/* Expand/iarith.scm 363 */
																						uint32_t BgL_a1091z00_2022;

																						{	/* Expand/iarith.scm 363 */
																							uint32_t BgL_xz00_2996;
																							long BgL_yz00_2997;

																							BgL_xz00_2996 =
																								BGL_BUINT32_TO_UINT32
																								(BgL_nz00_1972);
																							BgL_yz00_2997 =
																								(long) CINT(BgL_shiftz00_1973);
																							BgL_a1091z00_2022 =
																								(BgL_xz00_2996 >>
																								(int) (BgL_yz00_2997));
																						}
																						{	/* Expand/iarith.scm 363 */

																							BgL_test3513z00_6102 =
																								BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																								(BGL_UINT32_TO_BUINT32
																								(BgL_a1091z00_2022), BINT(0L));
																					}}
																					if (BgL_test3513z00_6102)
																						{	/* Expand/iarith.scm 363 */
																							uint32_t BgL_a1093z00_2019;

																							{	/* Expand/iarith.scm 363 */
																								uint32_t BgL_xz00_3000;
																								long BgL_yz00_3001;

																								BgL_xz00_3000 =
																									BGL_BUINT32_TO_UINT32
																									(BgL_nz00_1972);
																								BgL_yz00_3001 =
																									(long)
																									CINT(BgL_shiftz00_1973);
																								BgL_a1093z00_2019 =
																									(BgL_xz00_3000 >>
																									(int) (BgL_yz00_3001));
																							}
																							{	/* Expand/iarith.scm 363 */

																								BgL_test3510z00_6095 =
																									BGl_2zc3zd3z10zz__r4_numbers_6_5z00
																									(BGL_UINT32_TO_BUINT32
																									(BgL_a1093z00_2019),
																									BINT(268435456L));
																						}}
																					else
																						{	/* Expand/iarith.scm 363 */
																							BgL_test3510z00_6095 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Expand/iarith.scm 363 */
																					BgL_test3510z00_6095 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Expand/iarith.scm 363 */
																			BgL_test3510z00_6095 = ((bool_t) 0);
																		}
																	if (BgL_test3510z00_6095)
																		{	/* Expand/iarith.scm 363 */
																			uint32_t BgL_xz00_3004;
																			long BgL_yz00_3005;

																			BgL_xz00_3004 =
																				BGL_BUINT32_TO_UINT32(BgL_nz00_1972);
																			BgL_yz00_3005 =
																				(long) CINT(BgL_shiftz00_1973);
																			{	/* Expand/iarith.scm 363 */
																				uint32_t BgL_tmpz00_6119;

																				BgL_tmpz00_6119 =
																					(BgL_xz00_3004 >>
																					(int) (BgL_yz00_3005));
																				return
																					BGL_UINT32_TO_BUINT32
																					(BgL_tmpz00_6119);
																			}
																		}
																	else
																		{	/* Expand/iarith.scm 363 */
																			obj_t BgL_arg2994z00_2002;

																			if (NULLP(BgL_xz00_49))
																				{	/* Expand/iarith.scm 363 */
																					BgL_arg2994z00_2002 = BNIL;
																				}
																			else
																				{	/* Expand/iarith.scm 363 */
																					obj_t BgL_head1097z00_2005;

																					BgL_head1097z00_2005 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1095z00_2007;
																						obj_t BgL_tail1098z00_2008;

																						BgL_l1095z00_2007 = BgL_xz00_49;
																						BgL_tail1098z00_2008 =
																							BgL_head1097z00_2005;
																					BgL_zc3z04anonymousza32996ze3z87_2009:
																						if (NULLP
																							(BgL_l1095z00_2007))
																							{	/* Expand/iarith.scm 363 */
																								BgL_arg2994z00_2002 =
																									CDR(BgL_head1097z00_2005);
																							}
																						else
																							{	/* Expand/iarith.scm 363 */
																								obj_t BgL_newtail1099z00_2011;

																								{	/* Expand/iarith.scm 363 */
																									obj_t BgL_arg3000z00_2013;

																									{	/* Expand/iarith.scm 363 */
																										obj_t BgL_xz00_2014;

																										BgL_xz00_2014 =
																											CAR(
																											((obj_t)
																												BgL_l1095z00_2007));
																										BgL_arg3000z00_2013 =
																											BGL_PROCEDURE_CALL2
																											(BgL_ez00_50,
																											BgL_xz00_2014,
																											BgL_ez00_50);
																									}
																									BgL_newtail1099z00_2011 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg3000z00_2013, BNIL);
																								}
																								SET_CDR(BgL_tail1098z00_2008,
																									BgL_newtail1099z00_2011);
																								{	/* Expand/iarith.scm 363 */
																									obj_t BgL_arg2999z00_2012;

																									BgL_arg2999z00_2012 =
																										CDR(
																										((obj_t)
																											BgL_l1095z00_2007));
																									{
																										obj_t BgL_tail1098z00_6141;
																										obj_t BgL_l1095z00_6140;

																										BgL_l1095z00_6140 =
																											BgL_arg2999z00_2012;
																										BgL_tail1098z00_6141 =
																											BgL_newtail1099z00_2011;
																										BgL_tail1098z00_2008 =
																											BgL_tail1098z00_6141;
																										BgL_l1095z00_2007 =
																											BgL_l1095z00_6140;
																										goto
																											BgL_zc3z04anonymousza32996ze3z87_2009;
																									}
																								}
																							}
																					}
																				}
																			return
																				BGl_replacez12z12zztools_miscz00
																				(BgL_xz00_49,
																				BGl_epairifyzd2reczd2zztools_miscz00
																				(BgL_arg2994z00_2002, BgL_xz00_49));
																		}
																}
															}
														else
															{	/* Expand/iarith.scm 363 */
															BgL_tagzd22604zd2_1975:
																{	/* Expand/iarith.scm 363 */
																	obj_t BgL_arg3001z00_2025;

																	if (NULLP(BgL_xz00_49))
																		{	/* Expand/iarith.scm 363 */
																			BgL_arg3001z00_2025 = BNIL;
																		}
																	else
																		{	/* Expand/iarith.scm 363 */
																			obj_t BgL_head1102z00_2028;

																			BgL_head1102z00_2028 =
																				MAKE_YOUNG_PAIR(BNIL, BNIL);
																			{
																				obj_t BgL_l1100z00_2030;
																				obj_t BgL_tail1103z00_2031;

																				BgL_l1100z00_2030 = BgL_xz00_49;
																				BgL_tail1103z00_2031 =
																					BgL_head1102z00_2028;
																			BgL_zc3z04anonymousza33003ze3z87_2032:
																				if (NULLP(BgL_l1100z00_2030))
																					{	/* Expand/iarith.scm 363 */
																						BgL_arg3001z00_2025 =
																							CDR(BgL_head1102z00_2028);
																					}
																				else
																					{	/* Expand/iarith.scm 363 */
																						obj_t BgL_newtail1104z00_2034;

																						{	/* Expand/iarith.scm 363 */
																							obj_t BgL_arg3009z00_2036;

																							{	/* Expand/iarith.scm 363 */
																								obj_t BgL_xz00_2037;

																								BgL_xz00_2037 =
																									CAR(
																									((obj_t) BgL_l1100z00_2030));
																								BgL_arg3009z00_2036 =
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_50, BgL_xz00_2037,
																									BgL_ez00_50);
																							}
																							BgL_newtail1104z00_2034 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg3009z00_2036, BNIL);
																						}
																						SET_CDR(BgL_tail1103z00_2031,
																							BgL_newtail1104z00_2034);
																						{	/* Expand/iarith.scm 363 */
																							obj_t BgL_arg3008z00_2035;

																							BgL_arg3008z00_2035 =
																								CDR(
																								((obj_t) BgL_l1100z00_2030));
																							{
																								obj_t BgL_tail1103z00_6162;
																								obj_t BgL_l1100z00_6161;

																								BgL_l1100z00_6161 =
																									BgL_arg3008z00_2035;
																								BgL_tail1103z00_6162 =
																									BgL_newtail1104z00_2034;
																								BgL_tail1103z00_2031 =
																									BgL_tail1103z00_6162;
																								BgL_l1100z00_2030 =
																									BgL_l1100z00_6161;
																								goto
																									BgL_zc3z04anonymousza33003ze3z87_2032;
																							}
																						}
																					}
																			}
																		}
																	return
																		BGl_replacez12z12zztools_miscz00
																		(BgL_xz00_49,
																		BGl_epairifyzd2reczd2zztools_miscz00
																		(BgL_arg3001z00_2025, BgL_xz00_49));
																}
															}
													}
												else
													{	/* Expand/iarith.scm 363 */
														goto BgL_tagzd22604zd2_1975;
													}
											}
										else
											{	/* Expand/iarith.scm 363 */
												goto BgL_tagzd22604zd2_1975;
											}
									}
								else
									{	/* Expand/iarith.scm 363 */
										goto BgL_tagzd22604zd2_1975;
									}
							}
						else
							{	/* Expand/iarith.scm 363 */
								goto BgL_tagzd22604zd2_1975;
							}
					}
				else
					{	/* Expand/iarith.scm 363 */
						goto BgL_tagzd22604zd2_1975;
					}
			}
		}

	}



/* &expand-bit-urshu32 */
	obj_t BGl_z62expandzd2bitzd2urshu32z62zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3109, obj_t BgL_xz00_3110, obj_t BgL_ez00_3111)
	{
		{	/* Expand/iarith.scm 363 */
			return
				BGl_expandzd2bitzd2urshu32z00zzexpand_iarithmetiquez00(BgL_xz00_3110,
				BgL_ez00_3111);
		}

	}



/* expand-bit-ors32 */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2bitzd2ors32z00zzexpand_iarithmetiquez00(obj_t BgL_xz00_51,
		obj_t BgL_ez00_52)
	{
		{	/* Expand/iarith.scm 364 */
			{

				if (PAIRP(BgL_xz00_51))
					{	/* Expand/iarith.scm 365 */
						obj_t BgL_cdrzd22631zd2_2045;

						BgL_cdrzd22631zd2_2045 = CDR(((obj_t) BgL_xz00_51));
						if (PAIRP(BgL_cdrzd22631zd2_2045))
							{	/* Expand/iarith.scm 365 */
								obj_t BgL_carzd22634zd2_2047;
								obj_t BgL_cdrzd22635zd2_2048;

								BgL_carzd22634zd2_2047 = CAR(BgL_cdrzd22631zd2_2045);
								BgL_cdrzd22635zd2_2048 = CDR(BgL_cdrzd22631zd2_2045);
								if (PAIRP(BgL_carzd22634zd2_2047))
									{	/* Expand/iarith.scm 365 */
										obj_t BgL_cdrzd22638zd2_2050;

										BgL_cdrzd22638zd2_2050 = CDR(BgL_carzd22634zd2_2047);
										if (PAIRP(BgL_cdrzd22638zd2_2050))
											{	/* Expand/iarith.scm 365 */
												bool_t BgL_test3522z00_6179;

												{	/* Expand/iarith.scm 365 */
													obj_t BgL_tmpz00_6180;

													BgL_tmpz00_6180 = CAR(BgL_cdrzd22638zd2_2050);
													BgL_test3522z00_6179 = BGL_INT32P(BgL_tmpz00_6180);
												}
												if (BgL_test3522z00_6179)
													{	/* Expand/iarith.scm 365 */
														if (NULLP(CDR(BgL_cdrzd22638zd2_2050)))
															{	/* Expand/iarith.scm 365 */
																if (PAIRP(BgL_cdrzd22635zd2_2048))
																	{	/* Expand/iarith.scm 365 */
																		obj_t BgL_carzd22643zd2_2057;

																		BgL_carzd22643zd2_2057 =
																			CAR(BgL_cdrzd22635zd2_2048);
																		if (PAIRP(BgL_carzd22643zd2_2057))
																			{	/* Expand/iarith.scm 365 */
																				obj_t BgL_cdrzd22647zd2_2059;

																				BgL_cdrzd22647zd2_2059 =
																					CDR(BgL_carzd22643zd2_2057);
																				if (PAIRP(BgL_cdrzd22647zd2_2059))
																					{	/* Expand/iarith.scm 365 */
																						bool_t BgL_test3527z00_6194;

																						{	/* Expand/iarith.scm 365 */
																							obj_t BgL_tmpz00_6195;

																							BgL_tmpz00_6195 =
																								CAR(BgL_cdrzd22647zd2_2059);
																							BgL_test3527z00_6194 =
																								BGL_INT32P(BgL_tmpz00_6195);
																						}
																						if (BgL_test3527z00_6194)
																							{	/* Expand/iarith.scm 365 */
																								if (NULLP(CDR
																										(BgL_cdrzd22647zd2_2059)))
																									{	/* Expand/iarith.scm 365 */
																										if (NULLP(CDR
																												(BgL_cdrzd22635zd2_2048)))
																											{	/* Expand/iarith.scm 367 */
																												int32_t BgL_xz00_3041;
																												int32_t BgL_yz00_3042;

																												{	/* Expand/iarith.scm 365 */
																													obj_t BgL_tmpz00_6204;

																													BgL_tmpz00_6204 =
																														CAR
																														(BgL_carzd22634zd2_2047);
																													BgL_xz00_3041 =
																														BGL_BINT32_TO_INT32
																														(BgL_tmpz00_6204);
																												}
																												{	/* Expand/iarith.scm 365 */
																													obj_t BgL_tmpz00_6207;

																													BgL_tmpz00_6207 =
																														CAR
																														(BgL_carzd22643zd2_2057);
																													BgL_yz00_3042 =
																														BGL_BINT32_TO_INT32
																														(BgL_tmpz00_6207);
																												}
																												{	/* Expand/iarith.scm 367 */
																													int32_t
																														BgL_tmpz00_6210;
																													BgL_tmpz00_6210 =
																														(BgL_xz00_3041 |
																														BgL_yz00_3042);
																													return
																														BGL_INT32_TO_BINT32
																														(BgL_tmpz00_6210);
																												}
																											}
																										else
																											{	/* Expand/iarith.scm 365 */
																											BgL_tagzd22624zd2_2042:
																												{	/* Expand/iarith.scm 369 */
																													obj_t
																														BgL_arg3043z00_2074;
																													if (NULLP
																														(BgL_xz00_51))
																														{	/* Expand/iarith.scm 369 */
																															BgL_arg3043z00_2074
																																= BNIL;
																														}
																													else
																														{	/* Expand/iarith.scm 369 */
																															obj_t
																																BgL_head1107z00_2077;
																															BgL_head1107z00_2077
																																=
																																MAKE_YOUNG_PAIR
																																(BNIL, BNIL);
																															{
																																obj_t
																																	BgL_l1105z00_2079;
																																obj_t
																																	BgL_tail1108z00_2080;
																																BgL_l1105z00_2079
																																	= BgL_xz00_51;
																																BgL_tail1108z00_2080
																																	=
																																	BgL_head1107z00_2077;
																															BgL_zc3z04anonymousza33045ze3z87_2081:
																																if (NULLP
																																	(BgL_l1105z00_2079))
																																	{	/* Expand/iarith.scm 369 */
																																		BgL_arg3043z00_2074
																																			=
																																			CDR
																																			(BgL_head1107z00_2077);
																																	}
																																else
																																	{	/* Expand/iarith.scm 369 */
																																		obj_t
																																			BgL_newtail1109z00_2083;
																																		{	/* Expand/iarith.scm 369 */
																																			obj_t
																																				BgL_arg3050z00_2085;
																																			{	/* Expand/iarith.scm 369 */
																																				obj_t
																																					BgL_xz00_2086;
																																				BgL_xz00_2086
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_l1105z00_2079));
																																				BgL_arg3050z00_2085
																																					=
																																					BGL_PROCEDURE_CALL2
																																					(BgL_ez00_52,
																																					BgL_xz00_2086,
																																					BgL_ez00_52);
																																			}
																																			BgL_newtail1109z00_2083
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg3050z00_2085,
																																				BNIL);
																																		}
																																		SET_CDR
																																			(BgL_tail1108z00_2080,
																																			BgL_newtail1109z00_2083);
																																		{	/* Expand/iarith.scm 369 */
																																			obj_t
																																				BgL_arg3049z00_2084;
																																			BgL_arg3049z00_2084
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_l1105z00_2079));
																																			{
																																				obj_t
																																					BgL_tail1108z00_6231;
																																				obj_t
																																					BgL_l1105z00_6230;
																																				BgL_l1105z00_6230
																																					=
																																					BgL_arg3049z00_2084;
																																				BgL_tail1108z00_6231
																																					=
																																					BgL_newtail1109z00_2083;
																																				BgL_tail1108z00_2080
																																					=
																																					BgL_tail1108z00_6231;
																																				BgL_l1105z00_2079
																																					=
																																					BgL_l1105z00_6230;
																																				goto
																																					BgL_zc3z04anonymousza33045ze3z87_2081;
																																			}
																																		}
																																	}
																															}
																														}
																													return
																														BGl_replacez12z12zztools_miscz00
																														(BgL_xz00_51,
																														BGl_epairifyzd2reczd2zztools_miscz00
																														(BgL_arg3043z00_2074,
																															BgL_xz00_51));
																												}
																											}
																									}
																								else
																									{	/* Expand/iarith.scm 365 */
																										goto BgL_tagzd22624zd2_2042;
																									}
																							}
																						else
																							{	/* Expand/iarith.scm 365 */
																								goto BgL_tagzd22624zd2_2042;
																							}
																					}
																				else
																					{	/* Expand/iarith.scm 365 */
																						goto BgL_tagzd22624zd2_2042;
																					}
																			}
																		else
																			{	/* Expand/iarith.scm 365 */
																				goto BgL_tagzd22624zd2_2042;
																			}
																	}
																else
																	{	/* Expand/iarith.scm 365 */
																		goto BgL_tagzd22624zd2_2042;
																	}
															}
														else
															{	/* Expand/iarith.scm 365 */
																goto BgL_tagzd22624zd2_2042;
															}
													}
												else
													{	/* Expand/iarith.scm 365 */
														goto BgL_tagzd22624zd2_2042;
													}
											}
										else
											{	/* Expand/iarith.scm 365 */
												goto BgL_tagzd22624zd2_2042;
											}
									}
								else
									{	/* Expand/iarith.scm 365 */
										goto BgL_tagzd22624zd2_2042;
									}
							}
						else
							{	/* Expand/iarith.scm 365 */
								goto BgL_tagzd22624zd2_2042;
							}
					}
				else
					{	/* Expand/iarith.scm 365 */
						goto BgL_tagzd22624zd2_2042;
					}
			}
		}

	}



/* &expand-bit-ors32 */
	obj_t BGl_z62expandzd2bitzd2ors32z62zzexpand_iarithmetiquez00(obj_t
		BgL_envz00_3112, obj_t BgL_xz00_3113, obj_t BgL_ez00_3114)
	{
		{	/* Expand/iarith.scm 364 */
			return
				BGl_expandzd2bitzd2ors32z00zzexpand_iarithmetiquez00(BgL_xz00_3113,
				BgL_ez00_3114);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_iarithmetiquez00(void)
	{
		{	/* Expand/iarith.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_iarithmetiquez00(void)
	{
		{	/* Expand/iarith.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_iarithmetiquez00(void)
	{
		{	/* Expand/iarith.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_iarithmetiquez00(void)
	{
		{	/* Expand/iarith.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string3066z00zzexpand_iarithmetiquez00));
			return
				BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string3066z00zzexpand_iarithmetiquez00));
		}

	}

#ifdef __cplusplus
}
#endif
