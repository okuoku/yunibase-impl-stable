/*===========================================================================*/
/*   (Expand/initial.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/initial.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_INSTALL_TYPE_DEFINITIONS
#define BGL_EXPAND_INSTALL_TYPE_DEFINITIONS
#endif													// BGL_EXPAND_INSTALL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_expandzd2gzc3zd3zc2zzexpand_garithmetiquez00(obj_t, obj_t);
	static bool_t BGl_installedpz00zzexpand_installz00;
	static obj_t BGl_z62zc3z04anonymousza32745ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31846ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31676ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32567ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	extern obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_installz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31871ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32673ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33483ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33467ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t
		BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32101ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_expandzd2gze3zd3ze2zzexpand_garithmetiquez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_z52appendzd22zd2definez52zzexpand_installz00(void);
	BGL_IMPORT obj_t BGl_installzd2compilerzd2expanderz00zz__macroz00(obj_t,
		obj_t);
	extern obj_t BGl_privatezd2stampzd2zzast_privatez00(void);
	static obj_t BGl_z62zc3z04anonymousza32691ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ifzd2symzd2zzast_sexpz00(void);
	static obj_t BGl_z62zc3z04anonymousza32111ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32103ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzexpand_installz00(void);
	extern obj_t BGl_expandzd2izc3zd3zc2zzexpand_iarithmetiquez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_installzd2initialzd2expanderz00zzexpand_installz00(void);
	static obj_t BGl_z62zc3z04anonymousza32927ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_epairifyz00zztools_miscz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31883ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32847ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_installz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32121ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32113ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32105ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_expandzd2ize3zd3ze2zzexpand_iarithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_expandzd2gminzd2zzexpand_garithmetiquez00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_installz00(void);
	static obj_t BGl_z62zc3z04anonymousza32130ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33021ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_labelszd2symzd2zzast_labelsz00(void);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32123ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32115ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32107ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62z52appendzd22zd2definez30zzexpand_installz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32777ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_expandzd2gza2z70zzexpand_garithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_expandzd2gzb2z60zzexpand_garithmetiquez00(obj_t, obj_t);
	extern long BGl_localzd2arityzd2zztools_argsz00(obj_t);
	extern obj_t BGl_expandzd2gzd2z00zzexpand_garithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_expandzd2gzf2z20zzexpand_garithmetiquez00(obj_t, obj_t);
	static obj_t BGl_callzd2checkzd2zzexpand_installz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32140ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32132ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_expandzd2gzc3z11zzexpand_garithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_expandzd2gzd3z01zzexpand_garithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_expandzd2gze3z31zzexpand_garithmetiquez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32125ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32117ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62evsetz12z70zzexpand_installz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32109ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32892ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32795ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_expandzd2iza2z70zzexpand_iarithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_expandzd2izb2z60zzexpand_iarithmetiquez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_installz00(void);
	extern obj_t BGl_expandzd2izd2z00zzexpand_iarithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_expandzd2izf2z20zzexpand_iarithmetiquez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32053ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32134ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_initializa7ezd2Genvz12z67zzexpand_expanderz00(void);
	extern obj_t BGl_expandzd2izc3z11zzexpand_iarithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_expandzd2izd3z01zzexpand_iarithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(obj_t);
	extern obj_t BGl_expandzd2ize3z31zzexpand_iarithmetiquez00(obj_t, obj_t);
	extern obj_t BGl_initializa7ezd2Oenvz12z67zzexpand_expanderz00(void);
	static obj_t BGl_z62zc3z04anonymousza31503ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32127ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32208ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32119ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2errorzd2localiza7ationza2z75zzengine_paramz00;
	static obj_t BGl_z52appendzd22zd2idz52zzexpand_installz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31898ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33213ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32144ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32136ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32420ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33141ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33303ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31997ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_addzd2Ozd2macrozd2toplevelz12zc0zzexpand_expanderz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32316ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32146ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32138ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_boundzd2checkzd2zzexpand_installz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62pregexpzd2expanderzb0zzexpand_installz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32260ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32422ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31515ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32987ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_mapzd2checkzd2zzexpand_installz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33306ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2unsafezd2rangeza2zd2zzengine_paramz00;
	extern obj_t BGl_expandzd2maxfxzd2zzexpand_iarithmetiquez00(obj_t, obj_t);
	extern obj_t
		BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32610ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32076ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzexpand_installz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_labelsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_letz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzexpand_multiplezd2valueszd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_assertz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_mapz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_structz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_casez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_letz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_farithmetiquez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_iarithmetiquez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_garithmetiquez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_exitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_expanderz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_definez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_ifz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33308ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_expandzd2metazd2zzast_privatez00(obj_t, obj_t);
	extern obj_t BGl_za2profilezd2modeza2zd2zzengine_paramz00;
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31721ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33082ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32159ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_errzf2loczf2zzexpand_installz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62evrefz62zzexpand_installz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31900ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32540ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_expandzd2minfxzd2zzexpand_iarithmetiquez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_installz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_installz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_installz00(void);
	static obj_t BGl_installzd2expanderszd2zzexpand_installz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_installz00(void);
	static obj_t BGl_z62zc3z04anonymousza31545ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31902ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32097ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00;
	extern obj_t BGl_epairifyzd2reczd2zztools_miscz00(obj_t, obj_t);
	extern obj_t BGl_letzd2symzd2zzast_letz00(void);
	static obj_t BGl_z62zc3z04anonymousza31911ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32179ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32641ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31904ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32099ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT int BGl_bigloozd2compilerzd2debugz00zz__paramz00(void);
	static obj_t BGl_dispz00zzexpand_installz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31913ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33444ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62installzd2initialzd2expanderz62zzexpand_installz00(obj_t);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32538ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32287ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_loopze70ze7zzexpand_installz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_loopze71ze7zzexpand_installz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31915ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31907ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33276ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31940ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33382ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31917ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32379ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_expandzd2gmaxzd2zzexpand_garithmetiquez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32760ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33465ze3ze5zzexpand_installz00(obj_t,
		obj_t, obj_t);
	static obj_t __cnst[235];


	extern obj_t BGl_expandzd2methodzd2envz00zzexpand_definez00;
	extern obj_t BGl_expandzd2Ozd2valueszd2envzd2zzexpand_multiplezd2valueszd2;
	extern obj_t BGl_expandzd2bitzd2lshu32zd2envzd2zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2withzd2handlerzd2envzd2zzexpand_exitz00;
	extern obj_t BGl_expandzd2fatanzd2envz00zzexpand_farithmetiquez00;
	extern obj_t BGl_expandzd2zf2fxzd2envzf2zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2definezd2classzd2envzd2zzexpand_objectz00;
	extern obj_t BGl_expandzd2shrinkz12zd2envz12zzexpand_objectz00;
	extern obj_t BGl_expandzd2za2fxzd2envza2zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2setzd2exitzd2envzd2zzexpand_exitz00;
	extern obj_t BGl_expandzd2widenz12zd2envz12zzexpand_objectz00;
	extern obj_t
		BGl_expandzd2Ozd2callzd2withzd2valueszd2envzd2zzexpand_multiplezd2valueszd2;
	extern obj_t BGl_expandzd2zd2flzd2envzd2zzexpand_farithmetiquez00;
	extern obj_t BGl_expandzd2bitzd2lshzd2envzd2zzexpand_iarithmetiquez00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4200z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4257za7,
		BGl_z62zc3z04anonymousza33021ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4201z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4258za7,
		BGl_z62zc3z04anonymousza33082ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4202z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4259za7,
		BGl_z62zc3z04anonymousza33141ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4203z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4260za7,
		BGl_z62zc3z04anonymousza33213ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4204z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4261za7,
		BGl_z62zc3z04anonymousza33276ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4205z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4262za7,
		BGl_z62zc3z04anonymousza33303ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4206z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4263za7,
		BGl_z62zc3z04anonymousza33306ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4207z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4264za7,
		BGl_z62zc3z04anonymousza33308ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4126z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4265za7,
		BGl_z62zc3z04anonymousza31503ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4214z00zzexpand_installz00,
		BgL_bgl_string4214za700za7za7e4266za7, "Illegal `pregexp' form", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4208z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4267za7,
		BGl_z62zc3z04anonymousza33382ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4127z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4268za7,
		BGl_z62zc3z04anonymousza31515ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4215z00zzexpand_installz00,
		BgL_bgl_string4215za700za7za7e4269za7, "Illegal `mmap-ref' form", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4209z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4270za7,
		BGl_z62zc3z04anonymousza33444ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4128z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4271za7,
		BGl_z62zc3z04anonymousza31545ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4216z00zzexpand_installz00,
		BgL_bgl_string4216za700za7za7e4272za7, "Illegal `mmap-set!' form", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4129z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4273za7,
		BGl_z62zc3z04anonymousza31676ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4217z00zzexpand_installz00,
		BgL_bgl_string4217za700za7za7e4274za7, "profile", 7);
	      DEFINE_STRING(BGl_string4218z00zzexpand_installz00,
		BgL_bgl_string4218za700za7za7e4275za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string4219z00zzexpand_installz00,
		BgL_bgl_string4219za700za7za7e4276za7, "number", 6);
	extern obj_t BGl_expandzd2andzd2envz00zzexpand_ifz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4210z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4277za7,
		BGl_z62zc3z04anonymousza33465ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4211z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4278za7,
		BGl_z62zc3z04anonymousza33467ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4130z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4279za7,
		BGl_z62zc3z04anonymousza31721ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4212z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4280za7,
		BGl_z62zc3z04anonymousza33483ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4131z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4281za7,
		BGl_z62zc3z04anonymousza31762ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4213z00zzexpand_installz00,
		BgL_bgl_za762pregexpza7d2exp4282z00,
		BGl_z62pregexpzd2expanderzb0zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4132z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4283za7,
		BGl_z62zc3z04anonymousza31809ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4220z00zzexpand_installz00,
		BgL_bgl_string4220za700za7za7e4284za7, "Illegal `display-substring' call",
		32);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4133z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4285za7,
		BGl_z62zc3z04anonymousza31846ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4221z00zzexpand_installz00,
		BgL_bgl_string4221za700za7za7e4286za7, "Illegal `apply' form", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4134z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4287za7,
		BGl_z62zc3z04anonymousza31871ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4222z00zzexpand_installz00,
		BgL_bgl_string4222za700za7za7e4288za7, "Illegal `flonum->fixnum' call", 29);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4135z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4289za7,
		BGl_z62zc3z04anonymousza31883ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2vectorzd2mapzd2envzd2zzexpand_mapz00;
	   
		 
		DEFINE_STRING(BGl_string4223z00zzexpand_installz00,
		BgL_bgl_string4223za700za7za7e4290za7, "Illegal `fixnum->flonum' call", 29);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4136z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4291za7,
		BGl_z62zc3z04anonymousza31898ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4224z00zzexpand_installz00,
		BgL_bgl_string4224za700za7za7e4292za7, "Illegal `integer->char' call", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4137z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4293za7,
		BGl_z62zc3z04anonymousza31900ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4225z00zzexpand_installz00,
		BgL_bgl_string4225za700za7za7e4294za7, "Illegal `string->integer' radix",
		31);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4138z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4295za7,
		BGl_z62zc3z04anonymousza31902ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4226z00zzexpand_installz00,
		BgL_bgl_string4226za700za7za7e4296za7, "Illegal `blit-string!' form", 27);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4139z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4297za7,
		BGl_z62zc3z04anonymousza31904ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4227z00zzexpand_installz00,
		BgL_bgl_string4227za700za7za7e4298za7, "Illegal `substring-ci-at?' form",
		31);
	      DEFINE_STRING(BGl_string4228z00zzexpand_installz00,
		BgL_bgl_string4228za700za7za7e4299za7, "Illegal `substring-at?' form", 28);
	      DEFINE_STRING(BGl_string4229z00zzexpand_installz00,
		BgL_bgl_string4229za700za7za7e4300za7, "symbol", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4140z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4301za7,
		BGl_z62zc3z04anonymousza31907ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4141z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4302za7,
		BGl_z62zc3z04anonymousza31911ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4142z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4303za7,
		BGl_z62zc3z04anonymousza31913ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4230z00zzexpand_installz00,
		BgL_bgl_string4230za700za7za7e4304za7, "string", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4143z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4305za7,
		BGl_z62zc3z04anonymousza31915ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4231z00zzexpand_installz00,
		BgL_bgl_string4231za700za7za7e4306za7, "Illegal `vector-set!' form", 26);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4144z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4307za7,
		BGl_z62zc3z04anonymousza31917ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4232z00zzexpand_installz00,
		BgL_bgl_string4232za700za7za7e4308za7, "Illegal `vector-ref' form", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4145z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4309za7,
		BGl_z62zc3z04anonymousza31940ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4233z00zzexpand_installz00,
		BgL_bgl_string4233za700za7za7e4310za7, "Illegal `read/rp' form", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4146z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4311za7,
		BGl_z62zc3z04anonymousza31997ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4234z00zzexpand_installz00,
		BgL_bgl_string4234za700za7za7e4312za7, "Grammar arity mismatch", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4147z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4313za7,
		BGl_z62zc3z04anonymousza32053ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4235z00zzexpand_installz00,
		BgL_bgl_string4235za700za7za7e4314za7, "Illegal `read' form", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4148z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4315za7,
		BGl_z62zc3z04anonymousza32076ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4236z00zzexpand_installz00,
		BgL_bgl_string4236za700za7za7e4316za7, "Illegal `atan-2fl' call", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4149z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4317za7,
		BGl_z62zc3z04anonymousza32097ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4237z00zzexpand_installz00,
		BgL_bgl_string4237za700za7za7e4318za7, "Illegal `sqrtfl' call", 21);
	      DEFINE_STRING(BGl_string4238z00zzexpand_installz00,
		BgL_bgl_string4238za700za7za7e4319za7, "Illegal `eqv?' form", 19);
	      DEFINE_STRING(BGl_string4239z00zzexpand_installz00,
		BgL_bgl_string4239za700za7za7e4320za7, "Illegal `equal?' form", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4150z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4321za7,
		BGl_z62zc3z04anonymousza32099ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2duplicatezd2envz00zzexpand_objectz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4151z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4322za7,
		BGl_z62zc3z04anonymousza32101ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4152z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4323za7,
		BGl_z62zc3z04anonymousza32103ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4240z00zzexpand_installz00,
		BgL_bgl_string4240za700za7za7e4324za7, "Illegal `cons' form", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4153z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4325za7,
		BGl_z62zc3z04anonymousza32105ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4241z00zzexpand_installz00,
		BgL_bgl_string4241za700za7za7e4326za7, "Illegal 'string-length' form", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4154z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4327za7,
		BGl_z62zc3z04anonymousza32107ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4242z00zzexpand_installz00,
		BgL_bgl_string4242za700za7za7e4328za7, "Illegal `eappend' form", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4155z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4329za7,
		BGl_z62zc3z04anonymousza32109ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4243z00zzexpand_installz00,
		BgL_bgl_string4243za700za7za7e4330za7, "Illegal `append!' form", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4156z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4331za7,
		BGl_z62zc3z04anonymousza32111ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2orzd2envz00zzexpand_ifz00;
	   
		 
		DEFINE_STRING(BGl_string4244z00zzexpand_installz00,
		BgL_bgl_string4244za700za7za7e4332za7, "Illegal `append-2' form", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4157z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4333za7,
		BGl_z62zc3z04anonymousza32113ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4245z00zzexpand_installz00,
		BgL_bgl_string4245za700za7za7e4334za7, "Illegal `append' form", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4158z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4335za7,
		BGl_z62zc3z04anonymousza32115ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2unwindzd2protectzd2envzd2zzexpand_exitz00;
	   
		 
		DEFINE_STRING(BGl_string4246z00zzexpand_installz00,
		BgL_bgl_string4246za700za7za7e4336za7, ": argument not a ", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4159z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4337za7,
		BGl_z62zc3z04anonymousza32117ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4247z00zzexpand_installz00,
		BgL_bgl_string4247za700za7za7e4338za7, "index out of bound", 18);
	      DEFINE_STRING(BGl_string4248z00zzexpand_installz00,
		BgL_bgl_string4248za700za7za7e4339za7, "Illegal expression", 18);
	      DEFINE_STRING(BGl_string4249z00zzexpand_installz00,
		BgL_bgl_string4249za700za7za7e4340za7, ": argument not a list", 21);
	extern obj_t BGl_expandzd2zb2fxzd2envzb2zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2setz12zd2envz12zzexpand_definez00;
	extern obj_t BGl_expandzd2bitzd2rshzd2envzd2zzexpand_iarithmetiquez00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4160z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4341za7,
		BGl_z62zc3z04anonymousza32119ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4161z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4342za7,
		BGl_z62zc3z04anonymousza32121ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4162z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4343za7,
		BGl_z62zc3z04anonymousza32123ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4250z00zzexpand_installz00,
		BgL_bgl_string4250za700za7za7e4344za7, "Incorrect function arity", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4163z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4345za7,
		BGl_z62zc3z04anonymousza32125ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4251z00zzexpand_installz00,
		BgL_bgl_string4251za700za7za7e4346za7, "Illegal function arity", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4164z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4347za7,
		BGl_z62zc3z04anonymousza32127ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4252z00zzexpand_installz00,
		BgL_bgl_string4252za700za7za7e4348za7, "used with only two arguments", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4165z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4349za7,
		BGl_z62zc3z04anonymousza32130ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4253z00zzexpand_installz00,
		BgL_bgl_string4253za700za7za7e4350za7, "Various list length", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4166z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4351za7,
		BGl_z62zc3z04anonymousza32132ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4254z00zzexpand_installz00,
		BgL_bgl_string4254za700za7za7e4352za7, "expand_install", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4167z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4353za7,
		BGl_z62zc3z04anonymousza32134ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4255z00zzexpand_installz00,
		BgL_bgl_string4255za700za7za7e4354za7,
		"l1 loop cdr set-cdr! new-prev car pair? prev tail head l1::pair-nil l2 (@ display-2 __r4_output_6_10_3) (@ write-char-2 __r4_output_6_10_3) (@ display-flonum __r4_output_6_10_3) (@ display-fixnum __r4_output_6_10_3) (@ display-symbol __r4_output_6_10_3) (@ display-string __r4_output_6_10_3) error/location list length __error list? begin error/c-location warning/c-location location append-2! eappend-2 (quote ()) sqrtfl-ur atan-2fl-ur current-input-port correct-arity? vector-set-ur! $create-vector v (unspecified) $make-vector vector-copy3 $ $string-append $string-append-3 string? quote string->symbol symbol->string symbol? substring-ur $substring-at? $prefix-at? $substring-ci-at? $prefix-ci-at? blit-string-ur! integer->char-ur strtol $cons __r4_pairs_and_lists_6_3 $display-substring write-char-2 write-byte-2 current-output-port __r4_ports_6_10_1 @ newline-1 __r4_output_6_10_3 port read-string file->string number? GC-profile-push GC-profile-pop GC-collect-profile-push value mmap-set-ur! mmap-ref-ur $mmap-bound-c"
		"heck? mmap-length pregexp pregexp-split pregexp-replace* pregexp-replace pregexp-match pregexp-match-positions mmap-ref mmap-set! profile/gc profile cond-expand shrink! widen! duplicate co-instantiate instantiate with-access define-abstract-class define-final-class define-class assert call-with-values values exact->inexact inexact->exact with-input-from-file fprint print write-byte write-char display-substring display newline apply cons* flonum->fixnum fixnum->flonum char->integer string->integer integer->char blit-string! substring-ci-at? substring-at? substring symbol-append string-append f64vector-set! f64vector-ref f32vector-set! f32vector-ref u64vector-set! u64vector-ref s64vector-set! s64vector-ref u32vector-set! u32vector-ref s32vector-set! s32vector-ref u16vector-set! u16vector-ref s16vector-set! s16vector-ref u8vector-set! u8vector-ref s8vector-set! s8vector-ref vector-copy make-vector vector-map! vector-map vector-for-each vector read/rp read case labels letrec* letrec let let* atan-2fl sqrtfl atanf"
		"l minfl maxfl /fl *fl -fl +fl minfx maxfx /fx *fx -fx +fx eq? bit-ors32 bit-urshu32 bit-ursh bit-rshu32 bit-rshs32 bit-rsh bit-lshu32 bit-lsh sin cos min max >= <= > < = / * - + flonum? fixnum? eqv? equal? find reduce every any filter! filter for-each append-map map! map cons string-length eappend append! append warning error multiple-value-bind with-handler unwind-protect bind-exit jump-exit set-exit set! define-struct define-method define-generic define-inline define lambda not and or if |#meta| append-2 ",
		2560);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4168z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4355za7,
		BGl_z62zc3z04anonymousza32136ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4169z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4356za7,
		BGl_z62zc3z04anonymousza32138ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_installzd2initialzd2expanderzd2envzd2zzexpand_installz00,
		BgL_bgl_za762installza7d2ini4357z00,
		BGl_z62installzd2initialzd2expanderz62zzexpand_installz00, 0L, BUNSPEC, 0);
	BGL_IMPORT obj_t
		BGl_expandzd2compilezd2condzd2expandzd2envz00zz__expander_srfi0z00;
	extern obj_t BGl_expandzd2bitzd2urshu32zd2envzd2zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2bitzd2rshs32zd2envzd2zzexpand_iarithmetiquez00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4170z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4358za7,
		BGl_z62zc3z04anonymousza32140ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2genericzd2envz00zzexpand_definez00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4171z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4359za7,
		BGl_z62zc3z04anonymousza32142ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4172z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4360za7,
		BGl_z62zc3z04anonymousza32144ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4173z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4361za7,
		BGl_z62zc3z04anonymousza32146ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4174z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4362za7,
		BGl_z62zc3z04anonymousza32159ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4175z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4363za7,
		BGl_z62zc3z04anonymousza32179ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4176z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4364za7,
		BGl_z62zc3z04anonymousza32208ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4177z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4365za7,
		BGl_z62zc3z04anonymousza32260ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4178z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4366za7,
		BGl_z62zc3z04anonymousza32287ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4179z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4367za7,
		BGl_z62zc3z04anonymousza32316ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4180z00zzexpand_installz00,
		BgL_bgl_za762evsetza712za770za7za74368za7,
		BGl_z62evsetz12z70zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4181z00zzexpand_installz00,
		BgL_bgl_za762evrefza762za7za7exp4369z00, BGl_z62evrefz62zzexpand_installz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4182z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4370za7,
		BGl_z62zc3z04anonymousza32379ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4183z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4371za7,
		BGl_z62zc3z04anonymousza32420ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4184z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4372za7,
		BGl_z62zc3z04anonymousza32422ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4185z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4373za7,
		BGl_z62zc3z04anonymousza32538ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4186z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4374za7,
		BGl_z62zc3z04anonymousza32540ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2bindzd2exitzd2envzd2zzexpand_exitz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4187z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4375za7,
		BGl_z62zc3z04anonymousza32567ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4188z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4376za7,
		BGl_z62zc3z04anonymousza32610ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4189z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4377za7,
		BGl_z62zc3z04anonymousza32641ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4190z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4378za7,
		BGl_z62zc3z04anonymousza32673ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4191z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4379za7,
		BGl_z62zc3z04anonymousza32691ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4192z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4380za7,
		BGl_z62zc3z04anonymousza32745ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4193z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4381za7,
		BGl_z62zc3z04anonymousza32760ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4194z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4382za7,
		BGl_z62zc3z04anonymousza32777ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4195z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4383za7,
		BGl_z62zc3z04anonymousza32795ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4196z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4384za7,
		BGl_z62zc3z04anonymousza32847ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2bitzd2ors32zd2envzd2zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2forzd2eachzd2envzd2zzexpand_mapz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4197z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4385za7,
		BGl_z62zc3z04anonymousza32892ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4198z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4386za7,
		BGl_z62zc3z04anonymousza32927ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4199z00zzexpand_installz00,
		BgL_bgl_za762za7c3za704anonymo4387za7,
		BGl_z62zc3z04anonymousza32987ze3ze5zzexpand_installz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2casezd2envz00zzexpand_casez00;
	extern obj_t BGl_expandzd2minfxzd2envz00zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2instantiatezd2envz00zzexpand_objectz00;
	extern obj_t BGl_expandzd2inlinezd2envz00zzexpand_definez00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52appendzd22zd2definezd2envz80zzexpand_installz00,
		BgL_bgl_za762za752appendza7d224388za7,
		BGl_z62z52appendzd22zd2definez30zzexpand_installz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_expandzd2definezd2envz00zzexpand_definez00;
	extern obj_t BGl_expandzd2zf2flzd2envzf2zzexpand_farithmetiquez00;
	extern obj_t BGl_expandzd2reducezd2envz00zzexpand_mapz00;
	extern obj_t BGl_expandzd2structzd2envz00zzexpand_structz00;
	extern obj_t BGl_expandzd2labelszd2envz00zzexpand_letz00;
	extern obj_t BGl_expandzd2za2flzd2envza2zzexpand_farithmetiquez00;
	extern obj_t BGl_expandzd2mapz12zd2envz12zzexpand_mapz00;
	extern obj_t BGl_expandzd2cozd2instantiatezd2envzd2zzexpand_objectz00;
	extern obj_t BGl_expandzd2jumpzd2exitzd2envzd2zzexpand_exitz00;
	extern obj_t BGl_expandzd2maxfxzd2envz00zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2withzd2accesszd2envzd2zzexpand_objectz00;
	extern obj_t BGl_expandzd2everyzd2envz00zzexpand_mapz00;
	extern obj_t BGl_expandzd2zd2fxzd2envzd2zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2findzd2envz00zzexpand_mapz00;
	extern obj_t BGl_expandzd2vectorzd2forzd2eachzd2envz00zzexpand_mapz00;
	extern obj_t BGl_expandzd2filterzd2envz00zzexpand_mapz00;
	extern obj_t BGl_expandzd2bitzd2rshu32zd2envzd2zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2mvaluezd2bindzd2envzd2zzexpand_multiplezd2valueszd2;
	extern obj_t BGl_expandzd2eqzf3zd2envzf3zzexpand_iarithmetiquez00;
	extern obj_t BGl_expandzd2zb2flzd2envzb2zzexpand_farithmetiquez00;
	extern obj_t BGl_expandzd2appendzd2mapzd2envzd2zzexpand_mapz00;
	extern obj_t BGl_expandzd2assertzd2envz00zzexpand_assertz00;
	extern obj_t BGl_expandzd2ifzd2envz00zzexpand_ifz00;
	extern obj_t BGl_expandzd2notzd2envz00zzexpand_ifz00;
	extern obj_t BGl_expandzd2anyzd2envz00zzexpand_mapz00;
	extern obj_t BGl_expandzd2vectorzd2mapz12zd2envzc0zzexpand_mapz00;
	extern obj_t BGl_expandzd2letreczd2envz00zzexpand_letz00;
	extern obj_t BGl_expandzd2letzd2envz00zzexpand_letz00;
	extern obj_t BGl_expandzd2letza2zd2envza2zzexpand_letz00;
	extern obj_t BGl_expandzd2lambdazd2envz00zzexpand_lambdaz00;
	extern obj_t BGl_expandzd2fminzd2envz00zzexpand_farithmetiquez00;
	extern obj_t BGl_expandzd2fmaxzd2envz00zzexpand_farithmetiquez00;
	extern obj_t BGl_expandzd2mapzd2envz00zzexpand_mapz00;
	extern obj_t BGl_expandzd2bitzd2urshzd2envzd2zzexpand_iarithmetiquez00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzexpand_installz00));
		     ADD_ROOT((void *) (&BGl_z52appendzd22zd2idz52zzexpand_installz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzexpand_installz00(long
		BgL_checksumz00_7289, char *BgL_fromz00_7290)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_installz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_installz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_installz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_installz00();
					BGl_cnstzd2initzd2zzexpand_installz00();
					BGl_importedzd2moduleszd2initz00zzexpand_installz00();
					return BGl_toplevelzd2initzd2zzexpand_installz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_install");
			BGl_modulezd2initializa7ationz75zz__macroz00(0L, "expand_install");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"expand_install");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"expand_install");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_install");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"expand_install");
			BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(0L,
				"expand_install");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_install");
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "expand_install");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"expand_install");
			BGl_modulezd2initializa7ationz75zz__prognz00(0L, "expand_install");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"expand_install");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"expand_install");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_install");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "expand_install");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "expand_install");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 15 */
			{	/* Expand/initial.scm 15 */
				obj_t BgL_cportz00_5972;

				{	/* Expand/initial.scm 15 */
					obj_t BgL_stringz00_5979;

					BgL_stringz00_5979 = BGl_string4255z00zzexpand_installz00;
					{	/* Expand/initial.scm 15 */
						obj_t BgL_startz00_5980;

						BgL_startz00_5980 = BINT(0L);
						{	/* Expand/initial.scm 15 */
							obj_t BgL_endz00_5981;

							BgL_endz00_5981 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_5979)));
							{	/* Expand/initial.scm 15 */

								BgL_cportz00_5972 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_5979, BgL_startz00_5980, BgL_endz00_5981);
				}}}}
				{
					long BgL_iz00_5973;

					BgL_iz00_5973 = 234L;
				BgL_loopz00_5974:
					if ((BgL_iz00_5973 == -1L))
						{	/* Expand/initial.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/initial.scm 15 */
							{	/* Expand/initial.scm 15 */
								obj_t BgL_arg4256z00_5975;

								{	/* Expand/initial.scm 15 */

									{	/* Expand/initial.scm 15 */
										obj_t BgL_locationz00_5977;

										BgL_locationz00_5977 = BBOOL(((bool_t) 0));
										{	/* Expand/initial.scm 15 */

											BgL_arg4256z00_5975 =
												BGl_readz00zz__readerz00(BgL_cportz00_5972,
												BgL_locationz00_5977);
										}
									}
								}
								{	/* Expand/initial.scm 15 */
									int BgL_tmpz00_7324;

									BgL_tmpz00_7324 = (int) (BgL_iz00_5973);
									CNST_TABLE_SET(BgL_tmpz00_7324, BgL_arg4256z00_5975);
							}}
							{	/* Expand/initial.scm 15 */
								int BgL_auxz00_5978;

								BgL_auxz00_5978 = (int) ((BgL_iz00_5973 - 1L));
								{
									long BgL_iz00_7329;

									BgL_iz00_7329 = (long) (BgL_auxz00_5978);
									BgL_iz00_5973 = BgL_iz00_7329;
									goto BgL_loopz00_5974;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 15 */
			BGl_installedpz00zzexpand_installz00 = ((bool_t) 0);
			return (BGl_z52appendzd22zd2idz52zzexpand_installz00 =
				BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(0)), BUNSPEC);
		}

	}



/* install-initial-expander */
	BGL_EXPORTED_DEF obj_t
		BGl_installzd2initialzd2expanderz00zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 57 */
			if (BGl_installedpz00zzexpand_installz00)
				{	/* Expand/initial.scm 58 */
					return BFALSE;
				}
			else
				{	/* Expand/initial.scm 58 */
					BGl_installedpz00zzexpand_installz00 = ((bool_t) 1);
					BGL_TAIL return BGl_installzd2expanderszd2zzexpand_installz00();
				}
		}

	}



/* &install-initial-expander */
	obj_t BGl_z62installzd2initialzd2expanderz62zzexpand_installz00(obj_t
		BgL_envz00_5404)
	{
		{	/* Expand/initial.scm 57 */
			return BGl_installzd2initialzd2expanderz00zzexpand_installz00();
		}

	}



/* install-expanders */
	obj_t BGl_installzd2expanderszd2zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 65 */
			BGl_initializa7ezd2Oenvz12z67zzexpand_expanderz00();
			BGl_initializa7ezd2Genvz12z67zzexpand_expanderz00();
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(1),
				BGl_proc4126z00zzexpand_installz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(2),
				BGl_expandzd2ifzd2envz00zzexpand_ifz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(3),
				BGl_expandzd2orzd2envz00zzexpand_ifz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(4),
				BGl_expandzd2andzd2envz00zzexpand_ifz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(5),
				BGl_expandzd2notzd2envz00zzexpand_ifz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(6),
				BGl_expandzd2lambdazd2envz00zzexpand_lambdaz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(7),
				BGl_expandzd2definezd2envz00zzexpand_definez00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(8),
				BGl_expandzd2inlinezd2envz00zzexpand_definez00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(9),
				BGl_expandzd2genericzd2envz00zzexpand_definez00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(10),
				BGl_expandzd2methodzd2envz00zzexpand_definez00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(11),
				BGl_expandzd2structzd2envz00zzexpand_structz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(12),
				BGl_expandzd2setz12zd2envz12zzexpand_definez00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(13),
				BGl_expandzd2setzd2exitzd2envzd2zzexpand_exitz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(14),
				BGl_expandzd2jumpzd2exitzd2envzd2zzexpand_exitz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(15),
				BGl_expandzd2bindzd2exitzd2envzd2zzexpand_exitz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(16),
				BGl_expandzd2unwindzd2protectzd2envzd2zzexpand_exitz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(17),
				BGl_expandzd2withzd2handlerzd2envzd2zzexpand_exitz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(18),
				BGl_expandzd2mvaluezd2bindzd2envzd2zzexpand_multiplezd2valueszd2);
			BGl_installzd2compilerzd2expanderz00zz__macroz00
				(BGl_privatezd2stampzd2zzast_privatez00(),
				BGl_proc4127z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(19), BGl_proc4128z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(20), BGl_proc4129z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(21), BGl_proc4130z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(0), BGl_proc4131z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(22), BGl_proc4132z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(23), BGl_proc4133z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(24), BGl_proc4134z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(25), BGl_proc4135z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(26), BGl_expandzd2mapzd2envz00zzexpand_mapz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(26), BGl_proc4136z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(27), BGl_expandzd2mapz12zd2envz12zzexpand_mapz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(27), BGl_proc4137z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(28), BGl_expandzd2appendzd2mapzd2envzd2zzexpand_mapz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(28), BGl_proc4138z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(29), BGl_expandzd2forzd2eachzd2envzd2zzexpand_mapz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(29), BGl_proc4139z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(30), BGl_expandzd2filterzd2envz00zzexpand_mapz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(30), BGl_proc4140z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(31), BGl_proc4141z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(32), BGl_expandzd2anyzd2envz00zzexpand_mapz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(32), BGl_proc4142z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(33), BGl_expandzd2everyzd2envz00zzexpand_mapz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(33), BGl_proc4143z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(34), BGl_expandzd2reducezd2envz00zzexpand_mapz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(35), BGl_expandzd2findzd2envz00zzexpand_mapz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(2), BGl_proc4144z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(36), BGl_proc4145z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(37), BGl_proc4146z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(38), BGl_proc4147z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(39), BGl_proc4148z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(40), BGl_proc4149z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(41), BGl_proc4150z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(42), BGl_proc4151z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(43), BGl_proc4152z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(44), BGl_proc4153z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(45), BGl_proc4154z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(46), BGl_proc4155z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(47), BGl_proc4156z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(48), BGl_proc4157z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(49), BGl_proc4158z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(50), BGl_proc4159z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(40), BGl_proc4160z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(42), BGl_proc4161z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(43), BGl_proc4162z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(41), BGl_proc4163z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(44), BGl_proc4164z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(46), BGl_proc4165z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(45), BGl_proc4166z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(48), BGl_proc4167z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(47), BGl_proc4168z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(51), BGl_proc4169z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(52), BGl_proc4170z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(50), BGl_proc4171z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(49), BGl_proc4172z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(53),
				BGl_expandzd2bitzd2lshzd2envzd2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(54),
				BGl_expandzd2bitzd2lshu32zd2envzd2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(55),
				BGl_expandzd2bitzd2rshzd2envzd2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(56),
				BGl_expandzd2bitzd2rshs32zd2envzd2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(57),
				BGl_expandzd2bitzd2rshu32zd2envzd2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(58),
				BGl_expandzd2bitzd2urshzd2envzd2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(59),
				BGl_expandzd2bitzd2urshu32zd2envzd2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(60),
				BGl_expandzd2bitzd2ors32zd2envzd2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(61),
				BGl_expandzd2eqzf3zd2envzf3zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(62),
				BGl_expandzd2zb2fxzd2envzb2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(63),
				BGl_expandzd2zd2fxzd2envzd2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(64),
				BGl_expandzd2za2fxzd2envza2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(65),
				BGl_expandzd2zf2fxzd2envzf2zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(66),
				BGl_expandzd2maxfxzd2envz00zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(67),
				BGl_expandzd2minfxzd2envz00zzexpand_iarithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(68),
				BGl_expandzd2zb2flzd2envzb2zzexpand_farithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(69),
				BGl_expandzd2zd2flzd2envzd2zzexpand_farithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(70),
				BGl_expandzd2za2flzd2envza2zzexpand_farithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(71),
				BGl_expandzd2zf2flzd2envzf2zzexpand_farithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(72),
				BGl_expandzd2fmaxzd2envz00zzexpand_farithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(73),
				BGl_expandzd2fminzd2envz00zzexpand_farithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(74),
				BGl_expandzd2fatanzd2envz00zzexpand_farithmetiquez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(75), BGl_proc4173z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(76), BGl_proc4174z00zzexpand_installz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(77),
				BGl_expandzd2letza2zd2envza2zzexpand_letz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(78),
				BGl_expandzd2letzd2envz00zzexpand_letz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00
				(BGl_letzd2symzd2zzast_letz00(),
				BGl_expandzd2letzd2envz00zzexpand_letz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(79),
				BGl_expandzd2letreczd2envz00zzexpand_letz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(80),
				BGl_expandzd2letreczd2envz00zzexpand_letz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(81),
				BGl_expandzd2labelszd2envz00zzexpand_letz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00
				(BGl_labelszd2symzd2zzast_labelsz00(),
				BGl_expandzd2labelszd2envz00zzexpand_letz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(82),
				BGl_expandzd2casezd2envz00zzexpand_casez00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(83), BGl_proc4175z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(84), BGl_proc4176z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(85), BGl_proc4177z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(86),
				BGl_expandzd2vectorzd2forzd2eachzd2envz00zzexpand_mapz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(87), BGl_expandzd2vectorzd2mapzd2envzd2zzexpand_mapz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(88),
				BGl_expandzd2vectorzd2mapz12zd2envzc0zzexpand_mapz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(89), BGl_proc4178z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(90), BGl_proc4179z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(91), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(92), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(93), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(94), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(95), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(96), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(97), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(98), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(99), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(100), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(101), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(102), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(103), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(104), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(105), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(106), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(107), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(108), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(109), BGl_proc4181z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(110), BGl_proc4180z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(111), BGl_proc4182z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(111), BGl_proc4183z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(112), BGl_proc4184z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(112), BGl_proc4185z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(113), BGl_proc4186z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(114), BGl_proc4187z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(115), BGl_proc4188z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(116), BGl_proc4189z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(117), BGl_proc4190z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(118), BGl_proc4191z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(119), BGl_proc4192z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(120), BGl_proc4193z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(121), BGl_proc4194z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(122), BGl_proc4195z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(123), BGl_proc4196z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(124), BGl_proc4197z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(125), BGl_proc4198z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(126), BGl_proc4199z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(127), BGl_proc4200z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(128), BGl_proc4201z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(129), BGl_proc4202z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(130), BGl_proc4203z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(131), BGl_proc4204z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(132), BGl_proc4205z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(133), BGl_proc4206z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(134),
				BGl_expandzd2Ozd2valueszd2envzd2zzexpand_multiplezd2valueszd2);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(135),
				BGl_expandzd2Ozd2callzd2withzd2valueszd2envzd2zzexpand_multiplezd2valueszd2);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(136),
				BGl_expandzd2assertzd2envz00zzexpand_assertz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(137),
				BGl_expandzd2definezd2classzd2envzd2zzexpand_objectz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(138),
				BGl_expandzd2definezd2classzd2envzd2zzexpand_objectz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(139),
				BGl_expandzd2definezd2classzd2envzd2zzexpand_objectz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(140),
				BGl_expandzd2withzd2accesszd2envzd2zzexpand_objectz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(141),
				BGl_expandzd2instantiatezd2envz00zzexpand_objectz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(142),
				BGl_expandzd2cozd2instantiatezd2envzd2zzexpand_objectz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(143),
				BGl_expandzd2duplicatezd2envz00zzexpand_objectz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(144),
				BGl_expandzd2widenz12zd2envz12zzexpand_objectz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(145),
				BGl_expandzd2shrinkz12zd2envz12zzexpand_objectz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(146),
				BGl_expandzd2compilezd2condzd2expandzd2envz00zz__expander_srfi0z00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(147),
				BGl_proc4207z00zzexpand_installz00);
			BGl_installzd2compilerzd2expanderz00zz__macroz00(CNST_TABLE_REF(148),
				BGl_proc4208z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(149), BGl_proc4209z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(149), BGl_proc4210z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(150), BGl_proc4211z00zzexpand_installz00);
			BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(150), BGl_proc4212z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(151), BGl_proc4213z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(152), BGl_proc4213z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(153), BGl_proc4213z00zzexpand_installz00);
			BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(154), BGl_proc4213z00zzexpand_installz00);
			return
				BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(CNST_TABLE_REF(155), BGl_proc4213z00zzexpand_installz00);
		}

	}



/* &pregexp-expander */
	obj_t BGl_z62pregexpzd2expanderzb0zzexpand_installz00(obj_t BgL_envz00_5493,
		obj_t BgL_xz00_5494, obj_t BgL_ez00_5495)
	{
		{	/* Expand/initial.scm 1085 */
			{
				obj_t BgL_kz00_5999;
				obj_t BgL_regexpz00_6000;
				obj_t BgL_restz00_6001;
				obj_t BgL_kz00_5987;
				obj_t BgL_restz00_5988;

				if (PAIRP(BgL_xz00_5494))
					{	/* Expand/initial.scm 1085 */
						obj_t BgL_cdrzd21872zd2_6020;

						BgL_cdrzd21872zd2_6020 = CDR(((obj_t) BgL_xz00_5494));
						if (PAIRP(BgL_cdrzd21872zd2_6020))
							{	/* Expand/initial.scm 1085 */
								obj_t BgL_carzd21876zd2_6021;

								BgL_carzd21876zd2_6021 = CAR(BgL_cdrzd21872zd2_6020);
								if (STRINGP(BgL_carzd21876zd2_6021))
									{	/* Expand/initial.scm 1085 */
										obj_t BgL_arg3488z00_6022;
										obj_t BgL_arg3489z00_6023;

										BgL_arg3488z00_6022 = CAR(((obj_t) BgL_xz00_5494));
										BgL_arg3489z00_6023 = CDR(BgL_cdrzd21872zd2_6020);
										BgL_kz00_5999 = BgL_arg3488z00_6022;
										BgL_regexpz00_6000 = BgL_carzd21876zd2_6021;
										BgL_restz00_6001 = BgL_arg3489z00_6023;
										{	/* Expand/initial.scm 1092 */
											obj_t BgL_idz00_6002;

											BgL_idz00_6002 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(156));
											{	/* Expand/initial.scm 1094 */
												obj_t BgL_arg3495z00_6003;

												{	/* Expand/initial.scm 1094 */
													obj_t BgL_arg3496z00_6004;

													{	/* Expand/initial.scm 1094 */
														obj_t BgL_arg3500z00_6005;

														{	/* Expand/initial.scm 1094 */
															obj_t BgL_arg3502z00_6006;

															{	/* Expand/initial.scm 1094 */
																obj_t BgL_arg3503z00_6007;

																{	/* Expand/initial.scm 1094 */
																	obj_t BgL_arg3504z00_6008;

																	BgL_arg3504z00_6008 =
																		MAKE_YOUNG_PAIR(BgL_regexpz00_6000, BNIL);
																	BgL_arg3503z00_6007 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(156),
																		BgL_arg3504z00_6008);
																}
																BgL_arg3502z00_6006 =
																	MAKE_YOUNG_PAIR(BgL_arg3503z00_6007, BNIL);
															}
															BgL_arg3500z00_6005 =
																MAKE_YOUNG_PAIR(BgL_idz00_6002,
																BgL_arg3502z00_6006);
														}
														BgL_arg3496z00_6004 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
															BgL_arg3500z00_6005);
													}
													{	/* Expand/initial.scm 1094 */
														obj_t BgL_tmpfunz00_7730;

														BgL_tmpfunz00_7730 = ((obj_t) BgL_ez00_5495);
														BgL_arg3495z00_6003 =
															(VA_PROCEDUREP(BgL_tmpfunz00_7730)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7730)) (((obj_t)
																	BgL_ez00_5495), BgL_arg3496z00_6004,
																((obj_t) BgL_ez00_5495),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7730)) (((obj_t)
																	BgL_ez00_5495), BgL_arg3496z00_6004,
																((obj_t) BgL_ez00_5495)));
													}
												}
												BGl_addzd2Ozd2macrozd2toplevelz12zc0zzexpand_expanderz00
													(BgL_arg3495z00_6003);
											}
											{	/* Expand/initial.scm 1096 */
												obj_t BgL_arg3506z00_6009;

												{	/* Expand/initial.scm 1096 */
													obj_t BgL_arg3508z00_6010;

													{	/* Expand/initial.scm 1096 */
														obj_t BgL_arg3510z00_6011;

														if (NULLP(BgL_restz00_6001))
															{	/* Expand/initial.scm 1096 */
																BgL_arg3510z00_6011 = BNIL;
															}
														else
															{	/* Expand/initial.scm 1096 */
																obj_t BgL_head1388z00_6012;

																BgL_head1388z00_6012 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1386z00_6014;
																	obj_t BgL_tail1389z00_6015;

																	BgL_l1386z00_6014 = BgL_restz00_6001;
																	BgL_tail1389z00_6015 = BgL_head1388z00_6012;
																BgL_zc3z04anonymousza33512ze3z87_6013:
																	if (NULLP(BgL_l1386z00_6014))
																		{	/* Expand/initial.scm 1096 */
																			BgL_arg3510z00_6011 =
																				CDR(BgL_head1388z00_6012);
																		}
																	else
																		{	/* Expand/initial.scm 1096 */
																			obj_t BgL_newtail1390z00_6016;

																			{	/* Expand/initial.scm 1096 */
																				obj_t BgL_arg3515z00_6017;

																				{	/* Expand/initial.scm 1096 */
																					obj_t BgL_xz00_6018;

																					BgL_xz00_6018 =
																						CAR(((obj_t) BgL_l1386z00_6014));
																					{	/* Expand/initial.scm 1096 */
																						obj_t BgL_tmpfunz00_7747;

																						BgL_tmpfunz00_7747 =
																							((obj_t) BgL_ez00_5495);
																						BgL_arg3515z00_6017 =
																							(VA_PROCEDUREP(BgL_tmpfunz00_7747)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_7747)) (((obj_t)
																									BgL_ez00_5495), BgL_xz00_6018,
																								((obj_t) BgL_ez00_5495),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_7747)) (((obj_t)
																									BgL_ez00_5495), BgL_xz00_6018,
																								((obj_t) BgL_ez00_5495)));
																					}
																				}
																				BgL_newtail1390z00_6016 =
																					MAKE_YOUNG_PAIR(BgL_arg3515z00_6017,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1389z00_6015,
																				BgL_newtail1390z00_6016);
																			{	/* Expand/initial.scm 1096 */
																				obj_t BgL_arg3514z00_6019;

																				BgL_arg3514z00_6019 =
																					CDR(((obj_t) BgL_l1386z00_6014));
																				{
																					obj_t BgL_tail1389z00_7754;
																					obj_t BgL_l1386z00_7753;

																					BgL_l1386z00_7753 =
																						BgL_arg3514z00_6019;
																					BgL_tail1389z00_7754 =
																						BgL_newtail1390z00_6016;
																					BgL_tail1389z00_6015 =
																						BgL_tail1389z00_7754;
																					BgL_l1386z00_6014 = BgL_l1386z00_7753;
																					goto
																						BgL_zc3z04anonymousza33512ze3z87_6013;
																				}
																			}
																		}
																}
															}
														BgL_arg3508z00_6010 =
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_arg3510z00_6011, BNIL);
													}
													BgL_arg3506z00_6009 =
														MAKE_YOUNG_PAIR(BgL_idz00_6002,
														BgL_arg3508z00_6010);
												}
												return
													MAKE_YOUNG_PAIR(BgL_kz00_5999, BgL_arg3506z00_6009);
											}
										}
									}
								else
									{	/* Expand/initial.scm 1085 */
										obj_t BgL_arg3490z00_6024;

										BgL_arg3490z00_6024 = CAR(((obj_t) BgL_xz00_5494));
										BgL_kz00_5987 = BgL_arg3490z00_6024;
										BgL_restz00_5988 = BgL_cdrzd21872zd2_6020;
									BgL_tagzd21862zd2_5985:
										{	/* Expand/initial.scm 1098 */
											obj_t BgL_arg3517z00_5989;

											{	/* Expand/initial.scm 1098 */
												obj_t BgL_arg3518z00_5990;

												if (NULLP(BgL_restz00_5988))
													{	/* Expand/initial.scm 1098 */
														BgL_arg3518z00_5990 = BNIL;
													}
												else
													{	/* Expand/initial.scm 1098 */
														obj_t BgL_head1393z00_5991;

														BgL_head1393z00_5991 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1391z00_5993;
															obj_t BgL_tail1394z00_5994;

															BgL_l1391z00_5993 = BgL_restz00_5988;
															BgL_tail1394z00_5994 = BgL_head1393z00_5991;
														BgL_zc3z04anonymousza33520ze3z87_5992:
															if (NULLP(BgL_l1391z00_5993))
																{	/* Expand/initial.scm 1098 */
																	BgL_arg3518z00_5990 =
																		CDR(BgL_head1393z00_5991);
																}
															else
																{	/* Expand/initial.scm 1098 */
																	obj_t BgL_newtail1395z00_5995;

																	{	/* Expand/initial.scm 1098 */
																		obj_t BgL_arg3523z00_5996;

																		{	/* Expand/initial.scm 1098 */
																			obj_t BgL_xz00_5997;

																			BgL_xz00_5997 =
																				CAR(((obj_t) BgL_l1391z00_5993));
																			{	/* Expand/initial.scm 1098 */
																				obj_t BgL_tmpfunz00_7774;

																				BgL_tmpfunz00_7774 =
																					((obj_t) BgL_ez00_5495);
																				BgL_arg3523z00_5996 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_7774)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_7774))
																					(((obj_t) BgL_ez00_5495),
																						BgL_xz00_5997,
																						((obj_t) BgL_ez00_5495),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_7774))
																					(((obj_t) BgL_ez00_5495),
																						BgL_xz00_5997,
																						((obj_t) BgL_ez00_5495)));
																			}
																		}
																		BgL_newtail1395z00_5995 =
																			MAKE_YOUNG_PAIR(BgL_arg3523z00_5996,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1394z00_5994,
																		BgL_newtail1395z00_5995);
																	{	/* Expand/initial.scm 1098 */
																		obj_t BgL_arg3522z00_5998;

																		BgL_arg3522z00_5998 =
																			CDR(((obj_t) BgL_l1391z00_5993));
																		{
																			obj_t BgL_tail1394z00_7781;
																			obj_t BgL_l1391z00_7780;

																			BgL_l1391z00_7780 = BgL_arg3522z00_5998;
																			BgL_tail1394z00_7781 =
																				BgL_newtail1395z00_5995;
																			BgL_tail1394z00_5994 =
																				BgL_tail1394z00_7781;
																			BgL_l1391z00_5993 = BgL_l1391z00_7780;
																			goto
																				BgL_zc3z04anonymousza33520ze3z87_5992;
																		}
																	}
																}
														}
													}
												BgL_arg3517z00_5989 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg3518z00_5990, BNIL);
											}
											return
												MAKE_YOUNG_PAIR(BgL_kz00_5987, BgL_arg3517z00_5989);
										}
									}
							}
						else
							{	/* Expand/initial.scm 1085 */
								obj_t BgL_arg3493z00_6025;

								BgL_arg3493z00_6025 = CAR(((obj_t) BgL_xz00_5494));
								{
									obj_t BgL_restz00_7787;
									obj_t BgL_kz00_7786;

									BgL_kz00_7786 = BgL_arg3493z00_6025;
									BgL_restz00_7787 = BgL_cdrzd21872zd2_6020;
									BgL_restz00_5988 = BgL_restz00_7787;
									BgL_kz00_5987 = BgL_kz00_7786;
									goto BgL_tagzd21862zd2_5985;
								}
							}
					}
				else
					{	/* Expand/initial.scm 1085 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string4214z00zzexpand_installz00, BgL_xz00_5494);
					}
			}
		}

	}



/* &<@anonymous:3483> */
	obj_t BGl_z62zc3z04anonymousza33483ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5496, obj_t BgL_xz00_5497, obj_t BgL_ez00_5498)
	{
		{	/* Expand/initial.scm 1081 */
			return
				BGl_boundzd2checkzd2zzexpand_installz00(BgL_xz00_5497,
				CNST_TABLE_REF(157), CNST_TABLE_REF(158), ((obj_t) BgL_ez00_5498));
		}

	}



/* &<@anonymous:3467> */
	obj_t BGl_z62zc3z04anonymousza33467ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5499, obj_t BgL_xz00_5500, obj_t BgL_ez00_5501)
	{
		{	/* Expand/initial.scm 1069 */
			{
				obj_t BgL_vecz00_6030;
				obj_t BgL_kz00_6031;

				if (PAIRP(BgL_xz00_5500))
					{	/* Expand/initial.scm 1069 */
						obj_t BgL_cdrzd21851zd2_6038;

						BgL_cdrzd21851zd2_6038 = CDR(((obj_t) BgL_xz00_5500));
						if (PAIRP(BgL_cdrzd21851zd2_6038))
							{	/* Expand/initial.scm 1069 */
								obj_t BgL_cdrzd21855zd2_6039;

								BgL_cdrzd21855zd2_6039 = CDR(BgL_cdrzd21851zd2_6038);
								if (PAIRP(BgL_cdrzd21855zd2_6039))
									{	/* Expand/initial.scm 1069 */
										if (NULLP(CDR(BgL_cdrzd21855zd2_6039)))
											{	/* Expand/initial.scm 1069 */
												BgL_vecz00_6030 = CAR(BgL_cdrzd21851zd2_6038);
												BgL_kz00_6031 = CAR(BgL_cdrzd21855zd2_6039);
												{	/* Expand/initial.scm 1072 */
													obj_t BgL_evecz00_6032;
													obj_t BgL_ekz00_6033;

													{	/* Expand/initial.scm 1072 */
														obj_t BgL_tmpfunz00_7811;

														BgL_tmpfunz00_7811 = ((obj_t) BgL_ez00_5501);
														BgL_evecz00_6032 =
															(VA_PROCEDUREP(BgL_tmpfunz00_7811)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7811)) (((obj_t)
																	BgL_ez00_5501), BgL_vecz00_6030,
																((obj_t) BgL_ez00_5501),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7811)) (((obj_t)
																	BgL_ez00_5501), BgL_vecz00_6030,
																((obj_t) BgL_ez00_5501)));
													}
													{	/* Expand/initial.scm 1073 */
														obj_t BgL_tmpfunz00_7819;

														BgL_tmpfunz00_7819 = ((obj_t) BgL_ez00_5501);
														BgL_ekz00_6033 =
															(VA_PROCEDUREP(BgL_tmpfunz00_7819)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7819)) (((obj_t)
																	BgL_ez00_5501), BgL_kz00_6031,
																((obj_t) BgL_ez00_5501),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7819)) (((obj_t)
																	BgL_ez00_5501), BgL_kz00_6031,
																((obj_t) BgL_ez00_5501)));
													}
													if (CBOOL
														(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
														{	/* Expand/initial.scm 1075 */
															obj_t BgL_arg3477z00_6034;

															{	/* Expand/initial.scm 1075 */
																obj_t BgL_arg3479z00_6035;

																BgL_arg3479z00_6035 =
																	MAKE_YOUNG_PAIR(BgL_ekz00_6033, BNIL);
																BgL_arg3477z00_6034 =
																	MAKE_YOUNG_PAIR(BgL_evecz00_6032,
																	BgL_arg3479z00_6035);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(159),
																BgL_arg3477z00_6034);
														}
													else
														{	/* Expand/initial.scm 1076 */
															obj_t BgL_arg3480z00_6036;

															{	/* Expand/initial.scm 1076 */
																obj_t BgL_arg3481z00_6037;

																BgL_arg3481z00_6037 =
																	MAKE_YOUNG_PAIR(BgL_ekz00_6033, BNIL);
																BgL_arg3480z00_6036 =
																	MAKE_YOUNG_PAIR(BgL_evecz00_6032,
																	BgL_arg3481z00_6037);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(150),
																BgL_arg3480z00_6036);
														}
												}
											}
										else
											{	/* Expand/initial.scm 1069 */
											BgL_tagzd21844zd2_6028:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4215z00zzexpand_installz00, BgL_xz00_5500);
											}
									}
								else
									{	/* Expand/initial.scm 1069 */
										goto BgL_tagzd21844zd2_6028;
									}
							}
						else
							{	/* Expand/initial.scm 1069 */
								goto BgL_tagzd21844zd2_6028;
							}
					}
				else
					{	/* Expand/initial.scm 1069 */
						goto BgL_tagzd21844zd2_6028;
					}
			}
		}

	}



/* &<@anonymous:3465> */
	obj_t BGl_z62zc3z04anonymousza33465ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5502, obj_t BgL_xz00_5503, obj_t BgL_ez00_5504)
	{
		{	/* Expand/initial.scm 1063 */
			return
				BGl_boundzd2checkzd2zzexpand_installz00(BgL_xz00_5503,
				CNST_TABLE_REF(157), CNST_TABLE_REF(158), ((obj_t) BgL_ez00_5504));
		}

	}



/* &<@anonymous:3444> */
	obj_t BGl_z62zc3z04anonymousza33444ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5505, obj_t BgL_xz00_5506, obj_t BgL_ez00_5507)
	{
		{	/* Expand/initial.scm 1050 */
			{
				obj_t BgL_vecz00_6044;
				obj_t BgL_kz00_6045;
				obj_t BgL_objz00_6046;

				if (PAIRP(BgL_xz00_5506))
					{	/* Expand/initial.scm 1050 */
						obj_t BgL_cdrzd21827zd2_6056;

						BgL_cdrzd21827zd2_6056 = CDR(((obj_t) BgL_xz00_5506));
						if (PAIRP(BgL_cdrzd21827zd2_6056))
							{	/* Expand/initial.scm 1050 */
								obj_t BgL_cdrzd21832zd2_6057;

								BgL_cdrzd21832zd2_6057 = CDR(BgL_cdrzd21827zd2_6056);
								if (PAIRP(BgL_cdrzd21832zd2_6057))
									{	/* Expand/initial.scm 1050 */
										obj_t BgL_cdrzd21837zd2_6058;

										BgL_cdrzd21837zd2_6058 = CDR(BgL_cdrzd21832zd2_6057);
										if (PAIRP(BgL_cdrzd21837zd2_6058))
											{	/* Expand/initial.scm 1050 */
												if (NULLP(CDR(BgL_cdrzd21837zd2_6058)))
													{	/* Expand/initial.scm 1050 */
														BgL_vecz00_6044 = CAR(BgL_cdrzd21827zd2_6056);
														BgL_kz00_6045 = CAR(BgL_cdrzd21832zd2_6057);
														BgL_objz00_6046 = CAR(BgL_cdrzd21837zd2_6058);
														{	/* Expand/initial.scm 1053 */
															obj_t BgL_evecz00_6047;
															obj_t BgL_ekz00_6048;
															obj_t BgL_eobjz00_6049;

															{	/* Expand/initial.scm 1053 */
																obj_t BgL_tmpfunz00_7859;

																BgL_tmpfunz00_7859 = ((obj_t) BgL_ez00_5507);
																BgL_evecz00_6047 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_7859)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_7859)) ((
																			(obj_t) BgL_ez00_5507), BgL_vecz00_6044,
																		((obj_t) BgL_ez00_5507),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_7859)) ((
																			(obj_t) BgL_ez00_5507), BgL_vecz00_6044,
																		((obj_t) BgL_ez00_5507)));
															}
															{	/* Expand/initial.scm 1054 */
																obj_t BgL_tmpfunz00_7867;

																BgL_tmpfunz00_7867 = ((obj_t) BgL_ez00_5507);
																BgL_ekz00_6048 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_7867)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_7867)) ((
																			(obj_t) BgL_ez00_5507), BgL_kz00_6045,
																		((obj_t) BgL_ez00_5507),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_7867)) ((
																			(obj_t) BgL_ez00_5507), BgL_kz00_6045,
																		((obj_t) BgL_ez00_5507)));
															}
															{	/* Expand/initial.scm 1055 */
																obj_t BgL_tmpfunz00_7875;

																BgL_tmpfunz00_7875 = ((obj_t) BgL_ez00_5507);
																BgL_eobjz00_6049 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_7875)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_7875)) ((
																			(obj_t) BgL_ez00_5507), BgL_objz00_6046,
																		((obj_t) BgL_ez00_5507),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_7875)) ((
																			(obj_t) BgL_ez00_5507), BgL_objz00_6046,
																		((obj_t) BgL_ez00_5507)));
															}
															if (CBOOL
																(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
																{	/* Expand/initial.scm 1057 */
																	obj_t BgL_arg3455z00_6050;

																	{	/* Expand/initial.scm 1057 */
																		obj_t BgL_arg3457z00_6051;

																		{	/* Expand/initial.scm 1057 */
																			obj_t BgL_arg3459z00_6052;

																			BgL_arg3459z00_6052 =
																				MAKE_YOUNG_PAIR(BgL_eobjz00_6049, BNIL);
																			BgL_arg3457z00_6051 =
																				MAKE_YOUNG_PAIR(BgL_ekz00_6048,
																				BgL_arg3459z00_6052);
																		}
																		BgL_arg3455z00_6050 =
																			MAKE_YOUNG_PAIR(BgL_evecz00_6047,
																			BgL_arg3457z00_6051);
																	}
																	return
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(160),
																		BgL_arg3455z00_6050);
																}
															else
																{	/* Expand/initial.scm 1058 */
																	obj_t BgL_arg3460z00_6053;

																	{	/* Expand/initial.scm 1058 */
																		obj_t BgL_arg3462z00_6054;

																		{	/* Expand/initial.scm 1058 */
																			obj_t BgL_arg3463z00_6055;

																			BgL_arg3463z00_6055 =
																				MAKE_YOUNG_PAIR(BgL_eobjz00_6049, BNIL);
																			BgL_arg3462z00_6054 =
																				MAKE_YOUNG_PAIR(BgL_ekz00_6048,
																				BgL_arg3463z00_6055);
																		}
																		BgL_arg3460z00_6053 =
																			MAKE_YOUNG_PAIR(BgL_evecz00_6047,
																			BgL_arg3462z00_6054);
																	}
																	return
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(149),
																		BgL_arg3460z00_6053);
																}
														}
													}
												else
													{	/* Expand/initial.scm 1050 */
													BgL_tagzd21818zd2_6042:
														return
															BGl_errorz00zz__errorz00(BFALSE,
															BGl_string4216z00zzexpand_installz00,
															BgL_xz00_5506);
													}
											}
										else
											{	/* Expand/initial.scm 1050 */
												goto BgL_tagzd21818zd2_6042;
											}
									}
								else
									{	/* Expand/initial.scm 1050 */
										goto BgL_tagzd21818zd2_6042;
									}
							}
						else
							{	/* Expand/initial.scm 1050 */
								goto BgL_tagzd21818zd2_6042;
							}
					}
				else
					{	/* Expand/initial.scm 1050 */
						goto BgL_tagzd21818zd2_6042;
					}
			}
		}

	}



/* &<@anonymous:3382> */
	obj_t BGl_z62zc3z04anonymousza33382ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5508, obj_t BgL_xz00_5509, obj_t BgL_ez00_5510)
	{
		{	/* Expand/initial.scm 1021 */
			{
				obj_t BgL_lblz00_6061;
				obj_t BgL_exprsz00_6062;

				if (PAIRP(BgL_xz00_5509))
					{	/* Expand/initial.scm 1021 */
						obj_t BgL_cdrzd21808zd2_6100;

						BgL_cdrzd21808zd2_6100 = CDR(((obj_t) BgL_xz00_5509));
						if (PAIRP(BgL_cdrzd21808zd2_6100))
							{	/* Expand/initial.scm 1021 */
								obj_t BgL_carzd21811zd2_6101;

								BgL_carzd21811zd2_6101 = CAR(BgL_cdrzd21808zd2_6100);
								if (SYMBOLP(BgL_carzd21811zd2_6101))
									{	/* Expand/initial.scm 1021 */
										BgL_lblz00_6061 = BgL_carzd21811zd2_6101;
										BgL_exprsz00_6062 = CDR(BgL_cdrzd21808zd2_6100);
										{	/* Expand/initial.scm 1024 */
											bool_t BgL_test4413z00_7902;

											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
												(BGl_za2profilezd2modeza2zd2zzengine_paramz00))
												{	/* Expand/initial.scm 1024 */
													if (INTEGERP
														(BGl_za2profilezd2modeza2zd2zzengine_paramz00))
														{	/* Expand/initial.scm 1024 */
															BgL_test4413z00_7902 =
																(
																(long)
																CINT
																(BGl_za2profilezd2modeza2zd2zzengine_paramz00)
																== 0L);
														}
													else
														{	/* Expand/initial.scm 1024 */
															BgL_test4413z00_7902 =
																BGl_2zd3zd3zz__r4_numbers_6_5z00
																(BGl_za2profilezd2modeza2zd2zzengine_paramz00,
																BINT(0L));
														}
												}
											else
												{	/* Expand/initial.scm 1024 */
													BgL_test4413z00_7902 = ((bool_t) 1);
												}
											if (BgL_test4413z00_7902)
												{	/* Expand/initial.scm 1025 */
													obj_t BgL_arg3391z00_6063;

													BgL_arg3391z00_6063 =
														BGl_expandzd2prognzd2zz__prognz00
														(BgL_exprsz00_6062);
													return BGL_PROCEDURE_CALL2(BgL_ez00_5510,
														BgL_arg3391z00_6063, BgL_ez00_5510);
												}
											else
												{	/* Expand/initial.scm 1026 */
													obj_t BgL_laz00_6064;

													{	/* Expand/initial.scm 1026 */
														obj_t BgL_arg3440z00_6065;

														BgL_arg3440z00_6065 =
															MAKE_YOUNG_PAIR(BNIL,
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_exprsz00_6062, BNIL));
														BgL_laz00_6064 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
															BgL_arg3440z00_6065);
													}
													{	/* Expand/initial.scm 1026 */
														obj_t BgL_lamz00_6066;

														if (EPAIRP(BgL_xz00_5509))
															{	/* Expand/initial.scm 1028 */
																obj_t BgL_arg3434z00_6067;
																obj_t BgL_arg3436z00_6068;
																obj_t BgL_arg3439z00_6069;

																BgL_arg3434z00_6067 = CAR(BgL_laz00_6064);
																BgL_arg3436z00_6068 = CDR(BgL_laz00_6064);
																BgL_arg3439z00_6069 =
																	CER(((obj_t) BgL_xz00_5509));
																{	/* Expand/initial.scm 1028 */
																	obj_t BgL_res3987z00_6070;

																	BgL_res3987z00_6070 =
																		MAKE_YOUNG_EPAIR(BgL_arg3434z00_6067,
																		BgL_arg3436z00_6068, BgL_arg3439z00_6069);
																	BgL_lamz00_6066 = BgL_res3987z00_6070;
																}
															}
														else
															{	/* Expand/initial.scm 1027 */
																BgL_lamz00_6066 = BgL_laz00_6064;
															}
														{	/* Expand/initial.scm 1027 */
															obj_t BgL_valz00_6071;

															{	/* Expand/initial.scm 1030 */
																obj_t BgL_symz00_6072;

																BgL_symz00_6072 =
																	BGl_gensymz00zz__r4_symbols_6_4z00
																	(CNST_TABLE_REF(161));
																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																	(BgL_symz00_6072);
																BgL_valz00_6071 = BgL_symz00_6072;
															}
															{	/* Expand/initial.scm 1030 */
																obj_t BgL_auxz00_6073;

																{	/* Expand/initial.scm 1033 */
																	obj_t BgL_arg3396z00_6074;
																	obj_t BgL_arg3397z00_6075;

																	BgL_arg3396z00_6074 =
																		BGl_letzd2symzd2zzast_letz00();
																	{	/* Expand/initial.scm 1034 */
																		obj_t BgL_arg3398z00_6076;
																		obj_t BgL_arg3399z00_6077;

																		{	/* Expand/initial.scm 1034 */
																			obj_t BgL_arg3400z00_6078;

																			{	/* Expand/initial.scm 1034 */
																				obj_t BgL_arg3401z00_6079;

																				BgL_arg3401z00_6079 =
																					MAKE_YOUNG_PAIR(BgL_lamz00_6066,
																					BNIL);
																				BgL_arg3400z00_6078 =
																					MAKE_YOUNG_PAIR(BgL_lblz00_6061,
																					BgL_arg3401z00_6079);
																			}
																			BgL_arg3398z00_6076 =
																				MAKE_YOUNG_PAIR(BgL_arg3400z00_6078,
																				BNIL);
																		}
																		{	/* Expand/initial.scm 1035 */
																			obj_t BgL_arg3402z00_6080;
																			obj_t BgL_arg3403z00_6081;

																			{	/* Expand/initial.scm 1035 */
																				obj_t BgL_arg3404z00_6082;

																				{	/* Expand/initial.scm 1035 */
																					obj_t BgL_arg3405z00_6083;
																					obj_t BgL_arg3406z00_6084;

																					{	/* Expand/initial.scm 1035 */
																						obj_t BgL_arg1455z00_6085;

																						BgL_arg1455z00_6085 =
																							SYMBOL_TO_STRING(BgL_lblz00_6061);
																						BgL_arg3405z00_6083 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_6085);
																					}
																					BgL_arg3406z00_6084 =
																						MAKE_YOUNG_PAIR(BgL_lblz00_6061,
																						BNIL);
																					BgL_arg3404z00_6082 =
																						MAKE_YOUNG_PAIR(BgL_arg3405z00_6083,
																						BgL_arg3406z00_6084);
																				}
																				BgL_arg3402z00_6080 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(162),
																					BgL_arg3404z00_6082);
																			}
																			{	/* Expand/initial.scm 1037 */
																				obj_t BgL_arg3407z00_6086;

																				{	/* Expand/initial.scm 1037 */
																					obj_t BgL_arg3410z00_6087;

																					{	/* Expand/initial.scm 1037 */
																						obj_t BgL_arg3413z00_6088;
																						obj_t BgL_arg3416z00_6089;

																						{	/* Expand/initial.scm 1037 */
																							obj_t BgL_arg3422z00_6090;

																							{	/* Expand/initial.scm 1037 */
																								obj_t BgL_arg3425z00_6091;

																								{	/* Expand/initial.scm 1037 */
																									obj_t BgL_arg3429z00_6092;

																									BgL_arg3429z00_6092 =
																										MAKE_YOUNG_PAIR
																										(BgL_lblz00_6061, BNIL);
																									BgL_arg3425z00_6091 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg3429z00_6092, BNIL);
																								}
																								BgL_arg3422z00_6090 =
																									MAKE_YOUNG_PAIR
																									(BgL_valz00_6071,
																									BgL_arg3425z00_6091);
																							}
																							BgL_arg3413z00_6088 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg3422z00_6090, BNIL);
																						}
																						{	/* Expand/initial.scm 1038 */
																							obj_t BgL_arg3430z00_6093;
																							obj_t BgL_arg3432z00_6094;

																							BgL_arg3430z00_6093 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(163), BNIL);
																							BgL_arg3432z00_6094 =
																								MAKE_YOUNG_PAIR(BgL_valz00_6071,
																								BNIL);
																							BgL_arg3416z00_6089 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg3430z00_6093,
																								BgL_arg3432z00_6094);
																						}
																						BgL_arg3410z00_6087 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg3413z00_6088,
																							BgL_arg3416z00_6089);
																					}
																					BgL_arg3407z00_6086 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(78),
																						BgL_arg3410z00_6087);
																				}
																				BgL_arg3403z00_6081 =
																					MAKE_YOUNG_PAIR(BgL_arg3407z00_6086,
																					BNIL);
																			}
																			BgL_arg3399z00_6077 =
																				MAKE_YOUNG_PAIR(BgL_arg3402z00_6080,
																				BgL_arg3403z00_6081);
																		}
																		BgL_arg3397z00_6075 =
																			MAKE_YOUNG_PAIR(BgL_arg3398z00_6076,
																			BgL_arg3399z00_6077);
																	}
																	BgL_auxz00_6073 =
																		MAKE_YOUNG_PAIR(BgL_arg3396z00_6074,
																		BgL_arg3397z00_6075);
																}
																{	/* Expand/initial.scm 1033 */
																	obj_t BgL_resz00_6095;

																	if (EPAIRP(BgL_xz00_5509))
																		{	/* Expand/initial.scm 1041 */
																			obj_t BgL_arg3393z00_6096;
																			obj_t BgL_arg3394z00_6097;
																			obj_t BgL_arg3395z00_6098;

																			BgL_arg3393z00_6096 =
																				CAR(BgL_auxz00_6073);
																			BgL_arg3394z00_6097 =
																				CDR(BgL_auxz00_6073);
																			BgL_arg3395z00_6098 =
																				CER(((obj_t) BgL_xz00_5509));
																			{	/* Expand/initial.scm 1041 */
																				obj_t BgL_res3988z00_6099;

																				BgL_res3988z00_6099 =
																					MAKE_YOUNG_EPAIR(BgL_arg3393z00_6096,
																					BgL_arg3394z00_6097,
																					BgL_arg3395z00_6098);
																				BgL_resz00_6095 = BgL_res3988z00_6099;
																			}
																		}
																	else
																		{	/* Expand/initial.scm 1040 */
																			BgL_resz00_6095 = BgL_auxz00_6073;
																		}
																	{	/* Expand/initial.scm 1040 */

																		return
																			BGL_PROCEDURE_CALL2(BgL_ez00_5510,
																			BgL_auxz00_6073, BgL_ez00_5510);
																	}
																}
															}
														}
													}
												}
										}
									}
								else
									{	/* Expand/initial.scm 1021 */
									BgL_tagzd21801zd2_6059:
										return
											BGl_errorz00zz__errorz00
											(BGl_string4217z00zzexpand_installz00,
											BGl_string4218z00zzexpand_installz00, BgL_xz00_5509);
									}
							}
						else
							{	/* Expand/initial.scm 1021 */
								goto BgL_tagzd21801zd2_6059;
							}
					}
				else
					{	/* Expand/initial.scm 1021 */
						goto BgL_tagzd21801zd2_6059;
					}
			}
		}

	}



/* &<@anonymous:3308> */
	obj_t BGl_z62zc3z04anonymousza33308ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5511, obj_t BgL_xz00_5512, obj_t BgL_ez00_5513)
	{
		{	/* Expand/initial.scm 993 */
			{
				obj_t BgL_lblz00_6104;
				obj_t BgL_exprsz00_6105;

				if (PAIRP(BgL_xz00_5512))
					{	/* Expand/initial.scm 993 */
						obj_t BgL_cdrzd21791zd2_6143;

						BgL_cdrzd21791zd2_6143 = CDR(((obj_t) BgL_xz00_5512));
						if (PAIRP(BgL_cdrzd21791zd2_6143))
							{	/* Expand/initial.scm 993 */
								obj_t BgL_carzd21794zd2_6144;

								BgL_carzd21794zd2_6144 = CAR(BgL_cdrzd21791zd2_6143);
								if (SYMBOLP(BgL_carzd21794zd2_6144))
									{	/* Expand/initial.scm 993 */
										BgL_lblz00_6104 = BgL_carzd21794zd2_6144;
										BgL_exprsz00_6105 = CDR(BgL_cdrzd21791zd2_6143);
										{	/* Expand/initial.scm 996 */
											bool_t BgL_test4421z00_7979;

											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
												(BGl_za2profilezd2modeza2zd2zzengine_paramz00))
												{	/* Expand/initial.scm 996 */
													if (INTEGERP
														(BGl_za2profilezd2modeza2zd2zzengine_paramz00))
														{	/* Expand/initial.scm 996 */
															BgL_test4421z00_7979 =
																(
																(long)
																CINT
																(BGl_za2profilezd2modeza2zd2zzengine_paramz00)
																== 0L);
														}
													else
														{	/* Expand/initial.scm 996 */
															BgL_test4421z00_7979 =
																BGl_2zd3zd3zz__r4_numbers_6_5z00
																(BGl_za2profilezd2modeza2zd2zzengine_paramz00,
																BINT(0L));
														}
												}
											else
												{	/* Expand/initial.scm 996 */
													BgL_test4421z00_7979 = ((bool_t) 1);
												}
											if (BgL_test4421z00_7979)
												{	/* Expand/initial.scm 997 */
													obj_t BgL_arg3315z00_6106;

													BgL_arg3315z00_6106 =
														BGl_expandzd2prognzd2zz__prognz00
														(BgL_exprsz00_6105);
													return BGL_PROCEDURE_CALL2(BgL_ez00_5513,
														BgL_arg3315z00_6106, BgL_ez00_5513);
												}
											else
												{	/* Expand/initial.scm 998 */
													obj_t BgL_laz00_6107;

													{	/* Expand/initial.scm 998 */
														obj_t BgL_arg3375z00_6108;

														BgL_arg3375z00_6108 =
															MAKE_YOUNG_PAIR(BNIL,
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_exprsz00_6105, BNIL));
														BgL_laz00_6107 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
															BgL_arg3375z00_6108);
													}
													{	/* Expand/initial.scm 998 */
														obj_t BgL_lamz00_6109;

														if (EPAIRP(BgL_xz00_5512))
															{	/* Expand/initial.scm 1000 */
																obj_t BgL_arg3371z00_6110;
																obj_t BgL_arg3372z00_6111;
																obj_t BgL_arg3373z00_6112;

																BgL_arg3371z00_6110 = CAR(BgL_laz00_6107);
																BgL_arg3372z00_6111 = CDR(BgL_laz00_6107);
																BgL_arg3373z00_6112 =
																	CER(((obj_t) BgL_xz00_5512));
																{	/* Expand/initial.scm 1000 */
																	obj_t BgL_res3985z00_6113;

																	BgL_res3985z00_6113 =
																		MAKE_YOUNG_EPAIR(BgL_arg3371z00_6110,
																		BgL_arg3372z00_6111, BgL_arg3373z00_6112);
																	BgL_lamz00_6109 = BgL_res3985z00_6113;
																}
															}
														else
															{	/* Expand/initial.scm 999 */
																BgL_lamz00_6109 = BgL_laz00_6107;
															}
														{	/* Expand/initial.scm 999 */
															obj_t BgL_valz00_6114;

															{	/* Expand/initial.scm 1002 */
																obj_t BgL_symz00_6115;

																BgL_symz00_6115 =
																	BGl_gensymz00zz__r4_symbols_6_4z00
																	(CNST_TABLE_REF(161));
																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																	(BgL_symz00_6115);
																BgL_valz00_6114 = BgL_symz00_6115;
															}
															{	/* Expand/initial.scm 1002 */
																obj_t BgL_auxz00_6116;

																{	/* Expand/initial.scm 1005 */
																	obj_t BgL_arg3322z00_6117;
																	obj_t BgL_arg3323z00_6118;

																	BgL_arg3322z00_6117 =
																		BGl_letzd2symzd2zzast_letz00();
																	{	/* Expand/initial.scm 1006 */
																		obj_t BgL_arg3326z00_6119;
																		obj_t BgL_arg3327z00_6120;

																		{	/* Expand/initial.scm 1006 */
																			obj_t BgL_arg3337z00_6121;

																			{	/* Expand/initial.scm 1006 */
																				obj_t BgL_arg3338z00_6122;

																				BgL_arg3338z00_6122 =
																					MAKE_YOUNG_PAIR(BgL_lamz00_6109,
																					BNIL);
																				BgL_arg3337z00_6121 =
																					MAKE_YOUNG_PAIR(BgL_lblz00_6104,
																					BgL_arg3338z00_6122);
																			}
																			BgL_arg3326z00_6119 =
																				MAKE_YOUNG_PAIR(BgL_arg3337z00_6121,
																				BNIL);
																		}
																		{	/* Expand/initial.scm 1007 */
																			obj_t BgL_arg3339z00_6123;
																			obj_t BgL_arg3349z00_6124;

																			{	/* Expand/initial.scm 1007 */
																				obj_t BgL_arg3350z00_6125;

																				{	/* Expand/initial.scm 1007 */
																					obj_t BgL_arg3352z00_6126;
																					obj_t BgL_arg3356z00_6127;

																					{	/* Expand/initial.scm 1007 */
																						obj_t BgL_arg1455z00_6128;

																						BgL_arg1455z00_6128 =
																							SYMBOL_TO_STRING(BgL_lblz00_6104);
																						BgL_arg3352z00_6126 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_6128);
																					}
																					BgL_arg3356z00_6127 =
																						MAKE_YOUNG_PAIR(BgL_lblz00_6104,
																						BNIL);
																					BgL_arg3350z00_6125 =
																						MAKE_YOUNG_PAIR(BgL_arg3352z00_6126,
																						BgL_arg3356z00_6127);
																				}
																				BgL_arg3339z00_6123 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(164),
																					BgL_arg3350z00_6125);
																			}
																			{	/* Expand/initial.scm 1008 */
																				obj_t BgL_arg3357z00_6129;

																				{	/* Expand/initial.scm 1008 */
																					obj_t BgL_arg3358z00_6130;

																					{	/* Expand/initial.scm 1008 */
																						obj_t BgL_arg3359z00_6131;
																						obj_t BgL_arg3360z00_6132;

																						{	/* Expand/initial.scm 1008 */
																							obj_t BgL_arg3361z00_6133;

																							{	/* Expand/initial.scm 1008 */
																								obj_t BgL_arg3362z00_6134;

																								{	/* Expand/initial.scm 1008 */
																									obj_t BgL_arg3364z00_6135;

																									BgL_arg3364z00_6135 =
																										MAKE_YOUNG_PAIR
																										(BgL_lblz00_6104, BNIL);
																									BgL_arg3362z00_6134 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg3364z00_6135, BNIL);
																								}
																								BgL_arg3361z00_6133 =
																									MAKE_YOUNG_PAIR
																									(BgL_valz00_6114,
																									BgL_arg3362z00_6134);
																							}
																							BgL_arg3359z00_6131 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg3361z00_6133, BNIL);
																						}
																						{	/* Expand/initial.scm 1009 */
																							obj_t BgL_arg3367z00_6136;
																							obj_t BgL_arg3369z00_6137;

																							BgL_arg3367z00_6136 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(163), BNIL);
																							BgL_arg3369z00_6137 =
																								MAKE_YOUNG_PAIR(BgL_valz00_6114,
																								BNIL);
																							BgL_arg3360z00_6132 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg3367z00_6136,
																								BgL_arg3369z00_6137);
																						}
																						BgL_arg3358z00_6130 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg3359z00_6131,
																							BgL_arg3360z00_6132);
																					}
																					BgL_arg3357z00_6129 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(78),
																						BgL_arg3358z00_6130);
																				}
																				BgL_arg3349z00_6124 =
																					MAKE_YOUNG_PAIR(BgL_arg3357z00_6129,
																					BNIL);
																			}
																			BgL_arg3327z00_6120 =
																				MAKE_YOUNG_PAIR(BgL_arg3339z00_6123,
																				BgL_arg3349z00_6124);
																		}
																		BgL_arg3323z00_6118 =
																			MAKE_YOUNG_PAIR(BgL_arg3326z00_6119,
																			BgL_arg3327z00_6120);
																	}
																	BgL_auxz00_6116 =
																		MAKE_YOUNG_PAIR(BgL_arg3322z00_6117,
																		BgL_arg3323z00_6118);
																}
																{	/* Expand/initial.scm 1005 */
																	obj_t BgL_resz00_6138;

																	if (EPAIRP(BgL_xz00_5512))
																		{	/* Expand/initial.scm 1012 */
																			obj_t BgL_arg3317z00_6139;
																			obj_t BgL_arg3318z00_6140;
																			obj_t BgL_arg3321z00_6141;

																			BgL_arg3317z00_6139 =
																				CAR(BgL_auxz00_6116);
																			BgL_arg3318z00_6140 =
																				CDR(BgL_auxz00_6116);
																			BgL_arg3321z00_6141 =
																				CER(((obj_t) BgL_xz00_5512));
																			{	/* Expand/initial.scm 1012 */
																				obj_t BgL_res3986z00_6142;

																				BgL_res3986z00_6142 =
																					MAKE_YOUNG_EPAIR(BgL_arg3317z00_6139,
																					BgL_arg3318z00_6140,
																					BgL_arg3321z00_6141);
																				BgL_resz00_6138 = BgL_res3986z00_6142;
																			}
																		}
																	else
																		{	/* Expand/initial.scm 1011 */
																			BgL_resz00_6138 = BgL_auxz00_6116;
																		}
																	{	/* Expand/initial.scm 1011 */

																		return
																			BGL_PROCEDURE_CALL2(BgL_ez00_5513,
																			BgL_auxz00_6116, BgL_ez00_5513);
																	}
																}
															}
														}
													}
												}
										}
									}
								else
									{	/* Expand/initial.scm 993 */
									BgL_tagzd21784zd2_6102:
										return
											BGl_errorz00zz__errorz00
											(BGl_string4217z00zzexpand_installz00,
											BGl_string4218z00zzexpand_installz00, BgL_xz00_5512);
									}
							}
						else
							{	/* Expand/initial.scm 993 */
								goto BgL_tagzd21784zd2_6102;
							}
					}
				else
					{	/* Expand/initial.scm 993 */
						goto BgL_tagzd21784zd2_6102;
					}
			}
		}

	}



/* &<@anonymous:3306> */
	obj_t BGl_z62zc3z04anonymousza33306ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5514, obj_t BgL_xz00_5515, obj_t BgL_ez00_5516)
	{
		{	/* Expand/initial.scm 952 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5515,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5516));
		}

	}



/* &<@anonymous:3303> */
	obj_t BGl_z62zc3z04anonymousza33303ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5517, obj_t BgL_xz00_5518, obj_t BgL_ez00_5519)
	{
		{	/* Expand/initial.scm 946 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5518,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5519));
		}

	}



/* &<@anonymous:3276> */
	obj_t BGl_z62zc3z04anonymousza33276ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5520, obj_t BgL_xz00_5521, obj_t BgL_ez00_5522)
	{
		{	/* Expand/initial.scm 936 */
			{
				obj_t BgL_exprz00_6158;

				if (PAIRP(BgL_xz00_5521))
					{	/* Expand/initial.scm 936 */
						obj_t BgL_cdrzd21776zd2_6161;

						BgL_cdrzd21776zd2_6161 = CDR(((obj_t) BgL_xz00_5521));
						if (PAIRP(BgL_cdrzd21776zd2_6161))
							{	/* Expand/initial.scm 936 */
								obj_t BgL_cdrzd21779zd2_6162;

								BgL_cdrzd21779zd2_6162 = CDR(BgL_cdrzd21776zd2_6161);
								if (PAIRP(BgL_cdrzd21779zd2_6162))
									{	/* Expand/initial.scm 936 */
										if ((CAR(BgL_cdrzd21779zd2_6162) == CNST_TABLE_REF(167)))
											{	/* Expand/initial.scm 936 */
												if (NULLP(CDR(BgL_cdrzd21779zd2_6162)))
													{	/* Expand/initial.scm 936 */
														BgL_exprz00_6158 = CAR(BgL_cdrzd21776zd2_6161);
														{	/* Expand/initial.scm 939 */
															obj_t BgL_arg3294z00_6159;

															{	/* Expand/initial.scm 939 */
																obj_t BgL_arg3295z00_6160;

																BgL_arg3295z00_6160 =
																	MAKE_YOUNG_PAIR(BgL_exprz00_6158, BNIL);
																BgL_arg3294z00_6159 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(166),
																	BgL_arg3295z00_6160);
															}
															{	/* Expand/initial.scm 939 */
																obj_t BgL_tmpfunz00_8078;

																BgL_tmpfunz00_8078 = ((obj_t) BgL_ez00_5522);
																return
																	(VA_PROCEDUREP(BgL_tmpfunz00_8078)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_8078)) ((
																			(obj_t) BgL_ez00_5522),
																		BgL_arg3294z00_6159,
																		((obj_t) BgL_ez00_5522),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_8078)) ((
																			(obj_t) BgL_ez00_5522),
																		BgL_arg3294z00_6159,
																		((obj_t) BgL_ez00_5522)));
															}
														}
													}
												else
													{	/* Expand/initial.scm 936 */
													BgL_tagzd21771zd2_6148:
														if (NULLP(BgL_xz00_5521))
															{	/* Expand/initial.scm 941 */
																return BNIL;
															}
														else
															{	/* Expand/initial.scm 941 */
																obj_t BgL_head1383z00_6150;

																BgL_head1383z00_6150 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1381z00_6152;
																	obj_t BgL_tail1384z00_6153;

																	BgL_l1381z00_6152 = BgL_xz00_5521;
																	BgL_tail1384z00_6153 = BgL_head1383z00_6150;
																BgL_zc3z04anonymousza33297ze3z87_6151:
																	if (NULLP(BgL_l1381z00_6152))
																		{	/* Expand/initial.scm 941 */
																			return CDR(BgL_head1383z00_6150);
																		}
																	else
																		{	/* Expand/initial.scm 941 */
																			obj_t BgL_newtail1385z00_6154;

																			{	/* Expand/initial.scm 941 */
																				obj_t BgL_arg3301z00_6155;

																				{	/* Expand/initial.scm 941 */
																					obj_t BgL_xz00_6156;

																					BgL_xz00_6156 =
																						CAR(((obj_t) BgL_l1381z00_6152));
																					{	/* Expand/initial.scm 941 */
																						obj_t BgL_tmpfunz00_8095;

																						BgL_tmpfunz00_8095 =
																							((obj_t) BgL_ez00_5522);
																						BgL_arg3301z00_6155 =
																							(VA_PROCEDUREP(BgL_tmpfunz00_8095)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8095)) (((obj_t)
																									BgL_ez00_5522), BgL_xz00_6156,
																								((obj_t) BgL_ez00_5522),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8095)) (((obj_t)
																									BgL_ez00_5522), BgL_xz00_6156,
																								((obj_t) BgL_ez00_5522)));
																					}
																				}
																				BgL_newtail1385z00_6154 =
																					MAKE_YOUNG_PAIR(BgL_arg3301z00_6155,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1384z00_6153,
																				BgL_newtail1385z00_6154);
																			{	/* Expand/initial.scm 941 */
																				obj_t BgL_arg3299z00_6157;

																				BgL_arg3299z00_6157 =
																					CDR(((obj_t) BgL_l1381z00_6152));
																				{
																					obj_t BgL_tail1384z00_8102;
																					obj_t BgL_l1381z00_8101;

																					BgL_l1381z00_8101 =
																						BgL_arg3299z00_6157;
																					BgL_tail1384z00_8102 =
																						BgL_newtail1385z00_6154;
																					BgL_tail1384z00_6153 =
																						BgL_tail1384z00_8102;
																					BgL_l1381z00_6152 = BgL_l1381z00_8101;
																					goto
																						BgL_zc3z04anonymousza33297ze3z87_6151;
																				}
																			}
																		}
																}
															}
													}
											}
										else
											{	/* Expand/initial.scm 936 */
												goto BgL_tagzd21771zd2_6148;
											}
									}
								else
									{	/* Expand/initial.scm 936 */
										goto BgL_tagzd21771zd2_6148;
									}
							}
						else
							{	/* Expand/initial.scm 936 */
								goto BgL_tagzd21771zd2_6148;
							}
					}
				else
					{	/* Expand/initial.scm 936 */
						goto BgL_tagzd21771zd2_6148;
					}
			}
		}

	}



/* &<@anonymous:3213> */
	obj_t BGl_z62zc3z04anonymousza33213ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5523, obj_t BgL_xz00_5524, obj_t BgL_ez00_5525)
	{
		{	/* Expand/initial.scm 916 */
			{
				obj_t BgL_portz00_6204;
				obj_t BgL_portz00_6175;
				obj_t BgL_objz00_6176;

				if (PAIRP(BgL_xz00_5524))
					{	/* Expand/initial.scm 916 */
						obj_t BgL_cdrzd21742zd2_6210;

						BgL_cdrzd21742zd2_6210 = CDR(((obj_t) BgL_xz00_5524));
						if (PAIRP(BgL_cdrzd21742zd2_6210))
							{	/* Expand/initial.scm 916 */
								if (NULLP(CDR(BgL_cdrzd21742zd2_6210)))
									{	/* Expand/initial.scm 916 */
										BgL_portz00_6204 = CAR(BgL_cdrzd21742zd2_6210);
										{	/* Expand/initial.scm 919 */
											obj_t BgL_arg3226z00_6205;

											{	/* Expand/initial.scm 919 */
												obj_t BgL_arg3229z00_6206;
												obj_t BgL_arg3230z00_6207;

												{	/* Expand/initial.scm 919 */
													obj_t BgL_arg3231z00_6208;

													{	/* Expand/initial.scm 919 */
														obj_t BgL_arg3233z00_6209;

														BgL_arg3233z00_6209 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(169), BNIL);
														BgL_arg3231z00_6208 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(170),
															BgL_arg3233z00_6209);
													}
													BgL_arg3229z00_6206 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
														BgL_arg3231z00_6208);
												}
												BgL_arg3230z00_6207 =
													MAKE_YOUNG_PAIR(BgL_portz00_6204, BNIL);
												BgL_arg3226z00_6205 =
													MAKE_YOUNG_PAIR(BgL_arg3229z00_6206,
													BgL_arg3230z00_6207);
											}
											{	/* Expand/initial.scm 919 */
												obj_t BgL_tmpfunz00_8126;

												BgL_tmpfunz00_8126 = ((obj_t) BgL_ez00_5525);
												return
													(VA_PROCEDUREP(BgL_tmpfunz00_8126) ? ((obj_t(*)(obj_t,
																...))
														PROCEDURE_ENTRY(BgL_tmpfunz00_8126)) (((obj_t)
															BgL_ez00_5525), BgL_arg3226z00_6205,
														((obj_t) BgL_ez00_5525), BEOA) : ((obj_t(*)(obj_t,
																obj_t,
																obj_t))
														PROCEDURE_ENTRY(BgL_tmpfunz00_8126)) (((obj_t)
															BgL_ez00_5525), BgL_arg3226z00_6205,
														((obj_t) BgL_ez00_5525)));
											}
										}
									}
								else
									{	/* Expand/initial.scm 916 */
										obj_t BgL_arg3221z00_6211;
										obj_t BgL_arg3222z00_6212;

										BgL_arg3221z00_6211 = CAR(((obj_t) BgL_cdrzd21742zd2_6210));
										BgL_arg3222z00_6212 = CDR(((obj_t) BgL_cdrzd21742zd2_6210));
										BgL_portz00_6175 = BgL_arg3221z00_6211;
										BgL_objz00_6176 = BgL_arg3222z00_6212;
										{	/* Expand/initial.scm 922 */
											obj_t BgL_auxz00_6177;

											BgL_auxz00_6177 =
												BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
												(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
													(168)));
											{	/* Expand/initial.scm 923 */
												obj_t BgL_arg3234z00_6178;

												{	/* Expand/initial.scm 923 */
													obj_t BgL_arg3236z00_6179;
													obj_t BgL_arg3237z00_6180;

													BgL_arg3236z00_6179 = BGl_letzd2symzd2zzast_letz00();
													{	/* Expand/initial.scm 924 */
														obj_t BgL_arg3239z00_6181;
														obj_t BgL_arg3240z00_6182;

														{	/* Expand/initial.scm 924 */
															obj_t BgL_arg3241z00_6183;

															{	/* Expand/initial.scm 924 */
																obj_t BgL_arg3242z00_6184;

																BgL_arg3242z00_6184 =
																	MAKE_YOUNG_PAIR(BgL_portz00_6175, BNIL);
																BgL_arg3241z00_6183 =
																	MAKE_YOUNG_PAIR(BgL_auxz00_6177,
																	BgL_arg3242z00_6184);
															}
															BgL_arg3239z00_6181 =
																MAKE_YOUNG_PAIR(BgL_arg3241z00_6183, BNIL);
														}
														{	/* Expand/initial.scm 925 */
															obj_t BgL_arg3245z00_6185;
															obj_t BgL_arg3246z00_6186;

															if (NULLP(BgL_objz00_6176))
																{	/* Expand/initial.scm 925 */
																	BgL_arg3245z00_6185 = BNIL;
																}
															else
																{	/* Expand/initial.scm 925 */
																	obj_t BgL_head1373z00_6187;

																	BgL_head1373z00_6187 =
																		MAKE_YOUNG_PAIR(BNIL, BNIL);
																	{
																		obj_t BgL_l1371z00_6189;
																		obj_t BgL_tail1374z00_6190;

																		BgL_l1371z00_6189 = BgL_objz00_6176;
																		BgL_tail1374z00_6190 = BgL_head1373z00_6187;
																	BgL_zc3z04anonymousza33248ze3z87_6188:
																		if (NULLP(BgL_l1371z00_6189))
																			{	/* Expand/initial.scm 925 */
																				BgL_arg3245z00_6185 =
																					CDR(BgL_head1373z00_6187);
																			}
																		else
																			{	/* Expand/initial.scm 925 */
																				obj_t BgL_newtail1375z00_6191;

																				{	/* Expand/initial.scm 925 */
																					obj_t BgL_arg3251z00_6192;

																					{	/* Expand/initial.scm 925 */
																						obj_t BgL_yz00_6193;

																						BgL_yz00_6193 =
																							CAR(((obj_t) BgL_l1371z00_6189));
																						{	/* Expand/initial.scm 926 */
																							obj_t BgL_arg3252z00_6194;

																							{	/* Expand/initial.scm 926 */
																								obj_t BgL_arg3254z00_6195;
																								obj_t BgL_arg3255z00_6196;

																								BgL_arg3254z00_6195 =
																									BGl_dispz00zzexpand_installz00
																									(BgL_yz00_6193);
																								{	/* Expand/initial.scm 926 */
																									obj_t BgL_arg3256z00_6197;

																									BgL_arg3256z00_6197 =
																										MAKE_YOUNG_PAIR
																										(BgL_auxz00_6177, BNIL);
																									BgL_arg3255z00_6196 =
																										MAKE_YOUNG_PAIR
																										(BgL_yz00_6193,
																										BgL_arg3256z00_6197);
																								}
																								BgL_arg3252z00_6194 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg3254z00_6195,
																									BgL_arg3255z00_6196);
																							}
																							BgL_arg3251z00_6192 =
																								BGl_epairifyz00zztools_miscz00
																								(BgL_arg3252z00_6194,
																								BgL_xz00_5524);
																						}
																					}
																					BgL_newtail1375z00_6191 =
																						MAKE_YOUNG_PAIR(BgL_arg3251z00_6192,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1374z00_6190,
																					BgL_newtail1375z00_6191);
																				{	/* Expand/initial.scm 925 */
																					obj_t BgL_arg3250z00_6198;

																					BgL_arg3250z00_6198 =
																						CDR(((obj_t) BgL_l1371z00_6189));
																					{
																						obj_t BgL_tail1374z00_8158;
																						obj_t BgL_l1371z00_8157;

																						BgL_l1371z00_8157 =
																							BgL_arg3250z00_6198;
																						BgL_tail1374z00_8158 =
																							BgL_newtail1375z00_6191;
																						BgL_tail1374z00_6190 =
																							BgL_tail1374z00_8158;
																						BgL_l1371z00_6189 =
																							BgL_l1371z00_8157;
																						goto
																							BgL_zc3z04anonymousza33248ze3z87_6188;
																					}
																				}
																			}
																	}
																}
															{	/* Expand/initial.scm 928 */
																obj_t BgL_arg3259z00_6199;

																{	/* Expand/initial.scm 928 */
																	obj_t BgL_arg3263z00_6200;
																	obj_t BgL_arg3264z00_6201;

																	{	/* Expand/initial.scm 928 */
																		obj_t BgL_arg3266z00_6202;

																		{	/* Expand/initial.scm 928 */
																			obj_t BgL_arg3267z00_6203;

																			BgL_arg3267z00_6203 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(169),
																				BNIL);
																			BgL_arg3266z00_6202 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(170),
																				BgL_arg3267z00_6203);
																		}
																		BgL_arg3263z00_6200 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
																			BgL_arg3266z00_6202);
																	}
																	BgL_arg3264z00_6201 =
																		MAKE_YOUNG_PAIR(BgL_auxz00_6177, BNIL);
																	BgL_arg3259z00_6199 =
																		MAKE_YOUNG_PAIR(BgL_arg3263z00_6200,
																		BgL_arg3264z00_6201);
																}
																BgL_arg3246z00_6186 =
																	MAKE_YOUNG_PAIR(BgL_arg3259z00_6199, BNIL);
															}
															BgL_arg3240z00_6182 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_arg3245z00_6185, BgL_arg3246z00_6186);
														}
														BgL_arg3237z00_6180 =
															MAKE_YOUNG_PAIR(BgL_arg3239z00_6181,
															BgL_arg3240z00_6182);
													}
													BgL_arg3234z00_6178 =
														MAKE_YOUNG_PAIR(BgL_arg3236z00_6179,
														BgL_arg3237z00_6180);
												}
												{	/* Expand/initial.scm 923 */
													obj_t BgL_tmpfunz00_8177;

													BgL_tmpfunz00_8177 = ((obj_t) BgL_ez00_5525);
													return
														(VA_PROCEDUREP(BgL_tmpfunz00_8177)
														? ((obj_t(*)(obj_t,
																	...))
															PROCEDURE_ENTRY(BgL_tmpfunz00_8177)) (((obj_t)
																BgL_ez00_5525), BgL_arg3234z00_6178,
															((obj_t) BgL_ez00_5525), BEOA) : ((obj_t(*)(obj_t,
																	obj_t,
																	obj_t))
															PROCEDURE_ENTRY(BgL_tmpfunz00_8177)) (((obj_t)
																BgL_ez00_5525), BgL_arg3234z00_6178,
															((obj_t) BgL_ez00_5525)));
												}
											}
										}
									}
							}
						else
							{	/* Expand/initial.scm 916 */
							BgL_tagzd21736zd2_6164:
								if (NULLP(BgL_xz00_5524))
									{	/* Expand/initial.scm 931 */
										return BNIL;
									}
								else
									{	/* Expand/initial.scm 931 */
										obj_t BgL_head1378z00_6167;

										BgL_head1378z00_6167 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1376z00_6169;
											obj_t BgL_tail1379z00_6170;

											BgL_l1376z00_6169 = BgL_xz00_5524;
											BgL_tail1379z00_6170 = BgL_head1378z00_6167;
										BgL_zc3z04anonymousza33270ze3z87_6168:
											if (NULLP(BgL_l1376z00_6169))
												{	/* Expand/initial.scm 931 */
													return CDR(BgL_head1378z00_6167);
												}
											else
												{	/* Expand/initial.scm 931 */
													obj_t BgL_newtail1380z00_6171;

													{	/* Expand/initial.scm 931 */
														obj_t BgL_arg3273z00_6172;

														{	/* Expand/initial.scm 931 */
															obj_t BgL_xz00_6173;

															BgL_xz00_6173 = CAR(((obj_t) BgL_l1376z00_6169));
															{	/* Expand/initial.scm 931 */
																obj_t BgL_tmpfunz00_8193;

																BgL_tmpfunz00_8193 = ((obj_t) BgL_ez00_5525);
																BgL_arg3273z00_6172 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_8193)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_8193)) ((
																			(obj_t) BgL_ez00_5525), BgL_xz00_6173,
																		((obj_t) BgL_ez00_5525),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_8193)) ((
																			(obj_t) BgL_ez00_5525), BgL_xz00_6173,
																		((obj_t) BgL_ez00_5525)));
															}
														}
														BgL_newtail1380z00_6171 =
															MAKE_YOUNG_PAIR(BgL_arg3273z00_6172, BNIL);
													}
													SET_CDR(BgL_tail1379z00_6170,
														BgL_newtail1380z00_6171);
													{	/* Expand/initial.scm 931 */
														obj_t BgL_arg3272z00_6174;

														BgL_arg3272z00_6174 =
															CDR(((obj_t) BgL_l1376z00_6169));
														{
															obj_t BgL_tail1379z00_8200;
															obj_t BgL_l1376z00_8199;

															BgL_l1376z00_8199 = BgL_arg3272z00_6174;
															BgL_tail1379z00_8200 = BgL_newtail1380z00_6171;
															BgL_tail1379z00_6170 = BgL_tail1379z00_8200;
															BgL_l1376z00_6169 = BgL_l1376z00_8199;
															goto BgL_zc3z04anonymousza33270ze3z87_6168;
														}
													}
												}
										}
									}
							}
					}
				else
					{	/* Expand/initial.scm 916 */
						goto BgL_tagzd21736zd2_6164;
					}
			}
		}

	}



/* &<@anonymous:3141> */
	obj_t BGl_z62zc3z04anonymousza33141ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5526, obj_t BgL_xz00_5527, obj_t BgL_ez00_5528)
	{
		{	/* Expand/initial.scm 898 */
			{
				obj_t BgL_objz00_6216;

				if (PAIRP(BgL_xz00_5527))
					{	/* Expand/initial.scm 898 */
						if (NULLP(CDR(((obj_t) BgL_xz00_5527))))
							{	/* Expand/initial.scm 898 */
								{	/* Expand/initial.scm 901 */
									obj_t BgL_arg3154z00_6248;
									obj_t BgL_arg3155z00_6249;

									{	/* Expand/initial.scm 901 */
										obj_t BgL_arg3156z00_6250;

										{	/* Expand/initial.scm 901 */
											obj_t BgL_arg3157z00_6251;

											BgL_arg3157z00_6251 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(169), BNIL);
											BgL_arg3156z00_6250 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(170),
												BgL_arg3157z00_6251);
										}
										BgL_arg3154z00_6248 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(171), BgL_arg3156z00_6250);
									}
									{	/* Expand/initial.scm 902 */
										obj_t BgL_arg3158z00_6252;

										{	/* Expand/initial.scm 902 */
											obj_t BgL_arg3160z00_6253;

											{	/* Expand/initial.scm 902 */
												obj_t BgL_arg3161z00_6254;

												{	/* Expand/initial.scm 902 */
													obj_t BgL_arg3162z00_6255;

													BgL_arg3162z00_6255 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(172), BNIL);
													BgL_arg3161z00_6254 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(173),
														BgL_arg3162z00_6255);
												}
												BgL_arg3160z00_6253 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
													BgL_arg3161z00_6254);
											}
											BgL_arg3158z00_6252 =
												MAKE_YOUNG_PAIR(BgL_arg3160z00_6253, BNIL);
										}
										BgL_arg3155z00_6249 =
											MAKE_YOUNG_PAIR(BgL_arg3158z00_6252, BNIL);
									}
									return
										MAKE_YOUNG_PAIR(BgL_arg3154z00_6248, BgL_arg3155z00_6249);
								}
							}
						else
							{	/* Expand/initial.scm 898 */
								obj_t BgL_arg3146z00_6256;

								BgL_arg3146z00_6256 = CDR(((obj_t) BgL_xz00_5527));
								BgL_objz00_6216 = BgL_arg3146z00_6256;
								{	/* Expand/initial.scm 904 */
									obj_t BgL_pz00_6217;

									BgL_pz00_6217 =
										BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
										(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(168)));
									{	/* Expand/initial.scm 905 */
										obj_t BgL_arg3163z00_6218;

										{	/* Expand/initial.scm 905 */
											obj_t BgL_arg3171z00_6219;
											obj_t BgL_arg3172z00_6220;

											BgL_arg3171z00_6219 = BGl_letzd2symzd2zzast_letz00();
											{	/* Expand/initial.scm 906 */
												obj_t BgL_arg3174z00_6221;
												obj_t BgL_arg3175z00_6222;

												{	/* Expand/initial.scm 906 */
													obj_t BgL_arg3177z00_6223;

													{	/* Expand/initial.scm 906 */
														obj_t BgL_arg3178z00_6224;

														{	/* Expand/initial.scm 906 */
															obj_t BgL_arg3180z00_6225;

															{	/* Expand/initial.scm 906 */
																obj_t BgL_arg3181z00_6226;

																{	/* Expand/initial.scm 906 */
																	obj_t BgL_arg3182z00_6227;

																	{	/* Expand/initial.scm 906 */
																		obj_t BgL_arg3185z00_6228;

																		BgL_arg3185z00_6228 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(172),
																			BNIL);
																		BgL_arg3182z00_6227 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(173),
																			BgL_arg3185z00_6228);
																	}
																	BgL_arg3181z00_6226 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
																		BgL_arg3182z00_6227);
																}
																BgL_arg3180z00_6225 =
																	MAKE_YOUNG_PAIR(BgL_arg3181z00_6226, BNIL);
															}
															BgL_arg3178z00_6224 =
																MAKE_YOUNG_PAIR(BgL_arg3180z00_6225, BNIL);
														}
														BgL_arg3177z00_6223 =
															MAKE_YOUNG_PAIR(BgL_pz00_6217,
															BgL_arg3178z00_6224);
													}
													BgL_arg3174z00_6221 =
														MAKE_YOUNG_PAIR(BgL_arg3177z00_6223, BNIL);
												}
												{	/* Expand/initial.scm 907 */
													obj_t BgL_arg3186z00_6229;
													obj_t BgL_arg3187z00_6230;

													if (NULLP(BgL_objz00_6216))
														{	/* Expand/initial.scm 907 */
															BgL_arg3186z00_6229 = BNIL;
														}
													else
														{	/* Expand/initial.scm 907 */
															obj_t BgL_head1368z00_6231;

															BgL_head1368z00_6231 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_l1366z00_6233;
																obj_t BgL_tail1369z00_6234;

																BgL_l1366z00_6233 = BgL_objz00_6216;
																BgL_tail1369z00_6234 = BgL_head1368z00_6231;
															BgL_zc3z04anonymousza33189ze3z87_6232:
																if (NULLP(BgL_l1366z00_6233))
																	{	/* Expand/initial.scm 907 */
																		BgL_arg3186z00_6229 =
																			CDR(BgL_head1368z00_6231);
																	}
																else
																	{	/* Expand/initial.scm 907 */
																		obj_t BgL_newtail1370z00_6235;

																		{	/* Expand/initial.scm 907 */
																			obj_t BgL_arg3195z00_6236;

																			{	/* Expand/initial.scm 907 */
																				obj_t BgL_yz00_6237;

																				BgL_yz00_6237 =
																					CAR(((obj_t) BgL_l1366z00_6233));
																				{	/* Expand/initial.scm 908 */
																					obj_t BgL_arg3199z00_6238;

																					{	/* Expand/initial.scm 908 */
																						obj_t BgL_arg3200z00_6239;
																						obj_t BgL_arg3201z00_6240;

																						BgL_arg3200z00_6239 =
																							BGl_dispz00zzexpand_installz00
																							(BgL_yz00_6237);
																						{	/* Expand/initial.scm 908 */
																							obj_t BgL_arg3203z00_6241;

																							BgL_arg3203z00_6241 =
																								MAKE_YOUNG_PAIR(BgL_pz00_6217,
																								BNIL);
																							BgL_arg3201z00_6240 =
																								MAKE_YOUNG_PAIR(BgL_yz00_6237,
																								BgL_arg3203z00_6241);
																						}
																						BgL_arg3199z00_6238 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg3200z00_6239,
																							BgL_arg3201z00_6240);
																					}
																					BgL_arg3195z00_6236 =
																						BGl_epairifyz00zztools_miscz00
																						(BgL_arg3199z00_6238,
																						BgL_xz00_5527);
																				}
																			}
																			BgL_newtail1370z00_6235 =
																				MAKE_YOUNG_PAIR(BgL_arg3195z00_6236,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1369z00_6234,
																			BgL_newtail1370z00_6235);
																		{	/* Expand/initial.scm 907 */
																			obj_t BgL_arg3192z00_6242;

																			BgL_arg3192z00_6242 =
																				CDR(((obj_t) BgL_l1366z00_6233));
																			{
																				obj_t BgL_tail1369z00_8256;
																				obj_t BgL_l1366z00_8255;

																				BgL_l1366z00_8255 = BgL_arg3192z00_6242;
																				BgL_tail1369z00_8256 =
																					BgL_newtail1370z00_6235;
																				BgL_tail1369z00_6234 =
																					BgL_tail1369z00_8256;
																				BgL_l1366z00_6233 = BgL_l1366z00_8255;
																				goto
																					BgL_zc3z04anonymousza33189ze3z87_6232;
																			}
																		}
																	}
															}
														}
													{	/* Expand/initial.scm 910 */
														obj_t BgL_arg3204z00_6243;

														{	/* Expand/initial.scm 910 */
															obj_t BgL_arg3205z00_6244;
															obj_t BgL_arg3207z00_6245;

															{	/* Expand/initial.scm 910 */
																obj_t BgL_arg3208z00_6246;

																{	/* Expand/initial.scm 910 */
																	obj_t BgL_arg3210z00_6247;

																	BgL_arg3210z00_6247 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(169), BNIL);
																	BgL_arg3208z00_6246 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(170),
																		BgL_arg3210z00_6247);
																}
																BgL_arg3205z00_6244 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
																	BgL_arg3208z00_6246);
															}
															BgL_arg3207z00_6245 =
																MAKE_YOUNG_PAIR(BgL_pz00_6217, BNIL);
															BgL_arg3204z00_6243 =
																MAKE_YOUNG_PAIR(BgL_arg3205z00_6244,
																BgL_arg3207z00_6245);
														}
														BgL_arg3187z00_6230 =
															MAKE_YOUNG_PAIR(BgL_arg3204z00_6243, BNIL);
													}
													BgL_arg3175z00_6222 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg3186z00_6229, BgL_arg3187z00_6230);
												}
												BgL_arg3172z00_6220 =
													MAKE_YOUNG_PAIR(BgL_arg3174z00_6221,
													BgL_arg3175z00_6222);
											}
											BgL_arg3163z00_6218 =
												MAKE_YOUNG_PAIR(BgL_arg3171z00_6219,
												BgL_arg3172z00_6220);
										}
										{	/* Expand/initial.scm 905 */
											obj_t BgL_tmpfunz00_8275;

											BgL_tmpfunz00_8275 = ((obj_t) BgL_ez00_5528);
											return
												(VA_PROCEDUREP(BgL_tmpfunz00_8275) ? ((obj_t(*)(obj_t,
															...))
													PROCEDURE_ENTRY(BgL_tmpfunz00_8275)) (((obj_t)
														BgL_ez00_5528), BgL_arg3163z00_6218,
													((obj_t) BgL_ez00_5528), BEOA) : ((obj_t(*)(obj_t,
															obj_t,
															obj_t))
													PROCEDURE_ENTRY(BgL_tmpfunz00_8275)) (((obj_t)
														BgL_ez00_5528), BgL_arg3163z00_6218,
													((obj_t) BgL_ez00_5528)));
										}
									}
								}
							}
					}
				else
					{	/* Expand/initial.scm 898 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:3082> */
	obj_t BGl_z62zc3z04anonymousza33082ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5529, obj_t BgL_xz00_5530, obj_t BgL_ez00_5531)
	{
		{	/* Expand/initial.scm 884 */
			{
				obj_t BgL_objz00_6278;
				obj_t BgL_objz00_6269;
				obj_t BgL_portz00_6270;

				if (PAIRP(BgL_xz00_5530))
					{	/* Expand/initial.scm 884 */
						obj_t BgL_cdrzd21690zd2_6289;

						BgL_cdrzd21690zd2_6289 = CDR(((obj_t) BgL_xz00_5530));
						if (PAIRP(BgL_cdrzd21690zd2_6289))
							{	/* Expand/initial.scm 884 */
								if (NULLP(CDR(BgL_cdrzd21690zd2_6289)))
									{	/* Expand/initial.scm 884 */
										BgL_objz00_6278 = CAR(BgL_cdrzd21690zd2_6289);
										{	/* Expand/initial.scm 887 */
											obj_t BgL_arg3105z00_6279;
											obj_t BgL_arg3106z00_6280;

											{	/* Expand/initial.scm 887 */
												obj_t BgL_arg3114z00_6281;

												{	/* Expand/initial.scm 887 */
													obj_t BgL_arg3115z00_6282;

													BgL_arg3115z00_6282 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(169), BNIL);
													BgL_arg3114z00_6281 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(174),
														BgL_arg3115z00_6282);
												}
												BgL_arg3105z00_6279 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
													BgL_arg3114z00_6281);
											}
											{	/* Expand/initial.scm 888 */
												obj_t BgL_arg3116z00_6283;
												obj_t BgL_arg3118z00_6284;

												{	/* Expand/initial.scm 888 */
													obj_t BgL_tmpfunz00_8298;

													BgL_tmpfunz00_8298 = ((obj_t) BgL_ez00_5531);
													BgL_arg3116z00_6283 =
														(VA_PROCEDUREP(BgL_tmpfunz00_8298)
														? ((obj_t(*)(obj_t,
																	...))
															PROCEDURE_ENTRY(BgL_tmpfunz00_8298)) (((obj_t)
																BgL_ez00_5531), BgL_objz00_6278,
															((obj_t) BgL_ez00_5531), BEOA) : ((obj_t(*)(obj_t,
																	obj_t,
																	obj_t))
															PROCEDURE_ENTRY(BgL_tmpfunz00_8298)) (((obj_t)
																BgL_ez00_5531), BgL_objz00_6278,
															((obj_t) BgL_ez00_5531)));
												}
												{	/* Expand/initial.scm 889 */
													obj_t BgL_arg3121z00_6285;

													{	/* Expand/initial.scm 889 */
														obj_t BgL_arg3122z00_6286;

														{	/* Expand/initial.scm 889 */
															obj_t BgL_arg3124z00_6287;

															{	/* Expand/initial.scm 889 */
																obj_t BgL_arg3125z00_6288;

																BgL_arg3125z00_6288 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(172), BNIL);
																BgL_arg3124z00_6287 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(173),
																	BgL_arg3125z00_6288);
															}
															BgL_arg3122z00_6286 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
																BgL_arg3124z00_6287);
														}
														BgL_arg3121z00_6285 =
															MAKE_YOUNG_PAIR(BgL_arg3122z00_6286, BNIL);
													}
													BgL_arg3118z00_6284 =
														MAKE_YOUNG_PAIR(BgL_arg3121z00_6285, BNIL);
												}
												BgL_arg3106z00_6280 =
													MAKE_YOUNG_PAIR(BgL_arg3116z00_6283,
													BgL_arg3118z00_6284);
											}
											return
												MAKE_YOUNG_PAIR(BgL_arg3105z00_6279,
												BgL_arg3106z00_6280);
										}
									}
								else
									{	/* Expand/initial.scm 884 */
										obj_t BgL_cdrzd21706zd2_6290;

										BgL_cdrzd21706zd2_6290 =
											CDR(((obj_t) BgL_cdrzd21690zd2_6289));
										if (PAIRP(BgL_cdrzd21706zd2_6290))
											{	/* Expand/initial.scm 884 */
												if (NULLP(CDR(BgL_cdrzd21706zd2_6290)))
													{	/* Expand/initial.scm 884 */
														obj_t BgL_arg3093z00_6291;
														obj_t BgL_arg3094z00_6292;

														BgL_arg3093z00_6291 =
															CAR(((obj_t) BgL_cdrzd21690zd2_6289));
														BgL_arg3094z00_6292 = CAR(BgL_cdrzd21706zd2_6290);
														BgL_objz00_6269 = BgL_arg3093z00_6291;
														BgL_portz00_6270 = BgL_arg3094z00_6292;
														{	/* Expand/initial.scm 891 */
															obj_t BgL_arg3126z00_6271;
															obj_t BgL_arg3127z00_6272;

															{	/* Expand/initial.scm 891 */
																obj_t BgL_arg3128z00_6273;

																{	/* Expand/initial.scm 891 */
																	obj_t BgL_arg3129z00_6274;

																	BgL_arg3129z00_6274 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(169), BNIL);
																	BgL_arg3128z00_6273 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(174),
																		BgL_arg3129z00_6274);
																}
																BgL_arg3126z00_6271 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
																	BgL_arg3128z00_6273);
															}
															{	/* Expand/initial.scm 891 */
																obj_t BgL_arg3131z00_6275;
																obj_t BgL_arg3132z00_6276;

																{	/* Expand/initial.scm 891 */
																	obj_t BgL_tmpfunz00_8333;

																	BgL_tmpfunz00_8333 = ((obj_t) BgL_ez00_5531);
																	BgL_arg3131z00_6275 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_8333)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8333)) ((
																				(obj_t) BgL_ez00_5531), BgL_objz00_6269,
																			((obj_t) BgL_ez00_5531),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8333)) ((
																				(obj_t) BgL_ez00_5531), BgL_objz00_6269,
																			((obj_t) BgL_ez00_5531)));
																}
																{	/* Expand/initial.scm 891 */
																	obj_t BgL_arg3133z00_6277;

																	{	/* Expand/initial.scm 891 */
																		obj_t BgL_tmpfunz00_8341;

																		BgL_tmpfunz00_8341 =
																			((obj_t) BgL_ez00_5531);
																		BgL_arg3133z00_6277 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_8341)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8341)) ((
																					(obj_t) BgL_ez00_5531),
																				BgL_portz00_6270,
																				((obj_t) BgL_ez00_5531),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8341)) ((
																					(obj_t) BgL_ez00_5531),
																				BgL_portz00_6270,
																				((obj_t) BgL_ez00_5531)));
																	}
																	BgL_arg3132z00_6276 =
																		MAKE_YOUNG_PAIR(BgL_arg3133z00_6277, BNIL);
																}
																BgL_arg3127z00_6272 =
																	MAKE_YOUNG_PAIR(BgL_arg3131z00_6275,
																	BgL_arg3132z00_6276);
															}
															return
																MAKE_YOUNG_PAIR(BgL_arg3126z00_6271,
																BgL_arg3127z00_6272);
														}
													}
												else
													{	/* Expand/initial.scm 884 */
													BgL_tagzd21684zd2_6258:
														if (NULLP(BgL_xz00_5530))
															{	/* Expand/initial.scm 893 */
																return BNIL;
															}
														else
															{	/* Expand/initial.scm 893 */
																obj_t BgL_head1363z00_6261;

																BgL_head1363z00_6261 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1361z00_6263;
																	obj_t BgL_tail1364z00_6264;

																	BgL_l1361z00_6263 = BgL_xz00_5530;
																	BgL_tail1364z00_6264 = BgL_head1363z00_6261;
																BgL_zc3z04anonymousza33135ze3z87_6262:
																	if (NULLP(BgL_l1361z00_6263))
																		{	/* Expand/initial.scm 893 */
																			return CDR(BgL_head1363z00_6261);
																		}
																	else
																		{	/* Expand/initial.scm 893 */
																			obj_t BgL_newtail1365z00_6265;

																			{	/* Expand/initial.scm 893 */
																				obj_t BgL_arg3139z00_6266;

																				{	/* Expand/initial.scm 893 */
																					obj_t BgL_xz00_6267;

																					BgL_xz00_6267 =
																						CAR(((obj_t) BgL_l1361z00_6263));
																					{	/* Expand/initial.scm 893 */
																						obj_t BgL_tmpfunz00_8360;

																						BgL_tmpfunz00_8360 =
																							((obj_t) BgL_ez00_5531);
																						BgL_arg3139z00_6266 =
																							(VA_PROCEDUREP(BgL_tmpfunz00_8360)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8360)) (((obj_t)
																									BgL_ez00_5531), BgL_xz00_6267,
																								((obj_t) BgL_ez00_5531),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8360)) (((obj_t)
																									BgL_ez00_5531), BgL_xz00_6267,
																								((obj_t) BgL_ez00_5531)));
																					}
																				}
																				BgL_newtail1365z00_6265 =
																					MAKE_YOUNG_PAIR(BgL_arg3139z00_6266,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1364z00_6264,
																				BgL_newtail1365z00_6265);
																			{	/* Expand/initial.scm 893 */
																				obj_t BgL_arg3137z00_6268;

																				BgL_arg3137z00_6268 =
																					CDR(((obj_t) BgL_l1361z00_6263));
																				{
																					obj_t BgL_tail1364z00_8367;
																					obj_t BgL_l1361z00_8366;

																					BgL_l1361z00_8366 =
																						BgL_arg3137z00_6268;
																					BgL_tail1364z00_8367 =
																						BgL_newtail1365z00_6265;
																					BgL_tail1364z00_6264 =
																						BgL_tail1364z00_8367;
																					BgL_l1361z00_6263 = BgL_l1361z00_8366;
																					goto
																						BgL_zc3z04anonymousza33135ze3z87_6262;
																				}
																			}
																		}
																}
															}
													}
											}
										else
											{	/* Expand/initial.scm 884 */
												goto BgL_tagzd21684zd2_6258;
											}
									}
							}
						else
							{	/* Expand/initial.scm 884 */
								goto BgL_tagzd21684zd2_6258;
							}
					}
				else
					{	/* Expand/initial.scm 884 */
						goto BgL_tagzd21684zd2_6258;
					}
			}
		}

	}



/* &<@anonymous:3021> */
	obj_t BGl_z62zc3z04anonymousza33021ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5532, obj_t BgL_xz00_5533, obj_t BgL_ez00_5534)
	{
		{	/* Expand/initial.scm 870 */
			{
				obj_t BgL_objz00_6314;
				obj_t BgL_objz00_6305;
				obj_t BgL_portz00_6306;

				if (PAIRP(BgL_xz00_5533))
					{	/* Expand/initial.scm 870 */
						obj_t BgL_cdrzd21651zd2_6325;

						BgL_cdrzd21651zd2_6325 = CDR(((obj_t) BgL_xz00_5533));
						if (PAIRP(BgL_cdrzd21651zd2_6325))
							{	/* Expand/initial.scm 870 */
								if (NULLP(CDR(BgL_cdrzd21651zd2_6325)))
									{	/* Expand/initial.scm 870 */
										BgL_objz00_6314 = CAR(BgL_cdrzd21651zd2_6325);
										{	/* Expand/initial.scm 873 */
											obj_t BgL_arg3043z00_6315;
											obj_t BgL_arg3044z00_6316;

											{	/* Expand/initial.scm 873 */
												obj_t BgL_arg3049z00_6317;

												{	/* Expand/initial.scm 873 */
													obj_t BgL_arg3050z00_6318;

													BgL_arg3050z00_6318 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(169), BNIL);
													BgL_arg3049z00_6317 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(175),
														BgL_arg3050z00_6318);
												}
												BgL_arg3043z00_6315 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
													BgL_arg3049z00_6317);
											}
											{	/* Expand/initial.scm 874 */
												obj_t BgL_arg3054z00_6319;
												obj_t BgL_arg3055z00_6320;

												{	/* Expand/initial.scm 874 */
													obj_t BgL_tmpfunz00_8389;

													BgL_tmpfunz00_8389 = ((obj_t) BgL_ez00_5534);
													BgL_arg3054z00_6319 =
														(VA_PROCEDUREP(BgL_tmpfunz00_8389)
														? ((obj_t(*)(obj_t,
																	...))
															PROCEDURE_ENTRY(BgL_tmpfunz00_8389)) (((obj_t)
																BgL_ez00_5534), BgL_objz00_6314,
															((obj_t) BgL_ez00_5534), BEOA) : ((obj_t(*)(obj_t,
																	obj_t,
																	obj_t))
															PROCEDURE_ENTRY(BgL_tmpfunz00_8389)) (((obj_t)
																BgL_ez00_5534), BgL_objz00_6314,
															((obj_t) BgL_ez00_5534)));
												}
												{	/* Expand/initial.scm 875 */
													obj_t BgL_arg3058z00_6321;

													{	/* Expand/initial.scm 875 */
														obj_t BgL_arg3059z00_6322;

														{	/* Expand/initial.scm 875 */
															obj_t BgL_arg3061z00_6323;

															{	/* Expand/initial.scm 875 */
																obj_t BgL_arg3062z00_6324;

																BgL_arg3062z00_6324 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(172), BNIL);
																BgL_arg3061z00_6323 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(173),
																	BgL_arg3062z00_6324);
															}
															BgL_arg3059z00_6322 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
																BgL_arg3061z00_6323);
														}
														BgL_arg3058z00_6321 =
															MAKE_YOUNG_PAIR(BgL_arg3059z00_6322, BNIL);
													}
													BgL_arg3055z00_6320 =
														MAKE_YOUNG_PAIR(BgL_arg3058z00_6321, BNIL);
												}
												BgL_arg3044z00_6316 =
													MAKE_YOUNG_PAIR(BgL_arg3054z00_6319,
													BgL_arg3055z00_6320);
											}
											return
												MAKE_YOUNG_PAIR(BgL_arg3043z00_6315,
												BgL_arg3044z00_6316);
										}
									}
								else
									{	/* Expand/initial.scm 870 */
										obj_t BgL_cdrzd21667zd2_6326;

										BgL_cdrzd21667zd2_6326 =
											CDR(((obj_t) BgL_cdrzd21651zd2_6325));
										if (PAIRP(BgL_cdrzd21667zd2_6326))
											{	/* Expand/initial.scm 870 */
												if (NULLP(CDR(BgL_cdrzd21667zd2_6326)))
													{	/* Expand/initial.scm 870 */
														obj_t BgL_arg3031z00_6327;
														obj_t BgL_arg3032z00_6328;

														BgL_arg3031z00_6327 =
															CAR(((obj_t) BgL_cdrzd21651zd2_6325));
														BgL_arg3032z00_6328 = CAR(BgL_cdrzd21667zd2_6326);
														BgL_objz00_6305 = BgL_arg3031z00_6327;
														BgL_portz00_6306 = BgL_arg3032z00_6328;
														{	/* Expand/initial.scm 877 */
															obj_t BgL_arg3066z00_6307;
															obj_t BgL_arg3067z00_6308;

															{	/* Expand/initial.scm 877 */
																obj_t BgL_arg3070z00_6309;

																{	/* Expand/initial.scm 877 */
																	obj_t BgL_arg3071z00_6310;

																	BgL_arg3071z00_6310 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(169), BNIL);
																	BgL_arg3070z00_6309 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(175),
																		BgL_arg3071z00_6310);
																}
																BgL_arg3066z00_6307 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
																	BgL_arg3070z00_6309);
															}
															{	/* Expand/initial.scm 877 */
																obj_t BgL_arg3072z00_6311;
																obj_t BgL_arg3073z00_6312;

																{	/* Expand/initial.scm 877 */
																	obj_t BgL_tmpfunz00_8424;

																	BgL_tmpfunz00_8424 = ((obj_t) BgL_ez00_5534);
																	BgL_arg3072z00_6311 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_8424)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8424)) ((
																				(obj_t) BgL_ez00_5534), BgL_objz00_6305,
																			((obj_t) BgL_ez00_5534),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8424)) ((
																				(obj_t) BgL_ez00_5534), BgL_objz00_6305,
																			((obj_t) BgL_ez00_5534)));
																}
																{	/* Expand/initial.scm 877 */
																	obj_t BgL_arg3075z00_6313;

																	{	/* Expand/initial.scm 877 */
																		obj_t BgL_tmpfunz00_8432;

																		BgL_tmpfunz00_8432 =
																			((obj_t) BgL_ez00_5534);
																		BgL_arg3075z00_6313 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_8432)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8432)) ((
																					(obj_t) BgL_ez00_5534),
																				BgL_portz00_6306,
																				((obj_t) BgL_ez00_5534),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8432)) ((
																					(obj_t) BgL_ez00_5534),
																				BgL_portz00_6306,
																				((obj_t) BgL_ez00_5534)));
																	}
																	BgL_arg3073z00_6312 =
																		MAKE_YOUNG_PAIR(BgL_arg3075z00_6313, BNIL);
																}
																BgL_arg3067z00_6308 =
																	MAKE_YOUNG_PAIR(BgL_arg3072z00_6311,
																	BgL_arg3073z00_6312);
															}
															return
																MAKE_YOUNG_PAIR(BgL_arg3066z00_6307,
																BgL_arg3067z00_6308);
														}
													}
												else
													{	/* Expand/initial.scm 870 */
													BgL_tagzd21645zd2_6294:
														if (NULLP(BgL_xz00_5533))
															{	/* Expand/initial.scm 879 */
																return BNIL;
															}
														else
															{	/* Expand/initial.scm 879 */
																obj_t BgL_head1358z00_6297;

																BgL_head1358z00_6297 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1356z00_6299;
																	obj_t BgL_tail1359z00_6300;

																	BgL_l1356z00_6299 = BgL_xz00_5533;
																	BgL_tail1359z00_6300 = BgL_head1358z00_6297;
																BgL_zc3z04anonymousza33077ze3z87_6298:
																	if (NULLP(BgL_l1356z00_6299))
																		{	/* Expand/initial.scm 879 */
																			return CDR(BgL_head1358z00_6297);
																		}
																	else
																		{	/* Expand/initial.scm 879 */
																			obj_t BgL_newtail1360z00_6301;

																			{	/* Expand/initial.scm 879 */
																				obj_t BgL_arg3080z00_6302;

																				{	/* Expand/initial.scm 879 */
																					obj_t BgL_xz00_6303;

																					BgL_xz00_6303 =
																						CAR(((obj_t) BgL_l1356z00_6299));
																					{	/* Expand/initial.scm 879 */
																						obj_t BgL_tmpfunz00_8451;

																						BgL_tmpfunz00_8451 =
																							((obj_t) BgL_ez00_5534);
																						BgL_arg3080z00_6302 =
																							(VA_PROCEDUREP(BgL_tmpfunz00_8451)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8451)) (((obj_t)
																									BgL_ez00_5534), BgL_xz00_6303,
																								((obj_t) BgL_ez00_5534),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8451)) (((obj_t)
																									BgL_ez00_5534), BgL_xz00_6303,
																								((obj_t) BgL_ez00_5534)));
																					}
																				}
																				BgL_newtail1360z00_6301 =
																					MAKE_YOUNG_PAIR(BgL_arg3080z00_6302,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1359z00_6300,
																				BgL_newtail1360z00_6301);
																			{	/* Expand/initial.scm 879 */
																				obj_t BgL_arg3079z00_6304;

																				BgL_arg3079z00_6304 =
																					CDR(((obj_t) BgL_l1356z00_6299));
																				{
																					obj_t BgL_tail1359z00_8458;
																					obj_t BgL_l1356z00_8457;

																					BgL_l1356z00_8457 =
																						BgL_arg3079z00_6304;
																					BgL_tail1359z00_8458 =
																						BgL_newtail1360z00_6301;
																					BgL_tail1359z00_6300 =
																						BgL_tail1359z00_8458;
																					BgL_l1356z00_6299 = BgL_l1356z00_8457;
																					goto
																						BgL_zc3z04anonymousza33077ze3z87_6298;
																				}
																			}
																		}
																}
															}
													}
											}
										else
											{	/* Expand/initial.scm 870 */
												goto BgL_tagzd21645zd2_6294;
											}
									}
							}
						else
							{	/* Expand/initial.scm 870 */
								goto BgL_tagzd21645zd2_6294;
							}
					}
				else
					{	/* Expand/initial.scm 870 */
						goto BgL_tagzd21645zd2_6294;
					}
			}
		}

	}



/* &<@anonymous:2987> */
	obj_t BGl_z62zc3z04anonymousza32987ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5535, obj_t BgL_xz00_5536, obj_t BgL_ez00_5537)
	{
		{	/* Expand/initial.scm 858 */
			{
				obj_t BgL_oz00_6332;
				obj_t BgL_iz00_6333;
				obj_t BgL_jz00_6334;
				obj_t BgL_pz00_6335;

				if (PAIRP(BgL_xz00_5536))
					{	/* Expand/initial.scm 858 */
						obj_t BgL_cdrzd21620zd2_6352;

						BgL_cdrzd21620zd2_6352 = CDR(((obj_t) BgL_xz00_5536));
						if (PAIRP(BgL_cdrzd21620zd2_6352))
							{	/* Expand/initial.scm 858 */
								obj_t BgL_cdrzd21626zd2_6353;

								BgL_cdrzd21626zd2_6353 = CDR(BgL_cdrzd21620zd2_6352);
								if (PAIRP(BgL_cdrzd21626zd2_6353))
									{	/* Expand/initial.scm 858 */
										obj_t BgL_cdrzd21632zd2_6354;

										BgL_cdrzd21632zd2_6354 = CDR(BgL_cdrzd21626zd2_6353);
										if (PAIRP(BgL_cdrzd21632zd2_6354))
											{	/* Expand/initial.scm 858 */
												obj_t BgL_cdrzd21637zd2_6355;

												BgL_cdrzd21637zd2_6355 = CDR(BgL_cdrzd21632zd2_6354);
												if (PAIRP(BgL_cdrzd21637zd2_6355))
													{	/* Expand/initial.scm 858 */
														if (NULLP(CDR(BgL_cdrzd21637zd2_6355)))
															{	/* Expand/initial.scm 858 */
																BgL_oz00_6332 = CAR(BgL_cdrzd21620zd2_6352);
																BgL_iz00_6333 = CAR(BgL_cdrzd21626zd2_6353);
																BgL_jz00_6334 = CAR(BgL_cdrzd21632zd2_6354);
																BgL_pz00_6335 = CAR(BgL_cdrzd21637zd2_6355);
																if (CBOOL
																	(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
																	{	/* Expand/initial.scm 862 */
																		obj_t BgL_arg3003z00_6336;

																		{	/* Expand/initial.scm 862 */
																			obj_t BgL_arg3008z00_6337;
																			obj_t BgL_arg3009z00_6338;

																			{	/* Expand/initial.scm 862 */
																				obj_t BgL_tmpfunz00_8485;

																				BgL_tmpfunz00_8485 =
																					((obj_t) BgL_ez00_5537);
																				BgL_arg3008z00_6337 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_8485)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_8485))
																					(((obj_t) BgL_ez00_5537),
																						BgL_oz00_6332,
																						((obj_t) BgL_ez00_5537),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_8485))
																					(((obj_t) BgL_ez00_5537),
																						BgL_oz00_6332,
																						((obj_t) BgL_ez00_5537)));
																			}
																			{	/* Expand/initial.scm 862 */
																				obj_t BgL_arg3010z00_6339;
																				obj_t BgL_arg3011z00_6340;

																				{	/* Expand/initial.scm 862 */
																					obj_t BgL_tmpfunz00_8493;

																					BgL_tmpfunz00_8493 =
																						((obj_t) BgL_ez00_5537);
																					BgL_arg3010z00_6339 =
																						(VA_PROCEDUREP(BgL_tmpfunz00_8493)
																						? ((obj_t(*)(obj_t,
																									...))
																							PROCEDURE_ENTRY
																							(BgL_tmpfunz00_8493)) (((obj_t)
																								BgL_ez00_5537), BgL_iz00_6333,
																							((obj_t) BgL_ez00_5537),
																							BEOA) : ((obj_t(*)(obj_t, obj_t,
																									obj_t))
																							PROCEDURE_ENTRY
																							(BgL_tmpfunz00_8493)) (((obj_t)
																								BgL_ez00_5537), BgL_iz00_6333,
																							((obj_t) BgL_ez00_5537)));
																				}
																				{	/* Expand/initial.scm 862 */
																					obj_t BgL_arg3012z00_6341;
																					obj_t BgL_arg3013z00_6342;

																					{	/* Expand/initial.scm 862 */
																						obj_t BgL_tmpfunz00_8501;

																						BgL_tmpfunz00_8501 =
																							((obj_t) BgL_ez00_5537);
																						BgL_arg3012z00_6341 =
																							(VA_PROCEDUREP(BgL_tmpfunz00_8501)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8501)) (((obj_t)
																									BgL_ez00_5537), BgL_jz00_6334,
																								((obj_t) BgL_ez00_5537),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8501)) (((obj_t)
																									BgL_ez00_5537), BgL_jz00_6334,
																								((obj_t) BgL_ez00_5537)));
																					}
																					{	/* Expand/initial.scm 862 */
																						obj_t BgL_arg3014z00_6343;

																						{	/* Expand/initial.scm 862 */
																							obj_t BgL_tmpfunz00_8509;

																							BgL_tmpfunz00_8509 =
																								((obj_t) BgL_ez00_5537);
																							BgL_arg3014z00_6343 =
																								(VA_PROCEDUREP
																								(BgL_tmpfunz00_8509)
																								? ((obj_t(*)(obj_t,
																											...))
																									PROCEDURE_ENTRY
																									(BgL_tmpfunz00_8509)) ((
																										(obj_t) BgL_ez00_5537),
																									BgL_pz00_6335,
																									((obj_t) BgL_ez00_5537),
																									BEOA) : ((obj_t(*)(obj_t,
																											obj_t,
																											obj_t))
																									PROCEDURE_ENTRY
																									(BgL_tmpfunz00_8509)) ((
																										(obj_t) BgL_ez00_5537),
																									BgL_pz00_6335,
																									((obj_t) BgL_ez00_5537)));
																						}
																						BgL_arg3013z00_6342 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg3014z00_6343, BNIL);
																					}
																					BgL_arg3011z00_6340 =
																						MAKE_YOUNG_PAIR(BgL_arg3012z00_6341,
																						BgL_arg3013z00_6342);
																				}
																				BgL_arg3009z00_6338 =
																					MAKE_YOUNG_PAIR(BgL_arg3010z00_6339,
																					BgL_arg3011z00_6340);
																			}
																			BgL_arg3003z00_6336 =
																				MAKE_YOUNG_PAIR(BgL_arg3008z00_6337,
																				BgL_arg3009z00_6338);
																		}
																		return
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(176),
																			BgL_arg3003z00_6336);
																	}
																else
																	{	/* Expand/initial.scm 861 */
																		if (NULLP(BgL_xz00_5536))
																			{	/* Expand/initial.scm 863 */
																				return BNIL;
																			}
																		else
																			{	/* Expand/initial.scm 863 */
																				obj_t BgL_head1353z00_6344;

																				BgL_head1353z00_6344 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				{
																					obj_t BgL_l1351z00_6346;
																					obj_t BgL_tail1354z00_6347;

																					BgL_l1351z00_6346 = BgL_xz00_5536;
																					BgL_tail1354z00_6347 =
																						BgL_head1353z00_6344;
																				BgL_zc3z04anonymousza33016ze3z87_6345:
																					if (NULLP(BgL_l1351z00_6346))
																						{	/* Expand/initial.scm 863 */
																							return CDR(BgL_head1353z00_6344);
																						}
																					else
																						{	/* Expand/initial.scm 863 */
																							obj_t BgL_newtail1355z00_6348;

																							{	/* Expand/initial.scm 863 */
																								obj_t BgL_arg3019z00_6349;

																								{	/* Expand/initial.scm 863 */
																									obj_t BgL_xz00_6350;

																									BgL_xz00_6350 =
																										CAR(
																										((obj_t)
																											BgL_l1351z00_6346));
																									{	/* Expand/initial.scm 863 */
																										obj_t BgL_tmpfunz00_8531;

																										BgL_tmpfunz00_8531 =
																											((obj_t) BgL_ez00_5537);
																										BgL_arg3019z00_6349 =
																											(VA_PROCEDUREP
																											(BgL_tmpfunz00_8531)
																											? ((obj_t(*)(obj_t,
																														...))
																												PROCEDURE_ENTRY
																												(BgL_tmpfunz00_8531)) ((
																													(obj_t)
																													BgL_ez00_5537),
																												BgL_xz00_6350,
																												((obj_t) BgL_ez00_5537),
																												BEOA)
																											: ((obj_t(*)(obj_t, obj_t,
																														obj_t))
																												PROCEDURE_ENTRY
																												(BgL_tmpfunz00_8531)) ((
																													(obj_t)
																													BgL_ez00_5537),
																												BgL_xz00_6350,
																												((obj_t)
																													BgL_ez00_5537)));
																									}
																								}
																								BgL_newtail1355z00_6348 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg3019z00_6349, BNIL);
																							}
																							SET_CDR(BgL_tail1354z00_6347,
																								BgL_newtail1355z00_6348);
																							{	/* Expand/initial.scm 863 */
																								obj_t BgL_arg3018z00_6351;

																								BgL_arg3018z00_6351 =
																									CDR(
																									((obj_t) BgL_l1351z00_6346));
																								{
																									obj_t BgL_tail1354z00_8538;
																									obj_t BgL_l1351z00_8537;

																									BgL_l1351z00_8537 =
																										BgL_arg3018z00_6351;
																									BgL_tail1354z00_8538 =
																										BgL_newtail1355z00_6348;
																									BgL_tail1354z00_6347 =
																										BgL_tail1354z00_8538;
																									BgL_l1351z00_6346 =
																										BgL_l1351z00_8537;
																									goto
																										BgL_zc3z04anonymousza33016ze3z87_6345;
																								}
																							}
																						}
																				}
																			}
																	}
															}
														else
															{	/* Expand/initial.scm 858 */
															BgL_tagzd21609zd2_6330:
																return
																	BGl_errorz00zz__errorz00(BFALSE,
																	BGl_string4220z00zzexpand_installz00,
																	BgL_xz00_5536);
															}
													}
												else
													{	/* Expand/initial.scm 858 */
														goto BgL_tagzd21609zd2_6330;
													}
											}
										else
											{	/* Expand/initial.scm 858 */
												goto BgL_tagzd21609zd2_6330;
											}
									}
								else
									{	/* Expand/initial.scm 858 */
										goto BgL_tagzd21609zd2_6330;
									}
							}
						else
							{	/* Expand/initial.scm 858 */
								goto BgL_tagzd21609zd2_6330;
							}
					}
				else
					{	/* Expand/initial.scm 858 */
						goto BgL_tagzd21609zd2_6330;
					}
			}
		}

	}



/* &<@anonymous:2927> */
	obj_t BGl_z62zc3z04anonymousza32927ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5538, obj_t BgL_xz00_5539, obj_t BgL_ez00_5540)
	{
		{	/* Expand/initial.scm 846 */
			{
				obj_t BgL_oz00_6375;
				obj_t BgL_oz00_6368;
				obj_t BgL_portz00_6369;

				if (PAIRP(BgL_xz00_5539))
					{	/* Expand/initial.scm 846 */
						obj_t BgL_cdrzd21577zd2_6384;

						BgL_cdrzd21577zd2_6384 = CDR(((obj_t) BgL_xz00_5539));
						if (PAIRP(BgL_cdrzd21577zd2_6384))
							{	/* Expand/initial.scm 846 */
								if (NULLP(CDR(BgL_cdrzd21577zd2_6384)))
									{	/* Expand/initial.scm 846 */
										BgL_oz00_6375 = CAR(BgL_cdrzd21577zd2_6384);
										{	/* Expand/initial.scm 849 */
											obj_t BgL_arg2943z00_6376;
											obj_t BgL_arg2954z00_6377;

											BgL_arg2943z00_6376 =
												BGl_dispz00zzexpand_installz00(BgL_oz00_6375);
											{	/* Expand/initial.scm 849 */
												obj_t BgL_arg2955z00_6378;
												obj_t BgL_arg2956z00_6379;

												{	/* Expand/initial.scm 849 */
													obj_t BgL_tmpfunz00_8560;

													BgL_tmpfunz00_8560 = ((obj_t) BgL_ez00_5540);
													BgL_arg2955z00_6378 =
														(VA_PROCEDUREP(BgL_tmpfunz00_8560)
														? ((obj_t(*)(obj_t,
																	...))
															PROCEDURE_ENTRY(BgL_tmpfunz00_8560)) (((obj_t)
																BgL_ez00_5540), BgL_oz00_6375,
															((obj_t) BgL_ez00_5540), BEOA) : ((obj_t(*)(obj_t,
																	obj_t,
																	obj_t))
															PROCEDURE_ENTRY(BgL_tmpfunz00_8560)) (((obj_t)
																BgL_ez00_5540), BgL_oz00_6375,
															((obj_t) BgL_ez00_5540)));
												}
												{	/* Expand/initial.scm 849 */
													obj_t BgL_arg2966z00_6380;

													{	/* Expand/initial.scm 849 */
														obj_t BgL_arg2967z00_6381;

														{	/* Expand/initial.scm 849 */
															obj_t BgL_arg2968z00_6382;

															{	/* Expand/initial.scm 849 */
																obj_t BgL_arg2972z00_6383;

																BgL_arg2972z00_6383 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(172), BNIL);
																BgL_arg2968z00_6382 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(173),
																	BgL_arg2972z00_6383);
															}
															BgL_arg2967z00_6381 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
																BgL_arg2968z00_6382);
														}
														BgL_arg2966z00_6380 =
															MAKE_YOUNG_PAIR(BgL_arg2967z00_6381, BNIL);
													}
													BgL_arg2956z00_6379 =
														MAKE_YOUNG_PAIR(BgL_arg2966z00_6380, BNIL);
												}
												BgL_arg2954z00_6377 =
													MAKE_YOUNG_PAIR(BgL_arg2955z00_6378,
													BgL_arg2956z00_6379);
											}
											return
												MAKE_YOUNG_PAIR(BgL_arg2943z00_6376,
												BgL_arg2954z00_6377);
										}
									}
								else
									{	/* Expand/initial.scm 846 */
										obj_t BgL_cdrzd21593zd2_6385;

										BgL_cdrzd21593zd2_6385 =
											CDR(((obj_t) BgL_cdrzd21577zd2_6384));
										if (PAIRP(BgL_cdrzd21593zd2_6385))
											{	/* Expand/initial.scm 846 */
												if (NULLP(CDR(BgL_cdrzd21593zd2_6385)))
													{	/* Expand/initial.scm 846 */
														obj_t BgL_arg2936z00_6386;
														obj_t BgL_arg2940z00_6387;

														BgL_arg2936z00_6386 =
															CAR(((obj_t) BgL_cdrzd21577zd2_6384));
														BgL_arg2940z00_6387 = CAR(BgL_cdrzd21593zd2_6385);
														BgL_oz00_6368 = BgL_arg2936z00_6386;
														BgL_portz00_6369 = BgL_arg2940z00_6387;
														{	/* Expand/initial.scm 851 */
															obj_t BgL_arg2973z00_6370;
															obj_t BgL_arg2974z00_6371;

															BgL_arg2973z00_6370 =
																BGl_dispz00zzexpand_installz00(BgL_oz00_6368);
															{	/* Expand/initial.scm 851 */
																obj_t BgL_arg2975z00_6372;
																obj_t BgL_arg2976z00_6373;

																{	/* Expand/initial.scm 851 */
																	obj_t BgL_tmpfunz00_8590;

																	BgL_tmpfunz00_8590 = ((obj_t) BgL_ez00_5540);
																	BgL_arg2975z00_6372 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_8590)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8590)) ((
																				(obj_t) BgL_ez00_5540), BgL_oz00_6368,
																			((obj_t) BgL_ez00_5540),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8590)) ((
																				(obj_t) BgL_ez00_5540), BgL_oz00_6368,
																			((obj_t) BgL_ez00_5540)));
																}
																{	/* Expand/initial.scm 851 */
																	obj_t BgL_arg2977z00_6374;

																	{	/* Expand/initial.scm 851 */
																		obj_t BgL_tmpfunz00_8598;

																		BgL_tmpfunz00_8598 =
																			((obj_t) BgL_ez00_5540);
																		BgL_arg2977z00_6374 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_8598)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8598)) ((
																					(obj_t) BgL_ez00_5540),
																				BgL_portz00_6369,
																				((obj_t) BgL_ez00_5540),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8598)) ((
																					(obj_t) BgL_ez00_5540),
																				BgL_portz00_6369,
																				((obj_t) BgL_ez00_5540)));
																	}
																	BgL_arg2976z00_6373 =
																		MAKE_YOUNG_PAIR(BgL_arg2977z00_6374, BNIL);
																}
																BgL_arg2974z00_6371 =
																	MAKE_YOUNG_PAIR(BgL_arg2975z00_6372,
																	BgL_arg2976z00_6373);
															}
															return
																MAKE_YOUNG_PAIR(BgL_arg2973z00_6370,
																BgL_arg2974z00_6371);
														}
													}
												else
													{	/* Expand/initial.scm 846 */
													BgL_tagzd21571zd2_6357:
														if (NULLP(BgL_xz00_5539))
															{	/* Expand/initial.scm 853 */
																return BNIL;
															}
														else
															{	/* Expand/initial.scm 853 */
																obj_t BgL_head1348z00_6360;

																BgL_head1348z00_6360 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1346z00_6362;
																	obj_t BgL_tail1349z00_6363;

																	BgL_l1346z00_6362 = BgL_xz00_5539;
																	BgL_tail1349z00_6363 = BgL_head1348z00_6360;
																BgL_zc3z04anonymousza32979ze3z87_6361:
																	if (NULLP(BgL_l1346z00_6362))
																		{	/* Expand/initial.scm 853 */
																			return CDR(BgL_head1348z00_6360);
																		}
																	else
																		{	/* Expand/initial.scm 853 */
																			obj_t BgL_newtail1350z00_6364;

																			{	/* Expand/initial.scm 853 */
																				obj_t BgL_arg2985z00_6365;

																				{	/* Expand/initial.scm 853 */
																					obj_t BgL_xz00_6366;

																					BgL_xz00_6366 =
																						CAR(((obj_t) BgL_l1346z00_6362));
																					{	/* Expand/initial.scm 853 */
																						obj_t BgL_tmpfunz00_8617;

																						BgL_tmpfunz00_8617 =
																							((obj_t) BgL_ez00_5540);
																						BgL_arg2985z00_6365 =
																							(VA_PROCEDUREP(BgL_tmpfunz00_8617)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8617)) (((obj_t)
																									BgL_ez00_5540), BgL_xz00_6366,
																								((obj_t) BgL_ez00_5540),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_8617)) (((obj_t)
																									BgL_ez00_5540), BgL_xz00_6366,
																								((obj_t) BgL_ez00_5540)));
																					}
																				}
																				BgL_newtail1350z00_6364 =
																					MAKE_YOUNG_PAIR(BgL_arg2985z00_6365,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1349z00_6363,
																				BgL_newtail1350z00_6364);
																			{	/* Expand/initial.scm 853 */
																				obj_t BgL_arg2981z00_6367;

																				BgL_arg2981z00_6367 =
																					CDR(((obj_t) BgL_l1346z00_6362));
																				{
																					obj_t BgL_tail1349z00_8624;
																					obj_t BgL_l1346z00_8623;

																					BgL_l1346z00_8623 =
																						BgL_arg2981z00_6367;
																					BgL_tail1349z00_8624 =
																						BgL_newtail1350z00_6364;
																					BgL_tail1349z00_6363 =
																						BgL_tail1349z00_8624;
																					BgL_l1346z00_6362 = BgL_l1346z00_8623;
																					goto
																						BgL_zc3z04anonymousza32979ze3z87_6361;
																				}
																			}
																		}
																}
															}
													}
											}
										else
											{	/* Expand/initial.scm 846 */
												goto BgL_tagzd21571zd2_6357;
											}
									}
							}
						else
							{	/* Expand/initial.scm 846 */
								goto BgL_tagzd21571zd2_6357;
							}
					}
				else
					{	/* Expand/initial.scm 846 */
						goto BgL_tagzd21571zd2_6357;
					}
			}
		}

	}



/* &<@anonymous:2892> */
	obj_t BGl_z62zc3z04anonymousza32892ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5541, obj_t BgL_xz00_5542, obj_t BgL_ez00_5543)
	{
		{	/* Expand/initial.scm 832 */
			{
				obj_t BgL_portz00_6400;

				if (PAIRP(BgL_xz00_5542))
					{	/* Expand/initial.scm 832 */
						if (NULLP(CDR(((obj_t) BgL_xz00_5542))))
							{	/* Expand/initial.scm 832 */
								{	/* Expand/initial.scm 835 */
									obj_t BgL_arg2905z00_6406;
									obj_t BgL_arg2906z00_6407;

									{	/* Expand/initial.scm 835 */
										obj_t BgL_arg2907z00_6408;

										{	/* Expand/initial.scm 835 */
											obj_t BgL_arg2908z00_6409;

											BgL_arg2908z00_6409 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(169), BNIL);
											BgL_arg2907z00_6408 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(170),
												BgL_arg2908z00_6409);
										}
										BgL_arg2905z00_6406 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(171), BgL_arg2907z00_6408);
									}
									{	/* Expand/initial.scm 836 */
										obj_t BgL_arg2912z00_6410;

										{	/* Expand/initial.scm 836 */
											obj_t BgL_arg2913z00_6411;

											{	/* Expand/initial.scm 836 */
												obj_t BgL_arg2914z00_6412;

												{	/* Expand/initial.scm 836 */
													obj_t BgL_arg2915z00_6413;

													BgL_arg2915z00_6413 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(172), BNIL);
													BgL_arg2914z00_6412 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(173),
														BgL_arg2915z00_6413);
												}
												BgL_arg2913z00_6411 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
													BgL_arg2914z00_6412);
											}
											BgL_arg2912z00_6410 =
												MAKE_YOUNG_PAIR(BgL_arg2913z00_6411, BNIL);
										}
										BgL_arg2906z00_6407 =
											MAKE_YOUNG_PAIR(BgL_arg2912z00_6410, BNIL);
									}
									return
										MAKE_YOUNG_PAIR(BgL_arg2905z00_6406, BgL_arg2906z00_6407);
								}
							}
						else
							{	/* Expand/initial.scm 832 */
								obj_t BgL_cdrzd21562zd2_6414;

								BgL_cdrzd21562zd2_6414 = CDR(((obj_t) BgL_xz00_5542));
								if (PAIRP(BgL_cdrzd21562zd2_6414))
									{	/* Expand/initial.scm 832 */
										if (NULLP(CDR(BgL_cdrzd21562zd2_6414)))
											{	/* Expand/initial.scm 832 */
												BgL_portz00_6400 = CAR(BgL_cdrzd21562zd2_6414);
												{	/* Expand/initial.scm 838 */
													obj_t BgL_arg2916z00_6401;
													obj_t BgL_arg2917z00_6402;

													{	/* Expand/initial.scm 838 */
														obj_t BgL_arg2918z00_6403;

														{	/* Expand/initial.scm 838 */
															obj_t BgL_arg2919z00_6404;

															BgL_arg2919z00_6404 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(169), BNIL);
															BgL_arg2918z00_6403 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(170),
																BgL_arg2919z00_6404);
														}
														BgL_arg2916z00_6401 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
															BgL_arg2918z00_6403);
													}
													{	/* Expand/initial.scm 839 */
														obj_t BgL_arg2920z00_6405;

														{	/* Expand/initial.scm 839 */
															obj_t BgL_tmpfunz00_8665;

															BgL_tmpfunz00_8665 = ((obj_t) BgL_ez00_5543);
															BgL_arg2920z00_6405 =
																(VA_PROCEDUREP(BgL_tmpfunz00_8665)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8665)) (((obj_t)
																		BgL_ez00_5543), BgL_portz00_6400,
																	((obj_t) BgL_ez00_5543),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8665)) (((obj_t)
																		BgL_ez00_5543), BgL_portz00_6400,
																	((obj_t) BgL_ez00_5543)));
														}
														BgL_arg2917z00_6402 =
															MAKE_YOUNG_PAIR(BgL_arg2920z00_6405, BNIL);
													}
													return
														MAKE_YOUNG_PAIR(BgL_arg2916z00_6401,
														BgL_arg2917z00_6402);
												}
											}
										else
											{	/* Expand/initial.scm 832 */
											BgL_tagzd21554zd2_6389:
												if (NULLP(BgL_xz00_5542))
													{	/* Expand/initial.scm 841 */
														return BNIL;
													}
												else
													{	/* Expand/initial.scm 841 */
														obj_t BgL_head1343z00_6392;

														BgL_head1343z00_6392 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1341z00_6394;
															obj_t BgL_tail1344z00_6395;

															BgL_l1341z00_6394 = BgL_xz00_5542;
															BgL_tail1344z00_6395 = BgL_head1343z00_6392;
														BgL_zc3z04anonymousza32922ze3z87_6393:
															if (NULLP(BgL_l1341z00_6394))
																{	/* Expand/initial.scm 841 */
																	return CDR(BgL_head1343z00_6392);
																}
															else
																{	/* Expand/initial.scm 841 */
																	obj_t BgL_newtail1345z00_6396;

																	{	/* Expand/initial.scm 841 */
																		obj_t BgL_arg2925z00_6397;

																		{	/* Expand/initial.scm 841 */
																			obj_t BgL_xz00_6398;

																			BgL_xz00_6398 =
																				CAR(((obj_t) BgL_l1341z00_6394));
																			{	/* Expand/initial.scm 841 */
																				obj_t BgL_tmpfunz00_8684;

																				BgL_tmpfunz00_8684 =
																					((obj_t) BgL_ez00_5543);
																				BgL_arg2925z00_6397 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_8684)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_8684))
																					(((obj_t) BgL_ez00_5543),
																						BgL_xz00_6398,
																						((obj_t) BgL_ez00_5543),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_8684))
																					(((obj_t) BgL_ez00_5543),
																						BgL_xz00_6398,
																						((obj_t) BgL_ez00_5543)));
																			}
																		}
																		BgL_newtail1345z00_6396 =
																			MAKE_YOUNG_PAIR(BgL_arg2925z00_6397,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1344z00_6395,
																		BgL_newtail1345z00_6396);
																	{	/* Expand/initial.scm 841 */
																		obj_t BgL_arg2924z00_6399;

																		BgL_arg2924z00_6399 =
																			CDR(((obj_t) BgL_l1341z00_6394));
																		{
																			obj_t BgL_tail1344z00_8691;
																			obj_t BgL_l1341z00_8690;

																			BgL_l1341z00_8690 = BgL_arg2924z00_6399;
																			BgL_tail1344z00_8691 =
																				BgL_newtail1345z00_6396;
																			BgL_tail1344z00_6395 =
																				BgL_tail1344z00_8691;
																			BgL_l1341z00_6394 = BgL_l1341z00_8690;
																			goto
																				BgL_zc3z04anonymousza32922ze3z87_6393;
																		}
																	}
																}
														}
													}
											}
									}
								else
									{	/* Expand/initial.scm 832 */
										goto BgL_tagzd21554zd2_6389;
									}
							}
					}
				else
					{	/* Expand/initial.scm 832 */
						goto BgL_tagzd21554zd2_6389;
					}
			}
		}

	}



/* &<@anonymous:2847> */
	obj_t BGl_z62zc3z04anonymousza32847ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5544, obj_t BgL_xz00_5545, obj_t BgL_ez00_5546)
	{
		{	/* Expand/initial.scm 811 */
			{
				obj_t BgL_functionz00_6431;
				obj_t BgL_onezd2argzd2_6432;
				obj_t BgL_functionz00_6419;
				obj_t BgL_argsz00_6420;

				if (PAIRP(BgL_xz00_5545))
					{	/* Expand/initial.scm 811 */
						obj_t BgL_cdrzd21504zd2_6437;

						BgL_cdrzd21504zd2_6437 = CDR(((obj_t) BgL_xz00_5545));
						if (PAIRP(BgL_cdrzd21504zd2_6437))
							{	/* Expand/initial.scm 811 */
								obj_t BgL_cdrzd21508zd2_6438;

								BgL_cdrzd21508zd2_6438 = CDR(BgL_cdrzd21504zd2_6437);
								if (PAIRP(BgL_cdrzd21508zd2_6438))
									{	/* Expand/initial.scm 811 */
										if (NULLP(CDR(BgL_cdrzd21508zd2_6438)))
											{	/* Expand/initial.scm 811 */
												BgL_functionz00_6431 = CAR(BgL_cdrzd21504zd2_6437);
												BgL_onezd2argzd2_6432 = CAR(BgL_cdrzd21508zd2_6438);
												{	/* Expand/initial.scm 814 */
													obj_t BgL_arg2874z00_6433;

													{	/* Expand/initial.scm 814 */
														obj_t BgL_arg2875z00_6434;
														obj_t BgL_arg2876z00_6435;

														{	/* Expand/initial.scm 814 */
															obj_t BgL_tmpfunz00_8710;

															BgL_tmpfunz00_8710 = ((obj_t) BgL_ez00_5546);
															BgL_arg2875z00_6434 =
																(VA_PROCEDUREP(BgL_tmpfunz00_8710)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8710)) (((obj_t)
																		BgL_ez00_5546), BgL_functionz00_6431,
																	((obj_t) BgL_ez00_5546),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8710)) (((obj_t)
																		BgL_ez00_5546), BgL_functionz00_6431,
																	((obj_t) BgL_ez00_5546)));
														}
														{	/* Expand/initial.scm 815 */
															obj_t BgL_arg2877z00_6436;

															{	/* Expand/initial.scm 815 */
																obj_t BgL_tmpfunz00_8718;

																BgL_tmpfunz00_8718 = ((obj_t) BgL_ez00_5546);
																BgL_arg2877z00_6436 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_8718)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_8718)) ((
																			(obj_t) BgL_ez00_5546),
																		BgL_onezd2argzd2_6432,
																		((obj_t) BgL_ez00_5546),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_8718)) ((
																			(obj_t) BgL_ez00_5546),
																		BgL_onezd2argzd2_6432,
																		((obj_t) BgL_ez00_5546)));
															}
															BgL_arg2876z00_6435 =
																MAKE_YOUNG_PAIR(BgL_arg2877z00_6436, BNIL);
														}
														BgL_arg2874z00_6433 =
															MAKE_YOUNG_PAIR(BgL_arg2875z00_6434,
															BgL_arg2876z00_6435);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(123),
														BgL_arg2874z00_6433);
												}
											}
										else
											{	/* Expand/initial.scm 811 */
												obj_t BgL_arg2864z00_6439;
												obj_t BgL_arg2870z00_6440;

												BgL_arg2864z00_6439 =
													CAR(((obj_t) BgL_cdrzd21504zd2_6437));
												BgL_arg2870z00_6440 =
													CDR(((obj_t) BgL_cdrzd21504zd2_6437));
												BgL_functionz00_6419 = BgL_arg2864z00_6439;
												BgL_argsz00_6420 = BgL_arg2870z00_6440;
											BgL_tagzd21495zd2_6417:
												{	/* Expand/initial.scm 817 */
													obj_t BgL_arg2878z00_6421;

													{	/* Expand/initial.scm 817 */
														obj_t BgL_arg2879z00_6422;
														obj_t BgL_arg2880z00_6423;

														{	/* Expand/initial.scm 817 */
															obj_t BgL_tmpfunz00_8736;

															BgL_tmpfunz00_8736 = ((obj_t) BgL_ez00_5546);
															BgL_arg2879z00_6422 =
																(VA_PROCEDUREP(BgL_tmpfunz00_8736)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8736)) (((obj_t)
																		BgL_ez00_5546), BgL_functionz00_6419,
																	((obj_t) BgL_ez00_5546),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8736)) (((obj_t)
																		BgL_ez00_5546), BgL_functionz00_6419,
																	((obj_t) BgL_ez00_5546)));
														}
														{	/* Expand/initial.scm 819 */
															obj_t BgL_arg2881z00_6424;

															{	/* Expand/initial.scm 819 */
																obj_t BgL_arg2882z00_6425;

																{	/* Expand/initial.scm 819 */
																	obj_t BgL_arg2883z00_6426;

																	{	/* Expand/initial.scm 819 */
																		obj_t BgL_arg2887z00_6427;
																		obj_t BgL_arg2888z00_6428;

																		{	/* Expand/initial.scm 819 */
																			obj_t BgL_arg2889z00_6429;

																			{	/* Expand/initial.scm 819 */
																				obj_t BgL_arg2890z00_6430;

																				BgL_arg2890z00_6430 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(177),
																					BNIL);
																				BgL_arg2889z00_6429 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(122),
																					BgL_arg2890z00_6430);
																			}
																			BgL_arg2887z00_6427 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
																				BgL_arg2889z00_6429);
																		}
																		BgL_arg2888z00_6428 =
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_argsz00_6420, BNIL);
																		BgL_arg2883z00_6426 =
																			MAKE_YOUNG_PAIR(BgL_arg2887z00_6427,
																			BgL_arg2888z00_6428);
																	}
																	BgL_arg2882z00_6425 =
																		BGl_epairifyz00zztools_miscz00
																		(BgL_arg2883z00_6426, BgL_xz00_5545);
																}
																{	/* Expand/initial.scm 818 */
																	obj_t BgL_tmpfunz00_8753;

																	BgL_tmpfunz00_8753 = ((obj_t) BgL_ez00_5546);
																	BgL_arg2881z00_6424 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_8753)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8753)) ((
																				(obj_t) BgL_ez00_5546),
																			BgL_arg2882z00_6425,
																			((obj_t) BgL_ez00_5546),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8753)) ((
																				(obj_t) BgL_ez00_5546),
																			BgL_arg2882z00_6425,
																			((obj_t) BgL_ez00_5546)));
																}
															}
															BgL_arg2880z00_6423 =
																MAKE_YOUNG_PAIR(BgL_arg2881z00_6424, BNIL);
														}
														BgL_arg2878z00_6421 =
															MAKE_YOUNG_PAIR(BgL_arg2879z00_6422,
															BgL_arg2880z00_6423);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(123),
														BgL_arg2878z00_6421);
												}
											}
									}
								else
									{	/* Expand/initial.scm 811 */
										obj_t BgL_arg2872z00_6441;
										obj_t BgL_arg2873z00_6442;

										BgL_arg2872z00_6441 = CAR(((obj_t) BgL_cdrzd21504zd2_6437));
										BgL_arg2873z00_6442 = CDR(((obj_t) BgL_cdrzd21504zd2_6437));
										{
											obj_t BgL_argsz00_8764;
											obj_t BgL_functionz00_8763;

											BgL_functionz00_8763 = BgL_arg2872z00_6441;
											BgL_argsz00_8764 = BgL_arg2873z00_6442;
											BgL_argsz00_6420 = BgL_argsz00_8764;
											BgL_functionz00_6419 = BgL_functionz00_8763;
											goto BgL_tagzd21495zd2_6417;
										}
									}
							}
						else
							{	/* Expand/initial.scm 811 */
							BgL_tagzd21496zd2_6416:
								return
									BGl_errorz00zz__errorz00(BFALSE,
									BGl_string4221z00zzexpand_installz00, BgL_xz00_5545);
							}
					}
				else
					{	/* Expand/initial.scm 811 */
						goto BgL_tagzd21496zd2_6416;
					}
			}
		}

	}



/* &<@anonymous:2795> */
	obj_t BGl_z62zc3z04anonymousza32795ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5547, obj_t BgL_xz00_5548, obj_t BgL_ez00_5549)
	{
		{	/* Expand/initial.scm 793 */
			{
				obj_t BgL_x1z00_6465;
				obj_t BgL_x2z00_6466;
				obj_t BgL_x1z00_6455;
				obj_t BgL_x2z00_6456;
				obj_t BgL_restz00_6457;

				if (PAIRP(BgL_xz00_5548))
					{	/* Expand/initial.scm 793 */
						obj_t BgL_cdrzd21414zd2_6471;

						BgL_cdrzd21414zd2_6471 = CDR(((obj_t) BgL_xz00_5548));
						if (PAIRP(BgL_cdrzd21414zd2_6471))
							{	/* Expand/initial.scm 793 */
								if (NULLP(CDR(BgL_cdrzd21414zd2_6471)))
									{	/* Expand/initial.scm 793 */
										obj_t BgL_arg2804z00_6472;

										BgL_arg2804z00_6472 = CAR(BgL_cdrzd21414zd2_6471);
										{	/* Expand/initial.scm 796 */
											obj_t BgL_tmpfunz00_8782;

											BgL_tmpfunz00_8782 = ((obj_t) BgL_ez00_5549);
											return
												(VA_PROCEDUREP(BgL_tmpfunz00_8782) ? ((obj_t(*)(obj_t,
															...))
													PROCEDURE_ENTRY(BgL_tmpfunz00_8782)) (((obj_t)
														BgL_ez00_5549), BgL_arg2804z00_6472,
													((obj_t) BgL_ez00_5549), BEOA) : ((obj_t(*)(obj_t,
															obj_t,
															obj_t))
													PROCEDURE_ENTRY(BgL_tmpfunz00_8782)) (((obj_t)
														BgL_ez00_5549), BgL_arg2804z00_6472,
													((obj_t) BgL_ez00_5549)));
										}
									}
								else
									{	/* Expand/initial.scm 793 */
										obj_t BgL_cdrzd21431zd2_6473;

										BgL_cdrzd21431zd2_6473 =
											CDR(((obj_t) BgL_cdrzd21414zd2_6471));
										if (PAIRP(BgL_cdrzd21431zd2_6473))
											{	/* Expand/initial.scm 793 */
												if (NULLP(CDR(BgL_cdrzd21431zd2_6473)))
													{	/* Expand/initial.scm 793 */
														obj_t BgL_arg2811z00_6474;
														obj_t BgL_arg2812z00_6475;

														BgL_arg2811z00_6474 =
															CAR(((obj_t) BgL_cdrzd21414zd2_6471));
														BgL_arg2812z00_6475 = CAR(BgL_cdrzd21431zd2_6473);
														BgL_x1z00_6465 = BgL_arg2811z00_6474;
														BgL_x2z00_6466 = BgL_arg2812z00_6475;
														{	/* Expand/initial.scm 798 */
															obj_t BgL_arg2823z00_6467;

															{	/* Expand/initial.scm 798 */
																obj_t BgL_arg2824z00_6468;
																obj_t BgL_arg2826z00_6469;

																{	/* Expand/initial.scm 798 */
																	obj_t BgL_tmpfunz00_8800;

																	BgL_tmpfunz00_8800 = ((obj_t) BgL_ez00_5549);
																	BgL_arg2824z00_6468 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_8800)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8800)) ((
																				(obj_t) BgL_ez00_5549), BgL_x1z00_6465,
																			((obj_t) BgL_ez00_5549),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_8800)) ((
																				(obj_t) BgL_ez00_5549), BgL_x1z00_6465,
																			((obj_t) BgL_ez00_5549)));
																}
																{	/* Expand/initial.scm 798 */
																	obj_t BgL_arg2827z00_6470;

																	{	/* Expand/initial.scm 798 */
																		obj_t BgL_tmpfunz00_8808;

																		BgL_tmpfunz00_8808 =
																			((obj_t) BgL_ez00_5549);
																		BgL_arg2827z00_6470 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_8808)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8808)) ((
																					(obj_t) BgL_ez00_5549),
																				BgL_x2z00_6466, ((obj_t) BgL_ez00_5549),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8808)) ((
																					(obj_t) BgL_ez00_5549),
																				BgL_x2z00_6466,
																				((obj_t) BgL_ez00_5549)));
																	}
																	BgL_arg2826z00_6469 =
																		MAKE_YOUNG_PAIR(BgL_arg2827z00_6470, BNIL);
																}
																BgL_arg2823z00_6467 =
																	MAKE_YOUNG_PAIR(BgL_arg2824z00_6468,
																	BgL_arg2826z00_6469);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(178),
																BgL_arg2823z00_6467);
														}
													}
												else
													{	/* Expand/initial.scm 793 */
														obj_t BgL_cdrzd21454zd2_6477;

														BgL_cdrzd21454zd2_6477 =
															CDR(((obj_t) BgL_cdrzd21414zd2_6471));
														{	/* Expand/initial.scm 793 */
															obj_t BgL_arg2815z00_6478;
															obj_t BgL_arg2816z00_6479;
															obj_t BgL_arg2818z00_6480;

															BgL_arg2815z00_6478 =
																CAR(((obj_t) BgL_cdrzd21414zd2_6471));
															BgL_arg2816z00_6479 =
																CAR(((obj_t) BgL_cdrzd21454zd2_6477));
															BgL_arg2818z00_6480 =
																CDR(((obj_t) BgL_cdrzd21454zd2_6477));
															BgL_x1z00_6455 = BgL_arg2815z00_6478;
															BgL_x2z00_6456 = BgL_arg2816z00_6479;
															BgL_restz00_6457 = BgL_arg2818z00_6480;
															{	/* Expand/initial.scm 800 */
																obj_t BgL_arg2828z00_6458;

																{	/* Expand/initial.scm 800 */
																	obj_t BgL_arg2829z00_6459;
																	obj_t BgL_arg2830z00_6460;

																	{	/* Expand/initial.scm 800 */
																		obj_t BgL_tmpfunz00_8828;

																		BgL_tmpfunz00_8828 =
																			((obj_t) BgL_ez00_5549);
																		BgL_arg2829z00_6459 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_8828)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8828)) ((
																					(obj_t) BgL_ez00_5549),
																				BgL_x1z00_6455, ((obj_t) BgL_ez00_5549),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_8828)) ((
																					(obj_t) BgL_ez00_5549),
																				BgL_x1z00_6455,
																				((obj_t) BgL_ez00_5549)));
																	}
																	{	/* Expand/initial.scm 802 */
																		obj_t BgL_arg2831z00_6461;

																		{	/* Expand/initial.scm 802 */
																			obj_t BgL_arg2832z00_6462;

																			{	/* Expand/initial.scm 802 */
																				obj_t BgL_arg2833z00_6463;

																				{	/* Expand/initial.scm 802 */
																					obj_t BgL_arg2834z00_6464;

																					BgL_arg2834z00_6464 =
																						MAKE_YOUNG_PAIR(BgL_x2z00_6456,
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_restz00_6457, BNIL));
																					BgL_arg2833z00_6463 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(122),
																						BgL_arg2834z00_6464);
																				}
																				BgL_arg2832z00_6462 =
																					BGl_epairifyz00zztools_miscz00
																					(BgL_arg2833z00_6463, BgL_xz00_5548);
																			}
																			{	/* Expand/initial.scm 801 */
																				obj_t BgL_tmpfunz00_8841;

																				BgL_tmpfunz00_8841 =
																					((obj_t) BgL_ez00_5549);
																				BgL_arg2831z00_6461 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_8841)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_8841))
																					(((obj_t) BgL_ez00_5549),
																						BgL_arg2832z00_6462,
																						((obj_t) BgL_ez00_5549),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_8841))
																					(((obj_t) BgL_ez00_5549),
																						BgL_arg2832z00_6462,
																						((obj_t) BgL_ez00_5549)));
																			}
																		}
																		BgL_arg2830z00_6460 =
																			MAKE_YOUNG_PAIR(BgL_arg2831z00_6461,
																			BNIL);
																	}
																	BgL_arg2828z00_6458 =
																		MAKE_YOUNG_PAIR(BgL_arg2829z00_6459,
																		BgL_arg2830z00_6460);
																}
																return
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(178),
																	BgL_arg2828z00_6458);
															}
														}
													}
											}
										else
											{	/* Expand/initial.scm 793 */
											BgL_tagzd21407zd2_6444:
												if (NULLP(BgL_xz00_5548))
													{	/* Expand/initial.scm 806 */
														return BNIL;
													}
												else
													{	/* Expand/initial.scm 806 */
														obj_t BgL_head1338z00_6447;

														BgL_head1338z00_6447 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1336z00_6449;
															obj_t BgL_tail1339z00_6450;

															BgL_l1336z00_6449 = BgL_xz00_5548;
															BgL_tail1339z00_6450 = BgL_head1338z00_6447;
														BgL_zc3z04anonymousza32837ze3z87_6448:
															if (NULLP(BgL_l1336z00_6449))
																{	/* Expand/initial.scm 806 */
																	return CDR(BgL_head1338z00_6447);
																}
															else
																{	/* Expand/initial.scm 806 */
																	obj_t BgL_newtail1340z00_6451;

																	{	/* Expand/initial.scm 806 */
																		obj_t BgL_arg2844z00_6452;

																		{	/* Expand/initial.scm 806 */
																			obj_t BgL_xz00_6453;

																			BgL_xz00_6453 =
																				CAR(((obj_t) BgL_l1336z00_6449));
																			{	/* Expand/initial.scm 806 */
																				obj_t BgL_tmpfunz00_8861;

																				BgL_tmpfunz00_8861 =
																					((obj_t) BgL_ez00_5549);
																				BgL_arg2844z00_6452 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_8861)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_8861))
																					(((obj_t) BgL_ez00_5549),
																						BgL_xz00_6453,
																						((obj_t) BgL_ez00_5549),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_8861))
																					(((obj_t) BgL_ez00_5549),
																						BgL_xz00_6453,
																						((obj_t) BgL_ez00_5549)));
																			}
																		}
																		BgL_newtail1340z00_6451 =
																			MAKE_YOUNG_PAIR(BgL_arg2844z00_6452,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1339z00_6450,
																		BgL_newtail1340z00_6451);
																	{	/* Expand/initial.scm 806 */
																		obj_t BgL_arg2839z00_6454;

																		BgL_arg2839z00_6454 =
																			CDR(((obj_t) BgL_l1336z00_6449));
																		{
																			obj_t BgL_tail1339z00_8868;
																			obj_t BgL_l1336z00_8867;

																			BgL_l1336z00_8867 = BgL_arg2839z00_6454;
																			BgL_tail1339z00_8868 =
																				BgL_newtail1340z00_6451;
																			BgL_tail1339z00_6450 =
																				BgL_tail1339z00_8868;
																			BgL_l1336z00_6449 = BgL_l1336z00_8867;
																			goto
																				BgL_zc3z04anonymousza32837ze3z87_6448;
																		}
																	}
																}
														}
													}
											}
									}
							}
						else
							{	/* Expand/initial.scm 793 */
								goto BgL_tagzd21407zd2_6444;
							}
					}
				else
					{	/* Expand/initial.scm 793 */
						goto BgL_tagzd21407zd2_6444;
					}
			}
		}

	}



/* &<@anonymous:2777> */
	obj_t BGl_z62zc3z04anonymousza32777ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5550, obj_t BgL_xz00_5551, obj_t BgL_ez00_5552)
	{
		{	/* Expand/initial.scm 781 */
			{
				obj_t BgL_nz00_6484;

				if (PAIRP(BgL_xz00_5551))
					{	/* Expand/initial.scm 781 */
						obj_t BgL_cdrzd21382zd2_6487;

						BgL_cdrzd21382zd2_6487 = CDR(((obj_t) BgL_xz00_5551));
						if (PAIRP(BgL_cdrzd21382zd2_6487))
							{	/* Expand/initial.scm 781 */
								obj_t BgL_carzd21384zd2_6488;

								BgL_carzd21384zd2_6488 = CAR(BgL_cdrzd21382zd2_6487);
								if (REALP(BgL_carzd21384zd2_6488))
									{	/* Expand/initial.scm 781 */
										if (NULLP(CDR(BgL_cdrzd21382zd2_6487)))
											{	/* Expand/initial.scm 781 */
												return
													BINT((long) (REAL_TO_DOUBLE(BgL_carzd21384zd2_6488)));
											}
										else
											{	/* Expand/initial.scm 781 */
											BgL_tagzd21377zd2_6482:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4222z00zzexpand_installz00, BgL_xz00_5551);
											}
									}
								else
									{	/* Expand/initial.scm 781 */
										if (NULLP(CDR(((obj_t) BgL_cdrzd21382zd2_6487))))
											{	/* Expand/initial.scm 781 */
												obj_t BgL_arg2786z00_6489;

												BgL_arg2786z00_6489 =
													CAR(((obj_t) BgL_cdrzd21382zd2_6487));
												BgL_nz00_6484 = BgL_arg2786z00_6489;
												{	/* Expand/initial.scm 786 */
													obj_t BgL_arg2789z00_6485;

													{	/* Expand/initial.scm 786 */
														obj_t BgL_arg2793z00_6486;

														{	/* Expand/initial.scm 786 */
															obj_t BgL_tmpfunz00_8897;

															BgL_tmpfunz00_8897 = ((obj_t) BgL_ez00_5552);
															BgL_arg2793z00_6486 =
																(VA_PROCEDUREP(BgL_tmpfunz00_8897)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8897)) (((obj_t)
																		BgL_ez00_5552), BgL_nz00_6484,
																	((obj_t) BgL_ez00_5552),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8897)) (((obj_t)
																		BgL_ez00_5552), BgL_nz00_6484,
																	((obj_t) BgL_ez00_5552)));
														}
														BgL_arg2789z00_6485 =
															MAKE_YOUNG_PAIR(BgL_arg2793z00_6486, BNIL);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(121),
														BgL_arg2789z00_6485);
												}
											}
										else
											{	/* Expand/initial.scm 781 */
												goto BgL_tagzd21377zd2_6482;
											}
									}
							}
						else
							{	/* Expand/initial.scm 781 */
								goto BgL_tagzd21377zd2_6482;
							}
					}
				else
					{	/* Expand/initial.scm 781 */
						goto BgL_tagzd21377zd2_6482;
					}
			}
		}

	}



/* &<@anonymous:2760> */
	obj_t BGl_z62zc3z04anonymousza32760ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5553, obj_t BgL_xz00_5554, obj_t BgL_ez00_5555)
	{
		{	/* Expand/initial.scm 769 */
			{
				obj_t BgL_nz00_6493;

				if (PAIRP(BgL_xz00_5554))
					{	/* Expand/initial.scm 769 */
						obj_t BgL_cdrzd21353zd2_6496;

						BgL_cdrzd21353zd2_6496 = CDR(((obj_t) BgL_xz00_5554));
						if (PAIRP(BgL_cdrzd21353zd2_6496))
							{	/* Expand/initial.scm 769 */
								obj_t BgL_carzd21355zd2_6497;

								BgL_carzd21355zd2_6497 = CAR(BgL_cdrzd21353zd2_6496);
								if (INTEGERP(BgL_carzd21355zd2_6497))
									{	/* Expand/initial.scm 769 */
										if (NULLP(CDR(BgL_cdrzd21353zd2_6496)))
											{	/* Expand/initial.scm 769 */
												return
													DOUBLE_TO_REAL(
													(double) ((long) CINT(BgL_carzd21355zd2_6497)));
											}
										else
											{	/* Expand/initial.scm 769 */
											BgL_tagzd21348zd2_6491:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4223z00zzexpand_installz00, BgL_xz00_5554);
											}
									}
								else
									{	/* Expand/initial.scm 769 */
										if (NULLP(CDR(((obj_t) BgL_cdrzd21353zd2_6496))))
											{	/* Expand/initial.scm 769 */
												obj_t BgL_arg2771z00_6498;

												BgL_arg2771z00_6498 =
													CAR(((obj_t) BgL_cdrzd21353zd2_6496));
												BgL_nz00_6493 = BgL_arg2771z00_6498;
												{	/* Expand/initial.scm 774 */
													obj_t BgL_arg2773z00_6494;

													{	/* Expand/initial.scm 774 */
														obj_t BgL_arg2774z00_6495;

														{	/* Expand/initial.scm 774 */
															obj_t BgL_tmpfunz00_8930;

															BgL_tmpfunz00_8930 = ((obj_t) BgL_ez00_5555);
															BgL_arg2774z00_6495 =
																(VA_PROCEDUREP(BgL_tmpfunz00_8930)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8930)) (((obj_t)
																		BgL_ez00_5555), BgL_nz00_6493,
																	((obj_t) BgL_ez00_5555),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8930)) (((obj_t)
																		BgL_ez00_5555), BgL_nz00_6493,
																	((obj_t) BgL_ez00_5555)));
														}
														BgL_arg2773z00_6494 =
															MAKE_YOUNG_PAIR(BgL_arg2774z00_6495, BNIL);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(120),
														BgL_arg2773z00_6494);
												}
											}
										else
											{	/* Expand/initial.scm 769 */
												goto BgL_tagzd21348zd2_6491;
											}
									}
							}
						else
							{	/* Expand/initial.scm 769 */
								goto BgL_tagzd21348zd2_6491;
							}
					}
				else
					{	/* Expand/initial.scm 769 */
						goto BgL_tagzd21348zd2_6491;
					}
			}
		}

	}



/* &<@anonymous:2745> */
	obj_t BGl_z62zc3z04anonymousza32745ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5556, obj_t BgL_xz00_5557, obj_t BgL_ez00_5558)
	{
		{	/* Expand/initial.scm 757 */
			{
				obj_t BgL_nz00_6502;

				if (PAIRP(BgL_xz00_5557))
					{	/* Expand/initial.scm 757 */
						obj_t BgL_cdrzd21324zd2_6505;

						BgL_cdrzd21324zd2_6505 = CDR(((obj_t) BgL_xz00_5557));
						if (PAIRP(BgL_cdrzd21324zd2_6505))
							{	/* Expand/initial.scm 757 */
								obj_t BgL_carzd21326zd2_6506;

								BgL_carzd21326zd2_6506 = CAR(BgL_cdrzd21324zd2_6505);
								if (CHARP(BgL_carzd21326zd2_6506))
									{	/* Expand/initial.scm 757 */
										if (NULLP(CDR(BgL_cdrzd21324zd2_6505)))
											{	/* Expand/initial.scm 757 */
												return BINT((CCHAR(BgL_carzd21326zd2_6506)));
											}
										else
											{	/* Expand/initial.scm 757 */
											BgL_tagzd21319zd2_6500:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4224z00zzexpand_installz00, BgL_xz00_5557);
											}
									}
								else
									{	/* Expand/initial.scm 757 */
										if (NULLP(CDR(((obj_t) BgL_cdrzd21324zd2_6505))))
											{	/* Expand/initial.scm 757 */
												obj_t BgL_arg2755z00_6507;

												BgL_arg2755z00_6507 =
													CAR(((obj_t) BgL_cdrzd21324zd2_6505));
												BgL_nz00_6502 = BgL_arg2755z00_6507;
												{	/* Expand/initial.scm 762 */
													obj_t BgL_arg2757z00_6503;

													{	/* Expand/initial.scm 762 */
														obj_t BgL_arg2758z00_6504;

														{	/* Expand/initial.scm 762 */
															obj_t BgL_tmpfunz00_8963;

															BgL_tmpfunz00_8963 = ((obj_t) BgL_ez00_5558);
															BgL_arg2758z00_6504 =
																(VA_PROCEDUREP(BgL_tmpfunz00_8963)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8963)) (((obj_t)
																		BgL_ez00_5558), BgL_nz00_6502,
																	((obj_t) BgL_ez00_5558),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_8963)) (((obj_t)
																		BgL_ez00_5558), BgL_nz00_6502,
																	((obj_t) BgL_ez00_5558)));
														}
														BgL_arg2757z00_6503 =
															MAKE_YOUNG_PAIR(BgL_arg2758z00_6504, BNIL);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(119),
														BgL_arg2757z00_6503);
												}
											}
										else
											{	/* Expand/initial.scm 757 */
												goto BgL_tagzd21319zd2_6500;
											}
									}
							}
						else
							{	/* Expand/initial.scm 757 */
								goto BgL_tagzd21319zd2_6500;
							}
					}
				else
					{	/* Expand/initial.scm 757 */
						goto BgL_tagzd21319zd2_6500;
					}
			}
		}

	}



/* &<@anonymous:2691> */
	obj_t BGl_z62zc3z04anonymousza32691ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5559, obj_t BgL_xz00_5560, obj_t BgL_ez00_5561)
	{
		{	/* Expand/initial.scm 739 */
			{
				obj_t BgL_strz00_6534;
				obj_t BgL_strz00_6528;
				obj_t BgL_rz00_6529;
				obj_t BgL_strz00_6521;
				obj_t BgL_rz00_6522;
				obj_t BgL_startz00_6523;

				if (PAIRP(BgL_xz00_5560))
					{	/* Expand/initial.scm 739 */
						obj_t BgL_cdrzd21197zd2_6539;

						BgL_cdrzd21197zd2_6539 = CDR(((obj_t) BgL_xz00_5560));
						if (PAIRP(BgL_cdrzd21197zd2_6539))
							{	/* Expand/initial.scm 739 */
								if (NULLP(CDR(BgL_cdrzd21197zd2_6539)))
									{	/* Expand/initial.scm 739 */
										BgL_strz00_6534 = CAR(BgL_cdrzd21197zd2_6539);
										{	/* Expand/initial.scm 742 */
											obj_t BgL_arg2721z00_6535;

											{	/* Expand/initial.scm 742 */
												obj_t BgL_arg2722z00_6536;

												{	/* Expand/initial.scm 742 */
													obj_t BgL_arg2723z00_6537;

													{	/* Expand/initial.scm 742 */
														obj_t BgL_arg2724z00_6538;

														BgL_arg2724z00_6538 =
															MAKE_YOUNG_PAIR(BINT(10L), BNIL);
														BgL_arg2723z00_6537 =
															MAKE_YOUNG_PAIR(BINT(0L), BgL_arg2724z00_6538);
													}
													BgL_arg2722z00_6536 =
														MAKE_YOUNG_PAIR(BgL_strz00_6534,
														BgL_arg2723z00_6537);
												}
												BgL_arg2721z00_6535 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(179),
													BgL_arg2722z00_6536);
											}
											{	/* Expand/initial.scm 742 */
												obj_t BgL_tmpfunz00_8990;

												BgL_tmpfunz00_8990 = ((obj_t) BgL_ez00_5561);
												return
													(VA_PROCEDUREP(BgL_tmpfunz00_8990) ? ((obj_t(*)(obj_t,
																...))
														PROCEDURE_ENTRY(BgL_tmpfunz00_8990)) (((obj_t)
															BgL_ez00_5561), BgL_arg2721z00_6535,
														((obj_t) BgL_ez00_5561), BEOA) : ((obj_t(*)(obj_t,
																obj_t,
																obj_t))
														PROCEDURE_ENTRY(BgL_tmpfunz00_8990)) (((obj_t)
															BgL_ez00_5561), BgL_arg2721z00_6535,
														((obj_t) BgL_ez00_5561)));
											}
										}
									}
								else
									{	/* Expand/initial.scm 739 */
										obj_t BgL_cdrzd21214zd2_6540;

										BgL_cdrzd21214zd2_6540 =
											CDR(((obj_t) BgL_cdrzd21197zd2_6539));
										if (PAIRP(BgL_cdrzd21214zd2_6540))
											{	/* Expand/initial.scm 739 */
												obj_t BgL_carzd21218zd2_6541;

												BgL_carzd21218zd2_6541 = CAR(BgL_cdrzd21214zd2_6540);
												if (INTEGERP(BgL_carzd21218zd2_6541))
													{	/* Expand/initial.scm 739 */
														if (NULLP(CDR(BgL_cdrzd21214zd2_6540)))
															{	/* Expand/initial.scm 739 */
																obj_t BgL_arg2701z00_6542;

																BgL_arg2701z00_6542 =
																	CAR(((obj_t) BgL_cdrzd21197zd2_6539));
																BgL_strz00_6528 = BgL_arg2701z00_6542;
																BgL_rz00_6529 = BgL_carzd21218zd2_6541;
																{	/* Expand/initial.scm 744 */
																	bool_t BgL_test4512z00_9005;

																	if (((long) CINT(BgL_rz00_6529) >= 2L))
																		{	/* Expand/initial.scm 744 */
																			BgL_test4512z00_9005 =
																				((long) CINT(BgL_rz00_6529) <= 36L);
																		}
																	else
																		{	/* Expand/initial.scm 744 */
																			BgL_test4512z00_9005 = ((bool_t) 0);
																		}
																	if (BgL_test4512z00_9005)
																		{	/* Expand/initial.scm 745 */
																			obj_t BgL_arg2727z00_6530;

																			{	/* Expand/initial.scm 745 */
																				obj_t BgL_arg2728z00_6531;

																				{	/* Expand/initial.scm 745 */
																					obj_t BgL_arg2729z00_6532;

																					{	/* Expand/initial.scm 745 */
																						obj_t BgL_arg2731z00_6533;

																						BgL_arg2731z00_6533 =
																							MAKE_YOUNG_PAIR(BgL_rz00_6529,
																							BNIL);
																						BgL_arg2729z00_6532 =
																							MAKE_YOUNG_PAIR(BINT(0L),
																							BgL_arg2731z00_6533);
																					}
																					BgL_arg2728z00_6531 =
																						MAKE_YOUNG_PAIR(BgL_strz00_6528,
																						BgL_arg2729z00_6532);
																				}
																				BgL_arg2727z00_6530 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(179),
																					BgL_arg2728z00_6531);
																			}
																			{	/* Expand/initial.scm 745 */
																				obj_t BgL_tmpfunz00_9023;

																				BgL_tmpfunz00_9023 =
																					((obj_t) BgL_ez00_5561);
																				return
																					(VA_PROCEDUREP(BgL_tmpfunz00_9023)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9023))
																					(((obj_t) BgL_ez00_5561),
																						BgL_arg2727z00_6530,
																						((obj_t) BgL_ez00_5561),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9023))
																					(((obj_t) BgL_ez00_5561),
																						BgL_arg2727z00_6530,
																						((obj_t) BgL_ez00_5561)));
																			}
																		}
																	else
																		{	/* Expand/initial.scm 744 */
																			return
																				BGl_errorz00zz__errorz00(BFALSE,
																				BGl_string4225z00zzexpand_installz00,
																				BgL_rz00_6529);
																		}
																}
															}
														else
															{	/* Expand/initial.scm 739 */
																obj_t BgL_cdrzd21239zd2_6544;

																BgL_cdrzd21239zd2_6544 =
																	CDR(((obj_t) BgL_cdrzd21197zd2_6539));
																{	/* Expand/initial.scm 739 */
																	obj_t BgL_carzd21245zd2_6545;
																	obj_t BgL_cdrzd21246zd2_6546;

																	BgL_carzd21245zd2_6545 =
																		CAR(((obj_t) BgL_cdrzd21239zd2_6544));
																	BgL_cdrzd21246zd2_6546 =
																		CDR(((obj_t) BgL_cdrzd21239zd2_6544));
																	if (INTEGERP(BgL_carzd21245zd2_6545))
																		{	/* Expand/initial.scm 739 */
																			if (PAIRP(BgL_cdrzd21246zd2_6546))
																				{	/* Expand/initial.scm 739 */
																					obj_t BgL_carzd21253zd2_6547;

																					BgL_carzd21253zd2_6547 =
																						CAR(BgL_cdrzd21246zd2_6546);
																					if (INTEGERP(BgL_carzd21253zd2_6547))
																						{	/* Expand/initial.scm 739 */
																							if (NULLP(CDR
																									(BgL_cdrzd21246zd2_6546)))
																								{	/* Expand/initial.scm 739 */
																									obj_t BgL_arg2707z00_6548;

																									BgL_arg2707z00_6548 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd21197zd2_6539));
																									BgL_strz00_6521 =
																										BgL_arg2707z00_6548;
																									BgL_rz00_6522 =
																										BgL_carzd21245zd2_6545;
																									BgL_startz00_6523 =
																										BgL_carzd21253zd2_6547;
																								BgL_tagzd21189zd2_6510:
																									{	/* Expand/initial.scm 748 */
																										bool_t BgL_test4518z00_9044;

																										if (
																											((long)
																												CINT(BgL_rz00_6522) >=
																												2L))
																											{	/* Expand/initial.scm 748 */
																												BgL_test4518z00_9044 =
																													(
																													(long)
																													CINT(BgL_rz00_6522) <=
																													36L);
																											}
																										else
																											{	/* Expand/initial.scm 748 */
																												BgL_test4518z00_9044 =
																													((bool_t) 0);
																											}
																										if (BgL_test4518z00_9044)
																											{	/* Expand/initial.scm 749 */
																												obj_t
																													BgL_arg2734z00_6524;
																												{	/* Expand/initial.scm 749 */
																													obj_t
																														BgL_arg2736z00_6525;
																													{	/* Expand/initial.scm 749 */
																														obj_t
																															BgL_arg2737z00_6526;
																														{	/* Expand/initial.scm 749 */
																															obj_t
																																BgL_arg2738z00_6527;
																															BgL_arg2738z00_6527
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_rz00_6522,
																																BNIL);
																															BgL_arg2737z00_6526
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_startz00_6523,
																																BgL_arg2738z00_6527);
																														}
																														BgL_arg2736z00_6525
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_strz00_6521,
																															BgL_arg2737z00_6526);
																													}
																													BgL_arg2734z00_6524 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF
																														(179),
																														BgL_arg2736z00_6525);
																												}
																												{	/* Expand/initial.scm 749 */
																													obj_t
																														BgL_tmpfunz00_9061;
																													BgL_tmpfunz00_9061 =
																														((obj_t)
																														BgL_ez00_5561);
																													return (VA_PROCEDUREP
																														(BgL_tmpfunz00_9061)
																														? ((obj_t(*)(obj_t,
																																	...))
																															PROCEDURE_ENTRY
																															(BgL_tmpfunz00_9061))
																														(((obj_t)
																																BgL_ez00_5561),
																															BgL_arg2734z00_6524,
																															((obj_t)
																																BgL_ez00_5561),
																															BEOA)
																														: ((obj_t(*)(obj_t,
																																	obj_t,
																																	obj_t))
																															PROCEDURE_ENTRY
																															(BgL_tmpfunz00_9061))
																														(((obj_t)
																																BgL_ez00_5561),
																															BgL_arg2734z00_6524,
																															((obj_t)
																																BgL_ez00_5561)));
																												}
																											}
																										else
																											{	/* Expand/initial.scm 748 */
																												return
																													BGl_errorz00zz__errorz00
																													(BFALSE,
																													BGl_string4225z00zzexpand_installz00,
																													BgL_rz00_6522);
																											}
																									}
																								}
																							else
																								{	/* Expand/initial.scm 739 */
																								BgL_tagzd21190zd2_6509:
																									if (NULLP(BgL_xz00_5560))
																										{	/* Expand/initial.scm 752 */
																											return BNIL;
																										}
																									else
																										{	/* Expand/initial.scm 752 */
																											obj_t
																												BgL_head1333z00_6513;
																											BgL_head1333z00_6513 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											{
																												obj_t BgL_l1331z00_6515;
																												obj_t
																													BgL_tail1334z00_6516;
																												BgL_l1331z00_6515 =
																													BgL_xz00_5560;
																												BgL_tail1334z00_6516 =
																													BgL_head1333z00_6513;
																											BgL_zc3z04anonymousza32740ze3z87_6514:
																												if (NULLP
																													(BgL_l1331z00_6515))
																													{	/* Expand/initial.scm 752 */
																														return
																															CDR
																															(BgL_head1333z00_6513);
																													}
																												else
																													{	/* Expand/initial.scm 752 */
																														obj_t
																															BgL_newtail1335z00_6517;
																														{	/* Expand/initial.scm 752 */
																															obj_t
																																BgL_arg2743z00_6518;
																															{	/* Expand/initial.scm 752 */
																																obj_t
																																	BgL_xz00_6519;
																																BgL_xz00_6519 =
																																	CAR(((obj_t)
																																		BgL_l1331z00_6515));
																																{	/* Expand/initial.scm 752 */
																																	obj_t
																																		BgL_tmpfunz00_9078;
																																	BgL_tmpfunz00_9078
																																		=
																																		((obj_t)
																																		BgL_ez00_5561);
																																	BgL_arg2743z00_6518
																																		=
																																		(VA_PROCEDUREP
																																		(BgL_tmpfunz00_9078)
																																		? ((obj_t(*)
																																				(obj_t,
																																					...))
																																			PROCEDURE_ENTRY
																																			(BgL_tmpfunz00_9078))
																																		(((obj_t)
																																				BgL_ez00_5561),
																																			BgL_xz00_6519,
																																			((obj_t)
																																				BgL_ez00_5561),
																																			BEOA)
																																		: ((obj_t(*)
																																				(obj_t,
																																					obj_t,
																																					obj_t))
																																			PROCEDURE_ENTRY
																																			(BgL_tmpfunz00_9078))
																																		(((obj_t)
																																				BgL_ez00_5561),
																																			BgL_xz00_6519,
																																			((obj_t)
																																				BgL_ez00_5561)));
																																}
																															}
																															BgL_newtail1335z00_6517
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2743z00_6518,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_tail1334z00_6516,
																															BgL_newtail1335z00_6517);
																														{	/* Expand/initial.scm 752 */
																															obj_t
																																BgL_arg2742z00_6520;
																															BgL_arg2742z00_6520
																																=
																																CDR(((obj_t)
																																	BgL_l1331z00_6515));
																															{
																																obj_t
																																	BgL_tail1334z00_9085;
																																obj_t
																																	BgL_l1331z00_9084;
																																BgL_l1331z00_9084
																																	=
																																	BgL_arg2742z00_6520;
																																BgL_tail1334z00_9085
																																	=
																																	BgL_newtail1335z00_6517;
																																BgL_tail1334z00_6516
																																	=
																																	BgL_tail1334z00_9085;
																																BgL_l1331z00_6515
																																	=
																																	BgL_l1331z00_9084;
																																goto
																																	BgL_zc3z04anonymousza32740ze3z87_6514;
																															}
																														}
																													}
																											}
																										}
																								}
																						}
																					else
																						{	/* Expand/initial.scm 739 */
																							goto BgL_tagzd21190zd2_6509;
																						}
																				}
																			else
																				{	/* Expand/initial.scm 739 */
																					goto BgL_tagzd21190zd2_6509;
																				}
																		}
																	else
																		{	/* Expand/initial.scm 739 */
																			goto BgL_tagzd21190zd2_6509;
																		}
																}
															}
													}
												else
													{	/* Expand/initial.scm 739 */
														obj_t BgL_cdrzd21272zd2_6550;

														BgL_cdrzd21272zd2_6550 =
															CDR(((obj_t) BgL_cdrzd21197zd2_6539));
														{	/* Expand/initial.scm 739 */
															obj_t BgL_carzd21277zd2_6551;
															obj_t BgL_cdrzd21278zd2_6552;

															BgL_carzd21277zd2_6551 =
																CAR(((obj_t) BgL_cdrzd21272zd2_6550));
															BgL_cdrzd21278zd2_6552 =
																CDR(((obj_t) BgL_cdrzd21272zd2_6550));
															if (INTEGERP(BgL_carzd21277zd2_6551))
																{	/* Expand/initial.scm 739 */
																	if (PAIRP(BgL_cdrzd21278zd2_6552))
																		{	/* Expand/initial.scm 739 */
																			obj_t BgL_carzd21283zd2_6553;

																			BgL_carzd21283zd2_6553 =
																				CAR(BgL_cdrzd21278zd2_6552);
																			if (INTEGERP(BgL_carzd21283zd2_6553))
																				{	/* Expand/initial.scm 739 */
																					if (NULLP(CDR
																							(BgL_cdrzd21278zd2_6552)))
																						{	/* Expand/initial.scm 739 */
																							obj_t BgL_arg2716z00_6554;

																							BgL_arg2716z00_6554 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd21197zd2_6539));
																							{
																								obj_t BgL_startz00_9106;
																								obj_t BgL_rz00_9105;
																								obj_t BgL_strz00_9104;

																								BgL_strz00_9104 =
																									BgL_arg2716z00_6554;
																								BgL_rz00_9105 =
																									BgL_carzd21277zd2_6551;
																								BgL_startz00_9106 =
																									BgL_carzd21283zd2_6553;
																								BgL_startz00_6523 =
																									BgL_startz00_9106;
																								BgL_rz00_6522 = BgL_rz00_9105;
																								BgL_strz00_6521 =
																									BgL_strz00_9104;
																								goto BgL_tagzd21189zd2_6510;
																							}
																						}
																					else
																						{	/* Expand/initial.scm 739 */
																							goto BgL_tagzd21190zd2_6509;
																						}
																				}
																			else
																				{	/* Expand/initial.scm 739 */
																					goto BgL_tagzd21190zd2_6509;
																				}
																		}
																	else
																		{	/* Expand/initial.scm 739 */
																			goto BgL_tagzd21190zd2_6509;
																		}
																}
															else
																{	/* Expand/initial.scm 739 */
																	goto BgL_tagzd21190zd2_6509;
																}
														}
													}
											}
										else
											{	/* Expand/initial.scm 739 */
												goto BgL_tagzd21190zd2_6509;
											}
									}
							}
						else
							{	/* Expand/initial.scm 739 */
								goto BgL_tagzd21190zd2_6509;
							}
					}
				else
					{	/* Expand/initial.scm 739 */
						goto BgL_tagzd21190zd2_6509;
					}
			}
		}

	}



/* &<@anonymous:2673> */
	obj_t BGl_z62zc3z04anonymousza32673ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5562, obj_t BgL_xz00_5563, obj_t BgL_ez00_5564)
	{
		{	/* Expand/initial.scm 725 */
			{
				obj_t BgL_nz00_6558;

				if (PAIRP(BgL_xz00_5563))
					{	/* Expand/initial.scm 725 */
						obj_t BgL_cdrzd21165zd2_6563;

						BgL_cdrzd21165zd2_6563 = CDR(((obj_t) BgL_xz00_5563));
						if (PAIRP(BgL_cdrzd21165zd2_6563))
							{	/* Expand/initial.scm 725 */
								obj_t BgL_carzd21167zd2_6564;

								BgL_carzd21167zd2_6564 = CAR(BgL_cdrzd21165zd2_6563);
								if (INTEGERP(BgL_carzd21167zd2_6564))
									{	/* Expand/initial.scm 725 */
										if (NULLP(CDR(BgL_cdrzd21165zd2_6563)))
											{	/* Expand/initial.scm 725 */
												return BCHAR(((long) CINT(BgL_carzd21167zd2_6564)));
											}
										else
											{	/* Expand/initial.scm 725 */
											BgL_tagzd21160zd2_6556:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4224z00zzexpand_installz00, BgL_xz00_5563);
											}
									}
								else
									{	/* Expand/initial.scm 725 */
										if (NULLP(CDR(((obj_t) BgL_cdrzd21165zd2_6563))))
											{	/* Expand/initial.scm 725 */
												obj_t BgL_arg2684z00_6565;

												BgL_arg2684z00_6565 =
													CAR(((obj_t) BgL_cdrzd21165zd2_6563));
												BgL_nz00_6558 = BgL_arg2684z00_6565;
												if (CBOOL(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
													{	/* Expand/initial.scm 731 */
														obj_t BgL_arg2686z00_6559;

														{	/* Expand/initial.scm 731 */
															obj_t BgL_arg2687z00_6560;

															{	/* Expand/initial.scm 731 */
																obj_t BgL_tmpfunz00_9137;

																BgL_tmpfunz00_9137 = ((obj_t) BgL_ez00_5564);
																BgL_arg2687z00_6560 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_9137)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9137)) ((
																			(obj_t) BgL_ez00_5564), BgL_nz00_6558,
																		((obj_t) BgL_ez00_5564),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9137)) ((
																			(obj_t) BgL_ez00_5564), BgL_nz00_6558,
																		((obj_t) BgL_ez00_5564)));
															}
															BgL_arg2686z00_6559 =
																MAKE_YOUNG_PAIR(BgL_arg2687z00_6560, BNIL);
														}
														return
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(180),
															BgL_arg2686z00_6559);
													}
												else
													{	/* Expand/initial.scm 732 */
														obj_t BgL_arg2688z00_6561;

														{	/* Expand/initial.scm 732 */
															obj_t BgL_arg2689z00_6562;

															{	/* Expand/initial.scm 732 */
																obj_t BgL_tmpfunz00_9148;

																BgL_tmpfunz00_9148 = ((obj_t) BgL_ez00_5564);
																BgL_arg2689z00_6562 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_9148)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9148)) ((
																			(obj_t) BgL_ez00_5564), BgL_nz00_6558,
																		((obj_t) BgL_ez00_5564),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9148)) ((
																			(obj_t) BgL_ez00_5564), BgL_nz00_6558,
																		((obj_t) BgL_ez00_5564)));
															}
															BgL_arg2688z00_6561 =
																MAKE_YOUNG_PAIR(BgL_arg2689z00_6562, BNIL);
														}
														return
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(117),
															BgL_arg2688z00_6561);
													}
											}
										else
											{	/* Expand/initial.scm 725 */
												goto BgL_tagzd21160zd2_6556;
											}
									}
							}
						else
							{	/* Expand/initial.scm 725 */
								goto BgL_tagzd21160zd2_6556;
							}
					}
				else
					{	/* Expand/initial.scm 725 */
						goto BgL_tagzd21160zd2_6556;
					}
			}
		}

	}



/* &<@anonymous:2641> */
	obj_t BGl_z62zc3z04anonymousza32641ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5565, obj_t BgL_xz00_5566, obj_t BgL_ez00_5567)
	{
		{	/* Expand/initial.scm 708 */
			{
				obj_t BgL_s1z00_6569;
				obj_t BgL_o1z00_6570;
				obj_t BgL_s2z00_6571;
				obj_t BgL_o2z00_6572;
				obj_t BgL_lz00_6573;

				if (PAIRP(BgL_xz00_5566))
					{	/* Expand/initial.scm 708 */
						obj_t BgL_cdrzd21127zd2_6589;

						BgL_cdrzd21127zd2_6589 = CDR(((obj_t) BgL_xz00_5566));
						if (PAIRP(BgL_cdrzd21127zd2_6589))
							{	/* Expand/initial.scm 708 */
								obj_t BgL_cdrzd21134zd2_6590;

								BgL_cdrzd21134zd2_6590 = CDR(BgL_cdrzd21127zd2_6589);
								if (PAIRP(BgL_cdrzd21134zd2_6590))
									{	/* Expand/initial.scm 708 */
										obj_t BgL_cdrzd21141zd2_6591;

										BgL_cdrzd21141zd2_6591 = CDR(BgL_cdrzd21134zd2_6590);
										if (PAIRP(BgL_cdrzd21141zd2_6591))
											{	/* Expand/initial.scm 708 */
												obj_t BgL_cdrzd21147zd2_6592;

												BgL_cdrzd21147zd2_6592 = CDR(BgL_cdrzd21141zd2_6591);
												if (PAIRP(BgL_cdrzd21147zd2_6592))
													{	/* Expand/initial.scm 708 */
														obj_t BgL_cdrzd21152zd2_6593;

														BgL_cdrzd21152zd2_6593 =
															CDR(BgL_cdrzd21147zd2_6592);
														if (PAIRP(BgL_cdrzd21152zd2_6593))
															{	/* Expand/initial.scm 708 */
																if (NULLP(CDR(BgL_cdrzd21152zd2_6593)))
																	{	/* Expand/initial.scm 708 */
																		BgL_s1z00_6569 =
																			CAR(BgL_cdrzd21127zd2_6589);
																		BgL_o1z00_6570 =
																			CAR(BgL_cdrzd21134zd2_6590);
																		BgL_s2z00_6571 =
																			CAR(BgL_cdrzd21141zd2_6591);
																		BgL_o2z00_6572 =
																			CAR(BgL_cdrzd21147zd2_6592);
																		BgL_lz00_6573 = CAR(BgL_cdrzd21152zd2_6593);
																		{	/* Expand/initial.scm 711 */
																			obj_t BgL_s1z00_6574;
																			obj_t BgL_o1z00_6575;
																			obj_t BgL_s2z00_6576;
																			obj_t BgL_o2z00_6577;
																			obj_t BgL_lz00_6578;

																			{	/* Expand/initial.scm 711 */
																				obj_t BgL_tmpfunz00_9180;

																				BgL_tmpfunz00_9180 =
																					((obj_t) BgL_ez00_5567);
																				BgL_s1z00_6574 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_9180)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9180))
																					(((obj_t) BgL_ez00_5567),
																						BgL_s1z00_6569,
																						((obj_t) BgL_ez00_5567),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9180))
																					(((obj_t) BgL_ez00_5567),
																						BgL_s1z00_6569,
																						((obj_t) BgL_ez00_5567)));
																			}
																			{	/* Expand/initial.scm 712 */
																				obj_t BgL_tmpfunz00_9188;

																				BgL_tmpfunz00_9188 =
																					((obj_t) BgL_ez00_5567);
																				BgL_o1z00_6575 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_9188)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9188))
																					(((obj_t) BgL_ez00_5567),
																						BgL_o1z00_6570,
																						((obj_t) BgL_ez00_5567),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9188))
																					(((obj_t) BgL_ez00_5567),
																						BgL_o1z00_6570,
																						((obj_t) BgL_ez00_5567)));
																			}
																			{	/* Expand/initial.scm 713 */
																				obj_t BgL_tmpfunz00_9196;

																				BgL_tmpfunz00_9196 =
																					((obj_t) BgL_ez00_5567);
																				BgL_s2z00_6576 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_9196)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9196))
																					(((obj_t) BgL_ez00_5567),
																						BgL_s2z00_6571,
																						((obj_t) BgL_ez00_5567),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9196))
																					(((obj_t) BgL_ez00_5567),
																						BgL_s2z00_6571,
																						((obj_t) BgL_ez00_5567)));
																			}
																			{	/* Expand/initial.scm 714 */
																				obj_t BgL_tmpfunz00_9204;

																				BgL_tmpfunz00_9204 =
																					((obj_t) BgL_ez00_5567);
																				BgL_o2z00_6577 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_9204)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9204))
																					(((obj_t) BgL_ez00_5567),
																						BgL_o2z00_6572,
																						((obj_t) BgL_ez00_5567),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9204))
																					(((obj_t) BgL_ez00_5567),
																						BgL_o2z00_6572,
																						((obj_t) BgL_ez00_5567)));
																			}
																			{	/* Expand/initial.scm 715 */
																				obj_t BgL_tmpfunz00_9212;

																				BgL_tmpfunz00_9212 =
																					((obj_t) BgL_ez00_5567);
																				BgL_lz00_6578 =
																					(VA_PROCEDUREP(BgL_tmpfunz00_9212)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9212))
																					(((obj_t) BgL_ez00_5567),
																						BgL_lz00_6573,
																						((obj_t) BgL_ez00_5567),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_9212))
																					(((obj_t) BgL_ez00_5567),
																						BgL_lz00_6573,
																						((obj_t) BgL_ez00_5567)));
																			}
																			if (CBOOL
																				(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
																				{	/* Expand/initial.scm 717 */
																					obj_t BgL_arg2658z00_6579;

																					{	/* Expand/initial.scm 717 */
																						obj_t BgL_arg2659z00_6580;

																						{	/* Expand/initial.scm 717 */
																							obj_t BgL_arg2660z00_6581;

																							{	/* Expand/initial.scm 717 */
																								obj_t BgL_arg2662z00_6582;

																								{	/* Expand/initial.scm 717 */
																									obj_t BgL_arg2664z00_6583;

																									BgL_arg2664z00_6583 =
																										MAKE_YOUNG_PAIR
																										(BgL_lz00_6578, BNIL);
																									BgL_arg2662z00_6582 =
																										MAKE_YOUNG_PAIR
																										(BgL_o2z00_6577,
																										BgL_arg2664z00_6583);
																								}
																								BgL_arg2660z00_6581 =
																									MAKE_YOUNG_PAIR
																									(BgL_s2z00_6576,
																									BgL_arg2662z00_6582);
																							}
																							BgL_arg2659z00_6580 =
																								MAKE_YOUNG_PAIR(BgL_o1z00_6575,
																								BgL_arg2660z00_6581);
																						}
																						BgL_arg2658z00_6579 =
																							MAKE_YOUNG_PAIR(BgL_s1z00_6574,
																							BgL_arg2659z00_6580);
																					}
																					return
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(181),
																						BgL_arg2658z00_6579);
																				}
																			else
																				{	/* Expand/initial.scm 718 */
																					obj_t BgL_arg2665z00_6584;

																					{	/* Expand/initial.scm 718 */
																						obj_t BgL_arg2666z00_6585;

																						{	/* Expand/initial.scm 718 */
																							obj_t BgL_arg2667z00_6586;

																							{	/* Expand/initial.scm 718 */
																								obj_t BgL_arg2669z00_6587;

																								{	/* Expand/initial.scm 718 */
																									obj_t BgL_arg2670z00_6588;

																									BgL_arg2670z00_6588 =
																										MAKE_YOUNG_PAIR
																										(BgL_lz00_6578, BNIL);
																									BgL_arg2669z00_6587 =
																										MAKE_YOUNG_PAIR
																										(BgL_o2z00_6577,
																										BgL_arg2670z00_6588);
																								}
																								BgL_arg2667z00_6586 =
																									MAKE_YOUNG_PAIR
																									(BgL_s2z00_6576,
																									BgL_arg2669z00_6587);
																							}
																							BgL_arg2666z00_6585 =
																								MAKE_YOUNG_PAIR(BgL_o1z00_6575,
																								BgL_arg2667z00_6586);
																						}
																						BgL_arg2665z00_6584 =
																							MAKE_YOUNG_PAIR(BgL_s1z00_6574,
																							BgL_arg2666z00_6585);
																					}
																					return
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(116),
																						BgL_arg2665z00_6584);
																				}
																		}
																	}
																else
																	{	/* Expand/initial.scm 708 */
																	BgL_tagzd21114zd2_6567:
																		return
																			BGl_errorz00zz__errorz00(BFALSE,
																			BGl_string4226z00zzexpand_installz00,
																			BgL_xz00_5566);
																	}
															}
														else
															{	/* Expand/initial.scm 708 */
																goto BgL_tagzd21114zd2_6567;
															}
													}
												else
													{	/* Expand/initial.scm 708 */
														goto BgL_tagzd21114zd2_6567;
													}
											}
										else
											{	/* Expand/initial.scm 708 */
												goto BgL_tagzd21114zd2_6567;
											}
									}
								else
									{	/* Expand/initial.scm 708 */
										goto BgL_tagzd21114zd2_6567;
									}
							}
						else
							{	/* Expand/initial.scm 708 */
								goto BgL_tagzd21114zd2_6567;
							}
					}
				else
					{	/* Expand/initial.scm 708 */
						goto BgL_tagzd21114zd2_6567;
					}
			}
		}

	}



/* &<@anonymous:2610> */
	obj_t BGl_z62zc3z04anonymousza32610ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5568, obj_t BgL_xz00_5569, obj_t BgL_ez00_5570)
	{
		{	/* Expand/initial.scm 696 */
			{
				obj_t BgL_s1z00_6597;
				obj_t BgL_s2z00_6598;
				obj_t BgL_oz00_6599;

				if (PAIRP(BgL_xz00_5569))
					{	/* Expand/initial.scm 696 */
						obj_t BgL_cdrzd21018zd2_6604;

						BgL_cdrzd21018zd2_6604 = CDR(((obj_t) BgL_xz00_5569));
						if (PAIRP(BgL_cdrzd21018zd2_6604))
							{	/* Expand/initial.scm 696 */
								obj_t BgL_cdrzd21023zd2_6605;

								BgL_cdrzd21023zd2_6605 = CDR(BgL_cdrzd21018zd2_6604);
								if (PAIRP(BgL_cdrzd21023zd2_6605))
									{	/* Expand/initial.scm 696 */
										obj_t BgL_cdrzd21028zd2_6606;

										BgL_cdrzd21028zd2_6606 = CDR(BgL_cdrzd21023zd2_6605);
										if (PAIRP(BgL_cdrzd21028zd2_6606))
											{	/* Expand/initial.scm 696 */
												if (NULLP(CDR(BgL_cdrzd21028zd2_6606)))
													{	/* Expand/initial.scm 696 */
														BgL_s1z00_6597 = CAR(BgL_cdrzd21018zd2_6604);
														BgL_s2z00_6598 = CAR(BgL_cdrzd21023zd2_6605);
														BgL_oz00_6599 = CAR(BgL_cdrzd21028zd2_6606);
														{	/* Expand/initial.scm 699 */
															obj_t BgL_arg2630z00_6600;

															{	/* Expand/initial.scm 699 */
																obj_t BgL_arg2631z00_6601;

																{	/* Expand/initial.scm 699 */
																	obj_t BgL_arg2632z00_6602;

																	{	/* Expand/initial.scm 699 */
																		obj_t BgL_arg2633z00_6603;

																		BgL_arg2633z00_6603 =
																			MAKE_YOUNG_PAIR(BgL_oz00_6599, BNIL);
																		BgL_arg2632z00_6602 =
																			MAKE_YOUNG_PAIR(BgL_s2z00_6598,
																			BgL_arg2633z00_6603);
																	}
																	BgL_arg2631z00_6601 =
																		MAKE_YOUNG_PAIR(BgL_s1z00_6597,
																		BgL_arg2632z00_6602);
																}
																BgL_arg2630z00_6600 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(182),
																	BgL_arg2631z00_6601);
															}
															{	/* Expand/initial.scm 699 */
																obj_t BgL_tmpfunz00_9262;

																BgL_tmpfunz00_9262 = ((obj_t) BgL_ez00_5570);
																return
																	(VA_PROCEDUREP(BgL_tmpfunz00_9262)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9262)) ((
																			(obj_t) BgL_ez00_5570),
																		BgL_arg2630z00_6600,
																		((obj_t) BgL_ez00_5570),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9262)) ((
																			(obj_t) BgL_ez00_5570),
																		BgL_arg2630z00_6600,
																		((obj_t) BgL_ez00_5570)));
															}
														}
													}
												else
													{	/* Expand/initial.scm 696 */
														obj_t BgL_cdrzd21055zd2_6607;

														BgL_cdrzd21055zd2_6607 =
															CDR(((obj_t) BgL_cdrzd21018zd2_6604));
														{	/* Expand/initial.scm 696 */
															obj_t BgL_cdrzd21064zd2_6608;

															BgL_cdrzd21064zd2_6608 =
																CDR(((obj_t) BgL_cdrzd21055zd2_6607));
															{	/* Expand/initial.scm 696 */
																obj_t BgL_cdrzd21071zd2_6609;

																BgL_cdrzd21071zd2_6609 =
																	CDR(((obj_t) BgL_cdrzd21064zd2_6608));
																if (PAIRP(BgL_cdrzd21071zd2_6609))
																	{	/* Expand/initial.scm 696 */
																		if (NULLP(CDR(BgL_cdrzd21071zd2_6609)))
																			{	/* Expand/initial.scm 696 */
																				obj_t BgL_arg2623z00_6610;
																				obj_t BgL_arg2624z00_6611;
																				obj_t BgL_arg2626z00_6612;
																				obj_t BgL_arg2627z00_6613;

																				BgL_arg2623z00_6610 =
																					CAR(((obj_t) BgL_cdrzd21018zd2_6604));
																				BgL_arg2624z00_6611 =
																					CAR(((obj_t) BgL_cdrzd21055zd2_6607));
																				BgL_arg2626z00_6612 =
																					CAR(((obj_t) BgL_cdrzd21064zd2_6608));
																				BgL_arg2627z00_6613 =
																					CAR(BgL_cdrzd21071zd2_6609);
																				{	/* Expand/initial.scm 701 */
																					obj_t BgL_arg2635z00_6614;

																					{	/* Expand/initial.scm 701 */
																						obj_t BgL_arg2636z00_6615;

																						{	/* Expand/initial.scm 701 */
																							obj_t BgL_arg2637z00_6616;

																							{	/* Expand/initial.scm 701 */
																								obj_t BgL_arg2638z00_6617;

																								{	/* Expand/initial.scm 701 */
																									obj_t BgL_arg2639z00_6618;

																									BgL_arg2639z00_6618 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2627z00_6613, BNIL);
																									BgL_arg2638z00_6617 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2626z00_6612,
																										BgL_arg2639z00_6618);
																								}
																								BgL_arg2637z00_6616 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2624z00_6611,
																									BgL_arg2638z00_6617);
																							}
																							BgL_arg2636z00_6615 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2623z00_6610,
																								BgL_arg2637z00_6616);
																						}
																						BgL_arg2635z00_6614 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(183), BgL_arg2636z00_6615);
																					}
																					{	/* Expand/initial.scm 701 */
																						obj_t BgL_tmpfunz00_9297;

																						BgL_tmpfunz00_9297 =
																							((obj_t) BgL_ez00_5570);
																						return
																							(VA_PROCEDUREP(BgL_tmpfunz00_9297)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9297)) (((obj_t)
																									BgL_ez00_5570),
																								BgL_arg2635z00_6614,
																								((obj_t) BgL_ez00_5570),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9297)) (((obj_t)
																									BgL_ez00_5570),
																								BgL_arg2635z00_6614,
																								((obj_t) BgL_ez00_5570)));
																					}
																				}
																			}
																		else
																			{	/* Expand/initial.scm 696 */
																			BgL_tagzd21008zd2_6595:
																				return
																					BGl_errorz00zz__errorz00(BFALSE,
																					BGl_string4227z00zzexpand_installz00,
																					BgL_xz00_5569);
																			}
																	}
																else
																	{	/* Expand/initial.scm 696 */
																		goto BgL_tagzd21008zd2_6595;
																	}
															}
														}
													}
											}
										else
											{	/* Expand/initial.scm 696 */
												goto BgL_tagzd21008zd2_6595;
											}
									}
								else
									{	/* Expand/initial.scm 696 */
										goto BgL_tagzd21008zd2_6595;
									}
							}
						else
							{	/* Expand/initial.scm 696 */
								goto BgL_tagzd21008zd2_6595;
							}
					}
				else
					{	/* Expand/initial.scm 696 */
						goto BgL_tagzd21008zd2_6595;
					}
			}
		}

	}



/* &<@anonymous:2567> */
	obj_t BGl_z62zc3z04anonymousza32567ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5571, obj_t BgL_xz00_5572, obj_t BgL_ez00_5573)
	{
		{	/* Expand/initial.scm 684 */
			{
				obj_t BgL_s1z00_6622;
				obj_t BgL_s2z00_6623;
				obj_t BgL_oz00_6624;

				if (PAIRP(BgL_xz00_5572))
					{	/* Expand/initial.scm 684 */
						obj_t BgL_cdrzd2911zd2_6629;

						BgL_cdrzd2911zd2_6629 = CDR(((obj_t) BgL_xz00_5572));
						if (PAIRP(BgL_cdrzd2911zd2_6629))
							{	/* Expand/initial.scm 684 */
								obj_t BgL_cdrzd2916zd2_6630;

								BgL_cdrzd2916zd2_6630 = CDR(BgL_cdrzd2911zd2_6629);
								if (PAIRP(BgL_cdrzd2916zd2_6630))
									{	/* Expand/initial.scm 684 */
										obj_t BgL_cdrzd2921zd2_6631;

										BgL_cdrzd2921zd2_6631 = CDR(BgL_cdrzd2916zd2_6630);
										if (PAIRP(BgL_cdrzd2921zd2_6631))
											{	/* Expand/initial.scm 684 */
												if (NULLP(CDR(BgL_cdrzd2921zd2_6631)))
													{	/* Expand/initial.scm 684 */
														BgL_s1z00_6622 = CAR(BgL_cdrzd2911zd2_6629);
														BgL_s2z00_6623 = CAR(BgL_cdrzd2916zd2_6630);
														BgL_oz00_6624 = CAR(BgL_cdrzd2921zd2_6631);
														{	/* Expand/initial.scm 687 */
															obj_t BgL_arg2596z00_6625;

															{	/* Expand/initial.scm 687 */
																obj_t BgL_arg2597z00_6626;

																{	/* Expand/initial.scm 687 */
																	obj_t BgL_arg2599z00_6627;

																	{	/* Expand/initial.scm 687 */
																		obj_t BgL_arg2600z00_6628;

																		BgL_arg2600z00_6628 =
																			MAKE_YOUNG_PAIR(BgL_oz00_6624, BNIL);
																		BgL_arg2599z00_6627 =
																			MAKE_YOUNG_PAIR(BgL_s2z00_6623,
																			BgL_arg2600z00_6628);
																	}
																	BgL_arg2597z00_6626 =
																		MAKE_YOUNG_PAIR(BgL_s1z00_6622,
																		BgL_arg2599z00_6627);
																}
																BgL_arg2596z00_6625 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(184),
																	BgL_arg2597z00_6626);
															}
															{	/* Expand/initial.scm 687 */
																obj_t BgL_tmpfunz00_9326;

																BgL_tmpfunz00_9326 = ((obj_t) BgL_ez00_5573);
																return
																	(VA_PROCEDUREP(BgL_tmpfunz00_9326)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9326)) ((
																			(obj_t) BgL_ez00_5573),
																		BgL_arg2596z00_6625,
																		((obj_t) BgL_ez00_5573),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9326)) ((
																			(obj_t) BgL_ez00_5573),
																		BgL_arg2596z00_6625,
																		((obj_t) BgL_ez00_5573)));
															}
														}
													}
												else
													{	/* Expand/initial.scm 684 */
														obj_t BgL_cdrzd2948zd2_6632;

														BgL_cdrzd2948zd2_6632 =
															CDR(((obj_t) BgL_cdrzd2911zd2_6629));
														{	/* Expand/initial.scm 684 */
															obj_t BgL_cdrzd2957zd2_6633;

															BgL_cdrzd2957zd2_6633 =
																CDR(((obj_t) BgL_cdrzd2948zd2_6632));
															{	/* Expand/initial.scm 684 */
																obj_t BgL_cdrzd2964zd2_6634;

																BgL_cdrzd2964zd2_6634 =
																	CDR(((obj_t) BgL_cdrzd2957zd2_6633));
																if (PAIRP(BgL_cdrzd2964zd2_6634))
																	{	/* Expand/initial.scm 684 */
																		if (NULLP(CDR(BgL_cdrzd2964zd2_6634)))
																			{	/* Expand/initial.scm 684 */
																				obj_t BgL_arg2589z00_6635;
																				obj_t BgL_arg2590z00_6636;
																				obj_t BgL_arg2591z00_6637;
																				obj_t BgL_arg2592z00_6638;

																				BgL_arg2589z00_6635 =
																					CAR(((obj_t) BgL_cdrzd2911zd2_6629));
																				BgL_arg2590z00_6636 =
																					CAR(((obj_t) BgL_cdrzd2948zd2_6632));
																				BgL_arg2591z00_6637 =
																					CAR(((obj_t) BgL_cdrzd2957zd2_6633));
																				BgL_arg2592z00_6638 =
																					CAR(BgL_cdrzd2964zd2_6634);
																				{	/* Expand/initial.scm 689 */
																					obj_t BgL_arg2601z00_6639;

																					{	/* Expand/initial.scm 689 */
																						obj_t BgL_arg2604z00_6640;

																						{	/* Expand/initial.scm 689 */
																							obj_t BgL_arg2605z00_6641;

																							{	/* Expand/initial.scm 689 */
																								obj_t BgL_arg2607z00_6642;

																								{	/* Expand/initial.scm 689 */
																									obj_t BgL_arg2608z00_6643;

																									BgL_arg2608z00_6643 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2592z00_6638, BNIL);
																									BgL_arg2607z00_6642 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2591z00_6637,
																										BgL_arg2608z00_6643);
																								}
																								BgL_arg2605z00_6641 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2590z00_6636,
																									BgL_arg2607z00_6642);
																							}
																							BgL_arg2604z00_6640 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2589z00_6635,
																								BgL_arg2605z00_6641);
																						}
																						BgL_arg2601z00_6639 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(185), BgL_arg2604z00_6640);
																					}
																					{	/* Expand/initial.scm 689 */
																						obj_t BgL_tmpfunz00_9361;

																						BgL_tmpfunz00_9361 =
																							((obj_t) BgL_ez00_5573);
																						return
																							(VA_PROCEDUREP(BgL_tmpfunz00_9361)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9361)) (((obj_t)
																									BgL_ez00_5573),
																								BgL_arg2601z00_6639,
																								((obj_t) BgL_ez00_5573),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9361)) (((obj_t)
																									BgL_ez00_5573),
																								BgL_arg2601z00_6639,
																								((obj_t) BgL_ez00_5573)));
																					}
																				}
																			}
																		else
																			{	/* Expand/initial.scm 684 */
																			BgL_tagzd2901zd2_6620:
																				return
																					BGl_errorz00zz__errorz00(BFALSE,
																					BGl_string4228z00zzexpand_installz00,
																					BgL_xz00_5572);
																			}
																	}
																else
																	{	/* Expand/initial.scm 684 */
																		goto BgL_tagzd2901zd2_6620;
																	}
															}
														}
													}
											}
										else
											{	/* Expand/initial.scm 684 */
												goto BgL_tagzd2901zd2_6620;
											}
									}
								else
									{	/* Expand/initial.scm 684 */
										goto BgL_tagzd2901zd2_6620;
									}
							}
						else
							{	/* Expand/initial.scm 684 */
								goto BgL_tagzd2901zd2_6620;
							}
					}
				else
					{	/* Expand/initial.scm 684 */
						goto BgL_tagzd2901zd2_6620;
					}
			}
		}

	}



/* &<@anonymous:2540> */
	obj_t BGl_z62zc3z04anonymousza32540ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5574, obj_t BgL_xz00_5575, obj_t BgL_ez00_5576)
	{
		{	/* Expand/initial.scm 669 */
			{
				obj_t BgL_sz00_6655;
				obj_t BgL_minz00_6656;
				obj_t BgL_maxz00_6657;

				if (PAIRP(BgL_xz00_5575))
					{	/* Expand/initial.scm 669 */
						obj_t BgL_cdrzd2883zd2_6667;

						BgL_cdrzd2883zd2_6667 = CDR(((obj_t) BgL_xz00_5575));
						if (PAIRP(BgL_cdrzd2883zd2_6667))
							{	/* Expand/initial.scm 669 */
								obj_t BgL_cdrzd2888zd2_6668;

								BgL_cdrzd2888zd2_6668 = CDR(BgL_cdrzd2883zd2_6667);
								if (PAIRP(BgL_cdrzd2888zd2_6668))
									{	/* Expand/initial.scm 669 */
										obj_t BgL_cdrzd2893zd2_6669;

										BgL_cdrzd2893zd2_6669 = CDR(BgL_cdrzd2888zd2_6668);
										if (PAIRP(BgL_cdrzd2893zd2_6669))
											{	/* Expand/initial.scm 669 */
												if (NULLP(CDR(BgL_cdrzd2893zd2_6669)))
													{	/* Expand/initial.scm 669 */
														BgL_sz00_6655 = CAR(BgL_cdrzd2883zd2_6667);
														BgL_minz00_6656 = CAR(BgL_cdrzd2888zd2_6668);
														BgL_maxz00_6657 = CAR(BgL_cdrzd2893zd2_6669);
														{	/* Expand/initial.scm 672 */
															obj_t BgL_sz00_6658;
															obj_t BgL_minz00_6659;
															obj_t BgL_maxz00_6660;

															{	/* Expand/initial.scm 672 */
																obj_t BgL_tmpfunz00_9385;

																BgL_tmpfunz00_9385 = ((obj_t) BgL_ez00_5576);
																BgL_sz00_6658 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_9385)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9385)) ((
																			(obj_t) BgL_ez00_5576), BgL_sz00_6655,
																		((obj_t) BgL_ez00_5576),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9385)) ((
																			(obj_t) BgL_ez00_5576), BgL_sz00_6655,
																		((obj_t) BgL_ez00_5576)));
															}
															{	/* Expand/initial.scm 673 */
																obj_t BgL_tmpfunz00_9393;

																BgL_tmpfunz00_9393 = ((obj_t) BgL_ez00_5576);
																BgL_minz00_6659 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_9393)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9393)) ((
																			(obj_t) BgL_ez00_5576), BgL_minz00_6656,
																		((obj_t) BgL_ez00_5576),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9393)) ((
																			(obj_t) BgL_ez00_5576), BgL_minz00_6656,
																		((obj_t) BgL_ez00_5576)));
															}
															{	/* Expand/initial.scm 674 */
																obj_t BgL_tmpfunz00_9401;

																BgL_tmpfunz00_9401 = ((obj_t) BgL_ez00_5576);
																BgL_maxz00_6660 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_9401)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9401)) ((
																			(obj_t) BgL_ez00_5576), BgL_maxz00_6657,
																		((obj_t) BgL_ez00_5576),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9401)) ((
																			(obj_t) BgL_ez00_5576), BgL_maxz00_6657,
																		((obj_t) BgL_ez00_5576)));
															}
															if (CBOOL
																(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
																{	/* Expand/initial.scm 676 */
																	obj_t BgL_arg2553z00_6661;

																	{	/* Expand/initial.scm 676 */
																		obj_t BgL_arg2554z00_6662;

																		{	/* Expand/initial.scm 676 */
																			obj_t BgL_arg2555z00_6663;

																			BgL_arg2555z00_6663 =
																				MAKE_YOUNG_PAIR(BgL_maxz00_6660, BNIL);
																			BgL_arg2554z00_6662 =
																				MAKE_YOUNG_PAIR(BgL_minz00_6659,
																				BgL_arg2555z00_6663);
																		}
																		BgL_arg2553z00_6661 =
																			MAKE_YOUNG_PAIR(BgL_sz00_6658,
																			BgL_arg2554z00_6662);
																	}
																	return
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(186),
																		BgL_arg2553z00_6661);
																}
															else
																{	/* Expand/initial.scm 677 */
																	obj_t BgL_arg2556z00_6664;

																	{	/* Expand/initial.scm 677 */
																		obj_t BgL_arg2557z00_6665;

																		{	/* Expand/initial.scm 677 */
																			obj_t BgL_arg2560z00_6666;

																			BgL_arg2560z00_6666 =
																				MAKE_YOUNG_PAIR(BgL_maxz00_6660, BNIL);
																			BgL_arg2557z00_6665 =
																				MAKE_YOUNG_PAIR(BgL_minz00_6659,
																				BgL_arg2560z00_6666);
																		}
																		BgL_arg2556z00_6664 =
																			MAKE_YOUNG_PAIR(BgL_sz00_6658,
																			BgL_arg2557z00_6665);
																	}
																	return
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(113),
																		BgL_arg2556z00_6664);
																}
														}
													}
												else
													{	/* Expand/initial.scm 669 */
													BgL_tagzd2874zd2_6645:
														if (NULLP(BgL_xz00_5575))
															{	/* Expand/initial.scm 679 */
																return BNIL;
															}
														else
															{	/* Expand/initial.scm 679 */
																obj_t BgL_head1328z00_6647;

																BgL_head1328z00_6647 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1326z00_6649;
																	obj_t BgL_tail1329z00_6650;

																	BgL_l1326z00_6649 = BgL_xz00_5575;
																	BgL_tail1329z00_6650 = BgL_head1328z00_6647;
																BgL_zc3z04anonymousza32562ze3z87_6648:
																	if (NULLP(BgL_l1326z00_6649))
																		{	/* Expand/initial.scm 679 */
																			return CDR(BgL_head1328z00_6647);
																		}
																	else
																		{	/* Expand/initial.scm 679 */
																			obj_t BgL_newtail1330z00_6651;

																			{	/* Expand/initial.scm 679 */
																				obj_t BgL_arg2565z00_6652;

																				{	/* Expand/initial.scm 679 */
																					obj_t BgL_xz00_6653;

																					BgL_xz00_6653 =
																						CAR(((obj_t) BgL_l1326z00_6649));
																					{	/* Expand/initial.scm 679 */
																						obj_t BgL_tmpfunz00_9432;

																						BgL_tmpfunz00_9432 =
																							((obj_t) BgL_ez00_5576);
																						BgL_arg2565z00_6652 =
																							(VA_PROCEDUREP(BgL_tmpfunz00_9432)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9432)) (((obj_t)
																									BgL_ez00_5576), BgL_xz00_6653,
																								((obj_t) BgL_ez00_5576),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9432)) (((obj_t)
																									BgL_ez00_5576), BgL_xz00_6653,
																								((obj_t) BgL_ez00_5576)));
																					}
																				}
																				BgL_newtail1330z00_6651 =
																					MAKE_YOUNG_PAIR(BgL_arg2565z00_6652,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1329z00_6650,
																				BgL_newtail1330z00_6651);
																			{	/* Expand/initial.scm 679 */
																				obj_t BgL_arg2564z00_6654;

																				BgL_arg2564z00_6654 =
																					CDR(((obj_t) BgL_l1326z00_6649));
																				{
																					obj_t BgL_tail1329z00_9439;
																					obj_t BgL_l1326z00_9438;

																					BgL_l1326z00_9438 =
																						BgL_arg2564z00_6654;
																					BgL_tail1329z00_9439 =
																						BgL_newtail1330z00_6651;
																					BgL_tail1329z00_6650 =
																						BgL_tail1329z00_9439;
																					BgL_l1326z00_6649 = BgL_l1326z00_9438;
																					goto
																						BgL_zc3z04anonymousza32562ze3z87_6648;
																				}
																			}
																		}
																}
															}
													}
											}
										else
											{	/* Expand/initial.scm 669 */
												goto BgL_tagzd2874zd2_6645;
											}
									}
								else
									{	/* Expand/initial.scm 669 */
										goto BgL_tagzd2874zd2_6645;
									}
							}
						else
							{	/* Expand/initial.scm 669 */
								goto BgL_tagzd2874zd2_6645;
							}
					}
				else
					{	/* Expand/initial.scm 669 */
						goto BgL_tagzd2874zd2_6645;
					}
			}
		}

	}



/* &<@anonymous:2538> */
	obj_t BGl_z62zc3z04anonymousza32538ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5577, obj_t BgL_xz00_5578, obj_t BgL_ez00_5579)
	{
		{	/* Expand/initial.scm 663 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5578,
				CNST_TABLE_REF(187), BGl_string4229z00zzexpand_installz00,
				((obj_t) BgL_ez00_5579));
		}

	}



/* &<@anonymous:2422> */
	obj_t BGl_z62zc3z04anonymousza32422ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5580, obj_t BgL_xz00_5581, obj_t BgL_ez00_5582)
	{
		{	/* Expand/initial.scm 649 */
			{
				obj_t BgL_sym1z00_6694;
				obj_t BgL_sym2z00_6695;
				obj_t BgL_sym1z00_6683;
				obj_t BgL_sym2z00_6684;

				if (PAIRP(BgL_xz00_5581))
					{	/* Expand/initial.scm 649 */
						obj_t BgL_cdrzd2694zd2_6703;

						BgL_cdrzd2694zd2_6703 = CDR(((obj_t) BgL_xz00_5581));
						if (PAIRP(BgL_cdrzd2694zd2_6703))
							{	/* Expand/initial.scm 649 */
								if (NULLP(CDR(BgL_cdrzd2694zd2_6703)))
									{	/* Expand/initial.scm 649 */
										obj_t BgL_arg2428z00_6704;

										BgL_arg2428z00_6704 = CAR(BgL_cdrzd2694zd2_6703);
										{	/* Expand/initial.scm 652 */
											obj_t BgL_tmpfunz00_9459;

											BgL_tmpfunz00_9459 = ((obj_t) BgL_ez00_5582);
											return
												(VA_PROCEDUREP(BgL_tmpfunz00_9459) ? ((obj_t(*)(obj_t,
															...))
													PROCEDURE_ENTRY(BgL_tmpfunz00_9459)) (((obj_t)
														BgL_ez00_5582), BgL_arg2428z00_6704,
													((obj_t) BgL_ez00_5582), BEOA) : ((obj_t(*)(obj_t,
															obj_t,
															obj_t))
													PROCEDURE_ENTRY(BgL_tmpfunz00_9459)) (((obj_t)
														BgL_ez00_5582), BgL_arg2428z00_6704,
													((obj_t) BgL_ez00_5582)));
										}
									}
								else
									{	/* Expand/initial.scm 649 */
										obj_t BgL_carzd2707zd2_6705;
										obj_t BgL_cdrzd2708zd2_6706;

										BgL_carzd2707zd2_6705 =
											CAR(((obj_t) BgL_cdrzd2694zd2_6703));
										BgL_cdrzd2708zd2_6706 =
											CDR(((obj_t) BgL_cdrzd2694zd2_6703));
										if (PAIRP(BgL_carzd2707zd2_6705))
											{	/* Expand/initial.scm 649 */
												obj_t BgL_cdrzd2711zd2_6707;

												BgL_cdrzd2711zd2_6707 = CDR(BgL_carzd2707zd2_6705);
												if ((CAR(BgL_carzd2707zd2_6705) == CNST_TABLE_REF(190)))
													{	/* Expand/initial.scm 649 */
														if (PAIRP(BgL_cdrzd2711zd2_6707))
															{	/* Expand/initial.scm 649 */
																if (NULLP(CDR(BgL_cdrzd2711zd2_6707)))
																	{	/* Expand/initial.scm 649 */
																		if (PAIRP(BgL_cdrzd2708zd2_6706))
																			{	/* Expand/initial.scm 649 */
																				obj_t BgL_carzd2717zd2_6708;

																				BgL_carzd2717zd2_6708 =
																					CAR(BgL_cdrzd2708zd2_6706);
																				if (PAIRP(BgL_carzd2717zd2_6708))
																					{	/* Expand/initial.scm 649 */
																						obj_t BgL_cdrzd2721zd2_6709;

																						BgL_cdrzd2721zd2_6709 =
																							CDR(BgL_carzd2717zd2_6708);
																						if (
																							(CAR(BgL_carzd2717zd2_6708) ==
																								CNST_TABLE_REF(190)))
																							{	/* Expand/initial.scm 649 */
																								if (PAIRP
																									(BgL_cdrzd2721zd2_6709))
																									{	/* Expand/initial.scm 649 */
																										if (NULLP(CDR
																												(BgL_cdrzd2721zd2_6709)))
																											{	/* Expand/initial.scm 649 */
																												if (NULLP(CDR
																														(BgL_cdrzd2708zd2_6706)))
																													{	/* Expand/initial.scm 649 */
																														BgL_sym1z00_6694 =
																															CAR
																															(BgL_cdrzd2711zd2_6707);
																														BgL_sym2z00_6695 =
																															CAR
																															(BgL_cdrzd2721zd2_6709);
																														{	/* Expand/initial.scm 654 */
																															obj_t
																																BgL_arg2510z00_6696;
																															{	/* Expand/initial.scm 654 */
																																obj_t
																																	BgL_arg2511z00_6697;
																																{	/* Expand/initial.scm 654 */
																																	obj_t
																																		BgL_arg2512z00_6698;
																																	{	/* Expand/initial.scm 654 */
																																		obj_t
																																			BgL_arg2513z00_6699;
																																		obj_t
																																			BgL_arg2514z00_6700;
																																		{	/* Expand/initial.scm 654 */
																																			obj_t
																																				BgL_arg1455z00_6701;
																																			BgL_arg1455z00_6701
																																				=
																																				SYMBOL_TO_STRING
																																				(((obj_t) BgL_sym1z00_6694));
																																			BgL_arg2513z00_6699
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg1455z00_6701);
																																		}
																																		{	/* Expand/initial.scm 654 */
																																			obj_t
																																				BgL_arg1455z00_6702;
																																			BgL_arg1455z00_6702
																																				=
																																				SYMBOL_TO_STRING
																																				(((obj_t) BgL_sym2z00_6695));
																																			BgL_arg2514z00_6700
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg1455z00_6702);
																																		}
																																		BgL_arg2512z00_6698
																																			=
																																			string_append
																																			(BgL_arg2513z00_6699,
																																			BgL_arg2514z00_6700);
																																	}
																																	BgL_arg2511z00_6697
																																		=
																																		bstring_to_symbol
																																		(BgL_arg2512z00_6698);
																																}
																																BgL_arg2510z00_6696
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2511z00_6697,
																																	BNIL);
																															}
																															return
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(190),
																																BgL_arg2510z00_6696);
																														}
																													}
																												else
																													{	/* Expand/initial.scm 649 */
																													BgL_tagzd2688zd2_6672:
																														if (NULLP
																															(BgL_xz00_5581))
																															{	/* Expand/initial.scm 660 */
																																return BNIL;
																															}
																														else
																															{	/* Expand/initial.scm 660 */
																																obj_t
																																	BgL_head1323z00_6675;
																																BgL_head1323z00_6675
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BNIL, BNIL);
																																{
																																	obj_t
																																		BgL_l1321z00_6677;
																																	obj_t
																																		BgL_tail1324z00_6678;
																																	BgL_l1321z00_6677
																																		=
																																		BgL_xz00_5581;
																																	BgL_tail1324z00_6678
																																		=
																																		BgL_head1323z00_6675;
																																BgL_zc3z04anonymousza32529ze3z87_6676:
																																	if (NULLP
																																		(BgL_l1321z00_6677))
																																		{	/* Expand/initial.scm 660 */
																																			return
																																				CDR
																																				(BgL_head1323z00_6675);
																																		}
																																	else
																																		{	/* Expand/initial.scm 660 */
																																			obj_t
																																				BgL_newtail1325z00_6679;
																																			{	/* Expand/initial.scm 660 */
																																				obj_t
																																					BgL_arg2536z00_6680;
																																				{	/* Expand/initial.scm 660 */
																																					obj_t
																																						BgL_xz00_6681;
																																					BgL_xz00_6681
																																						=
																																						CAR(
																																						((obj_t) BgL_l1321z00_6677));
																																					{	/* Expand/initial.scm 660 */
																																						obj_t
																																							BgL_tmpfunz00_9522;
																																						BgL_tmpfunz00_9522
																																							=
																																							(
																																							(obj_t)
																																							BgL_ez00_5582);
																																						BgL_arg2536z00_6680
																																							=
																																							(VA_PROCEDUREP
																																							(BgL_tmpfunz00_9522)
																																							?
																																							((obj_t(*)(obj_t, ...)) PROCEDURE_ENTRY(BgL_tmpfunz00_9522)) (((obj_t) BgL_ez00_5582), BgL_xz00_6681, ((obj_t) BgL_ez00_5582), BEOA) : ((obj_t(*)(obj_t, obj_t, obj_t)) PROCEDURE_ENTRY(BgL_tmpfunz00_9522)) (((obj_t) BgL_ez00_5582), BgL_xz00_6681, ((obj_t) BgL_ez00_5582)));
																																					}
																																				}
																																				BgL_newtail1325z00_6679
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2536z00_6680,
																																					BNIL);
																																			}
																																			SET_CDR
																																				(BgL_tail1324z00_6678,
																																				BgL_newtail1325z00_6679);
																																			{	/* Expand/initial.scm 660 */
																																				obj_t
																																					BgL_arg2534z00_6682;
																																				BgL_arg2534z00_6682
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_l1321z00_6677));
																																				{
																																					obj_t
																																						BgL_tail1324z00_9529;
																																					obj_t
																																						BgL_l1321z00_9528;
																																					BgL_l1321z00_9528
																																						=
																																						BgL_arg2534z00_6682;
																																					BgL_tail1324z00_9529
																																						=
																																						BgL_newtail1325z00_6679;
																																					BgL_tail1324z00_6678
																																						=
																																						BgL_tail1324z00_9529;
																																					BgL_l1321z00_6677
																																						=
																																						BgL_l1321z00_9528;
																																					goto
																																						BgL_zc3z04anonymousza32529ze3z87_6676;
																																				}
																																			}
																																		}
																																}
																															}
																													}
																											}
																										else
																											{	/* Expand/initial.scm 649 */
																												obj_t
																													BgL_cdrzd2735zd2_6710;
																												BgL_cdrzd2735zd2_6710 =
																													CDR(((obj_t)
																														BgL_xz00_5581));
																												{	/* Expand/initial.scm 649 */
																													obj_t
																														BgL_cdrzd2739zd2_6711;
																													BgL_cdrzd2739zd2_6711
																														=
																														CDR(((obj_t)
																															BgL_cdrzd2735zd2_6710));
																													if (NULLP(CDR(((obj_t)
																																	BgL_cdrzd2739zd2_6711))))
																														{	/* Expand/initial.scm 649 */
																															obj_t
																																BgL_arg2452z00_6712;
																															obj_t
																																BgL_arg2453z00_6713;
																															BgL_arg2452z00_6712
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2735zd2_6710));
																															BgL_arg2453z00_6713
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2739zd2_6711));
																															BgL_sym1z00_6683 =
																																BgL_arg2452z00_6712;
																															BgL_sym2z00_6684 =
																																BgL_arg2453z00_6713;
																														BgL_tagzd2687zd2_6673:
																															{	/* Expand/initial.scm 657 */
																																obj_t
																																	BgL_arg2515z00_6685;
																																{	/* Expand/initial.scm 657 */
																																	obj_t
																																		BgL_arg2516z00_6686;
																																	{	/* Expand/initial.scm 657 */
																																		obj_t
																																			BgL_arg2518z00_6687;
																																		{	/* Expand/initial.scm 657 */
																																			obj_t
																																				BgL_arg2519z00_6688;
																																			{	/* Expand/initial.scm 657 */
																																				obj_t
																																					BgL_arg2521z00_6689;
																																				obj_t
																																					BgL_arg2524z00_6690;
																																				{	/* Expand/initial.scm 657 */
																																					obj_t
																																						BgL_arg2525z00_6691;
																																					BgL_arg2525z00_6691
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_sym1z00_6683,
																																						BNIL);
																																					BgL_arg2521z00_6689
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(188),
																																						BgL_arg2525z00_6691);
																																				}
																																				{	/* Expand/initial.scm 657 */
																																					obj_t
																																						BgL_arg2526z00_6692;
																																					{	/* Expand/initial.scm 657 */
																																						obj_t
																																							BgL_arg2527z00_6693;
																																						BgL_arg2527z00_6693
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_sym2z00_6684,
																																							BNIL);
																																						BgL_arg2526z00_6692
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(188),
																																							BgL_arg2527z00_6693);
																																					}
																																					BgL_arg2524z00_6690
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2526z00_6692,
																																						BNIL);
																																				}
																																				BgL_arg2519z00_6688
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2521z00_6689,
																																					BgL_arg2524z00_6690);
																																			}
																																			BgL_arg2518z00_6687
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(111),
																																				BgL_arg2519z00_6688);
																																		}
																																		BgL_arg2516z00_6686
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2518z00_6687,
																																			BNIL);
																																	}
																																	BgL_arg2515z00_6685
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(189),
																																		BgL_arg2516z00_6686);
																																}
																																{	/* Expand/initial.scm 656 */
																																	obj_t
																																		BgL_tmpfunz00_9561;
																																	BgL_tmpfunz00_9561
																																		=
																																		((obj_t)
																																		BgL_ez00_5582);
																																	return
																																		(VA_PROCEDUREP
																																		(BgL_tmpfunz00_9561)
																																		? ((obj_t(*)
																																				(obj_t,
																																					...))
																																			PROCEDURE_ENTRY
																																			(BgL_tmpfunz00_9561))
																																		(((obj_t)
																																				BgL_ez00_5582),
																																			BgL_arg2515z00_6685,
																																			((obj_t)
																																				BgL_ez00_5582),
																																			BEOA)
																																		: ((obj_t(*)
																																				(obj_t,
																																					obj_t,
																																					obj_t))
																																			PROCEDURE_ENTRY
																																			(BgL_tmpfunz00_9561))
																																		(((obj_t)
																																				BgL_ez00_5582),
																																			BgL_arg2515z00_6685,
																																			((obj_t)
																																				BgL_ez00_5582)));
																																}
																															}
																														}
																													else
																														{	/* Expand/initial.scm 649 */
																															goto
																																BgL_tagzd2688zd2_6672;
																														}
																												}
																											}
																									}
																								else
																									{	/* Expand/initial.scm 649 */
																										obj_t BgL_cdrzd2750zd2_6714;

																										BgL_cdrzd2750zd2_6714 =
																											CDR(
																											((obj_t) BgL_xz00_5581));
																										{	/* Expand/initial.scm 649 */
																											obj_t
																												BgL_cdrzd2754zd2_6715;
																											BgL_cdrzd2754zd2_6715 =
																												CDR(((obj_t)
																													BgL_cdrzd2750zd2_6714));
																											if (NULLP(CDR(((obj_t)
																															BgL_cdrzd2754zd2_6715))))
																												{	/* Expand/initial.scm 649 */
																													obj_t
																														BgL_arg2459z00_6716;
																													obj_t
																														BgL_arg2460z00_6717;
																													BgL_arg2459z00_6716 =
																														CAR(((obj_t)
																															BgL_cdrzd2750zd2_6714));
																													BgL_arg2460z00_6717 =
																														CAR(((obj_t)
																															BgL_cdrzd2754zd2_6715));
																													{
																														obj_t
																															BgL_sym2z00_9576;
																														obj_t
																															BgL_sym1z00_9575;
																														BgL_sym1z00_9575 =
																															BgL_arg2459z00_6716;
																														BgL_sym2z00_9576 =
																															BgL_arg2460z00_6717;
																														BgL_sym2z00_6684 =
																															BgL_sym2z00_9576;
																														BgL_sym1z00_6683 =
																															BgL_sym1z00_9575;
																														goto
																															BgL_tagzd2687zd2_6673;
																													}
																												}
																											else
																												{	/* Expand/initial.scm 649 */
																													goto
																														BgL_tagzd2688zd2_6672;
																												}
																										}
																									}
																							}
																						else
																							{	/* Expand/initial.scm 649 */
																								obj_t BgL_cdrzd2765zd2_6718;

																								BgL_cdrzd2765zd2_6718 =
																									CDR(((obj_t) BgL_xz00_5581));
																								{	/* Expand/initial.scm 649 */
																									obj_t BgL_cdrzd2769zd2_6719;

																									BgL_cdrzd2769zd2_6719 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd2765zd2_6718));
																									if (NULLP(CDR(((obj_t)
																													BgL_cdrzd2769zd2_6719))))
																										{	/* Expand/initial.scm 649 */
																											obj_t BgL_arg2465z00_6720;
																											obj_t BgL_arg2466z00_6721;

																											BgL_arg2465z00_6720 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd2765zd2_6718));
																											BgL_arg2466z00_6721 =
																												CAR(((obj_t)
																													BgL_cdrzd2769zd2_6719));
																											{
																												obj_t BgL_sym2z00_9590;
																												obj_t BgL_sym1z00_9589;

																												BgL_sym1z00_9589 =
																													BgL_arg2465z00_6720;
																												BgL_sym2z00_9590 =
																													BgL_arg2466z00_6721;
																												BgL_sym2z00_6684 =
																													BgL_sym2z00_9590;
																												BgL_sym1z00_6683 =
																													BgL_sym1z00_9589;
																												goto
																													BgL_tagzd2687zd2_6673;
																											}
																										}
																									else
																										{	/* Expand/initial.scm 649 */
																											goto
																												BgL_tagzd2688zd2_6672;
																										}
																								}
																							}
																					}
																				else
																					{	/* Expand/initial.scm 649 */
																						obj_t BgL_cdrzd2780zd2_6722;

																						BgL_cdrzd2780zd2_6722 =
																							CDR(((obj_t) BgL_xz00_5581));
																						{	/* Expand/initial.scm 649 */
																							obj_t BgL_cdrzd2784zd2_6723;

																							BgL_cdrzd2784zd2_6723 =
																								CDR(
																								((obj_t)
																									BgL_cdrzd2780zd2_6722));
																							if (NULLP(CDR(((obj_t)
																											BgL_cdrzd2784zd2_6723))))
																								{	/* Expand/initial.scm 649 */
																									obj_t BgL_arg2473z00_6724;
																									obj_t BgL_arg2474z00_6725;

																									BgL_arg2473z00_6724 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd2780zd2_6722));
																									BgL_arg2474z00_6725 =
																										CAR(((obj_t)
																											BgL_cdrzd2784zd2_6723));
																									{
																										obj_t BgL_sym2z00_9604;
																										obj_t BgL_sym1z00_9603;

																										BgL_sym1z00_9603 =
																											BgL_arg2473z00_6724;
																										BgL_sym2z00_9604 =
																											BgL_arg2474z00_6725;
																										BgL_sym2z00_6684 =
																											BgL_sym2z00_9604;
																										BgL_sym1z00_6683 =
																											BgL_sym1z00_9603;
																										goto BgL_tagzd2687zd2_6673;
																									}
																								}
																							else
																								{	/* Expand/initial.scm 649 */
																									goto BgL_tagzd2688zd2_6672;
																								}
																						}
																					}
																			}
																		else
																			{	/* Expand/initial.scm 649 */
																				goto BgL_tagzd2688zd2_6672;
																			}
																	}
																else
																	{	/* Expand/initial.scm 649 */
																		obj_t BgL_cdrzd2799zd2_6726;

																		BgL_cdrzd2799zd2_6726 =
																			CDR(((obj_t) BgL_xz00_5581));
																		{	/* Expand/initial.scm 649 */
																			obj_t BgL_cdrzd2803zd2_6727;

																			BgL_cdrzd2803zd2_6727 =
																				CDR(((obj_t) BgL_cdrzd2799zd2_6726));
																			if (PAIRP(BgL_cdrzd2803zd2_6727))
																				{	/* Expand/initial.scm 649 */
																					if (NULLP(CDR(BgL_cdrzd2803zd2_6727)))
																						{	/* Expand/initial.scm 649 */
																							obj_t BgL_arg2481z00_6728;
																							obj_t BgL_arg2482z00_6729;

																							BgL_arg2481z00_6728 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd2799zd2_6726));
																							BgL_arg2482z00_6729 =
																								CAR(BgL_cdrzd2803zd2_6727);
																							{
																								obj_t BgL_sym2z00_9618;
																								obj_t BgL_sym1z00_9617;

																								BgL_sym1z00_9617 =
																									BgL_arg2481z00_6728;
																								BgL_sym2z00_9618 =
																									BgL_arg2482z00_6729;
																								BgL_sym2z00_6684 =
																									BgL_sym2z00_9618;
																								BgL_sym1z00_6683 =
																									BgL_sym1z00_9617;
																								goto BgL_tagzd2687zd2_6673;
																							}
																						}
																					else
																						{	/* Expand/initial.scm 649 */
																							goto BgL_tagzd2688zd2_6672;
																						}
																				}
																			else
																				{	/* Expand/initial.scm 649 */
																					goto BgL_tagzd2688zd2_6672;
																				}
																		}
																	}
															}
														else
															{	/* Expand/initial.scm 649 */
																obj_t BgL_cdrzd2814zd2_6730;

																BgL_cdrzd2814zd2_6730 =
																	CDR(((obj_t) BgL_xz00_5581));
																{	/* Expand/initial.scm 649 */
																	obj_t BgL_cdrzd2818zd2_6731;

																	BgL_cdrzd2818zd2_6731 =
																		CDR(((obj_t) BgL_cdrzd2814zd2_6730));
																	if (PAIRP(BgL_cdrzd2818zd2_6731))
																		{	/* Expand/initial.scm 649 */
																			if (NULLP(CDR(BgL_cdrzd2818zd2_6731)))
																				{	/* Expand/initial.scm 649 */
																					obj_t BgL_arg2488z00_6732;
																					obj_t BgL_arg2490z00_6733;

																					BgL_arg2488z00_6732 =
																						CAR(
																						((obj_t) BgL_cdrzd2814zd2_6730));
																					BgL_arg2490z00_6733 =
																						CAR(BgL_cdrzd2818zd2_6731);
																					{
																						obj_t BgL_sym2z00_9632;
																						obj_t BgL_sym1z00_9631;

																						BgL_sym1z00_9631 =
																							BgL_arg2488z00_6732;
																						BgL_sym2z00_9632 =
																							BgL_arg2490z00_6733;
																						BgL_sym2z00_6684 = BgL_sym2z00_9632;
																						BgL_sym1z00_6683 = BgL_sym1z00_9631;
																						goto BgL_tagzd2687zd2_6673;
																					}
																				}
																			else
																				{	/* Expand/initial.scm 649 */
																					goto BgL_tagzd2688zd2_6672;
																				}
																		}
																	else
																		{	/* Expand/initial.scm 649 */
																			goto BgL_tagzd2688zd2_6672;
																		}
																}
															}
													}
												else
													{	/* Expand/initial.scm 649 */
														obj_t BgL_cdrzd2829zd2_6734;

														BgL_cdrzd2829zd2_6734 =
															CDR(((obj_t) BgL_xz00_5581));
														{	/* Expand/initial.scm 649 */
															obj_t BgL_cdrzd2833zd2_6735;

															BgL_cdrzd2833zd2_6735 =
																CDR(((obj_t) BgL_cdrzd2829zd2_6734));
															if (PAIRP(BgL_cdrzd2833zd2_6735))
																{	/* Expand/initial.scm 649 */
																	if (NULLP(CDR(BgL_cdrzd2833zd2_6735)))
																		{	/* Expand/initial.scm 649 */
																			obj_t BgL_arg2495z00_6736;
																			obj_t BgL_arg2497z00_6737;

																			BgL_arg2495z00_6736 =
																				CAR(((obj_t) BgL_cdrzd2829zd2_6734));
																			BgL_arg2497z00_6737 =
																				CAR(BgL_cdrzd2833zd2_6735);
																			{
																				obj_t BgL_sym2z00_9646;
																				obj_t BgL_sym1z00_9645;

																				BgL_sym1z00_9645 = BgL_arg2495z00_6736;
																				BgL_sym2z00_9646 = BgL_arg2497z00_6737;
																				BgL_sym2z00_6684 = BgL_sym2z00_9646;
																				BgL_sym1z00_6683 = BgL_sym1z00_9645;
																				goto BgL_tagzd2687zd2_6673;
																			}
																		}
																	else
																		{	/* Expand/initial.scm 649 */
																			goto BgL_tagzd2688zd2_6672;
																		}
																}
															else
																{	/* Expand/initial.scm 649 */
																	goto BgL_tagzd2688zd2_6672;
																}
														}
													}
											}
										else
											{	/* Expand/initial.scm 649 */
												obj_t BgL_cdrzd2850zd2_6739;

												BgL_cdrzd2850zd2_6739 =
													CDR(((obj_t) BgL_cdrzd2694zd2_6703));
												if (PAIRP(BgL_cdrzd2850zd2_6739))
													{	/* Expand/initial.scm 649 */
														if (NULLP(CDR(BgL_cdrzd2850zd2_6739)))
															{	/* Expand/initial.scm 649 */
																obj_t BgL_arg2505z00_6740;
																obj_t BgL_arg2506z00_6741;

																BgL_arg2505z00_6740 =
																	CAR(((obj_t) BgL_cdrzd2694zd2_6703));
																BgL_arg2506z00_6741 =
																	CAR(BgL_cdrzd2850zd2_6739);
																{
																	obj_t BgL_sym2z00_9658;
																	obj_t BgL_sym1z00_9657;

																	BgL_sym1z00_9657 = BgL_arg2505z00_6740;
																	BgL_sym2z00_9658 = BgL_arg2506z00_6741;
																	BgL_sym2z00_6684 = BgL_sym2z00_9658;
																	BgL_sym1z00_6683 = BgL_sym1z00_9657;
																	goto BgL_tagzd2687zd2_6673;
																}
															}
														else
															{	/* Expand/initial.scm 649 */
																goto BgL_tagzd2688zd2_6672;
															}
													}
												else
													{	/* Expand/initial.scm 649 */
														goto BgL_tagzd2688zd2_6672;
													}
											}
									}
							}
						else
							{	/* Expand/initial.scm 649 */
								goto BgL_tagzd2688zd2_6672;
							}
					}
				else
					{	/* Expand/initial.scm 649 */
						goto BgL_tagzd2688zd2_6672;
					}
			}
		}

	}



/* &<@anonymous:2420> */
	obj_t BGl_z62zc3z04anonymousza32420ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5583, obj_t BgL_xz00_5584, obj_t BgL_ez00_5585)
	{
		{	/* Expand/initial.scm 643 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5584,
				CNST_TABLE_REF(191), BGl_string4230z00zzexpand_installz00,
				((obj_t) BgL_ez00_5585));
		}

	}



/* &<@anonymous:2379> */
	obj_t BGl_z62zc3z04anonymousza32379ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5586, obj_t BgL_xz00_5587, obj_t BgL_ez00_5588)
	{
		{	/* Expand/initial.scm 631 */
			{
				obj_t BgL_str1z00_6764;
				obj_t BgL_str2z00_6765;
				obj_t BgL_str1z00_6755;
				obj_t BgL_str2z00_6756;
				obj_t BgL_str3z00_6757;

				if (PAIRP(BgL_xz00_5587))
					{	/* Expand/initial.scm 631 */
						obj_t BgL_cdrzd2602zd2_6770;

						BgL_cdrzd2602zd2_6770 = CDR(((obj_t) BgL_xz00_5587));
						if (PAIRP(BgL_cdrzd2602zd2_6770))
							{	/* Expand/initial.scm 631 */
								if (NULLP(CDR(BgL_cdrzd2602zd2_6770)))
									{	/* Expand/initial.scm 631 */
										obj_t BgL_arg2384z00_6771;

										BgL_arg2384z00_6771 = CAR(BgL_cdrzd2602zd2_6770);
										{	/* Expand/initial.scm 634 */
											obj_t BgL_tmpfunz00_9678;

											BgL_tmpfunz00_9678 = ((obj_t) BgL_ez00_5588);
											return
												(VA_PROCEDUREP(BgL_tmpfunz00_9678) ? ((obj_t(*)(obj_t,
															...))
													PROCEDURE_ENTRY(BgL_tmpfunz00_9678)) (((obj_t)
														BgL_ez00_5588), BgL_arg2384z00_6771,
													((obj_t) BgL_ez00_5588), BEOA) : ((obj_t(*)(obj_t,
															obj_t,
															obj_t))
													PROCEDURE_ENTRY(BgL_tmpfunz00_9678)) (((obj_t)
														BgL_ez00_5588), BgL_arg2384z00_6771,
													((obj_t) BgL_ez00_5588)));
										}
									}
								else
									{	/* Expand/initial.scm 631 */
										obj_t BgL_cdrzd2619zd2_6772;

										BgL_cdrzd2619zd2_6772 =
											CDR(((obj_t) BgL_cdrzd2602zd2_6770));
										if (PAIRP(BgL_cdrzd2619zd2_6772))
											{	/* Expand/initial.scm 631 */
												if (NULLP(CDR(BgL_cdrzd2619zd2_6772)))
													{	/* Expand/initial.scm 631 */
														obj_t BgL_arg2388z00_6773;
														obj_t BgL_arg2389z00_6774;

														BgL_arg2388z00_6773 =
															CAR(((obj_t) BgL_cdrzd2602zd2_6770));
														BgL_arg2389z00_6774 = CAR(BgL_cdrzd2619zd2_6772);
														BgL_str1z00_6764 = BgL_arg2388z00_6773;
														BgL_str2z00_6765 = BgL_arg2389z00_6774;
														{	/* Expand/initial.scm 636 */
															obj_t BgL_arg2401z00_6766;

															{	/* Expand/initial.scm 636 */
																obj_t BgL_arg2402z00_6767;
																obj_t BgL_arg2403z00_6768;

																{	/* Expand/initial.scm 636 */
																	obj_t BgL_tmpfunz00_9696;

																	BgL_tmpfunz00_9696 = ((obj_t) BgL_ez00_5588);
																	BgL_arg2402z00_6767 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_9696)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_9696)) ((
																				(obj_t) BgL_ez00_5588),
																			BgL_str1z00_6764, ((obj_t) BgL_ez00_5588),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_9696)) ((
																				(obj_t) BgL_ez00_5588),
																			BgL_str1z00_6764,
																			((obj_t) BgL_ez00_5588)));
																}
																{	/* Expand/initial.scm 636 */
																	obj_t BgL_arg2404z00_6769;

																	{	/* Expand/initial.scm 636 */
																		obj_t BgL_tmpfunz00_9704;

																		BgL_tmpfunz00_9704 =
																			((obj_t) BgL_ez00_5588);
																		BgL_arg2404z00_6769 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_9704)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_9704)) ((
																					(obj_t) BgL_ez00_5588),
																				BgL_str2z00_6765,
																				((obj_t) BgL_ez00_5588),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_9704)) ((
																					(obj_t) BgL_ez00_5588),
																				BgL_str2z00_6765,
																				((obj_t) BgL_ez00_5588)));
																	}
																	BgL_arg2403z00_6768 =
																		MAKE_YOUNG_PAIR(BgL_arg2404z00_6769, BNIL);
																}
																BgL_arg2401z00_6766 =
																	MAKE_YOUNG_PAIR(BgL_arg2402z00_6767,
																	BgL_arg2403z00_6768);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(193),
																BgL_arg2401z00_6766);
														}
													}
												else
													{	/* Expand/initial.scm 631 */
														obj_t BgL_cdrzd2642zd2_6776;

														BgL_cdrzd2642zd2_6776 =
															CDR(((obj_t) BgL_cdrzd2602zd2_6770));
														{	/* Expand/initial.scm 631 */
															obj_t BgL_cdrzd2649zd2_6777;

															BgL_cdrzd2649zd2_6777 =
																CDR(((obj_t) BgL_cdrzd2642zd2_6776));
															if (PAIRP(BgL_cdrzd2649zd2_6777))
																{	/* Expand/initial.scm 631 */
																	if (NULLP(CDR(BgL_cdrzd2649zd2_6777)))
																		{	/* Expand/initial.scm 631 */
																			obj_t BgL_arg2393z00_6778;
																			obj_t BgL_arg2395z00_6779;
																			obj_t BgL_arg2396z00_6780;

																			BgL_arg2393z00_6778 =
																				CAR(((obj_t) BgL_cdrzd2602zd2_6770));
																			BgL_arg2395z00_6779 =
																				CAR(((obj_t) BgL_cdrzd2642zd2_6776));
																			BgL_arg2396z00_6780 =
																				CAR(BgL_cdrzd2649zd2_6777);
																			BgL_str1z00_6755 = BgL_arg2393z00_6778;
																			BgL_str2z00_6756 = BgL_arg2395z00_6779;
																			BgL_str3z00_6757 = BgL_arg2396z00_6780;
																			{	/* Expand/initial.scm 638 */
																				obj_t BgL_arg2405z00_6758;

																				{	/* Expand/initial.scm 638 */
																					obj_t BgL_arg2407z00_6759;
																					obj_t BgL_arg2408z00_6760;

																					{	/* Expand/initial.scm 638 */
																						obj_t BgL_tmpfunz00_9730;

																						BgL_tmpfunz00_9730 =
																							((obj_t) BgL_ez00_5588);
																						BgL_arg2407z00_6759 =
																							(VA_PROCEDUREP(BgL_tmpfunz00_9730)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9730)) (((obj_t)
																									BgL_ez00_5588),
																								BgL_str1z00_6755,
																								((obj_t) BgL_ez00_5588),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9730)) (((obj_t)
																									BgL_ez00_5588),
																								BgL_str1z00_6755,
																								((obj_t) BgL_ez00_5588)));
																					}
																					{	/* Expand/initial.scm 638 */
																						obj_t BgL_arg2410z00_6761;
																						obj_t BgL_arg2411z00_6762;

																						{	/* Expand/initial.scm 638 */
																							obj_t BgL_tmpfunz00_9738;

																							BgL_tmpfunz00_9738 =
																								((obj_t) BgL_ez00_5588);
																							BgL_arg2410z00_6761 =
																								(VA_PROCEDUREP
																								(BgL_tmpfunz00_9738)
																								? ((obj_t(*)(obj_t,
																											...))
																									PROCEDURE_ENTRY
																									(BgL_tmpfunz00_9738)) ((
																										(obj_t) BgL_ez00_5588),
																									BgL_str2z00_6756,
																									((obj_t) BgL_ez00_5588),
																									BEOA) : ((obj_t(*)(obj_t,
																											obj_t,
																											obj_t))
																									PROCEDURE_ENTRY
																									(BgL_tmpfunz00_9738)) ((
																										(obj_t) BgL_ez00_5588),
																									BgL_str2z00_6756,
																									((obj_t) BgL_ez00_5588)));
																						}
																						{	/* Expand/initial.scm 638 */
																							obj_t BgL_arg2412z00_6763;

																							{	/* Expand/initial.scm 638 */
																								obj_t BgL_tmpfunz00_9746;

																								BgL_tmpfunz00_9746 =
																									((obj_t) BgL_ez00_5588);
																								BgL_arg2412z00_6763 =
																									(VA_PROCEDUREP
																									(BgL_tmpfunz00_9746)
																									? ((obj_t(*)(obj_t,
																												...))
																										PROCEDURE_ENTRY
																										(BgL_tmpfunz00_9746)) ((
																											(obj_t) BgL_ez00_5588),
																										BgL_str3z00_6757,
																										((obj_t) BgL_ez00_5588),
																										BEOA) : ((obj_t(*)(obj_t,
																												obj_t,
																												obj_t))
																										PROCEDURE_ENTRY
																										(BgL_tmpfunz00_9746)) ((
																											(obj_t) BgL_ez00_5588),
																										BgL_str3z00_6757,
																										((obj_t) BgL_ez00_5588)));
																							}
																							BgL_arg2411z00_6762 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2412z00_6763, BNIL);
																						}
																						BgL_arg2408z00_6760 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2410z00_6761,
																							BgL_arg2411z00_6762);
																					}
																					BgL_arg2405z00_6758 =
																						MAKE_YOUNG_PAIR(BgL_arg2407z00_6759,
																						BgL_arg2408z00_6760);
																				}
																				return
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(192),
																					BgL_arg2405z00_6758);
																			}
																		}
																	else
																		{	/* Expand/initial.scm 631 */
																		BgL_tagzd2595zd2_6744:
																			if (NULLP(BgL_xz00_5587))
																				{	/* Expand/initial.scm 640 */
																					return BNIL;
																				}
																			else
																				{	/* Expand/initial.scm 640 */
																					obj_t BgL_head1318z00_6747;

																					BgL_head1318z00_6747 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1316z00_6749;
																						obj_t BgL_tail1319z00_6750;

																						BgL_l1316z00_6749 = BgL_xz00_5587;
																						BgL_tail1319z00_6750 =
																							BgL_head1318z00_6747;
																					BgL_zc3z04anonymousza32414ze3z87_6748:
																						if (NULLP
																							(BgL_l1316z00_6749))
																							{	/* Expand/initial.scm 640 */
																								return
																									CDR(BgL_head1318z00_6747);
																							}
																						else
																							{	/* Expand/initial.scm 640 */
																								obj_t BgL_newtail1320z00_6751;

																								{	/* Expand/initial.scm 640 */
																									obj_t BgL_arg2418z00_6752;

																									{	/* Expand/initial.scm 640 */
																										obj_t BgL_xz00_6753;

																										BgL_xz00_6753 =
																											CAR(
																											((obj_t)
																												BgL_l1316z00_6749));
																										{	/* Expand/initial.scm 640 */
																											obj_t BgL_tmpfunz00_9767;

																											BgL_tmpfunz00_9767 =
																												((obj_t) BgL_ez00_5588);
																											BgL_arg2418z00_6752 =
																												(VA_PROCEDUREP
																												(BgL_tmpfunz00_9767)
																												? ((obj_t(*)(obj_t,
																															...))
																													PROCEDURE_ENTRY
																													(BgL_tmpfunz00_9767))
																												(((obj_t)
																														BgL_ez00_5588),
																													BgL_xz00_6753,
																													((obj_t)
																														BgL_ez00_5588),
																													BEOA)
																												: ((obj_t(*)(obj_t,
																															obj_t,
																															obj_t))
																													PROCEDURE_ENTRY
																													(BgL_tmpfunz00_9767))
																												(((obj_t)
																														BgL_ez00_5588),
																													BgL_xz00_6753,
																													((obj_t)
																														BgL_ez00_5588)));
																										}
																									}
																									BgL_newtail1320z00_6751 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2418z00_6752, BNIL);
																								}
																								SET_CDR(BgL_tail1319z00_6750,
																									BgL_newtail1320z00_6751);
																								{	/* Expand/initial.scm 640 */
																									obj_t BgL_arg2417z00_6754;

																									BgL_arg2417z00_6754 =
																										CDR(
																										((obj_t)
																											BgL_l1316z00_6749));
																									{
																										obj_t BgL_tail1319z00_9774;
																										obj_t BgL_l1316z00_9773;

																										BgL_l1316z00_9773 =
																											BgL_arg2417z00_6754;
																										BgL_tail1319z00_9774 =
																											BgL_newtail1320z00_6751;
																										BgL_tail1319z00_6750 =
																											BgL_tail1319z00_9774;
																										BgL_l1316z00_6749 =
																											BgL_l1316z00_9773;
																										goto
																											BgL_zc3z04anonymousza32414ze3z87_6748;
																									}
																								}
																							}
																					}
																				}
																		}
																}
															else
																{	/* Expand/initial.scm 631 */
																	goto BgL_tagzd2595zd2_6744;
																}
														}
													}
											}
										else
											{	/* Expand/initial.scm 631 */
												goto BgL_tagzd2595zd2_6744;
											}
									}
							}
						else
							{	/* Expand/initial.scm 631 */
								goto BgL_tagzd2595zd2_6744;
							}
					}
				else
					{	/* Expand/initial.scm 631 */
						goto BgL_tagzd2595zd2_6744;
					}
			}
		}

	}



/* &evset! */
	obj_t BGl_z62evsetz12z70zzexpand_installz00(obj_t BgL_envz00_5589,
		obj_t BgL_xz00_5590, obj_t BgL_ez00_5591)
	{
		{	/* Expand/initial.scm 596 */
			{
				obj_t BgL_opz00_6784;
				obj_t BgL_vecz00_6785;
				obj_t BgL_kz00_6786;
				obj_t BgL_objz00_6787;

				if (PAIRP(BgL_xz00_5590))
					{	/* Expand/initial.scm 596 */
						obj_t BgL_cdrzd2575zd2_6804;

						BgL_cdrzd2575zd2_6804 = CDR(((obj_t) BgL_xz00_5590));
						if (PAIRP(BgL_cdrzd2575zd2_6804))
							{	/* Expand/initial.scm 596 */
								obj_t BgL_cdrzd2581zd2_6805;

								BgL_cdrzd2581zd2_6805 = CDR(BgL_cdrzd2575zd2_6804);
								if (PAIRP(BgL_cdrzd2581zd2_6805))
									{	/* Expand/initial.scm 596 */
										obj_t BgL_cdrzd2586zd2_6806;

										BgL_cdrzd2586zd2_6806 = CDR(BgL_cdrzd2581zd2_6805);
										if (PAIRP(BgL_cdrzd2586zd2_6806))
											{	/* Expand/initial.scm 596 */
												if (NULLP(CDR(BgL_cdrzd2586zd2_6806)))
													{	/* Expand/initial.scm 596 */
														obj_t BgL_arg2361z00_6807;
														obj_t BgL_arg2363z00_6808;
														obj_t BgL_arg2364z00_6809;
														obj_t BgL_arg2365z00_6810;

														BgL_arg2361z00_6807 = CAR(((obj_t) BgL_xz00_5590));
														BgL_arg2363z00_6808 = CAR(BgL_cdrzd2575zd2_6804);
														BgL_arg2364z00_6809 = CAR(BgL_cdrzd2581zd2_6805);
														BgL_arg2365z00_6810 = CAR(BgL_cdrzd2586zd2_6806);
														BgL_opz00_6784 = BgL_arg2361z00_6807;
														BgL_vecz00_6785 = BgL_arg2363z00_6808;
														BgL_kz00_6786 = BgL_arg2364z00_6809;
														BgL_objz00_6787 = BgL_arg2365z00_6810;
														{	/* Expand/initial.scm 599 */
															obj_t BgL_evecz00_6788;
															obj_t BgL_ekz00_6789;
															obj_t BgL_eobjz00_6790;

															{	/* Expand/initial.scm 599 */
																obj_t BgL_tmpfunz00_9801;

																BgL_tmpfunz00_9801 = ((obj_t) BgL_ez00_5591);
																BgL_evecz00_6788 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_9801)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9801)) ((
																			(obj_t) BgL_ez00_5591), BgL_vecz00_6785,
																		((obj_t) BgL_ez00_5591),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9801)) ((
																			(obj_t) BgL_ez00_5591), BgL_vecz00_6785,
																		((obj_t) BgL_ez00_5591)));
															}
															{	/* Expand/initial.scm 600 */
																obj_t BgL_tmpfunz00_9809;

																BgL_tmpfunz00_9809 = ((obj_t) BgL_ez00_5591);
																BgL_ekz00_6789 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_9809)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9809)) ((
																			(obj_t) BgL_ez00_5591), BgL_kz00_6786,
																		((obj_t) BgL_ez00_5591),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9809)) ((
																			(obj_t) BgL_ez00_5591), BgL_kz00_6786,
																		((obj_t) BgL_ez00_5591)));
															}
															{	/* Expand/initial.scm 601 */
																obj_t BgL_tmpfunz00_9817;

																BgL_tmpfunz00_9817 = ((obj_t) BgL_ez00_5591);
																BgL_eobjz00_6790 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_9817)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9817)) ((
																			(obj_t) BgL_ez00_5591), BgL_objz00_6787,
																		((obj_t) BgL_ez00_5591),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_9817)) ((
																			(obj_t) BgL_ez00_5591), BgL_objz00_6787,
																		((obj_t) BgL_ez00_5591)));
															}
															if (CBOOL
																(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
																{	/* Expand/initial.scm 603 */
																	obj_t BgL_arg2367z00_6791;
																	obj_t BgL_arg2368z00_6792;

																	{	/* Expand/initial.scm 603 */
																		obj_t BgL_arg2369z00_6793;

																		{	/* Expand/initial.scm 603 */
																			obj_t BgL_arg2370z00_6794;
																			obj_t BgL_arg2371z00_6795;

																			{	/* Expand/initial.scm 603 */
																				obj_t BgL_symbolz00_6796;

																				BgL_symbolz00_6796 =
																					CNST_TABLE_REF(194);
																				{	/* Expand/initial.scm 603 */
																					obj_t BgL_arg1455z00_6797;

																					BgL_arg1455z00_6797 =
																						SYMBOL_TO_STRING
																						(BgL_symbolz00_6796);
																					BgL_arg2370z00_6794 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1455z00_6797);
																				}
																			}
																			{	/* Expand/initial.scm 603 */
																				obj_t BgL_arg1455z00_6798;

																				BgL_arg1455z00_6798 =
																					SYMBOL_TO_STRING(
																					((obj_t) BgL_opz00_6784));
																				BgL_arg2371z00_6795 =
																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																					(BgL_arg1455z00_6798);
																			}
																			BgL_arg2369z00_6793 =
																				string_append(BgL_arg2370z00_6794,
																				BgL_arg2371z00_6795);
																		}
																		BgL_arg2367z00_6791 =
																			bstring_to_symbol(BgL_arg2369z00_6793);
																	}
																	{	/* Expand/initial.scm 603 */
																		obj_t BgL_arg2373z00_6799;

																		{	/* Expand/initial.scm 603 */
																			obj_t BgL_arg2374z00_6800;

																			BgL_arg2374z00_6800 =
																				MAKE_YOUNG_PAIR(BgL_eobjz00_6790, BNIL);
																			BgL_arg2373z00_6799 =
																				MAKE_YOUNG_PAIR(BgL_ekz00_6789,
																				BgL_arg2374z00_6800);
																		}
																		BgL_arg2368z00_6792 =
																			MAKE_YOUNG_PAIR(BgL_evecz00_6788,
																			BgL_arg2373z00_6799);
																	}
																	return
																		MAKE_YOUNG_PAIR(BgL_arg2367z00_6791,
																		BgL_arg2368z00_6792);
																}
															else
																{	/* Expand/initial.scm 604 */
																	obj_t BgL_arg2375z00_6801;

																	{	/* Expand/initial.scm 604 */
																		obj_t BgL_arg2376z00_6802;

																		{	/* Expand/initial.scm 604 */
																			obj_t BgL_arg2377z00_6803;

																			BgL_arg2377z00_6803 =
																				MAKE_YOUNG_PAIR(BgL_eobjz00_6790, BNIL);
																			BgL_arg2376z00_6802 =
																				MAKE_YOUNG_PAIR(BgL_ekz00_6789,
																				BgL_arg2377z00_6803);
																		}
																		BgL_arg2375z00_6801 =
																			MAKE_YOUNG_PAIR(BgL_evecz00_6788,
																			BgL_arg2376z00_6802);
																	}
																	return
																		MAKE_YOUNG_PAIR(BgL_opz00_6784,
																		BgL_arg2375z00_6801);
																}
														}
													}
												else
													{	/* Expand/initial.scm 596 */
													BgL_tagzd2564zd2_6782:
														return
															BGl_errorz00zz__errorz00(BFALSE,
															BGl_string4231z00zzexpand_installz00,
															BgL_xz00_5590);
													}
											}
										else
											{	/* Expand/initial.scm 596 */
												goto BgL_tagzd2564zd2_6782;
											}
									}
								else
									{	/* Expand/initial.scm 596 */
										goto BgL_tagzd2564zd2_6782;
									}
							}
						else
							{	/* Expand/initial.scm 596 */
								goto BgL_tagzd2564zd2_6782;
							}
					}
				else
					{	/* Expand/initial.scm 596 */
						goto BgL_tagzd2564zd2_6782;
					}
			}
		}

	}



/* &evref */
	obj_t BGl_z62evrefz62zzexpand_installz00(obj_t BgL_envz00_5592,
		obj_t BgL_xz00_5593, obj_t BgL_ez00_5594)
	{
		{	/* Expand/initial.scm 586 */
			{
				obj_t BgL_opz00_6814;
				obj_t BgL_vecz00_6815;
				obj_t BgL_kz00_6816;

				if (PAIRP(BgL_xz00_5593))
					{	/* Expand/initial.scm 586 */
						obj_t BgL_cdrzd2552zd2_6830;

						BgL_cdrzd2552zd2_6830 = CDR(((obj_t) BgL_xz00_5593));
						if (PAIRP(BgL_cdrzd2552zd2_6830))
							{	/* Expand/initial.scm 586 */
								obj_t BgL_cdrzd2557zd2_6831;

								BgL_cdrzd2557zd2_6831 = CDR(BgL_cdrzd2552zd2_6830);
								if (PAIRP(BgL_cdrzd2557zd2_6831))
									{	/* Expand/initial.scm 586 */
										if (NULLP(CDR(BgL_cdrzd2557zd2_6831)))
											{	/* Expand/initial.scm 586 */
												obj_t BgL_arg2338z00_6832;
												obj_t BgL_arg2339z00_6833;
												obj_t BgL_arg2340z00_6834;

												BgL_arg2338z00_6832 = CAR(((obj_t) BgL_xz00_5593));
												BgL_arg2339z00_6833 = CAR(BgL_cdrzd2552zd2_6830);
												BgL_arg2340z00_6834 = CAR(BgL_cdrzd2557zd2_6831);
												BgL_opz00_6814 = BgL_arg2338z00_6832;
												BgL_vecz00_6815 = BgL_arg2339z00_6833;
												BgL_kz00_6816 = BgL_arg2340z00_6834;
												{	/* Expand/initial.scm 589 */
													obj_t BgL_evecz00_6817;
													obj_t BgL_ekz00_6818;

													{	/* Expand/initial.scm 589 */
														obj_t BgL_tmpfunz00_9860;

														BgL_tmpfunz00_9860 = ((obj_t) BgL_ez00_5594);
														BgL_evecz00_6817 =
															(VA_PROCEDUREP(BgL_tmpfunz00_9860)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_9860)) (((obj_t)
																	BgL_ez00_5594), BgL_vecz00_6815,
																((obj_t) BgL_ez00_5594),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_9860)) (((obj_t)
																	BgL_ez00_5594), BgL_vecz00_6815,
																((obj_t) BgL_ez00_5594)));
													}
													{	/* Expand/initial.scm 590 */
														obj_t BgL_tmpfunz00_9868;

														BgL_tmpfunz00_9868 = ((obj_t) BgL_ez00_5594);
														BgL_ekz00_6818 =
															(VA_PROCEDUREP(BgL_tmpfunz00_9868)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_9868)) (((obj_t)
																	BgL_ez00_5594), BgL_kz00_6816,
																((obj_t) BgL_ez00_5594),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_9868)) (((obj_t)
																	BgL_ez00_5594), BgL_kz00_6816,
																((obj_t) BgL_ez00_5594)));
													}
													if (CBOOL
														(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
														{	/* Expand/initial.scm 592 */
															obj_t BgL_arg2342z00_6819;
															obj_t BgL_arg2345z00_6820;

															{	/* Expand/initial.scm 592 */
																obj_t BgL_arg2346z00_6821;

																{	/* Expand/initial.scm 592 */
																	obj_t BgL_arg2348z00_6822;
																	obj_t BgL_arg2349z00_6823;

																	{	/* Expand/initial.scm 592 */
																		obj_t BgL_symbolz00_6824;

																		BgL_symbolz00_6824 = CNST_TABLE_REF(194);
																		{	/* Expand/initial.scm 592 */
																			obj_t BgL_arg1455z00_6825;

																			BgL_arg1455z00_6825 =
																				SYMBOL_TO_STRING(BgL_symbolz00_6824);
																			BgL_arg2348z00_6822 =
																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																				(BgL_arg1455z00_6825);
																		}
																	}
																	{	/* Expand/initial.scm 592 */
																		obj_t BgL_arg1455z00_6826;

																		BgL_arg1455z00_6826 =
																			SYMBOL_TO_STRING(
																			((obj_t) BgL_opz00_6814));
																		BgL_arg2349z00_6823 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_6826);
																	}
																	BgL_arg2346z00_6821 =
																		string_append(BgL_arg2348z00_6822,
																		BgL_arg2349z00_6823);
																}
																BgL_arg2342z00_6819 =
																	bstring_to_symbol(BgL_arg2346z00_6821);
															}
															{	/* Expand/initial.scm 592 */
																obj_t BgL_arg2350z00_6827;

																BgL_arg2350z00_6827 =
																	MAKE_YOUNG_PAIR(BgL_ekz00_6818, BNIL);
																BgL_arg2345z00_6820 =
																	MAKE_YOUNG_PAIR(BgL_evecz00_6817,
																	BgL_arg2350z00_6827);
															}
															return
																MAKE_YOUNG_PAIR(BgL_arg2342z00_6819,
																BgL_arg2345z00_6820);
														}
													else
														{	/* Expand/initial.scm 593 */
															obj_t BgL_arg2351z00_6828;

															{	/* Expand/initial.scm 593 */
																obj_t BgL_arg2352z00_6829;

																BgL_arg2352z00_6829 =
																	MAKE_YOUNG_PAIR(BgL_ekz00_6818, BNIL);
																BgL_arg2351z00_6828 =
																	MAKE_YOUNG_PAIR(BgL_evecz00_6817,
																	BgL_arg2352z00_6829);
															}
															return
																MAKE_YOUNG_PAIR(BgL_opz00_6814,
																BgL_arg2351z00_6828);
														}
												}
											}
										else
											{	/* Expand/initial.scm 586 */
											BgL_tagzd2543zd2_6812:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4232z00zzexpand_installz00, BgL_xz00_5593);
											}
									}
								else
									{	/* Expand/initial.scm 586 */
										goto BgL_tagzd2543zd2_6812;
									}
							}
						else
							{	/* Expand/initial.scm 586 */
								goto BgL_tagzd2543zd2_6812;
							}
					}
				else
					{	/* Expand/initial.scm 586 */
						goto BgL_tagzd2543zd2_6812;
					}
			}
		}

	}



/* &<@anonymous:2316> */
	obj_t BGl_z62zc3z04anonymousza32316ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5595, obj_t BgL_xz00_5596, obj_t BgL_ez00_5597)
	{
		{	/* Expand/initial.scm 577 */
			{

				if (PAIRP(BgL_xz00_5596))
					{	/* Expand/initial.scm 577 */
						obj_t BgL_cdrzd2526zd2_6845;

						BgL_cdrzd2526zd2_6845 = CDR(((obj_t) BgL_xz00_5596));
						if (PAIRP(BgL_cdrzd2526zd2_6845))
							{	/* Expand/initial.scm 577 */
								obj_t BgL_cdrzd2531zd2_6846;

								BgL_cdrzd2531zd2_6846 = CDR(BgL_cdrzd2526zd2_6845);
								if (PAIRP(BgL_cdrzd2531zd2_6846))
									{	/* Expand/initial.scm 577 */
										obj_t BgL_cdrzd2536zd2_6847;

										BgL_cdrzd2536zd2_6847 = CDR(BgL_cdrzd2531zd2_6846);
										if (PAIRP(BgL_cdrzd2536zd2_6847))
											{	/* Expand/initial.scm 577 */
												if (NULLP(CDR(BgL_cdrzd2536zd2_6847)))
													{	/* Expand/initial.scm 577 */
														{	/* Expand/initial.scm 580 */
															obj_t BgL_objz00_6848;

															BgL_objz00_6848 = CNST_TABLE_REF(195);
															SET_CAR(BgL_xz00_5596, BgL_objz00_6848);
														}
														{	/* Expand/initial.scm 581 */
															obj_t BgL_tmpfunz00_9910;

															BgL_tmpfunz00_9910 = ((obj_t) BgL_ez00_5597);
															return
																(VA_PROCEDUREP(BgL_tmpfunz00_9910)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_9910)) (((obj_t)
																		BgL_ez00_5597), BgL_xz00_5596,
																	((obj_t) BgL_ez00_5597),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_9910)) (((obj_t)
																		BgL_ez00_5597), BgL_xz00_5596,
																	((obj_t) BgL_ez00_5597)));
														}
													}
												else
													{	/* Expand/initial.scm 577 */
													BgL_tagzd2517zd2_6836:
														if (NULLP(BgL_xz00_5596))
															{	/* Expand/initial.scm 583 */
																return BNIL;
															}
														else
															{	/* Expand/initial.scm 583 */
																obj_t BgL_head1313z00_6837;

																BgL_head1313z00_6837 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1311z00_6839;
																	obj_t BgL_tail1314z00_6840;

																	BgL_l1311z00_6839 = BgL_xz00_5596;
																	BgL_tail1314z00_6840 = BgL_head1313z00_6837;
																BgL_zc3z04anonymousza32328ze3z87_6838:
																	if (NULLP(BgL_l1311z00_6839))
																		{	/* Expand/initial.scm 583 */
																			return CDR(BgL_head1313z00_6837);
																		}
																	else
																		{	/* Expand/initial.scm 583 */
																			obj_t BgL_newtail1315z00_6841;

																			{	/* Expand/initial.scm 583 */
																				obj_t BgL_arg2331z00_6842;

																				{	/* Expand/initial.scm 583 */
																					obj_t BgL_xz00_6843;

																					BgL_xz00_6843 =
																						CAR(((obj_t) BgL_l1311z00_6839));
																					{	/* Expand/initial.scm 583 */
																						obj_t BgL_tmpfunz00_9926;

																						BgL_tmpfunz00_9926 =
																							((obj_t) BgL_ez00_5597);
																						BgL_arg2331z00_6842 =
																							(VA_PROCEDUREP(BgL_tmpfunz00_9926)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9926)) (((obj_t)
																									BgL_ez00_5597), BgL_xz00_6843,
																								((obj_t) BgL_ez00_5597),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_9926)) (((obj_t)
																									BgL_ez00_5597), BgL_xz00_6843,
																								((obj_t) BgL_ez00_5597)));
																					}
																				}
																				BgL_newtail1315z00_6841 =
																					MAKE_YOUNG_PAIR(BgL_arg2331z00_6842,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1314z00_6840,
																				BgL_newtail1315z00_6841);
																			{	/* Expand/initial.scm 583 */
																				obj_t BgL_arg2330z00_6844;

																				BgL_arg2330z00_6844 =
																					CDR(((obj_t) BgL_l1311z00_6839));
																				{
																					obj_t BgL_tail1314z00_9933;
																					obj_t BgL_l1311z00_9932;

																					BgL_l1311z00_9932 =
																						BgL_arg2330z00_6844;
																					BgL_tail1314z00_9933 =
																						BgL_newtail1315z00_6841;
																					BgL_tail1314z00_6840 =
																						BgL_tail1314z00_9933;
																					BgL_l1311z00_6839 = BgL_l1311z00_9932;
																					goto
																						BgL_zc3z04anonymousza32328ze3z87_6838;
																				}
																			}
																		}
																}
															}
													}
											}
										else
											{	/* Expand/initial.scm 577 */
												goto BgL_tagzd2517zd2_6836;
											}
									}
								else
									{	/* Expand/initial.scm 577 */
										goto BgL_tagzd2517zd2_6836;
									}
							}
						else
							{	/* Expand/initial.scm 577 */
								goto BgL_tagzd2517zd2_6836;
							}
					}
				else
					{	/* Expand/initial.scm 577 */
						goto BgL_tagzd2517zd2_6836;
					}
			}
		}

	}



/* &<@anonymous:2287> */
	obj_t BGl_z62zc3z04anonymousza32287ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5598, obj_t BgL_xz00_5599, obj_t BgL_ez00_5600)
	{
		{	/* Expand/initial.scm 565 */
			{
				obj_t BgL_nz00_6867;
				obj_t BgL_nz00_6861;
				obj_t BgL_initz00_6862;

				if (PAIRP(BgL_xz00_5599))
					{	/* Expand/initial.scm 565 */
						obj_t BgL_cdrzd2485zd2_6872;

						BgL_cdrzd2485zd2_6872 = CDR(((obj_t) BgL_xz00_5599));
						if (PAIRP(BgL_cdrzd2485zd2_6872))
							{	/* Expand/initial.scm 565 */
								if (NULLP(CDR(BgL_cdrzd2485zd2_6872)))
									{	/* Expand/initial.scm 565 */
										BgL_nz00_6867 = CAR(BgL_cdrzd2485zd2_6872);
										{	/* Expand/initial.scm 568 */
											obj_t BgL_arg2301z00_6868;

											{	/* Expand/initial.scm 568 */
												obj_t BgL_arg2302z00_6869;
												obj_t BgL_arg2304z00_6870;

												{	/* Expand/initial.scm 568 */
													obj_t BgL_tmpfunz00_9949;

													BgL_tmpfunz00_9949 = ((obj_t) BgL_ez00_5600);
													BgL_arg2302z00_6869 =
														(VA_PROCEDUREP(BgL_tmpfunz00_9949)
														? ((obj_t(*)(obj_t,
																	...))
															PROCEDURE_ENTRY(BgL_tmpfunz00_9949)) (((obj_t)
																BgL_ez00_5600), BgL_nz00_6867,
															((obj_t) BgL_ez00_5600), BEOA) : ((obj_t(*)(obj_t,
																	obj_t,
																	obj_t))
															PROCEDURE_ENTRY(BgL_tmpfunz00_9949)) (((obj_t)
																BgL_ez00_5600), BgL_nz00_6867,
															((obj_t) BgL_ez00_5600)));
												}
												{	/* Expand/initial.scm 568 */
													obj_t BgL_arg2305z00_6871;

													{	/* Expand/initial.scm 568 */
														obj_t BgL_tmpfunz00_9958;

														BgL_tmpfunz00_9958 = ((obj_t) BgL_ez00_5600);
														BgL_arg2305z00_6871 =
															(VA_PROCEDUREP(BgL_tmpfunz00_9958)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_9958)) (((obj_t)
																	BgL_ez00_5600), CNST_TABLE_REF(197),
																((obj_t) BgL_ez00_5600),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_9958)) (((obj_t)
																	BgL_ez00_5600), CNST_TABLE_REF(197),
																((obj_t) BgL_ez00_5600)));
													}
													BgL_arg2304z00_6870 =
														MAKE_YOUNG_PAIR(BgL_arg2305z00_6871, BNIL);
												}
												BgL_arg2301z00_6868 =
													MAKE_YOUNG_PAIR(BgL_arg2302z00_6869,
													BgL_arg2304z00_6870);
											}
											return
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(196),
												BgL_arg2301z00_6868);
										}
									}
								else
									{	/* Expand/initial.scm 565 */
										obj_t BgL_cdrzd2501zd2_6873;

										BgL_cdrzd2501zd2_6873 =
											CDR(((obj_t) BgL_cdrzd2485zd2_6872));
										if (PAIRP(BgL_cdrzd2501zd2_6873))
											{	/* Expand/initial.scm 565 */
												if (NULLP(CDR(BgL_cdrzd2501zd2_6873)))
													{	/* Expand/initial.scm 565 */
														obj_t BgL_arg2296z00_6874;
														obj_t BgL_arg2297z00_6875;

														BgL_arg2296z00_6874 =
															CAR(((obj_t) BgL_cdrzd2485zd2_6872));
														BgL_arg2297z00_6875 = CAR(BgL_cdrzd2501zd2_6873);
														BgL_nz00_6861 = BgL_arg2296z00_6874;
														BgL_initz00_6862 = BgL_arg2297z00_6875;
														{	/* Expand/initial.scm 570 */
															obj_t BgL_arg2306z00_6863;

															{	/* Expand/initial.scm 570 */
																obj_t BgL_arg2307z00_6864;
																obj_t BgL_arg2308z00_6865;

																{	/* Expand/initial.scm 570 */
																	obj_t BgL_tmpfunz00_9981;

																	BgL_tmpfunz00_9981 = ((obj_t) BgL_ez00_5600);
																	BgL_arg2307z00_6864 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_9981)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_9981)) ((
																				(obj_t) BgL_ez00_5600), BgL_nz00_6861,
																			((obj_t) BgL_ez00_5600),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_9981)) ((
																				(obj_t) BgL_ez00_5600), BgL_nz00_6861,
																			((obj_t) BgL_ez00_5600)));
																}
																{	/* Expand/initial.scm 570 */
																	obj_t BgL_arg2309z00_6866;

																	{	/* Expand/initial.scm 570 */
																		obj_t BgL_tmpfunz00_9989;

																		BgL_tmpfunz00_9989 =
																			((obj_t) BgL_ez00_5600);
																		BgL_arg2309z00_6866 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_9989)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_9989)) ((
																					(obj_t) BgL_ez00_5600),
																				BgL_initz00_6862,
																				((obj_t) BgL_ez00_5600),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_9989)) ((
																					(obj_t) BgL_ez00_5600),
																				BgL_initz00_6862,
																				((obj_t) BgL_ez00_5600)));
																	}
																	BgL_arg2308z00_6865 =
																		MAKE_YOUNG_PAIR(BgL_arg2309z00_6866, BNIL);
																}
																BgL_arg2306z00_6863 =
																	MAKE_YOUNG_PAIR(BgL_arg2307z00_6864,
																	BgL_arg2308z00_6865);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(196),
																BgL_arg2306z00_6863);
														}
													}
												else
													{	/* Expand/initial.scm 565 */
													BgL_tagzd2479zd2_6850:
														if (NULLP(BgL_xz00_5599))
															{	/* Expand/initial.scm 572 */
																return BNIL;
															}
														else
															{	/* Expand/initial.scm 572 */
																obj_t BgL_head1308z00_6853;

																BgL_head1308z00_6853 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1306z00_6855;
																	obj_t BgL_tail1309z00_6856;

																	BgL_l1306z00_6855 = BgL_xz00_5599;
																	BgL_tail1309z00_6856 = BgL_head1308z00_6853;
																BgL_zc3z04anonymousza32311ze3z87_6854:
																	if (NULLP(BgL_l1306z00_6855))
																		{	/* Expand/initial.scm 572 */
																			return CDR(BgL_head1308z00_6853);
																		}
																	else
																		{	/* Expand/initial.scm 572 */
																			obj_t BgL_newtail1310z00_6857;

																			{	/* Expand/initial.scm 572 */
																				obj_t BgL_arg2314z00_6858;

																				{	/* Expand/initial.scm 572 */
																					obj_t BgL_xz00_6859;

																					BgL_xz00_6859 =
																						CAR(((obj_t) BgL_l1306z00_6855));
																					{	/* Expand/initial.scm 572 */
																						obj_t BgL_tmpfunz00_10009;

																						BgL_tmpfunz00_10009 =
																							((obj_t) BgL_ez00_5600);
																						BgL_arg2314z00_6858 =
																							(VA_PROCEDUREP
																							(BgL_tmpfunz00_10009)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10009)) (((obj_t)
																									BgL_ez00_5600), BgL_xz00_6859,
																								((obj_t) BgL_ez00_5600),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10009)) (((obj_t)
																									BgL_ez00_5600), BgL_xz00_6859,
																								((obj_t) BgL_ez00_5600)));
																					}
																				}
																				BgL_newtail1310z00_6857 =
																					MAKE_YOUNG_PAIR(BgL_arg2314z00_6858,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1309z00_6856,
																				BgL_newtail1310z00_6857);
																			{	/* Expand/initial.scm 572 */
																				obj_t BgL_arg2313z00_6860;

																				BgL_arg2313z00_6860 =
																					CDR(((obj_t) BgL_l1306z00_6855));
																				{
																					obj_t BgL_tail1309z00_10016;
																					obj_t BgL_l1306z00_10015;

																					BgL_l1306z00_10015 =
																						BgL_arg2313z00_6860;
																					BgL_tail1309z00_10016 =
																						BgL_newtail1310z00_6857;
																					BgL_tail1309z00_6856 =
																						BgL_tail1309z00_10016;
																					BgL_l1306z00_6855 =
																						BgL_l1306z00_10015;
																					goto
																						BgL_zc3z04anonymousza32311ze3z87_6854;
																				}
																			}
																		}
																}
															}
													}
											}
										else
											{	/* Expand/initial.scm 565 */
												goto BgL_tagzd2479zd2_6850;
											}
									}
							}
						else
							{	/* Expand/initial.scm 565 */
								goto BgL_tagzd2479zd2_6850;
							}
					}
				else
					{	/* Expand/initial.scm 565 */
						goto BgL_tagzd2479zd2_6850;
					}
			}
		}

	}



/* &<@anonymous:2260> */
	obj_t BGl_z62zc3z04anonymousza32260ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5601, obj_t BgL_xz00_5602, obj_t BgL_ez00_5603)
	{
		{	/* Expand/initial.scm 538 */
			{	/* Expand/initial.scm 539 */
				obj_t BgL_argsz00_6877;
				obj_t BgL_vz00_6878;

				BgL_argsz00_6877 = CDR(((obj_t) BgL_xz00_5602));
				BgL_vz00_6878 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(198)));
				{	/* Expand/initial.scm 542 */
					obj_t BgL_arg2261z00_6879;

					{	/* Expand/initial.scm 542 */
						obj_t BgL_arg2262z00_6880;

						{	/* Expand/initial.scm 542 */
							obj_t BgL_arg2263z00_6881;
							obj_t BgL_arg2264z00_6882;

							{	/* Expand/initial.scm 542 */
								obj_t BgL_arg2265z00_6883;

								{	/* Expand/initial.scm 542 */
									obj_t BgL_arg2266z00_6884;

									{	/* Expand/initial.scm 542 */
										obj_t BgL_arg2267z00_6885;

										{	/* Expand/initial.scm 542 */
											obj_t BgL_arg2268z00_6886;

											{	/* Expand/initial.scm 542 */
												long BgL_arg2269z00_6887;

												BgL_arg2269z00_6887 = bgl_list_length(BgL_argsz00_6877);
												BgL_arg2268z00_6886 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg2269z00_6887), BNIL);
											}
											BgL_arg2267z00_6885 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(199),
												BgL_arg2268z00_6886);
										}
										BgL_arg2266z00_6884 =
											MAKE_YOUNG_PAIR(BgL_arg2267z00_6885, BNIL);
									}
									BgL_arg2265z00_6883 =
										MAKE_YOUNG_PAIR(BgL_vz00_6878, BgL_arg2266z00_6884);
								}
								BgL_arg2263z00_6881 =
									MAKE_YOUNG_PAIR(BgL_arg2265z00_6883, BNIL);
							}
							{	/* Expand/initial.scm 543 */
								obj_t BgL_arg2270z00_6888;
								obj_t BgL_arg2271z00_6889;

								{
									long BgL_iz00_6891;
									obj_t BgL_argsz00_6892;
									obj_t BgL_resz00_6893;

									BgL_iz00_6891 = 0L;
									BgL_argsz00_6892 = BgL_argsz00_6877;
									BgL_resz00_6893 = BNIL;
								BgL_loopz00_6890:
									if (NULLP(BgL_argsz00_6892))
										{	/* Expand/initial.scm 546 */
											BgL_arg2270z00_6888 = bgl_reverse_bang(BgL_resz00_6893);
										}
									else
										{	/* Expand/initial.scm 548 */
											long BgL_arg2274z00_6894;
											obj_t BgL_arg2275z00_6895;
											obj_t BgL_arg2276z00_6896;

											BgL_arg2274z00_6894 = (BgL_iz00_6891 + 1L);
											BgL_arg2275z00_6895 = CDR(((obj_t) BgL_argsz00_6892));
											{	/* Expand/initial.scm 552 */
												obj_t BgL_arg2277z00_6897;

												{	/* Expand/initial.scm 552 */
													obj_t BgL_arg2279z00_6898;

													{	/* Expand/initial.scm 552 */
														obj_t BgL_arg2280z00_6899;

														{	/* Expand/initial.scm 552 */
															obj_t BgL_arg2281z00_6900;

															{	/* Expand/initial.scm 552 */
																obj_t BgL_arg2282z00_6901;

																{	/* Expand/initial.scm 552 */
																	obj_t BgL_arg2283z00_6902;

																	BgL_arg2283z00_6902 =
																		CAR(((obj_t) BgL_argsz00_6892));
																	BgL_arg2282z00_6901 =
																		MAKE_YOUNG_PAIR(BgL_arg2283z00_6902, BNIL);
																}
																BgL_arg2281z00_6900 =
																	MAKE_YOUNG_PAIR(BINT(BgL_iz00_6891),
																	BgL_arg2282z00_6901);
															}
															BgL_arg2280z00_6899 =
																MAKE_YOUNG_PAIR(BgL_vz00_6878,
																BgL_arg2281z00_6900);
														}
														BgL_arg2279z00_6898 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(200),
															BgL_arg2280z00_6899);
													}
													BgL_arg2277z00_6897 =
														BGl_epairifyz00zztools_miscz00(BgL_arg2279z00_6898,
														BgL_xz00_5602);
												}
												BgL_arg2276z00_6896 =
													MAKE_YOUNG_PAIR(BgL_arg2277z00_6897, BgL_resz00_6893);
											}
											{
												obj_t BgL_resz00_10048;
												obj_t BgL_argsz00_10047;
												long BgL_iz00_10046;

												BgL_iz00_10046 = BgL_arg2274z00_6894;
												BgL_argsz00_10047 = BgL_arg2275z00_6895;
												BgL_resz00_10048 = BgL_arg2276z00_6896;
												BgL_resz00_6893 = BgL_resz00_10048;
												BgL_argsz00_6892 = BgL_argsz00_10047;
												BgL_iz00_6891 = BgL_iz00_10046;
												goto BgL_loopz00_6890;
											}
										}
								}
								BgL_arg2271z00_6889 = MAKE_YOUNG_PAIR(BgL_vz00_6878, BNIL);
								BgL_arg2264z00_6882 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg2270z00_6888, BgL_arg2271z00_6889);
							}
							BgL_arg2262z00_6880 =
								MAKE_YOUNG_PAIR(BgL_arg2263z00_6881, BgL_arg2264z00_6882);
						}
						BgL_arg2261z00_6879 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(78), BgL_arg2262z00_6880);
					}
					{	/* Expand/initial.scm 542 */
						obj_t BgL_tmpfunz00_10060;

						BgL_tmpfunz00_10060 = ((obj_t) BgL_ez00_5603);
						return
							(VA_PROCEDUREP(BgL_tmpfunz00_10060) ? ((obj_t(*)(obj_t,
										...)) PROCEDURE_ENTRY(BgL_tmpfunz00_10060)) (((obj_t)
									BgL_ez00_5603), BgL_arg2261z00_6879, ((obj_t) BgL_ez00_5603),
								BEOA) : ((obj_t(*)(obj_t, obj_t,
										obj_t)) PROCEDURE_ENTRY(BgL_tmpfunz00_10060)) (((obj_t)
									BgL_ez00_5603), BgL_arg2261z00_6879,
								((obj_t) BgL_ez00_5603)));
					}
				}
			}
		}

	}



/* &<@anonymous:2208> */
	obj_t BGl_z62zc3z04anonymousza32208ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5604, obj_t BgL_xz00_5605, obj_t BgL_ez00_5606)
	{
		{	/* Expand/initial.scm 522 */
			{
				obj_t BgL_gz00_6906;
				obj_t BgL_portz00_6907;
				obj_t BgL_optsz00_6908;

				if (PAIRP(BgL_xz00_5605))
					{	/* Expand/initial.scm 522 */
						obj_t BgL_cdrzd2464zd2_6957;

						BgL_cdrzd2464zd2_6957 = CDR(((obj_t) BgL_xz00_5605));
						if (PAIRP(BgL_cdrzd2464zd2_6957))
							{	/* Expand/initial.scm 522 */
								obj_t BgL_cdrzd2469zd2_6958;

								BgL_cdrzd2469zd2_6958 = CDR(BgL_cdrzd2464zd2_6957);
								if (PAIRP(BgL_cdrzd2469zd2_6958))
									{	/* Expand/initial.scm 522 */
										BgL_gz00_6906 = CAR(BgL_cdrzd2464zd2_6957);
										BgL_portz00_6907 = CAR(BgL_cdrzd2469zd2_6958);
										BgL_optsz00_6908 = CDR(BgL_cdrzd2469zd2_6958);
										{	/* Expand/initial.scm 525 */
											bool_t BgL_test4628z00_10071;

											{	/* Expand/initial.scm 525 */
												int BgL_arg2258z00_6909;

												BgL_arg2258z00_6909 =
													BGl_bigloozd2compilerzd2debugz00zz__paramz00();
												BgL_test4628z00_10071 =
													((long) (BgL_arg2258z00_6909) >= 2L);
											}
											if (BgL_test4628z00_10071)
												{	/* Expand/initial.scm 526 */
													obj_t BgL_grz00_6910;

													{	/* Expand/initial.scm 526 */

														{	/* Expand/initial.scm 526 */

															BgL_grz00_6910 =
																BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
														}
													}
													{	/* Expand/initial.scm 527 */
														obj_t BgL_arg2217z00_6911;

														{	/* Expand/initial.scm 527 */
															obj_t BgL_arg2218z00_6912;
															obj_t BgL_arg2219z00_6913;

															{	/* Expand/initial.scm 527 */
																obj_t BgL_arg2220z00_6914;

																{	/* Expand/initial.scm 527 */
																	obj_t BgL_arg2221z00_6915;

																	{	/* Expand/initial.scm 527 */
																		obj_t BgL_arg2222z00_6916;

																		{	/* Expand/initial.scm 527 */
																			obj_t BgL_tmpfunz00_10082;

																			BgL_tmpfunz00_10082 =
																				((obj_t) BgL_ez00_5606);
																			BgL_arg2222z00_6916 =
																				(VA_PROCEDUREP(BgL_tmpfunz00_10082)
																				? ((obj_t(*)(obj_t,
																							...))
																					PROCEDURE_ENTRY(BgL_tmpfunz00_10082))
																				(((obj_t) BgL_ez00_5606), BgL_gz00_6906,
																					((obj_t) BgL_ez00_5606),
																					BEOA) : ((obj_t(*)(obj_t, obj_t,
																							obj_t))
																					PROCEDURE_ENTRY(BgL_tmpfunz00_10082))
																				(((obj_t) BgL_ez00_5606), BgL_gz00_6906,
																					((obj_t) BgL_ez00_5606)));
																		}
																		BgL_arg2221z00_6915 =
																			MAKE_YOUNG_PAIR(BgL_arg2222z00_6916,
																			BNIL);
																	}
																	BgL_arg2220z00_6914 =
																		MAKE_YOUNG_PAIR(BgL_grz00_6910,
																		BgL_arg2221z00_6915);
																}
																BgL_arg2218z00_6912 =
																	MAKE_YOUNG_PAIR(BgL_arg2220z00_6914, BNIL);
															}
															{	/* Expand/initial.scm 528 */
																obj_t BgL_arg2223z00_6917;

																{	/* Expand/initial.scm 528 */
																	obj_t BgL_arg2224z00_6918;

																	{	/* Expand/initial.scm 528 */
																		obj_t BgL_arg2225z00_6919;
																		obj_t BgL_arg2226z00_6920;

																		{	/* Expand/initial.scm 528 */
																			obj_t BgL_arg2227z00_6921;

																			{	/* Expand/initial.scm 528 */
																				obj_t BgL_arg2228z00_6922;

																				{	/* Expand/initial.scm 528 */
																					long BgL_arg2229z00_6923;

																					BgL_arg2229z00_6923 =
																						(1L +
																						bgl_list_length(BgL_optsz00_6908));
																					BgL_arg2228z00_6922 =
																						MAKE_YOUNG_PAIR(BINT
																						(BgL_arg2229z00_6923), BNIL);
																				}
																				BgL_arg2227z00_6921 =
																					MAKE_YOUNG_PAIR(BgL_grz00_6910,
																					BgL_arg2228z00_6922);
																			}
																			BgL_arg2225z00_6919 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(201),
																				BgL_arg2227z00_6921);
																		}
																		{	/* Expand/initial.scm 529 */
																			obj_t BgL_arg2231z00_6924;
																			obj_t BgL_arg2232z00_6925;

																			{	/* Expand/initial.scm 529 */
																				obj_t BgL_arg2233z00_6926;

																				{	/* Expand/initial.scm 529 */
																					obj_t BgL_arg2234z00_6927;
																					obj_t BgL_arg2235z00_6928;

																					{	/* Expand/initial.scm 529 */
																						obj_t BgL_tmpfunz00_10100;

																						BgL_tmpfunz00_10100 =
																							((obj_t) BgL_ez00_5606);
																						BgL_arg2234z00_6927 =
																							(VA_PROCEDUREP
																							(BgL_tmpfunz00_10100)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10100)) (((obj_t)
																									BgL_ez00_5606),
																								BgL_portz00_6907,
																								((obj_t) BgL_ez00_5606),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10100)) (((obj_t)
																									BgL_ez00_5606),
																								BgL_portz00_6907,
																								((obj_t) BgL_ez00_5606)));
																					}
																					{	/* Expand/initial.scm 529 */
																						obj_t BgL_arg2236z00_6929;

																						if (NULLP(BgL_optsz00_6908))
																							{	/* Expand/initial.scm 529 */
																								BgL_arg2236z00_6929 = BNIL;
																							}
																						else
																							{	/* Expand/initial.scm 529 */
																								obj_t BgL_head1298z00_6930;

																								BgL_head1298z00_6930 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1296z00_6932;
																									obj_t BgL_tail1299z00_6933;

																									BgL_l1296z00_6932 =
																										BgL_optsz00_6908;
																									BgL_tail1299z00_6933 =
																										BgL_head1298z00_6930;
																								BgL_zc3z04anonymousza32238ze3z87_6931:
																									if (NULLP
																										(BgL_l1296z00_6932))
																										{	/* Expand/initial.scm 529 */
																											BgL_arg2236z00_6929 =
																												CDR
																												(BgL_head1298z00_6930);
																										}
																									else
																										{	/* Expand/initial.scm 529 */
																											obj_t
																												BgL_newtail1300z00_6934;
																											{	/* Expand/initial.scm 529 */
																												obj_t
																													BgL_arg2241z00_6935;
																												{	/* Expand/initial.scm 529 */
																													obj_t BgL_xz00_6936;

																													BgL_xz00_6936 =
																														CAR(
																														((obj_t)
																															BgL_l1296z00_6932));
																													{	/* Expand/initial.scm 529 */
																														obj_t
																															BgL_tmpfunz00_10116;
																														BgL_tmpfunz00_10116
																															=
																															((obj_t)
																															BgL_ez00_5606);
																														BgL_arg2241z00_6935
																															=
																															(VA_PROCEDUREP
																															(BgL_tmpfunz00_10116)
																															? ((obj_t(*)
																																	(obj_t,
																																		...))
																																PROCEDURE_ENTRY
																																(BgL_tmpfunz00_10116))
																															(((obj_t)
																																	BgL_ez00_5606),
																																BgL_xz00_6936,
																																((obj_t)
																																	BgL_ez00_5606),
																																BEOA)
																															: ((obj_t(*)
																																	(obj_t, obj_t,
																																		obj_t))
																																PROCEDURE_ENTRY
																																(BgL_tmpfunz00_10116))
																															(((obj_t)
																																	BgL_ez00_5606),
																																BgL_xz00_6936,
																																((obj_t)
																																	BgL_ez00_5606)));
																													}
																												}
																												BgL_newtail1300z00_6934
																													=
																													MAKE_YOUNG_PAIR
																													(BgL_arg2241z00_6935,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1299z00_6933,
																												BgL_newtail1300z00_6934);
																											{	/* Expand/initial.scm 529 */
																												obj_t
																													BgL_arg2240z00_6937;
																												BgL_arg2240z00_6937 =
																													CDR(((obj_t)
																														BgL_l1296z00_6932));
																												{
																													obj_t
																														BgL_tail1299z00_10123;
																													obj_t
																														BgL_l1296z00_10122;
																													BgL_l1296z00_10122 =
																														BgL_arg2240z00_6937;
																													BgL_tail1299z00_10123
																														=
																														BgL_newtail1300z00_6934;
																													BgL_tail1299z00_6933 =
																														BgL_tail1299z00_10123;
																													BgL_l1296z00_6932 =
																														BgL_l1296z00_10122;
																													goto
																														BgL_zc3z04anonymousza32238ze3z87_6931;
																												}
																											}
																										}
																								}
																							}
																						BgL_arg2235z00_6928 =
																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_arg2236z00_6929, BNIL);
																					}
																					BgL_arg2233z00_6926 =
																						MAKE_YOUNG_PAIR(BgL_arg2234z00_6927,
																						BgL_arg2235z00_6928);
																				}
																				BgL_arg2231z00_6924 =
																					MAKE_YOUNG_PAIR(BgL_grz00_6910,
																					BgL_arg2233z00_6926);
																			}
																			{	/* Expand/initial.scm 530 */
																				obj_t BgL_arg2242z00_6938;

																				{	/* Expand/initial.scm 530 */
																					obj_t BgL_arg2243z00_6939;

																					{	/* Expand/initial.scm 530 */
																						obj_t BgL_arg2244z00_6940;
																						obj_t BgL_arg2245z00_6941;

																						{	/* Expand/initial.scm 530 */
																							obj_t BgL_arg2246z00_6942;

																							BgL_arg2246z00_6942 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(84), BNIL);
																							BgL_arg2244z00_6940 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(190), BgL_arg2246z00_6942);
																						}
																						{	/* Expand/initial.scm 530 */
																							obj_t BgL_arg2247z00_6943;

																							BgL_arg2247z00_6943 =
																								MAKE_YOUNG_PAIR(BgL_grz00_6910,
																								BNIL);
																							BgL_arg2245z00_6941 =
																								MAKE_YOUNG_PAIR
																								(BGl_string4234z00zzexpand_installz00,
																								BgL_arg2247z00_6943);
																						}
																						BgL_arg2243z00_6939 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2244z00_6940,
																							BgL_arg2245z00_6941);
																					}
																					BgL_arg2242z00_6938 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																						BgL_arg2243z00_6939);
																				}
																				BgL_arg2232z00_6925 =
																					MAKE_YOUNG_PAIR(BgL_arg2242z00_6938,
																					BNIL);
																			}
																			BgL_arg2226z00_6920 =
																				MAKE_YOUNG_PAIR(BgL_arg2231z00_6924,
																				BgL_arg2232z00_6925);
																		}
																		BgL_arg2224z00_6918 =
																			MAKE_YOUNG_PAIR(BgL_arg2225z00_6919,
																			BgL_arg2226z00_6920);
																	}
																	BgL_arg2223z00_6917 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																		BgL_arg2224z00_6918);
																}
																BgL_arg2219z00_6913 =
																	MAKE_YOUNG_PAIR(BgL_arg2223z00_6917, BNIL);
															}
															BgL_arg2217z00_6911 =
																MAKE_YOUNG_PAIR(BgL_arg2218z00_6912,
																BgL_arg2219z00_6913);
														}
														return
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(78),
															BgL_arg2217z00_6911);
													}
												}
											else
												{	/* Expand/initial.scm 531 */
													obj_t BgL_arg2248z00_6944;
													obj_t BgL_arg2249z00_6945;

													{	/* Expand/initial.scm 531 */
														obj_t BgL_tmpfunz00_10151;

														BgL_tmpfunz00_10151 = ((obj_t) BgL_ez00_5606);
														BgL_arg2248z00_6944 =
															(VA_PROCEDUREP(BgL_tmpfunz00_10151)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10151)) (((obj_t)
																	BgL_ez00_5606), BgL_gz00_6906,
																((obj_t) BgL_ez00_5606),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10151)) (((obj_t)
																	BgL_ez00_5606), BgL_gz00_6906,
																((obj_t) BgL_ez00_5606)));
													}
													{	/* Expand/initial.scm 531 */
														obj_t BgL_arg2250z00_6946;
														obj_t BgL_arg2251z00_6947;

														{	/* Expand/initial.scm 531 */
															obj_t BgL_tmpfunz00_10159;

															BgL_tmpfunz00_10159 = ((obj_t) BgL_ez00_5606);
															BgL_arg2250z00_6946 =
																(VA_PROCEDUREP(BgL_tmpfunz00_10159)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_10159)) ((
																		(obj_t) BgL_ez00_5606), BgL_portz00_6907,
																	((obj_t) BgL_ez00_5606),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_10159)) ((
																		(obj_t) BgL_ez00_5606), BgL_portz00_6907,
																	((obj_t) BgL_ez00_5606)));
														}
														{	/* Expand/initial.scm 531 */
															obj_t BgL_arg2252z00_6948;

															if (NULLP(BgL_optsz00_6908))
																{	/* Expand/initial.scm 531 */
																	BgL_arg2252z00_6948 = BNIL;
																}
															else
																{	/* Expand/initial.scm 531 */
																	obj_t BgL_head1303z00_6949;

																	BgL_head1303z00_6949 =
																		MAKE_YOUNG_PAIR(BNIL, BNIL);
																	{
																		obj_t BgL_l1301z00_6951;
																		obj_t BgL_tail1304z00_6952;

																		BgL_l1301z00_6951 = BgL_optsz00_6908;
																		BgL_tail1304z00_6952 = BgL_head1303z00_6949;
																	BgL_zc3z04anonymousza32254ze3z87_6950:
																		if (NULLP(BgL_l1301z00_6951))
																			{	/* Expand/initial.scm 531 */
																				BgL_arg2252z00_6948 =
																					CDR(BgL_head1303z00_6949);
																			}
																		else
																			{	/* Expand/initial.scm 531 */
																				obj_t BgL_newtail1305z00_6953;

																				{	/* Expand/initial.scm 531 */
																					obj_t BgL_arg2257z00_6954;

																					{	/* Expand/initial.scm 531 */
																						obj_t BgL_xz00_6955;

																						BgL_xz00_6955 =
																							CAR(((obj_t) BgL_l1301z00_6951));
																						{	/* Expand/initial.scm 531 */
																							obj_t BgL_tmpfunz00_10175;

																							BgL_tmpfunz00_10175 =
																								((obj_t) BgL_ez00_5606);
																							BgL_arg2257z00_6954 =
																								(VA_PROCEDUREP
																								(BgL_tmpfunz00_10175)
																								? ((obj_t(*)(obj_t,
																											...))
																									PROCEDURE_ENTRY
																									(BgL_tmpfunz00_10175)) ((
																										(obj_t) BgL_ez00_5606),
																									BgL_xz00_6955,
																									((obj_t) BgL_ez00_5606),
																									BEOA) : ((obj_t(*)(obj_t,
																											obj_t,
																											obj_t))
																									PROCEDURE_ENTRY
																									(BgL_tmpfunz00_10175)) ((
																										(obj_t) BgL_ez00_5606),
																									BgL_xz00_6955,
																									((obj_t) BgL_ez00_5606)));
																						}
																					}
																					BgL_newtail1305z00_6953 =
																						MAKE_YOUNG_PAIR(BgL_arg2257z00_6954,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1304z00_6952,
																					BgL_newtail1305z00_6953);
																				{	/* Expand/initial.scm 531 */
																					obj_t BgL_arg2256z00_6956;

																					BgL_arg2256z00_6956 =
																						CDR(((obj_t) BgL_l1301z00_6951));
																					{
																						obj_t BgL_tail1304z00_10182;
																						obj_t BgL_l1301z00_10181;

																						BgL_l1301z00_10181 =
																							BgL_arg2256z00_6956;
																						BgL_tail1304z00_10182 =
																							BgL_newtail1305z00_6953;
																						BgL_tail1304z00_6952 =
																							BgL_tail1304z00_10182;
																						BgL_l1301z00_6951 =
																							BgL_l1301z00_10181;
																						goto
																							BgL_zc3z04anonymousza32254ze3z87_6950;
																					}
																				}
																			}
																	}
																}
															BgL_arg2251z00_6947 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_arg2252z00_6948, BNIL);
														}
														BgL_arg2249z00_6945 =
															MAKE_YOUNG_PAIR(BgL_arg2250z00_6946,
															BgL_arg2251z00_6947);
													}
													return
														MAKE_YOUNG_PAIR(BgL_arg2248z00_6944,
														BgL_arg2249z00_6945);
												}
										}
									}
								else
									{	/* Expand/initial.scm 522 */
									BgL_tagzd2455zd2_6904:
										return
											BGl_errorz00zz__errorz00(BFALSE,
											BGl_string4233z00zzexpand_installz00, BgL_xz00_5605);
									}
							}
						else
							{	/* Expand/initial.scm 522 */
								goto BgL_tagzd2455zd2_6904;
							}
					}
				else
					{	/* Expand/initial.scm 522 */
						goto BgL_tagzd2455zd2_6904;
					}
			}
		}

	}



/* &<@anonymous:2179> */
	obj_t BGl_z62zc3z04anonymousza32179ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5607, obj_t BgL_xz00_5608, obj_t BgL_ez00_5609)
	{
		{	/* Expand/initial.scm 508 */
			{
				obj_t BgL_portz00_6975;
				obj_t BgL_portz00_6969;
				obj_t BgL_valuez00_6970;

				if (PAIRP(BgL_xz00_5608))
					{	/* Expand/initial.scm 508 */
						obj_t BgL_cdrzd2421zd2_6978;

						BgL_cdrzd2421zd2_6978 = CDR(((obj_t) BgL_xz00_5608));
						if (PAIRP(BgL_cdrzd2421zd2_6978))
							{	/* Expand/initial.scm 508 */
								if (NULLP(CDR(BgL_cdrzd2421zd2_6978)))
									{	/* Expand/initial.scm 508 */
										BgL_portz00_6975 = CAR(BgL_cdrzd2421zd2_6978);
										{	/* Expand/initial.scm 511 */
											obj_t BgL_arg2196z00_6976;

											{	/* Expand/initial.scm 511 */
												obj_t BgL_arg2197z00_6977;

												{	/* Expand/initial.scm 511 */
													obj_t BgL_tmpfunz00_10205;

													BgL_tmpfunz00_10205 = ((obj_t) BgL_ez00_5609);
													BgL_arg2197z00_6977 =
														(VA_PROCEDUREP(BgL_tmpfunz00_10205)
														? ((obj_t(*)(obj_t,
																	...))
															PROCEDURE_ENTRY(BgL_tmpfunz00_10205)) (((obj_t)
																BgL_ez00_5609), BgL_portz00_6975,
															((obj_t) BgL_ez00_5609), BEOA) : ((obj_t(*)(obj_t,
																	obj_t,
																	obj_t))
															PROCEDURE_ENTRY(BgL_tmpfunz00_10205)) (((obj_t)
																BgL_ez00_5609), BgL_portz00_6975,
															((obj_t) BgL_ez00_5609)));
												}
												BgL_arg2196z00_6976 =
													MAKE_YOUNG_PAIR(BgL_arg2197z00_6977, BNIL);
											}
											return
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(83),
												BgL_arg2196z00_6976);
										}
									}
								else
									{	/* Expand/initial.scm 508 */
										obj_t BgL_cdrzd2437zd2_6979;

										BgL_cdrzd2437zd2_6979 =
											CDR(((obj_t) BgL_cdrzd2421zd2_6978));
										if (PAIRP(BgL_cdrzd2437zd2_6979))
											{	/* Expand/initial.scm 508 */
												if (NULLP(CDR(BgL_cdrzd2437zd2_6979)))
													{	/* Expand/initial.scm 508 */
														obj_t BgL_arg2188z00_6980;
														obj_t BgL_arg2189z00_6981;

														BgL_arg2188z00_6980 =
															CAR(((obj_t) BgL_cdrzd2421zd2_6978));
														BgL_arg2189z00_6981 = CAR(BgL_cdrzd2437zd2_6979);
														BgL_portz00_6969 = BgL_arg2188z00_6980;
														BgL_valuez00_6970 = BgL_arg2189z00_6981;
														{	/* Expand/initial.scm 513 */
															obj_t BgL_arg2198z00_6971;

															{	/* Expand/initial.scm 513 */
																obj_t BgL_arg2199z00_6972;
																obj_t BgL_arg2200z00_6973;

																{	/* Expand/initial.scm 513 */
																	obj_t BgL_tmpfunz00_10227;

																	BgL_tmpfunz00_10227 = ((obj_t) BgL_ez00_5609);
																	BgL_arg2199z00_6972 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_10227)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_10227)) ((
																				(obj_t) BgL_ez00_5609),
																			BgL_portz00_6969, ((obj_t) BgL_ez00_5609),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_10227)) ((
																				(obj_t) BgL_ez00_5609),
																			BgL_portz00_6969,
																			((obj_t) BgL_ez00_5609)));
																}
																{	/* Expand/initial.scm 513 */
																	obj_t BgL_arg2201z00_6974;

																	{	/* Expand/initial.scm 513 */
																		obj_t BgL_tmpfunz00_10235;

																		BgL_tmpfunz00_10235 =
																			((obj_t) BgL_ez00_5609);
																		BgL_arg2201z00_6974 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_10235)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_10235)) ((
																					(obj_t) BgL_ez00_5609),
																				BgL_valuez00_6970,
																				((obj_t) BgL_ez00_5609),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_10235)) ((
																					(obj_t) BgL_ez00_5609),
																				BgL_valuez00_6970,
																				((obj_t) BgL_ez00_5609)));
																	}
																	BgL_arg2200z00_6973 =
																		MAKE_YOUNG_PAIR(BgL_arg2201z00_6974, BNIL);
																}
																BgL_arg2198z00_6971 =
																	MAKE_YOUNG_PAIR(BgL_arg2199z00_6972,
																	BgL_arg2200z00_6973);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(83),
																BgL_arg2198z00_6971);
														}
													}
												else
													{	/* Expand/initial.scm 508 */
													BgL_tagzd2415zd2_6960:
														return
															BGl_errorz00zz__errorz00(BFALSE,
															BGl_string4235z00zzexpand_installz00,
															BgL_xz00_5608);
													}
											}
										else
											{	/* Expand/initial.scm 508 */
												goto BgL_tagzd2415zd2_6960;
											}
									}
							}
						else
							{	/* Expand/initial.scm 508 */
								if (NULLP(BgL_cdrzd2421zd2_6978))
									{	/* Expand/initial.scm 508 */
										{	/* Expand/initial.scm 515 */
											obj_t BgL_arg2202z00_6964;

											{	/* Expand/initial.scm 515 */
												obj_t BgL_arg2203z00_6965;

												{	/* Expand/initial.scm 515 */
													obj_t BgL_arg2204z00_6966;

													{	/* Expand/initial.scm 515 */
														obj_t BgL_arg2205z00_6967;

														{	/* Expand/initial.scm 515 */
															obj_t BgL_arg2206z00_6968;

															BgL_arg2206z00_6968 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(172), BNIL);
															BgL_arg2205z00_6967 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(202),
																BgL_arg2206z00_6968);
														}
														BgL_arg2204z00_6966 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(171),
															BgL_arg2205z00_6967);
													}
													BgL_arg2203z00_6965 =
														MAKE_YOUNG_PAIR(BgL_arg2204z00_6966, BNIL);
												}
												BgL_arg2202z00_6964 =
													MAKE_YOUNG_PAIR(BgL_arg2203z00_6965, BNIL);
											}
											return
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(83),
												BgL_arg2202z00_6964);
										}
									}
								else
									{	/* Expand/initial.scm 508 */
										goto BgL_tagzd2415zd2_6960;
									}
							}
					}
				else
					{	/* Expand/initial.scm 508 */
						goto BgL_tagzd2415zd2_6960;
					}
			}
		}

	}



/* &<@anonymous:2159> */
	obj_t BGl_z62zc3z04anonymousza32159ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5610, obj_t BgL_xz00_5611, obj_t BgL_ez00_5612)
	{
		{	/* Expand/initial.scm 477 */
			{
				obj_t BgL_nz00_6985;
				obj_t BgL_mz00_6986;

				if (PAIRP(BgL_xz00_5611))
					{	/* Expand/initial.scm 477 */
						obj_t BgL_cdrzd2402zd2_6995;

						BgL_cdrzd2402zd2_6995 = CDR(((obj_t) BgL_xz00_5611));
						if (PAIRP(BgL_cdrzd2402zd2_6995))
							{	/* Expand/initial.scm 477 */
								obj_t BgL_cdrzd2406zd2_6996;

								BgL_cdrzd2406zd2_6996 = CDR(BgL_cdrzd2402zd2_6995);
								if (PAIRP(BgL_cdrzd2406zd2_6996))
									{	/* Expand/initial.scm 477 */
										if (NULLP(CDR(BgL_cdrzd2406zd2_6996)))
											{	/* Expand/initial.scm 477 */
												BgL_nz00_6985 = CAR(BgL_cdrzd2402zd2_6995);
												BgL_mz00_6986 = CAR(BgL_cdrzd2406zd2_6996);
												if (CBOOL(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
													{	/* Expand/initial.scm 481 */
														obj_t BgL_arg2168z00_6987;

														{	/* Expand/initial.scm 481 */
															obj_t BgL_arg2169z00_6988;
															obj_t BgL_arg2170z00_6989;

															{	/* Expand/initial.scm 481 */
																obj_t BgL_tmpfunz00_10274;

																BgL_tmpfunz00_10274 = ((obj_t) BgL_ez00_5612);
																BgL_arg2169z00_6988 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_10274)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_10274)) ((
																			(obj_t) BgL_ez00_5612), BgL_nz00_6985,
																		((obj_t) BgL_ez00_5612),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_10274)) ((
																			(obj_t) BgL_ez00_5612), BgL_nz00_6985,
																		((obj_t) BgL_ez00_5612)));
															}
															{	/* Expand/initial.scm 481 */
																obj_t BgL_arg2171z00_6990;

																{	/* Expand/initial.scm 481 */
																	obj_t BgL_tmpfunz00_10282;

																	BgL_tmpfunz00_10282 = ((obj_t) BgL_ez00_5612);
																	BgL_arg2171z00_6990 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_10282)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_10282)) ((
																				(obj_t) BgL_ez00_5612), BgL_mz00_6986,
																			((obj_t) BgL_ez00_5612),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_10282)) ((
																				(obj_t) BgL_ez00_5612), BgL_mz00_6986,
																			((obj_t) BgL_ez00_5612)));
																}
																BgL_arg2170z00_6989 =
																	MAKE_YOUNG_PAIR(BgL_arg2171z00_6990, BNIL);
															}
															BgL_arg2168z00_6987 =
																MAKE_YOUNG_PAIR(BgL_arg2169z00_6988,
																BgL_arg2170z00_6989);
														}
														return
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(203),
															BgL_arg2168z00_6987);
													}
												else
													{	/* Expand/initial.scm 482 */
														obj_t BgL_arg2172z00_6991;

														{	/* Expand/initial.scm 482 */
															obj_t BgL_arg2173z00_6992;
															obj_t BgL_arg2174z00_6993;

															{	/* Expand/initial.scm 482 */
																obj_t BgL_tmpfunz00_10294;

																BgL_tmpfunz00_10294 = ((obj_t) BgL_ez00_5612);
																BgL_arg2173z00_6992 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_10294)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_10294)) ((
																			(obj_t) BgL_ez00_5612), BgL_nz00_6985,
																		((obj_t) BgL_ez00_5612),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_10294)) ((
																			(obj_t) BgL_ez00_5612), BgL_nz00_6985,
																		((obj_t) BgL_ez00_5612)));
															}
															{	/* Expand/initial.scm 482 */
																obj_t BgL_arg2175z00_6994;

																{	/* Expand/initial.scm 482 */
																	obj_t BgL_tmpfunz00_10302;

																	BgL_tmpfunz00_10302 = ((obj_t) BgL_ez00_5612);
																	BgL_arg2175z00_6994 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_10302)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_10302)) ((
																				(obj_t) BgL_ez00_5612), BgL_mz00_6986,
																			((obj_t) BgL_ez00_5612),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_10302)) ((
																				(obj_t) BgL_ez00_5612), BgL_mz00_6986,
																			((obj_t) BgL_ez00_5612)));
																}
																BgL_arg2174z00_6993 =
																	MAKE_YOUNG_PAIR(BgL_arg2175z00_6994, BNIL);
															}
															BgL_arg2172z00_6991 =
																MAKE_YOUNG_PAIR(BgL_arg2173z00_6992,
																BgL_arg2174z00_6993);
														}
														return
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(76),
															BgL_arg2172z00_6991);
													}
											}
										else
											{	/* Expand/initial.scm 477 */
											BgL_tagzd2395zd2_6983:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4236z00zzexpand_installz00, BgL_xz00_5611);
											}
									}
								else
									{	/* Expand/initial.scm 477 */
										goto BgL_tagzd2395zd2_6983;
									}
							}
						else
							{	/* Expand/initial.scm 477 */
								goto BgL_tagzd2395zd2_6983;
							}
					}
				else
					{	/* Expand/initial.scm 477 */
						goto BgL_tagzd2395zd2_6983;
					}
			}
		}

	}



/* &<@anonymous:2146> */
	obj_t BGl_z62zc3z04anonymousza32146ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5613, obj_t BgL_xz00_5614, obj_t BgL_ez00_5615)
	{
		{	/* Expand/initial.scm 466 */
			{
				obj_t BgL_nz00_7000;

				if (PAIRP(BgL_xz00_5614))
					{	/* Expand/initial.scm 466 */
						obj_t BgL_cdrzd2389zd2_7005;

						BgL_cdrzd2389zd2_7005 = CDR(((obj_t) BgL_xz00_5614));
						if (PAIRP(BgL_cdrzd2389zd2_7005))
							{	/* Expand/initial.scm 466 */
								if (NULLP(CDR(BgL_cdrzd2389zd2_7005)))
									{	/* Expand/initial.scm 466 */
										BgL_nz00_7000 = CAR(BgL_cdrzd2389zd2_7005);
										if (CBOOL(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
											{	/* Expand/initial.scm 470 */
												obj_t BgL_arg2154z00_7001;

												{	/* Expand/initial.scm 470 */
													obj_t BgL_arg2155z00_7002;

													{	/* Expand/initial.scm 470 */
														obj_t BgL_tmpfunz00_10328;

														BgL_tmpfunz00_10328 = ((obj_t) BgL_ez00_5615);
														BgL_arg2155z00_7002 =
															(VA_PROCEDUREP(BgL_tmpfunz00_10328)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10328)) (((obj_t)
																	BgL_ez00_5615), BgL_nz00_7000,
																((obj_t) BgL_ez00_5615),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10328)) (((obj_t)
																	BgL_ez00_5615), BgL_nz00_7000,
																((obj_t) BgL_ez00_5615)));
													}
													BgL_arg2154z00_7001 =
														MAKE_YOUNG_PAIR(BgL_arg2155z00_7002, BNIL);
												}
												return
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(204),
													BgL_arg2154z00_7001);
											}
										else
											{	/* Expand/initial.scm 471 */
												obj_t BgL_arg2156z00_7003;

												{	/* Expand/initial.scm 471 */
													obj_t BgL_arg2157z00_7004;

													{	/* Expand/initial.scm 471 */
														obj_t BgL_tmpfunz00_10339;

														BgL_tmpfunz00_10339 = ((obj_t) BgL_ez00_5615);
														BgL_arg2157z00_7004 =
															(VA_PROCEDUREP(BgL_tmpfunz00_10339)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10339)) (((obj_t)
																	BgL_ez00_5615), BgL_nz00_7000,
																((obj_t) BgL_ez00_5615),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10339)) (((obj_t)
																	BgL_ez00_5615), BgL_nz00_7000,
																((obj_t) BgL_ez00_5615)));
													}
													BgL_arg2156z00_7003 =
														MAKE_YOUNG_PAIR(BgL_arg2157z00_7004, BNIL);
												}
												return
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(75),
													BgL_arg2156z00_7003);
											}
									}
								else
									{	/* Expand/initial.scm 466 */
									BgL_tagzd2384zd2_6998:
										return
											BGl_errorz00zz__errorz00(BFALSE,
											BGl_string4237z00zzexpand_installz00, BgL_xz00_5614);
									}
							}
						else
							{	/* Expand/initial.scm 466 */
								goto BgL_tagzd2384zd2_6998;
							}
					}
				else
					{	/* Expand/initial.scm 466 */
						goto BgL_tagzd2384zd2_6998;
					}
			}
		}

	}



/* &<@anonymous:2144> */
	obj_t BGl_z62zc3z04anonymousza32144ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5616, obj_t BgL_xz00_5617, obj_t BgL_ez00_5618)
	{
		{	/* Expand/initial.scm 430 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5617,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5618));
		}

	}



/* &<@anonymous:2142> */
	obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5619, obj_t BgL_xz00_5620, obj_t BgL_ez00_5621)
	{
		{	/* Expand/initial.scm 428 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5620,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5621));
		}

	}



/* &<@anonymous:2140> */
	obj_t BGl_z62zc3z04anonymousza32140ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5622, obj_t BgL_xz00_5623, obj_t BgL_ez00_5624)
	{
		{	/* Expand/initial.scm 426 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5623,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5624));
		}

	}



/* &<@anonymous:2138> */
	obj_t BGl_z62zc3z04anonymousza32138ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5625, obj_t BgL_xz00_5626, obj_t BgL_ez00_5627)
	{
		{	/* Expand/initial.scm 424 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5626,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5627));
		}

	}



/* &<@anonymous:2136> */
	obj_t BGl_z62zc3z04anonymousza32136ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5628, obj_t BgL_xz00_5629, obj_t BgL_ez00_5630)
	{
		{	/* Expand/initial.scm 422 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5629,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5630));
		}

	}



/* &<@anonymous:2134> */
	obj_t BGl_z62zc3z04anonymousza32134ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5631, obj_t BgL_xz00_5632, obj_t BgL_ez00_5633)
	{
		{	/* Expand/initial.scm 420 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5632,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5633));
		}

	}



/* &<@anonymous:2132> */
	obj_t BGl_z62zc3z04anonymousza32132ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5634, obj_t BgL_xz00_5635, obj_t BgL_ez00_5636)
	{
		{	/* Expand/initial.scm 418 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5635,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5636));
		}

	}



/* &<@anonymous:2130> */
	obj_t BGl_z62zc3z04anonymousza32130ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5637, obj_t BgL_xz00_5638, obj_t BgL_ez00_5639)
	{
		{	/* Expand/initial.scm 416 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5638,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5639));
		}

	}



/* &<@anonymous:2127> */
	obj_t BGl_z62zc3z04anonymousza32127ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5640, obj_t BgL_xz00_5641, obj_t BgL_ez00_5642)
	{
		{	/* Expand/initial.scm 414 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5641,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5642));
		}

	}



/* &<@anonymous:2125> */
	obj_t BGl_z62zc3z04anonymousza32125ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5643, obj_t BgL_xz00_5644, obj_t BgL_ez00_5645)
	{
		{	/* Expand/initial.scm 412 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5644,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5645));
		}

	}



/* &<@anonymous:2123> */
	obj_t BGl_z62zc3z04anonymousza32123ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5646, obj_t BgL_xz00_5647, obj_t BgL_ez00_5648)
	{
		{	/* Expand/initial.scm 410 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5647,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5648));
		}

	}



/* &<@anonymous:2121> */
	obj_t BGl_z62zc3z04anonymousza32121ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5649, obj_t BgL_xz00_5650, obj_t BgL_ez00_5651)
	{
		{	/* Expand/initial.scm 408 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5650,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5651));
		}

	}



/* &<@anonymous:2119> */
	obj_t BGl_z62zc3z04anonymousza32119ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5652, obj_t BgL_xz00_5653, obj_t BgL_ez00_5654)
	{
		{	/* Expand/initial.scm 406 */
			return
				BGl_callzd2checkzd2zzexpand_installz00(BgL_xz00_5653,
				CNST_TABLE_REF(165), BGl_string4219z00zzexpand_installz00,
				((obj_t) BgL_ez00_5654));
		}

	}



/* &<@anonymous:2117> */
	obj_t BGl_z62zc3z04anonymousza32117ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5655, obj_t BgL_xz00_5656, obj_t BgL_ez00_5657)
	{
		{	/* Expand/initial.scm 403 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 404 */
					return
						BGl_expandzd2gminzd2zzexpand_garithmetiquez00(BgL_xz00_5656,
						BgL_ez00_5657);
				}
			else
				{	/* Expand/initial.scm 404 */
					return
						BGl_expandzd2minfxzd2zzexpand_iarithmetiquez00(BgL_xz00_5656,
						BgL_ez00_5657);
				}
		}

	}



/* &<@anonymous:2115> */
	obj_t BGl_z62zc3z04anonymousza32115ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5658, obj_t BgL_xz00_5659, obj_t BgL_ez00_5660)
	{
		{	/* Expand/initial.scm 400 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 401 */
					return
						BGl_expandzd2gmaxzd2zzexpand_garithmetiquez00(BgL_xz00_5659,
						BgL_ez00_5660);
				}
			else
				{	/* Expand/initial.scm 401 */
					return
						BGl_expandzd2maxfxzd2zzexpand_iarithmetiquez00(BgL_xz00_5659,
						BgL_ez00_5660);
				}
		}

	}



/* &<@anonymous:2113> */
	obj_t BGl_z62zc3z04anonymousza32113ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5661, obj_t BgL_xz00_5662, obj_t BgL_ez00_5663)
	{
		{	/* Expand/initial.scm 397 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 398 */
					return
						BGl_expandzd2gze3zd3ze2zzexpand_garithmetiquez00(BgL_xz00_5662,
						BgL_ez00_5663);
				}
			else
				{	/* Expand/initial.scm 398 */
					return
						BGl_expandzd2ize3zd3ze2zzexpand_iarithmetiquez00(BgL_xz00_5662,
						BgL_ez00_5663);
				}
		}

	}



/* &<@anonymous:2111> */
	obj_t BGl_z62zc3z04anonymousza32111ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5664, obj_t BgL_xz00_5665, obj_t BgL_ez00_5666)
	{
		{	/* Expand/initial.scm 394 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 395 */
					return
						BGl_expandzd2gzc3zd3zc2zzexpand_garithmetiquez00(BgL_xz00_5665,
						BgL_ez00_5666);
				}
			else
				{	/* Expand/initial.scm 395 */
					return
						BGl_expandzd2izc3zd3zc2zzexpand_iarithmetiquez00(BgL_xz00_5665,
						BgL_ez00_5666);
				}
		}

	}



/* &<@anonymous:2109> */
	obj_t BGl_z62zc3z04anonymousza32109ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5667, obj_t BgL_xz00_5668, obj_t BgL_ez00_5669)
	{
		{	/* Expand/initial.scm 391 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 392 */
					return
						BGl_expandzd2gze3z31zzexpand_garithmetiquez00(BgL_xz00_5668,
						BgL_ez00_5669);
				}
			else
				{	/* Expand/initial.scm 392 */
					return
						BGl_expandzd2ize3z31zzexpand_iarithmetiquez00(BgL_xz00_5668,
						BgL_ez00_5669);
				}
		}

	}



/* &<@anonymous:2107> */
	obj_t BGl_z62zc3z04anonymousza32107ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5670, obj_t BgL_xz00_5671, obj_t BgL_ez00_5672)
	{
		{	/* Expand/initial.scm 388 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 389 */
					return
						BGl_expandzd2gzc3z11zzexpand_garithmetiquez00(BgL_xz00_5671,
						BgL_ez00_5672);
				}
			else
				{	/* Expand/initial.scm 389 */
					return
						BGl_expandzd2izc3z11zzexpand_iarithmetiquez00(BgL_xz00_5671,
						BgL_ez00_5672);
				}
		}

	}



/* &<@anonymous:2105> */
	obj_t BGl_z62zc3z04anonymousza32105ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5673, obj_t BgL_xz00_5674, obj_t BgL_ez00_5675)
	{
		{	/* Expand/initial.scm 385 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 386 */
					return
						BGl_expandzd2gzd3z01zzexpand_garithmetiquez00(BgL_xz00_5674,
						BgL_ez00_5675);
				}
			else
				{	/* Expand/initial.scm 386 */
					return
						BGl_expandzd2izd3z01zzexpand_iarithmetiquez00(BgL_xz00_5674,
						BgL_ez00_5675);
				}
		}

	}



/* &<@anonymous:2103> */
	obj_t BGl_z62zc3z04anonymousza32103ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5676, obj_t BgL_xz00_5677, obj_t BgL_ez00_5678)
	{
		{	/* Expand/initial.scm 382 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 383 */
					return
						BGl_expandzd2gzf2z20zzexpand_garithmetiquez00(BgL_xz00_5677,
						BgL_ez00_5678);
				}
			else
				{	/* Expand/initial.scm 383 */
					return
						BGl_expandzd2izf2z20zzexpand_iarithmetiquez00(BgL_xz00_5677,
						BgL_ez00_5678);
				}
		}

	}



/* &<@anonymous:2101> */
	obj_t BGl_z62zc3z04anonymousza32101ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5679, obj_t BgL_xz00_5680, obj_t BgL_ez00_5681)
	{
		{	/* Expand/initial.scm 379 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 380 */
					return
						BGl_expandzd2gza2z70zzexpand_garithmetiquez00(BgL_xz00_5680,
						BgL_ez00_5681);
				}
			else
				{	/* Expand/initial.scm 380 */
					return
						BGl_expandzd2iza2z70zzexpand_iarithmetiquez00(BgL_xz00_5680,
						BgL_ez00_5681);
				}
		}

	}



/* &<@anonymous:2099> */
	obj_t BGl_z62zc3z04anonymousza32099ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5682, obj_t BgL_xz00_5683, obj_t BgL_ez00_5684)
	{
		{	/* Expand/initial.scm 376 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 377 */
					return
						BGl_expandzd2gzd2z00zzexpand_garithmetiquez00(BgL_xz00_5683,
						BgL_ez00_5684);
				}
			else
				{	/* Expand/initial.scm 377 */
					return
						BGl_expandzd2izd2z00zzexpand_iarithmetiquez00(BgL_xz00_5683,
						BgL_ez00_5684);
				}
		}

	}



/* &<@anonymous:2097> */
	obj_t BGl_z62zc3z04anonymousza32097ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5685, obj_t BgL_xz00_5686, obj_t BgL_ez00_5687)
	{
		{	/* Expand/initial.scm 373 */
			if (CBOOL(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00))
				{	/* Expand/initial.scm 374 */
					return
						BGl_expandzd2gzb2z60zzexpand_garithmetiquez00(BgL_xz00_5686,
						BgL_ez00_5687);
				}
			else
				{	/* Expand/initial.scm 374 */
					return
						BGl_expandzd2izb2z60zzexpand_iarithmetiquez00(BgL_xz00_5686,
						BgL_ez00_5687);
				}
		}

	}



/* &<@anonymous:2076> */
	obj_t BGl_z62zc3z04anonymousza32076ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5688, obj_t BgL_xz00_5689, obj_t BgL_ez00_5690)
	{
		{	/* Expand/initial.scm 362 */
			{

				if (PAIRP(BgL_xz00_5689))
					{	/* Expand/initial.scm 362 */
						obj_t BgL_cdrzd2376zd2_7028;

						BgL_cdrzd2376zd2_7028 = CDR(((obj_t) BgL_xz00_5689));
						if (PAIRP(BgL_cdrzd2376zd2_7028))
							{	/* Expand/initial.scm 362 */
								bool_t BgL_test4661z00_10435;

								{	/* Expand/initial.scm 362 */
									obj_t BgL_tmpz00_10436;

									BgL_tmpz00_10436 = CAR(BgL_cdrzd2376zd2_7028);
									BgL_test4661z00_10435 = REALP(BgL_tmpz00_10436);
								}
								if (BgL_test4661z00_10435)
									{	/* Expand/initial.scm 362 */
										if (NULLP(CDR(BgL_cdrzd2376zd2_7028)))
											{	/* Expand/initial.scm 362 */
												return BTRUE;
											}
										else
											{	/* Expand/initial.scm 362 */
											BgL_tagzd2373zd2_7019:
												if (NULLP(BgL_xz00_5689))
													{	/* Expand/initial.scm 369 */
														return BNIL;
													}
												else
													{	/* Expand/initial.scm 369 */
														obj_t BgL_head1293z00_7020;

														BgL_head1293z00_7020 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1291z00_7022;
															obj_t BgL_tail1294z00_7023;

															BgL_l1291z00_7022 = BgL_xz00_5689;
															BgL_tail1294z00_7023 = BgL_head1293z00_7020;
														BgL_zc3z04anonymousza32092ze3z87_7021:
															if (NULLP(BgL_l1291z00_7022))
																{	/* Expand/initial.scm 369 */
																	return CDR(BgL_head1293z00_7020);
																}
															else
																{	/* Expand/initial.scm 369 */
																	obj_t BgL_newtail1295z00_7024;

																	{	/* Expand/initial.scm 369 */
																		obj_t BgL_arg2095z00_7025;

																		{	/* Expand/initial.scm 369 */
																			obj_t BgL_xz00_7026;

																			BgL_xz00_7026 =
																				CAR(((obj_t) BgL_l1291z00_7022));
																			BgL_arg2095z00_7025 =
																				BGL_PROCEDURE_CALL2(BgL_ez00_5690,
																				BgL_xz00_7026, BgL_ez00_5690);
																		}
																		BgL_newtail1295z00_7024 =
																			MAKE_YOUNG_PAIR(BgL_arg2095z00_7025,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1294z00_7023,
																		BgL_newtail1295z00_7024);
																	{	/* Expand/initial.scm 369 */
																		obj_t BgL_arg2094z00_7027;

																		BgL_arg2094z00_7027 =
																			CDR(((obj_t) BgL_l1291z00_7022));
																		{
																			obj_t BgL_tail1294z00_10460;
																			obj_t BgL_l1291z00_10459;

																			BgL_l1291z00_10459 = BgL_arg2094z00_7027;
																			BgL_tail1294z00_10460 =
																				BgL_newtail1295z00_7024;
																			BgL_tail1294z00_7023 =
																				BgL_tail1294z00_10460;
																			BgL_l1291z00_7022 = BgL_l1291z00_10459;
																			goto
																				BgL_zc3z04anonymousza32092ze3z87_7021;
																		}
																	}
																}
														}
													}
											}
									}
								else
									{	/* Expand/initial.scm 362 */
										bool_t BgL_test4665z00_10461;

										{	/* Expand/initial.scm 362 */
											obj_t BgL_arg2089z00_7029;

											BgL_arg2089z00_7029 =
												CAR(((obj_t) BgL_cdrzd2376zd2_7028));
											BgL_test4665z00_10461 =
												BGl_numberzf3zf3zz__r4_numbers_6_5z00
												(BgL_arg2089z00_7029);
										}
										if (BgL_test4665z00_10461)
											{	/* Expand/initial.scm 362 */
												if (NULLP(CDR(((obj_t) BgL_cdrzd2376zd2_7028))))
													{	/* Expand/initial.scm 362 */
														return BFALSE;
													}
												else
													{	/* Expand/initial.scm 362 */
														goto BgL_tagzd2373zd2_7019;
													}
											}
										else
											{	/* Expand/initial.scm 362 */
												goto BgL_tagzd2373zd2_7019;
											}
									}
							}
						else
							{	/* Expand/initial.scm 362 */
								goto BgL_tagzd2373zd2_7019;
							}
					}
				else
					{	/* Expand/initial.scm 362 */
						goto BgL_tagzd2373zd2_7019;
					}
			}
		}

	}



/* &<@anonymous:2053> */
	obj_t BGl_z62zc3z04anonymousza32053ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5691, obj_t BgL_xz00_5692, obj_t BgL_ez00_5693)
	{
		{	/* Expand/initial.scm 353 */
			{

				if (PAIRP(BgL_xz00_5692))
					{	/* Expand/initial.scm 353 */
						obj_t BgL_cdrzd2364zd2_7039;

						BgL_cdrzd2364zd2_7039 = CDR(((obj_t) BgL_xz00_5692));
						if (PAIRP(BgL_cdrzd2364zd2_7039))
							{	/* Expand/initial.scm 353 */
								if (INTEGERP(CAR(BgL_cdrzd2364zd2_7039)))
									{	/* Expand/initial.scm 353 */
										if (NULLP(CDR(BgL_cdrzd2364zd2_7039)))
											{	/* Expand/initial.scm 353 */
												return BTRUE;
											}
										else
											{	/* Expand/initial.scm 353 */
											BgL_tagzd2361zd2_7030:
												if (NULLP(BgL_xz00_5692))
													{	/* Expand/initial.scm 360 */
														return BNIL;
													}
												else
													{	/* Expand/initial.scm 360 */
														obj_t BgL_head1288z00_7031;

														BgL_head1288z00_7031 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1286z00_7033;
															obj_t BgL_tail1289z00_7034;

															BgL_l1286z00_7033 = BgL_xz00_5692;
															BgL_tail1289z00_7034 = BgL_head1288z00_7031;
														BgL_zc3z04anonymousza32070ze3z87_7032:
															if (NULLP(BgL_l1286z00_7033))
																{	/* Expand/initial.scm 360 */
																	return CDR(BgL_head1288z00_7031);
																}
															else
																{	/* Expand/initial.scm 360 */
																	obj_t BgL_newtail1290z00_7035;

																	{	/* Expand/initial.scm 360 */
																		obj_t BgL_arg2074z00_7036;

																		{	/* Expand/initial.scm 360 */
																			obj_t BgL_xz00_7037;

																			BgL_xz00_7037 =
																				CAR(((obj_t) BgL_l1286z00_7033));
																			BgL_arg2074z00_7036 =
																				BGL_PROCEDURE_CALL2(BgL_ez00_5693,
																				BgL_xz00_7037, BgL_ez00_5693);
																		}
																		BgL_newtail1290z00_7035 =
																			MAKE_YOUNG_PAIR(BgL_arg2074z00_7036,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1289z00_7034,
																		BgL_newtail1290z00_7035);
																	{	/* Expand/initial.scm 360 */
																		obj_t BgL_arg2072z00_7038;

																		BgL_arg2072z00_7038 =
																			CDR(((obj_t) BgL_l1286z00_7033));
																		{
																			obj_t BgL_tail1289z00_10499;
																			obj_t BgL_l1286z00_10498;

																			BgL_l1286z00_10498 = BgL_arg2072z00_7038;
																			BgL_tail1289z00_10499 =
																				BgL_newtail1290z00_7035;
																			BgL_tail1289z00_7034 =
																				BgL_tail1289z00_10499;
																			BgL_l1286z00_7033 = BgL_l1286z00_10498;
																			goto
																				BgL_zc3z04anonymousza32070ze3z87_7032;
																		}
																	}
																}
														}
													}
											}
									}
								else
									{	/* Expand/initial.scm 353 */
										bool_t BgL_test4673z00_10500;

										{	/* Expand/initial.scm 353 */
											obj_t BgL_arg2067z00_7040;

											BgL_arg2067z00_7040 =
												CAR(((obj_t) BgL_cdrzd2364zd2_7039));
											BgL_test4673z00_10500 =
												BGl_numberzf3zf3zz__r4_numbers_6_5z00
												(BgL_arg2067z00_7040);
										}
										if (BgL_test4673z00_10500)
											{	/* Expand/initial.scm 353 */
												if (NULLP(CDR(((obj_t) BgL_cdrzd2364zd2_7039))))
													{	/* Expand/initial.scm 353 */
														return BFALSE;
													}
												else
													{	/* Expand/initial.scm 353 */
														goto BgL_tagzd2361zd2_7030;
													}
											}
										else
											{	/* Expand/initial.scm 353 */
												goto BgL_tagzd2361zd2_7030;
											}
									}
							}
						else
							{	/* Expand/initial.scm 353 */
								goto BgL_tagzd2361zd2_7030;
							}
					}
				else
					{	/* Expand/initial.scm 353 */
						goto BgL_tagzd2361zd2_7030;
					}
			}
		}

	}



/* &<@anonymous:1997> */
	obj_t BGl_z62zc3z04anonymousza31997ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5694, obj_t BgL_xz00_5695, obj_t BgL_ez00_5696)
	{
		{	/* Expand/initial.scm 325 */
			{
				obj_t BgL_a1z00_7044;
				obj_t BgL_a2z00_7045;

				if (PAIRP(BgL_xz00_5695))
					{	/* Expand/initial.scm 325 */
						obj_t BgL_cdrzd2349zd2_7057;

						BgL_cdrzd2349zd2_7057 = CDR(((obj_t) BgL_xz00_5695));
						if (PAIRP(BgL_cdrzd2349zd2_7057))
							{	/* Expand/initial.scm 325 */
								obj_t BgL_cdrzd2353zd2_7058;

								BgL_cdrzd2353zd2_7058 = CDR(BgL_cdrzd2349zd2_7057);
								if (PAIRP(BgL_cdrzd2353zd2_7058))
									{	/* Expand/initial.scm 325 */
										if (NULLP(CDR(BgL_cdrzd2353zd2_7058)))
											{	/* Expand/initial.scm 325 */
												BgL_a1z00_7044 = CAR(BgL_cdrzd2349zd2_7057);
												BgL_a2z00_7045 = CAR(BgL_cdrzd2353zd2_7058);
												{	/* Expand/initial.scm 328 */
													bool_t BgL_test4679z00_10520;

													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_7044))
														{	/* Expand/initial.scm 328 */
															BgL_test4679z00_10520 =
																BGl_numberzf3zf3zz__r4_numbers_6_5z00
																(BgL_a2z00_7045);
														}
													else
														{	/* Expand/initial.scm 328 */
															BgL_test4679z00_10520 = ((bool_t) 0);
														}
													if (BgL_test4679z00_10520)
														{	/* Expand/initial.scm 329 */
															bool_t BgL_test4681z00_10524;

															if (INTEGERP(BgL_a1z00_7044))
																{	/* Expand/initial.scm 329 */
																	BgL_test4681z00_10524 =
																		INTEGERP(BgL_a2z00_7045);
																}
															else
																{	/* Expand/initial.scm 329 */
																	BgL_test4681z00_10524 = ((bool_t) 0);
																}
															if (BgL_test4681z00_10524)
																{	/* Expand/initial.scm 329 */
																	return
																		BBOOL(
																		((long) CINT(BgL_a1z00_7044) ==
																			(long) CINT(BgL_a2z00_7045)));
																}
															else
																{	/* Expand/initial.scm 329 */
																	return
																		BBOOL(BGl_2zd3zd3zz__r4_numbers_6_5z00
																		(BgL_a1z00_7044, BgL_a2z00_7045));
																}
														}
													else
														{	/* Expand/initial.scm 330 */
															obj_t BgL_arg2010z00_7046;
															obj_t BgL_arg2011z00_7047;

															{	/* Expand/initial.scm 330 */
																bool_t BgL_test4683z00_10534;

																if (CHARP(BgL_a1z00_7044))
																	{	/* Expand/initial.scm 330 */
																		BgL_test4683z00_10534 = ((bool_t) 1);
																	}
																else
																	{	/* Expand/initial.scm 330 */
																		if (CHARP(BgL_a2z00_7045))
																			{	/* Expand/initial.scm 331 */
																				BgL_test4683z00_10534 = ((bool_t) 1);
																			}
																		else
																			{	/* Expand/initial.scm 332 */
																				bool_t BgL_test4686z00_10539;

																				if (PAIRP(BgL_a1z00_7044))
																					{	/* Expand/initial.scm 332 */
																						if (
																							(CAR(BgL_a1z00_7044) ==
																								CNST_TABLE_REF(190)))
																							{	/* Expand/initial.scm 334 */
																								bool_t BgL__ortest_1113z00_7048;

																								BgL__ortest_1113z00_7048 =
																									INTEGERP(CAR(CDR
																										(BgL_a1z00_7044)));
																								if (BgL__ortest_1113z00_7048)
																									{	/* Expand/initial.scm 334 */
																										BgL_test4686z00_10539 =
																											BgL__ortest_1113z00_7048;
																									}
																								else
																									{	/* Expand/initial.scm 335 */
																										bool_t
																											BgL__ortest_1114z00_7049;
																										{	/* Expand/initial.scm 335 */
																											obj_t BgL_tmpz00_10550;

																											BgL_tmpz00_10550 =
																												CAR(CDR
																												(BgL_a1z00_7044));
																											BgL__ortest_1114z00_7049 =
																												CHARP(BgL_tmpz00_10550);
																										}
																										if (BgL__ortest_1114z00_7049)
																											{	/* Expand/initial.scm 335 */
																												BgL_test4686z00_10539 =
																													BgL__ortest_1114z00_7049;
																											}
																										else
																											{	/* Expand/initial.scm 336 */
																												bool_t
																													BgL__ortest_1116z00_7050;
																												{	/* Expand/initial.scm 336 */
																													obj_t
																														BgL_tmpz00_10555;
																													BgL_tmpz00_10555 =
																														CAR(CDR
																														(BgL_a1z00_7044));
																													BgL__ortest_1116z00_7050
																														=
																														SYMBOLP
																														(BgL_tmpz00_10555);
																												}
																												if (BgL__ortest_1116z00_7050)
																													{	/* Expand/initial.scm 336 */
																														BgL_test4686z00_10539
																															=
																															BgL__ortest_1116z00_7050;
																													}
																												else
																													{	/* Expand/initial.scm 337 */
																														obj_t
																															BgL_tmpz00_10560;
																														BgL_tmpz00_10560 =
																															CAR(CDR
																															(BgL_a1z00_7044));
																														BgL_test4686z00_10539
																															=
																															KEYWORDP
																															(BgL_tmpz00_10560);
																													}
																											}
																									}
																							}
																						else
																							{	/* Expand/initial.scm 333 */
																								BgL_test4686z00_10539 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Expand/initial.scm 332 */
																						BgL_test4686z00_10539 =
																							((bool_t) 0);
																					}
																				if (BgL_test4686z00_10539)
																					{	/* Expand/initial.scm 332 */
																						BgL_test4683z00_10534 =
																							((bool_t) 1);
																					}
																				else
																					{	/* Expand/initial.scm 332 */
																						if (PAIRP(BgL_a2z00_7045))
																							{	/* Expand/initial.scm 338 */
																								if (
																									(CAR(BgL_a2z00_7045) ==
																										CNST_TABLE_REF(190)))
																									{	/* Expand/initial.scm 340 */
																										bool_t
																											BgL__ortest_1119z00_7051;
																										BgL__ortest_1119z00_7051 =
																											INTEGERP(CAR(CDR
																												(BgL_a2z00_7045)));
																										if (BgL__ortest_1119z00_7051)
																											{	/* Expand/initial.scm 340 */
																												BgL_test4683z00_10534 =
																													BgL__ortest_1119z00_7051;
																											}
																										else
																											{	/* Expand/initial.scm 341 */
																												bool_t
																													BgL__ortest_1120z00_7052;
																												{	/* Expand/initial.scm 341 */
																													obj_t
																														BgL_tmpz00_10574;
																													BgL_tmpz00_10574 =
																														CAR(CDR
																														(BgL_a2z00_7045));
																													BgL__ortest_1120z00_7052
																														=
																														CHARP
																														(BgL_tmpz00_10574);
																												}
																												if (BgL__ortest_1120z00_7052)
																													{	/* Expand/initial.scm 341 */
																														BgL_test4683z00_10534
																															=
																															BgL__ortest_1120z00_7052;
																													}
																												else
																													{	/* Expand/initial.scm 342 */
																														bool_t
																															BgL__ortest_1121z00_7053;
																														{	/* Expand/initial.scm 342 */
																															obj_t
																																BgL_tmpz00_10579;
																															BgL_tmpz00_10579 =
																																CAR(CDR
																																(BgL_a2z00_7045));
																															BgL__ortest_1121z00_7053
																																=
																																SYMBOLP
																																(BgL_tmpz00_10579);
																														}
																														if (BgL__ortest_1121z00_7053)
																															{	/* Expand/initial.scm 342 */
																																BgL_test4683z00_10534
																																	=
																																	BgL__ortest_1121z00_7053;
																															}
																														else
																															{	/* Expand/initial.scm 343 */
																																obj_t
																																	BgL_tmpz00_10584;
																																BgL_tmpz00_10584
																																	=
																																	CAR(CDR
																																	(BgL_a2z00_7045));
																																BgL_test4683z00_10534
																																	=
																																	KEYWORDP
																																	(BgL_tmpz00_10584);
																															}
																													}
																											}
																									}
																								else
																									{	/* Expand/initial.scm 339 */
																										BgL_test4683z00_10534 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Expand/initial.scm 338 */
																								BgL_test4683z00_10534 =
																									((bool_t) 0);
																							}
																					}
																			}
																	}
																if (BgL_test4683z00_10534)
																	{	/* Expand/initial.scm 330 */
																		BgL_arg2010z00_7046 = CNST_TABLE_REF(61);
																	}
																else
																	{	/* Expand/initial.scm 330 */
																		BgL_arg2010z00_7046 = CNST_TABLE_REF(37);
																	}
															}
															{	/* Expand/initial.scm 346 */
																obj_t BgL_arg2049z00_7054;
																obj_t BgL_arg2050z00_7055;

																{	/* Expand/initial.scm 346 */
																	obj_t BgL_tmpfunz00_10596;

																	BgL_tmpfunz00_10596 = ((obj_t) BgL_ez00_5696);
																	BgL_arg2049z00_7054 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_10596)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_10596)) ((
																				(obj_t) BgL_ez00_5696), BgL_a1z00_7044,
																			((obj_t) BgL_ez00_5696),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_10596)) ((
																				(obj_t) BgL_ez00_5696), BgL_a1z00_7044,
																			((obj_t) BgL_ez00_5696)));
																}
																{	/* Expand/initial.scm 347 */
																	obj_t BgL_arg2051z00_7056;

																	{	/* Expand/initial.scm 347 */
																		obj_t BgL_tmpfunz00_10604;

																		BgL_tmpfunz00_10604 =
																			((obj_t) BgL_ez00_5696);
																		BgL_arg2051z00_7056 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_10604)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_10604)) ((
																					(obj_t) BgL_ez00_5696),
																				BgL_a2z00_7045, ((obj_t) BgL_ez00_5696),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_10604)) ((
																					(obj_t) BgL_ez00_5696),
																				BgL_a2z00_7045,
																				((obj_t) BgL_ez00_5696)));
																	}
																	BgL_arg2050z00_7055 =
																		MAKE_YOUNG_PAIR(BgL_arg2051z00_7056, BNIL);
																}
																BgL_arg2011z00_7047 =
																	MAKE_YOUNG_PAIR(BgL_arg2049z00_7054,
																	BgL_arg2050z00_7055);
															}
															return
																MAKE_YOUNG_PAIR(BgL_arg2010z00_7046,
																BgL_arg2011z00_7047);
														}
												}
											}
										else
											{	/* Expand/initial.scm 325 */
											BgL_tagzd2342zd2_7042:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4238z00zzexpand_installz00, BgL_xz00_5695);
											}
									}
								else
									{	/* Expand/initial.scm 325 */
										goto BgL_tagzd2342zd2_7042;
									}
							}
						else
							{	/* Expand/initial.scm 325 */
								goto BgL_tagzd2342zd2_7042;
							}
					}
				else
					{	/* Expand/initial.scm 325 */
						goto BgL_tagzd2342zd2_7042;
					}
			}
		}

	}



/* &<@anonymous:1940> */
	obj_t BGl_z62zc3z04anonymousza31940ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5697, obj_t BgL_xz00_5698, obj_t BgL_ez00_5699)
	{
		{	/* Expand/initial.scm 294 */
			{
				obj_t BgL_a1z00_7062;
				obj_t BgL_a2z00_7063;

				if (PAIRP(BgL_xz00_5698))
					{	/* Expand/initial.scm 294 */
						obj_t BgL_cdrzd2331zd2_7080;

						BgL_cdrzd2331zd2_7080 = CDR(((obj_t) BgL_xz00_5698));
						if (PAIRP(BgL_cdrzd2331zd2_7080))
							{	/* Expand/initial.scm 294 */
								obj_t BgL_cdrzd2335zd2_7081;

								BgL_cdrzd2335zd2_7081 = CDR(BgL_cdrzd2331zd2_7080);
								if (PAIRP(BgL_cdrzd2335zd2_7081))
									{	/* Expand/initial.scm 294 */
										if (NULLP(CDR(BgL_cdrzd2335zd2_7081)))
											{	/* Expand/initial.scm 294 */
												BgL_a1z00_7062 = CAR(BgL_cdrzd2331zd2_7080);
												BgL_a2z00_7063 = CAR(BgL_cdrzd2335zd2_7081);
												{	/* Expand/initial.scm 298 */
													bool_t BgL_test4701z00_10624;

													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_7062))
														{	/* Expand/initial.scm 298 */
															BgL_test4701z00_10624 =
																BGl_numberzf3zf3zz__r4_numbers_6_5z00
																(BgL_a2z00_7063);
														}
													else
														{	/* Expand/initial.scm 298 */
															BgL_test4701z00_10624 = ((bool_t) 0);
														}
													if (BgL_test4701z00_10624)
														{	/* Expand/initial.scm 299 */
															bool_t BgL_test4703z00_10628;

															if (INTEGERP(BgL_a1z00_7062))
																{	/* Expand/initial.scm 299 */
																	BgL_test4703z00_10628 =
																		INTEGERP(BgL_a2z00_7063);
																}
															else
																{	/* Expand/initial.scm 299 */
																	BgL_test4703z00_10628 = ((bool_t) 0);
																}
															if (BgL_test4703z00_10628)
																{	/* Expand/initial.scm 299 */
																	return
																		BBOOL(
																		((long) CINT(BgL_a1z00_7062) ==
																			(long) CINT(BgL_a2z00_7063)));
																}
															else
																{	/* Expand/initial.scm 299 */
																	return
																		BBOOL(BGl_2zd3zd3zz__r4_numbers_6_5z00
																		(BgL_a1z00_7062, BgL_a2z00_7063));
																}
														}
													else
														{	/* Expand/initial.scm 300 */
															bool_t BgL_test4705z00_10638;

															if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
																(BgL_a1z00_7062))
																{	/* Expand/initial.scm 300 */
																	BgL_test4705z00_10638 = ((bool_t) 1);
																}
															else
																{	/* Expand/initial.scm 300 */
																	BgL_test4705z00_10638 =
																		BGl_numberzf3zf3zz__r4_numbers_6_5z00
																		(BgL_a2z00_7063);
																}
															if (BgL_test4705z00_10638)
																{	/* Expand/initial.scm 301 */
																	obj_t BgL_arg1954z00_7064;

																	{	/* Expand/initial.scm 301 */
																		obj_t BgL_arg1955z00_7065;

																		{	/* Expand/initial.scm 301 */
																			obj_t BgL_arg1956z00_7066;

																			BgL_arg1956z00_7066 =
																				MAKE_YOUNG_PAIR(BgL_a2z00_7063, BNIL);
																			BgL_arg1955z00_7065 =
																				MAKE_YOUNG_PAIR(BgL_a1z00_7062,
																				BgL_arg1956z00_7066);
																		}
																		BgL_arg1954z00_7064 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(37),
																			BgL_arg1955z00_7065);
																	}
																	{	/* Expand/initial.scm 301 */
																		obj_t BgL_tmpfunz00_10652;

																		BgL_tmpfunz00_10652 =
																			((obj_t) BgL_ez00_5699);
																		return
																			(VA_PROCEDUREP(BgL_tmpfunz00_10652)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_10652)) ((
																					(obj_t) BgL_ez00_5699),
																				BgL_arg1954z00_7064,
																				((obj_t) BgL_ez00_5699),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_10652)) ((
																					(obj_t) BgL_ez00_5699),
																				BgL_arg1954z00_7064,
																				((obj_t) BgL_ez00_5699)));
																	}
																}
															else
																{	/* Expand/initial.scm 302 */
																	bool_t BgL_test4707z00_10654;

																	if (CHARP(BgL_a1z00_7062))
																		{	/* Expand/initial.scm 302 */
																			BgL_test4707z00_10654 = ((bool_t) 1);
																		}
																	else
																		{	/* Expand/initial.scm 302 */
																			if (CHARP(BgL_a2z00_7063))
																				{	/* Expand/initial.scm 303 */
																					BgL_test4707z00_10654 = ((bool_t) 1);
																				}
																			else
																				{	/* Expand/initial.scm 304 */
																					bool_t BgL_test4710z00_10659;

																					if (PAIRP(BgL_a1z00_7062))
																						{	/* Expand/initial.scm 304 */
																							if (
																								(CAR(BgL_a1z00_7062) ==
																									CNST_TABLE_REF(190)))
																								{	/* Expand/initial.scm 306 */
																									bool_t
																										BgL__ortest_1102z00_7067;
																									BgL__ortest_1102z00_7067 =
																										INTEGERP(CAR(CDR
																											(BgL_a1z00_7062)));
																									if (BgL__ortest_1102z00_7067)
																										{	/* Expand/initial.scm 306 */
																											BgL_test4710z00_10659 =
																												BgL__ortest_1102z00_7067;
																										}
																									else
																										{	/* Expand/initial.scm 307 */
																											bool_t
																												BgL__ortest_1105z00_7068;
																											{	/* Expand/initial.scm 307 */
																												obj_t BgL_tmpz00_10670;

																												BgL_tmpz00_10670 =
																													CAR(CDR
																													(BgL_a1z00_7062));
																												BgL__ortest_1105z00_7068
																													=
																													CHARP
																													(BgL_tmpz00_10670);
																											}
																											if (BgL__ortest_1105z00_7068)
																												{	/* Expand/initial.scm 307 */
																													BgL_test4710z00_10659
																														=
																														BgL__ortest_1105z00_7068;
																												}
																											else
																												{	/* Expand/initial.scm 308 */
																													bool_t
																														BgL__ortest_1106z00_7069;
																													{	/* Expand/initial.scm 308 */
																														obj_t
																															BgL_tmpz00_10675;
																														BgL_tmpz00_10675 =
																															CAR(CDR
																															(BgL_a1z00_7062));
																														BgL__ortest_1106z00_7069
																															=
																															SYMBOLP
																															(BgL_tmpz00_10675);
																													}
																													if (BgL__ortest_1106z00_7069)
																														{	/* Expand/initial.scm 308 */
																															BgL_test4710z00_10659
																																=
																																BgL__ortest_1106z00_7069;
																														}
																													else
																														{	/* Expand/initial.scm 309 */
																															obj_t
																																BgL_tmpz00_10680;
																															BgL_tmpz00_10680 =
																																CAR(CDR
																																(BgL_a1z00_7062));
																															BgL_test4710z00_10659
																																=
																																KEYWORDP
																																(BgL_tmpz00_10680);
																														}
																												}
																										}
																								}
																							else
																								{	/* Expand/initial.scm 305 */
																									BgL_test4710z00_10659 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Expand/initial.scm 304 */
																							BgL_test4710z00_10659 =
																								((bool_t) 0);
																						}
																					if (BgL_test4710z00_10659)
																						{	/* Expand/initial.scm 304 */
																							BgL_test4707z00_10654 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Expand/initial.scm 304 */
																							if (PAIRP(BgL_a2z00_7063))
																								{	/* Expand/initial.scm 310 */
																									if (
																										(CAR(BgL_a2z00_7063) ==
																											CNST_TABLE_REF(190)))
																										{	/* Expand/initial.scm 312 */
																											bool_t
																												BgL__ortest_1109z00_7070;
																											BgL__ortest_1109z00_7070 =
																												INTEGERP(CAR(CDR
																													(BgL_a2z00_7063)));
																											if (BgL__ortest_1109z00_7070)
																												{	/* Expand/initial.scm 312 */
																													BgL_test4707z00_10654
																														=
																														BgL__ortest_1109z00_7070;
																												}
																											else
																												{	/* Expand/initial.scm 313 */
																													bool_t
																														BgL__ortest_1111z00_7071;
																													{	/* Expand/initial.scm 313 */
																														obj_t
																															BgL_tmpz00_10694;
																														BgL_tmpz00_10694 =
																															CAR(CDR
																															(BgL_a2z00_7063));
																														BgL__ortest_1111z00_7071
																															=
																															CHARP
																															(BgL_tmpz00_10694);
																													}
																													if (BgL__ortest_1111z00_7071)
																														{	/* Expand/initial.scm 313 */
																															BgL_test4707z00_10654
																																=
																																BgL__ortest_1111z00_7071;
																														}
																													else
																														{	/* Expand/initial.scm 314 */
																															bool_t
																																BgL__ortest_1112z00_7072;
																															{	/* Expand/initial.scm 314 */
																																obj_t
																																	BgL_tmpz00_10699;
																																BgL_tmpz00_10699
																																	=
																																	CAR(CDR
																																	(BgL_a2z00_7063));
																																BgL__ortest_1112z00_7072
																																	=
																																	SYMBOLP
																																	(BgL_tmpz00_10699);
																															}
																															if (BgL__ortest_1112z00_7072)
																																{	/* Expand/initial.scm 314 */
																																	BgL_test4707z00_10654
																																		=
																																		BgL__ortest_1112z00_7072;
																																}
																															else
																																{	/* Expand/initial.scm 315 */
																																	obj_t
																																		BgL_tmpz00_10704;
																																	BgL_tmpz00_10704
																																		=
																																		CAR(CDR
																																		(BgL_a2z00_7063));
																																	BgL_test4707z00_10654
																																		=
																																		KEYWORDP
																																		(BgL_tmpz00_10704);
																																}
																														}
																												}
																										}
																									else
																										{	/* Expand/initial.scm 311 */
																											BgL_test4707z00_10654 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* Expand/initial.scm 310 */
																									BgL_test4707z00_10654 =
																										((bool_t) 0);
																								}
																						}
																				}
																		}
																	if (BgL_test4707z00_10654)
																		{	/* Expand/initial.scm 316 */
																			obj_t BgL_arg1979z00_7073;

																			{	/* Expand/initial.scm 316 */
																				obj_t BgL_arg1980z00_7074;

																				{	/* Expand/initial.scm 316 */
																					obj_t BgL_arg1981z00_7075;

																					BgL_arg1981z00_7075 =
																						MAKE_YOUNG_PAIR(BgL_a2z00_7063,
																						BNIL);
																					BgL_arg1980z00_7074 =
																						MAKE_YOUNG_PAIR(BgL_a1z00_7062,
																						BgL_arg1981z00_7075);
																				}
																				BgL_arg1979z00_7073 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(61),
																					BgL_arg1980z00_7074);
																			}
																			{	/* Expand/initial.scm 316 */
																				obj_t BgL_tmpfunz00_10718;

																				BgL_tmpfunz00_10718 =
																					((obj_t) BgL_ez00_5699);
																				return
																					(VA_PROCEDUREP(BgL_tmpfunz00_10718)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY
																						(BgL_tmpfunz00_10718)) (((obj_t)
																							BgL_ez00_5699),
																						BgL_arg1979z00_7073,
																						((obj_t) BgL_ez00_5699),
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY
																						(BgL_tmpfunz00_10718)) (((obj_t)
																							BgL_ez00_5699),
																						BgL_arg1979z00_7073,
																						((obj_t) BgL_ez00_5699)));
																			}
																		}
																	else
																		{	/* Expand/initial.scm 318 */
																			obj_t BgL_arg1982z00_7076;

																			{	/* Expand/initial.scm 318 */
																				obj_t BgL_arg1983z00_7077;
																				obj_t BgL_arg1984z00_7078;

																				{	/* Expand/initial.scm 318 */
																					obj_t BgL_tmpfunz00_10726;

																					BgL_tmpfunz00_10726 =
																						((obj_t) BgL_ez00_5699);
																					BgL_arg1983z00_7077 =
																						(VA_PROCEDUREP(BgL_tmpfunz00_10726)
																						? ((obj_t(*)(obj_t,
																									...))
																							PROCEDURE_ENTRY
																							(BgL_tmpfunz00_10726)) (((obj_t)
																								BgL_ez00_5699), BgL_a1z00_7062,
																							((obj_t) BgL_ez00_5699),
																							BEOA) : ((obj_t(*)(obj_t, obj_t,
																									obj_t))
																							PROCEDURE_ENTRY
																							(BgL_tmpfunz00_10726)) (((obj_t)
																								BgL_ez00_5699), BgL_a1z00_7062,
																							((obj_t) BgL_ez00_5699)));
																				}
																				{	/* Expand/initial.scm 318 */
																					obj_t BgL_arg1985z00_7079;

																					{	/* Expand/initial.scm 318 */
																						obj_t BgL_tmpfunz00_10734;

																						BgL_tmpfunz00_10734 =
																							((obj_t) BgL_ez00_5699);
																						BgL_arg1985z00_7079 =
																							(VA_PROCEDUREP
																							(BgL_tmpfunz00_10734)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10734)) (((obj_t)
																									BgL_ez00_5699),
																								BgL_a2z00_7063,
																								((obj_t) BgL_ez00_5699),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10734)) (((obj_t)
																									BgL_ez00_5699),
																								BgL_a2z00_7063,
																								((obj_t) BgL_ez00_5699)));
																					}
																					BgL_arg1984z00_7078 =
																						MAKE_YOUNG_PAIR(BgL_arg1985z00_7079,
																						BNIL);
																				}
																				BgL_arg1982z00_7076 =
																					MAKE_YOUNG_PAIR(BgL_arg1983z00_7077,
																					BgL_arg1984z00_7078);
																			}
																			return
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(36),
																				BgL_arg1982z00_7076);
																		}
																}
														}
												}
											}
										else
											{	/* Expand/initial.scm 294 */
											BgL_tagzd2324zd2_7060:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4239z00zzexpand_installz00, BgL_xz00_5698);
											}
									}
								else
									{	/* Expand/initial.scm 294 */
										goto BgL_tagzd2324zd2_7060;
									}
							}
						else
							{	/* Expand/initial.scm 294 */
								goto BgL_tagzd2324zd2_7060;
							}
					}
				else
					{	/* Expand/initial.scm 294 */
						goto BgL_tagzd2324zd2_7060;
					}
			}
		}

	}



/* &<@anonymous:1917> */
	obj_t BGl_z62zc3z04anonymousza31917ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5700, obj_t BgL_xz00_5701, obj_t BgL_ez00_5702)
	{
		{	/* Expand/initial.scm 283 */
			{	/* Expand/initial.scm 284 */
				obj_t BgL_resz00_7083;

				if (NULLP(BgL_xz00_5701))
					{	/* Expand/initial.scm 284 */
						BgL_resz00_7083 = BNIL;
					}
				else
					{	/* Expand/initial.scm 284 */
						obj_t BgL_head1283z00_7084;

						BgL_head1283z00_7084 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1281z00_7086;
							obj_t BgL_tail1284z00_7087;

							BgL_l1281z00_7086 = BgL_xz00_5701;
							BgL_tail1284z00_7087 = BgL_head1283z00_7084;
						BgL_zc3z04anonymousza31935ze3z87_7085:
							if (NULLP(BgL_l1281z00_7086))
								{	/* Expand/initial.scm 284 */
									BgL_resz00_7083 = CDR(BgL_head1283z00_7084);
								}
							else
								{	/* Expand/initial.scm 284 */
									obj_t BgL_newtail1285z00_7088;

									{	/* Expand/initial.scm 284 */
										obj_t BgL_arg1938z00_7089;

										{	/* Expand/initial.scm 284 */
											obj_t BgL_xz00_7090;

											BgL_xz00_7090 = CAR(((obj_t) BgL_l1281z00_7086));
											{	/* Expand/initial.scm 284 */
												obj_t BgL_tmpfunz00_10757;

												BgL_tmpfunz00_10757 = ((obj_t) BgL_ez00_5702);
												BgL_arg1938z00_7089 =
													(VA_PROCEDUREP(BgL_tmpfunz00_10757)
													? ((obj_t(*)(obj_t,
																...))
														PROCEDURE_ENTRY(BgL_tmpfunz00_10757)) (((obj_t)
															BgL_ez00_5702), BgL_xz00_7090,
														((obj_t) BgL_ez00_5702), BEOA) : ((obj_t(*)(obj_t,
																obj_t,
																obj_t))
														PROCEDURE_ENTRY(BgL_tmpfunz00_10757)) (((obj_t)
															BgL_ez00_5702), BgL_xz00_7090,
														((obj_t) BgL_ez00_5702)));
											}
										}
										BgL_newtail1285z00_7088 =
											MAKE_YOUNG_PAIR(BgL_arg1938z00_7089, BNIL);
									}
									SET_CDR(BgL_tail1284z00_7087, BgL_newtail1285z00_7088);
									{	/* Expand/initial.scm 284 */
										obj_t BgL_arg1937z00_7091;

										BgL_arg1937z00_7091 = CDR(((obj_t) BgL_l1281z00_7086));
										{
											obj_t BgL_tail1284z00_10764;
											obj_t BgL_l1281z00_10763;

											BgL_l1281z00_10763 = BgL_arg1937z00_7091;
											BgL_tail1284z00_10764 = BgL_newtail1285z00_7088;
											BgL_tail1284z00_7087 = BgL_tail1284z00_10764;
											BgL_l1281z00_7086 = BgL_l1281z00_10763;
											goto BgL_zc3z04anonymousza31935ze3z87_7085;
										}
									}
								}
						}
					}
				if (PAIRP(BgL_resz00_7083))
					{	/* Expand/initial.scm 285 */
						obj_t BgL_cdrzd2314zd2_7092;

						BgL_cdrzd2314zd2_7092 = CDR(BgL_resz00_7083);
						if (PAIRP(BgL_cdrzd2314zd2_7092))
							{	/* Expand/initial.scm 285 */
								obj_t BgL_cdrzd2317zd2_7093;

								BgL_cdrzd2317zd2_7093 = CDR(BgL_cdrzd2314zd2_7092);
								if (PAIRP(BgL_cdrzd2317zd2_7093))
									{	/* Expand/initial.scm 285 */
										obj_t BgL_cdrzd2320zd2_7094;

										BgL_cdrzd2320zd2_7094 = CDR(BgL_cdrzd2317zd2_7093);
										if ((CAR(BgL_cdrzd2317zd2_7093) == BTRUE))
											{	/* Expand/initial.scm 285 */
												if (PAIRP(BgL_cdrzd2320zd2_7094))
													{	/* Expand/initial.scm 285 */
														if ((CAR(BgL_cdrzd2320zd2_7094) == BFALSE))
															{	/* Expand/initial.scm 285 */
																if (NULLP(CDR(BgL_cdrzd2320zd2_7094)))
																	{	/* Expand/initial.scm 285 */
																		return CAR(BgL_cdrzd2314zd2_7092);
																	}
																else
																	{	/* Expand/initial.scm 285 */
																		return BgL_resz00_7083;
																	}
															}
														else
															{	/* Expand/initial.scm 285 */
																return BgL_resz00_7083;
															}
													}
												else
													{	/* Expand/initial.scm 285 */
														return BgL_resz00_7083;
													}
											}
										else
											{	/* Expand/initial.scm 285 */
												return BgL_resz00_7083;
											}
									}
								else
									{	/* Expand/initial.scm 285 */
										return BgL_resz00_7083;
									}
							}
						else
							{	/* Expand/initial.scm 285 */
								return BgL_resz00_7083;
							}
					}
				else
					{	/* Expand/initial.scm 285 */
						return BgL_resz00_7083;
					}
			}
		}

	}



/* &<@anonymous:1915> */
	obj_t BGl_z62zc3z04anonymousza31915ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5703, obj_t BgL_xz00_5704, obj_t BgL_ez00_5705)
	{
		{	/* Expand/initial.scm 272 */
			return
				BGl_mapzd2checkzd2zzexpand_installz00(BgL_xz00_5704,
				((obj_t) BgL_ez00_5705), BTRUE);
		}

	}



/* &<@anonymous:1913> */
	obj_t BGl_z62zc3z04anonymousza31913ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5706, obj_t BgL_xz00_5707, obj_t BgL_ez00_5708)
	{
		{	/* Expand/initial.scm 268 */
			return
				BGl_mapzd2checkzd2zzexpand_installz00(BgL_xz00_5707,
				((obj_t) BgL_ez00_5708), BFALSE);
		}

	}



/* &<@anonymous:1911> */
	obj_t BGl_z62zc3z04anonymousza31911ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5709, obj_t BgL_xz00_5710, obj_t BgL_ez00_5711)
	{
		{	/* Expand/initial.scm 262 */
			return
				BGl_mapzd2checkzd2zzexpand_installz00(BgL_xz00_5710,
				((obj_t) BgL_ez00_5711), BUNSPEC);
		}

	}



/* &<@anonymous:1907> */
	obj_t BGl_z62zc3z04anonymousza31907ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5712, obj_t BgL_xz00_5713, obj_t BgL_ez00_5714)
	{
		{	/* Expand/initial.scm 259 */
			return
				BGl_mapzd2checkzd2zzexpand_installz00(BgL_xz00_5713,
				((obj_t) BgL_ez00_5714), BNIL);
		}

	}



/* &<@anonymous:1904> */
	obj_t BGl_z62zc3z04anonymousza31904ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5715, obj_t BgL_xz00_5716, obj_t BgL_ez00_5717)
	{
		{	/* Expand/initial.scm 253 */
			return
				BGl_mapzd2checkzd2zzexpand_installz00(BgL_xz00_5716,
				((obj_t) BgL_ez00_5717), BUNSPEC);
		}

	}



/* &<@anonymous:1902> */
	obj_t BGl_z62zc3z04anonymousza31902ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5718, obj_t BgL_xz00_5719, obj_t BgL_ez00_5720)
	{
		{	/* Expand/initial.scm 247 */
			return
				BGl_mapzd2checkzd2zzexpand_installz00(BgL_xz00_5719,
				((obj_t) BgL_ez00_5720), CNST_TABLE_REF(205));
		}

	}



/* &<@anonymous:1900> */
	obj_t BGl_z62zc3z04anonymousza31900ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5721, obj_t BgL_xz00_5722, obj_t BgL_ez00_5723)
	{
		{	/* Expand/initial.scm 241 */
			return
				BGl_mapzd2checkzd2zzexpand_installz00(BgL_xz00_5722,
				((obj_t) BgL_ez00_5723), CNST_TABLE_REF(205));
		}

	}



/* &<@anonymous:1898> */
	obj_t BGl_z62zc3z04anonymousza31898ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5724, obj_t BgL_xz00_5725, obj_t BgL_ez00_5726)
	{
		{	/* Expand/initial.scm 236 */
			return
				BGl_mapzd2checkzd2zzexpand_installz00(BgL_xz00_5725,
				((obj_t) BgL_ez00_5726), CNST_TABLE_REF(205));
		}

	}



/* &<@anonymous:1883> */
	obj_t BGl_z62zc3z04anonymousza31883ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5727, obj_t BgL_xz00_5728, obj_t BgL_ez00_5729)
	{
		{	/* Expand/initial.scm 226 */
			{
				obj_t BgL_az00_7106;
				obj_t BgL_dz00_7107;

				if (PAIRP(BgL_xz00_5728))
					{	/* Expand/initial.scm 226 */
						obj_t BgL_cdrzd2298zd2_7112;

						BgL_cdrzd2298zd2_7112 = CDR(((obj_t) BgL_xz00_5728));
						if (PAIRP(BgL_cdrzd2298zd2_7112))
							{	/* Expand/initial.scm 226 */
								obj_t BgL_cdrzd2302zd2_7113;

								BgL_cdrzd2302zd2_7113 = CDR(BgL_cdrzd2298zd2_7112);
								if (PAIRP(BgL_cdrzd2302zd2_7113))
									{	/* Expand/initial.scm 226 */
										if (NULLP(CDR(BgL_cdrzd2302zd2_7113)))
											{	/* Expand/initial.scm 226 */
												BgL_az00_7106 = CAR(BgL_cdrzd2298zd2_7112);
												BgL_dz00_7107 = CAR(BgL_cdrzd2302zd2_7113);
												{	/* Expand/initial.scm 229 */
													obj_t BgL_arg1892z00_7108;

													{	/* Expand/initial.scm 229 */
														obj_t BgL_arg1893z00_7109;
														obj_t BgL_arg1894z00_7110;

														{	/* Expand/initial.scm 229 */
															obj_t BgL_tmpfunz00_10823;

															BgL_tmpfunz00_10823 = ((obj_t) BgL_ez00_5729);
															BgL_arg1893z00_7109 =
																(VA_PROCEDUREP(BgL_tmpfunz00_10823)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_10823)) ((
																		(obj_t) BgL_ez00_5729), BgL_az00_7106,
																	((obj_t) BgL_ez00_5729),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_10823)) ((
																		(obj_t) BgL_ez00_5729), BgL_az00_7106,
																	((obj_t) BgL_ez00_5729)));
														}
														{	/* Expand/initial.scm 229 */
															obj_t BgL_arg1896z00_7111;

															{	/* Expand/initial.scm 229 */
																obj_t BgL_tmpfunz00_10831;

																BgL_tmpfunz00_10831 = ((obj_t) BgL_ez00_5729);
																BgL_arg1896z00_7111 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_10831)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_10831)) ((
																			(obj_t) BgL_ez00_5729), BgL_dz00_7107,
																		((obj_t) BgL_ez00_5729),
																		BEOA) : ((obj_t(*)(obj_t, obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_10831)) ((
																			(obj_t) BgL_ez00_5729), BgL_dz00_7107,
																		((obj_t) BgL_ez00_5729)));
															}
															BgL_arg1894z00_7110 =
																MAKE_YOUNG_PAIR(BgL_arg1896z00_7111, BNIL);
														}
														BgL_arg1892z00_7108 =
															MAKE_YOUNG_PAIR(BgL_arg1893z00_7109,
															BgL_arg1894z00_7110);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(178),
														BgL_arg1892z00_7108);
												}
											}
										else
											{	/* Expand/initial.scm 226 */
											BgL_tagzd2291zd2_7104:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4240z00zzexpand_installz00, BgL_xz00_5728);
											}
									}
								else
									{	/* Expand/initial.scm 226 */
										goto BgL_tagzd2291zd2_7104;
									}
							}
						else
							{	/* Expand/initial.scm 226 */
								goto BgL_tagzd2291zd2_7104;
							}
					}
				else
					{	/* Expand/initial.scm 226 */
						goto BgL_tagzd2291zd2_7104;
					}
			}
		}

	}



/* &<@anonymous:1871> */
	obj_t BGl_z62zc3z04anonymousza31871ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5730, obj_t BgL_xz00_5731, obj_t BgL_ez00_5732)
	{
		{	/* Expand/initial.scm 214 */
			{
				obj_t BgL_sz00_7117;

				if (PAIRP(BgL_xz00_5731))
					{	/* Expand/initial.scm 214 */
						obj_t BgL_cdrzd2285zd2_7120;

						BgL_cdrzd2285zd2_7120 = CDR(((obj_t) BgL_xz00_5731));
						if (PAIRP(BgL_cdrzd2285zd2_7120))
							{	/* Expand/initial.scm 214 */
								if (NULLP(CDR(BgL_cdrzd2285zd2_7120)))
									{	/* Expand/initial.scm 214 */
										BgL_sz00_7117 = CAR(BgL_cdrzd2285zd2_7120);
										if (STRINGP(BgL_sz00_7117))
											{	/* Expand/initial.scm 217 */
												return BINT(STRING_LENGTH(BgL_sz00_7117));
											}
										else
											{	/* Expand/initial.scm 219 */
												obj_t BgL_arg1879z00_7118;

												{	/* Expand/initial.scm 219 */
													obj_t BgL_arg1880z00_7119;

													{	/* Expand/initial.scm 219 */
														obj_t BgL_tmpfunz00_10859;

														BgL_tmpfunz00_10859 = ((obj_t) BgL_ez00_5732);
														BgL_arg1880z00_7119 =
															(VA_PROCEDUREP(BgL_tmpfunz00_10859)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10859)) (((obj_t)
																	BgL_ez00_5732), BgL_sz00_7117,
																((obj_t) BgL_ez00_5732),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10859)) (((obj_t)
																	BgL_ez00_5732), BgL_sz00_7117,
																((obj_t) BgL_ez00_5732)));
													}
													BgL_arg1879z00_7118 =
														MAKE_YOUNG_PAIR(BgL_arg1880z00_7119, BNIL);
												}
												return
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
													BgL_arg1879z00_7118);
											}
									}
								else
									{	/* Expand/initial.scm 214 */
									BgL_tagzd2280zd2_7115:
										return
											BGl_errorz00zz__errorz00(BFALSE,
											BGl_string4241z00zzexpand_installz00, BgL_xz00_5731);
									}
							}
						else
							{	/* Expand/initial.scm 214 */
								goto BgL_tagzd2280zd2_7115;
							}
					}
				else
					{	/* Expand/initial.scm 214 */
						goto BgL_tagzd2280zd2_7115;
					}
			}
		}

	}



/* &<@anonymous:1846> */
	obj_t BGl_z62zc3z04anonymousza31846ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5733, obj_t BgL_xz00_5734, obj_t BgL_ez00_5735)
	{
		{	/* Expand/initial.scm 202 */
			{
				obj_t BgL_l1z00_7136;
				obj_t BgL_l2z00_7137;
				obj_t BgL_listsz00_7125;

				if (PAIRP(BgL_xz00_5734))
					{	/* Expand/initial.scm 202 */
						obj_t BgL_cdrzd2252zd2_7141;

						BgL_cdrzd2252zd2_7141 = CDR(((obj_t) BgL_xz00_5734));
						if (PAIRP(BgL_cdrzd2252zd2_7141))
							{	/* Expand/initial.scm 202 */
								obj_t BgL_cdrzd2256zd2_7142;

								BgL_cdrzd2256zd2_7142 = CDR(BgL_cdrzd2252zd2_7141);
								if (PAIRP(BgL_cdrzd2256zd2_7142))
									{	/* Expand/initial.scm 202 */
										if (NULLP(CDR(BgL_cdrzd2256zd2_7142)))
											{	/* Expand/initial.scm 202 */
												BgL_l1z00_7136 = CAR(BgL_cdrzd2252zd2_7141);
												BgL_l2z00_7137 = CAR(BgL_cdrzd2256zd2_7142);
												{	/* Expand/initial.scm 205 */
													obj_t BgL_arg1859z00_7138;

													{	/* Expand/initial.scm 205 */
														obj_t BgL_arg1860z00_7139;

														{	/* Expand/initial.scm 205 */
															obj_t BgL_arg1862z00_7140;

															BgL_arg1862z00_7140 =
																MAKE_YOUNG_PAIR(BgL_l2z00_7137, BNIL);
															BgL_arg1860z00_7139 =
																MAKE_YOUNG_PAIR(BgL_l1z00_7136,
																BgL_arg1862z00_7140);
														}
														BgL_arg1859z00_7138 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(206),
															BgL_arg1860z00_7139);
													}
													{	/* Expand/initial.scm 205 */
														obj_t BgL_tmpfunz00_10888;

														BgL_tmpfunz00_10888 = ((obj_t) BgL_ez00_5735);
														return
															(VA_PROCEDUREP(BgL_tmpfunz00_10888)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10888)) (((obj_t)
																	BgL_ez00_5735), BgL_arg1859z00_7138,
																((obj_t) BgL_ez00_5735),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10888)) (((obj_t)
																	BgL_ez00_5735), BgL_arg1859z00_7138,
																((obj_t) BgL_ez00_5735)));
													}
												}
											}
										else
											{	/* Expand/initial.scm 202 */
												BgL_listsz00_7125 = BgL_cdrzd2252zd2_7141;
											BgL_tagzd2243zd2_7123:
												{	/* Expand/initial.scm 207 */
													obj_t BgL_arg1863z00_7126;

													{	/* Expand/initial.scm 207 */
														obj_t BgL_arg1864z00_7127;

														if (NULLP(BgL_listsz00_7125))
															{	/* Expand/initial.scm 207 */
																BgL_arg1864z00_7127 = BNIL;
															}
														else
															{	/* Expand/initial.scm 207 */
																obj_t BgL_head1278z00_7128;

																BgL_head1278z00_7128 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1276z00_7130;
																	obj_t BgL_tail1279z00_7131;

																	BgL_l1276z00_7130 = BgL_listsz00_7125;
																	BgL_tail1279z00_7131 = BgL_head1278z00_7128;
																BgL_zc3z04anonymousza31866ze3z87_7129:
																	if (NULLP(BgL_l1276z00_7130))
																		{	/* Expand/initial.scm 207 */
																			BgL_arg1864z00_7127 =
																				CDR(BgL_head1278z00_7128);
																		}
																	else
																		{	/* Expand/initial.scm 207 */
																			obj_t BgL_newtail1280z00_7132;

																			{	/* Expand/initial.scm 207 */
																				obj_t BgL_arg1869z00_7133;

																				{	/* Expand/initial.scm 207 */
																					obj_t BgL_lz00_7134;

																					BgL_lz00_7134 =
																						CAR(((obj_t) BgL_l1276z00_7130));
																					{	/* Expand/initial.scm 207 */
																						obj_t BgL_tmpfunz00_10906;

																						BgL_tmpfunz00_10906 =
																							((obj_t) BgL_ez00_5735);
																						BgL_arg1869z00_7133 =
																							(VA_PROCEDUREP
																							(BgL_tmpfunz00_10906)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10906)) (((obj_t)
																									BgL_ez00_5735), BgL_lz00_7134,
																								((obj_t) BgL_ez00_5735),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10906)) (((obj_t)
																									BgL_ez00_5735), BgL_lz00_7134,
																								((obj_t) BgL_ez00_5735)));
																					}
																				}
																				BgL_newtail1280z00_7132 =
																					MAKE_YOUNG_PAIR(BgL_arg1869z00_7133,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1279z00_7131,
																				BgL_newtail1280z00_7132);
																			{	/* Expand/initial.scm 207 */
																				obj_t BgL_arg1868z00_7135;

																				BgL_arg1868z00_7135 =
																					CDR(((obj_t) BgL_l1276z00_7130));
																				{
																					obj_t BgL_tail1279z00_10913;
																					obj_t BgL_l1276z00_10912;

																					BgL_l1276z00_10912 =
																						BgL_arg1868z00_7135;
																					BgL_tail1279z00_10913 =
																						BgL_newtail1280z00_7132;
																					BgL_tail1279z00_7131 =
																						BgL_tail1279z00_10913;
																					BgL_l1276z00_7130 =
																						BgL_l1276z00_10912;
																					goto
																						BgL_zc3z04anonymousza31866ze3z87_7129;
																				}
																			}
																		}
																}
															}
														BgL_arg1863z00_7126 =
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_arg1864z00_7127, BNIL);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(23),
														BgL_arg1863z00_7126);
												}
											}
									}
								else
									{
										obj_t BgL_listsz00_10917;

										BgL_listsz00_10917 = BgL_cdrzd2252zd2_7141;
										BgL_listsz00_7125 = BgL_listsz00_10917;
										goto BgL_tagzd2243zd2_7123;
									}
							}
						else
							{
								obj_t BgL_listsz00_10918;

								BgL_listsz00_10918 = BgL_cdrzd2252zd2_7141;
								BgL_listsz00_7125 = BgL_listsz00_10918;
								goto BgL_tagzd2243zd2_7123;
							}
					}
				else
					{	/* Expand/initial.scm 202 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string4242z00zzexpand_installz00, BgL_xz00_5734);
					}
			}
		}

	}



/* &<@anonymous:1809> */
	obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5736, obj_t BgL_xz00_5737, obj_t BgL_ez00_5738)
	{
		{	/* Expand/initial.scm 190 */
			{
				obj_t BgL_l1z00_7158;
				obj_t BgL_l2z00_7159;
				obj_t BgL_listsz00_7147;

				if (PAIRP(BgL_xz00_5737))
					{	/* Expand/initial.scm 190 */
						obj_t BgL_cdrzd2215zd2_7163;

						BgL_cdrzd2215zd2_7163 = CDR(((obj_t) BgL_xz00_5737));
						if (PAIRP(BgL_cdrzd2215zd2_7163))
							{	/* Expand/initial.scm 190 */
								obj_t BgL_cdrzd2219zd2_7164;

								BgL_cdrzd2219zd2_7164 = CDR(BgL_cdrzd2215zd2_7163);
								if (PAIRP(BgL_cdrzd2219zd2_7164))
									{	/* Expand/initial.scm 190 */
										if (NULLP(CDR(BgL_cdrzd2219zd2_7164)))
											{	/* Expand/initial.scm 190 */
												BgL_l1z00_7158 = CAR(BgL_cdrzd2215zd2_7163);
												BgL_l2z00_7159 = CAR(BgL_cdrzd2219zd2_7164);
												{	/* Expand/initial.scm 193 */
													obj_t BgL_arg1835z00_7160;

													{	/* Expand/initial.scm 193 */
														obj_t BgL_arg1836z00_7161;

														{	/* Expand/initial.scm 193 */
															obj_t BgL_arg1837z00_7162;

															BgL_arg1837z00_7162 =
																MAKE_YOUNG_PAIR(BgL_l2z00_7159, BNIL);
															BgL_arg1836z00_7161 =
																MAKE_YOUNG_PAIR(BgL_l1z00_7158,
																BgL_arg1837z00_7162);
														}
														BgL_arg1835z00_7160 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(207),
															BgL_arg1836z00_7161);
													}
													{	/* Expand/initial.scm 193 */
														obj_t BgL_tmpfunz00_10942;

														BgL_tmpfunz00_10942 = ((obj_t) BgL_ez00_5738);
														return
															(VA_PROCEDUREP(BgL_tmpfunz00_10942)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10942)) (((obj_t)
																	BgL_ez00_5738), BgL_arg1835z00_7160,
																((obj_t) BgL_ez00_5738),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_10942)) (((obj_t)
																	BgL_ez00_5738), BgL_arg1835z00_7160,
																((obj_t) BgL_ez00_5738)));
													}
												}
											}
										else
											{	/* Expand/initial.scm 190 */
												BgL_listsz00_7147 = BgL_cdrzd2215zd2_7163;
											BgL_tagzd2206zd2_7145:
												{	/* Expand/initial.scm 195 */
													obj_t BgL_arg1838z00_7148;

													{	/* Expand/initial.scm 195 */
														obj_t BgL_arg1839z00_7149;

														if (NULLP(BgL_listsz00_7147))
															{	/* Expand/initial.scm 195 */
																BgL_arg1839z00_7149 = BNIL;
															}
														else
															{	/* Expand/initial.scm 195 */
																obj_t BgL_head1273z00_7150;

																BgL_head1273z00_7150 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1271z00_7152;
																	obj_t BgL_tail1274z00_7153;

																	BgL_l1271z00_7152 = BgL_listsz00_7147;
																	BgL_tail1274z00_7153 = BgL_head1273z00_7150;
																BgL_zc3z04anonymousza31841ze3z87_7151:
																	if (NULLP(BgL_l1271z00_7152))
																		{	/* Expand/initial.scm 195 */
																			BgL_arg1839z00_7149 =
																				CDR(BgL_head1273z00_7150);
																		}
																	else
																		{	/* Expand/initial.scm 195 */
																			obj_t BgL_newtail1275z00_7154;

																			{	/* Expand/initial.scm 195 */
																				obj_t BgL_arg1844z00_7155;

																				{	/* Expand/initial.scm 195 */
																					obj_t BgL_lz00_7156;

																					BgL_lz00_7156 =
																						CAR(((obj_t) BgL_l1271z00_7152));
																					{	/* Expand/initial.scm 195 */
																						obj_t BgL_tmpfunz00_10960;

																						BgL_tmpfunz00_10960 =
																							((obj_t) BgL_ez00_5738);
																						BgL_arg1844z00_7155 =
																							(VA_PROCEDUREP
																							(BgL_tmpfunz00_10960)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10960)) (((obj_t)
																									BgL_ez00_5738), BgL_lz00_7156,
																								((obj_t) BgL_ez00_5738),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_10960)) (((obj_t)
																									BgL_ez00_5738), BgL_lz00_7156,
																								((obj_t) BgL_ez00_5738)));
																					}
																				}
																				BgL_newtail1275z00_7154 =
																					MAKE_YOUNG_PAIR(BgL_arg1844z00_7155,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1274z00_7153,
																				BgL_newtail1275z00_7154);
																			{	/* Expand/initial.scm 195 */
																				obj_t BgL_arg1843z00_7157;

																				BgL_arg1843z00_7157 =
																					CDR(((obj_t) BgL_l1271z00_7152));
																				{
																					obj_t BgL_tail1274z00_10967;
																					obj_t BgL_l1271z00_10966;

																					BgL_l1271z00_10966 =
																						BgL_arg1843z00_7157;
																					BgL_tail1274z00_10967 =
																						BgL_newtail1275z00_7154;
																					BgL_tail1274z00_7153 =
																						BgL_tail1274z00_10967;
																					BgL_l1271z00_7152 =
																						BgL_l1271z00_10966;
																					goto
																						BgL_zc3z04anonymousza31841ze3z87_7151;
																				}
																			}
																		}
																}
															}
														BgL_arg1838z00_7148 =
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_arg1839z00_7149, BNIL);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
														BgL_arg1838z00_7148);
												}
											}
									}
								else
									{
										obj_t BgL_listsz00_10971;

										BgL_listsz00_10971 = BgL_cdrzd2215zd2_7163;
										BgL_listsz00_7147 = BgL_listsz00_10971;
										goto BgL_tagzd2206zd2_7145;
									}
							}
						else
							{
								obj_t BgL_listsz00_10972;

								BgL_listsz00_10972 = BgL_cdrzd2215zd2_7163;
								BgL_listsz00_7147 = BgL_listsz00_10972;
								goto BgL_tagzd2206zd2_7145;
							}
					}
				else
					{	/* Expand/initial.scm 190 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string4243z00zzexpand_installz00, BgL_xz00_5737);
					}
			}
		}

	}



/* &<@anonymous:1762> */
	obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5739, obj_t BgL_xz00_5740, obj_t BgL_ez00_5741)
	{
		{	/* Expand/initial.scm 178 */
			{
				obj_t BgL_l1z00_7168;
				obj_t BgL_l2z00_7169;

				if (PAIRP(BgL_xz00_5740))
					{	/* Expand/initial.scm 178 */
						obj_t BgL_cdrzd2195zd2_7181;

						BgL_cdrzd2195zd2_7181 = CDR(((obj_t) BgL_xz00_5740));
						if (PAIRP(BgL_cdrzd2195zd2_7181))
							{	/* Expand/initial.scm 178 */
								obj_t BgL_cdrzd2199zd2_7182;

								BgL_cdrzd2199zd2_7182 = CDR(BgL_cdrzd2195zd2_7181);
								if (PAIRP(BgL_cdrzd2199zd2_7182))
									{	/* Expand/initial.scm 178 */
										if (NULLP(CDR(BgL_cdrzd2199zd2_7182)))
											{	/* Expand/initial.scm 178 */
												BgL_l1z00_7168 = CAR(BgL_cdrzd2195zd2_7181);
												BgL_l2z00_7169 = CAR(BgL_cdrzd2199zd2_7182);
												if (
													((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >=
														2L))
													{	/* Expand/initial.scm 182 */
														obj_t BgL_arg1775z00_7170;

														{	/* Expand/initial.scm 182 */
															obj_t BgL_arg1798z00_7171;

															{	/* Expand/initial.scm 182 */
																obj_t BgL_arg1799z00_7172;

																BgL_arg1799z00_7172 =
																	MAKE_YOUNG_PAIR(BgL_l2z00_7169, BNIL);
																BgL_arg1798z00_7171 =
																	MAKE_YOUNG_PAIR(BgL_l1z00_7168,
																	BgL_arg1799z00_7172);
															}
															BgL_arg1775z00_7170 =
																MAKE_YOUNG_PAIR
																(BGl_z52appendzd22zd2idz52zzexpand_installz00,
																BgL_arg1798z00_7171);
														}
														{	/* Expand/initial.scm 182 */
															obj_t BgL_tmpfunz00_10998;

															BgL_tmpfunz00_10998 = ((obj_t) BgL_ez00_5741);
															return
																(VA_PROCEDUREP(BgL_tmpfunz00_10998)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_10998)) ((
																		(obj_t) BgL_ez00_5741), BgL_arg1775z00_7170,
																	((obj_t) BgL_ez00_5741),
																	BEOA) : ((obj_t(*)(obj_t, obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_10998)) ((
																		(obj_t) BgL_ez00_5741), BgL_arg1775z00_7170,
																	((obj_t) BgL_ez00_5741)));
														}
													}
												else
													{	/* Expand/initial.scm 181 */
														if (NULLP(BgL_xz00_5740))
															{	/* Expand/initial.scm 183 */
																return BNIL;
															}
														else
															{	/* Expand/initial.scm 183 */
																obj_t BgL_head1268z00_7173;

																BgL_head1268z00_7173 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1266z00_7175;
																	obj_t BgL_tail1269z00_7176;

																	BgL_l1266z00_7175 = BgL_xz00_5740;
																	BgL_tail1269z00_7176 = BgL_head1268z00_7173;
																BgL_zc3z04anonymousza31801ze3z87_7174:
																	if (NULLP(BgL_l1266z00_7175))
																		{	/* Expand/initial.scm 183 */
																			return CDR(BgL_head1268z00_7173);
																		}
																	else
																		{	/* Expand/initial.scm 183 */
																			obj_t BgL_newtail1270z00_7177;

																			{	/* Expand/initial.scm 183 */
																				obj_t BgL_arg1806z00_7178;

																				{	/* Expand/initial.scm 183 */
																					obj_t BgL_xz00_7179;

																					BgL_xz00_7179 =
																						CAR(((obj_t) BgL_l1266z00_7175));
																					{	/* Expand/initial.scm 183 */
																						obj_t BgL_tmpfunz00_11014;

																						BgL_tmpfunz00_11014 =
																							((obj_t) BgL_ez00_5741);
																						BgL_arg1806z00_7178 =
																							(VA_PROCEDUREP
																							(BgL_tmpfunz00_11014)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_11014)) (((obj_t)
																									BgL_ez00_5741), BgL_xz00_7179,
																								((obj_t) BgL_ez00_5741),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_11014)) (((obj_t)
																									BgL_ez00_5741), BgL_xz00_7179,
																								((obj_t) BgL_ez00_5741)));
																					}
																				}
																				BgL_newtail1270z00_7177 =
																					MAKE_YOUNG_PAIR(BgL_arg1806z00_7178,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1269z00_7176,
																				BgL_newtail1270z00_7177);
																			{	/* Expand/initial.scm 183 */
																				obj_t BgL_arg1805z00_7180;

																				BgL_arg1805z00_7180 =
																					CDR(((obj_t) BgL_l1266z00_7175));
																				{
																					obj_t BgL_tail1269z00_11021;
																					obj_t BgL_l1266z00_11020;

																					BgL_l1266z00_11020 =
																						BgL_arg1805z00_7180;
																					BgL_tail1269z00_11021 =
																						BgL_newtail1270z00_7177;
																					BgL_tail1269z00_7176 =
																						BgL_tail1269z00_11021;
																					BgL_l1266z00_7175 =
																						BgL_l1266z00_11020;
																					goto
																						BgL_zc3z04anonymousza31801ze3z87_7174;
																				}
																			}
																		}
																}
															}
													}
											}
										else
											{	/* Expand/initial.scm 178 */
											BgL_tagzd2188zd2_7166:
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string4244z00zzexpand_installz00, BgL_xz00_5740);
											}
									}
								else
									{	/* Expand/initial.scm 178 */
										goto BgL_tagzd2188zd2_7166;
									}
							}
						else
							{	/* Expand/initial.scm 178 */
								goto BgL_tagzd2188zd2_7166;
							}
					}
				else
					{	/* Expand/initial.scm 178 */
						goto BgL_tagzd2188zd2_7166;
					}
			}
		}

	}



/* &<@anonymous:1721> */
	obj_t BGl_z62zc3z04anonymousza31721ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5742, obj_t BgL_xz00_5743, obj_t BgL_ez00_5744)
	{
		{	/* Expand/initial.scm 167 */
			{
				obj_t BgL_l1z00_7198;
				obj_t BgL_l2z00_7199;
				obj_t BgL_listsz00_7187;

				if (PAIRP(BgL_xz00_5743))
					{	/* Expand/initial.scm 167 */
						obj_t BgL_cdrzd2160zd2_7203;

						BgL_cdrzd2160zd2_7203 = CDR(((obj_t) BgL_xz00_5743));
						if (PAIRP(BgL_cdrzd2160zd2_7203))
							{	/* Expand/initial.scm 167 */
								obj_t BgL_cdrzd2164zd2_7204;

								BgL_cdrzd2164zd2_7204 = CDR(BgL_cdrzd2160zd2_7203);
								if (PAIRP(BgL_cdrzd2164zd2_7204))
									{	/* Expand/initial.scm 167 */
										if (NULLP(CDR(BgL_cdrzd2164zd2_7204)))
											{	/* Expand/initial.scm 167 */
												BgL_l1z00_7198 = CAR(BgL_cdrzd2160zd2_7203);
												BgL_l2z00_7199 = CAR(BgL_cdrzd2164zd2_7204);
												{	/* Expand/initial.scm 170 */
													obj_t BgL_arg1746z00_7200;

													{	/* Expand/initial.scm 170 */
														obj_t BgL_arg1747z00_7201;

														{	/* Expand/initial.scm 170 */
															obj_t BgL_arg1748z00_7202;

															BgL_arg1748z00_7202 =
																MAKE_YOUNG_PAIR(BgL_l2z00_7199, BNIL);
															BgL_arg1747z00_7201 =
																MAKE_YOUNG_PAIR(BgL_l1z00_7198,
																BgL_arg1748z00_7202);
														}
														BgL_arg1746z00_7200 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
															BgL_arg1747z00_7201);
													}
													{	/* Expand/initial.scm 170 */
														obj_t BgL_tmpfunz00_11047;

														BgL_tmpfunz00_11047 = ((obj_t) BgL_ez00_5744);
														return
															(VA_PROCEDUREP(BgL_tmpfunz00_11047)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_11047)) (((obj_t)
																	BgL_ez00_5744), BgL_arg1746z00_7200,
																((obj_t) BgL_ez00_5744),
																BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_11047)) (((obj_t)
																	BgL_ez00_5744), BgL_arg1746z00_7200,
																((obj_t) BgL_ez00_5744)));
													}
												}
											}
										else
											{	/* Expand/initial.scm 167 */
												BgL_listsz00_7187 = BgL_cdrzd2160zd2_7203;
											BgL_tagzd2151zd2_7185:
												{	/* Expand/initial.scm 172 */
													obj_t BgL_arg1749z00_7188;

													{	/* Expand/initial.scm 172 */
														obj_t BgL_arg1750z00_7189;

														if (NULLP(BgL_listsz00_7187))
															{	/* Expand/initial.scm 172 */
																BgL_arg1750z00_7189 = BNIL;
															}
														else
															{	/* Expand/initial.scm 172 */
																obj_t BgL_head1263z00_7190;

																BgL_head1263z00_7190 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1261z00_7192;
																	obj_t BgL_tail1264z00_7193;

																	BgL_l1261z00_7192 = BgL_listsz00_7187;
																	BgL_tail1264z00_7193 = BgL_head1263z00_7190;
																BgL_zc3z04anonymousza31752ze3z87_7191:
																	if (NULLP(BgL_l1261z00_7192))
																		{	/* Expand/initial.scm 172 */
																			BgL_arg1750z00_7189 =
																				CDR(BgL_head1263z00_7190);
																		}
																	else
																		{	/* Expand/initial.scm 172 */
																			obj_t BgL_newtail1265z00_7194;

																			{	/* Expand/initial.scm 172 */
																				obj_t BgL_arg1755z00_7195;

																				{	/* Expand/initial.scm 172 */
																					obj_t BgL_lz00_7196;

																					BgL_lz00_7196 =
																						CAR(((obj_t) BgL_l1261z00_7192));
																					{	/* Expand/initial.scm 172 */
																						obj_t BgL_tmpfunz00_11065;

																						BgL_tmpfunz00_11065 =
																							((obj_t) BgL_ez00_5744);
																						BgL_arg1755z00_7195 =
																							(VA_PROCEDUREP
																							(BgL_tmpfunz00_11065)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_11065)) (((obj_t)
																									BgL_ez00_5744), BgL_lz00_7196,
																								((obj_t) BgL_ez00_5744),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_11065)) (((obj_t)
																									BgL_ez00_5744), BgL_lz00_7196,
																								((obj_t) BgL_ez00_5744)));
																					}
																				}
																				BgL_newtail1265z00_7194 =
																					MAKE_YOUNG_PAIR(BgL_arg1755z00_7195,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1264z00_7193,
																				BgL_newtail1265z00_7194);
																			{	/* Expand/initial.scm 172 */
																				obj_t BgL_arg1754z00_7197;

																				BgL_arg1754z00_7197 =
																					CDR(((obj_t) BgL_l1261z00_7192));
																				{
																					obj_t BgL_tail1264z00_11072;
																					obj_t BgL_l1261z00_11071;

																					BgL_l1261z00_11071 =
																						BgL_arg1754z00_7197;
																					BgL_tail1264z00_11072 =
																						BgL_newtail1265z00_7194;
																					BgL_tail1264z00_7193 =
																						BgL_tail1264z00_11072;
																					BgL_l1261z00_7192 =
																						BgL_l1261z00_11071;
																					goto
																						BgL_zc3z04anonymousza31752ze3z87_7191;
																				}
																			}
																		}
																}
															}
														BgL_arg1749z00_7188 =
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_arg1750z00_7189, BNIL);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
														BgL_arg1749z00_7188);
												}
											}
									}
								else
									{
										obj_t BgL_listsz00_11076;

										BgL_listsz00_11076 = BgL_cdrzd2160zd2_7203;
										BgL_listsz00_7187 = BgL_listsz00_11076;
										goto BgL_tagzd2151zd2_7185;
									}
							}
						else
							{
								obj_t BgL_listsz00_11077;

								BgL_listsz00_11077 = BgL_cdrzd2160zd2_7203;
								BgL_listsz00_7187 = BgL_listsz00_11077;
								goto BgL_tagzd2151zd2_7185;
							}
					}
				else
					{	/* Expand/initial.scm 167 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string4245z00zzexpand_installz00, BgL_xz00_5743);
					}
			}
		}

	}



/* &<@anonymous:1676> */
	obj_t BGl_z62zc3z04anonymousza31676ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5745, obj_t BgL_xz00_5746, obj_t BgL_ez00_5747)
	{
		{	/* Expand/initial.scm 155 */
			{	/* Expand/initial.scm 156 */
				obj_t BgL_locz00_7206;

				BgL_locz00_7206 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_xz00_5746);
				{	/* Expand/initial.scm 157 */
					bool_t BgL_test4763z00_11080;

					{	/* Expand/initial.scm 157 */
						bool_t BgL_test4764z00_11081;

						if (STRUCTP(BgL_locz00_7206))
							{	/* Expand/initial.scm 157 */
								BgL_test4764z00_11081 =
									(STRUCT_KEY(BgL_locz00_7206) == CNST_TABLE_REF(208));
							}
						else
							{	/* Expand/initial.scm 157 */
								BgL_test4764z00_11081 = ((bool_t) 0);
							}
						if (BgL_test4764z00_11081)
							{	/* Expand/initial.scm 157 */
								BgL_test4763z00_11080 =
									CBOOL(BGl_za2errorzd2localiza7ationza2z75zzengine_paramz00);
							}
						else
							{	/* Expand/initial.scm 157 */
								BgL_test4763z00_11080 = ((bool_t) 0);
							}
					}
					if (BgL_test4763z00_11080)
						{	/* Expand/initial.scm 158 */
							obj_t BgL_arg1681z00_7207;

							{	/* Expand/initial.scm 158 */
								obj_t BgL_arg1688z00_7208;
								obj_t BgL_arg1689z00_7209;

								BgL_arg1688z00_7208 =
									BGl_locationzd2fullzd2fnamez00zztools_locationz00
									(BgL_locz00_7206);
								{	/* Expand/initial.scm 159 */
									obj_t BgL_arg1691z00_7210;
									obj_t BgL_arg1692z00_7211;

									BgL_arg1691z00_7210 = STRUCT_REF(BgL_locz00_7206, (int) (1L));
									{	/* Expand/initial.scm 160 */
										obj_t BgL_arg1699z00_7212;

										{	/* Expand/initial.scm 160 */
											obj_t BgL_l1251z00_7213;

											BgL_l1251z00_7213 = CDR(((obj_t) BgL_xz00_5746));
											if (NULLP(BgL_l1251z00_7213))
												{	/* Expand/initial.scm 160 */
													BgL_arg1699z00_7212 = BNIL;
												}
											else
												{	/* Expand/initial.scm 160 */
													obj_t BgL_head1253z00_7214;

													BgL_head1253z00_7214 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													{
														obj_t BgL_l1251z00_7216;
														obj_t BgL_tail1254z00_7217;

														BgL_l1251z00_7216 = BgL_l1251z00_7213;
														BgL_tail1254z00_7217 = BgL_head1253z00_7214;
													BgL_zc3z04anonymousza31701ze3z87_7215:
														if (NULLP(BgL_l1251z00_7216))
															{	/* Expand/initial.scm 160 */
																BgL_arg1699z00_7212 = CDR(BgL_head1253z00_7214);
															}
														else
															{	/* Expand/initial.scm 160 */
																obj_t BgL_newtail1255z00_7218;

																{	/* Expand/initial.scm 160 */
																	obj_t BgL_arg1705z00_7219;

																	{	/* Expand/initial.scm 160 */
																		obj_t BgL_lz00_7220;

																		BgL_lz00_7220 =
																			CAR(((obj_t) BgL_l1251z00_7216));
																		{	/* Expand/initial.scm 160 */
																			obj_t BgL_tmpfunz00_11107;

																			BgL_tmpfunz00_11107 =
																				((obj_t) BgL_ez00_5747);
																			BgL_arg1705z00_7219 =
																				(VA_PROCEDUREP(BgL_tmpfunz00_11107)
																				? ((obj_t(*)(obj_t,
																							...))
																					PROCEDURE_ENTRY(BgL_tmpfunz00_11107))
																				(((obj_t) BgL_ez00_5747), BgL_lz00_7220,
																					((obj_t) BgL_ez00_5747),
																					BEOA) : ((obj_t(*)(obj_t, obj_t,
																							obj_t))
																					PROCEDURE_ENTRY(BgL_tmpfunz00_11107))
																				(((obj_t) BgL_ez00_5747), BgL_lz00_7220,
																					((obj_t) BgL_ez00_5747)));
																		}
																	}
																	BgL_newtail1255z00_7218 =
																		MAKE_YOUNG_PAIR(BgL_arg1705z00_7219, BNIL);
																}
																SET_CDR(BgL_tail1254z00_7217,
																	BgL_newtail1255z00_7218);
																{	/* Expand/initial.scm 160 */
																	obj_t BgL_arg1703z00_7221;

																	BgL_arg1703z00_7221 =
																		CDR(((obj_t) BgL_l1251z00_7216));
																	{
																		obj_t BgL_tail1254z00_11114;
																		obj_t BgL_l1251z00_11113;

																		BgL_l1251z00_11113 = BgL_arg1703z00_7221;
																		BgL_tail1254z00_11114 =
																			BgL_newtail1255z00_7218;
																		BgL_tail1254z00_7217 =
																			BgL_tail1254z00_11114;
																		BgL_l1251z00_7216 = BgL_l1251z00_11113;
																		goto BgL_zc3z04anonymousza31701ze3z87_7215;
																	}
																}
															}
													}
												}
										}
										BgL_arg1692z00_7211 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1699z00_7212, BNIL);
									}
									BgL_arg1689z00_7209 =
										MAKE_YOUNG_PAIR(BgL_arg1691z00_7210, BgL_arg1692z00_7211);
								}
								BgL_arg1681z00_7207 =
									MAKE_YOUNG_PAIR(BgL_arg1688z00_7208, BgL_arg1689z00_7209);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(209), BgL_arg1681z00_7207);
						}
					else
						{	/* Expand/initial.scm 162 */
							obj_t BgL_arg1708z00_7222;

							{	/* Expand/initial.scm 162 */
								obj_t BgL_arg1709z00_7223;

								{	/* Expand/initial.scm 162 */
									obj_t BgL_l1256z00_7224;

									BgL_l1256z00_7224 = CDR(((obj_t) BgL_xz00_5746));
									if (NULLP(BgL_l1256z00_7224))
										{	/* Expand/initial.scm 162 */
											BgL_arg1709z00_7223 = BNIL;
										}
									else
										{	/* Expand/initial.scm 162 */
											obj_t BgL_head1258z00_7225;

											BgL_head1258z00_7225 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1256z00_7227;
												obj_t BgL_tail1259z00_7228;

												BgL_l1256z00_7227 = BgL_l1256z00_7224;
												BgL_tail1259z00_7228 = BgL_head1258z00_7225;
											BgL_zc3z04anonymousza31712ze3z87_7226:
												if (NULLP(BgL_l1256z00_7227))
													{	/* Expand/initial.scm 162 */
														BgL_arg1709z00_7223 = CDR(BgL_head1258z00_7225);
													}
												else
													{	/* Expand/initial.scm 162 */
														obj_t BgL_newtail1260z00_7229;

														{	/* Expand/initial.scm 162 */
															obj_t BgL_arg1718z00_7230;

															{	/* Expand/initial.scm 162 */
																obj_t BgL_lz00_7231;

																BgL_lz00_7231 =
																	CAR(((obj_t) BgL_l1256z00_7227));
																{	/* Expand/initial.scm 162 */
																	obj_t BgL_tmpfunz00_11136;

																	BgL_tmpfunz00_11136 = ((obj_t) BgL_ez00_5747);
																	BgL_arg1718z00_7230 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_11136)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_11136)) ((
																				(obj_t) BgL_ez00_5747), BgL_lz00_7231,
																			((obj_t) BgL_ez00_5747),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_11136)) ((
																				(obj_t) BgL_ez00_5747), BgL_lz00_7231,
																			((obj_t) BgL_ez00_5747)));
																}
															}
															BgL_newtail1260z00_7229 =
																MAKE_YOUNG_PAIR(BgL_arg1718z00_7230, BNIL);
														}
														SET_CDR(BgL_tail1259z00_7228,
															BgL_newtail1260z00_7229);
														{	/* Expand/initial.scm 162 */
															obj_t BgL_arg1717z00_7232;

															BgL_arg1717z00_7232 =
																CDR(((obj_t) BgL_l1256z00_7227));
															{
																obj_t BgL_tail1259z00_11143;
																obj_t BgL_l1256z00_11142;

																BgL_l1256z00_11142 = BgL_arg1717z00_7232;
																BgL_tail1259z00_11143 = BgL_newtail1260z00_7229;
																BgL_tail1259z00_7228 = BgL_tail1259z00_11143;
																BgL_l1256z00_7227 = BgL_l1256z00_11142;
																goto BgL_zc3z04anonymousza31712ze3z87_7226;
															}
														}
													}
											}
										}
								}
								BgL_arg1708z00_7222 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1709z00_7223, BNIL);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg1708z00_7222);
						}
				}
			}
		}

	}



/* &<@anonymous:1545> */
	obj_t BGl_z62zc3z04anonymousza31545ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5748, obj_t BgL_xz00_5749, obj_t BgL_ez00_5750)
	{
		{	/* Expand/initial.scm 136 */
			{	/* Expand/initial.scm 137 */
				obj_t BgL_locz00_7234;

				BgL_locz00_7234 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_xz00_5749);
				{	/* Expand/initial.scm 138 */
					bool_t BgL_test4770z00_11148;

					{	/* Expand/initial.scm 138 */
						bool_t BgL_test4771z00_11149;

						if (STRUCTP(BgL_locz00_7234))
							{	/* Expand/initial.scm 138 */
								BgL_test4771z00_11149 =
									(STRUCT_KEY(BgL_locz00_7234) == CNST_TABLE_REF(208));
							}
						else
							{	/* Expand/initial.scm 138 */
								BgL_test4771z00_11149 = ((bool_t) 0);
							}
						if (BgL_test4771z00_11149)
							{	/* Expand/initial.scm 138 */
								BgL_test4770z00_11148 =
									CBOOL(BGl_za2errorzd2localiza7ationza2z75zzengine_paramz00);
							}
						else
							{	/* Expand/initial.scm 138 */
								BgL_test4770z00_11148 = ((bool_t) 0);
							}
					}
					if (BgL_test4770z00_11148)
						{
							obj_t BgL_l1z00_7248;
							obj_t BgL_l2z00_7249;
							obj_t BgL_l3z00_7250;
							obj_t BgL_listz00_7237;

							if (PAIRP(BgL_xz00_5749))
								{	/* Expand/initial.scm 139 */
									obj_t BgL_cdrzd2112zd2_7268;

									BgL_cdrzd2112zd2_7268 = CDR(((obj_t) BgL_xz00_5749));
									if (PAIRP(BgL_cdrzd2112zd2_7268))
										{	/* Expand/initial.scm 139 */
											obj_t BgL_cdrzd2117zd2_7269;

											BgL_cdrzd2117zd2_7269 = CDR(BgL_cdrzd2112zd2_7268);
											if (PAIRP(BgL_cdrzd2117zd2_7269))
												{	/* Expand/initial.scm 139 */
													obj_t BgL_cdrzd2122zd2_7270;

													BgL_cdrzd2122zd2_7270 = CDR(BgL_cdrzd2117zd2_7269);
													if (PAIRP(BgL_cdrzd2122zd2_7270))
														{	/* Expand/initial.scm 139 */
															if (NULLP(CDR(BgL_cdrzd2122zd2_7270)))
																{	/* Expand/initial.scm 139 */
																	BgL_l1z00_7248 = CAR(BgL_cdrzd2112zd2_7268);
																	BgL_l2z00_7249 = CAR(BgL_cdrzd2117zd2_7269);
																	BgL_l3z00_7250 = CAR(BgL_cdrzd2122zd2_7270);
																	{	/* Expand/initial.scm 142 */
																		obj_t BgL_arg1584z00_7251;

																		{	/* Expand/initial.scm 142 */
																			obj_t BgL_arg1585z00_7252;
																			obj_t BgL_arg1589z00_7253;

																			{	/* Expand/initial.scm 142 */
																				obj_t BgL_arg1591z00_7254;

																				{	/* Expand/initial.scm 142 */
																					obj_t BgL_arg1593z00_7255;
																					obj_t BgL_arg1594z00_7256;

																					{	/* Expand/initial.scm 142 */
																						obj_t BgL_tmpfunz00_11177;

																						BgL_tmpfunz00_11177 =
																							((obj_t) BgL_ez00_5750);
																						BgL_arg1593z00_7255 =
																							(VA_PROCEDUREP
																							(BgL_tmpfunz00_11177)
																							? ((obj_t(*)(obj_t,
																										...))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_11177)) (((obj_t)
																									BgL_ez00_5750),
																								BgL_l1z00_7248,
																								((obj_t) BgL_ez00_5750),
																								BEOA) : ((obj_t(*)(obj_t, obj_t,
																										obj_t))
																								PROCEDURE_ENTRY
																								(BgL_tmpfunz00_11177)) (((obj_t)
																									BgL_ez00_5750),
																								BgL_l1z00_7248,
																								((obj_t) BgL_ez00_5750)));
																					}
																					{	/* Expand/initial.scm 143 */
																						obj_t BgL_arg1595z00_7257;
																						obj_t BgL_arg1602z00_7258;

																						{	/* Expand/initial.scm 143 */
																							obj_t BgL_tmpfunz00_11185;

																							BgL_tmpfunz00_11185 =
																								((obj_t) BgL_ez00_5750);
																							BgL_arg1595z00_7257 =
																								(VA_PROCEDUREP
																								(BgL_tmpfunz00_11185)
																								? ((obj_t(*)(obj_t,
																											...))
																									PROCEDURE_ENTRY
																									(BgL_tmpfunz00_11185)) ((
																										(obj_t) BgL_ez00_5750),
																									BgL_l2z00_7249,
																									((obj_t) BgL_ez00_5750),
																									BEOA) : ((obj_t(*)(obj_t,
																											obj_t,
																											obj_t))
																									PROCEDURE_ENTRY
																									(BgL_tmpfunz00_11185)) ((
																										(obj_t) BgL_ez00_5750),
																									BgL_l2z00_7249,
																									((obj_t) BgL_ez00_5750)));
																						}
																						{	/* Expand/initial.scm 144 */
																							obj_t BgL_arg1605z00_7259;
																							obj_t BgL_arg1606z00_7260;

																							{	/* Expand/initial.scm 144 */
																								obj_t BgL_tmpfunz00_11193;

																								BgL_tmpfunz00_11193 =
																									((obj_t) BgL_ez00_5750);
																								BgL_arg1605z00_7259 =
																									(VA_PROCEDUREP
																									(BgL_tmpfunz00_11193)
																									? ((obj_t(*)(obj_t,
																												...))
																										PROCEDURE_ENTRY
																										(BgL_tmpfunz00_11193)) ((
																											(obj_t) BgL_ez00_5750),
																										BgL_l3z00_7250,
																										((obj_t) BgL_ez00_5750),
																										BEOA) : ((obj_t(*)(obj_t,
																												obj_t,
																												obj_t))
																										PROCEDURE_ENTRY
																										(BgL_tmpfunz00_11193)) ((
																											(obj_t) BgL_ez00_5750),
																										BgL_l3z00_7250,
																										((obj_t) BgL_ez00_5750)));
																							}
																							{	/* Expand/initial.scm 145 */
																								obj_t BgL_arg1609z00_7261;
																								obj_t BgL_arg1611z00_7262;

																								BgL_arg1609z00_7261 =
																									BGl_locationzd2fullzd2fnamez00zztools_locationz00
																									(BgL_locz00_7234);
																								{	/* Expand/initial.scm 146 */
																									obj_t BgL_arg1613z00_7263;

																									BgL_arg1613z00_7263 =
																										STRUCT_REF(
																										((obj_t) BgL_locz00_7234),
																										(int) (1L));
																									BgL_arg1611z00_7262 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1613z00_7263, BNIL);
																								}
																								BgL_arg1606z00_7260 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1609z00_7261,
																									BgL_arg1611z00_7262);
																							}
																							BgL_arg1602z00_7258 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1605z00_7259,
																								BgL_arg1606z00_7260);
																						}
																						BgL_arg1594z00_7256 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1595z00_7257,
																							BgL_arg1602z00_7258);
																					}
																					BgL_arg1591z00_7254 =
																						MAKE_YOUNG_PAIR(BgL_arg1593z00_7255,
																						BgL_arg1594z00_7256);
																				}
																				BgL_arg1585z00_7252 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(210),
																					BgL_arg1591z00_7254);
																			}
																			{	/* Expand/initial.scm 147 */
																				obj_t BgL_arg1615z00_7264;

																				{	/* Expand/initial.scm 147 */
																					obj_t BgL_arg1616z00_7265;

																					{	/* Expand/initial.scm 147 */
																						obj_t BgL_arg1625z00_7266;

																						{	/* Expand/initial.scm 147 */
																							obj_t BgL_arg1626z00_7267;

																							BgL_arg1626z00_7267 =
																								MAKE_YOUNG_PAIR(BFALSE, BNIL);
																							BgL_arg1625z00_7266 =
																								MAKE_YOUNG_PAIR(BFALSE,
																								BgL_arg1626z00_7267);
																						}
																						BgL_arg1616z00_7265 =
																							MAKE_YOUNG_PAIR(BFALSE,
																							BgL_arg1625z00_7266);
																					}
																					BgL_arg1615z00_7264 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																						BgL_arg1616z00_7265);
																				}
																				BgL_arg1589z00_7253 =
																					MAKE_YOUNG_PAIR(BgL_arg1615z00_7264,
																					BNIL);
																			}
																			BgL_arg1584z00_7251 =
																				MAKE_YOUNG_PAIR(BgL_arg1585z00_7252,
																				BgL_arg1589z00_7253);
																		}
																		return
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(211),
																			BgL_arg1584z00_7251);
																	}
																}
															else
																{	/* Expand/initial.scm 139 */
																	BgL_listz00_7237 = BgL_cdrzd2112zd2_7268;
																BgL_tagzd2102zd2_7235:
																	{	/* Expand/initial.scm 149 */
																		obj_t BgL_arg1627z00_7238;

																		{	/* Expand/initial.scm 149 */
																			obj_t BgL_arg1629z00_7239;

																			if (NULLP(BgL_listz00_7237))
																				{	/* Expand/initial.scm 149 */
																					BgL_arg1629z00_7239 = BNIL;
																				}
																			else
																				{	/* Expand/initial.scm 149 */
																					obj_t BgL_head1243z00_7240;

																					BgL_head1243z00_7240 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1241z00_7242;
																						obj_t BgL_tail1244z00_7243;

																						BgL_l1241z00_7242 =
																							BgL_listz00_7237;
																						BgL_tail1244z00_7243 =
																							BgL_head1243z00_7240;
																					BgL_zc3z04anonymousza31631ze3z87_7241:
																						if (NULLP
																							(BgL_l1241z00_7242))
																							{	/* Expand/initial.scm 149 */
																								BgL_arg1629z00_7239 =
																									CDR(BgL_head1243z00_7240);
																							}
																						else
																							{	/* Expand/initial.scm 149 */
																								obj_t BgL_newtail1245z00_7244;

																								{	/* Expand/initial.scm 149 */
																									obj_t BgL_arg1646z00_7245;

																									{	/* Expand/initial.scm 149 */
																										obj_t BgL_lz00_7246;

																										BgL_lz00_7246 =
																											CAR(
																											((obj_t)
																												BgL_l1241z00_7242));
																										{	/* Expand/initial.scm 149 */
																											obj_t BgL_tmpfunz00_11232;

																											BgL_tmpfunz00_11232 =
																												((obj_t) BgL_ez00_5750);
																											BgL_arg1646z00_7245 =
																												(VA_PROCEDUREP
																												(BgL_tmpfunz00_11232)
																												? ((obj_t(*)(obj_t,
																															...))
																													PROCEDURE_ENTRY
																													(BgL_tmpfunz00_11232))
																												(((obj_t)
																														BgL_ez00_5750),
																													BgL_lz00_7246,
																													((obj_t)
																														BgL_ez00_5750),
																													BEOA)
																												: ((obj_t(*)(obj_t,
																															obj_t,
																															obj_t))
																													PROCEDURE_ENTRY
																													(BgL_tmpfunz00_11232))
																												(((obj_t)
																														BgL_ez00_5750),
																													BgL_lz00_7246,
																													((obj_t)
																														BgL_ez00_5750)));
																										}
																									}
																									BgL_newtail1245z00_7244 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1646z00_7245, BNIL);
																								}
																								SET_CDR(BgL_tail1244z00_7243,
																									BgL_newtail1245z00_7244);
																								{	/* Expand/initial.scm 149 */
																									obj_t BgL_arg1642z00_7247;

																									BgL_arg1642z00_7247 =
																										CDR(
																										((obj_t)
																											BgL_l1241z00_7242));
																									{
																										obj_t BgL_tail1244z00_11239;
																										obj_t BgL_l1241z00_11238;

																										BgL_l1241z00_11238 =
																											BgL_arg1642z00_7247;
																										BgL_tail1244z00_11239 =
																											BgL_newtail1245z00_7244;
																										BgL_tail1244z00_7243 =
																											BgL_tail1244z00_11239;
																										BgL_l1241z00_7242 =
																											BgL_l1241z00_11238;
																										goto
																											BgL_zc3z04anonymousza31631ze3z87_7241;
																									}
																								}
																							}
																					}
																				}
																			BgL_arg1627z00_7238 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg1629z00_7239, BNIL);
																		}
																		return
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																			BgL_arg1627z00_7238);
																	}
																}
														}
													else
														{
															obj_t BgL_listz00_11243;

															BgL_listz00_11243 = BgL_cdrzd2112zd2_7268;
															BgL_listz00_7237 = BgL_listz00_11243;
															goto BgL_tagzd2102zd2_7235;
														}
												}
											else
												{
													obj_t BgL_listz00_11244;

													BgL_listz00_11244 = BgL_cdrzd2112zd2_7268;
													BgL_listz00_7237 = BgL_listz00_11244;
													goto BgL_tagzd2102zd2_7235;
												}
										}
									else
										{
											obj_t BgL_listz00_11245;

											BgL_listz00_11245 = BgL_cdrzd2112zd2_7268;
											BgL_listz00_7237 = BgL_listz00_11245;
											goto BgL_tagzd2102zd2_7235;
										}
								}
							else
								{	/* Expand/initial.scm 139 */
									return BFALSE;
								}
						}
					else
						{	/* Expand/initial.scm 150 */
							obj_t BgL_arg1650z00_7271;

							{	/* Expand/initial.scm 150 */
								obj_t BgL_arg1651z00_7272;

								{	/* Expand/initial.scm 150 */
									obj_t BgL_l1246z00_7273;

									BgL_l1246z00_7273 = CDR(((obj_t) BgL_xz00_5749));
									if (NULLP(BgL_l1246z00_7273))
										{	/* Expand/initial.scm 150 */
											BgL_arg1651z00_7272 = BNIL;
										}
									else
										{	/* Expand/initial.scm 150 */
											obj_t BgL_head1248z00_7274;

											BgL_head1248z00_7274 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1246z00_7276;
												obj_t BgL_tail1249z00_7277;

												BgL_l1246z00_7276 = BgL_l1246z00_7273;
												BgL_tail1249z00_7277 = BgL_head1248z00_7274;
											BgL_zc3z04anonymousza31653ze3z87_7275:
												if (NULLP(BgL_l1246z00_7276))
													{	/* Expand/initial.scm 150 */
														BgL_arg1651z00_7272 = CDR(BgL_head1248z00_7274);
													}
												else
													{	/* Expand/initial.scm 150 */
														obj_t BgL_newtail1250z00_7278;

														{	/* Expand/initial.scm 150 */
															obj_t BgL_arg1663z00_7279;

															{	/* Expand/initial.scm 150 */
																obj_t BgL_lz00_7280;

																BgL_lz00_7280 =
																	CAR(((obj_t) BgL_l1246z00_7276));
																{	/* Expand/initial.scm 150 */
																	obj_t BgL_tmpfunz00_11262;

																	BgL_tmpfunz00_11262 = ((obj_t) BgL_ez00_5750);
																	BgL_arg1663z00_7279 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_11262)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_11262)) ((
																				(obj_t) BgL_ez00_5750), BgL_lz00_7280,
																			((obj_t) BgL_ez00_5750),
																			BEOA) : ((obj_t(*)(obj_t, obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_11262)) ((
																				(obj_t) BgL_ez00_5750), BgL_lz00_7280,
																			((obj_t) BgL_ez00_5750)));
																}
															}
															BgL_newtail1250z00_7278 =
																MAKE_YOUNG_PAIR(BgL_arg1663z00_7279, BNIL);
														}
														SET_CDR(BgL_tail1249z00_7277,
															BgL_newtail1250z00_7278);
														{	/* Expand/initial.scm 150 */
															obj_t BgL_arg1661z00_7281;

															BgL_arg1661z00_7281 =
																CDR(((obj_t) BgL_l1246z00_7276));
															{
																obj_t BgL_tail1249z00_11269;
																obj_t BgL_l1246z00_11268;

																BgL_l1246z00_11268 = BgL_arg1661z00_7281;
																BgL_tail1249z00_11269 = BgL_newtail1250z00_7278;
																BgL_tail1249z00_7277 = BgL_tail1249z00_11269;
																BgL_l1246z00_7276 = BgL_l1246z00_11268;
																goto BgL_zc3z04anonymousza31653ze3z87_7275;
															}
														}
													}
											}
										}
								}
								BgL_arg1650z00_7271 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1651z00_7272, BNIL);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BgL_arg1650z00_7271);
						}
				}
			}
		}

	}



/* &<@anonymous:1515> */
	obj_t BGl_z62zc3z04anonymousza31515ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5751, obj_t BgL_xz00_5752, obj_t BgL_ez00_5753)
	{
		{	/* Expand/initial.scm 129 */
			{	/* Expand/initial.scm 130 */
				obj_t BgL_l01240z00_7282;

				{	/* Expand/initial.scm 130 */
					obj_t BgL_pairz00_7283;

					BgL_pairz00_7283 = CDR(((obj_t) BgL_xz00_5752));
					BgL_l01240z00_7282 = CDR(BgL_pairz00_7283);
				}
				{
					obj_t BgL_l1239z00_7285;

					BgL_l1239z00_7285 = BgL_l01240z00_7282;
				BgL_zc3z04anonymousza31516ze3z87_7284:
					if (NULLP(BgL_l1239z00_7285))
						{	/* Expand/initial.scm 130 */
							BgL_l01240z00_7282;
						}
					else
						{	/* Expand/initial.scm 130 */
							{	/* Expand/initial.scm 130 */
								obj_t BgL_arg1535z00_7286;

								{	/* Expand/initial.scm 130 */
									obj_t BgL_xz00_7287;

									BgL_xz00_7287 = CAR(((obj_t) BgL_l1239z00_7285));
									BgL_arg1535z00_7286 =
										BGL_PROCEDURE_CALL2(BgL_ez00_5753, BgL_xz00_7287,
										BgL_ez00_5753);
								}
								{	/* Expand/initial.scm 130 */
									obj_t BgL_tmpz00_11285;

									BgL_tmpz00_11285 = ((obj_t) BgL_l1239z00_7285);
									SET_CAR(BgL_tmpz00_11285, BgL_arg1535z00_7286);
								}
							}
							{	/* Expand/initial.scm 130 */
								obj_t BgL_arg1540z00_7288;

								BgL_arg1540z00_7288 = CDR(((obj_t) BgL_l1239z00_7285));
								{
									obj_t BgL_l1239z00_11290;

									BgL_l1239z00_11290 = BgL_arg1540z00_7288;
									BgL_l1239z00_7285 = BgL_l1239z00_11290;
									goto BgL_zc3z04anonymousza31516ze3z87_7284;
								}
							}
						}
				}
			}
			return BgL_xz00_5752;
		}

	}



/* &<@anonymous:1503> */
	obj_t BGl_z62zc3z04anonymousza31503ze3ze5zzexpand_installz00(obj_t
		BgL_envz00_5754, obj_t BgL_xz00_5755, obj_t BgL_ez00_5756)
	{
		{	/* Expand/initial.scm 73 */
			return
				BGl_expandzd2metazd2zzast_privatez00(BgL_xz00_5755,
				BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(BgL_ez00_5756));
		}

	}



/* call-check */
	obj_t BGl_callzd2checkzd2zzexpand_installz00(obj_t BgL_xz00_17,
		obj_t BgL_predz00_18, obj_t BgL_tnamez00_19, obj_t BgL_ez00_20)
	{
		{	/* Expand/initial.scm 1110 */
			{	/* Expand/initial.scm 1111 */
				obj_t BgL_funz00_3955;

				BgL_funz00_3955 = CAR(((obj_t) BgL_xz00_17));
				{	/* Expand/initial.scm 1111 */
					obj_t BgL_actualsz00_3956;

					BgL_actualsz00_3956 = CDR(((obj_t) BgL_xz00_17));
					{	/* Expand/initial.scm 1112 */
						obj_t BgL_formalsz00_3957;

						if (NULLP(BgL_actualsz00_3956))
							{	/* Expand/initial.scm 1113 */
								BgL_formalsz00_3957 = BNIL;
							}
						else
							{	/* Expand/initial.scm 1113 */
								obj_t BgL_head1398z00_4012;

								BgL_head1398z00_4012 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1396z00_4014;
									obj_t BgL_tail1399z00_4015;

									BgL_l1396z00_4014 = BgL_actualsz00_3956;
									BgL_tail1399z00_4015 = BgL_head1398z00_4012;
								BgL_zc3z04anonymousza33572ze3z87_4016:
									if (NULLP(BgL_l1396z00_4014))
										{	/* Expand/initial.scm 1113 */
											BgL_formalsz00_3957 = CDR(BgL_head1398z00_4012);
										}
									else
										{	/* Expand/initial.scm 1113 */
											obj_t BgL_newtail1400z00_4018;

											{	/* Expand/initial.scm 1113 */
												obj_t BgL_arg3575z00_4020;

												{	/* Expand/initial.scm 1113 */
													obj_t BgL_arg3576z00_4022;

													{	/* Expand/initial.scm 1113 */

														{	/* Expand/initial.scm 1113 */

															BgL_arg3576z00_4022 =
																BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
														}
													}
													BgL_arg3575z00_4020 =
														BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
														(BgL_arg3576z00_4022);
												}
												BgL_newtail1400z00_4018 =
													MAKE_YOUNG_PAIR(BgL_arg3575z00_4020, BNIL);
											}
											SET_CDR(BgL_tail1399z00_4015, BgL_newtail1400z00_4018);
											{	/* Expand/initial.scm 1113 */
												obj_t BgL_arg3574z00_4019;

												BgL_arg3574z00_4019 = CDR(((obj_t) BgL_l1396z00_4014));
												{
													obj_t BgL_tail1399z00_11310;
													obj_t BgL_l1396z00_11309;

													BgL_l1396z00_11309 = BgL_arg3574z00_4019;
													BgL_tail1399z00_11310 = BgL_newtail1400z00_4018;
													BgL_tail1399z00_4015 = BgL_tail1399z00_11310;
													BgL_l1396z00_4014 = BgL_l1396z00_11309;
													goto BgL_zc3z04anonymousza33572ze3z87_4016;
												}
											}
										}
								}
							}
						{	/* Expand/initial.scm 1113 */
							obj_t BgL_msgz00_3958;

							{	/* Expand/initial.scm 1114 */
								obj_t BgL_arg3570z00_4008;

								{	/* Expand/initial.scm 1114 */

									{	/* Expand/initial.scm 1114 */

										BgL_arg3570z00_4008 =
											BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
									}
								}
								BgL_msgz00_3958 =
									BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
									(BgL_arg3570z00_4008);
							}
							{	/* Expand/initial.scm 1114 */
								obj_t BgL_locz00_3959;

								BgL_locz00_3959 =
									BGl_findzd2locationzd2zztools_locationz00(BgL_xz00_17);
								{	/* Expand/initial.scm 1115 */

									{	/* Expand/initial.scm 1117 */
										obj_t BgL_arg3524z00_3960;

										{	/* Expand/initial.scm 1117 */
											obj_t BgL_arg3525z00_3961;
											obj_t BgL_arg3528z00_3962;

											BgL_arg3525z00_3961 = BGl_letzd2symzd2zzast_letz00();
											{	/* Expand/initial.scm 1117 */
												obj_t BgL_arg3529z00_3963;
												obj_t BgL_arg3531z00_3964;

												{	/* Expand/initial.scm 1117 */
													obj_t BgL_arg3532z00_3965;
													obj_t BgL_arg3533z00_3966;

													if (NULLP(BgL_formalsz00_3957))
														{	/* Expand/initial.scm 1117 */
															BgL_arg3532z00_3965 = BNIL;
														}
													else
														{	/* Expand/initial.scm 1117 */
															obj_t BgL_head1403z00_3970;

															BgL_head1403z00_3970 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_ll1401z00_3972;
																obj_t BgL_ll1402z00_3973;
																obj_t BgL_tail1404z00_3974;

																BgL_ll1401z00_3972 = BgL_formalsz00_3957;
																BgL_ll1402z00_3973 = BgL_actualsz00_3956;
																BgL_tail1404z00_3974 = BgL_head1403z00_3970;
															BgL_zc3z04anonymousza33535ze3z87_3975:
																if (NULLP(BgL_ll1401z00_3972))
																	{	/* Expand/initial.scm 1117 */
																		BgL_arg3532z00_3965 =
																			CDR(BgL_head1403z00_3970);
																	}
																else
																	{	/* Expand/initial.scm 1117 */
																		obj_t BgL_newtail1405z00_3977;

																		{	/* Expand/initial.scm 1117 */
																			obj_t BgL_arg3540z00_3980;

																			{	/* Expand/initial.scm 1117 */
																				obj_t BgL_fz00_3981;
																				obj_t BgL_vz00_3982;

																				BgL_fz00_3981 =
																					CAR(((obj_t) BgL_ll1401z00_3972));
																				BgL_vz00_3982 =
																					CAR(((obj_t) BgL_ll1402z00_3973));
																				{	/* Expand/initial.scm 1117 */
																					obj_t BgL_arg3541z00_3983;

																					BgL_arg3541z00_3983 =
																						BGL_PROCEDURE_CALL2(BgL_ez00_20,
																						BgL_vz00_3982, BgL_ez00_20);
																					{	/* Expand/initial.scm 1117 */
																						obj_t BgL_list3542z00_3984;

																						{	/* Expand/initial.scm 1117 */
																							obj_t BgL_arg3544z00_3985;

																							BgL_arg3544z00_3985 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg3541z00_3983, BNIL);
																							BgL_list3542z00_3984 =
																								MAKE_YOUNG_PAIR(BgL_fz00_3981,
																								BgL_arg3544z00_3985);
																						}
																						BgL_arg3540z00_3980 =
																							BgL_list3542z00_3984;
																					}
																				}
																			}
																			BgL_newtail1405z00_3977 =
																				MAKE_YOUNG_PAIR(BgL_arg3540z00_3980,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1404z00_3974,
																			BgL_newtail1405z00_3977);
																		{	/* Expand/initial.scm 1117 */
																			obj_t BgL_arg3538z00_3978;
																			obj_t BgL_arg3539z00_3979;

																			BgL_arg3538z00_3978 =
																				CDR(((obj_t) BgL_ll1401z00_3972));
																			BgL_arg3539z00_3979 =
																				CDR(((obj_t) BgL_ll1402z00_3973));
																			{
																				obj_t BgL_tail1404z00_11340;
																				obj_t BgL_ll1402z00_11339;
																				obj_t BgL_ll1401z00_11338;

																				BgL_ll1401z00_11338 =
																					BgL_arg3538z00_3978;
																				BgL_ll1402z00_11339 =
																					BgL_arg3539z00_3979;
																				BgL_tail1404z00_11340 =
																					BgL_newtail1405z00_3977;
																				BgL_tail1404z00_3974 =
																					BgL_tail1404z00_11340;
																				BgL_ll1402z00_3973 =
																					BgL_ll1402z00_11339;
																				BgL_ll1401z00_3972 =
																					BgL_ll1401z00_11338;
																				goto
																					BgL_zc3z04anonymousza33535ze3z87_3975;
																			}
																		}
																	}
															}
														}
													{	/* Expand/initial.scm 1118 */
														obj_t BgL_arg3545z00_3987;

														{	/* Expand/initial.scm 1118 */
															obj_t BgL_arg3546z00_3988;

															{	/* Expand/initial.scm 1118 */
																obj_t BgL_arg3548z00_3989;

																{	/* Expand/initial.scm 1118 */
																	obj_t BgL_arg3549z00_3990;

																	{	/* Expand/initial.scm 1118 */
																		obj_t BgL_arg1455z00_5316;

																		BgL_arg1455z00_5316 =
																			SYMBOL_TO_STRING(
																			((obj_t) BgL_funz00_3955));
																		BgL_arg3549z00_3990 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_5316);
																	}
																	BgL_arg3548z00_3989 =
																		string_append_3(BgL_arg3549z00_3990,
																		BGl_string4246z00zzexpand_installz00,
																		BgL_tnamez00_19);
																}
																BgL_arg3546z00_3988 =
																	MAKE_YOUNG_PAIR(BgL_arg3548z00_3989, BNIL);
															}
															BgL_arg3545z00_3987 =
																MAKE_YOUNG_PAIR(BgL_msgz00_3958,
																BgL_arg3546z00_3988);
														}
														BgL_arg3533z00_3966 =
															MAKE_YOUNG_PAIR(BgL_arg3545z00_3987, BNIL);
													}
													BgL_arg3529z00_3963 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg3532z00_3965, BgL_arg3533z00_3966);
												}
												BgL_arg3531z00_3964 =
													MAKE_YOUNG_PAIR(BGl_loopze71ze7zzexpand_installz00
													(BgL_formalsz00_3957, BgL_msgz00_3958,
														BgL_funz00_3955, BgL_locz00_3959, BgL_predz00_18,
														BgL_formalsz00_3957), BNIL);
												BgL_arg3528z00_3962 =
													MAKE_YOUNG_PAIR(BgL_arg3529z00_3963,
													BgL_arg3531z00_3964);
											}
											BgL_arg3524z00_3960 =
												MAKE_YOUNG_PAIR(BgL_arg3525z00_3961,
												BgL_arg3528z00_3962);
										}
										return
											BGl_epairifyzd2reczd2zztools_miscz00(BgL_arg3524z00_3960,
											BgL_xz00_17);
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zzexpand_installz00(obj_t BgL_formalsz00_5970,
		obj_t BgL_msgz00_5969, obj_t BgL_funz00_5968, obj_t BgL_locz00_5967,
		obj_t BgL_predz00_5966, obj_t BgL_argsz00_3993)
	{
		{	/* Expand/initial.scm 1120 */
			if (NULLP(BgL_argsz00_3993))
				{	/* Expand/initial.scm 1121 */
					return MAKE_YOUNG_PAIR(BgL_funz00_5968, BgL_formalsz00_5970);
				}
			else
				{	/* Expand/initial.scm 1123 */
					obj_t BgL_arg3555z00_3996;
					obj_t BgL_arg3556z00_3997;

					BgL_arg3555z00_3996 = BGl_ifzd2symzd2zzast_sexpz00();
					{	/* Expand/initial.scm 1123 */
						obj_t BgL_arg3558z00_3998;
						obj_t BgL_arg3560z00_3999;

						{	/* Expand/initial.scm 1123 */
							obj_t BgL_arg3561z00_4000;

							{	/* Expand/initial.scm 1123 */
								obj_t BgL_arg3563z00_4001;

								BgL_arg3563z00_4001 = CAR(((obj_t) BgL_argsz00_3993));
								BgL_arg3561z00_4000 =
									MAKE_YOUNG_PAIR(BgL_arg3563z00_4001, BNIL);
							}
							BgL_arg3558z00_3998 =
								MAKE_YOUNG_PAIR(BgL_predz00_5966, BgL_arg3561z00_4000);
						}
						{	/* Expand/initial.scm 1124 */
							obj_t BgL_arg3564z00_4002;
							obj_t BgL_arg3565z00_4003;

							{	/* Expand/initial.scm 1124 */
								obj_t BgL_arg3567z00_4004;

								BgL_arg3567z00_4004 = CDR(((obj_t) BgL_argsz00_3993));
								BgL_arg3564z00_4002 =
									BGl_loopze71ze7zzexpand_installz00(BgL_formalsz00_5970,
									BgL_msgz00_5969, BgL_funz00_5968, BgL_locz00_5967,
									BgL_predz00_5966, BgL_arg3567z00_4004);
							}
							{	/* Expand/initial.scm 1125 */
								obj_t BgL_arg3568z00_4005;

								{	/* Expand/initial.scm 1125 */
									obj_t BgL_arg3569z00_4006;

									BgL_arg3569z00_4006 = CAR(((obj_t) BgL_argsz00_3993));
									BgL_arg3568z00_4005 =
										BGl_errzf2loczf2zzexpand_installz00(BgL_locz00_5967,
										BgL_funz00_5968, BgL_msgz00_5969, BgL_arg3569z00_4006);
								}
								BgL_arg3565z00_4003 =
									MAKE_YOUNG_PAIR(BgL_arg3568z00_4005, BNIL);
							}
							BgL_arg3560z00_3999 =
								MAKE_YOUNG_PAIR(BgL_arg3564z00_4002, BgL_arg3565z00_4003);
						}
						BgL_arg3556z00_3997 =
							MAKE_YOUNG_PAIR(BgL_arg3558z00_3998, BgL_arg3560z00_3999);
					}
					return MAKE_YOUNG_PAIR(BgL_arg3555z00_3996, BgL_arg3556z00_3997);
				}
		}

	}



/* bound-check */
	obj_t BGl_boundzd2checkzd2zzexpand_installz00(obj_t BgL_xz00_21,
		obj_t BgL_flenz00_22, obj_t BgL_predz00_23, obj_t BgL_ez00_24)
	{
		{	/* Expand/initial.scm 1131 */
			{
				obj_t BgL_funz00_4025;
				obj_t BgL_aobjz00_4026;
				obj_t BgL_aoffz00_4027;
				obj_t BgL_restz00_4028;

				if (PAIRP(BgL_xz00_21))
					{	/* Expand/initial.scm 1132 */
						obj_t BgL_cdrzd21918zd2_4033;

						BgL_cdrzd21918zd2_4033 = CDR(((obj_t) BgL_xz00_21));
						if (PAIRP(BgL_cdrzd21918zd2_4033))
							{	/* Expand/initial.scm 1132 */
								obj_t BgL_cdrzd21924zd2_4035;

								BgL_cdrzd21924zd2_4035 = CDR(BgL_cdrzd21918zd2_4033);
								if (PAIRP(BgL_cdrzd21924zd2_4035))
									{	/* Expand/initial.scm 1132 */
										obj_t BgL_arg3580z00_4037;
										obj_t BgL_arg3581z00_4038;
										obj_t BgL_arg3582z00_4039;
										obj_t BgL_arg3584z00_4040;

										BgL_arg3580z00_4037 = CAR(((obj_t) BgL_xz00_21));
										BgL_arg3581z00_4038 = CAR(BgL_cdrzd21918zd2_4033);
										BgL_arg3582z00_4039 = CAR(BgL_cdrzd21924zd2_4035);
										BgL_arg3584z00_4040 = CDR(BgL_cdrzd21924zd2_4035);
										BgL_funz00_4025 = BgL_arg3580z00_4037;
										BgL_aobjz00_4026 = BgL_arg3581z00_4038;
										BgL_aoffz00_4027 = BgL_arg3582z00_4039;
										BgL_restz00_4028 = BgL_arg3584z00_4040;
										{	/* Expand/initial.scm 1134 */
											obj_t BgL_fobjz00_4041;
											obj_t BgL_foffz00_4042;
											obj_t BgL_lenz00_4043;
											obj_t BgL_locz00_4044;

											{	/* Expand/initial.scm 1134 */
												obj_t BgL_arg3636z00_4093;

												{	/* Expand/initial.scm 1134 */

													{	/* Expand/initial.scm 1134 */

														BgL_arg3636z00_4093 =
															BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
													}
												}
												BgL_fobjz00_4041 =
													BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
													(BgL_arg3636z00_4093);
											}
											{	/* Expand/initial.scm 1135 */
												obj_t BgL_arg3637z00_4095;

												{	/* Expand/initial.scm 1135 */

													{	/* Expand/initial.scm 1135 */

														BgL_arg3637z00_4095 =
															BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
													}
												}
												BgL_foffz00_4042 =
													BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
													(BgL_arg3637z00_4095);
											}
											{	/* Expand/initial.scm 1136 */
												obj_t BgL_arg3639z00_4097;

												{	/* Expand/initial.scm 1136 */

													{	/* Expand/initial.scm 1136 */

														BgL_arg3639z00_4097 =
															BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
													}
												}
												BgL_lenz00_4043 =
													BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
													(BgL_arg3639z00_4097);
											}
											BgL_locz00_4044 =
												BGl_findzd2locationzd2zztools_locationz00(BgL_xz00_21);
											{	/* Expand/initial.scm 1139 */
												obj_t BgL_arg3585z00_4045;

												{	/* Expand/initial.scm 1139 */
													obj_t BgL_arg3586z00_4046;
													obj_t BgL_arg3587z00_4047;

													BgL_arg3586z00_4046 = BGl_letzd2symzd2zzast_letz00();
													{	/* Expand/initial.scm 1139 */
														obj_t BgL_arg3589z00_4048;
														obj_t BgL_arg3590z00_4049;

														{	/* Expand/initial.scm 1139 */
															obj_t BgL_arg3592z00_4050;
															obj_t BgL_arg3593z00_4051;

															{	/* Expand/initial.scm 1139 */
																obj_t BgL_arg3595z00_4052;

																{	/* Expand/initial.scm 1139 */
																	obj_t BgL_arg3596z00_4053;

																	BgL_arg3596z00_4053 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_24,
																		BgL_aobjz00_4026, BgL_ez00_24);
																	BgL_arg3595z00_4052 =
																		MAKE_YOUNG_PAIR(BgL_arg3596z00_4053, BNIL);
																}
																BgL_arg3592z00_4050 =
																	MAKE_YOUNG_PAIR(BgL_fobjz00_4041,
																	BgL_arg3595z00_4052);
															}
															{	/* Expand/initial.scm 1140 */
																obj_t BgL_arg3597z00_4054;

																{	/* Expand/initial.scm 1140 */
																	obj_t BgL_arg3598z00_4055;

																	{	/* Expand/initial.scm 1140 */
																		obj_t BgL_arg3599z00_4056;

																		BgL_arg3599z00_4056 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_24,
																			BgL_aoffz00_4027, BgL_ez00_24);
																		BgL_arg3598z00_4055 =
																			MAKE_YOUNG_PAIR(BgL_arg3599z00_4056,
																			BNIL);
																	}
																	BgL_arg3597z00_4054 =
																		MAKE_YOUNG_PAIR(BgL_foffz00_4042,
																		BgL_arg3598z00_4055);
																}
																BgL_arg3593z00_4051 =
																	MAKE_YOUNG_PAIR(BgL_arg3597z00_4054, BNIL);
															}
															BgL_arg3589z00_4048 =
																MAKE_YOUNG_PAIR(BgL_arg3592z00_4050,
																BgL_arg3593z00_4051);
														}
														{	/* Expand/initial.scm 1141 */
															obj_t BgL_arg3600z00_4057;

															{	/* Expand/initial.scm 1141 */
																obj_t BgL_arg3601z00_4058;
																obj_t BgL_arg3603z00_4059;

																BgL_arg3601z00_4058 =
																	BGl_letzd2symzd2zzast_letz00();
																{	/* Expand/initial.scm 1142 */
																	obj_t BgL_arg3605z00_4060;
																	obj_t BgL_arg3606z00_4061;

																	{	/* Expand/initial.scm 1142 */
																		obj_t BgL_arg3607z00_4062;

																		{	/* Expand/initial.scm 1142 */
																			obj_t BgL_arg3609z00_4063;

																			{	/* Expand/initial.scm 1142 */
																				obj_t BgL_arg3610z00_4064;

																				{	/* Expand/initial.scm 1142 */
																					obj_t BgL_arg3611z00_4065;

																					BgL_arg3611z00_4065 =
																						MAKE_YOUNG_PAIR(BgL_fobjz00_4041,
																						BNIL);
																					BgL_arg3610z00_4064 =
																						MAKE_YOUNG_PAIR(BgL_flenz00_22,
																						BgL_arg3611z00_4065);
																				}
																				BgL_arg3609z00_4063 =
																					MAKE_YOUNG_PAIR(BgL_arg3610z00_4064,
																					BNIL);
																			}
																			BgL_arg3607z00_4062 =
																				MAKE_YOUNG_PAIR(BgL_lenz00_4043,
																				BgL_arg3609z00_4063);
																		}
																		BgL_arg3605z00_4060 =
																			MAKE_YOUNG_PAIR(BgL_arg3607z00_4062,
																			BNIL);
																	}
																	{	/* Expand/initial.scm 1143 */
																		obj_t BgL_arg3612z00_4066;

																		{	/* Expand/initial.scm 1143 */
																			obj_t BgL_arg3613z00_4067;
																			obj_t BgL_arg3614z00_4068;

																			BgL_arg3613z00_4067 =
																				BGl_ifzd2symzd2zzast_sexpz00();
																			{	/* Expand/initial.scm 1143 */
																				obj_t BgL_arg3616z00_4069;
																				obj_t BgL_arg3618z00_4070;

																				{	/* Expand/initial.scm 1143 */
																					obj_t BgL_arg3620z00_4071;

																					{	/* Expand/initial.scm 1143 */
																						obj_t BgL_arg3621z00_4072;

																						BgL_arg3621z00_4072 =
																							MAKE_YOUNG_PAIR(BgL_lenz00_4043,
																							BNIL);
																						BgL_arg3620z00_4071 =
																							MAKE_YOUNG_PAIR(BgL_foffz00_4042,
																							BgL_arg3621z00_4072);
																					}
																					BgL_arg3616z00_4069 =
																						MAKE_YOUNG_PAIR(BgL_predz00_23,
																						BgL_arg3620z00_4071);
																				}
																				{	/* Expand/initial.scm 1144 */
																					obj_t BgL_arg3622z00_4073;
																					obj_t BgL_arg3623z00_4074;

																					{	/* Expand/initial.scm 1144 */
																						obj_t BgL_arg3624z00_4075;

																						{	/* Expand/initial.scm 1144 */
																							obj_t BgL_arg3625z00_4076;

																							{	/* Expand/initial.scm 1144 */
																								obj_t BgL_arg3626z00_4077;

																								{	/* Expand/initial.scm 1144 */
																									obj_t BgL_arg3628z00_4078;

																									if (NULLP(BgL_restz00_4028))
																										{	/* Expand/initial.scm 1144 */
																											BgL_arg3628z00_4078 =
																												BNIL;
																										}
																									else
																										{	/* Expand/initial.scm 1144 */
																											obj_t
																												BgL_head1409z00_4081;
																											BgL_head1409z00_4081 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											{
																												obj_t BgL_l1407z00_4083;
																												obj_t
																													BgL_tail1410z00_4084;
																												BgL_l1407z00_4083 =
																													BgL_restz00_4028;
																												BgL_tail1410z00_4084 =
																													BgL_head1409z00_4081;
																											BgL_zc3z04anonymousza33630ze3z87_4085:
																												if (NULLP
																													(BgL_l1407z00_4083))
																													{	/* Expand/initial.scm 1144 */
																														BgL_arg3628z00_4078
																															=
																															CDR
																															(BgL_head1409z00_4081);
																													}
																												else
																													{	/* Expand/initial.scm 1144 */
																														obj_t
																															BgL_newtail1411z00_4087;
																														{	/* Expand/initial.scm 1144 */
																															obj_t
																																BgL_arg3634z00_4089;
																															{	/* Expand/initial.scm 1144 */
																																obj_t
																																	BgL_xz00_4090;
																																BgL_xz00_4090 =
																																	CAR(((obj_t)
																																		BgL_l1407z00_4083));
																																BgL_arg3634z00_4089
																																	=
																																	BGL_PROCEDURE_CALL2
																																	(BgL_ez00_24,
																																	BgL_xz00_4090,
																																	BgL_ez00_24);
																															}
																															BgL_newtail1411z00_4087
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg3634z00_4089,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_tail1410z00_4084,
																															BgL_newtail1411z00_4087);
																														{	/* Expand/initial.scm 1144 */
																															obj_t
																																BgL_arg3632z00_4088;
																															BgL_arg3632z00_4088
																																=
																																CDR(((obj_t)
																																	BgL_l1407z00_4083));
																															{
																																obj_t
																																	BgL_tail1410z00_11438;
																																obj_t
																																	BgL_l1407z00_11437;
																																BgL_l1407z00_11437
																																	=
																																	BgL_arg3632z00_4088;
																																BgL_tail1410z00_11438
																																	=
																																	BgL_newtail1411z00_4087;
																																BgL_tail1410z00_4084
																																	=
																																	BgL_tail1410z00_11438;
																																BgL_l1407z00_4083
																																	=
																																	BgL_l1407z00_11437;
																																goto
																																	BgL_zc3z04anonymousza33630ze3z87_4085;
																															}
																														}
																													}
																											}
																										}
																									BgL_arg3626z00_4077 =
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_arg3628z00_4078, BNIL);
																								}
																								BgL_arg3625z00_4076 =
																									MAKE_YOUNG_PAIR
																									(BgL_foffz00_4042,
																									BgL_arg3626z00_4077);
																							}
																							BgL_arg3624z00_4075 =
																								MAKE_YOUNG_PAIR
																								(BgL_fobjz00_4041,
																								BgL_arg3625z00_4076);
																						}
																						BgL_arg3622z00_4073 =
																							MAKE_YOUNG_PAIR(BgL_funz00_4025,
																							BgL_arg3624z00_4075);
																					}
																					BgL_arg3623z00_4074 =
																						MAKE_YOUNG_PAIR
																						(BGl_errzf2loczf2zzexpand_installz00
																						(BgL_locz00_4044, BgL_funz00_4025,
																							BGl_string4247z00zzexpand_installz00,
																							BgL_foffz00_4042), BNIL);
																					BgL_arg3618z00_4070 =
																						MAKE_YOUNG_PAIR(BgL_arg3622z00_4073,
																						BgL_arg3623z00_4074);
																				}
																				BgL_arg3614z00_4068 =
																					MAKE_YOUNG_PAIR(BgL_arg3616z00_4069,
																					BgL_arg3618z00_4070);
																			}
																			BgL_arg3612z00_4066 =
																				MAKE_YOUNG_PAIR(BgL_arg3613z00_4067,
																				BgL_arg3614z00_4068);
																		}
																		BgL_arg3606z00_4061 =
																			MAKE_YOUNG_PAIR(BgL_arg3612z00_4066,
																			BNIL);
																	}
																	BgL_arg3603z00_4059 =
																		MAKE_YOUNG_PAIR(BgL_arg3605z00_4060,
																		BgL_arg3606z00_4061);
																}
																BgL_arg3600z00_4057 =
																	MAKE_YOUNG_PAIR(BgL_arg3601z00_4058,
																	BgL_arg3603z00_4059);
															}
															BgL_arg3590z00_4049 =
																MAKE_YOUNG_PAIR(BgL_arg3600z00_4057, BNIL);
														}
														BgL_arg3587z00_4047 =
															MAKE_YOUNG_PAIR(BgL_arg3589z00_4048,
															BgL_arg3590z00_4049);
													}
													BgL_arg3585z00_4045 =
														MAKE_YOUNG_PAIR(BgL_arg3586z00_4046,
														BgL_arg3587z00_4047);
												}
												return
													BGl_epairifyzd2reczd2zztools_miscz00
													(BgL_arg3585z00_4045, BgL_xz00_21);
											}
										}
									}
								else
									{	/* Expand/initial.scm 1132 */
									BgL_tagzd21907zd2_4030:
										return
											BGl_errorz00zz__errorz00(BFALSE,
											BGl_string4248z00zzexpand_installz00, BgL_xz00_21);
									}
							}
						else
							{	/* Expand/initial.scm 1132 */
								goto BgL_tagzd21907zd2_4030;
							}
					}
				else
					{	/* Expand/initial.scm 1132 */
						goto BgL_tagzd21907zd2_4030;
					}
			}
		}

	}



/* map-check */
	obj_t BGl_mapzd2checkzd2zzexpand_installz00(obj_t BgL_xz00_25,
		obj_t BgL_ez00_26, obj_t BgL_nullzd2valzd2_27)
	{
		{	/* Expand/initial.scm 1153 */
			{
				obj_t BgL_opz00_4101;
				obj_t BgL_funz00_4102;
				obj_t BgL_argsz00_4103;
				obj_t BgL_actualz00_4104;
				obj_t BgL_opz00_4106;
				obj_t BgL_funz00_4107;
				obj_t BgL_actualsz00_4108;

				if (PAIRP(BgL_xz00_25))
					{	/* Expand/initial.scm 1154 */
						obj_t BgL_cdrzd21945zd2_4113;

						BgL_cdrzd21945zd2_4113 = CDR(((obj_t) BgL_xz00_25));
						if (PAIRP(BgL_cdrzd21945zd2_4113))
							{	/* Expand/initial.scm 1154 */
								if (NULLP(CDR(BgL_cdrzd21945zd2_4113)))
									{	/* Expand/initial.scm 1154 */
										obj_t BgL_arg3645z00_4117;

										BgL_arg3645z00_4117 = CAR(((obj_t) BgL_xz00_25));
										BGl_userzd2warningzd2zztools_errorz00(BgL_arg3645z00_4117,
											BGl_string4252z00zzexpand_installz00, BgL_xz00_25);
										return BgL_nullzd2valzd2_27;
									}
								else
									{	/* Expand/initial.scm 1154 */
										obj_t BgL_carzd21966zd2_4119;
										obj_t BgL_cdrzd21967zd2_4120;

										BgL_carzd21966zd2_4119 =
											CAR(((obj_t) BgL_cdrzd21945zd2_4113));
										BgL_cdrzd21967zd2_4120 =
											CDR(((obj_t) BgL_cdrzd21945zd2_4113));
										if (PAIRP(BgL_carzd21966zd2_4119))
											{	/* Expand/initial.scm 1154 */
												obj_t BgL_cdrzd21974zd2_4122;

												BgL_cdrzd21974zd2_4122 = CDR(BgL_carzd21966zd2_4119);
												if ((CAR(BgL_carzd21966zd2_4119) == CNST_TABLE_REF(6)))
													{	/* Expand/initial.scm 1154 */
														if (PAIRP(BgL_cdrzd21974zd2_4122))
															{	/* Expand/initial.scm 1154 */
																if (PAIRP(BgL_cdrzd21967zd2_4120))
																	{	/* Expand/initial.scm 1154 */
																		if (NULLP(CDR(BgL_cdrzd21967zd2_4120)))
																			{	/* Expand/initial.scm 1154 */
																				obj_t BgL_arg3655z00_4129;
																				obj_t BgL_arg3656z00_4130;
																				obj_t BgL_arg3658z00_4131;

																				BgL_arg3655z00_4129 =
																					CAR(((obj_t) BgL_xz00_25));
																				BgL_arg3656z00_4130 =
																					CAR(BgL_cdrzd21974zd2_4122);
																				BgL_arg3658z00_4131 =
																					CAR(BgL_cdrzd21967zd2_4120);
																				BgL_opz00_4101 = BgL_arg3655z00_4129;
																				BgL_funz00_4102 =
																					BgL_carzd21966zd2_4119;
																				BgL_argsz00_4103 = BgL_arg3656z00_4130;
																				BgL_actualz00_4104 =
																					BgL_arg3658z00_4131;
																				{	/* Expand/initial.scm 1159 */
																					long BgL_arityz00_4155;
																					obj_t BgL_tmpz00_4156;
																					obj_t BgL_ufunz00_4157;
																					obj_t BgL_msgzd2listzd2_4158;
																					obj_t BgL_locz00_4159;
																					obj_t BgL_msgez00_4160;

																					BgL_arityz00_4155 =
																						BGl_localzd2arityzd2zztools_argsz00
																						(BgL_argsz00_4103);
																					{	/* Expand/initial.scm 1160 */
																						obj_t BgL_arg3725z00_4193;

																						{	/* Expand/initial.scm 1160 */

																							{	/* Expand/initial.scm 1160 */

																								BgL_arg3725z00_4193 =
																									BGl_gensymz00zz__r4_symbols_6_4z00
																									(BFALSE);
																						}}
																						BgL_tmpz00_4156 =
																							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																							(BgL_arg3725z00_4193);
																					}
																					{	/* Expand/initial.scm 1161 */
																						obj_t BgL_arg3726z00_4195;

																						{	/* Expand/initial.scm 1161 */

																							{	/* Expand/initial.scm 1161 */

																								BgL_arg3726z00_4195 =
																									BGl_gensymz00zz__r4_symbols_6_4z00
																									(BFALSE);
																						}}
																						BgL_ufunz00_4157 =
																							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																							(BgL_arg3726z00_4195);
																					}
																					{	/* Expand/initial.scm 1162 */
																						obj_t BgL_arg3727z00_4197;

																						{	/* Expand/initial.scm 1162 */

																							{	/* Expand/initial.scm 1162 */

																								BgL_arg3727z00_4197 =
																									BGl_gensymz00zz__r4_symbols_6_4z00
																									(BFALSE);
																						}}
																						BgL_msgzd2listzd2_4158 =
																							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																							(BgL_arg3727z00_4197);
																					}
																					BgL_locz00_4159 =
																						BGl_findzd2locationzd2zztools_locationz00
																						(BgL_xz00_25);
																					{	/* Expand/initial.scm 1164 */
																						obj_t BgL_arg3728z00_4199;

																						{	/* Expand/initial.scm 1164 */
																							obj_t BgL_arg1455z00_5331;

																							BgL_arg1455z00_5331 =
																								SYMBOL_TO_STRING(
																								((obj_t) BgL_opz00_4101));
																							BgL_arg3728z00_4199 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_5331);
																						}
																						BgL_msgez00_4160 =
																							string_append(BgL_arg3728z00_4199,
																							BGl_string4249z00zzexpand_installz00);
																					}
																					{	/* Expand/initial.scm 1165 */
																						bool_t BgL_test4801z00_11502;

																						if ((BgL_arityz00_4155 == 1L))
																							{	/* Expand/initial.scm 1165 */
																								BgL_test4801z00_11502 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Expand/initial.scm 1165 */
																								BgL_test4801z00_11502 =
																									(BgL_arityz00_4155 == -1L);
																							}
																						if (BgL_test4801z00_11502)
																							{	/* Expand/initial.scm 1167 */
																								obj_t BgL_arg3687z00_4163;

																								{	/* Expand/initial.scm 1167 */
																									obj_t BgL_arg3688z00_4164;
																									obj_t BgL_arg3690z00_4165;

																									BgL_arg3688z00_4164 =
																										BGl_letzd2symzd2zzast_letz00
																										();
																									{	/* Expand/initial.scm 1167 */
																										obj_t BgL_arg3691z00_4166;
																										obj_t BgL_arg3692z00_4167;

																										{	/* Expand/initial.scm 1167 */
																											obj_t BgL_arg3693z00_4168;
																											obj_t BgL_arg3694z00_4169;

																											{	/* Expand/initial.scm 1167 */
																												obj_t
																													BgL_arg3695z00_4170;
																												{	/* Expand/initial.scm 1167 */
																													obj_t
																														BgL_arg3696z00_4171;
																													BgL_arg3696z00_4171 =
																														BGL_PROCEDURE_CALL2
																														(BgL_ez00_26,
																														BgL_actualz00_4104,
																														BgL_ez00_26);
																													BgL_arg3695z00_4170 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg3696z00_4171,
																														BNIL);
																												}
																												BgL_arg3693z00_4168 =
																													MAKE_YOUNG_PAIR
																													(BgL_tmpz00_4156,
																													BgL_arg3695z00_4170);
																											}
																											{	/* Expand/initial.scm 1168 */
																												obj_t
																													BgL_arg3697z00_4172;
																												{	/* Expand/initial.scm 1168 */
																													obj_t
																														BgL_arg3699z00_4173;
																													{	/* Expand/initial.scm 1168 */
																														obj_t
																															BgL_arg3700z00_4174;
																														BgL_arg3700z00_4174
																															=
																															BGL_PROCEDURE_CALL2
																															(BgL_ez00_26,
																															BgL_funz00_4102,
																															BgL_ez00_26);
																														BgL_arg3699z00_4173
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg3700z00_4174,
																															BNIL);
																													}
																													BgL_arg3697z00_4172 =
																														MAKE_YOUNG_PAIR
																														(BgL_ufunz00_4157,
																														BgL_arg3699z00_4173);
																												}
																												BgL_arg3694z00_4169 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg3697z00_4172,
																													BNIL);
																											}
																											BgL_arg3691z00_4166 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg3693z00_4168,
																												BgL_arg3694z00_4169);
																										}
																										{	/* Expand/initial.scm 1169 */
																											obj_t BgL_arg3703z00_4175;

																											{	/* Expand/initial.scm 1169 */
																												obj_t
																													BgL_arg3704z00_4176;
																												obj_t
																													BgL_arg3705z00_4177;
																												BgL_arg3704z00_4176 =
																													BGl_ifzd2symzd2zzast_sexpz00
																													();
																												{	/* Expand/initial.scm 1169 */
																													obj_t
																														BgL_arg3706z00_4178;
																													obj_t
																														BgL_arg3707z00_4179;
																													{	/* Expand/initial.scm 1169 */
																														obj_t
																															BgL_arg3708z00_4180;
																														BgL_arg3708z00_4180
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_tmpz00_4156,
																															BNIL);
																														BgL_arg3706z00_4178
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(212),
																															BgL_arg3708z00_4180);
																													}
																													{	/* Expand/initial.scm 1170 */
																														obj_t
																															BgL_arg3710z00_4181;
																														obj_t
																															BgL_arg3713z00_4182;
																														{	/* Expand/initial.scm 1170 */
																															obj_t
																																BgL_arg3714z00_4183;
																															{	/* Expand/initial.scm 1170 */
																																obj_t
																																	BgL_arg3715z00_4184;
																																BgL_arg3715z00_4184
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_tmpz00_4156,
																																	BNIL);
																																BgL_arg3714z00_4183
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_ufunz00_4157,
																																	BgL_arg3715z00_4184);
																															}
																															BgL_arg3710z00_4181
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_opz00_4101,
																																BgL_arg3714z00_4183);
																														}
																														{	/* Expand/initial.scm 1171 */
																															obj_t
																																BgL_arg3716z00_4185;
																															{	/* Expand/initial.scm 1171 */
																																obj_t
																																	BgL_arg3717z00_4186;
																																obj_t
																																	BgL_arg3718z00_4187;
																																{	/* Expand/initial.scm 1171 */
																																	obj_t
																																		BgL_arg3720z00_4188;
																																	{	/* Expand/initial.scm 1171 */
																																		obj_t
																																			BgL_arg3721z00_4189;
																																		BgL_arg3721z00_4189
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(213),
																																			BNIL);
																																		BgL_arg3720z00_4188
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(19),
																																			BgL_arg3721z00_4189);
																																	}
																																	BgL_arg3717z00_4186
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(171),
																																		BgL_arg3720z00_4188);
																																}
																																{	/* Expand/initial.scm 1171 */
																																	obj_t
																																		BgL_arg3722z00_4190;
																																	{	/* Expand/initial.scm 1171 */
																																		obj_t
																																			BgL_arg3723z00_4191;
																																		BgL_arg3723z00_4191
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_tmpz00_4156,
																																			BNIL);
																																		BgL_arg3722z00_4190
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_msgez00_4160,
																																			BgL_arg3723z00_4191);
																																	}
																																	BgL_arg3718z00_4187
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BFALSE,
																																		BgL_arg3722z00_4190);
																																}
																																BgL_arg3716z00_4185
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg3717z00_4186,
																																	BgL_arg3718z00_4187);
																															}
																															BgL_arg3713z00_4182
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg3716z00_4185,
																																BNIL);
																														}
																														BgL_arg3707z00_4179
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg3710z00_4181,
																															BgL_arg3713z00_4182);
																													}
																													BgL_arg3705z00_4177 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg3706z00_4178,
																														BgL_arg3707z00_4179);
																												}
																												BgL_arg3703z00_4175 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg3704z00_4176,
																													BgL_arg3705z00_4177);
																											}
																											BgL_arg3692z00_4167 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg3703z00_4175,
																												BNIL);
																										}
																										BgL_arg3690z00_4165 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg3691z00_4166,
																											BgL_arg3692z00_4167);
																									}
																									BgL_arg3687z00_4163 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg3688z00_4164,
																										BgL_arg3690z00_4165);
																								}
																								return
																									BGl_epairifyzd2reczd2zztools_miscz00
																									(BgL_arg3687z00_4163,
																									BgL_xz00_25);
																							}
																						else
																							{	/* Expand/initial.scm 1165 */
																								return
																									BGl_errorz00zz__errorz00
																									(BgL_opz00_4101,
																									BGl_string4251z00zzexpand_installz00,
																									BgL_xz00_25);
																							}
																					}
																				}
																			}
																		else
																			{	/* Expand/initial.scm 1154 */
																				obj_t BgL_cdrzd21990zd2_4132;

																				BgL_cdrzd21990zd2_4132 =
																					CDR(((obj_t) BgL_xz00_25));
																				{	/* Expand/initial.scm 1154 */
																					obj_t BgL_arg3659z00_4133;
																					obj_t BgL_arg3660z00_4134;
																					obj_t BgL_arg3661z00_4135;

																					BgL_arg3659z00_4133 =
																						CAR(((obj_t) BgL_xz00_25));
																					BgL_arg3660z00_4134 =
																						CAR(
																						((obj_t) BgL_cdrzd21990zd2_4132));
																					BgL_arg3661z00_4135 =
																						CDR(
																						((obj_t) BgL_cdrzd21990zd2_4132));
																					BgL_opz00_4106 = BgL_arg3659z00_4133;
																					BgL_funz00_4107 = BgL_arg3660z00_4134;
																					BgL_actualsz00_4108 =
																						BgL_arg3661z00_4135;
																				BgL_tagzd21934zd2_4109:
																					{	/* Expand/initial.scm 1175 */
																						obj_t BgL_formalsz00_4200;
																						obj_t BgL_lformalsz00_4201;
																						obj_t BgL_ufunz00_4202;
																						obj_t BgL_msgzd2listzd2_4203;
																						obj_t BgL_locz00_4204;

																						if (NULLP(BgL_actualsz00_4108))
																							{	/* Expand/initial.scm 1175 */
																								BgL_formalsz00_4200 = BNIL;
																							}
																						else
																							{	/* Expand/initial.scm 1175 */
																								obj_t BgL_head1414z00_4317;

																								BgL_head1414z00_4317 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1412z00_4319;
																									obj_t BgL_tail1415z00_4320;

																									BgL_l1412z00_4319 =
																										BgL_actualsz00_4108;
																									BgL_tail1415z00_4320 =
																										BgL_head1414z00_4317;
																								BgL_zc3z04anonymousza33822ze3z87_4321:
																									if (NULLP
																										(BgL_l1412z00_4319))
																										{	/* Expand/initial.scm 1175 */
																											BgL_formalsz00_4200 =
																												CDR
																												(BgL_head1414z00_4317);
																										}
																									else
																										{	/* Expand/initial.scm 1175 */
																											obj_t
																												BgL_newtail1416z00_4323;
																											{	/* Expand/initial.scm 1175 */
																												obj_t
																													BgL_arg3825z00_4325;
																												{	/* Expand/initial.scm 1175 */
																													obj_t
																														BgL_arg3826z00_4327;
																													{	/* Expand/initial.scm 1175 */

																														{	/* Expand/initial.scm 1175 */

																															BgL_arg3826z00_4327
																																=
																																BGl_gensymz00zz__r4_symbols_6_4z00
																																(BFALSE);
																														}
																													}
																													BgL_arg3825z00_4325 =
																														BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																														(BgL_arg3826z00_4327);
																												}
																												BgL_newtail1416z00_4323
																													=
																													MAKE_YOUNG_PAIR
																													(BgL_arg3825z00_4325,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1415z00_4320,
																												BgL_newtail1416z00_4323);
																											{	/* Expand/initial.scm 1175 */
																												obj_t
																													BgL_arg3824z00_4324;
																												BgL_arg3824z00_4324 =
																													CDR(((obj_t)
																														BgL_l1412z00_4319));
																												{
																													obj_t
																														BgL_tail1415z00_11570;
																													obj_t
																														BgL_l1412z00_11569;
																													BgL_l1412z00_11569 =
																														BgL_arg3824z00_4324;
																													BgL_tail1415z00_11570
																														=
																														BgL_newtail1416z00_4323;
																													BgL_tail1415z00_4320 =
																														BgL_tail1415z00_11570;
																													BgL_l1412z00_4319 =
																														BgL_l1412z00_11569;
																													goto
																														BgL_zc3z04anonymousza33822ze3z87_4321;
																												}
																											}
																										}
																								}
																							}
																						if (NULLP(BgL_actualsz00_4108))
																							{	/* Expand/initial.scm 1177 */
																								BgL_lformalsz00_4201 = BNIL;
																							}
																						else
																							{	/* Expand/initial.scm 1177 */
																								obj_t BgL_head1419z00_4332;

																								BgL_head1419z00_4332 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1417z00_4334;
																									obj_t BgL_tail1420z00_4335;

																									BgL_l1417z00_4334 =
																										BgL_actualsz00_4108;
																									BgL_tail1420z00_4335 =
																										BgL_head1419z00_4332;
																								BgL_zc3z04anonymousza33828ze3z87_4336:
																									if (NULLP
																										(BgL_l1417z00_4334))
																										{	/* Expand/initial.scm 1177 */
																											BgL_lformalsz00_4201 =
																												CDR
																												(BgL_head1419z00_4332);
																										}
																									else
																										{	/* Expand/initial.scm 1177 */
																											obj_t
																												BgL_newtail1421z00_4338;
																											{	/* Expand/initial.scm 1177 */
																												obj_t
																													BgL_arg3831z00_4340;
																												{	/* Expand/initial.scm 1177 */
																													obj_t
																														BgL_arg3832z00_4342;
																													{	/* Expand/initial.scm 1177 */

																														{	/* Expand/initial.scm 1177 */

																															BgL_arg3832z00_4342
																																=
																																BGl_gensymz00zz__r4_symbols_6_4z00
																																(BFALSE);
																														}
																													}
																													BgL_arg3831z00_4340 =
																														BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																														(BgL_arg3832z00_4342);
																												}
																												BgL_newtail1421z00_4338
																													=
																													MAKE_YOUNG_PAIR
																													(BgL_arg3831z00_4340,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1420z00_4335,
																												BgL_newtail1421z00_4338);
																											{	/* Expand/initial.scm 1177 */
																												obj_t
																													BgL_arg3830z00_4339;
																												BgL_arg3830z00_4339 =
																													CDR(((obj_t)
																														BgL_l1417z00_4334));
																												{
																													obj_t
																														BgL_tail1420z00_11584;
																													obj_t
																														BgL_l1417z00_11583;
																													BgL_l1417z00_11583 =
																														BgL_arg3830z00_4339;
																													BgL_tail1420z00_11584
																														=
																														BgL_newtail1421z00_4338;
																													BgL_tail1420z00_4335 =
																														BgL_tail1420z00_11584;
																													BgL_l1417z00_4334 =
																														BgL_l1417z00_11583;
																													goto
																														BgL_zc3z04anonymousza33828ze3z87_4336;
																												}
																											}
																										}
																								}
																							}
																						{	/* Expand/initial.scm 1179 */
																							obj_t BgL_arg3833z00_4345;

																							{	/* Expand/initial.scm 1179 */

																								{	/* Expand/initial.scm 1179 */

																									BgL_arg3833z00_4345 =
																										BGl_gensymz00zz__r4_symbols_6_4z00
																										(BFALSE);
																								}
																							}
																							BgL_ufunz00_4202 =
																								BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																								(BgL_arg3833z00_4345);
																						}
																						{	/* Expand/initial.scm 1180 */
																							obj_t BgL_arg3834z00_4347;

																							{	/* Expand/initial.scm 1180 */

																								{	/* Expand/initial.scm 1180 */

																									BgL_arg3834z00_4347 =
																										BGl_gensymz00zz__r4_symbols_6_4z00
																										(BFALSE);
																								}
																							}
																							BgL_msgzd2listzd2_4203 =
																								BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																								(BgL_arg3834z00_4347);
																						}
																						BgL_locz00_4204 =
																							BGl_findzd2locationzd2zztools_locationz00
																							(BgL_xz00_25);
																						{	/* Expand/initial.scm 1183 */
																							obj_t BgL_arg3729z00_4205;

																							{	/* Expand/initial.scm 1183 */
																								obj_t BgL_arg3730z00_4206;
																								obj_t BgL_arg3731z00_4207;

																								BgL_arg3730z00_4206 =
																									BGl_letzd2symzd2zzast_letz00
																									();
																								{	/* Expand/initial.scm 1183 */
																									obj_t BgL_arg3733z00_4208;
																									obj_t BgL_arg3734z00_4209;

																									{	/* Expand/initial.scm 1183 */
																										obj_t BgL_arg3736z00_4210;
																										obj_t BgL_arg3737z00_4211;

																										if (NULLP
																											(BgL_formalsz00_4200))
																											{	/* Expand/initial.scm 1183 */
																												BgL_arg3736z00_4210 =
																													BNIL;
																											}
																										else
																											{	/* Expand/initial.scm 1183 */
																												obj_t
																													BgL_head1424z00_4215;
																												BgL_head1424z00_4215 =
																													MAKE_YOUNG_PAIR(BNIL,
																													BNIL);
																												{
																													obj_t
																														BgL_ll1422z00_4217;
																													obj_t
																														BgL_ll1423z00_4218;
																													obj_t
																														BgL_tail1425z00_4219;
																													BgL_ll1422z00_4217 =
																														BgL_formalsz00_4200;
																													BgL_ll1423z00_4218 =
																														BgL_actualsz00_4108;
																													BgL_tail1425z00_4219 =
																														BgL_head1424z00_4215;
																												BgL_zc3z04anonymousza33739ze3z87_4220:
																													if (NULLP
																														(BgL_ll1422z00_4217))
																														{	/* Expand/initial.scm 1183 */
																															BgL_arg3736z00_4210
																																=
																																CDR
																																(BgL_head1424z00_4215);
																														}
																													else
																														{	/* Expand/initial.scm 1183 */
																															obj_t
																																BgL_newtail1426z00_4222;
																															{	/* Expand/initial.scm 1183 */
																																obj_t
																																	BgL_arg3744z00_4225;
																																{	/* Expand/initial.scm 1183 */
																																	obj_t
																																		BgL_fz00_4226;
																																	obj_t
																																		BgL_vz00_4227;
																																	BgL_fz00_4226
																																		=
																																		CAR(((obj_t)
																																			BgL_ll1422z00_4217));
																																	BgL_vz00_4227
																																		=
																																		CAR(((obj_t)
																																			BgL_ll1423z00_4218));
																																	{	/* Expand/initial.scm 1183 */
																																		obj_t
																																			BgL_arg3745z00_4228;
																																		BgL_arg3745z00_4228
																																			=
																																			BGL_PROCEDURE_CALL2
																																			(BgL_ez00_26,
																																			BgL_vz00_4227,
																																			BgL_ez00_26);
																																		{	/* Expand/initial.scm 1183 */
																																			obj_t
																																				BgL_list3746z00_4229;
																																			{	/* Expand/initial.scm 1183 */
																																				obj_t
																																					BgL_arg3747z00_4230;
																																				BgL_arg3747z00_4230
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg3745z00_4228,
																																					BNIL);
																																				BgL_list3746z00_4229
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_fz00_4226,
																																					BgL_arg3747z00_4230);
																																			}
																																			BgL_arg3744z00_4225
																																				=
																																				BgL_list3746z00_4229;
																																		}
																																	}
																																}
																																BgL_newtail1426z00_4222
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg3744z00_4225,
																																	BNIL);
																															}
																															SET_CDR
																																(BgL_tail1425z00_4219,
																																BgL_newtail1426z00_4222);
																															{	/* Expand/initial.scm 1183 */
																																obj_t
																																	BgL_arg3741z00_4223;
																																obj_t
																																	BgL_arg3743z00_4224;
																																BgL_arg3741z00_4223
																																	=
																																	CDR(((obj_t)
																																		BgL_ll1422z00_4217));
																																BgL_arg3743z00_4224
																																	=
																																	CDR(((obj_t)
																																		BgL_ll1423z00_4218));
																																{
																																	obj_t
																																		BgL_tail1425z00_11616;
																																	obj_t
																																		BgL_ll1423z00_11615;
																																	obj_t
																																		BgL_ll1422z00_11614;
																																	BgL_ll1422z00_11614
																																		=
																																		BgL_arg3741z00_4223;
																																	BgL_ll1423z00_11615
																																		=
																																		BgL_arg3743z00_4224;
																																	BgL_tail1425z00_11616
																																		=
																																		BgL_newtail1426z00_4222;
																																	BgL_tail1425z00_4219
																																		=
																																		BgL_tail1425z00_11616;
																																	BgL_ll1423z00_4218
																																		=
																																		BgL_ll1423z00_11615;
																																	BgL_ll1422z00_4217
																																		=
																																		BgL_ll1422z00_11614;
																																	goto
																																		BgL_zc3z04anonymousza33739ze3z87_4220;
																																}
																															}
																														}
																												}
																											}
																										{	/* Expand/initial.scm 1184 */
																											obj_t BgL_arg3748z00_4232;
																											obj_t BgL_arg3749z00_4233;

																											{	/* Expand/initial.scm 1184 */
																												obj_t
																													BgL_arg3750z00_4234;
																												{	/* Expand/initial.scm 1184 */
																													obj_t
																														BgL_arg3751z00_4235;
																													BgL_arg3751z00_4235 =
																														BGL_PROCEDURE_CALL2
																														(BgL_ez00_26,
																														BgL_funz00_4107,
																														BgL_ez00_26);
																													BgL_arg3750z00_4234 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg3751z00_4235,
																														BNIL);
																												}
																												BgL_arg3748z00_4232 =
																													MAKE_YOUNG_PAIR
																													(BgL_ufunz00_4202,
																													BgL_arg3750z00_4234);
																											}
																											{	/* Expand/initial.scm 1185 */
																												obj_t
																													BgL_arg3752z00_4236;
																												{	/* Expand/initial.scm 1185 */
																													obj_t
																														BgL_arg3753z00_4237;
																													{	/* Expand/initial.scm 1185 */
																														obj_t
																															BgL_arg3755z00_4238;
																														{	/* Expand/initial.scm 1185 */
																															obj_t
																																BgL_arg3756z00_4239;
																															{	/* Expand/initial.scm 1185 */
																																obj_t
																																	BgL_arg1455z00_5350;
																																BgL_arg1455z00_5350
																																	=
																																	SYMBOL_TO_STRING
																																	(((obj_t)
																																		BgL_opz00_4106));
																																BgL_arg3756z00_4239
																																	=
																																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																	(BgL_arg1455z00_5350);
																															}
																															BgL_arg3755z00_4238
																																=
																																string_append
																																(BgL_arg3756z00_4239,
																																BGl_string4249z00zzexpand_installz00);
																														}
																														BgL_arg3753z00_4237
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg3755z00_4238,
																															BNIL);
																													}
																													BgL_arg3752z00_4236 =
																														MAKE_YOUNG_PAIR
																														(BgL_msgzd2listzd2_4203,
																														BgL_arg3753z00_4237);
																												}
																												BgL_arg3749z00_4233 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg3752z00_4236,
																													BNIL);
																											}
																											BgL_arg3737z00_4211 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg3748z00_4232,
																												BgL_arg3749z00_4233);
																										}
																										BgL_arg3733z00_4208 =
																											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																											(BgL_arg3736z00_4210,
																											BgL_arg3737z00_4211);
																									}
																									{	/* Expand/initial.scm 1187 */
																										obj_t BgL_arg3757z00_4240;

																										{	/* Expand/initial.scm 1187 */
																											obj_t BgL_arg3759z00_4241;

																											{	/* Expand/initial.scm 1187 */
																												obj_t
																													BgL_arg3760z00_4242;
																												obj_t
																													BgL_arg3761z00_4243;
																												{	/* Expand/initial.scm 1187 */
																													obj_t
																														BgL_arg3762z00_4244;
																													{	/* Expand/initial.scm 1187 */
																														obj_t
																															BgL_arg3763z00_4245;
																														{	/* Expand/initial.scm 1187 */
																															long
																																BgL_arg3764z00_4246;
																															BgL_arg3764z00_4246
																																=
																																bgl_list_length
																																(BgL_actualsz00_4108);
																															BgL_arg3763z00_4245
																																=
																																MAKE_YOUNG_PAIR
																																(BINT
																																(BgL_arg3764z00_4246),
																																BNIL);
																														}
																														BgL_arg3762z00_4244
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_ufunz00_4202,
																															BgL_arg3763z00_4245);
																													}
																													BgL_arg3760z00_4242 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF
																														(201),
																														BgL_arg3762z00_4244);
																												}
																												{	/* Expand/initial.scm 1189 */
																													obj_t
																														BgL_arg3766z00_4247;
																													obj_t
																														BgL_arg3767z00_4248;
																													BgL_arg3766z00_4247 =
																														BGl_loopze70ze7zzexpand_installz00
																														(BgL_actualsz00_4108,
																														BgL_locz00_4204,
																														BgL_lformalsz00_4201,
																														BgL_opz00_4106,
																														BgL_ufunz00_4202,
																														BgL_formalsz00_4200,
																														BgL_msgzd2listzd2_4203,
																														BgL_formalsz00_4200);
																													{	/* Expand/initial.scm 1208 */
																														obj_t
																															BgL_arg3819z00_4313;
																														{	/* Expand/initial.scm 1208 */
																															long
																																BgL_arg3820z00_4314;
																															BgL_arg3820z00_4314
																																=
																																bgl_list_length
																																(BgL_actualsz00_4108);
																															BgL_arg3819z00_4313
																																=
																																BGl_errzf2loczf2zzexpand_installz00
																																(BgL_locz00_4204,
																																BgL_opz00_4106,
																																BGl_string4250z00zzexpand_installz00,
																																BINT
																																(BgL_arg3820z00_4314));
																														}
																														BgL_arg3767z00_4248
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg3819z00_4313,
																															BNIL);
																													}
																													BgL_arg3761z00_4243 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg3766z00_4247,
																														BgL_arg3767z00_4248);
																												}
																												BgL_arg3759z00_4241 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg3760z00_4242,
																													BgL_arg3761z00_4243);
																											}
																											BgL_arg3757z00_4240 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(2),
																												BgL_arg3759z00_4241);
																										}
																										BgL_arg3734z00_4209 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg3757z00_4240,
																											BNIL);
																									}
																									BgL_arg3731z00_4207 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg3733z00_4208,
																										BgL_arg3734z00_4209);
																								}
																								BgL_arg3729z00_4205 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg3730z00_4206,
																									BgL_arg3731z00_4207);
																							}
																							return
																								BGl_epairifyzd2reczd2zztools_miscz00
																								(BgL_arg3729z00_4205,
																								BgL_xz00_25);
																						}
																					}
																				}
																			}
																	}
																else
																	{	/* Expand/initial.scm 1154 */
																		obj_t BgL_cdrzd22005zd2_4137;

																		BgL_cdrzd22005zd2_4137 =
																			CDR(((obj_t) BgL_xz00_25));
																		{	/* Expand/initial.scm 1154 */
																			obj_t BgL_arg3664z00_4138;
																			obj_t BgL_arg3665z00_4139;
																			obj_t BgL_arg3668z00_4140;

																			BgL_arg3664z00_4138 =
																				CAR(((obj_t) BgL_xz00_25));
																			BgL_arg3665z00_4139 =
																				CAR(((obj_t) BgL_cdrzd22005zd2_4137));
																			BgL_arg3668z00_4140 =
																				CDR(((obj_t) BgL_cdrzd22005zd2_4137));
																			{
																				obj_t BgL_actualsz00_11662;
																				obj_t BgL_funz00_11661;
																				obj_t BgL_opz00_11660;

																				BgL_opz00_11660 = BgL_arg3664z00_4138;
																				BgL_funz00_11661 = BgL_arg3665z00_4139;
																				BgL_actualsz00_11662 =
																					BgL_arg3668z00_4140;
																				BgL_actualsz00_4108 =
																					BgL_actualsz00_11662;
																				BgL_funz00_4107 = BgL_funz00_11661;
																				BgL_opz00_4106 = BgL_opz00_11660;
																				goto BgL_tagzd21934zd2_4109;
																			}
																		}
																	}
															}
														else
															{	/* Expand/initial.scm 1154 */
																obj_t BgL_cdrzd22020zd2_4141;

																BgL_cdrzd22020zd2_4141 =
																	CDR(((obj_t) BgL_xz00_25));
																{	/* Expand/initial.scm 1154 */
																	obj_t BgL_arg3669z00_4142;
																	obj_t BgL_arg3671z00_4143;
																	obj_t BgL_arg3672z00_4144;

																	BgL_arg3669z00_4142 =
																		CAR(((obj_t) BgL_xz00_25));
																	BgL_arg3671z00_4143 =
																		CAR(((obj_t) BgL_cdrzd22020zd2_4141));
																	BgL_arg3672z00_4144 =
																		CDR(((obj_t) BgL_cdrzd22020zd2_4141));
																	{
																		obj_t BgL_actualsz00_11673;
																		obj_t BgL_funz00_11672;
																		obj_t BgL_opz00_11671;

																		BgL_opz00_11671 = BgL_arg3669z00_4142;
																		BgL_funz00_11672 = BgL_arg3671z00_4143;
																		BgL_actualsz00_11673 = BgL_arg3672z00_4144;
																		BgL_actualsz00_4108 = BgL_actualsz00_11673;
																		BgL_funz00_4107 = BgL_funz00_11672;
																		BgL_opz00_4106 = BgL_opz00_11671;
																		goto BgL_tagzd21934zd2_4109;
																	}
																}
															}
													}
												else
													{	/* Expand/initial.scm 1154 */
														obj_t BgL_cdrzd22035zd2_4145;

														BgL_cdrzd22035zd2_4145 = CDR(((obj_t) BgL_xz00_25));
														{	/* Expand/initial.scm 1154 */
															obj_t BgL_arg3674z00_4146;
															obj_t BgL_arg3675z00_4147;
															obj_t BgL_arg3677z00_4148;

															BgL_arg3674z00_4146 = CAR(((obj_t) BgL_xz00_25));
															BgL_arg3675z00_4147 =
																CAR(((obj_t) BgL_cdrzd22035zd2_4145));
															BgL_arg3677z00_4148 =
																CDR(((obj_t) BgL_cdrzd22035zd2_4145));
															{
																obj_t BgL_actualsz00_11684;
																obj_t BgL_funz00_11683;
																obj_t BgL_opz00_11682;

																BgL_opz00_11682 = BgL_arg3674z00_4146;
																BgL_funz00_11683 = BgL_arg3675z00_4147;
																BgL_actualsz00_11684 = BgL_arg3677z00_4148;
																BgL_actualsz00_4108 = BgL_actualsz00_11684;
																BgL_funz00_4107 = BgL_funz00_11683;
																BgL_opz00_4106 = BgL_opz00_11682;
																goto BgL_tagzd21934zd2_4109;
															}
														}
													}
											}
										else
											{	/* Expand/initial.scm 1154 */
												obj_t BgL_arg3680z00_4151;
												obj_t BgL_arg3681z00_4152;
												obj_t BgL_arg3683z00_4153;

												BgL_arg3680z00_4151 = CAR(((obj_t) BgL_xz00_25));
												BgL_arg3681z00_4152 =
													CAR(((obj_t) BgL_cdrzd21945zd2_4113));
												BgL_arg3683z00_4153 =
													CDR(((obj_t) BgL_cdrzd21945zd2_4113));
												{
													obj_t BgL_actualsz00_11693;
													obj_t BgL_funz00_11692;
													obj_t BgL_opz00_11691;

													BgL_opz00_11691 = BgL_arg3680z00_4151;
													BgL_funz00_11692 = BgL_arg3681z00_4152;
													BgL_actualsz00_11693 = BgL_arg3683z00_4153;
													BgL_actualsz00_4108 = BgL_actualsz00_11693;
													BgL_funz00_4107 = BgL_funz00_11692;
													BgL_opz00_4106 = BgL_opz00_11691;
													goto BgL_tagzd21934zd2_4109;
												}
											}
									}
							}
						else
							{	/* Expand/initial.scm 1154 */
							BgL_tagzd21935zd2_4110:
								{	/* Expand/initial.scm 1211 */
									obj_t BgL_arg3835z00_4349;

									BgL_arg3835z00_4349 = CAR(((obj_t) BgL_xz00_25));
									return
										BGl_errorz00zz__errorz00(BgL_arg3835z00_4349,
										BGl_string4218z00zzexpand_installz00, BgL_xz00_25);
								}
							}
					}
				else
					{	/* Expand/initial.scm 1154 */
						goto BgL_tagzd21935zd2_4110;
					}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzexpand_installz00(obj_t BgL_actualsz00_5965,
		obj_t BgL_locz00_5964, obj_t BgL_lformalsz00_5963, obj_t BgL_opz00_5962,
		obj_t BgL_ufunz00_5961, obj_t BgL_formalsz00_5960,
		obj_t BgL_msgzd2listzd2_5959, obj_t BgL_argsz00_4250)
	{
		{	/* Expand/initial.scm 1188 */
			if (NULLP(BgL_argsz00_4250))
				{	/* Expand/initial.scm 1189 */
					if ((bgl_list_length(BgL_actualsz00_5965) > 1L))
						{	/* Expand/initial.scm 1191 */
							obj_t BgL_arg3772z00_4255;
							obj_t BgL_arg3773z00_4256;

							BgL_arg3772z00_4255 = BGl_letzd2symzd2zzast_letz00();
							{	/* Expand/initial.scm 1192 */
								obj_t BgL_arg3774z00_4257;
								obj_t BgL_arg3776z00_4258;

								if (NULLP(BgL_lformalsz00_5963))
									{	/* Expand/initial.scm 1192 */
										BgL_arg3774z00_4257 = BNIL;
									}
								else
									{	/* Expand/initial.scm 1192 */
										obj_t BgL_head1430z00_4262;

										BgL_head1430z00_4262 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_ll1428z00_4264;
											obj_t BgL_ll1429z00_4265;
											obj_t BgL_tail1431z00_4266;

											BgL_ll1428z00_4264 = BgL_lformalsz00_5963;
											BgL_ll1429z00_4265 = BgL_formalsz00_5960;
											BgL_tail1431z00_4266 = BgL_head1430z00_4262;
										BgL_zc3z04anonymousza33778ze3z87_4267:
											if (NULLP(BgL_ll1428z00_4264))
												{	/* Expand/initial.scm 1192 */
													BgL_arg3774z00_4257 = CDR(BgL_head1430z00_4262);
												}
											else
												{	/* Expand/initial.scm 1192 */
													obj_t BgL_newtail1432z00_4269;

													{	/* Expand/initial.scm 1192 */
														obj_t BgL_arg3782z00_4272;

														{	/* Expand/initial.scm 1192 */
															obj_t BgL_lfz00_4273;
															obj_t BgL_fz00_4274;

															BgL_lfz00_4273 =
																CAR(((obj_t) BgL_ll1428z00_4264));
															BgL_fz00_4274 = CAR(((obj_t) BgL_ll1429z00_4265));
															{	/* Expand/initial.scm 1193 */
																obj_t BgL_arg3783z00_4275;

																{	/* Expand/initial.scm 1193 */
																	obj_t BgL_arg3784z00_4276;

																	{	/* Expand/initial.scm 1193 */
																		obj_t BgL_arg3785z00_4277;

																		BgL_arg3785z00_4277 =
																			MAKE_YOUNG_PAIR(BgL_fz00_4274, BNIL);
																		BgL_arg3784z00_4276 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(214),
																			BgL_arg3785z00_4277);
																	}
																	BgL_arg3783z00_4275 =
																		MAKE_YOUNG_PAIR(BgL_arg3784z00_4276, BNIL);
																}
																BgL_arg3782z00_4272 =
																	MAKE_YOUNG_PAIR(BgL_lfz00_4273,
																	BgL_arg3783z00_4275);
															}
														}
														BgL_newtail1432z00_4269 =
															MAKE_YOUNG_PAIR(BgL_arg3782z00_4272, BNIL);
													}
													SET_CDR(BgL_tail1431z00_4266,
														BgL_newtail1432z00_4269);
													{	/* Expand/initial.scm 1192 */
														obj_t BgL_arg3780z00_4270;
														obj_t BgL_arg3781z00_4271;

														BgL_arg3780z00_4270 =
															CDR(((obj_t) BgL_ll1428z00_4264));
														BgL_arg3781z00_4271 =
															CDR(((obj_t) BgL_ll1429z00_4265));
														{
															obj_t BgL_tail1431z00_11726;
															obj_t BgL_ll1429z00_11725;
															obj_t BgL_ll1428z00_11724;

															BgL_ll1428z00_11724 = BgL_arg3780z00_4270;
															BgL_ll1429z00_11725 = BgL_arg3781z00_4271;
															BgL_tail1431z00_11726 = BgL_newtail1432z00_4269;
															BgL_tail1431z00_4266 = BgL_tail1431z00_11726;
															BgL_ll1429z00_4265 = BgL_ll1429z00_11725;
															BgL_ll1428z00_4264 = BgL_ll1428z00_11724;
															goto BgL_zc3z04anonymousza33778ze3z87_4267;
														}
													}
												}
										}
									}
								{	/* Expand/initial.scm 1195 */
									obj_t BgL_arg3786z00_4279;

									{	/* Expand/initial.scm 1195 */
										obj_t BgL_arg3787z00_4280;
										obj_t BgL_arg3788z00_4281;

										BgL_arg3787z00_4280 = BGl_ifzd2symzd2zzast_sexpz00();
										{	/* Expand/initial.scm 1195 */
											obj_t BgL_arg3789z00_4282;
											obj_t BgL_arg3790z00_4283;

											{	/* Expand/initial.scm 1195 */
												obj_t BgL_arg3791z00_4284;

												BgL_arg3791z00_4284 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_lformalsz00_5963, BNIL);
												BgL_arg3789z00_4282 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(44),
													BgL_arg3791z00_4284);
											}
											{	/* Expand/initial.scm 1196 */
												obj_t BgL_arg3792z00_4285;
												obj_t BgL_arg3793z00_4286;

												{	/* Expand/initial.scm 1196 */
													obj_t BgL_arg3794z00_4287;

													BgL_arg3794z00_4287 =
														MAKE_YOUNG_PAIR(BgL_ufunz00_5961,
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_formalsz00_5960, BNIL));
													BgL_arg3792z00_4285 =
														MAKE_YOUNG_PAIR(BgL_opz00_5962,
														BgL_arg3794z00_4287);
												}
												{	/* Expand/initial.scm 1200 */
													obj_t BgL_arg3796z00_4289;

													{	/* Expand/initial.scm 1200 */
														obj_t BgL_arg3797z00_4290;

														{	/* Expand/initial.scm 1200 */
															obj_t BgL_arg3798z00_4291;

															BgL_arg3798z00_4291 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_lformalsz00_5963, BNIL);
															BgL_arg3797z00_4290 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(215),
																BgL_arg3798z00_4291);
														}
														BgL_arg3796z00_4289 =
															BGl_errzf2loczf2zzexpand_installz00
															(BgL_locz00_5964, BgL_opz00_5962,
															BGl_string4253z00zzexpand_installz00,
															BgL_arg3797z00_4290);
													}
													BgL_arg3793z00_4286 =
														MAKE_YOUNG_PAIR(BgL_arg3796z00_4289, BNIL);
												}
												BgL_arg3790z00_4283 =
													MAKE_YOUNG_PAIR(BgL_arg3792z00_4285,
													BgL_arg3793z00_4286);
											}
											BgL_arg3788z00_4281 =
												MAKE_YOUNG_PAIR(BgL_arg3789z00_4282,
												BgL_arg3790z00_4283);
										}
										BgL_arg3786z00_4279 =
											MAKE_YOUNG_PAIR(BgL_arg3787z00_4280, BgL_arg3788z00_4281);
									}
									BgL_arg3776z00_4258 =
										MAKE_YOUNG_PAIR(BgL_arg3786z00_4279, BNIL);
								}
								BgL_arg3773z00_4256 =
									MAKE_YOUNG_PAIR(BgL_arg3774z00_4257, BgL_arg3776z00_4258);
							}
							return MAKE_YOUNG_PAIR(BgL_arg3772z00_4255, BgL_arg3773z00_4256);
						}
					else
						{	/* Expand/initial.scm 1201 */
							obj_t BgL_arg3799z00_4292;

							BgL_arg3799z00_4292 =
								MAKE_YOUNG_PAIR(BgL_ufunz00_5961,
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_formalsz00_5960, BNIL));
							return MAKE_YOUNG_PAIR(BgL_opz00_5962, BgL_arg3799z00_4292);
						}
				}
			else
				{	/* Expand/initial.scm 1202 */
					obj_t BgL_arg3802z00_4295;
					obj_t BgL_arg3803z00_4296;

					BgL_arg3802z00_4295 = BGl_ifzd2symzd2zzast_sexpz00();
					{	/* Expand/initial.scm 1202 */
						obj_t BgL_arg3804z00_4297;
						obj_t BgL_arg3805z00_4298;

						{	/* Expand/initial.scm 1202 */
							obj_t BgL_arg3806z00_4299;

							{	/* Expand/initial.scm 1202 */
								obj_t BgL_arg3807z00_4300;

								BgL_arg3807z00_4300 = CAR(((obj_t) BgL_argsz00_4250));
								BgL_arg3806z00_4299 =
									MAKE_YOUNG_PAIR(BgL_arg3807z00_4300, BNIL);
							}
							BgL_arg3804z00_4297 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(212), BgL_arg3806z00_4299);
						}
						{	/* Expand/initial.scm 1203 */
							obj_t BgL_arg3808z00_4301;
							obj_t BgL_arg3809z00_4302;

							{	/* Expand/initial.scm 1203 */
								obj_t BgL_arg3810z00_4303;

								BgL_arg3810z00_4303 = CDR(((obj_t) BgL_argsz00_4250));
								BgL_arg3808z00_4301 =
									BGl_loopze70ze7zzexpand_installz00(BgL_actualsz00_5965,
									BgL_locz00_5964, BgL_lformalsz00_5963, BgL_opz00_5962,
									BgL_ufunz00_5961, BgL_formalsz00_5960, BgL_msgzd2listzd2_5959,
									BgL_arg3810z00_4303);
							}
							{	/* Expand/initial.scm 1204 */
								obj_t BgL_arg3811z00_4304;

								{	/* Expand/initial.scm 1204 */
									obj_t BgL_arg3812z00_4305;
									obj_t BgL_arg3813z00_4306;

									{	/* Expand/initial.scm 1204 */
										obj_t BgL_arg3814z00_4307;

										{	/* Expand/initial.scm 1204 */
											obj_t BgL_arg3815z00_4308;

											BgL_arg3815z00_4308 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(213), BNIL);
											BgL_arg3814z00_4307 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
												BgL_arg3815z00_4308);
										}
										BgL_arg3812z00_4305 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(171), BgL_arg3814z00_4307);
									}
									{	/* Expand/initial.scm 1204 */
										obj_t BgL_arg3816z00_4309;

										{	/* Expand/initial.scm 1204 */
											obj_t BgL_arg3817z00_4310;

											{	/* Expand/initial.scm 1204 */
												obj_t BgL_arg3818z00_4311;

												BgL_arg3818z00_4311 = CAR(((obj_t) BgL_argsz00_4250));
												BgL_arg3817z00_4310 =
													MAKE_YOUNG_PAIR(BgL_arg3818z00_4311, BNIL);
											}
											BgL_arg3816z00_4309 =
												MAKE_YOUNG_PAIR(BgL_msgzd2listzd2_5959,
												BgL_arg3817z00_4310);
										}
										BgL_arg3813z00_4306 =
											MAKE_YOUNG_PAIR(BFALSE, BgL_arg3816z00_4309);
									}
									BgL_arg3811z00_4304 =
										MAKE_YOUNG_PAIR(BgL_arg3812z00_4305, BgL_arg3813z00_4306);
								}
								BgL_arg3809z00_4302 =
									MAKE_YOUNG_PAIR(BgL_arg3811z00_4304, BNIL);
							}
							BgL_arg3805z00_4298 =
								MAKE_YOUNG_PAIR(BgL_arg3808z00_4301, BgL_arg3809z00_4302);
						}
						BgL_arg3803z00_4296 =
							MAKE_YOUNG_PAIR(BgL_arg3804z00_4297, BgL_arg3805z00_4298);
					}
					return MAKE_YOUNG_PAIR(BgL_arg3802z00_4295, BgL_arg3803z00_4296);
				}
		}

	}



/* err/loc */
	obj_t BGl_errzf2loczf2zzexpand_installz00(obj_t BgL_locz00_28,
		obj_t BgL_procz00_29, obj_t BgL_msgz00_30, obj_t BgL_objz00_31)
	{
		{	/* Expand/initial.scm 1216 */
			{	/* Expand/initial.scm 1217 */
				bool_t BgL_test4813z00_11773;

				if (STRUCTP(BgL_locz00_28))
					{	/* Expand/initial.scm 1217 */
						BgL_test4813z00_11773 =
							(STRUCT_KEY(BgL_locz00_28) == CNST_TABLE_REF(208));
					}
				else
					{	/* Expand/initial.scm 1217 */
						BgL_test4813z00_11773 = ((bool_t) 0);
					}
				if (BgL_test4813z00_11773)
					{	/* Expand/initial.scm 1218 */
						obj_t BgL_arg3837z00_4351;
						obj_t BgL_arg3838z00_4352;

						{	/* Expand/initial.scm 1218 */
							obj_t BgL_arg3839z00_4353;

							{	/* Expand/initial.scm 1218 */
								obj_t BgL_arg3840z00_4354;

								BgL_arg3840z00_4354 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(213), BNIL);
								BgL_arg3839z00_4353 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(216), BgL_arg3840z00_4354);
							}
							BgL_arg3837z00_4351 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(171), BgL_arg3839z00_4353);
						}
						{	/* Expand/initial.scm 1218 */
							obj_t BgL_arg3841z00_4355;
							obj_t BgL_arg3842z00_4356;

							{	/* Expand/initial.scm 1218 */
								obj_t BgL_arg3843z00_4357;

								BgL_arg3843z00_4357 = MAKE_YOUNG_PAIR(BgL_procz00_29, BNIL);
								BgL_arg3841z00_4355 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(190), BgL_arg3843z00_4357);
							}
							{	/* Expand/initial.scm 1221 */
								obj_t BgL_arg3844z00_4358;

								{	/* Expand/initial.scm 1221 */
									obj_t BgL_arg3845z00_4359;

									{	/* Expand/initial.scm 1221 */
										obj_t BgL_arg3846z00_4360;
										obj_t BgL_arg3847z00_4361;

										BgL_arg3846z00_4360 =
											BGl_locationzd2fullzd2fnamez00zztools_locationz00
											(BgL_locz00_28);
										BgL_arg3847z00_4361 =
											MAKE_YOUNG_PAIR(STRUCT_REF(BgL_locz00_28, (int) (1L)),
											BNIL);
										BgL_arg3845z00_4359 =
											MAKE_YOUNG_PAIR(BgL_arg3846z00_4360, BgL_arg3847z00_4361);
									}
									BgL_arg3844z00_4358 =
										MAKE_YOUNG_PAIR(BgL_objz00_31, BgL_arg3845z00_4359);
								}
								BgL_arg3842z00_4356 =
									MAKE_YOUNG_PAIR(BgL_msgz00_30, BgL_arg3844z00_4358);
							}
							BgL_arg3838z00_4352 =
								MAKE_YOUNG_PAIR(BgL_arg3841z00_4355, BgL_arg3842z00_4356);
						}
						return MAKE_YOUNG_PAIR(BgL_arg3837z00_4351, BgL_arg3838z00_4352);
					}
				else
					{	/* Expand/initial.scm 1223 */
						obj_t BgL_arg3849z00_4363;
						obj_t BgL_arg3850z00_4364;

						{	/* Expand/initial.scm 1223 */
							obj_t BgL_arg3851z00_4365;

							{	/* Expand/initial.scm 1223 */
								obj_t BgL_arg3852z00_4366;

								BgL_arg3852z00_4366 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(213), BNIL);
								BgL_arg3851z00_4365 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BgL_arg3852z00_4366);
							}
							BgL_arg3849z00_4363 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(171), BgL_arg3851z00_4365);
						}
						{	/* Expand/initial.scm 1223 */
							obj_t BgL_arg3853z00_4367;
							obj_t BgL_arg3854z00_4368;

							{	/* Expand/initial.scm 1223 */
								obj_t BgL_arg3855z00_4369;

								BgL_arg3855z00_4369 = MAKE_YOUNG_PAIR(BgL_procz00_29, BNIL);
								BgL_arg3853z00_4367 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(190), BgL_arg3855z00_4369);
							}
							{	/* Expand/initial.scm 1223 */
								obj_t BgL_arg3856z00_4370;

								BgL_arg3856z00_4370 = MAKE_YOUNG_PAIR(BgL_objz00_31, BNIL);
								BgL_arg3854z00_4368 =
									MAKE_YOUNG_PAIR(BgL_msgz00_30, BgL_arg3856z00_4370);
							}
							BgL_arg3850z00_4364 =
								MAKE_YOUNG_PAIR(BgL_arg3853z00_4367, BgL_arg3854z00_4368);
						}
						return MAKE_YOUNG_PAIR(BgL_arg3849z00_4363, BgL_arg3850z00_4364);
					}
			}
		}

	}



/* disp */
	obj_t BGl_dispz00zzexpand_installz00(obj_t BgL_objz00_32)
	{
		{	/* Expand/initial.scm 1228 */
			if (STRINGP(BgL_objz00_32))
				{	/* Expand/initial.scm 1230 */
					return CNST_TABLE_REF(217);
				}
			else
				{	/* Expand/initial.scm 1232 */
					bool_t BgL_test4816z00_11813;

					if (PAIRP(BgL_objz00_32))
						{	/* Expand/initial.scm 1232 */
							if ((CAR(BgL_objz00_32) == CNST_TABLE_REF(190)))
								{	/* Expand/initial.scm 1234 */
									obj_t BgL_tmpz00_11820;

									BgL_tmpz00_11820 = CAR(CDR(BgL_objz00_32));
									BgL_test4816z00_11813 = SYMBOLP(BgL_tmpz00_11820);
								}
							else
								{	/* Expand/initial.scm 1233 */
									BgL_test4816z00_11813 = ((bool_t) 0);
								}
						}
					else
						{	/* Expand/initial.scm 1232 */
							BgL_test4816z00_11813 = ((bool_t) 0);
						}
					if (BgL_test4816z00_11813)
						{	/* Expand/initial.scm 1232 */
							return CNST_TABLE_REF(218);
						}
					else
						{	/* Expand/initial.scm 1232 */
							if (INTEGERP(BgL_objz00_32))
								{	/* Expand/initial.scm 1236 */
									return CNST_TABLE_REF(219);
								}
							else
								{	/* Expand/initial.scm 1236 */
									if (REALP(BgL_objz00_32))
										{	/* Expand/initial.scm 1238 */
											return CNST_TABLE_REF(220);
										}
									else
										{	/* Expand/initial.scm 1238 */
											if (CHARP(BgL_objz00_32))
												{	/* Expand/initial.scm 1240 */
													return CNST_TABLE_REF(221);
												}
											else
												{	/* Expand/initial.scm 1240 */
													return CNST_TABLE_REF(222);
												}
										}
								}
						}
				}
		}

	}



/* %append-2-define */
	BGL_EXPORTED_DEF obj_t BGl_z52appendzd22zd2definez52zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 1266 */
			{	/* Expand/initial.scm 1267 */
				obj_t BgL_arg3869z00_4385;

				{	/* Expand/initial.scm 1267 */
					obj_t BgL_arg3870z00_4386;
					obj_t BgL_arg3871z00_4387;

					{	/* Expand/initial.scm 1267 */
						obj_t BgL_arg3872z00_4388;

						{	/* Expand/initial.scm 1267 */
							obj_t BgL_arg3873z00_4389;

							BgL_arg3873z00_4389 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(223), BNIL);
							BgL_arg3872z00_4388 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(224), BgL_arg3873z00_4389);
						}
						BgL_arg3870z00_4386 =
							MAKE_YOUNG_PAIR(BGl_z52appendzd22zd2idz52zzexpand_installz00,
							BgL_arg3872z00_4388);
					}
					{	/* Expand/initial.scm 1268 */
						obj_t BgL_arg3874z00_4390;

						{	/* Expand/initial.scm 1268 */
							obj_t BgL_arg3875z00_4391;

							{	/* Expand/initial.scm 1268 */
								obj_t BgL_arg3876z00_4392;
								obj_t BgL_arg3877z00_4393;

								{	/* Expand/initial.scm 1268 */
									obj_t BgL_arg3878z00_4394;

									{	/* Expand/initial.scm 1268 */
										obj_t BgL_arg3879z00_4395;

										{	/* Expand/initial.scm 1268 */
											obj_t BgL_arg3880z00_4396;

											{	/* Expand/initial.scm 1268 */
												obj_t BgL_arg3881z00_4397;

												{	/* Expand/initial.scm 1268 */
													obj_t BgL_arg3882z00_4398;
													obj_t BgL_arg3883z00_4399;

													{	/* Expand/initial.scm 1268 */
														obj_t BgL_arg3884z00_4400;

														BgL_arg3884z00_4400 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														BgL_arg3882z00_4398 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(190),
															BgL_arg3884z00_4400);
													}
													BgL_arg3883z00_4399 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(223), BNIL);
													BgL_arg3881z00_4397 =
														MAKE_YOUNG_PAIR(BgL_arg3882z00_4398,
														BgL_arg3883z00_4399);
												}
												BgL_arg3880z00_4396 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(25),
													BgL_arg3881z00_4397);
											}
											BgL_arg3879z00_4395 =
												MAKE_YOUNG_PAIR(BgL_arg3880z00_4396, BNIL);
										}
										BgL_arg3878z00_4394 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(225), BgL_arg3879z00_4395);
									}
									BgL_arg3876z00_4392 =
										MAKE_YOUNG_PAIR(BgL_arg3878z00_4394, BNIL);
								}
								{	/* Expand/initial.scm 1269 */
									obj_t BgL_arg3885z00_4401;

									{	/* Expand/initial.scm 1269 */
										obj_t BgL_arg3886z00_4402;

										{	/* Expand/initial.scm 1269 */
											obj_t BgL_arg3887z00_4403;
											obj_t BgL_arg3888z00_4404;

											{	/* Expand/initial.scm 1269 */
												obj_t BgL_arg3889z00_4405;

												{	/* Expand/initial.scm 1269 */
													obj_t BgL_arg3890z00_4406;

													{	/* Expand/initial.scm 1269 */
														obj_t BgL_arg3891z00_4407;
														obj_t BgL_arg3892z00_4408;

														{	/* Expand/initial.scm 1269 */
															obj_t BgL_arg3893z00_4409;

															BgL_arg3893z00_4409 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(226), BNIL);
															BgL_arg3891z00_4407 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(227),
																BgL_arg3893z00_4409);
														}
														{	/* Expand/initial.scm 1270 */
															obj_t BgL_arg3894z00_4410;

															{	/* Expand/initial.scm 1270 */
																obj_t BgL_arg3895z00_4411;

																{	/* Expand/initial.scm 1270 */
																	obj_t BgL_arg3896z00_4412;
																	obj_t BgL_arg3897z00_4413;

																	{	/* Expand/initial.scm 1270 */
																		obj_t BgL_arg3898z00_4414;

																		BgL_arg3898z00_4414 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(226),
																			BNIL);
																		BgL_arg3896z00_4412 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(228),
																			BgL_arg3898z00_4414);
																	}
																	{	/* Expand/initial.scm 1271 */
																		obj_t BgL_arg3899z00_4415;
																		obj_t BgL_arg3900z00_4416;

																		{	/* Expand/initial.scm 1271 */
																			obj_t BgL_arg3901z00_4417;

																			{	/* Expand/initial.scm 1271 */
																				obj_t BgL_arg3902z00_4418;
																				obj_t BgL_arg3903z00_4419;

																				{	/* Expand/initial.scm 1271 */
																					obj_t BgL_arg3904z00_4420;

																					{	/* Expand/initial.scm 1271 */
																						obj_t BgL_arg3905z00_4421;

																						{	/* Expand/initial.scm 1271 */
																							obj_t BgL_arg3906z00_4422;

																							{	/* Expand/initial.scm 1271 */
																								obj_t BgL_arg3907z00_4423;

																								{	/* Expand/initial.scm 1271 */
																									obj_t BgL_arg3908z00_4424;
																									obj_t BgL_arg3909z00_4425;

																									{	/* Expand/initial.scm 1271 */
																										obj_t BgL_arg3910z00_4426;

																										BgL_arg3910z00_4426 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(226),
																											BNIL);
																										BgL_arg3908z00_4424 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(229),
																											BgL_arg3910z00_4426);
																									}
																									BgL_arg3909z00_4425 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(223), BNIL);
																									BgL_arg3907z00_4423 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg3908z00_4424,
																										BgL_arg3909z00_4425);
																								}
																								BgL_arg3906z00_4422 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(25), BgL_arg3907z00_4423);
																							}
																							BgL_arg3905z00_4421 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg3906z00_4422, BNIL);
																						}
																						BgL_arg3904z00_4420 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(230), BgL_arg3905z00_4421);
																					}
																					BgL_arg3902z00_4418 =
																						MAKE_YOUNG_PAIR(BgL_arg3904z00_4420,
																						BNIL);
																				}
																				{	/* Expand/initial.scm 1272 */
																					obj_t BgL_arg3911z00_4427;
																					obj_t BgL_arg3912z00_4428;

																					{	/* Expand/initial.scm 1272 */
																						obj_t BgL_arg3913z00_4429;

																						{	/* Expand/initial.scm 1272 */
																							obj_t BgL_arg3914z00_4430;

																							BgL_arg3914z00_4430 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(230), BNIL);
																							BgL_arg3913z00_4429 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(227), BgL_arg3914z00_4430);
																						}
																						BgL_arg3911z00_4427 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(231), BgL_arg3913z00_4429);
																					}
																					{	/* Expand/initial.scm 1273 */
																						obj_t BgL_arg3915z00_4431;

																						{	/* Expand/initial.scm 1273 */
																							obj_t BgL_arg3916z00_4432;

																							{	/* Expand/initial.scm 1273 */
																								obj_t BgL_arg3917z00_4433;

																								{	/* Expand/initial.scm 1273 */
																									obj_t BgL_arg3918z00_4434;

																									{	/* Expand/initial.scm 1273 */
																										obj_t BgL_arg3919z00_4435;

																										BgL_arg3919z00_4435 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(226),
																											BNIL);
																										BgL_arg3918z00_4434 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(232),
																											BgL_arg3919z00_4435);
																									}
																									BgL_arg3917z00_4433 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg3918z00_4434, BNIL);
																								}
																								BgL_arg3916z00_4432 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(230), BgL_arg3917z00_4433);
																							}
																							BgL_arg3915z00_4431 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(233), BgL_arg3916z00_4432);
																						}
																						BgL_arg3912z00_4428 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg3915z00_4431, BNIL);
																					}
																					BgL_arg3903z00_4419 =
																						MAKE_YOUNG_PAIR(BgL_arg3911z00_4427,
																						BgL_arg3912z00_4428);
																				}
																				BgL_arg3901z00_4417 =
																					MAKE_YOUNG_PAIR(BgL_arg3902z00_4418,
																					BgL_arg3903z00_4419);
																			}
																			BgL_arg3899z00_4415 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(78),
																				BgL_arg3901z00_4417);
																		}
																		{	/* Expand/initial.scm 1274 */
																			obj_t BgL_arg3920z00_4436;

																			{	/* Expand/initial.scm 1274 */
																				obj_t BgL_arg3921z00_4437;

																				BgL_arg3921z00_4437 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				BgL_arg3920z00_4436 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(190),
																					BgL_arg3921z00_4437);
																			}
																			BgL_arg3900z00_4416 =
																				MAKE_YOUNG_PAIR(BgL_arg3920z00_4436,
																				BNIL);
																		}
																		BgL_arg3897z00_4413 =
																			MAKE_YOUNG_PAIR(BgL_arg3899z00_4415,
																			BgL_arg3900z00_4416);
																	}
																	BgL_arg3895z00_4411 =
																		MAKE_YOUNG_PAIR(BgL_arg3896z00_4412,
																		BgL_arg3897z00_4413);
																}
																BgL_arg3894z00_4410 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																	BgL_arg3895z00_4411);
															}
															BgL_arg3892z00_4408 =
																MAKE_YOUNG_PAIR(BgL_arg3894z00_4410, BNIL);
														}
														BgL_arg3890z00_4406 =
															MAKE_YOUNG_PAIR(BgL_arg3891z00_4407,
															BgL_arg3892z00_4408);
													}
													BgL_arg3889z00_4405 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(233),
														BgL_arg3890z00_4406);
												}
												BgL_arg3887z00_4403 =
													MAKE_YOUNG_PAIR(BgL_arg3889z00_4405, BNIL);
											}
											{	/* Expand/initial.scm 1275 */
												obj_t BgL_arg3922z00_4438;
												obj_t BgL_arg3923z00_4439;

												{	/* Expand/initial.scm 1275 */
													obj_t BgL_arg3924z00_4440;

													{	/* Expand/initial.scm 1275 */
														obj_t BgL_arg3925z00_4441;

														BgL_arg3925z00_4441 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(234), BNIL);
														BgL_arg3924z00_4440 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(225),
															BgL_arg3925z00_4441);
													}
													BgL_arg3922z00_4438 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(233),
														BgL_arg3924z00_4440);
												}
												{	/* Expand/initial.scm 1276 */
													obj_t BgL_arg3926z00_4442;

													{	/* Expand/initial.scm 1276 */
														obj_t BgL_arg3927z00_4443;

														BgL_arg3927z00_4443 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(225), BNIL);
														BgL_arg3926z00_4442 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(232),
															BgL_arg3927z00_4443);
													}
													BgL_arg3923z00_4439 =
														MAKE_YOUNG_PAIR(BgL_arg3926z00_4442, BNIL);
												}
												BgL_arg3888z00_4404 =
													MAKE_YOUNG_PAIR(BgL_arg3922z00_4438,
													BgL_arg3923z00_4439);
											}
											BgL_arg3886z00_4402 =
												MAKE_YOUNG_PAIR(BgL_arg3887z00_4403,
												BgL_arg3888z00_4404);
										}
										BgL_arg3885z00_4401 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(81), BgL_arg3886z00_4402);
									}
									BgL_arg3877z00_4393 =
										MAKE_YOUNG_PAIR(BgL_arg3885z00_4401, BNIL);
								}
								BgL_arg3875z00_4391 =
									MAKE_YOUNG_PAIR(BgL_arg3876z00_4392, BgL_arg3877z00_4393);
							}
							BgL_arg3874z00_4390 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(78), BgL_arg3875z00_4391);
						}
						BgL_arg3871z00_4387 = MAKE_YOUNG_PAIR(BgL_arg3874z00_4390, BNIL);
					}
					BgL_arg3869z00_4385 =
						MAKE_YOUNG_PAIR(BgL_arg3870z00_4386, BgL_arg3871z00_4387);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg3869z00_4385);
			}
		}

	}



/* &%append-2-define */
	obj_t BGl_z62z52appendzd22zd2definez30zzexpand_installz00(obj_t
		BgL_envz00_5958)
	{
		{	/* Expand/initial.scm 1266 */
			return BGl_z52appendzd22zd2definez52zzexpand_installz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_installz00(void)
	{
		{	/* Expand/initial.scm 15 */
			BGl_modulezd2initializa7ationz75zzexpand_ifz00(374982027L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(223654864L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_definez00(238068276L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_expanderz00(393376L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_exitz00(310806352L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_garithmetiquez00(349548704L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_iarithmetiquez00(327652022L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_farithmetiquez00(61378421L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_letz00(360327165L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_casez00(138168658L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_structz00(134408886L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_mapz00(218056301L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_assertz00(469953278L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_objectz00(310121664L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzexpand_multiplezd2valueszd2(352815637L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzast_letz00(469204197L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzast_labelsz00(129879154L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
			return
				BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string4254z00zzexpand_installz00));
		}

	}

#ifdef __cplusplus
}
#endif
