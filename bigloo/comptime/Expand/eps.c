/*===========================================================================*/
/*   (Expand/eps.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/eps.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_EPS_TYPE_DEFINITIONS
#define BGL_EXPAND_EPS_TYPE_DEFINITIONS

/* object type definitions */

#endif													// BGL_EXPAND_EPS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_expandze70ze7zzexpand_epsz00(obj_t, obj_t);
	static obj_t BGl_z62initialzd2expanderzb0zzexpand_epsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04exitza31195ze3ze70z60zzexpand_epsz00(obj_t);
	static obj_t BGl_compilezd2expanderzd2zzexpand_epsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62withzd2lexicalzb0zzexpand_epsz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_epsz00 = BUNSPEC;
	extern obj_t BGl_getzd2Ozd2macrozd2toplevelzd2zzexpand_expanderz00(void);
	BGL_IMPORT obj_t BGl_raisez00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_comptimezd2expandzf2errorz20zzexpand_epsz00(obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62addzd2macrozd2aliasz12z70zzexpand_epsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lexicalzd2stackzb0zzexpand_epsz00(obj_t);
	extern obj_t BGl_findzd2Gzd2expanderz00zzexpand_expanderz00(obj_t);
	static obj_t BGl_zc3z04exitza31330ze3ze70z60zzexpand_epsz00(obj_t);
	extern obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzexpand_epsz00(void);
	BGL_IMPORT obj_t BGl_getzd2compilerzd2expanderz00zz__macroz00(obj_t);
	BGL_IMPORT obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_epsz00(void);
	extern bool_t BGl_privatezd2sexpzf3z21zzast_privatez00(obj_t);
	static obj_t BGl_z62comptimezd2expandzf2errorz42zzexpand_epsz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_epsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_addzd2macrozd2definitionz12z12zzexpand_epsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31312ze3ze5zzexpand_epsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_comptimezd2expandzd2zzexpand_epsz00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62comptimezd2expandzb0zzexpand_epsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_z62handlerz62zzexpand_epsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2unitszd2zzexpand_epsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31314ze3ze5zzexpand_epsz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31250ze3ze5zzexpand_epsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2optimzd2Ozd2macrozf3za2zf3zzengine_paramz00;
	static obj_t BGl_appendzd221011zd2zzexpand_epsz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_epsz00(void);
	static obj_t BGl_z62addzd2macrozd2definitionz12z70zzexpand_epsz00(obj_t,
		obj_t);
	static obj_t BGl_z62condzd2expandzd2onlyzd2expanderzb0zzexpand_epsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31251ze3ze5zzexpand_epsz00(obj_t);
	extern obj_t BGl_findzd2Ozd2expanderz00zzexpand_expanderz00(obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_z62zc3z04anonymousza31333ze3ze5zzexpand_epsz00(obj_t, obj_t);
	static obj_t BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00 = BUNSPEC;
	static obj_t BGl_z62e2z62zzexpand_epsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31335ze3ze5zzexpand_epsz00(obj_t);
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_lexicalzd2stackzd2zzexpand_epsz00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04exitza31308ze3ze70z60zzexpand_epsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_comptimezd2expandzd2condzd2expandzd2onlyz00zzexpand_epsz00(obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31371ze3ze5zzexpand_epsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_expanderz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_withzd2lexicalzd2zzexpand_epsz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_z62errorz62zz__objectz00;
	extern obj_t BGl_userzd2errorzd2notifyz00zztools_errorz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31285ze3ze5zzexpand_epsz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_epsz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_epsz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_epsz00(void);
	static obj_t BGl_z62identifierzd2expanderzb0zzexpand_epsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_epsz00(void);
	static obj_t BGl_z62zc3z04anonymousza31375ze3ze5zzexpand_epsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31286ze3ze5zzexpand_epsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31197ze3ze5zzexpand_epsz00(obj_t, obj_t);
	static obj_t
		BGl_z62comptimezd2expandzd2condzd2expandzd2onlyz62zzexpand_epsz00(obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2unitszb0zzexpand_epsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31547ze3ze5zzexpand_epsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31385ze3ze5zzexpand_epsz00(obj_t);
	static obj_t BGl_z62applicationzd2expanderzb0zzexpand_epsz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31378ze3ze5zzexpand_epsz00(obj_t, obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_z62compilezd2expandzb0zzexpand_epsz00(obj_t, obj_t);
	static obj_t BGl_initialzd2expanderzd2zzexpand_epsz00(obj_t, obj_t);
	static obj_t BGl_expandza2ze70z45zzexpand_epsz00(obj_t, obj_t);
	extern obj_t BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(obj_t);
	static obj_t BGl_za2macroza2z00zzexpand_epsz00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zzexpand_epsz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_addzd2macrozd2aliasz12z12zzexpand_epsz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_compilezd2expandzd2zzexpand_epsz00(obj_t);
	static obj_t BGl_protozd2ze3frameze70zd6zzexpand_epsz00(obj_t);
	static obj_t __cnst[33];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2macrozd2definitionz12zd2envzc0zzexpand_epsz00,
		BgL_bgl_za762addza7d2macroza7d2025za7,
		BGl_z62addzd2macrozd2definitionz12z70zzexpand_epsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_lexicalzd2stackzd2envz00zzexpand_epsz00,
		BgL_bgl_za762lexicalza7d2sta2026z00,
		BGl_z62lexicalzd2stackzb0zzexpand_epsz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_checkzd2tozd2bezd2macroszd2envz00zzexpand_expanderz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2unitszd2envz00zzexpand_epsz00,
		BgL_bgl_za762expandza7d2unit2027z00,
		BGl_z62expandzd2unitszb0zzexpand_epsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_comptimezd2expandzd2condzd2expandzd2onlyzd2envzd2zzexpand_epsz00,
		BgL_bgl_za762comptimeza7d2ex2028z00,
		BGl_z62comptimezd2expandzd2condzd2expandzd2onlyz62zzexpand_epsz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_comptimezd2expandzf2errorzd2envzf2zzexpand_epsz00,
		BgL_bgl_za762comptimeza7d2ex2029z00,
		BGl_z62comptimezd2expandzf2errorz42zzexpand_epsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_initialzd2expanderzd2envz00zzexpand_epsz00,
		BgL_bgl_za762initialza7d2exp2030z00,
		BGl_z62initialzd2expanderzb0zzexpand_epsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_comptimezd2expandzd2envz00zzexpand_epsz00,
		BgL_bgl_za762comptimeza7d2ex2031z00,
		BGl_z62comptimezd2expandzb0zzexpand_epsz00, 0L, BUNSPEC, 1);
	static obj_t BGl_compilezd2expanderzd2envz00zzexpand_epsz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_compilezd2expandzd2envz00zzexpand_epsz00,
		BgL_bgl_za762compileza7d2exp2032z00,
		BGl_z62compilezd2expandzb0zzexpand_epsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_identifierzd2expanderzd2envz00zzexpand_epsz00,
		BgL_bgl_za762identifierza7d22033z00,
		BGl_z62identifierzd2expanderzb0zzexpand_epsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_applicationzd2expanderzd2envz00zzexpand_epsz00,
		BgL_bgl_za762applicationza7d2034z00,
		BGl_z62applicationzd2expanderzb0zzexpand_epsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2macrozd2aliasz12zd2envzc0zzexpand_epsz00,
		BgL_bgl_za762addza7d2macroza7d2035za7,
		BGl_z62addzd2macrozd2aliasz12z70zzexpand_epsz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2003z00zzexpand_epsz00,
		BgL_bgl_string2003za700za7za7e2036za7, "Expand", 6);
	      DEFINE_STRING(BGl_string2004z00zzexpand_epsz00,
		BgL_bgl_string2004za700za7za7e2037za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2005z00zzexpand_epsz00,
		BgL_bgl_string2005za700za7za7e2038za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2008z00zzexpand_epsz00,
		BgL_bgl_string2008za700za7za7e2039za7, " error", 6);
	      DEFINE_STRING(BGl_string2009z00zzexpand_epsz00,
		BgL_bgl_string2009za700za7za7e2040za7, "s", 1);
	      DEFINE_STRING(BGl_string2010z00zzexpand_epsz00,
		BgL_bgl_string2010za700za7za7e2041za7, "", 0);
	      DEFINE_STRING(BGl_string2011z00zzexpand_epsz00,
		BgL_bgl_string2011za700za7za7e2042za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string2012z00zzexpand_epsz00,
		BgL_bgl_string2012za700za7za7e2043za7, "failure during postlude hook", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2006z00zzexpand_epsz00,
		BgL_bgl_za762za7c3za704anonymo2044za7,
		BGl_z62zc3z04anonymousza31285ze3ze5zzexpand_epsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2013z00zzexpand_epsz00,
		BgL_bgl_string2013za700za7za7e2045za7, "module", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2007z00zzexpand_epsz00,
		BgL_bgl_za762za7c3za704anonymo2046za7,
		BGl_z62zc3z04anonymousza31250ze3ze5zzexpand_epsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2014z00zzexpand_epsz00,
		BgL_bgl_string2014za700za7za7e2047za7, "Illegal module clause", 21);
	      DEFINE_STRING(BGl_string2016z00zzexpand_epsz00,
		BgL_bgl_string2016za700za7za7e2048za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string2018z00zzexpand_epsz00,
		BgL_bgl_string2018za700za7za7e2049za7, "labels", 6);
	      DEFINE_STRING(BGl_string2019z00zzexpand_epsz00,
		BgL_bgl_string2019za700za7za7e2050za7, "let", 3);
	      DEFINE_STRING(BGl_string2020z00zzexpand_epsz00,
		BgL_bgl_string2020za700za7za7e2051za7, "Illegal application form", 24);
	      DEFINE_STRING(BGl_string2021z00zzexpand_epsz00,
		BgL_bgl_string2021za700za7za7e2052za7, "expand_eps", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2015z00zzexpand_epsz00,
		BgL_bgl_za762condza7d2expand2053z00,
		BGl_z62condzd2expandzd2onlyzd2expanderzb0zzexpand_epsz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2022z00zzexpand_epsz00,
		BgL_bgl_string2022za700za7za7e2054za7,
		"apply case free-pragma/effect free-pragma pragma/effect pragma labels letrec* letrec let* let define-method define-generic define-inline define jump-exit set-exit failure set! if quote @ lambda begin cond-expand (quote ()) (check-to-be-macros) define-expander define-macro nothing pass-started expand macro-alias-key ",
		317);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2017z00zzexpand_epsz00,
		BgL_bgl_za762za7c3za704anonymo2055za7,
		BGl_z62zc3z04anonymousza31547ze3ze5zzexpand_epsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_withzd2lexicalzd2envz00zzexpand_epsz00,
		BgL_bgl_za762withza7d2lexica2056z00,
		BGl_z62withzd2lexicalzb0zzexpand_epsz00, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzexpand_epsz00));
		     ADD_ROOT((void *) (&BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00));
		     ADD_ROOT((void *) (&BGl_za2macroza2z00zzexpand_epsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long
		BgL_checksumz00_1507, char *BgL_fromz00_1508)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_epsz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_epsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_epsz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_epsz00();
					BGl_cnstzd2initzd2zzexpand_epsz00();
					BGl_importedzd2moduleszd2initz00zzexpand_epsz00();
					return BGl_toplevelzd2initzd2zzexpand_epsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_epsz00(void)
	{
		{	/* Expand/eps.scm 17 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"expand_eps");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__macroz00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_eps");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_eps");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"expand_eps");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "expand_eps");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "expand_eps");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_epsz00(void)
	{
		{	/* Expand/eps.scm 17 */
			{	/* Expand/eps.scm 17 */
				obj_t BgL_cportz00_1390;

				{	/* Expand/eps.scm 17 */
					obj_t BgL_stringz00_1397;

					BgL_stringz00_1397 = BGl_string2022z00zzexpand_epsz00;
					{	/* Expand/eps.scm 17 */
						obj_t BgL_startz00_1398;

						BgL_startz00_1398 = BINT(0L);
						{	/* Expand/eps.scm 17 */
							obj_t BgL_endz00_1399;

							BgL_endz00_1399 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1397)));
							{	/* Expand/eps.scm 17 */

								BgL_cportz00_1390 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1397, BgL_startz00_1398, BgL_endz00_1399);
				}}}}
				{
					long BgL_iz00_1391;

					BgL_iz00_1391 = 32L;
				BgL_loopz00_1392:
					if ((BgL_iz00_1391 == -1L))
						{	/* Expand/eps.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Expand/eps.scm 17 */
							{	/* Expand/eps.scm 17 */
								obj_t BgL_arg2024z00_1393;

								{	/* Expand/eps.scm 17 */

									{	/* Expand/eps.scm 17 */
										obj_t BgL_locationz00_1395;

										BgL_locationz00_1395 = BBOOL(((bool_t) 0));
										{	/* Expand/eps.scm 17 */

											BgL_arg2024z00_1393 =
												BGl_readz00zz__readerz00(BgL_cportz00_1390,
												BgL_locationz00_1395);
										}
									}
								}
								{	/* Expand/eps.scm 17 */
									int BgL_tmpz00_1543;

									BgL_tmpz00_1543 = (int) (BgL_iz00_1391);
									CNST_TABLE_SET(BgL_tmpz00_1543, BgL_arg2024z00_1393);
							}}
							{	/* Expand/eps.scm 17 */
								int BgL_auxz00_1396;

								BgL_auxz00_1396 = (int) ((BgL_iz00_1391 - 1L));
								{
									long BgL_iz00_1548;

									BgL_iz00_1548 = (long) (BgL_auxz00_1396);
									BgL_iz00_1391 = BgL_iz00_1548;
									goto BgL_loopz00_1392;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_epsz00(void)
	{
		{	/* Expand/eps.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzexpand_epsz00(void)
	{
		{	/* Expand/eps.scm 17 */
			BGl_za2macroza2z00zzexpand_epsz00 = BNIL;
			return (BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00 = BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzexpand_epsz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_115;

				BgL_headz00_115 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_116;
					obj_t BgL_tailz00_117;

					BgL_prevz00_116 = BgL_headz00_115;
					BgL_tailz00_117 = BgL_l1z00_1;
				BgL_loopz00_118:
					if (PAIRP(BgL_tailz00_117))
						{
							obj_t BgL_newzd2prevzd2_120;

							BgL_newzd2prevzd2_120 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_117), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_116, BgL_newzd2prevzd2_120);
							{
								obj_t BgL_tailz00_1558;
								obj_t BgL_prevz00_1557;

								BgL_prevz00_1557 = BgL_newzd2prevzd2_120;
								BgL_tailz00_1558 = CDR(BgL_tailz00_117);
								BgL_tailz00_117 = BgL_tailz00_1558;
								BgL_prevz00_116 = BgL_prevz00_1557;
								goto BgL_loopz00_118;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_115);
				}
			}
		}

	}



/* add-macro-definition! */
	BGL_EXPORTED_DEF obj_t BGl_addzd2macrozd2definitionz12z12zzexpand_epsz00(obj_t
		BgL_formz00_35)
	{
		{	/* Expand/eps.scm 51 */
			return (BGl_za2macroza2z00zzexpand_epsz00 =
				MAKE_YOUNG_PAIR(BgL_formz00_35, BGl_za2macroza2z00zzexpand_epsz00),
				BUNSPEC);
		}

	}



/* &add-macro-definition! */
	obj_t BGl_z62addzd2macrozd2definitionz12z70zzexpand_epsz00(obj_t
		BgL_envz00_1287, obj_t BgL_formz00_1288)
	{
		{	/* Expand/eps.scm 51 */
			return
				BGl_addzd2macrozd2definitionz12z12zzexpand_epsz00(BgL_formz00_1288);
		}

	}



/* add-macro-alias! */
	BGL_EXPORTED_DEF obj_t BGl_addzd2macrozd2aliasz12z12zzexpand_epsz00(obj_t
		BgL_sym1z00_36, obj_t BgL_sym2z00_37)
	{
		{	/* Expand/eps.scm 57 */
			return
				BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_sym1z00_36,
				CNST_TABLE_REF(0), BgL_sym2z00_37);
		}

	}



/* &add-macro-alias! */
	obj_t BGl_z62addzd2macrozd2aliasz12z70zzexpand_epsz00(obj_t BgL_envz00_1289,
		obj_t BgL_sym1z00_1290, obj_t BgL_sym2z00_1291)
	{
		{	/* Expand/eps.scm 57 */
			return
				BGl_addzd2macrozd2aliasz12z12zzexpand_epsz00(BgL_sym1z00_1290,
				BgL_sym2z00_1291);
		}

	}



/* lexical-stack */
	BGL_EXPORTED_DEF obj_t BGl_lexicalzd2stackzd2zzexpand_epsz00(void)
	{
		{	/* Expand/eps.scm 73 */
			return BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00;
		}

	}



/* &lexical-stack */
	obj_t BGl_z62lexicalzd2stackzb0zzexpand_epsz00(obj_t BgL_envz00_1292)
	{
		{	/* Expand/eps.scm 73 */
			return BGl_lexicalzd2stackzd2zzexpand_epsz00();
		}

	}



/* with-lexical */
	BGL_EXPORTED_DEF obj_t BGl_withzd2lexicalzd2zzexpand_epsz00(obj_t
		BgL_newz00_38, obj_t BgL_markz00_39, obj_t BgL_locz00_40,
		obj_t BgL_thunkz00_41)
	{
		{	/* Expand/eps.scm 79 */
			{	/* Expand/eps.scm 80 */
				obj_t BgL_newzd2idzd2_148;
				obj_t BgL_oldzd2lexicalzd2stackz00_149;

				if (NULLP(BgL_newz00_38))
					{	/* Expand/eps.scm 80 */
						BgL_newzd2idzd2_148 = BNIL;
					}
				else
					{	/* Expand/eps.scm 80 */
						obj_t BgL_head1095z00_179;

						BgL_head1095z00_179 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1093z00_181;
							obj_t BgL_tail1096z00_182;

							BgL_l1093z00_181 = BgL_newz00_38;
							BgL_tail1096z00_182 = BgL_head1095z00_179;
						BgL_zc3z04anonymousza31200ze3z87_183:
							if (NULLP(BgL_l1093z00_181))
								{	/* Expand/eps.scm 80 */
									BgL_newzd2idzd2_148 = CDR(BgL_head1095z00_179);
								}
							else
								{	/* Expand/eps.scm 80 */
									obj_t BgL_newtail1097z00_185;

									{	/* Expand/eps.scm 80 */
										obj_t BgL_arg1203z00_187;

										{	/* Expand/eps.scm 80 */
											obj_t BgL_az00_188;

											BgL_az00_188 = CAR(((obj_t) BgL_l1093z00_181));
											BgL_arg1203z00_187 =
												BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_az00_188,
												BgL_locz00_40);
										}
										BgL_newtail1097z00_185 =
											MAKE_YOUNG_PAIR(BgL_arg1203z00_187, BNIL);
									}
									SET_CDR(BgL_tail1096z00_182, BgL_newtail1097z00_185);
									{	/* Expand/eps.scm 80 */
										obj_t BgL_arg1202z00_186;

										BgL_arg1202z00_186 = CDR(((obj_t) BgL_l1093z00_181));
										{
											obj_t BgL_tail1096z00_1581;
											obj_t BgL_l1093z00_1580;

											BgL_l1093z00_1580 = BgL_arg1202z00_186;
											BgL_tail1096z00_1581 = BgL_newtail1097z00_185;
											BgL_tail1096z00_182 = BgL_tail1096z00_1581;
											BgL_l1093z00_181 = BgL_l1093z00_1580;
											goto BgL_zc3z04anonymousza31200ze3z87_183;
										}
									}
								}
						}
					}
				BgL_oldzd2lexicalzd2stackz00_149 =
					BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00;
				{	/* Expand/eps.scm 83 */
					obj_t BgL_arg1188z00_150;

					if (NULLP(BgL_newzd2idzd2_148))
						{	/* Expand/eps.scm 83 */
							BgL_arg1188z00_150 = BNIL;
						}
					else
						{	/* Expand/eps.scm 83 */
							obj_t BgL_head1100z00_153;

							BgL_head1100z00_153 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1098z00_155;
								obj_t BgL_tail1101z00_156;

								BgL_l1098z00_155 = BgL_newzd2idzd2_148;
								BgL_tail1101z00_156 = BgL_head1100z00_153;
							BgL_zc3z04anonymousza31190ze3z87_157:
								if (NULLP(BgL_l1098z00_155))
									{	/* Expand/eps.scm 83 */
										BgL_arg1188z00_150 = CDR(BgL_head1100z00_153);
									}
								else
									{	/* Expand/eps.scm 83 */
										obj_t BgL_newtail1102z00_159;

										{	/* Expand/eps.scm 83 */
											obj_t BgL_arg1194z00_161;

											{	/* Expand/eps.scm 83 */
												obj_t BgL_oz00_162;

												BgL_oz00_162 = CAR(((obj_t) BgL_l1098z00_155));
												BgL_arg1194z00_161 =
													MAKE_YOUNG_PAIR(BgL_oz00_162, BgL_markz00_39);
											}
											BgL_newtail1102z00_159 =
												MAKE_YOUNG_PAIR(BgL_arg1194z00_161, BNIL);
										}
										SET_CDR(BgL_tail1101z00_156, BgL_newtail1102z00_159);
										{	/* Expand/eps.scm 83 */
											obj_t BgL_arg1193z00_160;

											BgL_arg1193z00_160 = CDR(((obj_t) BgL_l1098z00_155));
											{
												obj_t BgL_tail1101z00_1596;
												obj_t BgL_l1098z00_1595;

												BgL_l1098z00_1595 = BgL_arg1193z00_160;
												BgL_tail1101z00_1596 = BgL_newtail1102z00_159;
												BgL_tail1101z00_156 = BgL_tail1101z00_1596;
												BgL_l1098z00_155 = BgL_l1098z00_1595;
												goto BgL_zc3z04anonymousza31190ze3z87_157;
											}
										}
									}
							}
						}
					BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00 =
						BGl_appendzd221011zd2zzexpand_epsz00(BgL_arg1188z00_150,
						BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00);
				}
				{	/* Expand/eps.scm 84 */
					obj_t BgL_resz00_164;

					BgL_resz00_164 =
						BGl_zc3z04exitza31195ze3ze70z60zzexpand_epsz00(BgL_thunkz00_41);
					BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00 =
						BgL_oldzd2lexicalzd2stackz00_149;
					return BgL_resz00_164;
				}
			}
		}

	}



/* <@exit:1195>~0 */
	obj_t BGl_zc3z04exitza31195ze3ze70z60zzexpand_epsz00(obj_t BgL_thunkz00_1385)
	{
		{	/* Expand/eps.scm 84 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1056z00_166;

			if (SET_EXIT(BgL_an_exit1056z00_166))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1056z00_166 = (void *) jmpbuf;
					{	/* Expand/eps.scm 84 */
						obj_t BgL_env1060z00_167;

						BgL_env1060z00_167 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1060z00_167, BgL_an_exit1056z00_166, 1L);
						{	/* Expand/eps.scm 84 */
							obj_t BgL_an_exitd1057z00_168;

							BgL_an_exitd1057z00_168 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1060z00_167);
							{	/* Expand/eps.scm 84 */
								obj_t BgL_res1059z00_171;

								{	/* Expand/eps.scm 87 */
									obj_t BgL_zc3z04anonymousza31197ze3z87_1293;

									BgL_zc3z04anonymousza31197ze3z87_1293 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31197ze3ze5zzexpand_epsz00,
										(int) (1L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31197ze3z87_1293,
										(int) (0L), BgL_an_exitd1057z00_168);
									BgL_res1059z00_171 =
										BGl_withzd2exceptionzd2handlerz00zz__errorz00
										(BgL_zc3z04anonymousza31197ze3z87_1293, BgL_thunkz00_1385);
								}
								POP_ENV_EXIT(BgL_env1060z00_167);
								return BgL_res1059z00_171;
							}
						}
					}
				}
		}

	}



/* &with-lexical */
	obj_t BGl_z62withzd2lexicalzb0zzexpand_epsz00(obj_t BgL_envz00_1294,
		obj_t BgL_newz00_1295, obj_t BgL_markz00_1296, obj_t BgL_locz00_1297,
		obj_t BgL_thunkz00_1298)
	{
		{	/* Expand/eps.scm 79 */
			return
				BGl_withzd2lexicalzd2zzexpand_epsz00(BgL_newz00_1295, BgL_markz00_1296,
				BgL_locz00_1297, BgL_thunkz00_1298);
		}

	}



/* &<@anonymous:1197> */
	obj_t BGl_z62zc3z04anonymousza31197ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1299, obj_t BgL_ez00_1301)
	{
		{	/* Expand/eps.scm 86 */
			{	/* Expand/eps.scm 87 */
				obj_t BgL_an_exitd1057z00_1300;

				BgL_an_exitd1057z00_1300 = PROCEDURE_REF(BgL_envz00_1299, (int) (0L));
				{	/* Expand/eps.scm 87 */
					bool_t BgL_test2064z00_1614;

					{	/* Expand/eps.scm 87 */
						obj_t BgL_classz00_1401;

						BgL_classz00_1401 = BGl_z62errorz62zz__objectz00;
						if (BGL_OBJECTP(BgL_ez00_1301))
							{	/* Expand/eps.scm 87 */
								BgL_objectz00_bglt BgL_arg1807z00_1402;

								BgL_arg1807z00_1402 = (BgL_objectz00_bglt) (BgL_ez00_1301);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Expand/eps.scm 87 */
										long BgL_idxz00_1403;

										BgL_idxz00_1403 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1402);
										BgL_test2064z00_1614 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1403 + 3L)) == BgL_classz00_1401);
									}
								else
									{	/* Expand/eps.scm 87 */
										bool_t BgL_res1994z00_1406;

										{	/* Expand/eps.scm 87 */
											obj_t BgL_oclassz00_1407;

											{	/* Expand/eps.scm 87 */
												obj_t BgL_arg1815z00_1408;
												long BgL_arg1816z00_1409;

												BgL_arg1815z00_1408 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Expand/eps.scm 87 */
													long BgL_arg1817z00_1410;

													BgL_arg1817z00_1410 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1402);
													BgL_arg1816z00_1409 =
														(BgL_arg1817z00_1410 - OBJECT_TYPE);
												}
												BgL_oclassz00_1407 =
													VECTOR_REF(BgL_arg1815z00_1408, BgL_arg1816z00_1409);
											}
											{	/* Expand/eps.scm 87 */
												bool_t BgL__ortest_1115z00_1411;

												BgL__ortest_1115z00_1411 =
													(BgL_classz00_1401 == BgL_oclassz00_1407);
												if (BgL__ortest_1115z00_1411)
													{	/* Expand/eps.scm 87 */
														BgL_res1994z00_1406 = BgL__ortest_1115z00_1411;
													}
												else
													{	/* Expand/eps.scm 87 */
														long BgL_odepthz00_1412;

														{	/* Expand/eps.scm 87 */
															obj_t BgL_arg1804z00_1413;

															BgL_arg1804z00_1413 = (BgL_oclassz00_1407);
															BgL_odepthz00_1412 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1413);
														}
														if ((3L < BgL_odepthz00_1412))
															{	/* Expand/eps.scm 87 */
																obj_t BgL_arg1802z00_1414;

																{	/* Expand/eps.scm 87 */
																	obj_t BgL_arg1803z00_1415;

																	BgL_arg1803z00_1415 = (BgL_oclassz00_1407);
																	BgL_arg1802z00_1414 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1415,
																		3L);
																}
																BgL_res1994z00_1406 =
																	(BgL_arg1802z00_1414 == BgL_classz00_1401);
															}
														else
															{	/* Expand/eps.scm 87 */
																BgL_res1994z00_1406 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2064z00_1614 = BgL_res1994z00_1406;
									}
							}
						else
							{	/* Expand/eps.scm 87 */
								BgL_test2064z00_1614 = ((bool_t) 0);
							}
					}
					if (BgL_test2064z00_1614)
						{	/* Expand/eps.scm 87 */
							BGl_userzd2errorzd2notifyz00zztools_errorz00(BgL_ez00_1301,
								CNST_TABLE_REF(1));
							return
								unwind_stack_until(BgL_an_exitd1057z00_1300, BFALSE, BUNSPEC,
								BFALSE, BFALSE);
						}
					else
						{	/* Expand/eps.scm 87 */
							return BGl_raisez00zz__errorz00(BgL_ez00_1301);
						}
				}
			}
		}

	}



/* expand-units */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2unitszd2zzexpand_epsz00(obj_t
		BgL_unitsz00_42)
	{
		{	/* Expand/eps.scm 101 */
			{	/* Expand/eps.scm 102 */
				obj_t BgL_list1204z00_191;

				{	/* Expand/eps.scm 102 */
					obj_t BgL_arg1206z00_192;

					{	/* Expand/eps.scm 102 */
						obj_t BgL_arg1208z00_193;

						BgL_arg1208z00_193 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1206z00_192 =
							MAKE_YOUNG_PAIR(BGl_string2003z00zzexpand_epsz00,
							BgL_arg1208z00_193);
					}
					BgL_list1204z00_191 =
						MAKE_YOUNG_PAIR(BGl_string2004z00zzexpand_epsz00,
						BgL_arg1206z00_192);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1204z00_191);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string2003z00zzexpand_epsz00;
			{	/* Expand/eps.scm 102 */
				obj_t BgL_g1062z00_194;

				BgL_g1062z00_194 = BNIL;
				{
					obj_t BgL_hooksz00_197;
					obj_t BgL_hnamesz00_198;

					BgL_hooksz00_197 = BgL_g1062z00_194;
					BgL_hnamesz00_198 = BNIL;
				BgL_zc3z04anonymousza31209ze3z87_199:
					if (NULLP(BgL_hooksz00_197))
						{	/* Expand/eps.scm 102 */
							CNST_TABLE_REF(2);
						}
					else
						{	/* Expand/eps.scm 102 */
							bool_t BgL_test2070z00_1651;

							{	/* Expand/eps.scm 102 */
								obj_t BgL_fun1220z00_206;

								BgL_fun1220z00_206 = CAR(((obj_t) BgL_hooksz00_197));
								BgL_test2070z00_1651 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1220z00_206));
							}
							if (BgL_test2070z00_1651)
								{	/* Expand/eps.scm 102 */
									obj_t BgL_arg1215z00_203;
									obj_t BgL_arg1216z00_204;

									BgL_arg1215z00_203 = CDR(((obj_t) BgL_hooksz00_197));
									BgL_arg1216z00_204 = CDR(((obj_t) BgL_hnamesz00_198));
									{
										obj_t BgL_hnamesz00_1663;
										obj_t BgL_hooksz00_1662;

										BgL_hooksz00_1662 = BgL_arg1215z00_203;
										BgL_hnamesz00_1663 = BgL_arg1216z00_204;
										BgL_hnamesz00_198 = BgL_hnamesz00_1663;
										BgL_hooksz00_197 = BgL_hooksz00_1662;
										goto BgL_zc3z04anonymousza31209ze3z87_199;
									}
								}
							else
								{	/* Expand/eps.scm 102 */
									obj_t BgL_arg1218z00_205;

									BgL_arg1218z00_205 = CAR(((obj_t) BgL_hnamesz00_198));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2003z00zzexpand_epsz00,
										BGl_string2005z00zzexpand_epsz00, BgL_arg1218z00_205);
								}
						}
				}
			}
			{	/* Expand/eps.scm 104 */
				obj_t BgL_g1105z00_209;

				BgL_g1105z00_209 = bgl_reverse_bang(BGl_za2macroza2z00zzexpand_epsz00);
				{
					obj_t BgL_l1103z00_211;

					BgL_l1103z00_211 = BgL_g1105z00_209;
				BgL_zc3z04anonymousza31222ze3z87_212:
					if (PAIRP(BgL_l1103z00_211))
						{	/* Expand/eps.scm 106 */
							{	/* Expand/eps.scm 105 */
								obj_t BgL_xz00_214;

								BgL_xz00_214 = CAR(BgL_l1103z00_211);
								{	/* Expand/eps.scm 229 */
									obj_t BgL_xz00_1416;
									obj_t BgL_sz00_1418;

									BgL_xz00_1416 =
										BGl_initialzd2expanderzd2zzexpand_epsz00(BgL_xz00_214,
										BGl_initialzd2expanderzd2envz00zzexpand_epsz00);
									BgL_sz00_1418 = BNIL;
									BGl_expandze70ze7zzexpand_epsz00(BgL_xz00_1416,
										BgL_sz00_1418);
								}
							}
							{
								obj_t BgL_l1103z00_1673;

								BgL_l1103z00_1673 = CDR(BgL_l1103z00_211);
								BgL_l1103z00_211 = BgL_l1103z00_1673;
								goto BgL_zc3z04anonymousza31222ze3z87_212;
							}
						}
					else
						{	/* Expand/eps.scm 106 */
							((bool_t) 1);
						}
				}
			}
			{
				obj_t BgL_l1106z00_219;

				BgL_l1106z00_219 = BgL_unitsz00_42;
			BgL_zc3z04anonymousza31227ze3z87_220:
				if (PAIRP(BgL_l1106z00_219))
					{	/* Expand/eps.scm 115 */
						{	/* Expand/eps.scm 116 */
							obj_t BgL_unitz00_222;

							BgL_unitz00_222 = CAR(BgL_l1106z00_219);
							{	/* Expand/eps.scm 116 */
								bool_t BgL_test2073z00_1678;

								{	/* Expand/eps.scm 116 */
									obj_t BgL_tmpz00_1679;

									BgL_tmpz00_1679 =
										STRUCT_REF(((obj_t) BgL_unitz00_222), (int) (2L));
									BgL_test2073z00_1678 = PROCEDUREP(BgL_tmpz00_1679);
								}
								if (BgL_test2073z00_1678)
									{	/* Expand/eps.scm 116 */
										CNST_TABLE_REF(3);
									}
								else
									{	/* Expand/eps.scm 120 */
										obj_t BgL_g1064z00_225;

										BgL_g1064z00_225 =
											STRUCT_REF(((obj_t) BgL_unitz00_222), (int) (2L));
										{
											obj_t BgL_srcz00_228;
											obj_t BgL_resz00_229;

											BgL_srcz00_228 = BgL_g1064z00_225;
											BgL_resz00_229 = BNIL;
										BgL_zc3z04anonymousza31231ze3z87_230:
											if (NULLP(BgL_srcz00_228))
												{	/* Expand/eps.scm 123 */
													obj_t BgL_arg1233z00_232;

													BgL_arg1233z00_232 = bgl_reverse_bang(BgL_resz00_229);
													{	/* Expand/eps.scm 123 */
														obj_t BgL_xz00_1388;

														{	/* Expand/eps.scm 123 */
															int BgL_auxz00_1693;
															obj_t BgL_tmpz00_1691;

															BgL_auxz00_1693 = (int) (2L);
															BgL_tmpz00_1691 = ((obj_t) BgL_unitz00_222);
															BgL_xz00_1388 =
																STRUCT_SET(BgL_tmpz00_1691, BgL_auxz00_1693,
																BgL_arg1233z00_232);
														} BUNSPEC;
												}}
											else
												{

													{	/* Expand/eps.scm 124 */
														obj_t BgL_ezd2396zd2_236;

														BgL_ezd2396zd2_236 = CAR(((obj_t) BgL_srcz00_228));
														if (PAIRP(BgL_ezd2396zd2_236))
															{	/* Expand/eps.scm 124 */
																if (
																	(CAR(BgL_ezd2396zd2_236) ==
																		CNST_TABLE_REF(4)))
																	{	/* Expand/eps.scm 124 */
																		{	/* Expand/eps.scm 128 */
																			obj_t
																				BgL_zc3z04anonymousza31251ze3z87_1316;
																			BgL_zc3z04anonymousza31251ze3z87_1316 =
																				MAKE_FX_PROCEDURE
																				(BGl_z62zc3z04anonymousza31251ze3ze5zzexpand_epsz00,
																				(int) (0L), (int) (1L));
																			PROCEDURE_SET
																				(BgL_zc3z04anonymousza31251ze3z87_1316,
																				(int) (0L), BgL_srcz00_228);
																			BGl_withzd2exceptionzd2handlerz00zz__errorz00
																				(BGl_proc2007z00zzexpand_epsz00,
																				BgL_zc3z04anonymousza31251ze3z87_1316);
																		}
																		{	/* Expand/eps.scm 132 */
																			obj_t BgL_arg1268z00_252;

																			BgL_arg1268z00_252 =
																				CDR(((obj_t) BgL_srcz00_228));
																			{
																				obj_t BgL_srcz00_1712;

																				BgL_srcz00_1712 = BgL_arg1268z00_252;
																				BgL_srcz00_228 = BgL_srcz00_1712;
																				goto
																					BgL_zc3z04anonymousza31231ze3z87_230;
																			}
																		}
																	}
																else
																	{	/* Expand/eps.scm 124 */
																		if (
																			(CAR(BgL_ezd2396zd2_236) ==
																				CNST_TABLE_REF(5)))
																			{	/* Expand/eps.scm 124 */
																				{	/* Expand/eps.scm 136 */
																					obj_t
																						BgL_zc3z04anonymousza31286ze3z87_1314;
																					BgL_zc3z04anonymousza31286ze3z87_1314
																						=
																						MAKE_FX_PROCEDURE
																						(BGl_z62zc3z04anonymousza31286ze3ze5zzexpand_epsz00,
																						(int) (0L), (int) (1L));
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31286ze3z87_1314,
																						(int) (0L), BgL_srcz00_228);
																					BGl_withzd2exceptionzd2handlerz00zz__errorz00
																						(BGl_proc2006z00zzexpand_epsz00,
																						BgL_zc3z04anonymousza31286ze3z87_1314);
																				}
																				{	/* Expand/eps.scm 140 */
																					obj_t BgL_arg1305z00_261;

																					BgL_arg1305z00_261 =
																						CDR(((obj_t) BgL_srcz00_228));
																					{
																						obj_t BgL_srcz00_1725;

																						BgL_srcz00_1725 =
																							BgL_arg1305z00_261;
																						BgL_srcz00_228 = BgL_srcz00_1725;
																						goto
																							BgL_zc3z04anonymousza31231ze3z87_230;
																					}
																				}
																			}
																		else
																			{	/* Expand/eps.scm 124 */
																			BgL_tagzd2395zd2_235:
																				{	/* Expand/eps.scm 142 */
																					obj_t BgL_obodyz00_262;

																					BgL_obodyz00_262 =
																						CAR(((obj_t) BgL_srcz00_228));
																					{	/* Expand/eps.scm 142 */
																						obj_t BgL_nbodyz00_263;

																						BgL_nbodyz00_263 =
																							BGl_zc3z04exitza31308ze3ze70z60zzexpand_epsz00
																							(BgL_obodyz00_262);
																						{	/* Expand/eps.scm 143 */

																							{	/* Expand/eps.scm 151 */
																								obj_t BgL_arg1306z00_264;
																								obj_t BgL_arg1307z00_265;

																								BgL_arg1306z00_264 =
																									CDR(((obj_t) BgL_srcz00_228));
																								BgL_arg1307z00_265 =
																									MAKE_YOUNG_PAIR
																									(BgL_nbodyz00_263,
																									BgL_resz00_229);
																								{
																									obj_t BgL_resz00_1733;
																									obj_t BgL_srcz00_1732;

																									BgL_srcz00_1732 =
																										BgL_arg1306z00_264;
																									BgL_resz00_1733 =
																										BgL_arg1307z00_265;
																									BgL_resz00_229 =
																										BgL_resz00_1733;
																									BgL_srcz00_228 =
																										BgL_srcz00_1732;
																									goto
																										BgL_zc3z04anonymousza31231ze3z87_230;
																								}
																							}
																						}
																					}
																				}
																			}
																	}
															}
														else
															{	/* Expand/eps.scm 124 */
																goto BgL_tagzd2395zd2_235;
															}
													}
												}
										}
									}
							}
						}
						{
							obj_t BgL_l1106z00_1734;

							BgL_l1106z00_1734 = CDR(BgL_l1106z00_219);
							BgL_l1106z00_219 = BgL_l1106z00_1734;
							goto BgL_zc3z04anonymousza31227ze3z87_220;
						}
					}
				else
					{	/* Expand/eps.scm 115 */
						((bool_t) 1);
					}
			}
			{	/* Expand/eps.scm 154 */
				bool_t BgL_test2078z00_1736;

				if (CBOOL(BGl_za2optimzd2Ozd2macrozf3za2zf3zzengine_paramz00))
					{	/* Expand/eps.scm 154 */
						BgL_test2078z00_1736 = ((bool_t) 1);
					}
				else
					{	/* Expand/eps.scm 154 */
						if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
							(BGl_za2compilerzd2debugza2zd2zzengine_paramz00))
							{	/* Expand/eps.scm 155 */
								if (INTEGERP(BGl_za2compilerzd2debugza2zd2zzengine_paramz00))
									{	/* Expand/eps.scm 155 */
										BgL_test2078z00_1736 =
											(
											(long)
											CINT(BGl_za2compilerzd2debugza2zd2zzengine_paramz00) >=
											1L);
									}
								else
									{	/* Expand/eps.scm 155 */
										BgL_test2078z00_1736 =
											BGl_2ze3zd3z30zz__r4_numbers_6_5z00
											(BGl_za2compilerzd2debugza2zd2zzengine_paramz00,
											BINT(1L));
									}
							}
						else
							{	/* Expand/eps.scm 155 */
								BgL_test2078z00_1736 = ((bool_t) 0);
							}
					}
				if (BgL_test2078z00_1736)
					{
						obj_t BgL_l1108z00_288;

						BgL_l1108z00_288 = BgL_unitsz00_42;
					BgL_zc3z04anonymousza31318ze3z87_289:
						if (PAIRP(BgL_l1108z00_288))
							{	/* Expand/eps.scm 156 */
								{	/* Expand/eps.scm 157 */
									obj_t BgL_unitz00_291;

									BgL_unitz00_291 = CAR(BgL_l1108z00_288);
									{	/* Expand/eps.scm 157 */
										bool_t BgL_test2083z00_1750;

										{	/* Expand/eps.scm 157 */
											obj_t BgL_tmpz00_1751;

											BgL_tmpz00_1751 =
												STRUCT_REF(((obj_t) BgL_unitz00_291), (int) (2L));
											BgL_test2083z00_1750 = PROCEDUREP(BgL_tmpz00_1751);
										}
										if (BgL_test2083z00_1750)
											{	/* Expand/eps.scm 157 */
												CNST_TABLE_REF(3);
											}
										else
											{	/* Expand/eps.scm 161 */
												obj_t BgL_g1073z00_294;

												BgL_g1073z00_294 =
													STRUCT_REF(((obj_t) BgL_unitz00_291), (int) (2L));
												{
													obj_t BgL_srcz00_297;
													obj_t BgL_resz00_298;

													BgL_srcz00_297 = BgL_g1073z00_294;
													BgL_resz00_298 = BNIL;
												BgL_zc3z04anonymousza31322ze3z87_299:
													if (NULLP(BgL_srcz00_297))
														{	/* Expand/eps.scm 165 */
															obj_t BgL_arg1325z00_301;

															BgL_arg1325z00_301 =
																BGl_appendzd221011zd2zzexpand_epsz00
																(BGl_getzd2Ozd2macrozd2toplevelzd2zzexpand_expanderz00
																(), bgl_reverse_bang(BgL_resz00_298));
															{	/* Expand/eps.scm 164 */
																obj_t BgL_xz00_1389;

																{	/* Expand/eps.scm 164 */
																	int BgL_auxz00_1767;
																	obj_t BgL_tmpz00_1765;

																	BgL_auxz00_1767 = (int) (2L);
																	BgL_tmpz00_1765 = ((obj_t) BgL_unitz00_291);
																	BgL_xz00_1389 =
																		STRUCT_SET(BgL_tmpz00_1765, BgL_auxz00_1767,
																		BgL_arg1325z00_301);
																} BUNSPEC;
														}}
													else
														{	/* Expand/eps.scm 166 */
															obj_t BgL_obodyz00_304;

															BgL_obodyz00_304 = CAR(((obj_t) BgL_srcz00_297));
															{	/* Expand/eps.scm 166 */
																obj_t BgL_nbodyz00_305;

																BgL_nbodyz00_305 =
																	BGl_zc3z04exitza31330ze3ze70z60zzexpand_epsz00
																	(BgL_obodyz00_304);
																{	/* Expand/eps.scm 167 */

																	{	/* Expand/eps.scm 175 */
																		obj_t BgL_arg1328z00_306;
																		obj_t BgL_arg1329z00_307;

																		BgL_arg1328z00_306 =
																			CDR(((obj_t) BgL_srcz00_297));
																		BgL_arg1329z00_307 =
																			MAKE_YOUNG_PAIR(BgL_nbodyz00_305,
																			BgL_resz00_298);
																		{
																			obj_t BgL_resz00_1777;
																			obj_t BgL_srcz00_1776;

																			BgL_srcz00_1776 = BgL_arg1328z00_306;
																			BgL_resz00_1777 = BgL_arg1329z00_307;
																			BgL_resz00_298 = BgL_resz00_1777;
																			BgL_srcz00_297 = BgL_srcz00_1776;
																			goto BgL_zc3z04anonymousza31322ze3z87_299;
																		}
																	}
																}
															}
														}
												}
											}
									}
								}
								{
									obj_t BgL_l1108z00_1778;

									BgL_l1108z00_1778 = CDR(BgL_l1108z00_288);
									BgL_l1108z00_288 = BgL_l1108z00_1778;
									goto BgL_zc3z04anonymousza31318ze3z87_289;
								}
							}
						else
							{	/* Expand/eps.scm 156 */
								((bool_t) 1);
							}
					}
				else
					{	/* Expand/eps.scm 154 */
						((bool_t) 0);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Expand/eps.scm 178 */
					{	/* Expand/eps.scm 178 */
						obj_t BgL_port1110z00_330;

						{	/* Expand/eps.scm 178 */
							obj_t BgL_tmpz00_1783;

							BgL_tmpz00_1783 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1110z00_330 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1783);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1110z00_330);
						bgl_display_string(BGl_string2008z00zzexpand_epsz00,
							BgL_port1110z00_330);
						{	/* Expand/eps.scm 178 */
							obj_t BgL_arg1342z00_331;

							{	/* Expand/eps.scm 178 */
								bool_t BgL_test2086z00_1788;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Expand/eps.scm 178 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Expand/eps.scm 178 */
												BgL_test2086z00_1788 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Expand/eps.scm 178 */
												BgL_test2086z00_1788 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Expand/eps.scm 178 */
										BgL_test2086z00_1788 = ((bool_t) 0);
									}
								if (BgL_test2086z00_1788)
									{	/* Expand/eps.scm 178 */
										BgL_arg1342z00_331 = BGl_string2009z00zzexpand_epsz00;
									}
								else
									{	/* Expand/eps.scm 178 */
										BgL_arg1342z00_331 = BGl_string2010z00zzexpand_epsz00;
									}
							}
							bgl_display_obj(BgL_arg1342z00_331, BgL_port1110z00_330);
						}
						bgl_display_string(BGl_string2011z00zzexpand_epsz00,
							BgL_port1110z00_330);
						bgl_display_char(((unsigned char) 10), BgL_port1110z00_330);
					}
					{	/* Expand/eps.scm 178 */
						obj_t BgL_list1346z00_335;

						BgL_list1346z00_335 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1346z00_335);
					}
				}
			else
				{	/* Expand/eps.scm 178 */
					obj_t BgL_g1085z00_336;
					obj_t BgL_g1086z00_337;

					{	/* Expand/eps.scm 178 */
						obj_t BgL_list1363z00_350;

						BgL_list1363z00_350 =
							MAKE_YOUNG_PAIR
							(BGl_checkzd2tozd2bezd2macroszd2envz00zzexpand_expanderz00, BNIL);
						BgL_g1085z00_336 = BgL_list1363z00_350;
					}
					BgL_g1086z00_337 = CNST_TABLE_REF(6);
					{
						obj_t BgL_hooksz00_339;
						obj_t BgL_hnamesz00_340;

						BgL_hooksz00_339 = BgL_g1085z00_336;
						BgL_hnamesz00_340 = BgL_g1086z00_337;
					BgL_zc3z04anonymousza31347ze3z87_341:
						if (NULLP(BgL_hooksz00_339))
							{	/* Expand/eps.scm 178 */
								return BgL_unitsz00_42;
							}
						else
							{	/* Expand/eps.scm 178 */
								bool_t BgL_test2090z00_1807;

								{	/* Expand/eps.scm 178 */
									obj_t BgL_fun1362z00_348;

									BgL_fun1362z00_348 = CAR(((obj_t) BgL_hooksz00_339));
									BgL_test2090z00_1807 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1362z00_348));
								}
								if (BgL_test2090z00_1807)
									{	/* Expand/eps.scm 178 */
										obj_t BgL_arg1351z00_345;
										obj_t BgL_arg1352z00_346;

										BgL_arg1351z00_345 = CDR(((obj_t) BgL_hooksz00_339));
										BgL_arg1352z00_346 = CDR(((obj_t) BgL_hnamesz00_340));
										{
											obj_t BgL_hnamesz00_1819;
											obj_t BgL_hooksz00_1818;

											BgL_hooksz00_1818 = BgL_arg1351z00_345;
											BgL_hnamesz00_1819 = BgL_arg1352z00_346;
											BgL_hnamesz00_340 = BgL_hnamesz00_1819;
											BgL_hooksz00_339 = BgL_hooksz00_1818;
											goto BgL_zc3z04anonymousza31347ze3z87_341;
										}
									}
								else
									{	/* Expand/eps.scm 178 */
										obj_t BgL_arg1361z00_347;

										BgL_arg1361z00_347 = CAR(((obj_t) BgL_hnamesz00_340));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string2012z00zzexpand_epsz00, BgL_arg1361z00_347);
									}
							}
					}
				}
		}

	}



/* <@exit:1308>~0 */
	obj_t BGl_zc3z04exitza31308ze3ze70z60zzexpand_epsz00(obj_t BgL_obodyz00_1383)
	{
		{	/* Expand/eps.scm 143 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1066z00_267;

			if (SET_EXIT(BgL_an_exit1066z00_267))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1066z00_267 = (void *) jmpbuf;
					{	/* Expand/eps.scm 143 */
						obj_t BgL_env1070z00_268;

						BgL_env1070z00_268 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1070z00_268, BgL_an_exit1066z00_267, 1L);
						{	/* Expand/eps.scm 143 */
							obj_t BgL_an_exitd1067z00_269;

							BgL_an_exitd1067z00_269 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1070z00_268);
							{	/* Expand/eps.scm 146 */
								obj_t BgL_res1069z00_272;

								{	/* Expand/eps.scm 146 */
									obj_t BgL_zc3z04anonymousza31314ze3z87_1309;
									obj_t BgL_zc3z04anonymousza31312ze3z87_1313;

									BgL_zc3z04anonymousza31314ze3z87_1309 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31314ze3ze5zzexpand_epsz00,
										(int) (0L), (int) (1L));
									BgL_zc3z04anonymousza31312ze3z87_1313 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31312ze3ze5zzexpand_epsz00,
										(int) (1L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31314ze3z87_1309,
										(int) (0L), BgL_obodyz00_1383);
									PROCEDURE_SET(BgL_zc3z04anonymousza31312ze3z87_1313,
										(int) (0L), BgL_an_exitd1067z00_269);
									BgL_res1069z00_272 =
										BGl_withzd2exceptionzd2handlerz00zz__errorz00
										(BgL_zc3z04anonymousza31312ze3z87_1313,
										BgL_zc3z04anonymousza31314ze3z87_1309);
								}
								POP_ENV_EXIT(BgL_env1070z00_268);
								return BgL_res1069z00_272;
							}
						}
					}
				}
		}

	}



/* <@exit:1330>~0 */
	obj_t BGl_zc3z04exitza31330ze3ze70z60zzexpand_epsz00(obj_t BgL_obodyz00_1384)
	{
		{	/* Expand/eps.scm 167 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1075z00_309;

			if (SET_EXIT(BgL_an_exit1075z00_309))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1075z00_309 = (void *) jmpbuf;
					{	/* Expand/eps.scm 167 */
						obj_t BgL_env1079z00_310;

						BgL_env1079z00_310 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1079z00_310, BgL_an_exit1075z00_309, 1L);
						{	/* Expand/eps.scm 167 */
							obj_t BgL_an_exitd1076z00_311;

							BgL_an_exitd1076z00_311 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1079z00_310);
							{	/* Expand/eps.scm 170 */
								obj_t BgL_res1078z00_314;

								{	/* Expand/eps.scm 170 */
									obj_t BgL_zc3z04anonymousza31335ze3z87_1303;
									obj_t BgL_zc3z04anonymousza31333ze3z87_1308;

									BgL_zc3z04anonymousza31335ze3z87_1303 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31335ze3ze5zzexpand_epsz00,
										(int) (0L), (int) (1L));
									BgL_zc3z04anonymousza31333ze3z87_1308 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31333ze3ze5zzexpand_epsz00,
										(int) (1L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31335ze3z87_1303,
										(int) (0L), BgL_obodyz00_1384);
									PROCEDURE_SET(BgL_zc3z04anonymousza31333ze3z87_1308,
										(int) (0L), BgL_an_exitd1076z00_311);
									BgL_res1078z00_314 =
										BGl_withzd2exceptionzd2handlerz00zz__errorz00
										(BgL_zc3z04anonymousza31333ze3z87_1308,
										BgL_zc3z04anonymousza31335ze3z87_1303);
								}
								POP_ENV_EXIT(BgL_env1079z00_310);
								return BgL_res1078z00_314;
							}
						}
					}
				}
		}

	}



/* &expand-units */
	obj_t BGl_z62expandzd2unitszb0zzexpand_epsz00(obj_t BgL_envz00_1318,
		obj_t BgL_unitsz00_1319)
	{
		{	/* Expand/eps.scm 101 */
			return BGl_expandzd2unitszd2zzexpand_epsz00(BgL_unitsz00_1319);
		}

	}



/* &handler */
	obj_t BGl_z62handlerz62zzexpand_epsz00(obj_t BgL_ez00_351)
	{
		{	/* Expand/eps.scm 113 */
			{	/* Expand/eps.scm 111 */
				bool_t BgL_test2091z00_1858;

				{	/* Expand/eps.scm 111 */
					obj_t BgL_classz00_899;

					BgL_classz00_899 = BGl_z62errorz62zz__objectz00;
					if (BGL_OBJECTP(BgL_ez00_351))
						{	/* Expand/eps.scm 111 */
							BgL_objectz00_bglt BgL_arg1807z00_901;

							BgL_arg1807z00_901 = (BgL_objectz00_bglt) (BgL_ez00_351);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Expand/eps.scm 111 */
									long BgL_idxz00_907;

									BgL_idxz00_907 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_901);
									BgL_test2091z00_1858 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_907 + 3L)) == BgL_classz00_899);
								}
							else
								{	/* Expand/eps.scm 111 */
									bool_t BgL_res1995z00_932;

									{	/* Expand/eps.scm 111 */
										obj_t BgL_oclassz00_915;

										{	/* Expand/eps.scm 111 */
											obj_t BgL_arg1815z00_923;
											long BgL_arg1816z00_924;

											BgL_arg1815z00_923 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Expand/eps.scm 111 */
												long BgL_arg1817z00_925;

												BgL_arg1817z00_925 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_901);
												BgL_arg1816z00_924 = (BgL_arg1817z00_925 - OBJECT_TYPE);
											}
											BgL_oclassz00_915 =
												VECTOR_REF(BgL_arg1815z00_923, BgL_arg1816z00_924);
										}
										{	/* Expand/eps.scm 111 */
											bool_t BgL__ortest_1115z00_916;

											BgL__ortest_1115z00_916 =
												(BgL_classz00_899 == BgL_oclassz00_915);
											if (BgL__ortest_1115z00_916)
												{	/* Expand/eps.scm 111 */
													BgL_res1995z00_932 = BgL__ortest_1115z00_916;
												}
											else
												{	/* Expand/eps.scm 111 */
													long BgL_odepthz00_917;

													{	/* Expand/eps.scm 111 */
														obj_t BgL_arg1804z00_918;

														BgL_arg1804z00_918 = (BgL_oclassz00_915);
														BgL_odepthz00_917 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_918);
													}
													if ((3L < BgL_odepthz00_917))
														{	/* Expand/eps.scm 111 */
															obj_t BgL_arg1802z00_920;

															{	/* Expand/eps.scm 111 */
																obj_t BgL_arg1803z00_921;

																BgL_arg1803z00_921 = (BgL_oclassz00_915);
																BgL_arg1802z00_920 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_921,
																	3L);
															}
															BgL_res1995z00_932 =
																(BgL_arg1802z00_920 == BgL_classz00_899);
														}
													else
														{	/* Expand/eps.scm 111 */
															BgL_res1995z00_932 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2091z00_1858 = BgL_res1995z00_932;
								}
						}
					else
						{	/* Expand/eps.scm 111 */
							BgL_test2091z00_1858 = ((bool_t) 0);
						}
				}
				if (BgL_test2091z00_1858)
					{	/* Expand/eps.scm 111 */
						return
							BGl_userzd2errorzd2notifyz00zztools_errorz00(BgL_ez00_351,
							CNST_TABLE_REF(1));
					}
				else
					{	/* Expand/eps.scm 111 */
						BGL_TAIL return BGl_raisez00zz__errorz00(BgL_ez00_351);
					}
			}
		}

	}



/* &<@anonymous:1335> */
	obj_t BGl_z62zc3z04anonymousza31335ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1320)
	{
		{	/* Expand/eps.scm 173 */
			return
				BGl_compilezd2expanderzd2zzexpand_epsz00(PROCEDURE_REF(BgL_envz00_1320,
					(int) (0L)), BGl_compilezd2expanderzd2envz00zzexpand_epsz00, BNIL);
		}

	}



/* &<@anonymous:1333> */
	obj_t BGl_z62zc3z04anonymousza31333ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1322, obj_t BgL_ez00_1324)
	{
		{	/* Expand/eps.scm 169 */
			{	/* Expand/eps.scm 170 */
				obj_t BgL_an_exitd1076z00_1323;

				BgL_an_exitd1076z00_1323 = PROCEDURE_REF(BgL_envz00_1322, (int) (0L));
				BGl_z62handlerz62zzexpand_epsz00(BgL_ez00_1324);
				{	/* Expand/eps.scm 171 */
					bool_t BgL_test2096z00_1890;

					{	/* Expand/eps.scm 171 */
						obj_t BgL_classz00_1421;

						BgL_classz00_1421 = BGl_z62errorz62zz__objectz00;
						if (BGL_OBJECTP(BgL_ez00_1324))
							{	/* Expand/eps.scm 171 */
								BgL_objectz00_bglt BgL_arg1807z00_1422;

								BgL_arg1807z00_1422 = (BgL_objectz00_bglt) (BgL_ez00_1324);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Expand/eps.scm 171 */
										long BgL_idxz00_1423;

										BgL_idxz00_1423 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1422);
										BgL_test2096z00_1890 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1423 + 3L)) == BgL_classz00_1421);
									}
								else
									{	/* Expand/eps.scm 171 */
										bool_t BgL_res1998z00_1426;

										{	/* Expand/eps.scm 171 */
											obj_t BgL_oclassz00_1427;

											{	/* Expand/eps.scm 171 */
												obj_t BgL_arg1815z00_1428;
												long BgL_arg1816z00_1429;

												BgL_arg1815z00_1428 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Expand/eps.scm 171 */
													long BgL_arg1817z00_1430;

													BgL_arg1817z00_1430 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1422);
													BgL_arg1816z00_1429 =
														(BgL_arg1817z00_1430 - OBJECT_TYPE);
												}
												BgL_oclassz00_1427 =
													VECTOR_REF(BgL_arg1815z00_1428, BgL_arg1816z00_1429);
											}
											{	/* Expand/eps.scm 171 */
												bool_t BgL__ortest_1115z00_1431;

												BgL__ortest_1115z00_1431 =
													(BgL_classz00_1421 == BgL_oclassz00_1427);
												if (BgL__ortest_1115z00_1431)
													{	/* Expand/eps.scm 171 */
														BgL_res1998z00_1426 = BgL__ortest_1115z00_1431;
													}
												else
													{	/* Expand/eps.scm 171 */
														long BgL_odepthz00_1432;

														{	/* Expand/eps.scm 171 */
															obj_t BgL_arg1804z00_1433;

															BgL_arg1804z00_1433 = (BgL_oclassz00_1427);
															BgL_odepthz00_1432 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1433);
														}
														if ((3L < BgL_odepthz00_1432))
															{	/* Expand/eps.scm 171 */
																obj_t BgL_arg1802z00_1434;

																{	/* Expand/eps.scm 171 */
																	obj_t BgL_arg1803z00_1435;

																	BgL_arg1803z00_1435 = (BgL_oclassz00_1427);
																	BgL_arg1802z00_1434 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1435,
																		3L);
																}
																BgL_res1998z00_1426 =
																	(BgL_arg1802z00_1434 == BgL_classz00_1421);
															}
														else
															{	/* Expand/eps.scm 171 */
																BgL_res1998z00_1426 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2096z00_1890 = BgL_res1998z00_1426;
									}
							}
						else
							{	/* Expand/eps.scm 171 */
								BgL_test2096z00_1890 = ((bool_t) 0);
							}
					}
					if (BgL_test2096z00_1890)
						{	/* Expand/eps.scm 172 */
							obj_t BgL_val1077z00_1436;

							BgL_val1077z00_1436 = CNST_TABLE_REF(7);
							return
								unwind_stack_until(BgL_an_exitd1076z00_1323, BFALSE,
								BgL_val1077z00_1436, BFALSE, BFALSE);
						}
					else
						{	/* Expand/eps.scm 171 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &<@anonymous:1314> */
	obj_t BGl_z62zc3z04anonymousza31314ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1325)
	{
		{	/* Expand/eps.scm 149 */
			return
				BGl_initialzd2expanderzd2zzexpand_epsz00(PROCEDURE_REF(BgL_envz00_1325,
					(int) (0L)), BGl_initialzd2expanderzd2envz00zzexpand_epsz00);
		}

	}



/* &<@anonymous:1312> */
	obj_t BGl_z62zc3z04anonymousza31312ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1327, obj_t BgL_ez00_1329)
	{
		{	/* Expand/eps.scm 145 */
			{	/* Expand/eps.scm 146 */
				obj_t BgL_an_exitd1067z00_1328;

				BgL_an_exitd1067z00_1328 = PROCEDURE_REF(BgL_envz00_1327, (int) (0L));
				BGl_z62handlerz62zzexpand_epsz00(BgL_ez00_1329);
				{	/* Expand/eps.scm 147 */
					bool_t BgL_test2101z00_1921;

					{	/* Expand/eps.scm 147 */
						obj_t BgL_classz00_1437;

						BgL_classz00_1437 = BGl_z62errorz62zz__objectz00;
						if (BGL_OBJECTP(BgL_ez00_1329))
							{	/* Expand/eps.scm 147 */
								BgL_objectz00_bglt BgL_arg1807z00_1438;

								BgL_arg1807z00_1438 = (BgL_objectz00_bglt) (BgL_ez00_1329);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Expand/eps.scm 147 */
										long BgL_idxz00_1439;

										BgL_idxz00_1439 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1438);
										BgL_test2101z00_1921 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1439 + 3L)) == BgL_classz00_1437);
									}
								else
									{	/* Expand/eps.scm 147 */
										bool_t BgL_res1997z00_1442;

										{	/* Expand/eps.scm 147 */
											obj_t BgL_oclassz00_1443;

											{	/* Expand/eps.scm 147 */
												obj_t BgL_arg1815z00_1444;
												long BgL_arg1816z00_1445;

												BgL_arg1815z00_1444 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Expand/eps.scm 147 */
													long BgL_arg1817z00_1446;

													BgL_arg1817z00_1446 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1438);
													BgL_arg1816z00_1445 =
														(BgL_arg1817z00_1446 - OBJECT_TYPE);
												}
												BgL_oclassz00_1443 =
													VECTOR_REF(BgL_arg1815z00_1444, BgL_arg1816z00_1445);
											}
											{	/* Expand/eps.scm 147 */
												bool_t BgL__ortest_1115z00_1447;

												BgL__ortest_1115z00_1447 =
													(BgL_classz00_1437 == BgL_oclassz00_1443);
												if (BgL__ortest_1115z00_1447)
													{	/* Expand/eps.scm 147 */
														BgL_res1997z00_1442 = BgL__ortest_1115z00_1447;
													}
												else
													{	/* Expand/eps.scm 147 */
														long BgL_odepthz00_1448;

														{	/* Expand/eps.scm 147 */
															obj_t BgL_arg1804z00_1449;

															BgL_arg1804z00_1449 = (BgL_oclassz00_1443);
															BgL_odepthz00_1448 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1449);
														}
														if ((3L < BgL_odepthz00_1448))
															{	/* Expand/eps.scm 147 */
																obj_t BgL_arg1802z00_1450;

																{	/* Expand/eps.scm 147 */
																	obj_t BgL_arg1803z00_1451;

																	BgL_arg1803z00_1451 = (BgL_oclassz00_1443);
																	BgL_arg1802z00_1450 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1451,
																		3L);
																}
																BgL_res1997z00_1442 =
																	(BgL_arg1802z00_1450 == BgL_classz00_1437);
															}
														else
															{	/* Expand/eps.scm 147 */
																BgL_res1997z00_1442 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2101z00_1921 = BgL_res1997z00_1442;
									}
							}
						else
							{	/* Expand/eps.scm 147 */
								BgL_test2101z00_1921 = ((bool_t) 0);
							}
					}
					if (BgL_test2101z00_1921)
						{	/* Expand/eps.scm 148 */
							obj_t BgL_val1068z00_1452;

							BgL_val1068z00_1452 = CNST_TABLE_REF(7);
							return
								unwind_stack_until(BgL_an_exitd1067z00_1328, BFALSE,
								BgL_val1068z00_1452, BFALSE, BFALSE);
						}
					else
						{	/* Expand/eps.scm 147 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &<@anonymous:1286> */
	obj_t BGl_z62zc3z04anonymousza31286ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1330)
	{
		{	/* Expand/eps.scm 138 */
			{	/* Expand/eps.scm 139 */
				obj_t BgL_srcz00_1331;

				BgL_srcz00_1331 = PROCEDURE_REF(BgL_envz00_1330, (int) (0L));
				{	/* Expand/eps.scm 139 */
					obj_t BgL_arg1304z00_1453;

					BgL_arg1304z00_1453 = CAR(((obj_t) BgL_srcz00_1331));
					return
						BGl_initialzd2expanderzd2zzexpand_epsz00(BgL_arg1304z00_1453,
						BGl_initialzd2expanderzd2envz00zzexpand_epsz00);
				}
			}
		}

	}



/* &<@anonymous:1285> */
	obj_t BGl_z62zc3z04anonymousza31285ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1332, obj_t BgL_ez00_1333)
	{
		{	/* Expand/eps.scm 135 */
			BGl_z62handlerz62zzexpand_epsz00(BgL_ez00_1333);
			return CNST_TABLE_REF(7);
		}

	}



/* &<@anonymous:1251> */
	obj_t BGl_z62zc3z04anonymousza31251ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1334)
	{
		{	/* Expand/eps.scm 130 */
			{	/* Expand/eps.scm 131 */
				obj_t BgL_srcz00_1335;

				BgL_srcz00_1335 = PROCEDURE_REF(BgL_envz00_1334, (int) (0L));
				{	/* Expand/eps.scm 131 */
					obj_t BgL_arg1252z00_1454;

					BgL_arg1252z00_1454 = CAR(((obj_t) BgL_srcz00_1335));
					return
						BGl_initialzd2expanderzd2zzexpand_epsz00(BgL_arg1252z00_1454,
						BGl_initialzd2expanderzd2envz00zzexpand_epsz00);
				}
			}
		}

	}



/* &<@anonymous:1250> */
	obj_t BGl_z62zc3z04anonymousza31250ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1336, obj_t BgL_ez00_1337)
	{
		{	/* Expand/eps.scm 127 */
			BGl_z62handlerz62zzexpand_epsz00(BgL_ez00_1337);
			return CNST_TABLE_REF(7);
		}

	}



/* comptime-expand */
	BGL_EXPORTED_DEF obj_t BGl_comptimezd2expandzd2zzexpand_epsz00(obj_t
		BgL_xz00_43)
	{
		{	/* Expand/eps.scm 183 */
			return
				BGl_initialzd2expanderzd2zzexpand_epsz00(BgL_xz00_43,
				BGl_initialzd2expanderzd2envz00zzexpand_epsz00);
		}

	}



/* &comptime-expand */
	obj_t BGl_z62comptimezd2expandzb0zzexpand_epsz00(obj_t BgL_envz00_1339,
		obj_t BgL_xz00_1340)
	{
		{	/* Expand/eps.scm 183 */
			return BGl_comptimezd2expandzd2zzexpand_epsz00(BgL_xz00_1340);
		}

	}



/* comptime-expand/error */
	BGL_EXPORTED_DEF obj_t BGl_comptimezd2expandzf2errorz20zzexpand_epsz00(obj_t
		BgL_xz00_44)
	{
		{	/* Expand/eps.scm 189 */
			{	/* Expand/eps.scm 193 */
				obj_t BgL_zc3z04anonymousza31375ze3z87_1341;
				obj_t BgL_zc3z04anonymousza31371ze3z87_1342;

				BgL_zc3z04anonymousza31375ze3z87_1341 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31375ze3ze5zzexpand_epsz00,
					(int) (0L), (int) (1L));
				BgL_zc3z04anonymousza31371ze3z87_1342 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31371ze3ze5zzexpand_epsz00,
					(int) (1L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31375ze3z87_1341,
					(int) (0L), BgL_xz00_44);
				PROCEDURE_SET(BgL_zc3z04anonymousza31371ze3z87_1342,
					(int) (0L), BgL_xz00_44);
				return
					BGl_withzd2exceptionzd2handlerz00zz__errorz00
					(BgL_zc3z04anonymousza31371ze3z87_1342,
					BgL_zc3z04anonymousza31375ze3z87_1341);
			}
		}

	}



/* &comptime-expand/error */
	obj_t BGl_z62comptimezd2expandzf2errorz42zzexpand_epsz00(obj_t
		BgL_envz00_1343, obj_t BgL_xz00_1344)
	{
		{	/* Expand/eps.scm 189 */
			return BGl_comptimezd2expandzf2errorz20zzexpand_epsz00(BgL_xz00_1344);
		}

	}



/* &<@anonymous:1375> */
	obj_t BGl_z62zc3z04anonymousza31375ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1345)
	{
		{	/* Expand/eps.scm 197 */
			return
				BGl_initialzd2expanderzd2zzexpand_epsz00(PROCEDURE_REF(BgL_envz00_1345,
					(int) (0L)), BGl_initialzd2expanderzd2envz00zzexpand_epsz00);
		}

	}



/* &<@anonymous:1371> */
	obj_t BGl_z62zc3z04anonymousza31371ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1347, obj_t BgL_ez00_1349)
	{
		{	/* Expand/eps.scm 192 */
			{	/* Expand/eps.scm 193 */
				obj_t BgL_xz00_1348;

				BgL_xz00_1348 = PROCEDURE_REF(BgL_envz00_1347, (int) (0L));
				{	/* Expand/eps.scm 193 */
					bool_t BgL_test2106z00_1979;

					{	/* Expand/eps.scm 193 */
						obj_t BgL_classz00_1455;

						BgL_classz00_1455 = BGl_z62errorz62zz__objectz00;
						if (BGL_OBJECTP(BgL_ez00_1349))
							{	/* Expand/eps.scm 193 */
								BgL_objectz00_bglt BgL_arg1807z00_1456;

								BgL_arg1807z00_1456 = (BgL_objectz00_bglt) (BgL_ez00_1349);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Expand/eps.scm 193 */
										long BgL_idxz00_1457;

										BgL_idxz00_1457 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1456);
										BgL_test2106z00_1979 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1457 + 3L)) == BgL_classz00_1455);
									}
								else
									{	/* Expand/eps.scm 193 */
										bool_t BgL_res2000z00_1460;

										{	/* Expand/eps.scm 193 */
											obj_t BgL_oclassz00_1461;

											{	/* Expand/eps.scm 193 */
												obj_t BgL_arg1815z00_1462;
												long BgL_arg1816z00_1463;

												BgL_arg1815z00_1462 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Expand/eps.scm 193 */
													long BgL_arg1817z00_1464;

													BgL_arg1817z00_1464 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1456);
													BgL_arg1816z00_1463 =
														(BgL_arg1817z00_1464 - OBJECT_TYPE);
												}
												BgL_oclassz00_1461 =
													VECTOR_REF(BgL_arg1815z00_1462, BgL_arg1816z00_1463);
											}
											{	/* Expand/eps.scm 193 */
												bool_t BgL__ortest_1115z00_1465;

												BgL__ortest_1115z00_1465 =
													(BgL_classz00_1455 == BgL_oclassz00_1461);
												if (BgL__ortest_1115z00_1465)
													{	/* Expand/eps.scm 193 */
														BgL_res2000z00_1460 = BgL__ortest_1115z00_1465;
													}
												else
													{	/* Expand/eps.scm 193 */
														long BgL_odepthz00_1466;

														{	/* Expand/eps.scm 193 */
															obj_t BgL_arg1804z00_1467;

															BgL_arg1804z00_1467 = (BgL_oclassz00_1461);
															BgL_odepthz00_1466 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1467);
														}
														if ((3L < BgL_odepthz00_1466))
															{	/* Expand/eps.scm 193 */
																obj_t BgL_arg1802z00_1468;

																{	/* Expand/eps.scm 193 */
																	obj_t BgL_arg1803z00_1469;

																	BgL_arg1803z00_1469 = (BgL_oclassz00_1461);
																	BgL_arg1802z00_1468 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1469,
																		3L);
																}
																BgL_res2000z00_1460 =
																	(BgL_arg1802z00_1468 == BgL_classz00_1455);
															}
														else
															{	/* Expand/eps.scm 193 */
																BgL_res2000z00_1460 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2106z00_1979 = BgL_res2000z00_1460;
									}
							}
						else
							{	/* Expand/eps.scm 193 */
								BgL_test2106z00_1979 = ((bool_t) 0);
							}
					}
					if (BgL_test2106z00_1979)
						{	/* Expand/eps.scm 193 */
							BGl_userzd2errorzd2notifyz00zztools_errorz00(BgL_ez00_1349,
								CNST_TABLE_REF(1));
						}
					else
						{	/* Expand/eps.scm 193 */
							BFALSE;
						}
				}
				{	/* Expand/eps.scm 195 */
					obj_t BgL_list1373z00_1470;

					BgL_list1373z00_1470 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
					BGl_userzd2errorzd2zztools_errorz00(BGl_string2013z00zzexpand_epsz00,
						BGl_string2014z00zzexpand_epsz00, BgL_xz00_1348,
						BgL_list1373z00_1470);
				}
				{	/* Expand/eps.scm 196 */
					obj_t BgL_list1374z00_1471;

					BgL_list1374z00_1471 = MAKE_YOUNG_PAIR(BINT(1L), BNIL);
					return BGl_exitz00zz__errorz00(BgL_list1374z00_1471);
				}
			}
		}

	}



/* comptime-expand-cond-expand-only */
	BGL_EXPORTED_DEF obj_t
		BGl_comptimezd2expandzd2condzd2expandzd2onlyz00zzexpand_epsz00(obj_t
		BgL_xz00_45)
	{
		{	/* Expand/eps.scm 203 */
			{	/* Expand/eps.scm 218 */
				obj_t BgL_zc3z04anonymousza31385ze3z87_1351;
				obj_t BgL_zc3z04anonymousza31378ze3z87_1352;

				BgL_zc3z04anonymousza31385ze3z87_1351 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31385ze3ze5zzexpand_epsz00,
					(int) (0L), (int) (2L));
				BgL_zc3z04anonymousza31378ze3z87_1352 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31378ze3ze5zzexpand_epsz00,
					(int) (1L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31385ze3z87_1351,
					(int) (0L), BgL_xz00_45);
				PROCEDURE_SET(BgL_zc3z04anonymousza31385ze3z87_1351,
					(int) (1L), BGl_proc2015z00zzexpand_epsz00);
				PROCEDURE_SET(BgL_zc3z04anonymousza31378ze3z87_1352,
					(int) (0L), BgL_xz00_45);
				return
					BGl_withzd2exceptionzd2handlerz00zz__errorz00
					(BgL_zc3z04anonymousza31378ze3z87_1352,
					BgL_zc3z04anonymousza31385ze3z87_1351);
			}
		}

	}



/* &comptime-expand-cond-expand-only */
	obj_t BGl_z62comptimezd2expandzd2condzd2expandzd2onlyz62zzexpand_epsz00(obj_t
		BgL_envz00_1353, obj_t BgL_xz00_1354)
	{
		{	/* Expand/eps.scm 203 */
			return
				BGl_comptimezd2expandzd2condzd2expandzd2onlyz00zzexpand_epsz00
				(BgL_xz00_1354);
		}

	}



/* &<@anonymous:1385> */
	obj_t BGl_z62zc3z04anonymousza31385ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1355)
	{
		{	/* Expand/eps.scm 222 */
			{	/* Expand/eps.scm 223 */
				obj_t BgL_xz00_1356;
				obj_t BgL_condzd2expandzd2onlyzd2expanderzd2_1357;

				BgL_xz00_1356 = PROCEDURE_REF(BgL_envz00_1355, (int) (0L));
				BgL_condzd2expandzd2onlyzd2expanderzd2_1357 =
					((obj_t) PROCEDURE_REF(BgL_envz00_1355, (int) (1L)));
				return
					BGl_initialzd2expanderzd2zzexpand_epsz00(BgL_xz00_1356,
					BgL_condzd2expandzd2onlyzd2expanderzd2_1357);
			}
		}

	}



/* &<@anonymous:1378> */
	obj_t BGl_z62zc3z04anonymousza31378ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1358, obj_t BgL_ez00_1360)
	{
		{	/* Expand/eps.scm 217 */
			{	/* Expand/eps.scm 218 */
				obj_t BgL_xz00_1359;

				BgL_xz00_1359 = PROCEDURE_REF(BgL_envz00_1358, (int) (0L));
				{	/* Expand/eps.scm 218 */
					bool_t BgL_test2111z00_2031;

					{	/* Expand/eps.scm 218 */
						obj_t BgL_classz00_1472;

						BgL_classz00_1472 = BGl_z62errorz62zz__objectz00;
						if (BGL_OBJECTP(BgL_ez00_1360))
							{	/* Expand/eps.scm 218 */
								BgL_objectz00_bglt BgL_arg1807z00_1473;

								BgL_arg1807z00_1473 = (BgL_objectz00_bglt) (BgL_ez00_1360);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Expand/eps.scm 218 */
										long BgL_idxz00_1474;

										BgL_idxz00_1474 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1473);
										BgL_test2111z00_2031 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1474 + 3L)) == BgL_classz00_1472);
									}
								else
									{	/* Expand/eps.scm 218 */
										bool_t BgL_res2001z00_1477;

										{	/* Expand/eps.scm 218 */
											obj_t BgL_oclassz00_1478;

											{	/* Expand/eps.scm 218 */
												obj_t BgL_arg1815z00_1479;
												long BgL_arg1816z00_1480;

												BgL_arg1815z00_1479 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Expand/eps.scm 218 */
													long BgL_arg1817z00_1481;

													BgL_arg1817z00_1481 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1473);
													BgL_arg1816z00_1480 =
														(BgL_arg1817z00_1481 - OBJECT_TYPE);
												}
												BgL_oclassz00_1478 =
													VECTOR_REF(BgL_arg1815z00_1479, BgL_arg1816z00_1480);
											}
											{	/* Expand/eps.scm 218 */
												bool_t BgL__ortest_1115z00_1482;

												BgL__ortest_1115z00_1482 =
													(BgL_classz00_1472 == BgL_oclassz00_1478);
												if (BgL__ortest_1115z00_1482)
													{	/* Expand/eps.scm 218 */
														BgL_res2001z00_1477 = BgL__ortest_1115z00_1482;
													}
												else
													{	/* Expand/eps.scm 218 */
														long BgL_odepthz00_1483;

														{	/* Expand/eps.scm 218 */
															obj_t BgL_arg1804z00_1484;

															BgL_arg1804z00_1484 = (BgL_oclassz00_1478);
															BgL_odepthz00_1483 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1484);
														}
														if ((3L < BgL_odepthz00_1483))
															{	/* Expand/eps.scm 218 */
																obj_t BgL_arg1802z00_1485;

																{	/* Expand/eps.scm 218 */
																	obj_t BgL_arg1803z00_1486;

																	BgL_arg1803z00_1486 = (BgL_oclassz00_1478);
																	BgL_arg1802z00_1485 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1486,
																		3L);
																}
																BgL_res2001z00_1477 =
																	(BgL_arg1802z00_1485 == BgL_classz00_1472);
															}
														else
															{	/* Expand/eps.scm 218 */
																BgL_res2001z00_1477 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2111z00_2031 = BgL_res2001z00_1477;
									}
							}
						else
							{	/* Expand/eps.scm 218 */
								BgL_test2111z00_2031 = ((bool_t) 0);
							}
					}
					if (BgL_test2111z00_2031)
						{	/* Expand/eps.scm 218 */
							BGl_userzd2errorzd2notifyz00zztools_errorz00(BgL_ez00_1360,
								CNST_TABLE_REF(1));
						}
					else
						{	/* Expand/eps.scm 218 */
							BFALSE;
						}
				}
				{	/* Expand/eps.scm 220 */
					obj_t BgL_list1383z00_1487;

					BgL_list1383z00_1487 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
					BGl_userzd2errorzd2zztools_errorz00(BGl_string2013z00zzexpand_epsz00,
						BGl_string2014z00zzexpand_epsz00, BgL_xz00_1359,
						BgL_list1383z00_1487);
				}
				{	/* Expand/eps.scm 221 */
					obj_t BgL_list1384z00_1488;

					BgL_list1384z00_1488 = MAKE_YOUNG_PAIR(BINT(1L), BNIL);
					return BGl_exitz00zz__errorz00(BgL_list1384z00_1488);
				}
			}
		}

	}



/* &cond-expand-only-expander */
	obj_t BGl_z62condzd2expandzd2onlyzd2expanderzb0zzexpand_epsz00(obj_t
		BgL_envz00_1361, obj_t BgL_xz00_1362, obj_t BgL_ez00_1363)
	{
		{	/* Expand/eps.scm 213 */
			{
				obj_t BgL_restz00_1490;

				if (PAIRP(BgL_xz00_1362))
					{	/* Expand/eps.scm 213 */
						if ((CAR(((obj_t) BgL_xz00_1362)) == CNST_TABLE_REF(8)))
							{	/* Expand/eps.scm 213 */
								return
									BGl_initialzd2expanderzd2zzexpand_epsz00(BgL_xz00_1362,
									BgL_ez00_1363);
							}
						else
							{	/* Expand/eps.scm 213 */
								if ((CAR(((obj_t) BgL_xz00_1362)) == CNST_TABLE_REF(9)))
									{	/* Expand/eps.scm 213 */
										obj_t BgL_arg1434z00_1497;

										BgL_arg1434z00_1497 = CDR(((obj_t) BgL_xz00_1362));
										BgL_restz00_1490 = BgL_arg1434z00_1497;
										{	/* Expand/eps.scm 210 */
											obj_t BgL_arg1453z00_1491;

											{
												obj_t BgL_l1112z00_1493;

												BgL_l1112z00_1493 = BgL_restz00_1490;
											BgL_zc3z04anonymousza31454ze3z87_1492:
												if (NULLP(BgL_l1112z00_1493))
													{	/* Expand/eps.scm 210 */
														BgL_arg1453z00_1491 = BgL_restz00_1490;
													}
												else
													{	/* Expand/eps.scm 210 */
														{	/* Expand/eps.scm 210 */
															obj_t BgL_arg1472z00_1494;

															{	/* Expand/eps.scm 210 */
																obj_t BgL_xz00_1495;

																BgL_xz00_1495 =
																	CAR(((obj_t) BgL_l1112z00_1493));
																BgL_arg1472z00_1494 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_1363,
																	BgL_xz00_1495, BgL_ez00_1363);
															}
															{	/* Expand/eps.scm 210 */
																obj_t BgL_tmpz00_2085;

																BgL_tmpz00_2085 = ((obj_t) BgL_l1112z00_1493);
																SET_CAR(BgL_tmpz00_2085, BgL_arg1472z00_1494);
															}
														}
														{	/* Expand/eps.scm 210 */
															obj_t BgL_arg1473z00_1496;

															BgL_arg1473z00_1496 =
																CDR(((obj_t) BgL_l1112z00_1493));
															{
																obj_t BgL_l1112z00_2090;

																BgL_l1112z00_2090 = BgL_arg1473z00_1496;
																BgL_l1112z00_1493 = BgL_l1112z00_2090;
																goto BgL_zc3z04anonymousza31454ze3z87_1492;
															}
														}
													}
											}
											{	/* Expand/eps.scm 210 */
												obj_t BgL_tmpz00_2091;

												BgL_tmpz00_2091 = ((obj_t) BgL_xz00_1362);
												SET_CDR(BgL_tmpz00_2091, BgL_arg1453z00_1491);
											}
										}
										return BgL_xz00_1362;
									}
								else
									{	/* Expand/eps.scm 213 */
										return BgL_xz00_1362;
									}
							}
					}
				else
					{	/* Expand/eps.scm 213 */
						return BgL_xz00_1362;
					}
			}
		}

	}



/* compile-expand */
	BGL_EXPORTED_DEF obj_t BGl_compilezd2expandzd2zzexpand_epsz00(obj_t
		BgL_xz00_46)
	{
		{	/* Expand/eps.scm 228 */
			{	/* Expand/eps.scm 229 */
				obj_t BgL_sz00_1499;

				BgL_sz00_1499 = BNIL;
				return BGl_expandze70ze7zzexpand_epsz00(BgL_xz00_46, BgL_sz00_1499);
			}
		}

	}



/* &compile-expand */
	obj_t BGl_z62compilezd2expandzb0zzexpand_epsz00(obj_t BgL_envz00_1364,
		obj_t BgL_xz00_1365)
	{
		{	/* Expand/eps.scm 228 */
			return BGl_compilezd2expandzd2zzexpand_epsz00(BgL_xz00_1365);
		}

	}



/* initial-expander */
	obj_t BGl_initialzd2expanderzd2zzexpand_epsz00(obj_t BgL_xz00_47,
		obj_t BgL_ez00_48)
	{
		{	/* Expand/eps.scm 234 */
			{	/* Expand/eps.scm 236 */
				obj_t BgL_e1z00_403;

				if (SYMBOLP(BgL_xz00_47))
					{	/* Expand/eps.scm 237 */
						BgL_e1z00_403 = BGl_identifierzd2expanderzd2envz00zzexpand_epsz00;
					}
				else
					{	/* Expand/eps.scm 237 */
						if (NULLP(BgL_xz00_47))
							{	/* Expand/eps.scm 239 */
								BgL_e1z00_403 =
									BGl_errorz00zz__errorz00(BFALSE,
									BGl_string2016z00zzexpand_epsz00, BNIL);
							}
						else
							{	/* Expand/eps.scm 239 */
								if (PAIRP(BgL_xz00_47))
									{	/* Expand/eps.scm 243 */
										bool_t BgL_test2123z00_2103;

										{	/* Expand/eps.scm 243 */
											obj_t BgL_tmpz00_2104;

											BgL_tmpz00_2104 = CAR(BgL_xz00_47);
											BgL_test2123z00_2103 = SYMBOLP(BgL_tmpz00_2104);
										}
										if (BgL_test2123z00_2103)
											{	/* Expand/eps.scm 244 */
												obj_t BgL_idz00_418;

												BgL_idz00_418 =
													BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(CAR
													(BgL_xz00_47),
													BGl_findzd2locationzd2zztools_locationz00
													(BgL_xz00_47));
												{	/* Expand/eps.scm 246 */
													bool_t BgL_test2124z00_2110;

													{	/* Expand/eps.scm 246 */
														obj_t BgL_tmpz00_2111;

														BgL_tmpz00_2111 =
															BGl_assqz00zz__r4_pairs_and_lists_6_3z00
															(BgL_idz00_418,
															BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00);
														BgL_test2124z00_2110 = PAIRP(BgL_tmpz00_2111);
													}
													if (BgL_test2124z00_2110)
														{	/* Expand/eps.scm 246 */
															BgL_e1z00_403 =
																BGl_applicationzd2expanderzd2envz00zzexpand_epsz00;
														}
													else
														{	/* Expand/eps.scm 248 */
															obj_t BgL_g1087z00_422;

															BgL_g1087z00_422 =
																BGl_getzd2compilerzd2expanderz00zz__macroz00
																(BgL_idz00_418);
															if (CBOOL(BgL_g1087z00_422))
																{	/* Expand/eps.scm 248 */
																	BgL_e1z00_403 = BgL_g1087z00_422;
																}
															else
																{	/* Expand/eps.scm 248 */
																	BgL_e1z00_403 =
																		BGl_applicationzd2expanderzd2envz00zzexpand_epsz00;
																}
														}
												}
											}
										else
											{	/* Expand/eps.scm 243 */
												BgL_e1z00_403 =
													BGl_applicationzd2expanderzd2envz00zzexpand_epsz00;
											}
									}
								else
									{	/* Expand/eps.scm 241 */
										BgL_e1z00_403 = BGl_proc2017z00zzexpand_epsz00;
									}
							}
					}
				{	/* Expand/eps.scm 255 */
					obj_t BgL_newz00_404;

					BgL_newz00_404 =
						BGL_PROCEDURE_CALL2(BgL_e1z00_403, BgL_xz00_47, BgL_ez00_48);
					{	/* Expand/eps.scm 256 */
						bool_t BgL_test2126z00_2122;

						if (PAIRP(BgL_newz00_404))
							{	/* Expand/eps.scm 256 */
								if (EPAIRP(BgL_newz00_404))
									{	/* Expand/eps.scm 256 */
										BgL_test2126z00_2122 = ((bool_t) 0);
									}
								else
									{	/* Expand/eps.scm 256 */
										BgL_test2126z00_2122 = EPAIRP(BgL_xz00_47);
									}
							}
						else
							{	/* Expand/eps.scm 256 */
								BgL_test2126z00_2122 = ((bool_t) 0);
							}
						if (BgL_test2126z00_2122)
							{	/* Expand/eps.scm 257 */
								obj_t BgL_arg1485z00_408;
								obj_t BgL_arg1489z00_409;
								obj_t BgL_arg1502z00_410;

								BgL_arg1485z00_408 = CAR(BgL_newz00_404);
								BgL_arg1489z00_409 = CDR(BgL_newz00_404);
								BgL_arg1502z00_410 = CER(((obj_t) BgL_xz00_47));
								{	/* Expand/eps.scm 257 */
									obj_t BgL_res2002z00_1125;

									BgL_res2002z00_1125 =
										MAKE_YOUNG_EPAIR(BgL_arg1485z00_408, BgL_arg1489z00_409,
										BgL_arg1502z00_410);
									return BgL_res2002z00_1125;
								}
							}
						else
							{	/* Expand/eps.scm 256 */
								return BgL_newz00_404;
							}
					}
				}
			}
		}

	}



/* &initial-expander */
	obj_t BGl_z62initialzd2expanderzb0zzexpand_epsz00(obj_t BgL_envz00_1310,
		obj_t BgL_xz00_1311, obj_t BgL_ez00_1312)
	{
		{	/* Expand/eps.scm 234 */
			return
				BGl_initialzd2expanderzd2zzexpand_epsz00(BgL_xz00_1311, BgL_ez00_1312);
		}

	}



/* &<@anonymous:1547> */
	obj_t BGl_z62zc3z04anonymousza31547ze3ze5zzexpand_epsz00(obj_t
		BgL_envz00_1367, obj_t BgL_xz00_1368, obj_t BgL_ez00_1369)
	{
		{	/* Expand/eps.scm 242 */
			return BgL_xz00_1368;
		}

	}



/* compile-expander */
	obj_t BGl_compilezd2expanderzd2zzexpand_epsz00(obj_t BgL_xz00_49,
		obj_t BgL_ez00_50, obj_t BgL_sz00_51)
	{
		{	/* Expand/eps.scm 266 */
			return BGl_expandze70ze7zzexpand_epsz00(BgL_xz00_49, BgL_sz00_51);
		}

	}



/* proto->frame~0 */
	obj_t BGl_protozd2ze3frameze70zd6zzexpand_epsz00(obj_t BgL_pz00_440)
	{
		{	/* Expand/eps.scm 273 */
			{	/* Expand/eps.scm 273 */
				obj_t BgL_l01116z00_442;

				BgL_l01116z00_442 =
					BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(BgL_pz00_440);
				{
					obj_t BgL_l1115z00_444;

					BgL_l1115z00_444 = BgL_l01116z00_442;
				BgL_zc3z04anonymousza31550ze3z87_445:
					if (NULLP(BgL_l1115z00_444))
						{	/* Expand/eps.scm 273 */
							return BgL_l01116z00_442;
						}
					else
						{	/* Expand/eps.scm 273 */
							{	/* Expand/eps.scm 273 */
								obj_t BgL_arg1552z00_447;

								{	/* Expand/eps.scm 273 */
									obj_t BgL_arg1553z00_448;

									BgL_arg1553z00_448 = CAR(((obj_t) BgL_l1115z00_444));
									BgL_arg1552z00_447 =
										BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_arg1553z00_448,
										BFALSE);
								}
								{	/* Expand/eps.scm 273 */
									obj_t BgL_tmpz00_2141;

									BgL_tmpz00_2141 = ((obj_t) BgL_l1115z00_444);
									SET_CAR(BgL_tmpz00_2141, BgL_arg1552z00_447);
								}
							}
							{	/* Expand/eps.scm 273 */
								obj_t BgL_arg1559z00_449;

								BgL_arg1559z00_449 = CDR(((obj_t) BgL_l1115z00_444));
								{
									obj_t BgL_l1115z00_2146;

									BgL_l1115z00_2146 = BgL_arg1559z00_449;
									BgL_l1115z00_444 = BgL_l1115z00_2146;
									goto BgL_zc3z04anonymousza31550ze3z87_445;
								}
							}
						}
				}
			}
		}

	}



/* expand~0 */
	obj_t BGl_expandze70ze7zzexpand_epsz00(obj_t BgL_xz00_472, obj_t BgL_sz00_473)
	{
		{	/* Expand/eps.scm 386 */
			{
				obj_t BgL_xz00_463;
				obj_t BgL_sz00_464;

				{
					obj_t BgL_pz00_477;
					obj_t BgL_bodyz00_478;
					obj_t BgL_loopz00_480;
					obj_t BgL_bindingsz00_481;
					obj_t BgL_bodyz00_482;
					obj_t BgL_bindingsz00_484;
					obj_t BgL_bodyz00_485;
					obj_t BgL_bindingsz00_487;
					obj_t BgL_bodyz00_488;
					obj_t BgL_bodyz00_491;
					obj_t BgL_varz00_494;
					obj_t BgL_clausesz00_495;
					obj_t BgL_funz00_498;
					obj_t BgL_argz00_499;

					if (NULLP(BgL_xz00_472))
						{	/* Expand/eps.scm 386 */
							return BgL_xz00_472;
						}
					else
						{	/* Expand/eps.scm 386 */
							if (PAIRP(BgL_xz00_472))
								{	/* Expand/eps.scm 386 */
									if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(11)))
										{	/* Expand/eps.scm 386 */
											return BgL_xz00_472;
										}
									else
										{	/* Expand/eps.scm 386 */
											if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(12)))
												{	/* Expand/eps.scm 386 */
													return BgL_xz00_472;
												}
											else
												{	/* Expand/eps.scm 386 */
													if (
														(CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(9)))
														{	/* Expand/eps.scm 386 */
														BgL_tagzd2416zd2_476:
															{	/* Expand/eps.scm 288 */
																obj_t BgL_arg1875z00_665;

																{	/* Expand/eps.scm 288 */
																	obj_t BgL_arg1876z00_666;

																	BgL_arg1876z00_666 =
																		CDR(((obj_t) BgL_xz00_472));
																	BgL_arg1875z00_665 =
																		BGl_expandza2ze70z45zzexpand_epsz00
																		(BgL_arg1876z00_666, BgL_sz00_473);
																}
																{	/* Expand/eps.scm 288 */
																	obj_t BgL_tmpz00_2169;

																	BgL_tmpz00_2169 = ((obj_t) BgL_xz00_472);
																	SET_CDR(BgL_tmpz00_2169, BgL_arg1875z00_665);
																}
															}
															return BgL_xz00_472;
														}
													else
														{	/* Expand/eps.scm 386 */
															if (
																(CAR(
																		((obj_t) BgL_xz00_472)) ==
																	CNST_TABLE_REF(13)))
																{	/* Expand/eps.scm 386 */
																	goto BgL_tagzd2416zd2_476;
																}
															else
																{	/* Expand/eps.scm 386 */
																	if (
																		(CAR(
																				((obj_t) BgL_xz00_472)) ==
																			CNST_TABLE_REF(14)))
																		{	/* Expand/eps.scm 386 */
																			goto BgL_tagzd2416zd2_476;
																		}
																	else
																		{	/* Expand/eps.scm 386 */
																			obj_t BgL_carzd2474zd2_517;
																			obj_t BgL_cdrzd2475zd2_518;

																			BgL_carzd2474zd2_517 =
																				CAR(((obj_t) BgL_xz00_472));
																			BgL_cdrzd2475zd2_518 =
																				CDR(((obj_t) BgL_xz00_472));
																			{

																				if (
																					(BgL_carzd2474zd2_517 ==
																						CNST_TABLE_REF(18)))
																					{	/* Expand/eps.scm 386 */
																					BgL_kapzd2476zd2_519:
																						if (PAIRP(BgL_cdrzd2475zd2_518))
																							{	/* Expand/eps.scm 386 */
																								BgL_pz00_477 =
																									CAR(BgL_cdrzd2475zd2_518);
																								BgL_bodyz00_478 =
																									CDR(BgL_cdrzd2475zd2_518);
																								{	/* Expand/eps.scm 291 */
																									obj_t BgL_framez00_667;

																									BgL_framez00_667 =
																										BGl_protozd2ze3frameze70zd6zzexpand_epsz00
																										(BgL_pz00_477);
																									{	/* Expand/eps.scm 292 */
																										obj_t BgL_arg1877z00_668;
																										obj_t BgL_arg1878z00_669;

																										BgL_arg1877z00_668 =
																											CDR(
																											((obj_t) BgL_xz00_472));
																										BgL_arg1878z00_669 =
																											BGl_expandza2ze70z45zzexpand_epsz00
																											(BgL_bodyz00_478,
																											BGl_appendzd221011zd2zzexpand_epsz00
																											(BgL_framez00_667,
																												BgL_sz00_473));
																										{	/* Expand/eps.scm 292 */
																											obj_t BgL_tmpz00_2196;

																											BgL_tmpz00_2196 =
																												((obj_t)
																												BgL_arg1877z00_668);
																											SET_CDR(BgL_tmpz00_2196,
																												BgL_arg1878z00_669);
																										}
																									}
																									return BgL_xz00_472;
																								}
																							}
																						else
																							{	/* Expand/eps.scm 386 */
																								if (
																									(CAR(
																											((obj_t) BgL_xz00_472)) ==
																										CNST_TABLE_REF(10)))
																									{	/* Expand/eps.scm 386 */
																										BgL_xz00_463 = BgL_xz00_472;
																										BgL_sz00_464 = BgL_sz00_473;
																									BgL_zc3z04anonymousza31566ze3z87_465:
																										{	/* Expand/eps.scm 279 */
																											obj_t BgL_framez00_466;

																											{	/* Expand/eps.scm 279 */
																												obj_t
																													BgL_arg1584z00_471;
																												{	/* Expand/eps.scm 279 */
																													obj_t
																														BgL_pairz00_1135;
																													BgL_pairz00_1135 =
																														CDR(((obj_t)
																															BgL_xz00_463));
																													BgL_arg1584z00_471 =
																														CAR
																														(BgL_pairz00_1135);
																												}
																												BgL_framez00_466 =
																													BGl_protozd2ze3frameze70zd6zzexpand_epsz00
																													(BgL_arg1584z00_471);
																											}
																											{	/* Expand/eps.scm 280 */
																												obj_t
																													BgL_arg1571z00_467;
																												obj_t
																													BgL_arg1573z00_468;
																												BgL_arg1571z00_467 =
																													CDR(((obj_t)
																														BgL_xz00_463));
																												{	/* Expand/eps.scm 280 */
																													obj_t
																														BgL_arg1575z00_469;
																													obj_t
																														BgL_arg1576z00_470;
																													{	/* Expand/eps.scm 280 */
																														obj_t
																															BgL_pairz00_1140;
																														BgL_pairz00_1140 =
																															CDR(((obj_t)
																																BgL_xz00_463));
																														BgL_arg1575z00_469 =
																															CDR
																															(BgL_pairz00_1140);
																													}
																													BgL_arg1576z00_470 =
																														BGl_appendzd221011zd2zzexpand_epsz00
																														(BgL_framez00_466,
																														BgL_sz00_464);
																													BgL_arg1573z00_468 =
																														BGl_expandza2ze70z45zzexpand_epsz00
																														(BgL_arg1575z00_469,
																														BgL_arg1576z00_470);
																												}
																												{	/* Expand/eps.scm 280 */
																													obj_t BgL_tmpz00_2217;

																													BgL_tmpz00_2217 =
																														((obj_t)
																														BgL_arg1571z00_467);
																													SET_CDR
																														(BgL_tmpz00_2217,
																														BgL_arg1573z00_468);
																												}
																											}
																											return BgL_xz00_463;
																										}
																									}
																								else
																									{	/* Expand/eps.scm 386 */
																										if (
																											(CAR(
																													((obj_t)
																														BgL_xz00_472)) ==
																												CNST_TABLE_REF(15)))
																											{	/* Expand/eps.scm 386 */
																											BgL_tagzd2423zd2_493:
																												{	/* Expand/eps.scm 349 */
																													obj_t
																														BgL_arg1946z00_776;
																													{	/* Expand/eps.scm 349 */
																														obj_t
																															BgL_arg1947z00_777;
																														BgL_arg1947z00_777 =
																															CDR(((obj_t)
																																BgL_xz00_472));
																														BgL_arg1946z00_776 =
																															BGl_expandza2ze70z45zzexpand_epsz00
																															(BgL_arg1947z00_777,
																															BgL_sz00_473);
																													}
																													{	/* Expand/eps.scm 349 */
																														obj_t
																															BgL_tmpz00_2228;
																														BgL_tmpz00_2228 =
																															((obj_t)
																															BgL_xz00_472);
																														SET_CDR
																															(BgL_tmpz00_2228,
																															BgL_arg1946z00_776);
																													}
																												}
																												return BgL_xz00_472;
																											}
																										else
																											{	/* Expand/eps.scm 386 */
																												if (
																													(CAR(
																															((obj_t)
																																BgL_xz00_472))
																														==
																														CNST_TABLE_REF(16)))
																													{	/* Expand/eps.scm 386 */
																													BgL_tagzd2425zd2_497:
																														{	/* Expand/eps.scm 359 */
																															obj_t
																																BgL_arg1956z00_790;
																															{	/* Expand/eps.scm 359 */
																																obj_t
																																	BgL_arg1957z00_791;
																																BgL_arg1957z00_791
																																	=
																																	CDR(((obj_t)
																																		BgL_xz00_472));
																																BgL_arg1956z00_790
																																	=
																																	BGl_expandza2ze70z45zzexpand_epsz00
																																	(BgL_arg1957z00_791,
																																	BgL_sz00_473);
																															}
																															{	/* Expand/eps.scm 359 */
																																obj_t
																																	BgL_tmpz00_2239;
																																BgL_tmpz00_2239
																																	=
																																	((obj_t)
																																	BgL_xz00_472);
																																SET_CDR
																																	(BgL_tmpz00_2239,
																																	BgL_arg1956z00_790);
																															}
																														}
																														return BgL_xz00_472;
																													}
																												else
																													{	/* Expand/eps.scm 386 */
																														if (
																															(CAR(
																																	((obj_t)
																																		BgL_xz00_472))
																																==
																																CNST_TABLE_REF
																																(17)))
																															{	/* Expand/eps.scm 386 */
																																goto
																																	BgL_tagzd2425zd2_497;
																															}
																														else
																															{	/* Expand/eps.scm 386 */
																																if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(CAR(((obj_t) BgL_xz00_472)), BUNSPEC))
																																	{	/* Expand/eps.scm 386 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_xz00_472))))
																																			{	/* Expand/eps.scm 386 */
																																				return
																																					BgL_xz00_472;
																																			}
																																		else
																																			{	/* Expand/eps.scm 386 */
																																				if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																					{	/* Expand/eps.scm 386 */
																																						BGL_TAIL
																																							return
																																							BGl_expandza2ze70z45zzexpand_epsz00
																																							(BgL_xz00_472,
																																							BgL_sz00_473);
																																					}
																																				else
																																					{	/* Expand/eps.scm 386 */
																																					BgL_tagzd2429zd2_503:
																																						{	/* Expand/eps.scm 370 */
																																							bool_t
																																								BgL_test2146z00_2258;
																																							{	/* Expand/eps.scm 370 */
																																								bool_t
																																									BgL_test2147z00_2259;
																																								{	/* Expand/eps.scm 370 */
																																									obj_t
																																										BgL_tmpz00_2260;
																																									BgL_tmpz00_2260
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_xz00_472));
																																									BgL_test2147z00_2259
																																										=
																																										SYMBOLP
																																										(BgL_tmpz00_2260);
																																								}
																																								if (BgL_test2147z00_2259)
																																									{	/* Expand/eps.scm 370 */
																																										obj_t
																																											BgL_arg1976z00_821;
																																										{	/* Expand/eps.scm 370 */
																																											obj_t
																																												BgL_arg1977z00_822;
																																											BgL_arg1977z00_822
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_xz00_472));
																																											BgL_arg1976z00_821
																																												=
																																												BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																												(BgL_arg1977z00_822,
																																												BFALSE);
																																										}
																																										BgL_test2146z00_2258
																																											=
																																											(BgL_arg1976z00_821
																																											==
																																											CNST_TABLE_REF
																																											(10));
																																									}
																																								else
																																									{	/* Expand/eps.scm 370 */
																																										BgL_test2146z00_2258
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																							if (BgL_test2146z00_2258)
																																								{
																																									obj_t
																																										BgL_sz00_2270;
																																									obj_t
																																										BgL_xz00_2269;
																																									BgL_xz00_2269
																																										=
																																										BgL_xz00_472;
																																									BgL_sz00_2270
																																										=
																																										BgL_sz00_473;
																																									BgL_sz00_464
																																										=
																																										BgL_sz00_2270;
																																									BgL_xz00_463
																																										=
																																										BgL_xz00_2269;
																																									goto
																																										BgL_zc3z04anonymousza31566ze3z87_465;
																																								}
																																							else
																																								{	/* Expand/eps.scm 372 */
																																									obj_t
																																										BgL_nxz00_802;
																																									BgL_nxz00_802
																																										=
																																										BGl_expandza2ze70z45zzexpand_epsz00
																																										(BgL_xz00_472,
																																										BgL_sz00_473);
																																									{	/* Expand/eps.scm 373 */
																																										bool_t
																																											BgL_test2148z00_2272;
																																										{	/* Expand/eps.scm 373 */
																																											obj_t
																																												BgL_tmpz00_2273;
																																											BgL_tmpz00_2273
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_nxz00_802));
																																											BgL_test2148z00_2272
																																												=
																																												SYMBOLP
																																												(BgL_tmpz00_2273);
																																										}
																																										if (BgL_test2148z00_2272)
																																											{	/* Expand/eps.scm 374 */
																																												obj_t
																																													BgL_idz00_805;
																																												{	/* Expand/eps.scm 374 */
																																													obj_t
																																														BgL_arg1974z00_818;
																																													BgL_arg1974z00_818
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_nxz00_802));
																																													BgL_idz00_805
																																														=
																																														BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																														(BgL_arg1974z00_818,
																																														BFALSE);
																																												}
																																												{	/* Expand/eps.scm 374 */

																																													{	/* Expand/eps.scm 375 */
																																														bool_t
																																															BgL_test2149z00_2280;
																																														{	/* Expand/eps.scm 375 */
																																															obj_t
																																																BgL_tmpz00_2281;
																																															BgL_tmpz00_2281
																																																=
																																																BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																																(BgL_idz00_805,
																																																BgL_sz00_473);
																																															BgL_test2149z00_2280
																																																=
																																																PAIRP
																																																(BgL_tmpz00_2281);
																																														}
																																														if (BgL_test2149z00_2280)
																																															{	/* Expand/eps.scm 375 */
																																																return
																																																	BgL_nxz00_802;
																																															}
																																														else
																																															{	/* Expand/eps.scm 377 */
																																																obj_t
																																																	BgL_bz00_808;
																																																{	/* Expand/eps.scm 377 */
																																																	obj_t
																																																		BgL__ortest_1090z00_816;
																																																	BgL__ortest_1090z00_816
																																																		=
																																																		BGl_findzd2Ozd2expanderz00zzexpand_expanderz00
																																																		(BgL_idz00_805);
																																																	if (CBOOL(BgL__ortest_1090z00_816))
																																																		{	/* Expand/eps.scm 377 */
																																																			BgL_bz00_808
																																																				=
																																																				BgL__ortest_1090z00_816;
																																																		}
																																																	else
																																																		{	/* Expand/eps.scm 377 */
																																																			BgL_bz00_808
																																																				=
																																																				BGl_findzd2Gzd2expanderz00zzexpand_expanderz00
																																																				(BgL_idz00_805);
																																																		}
																																																}
																																																if (CBOOL(BgL_bz00_808))
																																																	{	/* Expand/eps.scm 380 */
																																																		obj_t
																																																			BgL_ez00_809;
																																																		BgL_ez00_809
																																																			=
																																																			STRUCT_REF
																																																			(
																																																			((obj_t) BgL_bz00_808), (int) (1L));
																																																		{	/* Expand/eps.scm 382 */
																																																			obj_t
																																																				BgL_e2z00_1376;
																																																			BgL_e2z00_1376
																																																				=
																																																				MAKE_FX_PROCEDURE
																																																				(BGl_z62e2z62zzexpand_epsz00,
																																																				(int)
																																																				(2L),
																																																				(int)
																																																				(1L));
																																																			PROCEDURE_SET
																																																				(BgL_e2z00_1376,
																																																				(int)
																																																				(0L),
																																																				BgL_sz00_473);
																																																			{	/* Expand/eps.scm 381 */

																																																				return
																																																					BGL_PROCEDURE_CALL2
																																																					(BgL_ez00_809,
																																																					BgL_xz00_472,
																																																					BgL_e2z00_1376);
																																																			}
																																																		}
																																																	}
																																																else
																																																	{	/* Expand/eps.scm 379 */
																																																		return
																																																			BgL_nxz00_802;
																																																	}
																																															}
																																													}
																																												}
																																											}
																																										else
																																											{	/* Expand/eps.scm 373 */
																																												return
																																													BgL_nxz00_802;
																																											}
																																									}
																																								}
																																						}
																																					}
																																			}
																																	}
																																else
																																	{	/* Expand/eps.scm 386 */
																																		if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																			{	/* Expand/eps.scm 386 */
																																				BGL_TAIL
																																					return
																																					BGl_expandza2ze70z45zzexpand_epsz00
																																					(BgL_xz00_472,
																																					BgL_sz00_473);
																																			}
																																		else
																																			{	/* Expand/eps.scm 386 */
																																				goto
																																					BgL_tagzd2429zd2_503;
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																				else
																					{	/* Expand/eps.scm 386 */
																						if (
																							(BgL_carzd2474zd2_517 ==
																								CNST_TABLE_REF(19)))
																							{	/* Expand/eps.scm 386 */
																								goto BgL_kapzd2476zd2_519;
																							}
																						else
																							{	/* Expand/eps.scm 386 */
																								if (
																									(BgL_carzd2474zd2_517 ==
																										CNST_TABLE_REF(20)))
																									{	/* Expand/eps.scm 386 */
																										goto BgL_kapzd2476zd2_519;
																									}
																								else
																									{	/* Expand/eps.scm 386 */
																										if (
																											(BgL_carzd2474zd2_517 ==
																												CNST_TABLE_REF(21)))
																											{	/* Expand/eps.scm 386 */
																												goto
																													BgL_kapzd2476zd2_519;
																											}
																										else
																											{	/* Expand/eps.scm 386 */
																												if (
																													(CAR(
																															((obj_t)
																																BgL_xz00_472))
																														==
																														CNST_TABLE_REF(22)))
																													{	/* Expand/eps.scm 386 */
																														if (PAIRP
																															(BgL_cdrzd2475zd2_518))
																															{	/* Expand/eps.scm 386 */
																																obj_t
																																	BgL_carzd2564zd2_524;
																																obj_t
																																	BgL_cdrzd2565zd2_525;
																																BgL_carzd2564zd2_524
																																	=
																																	CAR
																																	(BgL_cdrzd2475zd2_518);
																																BgL_cdrzd2565zd2_525
																																	=
																																	CDR
																																	(BgL_cdrzd2475zd2_518);
																																if (SYMBOLP
																																	(BgL_carzd2564zd2_524))
																																	{	/* Expand/eps.scm 386 */
																																		if (PAIRP
																																			(BgL_cdrzd2565zd2_525))
																																			{	/* Expand/eps.scm 386 */
																																				BgL_loopz00_480
																																					=
																																					BgL_carzd2564zd2_524;
																																				BgL_bindingsz00_481
																																					=
																																					CAR
																																					(BgL_cdrzd2565zd2_525);
																																				BgL_bodyz00_482
																																					=
																																					CDR
																																					(BgL_cdrzd2565zd2_525);
																																				{	/* Expand/eps.scm 295 */
																																					obj_t
																																						BgL_idz00_671;
																																					BgL_idz00_671
																																						=
																																						BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																						(BgL_loopz00_480,
																																						BFALSE);
																																					{	/* Expand/eps.scm 295 */
																																						obj_t
																																							BgL_framez00_672;
																																						{	/* Expand/eps.scm 296 */
																																							obj_t
																																								BgL_arg1889z00_686;
																																							if (NULLP(BgL_bindingsz00_481))
																																								{	/* Expand/eps.scm 296 */
																																									BgL_arg1889z00_686
																																										=
																																										BNIL;
																																								}
																																							else
																																								{	/* Expand/eps.scm 296 */
																																									obj_t
																																										BgL_head1122z00_689;
																																									BgL_head1122z00_689
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BNIL,
																																										BNIL);
																																									{
																																										obj_t
																																											BgL_l1120z00_691;
																																										obj_t
																																											BgL_tail1123z00_692;
																																										BgL_l1120z00_691
																																											=
																																											BgL_bindingsz00_481;
																																										BgL_tail1123z00_692
																																											=
																																											BgL_head1122z00_689;
																																									BgL_zc3z04anonymousza31891ze3z87_693:
																																										if (NULLP(BgL_l1120z00_691))
																																											{	/* Expand/eps.scm 296 */
																																												BgL_arg1889z00_686
																																													=
																																													CDR
																																													(BgL_head1122z00_689);
																																											}
																																										else
																																											{	/* Expand/eps.scm 296 */
																																												obj_t
																																													BgL_newtail1124z00_695;
																																												{	/* Expand/eps.scm 296 */
																																													obj_t
																																														BgL_arg1894z00_697;
																																													{	/* Expand/eps.scm 296 */
																																														obj_t
																																															BgL_bz00_698;
																																														BgL_bz00_698
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_l1120z00_691));
																																														{	/* Expand/eps.scm 298 */
																																															bool_t
																																																BgL_test2162z00_2337;
																																															if (PAIRP(BgL_bz00_698))
																																																{	/* Expand/eps.scm 298 */
																																																	obj_t
																																																		BgL_tmpz00_2340;
																																																	BgL_tmpz00_2340
																																																		=
																																																		CAR
																																																		(BgL_bz00_698);
																																																	BgL_test2162z00_2337
																																																		=
																																																		SYMBOLP
																																																		(BgL_tmpz00_2340);
																																																}
																																															else
																																																{	/* Expand/eps.scm 298 */
																																																	BgL_test2162z00_2337
																																																		=
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																															if (BgL_test2162z00_2337)
																																																{	/* Expand/eps.scm 298 */
																																																	BgL_arg1894z00_697
																																																		=
																																																		BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																		(CAR
																																																		(BgL_bz00_698),
																																																		BFALSE);
																																																}
																																															else
																																																{	/* Expand/eps.scm 298 */
																																																	if (SYMBOLP(BgL_bz00_698))
																																																		{	/* Expand/eps.scm 300 */
																																																			BgL_arg1894z00_697
																																																				=
																																																				BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																				(BgL_bz00_698,
																																																				BFALSE);
																																																		}
																																																	else
																																																		{	/* Expand/eps.scm 300 */
																																																			BgL_arg1894z00_697
																																																				=
																																																				BGl_errorz00zz__errorz00
																																																				(BGl_string2019z00zzexpand_epsz00,
																																																				BGl_string2016z00zzexpand_epsz00,
																																																				BgL_xz00_472);
																																																		}
																																																}
																																														}
																																													}
																																													BgL_newtail1124z00_695
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1894z00_697,
																																														BNIL);
																																												}
																																												SET_CDR
																																													(BgL_tail1123z00_692,
																																													BgL_newtail1124z00_695);
																																												{	/* Expand/eps.scm 296 */
																																													obj_t
																																														BgL_arg1893z00_696;
																																													BgL_arg1893z00_696
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_l1120z00_691));
																																													{
																																														obj_t
																																															BgL_tail1123z00_2354;
																																														obj_t
																																															BgL_l1120z00_2353;
																																														BgL_l1120z00_2353
																																															=
																																															BgL_arg1893z00_696;
																																														BgL_tail1123z00_2354
																																															=
																																															BgL_newtail1124z00_695;
																																														BgL_tail1123z00_692
																																															=
																																															BgL_tail1123z00_2354;
																																														BgL_l1120z00_691
																																															=
																																															BgL_l1120z00_2353;
																																														goto
																																															BgL_zc3z04anonymousza31891ze3z87_693;
																																													}
																																												}
																																											}
																																									}
																																								}
																																							BgL_framez00_672
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_idz00_671,
																																								BgL_arg1889z00_686);
																																						}
																																						{	/* Expand/eps.scm 296 */
																																							obj_t
																																								BgL_nsz00_673;
																																							BgL_nsz00_673
																																								=
																																								BGl_appendzd221011zd2zzexpand_epsz00
																																								(BgL_framez00_672,
																																								BgL_sz00_473);
																																							{	/* Expand/eps.scm 305 */

																																								{
																																									obj_t
																																										BgL_l1125z00_675;
																																									BgL_l1125z00_675
																																										=
																																										BgL_bindingsz00_481;
																																								BgL_zc3z04anonymousza31880ze3z87_676:
																																									if (PAIRP(BgL_l1125z00_675))
																																										{	/* Expand/eps.scm 306 */
																																											{	/* Expand/eps.scm 307 */
																																												obj_t
																																													BgL_bz00_678;
																																												BgL_bz00_678
																																													=
																																													CAR
																																													(BgL_l1125z00_675);
																																												if (PAIRP(BgL_bz00_678))
																																													{	/* Expand/eps.scm 308 */
																																														obj_t
																																															BgL_tmpz00_2362;
																																														BgL_tmpz00_2362
																																															=
																																															BGl_expandza2ze70z45zzexpand_epsz00
																																															(CDR
																																															(BgL_bz00_678),
																																															BgL_nsz00_673);
																																														SET_CDR
																																															(BgL_bz00_678,
																																															BgL_tmpz00_2362);
																																													}
																																												else
																																													{	/* Expand/eps.scm 307 */
																																														BFALSE;
																																													}
																																											}
																																											{
																																												obj_t
																																													BgL_l1125z00_2366;
																																												BgL_l1125z00_2366
																																													=
																																													CDR
																																													(BgL_l1125z00_675);
																																												BgL_l1125z00_675
																																													=
																																													BgL_l1125z00_2366;
																																												goto
																																													BgL_zc3z04anonymousza31880ze3z87_676;
																																											}
																																										}
																																									else
																																										{	/* Expand/eps.scm 306 */
																																											((bool_t) 1);
																																										}
																																								}
																																								{	/* Expand/eps.scm 310 */
																																									obj_t
																																										BgL_arg1887z00_684;
																																									obj_t
																																										BgL_arg1888z00_685;
																																									{	/* Expand/eps.scm 310 */
																																										obj_t
																																											BgL_pairz00_1159;
																																										BgL_pairz00_1159
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_xz00_472));
																																										BgL_arg1887z00_684
																																											=
																																											CDR
																																											(BgL_pairz00_1159);
																																									}
																																									BgL_arg1888z00_685
																																										=
																																										BGl_expandza2ze70z45zzexpand_epsz00
																																										(BgL_bodyz00_482,
																																										BgL_nsz00_673);
																																									{	/* Expand/eps.scm 310 */
																																										obj_t
																																											BgL_tmpz00_2372;
																																										BgL_tmpz00_2372
																																											=
																																											(
																																											(obj_t)
																																											BgL_arg1887z00_684);
																																										SET_CDR
																																											(BgL_tmpz00_2372,
																																											BgL_arg1888z00_685);
																																									}
																																								}
																																								return
																																									BgL_xz00_472;
																																							}
																																						}
																																					}
																																				}
																																			}
																																		else
																																			{	/* Expand/eps.scm 386 */
																																				obj_t
																																					BgL_cdrzd2584zd2_530;
																																				BgL_cdrzd2584zd2_530
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_xz00_472));
																																				{	/* Expand/eps.scm 386 */
																																					obj_t
																																						BgL_arg1625z00_531;
																																					obj_t
																																						BgL_arg1626z00_532;
																																					BgL_arg1625z00_531
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2584zd2_530));
																																					BgL_arg1626z00_532
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd2584zd2_530));
																																					BgL_bindingsz00_484
																																						=
																																						BgL_arg1625z00_531;
																																					BgL_bodyz00_485
																																						=
																																						BgL_arg1626z00_532;
																																				BgL_tagzd2419zd2_486:
																																					{	/* Expand/eps.scm 313 */
																																						obj_t
																																							BgL_framez00_707;
																																						if (NULLP(BgL_bindingsz00_484))
																																							{	/* Expand/eps.scm 313 */
																																								BgL_framez00_707
																																									=
																																									BNIL;
																																							}
																																						else
																																							{	/* Expand/eps.scm 313 */
																																								obj_t
																																									BgL_head1129z00_723;
																																								BgL_head1129z00_723
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BNIL,
																																									BNIL);
																																								{
																																									obj_t
																																										BgL_l1127z00_725;
																																									obj_t
																																										BgL_tail1130z00_726;
																																									BgL_l1127z00_725
																																										=
																																										BgL_bindingsz00_484;
																																									BgL_tail1130z00_726
																																										=
																																										BgL_head1129z00_723;
																																								BgL_zc3z04anonymousza31915ze3z87_727:
																																									if (NULLP(BgL_l1127z00_725))
																																										{	/* Expand/eps.scm 313 */
																																											BgL_framez00_707
																																												=
																																												CDR
																																												(BgL_head1129z00_723);
																																										}
																																									else
																																										{	/* Expand/eps.scm 313 */
																																											obj_t
																																												BgL_newtail1131z00_729;
																																											{	/* Expand/eps.scm 313 */
																																												obj_t
																																													BgL_arg1918z00_731;
																																												{	/* Expand/eps.scm 313 */
																																													obj_t
																																														BgL_bz00_732;
																																													BgL_bz00_732
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_l1127z00_725));
																																													{	/* Expand/eps.scm 315 */
																																														bool_t
																																															BgL_test2169z00_2391;
																																														if (PAIRP(BgL_bz00_732))
																																															{	/* Expand/eps.scm 315 */
																																																obj_t
																																																	BgL_tmpz00_2394;
																																																BgL_tmpz00_2394
																																																	=
																																																	CAR
																																																	(BgL_bz00_732);
																																																BgL_test2169z00_2391
																																																	=
																																																	SYMBOLP
																																																	(BgL_tmpz00_2394);
																																															}
																																														else
																																															{	/* Expand/eps.scm 315 */
																																																BgL_test2169z00_2391
																																																	=
																																																	(
																																																	(bool_t)
																																																	0);
																																															}
																																														if (BgL_test2169z00_2391)
																																															{	/* Expand/eps.scm 315 */
																																																BgL_arg1918z00_731
																																																	=
																																																	BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																	(CAR
																																																	(BgL_bz00_732),
																																																	BFALSE);
																																															}
																																														else
																																															{	/* Expand/eps.scm 315 */
																																																if (SYMBOLP(BgL_bz00_732))
																																																	{	/* Expand/eps.scm 317 */
																																																		BgL_arg1918z00_731
																																																			=
																																																			BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																			(BgL_bz00_732,
																																																			BFALSE);
																																																	}
																																																else
																																																	{	/* Expand/eps.scm 320 */
																																																		obj_t
																																																			BgL_arg1925z00_738;
																																																		BgL_arg1925z00_738
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_xz00_472));
																																																		BgL_arg1918z00_731
																																																			=
																																																			BGl_errorz00zz__errorz00
																																																			(BgL_arg1925z00_738,
																																																			BGl_string2016z00zzexpand_epsz00,
																																																			BgL_xz00_472);
																																																	}
																																															}
																																													}
																																												}
																																												BgL_newtail1131z00_729
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1918z00_731,
																																													BNIL);
																																											}
																																											SET_CDR
																																												(BgL_tail1130z00_726,
																																												BgL_newtail1131z00_729);
																																											{	/* Expand/eps.scm 313 */
																																												obj_t
																																													BgL_arg1917z00_730;
																																												BgL_arg1917z00_730
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_l1127z00_725));
																																												{
																																													obj_t
																																														BgL_tail1130z00_2410;
																																													obj_t
																																														BgL_l1127z00_2409;
																																													BgL_l1127z00_2409
																																														=
																																														BgL_arg1917z00_730;
																																													BgL_tail1130z00_2410
																																														=
																																														BgL_newtail1131z00_729;
																																													BgL_tail1130z00_726
																																														=
																																														BgL_tail1130z00_2410;
																																													BgL_l1127z00_725
																																														=
																																														BgL_l1127z00_2409;
																																													goto
																																														BgL_zc3z04anonymousza31915ze3z87_727;
																																												}
																																											}
																																										}
																																								}
																																							}
																																						{	/* Expand/eps.scm 313 */
																																							obj_t
																																								BgL_nsz00_708;
																																							BgL_nsz00_708
																																								=
																																								BGl_appendzd221011zd2zzexpand_epsz00
																																								(BgL_framez00_707,
																																								BgL_sz00_473);
																																							{	/* Expand/eps.scm 322 */

																																								{
																																									obj_t
																																										BgL_l1132z00_710;
																																									BgL_l1132z00_710
																																										=
																																										BgL_bindingsz00_484;
																																								BgL_zc3z04anonymousza31902ze3z87_711:
																																									if (PAIRP(BgL_l1132z00_710))
																																										{	/* Expand/eps.scm 323 */
																																											{	/* Expand/eps.scm 324 */
																																												obj_t
																																													BgL_bz00_713;
																																												BgL_bz00_713
																																													=
																																													CAR
																																													(BgL_l1132z00_710);
																																												if (PAIRP(BgL_bz00_713))
																																													{	/* Expand/eps.scm 325 */
																																														obj_t
																																															BgL_tmpz00_2417;
																																														BgL_tmpz00_2417
																																															=
																																															BGl_expandza2ze70z45zzexpand_epsz00
																																															(CDR
																																															(BgL_bz00_713),
																																															BgL_nsz00_708);
																																														SET_CDR
																																															(BgL_bz00_713,
																																															BgL_tmpz00_2417);
																																													}
																																												else
																																													{	/* Expand/eps.scm 324 */
																																														BFALSE;
																																													}
																																											}
																																											{
																																												obj_t
																																													BgL_l1132z00_2421;
																																												BgL_l1132z00_2421
																																													=
																																													CDR
																																													(BgL_l1132z00_710);
																																												BgL_l1132z00_710
																																													=
																																													BgL_l1132z00_2421;
																																												goto
																																													BgL_zc3z04anonymousza31902ze3z87_711;
																																											}
																																										}
																																									else
																																										{	/* Expand/eps.scm 323 */
																																											((bool_t) 1);
																																										}
																																								}
																																								{	/* Expand/eps.scm 327 */
																																									obj_t
																																										BgL_arg1912z00_719;
																																									obj_t
																																										BgL_arg1913z00_720;
																																									BgL_arg1912z00_719
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_xz00_472));
																																									BgL_arg1913z00_720
																																										=
																																										BGl_expandza2ze70z45zzexpand_epsz00
																																										(BgL_bodyz00_485,
																																										BgL_nsz00_708);
																																									{	/* Expand/eps.scm 327 */
																																										obj_t
																																											BgL_tmpz00_2426;
																																										BgL_tmpz00_2426
																																											=
																																											(
																																											(obj_t)
																																											BgL_arg1912z00_719);
																																										SET_CDR
																																											(BgL_tmpz00_2426,
																																											BgL_arg1913z00_720);
																																									}
																																								}
																																								return
																																									BgL_xz00_472;
																																							}
																																						}
																																					}
																																				}
																																			}
																																	}
																																else
																																	{	/* Expand/eps.scm 386 */
																																		obj_t
																																			BgL_cdrzd2603zd2_533;
																																		BgL_cdrzd2603zd2_533
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_472));
																																		{	/* Expand/eps.scm 386 */
																																			obj_t
																																				BgL_arg1627z00_534;
																																			obj_t
																																				BgL_arg1629z00_535;
																																			BgL_arg1627z00_534
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2603zd2_533));
																																			BgL_arg1629z00_535
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd2603zd2_533));
																																			{
																																				obj_t
																																					BgL_bodyz00_2436;
																																				obj_t
																																					BgL_bindingsz00_2435;
																																				BgL_bindingsz00_2435
																																					=
																																					BgL_arg1627z00_534;
																																				BgL_bodyz00_2436
																																					=
																																					BgL_arg1629z00_535;
																																				BgL_bodyz00_485
																																					=
																																					BgL_bodyz00_2436;
																																				BgL_bindingsz00_484
																																					=
																																					BgL_bindingsz00_2435;
																																				goto
																																					BgL_tagzd2419zd2_486;
																																			}
																																		}
																																	}
																															}
																														else
																															{	/* Expand/eps.scm 386 */
																																if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																	{	/* Expand/eps.scm 386 */
																																		BGL_TAIL
																																			return
																																			BGl_expandza2ze70z45zzexpand_epsz00
																																			(BgL_xz00_472,
																																			BgL_sz00_473);
																																	}
																																else
																																	{	/* Expand/eps.scm 386 */
																																		goto
																																			BgL_tagzd2429zd2_503;
																																	}
																															}
																													}
																												else
																													{	/* Expand/eps.scm 386 */
																														obj_t
																															BgL_carzd2664zd2_537;
																														obj_t
																															BgL_cdrzd2665zd2_538;
																														BgL_carzd2664zd2_537
																															=
																															CAR(((obj_t)
																																BgL_xz00_472));
																														BgL_cdrzd2665zd2_538
																															=
																															CDR(((obj_t)
																																BgL_xz00_472));
																														{

																															if (
																																(BgL_carzd2664zd2_537
																																	==
																																	CNST_TABLE_REF
																																	(23)))
																																{	/* Expand/eps.scm 386 */
																																BgL_kapzd2666zd2_539:
																																	if (PAIRP
																																		(BgL_cdrzd2665zd2_538))
																																		{
																																			obj_t
																																				BgL_bodyz00_2451;
																																			obj_t
																																				BgL_bindingsz00_2449;
																																			BgL_bindingsz00_2449
																																				=
																																				CAR
																																				(BgL_cdrzd2665zd2_538);
																																			BgL_bodyz00_2451
																																				=
																																				CDR
																																				(BgL_cdrzd2665zd2_538);
																																			BgL_bodyz00_485
																																				=
																																				BgL_bodyz00_2451;
																																			BgL_bindingsz00_484
																																				=
																																				BgL_bindingsz00_2449;
																																			goto
																																				BgL_tagzd2419zd2_486;
																																		}
																																	else
																																		{	/* Expand/eps.scm 386 */
																																			if (
																																				(CAR(
																																						((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(10)))
																																				{
																																					obj_t
																																						BgL_sz00_2459;
																																					obj_t
																																						BgL_xz00_2458;
																																					BgL_xz00_2458
																																						=
																																						BgL_xz00_472;
																																					BgL_sz00_2459
																																						=
																																						BgL_sz00_473;
																																					BgL_sz00_464
																																						=
																																						BgL_sz00_2459;
																																					BgL_xz00_463
																																						=
																																						BgL_xz00_2458;
																																					goto
																																						BgL_zc3z04anonymousza31566ze3z87_465;
																																				}
																																			else
																																				{	/* Expand/eps.scm 386 */
																																					if (
																																						(CAR
																																							(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(15)))
																																						{	/* Expand/eps.scm 386 */
																																							goto
																																								BgL_tagzd2423zd2_493;
																																						}
																																					else
																																						{	/* Expand/eps.scm 386 */
																																							if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(16)))
																																								{	/* Expand/eps.scm 386 */
																																									goto
																																										BgL_tagzd2425zd2_497;
																																								}
																																							else
																																								{	/* Expand/eps.scm 386 */
																																									if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(17)))
																																										{	/* Expand/eps.scm 386 */
																																											goto
																																												BgL_tagzd2425zd2_497;
																																										}
																																									else
																																										{	/* Expand/eps.scm 386 */
																																											if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(CAR(((obj_t) BgL_xz00_472)), BUNSPEC))
																																												{	/* Expand/eps.scm 386 */
																																													if (NULLP(CDR(((obj_t) BgL_xz00_472))))
																																														{	/* Expand/eps.scm 386 */
																																															return
																																																BgL_xz00_472;
																																														}
																																													else
																																														{	/* Expand/eps.scm 386 */
																																															if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																																{	/* Expand/eps.scm 386 */
																																																	BGL_TAIL
																																																		return
																																																		BGl_expandza2ze70z45zzexpand_epsz00
																																																		(BgL_xz00_472,
																																																		BgL_sz00_473);
																																																}
																																															else
																																																{	/* Expand/eps.scm 386 */
																																																	goto
																																																		BgL_tagzd2429zd2_503;
																																																}
																																														}
																																												}
																																											else
																																												{	/* Expand/eps.scm 386 */
																																													if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																														{	/* Expand/eps.scm 386 */
																																															BGL_TAIL
																																																return
																																																BGl_expandza2ze70z45zzexpand_epsz00
																																																(BgL_xz00_472,
																																																BgL_sz00_473);
																																														}
																																													else
																																														{	/* Expand/eps.scm 386 */
																																															goto
																																																BgL_tagzd2429zd2_503;
																																														}
																																												}
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																															else
																																{	/* Expand/eps.scm 386 */
																																	if (
																																		(BgL_carzd2664zd2_537
																																			==
																																			CNST_TABLE_REF
																																			(24)))
																																		{	/* Expand/eps.scm 386 */
																																			goto
																																				BgL_kapzd2666zd2_539;
																																		}
																																	else
																																		{	/* Expand/eps.scm 386 */
																																			if (
																																				(BgL_carzd2664zd2_537
																																					==
																																					CNST_TABLE_REF
																																					(25)))
																																				{	/* Expand/eps.scm 386 */
																																					goto
																																						BgL_kapzd2666zd2_539;
																																				}
																																			else
																																				{	/* Expand/eps.scm 386 */
																																					if (
																																						(CAR
																																							(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(26)))
																																						{	/* Expand/eps.scm 386 */
																																							if (PAIRP(BgL_cdrzd2665zd2_538))
																																								{	/* Expand/eps.scm 386 */
																																									BgL_bindingsz00_487
																																										=
																																										CAR
																																										(BgL_cdrzd2665zd2_538);
																																									BgL_bodyz00_488
																																										=
																																										CDR
																																										(BgL_cdrzd2665zd2_538);
																																									{	/* Expand/eps.scm 330 */
																																										obj_t
																																											BgL_framez00_742;
																																										if (NULLP(BgL_bindingsz00_487))
																																											{	/* Expand/eps.scm 330 */
																																												BgL_framez00_742
																																													=
																																													BNIL;
																																											}
																																										else
																																											{	/* Expand/eps.scm 330 */
																																												obj_t
																																													BgL_head1136z00_761;
																																												BgL_head1136z00_761
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BNIL,
																																													BNIL);
																																												{
																																													obj_t
																																														BgL_l1134z00_763;
																																													obj_t
																																														BgL_tail1137z00_764;
																																													BgL_l1134z00_763
																																														=
																																														BgL_bindingsz00_487;
																																													BgL_tail1137z00_764
																																														=
																																														BgL_head1136z00_761;
																																												BgL_zc3z04anonymousza31938ze3z87_765:
																																													if (NULLP(BgL_l1134z00_763))
																																														{	/* Expand/eps.scm 330 */
																																															BgL_framez00_742
																																																=
																																																CDR
																																																(BgL_head1136z00_761);
																																														}
																																													else
																																														{	/* Expand/eps.scm 330 */
																																															obj_t
																																																BgL_newtail1138z00_767;
																																															{	/* Expand/eps.scm 330 */
																																																obj_t
																																																	BgL_arg1941z00_769;
																																																{	/* Expand/eps.scm 330 */
																																																	obj_t
																																																		BgL_bz00_770;
																																																	BgL_bz00_770
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_l1134z00_763));
																																																	if (PAIRP(BgL_bz00_770))
																																																		{	/* Expand/eps.scm 331 */
																																																			BgL_arg1941z00_769
																																																				=
																																																				BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																				(CAR
																																																				(BgL_bz00_770),
																																																				BFALSE);
																																																		}
																																																	else
																																																		{	/* Expand/eps.scm 331 */
																																																			BgL_arg1941z00_769
																																																				=
																																																				BGl_errorz00zz__errorz00
																																																				(BGl_string2018z00zzexpand_epsz00,
																																																				BGl_string2016z00zzexpand_epsz00,
																																																				BgL_xz00_472);
																																																		}
																																																}
																																																BgL_newtail1138z00_767
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg1941z00_769,
																																																	BNIL);
																																															}
																																															SET_CDR
																																																(BgL_tail1137z00_764,
																																																BgL_newtail1138z00_767);
																																															{	/* Expand/eps.scm 330 */
																																																obj_t
																																																	BgL_arg1940z00_768;
																																																BgL_arg1940z00_768
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_l1134z00_763));
																																																{
																																																	obj_t
																																																		BgL_tail1137z00_2520;
																																																	obj_t
																																																		BgL_l1134z00_2519;
																																																	BgL_l1134z00_2519
																																																		=
																																																		BgL_arg1940z00_768;
																																																	BgL_tail1137z00_2520
																																																		=
																																																		BgL_newtail1138z00_767;
																																																	BgL_tail1137z00_764
																																																		=
																																																		BgL_tail1137z00_2520;
																																																	BgL_l1134z00_763
																																																		=
																																																		BgL_l1134z00_2519;
																																																	goto
																																																		BgL_zc3z04anonymousza31938ze3z87_765;
																																																}
																																															}
																																														}
																																												}
																																											}
																																										{	/* Expand/eps.scm 330 */
																																											obj_t
																																												BgL_nsz00_743;
																																											BgL_nsz00_743
																																												=
																																												BGl_appendzd221011zd2zzexpand_epsz00
																																												(BgL_framez00_742,
																																												BgL_sz00_473);
																																											{	/* Expand/eps.scm 335 */

																																												{
																																													obj_t
																																														BgL_l1139z00_745;
																																													BgL_l1139z00_745
																																														=
																																														BgL_bindingsz00_487;
																																												BgL_zc3z04anonymousza31927ze3z87_746:
																																													if (PAIRP(BgL_l1139z00_745))
																																														{	/* Expand/eps.scm 336 */
																																															{	/* Expand/eps.scm 337 */
																																																obj_t
																																																	BgL_bz00_748;
																																																BgL_bz00_748
																																																	=
																																																	CAR
																																																	(BgL_l1139z00_745);
																																																{	/* Expand/eps.scm 337 */
																																																	obj_t
																																																		BgL_fz00_749;
																																																	{	/* Expand/eps.scm 337 */
																																																		obj_t
																																																			BgL_arg1933z00_754;
																																																		{	/* Expand/eps.scm 337 */
																																																			obj_t
																																																				BgL_pairz00_1183;
																																																			BgL_pairz00_1183
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_bz00_748));
																																																			BgL_arg1933z00_754
																																																				=
																																																				CAR
																																																				(BgL_pairz00_1183);
																																																		}
																																																		BgL_fz00_749
																																																			=
																																																			BGl_protozd2ze3frameze70zd6zzexpand_epsz00
																																																			(BgL_arg1933z00_754);
																																																	}
																																																	{	/* Expand/eps.scm 338 */
																																																		obj_t
																																																			BgL_arg1929z00_750;
																																																		obj_t
																																																			BgL_arg1930z00_751;
																																																		BgL_arg1929z00_750
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_bz00_748));
																																																		{	/* Expand/eps.scm 339 */
																																																			obj_t
																																																				BgL_arg1931z00_752;
																																																			obj_t
																																																				BgL_arg1932z00_753;
																																																			{	/* Expand/eps.scm 339 */
																																																				obj_t
																																																					BgL_pairz00_1188;
																																																				BgL_pairz00_1188
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_bz00_748));
																																																				BgL_arg1931z00_752
																																																					=
																																																					CDR
																																																					(BgL_pairz00_1188);
																																																			}
																																																			BgL_arg1932z00_753
																																																				=
																																																				BGl_appendzd221011zd2zzexpand_epsz00
																																																				(BgL_fz00_749,
																																																				BgL_nsz00_743);
																																																			BgL_arg1930z00_751
																																																				=
																																																				BGl_expandza2ze70z45zzexpand_epsz00
																																																				(BgL_arg1931z00_752,
																																																				BgL_arg1932z00_753);
																																																		}
																																																		{	/* Expand/eps.scm 338 */
																																																			obj_t
																																																				BgL_tmpz00_2536;
																																																			BgL_tmpz00_2536
																																																				=
																																																				(
																																																				(obj_t)
																																																				BgL_arg1929z00_750);
																																																			SET_CDR
																																																				(BgL_tmpz00_2536,
																																																				BgL_arg1930z00_751);
																																																		}
																																																	}
																																																}
																																															}
																																															{
																																																obj_t
																																																	BgL_l1139z00_2539;
																																																BgL_l1139z00_2539
																																																	=
																																																	CDR
																																																	(BgL_l1139z00_745);
																																																BgL_l1139z00_745
																																																	=
																																																	BgL_l1139z00_2539;
																																																goto
																																																	BgL_zc3z04anonymousza31927ze3z87_746;
																																															}
																																														}
																																													else
																																														{	/* Expand/eps.scm 336 */
																																															((bool_t) 1);
																																														}
																																												}
																																												{	/* Expand/eps.scm 341 */
																																													obj_t
																																														BgL_arg1935z00_757;
																																													obj_t
																																														BgL_arg1936z00_758;
																																													BgL_arg1935z00_757
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_xz00_472));
																																													BgL_arg1936z00_758
																																														=
																																														BGl_expandza2ze70z45zzexpand_epsz00
																																														(BgL_bodyz00_488,
																																														BgL_nsz00_743);
																																													{	/* Expand/eps.scm 341 */
																																														obj_t
																																															BgL_tmpz00_2544;
																																														BgL_tmpz00_2544
																																															=
																																															(
																																															(obj_t)
																																															BgL_arg1935z00_757);
																																														SET_CDR
																																															(BgL_tmpz00_2544,
																																															BgL_arg1936z00_758);
																																													}
																																												}
																																												return
																																													BgL_xz00_472;
																																											}
																																										}
																																									}
																																								}
																																							else
																																								{	/* Expand/eps.scm 386 */
																																									if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																										{	/* Expand/eps.scm 386 */
																																											BGL_TAIL
																																												return
																																												BGl_expandza2ze70z45zzexpand_epsz00
																																												(BgL_xz00_472,
																																												BgL_sz00_473);
																																										}
																																									else
																																										{	/* Expand/eps.scm 386 */
																																											goto
																																												BgL_tagzd2429zd2_503;
																																										}
																																								}
																																						}
																																					else
																																						{	/* Expand/eps.scm 386 */
																																							if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(10)))
																																								{
																																									obj_t
																																										BgL_sz00_2558;
																																									obj_t
																																										BgL_xz00_2557;
																																									BgL_xz00_2557
																																										=
																																										BgL_xz00_472;
																																									BgL_sz00_2558
																																										=
																																										BgL_sz00_473;
																																									BgL_sz00_464
																																										=
																																										BgL_sz00_2558;
																																									BgL_xz00_463
																																										=
																																										BgL_xz00_2557;
																																									goto
																																										BgL_zc3z04anonymousza31566ze3z87_465;
																																								}
																																							else
																																								{	/* Expand/eps.scm 386 */
																																									obj_t
																																										BgL_carzd2778zd2_549;
																																									obj_t
																																										BgL_cdrzd2779zd2_550;
																																									BgL_carzd2778zd2_549
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_xz00_472));
																																									BgL_cdrzd2779zd2_550
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_xz00_472));
																																									{

																																										if ((BgL_carzd2778zd2_549 == CNST_TABLE_REF(27)))
																																											{	/* Expand/eps.scm 386 */
																																											BgL_kapzd2780zd2_551:
																																												if (PAIRP(BgL_cdrzd2779zd2_550))
																																													{	/* Expand/eps.scm 386 */
																																														BgL_bodyz00_491
																																															=
																																															CDR
																																															(BgL_cdrzd2779zd2_550);
																																														{	/* Expand/eps.scm 346 */
																																															obj_t
																																																BgL_arg1944z00_774;
																																															obj_t
																																																BgL_arg1945z00_775;
																																															BgL_arg1944z00_774
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_xz00_472));
																																															BgL_arg1945z00_775
																																																=
																																																BGl_expandza2ze70z45zzexpand_epsz00
																																																(BgL_bodyz00_491,
																																																BgL_sz00_473);
																																															{	/* Expand/eps.scm 346 */
																																																obj_t
																																																	BgL_tmpz00_2571;
																																																BgL_tmpz00_2571
																																																	=
																																																	(
																																																	(obj_t)
																																																	BgL_arg1944z00_774);
																																																SET_CDR
																																																	(BgL_tmpz00_2571,
																																																	BgL_arg1945z00_775);
																																															}
																																														}
																																														return
																																															BgL_xz00_472;
																																													}
																																												else
																																													{	/* Expand/eps.scm 386 */
																																														if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(15)))
																																															{	/* Expand/eps.scm 386 */
																																																goto
																																																	BgL_tagzd2423zd2_493;
																																															}
																																														else
																																															{	/* Expand/eps.scm 386 */
																																																if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(16)))
																																																	{	/* Expand/eps.scm 386 */
																																																		goto
																																																			BgL_tagzd2425zd2_497;
																																																	}
																																																else
																																																	{	/* Expand/eps.scm 386 */
																																																		if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(17)))
																																																			{	/* Expand/eps.scm 386 */
																																																				goto
																																																					BgL_tagzd2425zd2_497;
																																																			}
																																																		else
																																																			{	/* Expand/eps.scm 386 */
																																																				if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(CAR(((obj_t) BgL_xz00_472)), BUNSPEC))
																																																					{	/* Expand/eps.scm 386 */
																																																						if (NULLP(CDR(((obj_t) BgL_xz00_472))))
																																																							{	/* Expand/eps.scm 386 */
																																																								return
																																																									BgL_xz00_472;
																																																							}
																																																						else
																																																							{	/* Expand/eps.scm 386 */
																																																								if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																																									{	/* Expand/eps.scm 386 */
																																																										BGL_TAIL
																																																											return
																																																											BGl_expandza2ze70z45zzexpand_epsz00
																																																											(BgL_xz00_472,
																																																											BgL_sz00_473);
																																																									}
																																																								else
																																																									{	/* Expand/eps.scm 386 */
																																																										goto
																																																											BgL_tagzd2429zd2_503;
																																																									}
																																																							}
																																																					}
																																																				else
																																																					{	/* Expand/eps.scm 386 */
																																																						if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																																							{	/* Expand/eps.scm 386 */
																																																								BGL_TAIL
																																																									return
																																																									BGl_expandza2ze70z45zzexpand_epsz00
																																																									(BgL_xz00_472,
																																																									BgL_sz00_473);
																																																							}
																																																						else
																																																							{	/* Expand/eps.scm 386 */
																																																								goto
																																																									BgL_tagzd2429zd2_503;
																																																							}
																																																					}
																																																			}
																																																	}
																																															}
																																													}
																																											}
																																										else
																																											{	/* Expand/eps.scm 386 */
																																												if ((BgL_carzd2778zd2_549 == CNST_TABLE_REF(28)))
																																													{	/* Expand/eps.scm 386 */
																																														goto
																																															BgL_kapzd2780zd2_551;
																																													}
																																												else
																																													{	/* Expand/eps.scm 386 */
																																														if ((BgL_carzd2778zd2_549 == CNST_TABLE_REF(29)))
																																															{	/* Expand/eps.scm 386 */
																																																goto
																																																	BgL_kapzd2780zd2_551;
																																															}
																																														else
																																															{	/* Expand/eps.scm 386 */
																																																if ((BgL_carzd2778zd2_549 == CNST_TABLE_REF(30)))
																																																	{	/* Expand/eps.scm 386 */
																																																		goto
																																																			BgL_kapzd2780zd2_551;
																																																	}
																																																else
																																																	{	/* Expand/eps.scm 386 */
																																																		if ((BgL_carzd2778zd2_549 == CNST_TABLE_REF(15)))
																																																			{	/* Expand/eps.scm 386 */
																																																				goto
																																																					BgL_tagzd2423zd2_493;
																																																			}
																																																		else
																																																			{	/* Expand/eps.scm 386 */
																																																				obj_t
																																																					BgL_cdrzd2825zd2_554;
																																																				BgL_cdrzd2825zd2_554
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_xz00_472));
																																																				if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(31)))
																																																					{	/* Expand/eps.scm 386 */
																																																						if (PAIRP(BgL_cdrzd2825zd2_554))
																																																							{	/* Expand/eps.scm 386 */
																																																								BgL_varz00_494
																																																									=
																																																									CAR
																																																									(BgL_cdrzd2825zd2_554);
																																																								BgL_clausesz00_495
																																																									=
																																																									CDR
																																																									(BgL_cdrzd2825zd2_554);
																																																								{	/* Expand/eps.scm 352 */
																																																									obj_t
																																																										BgL_arg1948z00_778;
																																																									obj_t
																																																										BgL_arg1949z00_779;
																																																									BgL_arg1948z00_778
																																																										=
																																																										CDR
																																																										(
																																																										((obj_t) BgL_xz00_472));
																																																									BgL_arg1949z00_779
																																																										=
																																																										BGl_expandze70ze7zzexpand_epsz00
																																																										(BgL_varz00_494,
																																																										BgL_sz00_473);
																																																									{	/* Expand/eps.scm 352 */
																																																										obj_t
																																																											BgL_tmpz00_2628;
																																																										BgL_tmpz00_2628
																																																											=
																																																											(
																																																											(obj_t)
																																																											BgL_arg1948z00_778);
																																																										SET_CAR
																																																											(BgL_tmpz00_2628,
																																																											BgL_arg1949z00_779);
																																																									}
																																																								}
																																																								{
																																																									obj_t
																																																										BgL_l1141z00_781;
																																																									BgL_l1141z00_781
																																																										=
																																																										BgL_clausesz00_495;
																																																								BgL_zc3z04anonymousza31950ze3z87_782:
																																																									if (PAIRP(BgL_l1141z00_781))
																																																										{	/* Expand/eps.scm 353 */
																																																											{	/* Expand/eps.scm 354 */
																																																												obj_t
																																																													BgL_clausez00_784;
																																																												BgL_clausez00_784
																																																													=
																																																													CAR
																																																													(BgL_l1141z00_781);
																																																												if (PAIRP(BgL_clausez00_784))
																																																													{	/* Expand/eps.scm 355 */
																																																														obj_t
																																																															BgL_tmpz00_2636;
																																																														BgL_tmpz00_2636
																																																															=
																																																															BGl_expandza2ze70z45zzexpand_epsz00
																																																															(CDR
																																																															(BgL_clausez00_784),
																																																															BgL_sz00_473);
																																																														SET_CDR
																																																															(BgL_clausez00_784,
																																																															BgL_tmpz00_2636);
																																																													}
																																																												else
																																																													{	/* Expand/eps.scm 354 */
																																																														BFALSE;
																																																													}
																																																											}
																																																											{
																																																												obj_t
																																																													BgL_l1141z00_2640;
																																																												BgL_l1141z00_2640
																																																													=
																																																													CDR
																																																													(BgL_l1141z00_781);
																																																												BgL_l1141z00_781
																																																													=
																																																													BgL_l1141z00_2640;
																																																												goto
																																																													BgL_zc3z04anonymousza31950ze3z87_782;
																																																											}
																																																										}
																																																									else
																																																										{	/* Expand/eps.scm 353 */
																																																											((bool_t) 1);
																																																										}
																																																								}
																																																								return
																																																									BgL_xz00_472;
																																																							}
																																																						else
																																																							{	/* Expand/eps.scm 386 */
																																																								if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																																									{	/* Expand/eps.scm 386 */
																																																										BGL_TAIL
																																																											return
																																																											BGl_expandza2ze70z45zzexpand_epsz00
																																																											(BgL_xz00_472,
																																																											BgL_sz00_473);
																																																									}
																																																								else
																																																									{	/* Expand/eps.scm 386 */
																																																										goto
																																																											BgL_tagzd2429zd2_503;
																																																									}
																																																							}
																																																					}
																																																				else
																																																					{	/* Expand/eps.scm 386 */
																																																						if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(16)))
																																																							{	/* Expand/eps.scm 386 */
																																																								goto
																																																									BgL_tagzd2425zd2_497;
																																																							}
																																																						else
																																																							{	/* Expand/eps.scm 386 */
																																																								if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(17)))
																																																									{	/* Expand/eps.scm 386 */
																																																										goto
																																																											BgL_tagzd2425zd2_497;
																																																									}
																																																								else
																																																									{	/* Expand/eps.scm 386 */
																																																										if ((CAR(((obj_t) BgL_xz00_472)) == CNST_TABLE_REF(32)))
																																																											{	/* Expand/eps.scm 386 */
																																																												if (PAIRP(BgL_cdrzd2825zd2_554))
																																																													{	/* Expand/eps.scm 386 */
																																																														obj_t
																																																															BgL_cdrzd2855zd2_569;
																																																														BgL_cdrzd2855zd2_569
																																																															=
																																																															CDR
																																																															(BgL_cdrzd2825zd2_554);
																																																														if (PAIRP(BgL_cdrzd2855zd2_569))
																																																															{	/* Expand/eps.scm 386 */
																																																																if (NULLP(CDR(BgL_cdrzd2855zd2_569)))
																																																																	{	/* Expand/eps.scm 386 */
																																																																		BgL_funz00_498
																																																																			=
																																																																			CAR
																																																																			(BgL_cdrzd2825zd2_554);
																																																																		BgL_argz00_499
																																																																			=
																																																																			CAR
																																																																			(BgL_cdrzd2855zd2_569);
																																																																		{	/* Expand/eps.scm 362 */
																																																																			obj_t
																																																																				BgL_arg1958z00_792;
																																																																			obj_t
																																																																				BgL_arg1959z00_793;
																																																																			BgL_arg1958z00_792
																																																																				=
																																																																				CDR
																																																																				(
																																																																				((obj_t) BgL_xz00_472));
																																																																			BgL_arg1959z00_793
																																																																				=
																																																																				BGl_expandze70ze7zzexpand_epsz00
																																																																				(BgL_funz00_498,
																																																																				BgL_sz00_473);
																																																																			{	/* Expand/eps.scm 362 */
																																																																				obj_t
																																																																					BgL_tmpz00_2673;
																																																																				BgL_tmpz00_2673
																																																																					=
																																																																					(
																																																																					(obj_t)
																																																																					BgL_arg1958z00_792);
																																																																				SET_CAR
																																																																					(BgL_tmpz00_2673,
																																																																					BgL_arg1959z00_793);
																																																																			}
																																																																		}
																																																																		{	/* Expand/eps.scm 363 */
																																																																			obj_t
																																																																				BgL_arg1960z00_794;
																																																																			obj_t
																																																																				BgL_arg1961z00_795;
																																																																			{	/* Expand/eps.scm 363 */
																																																																				obj_t
																																																																					BgL_pairz00_1210;
																																																																				BgL_pairz00_1210
																																																																					=
																																																																					CDR
																																																																					(
																																																																					((obj_t) BgL_xz00_472));
																																																																				BgL_arg1960z00_794
																																																																					=
																																																																					CDR
																																																																					(BgL_pairz00_1210);
																																																																			}
																																																																			BgL_arg1961z00_795
																																																																				=
																																																																				BGl_expandze70ze7zzexpand_epsz00
																																																																				(BgL_argz00_499,
																																																																				BgL_sz00_473);
																																																																			{	/* Expand/eps.scm 363 */
																																																																				obj_t
																																																																					BgL_tmpz00_2680;
																																																																				BgL_tmpz00_2680
																																																																					=
																																																																					(
																																																																					(obj_t)
																																																																					BgL_arg1960z00_794);
																																																																				SET_CAR
																																																																					(BgL_tmpz00_2680,
																																																																					BgL_arg1961z00_795);
																																																																			}
																																																																		}
																																																																		return
																																																																			BgL_xz00_472;
																																																																	}
																																																																else
																																																																	{	/* Expand/eps.scm 386 */
																																																																		if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																																																			{	/* Expand/eps.scm 386 */
																																																																				BGL_TAIL
																																																																					return
																																																																					BGl_expandza2ze70z45zzexpand_epsz00
																																																																					(BgL_xz00_472,
																																																																					BgL_sz00_473);
																																																																			}
																																																																		else
																																																																			{	/* Expand/eps.scm 386 */
																																																																				goto
																																																																					BgL_tagzd2429zd2_503;
																																																																			}
																																																																	}
																																																															}
																																																														else
																																																															{	/* Expand/eps.scm 386 */
																																																																if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																																																	{	/* Expand/eps.scm 386 */
																																																																		BGL_TAIL
																																																																			return
																																																																			BGl_expandza2ze70z45zzexpand_epsz00
																																																																			(BgL_xz00_472,
																																																																			BgL_sz00_473);
																																																																	}
																																																																else
																																																																	{	/* Expand/eps.scm 386 */
																																																																		goto
																																																																			BgL_tagzd2429zd2_503;
																																																																	}
																																																															}
																																																													}
																																																												else
																																																													{	/* Expand/eps.scm 386 */
																																																														if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																																															{	/* Expand/eps.scm 386 */
																																																																BGL_TAIL
																																																																	return
																																																																	BGl_expandza2ze70z45zzexpand_epsz00
																																																																	(BgL_xz00_472,
																																																																	BgL_sz00_473);
																																																															}
																																																														else
																																																															{	/* Expand/eps.scm 386 */
																																																																goto
																																																																	BgL_tagzd2429zd2_503;
																																																															}
																																																													}
																																																											}
																																																										else
																																																											{	/* Expand/eps.scm 386 */
																																																												if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(CAR(((obj_t) BgL_xz00_472)), BUNSPEC))
																																																													{	/* Expand/eps.scm 386 */
																																																														if (NULLP(CDR(((obj_t) BgL_xz00_472))))
																																																															{	/* Expand/eps.scm 386 */
																																																																return
																																																																	BgL_xz00_472;
																																																															}
																																																														else
																																																															{	/* Expand/eps.scm 386 */
																																																																if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																																																	{	/* Expand/eps.scm 386 */
																																																																		BGL_TAIL
																																																																			return
																																																																			BGl_expandza2ze70z45zzexpand_epsz00
																																																																			(BgL_xz00_472,
																																																																			BgL_sz00_473);
																																																																	}
																																																																else
																																																																	{	/* Expand/eps.scm 386 */
																																																																		goto
																																																																			BgL_tagzd2429zd2_503;
																																																																	}
																																																															}
																																																													}
																																																												else
																																																													{	/* Expand/eps.scm 386 */
																																																														if (BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_xz00_472))
																																																															{	/* Expand/eps.scm 386 */
																																																																BGL_TAIL
																																																																	return
																																																																	BGl_expandza2ze70z45zzexpand_epsz00
																																																																	(BgL_xz00_472,
																																																																	BgL_sz00_473);
																																																															}
																																																														else
																																																															{	/* Expand/eps.scm 386 */
																																																																goto
																																																																	BgL_tagzd2429zd2_503;
																																																															}
																																																													}
																																																											}
																																																									}
																																																							}
																																																					}
																																																			}
																																																	}
																																															}
																																													}
																																											}
																																									}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																													}
																											}
																									}
																							}
																					}
																			}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Expand/eps.scm 386 */
									return BgL_xz00_472;
								}
						}
				}
			}
		}

	}



/* expand*~0 */
	obj_t BGl_expandza2ze70z45zzexpand_epsz00(obj_t BgL_xz00_451,
		obj_t BgL_sz00_452)
	{
		{	/* Expand/eps.scm 276 */
			{
				obj_t BgL_l1118z00_456;

				BgL_l1118z00_456 = BgL_xz00_451;
			BgL_zc3z04anonymousza31561ze3z87_457:
				if (NULLP(BgL_l1118z00_456))
					{	/* Expand/eps.scm 276 */
						return BgL_xz00_451;
					}
				else
					{	/* Expand/eps.scm 276 */
						{	/* Expand/eps.scm 276 */
							obj_t BgL_arg1564z00_459;

							{	/* Expand/eps.scm 276 */
								obj_t BgL_xz00_460;

								BgL_xz00_460 = CAR(((obj_t) BgL_l1118z00_456));
								BgL_arg1564z00_459 =
									BGl_expandze70ze7zzexpand_epsz00(BgL_xz00_460, BgL_sz00_452);
							}
							{	/* Expand/eps.scm 276 */
								obj_t BgL_tmpz00_2713;

								BgL_tmpz00_2713 = ((obj_t) BgL_l1118z00_456);
								SET_CAR(BgL_tmpz00_2713, BgL_arg1564z00_459);
							}
						}
						{	/* Expand/eps.scm 276 */
							obj_t BgL_arg1565z00_461;

							BgL_arg1565z00_461 = CDR(((obj_t) BgL_l1118z00_456));
							{
								obj_t BgL_l1118z00_2718;

								BgL_l1118z00_2718 = BgL_arg1565z00_461;
								BgL_l1118z00_456 = BgL_l1118z00_2718;
								goto BgL_zc3z04anonymousza31561ze3z87_457;
							}
						}
					}
			}
		}

	}



/* &e2 */
	obj_t BGl_z62e2z62zzexpand_epsz00(obj_t BgL_envz00_1377, obj_t BgL_xz00_1379,
		obj_t BgL_ez00_1380)
	{
		{	/* Expand/eps.scm 381 */
			{	/* Expand/eps.scm 382 */
				obj_t BgL_sz00_1378;

				BgL_sz00_1378 = PROCEDURE_REF(BgL_envz00_1377, (int) (0L));
				{	/* Expand/eps.scm 383 */
					obj_t BgL_xz00_1503;

					BgL_xz00_1503 =
						BGl_initialzd2expanderzd2zzexpand_epsz00(BgL_xz00_1379,
						BGl_initialzd2expanderzd2envz00zzexpand_epsz00);
					return BGl_expandze70ze7zzexpand_epsz00(BgL_xz00_1503,
						((obj_t) BgL_sz00_1378));
				}
			}
		}

	}



/* &identifier-expander */
	obj_t BGl_z62identifierzd2expanderzb0zzexpand_epsz00(obj_t BgL_envz00_1370,
		obj_t BgL_idz00_1371, obj_t BgL_ez00_1372)
	{
		{	/* Expand/eps.scm 392 */
			{	/* Expand/eps.scm 393 */
				bool_t BgL_test2227z00_2724;

				{	/* Expand/eps.scm 393 */
					obj_t BgL_tmpz00_2725;

					BgL_tmpz00_2725 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_1371,
						BGl_za2lexicalzd2stackza2zd2zzexpand_epsz00);
					BgL_test2227z00_2724 = PAIRP(BgL_tmpz00_2725);
				}
				if (BgL_test2227z00_2724)
					{	/* Expand/eps.scm 393 */
						return BgL_idz00_1371;
					}
				else
					{	/* Expand/eps.scm 395 */
						obj_t BgL_az00_1506;

						BgL_az00_1506 =
							BGl_getpropz00zz__r4_symbols_6_4z00(BgL_idz00_1371,
							CNST_TABLE_REF(0));
						if (CBOOL(BgL_az00_1506))
							{	/* Expand/eps.scm 396 */
								return
									BGL_PROCEDURE_CALL2(BgL_ez00_1372, BgL_az00_1506,
									BgL_ez00_1372);
							}
						else
							{	/* Expand/eps.scm 396 */
								return BgL_idz00_1371;
							}
					}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzexpand_epsz00(obj_t BgL_ez00_1382, obj_t BgL_xz00_1381,
		obj_t BgL_xza2za2_836)
	{
		{	/* Expand/eps.scm 404 */
			if (PAIRP(BgL_xza2za2_836))
				{	/* Expand/eps.scm 406 */
					{	/* Expand/eps.scm 407 */
						obj_t BgL_arg1986z00_839;

						{	/* Expand/eps.scm 407 */
							obj_t BgL_arg1987z00_840;

							BgL_arg1987z00_840 = CAR(BgL_xza2za2_836);
							BgL_arg1986z00_839 =
								BGL_PROCEDURE_CALL2(BgL_ez00_1382, BgL_arg1987z00_840,
								BgL_ez00_1382);
						}
						SET_CAR(BgL_xza2za2_836, BgL_arg1986z00_839);
					}
					{	/* Expand/eps.scm 408 */
						obj_t BgL_tmpz00_2746;

						BgL_tmpz00_2746 =
							BGl_loopze70ze7zzexpand_epsz00(BgL_ez00_1382, BgL_xz00_1381,
							CDR(BgL_xza2za2_836));
						SET_CDR(BgL_xza2za2_836, BgL_tmpz00_2746);
					}
					return BgL_xza2za2_836;
				}
			else
				{	/* Expand/eps.scm 406 */
					if (NULLP(BgL_xza2za2_836))
						{	/* Expand/eps.scm 410 */
							return BNIL;
						}
					else
						{	/* Expand/eps.scm 410 */
							BGL_TAIL return
								BGl_errorz00zz__errorz00(BFALSE,
								BGl_string2020z00zzexpand_epsz00, BgL_xz00_1381);
						}
				}
		}

	}



/* &application-expander */
	obj_t BGl_z62applicationzd2expanderzb0zzexpand_epsz00(obj_t BgL_envz00_1373,
		obj_t BgL_xz00_1374, obj_t BgL_ez00_1375)
	{
		{	/* Expand/eps.scm 403 */
			return
				BGl_loopze70ze7zzexpand_epsz00(BgL_ez00_1375, BgL_xz00_1374,
				BgL_xz00_1374);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_epsz00(void)
	{
		{	/* Expand/eps.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_epsz00(void)
	{
		{	/* Expand/eps.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_epsz00(void)
	{
		{	/* Expand/eps.scm 17 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_epsz00(void)
	{
		{	/* Expand/eps.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zzexpand_expanderz00(393376L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
			return
				BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string2021z00zzexpand_epsz00));
		}

	}

#ifdef __cplusplus
}
#endif
