/*===========================================================================*/
/*   (Expand/object.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/object.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_OBJECT_TYPE_DEFINITIONS
#define BGL_EXPAND_OBJECT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;


#endif													// BGL_EXPAND_OBJECT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_instantiatezd2ze3makez31zzexpand_objectz00(obj_t,
		BgL_typez00_bglt, obj_t);
	extern obj_t
		BGl_classgenzd2allocatezd2exprz00zzobject_classgenz00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31595ze3ze5zzexpand_objectz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_objectz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_duplicatezd2ze3makez31zzexpand_objectz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern bool_t BGl_widezd2classzf3z21zzobject_classz00(obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_declarezd2classz12zc0zzmodule_classz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_objectz00(void);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	static obj_t BGl_z62expandzd2widenz12za2zzexpand_objectz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_objectz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_findzd2classzd2slotz00zzobject_toolsz00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62expandzd2withzd2accessz62zzexpand_objectz00(obj_t, obj_t,
		obj_t);
	extern obj_t
		BGl_classgenzd2widenzd2exprz00zzobject_classgenz00(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_slotz00zzobject_slotsz00;
	BGL_IMPORT obj_t make_vector(long, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_objectz00(void);
	static obj_t BGl_z62expandzd2shrinkz12za2zzexpand_objectz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2duplicatezd2zzexpand_objectz00(obj_t,
		obj_t);
	extern obj_t BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(obj_t);
	static obj_t BGl_inlinablezd2callzf3ze70zc6zzexpand_objectz00(obj_t, obj_t);
	static obj_t BGl_instantiatezd2fillzd2zzexpand_objectz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_expandzd2wideningzd2zzexpand_objectz00(obj_t,
		BgL_typez00_bglt, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2instantiatezd2zzexpand_objectz00(obj_t,
		obj_t);
	static obj_t BGl_cozd2instantiatezd2ze3letze3zzexpand_objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_lexicalzd2stackzd2zzexpand_epsz00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32309ze3ze5zzexpand_objectz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2shrinkz12zc0zzexpand_objectz00(obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2definezd2classz62zzexpand_objectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2duplicatezb0zzexpand_objectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31362ze3ze5zzexpand_objectz00(obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	static obj_t BGl_objectzd2epairifyzd2zzexpand_objectz00(obj_t, obj_t);
	extern obj_t BGl_replacez12z12zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2widenz12zc0zzexpand_objectz00(obj_t,
		obj_t);
	static obj_t BGl_makezd2azd2shrinkz12z12zzexpand_objectz00(obj_t, obj_t);
	extern bool_t BGl_slotzd2defaultzf3z21zzobject_slotsz00(BgL_slotz00_bglt);
	BGL_IMPORT obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzexpand_objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classgenz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_fieldzd2accesszd2zzast_objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_findzd2slotzd2offsetz00zzexpand_objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_withzd2lexicalzd2zzexpand_epsz00(obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern bool_t BGl_slotzd2virtualzf3z21zzobject_slotsz00(BgL_slotz00_bglt);
	extern BgL_typez00_bglt BGl_typezd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32280ze3ze5zzexpand_objectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_allocatezd2exprzd2zzexpand_objectz00(obj_t);
	extern obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2warningzd2defaultzd2slotzd2valueza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2cozd2instantiatez00zzexpand_objectz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_objectz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_objectz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	extern obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_expandzd2withzd2accessz00zzexpand_objectz00(obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_objectz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_objectz00(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_parsezd2prototypezd2zzmodule_prototypez00(obj_t);
	static obj_t BGl_withzd2accesszd2expanderz00zzexpand_objectz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_inlinezd2definitionzd2queuez00zzread_inlinez00(void);
	static bool_t BGl_inlinablezf3ze70z14zzexpand_objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2definezd2classz00zzexpand_objectz00(obj_t, obj_t);
	extern obj_t
		BGl_classzd2finaliza7erzd2addzd2staticz12z67zzmodule_classz00(void);
	static obj_t BGl_z62expandzd2cozd2instantiatez62zzexpand_objectz00(obj_t,
		obj_t, obj_t);
	extern obj_t
		BGl_findzd2classzd2constructorz00zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62slotzd2setzb0zzexpand_objectz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzexpand_objectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzexpand_objectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_IMPORT obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t BGl_z62expandzd2instantiatezb0zzexpand_objectz00(obj_t, obj_t,
		obj_t);
	static obj_t __cnst[46];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2definezd2classzd2envzd2zzexpand_objectz00,
		BgL_bgl_za762expandza7d2defi2691z00,
		BGl_z62expandzd2definezd2classz62zzexpand_objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2shrinkz12zd2envz12zzexpand_objectz00,
		BgL_bgl_za762expandza7d2shri2692z00,
		BGl_z62expandzd2shrinkz12za2zzexpand_objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2widenz12zd2envz12zzexpand_objectz00,
		BgL_bgl_za762expandza7d2wide2693z00,
		BGl_z62expandzd2widenz12za2zzexpand_objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2duplicatezd2envz00zzexpand_objectz00,
		BgL_bgl_za762expandza7d2dupl2694z00,
		BGl_z62expandzd2duplicatezb0zzexpand_objectz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2665z00zzexpand_objectz00,
		BgL_bgl_string2665za700za7za7e2695za7, "Illegal define-class", 20);
	      DEFINE_STRING(BGl_string2666z00zzexpand_objectz00,
		BgL_bgl_string2666za700za7za7e2696za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string2667z00zzexpand_objectz00,
		BgL_bgl_string2667za700za7za7e2697za7, "Illegal field", 13);
	      DEFINE_STRING(BGl_string2668z00zzexpand_objectz00,
		BgL_bgl_string2668za700za7za7e2698za7, "Illegal class", 13);
	      DEFINE_STRING(BGl_string2669z00zzexpand_objectz00,
		BgL_bgl_string2669za700za7za7e2699za7, "with-access", 11);
	      DEFINE_STRING(BGl_string2670z00zzexpand_objectz00,
		BgL_bgl_string2670za700za7za7e2700za7, "No such field", 13);
	      DEFINE_STRING(BGl_string2671z00zzexpand_objectz00,
		BgL_bgl_string2671za700za7za7e2701za7,
		"Abstract classes can't be instantiated", 38);
	      DEFINE_STRING(BGl_string2672z00zzexpand_objectz00,
		BgL_bgl_string2672za700za7za7e2702za7, "Cannot find class definition", 28);
	      DEFINE_STRING(BGl_string2673z00zzexpand_objectz00,
		BgL_bgl_string2673za700za7za7e2703za7, "instantiate", 11);
	      DEFINE_STRING(BGl_string2674z00zzexpand_objectz00,
		BgL_bgl_string2674za700za7za7e2704za7, "Illegal argument \"~a\"", 21);
	      DEFINE_STRING(BGl_string2675z00zzexpand_objectz00,
		BgL_bgl_string2675za700za7za7e2705za7,
		"Cannot inline slot \"~s\" default value", 37);
	      DEFINE_STRING(BGl_string2676z00zzexpand_objectz00,
		BgL_bgl_string2676za700za7za7e2706za7, "Missing value for field \"~a\"",
		28);
	      DEFINE_STRING(BGl_string2677z00zzexpand_objectz00,
		BgL_bgl_string2677za700za7za7e2707za7, "co-instantiate", 14);
	      DEFINE_STRING(BGl_string2678z00zzexpand_objectz00,
		BgL_bgl_string2678za700za7za7e2708za7, "Illegal binding", 15);
	      DEFINE_STRING(BGl_string2679z00zzexpand_objectz00,
		BgL_bgl_string2679za700za7za7e2709za7, "Illegal variable type", 21);
	      DEFINE_STRING(BGl_string2680z00zzexpand_objectz00,
		BgL_bgl_string2680za700za7za7e2710za7,
		"Abstract classes can't be duplicated", 36);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2instantiatezd2envz00zzexpand_objectz00,
		BgL_bgl_za762expandza7d2inst2711z00,
		BGl_z62expandzd2instantiatezb0zzexpand_objectz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2681z00zzexpand_objectz00,
		BgL_bgl_string2681za700za7za7e2712za7, "Illegal class type \"~a\"", 23);
	      DEFINE_STRING(BGl_string2682z00zzexpand_objectz00,
		BgL_bgl_string2682za700za7za7e2713za7, "duplicate", 9);
	      DEFINE_STRING(BGl_string2683z00zzexpand_objectz00,
		BgL_bgl_string2683za700za7za7e2714za7, "wident!", 7);
	      DEFINE_STRING(BGl_string2684z00zzexpand_objectz00,
		BgL_bgl_string2684za700za7za7e2715za7, "Illegal shrink!", 15);
	      DEFINE_STRING(BGl_string2685z00zzexpand_objectz00,
		BgL_bgl_string2685za700za7za7e2716za7, "shrink!", 7);
	      DEFINE_STRING(BGl_string2686z00zzexpand_objectz00,
		BgL_bgl_string2686za700za7za7e2717za7, "Not a wide object", 17);
	      DEFINE_STRING(BGl_string2687z00zzexpand_objectz00,
		BgL_bgl_string2687za700za7za7e2718za7, "Field unknown \"~a\"", 18);
	      DEFINE_STRING(BGl_string2688z00zzexpand_objectz00,
		BgL_bgl_string2688za700za7za7e2719za7, "expand_object", 13);
	      DEFINE_STRING(BGl_string2689z00zzexpand_objectz00,
		BgL_bgl_string2689za700za7za7e2720za7,
		"if error object-widening object? object-widening-set! object-class class-super class-num object-class-num-set! o shrink! cast duplicated tmp instantiate co-instantiate bit-rsh bit-lsh bit-xor bit-and bit-or - + class-nil lambda begin quote sifun default-inline no-literal define-inline class-constructor new class-field-default-value vector-ref-ur @ class-all-fields __object set! let i define-class static define-abstract-class define-final-class class ",
		454);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2cozd2instantiatezd2envzd2zzexpand_objectz00,
		BgL_bgl_za762expandza7d2coza7d2721za7,
		BGl_z62expandzd2cozd2instantiatez62zzexpand_objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2withzd2accesszd2envzd2zzexpand_objectz00,
		BgL_bgl_za762expandza7d2with2722z00,
		BGl_z62expandzd2withzd2accessz62zzexpand_objectz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzexpand_objectz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzexpand_objectz00(long
		BgL_checksumz00_4274, char *BgL_fromz00_4275)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_objectz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_objectz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_objectz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_objectz00();
					BGl_cnstzd2initzd2zzexpand_objectz00();
					BGl_importedzd2moduleszd2initz00zzexpand_objectz00();
					return BGl_methodzd2initzd2zzexpand_objectz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_objectz00(void)
	{
		{	/* Expand/object.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_object");
			BGl_modulezd2initializa7ationz75zz__prognz00(0L, "expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_object");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"expand_object");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_objectz00(void)
	{
		{	/* Expand/object.scm 15 */
			{	/* Expand/object.scm 15 */
				obj_t BgL_cportz00_4196;

				{	/* Expand/object.scm 15 */
					obj_t BgL_stringz00_4203;

					BgL_stringz00_4203 = BGl_string2689z00zzexpand_objectz00;
					{	/* Expand/object.scm 15 */
						obj_t BgL_startz00_4204;

						BgL_startz00_4204 = BINT(0L);
						{	/* Expand/object.scm 15 */
							obj_t BgL_endz00_4205;

							BgL_endz00_4205 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4203)));
							{	/* Expand/object.scm 15 */

								BgL_cportz00_4196 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4203, BgL_startz00_4204, BgL_endz00_4205);
				}}}}
				{
					long BgL_iz00_4197;

					BgL_iz00_4197 = 45L;
				BgL_loopz00_4198:
					if ((BgL_iz00_4197 == -1L))
						{	/* Expand/object.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/object.scm 15 */
							{	/* Expand/object.scm 15 */
								obj_t BgL_arg2690z00_4199;

								{	/* Expand/object.scm 15 */

									{	/* Expand/object.scm 15 */
										obj_t BgL_locationz00_4201;

										BgL_locationz00_4201 = BBOOL(((bool_t) 0));
										{	/* Expand/object.scm 15 */

											BgL_arg2690z00_4199 =
												BGl_readz00zz__readerz00(BgL_cportz00_4196,
												BgL_locationz00_4201);
										}
									}
								}
								{	/* Expand/object.scm 15 */
									int BgL_tmpz00_4310;

									BgL_tmpz00_4310 = (int) (BgL_iz00_4197);
									CNST_TABLE_SET(BgL_tmpz00_4310, BgL_arg2690z00_4199);
							}}
							{	/* Expand/object.scm 15 */
								int BgL_auxz00_4202;

								BgL_auxz00_4202 = (int) ((BgL_iz00_4197 - 1L));
								{
									long BgL_iz00_4315;

									BgL_iz00_4315 = (long) (BgL_auxz00_4202);
									BgL_iz00_4197 = BgL_iz00_4315;
									goto BgL_loopz00_4198;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_objectz00(void)
	{
		{	/* Expand/object.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* expand-define-class */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2definezd2classz00zzexpand_objectz00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Expand/object.scm 52 */
			{
				obj_t BgL_restz00_1673;

				if (PAIRP(BgL_xz00_3))
					{	/* Expand/object.scm 53 */
						obj_t BgL_carzd2106zd2_1678;
						obj_t BgL_cdrzd2107zd2_1679;

						BgL_carzd2106zd2_1678 = CAR(((obj_t) BgL_xz00_3));
						BgL_cdrzd2107zd2_1679 = CDR(((obj_t) BgL_xz00_3));
						if ((BgL_carzd2106zd2_1678 == CNST_TABLE_REF(4)))
							{	/* Expand/object.scm 53 */
								BgL_restz00_1673 = BgL_cdrzd2107zd2_1679;
							BgL_tagzd2101zd2_1674:
								{	/* Expand/object.scm 55 */
									obj_t BgL_nxz00_1684;

									{	/* Expand/object.scm 55 */
										obj_t BgL_arg1326z00_1692;

										{	/* Expand/object.scm 55 */
											obj_t BgL_arg1327z00_1693;

											BgL_arg1327z00_1693 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_restz00_1673, BNIL);
											BgL_arg1326z00_1692 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1327z00_1693);
										}
										BgL_nxz00_1684 =
											BGl_evepairifyz00zz__prognz00(BgL_arg1326z00_1692,
											BgL_xz00_3);
									}
									{	/* Expand/object.scm 55 */
										obj_t BgL_protoz00_1685;

										BgL_protoz00_1685 =
											BGl_parsezd2prototypezd2zzmodule_prototypez00
											(BgL_nxz00_1684);
										{	/* Expand/object.scm 56 */

											if (CBOOL(BgL_protoz00_1685))
												{	/* Expand/object.scm 57 */
													{	/* Expand/object.scm 61 */
														obj_t BgL_arg1319z00_1686;
														bool_t BgL_arg1320z00_1687;
														bool_t BgL_arg1321z00_1688;

														BgL_arg1319z00_1686 =
															CDR(((obj_t) BgL_protoz00_1685));
														BgL_arg1320z00_1687 =
															(CAR(((obj_t) BgL_xz00_3)) == CNST_TABLE_REF(1));
														BgL_arg1321z00_1688 =
															(CAR(((obj_t) BgL_xz00_3)) == CNST_TABLE_REF(2));
														BGl_declarezd2classz12zc0zzmodule_classz00
															(BgL_arg1319z00_1686,
															BGl_za2moduleza2z00zzmodule_modulez00,
															CNST_TABLE_REF(3), BgL_arg1320z00_1687,
															BgL_arg1321z00_1688, BgL_nxz00_1684, BFALSE);
													}
													return
														BGl_classzd2finaliza7erzd2addzd2staticz12z67zzmodule_classz00
														();
												}
											else
												{	/* Expand/object.scm 58 */
													obj_t BgL_arg1325z00_1691;

													BgL_arg1325z00_1691 = CAR(((obj_t) BgL_xz00_3));
													return
														BGl_errorz00zz__errorz00(BgL_arg1325z00_1691,
														BGl_string2665z00zzexpand_objectz00, BgL_xz00_3);
												}
										}
									}
								}
							}
						else
							{	/* Expand/object.scm 53 */
								if ((BgL_carzd2106zd2_1678 == CNST_TABLE_REF(1)))
									{
										obj_t BgL_restz00_4353;

										BgL_restz00_4353 = BgL_cdrzd2107zd2_1679;
										BgL_restz00_1673 = BgL_restz00_4353;
										goto BgL_tagzd2101zd2_1674;
									}
								else
									{	/* Expand/object.scm 53 */
										if ((BgL_carzd2106zd2_1678 == CNST_TABLE_REF(2)))
											{
												obj_t BgL_restz00_4357;

												BgL_restz00_4357 = BgL_cdrzd2107zd2_1679;
												BgL_restz00_1673 = BgL_restz00_4357;
												goto BgL_tagzd2101zd2_1674;
											}
										else
											{	/* Expand/object.scm 53 */
											BgL_tagzd2102zd2_1675:
												return
													BGl_errorz00zz__errorz00(BgL_xz00_3,
													BGl_string2665z00zzexpand_objectz00, BgL_xz00_3);
											}
									}
							}
					}
				else
					{	/* Expand/object.scm 53 */
						goto BgL_tagzd2102zd2_1675;
					}
			}
		}

	}



/* &expand-define-class */
	obj_t BGl_z62expandzd2definezd2classz62zzexpand_objectz00(obj_t
		BgL_envz00_4128, obj_t BgL_xz00_4129, obj_t BgL_ez00_4130)
	{
		{	/* Expand/object.scm 52 */
			return
				BGl_expandzd2definezd2classz00zzexpand_objectz00(BgL_xz00_4129,
				BgL_ez00_4130);
		}

	}



/* expand-with-access */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2withzd2accessz00zzexpand_objectz00(obj_t
		BgL_xz00_5, obj_t BgL_ez00_6)
	{
		{	/* Expand/object.scm 71 */
			{
				obj_t BgL_withzd2accesszd2_1694;
				obj_t BgL_instancez00_1695;
				obj_t BgL_slotsz00_1696;
				obj_t BgL_bodyz00_1697;

				if (PAIRP(BgL_xz00_5))
					{	/* Expand/object.scm 72 */
						obj_t BgL_cdrzd2123zd2_1702;

						BgL_cdrzd2123zd2_1702 = CDR(((obj_t) BgL_xz00_5));
						if (PAIRP(BgL_cdrzd2123zd2_1702))
							{	/* Expand/object.scm 72 */
								obj_t BgL_cdrzd2129zd2_1704;

								BgL_cdrzd2129zd2_1704 = CDR(BgL_cdrzd2123zd2_1702);
								if (PAIRP(BgL_cdrzd2129zd2_1704))
									{	/* Expand/object.scm 72 */
										obj_t BgL_carzd2133zd2_1706;
										obj_t BgL_cdrzd2134zd2_1707;

										BgL_carzd2133zd2_1706 = CAR(BgL_cdrzd2129zd2_1704);
										BgL_cdrzd2134zd2_1707 = CDR(BgL_cdrzd2129zd2_1704);
										if (PAIRP(BgL_carzd2133zd2_1706))
											{	/* Expand/object.scm 72 */
												if (PAIRP(BgL_cdrzd2134zd2_1707))
													{	/* Expand/object.scm 72 */
														obj_t BgL_arg1333z00_1710;
														obj_t BgL_arg1335z00_1711;

														BgL_arg1333z00_1710 = CAR(((obj_t) BgL_xz00_5));
														BgL_arg1335z00_1711 = CAR(BgL_cdrzd2123zd2_1702);
														BgL_withzd2accesszd2_1694 = BgL_arg1333z00_1710;
														BgL_instancez00_1695 = BgL_arg1335z00_1711;
														BgL_slotsz00_1696 = BgL_carzd2133zd2_1706;
														BgL_bodyz00_1697 = BgL_cdrzd2134zd2_1707;
														{	/* Expand/object.scm 74 */
															obj_t BgL_locz00_1712;

															BgL_locz00_1712 =
																BGl_findzd2locationzd2zztools_locationz00
																(BgL_xz00_5);
															{	/* Expand/object.scm 74 */
																BgL_typez00_bglt BgL_classz00_1713;

																BgL_classz00_1713 =
																	BGl_typezd2ofzd2idz00zzast_identz00
																	(BgL_withzd2accesszd2_1694, BgL_locz00_1712);
																{	/* Expand/object.scm 75 */

																	{	/* Expand/object.scm 77 */
																		bool_t BgL_test2735z00_4380;

																		{	/* Expand/object.scm 77 */
																			obj_t BgL_classz00_3196;

																			BgL_classz00_3196 =
																				BGl_tclassz00zzobject_classz00;
																			{	/* Expand/object.scm 77 */
																				BgL_objectz00_bglt BgL_arg1807z00_3198;

																				{	/* Expand/object.scm 77 */
																					obj_t BgL_tmpz00_4381;

																					BgL_tmpz00_4381 =
																						((obj_t)
																						((BgL_objectz00_bglt)
																							BgL_classz00_1713));
																					BgL_arg1807z00_3198 =
																						(BgL_objectz00_bglt)
																						(BgL_tmpz00_4381);
																				}
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Expand/object.scm 77 */
																						long BgL_idxz00_3204;

																						BgL_idxz00_3204 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_3198);
																						BgL_test2735z00_4380 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_3204 + 2L)) ==
																							BgL_classz00_3196);
																					}
																				else
																					{	/* Expand/object.scm 77 */
																						bool_t BgL_res2646z00_3229;

																						{	/* Expand/object.scm 77 */
																							obj_t BgL_oclassz00_3212;

																							{	/* Expand/object.scm 77 */
																								obj_t BgL_arg1815z00_3220;
																								long BgL_arg1816z00_3221;

																								BgL_arg1815z00_3220 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Expand/object.scm 77 */
																									long BgL_arg1817z00_3222;

																									BgL_arg1817z00_3222 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_3198);
																									BgL_arg1816z00_3221 =
																										(BgL_arg1817z00_3222 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_3212 =
																									VECTOR_REF
																									(BgL_arg1815z00_3220,
																									BgL_arg1816z00_3221);
																							}
																							{	/* Expand/object.scm 77 */
																								bool_t BgL__ortest_1115z00_3213;

																								BgL__ortest_1115z00_3213 =
																									(BgL_classz00_3196 ==
																									BgL_oclassz00_3212);
																								if (BgL__ortest_1115z00_3213)
																									{	/* Expand/object.scm 77 */
																										BgL_res2646z00_3229 =
																											BgL__ortest_1115z00_3213;
																									}
																								else
																									{	/* Expand/object.scm 77 */
																										long BgL_odepthz00_3214;

																										{	/* Expand/object.scm 77 */
																											obj_t BgL_arg1804z00_3215;

																											BgL_arg1804z00_3215 =
																												(BgL_oclassz00_3212);
																											BgL_odepthz00_3214 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_3215);
																										}
																										if (
																											(2L < BgL_odepthz00_3214))
																											{	/* Expand/object.scm 77 */
																												obj_t
																													BgL_arg1802z00_3217;
																												{	/* Expand/object.scm 77 */
																													obj_t
																														BgL_arg1803z00_3218;
																													BgL_arg1803z00_3218 =
																														(BgL_oclassz00_3212);
																													BgL_arg1802z00_3217 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_3218,
																														2L);
																												}
																												BgL_res2646z00_3229 =
																													(BgL_arg1802z00_3217
																													== BgL_classz00_3196);
																											}
																										else
																											{	/* Expand/object.scm 77 */
																												BgL_res2646z00_3229 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2735z00_4380 =
																							BgL_res2646z00_3229;
																					}
																			}
																		}
																		if (BgL_test2735z00_4380)
																			{
																				obj_t BgL_sz00_1717;
																				obj_t BgL_nslotsz00_1718;

																				BgL_sz00_1717 = BgL_slotsz00_1696;
																				BgL_nslotsz00_1718 = BNIL;
																			BgL_zc3z04anonymousza31337ze3z87_1719:
																				if (NULLP(BgL_sz00_1717))
																					{	/* Expand/object.scm 84 */
																						obj_t BgL_auxz00_1721;

																						BgL_auxz00_1721 =
																							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																							(BGl_gensymz00zz__r4_symbols_6_4z00
																							(CNST_TABLE_REF(5)));
																						{	/* Expand/object.scm 84 */
																							obj_t BgL_instancez00_1722;

																							BgL_instancez00_1722 =
																								BGL_PROCEDURE_CALL2(BgL_ez00_6,
																								BgL_instancez00_1695,
																								BgL_ez00_6);
																							{	/* Expand/object.scm 85 */
																								obj_t BgL_classzd2idzd2_1723;

																								BgL_classzd2idzd2_1723 =
																									(((BgL_typez00_bglt)
																										COBJECT
																										(BgL_classz00_1713))->
																									BgL_idz00);
																								{	/* Expand/object.scm 86 */
																									obj_t BgL_tauxz00_1724;

																									BgL_tauxz00_1724 =
																										BGl_makezd2typedzd2identz00zzast_identz00
																										(BgL_auxz00_1721,
																										BgL_classzd2idzd2_1723);
																									{	/* Expand/object.scm 87 */

																										{	/* Expand/object.scm 90 */
																											obj_t BgL_arg1339z00_1725;

																											{	/* Expand/object.scm 90 */
																												obj_t
																													BgL_arg1340z00_1726;
																												obj_t
																													BgL_arg1342z00_1727;
																												if (NULLP
																													(BgL_nslotsz00_1718))
																													{	/* Expand/object.scm 90 */
																														BgL_arg1340z00_1726
																															= BNIL;
																													}
																												else
																													{	/* Expand/object.scm 90 */
																														obj_t
																															BgL_head1265z00_1731;
																														{	/* Expand/object.scm 90 */
																															obj_t
																																BgL_arg1352z00_1743;
																															{	/* Expand/object.scm 90 */
																																obj_t
																																	BgL_pairz00_3232;
																																BgL_pairz00_3232
																																	=
																																	CAR(((obj_t)
																																		BgL_nslotsz00_1718));
																																BgL_arg1352z00_1743
																																	=
																																	CAR
																																	(BgL_pairz00_3232);
																															}
																															BgL_head1265z00_1731
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1352z00_1743,
																																BNIL);
																														}
																														{	/* Expand/object.scm 90 */
																															obj_t
																																BgL_g1268z00_1732;
																															BgL_g1268z00_1732
																																=
																																CDR(((obj_t)
																																	BgL_nslotsz00_1718));
																															{
																																obj_t
																																	BgL_l1263z00_1734;
																																obj_t
																																	BgL_tail1266z00_1735;
																																BgL_l1263z00_1734
																																	=
																																	BgL_g1268z00_1732;
																																BgL_tail1266z00_1735
																																	=
																																	BgL_head1265z00_1731;
																															BgL_zc3z04anonymousza31345ze3z87_1736:
																																if (NULLP
																																	(BgL_l1263z00_1734))
																																	{	/* Expand/object.scm 90 */
																																		BgL_arg1340z00_1726
																																			=
																																			BgL_head1265z00_1731;
																																	}
																																else
																																	{	/* Expand/object.scm 90 */
																																		obj_t
																																			BgL_newtail1267z00_1738;
																																		{	/* Expand/object.scm 90 */
																																			obj_t
																																				BgL_arg1349z00_1740;
																																			{	/* Expand/object.scm 90 */
																																				obj_t
																																					BgL_pairz00_3235;
																																				BgL_pairz00_3235
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_l1263z00_1734));
																																				BgL_arg1349z00_1740
																																					=
																																					CAR
																																					(BgL_pairz00_3235);
																																			}
																																			BgL_newtail1267z00_1738
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1349z00_1740,
																																				BNIL);
																																		}
																																		SET_CDR
																																			(BgL_tail1266z00_1735,
																																			BgL_newtail1267z00_1738);
																																		{	/* Expand/object.scm 90 */
																																			obj_t
																																				BgL_arg1348z00_1739;
																																			BgL_arg1348z00_1739
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_l1263z00_1734));
																																			{
																																				obj_t
																																					BgL_tail1266z00_4434;
																																				obj_t
																																					BgL_l1263z00_4433;
																																				BgL_l1263z00_4433
																																					=
																																					BgL_arg1348z00_1739;
																																				BgL_tail1266z00_4434
																																					=
																																					BgL_newtail1267z00_1738;
																																				BgL_tail1266z00_1735
																																					=
																																					BgL_tail1266z00_4434;
																																				BgL_l1263z00_1734
																																					=
																																					BgL_l1263z00_4433;
																																				goto
																																					BgL_zc3z04anonymousza31345ze3z87_1736;
																																			}
																																		}
																																	}
																															}
																														}
																													}
																												BgL_arg1342z00_1727 =
																													BGl_findzd2locationzd2zztools_locationz00
																													(BgL_xz00_5);
																												{	/* Expand/object.scm 92 */
																													obj_t
																														BgL_zc3z04anonymousza31362ze3z87_4131;
																													BgL_zc3z04anonymousza31362ze3z87_4131
																														=
																														MAKE_FX_PROCEDURE
																														(BGl_z62zc3z04anonymousza31362ze3ze5zzexpand_objectz00,
																														(int) (0L),
																														(int) (8L));
																													PROCEDURE_SET
																														(BgL_zc3z04anonymousza31362ze3z87_4131,
																														(int) (0L),
																														BgL_ez00_6);
																													PROCEDURE_SET
																														(BgL_zc3z04anonymousza31362ze3z87_4131,
																														(int) (1L),
																														BgL_auxz00_1721);
																													PROCEDURE_SET
																														(BgL_zc3z04anonymousza31362ze3z87_4131,
																														(int) (2L),
																														((obj_t)
																															BgL_classz00_1713));
																													PROCEDURE_SET
																														(BgL_zc3z04anonymousza31362ze3z87_4131,
																														(int) (3L),
																														BgL_nslotsz00_1718);
																													PROCEDURE_SET
																														(BgL_zc3z04anonymousza31362ze3z87_4131,
																														(int) (4L),
																														BgL_xz00_5);
																													PROCEDURE_SET
																														(BgL_zc3z04anonymousza31362ze3z87_4131,
																														(int) (5L),
																														BgL_instancez00_1722);
																													PROCEDURE_SET
																														(BgL_zc3z04anonymousza31362ze3z87_4131,
																														(int) (6L),
																														BgL_tauxz00_1724);
																													PROCEDURE_SET
																														(BgL_zc3z04anonymousza31362ze3z87_4131,
																														(int) (7L),
																														BgL_bodyz00_1697);
																													BgL_arg1339z00_1725 =
																														BGl_withzd2lexicalzd2zzexpand_epsz00
																														(BgL_arg1340z00_1726,
																														BgL_auxz00_1721,
																														BgL_arg1342z00_1727,
																														BgL_zc3z04anonymousza31362ze3z87_4131);
																											}}
																											return
																												BGl_replacez12z12zztools_miscz00
																												(BgL_xz00_5,
																												BgL_arg1339z00_1725);
																										}
																									}
																								}
																							}
																						}
																					}
																				else
																					{	/* Expand/object.scm 83 */
																						if (PAIRP(BgL_sz00_1717))
																							{	/* Expand/object.scm 99 */
																								bool_t BgL_test2743z00_4460;

																								{	/* Expand/object.scm 99 */
																									obj_t BgL_tmpz00_4461;

																									BgL_tmpz00_4461 =
																										CAR(BgL_sz00_1717);
																									BgL_test2743z00_4460 =
																										SYMBOLP(BgL_tmpz00_4461);
																								}
																								if (BgL_test2743z00_4460)
																									{	/* Expand/object.scm 100 */
																										obj_t BgL_arg1421z00_1760;
																										obj_t BgL_arg1422z00_1761;

																										BgL_arg1421z00_1760 =
																											CDR(BgL_sz00_1717);
																										{	/* Expand/object.scm 100 */
																											obj_t BgL_arg1434z00_1762;

																											{	/* Expand/object.scm 100 */
																												obj_t
																													BgL_arg1437z00_1763;
																												obj_t
																													BgL_arg1448z00_1764;
																												BgL_arg1437z00_1763 =
																													CAR(BgL_sz00_1717);
																												BgL_arg1448z00_1764 =
																													CAR(BgL_sz00_1717);
																												{	/* Expand/object.scm 100 */
																													obj_t
																														BgL_list1449z00_1765;
																													{	/* Expand/object.scm 100 */
																														obj_t
																															BgL_arg1453z00_1766;
																														BgL_arg1453z00_1766
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1448z00_1764,
																															BNIL);
																														BgL_list1449z00_1765
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1437z00_1763,
																															BgL_arg1453z00_1766);
																													}
																													BgL_arg1434z00_1762 =
																														BgL_list1449z00_1765;
																												}
																											}
																											BgL_arg1422z00_1761 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1434z00_1762,
																												BgL_nslotsz00_1718);
																										}
																										{
																											obj_t BgL_nslotsz00_4471;
																											obj_t BgL_sz00_4470;

																											BgL_sz00_4470 =
																												BgL_arg1421z00_1760;
																											BgL_nslotsz00_4471 =
																												BgL_arg1422z00_1761;
																											BgL_nslotsz00_1718 =
																												BgL_nslotsz00_4471;
																											BgL_sz00_1717 =
																												BgL_sz00_4470;
																											goto
																												BgL_zc3z04anonymousza31337ze3z87_1719;
																										}
																									}
																								else
																									{	/* Expand/object.scm 101 */
																										bool_t BgL_test2744z00_4472;

																										{	/* Expand/object.scm 101 */
																											bool_t
																												BgL_test2745z00_4473;
																											{	/* Expand/object.scm 101 */
																												obj_t BgL_tmpz00_4474;

																												BgL_tmpz00_4474 =
																													CAR(BgL_sz00_1717);
																												BgL_test2745z00_4473 =
																													PAIRP
																													(BgL_tmpz00_4474);
																											}
																											if (BgL_test2745z00_4473)
																												{	/* Expand/object.scm 102 */
																													bool_t
																														BgL_test2746z00_4477;
																													{	/* Expand/object.scm 102 */
																														obj_t
																															BgL_tmpz00_4478;
																														BgL_tmpz00_4478 =
																															CAR(CAR
																															(BgL_sz00_1717));
																														BgL_test2746z00_4477
																															=
																															SYMBOLP
																															(BgL_tmpz00_4478);
																													}
																													if (BgL_test2746z00_4477)
																														{	/* Expand/object.scm 103 */
																															bool_t
																																BgL_test2747z00_4482;
																															{	/* Expand/object.scm 103 */
																																obj_t
																																	BgL_tmpz00_4483;
																																BgL_tmpz00_4483
																																	=
																																	CDR(CAR
																																	(BgL_sz00_1717));
																																BgL_test2747z00_4482
																																	=
																																	PAIRP
																																	(BgL_tmpz00_4483);
																															}
																															if (BgL_test2747z00_4482)
																																{	/* Expand/object.scm 104 */
																																	bool_t
																																		BgL_test2748z00_4487;
																																	{	/* Expand/object.scm 104 */
																																		obj_t
																																			BgL_tmpz00_4488;
																																		{	/* Expand/object.scm 104 */
																																			obj_t
																																				BgL_pairz00_3249;
																																			BgL_pairz00_3249
																																				=
																																				CAR
																																				(BgL_sz00_1717);
																																			BgL_tmpz00_4488
																																				=
																																				CAR(CDR
																																				(BgL_pairz00_3249));
																																		}
																																		BgL_test2748z00_4487
																																			=
																																			SYMBOLP
																																			(BgL_tmpz00_4488);
																																	}
																																	if (BgL_test2748z00_4487)
																																		{	/* Expand/object.scm 105 */
																																			obj_t
																																				BgL_tmpz00_4493;
																																			{	/* Expand/object.scm 105 */
																																				obj_t
																																					BgL_pairz00_3254;
																																				BgL_pairz00_3254
																																					=
																																					CAR
																																					(BgL_sz00_1717);
																																				BgL_tmpz00_4493
																																					=
																																					CDR
																																					(CDR
																																					(BgL_pairz00_3254));
																																			}
																																			BgL_test2744z00_4472
																																				=
																																				NULLP
																																				(BgL_tmpz00_4493);
																																		}
																																	else
																																		{	/* Expand/object.scm 104 */
																																			BgL_test2744z00_4472
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Expand/object.scm 103 */
																																	BgL_test2744z00_4472
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																													else
																														{	/* Expand/object.scm 102 */
																															BgL_test2744z00_4472
																																= ((bool_t) 0);
																														}
																												}
																											else
																												{	/* Expand/object.scm 101 */
																													BgL_test2744z00_4472 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test2744z00_4472)
																											{	/* Expand/object.scm 106 */
																												obj_t
																													BgL_arg1559z00_1788;
																												obj_t
																													BgL_arg1561z00_1789;
																												BgL_arg1559z00_1788 =
																													CDR(BgL_sz00_1717);
																												BgL_arg1561z00_1789 =
																													MAKE_YOUNG_PAIR(CAR
																													(BgL_sz00_1717),
																													BgL_nslotsz00_1718);
																												{
																													obj_t
																														BgL_nslotsz00_4502;
																													obj_t BgL_sz00_4501;

																													BgL_sz00_4501 =
																														BgL_arg1559z00_1788;
																													BgL_nslotsz00_4502 =
																														BgL_arg1561z00_1789;
																													BgL_nslotsz00_1718 =
																														BgL_nslotsz00_4502;
																													BgL_sz00_1717 =
																														BgL_sz00_4501;
																													goto
																														BgL_zc3z04anonymousza31337ze3z87_1719;
																												}
																											}
																										else
																											{	/* Expand/object.scm 101 */
																												return
																													BGl_errorz00zz__errorz00
																													(CAR(BgL_sz00_1717),
																													BGl_string2666z00zzexpand_objectz00,
																													BgL_xz00_5);
																											}
																									}
																							}
																						else
																							{	/* Expand/object.scm 97 */
																								return
																									BGl_errorz00zz__errorz00
																									(BgL_sz00_1717,
																									BGl_string2667z00zzexpand_objectz00,
																									BgL_xz00_5);
																							}
																					}
																			}
																		else
																			{	/* Expand/object.scm 77 */
																				return
																					BGl_errorz00zz__errorz00
																					(BgL_withzd2accesszd2_1694,
																					BGl_string2668z00zzexpand_objectz00,
																					BgL_xz00_5);
																			}
																	}
																}
															}
														}
													}
												else
													{	/* Expand/object.scm 72 */
													BgL_tagzd2112zd2_1699:
														return
															BGl_errorz00zz__errorz00
															(BGl_string2669z00zzexpand_objectz00,
															BGl_string2666z00zzexpand_objectz00, BgL_xz00_5);
													}
											}
										else
											{	/* Expand/object.scm 72 */
												goto BgL_tagzd2112zd2_1699;
											}
									}
								else
									{	/* Expand/object.scm 72 */
										goto BgL_tagzd2112zd2_1699;
									}
							}
						else
							{	/* Expand/object.scm 72 */
								goto BgL_tagzd2112zd2_1699;
							}
					}
				else
					{	/* Expand/object.scm 72 */
						goto BgL_tagzd2112zd2_1699;
					}
			}
		}

	}



/* &expand-with-access */
	obj_t BGl_z62expandzd2withzd2accessz62zzexpand_objectz00(obj_t
		BgL_envz00_4132, obj_t BgL_xz00_4133, obj_t BgL_ez00_4134)
	{
		{	/* Expand/object.scm 71 */
			return
				BGl_expandzd2withzd2accessz00zzexpand_objectz00(BgL_xz00_4133,
				BgL_ez00_4134);
		}

	}



/* &<@anonymous:1362> */
	obj_t BGl_z62zc3z04anonymousza31362ze3ze5zzexpand_objectz00(obj_t
		BgL_envz00_4135)
	{
		{	/* Expand/object.scm 91 */
			{	/* Expand/object.scm 92 */
				obj_t BgL_ez00_4136;
				obj_t BgL_auxz00_4137;
				BgL_typez00_bglt BgL_classz00_4138;
				obj_t BgL_nslotsz00_4139;
				obj_t BgL_xz00_4140;
				obj_t BgL_instancez00_4141;
				obj_t BgL_tauxz00_4142;
				obj_t BgL_bodyz00_4143;

				BgL_ez00_4136 = ((obj_t) PROCEDURE_REF(BgL_envz00_4135, (int) (0L)));
				BgL_auxz00_4137 = ((obj_t) PROCEDURE_REF(BgL_envz00_4135, (int) (1L)));
				BgL_classz00_4138 =
					((BgL_typez00_bglt) PROCEDURE_REF(BgL_envz00_4135, (int) (2L)));
				BgL_nslotsz00_4139 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4135, (int) (3L)));
				BgL_xz00_4140 = PROCEDURE_REF(BgL_envz00_4135, (int) (4L));
				BgL_instancez00_4141 = PROCEDURE_REF(BgL_envz00_4135, (int) (5L));
				BgL_tauxz00_4142 = ((obj_t) PROCEDURE_REF(BgL_envz00_4135, (int) (6L)));
				BgL_bodyz00_4143 = ((obj_t) PROCEDURE_REF(BgL_envz00_4135, (int) (7L)));
				{	/* Expand/object.scm 92 */
					obj_t BgL_ez00_4207;

					{	/* Expand/object.scm 93 */
						obj_t BgL_arg1378z00_4208;

						BgL_arg1378z00_4208 =
							BGl_withzd2accesszd2expanderz00zzexpand_objectz00(BgL_ez00_4136,
							BgL_auxz00_4137, ((obj_t) BgL_classz00_4138), BgL_nslotsz00_4139,
							BgL_xz00_4140);
						BgL_ez00_4207 =
							BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00
							(BgL_arg1378z00_4208);
					}
					{	/* Expand/object.scm 95 */
						obj_t BgL_arg1364z00_4209;

						{	/* Expand/object.scm 95 */
							obj_t BgL_arg1367z00_4210;
							obj_t BgL_arg1370z00_4211;

							{	/* Expand/object.scm 95 */
								obj_t BgL_arg1371z00_4212;

								{	/* Expand/object.scm 95 */
									obj_t BgL_arg1375z00_4213;

									BgL_arg1375z00_4213 =
										MAKE_YOUNG_PAIR(BgL_instancez00_4141, BNIL);
									BgL_arg1371z00_4212 =
										MAKE_YOUNG_PAIR(BgL_tauxz00_4142, BgL_arg1375z00_4213);
								}
								BgL_arg1367z00_4210 =
									MAKE_YOUNG_PAIR(BgL_arg1371z00_4212, BNIL);
							}
							{	/* Expand/object.scm 96 */
								obj_t BgL_arg1376z00_4214;

								{	/* Expand/object.scm 96 */
									obj_t BgL_arg1377z00_4215;

									BgL_arg1377z00_4215 =
										BGl_expandzd2prognzd2zz__prognz00(BgL_bodyz00_4143);
									BgL_arg1376z00_4214 =
										BGL_PROCEDURE_CALL2(BgL_ez00_4207, BgL_arg1377z00_4215,
										BgL_ez00_4207);
								}
								BgL_arg1370z00_4211 =
									MAKE_YOUNG_PAIR(BgL_arg1376z00_4214, BNIL);
							}
							BgL_arg1364z00_4209 =
								MAKE_YOUNG_PAIR(BgL_arg1367z00_4210, BgL_arg1370z00_4211);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg1364z00_4209);
					}
				}
			}
		}

	}



/* with-access-expander */
	obj_t BGl_withzd2accesszd2expanderz00zzexpand_objectz00(obj_t BgL_oldez00_7,
		obj_t BgL_iz00_8, obj_t BgL_classz00_9, obj_t BgL_slotsz00_10,
		obj_t BgL_formz00_11)
	{
		{	/* Expand/object.scm 115 */
			{	/* Expand/object.scm 119 */
				obj_t BgL_idsz00_1808;

				if (NULLP(BgL_slotsz00_10))
					{	/* Expand/object.scm 119 */
						BgL_idsz00_1808 = BNIL;
					}
				else
					{	/* Expand/object.scm 119 */
						obj_t BgL_head1271z00_1880;

						{	/* Expand/object.scm 119 */
							obj_t BgL_arg1710z00_1892;

							{	/* Expand/object.scm 119 */
								obj_t BgL_pairz00_3273;

								BgL_pairz00_3273 = CAR(((obj_t) BgL_slotsz00_10));
								BgL_arg1710z00_1892 = CAR(BgL_pairz00_3273);
							}
							BgL_head1271z00_1880 = MAKE_YOUNG_PAIR(BgL_arg1710z00_1892, BNIL);
						}
						{	/* Expand/object.scm 119 */
							obj_t BgL_g1274z00_1881;

							BgL_g1274z00_1881 = CDR(((obj_t) BgL_slotsz00_10));
							{
								obj_t BgL_l1269z00_1883;
								obj_t BgL_tail1272z00_1884;

								BgL_l1269z00_1883 = BgL_g1274z00_1881;
								BgL_tail1272z00_1884 = BgL_head1271z00_1880;
							BgL_zc3z04anonymousza31702ze3z87_1885:
								if (NULLP(BgL_l1269z00_1883))
									{	/* Expand/object.scm 119 */
										BgL_idsz00_1808 = BgL_head1271z00_1880;
									}
								else
									{	/* Expand/object.scm 119 */
										obj_t BgL_newtail1273z00_1887;

										{	/* Expand/object.scm 119 */
											obj_t BgL_arg1708z00_1889;

											{	/* Expand/object.scm 119 */
												obj_t BgL_pairz00_3276;

												BgL_pairz00_3276 = CAR(((obj_t) BgL_l1269z00_1883));
												BgL_arg1708z00_1889 = CAR(BgL_pairz00_3276);
											}
											BgL_newtail1273z00_1887 =
												MAKE_YOUNG_PAIR(BgL_arg1708z00_1889, BNIL);
										}
										SET_CDR(BgL_tail1272z00_1884, BgL_newtail1273z00_1887);
										{	/* Expand/object.scm 119 */
											obj_t BgL_arg1705z00_1888;

											BgL_arg1705z00_1888 = CDR(((obj_t) BgL_l1269z00_1883));
											{
												obj_t BgL_tail1272z00_4565;
												obj_t BgL_l1269z00_4564;

												BgL_l1269z00_4564 = BgL_arg1705z00_1888;
												BgL_tail1272z00_4565 = BgL_newtail1273z00_1887;
												BgL_tail1272z00_1884 = BgL_tail1272z00_4565;
												BgL_l1269z00_1883 = BgL_l1269z00_4564;
												goto BgL_zc3z04anonymousza31702ze3z87_1885;
											}
										}
									}
							}
						}
					}
				{	/* Expand/object.scm 120 */
					obj_t BgL_zc3z04anonymousza31595ze3z87_4144;

					BgL_zc3z04anonymousza31595ze3z87_4144 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31595ze3ze5zzexpand_objectz00, (int) (2L),
						(int) (6L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_4144, (int) (0L),
						BgL_oldez00_7);
					PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_4144, (int) (1L),
						BgL_slotsz00_10);
					PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_4144, (int) (2L),
						BgL_classz00_9);
					PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_4144, (int) (3L),
						BgL_formz00_11);
					PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_4144, (int) (4L),
						BgL_iz00_8);
					PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_4144, (int) (5L),
						BgL_idsz00_1808);
					return BgL_zc3z04anonymousza31595ze3z87_4144;
				}
			}
		}

	}



/* &<@anonymous:1595> */
	obj_t BGl_z62zc3z04anonymousza31595ze3ze5zzexpand_objectz00(obj_t
		BgL_envz00_4145, obj_t BgL_xz00_4152, obj_t BgL_ez00_4153)
	{
		{	/* Expand/object.scm 120 */
			{	/* Expand/object.scm 120 */
				obj_t BgL_oldez00_4146;
				obj_t BgL_slotsz00_4147;
				obj_t BgL_classz00_4148;
				obj_t BgL_formz00_4149;
				obj_t BgL_iz00_4150;
				obj_t BgL_idsz00_4151;

				BgL_oldez00_4146 = ((obj_t) PROCEDURE_REF(BgL_envz00_4145, (int) (0L)));
				BgL_slotsz00_4147 = PROCEDURE_REF(BgL_envz00_4145, (int) (1L));
				BgL_classz00_4148 = PROCEDURE_REF(BgL_envz00_4145, (int) (2L));
				BgL_formz00_4149 = PROCEDURE_REF(BgL_envz00_4145, (int) (3L));
				BgL_iz00_4150 = PROCEDURE_REF(BgL_envz00_4145, (int) (4L));
				BgL_idsz00_4151 = ((obj_t) PROCEDURE_REF(BgL_envz00_4145, (int) (5L)));
				{
					obj_t BgL_varz00_4237;
					obj_t BgL_varz00_4219;
					obj_t BgL_valz00_4220;

					if (SYMBOLP(BgL_xz00_4152))
						{	/* Expand/object.scm 120 */
							BgL_varz00_4237 = BgL_xz00_4152;
							{	/* Expand/object.scm 123 */
								bool_t BgL_test2752z00_4597;

								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_varz00_4237, BgL_idsz00_4151)))
									{	/* Expand/object.scm 124 */
										obj_t BgL_cellz00_4238;

										BgL_cellz00_4238 =
											BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_varz00_4237,
											BGl_lexicalzd2stackzd2zzexpand_epsz00());
										if (PAIRP(BgL_cellz00_4238))
											{	/* Expand/object.scm 125 */
												BgL_test2752z00_4597 =
													(CDR(BgL_cellz00_4238) == BgL_iz00_4150);
											}
										else
											{	/* Expand/object.scm 125 */
												BgL_test2752z00_4597 = ((bool_t) 0);
											}
									}
								else
									{	/* Expand/object.scm 123 */
										BgL_test2752z00_4597 = ((bool_t) 0);
									}
								if (BgL_test2752z00_4597)
									{	/* Expand/object.scm 126 */
										obj_t BgL_slotz00_4239;

										{	/* Expand/object.scm 126 */
											obj_t BgL_arg1646z00_4240;

											{	/* Expand/object.scm 117 */
												obj_t BgL_pairz00_4241;

												BgL_pairz00_4241 =
													BGl_assqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_varz00_4237, BgL_slotsz00_4147);
												BgL_arg1646z00_4240 = CAR(CDR(BgL_pairz00_4241));
											}
											BgL_slotz00_4239 =
												BGl_findzd2classzd2slotz00zzobject_toolsz00(
												((BgL_typez00_bglt) BgL_classz00_4148),
												BgL_arg1646z00_4240);
										}
										if (CBOOL(BgL_slotz00_4239))
											{	/* Expand/object.scm 129 */
												obj_t BgL_arg1629z00_4242;

												{	/* Expand/object.scm 129 */
													obj_t BgL_arg1630z00_4243;

													{	/* Expand/object.scm 117 */
														obj_t BgL_pairz00_4244;

														BgL_pairz00_4244 =
															BGl_assqz00zz__r4_pairs_and_lists_6_3z00
															(BgL_varz00_4237, BgL_slotsz00_4147);
														BgL_arg1630z00_4243 = CAR(CDR(BgL_pairz00_4244));
													}
													{	/* Expand/object.scm 129 */

														BgL_arg1629z00_4242 =
															BGl_fieldzd2accesszd2zzast_objectz00
															(BgL_iz00_4150, BgL_arg1630z00_4243, BFALSE);
													}
												}
												return
													BGL_PROCEDURE_CALL2(BgL_oldez00_4146,
													BgL_arg1629z00_4242, BgL_oldez00_4146);
											}
										else
											{	/* Expand/object.scm 128 */
												obj_t BgL_arg1642z00_4245;

												{	/* Expand/object.scm 117 */
													obj_t BgL_pairz00_4246;

													BgL_pairz00_4246 =
														BGl_assqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_varz00_4237, BgL_slotsz00_4147);
													BgL_arg1642z00_4245 = CAR(CDR(BgL_pairz00_4246));
												}
												return
													BGl_errorz00zz__errorz00(BgL_arg1642z00_4245,
													BGl_string2670z00zzexpand_objectz00,
													BgL_formz00_4149);
											}
									}
								else
									{	/* Expand/object.scm 123 */
										return
											BGL_PROCEDURE_CALL2(BgL_oldez00_4146, BgL_varz00_4237,
											BgL_oldez00_4146);
									}
							}
						}
					else
						{	/* Expand/object.scm 120 */
							if (PAIRP(BgL_xz00_4152))
								{	/* Expand/object.scm 120 */
									obj_t BgL_cdrzd2154zd2_4247;

									BgL_cdrzd2154zd2_4247 = CDR(((obj_t) BgL_xz00_4152));
									if ((CAR(((obj_t) BgL_xz00_4152)) == CNST_TABLE_REF(7)))
										{	/* Expand/object.scm 120 */
											if (PAIRP(BgL_cdrzd2154zd2_4247))
												{	/* Expand/object.scm 120 */
													obj_t BgL_carzd2157zd2_4248;
													obj_t BgL_cdrzd2158zd2_4249;

													BgL_carzd2157zd2_4248 = CAR(BgL_cdrzd2154zd2_4247);
													BgL_cdrzd2158zd2_4249 = CDR(BgL_cdrzd2154zd2_4247);
													if (SYMBOLP(BgL_carzd2157zd2_4248))
														{	/* Expand/object.scm 120 */
															if (PAIRP(BgL_cdrzd2158zd2_4249))
																{	/* Expand/object.scm 120 */
																	if (NULLP(CDR(BgL_cdrzd2158zd2_4249)))
																		{	/* Expand/object.scm 120 */
																			BgL_varz00_4219 = BgL_carzd2157zd2_4248;
																			BgL_valz00_4220 =
																				CAR(BgL_cdrzd2158zd2_4249);
																			{	/* Expand/object.scm 132 */
																				obj_t BgL_valz00_4221;

																				BgL_valz00_4221 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_4153,
																					BgL_valz00_4220, BgL_ez00_4153);
																				{	/* Expand/object.scm 133 */
																					bool_t BgL_test2762z00_4657;

																					if (CBOOL
																						(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																							(BgL_varz00_4219,
																								BgL_idsz00_4151)))
																						{	/* Expand/object.scm 134 */
																							obj_t BgL_cellz00_4222;

																							BgL_cellz00_4222 =
																								BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																								(BgL_varz00_4219,
																								BGl_lexicalzd2stackzd2zzexpand_epsz00
																								());
																							if (PAIRP(BgL_cellz00_4222))
																								{	/* Expand/object.scm 135 */
																									BgL_test2762z00_4657 =
																										(CDR(BgL_cellz00_4222) ==
																										BgL_iz00_4150);
																								}
																							else
																								{	/* Expand/object.scm 135 */
																									BgL_test2762z00_4657 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Expand/object.scm 133 */
																							BgL_test2762z00_4657 =
																								((bool_t) 0);
																						}
																					if (BgL_test2762z00_4657)
																						{	/* Expand/object.scm 136 */
																							obj_t BgL_slotz00_4223;

																							{	/* Expand/object.scm 136 */
																								obj_t BgL_arg1691z00_4224;

																								{	/* Expand/object.scm 117 */
																									obj_t BgL_pairz00_4225;

																									BgL_pairz00_4225 =
																										BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																										(BgL_varz00_4219,
																										BgL_slotsz00_4147);
																									BgL_arg1691z00_4224 =
																										CAR(CDR(BgL_pairz00_4225));
																								}
																								BgL_slotz00_4223 =
																									BGl_findzd2classzd2slotz00zzobject_toolsz00
																									(((BgL_typez00_bglt)
																										BgL_classz00_4148),
																									BgL_arg1691z00_4224);
																							}
																							if (CBOOL(BgL_slotz00_4223))
																								{	/* Expand/object.scm 140 */
																									obj_t BgL_arg1675z00_4226;

																									{	/* Expand/object.scm 140 */
																										obj_t BgL_idz00_4227;

																										{	/* Expand/object.scm 140 */
																											obj_t BgL_arg1688z00_4228;

																											{	/* Expand/object.scm 117 */
																												obj_t BgL_pairz00_4229;

																												BgL_pairz00_4229 =
																													BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																													(BgL_varz00_4219,
																													BgL_slotsz00_4147);
																												BgL_arg1688z00_4228 =
																													CAR(CDR
																													(BgL_pairz00_4229));
																											}
																											{	/* Expand/object.scm 140 */

																												BgL_idz00_4227 =
																													BGl_fieldzd2accesszd2zzast_objectz00
																													(BgL_iz00_4150,
																													BgL_arg1688z00_4228,
																													BFALSE);
																											}
																										}
																										{	/* Expand/object.scm 140 */
																											obj_t BgL_nxz00_4230;

																											{	/* Expand/object.scm 141 */
																												obj_t
																													BgL_arg1678z00_4231;
																												{	/* Expand/object.scm 141 */
																													obj_t
																														BgL_arg1681z00_4232;
																													BgL_arg1681z00_4232 =
																														MAKE_YOUNG_PAIR
																														(BgL_valz00_4221,
																														BNIL);
																													BgL_arg1678z00_4231 =
																														MAKE_YOUNG_PAIR
																														(BgL_idz00_4227,
																														BgL_arg1681z00_4232);
																												}
																												BgL_nxz00_4230 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(7),
																													BgL_arg1678z00_4231);
																											}
																											{	/* Expand/object.scm 141 */

																												BgL_arg1675z00_4226 =
																													BGL_PROCEDURE_CALL2
																													(BgL_oldez00_4146,
																													BgL_nxz00_4230,
																													BgL_oldez00_4146);
																											}
																										}
																									}
																									return
																										BGl_objectzd2epairifyzd2zzexpand_objectz00
																										(BgL_arg1675z00_4226,
																										BgL_xz00_4152);
																								}
																							else
																								{	/* Expand/object.scm 138 */
																									obj_t BgL_arg1689z00_4233;

																									{	/* Expand/object.scm 117 */
																										obj_t BgL_pairz00_4234;

																										BgL_pairz00_4234 =
																											BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																											(BgL_varz00_4219,
																											BgL_slotsz00_4147);
																										BgL_arg1689z00_4233 =
																											CAR(CDR
																											(BgL_pairz00_4234));
																									}
																									return
																										BGl_errorz00zz__errorz00
																										(BgL_arg1689z00_4233,
																										BGl_string2670z00zzexpand_objectz00,
																										BgL_formz00_4149);
																								}
																						}
																					else
																						{	/* Expand/object.scm 133 */
																							{	/* Expand/object.scm 145 */
																								obj_t BgL_arg1692z00_4235;

																								{	/* Expand/object.scm 145 */
																									obj_t BgL_pairz00_4236;

																									BgL_pairz00_4236 =
																										CDR(
																										((obj_t) BgL_xz00_4152));
																									BgL_arg1692z00_4235 =
																										CDR(BgL_pairz00_4236);
																								}
																								{	/* Expand/object.scm 145 */
																									obj_t BgL_tmpz00_4695;

																									BgL_tmpz00_4695 =
																										((obj_t)
																										BgL_arg1692z00_4235);
																									SET_CAR(BgL_tmpz00_4695,
																										BgL_valz00_4221);
																								}
																							}
																							return
																								BGL_PROCEDURE_CALL2
																								(BgL_oldez00_4146,
																								BgL_xz00_4152,
																								BgL_oldez00_4146);
																						}
																				}
																			}
																		}
																	else
																		{	/* Expand/object.scm 120 */
																		BgL_tagzd2143zd2_4216:
																			return
																				BGL_PROCEDURE_CALL2(BgL_oldez00_4146,
																				BgL_xz00_4152, BgL_ez00_4153);
																		}
																}
															else
																{	/* Expand/object.scm 120 */
																	goto BgL_tagzd2143zd2_4216;
																}
														}
													else
														{	/* Expand/object.scm 120 */
															goto BgL_tagzd2143zd2_4216;
														}
												}
											else
												{	/* Expand/object.scm 120 */
													goto BgL_tagzd2143zd2_4216;
												}
										}
									else
										{	/* Expand/object.scm 120 */
											goto BgL_tagzd2143zd2_4216;
										}
								}
							else
								{	/* Expand/object.scm 120 */
									goto BgL_tagzd2143zd2_4216;
								}
						}
				}
			}
		}

	}



/* expand-instantiate */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2instantiatezd2zzexpand_objectz00(obj_t
		BgL_xz00_12, obj_t BgL_ez00_13)
	{
		{	/* Expand/object.scm 153 */
			{
				obj_t BgL_instantiatez00_1898;

				if (PAIRP(BgL_xz00_12))
					{	/* Expand/object.scm 154 */
						obj_t BgL_arg1717z00_1903;

						BgL_arg1717z00_1903 = CAR(((obj_t) BgL_xz00_12));
						BgL_instantiatez00_1898 = BgL_arg1717z00_1903;
						{	/* Expand/object.scm 156 */
							BgL_typez00_bglt BgL_classz00_1904;

							BgL_classz00_1904 =
								BGl_typezd2ofzd2idz00zzast_identz00(BgL_instantiatez00_1898,
								BGl_findzd2locationzd2zztools_locationz00(BgL_xz00_12));
							{	/* Expand/object.scm 158 */
								bool_t BgL_test2767z00_4715;

								{	/* Expand/object.scm 158 */
									obj_t BgL_classz00_3322;

									BgL_classz00_3322 = BGl_tclassz00zzobject_classz00;
									{	/* Expand/object.scm 158 */
										BgL_objectz00_bglt BgL_arg1807z00_3324;

										{	/* Expand/object.scm 158 */
											obj_t BgL_tmpz00_4716;

											BgL_tmpz00_4716 =
												((obj_t) ((BgL_objectz00_bglt) BgL_classz00_1904));
											BgL_arg1807z00_3324 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4716);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Expand/object.scm 158 */
												long BgL_idxz00_3330;

												BgL_idxz00_3330 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3324);
												BgL_test2767z00_4715 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3330 + 2L)) == BgL_classz00_3322);
											}
										else
											{	/* Expand/object.scm 158 */
												bool_t BgL_res2648z00_3355;

												{	/* Expand/object.scm 158 */
													obj_t BgL_oclassz00_3338;

													{	/* Expand/object.scm 158 */
														obj_t BgL_arg1815z00_3346;
														long BgL_arg1816z00_3347;

														BgL_arg1815z00_3346 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Expand/object.scm 158 */
															long BgL_arg1817z00_3348;

															BgL_arg1817z00_3348 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3324);
															BgL_arg1816z00_3347 =
																(BgL_arg1817z00_3348 - OBJECT_TYPE);
														}
														BgL_oclassz00_3338 =
															VECTOR_REF(BgL_arg1815z00_3346,
															BgL_arg1816z00_3347);
													}
													{	/* Expand/object.scm 158 */
														bool_t BgL__ortest_1115z00_3339;

														BgL__ortest_1115z00_3339 =
															(BgL_classz00_3322 == BgL_oclassz00_3338);
														if (BgL__ortest_1115z00_3339)
															{	/* Expand/object.scm 158 */
																BgL_res2648z00_3355 = BgL__ortest_1115z00_3339;
															}
														else
															{	/* Expand/object.scm 158 */
																long BgL_odepthz00_3340;

																{	/* Expand/object.scm 158 */
																	obj_t BgL_arg1804z00_3341;

																	BgL_arg1804z00_3341 = (BgL_oclassz00_3338);
																	BgL_odepthz00_3340 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3341);
																}
																if ((2L < BgL_odepthz00_3340))
																	{	/* Expand/object.scm 158 */
																		obj_t BgL_arg1802z00_3343;

																		{	/* Expand/object.scm 158 */
																			obj_t BgL_arg1803z00_3344;

																			BgL_arg1803z00_3344 =
																				(BgL_oclassz00_3338);
																			BgL_arg1802z00_3343 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3344, 2L);
																		}
																		BgL_res2648z00_3355 =
																			(BgL_arg1802z00_3343 ==
																			BgL_classz00_3322);
																	}
																else
																	{	/* Expand/object.scm 158 */
																		BgL_res2648z00_3355 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2767z00_4715 = BgL_res2648z00_3355;
											}
									}
								}
								if (BgL_test2767z00_4715)
									{	/* Expand/object.scm 160 */
										bool_t BgL_test2771z00_4739;

										{
											BgL_tclassz00_bglt BgL_auxz00_4740;

											{
												obj_t BgL_auxz00_4741;

												{	/* Expand/object.scm 160 */
													BgL_objectz00_bglt BgL_tmpz00_4742;

													BgL_tmpz00_4742 =
														((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_classz00_1904));
													BgL_auxz00_4741 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4742);
												}
												BgL_auxz00_4740 =
													((BgL_tclassz00_bglt) BgL_auxz00_4741);
											}
											BgL_test2771z00_4739 =
												(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4740))->
												BgL_abstractzf3zf3);
										}
										if (BgL_test2771z00_4739)
											{	/* Expand/object.scm 160 */
												return
													BGl_errorz00zz__errorz00(BgL_instantiatez00_1898,
													BGl_string2671z00zzexpand_objectz00, BgL_xz00_12);
											}
										else
											{	/* Expand/object.scm 160 */
												return
													BGl_replacez12z12zztools_miscz00(BgL_xz00_12,
													BGl_instantiatezd2ze3makez31zzexpand_objectz00
													(BgL_xz00_12, BgL_classz00_1904, BgL_ez00_13));
											}
									}
								else
									{	/* Expand/object.scm 158 */
										return
											BGl_errorz00zz__errorz00(BgL_instantiatez00_1898,
											BGl_string2672z00zzexpand_objectz00, BgL_xz00_12);
									}
							}
						}
					}
				else
					{	/* Expand/object.scm 154 */
						return
							BGl_errorz00zz__errorz00(BGl_string2673z00zzexpand_objectz00,
							BGl_string2666z00zzexpand_objectz00, BgL_xz00_12);
					}
			}
		}

	}



/* &expand-instantiate */
	obj_t BGl_z62expandzd2instantiatezb0zzexpand_objectz00(obj_t BgL_envz00_4154,
		obj_t BgL_xz00_4155, obj_t BgL_ez00_4156)
	{
		{	/* Expand/object.scm 153 */
			return
				BGl_expandzd2instantiatezd2zzexpand_objectz00(BgL_xz00_4155,
				BgL_ez00_4156);
		}

	}



/* instantiate->make */
	obj_t BGl_instantiatezd2ze3makez31zzexpand_objectz00(obj_t BgL_xz00_14,
		BgL_typez00_bglt BgL_classz00_15, obj_t BgL_ez00_16)
	{
		{	/* Expand/object.scm 170 */
			{	/* Expand/object.scm 171 */
				obj_t BgL_oz00_1909;

				BgL_oz00_1909 =
					BGl_allocatezd2exprzd2zzexpand_objectz00(((obj_t) BgL_classz00_15));
				{	/* Expand/object.scm 172 */
					obj_t BgL_arg1724z00_1910;
					obj_t BgL_arg1733z00_1911;
					obj_t BgL_arg1734z00_1912;

					BgL_arg1724z00_1910 = CAR(((obj_t) BgL_xz00_14));
					BgL_arg1733z00_1911 = CDR(((obj_t) BgL_xz00_14));
					{
						BgL_tclassz00_bglt BgL_auxz00_4760;

						{
							obj_t BgL_auxz00_4761;

							{	/* Expand/object.scm 173 */
								BgL_objectz00_bglt BgL_tmpz00_4762;

								BgL_tmpz00_4762 =
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_15));
								BgL_auxz00_4761 = BGL_OBJECT_WIDENING(BgL_tmpz00_4762);
							}
							BgL_auxz00_4760 = ((BgL_tclassz00_bglt) BgL_auxz00_4761);
						}
						BgL_arg1734z00_1912 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4760))->BgL_slotsz00);
					}
					return
						BGl_instantiatezd2fillzd2zzexpand_objectz00(BgL_arg1724z00_1910,
						BgL_arg1733z00_1911, ((obj_t) BgL_classz00_15), BgL_arg1734z00_1912,
						BgL_oz00_1909, BgL_xz00_14, BgL_ez00_16);
				}
			}
		}

	}



/* instantiate-fill */
	obj_t BGl_instantiatezd2fillzd2zzexpand_objectz00(obj_t BgL_opz00_17,
		obj_t BgL_providedz00_18, obj_t BgL_classz00_19, obj_t BgL_slotsz00_20,
		obj_t BgL_initz00_21, obj_t BgL_xz00_22, obj_t BgL_ez00_23)
	{
		{	/* Expand/object.scm 178 */
			{
				BgL_globalz00_bglt BgL_gz00_2362;
				obj_t BgL_sz00_2363;
				obj_t BgL_sz00_2386;
				obj_t BgL_slotsz00_2395;

				{	/* Expand/object.scm 312 */
					obj_t BgL_idz00_1921;

					BgL_idz00_1921 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_classz00_19)))->BgL_idz00);
					{	/* Expand/object.scm 312 */
						obj_t BgL_newz00_1922;

						BgL_newz00_1922 =
							BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(13));
						{	/* Expand/object.scm 313 */
							obj_t BgL_tnewz00_1923;

							BgL_tnewz00_1923 =
								BGl_makezd2typedzd2identz00zzast_identz00(BgL_newz00_1922,
								BgL_idz00_1921);
							{	/* Expand/object.scm 314 */
								obj_t BgL_argsz00_1924;

								BgL_slotsz00_2395 = BgL_slotsz00_20;
								{	/* Expand/object.scm 246 */
									long BgL_offsetz00_2397;
									obj_t BgL_vargsz00_2398;

									{	/* Expand/object.scm 246 */
										long BgL_arg2154z00_2442;
										long BgL_arg2155z00_2443;

										{	/* Expand/object.scm 246 */
											obj_t BgL_arg2156z00_2444;

											{
												BgL_tclassz00_bglt BgL_auxz00_4775;

												{
													obj_t BgL_auxz00_4776;

													{	/* Expand/object.scm 246 */
														BgL_objectz00_bglt BgL_tmpz00_4777;

														BgL_tmpz00_4777 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_classz00_19));
														BgL_auxz00_4776 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4777);
													}
													BgL_auxz00_4775 =
														((BgL_tclassz00_bglt) BgL_auxz00_4776);
												}
												BgL_arg2156z00_2444 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4775))->
													BgL_slotsz00);
											}
											BgL_arg2154z00_2442 =
												bgl_list_length(BgL_arg2156z00_2444);
										}
										BgL_arg2155z00_2443 = bgl_list_length(BgL_slotsz00_2395);
										BgL_offsetz00_2397 =
											(BgL_arg2154z00_2442 - BgL_arg2155z00_2443);
									}
									BgL_vargsz00_2398 =
										make_vector(bgl_list_length(BgL_slotsz00_2395), BUNSPEC);
									{
										obj_t BgL_providedz00_2400;

										BgL_providedz00_2400 = BgL_providedz00_18;
									BgL_zc3z04anonymousza32125ze3z87_2401:
										if (PAIRP(BgL_providedz00_2400))
											{	/* Expand/object.scm 251 */
												obj_t BgL_pz00_2403;

												BgL_pz00_2403 = CAR(BgL_providedz00_2400);
												{
													obj_t BgL_szd2namezd2_2404;
													obj_t BgL_valuez00_2405;

													if (PAIRP(BgL_pz00_2403))
														{	/* Expand/object.scm 252 */
															obj_t BgL_carzd2672zd2_2410;
															obj_t BgL_cdrzd2673zd2_2411;

															BgL_carzd2672zd2_2410 =
																CAR(((obj_t) BgL_pz00_2403));
															BgL_cdrzd2673zd2_2411 =
																CDR(((obj_t) BgL_pz00_2403));
															if (SYMBOLP(BgL_carzd2672zd2_2410))
																{	/* Expand/object.scm 252 */
																	if (PAIRP(BgL_cdrzd2673zd2_2411))
																		{	/* Expand/object.scm 252 */
																			if (NULLP(CDR(BgL_cdrzd2673zd2_2411)))
																				{	/* Expand/object.scm 252 */
																					BgL_szd2namezd2_2404 =
																						BgL_carzd2672zd2_2410;
																					BgL_valuez00_2405 =
																						CAR(BgL_cdrzd2673zd2_2411);
																					{	/* Expand/object.scm 255 */
																						long BgL_arg2134z00_2418;
																						obj_t BgL_arg2135z00_2419;

																						{	/* Expand/object.scm 255 */
																							obj_t BgL_arg2136z00_2420;

																							BgL_arg2136z00_2420 =
																								BGl_findzd2slotzd2offsetz00zzexpand_objectz00
																								(BgL_slotsz00_2395,
																								BgL_szd2namezd2_2404,
																								BgL_opz00_17, BgL_pz00_2403);
																							BgL_arg2134z00_2418 =
																								((long)
																								CINT(BgL_arg2136z00_2420) -
																								BgL_offsetz00_2397);
																						}
																						BgL_arg2135z00_2419 =
																							MAKE_YOUNG_PAIR(BTRUE,
																							BGl_objectzd2epairifyzd2zzexpand_objectz00
																							(BgL_valuez00_2405,
																								BgL_pz00_2403));
																						VECTOR_SET(BgL_vargsz00_2398,
																							BgL_arg2134z00_2418,
																							BgL_arg2135z00_2419);
																				}}
																			else
																				{	/* Expand/object.scm 252 */
																				BgL_tagzd2666zd2_2407:
																					{	/* Expand/object.scm 258 */
																						obj_t BgL_arg2138z00_2422;

																						{	/* Expand/object.scm 258 */
																							obj_t BgL_list2139z00_2423;

																							BgL_list2139z00_2423 =
																								MAKE_YOUNG_PAIR(BgL_pz00_2403,
																								BNIL);
																							BgL_arg2138z00_2422 =
																								BGl_formatz00zz__r4_output_6_10_3z00
																								(BGl_string2674z00zzexpand_objectz00,
																								BgL_list2139z00_2423);
																						}
																						BGl_errorz00zz__errorz00
																							(BgL_opz00_17,
																							BgL_arg2138z00_2422, BgL_xz00_22);
																					}
																				}
																		}
																	else
																		{	/* Expand/object.scm 252 */
																			goto BgL_tagzd2666zd2_2407;
																		}
																}
															else
																{	/* Expand/object.scm 252 */
																	goto BgL_tagzd2666zd2_2407;
																}
														}
													else
														{	/* Expand/object.scm 252 */
															goto BgL_tagzd2666zd2_2407;
														}
												}
												{
													obj_t BgL_providedz00_4814;

													BgL_providedz00_4814 = CDR(BgL_providedz00_2400);
													BgL_providedz00_2400 = BgL_providedz00_4814;
													goto BgL_zc3z04anonymousza32125ze3z87_2401;
												}
											}
										else
											{	/* Expand/object.scm 250 */
												((bool_t) 0);
											}
									}
									{
										long BgL_iz00_2427;
										obj_t BgL_slotsz00_2428;

										BgL_iz00_2427 = 0L;
										BgL_slotsz00_2428 = BgL_slotsz00_2395;
									BgL_zc3z04anonymousza32142ze3z87_2429:
										if (PAIRP(BgL_slotsz00_2428))
											{	/* Expand/object.scm 264 */
												obj_t BgL_sz00_2431;

												BgL_sz00_2431 = CAR(BgL_slotsz00_2428);
												{	/* Expand/object.scm 266 */
													bool_t BgL_test2778z00_4819;

													{	/* Expand/object.scm 266 */
														obj_t BgL_tmpz00_4820;

														BgL_tmpz00_4820 =
															VECTOR_REF(BgL_vargsz00_2398, BgL_iz00_2427);
														BgL_test2778z00_4819 = PAIRP(BgL_tmpz00_4820);
													}
													if (BgL_test2778z00_4819)
														{	/* Expand/object.scm 266 */
															BUNSPEC;
														}
													else
														{	/* Expand/object.scm 266 */
															if (BGl_slotzd2defaultzf3z21zzobject_slotsz00(
																	((BgL_slotz00_bglt) BgL_sz00_2431)))
																{	/* Expand/object.scm 269 */
																	obj_t BgL_arg2147z00_2435;

																	{	/* Expand/object.scm 269 */
																		obj_t BgL_tmpz00_4826;

																		BgL_sz00_2386 = BgL_sz00_2431;
																		{	/* Expand/object.scm 237 */
																			BgL_globalz00_bglt BgL_gz00_2388;
																			obj_t BgL_mz00_2389;

																			{
																				BgL_tclassz00_bglt BgL_auxz00_4827;

																				{
																					obj_t BgL_auxz00_4828;

																					{	/* Expand/object.scm 237 */
																						BgL_objectz00_bglt BgL_tmpz00_4829;

																						BgL_tmpz00_4829 =
																							((BgL_objectz00_bglt)
																							((BgL_typez00_bglt)
																								BgL_classz00_19));
																						BgL_auxz00_4828 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_4829);
																					}
																					BgL_auxz00_4827 =
																						((BgL_tclassz00_bglt)
																						BgL_auxz00_4828);
																				}
																				BgL_gz00_2388 =
																					(((BgL_tclassz00_bglt)
																						COBJECT(BgL_auxz00_4827))->
																					BgL_holderz00);
																			}
																			{	/* Expand/object.scm 238 */
																				BgL_globalz00_bglt BgL_arg2122z00_2393;

																				{	/* Expand/object.scm 238 */
																					BgL_typez00_bglt BgL_oz00_3622;

																					BgL_oz00_3622 =
																						((BgL_typez00_bglt)
																						(((BgL_slotz00_bglt) COBJECT(
																									((BgL_slotz00_bglt)
																										BgL_sz00_2386)))->
																							BgL_classzd2ownerzd2));
																					{
																						BgL_tclassz00_bglt BgL_auxz00_4838;

																						{
																							obj_t BgL_auxz00_4839;

																							{	/* Expand/object.scm 238 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_4840;
																								BgL_tmpz00_4840 =
																									((BgL_objectz00_bglt)
																									BgL_oz00_3622);
																								BgL_auxz00_4839 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_4840);
																							}
																							BgL_auxz00_4838 =
																								((BgL_tclassz00_bglt)
																								BgL_auxz00_4839);
																						}
																						BgL_arg2122z00_2393 =
																							(((BgL_tclassz00_bglt)
																								COBJECT(BgL_auxz00_4838))->
																							BgL_holderz00);
																					}
																				}
																				BgL_mz00_2389 =
																					(((BgL_globalz00_bglt)
																						COBJECT(BgL_arg2122z00_2393))->
																					BgL_modulez00);
																			}
																			{	/* Expand/object.scm 239 */
																				bool_t BgL_test2780z00_4846;

																				{	/* Expand/object.scm 239 */
																					obj_t BgL_arg2121z00_2392;

																					BgL_arg2121z00_2392 =
																						(((BgL_slotz00_bglt) COBJECT(
																								((BgL_slotz00_bglt)
																									BgL_sz00_2386)))->
																						BgL_defaultzd2valuezd2);
																					BgL_test2780z00_4846 =
																						BGl_inlinablezf3ze70z14zzexpand_objectz00
																						(BgL_arg2121z00_2392,
																						BgL_mz00_2389);
																				}
																				if (BgL_test2780z00_4846)
																					{	/* Expand/object.scm 239 */
																						BgL_tmpz00_4826 =
																							(((BgL_slotz00_bglt) COBJECT(
																									((BgL_slotz00_bglt)
																										BgL_sz00_2386)))->
																							BgL_defaultzd2valuezd2);
																					}
																				else
																					{	/* Expand/object.scm 239 */
																						BgL_gz00_2362 = BgL_gz00_2388;
																						BgL_sz00_2363 = BgL_sz00_2386;
																						if (CBOOL
																							(BGl_za2warningzd2defaultzd2slotzd2valueza2zd2zzengine_paramz00))
																							{	/* Expand/object.scm 224 */
																								{	/* Expand/object.scm 225 */
																									obj_t BgL_arg2097z00_2365;
																									obj_t BgL_arg2098z00_2366;
																									obj_t BgL_arg2099z00_2367;

																									BgL_arg2097z00_2365 =
																										BGl_findzd2locationzd2zztools_locationz00
																										(BgL_xz00_22);
																									{	/* Expand/object.scm 227 */
																										obj_t BgL_arg2100z00_2368;

																										BgL_arg2100z00_2368 =
																											(((BgL_slotz00_bglt)
																												COBJECT((
																														(BgL_slotz00_bglt)
																														BgL_sz00_2363)))->
																											BgL_idz00);
																										{	/* Expand/object.scm 227 */
																											obj_t
																												BgL_list2101z00_2369;
																											BgL_list2101z00_2369 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2100z00_2368,
																												BNIL);
																											BgL_arg2098z00_2366 =
																												BGl_formatz00zz__r4_output_6_10_3z00
																												(BGl_string2675z00zzexpand_objectz00,
																												BgL_list2101z00_2369);
																										}
																									}
																									BgL_arg2099z00_2367 =
																										(((BgL_slotz00_bglt)
																											COBJECT((
																													(BgL_slotz00_bglt)
																													BgL_sz00_2363)))->
																										BgL_defaultzd2valuezd2);
																									BGl_userzd2warningzf2locationz20zztools_errorz00
																										(BgL_arg2097z00_2365,
																										BgL_opz00_17,
																										BgL_arg2098z00_2366,
																										BgL_arg2099z00_2367);
																								}
																								{	/* Expand/object.scm 229 */
																									obj_t BgL_arg2102z00_2370;

																									{	/* Expand/object.scm 229 */
																										obj_t BgL_tmpz00_4862;

																										BgL_tmpz00_4862 =
																											BGL_CURRENT_DYNAMIC_ENV();
																										BgL_arg2102z00_2370 =
																											BGL_ENV_CURRENT_ERROR_PORT
																											(BgL_tmpz00_4862);
																									}
																									bgl_display_char(((unsigned
																												char) 10),
																										BgL_arg2102z00_2370);
																							}}
																						else
																							{	/* Expand/object.scm 224 */
																								BFALSE;
																							}
																						{	/* Expand/object.scm 232 */
																							obj_t BgL_arg2103z00_2371;

																							{	/* Expand/object.scm 232 */
																								obj_t BgL_arg2104z00_2372;

																								{	/* Expand/object.scm 232 */
																									obj_t BgL_arg2105z00_2373;

																									{	/* Expand/object.scm 232 */
																										obj_t BgL_arg2106z00_2374;
																										obj_t BgL_arg2107z00_2375;

																										{	/* Expand/object.scm 232 */
																											obj_t BgL_arg2108z00_2376;
																											obj_t BgL_arg2109z00_2377;

																											{	/* Expand/object.scm 232 */
																												obj_t
																													BgL_arg2110z00_2378;
																												{	/* Expand/object.scm 232 */
																													obj_t
																														BgL_arg2111z00_2379;
																													BgL_arg2111z00_2379 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(8),
																														BNIL);
																													BgL_arg2110z00_2378 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(9),
																														BgL_arg2111z00_2379);
																												}
																												BgL_arg2108z00_2376 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(10),
																													BgL_arg2110z00_2378);
																											}
																											{	/* Expand/object.scm 233 */
																												obj_t
																													BgL_arg2112z00_2380;
																												{	/* Expand/object.scm 233 */
																													obj_t
																														BgL_arg2113z00_2381;
																													{	/* Expand/object.scm 233 */
																														obj_t
																															BgL_arg2114z00_2382;
																														obj_t
																															BgL_arg2115z00_2383;
																														BgL_arg2114z00_2382
																															=
																															(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_gz00_2362)))->BgL_idz00);
																														BgL_arg2115z00_2383
																															=
																															MAKE_YOUNG_PAIR(((
																																	(BgL_globalz00_bglt)
																																	COBJECT
																																	(BgL_gz00_2362))->
																																BgL_modulez00),
																															BNIL);
																														BgL_arg2113z00_2381
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg2114z00_2382,
																															BgL_arg2115z00_2383);
																													}
																													BgL_arg2112z00_2380 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(10),
																														BgL_arg2113z00_2381);
																												}
																												BgL_arg2109z00_2377 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg2112z00_2380,
																													BNIL);
																											}
																											BgL_arg2106z00_2374 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2108z00_2376,
																												BgL_arg2109z00_2377);
																										}
																										{	/* Expand/object.scm 234 */
																											long BgL_arg2117z00_2385;

																											BgL_arg2117z00_2385 =
																												(((BgL_slotz00_bglt)
																													COBJECT((
																															(BgL_slotz00_bglt)
																															BgL_sz00_2363)))->
																												BgL_indexz00);
																											BgL_arg2107z00_2375 =
																												MAKE_YOUNG_PAIR(BINT
																												(BgL_arg2117z00_2385),
																												BNIL);
																										}
																										BgL_arg2105z00_2373 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2106z00_2374,
																											BgL_arg2107z00_2375);
																									}
																									BgL_arg2104z00_2372 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(11),
																										BgL_arg2105z00_2373);
																								}
																								BgL_arg2103z00_2371 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2104z00_2372, BNIL);
																							}
																							BgL_tmpz00_4826 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(12), BgL_arg2103z00_2371);
																		}}}}
																		BgL_arg2147z00_2435 =
																			MAKE_YOUNG_PAIR(BTRUE, BgL_tmpz00_4826);
																	}
																	VECTOR_SET(BgL_vargsz00_2398, BgL_iz00_2427,
																		BgL_arg2147z00_2435);
																}
															else
																{	/* Expand/object.scm 271 */
																	obj_t BgL_arg2149z00_2437;

																	BgL_arg2149z00_2437 =
																		MAKE_YOUNG_PAIR(BFALSE, BUNSPEC);
																	VECTOR_SET(BgL_vargsz00_2398, BgL_iz00_2427,
																		BgL_arg2149z00_2437);
																}
														}
												}
												{
													obj_t BgL_slotsz00_4897;
													long BgL_iz00_4895;

													BgL_iz00_4895 = (BgL_iz00_2427 + 1L);
													BgL_slotsz00_4897 = CDR(BgL_slotsz00_2428);
													BgL_slotsz00_2428 = BgL_slotsz00_4897;
													BgL_iz00_2427 = BgL_iz00_4895;
													goto BgL_zc3z04anonymousza32142ze3z87_2429;
												}
											}
										else
											{	/* Expand/object.scm 263 */
												((bool_t) 0);
											}
									}
									BgL_argsz00_1924 =
										BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
										(BgL_vargsz00_2398);
								}
								{	/* Expand/object.scm 315 */

									{
										obj_t BgL_ll1285z00_1926;
										obj_t BgL_ll1286z00_1927;

										BgL_ll1285z00_1926 = BgL_argsz00_1924;
										BgL_ll1286z00_1927 = BgL_slotsz00_20;
									BgL_zc3z04anonymousza31735ze3z87_1928:
										if (NULLP(BgL_ll1285z00_1926))
											{	/* Expand/object.scm 317 */
												((bool_t) 1);
											}
										else
											{	/* Expand/object.scm 317 */
												{	/* Expand/object.scm 318 */
													obj_t BgL_az00_1930;
													obj_t BgL_sz00_1931;

													BgL_az00_1930 = CAR(((obj_t) BgL_ll1285z00_1926));
													BgL_sz00_1931 = CAR(((obj_t) BgL_ll1286z00_1927));
													{	/* Expand/object.scm 318 */
														bool_t BgL_test2783z00_4906;

														if (CBOOL(CAR(((obj_t) BgL_az00_1930))))
															{	/* Expand/object.scm 318 */
																BgL_test2783z00_4906 = ((bool_t) 1);
															}
														else
															{	/* Expand/object.scm 318 */
																BgL_test2783z00_4906 =
																	BGl_slotzd2virtualzf3z21zzobject_slotsz00(
																	((BgL_slotz00_bglt) BgL_sz00_1931));
															}
														if (BgL_test2783z00_4906)
															{	/* Expand/object.scm 318 */
																BFALSE;
															}
														else
															{	/* Expand/object.scm 321 */
																obj_t BgL_arg1739z00_1934;

																{	/* Expand/object.scm 321 */
																	obj_t BgL_arg1740z00_1935;

																	BgL_arg1740z00_1935 =
																		(((BgL_slotz00_bglt) COBJECT(
																				((BgL_slotz00_bglt) BgL_sz00_1931)))->
																		BgL_idz00);
																	{	/* Expand/object.scm 321 */
																		obj_t BgL_list1741z00_1936;

																		BgL_list1741z00_1936 =
																			MAKE_YOUNG_PAIR(BgL_arg1740z00_1935,
																			BNIL);
																		BgL_arg1739z00_1934 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string2676z00zzexpand_objectz00,
																			BgL_list1741z00_1936);
																	}
																}
																BGl_errorz00zz__errorz00(BgL_opz00_17,
																	BgL_arg1739z00_1934, BgL_xz00_22);
															}
													}
												}
												{	/* Expand/object.scm 317 */
													obj_t BgL_arg1746z00_1938;
													obj_t BgL_arg1747z00_1939;

													BgL_arg1746z00_1938 =
														CDR(((obj_t) BgL_ll1285z00_1926));
													BgL_arg1747z00_1939 =
														CDR(((obj_t) BgL_ll1286z00_1927));
													{
														obj_t BgL_ll1286z00_4923;
														obj_t BgL_ll1285z00_4922;

														BgL_ll1285z00_4922 = BgL_arg1746z00_1938;
														BgL_ll1286z00_4923 = BgL_arg1747z00_1939;
														BgL_ll1286z00_1927 = BgL_ll1286z00_4923;
														BgL_ll1285z00_1926 = BgL_ll1285z00_4922;
														goto BgL_zc3z04anonymousza31735ze3z87_1928;
													}
												}
											}
									}
									{	/* Expand/object.scm 326 */
										obj_t BgL_arg1748z00_1941;

										{	/* Expand/object.scm 326 */
											obj_t BgL_arg1749z00_1942;
											obj_t BgL_arg1750z00_1943;

											{	/* Expand/object.scm 326 */
												obj_t BgL_arg1751z00_1944;

												{	/* Expand/object.scm 326 */
													obj_t BgL_arg1752z00_1945;

													BgL_arg1752z00_1945 =
														MAKE_YOUNG_PAIR(BgL_initz00_21, BNIL);
													BgL_arg1751z00_1944 =
														MAKE_YOUNG_PAIR(BgL_tnewz00_1923,
														BgL_arg1752z00_1945);
												}
												BgL_arg1749z00_1942 =
													MAKE_YOUNG_PAIR(BgL_arg1751z00_1944, BNIL);
											}
											{	/* Expand/object.scm 329 */
												obj_t BgL_arg1753z00_1946;
												obj_t BgL_arg1754z00_1947;

												{	/* Expand/object.scm 329 */
													obj_t BgL_zc3z04anonymousza31762ze3z87_4158;

													BgL_zc3z04anonymousza31762ze3z87_4158 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31762ze3ze5zzexpand_objectz00,
														(int) (2L), (int) (2L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31762ze3z87_4158,
														(int) (0L), BgL_ez00_23);
													PROCEDURE_SET(BgL_zc3z04anonymousza31762ze3z87_4158,
														(int) (1L), BgL_newz00_1922);
													{	/* Expand/object.scm 328 */
														obj_t BgL_list1756z00_1949;

														{	/* Expand/object.scm 328 */
															obj_t BgL_arg1761z00_1950;

															BgL_arg1761z00_1950 =
																MAKE_YOUNG_PAIR(BgL_argsz00_1924, BNIL);
															BgL_list1756z00_1949 =
																MAKE_YOUNG_PAIR(BgL_slotsz00_20,
																BgL_arg1761z00_1950);
														}
														BgL_arg1753z00_1946 =
															BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
															(BgL_zc3z04anonymousza31762ze3z87_4158,
															BgL_list1756z00_1949);
												}}
												{	/* Expand/object.scm 336 */
													obj_t BgL_arg1798z00_1964;
													obj_t BgL_arg1799z00_1965;

													if (CBOOL
														(BGl_findzd2classzd2constructorz00zzobject_classz00(
																((BgL_typez00_bglt) BgL_classz00_19))))
														{	/* Expand/object.scm 337 */
															BgL_globalz00_bglt BgL_gz00_1967;

															{
																BgL_tclassz00_bglt BgL_auxz00_4941;

																{
																	obj_t BgL_auxz00_4942;

																	{	/* Expand/object.scm 337 */
																		BgL_objectz00_bglt BgL_tmpz00_4943;

																		BgL_tmpz00_4943 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_classz00_19));
																		BgL_auxz00_4942 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4943);
																	}
																	BgL_auxz00_4941 =
																		((BgL_tclassz00_bglt) BgL_auxz00_4942);
																}
																BgL_gz00_1967 =
																	(((BgL_tclassz00_bglt)
																		COBJECT(BgL_auxz00_4941))->BgL_holderz00);
															}
															{	/* Expand/object.scm 337 */
																obj_t BgL_nxz00_1968;

																{	/* Expand/object.scm 338 */
																	obj_t BgL_arg1806z00_1970;
																	obj_t BgL_arg1808z00_1971;

																	{	/* Expand/object.scm 338 */
																		obj_t BgL_arg1812z00_1972;
																		obj_t BgL_arg1820z00_1973;

																		{	/* Expand/object.scm 338 */
																			obj_t BgL_arg1822z00_1974;

																			{	/* Expand/object.scm 338 */
																				obj_t BgL_arg1823z00_1975;

																				BgL_arg1823z00_1975 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																					BNIL);
																				BgL_arg1822z00_1974 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																					BgL_arg1823z00_1975);
																			}
																			BgL_arg1812z00_1972 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																				BgL_arg1822z00_1974);
																		}
																		{	/* Expand/object.scm 339 */
																			obj_t BgL_arg1831z00_1976;

																			{	/* Expand/object.scm 339 */
																				obj_t BgL_arg1832z00_1977;

																				{	/* Expand/object.scm 339 */
																					obj_t BgL_arg1833z00_1978;
																					obj_t BgL_arg1834z00_1979;

																					BgL_arg1833z00_1978 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									BgL_gz00_1967)))->BgL_idz00);
																					BgL_arg1834z00_1979 =
																						MAKE_YOUNG_PAIR(((
																								(BgL_globalz00_bglt)
																								COBJECT(BgL_gz00_1967))->
																							BgL_modulez00), BNIL);
																					BgL_arg1832z00_1977 =
																						MAKE_YOUNG_PAIR(BgL_arg1833z00_1978,
																						BgL_arg1834z00_1979);
																				}
																				BgL_arg1831z00_1976 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																					BgL_arg1832z00_1977);
																			}
																			BgL_arg1820z00_1973 =
																				MAKE_YOUNG_PAIR(BgL_arg1831z00_1976,
																				BNIL);
																		}
																		BgL_arg1806z00_1970 =
																			MAKE_YOUNG_PAIR(BgL_arg1812z00_1972,
																			BgL_arg1820z00_1973);
																	}
																	BgL_arg1808z00_1971 =
																		MAKE_YOUNG_PAIR(BgL_newz00_1922, BNIL);
																	BgL_nxz00_1968 =
																		MAKE_YOUNG_PAIR(BgL_arg1806z00_1970,
																		BgL_arg1808z00_1971);
																}
																{	/* Expand/object.scm 338 */

																	{	/* Expand/object.scm 341 */
																		obj_t BgL_arg1805z00_1969;

																		BgL_arg1805z00_1969 =
																			BGl_objectzd2epairifyzd2zzexpand_objectz00
																			(BgL_nxz00_1968, BgL_xz00_22);
																		BgL_arg1798z00_1964 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_23,
																			BgL_arg1805z00_1969, BgL_ez00_23);
																	}
																}
															}
														}
													else
														{	/* Expand/object.scm 336 */
															BgL_arg1798z00_1964 = BFALSE;
														}
													{	/* Expand/object.scm 344 */
														obj_t BgL_arg1836z00_1981;
														obj_t BgL_arg1837z00_1982;

														{	/* Expand/object.scm 344 */
															obj_t BgL_zc3z04anonymousza31841ze3z87_4157;

															BgL_zc3z04anonymousza31841ze3z87_4157 =
																MAKE_FX_PROCEDURE
																(BGl_z62zc3z04anonymousza31841ze3ze5zzexpand_objectz00,
																(int) (2L), (int) (2L));
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31841ze3z87_4157,
																(int) (0L), BgL_ez00_23);
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31841ze3z87_4157,
																(int) (1L), BgL_newz00_1922);
															{	/* Expand/object.scm 343 */
																obj_t BgL_list1839z00_1984;

																{	/* Expand/object.scm 343 */
																	obj_t BgL_arg1840z00_1985;

																	BgL_arg1840z00_1985 =
																		MAKE_YOUNG_PAIR(BgL_argsz00_1924, BNIL);
																	BgL_list1839z00_1984 =
																		MAKE_YOUNG_PAIR(BgL_slotsz00_20,
																		BgL_arg1840z00_1985);
																}
																BgL_arg1836z00_1981 =
																	BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																	(BgL_zc3z04anonymousza31841ze3z87_4157,
																	BgL_list1839z00_1984);
														}}
														BgL_arg1837z00_1982 =
															MAKE_YOUNG_PAIR(BgL_newz00_1922, BNIL);
														BgL_arg1799z00_1965 =
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_arg1836z00_1981, BgL_arg1837z00_1982);
													}
													BgL_arg1754z00_1947 =
														MAKE_YOUNG_PAIR(BgL_arg1798z00_1964,
														BgL_arg1799z00_1965);
												}
												BgL_arg1750z00_1943 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1753z00_1946, BgL_arg1754z00_1947);
											}
											BgL_arg1748z00_1941 =
												MAKE_YOUNG_PAIR(BgL_arg1749z00_1942,
												BgL_arg1750z00_1943);
										}
										return
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg1748z00_1941);
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* inlinable-call?~0 */
	obj_t BGl_inlinablezd2callzf3ze70zc6zzexpand_objectz00(obj_t BgL_funz00_2003,
		obj_t BgL_modulez00_2004)
	{
		{	/* Expand/object.scm 186 */
			{
				obj_t BgL_gz00_2302;
				obj_t BgL_funz00_2313;
				obj_t BgL_modulez00_2314;

				{	/* Expand/object.scm 181 */
					obj_t BgL_gz00_2006;

					{	/* Expand/object.scm 181 */
						obj_t BgL_list1864z00_2022;

						BgL_list1864z00_2022 = MAKE_YOUNG_PAIR(BgL_modulez00_2004, BNIL);
						BgL_gz00_2006 =
							BGl_findzd2globalzd2zzast_envz00(BgL_funz00_2003,
							BgL_list1864z00_2022);
					}
					{	/* Expand/object.scm 182 */
						bool_t BgL_test2786z00_4991;

						{	/* Expand/object.scm 182 */
							bool_t BgL_test2787z00_4992;

							{	/* Expand/object.scm 182 */
								obj_t BgL_classz00_3363;

								BgL_classz00_3363 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_2006))
									{	/* Expand/object.scm 182 */
										BgL_objectz00_bglt BgL_arg1807z00_3365;

										BgL_arg1807z00_3365 = (BgL_objectz00_bglt) (BgL_gz00_2006);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Expand/object.scm 182 */
												long BgL_idxz00_3371;

												BgL_idxz00_3371 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3365);
												BgL_test2787z00_4992 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3371 + 2L)) == BgL_classz00_3363);
											}
										else
											{	/* Expand/object.scm 182 */
												bool_t BgL_res2649z00_3396;

												{	/* Expand/object.scm 182 */
													obj_t BgL_oclassz00_3379;

													{	/* Expand/object.scm 182 */
														obj_t BgL_arg1815z00_3387;
														long BgL_arg1816z00_3388;

														BgL_arg1815z00_3387 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Expand/object.scm 182 */
															long BgL_arg1817z00_3389;

															BgL_arg1817z00_3389 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3365);
															BgL_arg1816z00_3388 =
																(BgL_arg1817z00_3389 - OBJECT_TYPE);
														}
														BgL_oclassz00_3379 =
															VECTOR_REF(BgL_arg1815z00_3387,
															BgL_arg1816z00_3388);
													}
													{	/* Expand/object.scm 182 */
														bool_t BgL__ortest_1115z00_3380;

														BgL__ortest_1115z00_3380 =
															(BgL_classz00_3363 == BgL_oclassz00_3379);
														if (BgL__ortest_1115z00_3380)
															{	/* Expand/object.scm 182 */
																BgL_res2649z00_3396 = BgL__ortest_1115z00_3380;
															}
														else
															{	/* Expand/object.scm 182 */
																long BgL_odepthz00_3381;

																{	/* Expand/object.scm 182 */
																	obj_t BgL_arg1804z00_3382;

																	BgL_arg1804z00_3382 = (BgL_oclassz00_3379);
																	BgL_odepthz00_3381 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3382);
																}
																if ((2L < BgL_odepthz00_3381))
																	{	/* Expand/object.scm 182 */
																		obj_t BgL_arg1802z00_3384;

																		{	/* Expand/object.scm 182 */
																			obj_t BgL_arg1803z00_3385;

																			BgL_arg1803z00_3385 =
																				(BgL_oclassz00_3379);
																			BgL_arg1802z00_3384 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3385, 2L);
																		}
																		BgL_res2649z00_3396 =
																			(BgL_arg1802z00_3384 ==
																			BgL_classz00_3363);
																	}
																else
																	{	/* Expand/object.scm 182 */
																		BgL_res2649z00_3396 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2787z00_4992 = BgL_res2649z00_3396;
											}
									}
								else
									{	/* Expand/object.scm 182 */
										BgL_test2787z00_4992 = ((bool_t) 0);
									}
							}
							if (BgL_test2787z00_4992)
								{	/* Expand/object.scm 182 */
									BgL_valuez00_bglt BgL_arg1863z00_2021;

									BgL_arg1863z00_2021 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_2006))))->
										BgL_valuez00);
									{	/* Expand/object.scm 182 */
										obj_t BgL_classz00_3398;

										BgL_classz00_3398 = BGl_sfunz00zzast_varz00;
										{	/* Expand/object.scm 182 */
											BgL_objectz00_bglt BgL_arg1807z00_3400;

											{	/* Expand/object.scm 182 */
												obj_t BgL_tmpz00_5018;

												BgL_tmpz00_5018 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1863z00_2021));
												BgL_arg1807z00_3400 =
													(BgL_objectz00_bglt) (BgL_tmpz00_5018);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Expand/object.scm 182 */
													long BgL_idxz00_3406;

													BgL_idxz00_3406 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3400);
													BgL_test2786z00_4991 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3406 + 3L)) == BgL_classz00_3398);
												}
											else
												{	/* Expand/object.scm 182 */
													bool_t BgL_res2650z00_3431;

													{	/* Expand/object.scm 182 */
														obj_t BgL_oclassz00_3414;

														{	/* Expand/object.scm 182 */
															obj_t BgL_arg1815z00_3422;
															long BgL_arg1816z00_3423;

															BgL_arg1815z00_3422 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Expand/object.scm 182 */
																long BgL_arg1817z00_3424;

																BgL_arg1817z00_3424 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3400);
																BgL_arg1816z00_3423 =
																	(BgL_arg1817z00_3424 - OBJECT_TYPE);
															}
															BgL_oclassz00_3414 =
																VECTOR_REF(BgL_arg1815z00_3422,
																BgL_arg1816z00_3423);
														}
														{	/* Expand/object.scm 182 */
															bool_t BgL__ortest_1115z00_3415;

															BgL__ortest_1115z00_3415 =
																(BgL_classz00_3398 == BgL_oclassz00_3414);
															if (BgL__ortest_1115z00_3415)
																{	/* Expand/object.scm 182 */
																	BgL_res2650z00_3431 =
																		BgL__ortest_1115z00_3415;
																}
															else
																{	/* Expand/object.scm 182 */
																	long BgL_odepthz00_3416;

																	{	/* Expand/object.scm 182 */
																		obj_t BgL_arg1804z00_3417;

																		BgL_arg1804z00_3417 = (BgL_oclassz00_3414);
																		BgL_odepthz00_3416 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3417);
																	}
																	if ((3L < BgL_odepthz00_3416))
																		{	/* Expand/object.scm 182 */
																			obj_t BgL_arg1802z00_3419;

																			{	/* Expand/object.scm 182 */
																				obj_t BgL_arg1803z00_3420;

																				BgL_arg1803z00_3420 =
																					(BgL_oclassz00_3414);
																				BgL_arg1802z00_3419 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3420, 3L);
																			}
																			BgL_res2650z00_3431 =
																				(BgL_arg1802z00_3419 ==
																				BgL_classz00_3398);
																		}
																	else
																		{	/* Expand/object.scm 182 */
																			BgL_res2650z00_3431 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2786z00_4991 = BgL_res2650z00_3431;
												}
										}
									}
								}
							else
								{	/* Expand/object.scm 182 */
									BgL_test2786z00_4991 = ((bool_t) 0);
								}
						}
						if (BgL_test2786z00_4991)
							{	/* Expand/object.scm 183 */
								bool_t BgL__ortest_1111z00_2010;

								if (CBOOL(
										(((BgL_funz00_bglt) COBJECT(
													((BgL_funz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_gz00_2006))))->
															BgL_valuez00))))->BgL_sidezd2effectzd2)))
									{	/* Expand/object.scm 183 */
										BgL__ortest_1111z00_2010 = ((bool_t) 0);
									}
								else
									{	/* Expand/object.scm 183 */
										BgL__ortest_1111z00_2010 = ((bool_t) 1);
									}
								if (BgL__ortest_1111z00_2010)
									{	/* Expand/object.scm 183 */
										return BBOOL(BgL__ortest_1111z00_2010);
									}
								else
									{	/* Expand/object.scm 184 */
										obj_t BgL__ortest_1112z00_2011;

										BgL__ortest_1112z00_2011 =
											BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF
											(17),
											(((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
															BgL_gz00_2006)))->BgL_pragmaz00));
										if (CBOOL(BgL__ortest_1112z00_2011))
											{	/* Expand/object.scm 184 */
												return BgL__ortest_1112z00_2011;
											}
										else
											{	/* Expand/object.scm 184 */
												if (
													((((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_gz00_2006))))->
																			BgL_valuez00))))->BgL_classz00) ==
														CNST_TABLE_REF(18)))
													{	/* Expand/object.scm 186 */
														obj_t BgL_arg1856z00_2013;

														BgL_gz00_2302 = BgL_gz00_2006;
														if (
															((((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt)
																				(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_gz00_2302))))->
																					BgL_valuez00))))->BgL_bodyz00) ==
																BUNSPEC))
															{	/* Expand/object.scm 212 */
																obj_t BgL__ortest_1125z00_2307;

																{	/* Expand/object.scm 212 */
																	obj_t BgL_arg2060z00_2308;
																	obj_t BgL_arg2061z00_2309;

																	BgL_arg2060z00_2308 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_gz00_2302))))->BgL_idz00);
																	BgL_arg2061z00_2309 =
																		(((BgL_globalz00_bglt)
																			COBJECT(((BgL_globalz00_bglt)
																					BgL_gz00_2302)))->BgL_modulez00);
																	BgL_funz00_2313 = BgL_arg2060z00_2308;
																	BgL_modulez00_2314 = BgL_arg2061z00_2309;
																	{	/* Expand/object.scm 216 */
																		obj_t BgL_g1283z00_2316;

																		BgL_g1283z00_2316 =
																			BGl_inlinezd2definitionzd2queuez00zzread_inlinez00
																			();
																		{
																			obj_t BgL_l1281z00_2318;

																			BgL_l1281z00_2318 = BgL_g1283z00_2316;
																		BgL_zc3z04anonymousza32066ze3z87_2319:
																			if (NULLP(BgL_l1281z00_2318))
																				{	/* Expand/object.scm 221 */
																					BgL__ortest_1125z00_2307 = BFALSE;
																				}
																			else
																				{	/* Expand/object.scm 217 */
																					obj_t BgL__ortest_1284z00_2321;

																					{	/* Expand/object.scm 217 */
																						obj_t BgL_defz00_2323;

																						BgL_defz00_2323 =
																							CAR(((obj_t) BgL_l1281z00_2318));
																						if (PAIRP(BgL_defz00_2323))
																							{	/* Expand/object.scm 217 */
																								obj_t BgL_cdrzd2638zd2_2330;

																								BgL_cdrzd2638zd2_2330 =
																									CDR(
																									((obj_t) BgL_defz00_2323));
																								if (
																									(CAR(
																											((obj_t) BgL_defz00_2323))
																										== CNST_TABLE_REF(15)))
																									{	/* Expand/object.scm 217 */
																										if (PAIRP
																											(BgL_cdrzd2638zd2_2330))
																											{	/* Expand/object.scm 217 */
																												obj_t
																													BgL_carzd2642zd2_2334;
																												obj_t
																													BgL_cdrzd2643zd2_2335;
																												BgL_carzd2642zd2_2334 =
																													CAR
																													(BgL_cdrzd2638zd2_2330);
																												BgL_cdrzd2643zd2_2335 =
																													CDR
																													(BgL_cdrzd2638zd2_2330);
																												if (PAIRP
																													(BgL_carzd2642zd2_2334))
																													{	/* Expand/object.scm 217 */
																														obj_t
																															BgL_carzd2646zd2_2337;
																														BgL_carzd2646zd2_2337
																															=
																															CAR
																															(BgL_carzd2642zd2_2334);
																														if (PAIRP
																															(BgL_carzd2646zd2_2337))
																															{	/* Expand/object.scm 217 */
																																obj_t
																																	BgL_cdrzd2651zd2_2339;
																																BgL_cdrzd2651zd2_2339
																																	=
																																	CDR
																																	(BgL_carzd2646zd2_2337);
																																if ((CAR
																																		(BgL_carzd2646zd2_2337)
																																		==
																																		CNST_TABLE_REF
																																		(10)))
																																	{	/* Expand/object.scm 217 */
																																		if (PAIRP
																																			(BgL_cdrzd2651zd2_2339))
																																			{	/* Expand/object.scm 217 */
																																				obj_t
																																					BgL_cdrzd2655zd2_2343;
																																				BgL_cdrzd2655zd2_2343
																																					=
																																					CDR
																																					(BgL_cdrzd2651zd2_2339);
																																				if (PAIRP(BgL_cdrzd2655zd2_2343))
																																					{	/* Expand/object.scm 217 */
																																						if (NULLP(CDR(BgL_cdrzd2655zd2_2343)))
																																							{	/* Expand/object.scm 217 */
																																								if (NULLP(CDR(BgL_carzd2642zd2_2334)))
																																									{	/* Expand/object.scm 217 */
																																										if (PAIRP(BgL_cdrzd2643zd2_2335))
																																											{	/* Expand/object.scm 217 */
																																												if (NULLP(CDR(BgL_cdrzd2643zd2_2335)))
																																													{	/* Expand/object.scm 219 */
																																														bool_t
																																															BgL_test2813z00_5120;
																																														if ((CAR(BgL_cdrzd2651zd2_2339) == BgL_funz00_2313))
																																															{	/* Expand/object.scm 219 */
																																																BgL_test2813z00_5120
																																																	=
																																																	(CAR
																																																	(BgL_cdrzd2655zd2_2343)
																																																	==
																																																	BgL_modulez00_2314);
																																															}
																																														else
																																															{	/* Expand/object.scm 219 */
																																																BgL_test2813z00_5120
																																																	=
																																																	(
																																																	(bool_t)
																																																	0);
																																															}
																																														if (BgL_test2813z00_5120)
																																															{	/* Expand/object.scm 219 */
																																																BgL__ortest_1284z00_2321
																																																	=
																																																	CAR
																																																	(BgL_cdrzd2643zd2_2335);
																																															}
																																														else
																																															{	/* Expand/object.scm 219 */
																																																BgL__ortest_1284z00_2321
																																																	=
																																																	BFALSE;
																																															}
																																													}
																																												else
																																													{	/* Expand/object.scm 217 */
																																														BgL__ortest_1284z00_2321
																																															=
																																															BFALSE;
																																													}
																																											}
																																										else
																																											{	/* Expand/object.scm 217 */
																																												BgL__ortest_1284z00_2321
																																													=
																																													BFALSE;
																																											}
																																									}
																																								else
																																									{	/* Expand/object.scm 217 */
																																										BgL__ortest_1284z00_2321
																																											=
																																											BFALSE;
																																									}
																																							}
																																						else
																																							{	/* Expand/object.scm 217 */
																																								BgL__ortest_1284z00_2321
																																									=
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Expand/object.scm 217 */
																																						BgL__ortest_1284z00_2321
																																							=
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Expand/object.scm 217 */
																																				BgL__ortest_1284z00_2321
																																					=
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Expand/object.scm 217 */
																																		BgL__ortest_1284z00_2321
																																			= BFALSE;
																																	}
																															}
																														else
																															{	/* Expand/object.scm 217 */
																																BgL__ortest_1284z00_2321
																																	= BFALSE;
																															}
																													}
																												else
																													{	/* Expand/object.scm 217 */
																														BgL__ortest_1284z00_2321
																															= BFALSE;
																													}
																											}
																										else
																											{	/* Expand/object.scm 217 */
																												BgL__ortest_1284z00_2321
																													= BFALSE;
																											}
																									}
																								else
																									{	/* Expand/object.scm 217 */
																										BgL__ortest_1284z00_2321 =
																											BFALSE;
																									}
																							}
																						else
																							{	/* Expand/object.scm 217 */
																								BgL__ortest_1284z00_2321 =
																									BFALSE;
																							}
																					}
																					if (CBOOL(BgL__ortest_1284z00_2321))
																						{	/* Expand/object.scm 221 */
																							BgL__ortest_1125z00_2307 =
																								BgL__ortest_1284z00_2321;
																						}
																					else
																						{
																							obj_t BgL_l1281z00_5129;

																							BgL_l1281z00_5129 =
																								CDR(
																								((obj_t) BgL_l1281z00_2318));
																							BgL_l1281z00_2318 =
																								BgL_l1281z00_5129;
																							goto
																								BgL_zc3z04anonymousza32066ze3z87_2319;
																						}
																				}
																		}
																	}
																}
																if (CBOOL(BgL__ortest_1125z00_2307))
																	{	/* Expand/object.scm 212 */
																		BgL_arg1856z00_2013 =
																			BgL__ortest_1125z00_2307;
																	}
																else
																	{	/* Expand/object.scm 212 */
																		BgL_arg1856z00_2013 = CNST_TABLE_REF(16);
																	}
															}
														else
															{	/* Expand/object.scm 211 */
																BgL_arg1856z00_2013 =
																	(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt)
																				(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_gz00_2302))))->
																					BgL_valuez00))))->BgL_bodyz00);
															}
														return
															BBOOL(BGl_inlinablezf3ze70z14zzexpand_objectz00
															(BgL_arg1856z00_2013, BgL_modulez00_2004));
													}
												else
													{	/* Expand/object.scm 185 */
														return BFALSE;
													}
											}
									}
							}
						else
							{	/* Expand/object.scm 182 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* inlinable?~0 */
	bool_t BGl_inlinablezf3ze70z14zzexpand_objectz00(obj_t BgL_nz00_2023,
		obj_t BgL_modulez00_2024)
	{
		{	/* Expand/object.scm 208 */
		BGl_inlinablezf3ze70z14zzexpand_objectz00:
			{	/* Expand/object.scm 189 */
				bool_t BgL__ortest_1114z00_2026;

				BgL__ortest_1114z00_2026 =
					BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_nz00_2023);
				if (BgL__ortest_1114z00_2026)
					{	/* Expand/object.scm 189 */
						return BgL__ortest_1114z00_2026;
					}
				else
					{	/* Expand/object.scm 190 */
						bool_t BgL__ortest_1116z00_2027;

						BgL__ortest_1116z00_2027 = STRINGP(BgL_nz00_2023);
						if (BgL__ortest_1116z00_2027)
							{	/* Expand/object.scm 190 */
								return BgL__ortest_1116z00_2027;
							}
						else
							{	/* Expand/object.scm 191 */
								bool_t BgL__ortest_1118z00_2028;

								BgL__ortest_1118z00_2028 = CHARP(BgL_nz00_2023);
								if (BgL__ortest_1118z00_2028)
									{	/* Expand/object.scm 191 */
										return BgL__ortest_1118z00_2028;
									}
								else
									{	/* Expand/object.scm 192 */
										bool_t BgL__ortest_1119z00_2029;

										BgL__ortest_1119z00_2029 = BOOLEANP(BgL_nz00_2023);
										if (BgL__ortest_1119z00_2029)
											{	/* Expand/object.scm 192 */
												return BgL__ortest_1119z00_2029;
											}
										else
											{	/* Expand/object.scm 193 */
												bool_t BgL__ortest_1120z00_2030;

												BgL__ortest_1120z00_2030 = (BgL_nz00_2023 == BUNSPEC);
												if (BgL__ortest_1120z00_2030)
													{	/* Expand/object.scm 193 */
														return BgL__ortest_1120z00_2030;
													}
												else
													{	/* Expand/object.scm 194 */
														bool_t BgL__ortest_1121z00_2031;

														{	/* Expand/object.scm 194 */
															obj_t BgL_classz00_3437;

															BgL_classz00_3437 = BGl_literalz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_nz00_2023))
																{	/* Expand/object.scm 194 */
																	BgL_objectz00_bglt BgL_arg1807z00_3439;

																	BgL_arg1807z00_3439 =
																		(BgL_objectz00_bglt) (BgL_nz00_2023);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Expand/object.scm 194 */
																			long BgL_idxz00_3445;

																			BgL_idxz00_3445 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3439);
																			BgL__ortest_1121z00_2031 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3445 + 3L)) ==
																				BgL_classz00_3437);
																		}
																	else
																		{	/* Expand/object.scm 194 */
																			bool_t BgL_res2651z00_3470;

																			{	/* Expand/object.scm 194 */
																				obj_t BgL_oclassz00_3453;

																				{	/* Expand/object.scm 194 */
																					obj_t BgL_arg1815z00_3461;
																					long BgL_arg1816z00_3462;

																					BgL_arg1815z00_3461 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Expand/object.scm 194 */
																						long BgL_arg1817z00_3463;

																						BgL_arg1817z00_3463 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3439);
																						BgL_arg1816z00_3462 =
																							(BgL_arg1817z00_3463 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3453 =
																						VECTOR_REF(BgL_arg1815z00_3461,
																						BgL_arg1816z00_3462);
																				}
																				{	/* Expand/object.scm 194 */
																					bool_t BgL__ortest_1115z00_3454;

																					BgL__ortest_1115z00_3454 =
																						(BgL_classz00_3437 ==
																						BgL_oclassz00_3453);
																					if (BgL__ortest_1115z00_3454)
																						{	/* Expand/object.scm 194 */
																							BgL_res2651z00_3470 =
																								BgL__ortest_1115z00_3454;
																						}
																					else
																						{	/* Expand/object.scm 194 */
																							long BgL_odepthz00_3455;

																							{	/* Expand/object.scm 194 */
																								obj_t BgL_arg1804z00_3456;

																								BgL_arg1804z00_3456 =
																									(BgL_oclassz00_3453);
																								BgL_odepthz00_3455 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3456);
																							}
																							if ((3L < BgL_odepthz00_3455))
																								{	/* Expand/object.scm 194 */
																									obj_t BgL_arg1802z00_3458;

																									{	/* Expand/object.scm 194 */
																										obj_t BgL_arg1803z00_3459;

																										BgL_arg1803z00_3459 =
																											(BgL_oclassz00_3453);
																										BgL_arg1802z00_3458 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3459, 3L);
																									}
																									BgL_res2651z00_3470 =
																										(BgL_arg1802z00_3458 ==
																										BgL_classz00_3437);
																								}
																							else
																								{	/* Expand/object.scm 194 */
																									BgL_res2651z00_3470 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL__ortest_1121z00_2031 =
																				BgL_res2651z00_3470;
																		}
																}
															else
																{	/* Expand/object.scm 194 */
																	BgL__ortest_1121z00_2031 = ((bool_t) 0);
																}
														}
														if (BgL__ortest_1121z00_2031)
															{	/* Expand/object.scm 194 */
																return BgL__ortest_1121z00_2031;
															}
														else
															{
																obj_t BgL_funz00_2040;
																obj_t BgL_argsz00_2041;
																obj_t BgL_funz00_2043;
																obj_t BgL_modz00_2044;
																obj_t BgL_argsz00_2045;

																if (PAIRP(BgL_nz00_2023))
																	{	/* Expand/object.scm 195 */
																		if (
																			(CAR(
																					((obj_t) BgL_nz00_2023)) ==
																				CNST_TABLE_REF(19)))
																			{	/* Expand/object.scm 195 */
																				return ((bool_t) 1);
																			}
																		else
																			{	/* Expand/object.scm 195 */
																				if (
																					(CAR(
																							((obj_t) BgL_nz00_2023)) ==
																						CNST_TABLE_REF(20)))
																					{	/* Expand/object.scm 195 */
																						obj_t BgL_arg1872z00_2054;

																						BgL_arg1872z00_2054 =
																							CDR(((obj_t) BgL_nz00_2023));
																						{
																							obj_t BgL_nz00_5189;

																							BgL_nz00_5189 =
																								BgL_arg1872z00_2054;
																							BgL_nz00_2023 = BgL_nz00_5189;
																							goto
																								BGl_inlinablezf3ze70z14zzexpand_objectz00;
																						}
																					}
																				else
																					{	/* Expand/object.scm 195 */
																						obj_t BgL_carzd2208zd2_2055;
																						obj_t BgL_cdrzd2209zd2_2056;

																						BgL_carzd2208zd2_2055 =
																							CAR(((obj_t) BgL_nz00_2023));
																						BgL_cdrzd2209zd2_2056 =
																							CDR(((obj_t) BgL_nz00_2023));
																						{

																							if (
																								(BgL_carzd2208zd2_2055 ==
																									CNST_TABLE_REF(23)))
																								{	/* Expand/object.scm 195 */
																								BgL_kapzd2210zd2_2057:
																									if (PAIRP
																										(BgL_cdrzd2209zd2_2056))
																										{	/* Expand/object.scm 195 */
																											obj_t
																												BgL_cdrzd2214zd2_2153;
																											BgL_cdrzd2214zd2_2153 =
																												CDR
																												(BgL_cdrzd2209zd2_2056);
																											if (PAIRP
																												(BgL_cdrzd2214zd2_2153))
																												{	/* Expand/object.scm 195 */
																													if (NULLP(CDR
																															(BgL_cdrzd2214zd2_2153)))
																														{	/* Expand/object.scm 195 */
																															obj_t
																																BgL_arg1951z00_2157;
																															obj_t
																																BgL_arg1952z00_2158;
																															BgL_arg1951z00_2157
																																=
																																CAR
																																(BgL_cdrzd2209zd2_2056);
																															BgL_arg1952z00_2158
																																=
																																CAR
																																(BgL_cdrzd2214zd2_2153);
																															if (BGl_inlinablezf3ze70z14zzexpand_objectz00(BgL_arg1951z00_2157, BgL_modulez00_2024))
																																{
																																	obj_t
																																		BgL_nz00_5209;
																																	BgL_nz00_5209
																																		=
																																		BgL_arg1952z00_2158;
																																	BgL_nz00_2023
																																		=
																																		BgL_nz00_5209;
																																	goto
																																		BGl_inlinablezf3ze70z14zzexpand_objectz00;
																																}
																															else
																																{	/* Expand/object.scm 199 */
																																	return (
																																		(bool_t) 0);
																																}
																														}
																													else
																														{	/* Expand/object.scm 195 */
																															if (
																																(CAR(
																																		((obj_t)
																																			BgL_nz00_2023))
																																	==
																																	CNST_TABLE_REF
																																	(21)))
																																{	/* Expand/object.scm 195 */
																																	return (
																																		(bool_t) 1);
																																}
																															else
																																{	/* Expand/object.scm 195 */
																																	obj_t
																																		BgL_carzd2233zd2_2161;
																																	BgL_carzd2233zd2_2161
																																		=
																																		CAR(((obj_t)
																																			BgL_nz00_2023));
																																	if (SYMBOLP
																																		(BgL_carzd2233zd2_2161))
																																		{	/* Expand/object.scm 195 */
																																			obj_t
																																				BgL_arg1956z00_2163;
																																			BgL_arg1956z00_2163
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_nz00_2023));
																																			BgL_funz00_2040
																																				=
																																				BgL_carzd2233zd2_2161;
																																			BgL_argsz00_2041
																																				=
																																				BgL_arg1956z00_2163;
																																		BgL_tagzd2179zd2_2042:
																																			{	/* Expand/object.scm 203 */
																																				obj_t
																																					BgL__andtest_1123z00_2282;
																																				BgL__andtest_1123z00_2282
																																					=
																																					BGl_inlinablezd2callzf3ze70zc6zzexpand_objectz00
																																					(BgL_funz00_2040,
																																					BgL_modulez00_2024);
																																				if (CBOOL(BgL__andtest_1123z00_2282))
																																					{
																																						obj_t
																																							BgL_l1275z00_2284;
																																						BgL_l1275z00_2284
																																							=
																																							BgL_argsz00_2041;
																																					BgL_zc3z04anonymousza32048ze3z87_2285:
																																						if (NULLP(BgL_l1275z00_2284))
																																							{	/* Expand/object.scm 204 */
																																								return
																																									(
																																									(bool_t)
																																									1);
																																							}
																																						else
																																							{	/* Expand/object.scm 204 */
																																								bool_t
																																									BgL_test2839z00_5226;
																																								{	/* Expand/object.scm 204 */
																																									obj_t
																																										BgL_az00_2290;
																																									BgL_az00_2290
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_l1275z00_2284));
																																									BgL_test2839z00_5226
																																										=
																																										BGl_inlinablezf3ze70z14zzexpand_objectz00
																																										(BgL_az00_2290,
																																										BgL_modulez00_2024);
																																								}
																																								if (BgL_test2839z00_5226)
																																									{	/* Expand/object.scm 204 */
																																										obj_t
																																											BgL_arg2050z00_2289;
																																										BgL_arg2050z00_2289
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_l1275z00_2284));
																																										{
																																											obj_t
																																												BgL_l1275z00_5232;
																																											BgL_l1275z00_5232
																																												=
																																												BgL_arg2050z00_2289;
																																											BgL_l1275z00_2284
																																												=
																																												BgL_l1275z00_5232;
																																											goto
																																												BgL_zc3z04anonymousza32048ze3z87_2285;
																																										}
																																									}
																																								else
																																									{	/* Expand/object.scm 204 */
																																										return
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																					}
																																				else
																																					{	/* Expand/object.scm 203 */
																																						return
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		}
																																	else
																																		{	/* Expand/object.scm 195 */
																																			if (PAIRP
																																				(BgL_carzd2233zd2_2161))
																																				{	/* Expand/object.scm 195 */
																																					obj_t
																																						BgL_cdrzd2250zd2_2166;
																																					BgL_cdrzd2250zd2_2166
																																						=
																																						CDR
																																						(BgL_carzd2233zd2_2161);
																																					if (
																																						(CAR
																																							(BgL_carzd2233zd2_2161)
																																							==
																																							CNST_TABLE_REF
																																							(10)))
																																						{	/* Expand/object.scm 195 */
																																							if (PAIRP(BgL_cdrzd2250zd2_2166))
																																								{	/* Expand/object.scm 195 */
																																									obj_t
																																										BgL_carzd2253zd2_2170;
																																									obj_t
																																										BgL_cdrzd2254zd2_2171;
																																									BgL_carzd2253zd2_2170
																																										=
																																										CAR
																																										(BgL_cdrzd2250zd2_2166);
																																									BgL_cdrzd2254zd2_2171
																																										=
																																										CDR
																																										(BgL_cdrzd2250zd2_2166);
																																									if (SYMBOLP(BgL_carzd2253zd2_2170))
																																										{	/* Expand/object.scm 195 */
																																											if (PAIRP(BgL_cdrzd2254zd2_2171))
																																												{	/* Expand/object.scm 195 */
																																													obj_t
																																														BgL_carzd2259zd2_2174;
																																													BgL_carzd2259zd2_2174
																																														=
																																														CAR
																																														(BgL_cdrzd2254zd2_2171);
																																													if (SYMBOLP(BgL_carzd2259zd2_2174))
																																														{	/* Expand/object.scm 195 */
																																															if (NULLP(CDR(BgL_cdrzd2254zd2_2171)))
																																																{	/* Expand/object.scm 195 */
																																																	obj_t
																																																		BgL_arg1966z00_2178;
																																																	BgL_arg1966z00_2178
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_nz00_2023));
																																																	BgL_funz00_2043
																																																		=
																																																		BgL_carzd2253zd2_2170;
																																																	BgL_modz00_2044
																																																		=
																																																		BgL_carzd2259zd2_2174;
																																																	BgL_argsz00_2045
																																																		=
																																																		BgL_arg1966z00_2178;
																																																BgL_tagzd2180zd2_2046:
																																																	{	/* Expand/object.scm 206 */
																																																		obj_t
																																																			BgL__andtest_1124z00_2292;
																																																		BgL__andtest_1124z00_2292
																																																			=
																																																			BGl_inlinablezd2callzf3ze70zc6zzexpand_objectz00
																																																			(BgL_funz00_2043,
																																																			BgL_modz00_2044);
																																																		if (CBOOL(BgL__andtest_1124z00_2292))
																																																			{
																																																				obj_t
																																																					BgL_l1278z00_2294;
																																																				BgL_l1278z00_2294
																																																					=
																																																					BgL_argsz00_2045;
																																																			BgL_zc3z04anonymousza32051ze3z87_2295:
																																																				if (NULLP(BgL_l1278z00_2294))
																																																					{	/* Expand/object.scm 207 */
																																																						return
																																																							(
																																																							(bool_t)
																																																							1);
																																																					}
																																																				else
																																																					{	/* Expand/object.scm 207 */
																																																						bool_t
																																																							BgL_test2849z00_5261;
																																																						{	/* Expand/object.scm 207 */
																																																							obj_t
																																																								BgL_az00_2300;
																																																							BgL_az00_2300
																																																								=
																																																								CAR
																																																								(
																																																								((obj_t) BgL_l1278z00_2294));
																																																							BgL_test2849z00_5261
																																																								=
																																																								BGl_inlinablezf3ze70z14zzexpand_objectz00
																																																								(BgL_az00_2300,
																																																								BgL_modulez00_2024);
																																																						}
																																																						if (BgL_test2849z00_5261)
																																																							{	/* Expand/object.scm 207 */
																																																								obj_t
																																																									BgL_arg2055z00_2299;
																																																								BgL_arg2055z00_2299
																																																									=
																																																									CDR
																																																									(
																																																									((obj_t) BgL_l1278z00_2294));
																																																								{
																																																									obj_t
																																																										BgL_l1278z00_5267;
																																																									BgL_l1278z00_5267
																																																										=
																																																										BgL_arg2055z00_2299;
																																																									BgL_l1278z00_2294
																																																										=
																																																										BgL_l1278z00_5267;
																																																									goto
																																																										BgL_zc3z04anonymousza32051ze3z87_2295;
																																																								}
																																																							}
																																																						else
																																																							{	/* Expand/object.scm 207 */
																																																								return
																																																									(
																																																									(bool_t)
																																																									0);
																																																							}
																																																					}
																																																			}
																																																		else
																																																			{	/* Expand/object.scm 206 */
																																																				return
																																																					(
																																																					(bool_t)
																																																					0);
																																																			}
																																																	}
																																																}
																																															else
																																																{	/* Expand/object.scm 195 */
																																																	return
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																													else
																																														{	/* Expand/object.scm 195 */
																																															return
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																											else
																																												{	/* Expand/object.scm 195 */
																																													return
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																									else
																																										{	/* Expand/object.scm 195 */
																																											return
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Expand/object.scm 195 */
																																									return
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					else
																																						{	/* Expand/object.scm 195 */
																																							return
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Expand/object.scm 195 */
																																					return
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																														}
																												}
																											else
																												{	/* Expand/object.scm 195 */
																													if (
																														(CAR(
																																((obj_t)
																																	BgL_nz00_2023))
																															==
																															CNST_TABLE_REF
																															(21)))
																														{	/* Expand/object.scm 195 */
																															return ((bool_t)
																																1);
																														}
																													else
																														{	/* Expand/object.scm 195 */
																															obj_t
																																BgL_cdrzd2274zd2_2185;
																															BgL_cdrzd2274zd2_2185
																																=
																																CDR(((obj_t)
																																	BgL_nz00_2023));
																															if ((CAR(((obj_t)
																																			BgL_nz00_2023))
																																	==
																																	CNST_TABLE_REF
																																	(22)))
																																{	/* Expand/object.scm 195 */
																																	bool_t
																																		BgL_test2852z00_5280;
																																	{	/* Expand/object.scm 195 */
																																		obj_t
																																			BgL_tmpz00_5281;
																																		BgL_tmpz00_5281
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd2274zd2_2185));
																																		BgL_test2852z00_5280
																																			=
																																			SYMBOLP
																																			(BgL_tmpz00_5281);
																																	}
																																	if (BgL_test2852z00_5280)
																																		{	/* Expand/object.scm 195 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd2274zd2_2185))))
																																				{	/* Expand/object.scm 195 */
																																					return
																																						(
																																						(bool_t)
																																						1);
																																				}
																																			else
																																				{	/* Expand/object.scm 195 */
																																					obj_t
																																						BgL_carzd2282zd2_2192;
																																					BgL_carzd2282zd2_2192
																																						=
																																						CAR(
																																						((obj_t) BgL_nz00_2023));
																																					if (SYMBOLP(BgL_carzd2282zd2_2192))
																																						{
																																							obj_t
																																								BgL_argsz00_5294;
																																							obj_t
																																								BgL_funz00_5293;
																																							BgL_funz00_5293
																																								=
																																								BgL_carzd2282zd2_2192;
																																							BgL_argsz00_5294
																																								=
																																								BgL_cdrzd2274zd2_2185;
																																							BgL_argsz00_2041
																																								=
																																								BgL_argsz00_5294;
																																							BgL_funz00_2040
																																								=
																																								BgL_funz00_5293;
																																							goto
																																								BgL_tagzd2179zd2_2042;
																																						}
																																					else
																																						{	/* Expand/object.scm 195 */
																																							if (PAIRP(BgL_carzd2282zd2_2192))
																																								{	/* Expand/object.scm 195 */
																																									obj_t
																																										BgL_cdrzd2299zd2_2197;
																																									BgL_cdrzd2299zd2_2197
																																										=
																																										CDR
																																										(BgL_carzd2282zd2_2192);
																																									if ((CAR(BgL_carzd2282zd2_2192) == CNST_TABLE_REF(10)))
																																										{	/* Expand/object.scm 195 */
																																											if (PAIRP(BgL_cdrzd2299zd2_2197))
																																												{	/* Expand/object.scm 195 */
																																													obj_t
																																														BgL_carzd2302zd2_2201;
																																													obj_t
																																														BgL_cdrzd2303zd2_2202;
																																													BgL_carzd2302zd2_2201
																																														=
																																														CAR
																																														(BgL_cdrzd2299zd2_2197);
																																													BgL_cdrzd2303zd2_2202
																																														=
																																														CDR
																																														(BgL_cdrzd2299zd2_2197);
																																													if (SYMBOLP(BgL_carzd2302zd2_2201))
																																														{	/* Expand/object.scm 195 */
																																															if (PAIRP(BgL_cdrzd2303zd2_2202))
																																																{	/* Expand/object.scm 195 */
																																																	obj_t
																																																		BgL_carzd2308zd2_2205;
																																																	BgL_carzd2308zd2_2205
																																																		=
																																																		CAR
																																																		(BgL_cdrzd2303zd2_2202);
																																																	if (SYMBOLP(BgL_carzd2308zd2_2205))
																																																		{	/* Expand/object.scm 195 */
																																																			if (NULLP(CDR(BgL_cdrzd2303zd2_2202)))
																																																				{	/* Expand/object.scm 195 */
																																																					obj_t
																																																						BgL_arg1990z00_2209;
																																																					BgL_arg1990z00_2209
																																																						=
																																																						CDR
																																																						(
																																																						((obj_t) BgL_nz00_2023));
																																																					{
																																																						obj_t
																																																							BgL_argsz00_5320;
																																																						obj_t
																																																							BgL_modz00_5319;
																																																						obj_t
																																																							BgL_funz00_5318;
																																																						BgL_funz00_5318
																																																							=
																																																							BgL_carzd2302zd2_2201;
																																																						BgL_modz00_5319
																																																							=
																																																							BgL_carzd2308zd2_2205;
																																																						BgL_argsz00_5320
																																																							=
																																																							BgL_arg1990z00_2209;
																																																						BgL_argsz00_2045
																																																							=
																																																							BgL_argsz00_5320;
																																																						BgL_modz00_2044
																																																							=
																																																							BgL_modz00_5319;
																																																						BgL_funz00_2043
																																																							=
																																																							BgL_funz00_5318;
																																																						goto
																																																							BgL_tagzd2180zd2_2046;
																																																					}
																																																				}
																																																			else
																																																				{	/* Expand/object.scm 195 */
																																																					return
																																																						(
																																																						(bool_t)
																																																						0);
																																																				}
																																																		}
																																																	else
																																																		{	/* Expand/object.scm 195 */
																																																			return
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																															else
																																																{	/* Expand/object.scm 195 */
																																																	return
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																													else
																																														{	/* Expand/object.scm 195 */
																																															return
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																											else
																																												{	/* Expand/object.scm 195 */
																																													return
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																									else
																																										{	/* Expand/object.scm 195 */
																																											return
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Expand/object.scm 195 */
																																									return
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																				}
																																		}
																																	else
																																		{	/* Expand/object.scm 195 */
																																			obj_t
																																				BgL_carzd2319zd2_2213;
																																			BgL_carzd2319zd2_2213
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_nz00_2023));
																																			if (SYMBOLP(BgL_carzd2319zd2_2213))
																																				{
																																					obj_t
																																						BgL_argsz00_5326;
																																					obj_t
																																						BgL_funz00_5325;
																																					BgL_funz00_5325
																																						=
																																						BgL_carzd2319zd2_2213;
																																					BgL_argsz00_5326
																																						=
																																						BgL_cdrzd2274zd2_2185;
																																					BgL_argsz00_2041
																																						=
																																						BgL_argsz00_5326;
																																					BgL_funz00_2040
																																						=
																																						BgL_funz00_5325;
																																					goto
																																						BgL_tagzd2179zd2_2042;
																																				}
																																			else
																																				{	/* Expand/object.scm 195 */
																																					if (PAIRP(BgL_carzd2319zd2_2213))
																																						{	/* Expand/object.scm 195 */
																																							obj_t
																																								BgL_cdrzd2336zd2_2218;
																																							BgL_cdrzd2336zd2_2218
																																								=
																																								CDR
																																								(BgL_carzd2319zd2_2213);
																																							if ((CAR(BgL_carzd2319zd2_2213) == CNST_TABLE_REF(10)))
																																								{	/* Expand/object.scm 195 */
																																									if (PAIRP(BgL_cdrzd2336zd2_2218))
																																										{	/* Expand/object.scm 195 */
																																											obj_t
																																												BgL_carzd2339zd2_2222;
																																											obj_t
																																												BgL_cdrzd2340zd2_2223;
																																											BgL_carzd2339zd2_2222
																																												=
																																												CAR
																																												(BgL_cdrzd2336zd2_2218);
																																											BgL_cdrzd2340zd2_2223
																																												=
																																												CDR
																																												(BgL_cdrzd2336zd2_2218);
																																											if (SYMBOLP(BgL_carzd2339zd2_2222))
																																												{	/* Expand/object.scm 195 */
																																													if (PAIRP(BgL_cdrzd2340zd2_2223))
																																														{	/* Expand/object.scm 195 */
																																															obj_t
																																																BgL_carzd2345zd2_2226;
																																															BgL_carzd2345zd2_2226
																																																=
																																																CAR
																																																(BgL_cdrzd2340zd2_2223);
																																															if (SYMBOLP(BgL_carzd2345zd2_2226))
																																																{	/* Expand/object.scm 195 */
																																																	if (NULLP(CDR(BgL_cdrzd2340zd2_2223)))
																																																		{	/* Expand/object.scm 195 */
																																																			obj_t
																																																				BgL_arg2006z00_2230;
																																																			BgL_arg2006z00_2230
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_nz00_2023));
																																																			{
																																																				obj_t
																																																					BgL_argsz00_5352;
																																																				obj_t
																																																					BgL_modz00_5351;
																																																				obj_t
																																																					BgL_funz00_5350;
																																																				BgL_funz00_5350
																																																					=
																																																					BgL_carzd2339zd2_2222;
																																																				BgL_modz00_5351
																																																					=
																																																					BgL_carzd2345zd2_2226;
																																																				BgL_argsz00_5352
																																																					=
																																																					BgL_arg2006z00_2230;
																																																				BgL_argsz00_2045
																																																					=
																																																					BgL_argsz00_5352;
																																																				BgL_modz00_2044
																																																					=
																																																					BgL_modz00_5351;
																																																				BgL_funz00_2043
																																																					=
																																																					BgL_funz00_5350;
																																																				goto
																																																					BgL_tagzd2180zd2_2046;
																																																			}
																																																		}
																																																	else
																																																		{	/* Expand/object.scm 195 */
																																																			return
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																															else
																																																{	/* Expand/object.scm 195 */
																																																	return
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																													else
																																														{	/* Expand/object.scm 195 */
																																															return
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																											else
																																												{	/* Expand/object.scm 195 */
																																													return
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																									else
																																										{	/* Expand/object.scm 195 */
																																											return
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Expand/object.scm 195 */
																																									return
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					else
																																						{	/* Expand/object.scm 195 */
																																							return
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																}
																															else
																																{	/* Expand/object.scm 195 */
																																	obj_t
																																		BgL_carzd2356zd2_2234;
																																	BgL_carzd2356zd2_2234
																																		=
																																		CAR(((obj_t)
																																			BgL_nz00_2023));
																																	if (SYMBOLP
																																		(BgL_carzd2356zd2_2234))
																																		{
																																			obj_t
																																				BgL_argsz00_5358;
																																			obj_t
																																				BgL_funz00_5357;
																																			BgL_funz00_5357
																																				=
																																				BgL_carzd2356zd2_2234;
																																			BgL_argsz00_5358
																																				=
																																				BgL_cdrzd2274zd2_2185;
																																			BgL_argsz00_2041
																																				=
																																				BgL_argsz00_5358;
																																			BgL_funz00_2040
																																				=
																																				BgL_funz00_5357;
																																			goto
																																				BgL_tagzd2179zd2_2042;
																																		}
																																	else
																																		{	/* Expand/object.scm 195 */
																																			if (PAIRP
																																				(BgL_carzd2356zd2_2234))
																																				{	/* Expand/object.scm 195 */
																																					obj_t
																																						BgL_cdrzd2373zd2_2239;
																																					BgL_cdrzd2373zd2_2239
																																						=
																																						CDR
																																						(BgL_carzd2356zd2_2234);
																																					if (
																																						(CAR
																																							(BgL_carzd2356zd2_2234)
																																							==
																																							CNST_TABLE_REF
																																							(10)))
																																						{	/* Expand/object.scm 195 */
																																							if (PAIRP(BgL_cdrzd2373zd2_2239))
																																								{	/* Expand/object.scm 195 */
																																									obj_t
																																										BgL_carzd2376zd2_2243;
																																									obj_t
																																										BgL_cdrzd2377zd2_2244;
																																									BgL_carzd2376zd2_2243
																																										=
																																										CAR
																																										(BgL_cdrzd2373zd2_2239);
																																									BgL_cdrzd2377zd2_2244
																																										=
																																										CDR
																																										(BgL_cdrzd2373zd2_2239);
																																									if (SYMBOLP(BgL_carzd2376zd2_2243))
																																										{	/* Expand/object.scm 195 */
																																											if (PAIRP(BgL_cdrzd2377zd2_2244))
																																												{	/* Expand/object.scm 195 */
																																													obj_t
																																														BgL_carzd2382zd2_2247;
																																													BgL_carzd2382zd2_2247
																																														=
																																														CAR
																																														(BgL_cdrzd2377zd2_2244);
																																													if (SYMBOLP(BgL_carzd2382zd2_2247))
																																														{	/* Expand/object.scm 195 */
																																															if (NULLP(CDR(BgL_cdrzd2377zd2_2244)))
																																																{	/* Expand/object.scm 195 */
																																																	obj_t
																																																		BgL_arg2021z00_2251;
																																																	BgL_arg2021z00_2251
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_nz00_2023));
																																																	{
																																																		obj_t
																																																			BgL_argsz00_5384;
																																																		obj_t
																																																			BgL_modz00_5383;
																																																		obj_t
																																																			BgL_funz00_5382;
																																																		BgL_funz00_5382
																																																			=
																																																			BgL_carzd2376zd2_2243;
																																																		BgL_modz00_5383
																																																			=
																																																			BgL_carzd2382zd2_2247;
																																																		BgL_argsz00_5384
																																																			=
																																																			BgL_arg2021z00_2251;
																																																		BgL_argsz00_2045
																																																			=
																																																			BgL_argsz00_5384;
																																																		BgL_modz00_2044
																																																			=
																																																			BgL_modz00_5383;
																																																		BgL_funz00_2043
																																																			=
																																																			BgL_funz00_5382;
																																																		goto
																																																			BgL_tagzd2180zd2_2046;
																																																	}
																																																}
																																															else
																																																{	/* Expand/object.scm 195 */
																																																	return
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																													else
																																														{	/* Expand/object.scm 195 */
																																															return
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																											else
																																												{	/* Expand/object.scm 195 */
																																													return
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																									else
																																										{	/* Expand/object.scm 195 */
																																											return
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Expand/object.scm 195 */
																																									return
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					else
																																						{	/* Expand/object.scm 195 */
																																							return
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Expand/object.scm 195 */
																																					return
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																														}
																												}
																										}
																									else
																										{	/* Expand/object.scm 195 */
																											if (
																												(CAR(
																														((obj_t)
																															BgL_nz00_2023)) ==
																													CNST_TABLE_REF(21)))
																												{	/* Expand/object.scm 195 */
																													return ((bool_t) 1);
																												}
																											else
																												{	/* Expand/object.scm 195 */
																													obj_t
																														BgL_carzd2401zd2_2258;
																													BgL_carzd2401zd2_2258
																														=
																														CAR(((obj_t)
																															BgL_nz00_2023));
																													if (SYMBOLP
																														(BgL_carzd2401zd2_2258))
																														{	/* Expand/object.scm 195 */
																															obj_t
																																BgL_arg2030z00_2260;
																															BgL_arg2030z00_2260
																																=
																																CDR(((obj_t)
																																	BgL_nz00_2023));
																															{
																																obj_t
																																	BgL_argsz00_5397;
																																obj_t
																																	BgL_funz00_5396;
																																BgL_funz00_5396
																																	=
																																	BgL_carzd2401zd2_2258;
																																BgL_argsz00_5397
																																	=
																																	BgL_arg2030z00_2260;
																																BgL_argsz00_2041
																																	=
																																	BgL_argsz00_5397;
																																BgL_funz00_2040
																																	=
																																	BgL_funz00_5396;
																																goto
																																	BgL_tagzd2179zd2_2042;
																															}
																														}
																													else
																														{	/* Expand/object.scm 195 */
																															if (PAIRP
																																(BgL_carzd2401zd2_2258))
																																{	/* Expand/object.scm 195 */
																																	obj_t
																																		BgL_cdrzd2418zd2_2263;
																																	BgL_cdrzd2418zd2_2263
																																		=
																																		CDR
																																		(BgL_carzd2401zd2_2258);
																																	if ((CAR
																																			(BgL_carzd2401zd2_2258)
																																			==
																																			CNST_TABLE_REF
																																			(10)))
																																		{	/* Expand/object.scm 195 */
																																			if (PAIRP
																																				(BgL_cdrzd2418zd2_2263))
																																				{	/* Expand/object.scm 195 */
																																					obj_t
																																						BgL_carzd2421zd2_2267;
																																					obj_t
																																						BgL_cdrzd2422zd2_2268;
																																					BgL_carzd2421zd2_2267
																																						=
																																						CAR
																																						(BgL_cdrzd2418zd2_2263);
																																					BgL_cdrzd2422zd2_2268
																																						=
																																						CDR
																																						(BgL_cdrzd2418zd2_2263);
																																					if (SYMBOLP(BgL_carzd2421zd2_2267))
																																						{	/* Expand/object.scm 195 */
																																							if (PAIRP(BgL_cdrzd2422zd2_2268))
																																								{	/* Expand/object.scm 195 */
																																									obj_t
																																										BgL_carzd2427zd2_2271;
																																									BgL_carzd2427zd2_2271
																																										=
																																										CAR
																																										(BgL_cdrzd2422zd2_2268);
																																									if (SYMBOLP(BgL_carzd2427zd2_2271))
																																										{	/* Expand/object.scm 195 */
																																											if (NULLP(CDR(BgL_cdrzd2422zd2_2268)))
																																												{	/* Expand/object.scm 195 */
																																													obj_t
																																														BgL_arg2041z00_2275;
																																													BgL_arg2041z00_2275
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_nz00_2023));
																																													{
																																														obj_t
																																															BgL_argsz00_5423;
																																														obj_t
																																															BgL_modz00_5422;
																																														obj_t
																																															BgL_funz00_5421;
																																														BgL_funz00_5421
																																															=
																																															BgL_carzd2421zd2_2267;
																																														BgL_modz00_5422
																																															=
																																															BgL_carzd2427zd2_2271;
																																														BgL_argsz00_5423
																																															=
																																															BgL_arg2041z00_2275;
																																														BgL_argsz00_2045
																																															=
																																															BgL_argsz00_5423;
																																														BgL_modz00_2044
																																															=
																																															BgL_modz00_5422;
																																														BgL_funz00_2043
																																															=
																																															BgL_funz00_5421;
																																														goto
																																															BgL_tagzd2180zd2_2046;
																																													}
																																												}
																																											else
																																												{	/* Expand/object.scm 195 */
																																													return
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																									else
																																										{	/* Expand/object.scm 195 */
																																											return
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Expand/object.scm 195 */
																																									return
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					else
																																						{	/* Expand/object.scm 195 */
																																							return
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Expand/object.scm 195 */
																																					return
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Expand/object.scm 195 */
																																			return (
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Expand/object.scm 195 */
																																	return (
																																		(bool_t) 0);
																																}
																														}
																												}
																										}
																								}
																							else
																								{	/* Expand/object.scm 195 */
																									if (
																										(BgL_carzd2208zd2_2055 ==
																											CNST_TABLE_REF(24)))
																										{	/* Expand/object.scm 195 */
																											goto
																												BgL_kapzd2210zd2_2057;
																										}
																									else
																										{	/* Expand/object.scm 195 */
																											if (
																												(BgL_carzd2208zd2_2055
																													==
																													CNST_TABLE_REF(25)))
																												{	/* Expand/object.scm 195 */
																													goto
																														BgL_kapzd2210zd2_2057;
																												}
																											else
																												{	/* Expand/object.scm 195 */
																													if (
																														(BgL_carzd2208zd2_2055
																															==
																															CNST_TABLE_REF
																															(26)))
																														{	/* Expand/object.scm 195 */
																															goto
																																BgL_kapzd2210zd2_2057;
																														}
																													else
																														{	/* Expand/object.scm 195 */
																															if (
																																(BgL_carzd2208zd2_2055
																																	==
																																	CNST_TABLE_REF
																																	(27)))
																																{	/* Expand/object.scm 195 */
																																	goto
																																		BgL_kapzd2210zd2_2057;
																																}
																															else
																																{	/* Expand/object.scm 195 */
																																	if (
																																		(BgL_carzd2208zd2_2055
																																			==
																																			CNST_TABLE_REF
																																			(28)))
																																		{	/* Expand/object.scm 195 */
																																			goto
																																				BgL_kapzd2210zd2_2057;
																																		}
																																	else
																																		{	/* Expand/object.scm 195 */
																																			if (
																																				(BgL_carzd2208zd2_2055
																																					==
																																					CNST_TABLE_REF
																																					(29)))
																																				{	/* Expand/object.scm 195 */
																																					goto
																																						BgL_kapzd2210zd2_2057;
																																				}
																																			else
																																				{	/* Expand/object.scm 195 */
																																					if (
																																						(BgL_carzd2208zd2_2055
																																							==
																																							CNST_TABLE_REF
																																							(21)))
																																						{	/* Expand/object.scm 195 */
																																							return
																																								(
																																								(bool_t)
																																								1);
																																						}
																																					else
																																						{	/* Expand/object.scm 195 */
																																							obj_t
																																								BgL_cdrzd2447zd2_2060;
																																							BgL_cdrzd2447zd2_2060
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_nz00_2023));
																																							if ((CAR(((obj_t) BgL_nz00_2023)) == CNST_TABLE_REF(22)))
																																								{	/* Expand/object.scm 195 */
																																									if (PAIRP(BgL_cdrzd2447zd2_2060))
																																										{	/* Expand/object.scm 195 */
																																											bool_t
																																												BgL_test2896z00_5454;
																																											{	/* Expand/object.scm 195 */
																																												obj_t
																																													BgL_tmpz00_5455;
																																												BgL_tmpz00_5455
																																													=
																																													CAR
																																													(BgL_cdrzd2447zd2_2060);
																																												BgL_test2896z00_5454
																																													=
																																													SYMBOLP
																																													(BgL_tmpz00_5455);
																																											}
																																											if (BgL_test2896z00_5454)
																																												{	/* Expand/object.scm 195 */
																																													if (NULLP(CDR(BgL_cdrzd2447zd2_2060)))
																																														{	/* Expand/object.scm 195 */
																																															return
																																																(
																																																(bool_t)
																																																1);
																																														}
																																													else
																																														{	/* Expand/object.scm 195 */
																																															obj_t
																																																BgL_carzd2455zd2_2068;
																																															BgL_carzd2455zd2_2068
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_nz00_2023));
																																															if (SYMBOLP(BgL_carzd2455zd2_2068))
																																																{
																																																	obj_t
																																																		BgL_argsz00_5466;
																																																	obj_t
																																																		BgL_funz00_5465;
																																																	BgL_funz00_5465
																																																		=
																																																		BgL_carzd2455zd2_2068;
																																																	BgL_argsz00_5466
																																																		=
																																																		BgL_cdrzd2447zd2_2060;
																																																	BgL_argsz00_2041
																																																		=
																																																		BgL_argsz00_5466;
																																																	BgL_funz00_2040
																																																		=
																																																		BgL_funz00_5465;
																																																	goto
																																																		BgL_tagzd2179zd2_2042;
																																																}
																																															else
																																																{	/* Expand/object.scm 195 */
																																																	if (PAIRP(BgL_carzd2455zd2_2068))
																																																		{	/* Expand/object.scm 195 */
																																																			obj_t
																																																				BgL_cdrzd2472zd2_2073;
																																																			BgL_cdrzd2472zd2_2073
																																																				=
																																																				CDR
																																																				(BgL_carzd2455zd2_2068);
																																																			if ((CAR(BgL_carzd2455zd2_2068) == CNST_TABLE_REF(10)))
																																																				{	/* Expand/object.scm 195 */
																																																					if (PAIRP(BgL_cdrzd2472zd2_2073))
																																																						{	/* Expand/object.scm 195 */
																																																							obj_t
																																																								BgL_carzd2475zd2_2077;
																																																							obj_t
																																																								BgL_cdrzd2476zd2_2078;
																																																							BgL_carzd2475zd2_2077
																																																								=
																																																								CAR
																																																								(BgL_cdrzd2472zd2_2073);
																																																							BgL_cdrzd2476zd2_2078
																																																								=
																																																								CDR
																																																								(BgL_cdrzd2472zd2_2073);
																																																							if (SYMBOLP(BgL_carzd2475zd2_2077))
																																																								{	/* Expand/object.scm 195 */
																																																									if (PAIRP(BgL_cdrzd2476zd2_2078))
																																																										{	/* Expand/object.scm 195 */
																																																											obj_t
																																																												BgL_carzd2481zd2_2081;
																																																											BgL_carzd2481zd2_2081
																																																												=
																																																												CAR
																																																												(BgL_cdrzd2476zd2_2078);
																																																											if (SYMBOLP(BgL_carzd2481zd2_2081))
																																																												{	/* Expand/object.scm 195 */
																																																													if (NULLP(CDR(BgL_cdrzd2476zd2_2078)))
																																																														{	/* Expand/object.scm 195 */
																																																															obj_t
																																																																BgL_arg1893z00_2085;
																																																															BgL_arg1893z00_2085
																																																																=
																																																																CDR
																																																																(
																																																																((obj_t) BgL_nz00_2023));
																																																															{
																																																																obj_t
																																																																	BgL_argsz00_5492;
																																																																obj_t
																																																																	BgL_modz00_5491;
																																																																obj_t
																																																																	BgL_funz00_5490;
																																																																BgL_funz00_5490
																																																																	=
																																																																	BgL_carzd2475zd2_2077;
																																																																BgL_modz00_5491
																																																																	=
																																																																	BgL_carzd2481zd2_2081;
																																																																BgL_argsz00_5492
																																																																	=
																																																																	BgL_arg1893z00_2085;
																																																																BgL_argsz00_2045
																																																																	=
																																																																	BgL_argsz00_5492;
																																																																BgL_modz00_2044
																																																																	=
																																																																	BgL_modz00_5491;
																																																																BgL_funz00_2043
																																																																	=
																																																																	BgL_funz00_5490;
																																																																goto
																																																																	BgL_tagzd2180zd2_2046;
																																																															}
																																																														}
																																																													else
																																																														{	/* Expand/object.scm 195 */
																																																															return
																																																																(
																																																																(bool_t)
																																																																0);
																																																														}
																																																												}
																																																											else
																																																												{	/* Expand/object.scm 195 */
																																																													return
																																																														(
																																																														(bool_t)
																																																														0);
																																																												}
																																																										}
																																																									else
																																																										{	/* Expand/object.scm 195 */
																																																											return
																																																												(
																																																												(bool_t)
																																																												0);
																																																										}
																																																								}
																																																							else
																																																								{	/* Expand/object.scm 195 */
																																																									return
																																																										(
																																																										(bool_t)
																																																										0);
																																																								}
																																																						}
																																																					else
																																																						{	/* Expand/object.scm 195 */
																																																							return
																																																								(
																																																								(bool_t)
																																																								0);
																																																						}
																																																				}
																																																			else
																																																				{	/* Expand/object.scm 195 */
																																																					return
																																																						(
																																																						(bool_t)
																																																						0);
																																																				}
																																																		}
																																																	else
																																																		{	/* Expand/object.scm 195 */
																																																			return
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																														}
																																												}
																																											else
																																												{	/* Expand/object.scm 195 */
																																													obj_t
																																														BgL_carzd2492zd2_2089;
																																													BgL_carzd2492zd2_2089
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_nz00_2023));
																																													if (SYMBOLP(BgL_carzd2492zd2_2089))
																																														{
																																															obj_t
																																																BgL_argsz00_5498;
																																															obj_t
																																																BgL_funz00_5497;
																																															BgL_funz00_5497
																																																=
																																																BgL_carzd2492zd2_2089;
																																															BgL_argsz00_5498
																																																=
																																																BgL_cdrzd2447zd2_2060;
																																															BgL_argsz00_2041
																																																=
																																																BgL_argsz00_5498;
																																															BgL_funz00_2040
																																																=
																																																BgL_funz00_5497;
																																															goto
																																																BgL_tagzd2179zd2_2042;
																																														}
																																													else
																																														{	/* Expand/object.scm 195 */
																																															if (PAIRP(BgL_carzd2492zd2_2089))
																																																{	/* Expand/object.scm 195 */
																																																	obj_t
																																																		BgL_cdrzd2509zd2_2094;
																																																	BgL_cdrzd2509zd2_2094
																																																		=
																																																		CDR
																																																		(BgL_carzd2492zd2_2089);
																																																	if ((CAR(BgL_carzd2492zd2_2089) == CNST_TABLE_REF(10)))
																																																		{	/* Expand/object.scm 195 */
																																																			if (PAIRP(BgL_cdrzd2509zd2_2094))
																																																				{	/* Expand/object.scm 195 */
																																																					obj_t
																																																						BgL_carzd2512zd2_2098;
																																																					obj_t
																																																						BgL_cdrzd2513zd2_2099;
																																																					BgL_carzd2512zd2_2098
																																																						=
																																																						CAR
																																																						(BgL_cdrzd2509zd2_2094);
																																																					BgL_cdrzd2513zd2_2099
																																																						=
																																																						CDR
																																																						(BgL_cdrzd2509zd2_2094);
																																																					if (SYMBOLP(BgL_carzd2512zd2_2098))
																																																						{	/* Expand/object.scm 195 */
																																																							if (PAIRP(BgL_cdrzd2513zd2_2099))
																																																								{	/* Expand/object.scm 195 */
																																																									obj_t
																																																										BgL_carzd2518zd2_2102;
																																																									BgL_carzd2518zd2_2102
																																																										=
																																																										CAR
																																																										(BgL_cdrzd2513zd2_2099);
																																																									if (SYMBOLP(BgL_carzd2518zd2_2102))
																																																										{	/* Expand/object.scm 195 */
																																																											if (NULLP(CDR(BgL_cdrzd2513zd2_2099)))
																																																												{	/* Expand/object.scm 195 */
																																																													obj_t
																																																														BgL_arg1912z00_2106;
																																																													BgL_arg1912z00_2106
																																																														=
																																																														CDR
																																																														(
																																																														((obj_t) BgL_nz00_2023));
																																																													{
																																																														obj_t
																																																															BgL_argsz00_5524;
																																																														obj_t
																																																															BgL_modz00_5523;
																																																														obj_t
																																																															BgL_funz00_5522;
																																																														BgL_funz00_5522
																																																															=
																																																															BgL_carzd2512zd2_2098;
																																																														BgL_modz00_5523
																																																															=
																																																															BgL_carzd2518zd2_2102;
																																																														BgL_argsz00_5524
																																																															=
																																																															BgL_arg1912z00_2106;
																																																														BgL_argsz00_2045
																																																															=
																																																															BgL_argsz00_5524;
																																																														BgL_modz00_2044
																																																															=
																																																															BgL_modz00_5523;
																																																														BgL_funz00_2043
																																																															=
																																																															BgL_funz00_5522;
																																																														goto
																																																															BgL_tagzd2180zd2_2046;
																																																													}
																																																												}
																																																											else
																																																												{	/* Expand/object.scm 195 */
																																																													return
																																																														(
																																																														(bool_t)
																																																														0);
																																																												}
																																																										}
																																																									else
																																																										{	/* Expand/object.scm 195 */
																																																											return
																																																												(
																																																												(bool_t)
																																																												0);
																																																										}
																																																								}
																																																							else
																																																								{	/* Expand/object.scm 195 */
																																																									return
																																																										(
																																																										(bool_t)
																																																										0);
																																																								}
																																																						}
																																																					else
																																																						{	/* Expand/object.scm 195 */
																																																							return
																																																								(
																																																								(bool_t)
																																																								0);
																																																						}
																																																				}
																																																			else
																																																				{	/* Expand/object.scm 195 */
																																																					return
																																																						(
																																																						(bool_t)
																																																						0);
																																																				}
																																																		}
																																																	else
																																																		{	/* Expand/object.scm 195 */
																																																			return
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																															else
																																																{	/* Expand/object.scm 195 */
																																																	return
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																												}
																																										}
																																									else
																																										{	/* Expand/object.scm 195 */
																																											obj_t
																																												BgL_carzd2529zd2_2110;
																																											BgL_carzd2529zd2_2110
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_nz00_2023));
																																											if (SYMBOLP(BgL_carzd2529zd2_2110))
																																												{
																																													obj_t
																																														BgL_argsz00_5530;
																																													obj_t
																																														BgL_funz00_5529;
																																													BgL_funz00_5529
																																														=
																																														BgL_carzd2529zd2_2110;
																																													BgL_argsz00_5530
																																														=
																																														BgL_cdrzd2447zd2_2060;
																																													BgL_argsz00_2041
																																														=
																																														BgL_argsz00_5530;
																																													BgL_funz00_2040
																																														=
																																														BgL_funz00_5529;
																																													goto
																																														BgL_tagzd2179zd2_2042;
																																												}
																																											else
																																												{	/* Expand/object.scm 195 */
																																													if (PAIRP(BgL_carzd2529zd2_2110))
																																														{	/* Expand/object.scm 195 */
																																															obj_t
																																																BgL_cdrzd2546zd2_2115;
																																															BgL_cdrzd2546zd2_2115
																																																=
																																																CDR
																																																(BgL_carzd2529zd2_2110);
																																															if ((CAR(BgL_carzd2529zd2_2110) == CNST_TABLE_REF(10)))
																																																{	/* Expand/object.scm 195 */
																																																	if (PAIRP(BgL_cdrzd2546zd2_2115))
																																																		{	/* Expand/object.scm 195 */
																																																			obj_t
																																																				BgL_carzd2549zd2_2119;
																																																			obj_t
																																																				BgL_cdrzd2550zd2_2120;
																																																			BgL_carzd2549zd2_2119
																																																				=
																																																				CAR
																																																				(BgL_cdrzd2546zd2_2115);
																																																			BgL_cdrzd2550zd2_2120
																																																				=
																																																				CDR
																																																				(BgL_cdrzd2546zd2_2115);
																																																			if (SYMBOLP(BgL_carzd2549zd2_2119))
																																																				{	/* Expand/object.scm 195 */
																																																					if (PAIRP(BgL_cdrzd2550zd2_2120))
																																																						{	/* Expand/object.scm 195 */
																																																							obj_t
																																																								BgL_carzd2555zd2_2123;
																																																							BgL_carzd2555zd2_2123
																																																								=
																																																								CAR
																																																								(BgL_cdrzd2550zd2_2120);
																																																							if (SYMBOLP(BgL_carzd2555zd2_2123))
																																																								{	/* Expand/object.scm 195 */
																																																									if (NULLP(CDR(BgL_cdrzd2550zd2_2120)))
																																																										{	/* Expand/object.scm 195 */
																																																											obj_t
																																																												BgL_arg1928z00_2127;
																																																											BgL_arg1928z00_2127
																																																												=
																																																												CDR
																																																												(
																																																												((obj_t) BgL_nz00_2023));
																																																											{
																																																												obj_t
																																																													BgL_argsz00_5556;
																																																												obj_t
																																																													BgL_modz00_5555;
																																																												obj_t
																																																													BgL_funz00_5554;
																																																												BgL_funz00_5554
																																																													=
																																																													BgL_carzd2549zd2_2119;
																																																												BgL_modz00_5555
																																																													=
																																																													BgL_carzd2555zd2_2123;
																																																												BgL_argsz00_5556
																																																													=
																																																													BgL_arg1928z00_2127;
																																																												BgL_argsz00_2045
																																																													=
																																																													BgL_argsz00_5556;
																																																												BgL_modz00_2044
																																																													=
																																																													BgL_modz00_5555;
																																																												BgL_funz00_2043
																																																													=
																																																													BgL_funz00_5554;
																																																												goto
																																																													BgL_tagzd2180zd2_2046;
																																																											}
																																																										}
																																																									else
																																																										{	/* Expand/object.scm 195 */
																																																											return
																																																												(
																																																												(bool_t)
																																																												0);
																																																										}
																																																								}
																																																							else
																																																								{	/* Expand/object.scm 195 */
																																																									return
																																																										(
																																																										(bool_t)
																																																										0);
																																																								}
																																																						}
																																																					else
																																																						{	/* Expand/object.scm 195 */
																																																							return
																																																								(
																																																								(bool_t)
																																																								0);
																																																						}
																																																				}
																																																			else
																																																				{	/* Expand/object.scm 195 */
																																																					return
																																																						(
																																																						(bool_t)
																																																						0);
																																																				}
																																																		}
																																																	else
																																																		{	/* Expand/object.scm 195 */
																																																			return
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																															else
																																																{	/* Expand/object.scm 195 */
																																																	return
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																													else
																																														{	/* Expand/object.scm 195 */
																																															return
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																										}
																																								}
																																							else
																																								{	/* Expand/object.scm 195 */
																																									obj_t
																																										BgL_carzd2566zd2_2130;
																																									BgL_carzd2566zd2_2130
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_nz00_2023));
																																									if (SYMBOLP(BgL_carzd2566zd2_2130))
																																										{
																																											obj_t
																																												BgL_argsz00_5562;
																																											obj_t
																																												BgL_funz00_5561;
																																											BgL_funz00_5561
																																												=
																																												BgL_carzd2566zd2_2130;
																																											BgL_argsz00_5562
																																												=
																																												BgL_cdrzd2447zd2_2060;
																																											BgL_argsz00_2041
																																												=
																																												BgL_argsz00_5562;
																																											BgL_funz00_2040
																																												=
																																												BgL_funz00_5561;
																																											goto
																																												BgL_tagzd2179zd2_2042;
																																										}
																																									else
																																										{	/* Expand/object.scm 195 */
																																											if (PAIRP(BgL_carzd2566zd2_2130))
																																												{	/* Expand/object.scm 195 */
																																													obj_t
																																														BgL_cdrzd2583zd2_2135;
																																													BgL_cdrzd2583zd2_2135
																																														=
																																														CDR
																																														(BgL_carzd2566zd2_2130);
																																													if ((CAR(BgL_carzd2566zd2_2130) == CNST_TABLE_REF(10)))
																																														{	/* Expand/object.scm 195 */
																																															if (PAIRP(BgL_cdrzd2583zd2_2135))
																																																{	/* Expand/object.scm 195 */
																																																	obj_t
																																																		BgL_carzd2586zd2_2139;
																																																	obj_t
																																																		BgL_cdrzd2587zd2_2140;
																																																	BgL_carzd2586zd2_2139
																																																		=
																																																		CAR
																																																		(BgL_cdrzd2583zd2_2135);
																																																	BgL_cdrzd2587zd2_2140
																																																		=
																																																		CDR
																																																		(BgL_cdrzd2583zd2_2135);
																																																	if (SYMBOLP(BgL_carzd2586zd2_2139))
																																																		{	/* Expand/object.scm 195 */
																																																			if (PAIRP(BgL_cdrzd2587zd2_2140))
																																																				{	/* Expand/object.scm 195 */
																																																					obj_t
																																																						BgL_carzd2592zd2_2143;
																																																					BgL_carzd2592zd2_2143
																																																						=
																																																						CAR
																																																						(BgL_cdrzd2587zd2_2140);
																																																					if (SYMBOLP(BgL_carzd2592zd2_2143))
																																																						{	/* Expand/object.scm 195 */
																																																							if (NULLP(CDR(BgL_cdrzd2587zd2_2140)))
																																																								{	/* Expand/object.scm 195 */
																																																									obj_t
																																																										BgL_arg1942z00_2147;
																																																									BgL_arg1942z00_2147
																																																										=
																																																										CDR
																																																										(
																																																										((obj_t) BgL_nz00_2023));
																																																									{
																																																										obj_t
																																																											BgL_argsz00_5588;
																																																										obj_t
																																																											BgL_modz00_5587;
																																																										obj_t
																																																											BgL_funz00_5586;
																																																										BgL_funz00_5586
																																																											=
																																																											BgL_carzd2586zd2_2139;
																																																										BgL_modz00_5587
																																																											=
																																																											BgL_carzd2592zd2_2143;
																																																										BgL_argsz00_5588
																																																											=
																																																											BgL_arg1942z00_2147;
																																																										BgL_argsz00_2045
																																																											=
																																																											BgL_argsz00_5588;
																																																										BgL_modz00_2044
																																																											=
																																																											BgL_modz00_5587;
																																																										BgL_funz00_2043
																																																											=
																																																											BgL_funz00_5586;
																																																										goto
																																																											BgL_tagzd2180zd2_2046;
																																																									}
																																																								}
																																																							else
																																																								{	/* Expand/object.scm 195 */
																																																									return
																																																										(
																																																										(bool_t)
																																																										0);
																																																								}
																																																						}
																																																					else
																																																						{	/* Expand/object.scm 195 */
																																																							return
																																																								(
																																																								(bool_t)
																																																								0);
																																																						}
																																																				}
																																																			else
																																																				{	/* Expand/object.scm 195 */
																																																					return
																																																						(
																																																						(bool_t)
																																																						0);
																																																				}
																																																		}
																																																	else
																																																		{	/* Expand/object.scm 195 */
																																																			return
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																															else
																																																{	/* Expand/object.scm 195 */
																																																	return
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																													else
																																														{	/* Expand/object.scm 195 */
																																															return
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																											else
																																												{	/* Expand/object.scm 195 */
																																													return
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																					}
																			}
																	}
																else
																	{	/* Expand/object.scm 195 */
																		return ((bool_t) 0);
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &<@anonymous:1841> */
	obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzexpand_objectz00(obj_t
		BgL_envz00_4159, obj_t BgL_slotz00_4162, obj_t BgL_valz00_4163)
	{
		{	/* Expand/object.scm 343 */
			{	/* Expand/object.scm 344 */
				obj_t BgL_ez00_4160;
				obj_t BgL_newz00_4161;

				BgL_ez00_4160 = ((obj_t) PROCEDURE_REF(BgL_envz00_4159, (int) (0L)));
				BgL_newz00_4161 = ((obj_t) PROCEDURE_REF(BgL_envz00_4159, (int) (1L)));
				{	/* Expand/object.scm 344 */
					bool_t BgL_test2930z00_5595;

					if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
							((BgL_slotz00_bglt) BgL_slotz00_4162)))
						{	/* Expand/object.scm 344 */
							if (
								(((BgL_slotz00_bglt) COBJECT(
											((BgL_slotz00_bglt) BgL_slotz00_4162)))->
									BgL_readzd2onlyzf3z21))
								{	/* Expand/object.scm 345 */
									BgL_test2930z00_5595 = ((bool_t) 0);
								}
							else
								{	/* Expand/object.scm 345 */
									BgL_test2930z00_5595 = CBOOL(CAR(((obj_t) BgL_valz00_4163)));
								}
						}
					else
						{	/* Expand/object.scm 344 */
							BgL_test2930z00_5595 = ((bool_t) 0);
						}
					if (BgL_test2930z00_5595)
						{	/* Expand/object.scm 347 */
							obj_t BgL_vz00_4250;
							obj_t BgL_idz00_4251;

							{	/* Expand/object.scm 347 */
								obj_t BgL_arg1850z00_4252;

								BgL_arg1850z00_4252 = CDR(((obj_t) BgL_valz00_4163));
								BgL_vz00_4250 =
									BGL_PROCEDURE_CALL2(BgL_ez00_4160, BgL_arg1850z00_4252,
									BgL_ez00_4160);
							}
							BgL_idz00_4251 =
								(((BgL_slotz00_bglt) COBJECT(
										((BgL_slotz00_bglt) BgL_slotz00_4162)))->BgL_idz00);
							{	/* Expand/object.scm 349 */
								obj_t BgL_arg1845z00_4253;
								obj_t BgL_arg1846z00_4254;

								BgL_arg1845z00_4253 = CDR(((obj_t) BgL_valz00_4163));
								{	/* Expand/object.scm 350 */
									obj_t BgL_arg1847z00_4255;

									{	/* Expand/object.scm 350 */
										obj_t BgL_arg1848z00_4256;
										obj_t BgL_arg1849z00_4257;

										BgL_arg1848z00_4256 =
											BGl_fieldzd2accesszd2zzast_objectz00(BgL_newz00_4161,
											BgL_idz00_4251, BTRUE);
										BgL_arg1849z00_4257 = MAKE_YOUNG_PAIR(BgL_vz00_4250, BNIL);
										BgL_arg1847z00_4255 =
											MAKE_YOUNG_PAIR(BgL_arg1848z00_4256, BgL_arg1849z00_4257);
									}
									BgL_arg1846z00_4254 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1847z00_4255);
								}
								return
									BGl_objectzd2epairifyzd2zzexpand_objectz00
									(BgL_arg1846z00_4254, BgL_arg1845z00_4253);
							}
						}
					else
						{	/* Expand/object.scm 344 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &<@anonymous:1762> */
	obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzexpand_objectz00(obj_t
		BgL_envz00_4164, obj_t BgL_slotz00_4167, obj_t BgL_valz00_4168)
	{
		{	/* Expand/object.scm 328 */
			{	/* Expand/object.scm 329 */
				obj_t BgL_ez00_4165;
				obj_t BgL_newz00_4166;

				BgL_ez00_4165 = ((obj_t) PROCEDURE_REF(BgL_envz00_4164, (int) (0L)));
				BgL_newz00_4166 = ((obj_t) PROCEDURE_REF(BgL_envz00_4164, (int) (1L)));
				if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
						((BgL_slotz00_bglt) BgL_slotz00_4167)))
					{	/* Expand/object.scm 329 */
						return BFALSE;
					}
				else
					{	/* Expand/object.scm 330 */
						obj_t BgL_vz00_4258;
						obj_t BgL_idz00_4259;

						{	/* Expand/object.scm 330 */
							obj_t BgL_arg1775z00_4260;

							BgL_arg1775z00_4260 = CDR(((obj_t) BgL_valz00_4168));
							BgL_vz00_4258 =
								BGL_PROCEDURE_CALL2(BgL_ez00_4165, BgL_arg1775z00_4260,
								BgL_ez00_4165);
						}
						BgL_idz00_4259 =
							(((BgL_slotz00_bglt) COBJECT(
									((BgL_slotz00_bglt) BgL_slotz00_4167)))->BgL_idz00);
						{	/* Expand/object.scm 332 */
							obj_t BgL_arg1765z00_4261;
							obj_t BgL_arg1767z00_4262;

							BgL_arg1765z00_4261 = CDR(((obj_t) BgL_valz00_4168));
							{	/* Expand/object.scm 333 */
								obj_t BgL_arg1770z00_4263;

								{	/* Expand/object.scm 333 */
									obj_t BgL_arg1771z00_4264;
									obj_t BgL_arg1773z00_4265;

									BgL_arg1771z00_4264 =
										BGl_fieldzd2accesszd2zzast_objectz00(BgL_newz00_4166,
										BgL_idz00_4259, BTRUE);
									BgL_arg1773z00_4265 = MAKE_YOUNG_PAIR(BgL_vz00_4258, BNIL);
									BgL_arg1770z00_4263 =
										MAKE_YOUNG_PAIR(BgL_arg1771z00_4264, BgL_arg1773z00_4265);
								}
								BgL_arg1767z00_4262 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1770z00_4263);
							}
							return
								BGl_objectzd2epairifyzd2zzexpand_objectz00(BgL_arg1767z00_4262,
								BgL_arg1765z00_4261);
						}
					}
			}
		}

	}



/* expand-co-instantiate */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2cozd2instantiatez00zzexpand_objectz00(obj_t BgL_xz00_24,
		obj_t BgL_ez00_25)
	{
		{	/* Expand/object.scm 358 */
			{

				if (PAIRP(BgL_xz00_24))
					{	/* Expand/object.scm 359 */
						obj_t BgL_cdrzd2705zd2_2510;

						BgL_cdrzd2705zd2_2510 = CDR(((obj_t) BgL_xz00_24));
						if ((CAR(((obj_t) BgL_xz00_24)) == CNST_TABLE_REF(30)))
							{	/* Expand/object.scm 359 */
								if (PAIRP(BgL_cdrzd2705zd2_2510))
									{	/* Expand/object.scm 359 */
										obj_t BgL_arg2192z00_2514;
										obj_t BgL_arg2193z00_2515;

										BgL_arg2192z00_2514 = CAR(BgL_cdrzd2705zd2_2510);
										BgL_arg2193z00_2515 = CDR(BgL_cdrzd2705zd2_2510);
										return
											BGl_replacez12z12zztools_miscz00(BgL_xz00_24,
											BGl_cozd2instantiatezd2ze3letze3zzexpand_objectz00
											(BgL_arg2192z00_2514, BgL_arg2193z00_2515, BgL_xz00_24,
												BgL_ez00_25));
									}
								else
									{	/* Expand/object.scm 359 */
									BgL_tagzd2698zd2_2507:
										return
											BGl_errorz00zz__errorz00
											(BGl_string2677z00zzexpand_objectz00,
											BGl_string2666z00zzexpand_objectz00, BgL_xz00_24);
									}
							}
						else
							{	/* Expand/object.scm 359 */
								goto BgL_tagzd2698zd2_2507;
							}
					}
				else
					{	/* Expand/object.scm 359 */
						goto BgL_tagzd2698zd2_2507;
					}
			}
		}

	}



/* &expand-co-instantiate */
	obj_t BGl_z62expandzd2cozd2instantiatez62zzexpand_objectz00(obj_t
		BgL_envz00_4169, obj_t BgL_xz00_4170, obj_t BgL_ez00_4171)
	{
		{	/* Expand/object.scm 358 */
			return
				BGl_expandzd2cozd2instantiatez00zzexpand_objectz00(BgL_xz00_4170,
				BgL_ez00_4171);
		}

	}



/* co-instantiate->let */
	obj_t BGl_cozd2instantiatezd2ze3letze3zzexpand_objectz00(obj_t
		BgL_bindingsz00_26, obj_t BgL_bodyz00_27, obj_t BgL_xz00_28,
		obj_t BgL_ez00_29)
	{
		{	/* Expand/object.scm 368 */
			{
				obj_t BgL_exprz00_2611;
				obj_t BgL_bdgz00_2612;
				obj_t BgL_locz00_2613;

				{	/* Expand/object.scm 390 */
					obj_t BgL_locz00_2519;
					obj_t BgL_varsz00_2520;

					BgL_locz00_2519 =
						BGl_findzd2locationzd2zztools_locationz00(BgL_xz00_28);
					if (NULLP(BgL_bindingsz00_26))
						{	/* Expand/object.scm 391 */
							BgL_varsz00_2520 = BNIL;
						}
					else
						{	/* Expand/object.scm 391 */
							obj_t BgL_head1290z00_2570;

							BgL_head1290z00_2570 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1288z00_2572;
								obj_t BgL_tail1291z00_2573;

								BgL_l1288z00_2572 = BgL_bindingsz00_26;
								BgL_tail1291z00_2573 = BgL_head1290z00_2570;
							BgL_zc3z04anonymousza32224ze3z87_2574:
								if (NULLP(BgL_l1288z00_2572))
									{	/* Expand/object.scm 391 */
										BgL_varsz00_2520 = CDR(BgL_head1290z00_2570);
									}
								else
									{	/* Expand/object.scm 391 */
										obj_t BgL_newtail1292z00_2576;

										{	/* Expand/object.scm 391 */
											obj_t BgL_arg2227z00_2578;

											{	/* Expand/object.scm 391 */
												obj_t BgL_bdgz00_2579;

												BgL_bdgz00_2579 = CAR(((obj_t) BgL_l1288z00_2572));
												{
													obj_t BgL_varz00_2580;
													obj_t BgL_exprz00_2581;

													if (PAIRP(BgL_bdgz00_2579))
														{	/* Expand/object.scm 392 */
															obj_t BgL_carzd2730zd2_2586;
															obj_t BgL_cdrzd2731zd2_2587;

															BgL_carzd2730zd2_2586 =
																CAR(((obj_t) BgL_bdgz00_2579));
															BgL_cdrzd2731zd2_2587 =
																CDR(((obj_t) BgL_bdgz00_2579));
															if (SYMBOLP(BgL_carzd2730zd2_2586))
																{	/* Expand/object.scm 392 */
																	if (PAIRP(BgL_cdrzd2731zd2_2587))
																		{	/* Expand/object.scm 392 */
																			if (NULLP(CDR(BgL_cdrzd2731zd2_2587)))
																				{	/* Expand/object.scm 392 */
																					BgL_varz00_2580 =
																						BgL_carzd2730zd2_2586;
																					BgL_exprz00_2581 =
																						CAR(BgL_cdrzd2731zd2_2587);
																					{	/* Expand/object.scm 394 */
																						obj_t BgL_locz00_2594;

																						BgL_locz00_2594 =
																							BGl_findzd2locationzf2locz20zztools_locationz00
																							(BgL_bdgz00_2579, BgL_xz00_28);
																						{	/* Expand/object.scm 394 */
																							obj_t BgL_idzd2typezd2_2595;

																							BgL_idzd2typezd2_2595 =
																								BGl_parsezd2idzd2zzast_identz00
																								(BgL_varz00_2580,
																								BgL_locz00_2594);
																							{	/* Expand/object.scm 395 */
																								obj_t BgL_idz00_2596;

																								BgL_idz00_2596 =
																									CAR(BgL_idzd2typezd2_2595);
																								{	/* Expand/object.scm 396 */
																									obj_t BgL_tz00_2597;

																									BgL_tz00_2597 =
																										CDR(BgL_idzd2typezd2_2595);
																									{	/* Expand/object.scm 397 */
																										obj_t BgL_klassz00_2598;

																										BgL_exprz00_2611 =
																											BgL_exprz00_2581;
																										BgL_bdgz00_2612 =
																											BgL_bdgz00_2579;
																										BgL_locz00_2613 =
																											BgL_locz00_2594;
																										{
																											obj_t
																												BgL_instantiatez00_2615;
																											obj_t BgL_bodyz00_2616;

																											if (PAIRP
																												(BgL_exprz00_2611))
																												{	/* Expand/object.scm 388 */
																													obj_t
																														BgL_arg2247z00_2621;
																													obj_t
																														BgL_arg2248z00_2622;
																													BgL_arg2247z00_2621 =
																														CAR(((obj_t)
																															BgL_exprz00_2611));
																													BgL_arg2248z00_2622 =
																														CDR(((obj_t)
																															BgL_exprz00_2611));
																													BgL_instantiatez00_2615
																														=
																														BgL_arg2247z00_2621;
																													BgL_bodyz00_2616 =
																														BgL_arg2248z00_2622;
																													{	/* Expand/object.scm 373 */
																														obj_t
																															BgL_locz00_2623;
																														BgL_locz00_2623 =
																															BGl_findzd2locationzf2locz20zztools_locationz00
																															(BgL_bodyz00_2616,
																															BgL_locz00_2613);
																														{	/* Expand/object.scm 373 */
																															obj_t
																																BgL_idzd2typezd2_2624;
																															BgL_idzd2typezd2_2624
																																=
																																BGl_parsezd2idzd2zzast_identz00
																																(BgL_instantiatez00_2615,
																																BgL_locz00_2623);
																															{	/* Expand/object.scm 374 */
																																obj_t
																																	BgL_kclassz00_2625;
																																BgL_kclassz00_2625
																																	=
																																	CDR
																																	(BgL_idzd2typezd2_2624);
																																{	/* Expand/object.scm 375 */

																																	if (
																																		(CAR
																																			(BgL_idzd2typezd2_2624)
																																			==
																																			CNST_TABLE_REF
																																			(31)))
																																		{	/* Expand/object.scm 379 */
																																			bool_t
																																				BgL_test2945z00_5704;
																																			{	/* Expand/object.scm 379 */
																																				obj_t
																																					BgL_classz00_3678;
																																				BgL_classz00_3678
																																					=
																																					BGl_tclassz00zzobject_classz00;
																																				if (BGL_OBJECTP(BgL_kclassz00_2625))
																																					{	/* Expand/object.scm 379 */
																																						BgL_objectz00_bglt
																																							BgL_arg1807z00_3680;
																																						BgL_arg1807z00_3680
																																							=
																																							(BgL_objectz00_bglt)
																																							(BgL_kclassz00_2625);
																																						if (BGL_CONDEXPAND_ISA_ARCH64())
																																							{	/* Expand/object.scm 379 */
																																								long
																																									BgL_idxz00_3686;
																																								BgL_idxz00_3686
																																									=
																																									BGL_OBJECT_INHERITANCE_NUM
																																									(BgL_arg1807z00_3680);
																																								BgL_test2945z00_5704
																																									=
																																									(VECTOR_REF
																																									(BGl_za2inheritancesza2z00zz__objectz00,
																																										(BgL_idxz00_3686
																																											+
																																											2L))
																																									==
																																									BgL_classz00_3678);
																																							}
																																						else
																																							{	/* Expand/object.scm 379 */
																																								bool_t
																																									BgL_res2653z00_3711;
																																								{	/* Expand/object.scm 379 */
																																									obj_t
																																										BgL_oclassz00_3694;
																																									{	/* Expand/object.scm 379 */
																																										obj_t
																																											BgL_arg1815z00_3702;
																																										long
																																											BgL_arg1816z00_3703;
																																										BgL_arg1815z00_3702
																																											=
																																											(BGl_za2classesza2z00zz__objectz00);
																																										{	/* Expand/object.scm 379 */
																																											long
																																												BgL_arg1817z00_3704;
																																											BgL_arg1817z00_3704
																																												=
																																												BGL_OBJECT_CLASS_NUM
																																												(BgL_arg1807z00_3680);
																																											BgL_arg1816z00_3703
																																												=
																																												(BgL_arg1817z00_3704
																																												-
																																												OBJECT_TYPE);
																																										}
																																										BgL_oclassz00_3694
																																											=
																																											VECTOR_REF
																																											(BgL_arg1815z00_3702,
																																											BgL_arg1816z00_3703);
																																									}
																																									{	/* Expand/object.scm 379 */
																																										bool_t
																																											BgL__ortest_1115z00_3695;
																																										BgL__ortest_1115z00_3695
																																											=
																																											(BgL_classz00_3678
																																											==
																																											BgL_oclassz00_3694);
																																										if (BgL__ortest_1115z00_3695)
																																											{	/* Expand/object.scm 379 */
																																												BgL_res2653z00_3711
																																													=
																																													BgL__ortest_1115z00_3695;
																																											}
																																										else
																																											{	/* Expand/object.scm 379 */
																																												long
																																													BgL_odepthz00_3696;
																																												{	/* Expand/object.scm 379 */
																																													obj_t
																																														BgL_arg1804z00_3697;
																																													BgL_arg1804z00_3697
																																														=
																																														(BgL_oclassz00_3694);
																																													BgL_odepthz00_3696
																																														=
																																														BGL_CLASS_DEPTH
																																														(BgL_arg1804z00_3697);
																																												}
																																												if ((2L < BgL_odepthz00_3696))
																																													{	/* Expand/object.scm 379 */
																																														obj_t
																																															BgL_arg1802z00_3699;
																																														{	/* Expand/object.scm 379 */
																																															obj_t
																																																BgL_arg1803z00_3700;
																																															BgL_arg1803z00_3700
																																																=
																																																(BgL_oclassz00_3694);
																																															BgL_arg1802z00_3699
																																																=
																																																BGL_CLASS_ANCESTORS_REF
																																																(BgL_arg1803z00_3700,
																																																2L);
																																														}
																																														BgL_res2653z00_3711
																																															=
																																															(BgL_arg1802z00_3699
																																															==
																																															BgL_classz00_3678);
																																													}
																																												else
																																													{	/* Expand/object.scm 379 */
																																														BgL_res2653z00_3711
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																								}
																																								BgL_test2945z00_5704
																																									=
																																									BgL_res2653z00_3711;
																																							}
																																					}
																																				else
																																					{	/* Expand/object.scm 379 */
																																						BgL_test2945z00_5704
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																			if (BgL_test2945z00_5704)
																																				{	/* Expand/object.scm 381 */
																																					bool_t
																																						BgL_test2950z00_5727;
																																					{
																																						BgL_tclassz00_bglt
																																							BgL_auxz00_5728;
																																						{
																																							obj_t
																																								BgL_auxz00_5729;
																																							{	/* Expand/object.scm 381 */
																																								BgL_objectz00_bglt
																																									BgL_tmpz00_5730;
																																								BgL_tmpz00_5730
																																									=
																																									(
																																									(BgL_objectz00_bglt)
																																									((BgL_typez00_bglt) BgL_kclassz00_2625));
																																								BgL_auxz00_5729
																																									=
																																									BGL_OBJECT_WIDENING
																																									(BgL_tmpz00_5730);
																																							}
																																							BgL_auxz00_5728
																																								=
																																								(
																																								(BgL_tclassz00_bglt)
																																								BgL_auxz00_5729);
																																						}
																																						BgL_test2950z00_5727
																																							=
																																							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5728))->BgL_abstractzf3zf3);
																																					}
																																					if (BgL_test2950z00_5727)
																																						{	/* Expand/object.scm 381 */
																																							BgL_klassz00_2598
																																								=
																																								BGl_errorz00zz__errorz00
																																								(BgL_instantiatez00_2615,
																																								BGl_string2671z00zzexpand_objectz00,
																																								BgL_bdgz00_2612);
																																						}
																																					else
																																						{	/* Expand/object.scm 381 */
																																							BgL_klassz00_2598
																																								=
																																								BgL_kclassz00_2625;
																																						}
																																				}
																																			else
																																				{	/* Expand/object.scm 379 */
																																					BgL_klassz00_2598
																																						=
																																						BGl_errorz00zz__errorz00
																																						(BgL_instantiatez00_2615,
																																						BGl_string2668z00zzexpand_objectz00,
																																						BgL_bdgz00_2612);
																																				}
																																		}
																																	else
																																		{	/* Expand/object.scm 377 */
																																			BgL_klassz00_2598
																																				=
																																				BGl_errorz00zz__errorz00
																																				(BgL_instantiatez00_2615,
																																				BGl_string2678z00zzexpand_objectz00,
																																				BgL_bdgz00_2612);
																																		}
																																}
																															}
																														}
																													}
																												}
																											else
																												{	/* Expand/object.scm 388 */
																													BgL_klassz00_2598 =
																														BGl_errorz00zz__errorz00
																														(BGl_string2677z00zzexpand_objectz00,
																														BGl_string2678z00zzexpand_objectz00,
																														BgL_bdgz00_2612);
																												}
																										}
																										{	/* Expand/object.scm 398 */

																											{	/* Expand/object.scm 399 */
																												bool_t
																													BgL_test2951z00_5740;
																												{	/* Expand/object.scm 399 */
																													bool_t
																														BgL_test2952z00_5741;
																													{	/* Expand/object.scm 399 */
																														BgL_typez00_bglt
																															BgL_arg2243z00_2608;
																														BgL_arg2243z00_2608
																															=
																															BGl_getzd2defaultzd2typez00zztype_cachez00
																															();
																														BgL_test2952z00_5741
																															=
																															(BgL_tz00_2597 ==
																															((obj_t)
																																BgL_arg2243z00_2608));
																													}
																													if (BgL_test2952z00_5741)
																														{	/* Expand/object.scm 399 */
																															BgL_test2951z00_5740
																																= ((bool_t) 1);
																														}
																													else
																														{	/* Expand/object.scm 399 */
																															BgL_test2951z00_5740
																																=
																																(BgL_tz00_2597
																																==
																																BgL_klassz00_2598);
																														}
																												}
																												if (BgL_test2951z00_5740)
																													{	/* Expand/object.scm 401 */
																														obj_t
																															BgL_list2239z00_2603;
																														{	/* Expand/object.scm 401 */
																															obj_t
																																BgL_arg2240z00_2604;
																															{	/* Expand/object.scm 401 */
																																obj_t
																																	BgL_arg2241z00_2605;
																																BgL_arg2241z00_2605
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_exprz00_2581,
																																	BNIL);
																																BgL_arg2240z00_2604
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_klassz00_2598,
																																	BgL_arg2241z00_2605);
																															}
																															BgL_list2239z00_2603
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_idz00_2596,
																																BgL_arg2240z00_2604);
																														}
																														BgL_arg2227z00_2578
																															=
																															BgL_list2239z00_2603;
																													}
																												else
																													{	/* Expand/object.scm 399 */
																														BgL_arg2227z00_2578
																															=
																															BGl_errorz00zz__errorz00
																															(CAR(BgL_xz00_28),
																															BGl_string2679z00zzexpand_objectz00,
																															BgL_bdgz00_2579);
																													}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			else
																				{	/* Expand/object.scm 392 */
																				BgL_tagzd2724zd2_2583:
																					BgL_arg2227z00_2578 =
																						BGl_errorz00zz__errorz00(CAR
																						(BgL_xz00_28),
																						BGl_string2678z00zzexpand_objectz00,
																						BgL_bdgz00_2579);
																				}
																		}
																	else
																		{	/* Expand/object.scm 392 */
																			goto BgL_tagzd2724zd2_2583;
																		}
																}
															else
																{	/* Expand/object.scm 392 */
																	goto BgL_tagzd2724zd2_2583;
																}
														}
													else
														{	/* Expand/object.scm 392 */
															goto BgL_tagzd2724zd2_2583;
														}
												}
											}
											BgL_newtail1292z00_2576 =
												MAKE_YOUNG_PAIR(BgL_arg2227z00_2578, BNIL);
										}
										SET_CDR(BgL_tail1291z00_2573, BgL_newtail1292z00_2576);
										{	/* Expand/object.scm 391 */
											obj_t BgL_arg2226z00_2577;

											BgL_arg2226z00_2577 = CDR(((obj_t) BgL_l1288z00_2572));
											{
												obj_t BgL_tail1291z00_5759;
												obj_t BgL_l1288z00_5758;

												BgL_l1288z00_5758 = BgL_arg2226z00_2577;
												BgL_tail1291z00_5759 = BgL_newtail1292z00_2576;
												BgL_tail1291z00_2573 = BgL_tail1291z00_5759;
												BgL_l1288z00_2572 = BgL_l1288z00_5758;
												goto BgL_zc3z04anonymousza32224ze3z87_2574;
											}
										}
									}
							}
						}
					{	/* Expand/object.scm 406 */
						obj_t BgL_arg2197z00_2521;

						{	/* Expand/object.scm 406 */
							obj_t BgL_arg2198z00_2522;
							obj_t BgL_arg2199z00_2523;

							if (NULLP(BgL_varsz00_2520))
								{	/* Expand/object.scm 406 */
									BgL_arg2198z00_2522 = BNIL;
								}
							else
								{	/* Expand/object.scm 406 */
									obj_t BgL_head1295z00_2526;

									BgL_head1295z00_2526 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1293z00_2528;
										obj_t BgL_tail1296z00_2529;

										BgL_l1293z00_2528 = BgL_varsz00_2520;
										BgL_tail1296z00_2529 = BgL_head1295z00_2526;
									BgL_zc3z04anonymousza32201ze3z87_2530:
										if (NULLP(BgL_l1293z00_2528))
											{	/* Expand/object.scm 406 */
												BgL_arg2198z00_2522 = CDR(BgL_head1295z00_2526);
											}
										else
											{	/* Expand/object.scm 406 */
												obj_t BgL_newtail1297z00_2532;

												{	/* Expand/object.scm 406 */
													obj_t BgL_arg2204z00_2534;

													{	/* Expand/object.scm 406 */
														obj_t BgL_varz00_2535;

														BgL_varz00_2535 = CAR(((obj_t) BgL_l1293z00_2528));
														{	/* Expand/object.scm 407 */
															obj_t BgL_idz00_2536;
															obj_t BgL_klassz00_2537;

															BgL_idz00_2536 = CAR(((obj_t) BgL_varz00_2535));
															{	/* Expand/object.scm 408 */
																obj_t BgL_pairz00_3735;

																BgL_pairz00_3735 =
																	CDR(((obj_t) BgL_varz00_2535));
																BgL_klassz00_2537 = CAR(BgL_pairz00_3735);
															}
															{	/* Expand/object.scm 410 */
																obj_t BgL_arg2205z00_2538;

																{	/* Expand/object.scm 410 */
																	obj_t BgL_arg2206z00_2539;
																	obj_t BgL_arg2207z00_2540;

																	{	/* Expand/object.scm 410 */
																		obj_t BgL_arg2208z00_2541;

																		BgL_arg2208z00_2541 =
																			(((BgL_typez00_bglt) COBJECT(
																					((BgL_typez00_bglt)
																						BgL_klassz00_2537)))->BgL_idz00);
																		BgL_arg2206z00_2539 =
																			BGl_makezd2typedzd2identz00zzast_identz00
																			(BgL_idz00_2536, BgL_arg2208z00_2541);
																	}
																	BgL_arg2207z00_2540 =
																		MAKE_YOUNG_PAIR
																		(BGl_allocatezd2exprzd2zzexpand_objectz00
																		(BgL_klassz00_2537), BNIL);
																	BgL_arg2205z00_2538 =
																		MAKE_YOUNG_PAIR(BgL_arg2206z00_2539,
																		BgL_arg2207z00_2540);
																}
																BgL_arg2204z00_2534 =
																	BGl_objectzd2epairifyzd2zzexpand_objectz00
																	(BgL_arg2205z00_2538, BgL_xz00_28);
															}
														}
													}
													BgL_newtail1297z00_2532 =
														MAKE_YOUNG_PAIR(BgL_arg2204z00_2534, BNIL);
												}
												SET_CDR(BgL_tail1296z00_2529, BgL_newtail1297z00_2532);
												{	/* Expand/object.scm 406 */
													obj_t BgL_arg2203z00_2533;

													BgL_arg2203z00_2533 =
														CDR(((obj_t) BgL_l1293z00_2528));
													{
														obj_t BgL_tail1296z00_5785;
														obj_t BgL_l1293z00_5784;

														BgL_l1293z00_5784 = BgL_arg2203z00_2533;
														BgL_tail1296z00_5785 = BgL_newtail1297z00_2532;
														BgL_tail1296z00_2529 = BgL_tail1296z00_5785;
														BgL_l1293z00_2528 = BgL_l1293z00_5784;
														goto BgL_zc3z04anonymousza32201ze3z87_2530;
													}
												}
											}
									}
								}
							{	/* Expand/object.scm 413 */
								obj_t BgL_arg2210z00_2544;
								obj_t BgL_arg2211z00_2545;

								if (NULLP(BgL_varsz00_2520))
									{	/* Expand/object.scm 413 */
										BgL_arg2210z00_2544 = BNIL;
									}
								else
									{	/* Expand/object.scm 413 */
										obj_t BgL_head1300z00_2548;

										BgL_head1300z00_2548 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1298z00_2550;
											obj_t BgL_tail1301z00_2551;

											BgL_l1298z00_2550 = BgL_varsz00_2520;
											BgL_tail1301z00_2551 = BgL_head1300z00_2548;
										BgL_zc3z04anonymousza32213ze3z87_2552:
											if (NULLP(BgL_l1298z00_2550))
												{	/* Expand/object.scm 413 */
													BgL_arg2210z00_2544 = CDR(BgL_head1300z00_2548);
												}
											else
												{	/* Expand/object.scm 413 */
													obj_t BgL_newtail1302z00_2554;

													{	/* Expand/object.scm 413 */
														obj_t BgL_arg2216z00_2556;

														{	/* Expand/object.scm 413 */
															obj_t BgL_varz00_2557;

															BgL_varz00_2557 =
																CAR(((obj_t) BgL_l1298z00_2550));
															{	/* Expand/object.scm 414 */
																obj_t BgL_idz00_2558;
																obj_t BgL_klassz00_2559;
																obj_t BgL_exprz00_2560;

																BgL_idz00_2558 = CAR(((obj_t) BgL_varz00_2557));
																{	/* Expand/object.scm 415 */
																	obj_t BgL_pairz00_3745;

																	BgL_pairz00_3745 =
																		CDR(((obj_t) BgL_varz00_2557));
																	BgL_klassz00_2559 = CAR(BgL_pairz00_3745);
																}
																{	/* Expand/object.scm 416 */
																	obj_t BgL_pairz00_3751;

																	{	/* Expand/object.scm 416 */
																		obj_t BgL_pairz00_3750;

																		BgL_pairz00_3750 =
																			CDR(((obj_t) BgL_varz00_2557));
																		BgL_pairz00_3751 = CDR(BgL_pairz00_3750);
																	}
																	BgL_exprz00_2560 = CAR(BgL_pairz00_3751);
																}
																{	/* Expand/object.scm 417 */
																	obj_t BgL_arg2217z00_2561;
																	obj_t BgL_arg2218z00_2562;
																	obj_t BgL_arg2219z00_2563;

																	BgL_arg2217z00_2561 =
																		CAR(((obj_t) BgL_exprz00_2560));
																	BgL_arg2218z00_2562 =
																		CDR(((obj_t) BgL_exprz00_2560));
																	{
																		BgL_tclassz00_bglt BgL_auxz00_5807;

																		{
																			obj_t BgL_auxz00_5808;

																			{	/* Expand/object.scm 418 */
																				BgL_objectz00_bglt BgL_tmpz00_5809;

																				BgL_tmpz00_5809 =
																					((BgL_objectz00_bglt)
																					((BgL_typez00_bglt)
																						BgL_klassz00_2559));
																				BgL_auxz00_5808 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5809);
																			}
																			BgL_auxz00_5807 =
																				((BgL_tclassz00_bglt) BgL_auxz00_5808);
																		}
																		BgL_arg2219z00_2563 =
																			(((BgL_tclassz00_bglt)
																				COBJECT(BgL_auxz00_5807))->
																			BgL_slotsz00);
																	}
																	BgL_arg2216z00_2556 =
																		BGl_instantiatezd2fillzd2zzexpand_objectz00
																		(BgL_arg2217z00_2561, BgL_arg2218z00_2562,
																		BgL_klassz00_2559, BgL_arg2219z00_2563,
																		BgL_idz00_2558, BgL_exprz00_2560,
																		BgL_ez00_29);
																}
															}
														}
														BgL_newtail1302z00_2554 =
															MAKE_YOUNG_PAIR(BgL_arg2216z00_2556, BNIL);
													}
													SET_CDR(BgL_tail1301z00_2551,
														BgL_newtail1302z00_2554);
													{	/* Expand/object.scm 413 */
														obj_t BgL_arg2215z00_2555;

														BgL_arg2215z00_2555 =
															CDR(((obj_t) BgL_l1298z00_2550));
														{
															obj_t BgL_tail1301z00_5821;
															obj_t BgL_l1298z00_5820;

															BgL_l1298z00_5820 = BgL_arg2215z00_2555;
															BgL_tail1301z00_5821 = BgL_newtail1302z00_2554;
															BgL_tail1301z00_2551 = BgL_tail1301z00_5821;
															BgL_l1298z00_2550 = BgL_l1298z00_5820;
															goto BgL_zc3z04anonymousza32213ze3z87_2552;
														}
													}
												}
										}
									}
								{	/* Expand/object.scm 420 */
									obj_t BgL_arg2220z00_2565;

									{	/* Expand/object.scm 420 */
										obj_t BgL_arg2221z00_2566;

										{	/* Expand/object.scm 420 */
											obj_t BgL_arg2222z00_2567;

											BgL_arg2222z00_2567 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_bodyz00_27, BNIL);
											BgL_arg2221z00_2566 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
												BgL_arg2222z00_2567);
										}
										BgL_arg2220z00_2565 =
											BGL_PROCEDURE_CALL2(BgL_ez00_29, BgL_arg2221z00_2566,
											BgL_ez00_29);
									}
									BgL_arg2211z00_2545 =
										MAKE_YOUNG_PAIR(BgL_arg2220z00_2565, BNIL);
								}
								BgL_arg2199z00_2523 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg2210z00_2544, BgL_arg2211z00_2545);
							}
							BgL_arg2197z00_2521 =
								MAKE_YOUNG_PAIR(BgL_arg2198z00_2522, BgL_arg2199z00_2523);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg2197z00_2521);
					}
				}
			}
		}

	}



/* expand-duplicate */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2duplicatezd2zzexpand_objectz00(obj_t
		BgL_xz00_30, obj_t BgL_ez00_31)
	{
		{	/* Expand/object.scm 425 */
			{
				obj_t BgL_duplicatez00_2632;
				obj_t BgL_dupz00_2633;
				obj_t BgL_provz00_2634;

				if (PAIRP(BgL_xz00_30))
					{	/* Expand/object.scm 426 */
						obj_t BgL_cdrzd2748zd2_2639;

						BgL_cdrzd2748zd2_2639 = CDR(((obj_t) BgL_xz00_30));
						if (PAIRP(BgL_cdrzd2748zd2_2639))
							{	/* Expand/object.scm 426 */
								obj_t BgL_arg2256z00_2641;
								obj_t BgL_arg2257z00_2642;
								obj_t BgL_arg2258z00_2643;

								BgL_arg2256z00_2641 = CAR(((obj_t) BgL_xz00_30));
								BgL_arg2257z00_2642 = CAR(BgL_cdrzd2748zd2_2639);
								BgL_arg2258z00_2643 = CDR(BgL_cdrzd2748zd2_2639);
								BgL_duplicatez00_2632 = BgL_arg2256z00_2641;
								BgL_dupz00_2633 = BgL_arg2257z00_2642;
								BgL_provz00_2634 = BgL_arg2258z00_2643;
								{	/* Expand/object.scm 428 */
									obj_t BgL_idzd2typezd2_2644;

									BgL_idzd2typezd2_2644 =
										BGl_parsezd2idzd2zzast_identz00(BgL_duplicatez00_2632,
										BGl_findzd2locationzd2zztools_locationz00(BgL_xz00_30));
									{	/* Expand/object.scm 428 */
										obj_t BgL_idz00_2645;

										BgL_idz00_2645 = CAR(BgL_idzd2typezd2_2644);
										{	/* Expand/object.scm 429 */
											obj_t BgL_classz00_2646;

											BgL_classz00_2646 = CDR(BgL_idzd2typezd2_2644);
											{	/* Expand/object.scm 430 */

												{	/* Expand/object.scm 432 */
													bool_t BgL_test2959z00_5849;

													{	/* Expand/object.scm 432 */
														obj_t BgL_classz00_3760;

														BgL_classz00_3760 = BGl_tclassz00zzobject_classz00;
														if (BGL_OBJECTP(BgL_classz00_2646))
															{	/* Expand/object.scm 432 */
																BgL_objectz00_bglt BgL_arg1807z00_3762;

																BgL_arg1807z00_3762 =
																	(BgL_objectz00_bglt) (BgL_classz00_2646);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Expand/object.scm 432 */
																		long BgL_idxz00_3768;

																		BgL_idxz00_3768 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3762);
																		BgL_test2959z00_5849 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3768 + 2L)) ==
																			BgL_classz00_3760);
																	}
																else
																	{	/* Expand/object.scm 432 */
																		bool_t BgL_res2655z00_3793;

																		{	/* Expand/object.scm 432 */
																			obj_t BgL_oclassz00_3776;

																			{	/* Expand/object.scm 432 */
																				obj_t BgL_arg1815z00_3784;
																				long BgL_arg1816z00_3785;

																				BgL_arg1815z00_3784 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Expand/object.scm 432 */
																					long BgL_arg1817z00_3786;

																					BgL_arg1817z00_3786 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3762);
																					BgL_arg1816z00_3785 =
																						(BgL_arg1817z00_3786 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3776 =
																					VECTOR_REF(BgL_arg1815z00_3784,
																					BgL_arg1816z00_3785);
																			}
																			{	/* Expand/object.scm 432 */
																				bool_t BgL__ortest_1115z00_3777;

																				BgL__ortest_1115z00_3777 =
																					(BgL_classz00_3760 ==
																					BgL_oclassz00_3776);
																				if (BgL__ortest_1115z00_3777)
																					{	/* Expand/object.scm 432 */
																						BgL_res2655z00_3793 =
																							BgL__ortest_1115z00_3777;
																					}
																				else
																					{	/* Expand/object.scm 432 */
																						long BgL_odepthz00_3778;

																						{	/* Expand/object.scm 432 */
																							obj_t BgL_arg1804z00_3779;

																							BgL_arg1804z00_3779 =
																								(BgL_oclassz00_3776);
																							BgL_odepthz00_3778 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3779);
																						}
																						if ((2L < BgL_odepthz00_3778))
																							{	/* Expand/object.scm 432 */
																								obj_t BgL_arg1802z00_3781;

																								{	/* Expand/object.scm 432 */
																									obj_t BgL_arg1803z00_3782;

																									BgL_arg1803z00_3782 =
																										(BgL_oclassz00_3776);
																									BgL_arg1802z00_3781 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3782, 2L);
																								}
																								BgL_res2655z00_3793 =
																									(BgL_arg1802z00_3781 ==
																									BgL_classz00_3760);
																							}
																						else
																							{	/* Expand/object.scm 432 */
																								BgL_res2655z00_3793 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2959z00_5849 = BgL_res2655z00_3793;
																	}
															}
														else
															{	/* Expand/object.scm 432 */
																BgL_test2959z00_5849 = ((bool_t) 0);
															}
													}
													if (BgL_test2959z00_5849)
														{	/* Expand/object.scm 434 */
															bool_t BgL_test2964z00_5872;

															{
																BgL_tclassz00_bglt BgL_auxz00_5873;

																{
																	obj_t BgL_auxz00_5874;

																	{	/* Expand/object.scm 434 */
																		BgL_objectz00_bglt BgL_tmpz00_5875;

																		BgL_tmpz00_5875 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_classz00_2646));
																		BgL_auxz00_5874 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5875);
																	}
																	BgL_auxz00_5873 =
																		((BgL_tclassz00_bglt) BgL_auxz00_5874);
																}
																BgL_test2964z00_5872 =
																	(((BgL_tclassz00_bglt)
																		COBJECT(BgL_auxz00_5873))->
																	BgL_abstractzf3zf3);
															}
															if (BgL_test2964z00_5872)
																{	/* Expand/object.scm 434 */
																	return
																		BGl_errorz00zz__errorz00
																		(BgL_duplicatez00_2632,
																		BGl_string2680z00zzexpand_objectz00,
																		BgL_xz00_30);
																}
															else
																{	/* Expand/object.scm 434 */
																	return
																		BGl_replacez12z12zztools_miscz00
																		(BgL_xz00_30,
																		BGl_duplicatezd2ze3makez31zzexpand_objectz00
																		(BgL_classz00_2646, BgL_dupz00_2633,
																			BgL_provz00_2634, BgL_xz00_30,
																			BgL_ez00_31));
																}
														}
													else
														{	/* Expand/object.scm 433 */
															obj_t BgL_arg2262z00_2650;

															{	/* Expand/object.scm 433 */
																obj_t BgL_list2263z00_2651;

																BgL_list2263z00_2651 =
																	MAKE_YOUNG_PAIR(BgL_idz00_2645, BNIL);
																BgL_arg2262z00_2650 =
																	BGl_formatz00zz__r4_output_6_10_3z00
																	(BGl_string2681z00zzexpand_objectz00,
																	BgL_list2263z00_2651);
															}
															return
																BGl_errorz00zz__errorz00(BgL_duplicatez00_2632,
																BgL_arg2262z00_2650, BgL_xz00_30);
														}
												}
											}
										}
									}
								}
							}
						else
							{	/* Expand/object.scm 426 */
							BgL_tagzd2739zd2_2636:
								return
									BGl_errorz00zz__errorz00(BGl_string2682z00zzexpand_objectz00,
									BGl_string2666z00zzexpand_objectz00, BgL_xz00_30);
							}
					}
				else
					{	/* Expand/object.scm 426 */
						goto BgL_tagzd2739zd2_2636;
					}
			}
		}

	}



/* &expand-duplicate */
	obj_t BGl_z62expandzd2duplicatezb0zzexpand_objectz00(obj_t BgL_envz00_4172,
		obj_t BgL_xz00_4173, obj_t BgL_ez00_4174)
	{
		{	/* Expand/object.scm 425 */
			return
				BGl_expandzd2duplicatezd2zzexpand_objectz00(BgL_xz00_4173,
				BgL_ez00_4174);
		}

	}



/* duplicate->make */
	obj_t BGl_duplicatezd2ze3makez31zzexpand_objectz00(obj_t BgL_classz00_32,
		obj_t BgL_duplicatedz00_33, obj_t BgL_providedz00_34, obj_t BgL_xz00_35,
		obj_t BgL_ez00_36)
	{
		{	/* Expand/object.scm 450 */
			{
				obj_t BgL_slotsz00_2782;
				obj_t BgL_slotsz00_2725;
				obj_t BgL_dupvarz00_2726;
				obj_t BgL_parentz00_2727;

				{	/* Expand/object.scm 507 */
					obj_t BgL_idz00_2656;

					BgL_idz00_2656 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_classz00_32)))->BgL_idz00);
					{	/* Expand/object.scm 507 */
						obj_t BgL_slotsz00_2657;

						{
							BgL_tclassz00_bglt BgL_auxz00_5891;

							{
								obj_t BgL_auxz00_5892;

								{	/* Expand/object.scm 508 */
									BgL_objectz00_bglt BgL_tmpz00_5893;

									BgL_tmpz00_5893 =
										((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_32));
									BgL_auxz00_5892 = BGL_OBJECT_WIDENING(BgL_tmpz00_5893);
								}
								BgL_auxz00_5891 = ((BgL_tclassz00_bglt) BgL_auxz00_5892);
							}
							BgL_slotsz00_2657 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5891))->BgL_slotsz00);
						}
						{	/* Expand/object.scm 508 */
							obj_t BgL_newz00_2658;

							BgL_newz00_2658 =
								BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(13));
							{	/* Expand/object.scm 509 */
								obj_t BgL_tmpz00_2659;

								BgL_tmpz00_2659 =
									BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(32));
								{	/* Expand/object.scm 510 */
									obj_t BgL_tnewz00_2660;

									BgL_tnewz00_2660 =
										BGl_makezd2typedzd2identz00zzast_identz00(BgL_newz00_2658,
										BgL_idz00_2656);
									{	/* Expand/object.scm 511 */
										obj_t BgL_dupvarz00_2661;

										BgL_dupvarz00_2661 =
											BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
											(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(33)));
										{	/* Expand/object.scm 512 */
											obj_t BgL_parentz00_2662;

											BgL_slotsz00_2782 = BgL_slotsz00_2657;
											if (NULLP(BgL_slotsz00_2782))
												{	/* Expand/object.scm 490 */
													BgL_parentz00_2662 = BgL_classz00_32;
												}
											else
												{	/* Expand/object.scm 492 */
													obj_t BgL_g1129z00_2785;
													obj_t BgL_g1130z00_2786;

													BgL_g1129z00_2785 =
														(((BgL_slotz00_bglt) COBJECT(
																((BgL_slotz00_bglt)
																	CAR(
																		((obj_t) BgL_slotsz00_2782)))))->
														BgL_classzd2ownerzd2);
													BgL_g1130z00_2786 = CDR(((obj_t) BgL_slotsz00_2782));
													{
														obj_t BgL_parentz00_2788;
														obj_t BgL_slotsz00_2789;

														BgL_parentz00_2788 = BgL_g1129z00_2785;
														BgL_slotsz00_2789 = BgL_g1130z00_2786;
														if (NULLP(BgL_slotsz00_2789))
															{	/* Expand/object.scm 495 */
																BgL_parentz00_2662 = BgL_parentz00_2788;
															}
														else
															{	/* Expand/object.scm 497 */
																bool_t BgL_test2967z00_5917;

																{	/* Expand/object.scm 497 */
																	obj_t BgL_arg2366z00_2796;

																	BgL_arg2366z00_2796 =
																		(((BgL_slotz00_bglt) COBJECT(
																				((BgL_slotz00_bglt)
																					CAR(
																						((obj_t) BgL_slotsz00_2789)))))->
																		BgL_classzd2ownerzd2);
																	BgL_test2967z00_5917 =
																		BGl_typezd2subclasszf3z21zzobject_classz00((
																			(BgL_typez00_bglt) BgL_parentz00_2788),
																		((BgL_typez00_bglt) BgL_arg2366z00_2796));
																}
																if (BgL_test2967z00_5917)
																	{	/* Expand/object.scm 497 */
																		BgL_parentz00_2662 =
																			(((BgL_slotz00_bglt) COBJECT(
																					((BgL_slotz00_bglt)
																						CAR(
																							((obj_t) BgL_slotsz00_2789)))))->
																			BgL_classzd2ownerzd2);
																	}
																else
																	{	/* Expand/object.scm 497 */
																		BgL_parentz00_2662 = BgL_parentz00_2788;
																	}
															}
													}
												}
											{	/* Expand/object.scm 513 */
												obj_t BgL_tdupvarz00_2663;

												{	/* Expand/object.scm 514 */
													obj_t BgL_arg2317z00_2723;

													BgL_arg2317z00_2723 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_parentz00_2662)))->
														BgL_idz00);
													BgL_tdupvarz00_2663 =
														BGl_makezd2typedzd2identz00zzast_identz00
														(BgL_dupvarz00_2661, BgL_arg2317z00_2723);
												}
												{	/* Expand/object.scm 514 */
													obj_t BgL_argsz00_2664;

													BgL_slotsz00_2725 = BgL_slotsz00_2657;
													BgL_dupvarz00_2726 = BgL_dupvarz00_2661;
													BgL_parentz00_2727 = BgL_parentz00_2662;
													{	/* Expand/object.scm 453 */
														obj_t BgL_vargsz00_2729;

														BgL_vargsz00_2729 =
															make_vector(bgl_list_length(BgL_slotsz00_2725),
															BUNSPEC);
														{
															obj_t BgL_providedz00_2731;

															BgL_providedz00_2731 = BgL_providedz00_34;
														BgL_zc3z04anonymousza32320ze3z87_2732:
															if (PAIRP(BgL_providedz00_2731))
																{	/* Expand/object.scm 457 */
																	obj_t BgL_pz00_2734;

																	BgL_pz00_2734 = CAR(BgL_providedz00_2731);
																	{
																		obj_t BgL_szd2namezd2_2735;
																		obj_t BgL_valuez00_2736;

																		if (PAIRP(BgL_pz00_2734))
																			{	/* Expand/object.scm 458 */
																				obj_t BgL_carzd2763zd2_2741;
																				obj_t BgL_cdrzd2764zd2_2742;

																				BgL_carzd2763zd2_2741 =
																					CAR(((obj_t) BgL_pz00_2734));
																				BgL_cdrzd2764zd2_2742 =
																					CDR(((obj_t) BgL_pz00_2734));
																				if (SYMBOLP(BgL_carzd2763zd2_2741))
																					{	/* Expand/object.scm 458 */
																						if (PAIRP(BgL_cdrzd2764zd2_2742))
																							{	/* Expand/object.scm 458 */
																								if (NULLP(CDR
																										(BgL_cdrzd2764zd2_2742)))
																									{	/* Expand/object.scm 458 */
																										BgL_szd2namezd2_2735 =
																											BgL_carzd2763zd2_2741;
																										BgL_valuez00_2736 =
																											CAR
																											(BgL_cdrzd2764zd2_2742);
																										{	/* Expand/object.scm 462 */
																											obj_t BgL_arg2330z00_2749;
																											obj_t BgL_arg2331z00_2750;

																											BgL_arg2330z00_2749 =
																												BGl_findzd2slotzd2offsetz00zzexpand_objectz00
																												(BgL_slotsz00_2725,
																												BgL_szd2namezd2_2735,
																												BGl_string2682z00zzexpand_objectz00,
																												BgL_pz00_2734);
																											BgL_arg2331z00_2750 =
																												MAKE_YOUNG_PAIR(BTRUE,
																												BGl_objectzd2epairifyzd2zzexpand_objectz00
																												(BgL_valuez00_2736,
																													BgL_pz00_2734));
																											VECTOR_SET
																												(BgL_vargsz00_2729,
																												(long)
																												CINT
																												(BgL_arg2330z00_2749),
																												BgL_arg2331z00_2750);
																									}}
																								else
																									{	/* Expand/object.scm 458 */
																									BgL_tagzd2757zd2_2738:
																										{	/* Expand/object.scm 465 */
																											obj_t BgL_arg2335z00_2752;

																											BgL_arg2335z00_2752 =
																												CAR(
																												((obj_t) BgL_xz00_35));
																											BGl_errorz00zz__errorz00
																												(BgL_arg2335z00_2752,
																												BGl_string2666z00zzexpand_objectz00,
																												BgL_xz00_35);
																										}
																									}
																							}
																						else
																							{	/* Expand/object.scm 458 */
																								goto BgL_tagzd2757zd2_2738;
																							}
																					}
																				else
																					{	/* Expand/object.scm 458 */
																						goto BgL_tagzd2757zd2_2738;
																					}
																			}
																		else
																			{	/* Expand/object.scm 458 */
																				goto BgL_tagzd2757zd2_2738;
																			}
																	}
																	{
																		obj_t BgL_providedz00_5959;

																		BgL_providedz00_5959 =
																			CDR(BgL_providedz00_2731);
																		BgL_providedz00_2731 = BgL_providedz00_5959;
																		goto BgL_zc3z04anonymousza32320ze3z87_2732;
																	}
																}
															else
																{	/* Expand/object.scm 456 */
																	((bool_t) 0);
																}
														}
														{
															long BgL_iz00_2756;
															obj_t BgL_slotsz00_2757;

															BgL_iz00_2756 = 0L;
															BgL_slotsz00_2757 = BgL_slotsz00_2725;
														BgL_zc3z04anonymousza32337ze3z87_2758:
															if (PAIRP(BgL_slotsz00_2757))
																{	/* Expand/object.scm 471 */
																	obj_t BgL_valuez00_2760;

																	BgL_valuez00_2760 =
																		VECTOR_REF(BgL_vargsz00_2729,
																		BgL_iz00_2756);
																	if (PAIRP(BgL_valuez00_2760))
																		{	/* Expand/object.scm 472 */
																			BFALSE;
																		}
																	else
																		{	/* Expand/object.scm 475 */
																			obj_t BgL_slotz00_2762;

																			BgL_slotz00_2762 = CAR(BgL_slotsz00_2757);
																			{	/* Expand/object.scm 475 */
																				obj_t BgL_claza7za7z00_2763;

																				BgL_claza7za7z00_2763 =
																					(((BgL_slotz00_bglt) COBJECT(
																							((BgL_slotz00_bglt)
																								BgL_slotz00_2762)))->
																					BgL_classzd2ownerzd2);
																				{	/* Expand/object.scm 476 */
																					obj_t BgL_valz00_2764;

																					if (
																						(BgL_claza7za7z00_2763 ==
																							BgL_parentz00_2727))
																						{	/* Expand/object.scm 478 */
																							obj_t BgL_arg2341z00_2766;

																							BgL_arg2341z00_2766 =
																								(((BgL_slotz00_bglt) COBJECT(
																										((BgL_slotz00_bglt)
																											BgL_slotz00_2762)))->
																								BgL_idz00);
																							BgL_valz00_2764 =
																								BGl_fieldzd2accesszd2zzast_objectz00
																								(BgL_dupvarz00_2726,
																								BgL_arg2341z00_2766, BTRUE);
																						}
																					else
																						{	/* Expand/object.scm 479 */
																							obj_t BgL_tmpz00_2767;

																							BgL_tmpz00_2767 =
																								BGl_gensymz00zz__r4_symbols_6_4z00
																								(CNST_TABLE_REF(32));
																							{	/* Expand/object.scm 479 */
																								obj_t BgL_cidz00_2768;

																								BgL_cidz00_2768 =
																									(((BgL_typez00_bglt) COBJECT(
																											((BgL_typez00_bglt)
																												BgL_claza7za7z00_2763)))->
																									BgL_idz00);
																								{	/* Expand/object.scm 480 */
																									obj_t BgL_ttmpz00_2769;

																									BgL_ttmpz00_2769 =
																										BGl_makezd2typedzd2identz00zzast_identz00
																										(BgL_tmpz00_2767,
																										BgL_cidz00_2768);
																									{	/* Expand/object.scm 481 */

																										{	/* Expand/object.scm 482 */
																											obj_t BgL_arg2342z00_2770;

																											{	/* Expand/object.scm 482 */
																												obj_t
																													BgL_arg2345z00_2771;
																												obj_t
																													BgL_arg2346z00_2772;
																												{	/* Expand/object.scm 482 */
																													obj_t
																														BgL_arg2348z00_2773;
																													{	/* Expand/object.scm 482 */
																														obj_t
																															BgL_arg2349z00_2774;
																														BgL_arg2349z00_2774
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_dupvarz00_2726,
																															BNIL);
																														BgL_arg2348z00_2773
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_ttmpz00_2769,
																															BgL_arg2349z00_2774);
																													}
																													BgL_arg2345z00_2771 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2348z00_2773,
																														BNIL);
																												}
																												{	/* Expand/object.scm 483 */
																													obj_t
																														BgL_arg2350z00_2775;
																													{	/* Expand/object.scm 483 */
																														obj_t
																															BgL_arg2351z00_2776;
																														BgL_arg2351z00_2776
																															=
																															(((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt) BgL_slotz00_2762)))->BgL_idz00);
																														BgL_arg2350z00_2775
																															=
																															BGl_fieldzd2accesszd2zzast_objectz00
																															(BgL_tmpz00_2767,
																															BgL_arg2351z00_2776,
																															BTRUE);
																													}
																													BgL_arg2346z00_2772 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2350z00_2775,
																														BNIL);
																												}
																												BgL_arg2342z00_2770 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg2345z00_2771,
																													BgL_arg2346z00_2772);
																											}
																											BgL_valz00_2764 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(6),
																												BgL_arg2342z00_2770);
																										}
																									}
																								}
																							}
																						}
																					{	/* Expand/object.scm 477 */

																						{	/* Expand/object.scm 484 */
																							obj_t BgL_arg2340z00_2765;

																							BgL_arg2340z00_2765 =
																								MAKE_YOUNG_PAIR(BTRUE,
																								BgL_valz00_2764);
																							VECTOR_SET(BgL_vargsz00_2729,
																								BgL_iz00_2756,
																								BgL_arg2340z00_2765);
																						}
																					}
																				}
																			}
																		}
																	{
																		obj_t BgL_slotsz00_5993;
																		long BgL_iz00_5991;

																		BgL_iz00_5991 = (BgL_iz00_2756 + 1L);
																		BgL_slotsz00_5993 = CDR(BgL_slotsz00_2757);
																		BgL_slotsz00_2757 = BgL_slotsz00_5993;
																		BgL_iz00_2756 = BgL_iz00_5991;
																		goto BgL_zc3z04anonymousza32337ze3z87_2758;
																	}
																}
															else
																{	/* Expand/object.scm 470 */
																	((bool_t) 0);
																}
														}
														BgL_argsz00_2664 =
															BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
															(BgL_vargsz00_2729);
													}
													{	/* Expand/object.scm 515 */

														{	/* Expand/object.scm 518 */
															obj_t BgL_arg2265z00_2665;

															{	/* Expand/object.scm 518 */
																obj_t BgL_arg2266z00_2666;
																obj_t BgL_arg2267z00_2667;

																{	/* Expand/object.scm 518 */
																	obj_t BgL_arg2268z00_2668;
																	obj_t BgL_arg2269z00_2669;

																	{	/* Expand/object.scm 518 */
																		obj_t BgL_arg2270z00_2670;

																		{	/* Expand/object.scm 518 */
																			obj_t BgL_arg2271z00_2671;

																			BgL_arg2271z00_2671 =
																				BGL_PROCEDURE_CALL2(BgL_ez00_36,
																				BgL_duplicatedz00_33, BgL_ez00_36);
																			BgL_arg2270z00_2670 =
																				MAKE_YOUNG_PAIR(BgL_arg2271z00_2671,
																				BNIL);
																		}
																		BgL_arg2268z00_2668 =
																			MAKE_YOUNG_PAIR(BgL_tdupvarz00_2663,
																			BgL_arg2270z00_2670);
																	}
																	{	/* Expand/object.scm 519 */
																		obj_t BgL_arg2272z00_2672;

																		{	/* Expand/object.scm 519 */
																			obj_t BgL_arg2273z00_2673;

																			BgL_arg2273z00_2673 =
																				MAKE_YOUNG_PAIR
																				(BGl_allocatezd2exprzd2zzexpand_objectz00
																				(BgL_classz00_32), BNIL);
																			BgL_arg2272z00_2672 =
																				MAKE_YOUNG_PAIR(BgL_tnewz00_2660,
																				BgL_arg2273z00_2673);
																		}
																		BgL_arg2269z00_2669 =
																			MAKE_YOUNG_PAIR(BgL_arg2272z00_2672,
																			BNIL);
																	}
																	BgL_arg2266z00_2666 =
																		MAKE_YOUNG_PAIR(BgL_arg2268z00_2668,
																		BgL_arg2269z00_2669);
																}
																{	/* Expand/object.scm 522 */
																	obj_t BgL_arg2275z00_2675;
																	obj_t BgL_arg2276z00_2676;

																	{	/* Expand/object.scm 522 */
																		obj_t BgL_zc3z04anonymousza32280ze3z87_4177;

																		BgL_zc3z04anonymousza32280ze3z87_4177 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza32280ze3ze5zzexpand_objectz00,
																			(int) (2L), (int) (2L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza32280ze3z87_4177,
																			(int) (0L), BgL_ez00_36);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza32280ze3z87_4177,
																			(int) (1L), BgL_newz00_2658);
																		{	/* Expand/object.scm 521 */
																			obj_t BgL_list2278z00_2678;

																			{	/* Expand/object.scm 521 */
																				obj_t BgL_arg2279z00_2679;

																				BgL_arg2279z00_2679 =
																					MAKE_YOUNG_PAIR(BgL_argsz00_2664,
																					BNIL);
																				BgL_list2278z00_2678 =
																					MAKE_YOUNG_PAIR(BgL_slotsz00_2657,
																					BgL_arg2279z00_2679);
																			}
																			BgL_arg2275z00_2675 =
																				BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																				(BgL_zc3z04anonymousza32280ze3z87_4177,
																				BgL_list2278z00_2678);
																	}}
																	{	/* Expand/object.scm 527 */
																		obj_t BgL_arg2287z00_2689;
																		obj_t BgL_arg2288z00_2690;

																		if (CBOOL
																			(BGl_findzd2classzd2constructorz00zzobject_classz00
																				(((BgL_typez00_bglt) BgL_classz00_32))))
																			{	/* Expand/object.scm 528 */
																				BgL_globalz00_bglt BgL_gz00_2692;

																				{
																					BgL_tclassz00_bglt BgL_auxz00_6022;

																					{
																						obj_t BgL_auxz00_6023;

																						{	/* Expand/object.scm 528 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_6024;
																							BgL_tmpz00_6024 =
																								((BgL_objectz00_bglt) (
																									(BgL_typez00_bglt)
																									BgL_classz00_32));
																							BgL_auxz00_6023 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_6024);
																						}
																						BgL_auxz00_6022 =
																							((BgL_tclassz00_bglt)
																							BgL_auxz00_6023);
																					}
																					BgL_gz00_2692 =
																						(((BgL_tclassz00_bglt)
																							COBJECT(BgL_auxz00_6022))->
																						BgL_holderz00);
																				}
																				{	/* Expand/object.scm 529 */
																					obj_t BgL_arg2290z00_2693;

																					{	/* Expand/object.scm 529 */
																						obj_t BgL_arg2291z00_2694;
																						obj_t BgL_arg2292z00_2695;

																						{	/* Expand/object.scm 529 */
																							obj_t BgL_arg2293z00_2696;
																							obj_t BgL_arg2294z00_2697;

																							{	/* Expand/object.scm 529 */
																								obj_t BgL_arg2295z00_2698;

																								{	/* Expand/object.scm 529 */
																									obj_t BgL_arg2296z00_2699;

																									BgL_arg2296z00_2699 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(8), BNIL);
																									BgL_arg2295z00_2698 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(14),
																										BgL_arg2296z00_2699);
																								}
																								BgL_arg2293z00_2696 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(10), BgL_arg2295z00_2698);
																							}
																							{	/* Expand/object.scm 530 */
																								obj_t BgL_arg2297z00_2700;

																								{	/* Expand/object.scm 530 */
																									obj_t BgL_arg2298z00_2701;

																									{	/* Expand/object.scm 530 */
																										obj_t BgL_arg2299z00_2702;
																										obj_t BgL_arg2301z00_2703;

																										BgL_arg2299z00_2702 =
																											(((BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														BgL_gz00_2692)))->
																											BgL_idz00);
																										BgL_arg2301z00_2703 =
																											MAKE_YOUNG_PAIR(((
																													(BgL_globalz00_bglt)
																													COBJECT
																													(BgL_gz00_2692))->
																												BgL_modulez00), BNIL);
																										BgL_arg2298z00_2701 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2299z00_2702,
																											BgL_arg2301z00_2703);
																									}
																									BgL_arg2297z00_2700 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(10),
																										BgL_arg2298z00_2701);
																								}
																								BgL_arg2294z00_2697 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2297z00_2700, BNIL);
																							}
																							BgL_arg2291z00_2694 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2293z00_2696,
																								BgL_arg2294z00_2697);
																						}
																						BgL_arg2292z00_2695 =
																							MAKE_YOUNG_PAIR(BgL_newz00_2658,
																							BNIL);
																						BgL_arg2290z00_2693 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2291z00_2694,
																							BgL_arg2292z00_2695);
																					}
																					BgL_arg2287z00_2689 =
																						BGL_PROCEDURE_CALL2(BgL_ez00_36,
																						BgL_arg2290z00_2693, BgL_ez00_36);
																				}
																			}
																		else
																			{	/* Expand/object.scm 527 */
																				BgL_arg2287z00_2689 = BFALSE;
																			}
																		{	/* Expand/object.scm 535 */
																			obj_t BgL_arg2304z00_2705;
																			obj_t BgL_arg2305z00_2706;

																			{	/* Expand/object.scm 535 */
																				obj_t
																					BgL_zc3z04anonymousza32309ze3z87_4176;
																				BgL_zc3z04anonymousza32309ze3z87_4176 =
																					MAKE_FX_PROCEDURE
																					(BGl_z62zc3z04anonymousza32309ze3ze5zzexpand_objectz00,
																					(int) (2L), (int) (2L));
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza32309ze3z87_4176,
																					(int) (0L), BgL_ez00_36);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza32309ze3z87_4176,
																					(int) (1L), BgL_newz00_2658);
																				{	/* Expand/object.scm 534 */
																					obj_t BgL_list2307z00_2708;

																					{	/* Expand/object.scm 534 */
																						obj_t BgL_arg2308z00_2709;

																						BgL_arg2308z00_2709 =
																							MAKE_YOUNG_PAIR(BgL_argsz00_2664,
																							BNIL);
																						BgL_list2307z00_2708 =
																							MAKE_YOUNG_PAIR(BgL_slotsz00_2657,
																							BgL_arg2308z00_2709);
																					}
																					BgL_arg2304z00_2705 =
																						BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																						(BgL_zc3z04anonymousza32309ze3z87_4176,
																						BgL_list2307z00_2708);
																			}}
																			BgL_arg2305z00_2706 =
																				MAKE_YOUNG_PAIR(BgL_newz00_2658, BNIL);
																			BgL_arg2288z00_2690 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg2304z00_2705,
																				BgL_arg2305z00_2706);
																		}
																		BgL_arg2276z00_2676 =
																			MAKE_YOUNG_PAIR(BgL_arg2287z00_2689,
																			BgL_arg2288z00_2690);
																	}
																	BgL_arg2267z00_2667 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_arg2275z00_2675, BgL_arg2276z00_2676);
																}
																BgL_arg2265z00_2665 =
																	MAKE_YOUNG_PAIR(BgL_arg2266z00_2666,
																	BgL_arg2267z00_2667);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																BgL_arg2265z00_2665);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &slot-set */
	obj_t BGl_z62slotzd2setzb0zzexpand_objectz00(obj_t BgL_newz00_2800,
		obj_t BgL_slotz00_2801, obj_t BgL_vz00_2802)
	{
		{	/* Expand/object.scm 505 */
			{	/* Expand/object.scm 503 */
				obj_t BgL_idz00_2805;

				BgL_idz00_2805 =
					(((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_slotz00_2801)))->BgL_idz00);
				{	/* Expand/object.scm 505 */
					obj_t BgL_arg2370z00_2806;

					{	/* Expand/object.scm 505 */
						obj_t BgL_arg2371z00_2807;
						obj_t BgL_arg2373z00_2808;

						BgL_arg2371z00_2807 =
							BGl_fieldzd2accesszd2zzast_objectz00(BgL_newz00_2800,
							BgL_idz00_2805, BTRUE);
						BgL_arg2373z00_2808 = MAKE_YOUNG_PAIR(BgL_vz00_2802, BNIL);
						BgL_arg2370z00_2806 =
							MAKE_YOUNG_PAIR(BgL_arg2371z00_2807, BgL_arg2373z00_2808);
					}
					return MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg2370z00_2806);
				}
			}
		}

	}



/* &<@anonymous:2309> */
	obj_t BGl_z62zc3z04anonymousza32309ze3ze5zzexpand_objectz00(obj_t
		BgL_envz00_4178, obj_t BgL_slotz00_4181, obj_t BgL_valz00_4182)
	{
		{	/* Expand/object.scm 534 */
			{	/* Expand/object.scm 535 */
				obj_t BgL_ez00_4179;
				obj_t BgL_newz00_4180;

				BgL_ez00_4179 = ((obj_t) PROCEDURE_REF(BgL_envz00_4178, (int) (0L)));
				BgL_newz00_4180 = ((obj_t) PROCEDURE_REF(BgL_envz00_4178, (int) (1L)));
				{	/* Expand/object.scm 535 */
					bool_t BgL_test2977z00_6082;

					if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
							((BgL_slotz00_bglt) BgL_slotz00_4181)))
						{	/* Expand/object.scm 535 */
							if (
								(((BgL_slotz00_bglt) COBJECT(
											((BgL_slotz00_bglt) BgL_slotz00_4181)))->
									BgL_readzd2onlyzf3z21))
								{	/* Expand/object.scm 536 */
									BgL_test2977z00_6082 = ((bool_t) 0);
								}
							else
								{	/* Expand/object.scm 536 */
									BgL_test2977z00_6082 = CBOOL(CAR(((obj_t) BgL_valz00_4182)));
								}
						}
					else
						{	/* Expand/object.scm 535 */
							BgL_test2977z00_6082 = ((bool_t) 0);
						}
					if (BgL_test2977z00_6082)
						{	/* Expand/object.scm 538 */
							obj_t BgL_arg2313z00_4266;
							obj_t BgL_arg2314z00_4267;

							BgL_arg2313z00_4266 = CDR(((obj_t) BgL_valz00_4182));
							{	/* Expand/object.scm 539 */
								obj_t BgL_arg2315z00_4268;

								{	/* Expand/object.scm 539 */
									obj_t BgL_arg2316z00_4269;

									BgL_arg2316z00_4269 = CDR(((obj_t) BgL_valz00_4182));
									BgL_arg2315z00_4268 =
										BGL_PROCEDURE_CALL2(BgL_ez00_4179, BgL_arg2316z00_4269,
										BgL_ez00_4179);
								}
								BgL_arg2314z00_4267 =
									BGl_z62slotzd2setzb0zzexpand_objectz00(BgL_newz00_4180,
									BgL_slotz00_4181, BgL_arg2315z00_4268);
							}
							return
								BGl_objectzd2epairifyzd2zzexpand_objectz00(BgL_arg2314z00_4267,
								BgL_arg2313z00_4266);
						}
					else
						{	/* Expand/object.scm 535 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &<@anonymous:2280> */
	obj_t BGl_z62zc3z04anonymousza32280ze3ze5zzexpand_objectz00(obj_t
		BgL_envz00_4183, obj_t BgL_slotz00_4186, obj_t BgL_valz00_4187)
	{
		{	/* Expand/object.scm 521 */
			{	/* Expand/object.scm 522 */
				obj_t BgL_ez00_4184;
				obj_t BgL_newz00_4185;

				BgL_ez00_4184 = ((obj_t) PROCEDURE_REF(BgL_envz00_4183, (int) (0L)));
				BgL_newz00_4185 = ((obj_t) PROCEDURE_REF(BgL_envz00_4183, (int) (1L)));
				if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
						((BgL_slotz00_bglt) BgL_slotz00_4186)))
					{	/* Expand/object.scm 522 */
						return BFALSE;
					}
				else
					{	/* Expand/object.scm 523 */
						obj_t BgL_arg2282z00_4270;
						obj_t BgL_arg2283z00_4271;

						BgL_arg2282z00_4270 = CDR(((obj_t) BgL_valz00_4187));
						{	/* Expand/object.scm 524 */
							obj_t BgL_arg2284z00_4272;

							{	/* Expand/object.scm 524 */
								obj_t BgL_arg2286z00_4273;

								BgL_arg2286z00_4273 = CDR(((obj_t) BgL_valz00_4187));
								BgL_arg2284z00_4272 =
									BGL_PROCEDURE_CALL2(BgL_ez00_4184, BgL_arg2286z00_4273,
									BgL_ez00_4184);
							}
							BgL_arg2283z00_4271 =
								BGl_z62slotzd2setzb0zzexpand_objectz00(BgL_newz00_4185,
								BgL_slotz00_4186, BgL_arg2284z00_4272);
						}
						return
							BGl_objectzd2epairifyzd2zzexpand_objectz00(BgL_arg2283z00_4271,
							BgL_arg2282z00_4270);
					}
			}
		}

	}



/* allocate-expr */
	obj_t BGl_allocatezd2exprzd2zzexpand_objectz00(obj_t BgL_classz00_37)
	{
		{	/* Expand/object.scm 546 */
			if (BGl_widezd2classzf3z21zzobject_classz00(BgL_classz00_37))
				{	/* Expand/object.scm 548 */
					obj_t BgL_superz00_2813;

					{
						BgL_tclassz00_bglt BgL_auxz00_6125;

						{
							obj_t BgL_auxz00_6126;

							{	/* Expand/object.scm 548 */
								BgL_objectz00_bglt BgL_tmpz00_6127;

								BgL_tmpz00_6127 =
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_37));
								BgL_auxz00_6126 = BGL_OBJECT_WIDENING(BgL_tmpz00_6127);
							}
							BgL_auxz00_6125 = ((BgL_tclassz00_bglt) BgL_auxz00_6126);
						}
						BgL_superz00_2813 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6125))->
							BgL_itszd2superzd2);
					}
					{	/* Expand/object.scm 550 */
						obj_t BgL_arg2375z00_2814;

						BgL_arg2375z00_2814 =
							BGl_classgenzd2allocatezd2exprz00zzobject_classgenz00(
							((BgL_typez00_bglt) BgL_superz00_2813));
						return
							BGl_classgenzd2widenzd2exprz00zzobject_classgenz00(
							((BgL_typez00_bglt) BgL_classz00_37), BgL_arg2375z00_2814);
					}
				}
			else
				{	/* Expand/object.scm 547 */
					return
						BGl_classgenzd2allocatezd2exprz00zzobject_classgenz00(
						((BgL_typez00_bglt) BgL_classz00_37));
				}
		}

	}



/* expand-widen! */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2widenz12zc0zzexpand_objectz00(obj_t
		BgL_xz00_38, obj_t BgL_ez00_39)
	{
		{	/* Expand/object.scm 556 */
			{
				obj_t BgL_widenz12z12_2815;
				obj_t BgL_objz00_2816;
				obj_t BgL_providedz00_2817;

				if (PAIRP(BgL_xz00_38))
					{	/* Expand/object.scm 557 */
						obj_t BgL_cdrzd2782zd2_2822;

						BgL_cdrzd2782zd2_2822 = CDR(((obj_t) BgL_xz00_38));
						if (PAIRP(BgL_cdrzd2782zd2_2822))
							{	/* Expand/object.scm 557 */
								obj_t BgL_arg2378z00_2824;
								obj_t BgL_arg2379z00_2825;
								obj_t BgL_arg2380z00_2826;

								BgL_arg2378z00_2824 = CAR(((obj_t) BgL_xz00_38));
								BgL_arg2379z00_2825 = CAR(BgL_cdrzd2782zd2_2822);
								BgL_arg2380z00_2826 = CDR(BgL_cdrzd2782zd2_2822);
								BgL_widenz12z12_2815 = BgL_arg2378z00_2824;
								BgL_objz00_2816 = BgL_arg2379z00_2825;
								BgL_providedz00_2817 = BgL_arg2380z00_2826;
								{	/* Expand/object.scm 559 */
									BgL_typez00_bglt BgL_classz00_2827;

									BgL_classz00_2827 =
										BGl_typezd2ofzd2idz00zzast_identz00(BgL_widenz12z12_2815,
										BGl_findzd2locationzd2zztools_locationz00(BgL_xz00_38));
									{	/* Expand/object.scm 560 */
										bool_t BgL_test2984z00_6151;

										{	/* Expand/object.scm 560 */
											bool_t BgL_test2985z00_6152;

											{	/* Expand/object.scm 560 */
												obj_t BgL_classz00_3846;

												BgL_classz00_3846 = BGl_tclassz00zzobject_classz00;
												{	/* Expand/object.scm 560 */
													BgL_objectz00_bglt BgL_arg1807z00_3848;

													{	/* Expand/object.scm 560 */
														obj_t BgL_tmpz00_6153;

														BgL_tmpz00_6153 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_classz00_2827));
														BgL_arg1807z00_3848 =
															(BgL_objectz00_bglt) (BgL_tmpz00_6153);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Expand/object.scm 560 */
															long BgL_idxz00_3854;

															BgL_idxz00_3854 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3848);
															BgL_test2985z00_6152 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_3854 + 2L)) == BgL_classz00_3846);
														}
													else
														{	/* Expand/object.scm 560 */
															bool_t BgL_res2657z00_3879;

															{	/* Expand/object.scm 560 */
																obj_t BgL_oclassz00_3862;

																{	/* Expand/object.scm 560 */
																	obj_t BgL_arg1815z00_3870;
																	long BgL_arg1816z00_3871;

																	BgL_arg1815z00_3870 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Expand/object.scm 560 */
																		long BgL_arg1817z00_3872;

																		BgL_arg1817z00_3872 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3848);
																		BgL_arg1816z00_3871 =
																			(BgL_arg1817z00_3872 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_3862 =
																		VECTOR_REF(BgL_arg1815z00_3870,
																		BgL_arg1816z00_3871);
																}
																{	/* Expand/object.scm 560 */
																	bool_t BgL__ortest_1115z00_3863;

																	BgL__ortest_1115z00_3863 =
																		(BgL_classz00_3846 == BgL_oclassz00_3862);
																	if (BgL__ortest_1115z00_3863)
																		{	/* Expand/object.scm 560 */
																			BgL_res2657z00_3879 =
																				BgL__ortest_1115z00_3863;
																		}
																	else
																		{	/* Expand/object.scm 560 */
																			long BgL_odepthz00_3864;

																			{	/* Expand/object.scm 560 */
																				obj_t BgL_arg1804z00_3865;

																				BgL_arg1804z00_3865 =
																					(BgL_oclassz00_3862);
																				BgL_odepthz00_3864 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_3865);
																			}
																			if ((2L < BgL_odepthz00_3864))
																				{	/* Expand/object.scm 560 */
																					obj_t BgL_arg1802z00_3867;

																					{	/* Expand/object.scm 560 */
																						obj_t BgL_arg1803z00_3868;

																						BgL_arg1803z00_3868 =
																							(BgL_oclassz00_3862);
																						BgL_arg1802z00_3867 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_3868, 2L);
																					}
																					BgL_res2657z00_3879 =
																						(BgL_arg1802z00_3867 ==
																						BgL_classz00_3846);
																				}
																			else
																				{	/* Expand/object.scm 560 */
																					BgL_res2657z00_3879 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2985z00_6152 = BgL_res2657z00_3879;
														}
												}
											}
											if (BgL_test2985z00_6152)
												{	/* Expand/object.scm 560 */
													obj_t BgL_tmpz00_6176;

													{
														BgL_tclassz00_bglt BgL_auxz00_6177;

														{
															obj_t BgL_auxz00_6178;

															{	/* Expand/object.scm 560 */
																BgL_objectz00_bglt BgL_tmpz00_6179;

																BgL_tmpz00_6179 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_classz00_2827));
																BgL_auxz00_6178 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6179);
															}
															BgL_auxz00_6177 =
																((BgL_tclassz00_bglt) BgL_auxz00_6178);
														}
														BgL_tmpz00_6176 =
															(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6177))->
															BgL_wideningz00);
													}
													BgL_test2984z00_6151 = CBOOL(BgL_tmpz00_6176);
												}
											else
												{	/* Expand/object.scm 560 */
													BgL_test2984z00_6151 = ((bool_t) 0);
												}
										}
										if (BgL_test2984z00_6151)
											{	/* Expand/object.scm 561 */
												obj_t BgL_arg2383z00_2830;

												{	/* Expand/object.scm 561 */
													obj_t BgL_arg2384z00_2831;

													BgL_arg2384z00_2831 =
														BGl_expandzd2wideningzd2zzexpand_objectz00
														(BgL_xz00_38, BgL_classz00_2827, BgL_objz00_2816,
														BgL_providedz00_2817, BgL_ez00_39);
													BgL_arg2383z00_2830 =
														BGL_PROCEDURE_CALL2(BgL_ez00_39,
														BgL_arg2384z00_2831, BgL_ez00_39);
												}
												return
													BGl_replacez12z12zztools_miscz00(BgL_xz00_38,
													BgL_arg2383z00_2830);
											}
										else
											{	/* Expand/object.scm 563 */
												obj_t BgL_arg2385z00_2832;

												{	/* Expand/object.scm 563 */
													obj_t BgL_arg2386z00_2833;

													BgL_arg2386z00_2833 =
														(((BgL_typez00_bglt) COBJECT(BgL_classz00_2827))->
														BgL_idz00);
													{	/* Expand/object.scm 563 */
														obj_t BgL_list2387z00_2834;

														BgL_list2387z00_2834 =
															MAKE_YOUNG_PAIR(BgL_arg2386z00_2833, BNIL);
														BgL_arg2385z00_2832 =
															BGl_formatz00zz__r4_output_6_10_3z00
															(BGl_string2681z00zzexpand_objectz00,
															BgL_list2387z00_2834);
													}
												}
												return
													BGl_errorz00zz__errorz00(BgL_widenz12z12_2815,
													BgL_arg2385z00_2832, BgL_xz00_38);
											}
									}
								}
							}
						else
							{	/* Expand/object.scm 557 */
							BgL_tagzd2773zd2_2819:
								return
									BGl_errorz00zz__errorz00(BGl_string2683z00zzexpand_objectz00,
									BGl_string2666z00zzexpand_objectz00, BgL_xz00_38);
							}
					}
				else
					{	/* Expand/object.scm 557 */
						goto BgL_tagzd2773zd2_2819;
					}
			}
		}

	}



/* &expand-widen! */
	obj_t BGl_z62expandzd2widenz12za2zzexpand_objectz00(obj_t BgL_envz00_4188,
		obj_t BgL_xz00_4189, obj_t BgL_ez00_4190)
	{
		{	/* Expand/object.scm 556 */
			return
				BGl_expandzd2widenz12zc0zzexpand_objectz00(BgL_xz00_4189,
				BgL_ez00_4190);
		}

	}



/* expand-widening */
	obj_t BGl_expandzd2wideningzd2zzexpand_objectz00(obj_t BgL_formz00_40,
		BgL_typez00_bglt BgL_classz00_41, obj_t BgL_oz00_42,
		obj_t BgL_providedz00_43, obj_t BgL_ez00_44)
	{
		{	/* Expand/object.scm 571 */
			{	/* Expand/object.scm 572 */
				obj_t BgL_superz00_2837;

				{
					BgL_tclassz00_bglt BgL_auxz00_6199;

					{
						obj_t BgL_auxz00_6200;

						{	/* Expand/object.scm 572 */
							BgL_objectz00_bglt BgL_tmpz00_6201;

							BgL_tmpz00_6201 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_41));
							BgL_auxz00_6200 = BGL_OBJECT_WIDENING(BgL_tmpz00_6201);
						}
						BgL_auxz00_6199 = ((BgL_tclassz00_bglt) BgL_auxz00_6200);
					}
					BgL_superz00_2837 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6199))->
						BgL_itszd2superzd2);
				}
				{	/* Expand/object.scm 572 */
					obj_t BgL_tidz00_2838;

					BgL_tidz00_2838 =
						(((BgL_typez00_bglt) COBJECT(BgL_classz00_41))->BgL_idz00);
					{	/* Expand/object.scm 573 */
						obj_t BgL_sidz00_2839;

						BgL_sidz00_2839 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_superz00_2837)))->BgL_idz00);
						{	/* Expand/object.scm 574 */
							obj_t BgL_tmpz00_2840;

							BgL_tmpz00_2840 =
								BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
								(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(32)));
							{	/* Expand/object.scm 575 */
								obj_t BgL_ttmpz00_2841;

								BgL_ttmpz00_2841 =
									BGl_makezd2typedzd2identz00zzast_identz00(BgL_tmpz00_2840,
									BgL_sidz00_2839);
								{	/* Expand/object.scm 576 */
									obj_t BgL_slotsz00_2842;

									{	/* Expand/object.scm 577 */
										obj_t BgL_hook1307z00_2855;

										BgL_hook1307z00_2855 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
										{	/* Expand/object.scm 578 */
											obj_t BgL_g1308z00_2856;

											{
												BgL_tclassz00_bglt BgL_auxz00_6215;

												{
													obj_t BgL_auxz00_6216;

													{	/* Expand/object.scm 578 */
														BgL_objectz00_bglt BgL_tmpz00_6217;

														BgL_tmpz00_6217 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_classz00_41));
														BgL_auxz00_6216 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_6217);
													}
													BgL_auxz00_6215 =
														((BgL_tclassz00_bglt) BgL_auxz00_6216);
												}
												BgL_g1308z00_2856 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6215))->
													BgL_slotsz00);
											}
											{
												obj_t BgL_l1304z00_2858;
												obj_t BgL_h1305z00_2859;

												BgL_l1304z00_2858 = BgL_g1308z00_2856;
												BgL_h1305z00_2859 = BgL_hook1307z00_2855;
											BgL_zc3z04anonymousza32403ze3z87_2860:
												if (NULLP(BgL_l1304z00_2858))
													{	/* Expand/object.scm 578 */
														BgL_slotsz00_2842 = CDR(BgL_hook1307z00_2855);
													}
												else
													{	/* Expand/object.scm 578 */
														if (
															((((BgL_slotz00_bglt) COBJECT(
																			((BgL_slotz00_bglt)
																				CAR(
																					((obj_t) BgL_l1304z00_2858)))))->
																	BgL_classzd2ownerzd2) ==
																((obj_t) BgL_classz00_41)))
															{	/* Expand/object.scm 578 */
																obj_t BgL_nh1306z00_2865;

																{	/* Expand/object.scm 578 */
																	obj_t BgL_arg2408z00_2867;

																	BgL_arg2408z00_2867 =
																		CAR(((obj_t) BgL_l1304z00_2858));
																	BgL_nh1306z00_2865 =
																		MAKE_YOUNG_PAIR(BgL_arg2408z00_2867, BNIL);
																}
																SET_CDR(BgL_h1305z00_2859, BgL_nh1306z00_2865);
																{	/* Expand/object.scm 578 */
																	obj_t BgL_arg2407z00_2866;

																	BgL_arg2407z00_2866 =
																		CDR(((obj_t) BgL_l1304z00_2858));
																	{
																		obj_t BgL_h1305z00_6240;
																		obj_t BgL_l1304z00_6239;

																		BgL_l1304z00_6239 = BgL_arg2407z00_2866;
																		BgL_h1305z00_6240 = BgL_nh1306z00_2865;
																		BgL_h1305z00_2859 = BgL_h1305z00_6240;
																		BgL_l1304z00_2858 = BgL_l1304z00_6239;
																		goto BgL_zc3z04anonymousza32403ze3z87_2860;
																	}
																}
															}
														else
															{	/* Expand/object.scm 578 */
																obj_t BgL_arg2410z00_2868;

																BgL_arg2410z00_2868 =
																	CDR(((obj_t) BgL_l1304z00_2858));
																{
																	obj_t BgL_l1304z00_6243;

																	BgL_l1304z00_6243 = BgL_arg2410z00_2868;
																	BgL_l1304z00_2858 = BgL_l1304z00_6243;
																	goto BgL_zc3z04anonymousza32403ze3z87_2860;
																}
															}
													}
											}
										}
									}
									{	/* Expand/object.scm 577 */

										{	/* Expand/object.scm 579 */
											obj_t BgL_arg2389z00_2843;

											{	/* Expand/object.scm 579 */
												obj_t BgL_arg2390z00_2844;
												obj_t BgL_arg2391z00_2845;

												{	/* Expand/object.scm 579 */
													obj_t BgL_arg2392z00_2846;

													{	/* Expand/object.scm 579 */
														obj_t BgL_arg2393z00_2847;

														BgL_arg2393z00_2847 =
															MAKE_YOUNG_PAIR(BgL_oz00_42, BNIL);
														BgL_arg2392z00_2846 =
															MAKE_YOUNG_PAIR(BgL_ttmpz00_2841,
															BgL_arg2393z00_2847);
													}
													BgL_arg2390z00_2844 =
														MAKE_YOUNG_PAIR(BgL_arg2392z00_2846, BNIL);
												}
												{	/* Expand/object.scm 580 */
													obj_t BgL_arg2395z00_2848;
													obj_t BgL_arg2396z00_2849;

													BgL_arg2395z00_2848 =
														BGl_classgenzd2widenzd2exprz00zzobject_classgenz00(
														((BgL_typez00_bglt) BgL_classz00_41),
														BgL_tmpz00_2840);
													{	/* Expand/object.scm 581 */
														obj_t BgL_arg2397z00_2850;

														{	/* Expand/object.scm 581 */
															obj_t BgL_arg2398z00_2851;
															obj_t BgL_arg2399z00_2852;
															obj_t BgL_arg2401z00_2853;

															BgL_arg2398z00_2851 =
																CAR(((obj_t) BgL_formz00_40));
															{	/* Expand/object.scm 581 */
																obj_t BgL_pairz00_3904;

																BgL_pairz00_3904 =
																	CDR(((obj_t) BgL_formz00_40));
																BgL_arg2399z00_2852 = CDR(BgL_pairz00_3904);
															}
															{	/* Expand/object.scm 582 */
																obj_t BgL_list2402z00_2854;

																BgL_list2402z00_2854 =
																	MAKE_YOUNG_PAIR(BgL_tmpz00_2840, BNIL);
																BgL_arg2401z00_2853 =
																	BGl_makezd2privatezd2sexpz00zzast_privatez00
																	(CNST_TABLE_REF(34), BgL_tidz00_2838,
																	BgL_list2402z00_2854);
															}
															BgL_arg2397z00_2850 =
																BGl_instantiatezd2fillzd2zzexpand_objectz00
																(BgL_arg2398z00_2851, BgL_arg2399z00_2852,
																((obj_t) BgL_classz00_41), BgL_slotsz00_2842,
																BgL_arg2401z00_2853, BgL_formz00_40,
																BgL_ez00_44);
														}
														BgL_arg2396z00_2849 =
															MAKE_YOUNG_PAIR(BgL_arg2397z00_2850, BNIL);
													}
													BgL_arg2391z00_2845 =
														MAKE_YOUNG_PAIR(BgL_arg2395z00_2848,
														BgL_arg2396z00_2849);
												}
												BgL_arg2389z00_2843 =
													MAKE_YOUNG_PAIR(BgL_arg2390z00_2844,
													BgL_arg2391z00_2845);
											}
											return
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg2389z00_2843);
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* expand-shrink! */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2shrinkz12zc0zzexpand_objectz00(obj_t
		BgL_xz00_45, obj_t BgL_ez00_46)
	{
		{	/* Expand/object.scm 587 */
			{
				obj_t BgL_symz00_2873;
				obj_t BgL_oz00_2874;

				if (PAIRP(BgL_xz00_45))
					{	/* Expand/object.scm 588 */
						obj_t BgL_carzd2797zd2_2879;
						obj_t BgL_cdrzd2798zd2_2880;

						BgL_carzd2797zd2_2879 = CAR(((obj_t) BgL_xz00_45));
						BgL_cdrzd2798zd2_2880 = CDR(((obj_t) BgL_xz00_45));
						if (SYMBOLP(BgL_carzd2797zd2_2879))
							{	/* Expand/object.scm 588 */
								if (PAIRP(BgL_cdrzd2798zd2_2880))
									{	/* Expand/object.scm 588 */
										if (NULLP(CDR(BgL_cdrzd2798zd2_2880)))
											{	/* Expand/object.scm 588 */
												BgL_symz00_2873 = BgL_carzd2797zd2_2879;
												BgL_oz00_2874 = CAR(BgL_cdrzd2798zd2_2880);
												{	/* Expand/object.scm 590 */
													obj_t BgL_sz00_2887;

													BgL_sz00_2887 =
														BGl_parsezd2idzd2zzast_identz00(BgL_symz00_2873,
														BGl_findzd2locationzd2zztools_locationz00
														(BgL_xz00_45));
													if ((CAR(BgL_sz00_2887) == CNST_TABLE_REF(35)))
														{	/* Expand/object.scm 591 */
															return
																BGl_replacez12z12zztools_miscz00(BgL_xz00_45,
																BGl_makezd2azd2shrinkz12z12zzexpand_objectz00
																(BgL_ez00_46, BgL_oz00_2874));
														}
													else
														{	/* Expand/object.scm 591 */
															return
																BGl_errorz00zz__errorz00(BgL_symz00_2873,
																BGl_string2684z00zzexpand_objectz00,
																BgL_xz00_45);
														}
												}
											}
										else
											{	/* Expand/object.scm 588 */
											BgL_tagzd2791zd2_2876:
												return
													BGl_errorz00zz__errorz00
													(BGl_string2685z00zzexpand_objectz00,
													BGl_string2666z00zzexpand_objectz00, BgL_xz00_45);
											}
									}
								else
									{	/* Expand/object.scm 588 */
										goto BgL_tagzd2791zd2_2876;
									}
							}
						else
							{	/* Expand/object.scm 588 */
								goto BgL_tagzd2791zd2_2876;
							}
					}
				else
					{	/* Expand/object.scm 588 */
						goto BgL_tagzd2791zd2_2876;
					}
			}
		}

	}



/* &expand-shrink! */
	obj_t BGl_z62expandzd2shrinkz12za2zzexpand_objectz00(obj_t BgL_envz00_4191,
		obj_t BgL_xz00_4192, obj_t BgL_ez00_4193)
	{
		{	/* Expand/object.scm 587 */
			return
				BGl_expandzd2shrinkz12zc0zzexpand_objectz00(BgL_xz00_4192,
				BgL_ez00_4193);
		}

	}



/* make-a-shrink! */
	obj_t BGl_makezd2azd2shrinkz12z12zzexpand_objectz00(obj_t BgL_ez00_47,
		obj_t BgL_oz00_48)
	{
		{	/* Expand/object.scm 600 */
			{	/* Expand/object.scm 601 */
				obj_t BgL_newoz00_2893;

				BgL_newoz00_2893 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(36)));
				if (CBOOL(BGl_za2unsafezd2typeza2zd2zzengine_paramz00))
					{	/* Expand/object.scm 603 */
						obj_t BgL_arg2426z00_2894;

						{	/* Expand/object.scm 603 */
							obj_t BgL_arg2428z00_2895;
							obj_t BgL_arg2429z00_2896;

							{	/* Expand/object.scm 603 */
								obj_t BgL_arg2430z00_2897;

								{	/* Expand/object.scm 603 */
									obj_t BgL_arg2431z00_2898;

									{	/* Expand/object.scm 603 */
										obj_t BgL_arg2432z00_2899;

										BgL_arg2432z00_2899 =
											BGL_PROCEDURE_CALL2(BgL_ez00_47, BgL_oz00_48,
											BgL_ez00_47);
										BgL_arg2431z00_2898 =
											MAKE_YOUNG_PAIR(BgL_arg2432z00_2899, BNIL);
									}
									BgL_arg2430z00_2897 =
										MAKE_YOUNG_PAIR(BgL_newoz00_2893, BgL_arg2431z00_2898);
								}
								BgL_arg2428z00_2895 =
									MAKE_YOUNG_PAIR(BgL_arg2430z00_2897, BNIL);
							}
							{	/* Expand/object.scm 604 */
								obj_t BgL_arg2434z00_2900;
								obj_t BgL_arg2435z00_2901;

								{	/* Expand/object.scm 604 */
									obj_t BgL_arg2437z00_2902;
									obj_t BgL_arg2438z00_2903;

									{	/* Expand/object.scm 604 */
										obj_t BgL_arg2439z00_2904;

										{	/* Expand/object.scm 604 */
											obj_t BgL_arg2442z00_2905;

											BgL_arg2442z00_2905 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
											BgL_arg2439z00_2904 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(37),
												BgL_arg2442z00_2905);
										}
										BgL_arg2437z00_2902 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg2439z00_2904);
									}
									{	/* Expand/object.scm 606 */
										obj_t BgL_arg2443z00_2906;

										{	/* Expand/object.scm 606 */
											obj_t BgL_arg2444z00_2907;

											{	/* Expand/object.scm 606 */
												obj_t BgL_arg2445z00_2908;
												obj_t BgL_arg2446z00_2909;

												{	/* Expand/object.scm 606 */
													obj_t BgL_arg2447z00_2910;

													{	/* Expand/object.scm 606 */
														obj_t BgL_arg2449z00_2911;

														BgL_arg2449z00_2911 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
														BgL_arg2447z00_2910 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(38),
															BgL_arg2449z00_2911);
													}
													BgL_arg2445z00_2908 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
														BgL_arg2447z00_2910);
												}
												{	/* Expand/object.scm 607 */
													obj_t BgL_arg2450z00_2912;

													{	/* Expand/object.scm 607 */
														obj_t BgL_arg2451z00_2913;
														obj_t BgL_arg2452z00_2914;

														{	/* Expand/object.scm 607 */
															obj_t BgL_arg2453z00_2915;

															{	/* Expand/object.scm 607 */
																obj_t BgL_arg2455z00_2916;

																BgL_arg2455z00_2916 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
																BgL_arg2453z00_2915 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(39),
																	BgL_arg2455z00_2916);
															}
															BgL_arg2451z00_2913 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																BgL_arg2453z00_2915);
														}
														{	/* Expand/object.scm 608 */
															obj_t BgL_arg2456z00_2917;

															{	/* Expand/object.scm 608 */
																obj_t BgL_arg2457z00_2918;
																obj_t BgL_arg2458z00_2919;

																{	/* Expand/object.scm 608 */
																	obj_t BgL_arg2459z00_2920;

																	{	/* Expand/object.scm 608 */
																		obj_t BgL_arg2460z00_2921;

																		BgL_arg2460z00_2921 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
																		BgL_arg2459z00_2920 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(40),
																			BgL_arg2460z00_2921);
																	}
																	BgL_arg2457z00_2918 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																		BgL_arg2459z00_2920);
																}
																BgL_arg2458z00_2919 =
																	MAKE_YOUNG_PAIR(BgL_newoz00_2893, BNIL);
																BgL_arg2456z00_2917 =
																	MAKE_YOUNG_PAIR(BgL_arg2457z00_2918,
																	BgL_arg2458z00_2919);
															}
															BgL_arg2452z00_2914 =
																MAKE_YOUNG_PAIR(BgL_arg2456z00_2917, BNIL);
														}
														BgL_arg2450z00_2912 =
															MAKE_YOUNG_PAIR(BgL_arg2451z00_2913,
															BgL_arg2452z00_2914);
													}
													BgL_arg2446z00_2909 =
														MAKE_YOUNG_PAIR(BgL_arg2450z00_2912, BNIL);
												}
												BgL_arg2444z00_2907 =
													MAKE_YOUNG_PAIR(BgL_arg2445z00_2908,
													BgL_arg2446z00_2909);
											}
											BgL_arg2443z00_2906 =
												MAKE_YOUNG_PAIR(BgL_arg2444z00_2907, BNIL);
										}
										BgL_arg2438z00_2903 =
											MAKE_YOUNG_PAIR(BgL_newoz00_2893, BgL_arg2443z00_2906);
									}
									BgL_arg2434z00_2900 =
										MAKE_YOUNG_PAIR(BgL_arg2437z00_2902, BgL_arg2438z00_2903);
								}
								{	/* Expand/object.scm 609 */
									obj_t BgL_arg2461z00_2922;
									obj_t BgL_arg2462z00_2923;

									{	/* Expand/object.scm 609 */
										obj_t BgL_arg2463z00_2924;

										{	/* Expand/object.scm 609 */
											obj_t BgL_arg2465z00_2925;

											BgL_arg2465z00_2925 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											BgL_arg2463z00_2924 =
												MAKE_YOUNG_PAIR(BgL_newoz00_2893, BgL_arg2465z00_2925);
										}
										BgL_arg2461z00_2922 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(41), BgL_arg2463z00_2924);
									}
									BgL_arg2462z00_2923 = MAKE_YOUNG_PAIR(BgL_newoz00_2893, BNIL);
									BgL_arg2435z00_2901 =
										MAKE_YOUNG_PAIR(BgL_arg2461z00_2922, BgL_arg2462z00_2923);
								}
								BgL_arg2429z00_2896 =
									MAKE_YOUNG_PAIR(BgL_arg2434z00_2900, BgL_arg2435z00_2901);
							}
							BgL_arg2426z00_2894 =
								MAKE_YOUNG_PAIR(BgL_arg2428z00_2895, BgL_arg2429z00_2896);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg2426z00_2894);
					}
				else
					{	/* Expand/object.scm 611 */
						obj_t BgL_arg2466z00_2926;

						{	/* Expand/object.scm 611 */
							obj_t BgL_arg2469z00_2927;
							obj_t BgL_arg2470z00_2928;

							{	/* Expand/object.scm 611 */
								obj_t BgL_arg2471z00_2929;

								{	/* Expand/object.scm 611 */
									obj_t BgL_arg2473z00_2930;

									{	/* Expand/object.scm 611 */
										obj_t BgL_arg2474z00_2931;

										BgL_arg2474z00_2931 =
											BGL_PROCEDURE_CALL2(BgL_ez00_47, BgL_oz00_48,
											BgL_ez00_47);
										BgL_arg2473z00_2930 =
											MAKE_YOUNG_PAIR(BgL_arg2474z00_2931, BNIL);
									}
									BgL_arg2471z00_2929 =
										MAKE_YOUNG_PAIR(BgL_newoz00_2893, BgL_arg2473z00_2930);
								}
								BgL_arg2469z00_2927 =
									MAKE_YOUNG_PAIR(BgL_arg2471z00_2929, BNIL);
							}
							{	/* Expand/object.scm 612 */
								obj_t BgL_arg2475z00_2932;

								{	/* Expand/object.scm 612 */
									obj_t BgL_arg2476z00_2933;

									{	/* Expand/object.scm 612 */
										obj_t BgL_arg2479z00_2934;
										obj_t BgL_arg2480z00_2935;

										{	/* Expand/object.scm 612 */
											obj_t BgL_arg2481z00_2936;

											BgL_arg2481z00_2936 =
												MAKE_YOUNG_PAIR(BgL_newoz00_2893, BNIL);
											BgL_arg2479z00_2934 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(42),
												BgL_arg2481z00_2936);
										}
										{	/* Expand/object.scm 613 */
											obj_t BgL_arg2482z00_2937;
											obj_t BgL_arg2483z00_2938;

											{	/* Expand/object.scm 613 */
												obj_t BgL_arg2484z00_2939;

												{	/* Expand/object.scm 613 */
													obj_t BgL_arg2486z00_2940;
													obj_t BgL_arg2487z00_2941;

													{	/* Expand/object.scm 613 */
														obj_t BgL_arg2488z00_2942;

														BgL_arg2488z00_2942 =
															MAKE_YOUNG_PAIR(BgL_newoz00_2893, BNIL);
														BgL_arg2486z00_2940 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(43),
															BgL_arg2488z00_2942);
													}
													{	/* Expand/object.scm 615 */
														obj_t BgL_arg2490z00_2943;
														obj_t BgL_arg2491z00_2944;

														{	/* Expand/object.scm 615 */
															obj_t BgL_arg2492z00_2945;

															{	/* Expand/object.scm 615 */
																obj_t BgL_arg2493z00_2946;
																obj_t BgL_arg2495z00_2947;

																{	/* Expand/object.scm 615 */
																	obj_t BgL_arg2497z00_2948;
																	obj_t BgL_arg2500z00_2949;

																	{	/* Expand/object.scm 615 */
																		obj_t BgL_arg2501z00_2950;

																		{	/* Expand/object.scm 615 */
																			obj_t BgL_arg2502z00_2951;

																			BgL_arg2502z00_2951 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																				BNIL);
																			BgL_arg2501z00_2950 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(37),
																				BgL_arg2502z00_2951);
																		}
																		BgL_arg2497z00_2948 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																			BgL_arg2501z00_2950);
																	}
																	{	/* Expand/object.scm 617 */
																		obj_t BgL_arg2503z00_2952;

																		{	/* Expand/object.scm 617 */
																			obj_t BgL_arg2505z00_2953;

																			{	/* Expand/object.scm 617 */
																				obj_t BgL_arg2506z00_2954;
																				obj_t BgL_arg2508z00_2955;

																				{	/* Expand/object.scm 617 */
																					obj_t BgL_arg2509z00_2956;

																					{	/* Expand/object.scm 617 */
																						obj_t BgL_arg2510z00_2957;

																						BgL_arg2510z00_2957 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																							BNIL);
																						BgL_arg2509z00_2956 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(38), BgL_arg2510z00_2957);
																					}
																					BgL_arg2506z00_2954 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																						BgL_arg2509z00_2956);
																				}
																				{	/* Expand/object.scm 618 */
																					obj_t BgL_arg2511z00_2958;

																					{	/* Expand/object.scm 618 */
																						obj_t BgL_arg2512z00_2959;
																						obj_t BgL_arg2513z00_2960;

																						{	/* Expand/object.scm 618 */
																							obj_t BgL_arg2514z00_2961;

																							{	/* Expand/object.scm 618 */
																								obj_t BgL_arg2515z00_2962;

																								BgL_arg2515z00_2962 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(8), BNIL);
																								BgL_arg2514z00_2961 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(39), BgL_arg2515z00_2962);
																							}
																							BgL_arg2512z00_2959 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(10), BgL_arg2514z00_2961);
																						}
																						{	/* Expand/object.scm 619 */
																							obj_t BgL_arg2516z00_2963;

																							{	/* Expand/object.scm 619 */
																								obj_t BgL_arg2518z00_2964;
																								obj_t BgL_arg2519z00_2965;

																								{	/* Expand/object.scm 619 */
																									obj_t BgL_arg2521z00_2966;

																									{	/* Expand/object.scm 619 */
																										obj_t BgL_arg2524z00_2967;

																										BgL_arg2524z00_2967 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(8), BNIL);
																										BgL_arg2521z00_2966 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(40),
																											BgL_arg2524z00_2967);
																									}
																									BgL_arg2518z00_2964 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(10),
																										BgL_arg2521z00_2966);
																								}
																								BgL_arg2519z00_2965 =
																									MAKE_YOUNG_PAIR
																									(BgL_newoz00_2893, BNIL);
																								BgL_arg2516z00_2963 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2518z00_2964,
																									BgL_arg2519z00_2965);
																							}
																							BgL_arg2513z00_2960 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2516z00_2963, BNIL);
																						}
																						BgL_arg2511z00_2958 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2512z00_2959,
																							BgL_arg2513z00_2960);
																					}
																					BgL_arg2508z00_2955 =
																						MAKE_YOUNG_PAIR(BgL_arg2511z00_2958,
																						BNIL);
																				}
																				BgL_arg2505z00_2953 =
																					MAKE_YOUNG_PAIR(BgL_arg2506z00_2954,
																					BgL_arg2508z00_2955);
																			}
																			BgL_arg2503z00_2952 =
																				MAKE_YOUNG_PAIR(BgL_arg2505z00_2953,
																				BNIL);
																		}
																		BgL_arg2500z00_2949 =
																			MAKE_YOUNG_PAIR(BgL_newoz00_2893,
																			BgL_arg2503z00_2952);
																	}
																	BgL_arg2493z00_2946 =
																		MAKE_YOUNG_PAIR(BgL_arg2497z00_2948,
																		BgL_arg2500z00_2949);
																}
																{	/* Expand/object.scm 620 */
																	obj_t BgL_arg2525z00_2968;
																	obj_t BgL_arg2526z00_2969;

																	{	/* Expand/object.scm 620 */
																		obj_t BgL_arg2527z00_2970;

																		{	/* Expand/object.scm 620 */
																			obj_t BgL_arg2528z00_2971;

																			BgL_arg2528z00_2971 =
																				MAKE_YOUNG_PAIR(BFALSE, BNIL);
																			BgL_arg2527z00_2970 =
																				MAKE_YOUNG_PAIR(BgL_newoz00_2893,
																				BgL_arg2528z00_2971);
																		}
																		BgL_arg2525z00_2968 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(41),
																			BgL_arg2527z00_2970);
																	}
																	BgL_arg2526z00_2969 =
																		MAKE_YOUNG_PAIR(BgL_newoz00_2893, BNIL);
																	BgL_arg2495z00_2947 =
																		MAKE_YOUNG_PAIR(BgL_arg2525z00_2968,
																		BgL_arg2526z00_2969);
																}
																BgL_arg2492z00_2945 =
																	MAKE_YOUNG_PAIR(BgL_arg2493z00_2946,
																	BgL_arg2495z00_2947);
															}
															BgL_arg2490z00_2943 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
																BgL_arg2492z00_2945);
														}
														{	/* Expand/object.scm 622 */
															obj_t BgL_arg2529z00_2972;

															{	/* Expand/object.scm 622 */
																obj_t BgL_arg2534z00_2973;

																{	/* Expand/object.scm 622 */
																	obj_t BgL_arg2536z00_2974;

																	{	/* Expand/object.scm 622 */
																		obj_t BgL_arg2537z00_2975;

																		BgL_arg2537z00_2975 =
																			MAKE_YOUNG_PAIR(BgL_newoz00_2893, BNIL);
																		BgL_arg2536z00_2974 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2686z00zzexpand_objectz00,
																			BgL_arg2537z00_2975);
																	}
																	BgL_arg2534z00_2973 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2685z00zzexpand_objectz00,
																		BgL_arg2536z00_2974);
																}
																BgL_arg2529z00_2972 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(44),
																	BgL_arg2534z00_2973);
															}
															BgL_arg2491z00_2944 =
																MAKE_YOUNG_PAIR(BgL_arg2529z00_2972, BNIL);
														}
														BgL_arg2487z00_2941 =
															MAKE_YOUNG_PAIR(BgL_arg2490z00_2943,
															BgL_arg2491z00_2944);
													}
													BgL_arg2484z00_2939 =
														MAKE_YOUNG_PAIR(BgL_arg2486z00_2940,
														BgL_arg2487z00_2941);
												}
												BgL_arg2482z00_2937 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(45),
													BgL_arg2484z00_2939);
											}
											{	/* Expand/object.scm 623 */
												obj_t BgL_arg2538z00_2976;

												{	/* Expand/object.scm 623 */
													obj_t BgL_arg2539z00_2977;

													{	/* Expand/object.scm 623 */
														obj_t BgL_arg2540z00_2978;

														{	/* Expand/object.scm 623 */
															obj_t BgL_arg2542z00_2979;

															BgL_arg2542z00_2979 =
																MAKE_YOUNG_PAIR(BgL_newoz00_2893, BNIL);
															BgL_arg2540z00_2978 =
																MAKE_YOUNG_PAIR
																(BGl_string2686z00zzexpand_objectz00,
																BgL_arg2542z00_2979);
														}
														BgL_arg2539z00_2977 =
															MAKE_YOUNG_PAIR
															(BGl_string2685z00zzexpand_objectz00,
															BgL_arg2540z00_2978);
													}
													BgL_arg2538z00_2976 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(44),
														BgL_arg2539z00_2977);
												}
												BgL_arg2483z00_2938 =
													MAKE_YOUNG_PAIR(BgL_arg2538z00_2976, BNIL);
											}
											BgL_arg2480z00_2935 =
												MAKE_YOUNG_PAIR(BgL_arg2482z00_2937,
												BgL_arg2483z00_2938);
										}
										BgL_arg2476z00_2933 =
											MAKE_YOUNG_PAIR(BgL_arg2479z00_2934, BgL_arg2480z00_2935);
									}
									BgL_arg2475z00_2932 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(45), BgL_arg2476z00_2933);
								}
								BgL_arg2470z00_2928 =
									MAKE_YOUNG_PAIR(BgL_arg2475z00_2932, BNIL);
							}
							BgL_arg2466z00_2926 =
								MAKE_YOUNG_PAIR(BgL_arg2469z00_2927, BgL_arg2470z00_2928);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg2466z00_2926);
					}
			}
		}

	}



/* object-epairify */
	obj_t BGl_objectzd2epairifyzd2zzexpand_objectz00(obj_t BgL_objz00_49,
		obj_t BgL_epairz00_50)
	{
		{	/* Expand/object.scm 628 */
		BGl_objectzd2epairifyzd2zzexpand_objectz00:
			if (EPAIRP(BgL_objz00_49))
				{	/* Expand/object.scm 629 */
					return BgL_objz00_49;
				}
			else
				{	/* Expand/object.scm 629 */
					if (EPAIRP(BgL_epairz00_50))
						{	/* Expand/object.scm 631 */
							if (PAIRP(BgL_objz00_49))
								{	/* Expand/object.scm 633 */
									obj_t BgL_arg2548z00_2984;
									obj_t BgL_arg2549z00_2985;
									obj_t BgL_arg2551z00_2986;

									BgL_arg2548z00_2984 = CAR(BgL_objz00_49);
									BgL_arg2549z00_2985 = CDR(BgL_objz00_49);
									BgL_arg2551z00_2986 = CER(((obj_t) BgL_epairz00_50));
									{	/* Expand/object.scm 633 */
										obj_t BgL_res2658z00_3913;

										BgL_res2658z00_3913 =
											MAKE_YOUNG_EPAIR(BgL_arg2548z00_2984, BgL_arg2549z00_2985,
											BgL_arg2551z00_2986);
										return BgL_res2658z00_3913;
									}
								}
							else
								{	/* Expand/object.scm 634 */
									obj_t BgL_arg2552z00_2987;

									{	/* Expand/object.scm 634 */
										obj_t BgL_arg2553z00_2988;

										BgL_arg2553z00_2988 = MAKE_YOUNG_PAIR(BgL_objz00_49, BNIL);
										BgL_arg2552z00_2987 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg2553z00_2988);
									}
									{
										obj_t BgL_objz00_6439;

										BgL_objz00_6439 = BgL_arg2552z00_2987;
										BgL_objz00_49 = BgL_objz00_6439;
										goto BGl_objectzd2epairifyzd2zzexpand_objectz00;
									}
								}
						}
					else
						{	/* Expand/object.scm 631 */
							return BgL_objz00_49;
						}
				}
		}

	}



/* find-slot-offset */
	obj_t BGl_findzd2slotzd2offsetz00zzexpand_objectz00(obj_t BgL_slotsz00_53,
		obj_t BgL_namez00_54, obj_t BgL_formz00_55, obj_t BgL_sexpz00_56)
	{
		{	/* Expand/object.scm 646 */
			{	/* Expand/object.scm 647 */
				obj_t BgL_sz00_2989;

				{
					obj_t BgL_list1310z00_2994;

					BgL_list1310z00_2994 = BgL_slotsz00_53;
				BgL_zc3z04anonymousza32557ze3z87_2995:
					if (PAIRP(BgL_list1310z00_2994))
						{	/* Expand/object.scm 647 */
							if (
								((((BgL_slotz00_bglt) COBJECT(
												((BgL_slotz00_bglt)
													CAR(BgL_list1310z00_2994))))->BgL_idz00) ==
									BgL_namez00_54))
								{	/* Expand/object.scm 647 */
									BgL_sz00_2989 = CAR(BgL_list1310z00_2994);
								}
							else
								{
									obj_t BgL_list1310z00_6448;

									BgL_list1310z00_6448 = CDR(BgL_list1310z00_2994);
									BgL_list1310z00_2994 = BgL_list1310z00_6448;
									goto BgL_zc3z04anonymousza32557ze3z87_2995;
								}
						}
					else
						{	/* Expand/object.scm 647 */
							BgL_sz00_2989 = BFALSE;
						}
				}
				{	/* Expand/object.scm 648 */
					bool_t BgL_test3002z00_6450;

					{	/* Expand/object.scm 648 */
						obj_t BgL_classz00_3918;

						BgL_classz00_3918 = BGl_slotz00zzobject_slotsz00;
						if (BGL_OBJECTP(BgL_sz00_2989))
							{	/* Expand/object.scm 648 */
								BgL_objectz00_bglt BgL_arg1807z00_3920;

								BgL_arg1807z00_3920 = (BgL_objectz00_bglt) (BgL_sz00_2989);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Expand/object.scm 648 */
										long BgL_idxz00_3926;

										BgL_idxz00_3926 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3920);
										BgL_test3002z00_6450 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3926 + 1L)) == BgL_classz00_3918);
									}
								else
									{	/* Expand/object.scm 648 */
										bool_t BgL_res2659z00_3951;

										{	/* Expand/object.scm 648 */
											obj_t BgL_oclassz00_3934;

											{	/* Expand/object.scm 648 */
												obj_t BgL_arg1815z00_3942;
												long BgL_arg1816z00_3943;

												BgL_arg1815z00_3942 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Expand/object.scm 648 */
													long BgL_arg1817z00_3944;

													BgL_arg1817z00_3944 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3920);
													BgL_arg1816z00_3943 =
														(BgL_arg1817z00_3944 - OBJECT_TYPE);
												}
												BgL_oclassz00_3934 =
													VECTOR_REF(BgL_arg1815z00_3942, BgL_arg1816z00_3943);
											}
											{	/* Expand/object.scm 648 */
												bool_t BgL__ortest_1115z00_3935;

												BgL__ortest_1115z00_3935 =
													(BgL_classz00_3918 == BgL_oclassz00_3934);
												if (BgL__ortest_1115z00_3935)
													{	/* Expand/object.scm 648 */
														BgL_res2659z00_3951 = BgL__ortest_1115z00_3935;
													}
												else
													{	/* Expand/object.scm 648 */
														long BgL_odepthz00_3936;

														{	/* Expand/object.scm 648 */
															obj_t BgL_arg1804z00_3937;

															BgL_arg1804z00_3937 = (BgL_oclassz00_3934);
															BgL_odepthz00_3936 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3937);
														}
														if ((1L < BgL_odepthz00_3936))
															{	/* Expand/object.scm 648 */
																obj_t BgL_arg1802z00_3939;

																{	/* Expand/object.scm 648 */
																	obj_t BgL_arg1803z00_3940;

																	BgL_arg1803z00_3940 = (BgL_oclassz00_3934);
																	BgL_arg1802z00_3939 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3940,
																		1L);
																}
																BgL_res2659z00_3951 =
																	(BgL_arg1802z00_3939 == BgL_classz00_3918);
															}
														else
															{	/* Expand/object.scm 648 */
																BgL_res2659z00_3951 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3002z00_6450 = BgL_res2659z00_3951;
									}
							}
						else
							{	/* Expand/object.scm 648 */
								BgL_test3002z00_6450 = ((bool_t) 0);
							}
					}
					if (BgL_test3002z00_6450)
						{	/* Expand/object.scm 648 */
							return
								BINT(
								(((BgL_slotz00_bglt) COBJECT(
											((BgL_slotz00_bglt) BgL_sz00_2989)))->BgL_indexz00));
						}
					else
						{	/* Expand/object.scm 650 */
							obj_t BgL_arg2555z00_2991;

							{	/* Expand/object.scm 650 */
								obj_t BgL_list2556z00_2992;

								BgL_list2556z00_2992 = MAKE_YOUNG_PAIR(BgL_namez00_54, BNIL);
								BgL_arg2555z00_2991 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string2687z00zzexpand_objectz00, BgL_list2556z00_2992);
							}
							return
								BGl_errorz00zz__errorz00(BgL_formz00_55, BgL_arg2555z00_2991,
								BgL_sexpz00_56);
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_objectz00(void)
	{
		{	/* Expand/object.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_objectz00(void)
	{
		{	/* Expand/object.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_objectz00(void)
	{
		{	/* Expand/object.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_objectz00(void)
	{
		{	/* Expand/object.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzast_objectz00(520121776L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzread_inlinez00(500058826L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzobject_toolsz00(196511171L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzobject_classgenz00(502726840L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(223654864L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzmodule_prototypez00(499400840L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
			return
				BGl_modulezd2initializa7ationz75zzmodule_classz00(153808441L,
				BSTRING_TO_STRING(BGl_string2688z00zzexpand_objectz00));
		}

	}

#ifdef __cplusplus
}
#endif
