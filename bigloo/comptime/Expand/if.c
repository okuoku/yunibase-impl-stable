/*===========================================================================*/
/*   (Expand/if.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/if.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_IF_TYPE_DEFINITIONS
#define BGL_EXPAND_IF_TYPE_DEFINITIONS
#endif													// BGL_EXPAND_IF_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_expandzd2andzf2boolz20zzexpand_ifz00(obj_t);
	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_expandzd2ifzd2withz00zzexpand_ifz00(obj_t, obj_t, obj_t,
		bool_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static bool_t BGl_iszd2azd2validzd2constantzf3z21zzexpand_ifz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_ifz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzexpand_ifz00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2notzd2zzexpand_ifz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_ifz00(void);
	static obj_t BGl_objectzd2initzd2zzexpand_ifz00(void);
	static obj_t BGl_z62expandzd2notzb0zzexpand_ifz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2andzd2zzexpand_ifz00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2ifzb0zzexpand_ifz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_findzd2casezd2expz00zzexpand_ifz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_ifz00(void);
	static obj_t BGl_z62expandzd2andzb0zzexpand_ifz00(obj_t, obj_t, obj_t);
	static obj_t BGl_valzd2ze3constantze70zd6zzexpand_ifz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2ifzd2zzexpand_ifz00(obj_t, obj_t);
	extern obj_t BGl_lexicalzd2stackzd2zzexpand_epsz00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62expandzd2orzb0zzexpand_ifz00(obj_t, obj_t, obj_t);
	static obj_t BGl_expandzd2orzf2boolz20zzexpand_ifz00(obj_t);
	static obj_t BGl_z62expandzd2testzb0zzexpand_ifz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_replacez12z12zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzexpand_ifz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_iszd2casezf3z21zzexpand_ifz00(obj_t);
	static bool_t BGl_za2casezd2enabledzf3za2z21zzexpand_ifz00;
	BGL_EXPORTED_DECL obj_t BGl_expandzd2orzd2zzexpand_ifz00(obj_t, obj_t);
	static obj_t BGl_makezd2clausezd2zzexpand_ifz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_ifz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_ifz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_ifz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_ifz00(void);
	static obj_t BGl_newzd2eze70z35zzexpand_ifz00(obj_t, obj_t);
	extern obj_t BGl_epairifyzd2reczd2zztools_miscz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_loopze70ze7zzexpand_ifz00(obj_t, obj_t);
	static obj_t BGl_loopze71ze7zzexpand_ifz00(obj_t, obj_t);
	static obj_t BGl_loopze72ze7zzexpand_ifz00(obj_t, obj_t);
	static obj_t BGl_loopze73ze7zzexpand_ifz00(obj_t, obj_t);
	static obj_t BGl_ifzd2ze3casez12z23zzexpand_ifz00(obj_t);
	BGL_IMPORT obj_t BGl_za2nilza2z00zz__evalz00;
	static obj_t __cnst[16];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2andzd2envz00zzexpand_ifz00,
		BgL_bgl_za762expandza7d2andza71893za7, BGl_z62expandzd2andzb0zzexpand_ifz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2orzd2envz00zzexpand_ifz00,
		BgL_bgl_za762expandza7d2orza7b1894za7, BGl_z62expandzd2orzb0zzexpand_ifz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1883z00zzexpand_ifz00,
		BgL_bgl_string1883za700za7za7e1895za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string1884z00zzexpand_ifz00,
		BgL_bgl_string1884za700za7za7e1896za7, "Illegal `if' form", 17);
	      DEFINE_STRING(BGl_string1885z00zzexpand_ifz00,
		BgL_bgl_string1885za700za7za7e1897za7, "_", 1);
	      DEFINE_STRING(BGl_string1886z00zzexpand_ifz00,
		BgL_bgl_string1886za700za7za7e1898za7, "or", 2);
	      DEFINE_STRING(BGl_string1887z00zzexpand_ifz00,
		BgL_bgl_string1887za700za7za7e1899za7, "test_", 5);
	      DEFINE_STRING(BGl_string1888z00zzexpand_ifz00,
		BgL_bgl_string1888za700za7za7e1900za7, "and", 3);
	      DEFINE_STRING(BGl_string1889z00zzexpand_ifz00,
		BgL_bgl_string1889za700za7za7e1901za7, "Illegal `not' form", 18);
	      DEFINE_STRING(BGl_string1890z00zzexpand_ifz00,
		BgL_bgl_string1890za700za7za7e1902za7, "expand_if", 9);
	      DEFINE_STRING(BGl_string1891z00zzexpand_ifz00,
		BgL_bgl_string1891za700za7za7e1903za7,
		"non-user else quote memq eqv? char=? =fx eq? c-eq? case and or let null? test if ",
		81);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_expandzd2testzd2envz00zzexpand_ifz00,
		BgL_bgl_za762expandza7d2test1904z00, BGl_z62expandzd2testzb0zzexpand_ifz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2ifzd2envz00zzexpand_ifz00,
		BgL_bgl_za762expandza7d2ifza7b1905za7, BGl_z62expandzd2ifzb0zzexpand_ifz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2notzd2envz00zzexpand_ifz00,
		BgL_bgl_za762expandza7d2notza71906za7, BGl_z62expandzd2notzb0zzexpand_ifz00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzexpand_ifz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzexpand_ifz00(long
		BgL_checksumz00_858, char *BgL_fromz00_859)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_ifz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_ifz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_ifz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_ifz00();
					BGl_cnstzd2initzd2zzexpand_ifz00();
					BGl_importedzd2moduleszd2initz00zzexpand_ifz00();
					return BGl_toplevelzd2initzd2zzexpand_ifz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_ifz00(void)
	{
		{	/* Expand/if.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "expand_if");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_if");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_if");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "expand_if");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_if");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "expand_if");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "expand_if");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "expand_if");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "expand_if");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"expand_if");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_if");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_ifz00(void)
	{
		{	/* Expand/if.scm 15 */
			{	/* Expand/if.scm 15 */
				obj_t BgL_cportz00_827;

				{	/* Expand/if.scm 15 */
					obj_t BgL_stringz00_834;

					BgL_stringz00_834 = BGl_string1891z00zzexpand_ifz00;
					{	/* Expand/if.scm 15 */
						obj_t BgL_startz00_835;

						BgL_startz00_835 = BINT(0L);
						{	/* Expand/if.scm 15 */
							obj_t BgL_endz00_836;

							BgL_endz00_836 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_834)));
							{	/* Expand/if.scm 15 */

								BgL_cportz00_827 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_834, BgL_startz00_835, BgL_endz00_836);
				}}}}
				{
					long BgL_iz00_828;

					BgL_iz00_828 = 15L;
				BgL_loopz00_829:
					if ((BgL_iz00_828 == -1L))
						{	/* Expand/if.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/if.scm 15 */
							{	/* Expand/if.scm 15 */
								obj_t BgL_arg1892z00_830;

								{	/* Expand/if.scm 15 */

									{	/* Expand/if.scm 15 */
										obj_t BgL_locationz00_832;

										BgL_locationz00_832 = BBOOL(((bool_t) 0));
										{	/* Expand/if.scm 15 */

											BgL_arg1892z00_830 =
												BGl_readz00zz__readerz00(BgL_cportz00_827,
												BgL_locationz00_832);
										}
									}
								}
								{	/* Expand/if.scm 15 */
									int BgL_tmpz00_888;

									BgL_tmpz00_888 = (int) (BgL_iz00_828);
									CNST_TABLE_SET(BgL_tmpz00_888, BgL_arg1892z00_830);
							}}
							{	/* Expand/if.scm 15 */
								int BgL_auxz00_833;

								BgL_auxz00_833 = (int) ((BgL_iz00_828 - 1L));
								{
									long BgL_iz00_893;

									BgL_iz00_893 = (long) (BgL_auxz00_833);
									BgL_iz00_828 = BgL_iz00_893;
									goto BgL_loopz00_829;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_ifz00(void)
	{
		{	/* Expand/if.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzexpand_ifz00(void)
	{
		{	/* Expand/if.scm 15 */
			return (BGl_za2casezd2enabledzf3za2z21zzexpand_ifz00 =
				((bool_t) 1), BUNSPEC);
		}

	}



/* expand-or/bool */
	obj_t BGl_expandzd2orzf2boolz20zzexpand_ifz00(obj_t BgL_expz00_3)
	{
		{	/* Expand/if.scm 28 */
			{	/* Expand/if.scm 29 */
				obj_t BgL_resz00_35;

				{	/* Expand/if.scm 29 */
					obj_t BgL_g1016z00_36;

					BgL_g1016z00_36 = CDR(((obj_t) BgL_expz00_3));
					BgL_resz00_35 =
						BGl_loopze73ze7zzexpand_ifz00(BgL_expz00_3, BgL_g1016z00_36);
				}
				return BGl_replacez12z12zztools_miscz00(BgL_expz00_3, BgL_resz00_35);
			}
		}

	}



/* loop~3 */
	obj_t BGl_loopze73ze7zzexpand_ifz00(obj_t BgL_expz00_826, obj_t BgL_sorz00_38)
	{
		{	/* Expand/if.scm 29 */
			if (NULLP(BgL_sorz00_38))
				{	/* Expand/if.scm 31 */
					return BFALSE;
				}
			else
				{	/* Expand/if.scm 31 */
					if (PAIRP(BgL_sorz00_38))
						{	/* Expand/if.scm 33 */
							if (NULLP(CDR(BgL_sorz00_38)))
								{	/* Expand/if.scm 35 */
									return CAR(BgL_sorz00_38);
								}
							else
								{	/* Expand/if.scm 38 */
									obj_t BgL_resz00_44;

									{	/* Expand/if.scm 38 */
										obj_t BgL_arg1049z00_52;

										{	/* Expand/if.scm 38 */
											obj_t BgL_arg1050z00_53;
											obj_t BgL_arg1051z00_54;

											BgL_arg1050z00_53 = CAR(BgL_sorz00_38);
											{	/* Expand/if.scm 38 */
												obj_t BgL_arg1052z00_55;

												BgL_arg1052z00_55 =
													MAKE_YOUNG_PAIR(BGl_loopze73ze7zzexpand_ifz00
													(BgL_expz00_826, CDR(BgL_sorz00_38)), BNIL);
												BgL_arg1051z00_54 =
													MAKE_YOUNG_PAIR(BTRUE, BgL_arg1052z00_55);
											}
											BgL_arg1049z00_52 =
												MAKE_YOUNG_PAIR(BgL_arg1050z00_53, BgL_arg1051z00_54);
										}
										BgL_resz00_44 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1049z00_52);
									}
									{	/* Expand/if.scm 40 */
										bool_t BgL_test1912z00_916;

										{	/* Expand/if.scm 40 */
											obj_t BgL_tmpz00_917;

											BgL_tmpz00_917 = CAR(BgL_sorz00_38);
											BgL_test1912z00_916 = EPAIRP(BgL_tmpz00_917);
										}
										if (BgL_test1912z00_916)
											{	/* Expand/if.scm 41 */
												obj_t BgL_arg1044z00_47;
												obj_t BgL_arg1045z00_48;
												obj_t BgL_arg1046z00_49;

												BgL_arg1044z00_47 = CAR(BgL_resz00_44);
												BgL_arg1045z00_48 = CDR(BgL_resz00_44);
												BgL_arg1046z00_49 = CER(CAR(BgL_sorz00_38));
												{	/* Expand/if.scm 41 */
													obj_t BgL_res1874z00_599;

													BgL_res1874z00_599 =
														MAKE_YOUNG_EPAIR(BgL_arg1044z00_47,
														BgL_arg1045z00_48, BgL_arg1046z00_49);
													return BgL_res1874z00_599;
												}
											}
										else
											{	/* Expand/if.scm 40 */
												return BgL_resz00_44;
											}
									}
								}
						}
					else
						{	/* Expand/if.scm 33 */
							return
								BGl_errorz00zz__errorz00(BFALSE,
								BGl_string1883z00zzexpand_ifz00, BgL_expz00_826);
						}
				}
		}

	}



/* expand-and/bool */
	obj_t BGl_expandzd2andzf2boolz20zzexpand_ifz00(obj_t BgL_expz00_4)
	{
		{	/* Expand/if.scm 49 */
			{	/* Expand/if.scm 50 */
				obj_t BgL_resz00_60;

				{	/* Expand/if.scm 50 */
					obj_t BgL_g1017z00_61;

					BgL_g1017z00_61 = CDR(((obj_t) BgL_expz00_4));
					BgL_resz00_60 =
						BGl_loopze72ze7zzexpand_ifz00(BgL_expz00_4, BgL_g1017z00_61);
				}
				return BGl_replacez12z12zztools_miscz00(BgL_expz00_4, BgL_resz00_60);
			}
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zzexpand_ifz00(obj_t BgL_expz00_825,
		obj_t BgL_sandz00_63)
	{
		{	/* Expand/if.scm 50 */
			if (NULLP(BgL_sandz00_63))
				{	/* Expand/if.scm 52 */
					return BTRUE;
				}
			else
				{	/* Expand/if.scm 52 */
					if (PAIRP(BgL_sandz00_63))
						{	/* Expand/if.scm 54 */
							if (NULLP(CDR(BgL_sandz00_63)))
								{	/* Expand/if.scm 56 */
									return CAR(BgL_sandz00_63);
								}
							else
								{	/* Expand/if.scm 59 */
									obj_t BgL_resz00_69;

									{	/* Expand/if.scm 59 */
										obj_t BgL_arg1076z00_77;

										{	/* Expand/if.scm 59 */
											obj_t BgL_arg1078z00_78;
											obj_t BgL_arg1079z00_79;

											BgL_arg1078z00_78 = CAR(BgL_sandz00_63);
											{	/* Expand/if.scm 59 */
												obj_t BgL_arg1080z00_80;
												obj_t BgL_arg1082z00_81;

												BgL_arg1080z00_80 =
													BGl_loopze72ze7zzexpand_ifz00(BgL_expz00_825,
													CDR(BgL_sandz00_63));
												BgL_arg1082z00_81 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
												BgL_arg1079z00_79 =
													MAKE_YOUNG_PAIR(BgL_arg1080z00_80, BgL_arg1082z00_81);
											}
											BgL_arg1076z00_77 =
												MAKE_YOUNG_PAIR(BgL_arg1078z00_78, BgL_arg1079z00_79);
										}
										BgL_resz00_69 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1076z00_77);
									}
									{	/* Expand/if.scm 61 */
										bool_t BgL_test1916z00_946;

										{	/* Expand/if.scm 61 */
											obj_t BgL_tmpz00_947;

											BgL_tmpz00_947 = CAR(BgL_sandz00_63);
											BgL_test1916z00_946 = EPAIRP(BgL_tmpz00_947);
										}
										if (BgL_test1916z00_946)
											{	/* Expand/if.scm 62 */
												obj_t BgL_arg1065z00_72;
												obj_t BgL_arg1066z00_73;
												obj_t BgL_arg1068z00_74;

												BgL_arg1065z00_72 = CAR(BgL_resz00_69);
												BgL_arg1066z00_73 = CDR(BgL_resz00_69);
												BgL_arg1068z00_74 = CER(CAR(BgL_sandz00_63));
												{	/* Expand/if.scm 62 */
													obj_t BgL_res1875z00_610;

													BgL_res1875z00_610 =
														MAKE_YOUNG_EPAIR(BgL_arg1065z00_72,
														BgL_arg1066z00_73, BgL_arg1068z00_74);
													return BgL_res1875z00_610;
												}
											}
										else
											{	/* Expand/if.scm 61 */
												return BgL_resz00_69;
											}
									}
								}
						}
					else
						{	/* Expand/if.scm 54 */
							return
								BGl_errorz00zz__errorz00(BFALSE,
								BGl_string1883z00zzexpand_ifz00, BgL_expz00_825);
						}
				}
		}

	}



/* new-e~0 */
	obj_t BGl_newzd2eze70z35zzexpand_ifz00(obj_t BgL_xz00_111, obj_t BgL_ez00_112)
	{
		{	/* Expand/if.scm 71 */
		BGl_newzd2eze70z35zzexpand_ifz00:
			{

				if (PAIRP(BgL_xz00_111))
					{	/* Expand/if.scm 71 */
						if ((CAR(((obj_t) BgL_xz00_111)) == CNST_TABLE_REF(4)))
							{	/* Expand/if.scm 71 */
								{
									obj_t BgL_xz00_963;

									BgL_xz00_963 =
										BGl_expandzd2orzf2boolz20zzexpand_ifz00(BgL_xz00_111);
									BgL_xz00_111 = BgL_xz00_963;
									goto BGl_newzd2eze70z35zzexpand_ifz00;
								}
							}
						else
							{	/* Expand/if.scm 71 */
								if ((CAR(((obj_t) BgL_xz00_111)) == CNST_TABLE_REF(5)))
									{	/* Expand/if.scm 71 */
										{
											obj_t BgL_xz00_970;

											BgL_xz00_970 =
												BGl_expandzd2andzf2boolz20zzexpand_ifz00(BgL_xz00_111);
											BgL_xz00_111 = BgL_xz00_970;
											goto BGl_newzd2eze70z35zzexpand_ifz00;
										}
									}
								else
									{	/* Expand/if.scm 71 */
									BgL_tagzd2361zd2_116:
										return
											BGL_PROCEDURE_CALL2(BgL_ez00_112, BgL_xz00_111,
											BgL_ez00_112);
									}
							}
					}
				else
					{	/* Expand/if.scm 71 */
						goto BgL_tagzd2361zd2_116;
					}
			}
		}

	}



/* &expand-test */
	obj_t BGl_z62expandzd2testzb0zzexpand_ifz00(obj_t BgL_envz00_808,
		obj_t BgL_xz00_809, obj_t BgL_ez00_810)
	{
		{	/* Expand/if.scm 70 */
			if (CBOOL(BGl_za2nilza2z00zz__evalz00))
				{	/* Expand/if.scm 80 */
					obj_t BgL_resz00_839;

					BgL_resz00_839 =
						BGl_newzd2eze70z35zzexpand_ifz00(BgL_xz00_809, BgL_ez00_810);
					{	/* Expand/if.scm 81 */
						bool_t BgL_test1921z00_980;

						if (PAIRP(BgL_xz00_809))
							{	/* Expand/if.scm 81 */
								BgL_test1921z00_980 = PAIRP(BgL_resz00_839);
							}
						else
							{	/* Expand/if.scm 81 */
								BgL_test1921z00_980 = ((bool_t) 0);
							}
						if (BgL_test1921z00_980)
							{	/* Expand/if.scm 81 */
								return
									BGl_replacez12z12zztools_miscz00(BgL_xz00_809,
									BgL_resz00_839);
							}
						else
							{	/* Expand/if.scm 81 */
								return BgL_resz00_839;
							}
					}
				}
			else
				{	/* Expand/if.scm 84 */
					obj_t BgL_auxz00_840;

					BgL_auxz00_840 =
						BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(1));
					{	/* Expand/if.scm 85 */
						obj_t BgL_arg1087z00_841;

						{	/* Expand/if.scm 85 */
							obj_t BgL_arg1088z00_842;
							obj_t BgL_arg1090z00_843;

							{	/* Expand/if.scm 85 */
								obj_t BgL_arg1092z00_844;

								{	/* Expand/if.scm 85 */
									obj_t BgL_arg1097z00_845;

									{	/* Expand/if.scm 85 */
										obj_t BgL_arg1102z00_846;

										{	/* Expand/if.scm 85 */
											obj_t BgL_resz00_847;

											BgL_resz00_847 =
												BGl_newzd2eze70z35zzexpand_ifz00(BgL_xz00_809,
												BgL_ez00_810);
											if (PAIRP(BgL_resz00_847))
												{	/* Expand/if.scm 89 */
													BgL_arg1102z00_846 =
														MAKE_YOUNG_PAIR(CAR(BgL_resz00_847),
														CDR(BgL_resz00_847));
												}
											else
												{	/* Expand/if.scm 89 */
													BgL_arg1102z00_846 = BgL_resz00_847;
												}
										}
										BgL_arg1097z00_845 =
											MAKE_YOUNG_PAIR(BgL_arg1102z00_846, BNIL);
									}
									BgL_arg1092z00_844 =
										MAKE_YOUNG_PAIR(BgL_auxz00_840, BgL_arg1097z00_845);
								}
								BgL_arg1088z00_842 = MAKE_YOUNG_PAIR(BgL_arg1092z00_844, BNIL);
							}
							{	/* Expand/if.scm 93 */
								obj_t BgL_arg1115z00_848;

								{	/* Expand/if.scm 93 */
									obj_t BgL_arg1122z00_849;

									{	/* Expand/if.scm 93 */
										obj_t BgL_arg1123z00_850;

										{	/* Expand/if.scm 93 */
											obj_t BgL_arg1125z00_851;
											obj_t BgL_arg1126z00_852;

											{	/* Expand/if.scm 93 */
												obj_t BgL_arg1127z00_853;

												{	/* Expand/if.scm 93 */
													obj_t BgL_arg1129z00_854;
													obj_t BgL_arg1131z00_855;

													{	/* Expand/if.scm 93 */
														obj_t BgL_arg1132z00_856;

														BgL_arg1132z00_856 =
															MAKE_YOUNG_PAIR(BgL_auxz00_840, BNIL);
														BgL_arg1129z00_854 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
															BgL_arg1132z00_856);
													}
													{	/* Expand/if.scm 93 */
														obj_t BgL_arg1137z00_857;

														BgL_arg1137z00_857 = MAKE_YOUNG_PAIR(BTRUE, BNIL);
														BgL_arg1131z00_855 =
															MAKE_YOUNG_PAIR(BFALSE, BgL_arg1137z00_857);
													}
													BgL_arg1127z00_853 =
														MAKE_YOUNG_PAIR(BgL_arg1129z00_854,
														BgL_arg1131z00_855);
												}
												BgL_arg1125z00_851 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
													BgL_arg1127z00_853);
											}
											BgL_arg1126z00_852 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											BgL_arg1123z00_850 =
												MAKE_YOUNG_PAIR(BgL_arg1125z00_851, BgL_arg1126z00_852);
										}
										BgL_arg1122z00_849 =
											MAKE_YOUNG_PAIR(BgL_auxz00_840, BgL_arg1123z00_850);
									}
									BgL_arg1115z00_848 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1122z00_849);
								}
								BgL_arg1090z00_843 = MAKE_YOUNG_PAIR(BgL_arg1115z00_848, BNIL);
							}
							BgL_arg1087z00_841 =
								MAKE_YOUNG_PAIR(BgL_arg1088z00_842, BgL_arg1090z00_843);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1087z00_841);
					}
				}
		}

	}



/* expand-if */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2ifzd2zzexpand_ifz00(obj_t BgL_xz00_7,
		obj_t BgL_ez00_8)
	{
		{	/* Expand/if.scm 104 */
			{
				obj_t BgL_testz00_138;
				obj_t BgL_alorsz00_139;
				obj_t BgL_testz00_134;
				obj_t BgL_alorsz00_135;
				obj_t BgL_sinonz00_136;

				if (PAIRP(BgL_xz00_7))
					{	/* Expand/if.scm 105 */
						obj_t BgL_cdrzd2379zd2_144;

						BgL_cdrzd2379zd2_144 = CDR(((obj_t) BgL_xz00_7));
						if (PAIRP(BgL_cdrzd2379zd2_144))
							{	/* Expand/if.scm 105 */
								obj_t BgL_cdrzd2383zd2_146;

								BgL_cdrzd2383zd2_146 = CDR(BgL_cdrzd2379zd2_144);
								if ((CAR(BgL_cdrzd2379zd2_144) == BTRUE))
									{	/* Expand/if.scm 105 */
										if (PAIRP(BgL_cdrzd2383zd2_146))
											{	/* Expand/if.scm 105 */
												obj_t BgL_cdrzd2387zd2_150;

												BgL_cdrzd2387zd2_150 = CDR(BgL_cdrzd2383zd2_146);
												if (PAIRP(BgL_cdrzd2387zd2_150))
													{	/* Expand/if.scm 105 */
														if (NULLP(CDR(BgL_cdrzd2387zd2_150)))
															{	/* Expand/if.scm 105 */
																obj_t BgL_arg1164z00_154;

																BgL_arg1164z00_154 = CAR(BgL_cdrzd2383zd2_146);
																{	/* Expand/if.scm 107 */
																	obj_t BgL_arg1225z00_632;

																	BgL_arg1225z00_632 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_8,
																		BgL_arg1164z00_154, BgL_ez00_8);
																	return
																		BGl_replacez12z12zztools_miscz00(BgL_xz00_7,
																		BgL_arg1225z00_632);
																}
															}
														else
															{	/* Expand/if.scm 105 */
															BgL_tagzd2371zd2_141:
																return
																	BGl_errorz00zz__errorz00(BFALSE,
																	BGl_string1884z00zzexpand_ifz00, BgL_xz00_7);
															}
													}
												else
													{	/* Expand/if.scm 105 */
														obj_t BgL_cdrzd2434zd2_158;

														BgL_cdrzd2434zd2_158 =
															CDR(((obj_t) BgL_cdrzd2379zd2_144));
														if (NULLP(CDR(((obj_t) BgL_cdrzd2434zd2_158))))
															{	/* Expand/if.scm 105 */
																obj_t BgL_arg1182z00_161;
																obj_t BgL_arg1183z00_162;

																BgL_arg1182z00_161 =
																	CAR(((obj_t) BgL_cdrzd2379zd2_144));
																BgL_arg1183z00_162 =
																	CAR(((obj_t) BgL_cdrzd2434zd2_158));
																BgL_testz00_138 = BgL_arg1182z00_161;
																BgL_alorsz00_139 = BgL_arg1183z00_162;
															BgL_tagzd2370zd2_140:
																{	/* Expand/if.scm 133 */
																	obj_t BgL_resz00_236;

																	{	/* Expand/if.scm 133 */
																		obj_t BgL_arg1306z00_237;

																		{	/* Expand/if.scm 133 */
																			obj_t BgL_arg1307z00_238;
																			obj_t BgL_arg1308z00_239;

																			BgL_arg1307z00_238 =
																				BGl_expandzd2ifzd2withz00zzexpand_ifz00
																				(BGl_expandzd2testzd2envz00zzexpand_ifz00,
																				BgL_testz00_138, BgL_ez00_8,
																				((bool_t) 1));
																			{	/* Expand/if.scm 134 */
																				obj_t BgL_arg1310z00_240;
																				obj_t BgL_arg1311z00_241;

																				BgL_arg1310z00_240 =
																					BGl_expandzd2ifzd2withz00zzexpand_ifz00
																					(BgL_ez00_8, BgL_alorsz00_139,
																					BgL_ez00_8, ((bool_t) 1));
																				BgL_arg1311z00_241 =
																					MAKE_YOUNG_PAIR(BFALSE, BNIL);
																				BgL_arg1308z00_239 =
																					MAKE_YOUNG_PAIR(BgL_arg1310z00_240,
																					BgL_arg1311z00_241);
																			}
																			BgL_arg1306z00_237 =
																				MAKE_YOUNG_PAIR(BgL_arg1307z00_238,
																				BgL_arg1308z00_239);
																		}
																		BgL_resz00_236 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																			BgL_arg1306z00_237);
																	}
																	return
																		BGl_epairifyzd2reczd2zztools_miscz00
																		(BgL_resz00_236, BgL_xz00_7);
																}
															}
														else
															{	/* Expand/if.scm 105 */
																goto BgL_tagzd2371zd2_141;
															}
													}
											}
										else
											{	/* Expand/if.scm 105 */
												goto BgL_tagzd2371zd2_141;
											}
									}
								else
									{	/* Expand/if.scm 105 */
										obj_t BgL_cdrzd2467zd2_165;

										BgL_cdrzd2467zd2_165 = CDR(((obj_t) BgL_cdrzd2379zd2_144));
										if ((CAR(((obj_t) BgL_cdrzd2379zd2_144)) == BFALSE))
											{	/* Expand/if.scm 105 */
												if (PAIRP(BgL_cdrzd2467zd2_165))
													{	/* Expand/if.scm 105 */
														obj_t BgL_cdrzd2471zd2_169;

														BgL_cdrzd2471zd2_169 = CDR(BgL_cdrzd2467zd2_165);
														if (PAIRP(BgL_cdrzd2471zd2_169))
															{	/* Expand/if.scm 105 */
																if (NULLP(CDR(BgL_cdrzd2471zd2_169)))
																	{	/* Expand/if.scm 105 */
																		obj_t BgL_arg1197z00_174;

																		BgL_arg1197z00_174 =
																			CAR(BgL_cdrzd2471zd2_169);
																		{	/* Expand/if.scm 109 */
																			obj_t BgL_arg1226z00_645;

																			BgL_arg1226z00_645 =
																				BGL_PROCEDURE_CALL2(BgL_ez00_8,
																				BgL_arg1197z00_174, BgL_ez00_8);
																			return
																				BGl_replacez12z12zztools_miscz00
																				(BgL_xz00_7, BgL_arg1226z00_645);
																		}
																	}
																else
																	{	/* Expand/if.scm 105 */
																		goto BgL_tagzd2371zd2_141;
																	}
															}
														else
															{	/* Expand/if.scm 105 */
																obj_t BgL_cdrzd2508zd2_177;

																BgL_cdrzd2508zd2_177 =
																	CDR(((obj_t) BgL_cdrzd2379zd2_144));
																if (NULLP(CDR(((obj_t) BgL_cdrzd2508zd2_177))))
																	{	/* Expand/if.scm 105 */
																		obj_t BgL_arg1201z00_180;
																		obj_t BgL_arg1202z00_181;

																		BgL_arg1201z00_180 =
																			CAR(((obj_t) BgL_cdrzd2379zd2_144));
																		BgL_arg1202z00_181 =
																			CAR(((obj_t) BgL_cdrzd2508zd2_177));
																		{
																			obj_t BgL_alorsz00_1089;
																			obj_t BgL_testz00_1088;

																			BgL_testz00_1088 = BgL_arg1201z00_180;
																			BgL_alorsz00_1089 = BgL_arg1202z00_181;
																			BgL_alorsz00_139 = BgL_alorsz00_1089;
																			BgL_testz00_138 = BgL_testz00_1088;
																			goto BgL_tagzd2370zd2_140;
																		}
																	}
																else
																	{	/* Expand/if.scm 105 */
																		goto BgL_tagzd2371zd2_141;
																	}
															}
													}
												else
													{	/* Expand/if.scm 105 */
														goto BgL_tagzd2371zd2_141;
													}
											}
										else
											{	/* Expand/if.scm 105 */
												obj_t BgL_cdrzd2538zd2_184;

												BgL_cdrzd2538zd2_184 =
													CDR(((obj_t) BgL_cdrzd2379zd2_144));
												if (PAIRP(BgL_cdrzd2538zd2_184))
													{	/* Expand/if.scm 105 */
														obj_t BgL_cdrzd2543zd2_186;

														BgL_cdrzd2543zd2_186 = CDR(BgL_cdrzd2538zd2_184);
														if (PAIRP(BgL_cdrzd2543zd2_186))
															{	/* Expand/if.scm 105 */
																if (NULLP(CDR(BgL_cdrzd2543zd2_186)))
																	{	/* Expand/if.scm 105 */
																		obj_t BgL_arg1208z00_190;
																		obj_t BgL_arg1209z00_191;
																		obj_t BgL_arg1210z00_192;

																		BgL_arg1208z00_190 =
																			CAR(((obj_t) BgL_cdrzd2379zd2_144));
																		BgL_arg1209z00_191 =
																			CAR(BgL_cdrzd2538zd2_184);
																		BgL_arg1210z00_192 =
																			CAR(BgL_cdrzd2543zd2_186);
																		BgL_testz00_134 = BgL_arg1208z00_190;
																		BgL_alorsz00_135 = BgL_arg1209z00_191;
																		BgL_sinonz00_136 = BgL_arg1210z00_192;
																		{	/* Expand/if.scm 111 */
																			obj_t BgL_newzd2testzd2_205;
																			obj_t BgL_newzd2thenzd2_206;
																			obj_t BgL_newzd2elsezd2_207;

																			BgL_newzd2testzd2_205 =
																				BGl_replacez12z12zztools_miscz00
																				(BgL_testz00_134,
																				BGl_expandzd2ifzd2withz00zzexpand_ifz00
																				(BGl_expandzd2testzd2envz00zzexpand_ifz00,
																					BgL_testz00_134, BgL_ez00_8,
																					((bool_t) 1)));
																			BgL_newzd2thenzd2_206 =
																				BGl_replacez12z12zztools_miscz00
																				(BgL_alorsz00_135,
																				BGl_expandzd2ifzd2withz00zzexpand_ifz00
																				(BgL_ez00_8, BgL_alorsz00_135,
																					BgL_ez00_8, ((bool_t) 1)));
																			BgL_newzd2elsezd2_207 =
																				BGl_replacez12z12zztools_miscz00
																				(BgL_sinonz00_136,
																				BGl_expandzd2ifzd2withz00zzexpand_ifz00
																				(BgL_ez00_8, BgL_sinonz00_136,
																					BgL_ez00_8, ((bool_t) 0)));
																			{	/* Expand/if.scm 114 */
																				obj_t BgL_resz00_208;

																				{	/* Expand/if.scm 114 */
																					obj_t BgL_exp0z00_209;

																					{	/* Expand/if.scm 114 */
																						obj_t BgL_arg1252z00_230;

																						{	/* Expand/if.scm 114 */
																							obj_t BgL_arg1268z00_231;

																							{	/* Expand/if.scm 114 */
																								obj_t BgL_arg1272z00_232;

																								BgL_arg1272z00_232 =
																									MAKE_YOUNG_PAIR
																									(BgL_newzd2elsezd2_207, BNIL);
																								BgL_arg1268z00_231 =
																									MAKE_YOUNG_PAIR
																									(BgL_newzd2thenzd2_206,
																									BgL_arg1272z00_232);
																							}
																							BgL_arg1252z00_230 =
																								MAKE_YOUNG_PAIR
																								(BgL_newzd2testzd2_205,
																								BgL_arg1268z00_231);
																						}
																						BgL_exp0z00_209 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																							BgL_arg1252z00_230);
																					}
																					{	/* Expand/if.scm 114 */
																						obj_t BgL_exp1z00_210;

																						BgL_exp1z00_210 =
																							BGl_epairifyzd2reczd2zztools_miscz00
																							(BgL_exp0z00_209, BgL_xz00_7);
																						{	/* Expand/if.scm 115 */
																							obj_t BgL_expz00_211;

																							{	/* Expand/if.scm 116 */
																								obj_t BgL_arg1248z00_228;
																								obj_t BgL_arg1249z00_229;

																								BgL_arg1248z00_228 =
																									CAR(
																									((obj_t) BgL_exp1z00_210));
																								BgL_arg1249z00_229 =
																									CDR(
																									((obj_t) BgL_exp1z00_210));
																								BgL_expz00_211 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1248z00_228,
																									BgL_arg1249z00_229);
																							}
																							{	/* Expand/if.scm 116 */

																								{	/* Expand/if.scm 117 */
																									bool_t BgL_test1939z00_1121;

																									if (BGl_za2casezd2enabledzf3za2z21zzexpand_ifz00)
																										{	/* Expand/if.scm 118 */
																											bool_t
																												BgL_test1941z00_1123;
																											{	/* Expand/if.scm 118 */
																												obj_t
																													BgL_arg1242z00_226;
																												{	/* Expand/if.scm 118 */
																													obj_t
																														BgL_arg1244z00_227;
																													BgL_arg1244z00_227 =
																														BGl_lexicalzd2stackzd2zzexpand_epsz00
																														();
																													BgL_arg1242z00_226 =
																														BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																														(CNST_TABLE_REF(6),
																														BgL_arg1244z00_227);
																												}
																												BgL_test1941z00_1123 =
																													PAIRP
																													(BgL_arg1242z00_226);
																											}
																											if (BgL_test1941z00_1123)
																												{	/* Expand/if.scm 118 */
																													BgL_test1939z00_1121 =
																														((bool_t) 0);
																												}
																											else
																												{	/* Expand/if.scm 118 */
																													BgL_test1939z00_1121 =
																														((bool_t) 1);
																												}
																										}
																									else
																										{	/* Expand/if.scm 117 */
																											BgL_test1939z00_1121 =
																												((bool_t) 0);
																										}
																									if (BgL_test1939z00_1121)
																										{	/* Expand/if.scm 119 */
																											obj_t
																												BgL_newzd2expzd2_218;
																											BgL_newzd2expzd2_218 =
																												BGl_findzd2casezd2expz00zzexpand_ifz00
																												(BgL_expz00_211);
																											if (CBOOL
																												(BgL_newzd2expzd2_218))
																												{	/* Expand/if.scm 121 */
																													obj_t
																														BgL_newzd2newzd2expz00_219;
																													{	/* Expand/if.scm 122 */
																														obj_t
																															BgL_arg1239z00_224;
																														BgL_arg1239z00_224 =
																															BGl_ifzd2ze3casez12z23zzexpand_ifz00
																															(BgL_newzd2expzd2_218);
																														BgL_newzd2newzd2expz00_219
																															=
																															BGL_PROCEDURE_CALL2
																															(BgL_ez00_8,
																															BgL_arg1239z00_224,
																															BgL_ez00_8);
																													}
																													{	/* Expand/if.scm 123 */
																														obj_t
																															BgL_arg1233z00_220;
																														obj_t
																															BgL_arg1234z00_221;
																														BgL_arg1233z00_220 =
																															CAR(((obj_t)
																																BgL_newzd2expzd2_218));
																														BgL_arg1234z00_221 =
																															CAR(((obj_t)
																																BgL_newzd2newzd2expz00_219));
																														{	/* Expand/if.scm 123 */
																															obj_t
																																BgL_tmpz00_1141;
																															BgL_tmpz00_1141 =
																																((obj_t)
																																BgL_arg1233z00_220);
																															SET_CAR
																																(BgL_tmpz00_1141,
																																BgL_arg1234z00_221);
																														}
																													}
																													{	/* Expand/if.scm 125 */
																														obj_t
																															BgL_arg1236z00_222;
																														obj_t
																															BgL_arg1238z00_223;
																														BgL_arg1236z00_222 =
																															CAR(((obj_t)
																																BgL_newzd2expzd2_218));
																														BgL_arg1238z00_223 =
																															CDR(((obj_t)
																																BgL_newzd2newzd2expz00_219));
																														{	/* Expand/if.scm 125 */
																															obj_t
																																BgL_tmpz00_1148;
																															BgL_tmpz00_1148 =
																																((obj_t)
																																BgL_arg1236z00_222);
																															SET_CDR
																																(BgL_tmpz00_1148,
																																BgL_arg1238z00_223);
																														}
																													}
																													BgL_resz00_208 =
																														BgL_expz00_211;
																												}
																											else
																												{	/* Expand/if.scm 120 */
																													BgL_resz00_208 =
																														BgL_expz00_211;
																												}
																										}
																									else
																										{	/* Expand/if.scm 117 */
																											BgL_resz00_208 =
																												BgL_expz00_211;
																										}
																								}
																							}
																						}
																					}
																				}
																				BGl_replacez12z12zztools_miscz00
																					(BgL_xz00_7, BgL_resz00_208);
																			}
																			return BgL_xz00_7;
																		}
																	}
																else
																	{	/* Expand/if.scm 105 */
																		goto BgL_tagzd2371zd2_141;
																	}
															}
														else
															{	/* Expand/if.scm 105 */
																if (NULLP(CDR(((obj_t) BgL_cdrzd2538zd2_184))))
																	{	/* Expand/if.scm 105 */
																		obj_t BgL_arg1218z00_198;
																		obj_t BgL_arg1219z00_199;

																		BgL_arg1218z00_198 =
																			CAR(((obj_t) BgL_cdrzd2379zd2_144));
																		BgL_arg1219z00_199 =
																			CAR(((obj_t) BgL_cdrzd2538zd2_184));
																		{
																			obj_t BgL_alorsz00_1161;
																			obj_t BgL_testz00_1160;

																			BgL_testz00_1160 = BgL_arg1218z00_198;
																			BgL_alorsz00_1161 = BgL_arg1219z00_199;
																			BgL_alorsz00_139 = BgL_alorsz00_1161;
																			BgL_testz00_138 = BgL_testz00_1160;
																			goto BgL_tagzd2370zd2_140;
																		}
																	}
																else
																	{	/* Expand/if.scm 105 */
																		goto BgL_tagzd2371zd2_141;
																	}
															}
													}
												else
													{	/* Expand/if.scm 105 */
														goto BgL_tagzd2371zd2_141;
													}
											}
									}
							}
						else
							{	/* Expand/if.scm 105 */
								goto BgL_tagzd2371zd2_141;
							}
					}
				else
					{	/* Expand/if.scm 105 */
						goto BgL_tagzd2371zd2_141;
					}
			}
		}

	}



/* &expand-if */
	obj_t BGl_z62expandzd2ifzb0zzexpand_ifz00(obj_t BgL_envz00_811,
		obj_t BgL_xz00_812, obj_t BgL_ez00_813)
	{
		{	/* Expand/if.scm 104 */
			return BGl_expandzd2ifzd2zzexpand_ifz00(BgL_xz00_812, BgL_ez00_813);
		}

	}



/* expand-if-with */
	obj_t BGl_expandzd2ifzd2withz00zzexpand_ifz00(obj_t BgL_e1z00_9,
		obj_t BgL_xz00_10, obj_t BgL_e2z00_11, bool_t BgL_casezf3zf3_12)
	{
		{	/* Expand/if.scm 148 */
			{	/* Expand/if.scm 149 */
				bool_t BgL_casezd2enabledzf3z21_242;

				BgL_casezd2enabledzf3z21_242 =
					BGl_za2casezd2enabledzf3za2z21zzexpand_ifz00;
				BGl_za2casezd2enabledzf3za2z21zzexpand_ifz00 = BgL_casezf3zf3_12;
				{	/* Expand/if.scm 151 */
					obj_t BgL_resz00_243;

					BgL_resz00_243 =
						BGL_PROCEDURE_CALL2(BgL_e1z00_9, BgL_xz00_10, BgL_e2z00_11);
					BGl_za2casezd2enabledzf3za2z21zzexpand_ifz00 =
						BgL_casezd2enabledzf3z21_242;
					{	/* Expand/if.scm 153 */
						bool_t BgL_test1944z00_1168;

						if (PAIRP(BgL_xz00_10))
							{	/* Expand/if.scm 153 */
								BgL_test1944z00_1168 = PAIRP(BgL_resz00_243);
							}
						else
							{	/* Expand/if.scm 153 */
								BgL_test1944z00_1168 = ((bool_t) 0);
							}
						if (BgL_test1944z00_1168)
							{	/* Expand/if.scm 153 */
								return
									BGl_replacez12z12zztools_miscz00(BgL_xz00_10, BgL_resz00_243);
							}
						else
							{	/* Expand/if.scm 153 */
								return BgL_resz00_243;
							}
					}
				}
			}
		}

	}



/* find-case-exp */
	obj_t BGl_findzd2casezd2expz00zzexpand_ifz00(obj_t BgL_expz00_13)
	{
		{	/* Expand/if.scm 164 */
		BGl_findzd2casezd2expz00zzexpand_ifz00:
			{	/* Expand/if.scm 166 */
				obj_t BgL_iszd2casezd2_247;

				BgL_iszd2casezd2_247 = BGl_iszd2casezf3z21zzexpand_ifz00(BgL_expz00_13);
				if (CBOOL(BgL_iszd2casezd2_247))
					{	/* Expand/if.scm 167 */
						return BgL_iszd2casezd2_247;
					}
				else
					{	/* Expand/if.scm 167 */
						if (PAIRP(BgL_expz00_13))
							{	/* Expand/if.scm 169 */
								obj_t BgL_cdrzd2618zd2_253;

								BgL_cdrzd2618zd2_253 = CDR(((obj_t) BgL_expz00_13));
								if ((CAR(((obj_t) BgL_expz00_13)) == CNST_TABLE_REF(0)))
									{	/* Expand/if.scm 169 */
										if (PAIRP(BgL_cdrzd2618zd2_253))
											{	/* Expand/if.scm 169 */
												obj_t BgL_cdrzd2621zd2_257;

												BgL_cdrzd2621zd2_257 = CDR(BgL_cdrzd2618zd2_253);
												if (PAIRP(BgL_cdrzd2621zd2_257))
													{	/* Expand/if.scm 169 */
														obj_t BgL_cdrzd2624zd2_259;

														BgL_cdrzd2624zd2_259 = CDR(BgL_cdrzd2621zd2_257);
														if (PAIRP(BgL_cdrzd2624zd2_259))
															{	/* Expand/if.scm 169 */
																if (NULLP(CDR(BgL_cdrzd2624zd2_259)))
																	{
																		obj_t BgL_expz00_1196;

																		BgL_expz00_1196 = CAR(BgL_cdrzd2624zd2_259);
																		BgL_expz00_13 = BgL_expz00_1196;
																		goto BGl_findzd2casezd2expz00zzexpand_ifz00;
																	}
																else
																	{	/* Expand/if.scm 169 */
																		return BFALSE;
																	}
															}
														else
															{	/* Expand/if.scm 169 */
																return BFALSE;
															}
													}
												else
													{	/* Expand/if.scm 169 */
														return BFALSE;
													}
											}
										else
											{	/* Expand/if.scm 169 */
												return BFALSE;
											}
									}
								else
									{	/* Expand/if.scm 169 */
										return BFALSE;
									}
							}
						else
							{	/* Expand/if.scm 169 */
								return BFALSE;
							}
					}
			}
		}

	}



/* is-case? */
	obj_t BGl_iszd2casezf3z21zzexpand_ifz00(obj_t BgL_expz00_14)
	{
		{	/* Expand/if.scm 190 */
			{
				obj_t BgL_varz00_268;
				obj_t BgL_expz72z72_269;
				long BgL_nbzd2clauseszd2_270;

				BgL_varz00_268 = BNIL;
				BgL_expz72z72_269 = BgL_expz00_14;
				BgL_nbzd2clauseszd2_270 = 1L;
			BgL_zc3z04anonymousza31326ze3z87_271:
				{
					obj_t BgL_testz00_272;
					obj_t BgL_sinonz00_273;

					if (PAIRP(BgL_expz72z72_269))
						{	/* Expand/if.scm 191 */
							obj_t BgL_cdrzd2637zd2_278;

							BgL_cdrzd2637zd2_278 = CDR(((obj_t) BgL_expz72z72_269));
							if ((CAR(((obj_t) BgL_expz72z72_269)) == CNST_TABLE_REF(0)))
								{	/* Expand/if.scm 191 */
									if (PAIRP(BgL_cdrzd2637zd2_278))
										{	/* Expand/if.scm 191 */
											obj_t BgL_cdrzd2641zd2_282;

											BgL_cdrzd2641zd2_282 = CDR(BgL_cdrzd2637zd2_278);
											if (PAIRP(BgL_cdrzd2641zd2_282))
												{	/* Expand/if.scm 191 */
													obj_t BgL_cdrzd2645zd2_284;

													BgL_cdrzd2645zd2_284 = CDR(BgL_cdrzd2641zd2_282);
													if (PAIRP(BgL_cdrzd2645zd2_284))
														{	/* Expand/if.scm 191 */
															if (NULLP(CDR(BgL_cdrzd2645zd2_284)))
																{	/* Expand/if.scm 191 */
																	BgL_testz00_272 = CAR(BgL_cdrzd2637zd2_278);
																	BgL_sinonz00_273 = CAR(BgL_cdrzd2645zd2_284);
																	{
																		obj_t BgL_newzd2varzd2_295;
																		obj_t BgL_exp1z00_296;
																		obj_t BgL_exp1z00_292;
																		obj_t BgL_exp2z00_293;

																		if (PAIRP(BgL_testz00_272))
																			{	/* Expand/if.scm 191 */
																				obj_t BgL_carzd2658zd2_300;
																				obj_t BgL_cdrzd2659zd2_301;

																				BgL_carzd2658zd2_300 =
																					CAR(((obj_t) BgL_testz00_272));
																				BgL_cdrzd2659zd2_301 =
																					CDR(((obj_t) BgL_testz00_272));
																				{

																					if (
																						(BgL_carzd2658zd2_300 ==
																							CNST_TABLE_REF(7)))
																						{	/* Expand/if.scm 191 */
																						BgL_kapzd2660zd2_302:
																							if (PAIRP(BgL_cdrzd2659zd2_301))
																								{	/* Expand/if.scm 191 */
																									obj_t BgL_cdrzd2664zd2_332;

																									BgL_cdrzd2664zd2_332 =
																										CDR(BgL_cdrzd2659zd2_301);
																									if (PAIRP
																										(BgL_cdrzd2664zd2_332))
																										{	/* Expand/if.scm 191 */
																											if (NULLP(CDR
																													(BgL_cdrzd2664zd2_332)))
																												{	/* Expand/if.scm 191 */
																													BgL_exp1z00_292 =
																														CAR
																														(BgL_cdrzd2659zd2_301);
																													BgL_exp2z00_293 =
																														CAR
																														(BgL_cdrzd2664zd2_332);
																													if (BGl_iszd2azd2validzd2constantzf3z21zzexpand_ifz00(BgL_exp1z00_292))
																														{	/* Expand/if.scm 199 */
																															if (NULLP
																																(BgL_varz00_268))
																																{
																																	long
																																		BgL_nbzd2clauseszd2_1241;
																																	obj_t
																																		BgL_expz72z72_1240;
																																	obj_t
																																		BgL_varz00_1239;
																																	BgL_varz00_1239
																																		=
																																		BgL_exp2z00_293;
																																	BgL_expz72z72_1240
																																		=
																																		BgL_sinonz00_273;
																																	BgL_nbzd2clauseszd2_1241
																																		=
																																		(BgL_nbzd2clauseszd2_270
																																		+ 1L);
																																	BgL_nbzd2clauseszd2_270
																																		=
																																		BgL_nbzd2clauseszd2_1241;
																																	BgL_expz72z72_269
																																		=
																																		BgL_expz72z72_1240;
																																	BgL_varz00_268
																																		=
																																		BgL_varz00_1239;
																																	goto
																																		BgL_zc3z04anonymousza31326ze3z87_271;
																																}
																															else
																																{	/* Expand/if.scm 201 */
																																	if (
																																		(BgL_varz00_268
																																			==
																																			BgL_exp2z00_293))
																																		{
																																			long
																																				BgL_nbzd2clauseszd2_1247;
																																			obj_t
																																				BgL_expz72z72_1246;
																																			obj_t
																																				BgL_varz00_1245;
																																			BgL_varz00_1245
																																				=
																																				BgL_exp2z00_293;
																																			BgL_expz72z72_1246
																																				=
																																				BgL_sinonz00_273;
																																			BgL_nbzd2clauseszd2_1247
																																				=
																																				(BgL_nbzd2clauseszd2_270
																																				+ 1L);
																																			BgL_nbzd2clauseszd2_270
																																				=
																																				BgL_nbzd2clauseszd2_1247;
																																			BgL_expz72z72_269
																																				=
																																				BgL_expz72z72_1246;
																																			BgL_varz00_268
																																				=
																																				BgL_varz00_1245;
																																			goto
																																				BgL_zc3z04anonymousza31326ze3z87_271;
																																		}
																																	else
																																		{	/* Expand/if.scm 203 */
																																			if (
																																				(BgL_nbzd2clauseszd2_270
																																					> 3L))
																																				{	/* Expand/if.scm 207 */
																																					obj_t
																																						BgL_list1491z00_344;
																																					{	/* Expand/if.scm 207 */
																																						obj_t
																																							BgL_arg1502z00_345;
																																						{	/* Expand/if.scm 207 */
																																							obj_t
																																								BgL_arg1509z00_346;
																																							BgL_arg1509z00_346
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_expz72z72_269,
																																								BNIL);
																																							BgL_arg1502z00_345
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_varz00_268,
																																								BgL_arg1509z00_346);
																																						}
																																						BgL_list1491z00_344
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_expz00_14,
																																							BgL_arg1502z00_345);
																																					}
																																					return
																																						BgL_list1491z00_344;
																																				}
																																			else
																																				{	/* Expand/if.scm 206 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																}
																														}
																													else
																														{	/* Expand/if.scm 199 */
																															if (BGl_iszd2azd2validzd2constantzf3z21zzexpand_ifz00(BgL_exp2z00_293))
																																{	/* Expand/if.scm 209 */
																																	if (NULLP
																																		(BgL_varz00_268))
																																		{
																																			long
																																				BgL_nbzd2clauseszd2_1260;
																																			obj_t
																																				BgL_expz72z72_1259;
																																			obj_t
																																				BgL_varz00_1258;
																																			BgL_varz00_1258
																																				=
																																				BgL_exp1z00_292;
																																			BgL_expz72z72_1259
																																				=
																																				BgL_sinonz00_273;
																																			BgL_nbzd2clauseszd2_1260
																																				=
																																				(BgL_nbzd2clauseszd2_270
																																				+ 1L);
																																			BgL_nbzd2clauseszd2_270
																																				=
																																				BgL_nbzd2clauseszd2_1260;
																																			BgL_expz72z72_269
																																				=
																																				BgL_expz72z72_1259;
																																			BgL_varz00_268
																																				=
																																				BgL_varz00_1258;
																																			goto
																																				BgL_zc3z04anonymousza31326ze3z87_271;
																																		}
																																	else
																																		{	/* Expand/if.scm 211 */
																																			if (
																																				(BgL_varz00_268
																																					==
																																					BgL_exp1z00_292))
																																				{
																																					long
																																						BgL_nbzd2clauseszd2_1266;
																																					obj_t
																																						BgL_expz72z72_1265;
																																					obj_t
																																						BgL_varz00_1264;
																																					BgL_varz00_1264
																																						=
																																						BgL_exp1z00_292;
																																					BgL_expz72z72_1265
																																						=
																																						BgL_sinonz00_273;
																																					BgL_nbzd2clauseszd2_1266
																																						=
																																						(BgL_nbzd2clauseszd2_270
																																						+
																																						1L);
																																					BgL_nbzd2clauseszd2_270
																																						=
																																						BgL_nbzd2clauseszd2_1266;
																																					BgL_expz72z72_269
																																						=
																																						BgL_expz72z72_1265;
																																					BgL_varz00_268
																																						=
																																						BgL_varz00_1264;
																																					goto
																																						BgL_zc3z04anonymousza31326ze3z87_271;
																																				}
																																			else
																																				{	/* Expand/if.scm 213 */
																																					if (
																																						(BgL_nbzd2clauseszd2_270
																																							>
																																							3L))
																																						{	/* Expand/if.scm 217 */
																																							obj_t
																																								BgL_list1516z00_352;
																																							{	/* Expand/if.scm 217 */
																																								obj_t
																																									BgL_arg1535z00_353;
																																								{	/* Expand/if.scm 217 */
																																									obj_t
																																										BgL_arg1540z00_354;
																																									BgL_arg1540z00_354
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_expz72z72_269,
																																										BNIL);
																																									BgL_arg1535z00_353
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_varz00_268,
																																										BgL_arg1540z00_354);
																																								}
																																								BgL_list1516z00_352
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_expz00_14,
																																									BgL_arg1535z00_353);
																																							}
																																							return
																																								BgL_list1516z00_352;
																																						}
																																					else
																																						{	/* Expand/if.scm 216 */
																																							return
																																								BFALSE;
																																						}
																																				}
																																		}
																																}
																															else
																																{	/* Expand/if.scm 209 */
																																	return BFALSE;
																																}
																														}
																												}
																											else
																												{	/* Expand/if.scm 191 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/if.scm 191 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/if.scm 191 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Expand/if.scm 191 */
																							if (
																								(BgL_carzd2658zd2_300 ==
																									CNST_TABLE_REF(8)))
																								{	/* Expand/if.scm 191 */
																									goto BgL_kapzd2660zd2_302;
																								}
																							else
																								{	/* Expand/if.scm 191 */
																									if (
																										(BgL_carzd2658zd2_300 ==
																											CNST_TABLE_REF(9)))
																										{	/* Expand/if.scm 191 */
																											goto BgL_kapzd2660zd2_302;
																										}
																									else
																										{	/* Expand/if.scm 191 */
																											if (
																												(BgL_carzd2658zd2_300 ==
																													CNST_TABLE_REF(10)))
																												{	/* Expand/if.scm 191 */
																													goto
																														BgL_kapzd2660zd2_302;
																												}
																											else
																												{	/* Expand/if.scm 191 */
																													if (
																														(BgL_carzd2658zd2_300
																															==
																															CNST_TABLE_REF
																															(11)))
																														{	/* Expand/if.scm 191 */
																															goto
																																BgL_kapzd2660zd2_302;
																														}
																													else
																														{	/* Expand/if.scm 191 */
																															if (
																																(CAR(
																																		((obj_t)
																																			BgL_testz00_272))
																																	==
																																	CNST_TABLE_REF
																																	(12)))
																																{	/* Expand/if.scm 191 */
																																	if (PAIRP
																																		(BgL_cdrzd2659zd2_301))
																																		{	/* Expand/if.scm 191 */
																																			obj_t
																																				BgL_cdrzd2696zd2_313;
																																			BgL_cdrzd2696zd2_313
																																				=
																																				CDR
																																				(BgL_cdrzd2659zd2_301);
																																			if (PAIRP
																																				(BgL_cdrzd2696zd2_313))
																																				{	/* Expand/if.scm 191 */
																																					obj_t
																																						BgL_carzd2699zd2_315;
																																					BgL_carzd2699zd2_315
																																						=
																																						CAR
																																						(BgL_cdrzd2696zd2_313);
																																					if (PAIRP(BgL_carzd2699zd2_315))
																																						{	/* Expand/if.scm 191 */
																																							obj_t
																																								BgL_cdrzd2703zd2_317;
																																							BgL_cdrzd2703zd2_317
																																								=
																																								CDR
																																								(BgL_carzd2699zd2_315);
																																							if ((CAR(BgL_carzd2699zd2_315) == CNST_TABLE_REF(13)))
																																								{	/* Expand/if.scm 191 */
																																									if (PAIRP(BgL_cdrzd2703zd2_317))
																																										{	/* Expand/if.scm 191 */
																																											if (NULLP(CDR(BgL_cdrzd2703zd2_317)))
																																												{	/* Expand/if.scm 191 */
																																													if (NULLP(CDR(BgL_cdrzd2696zd2_313)))
																																														{	/* Expand/if.scm 191 */
																																															BgL_newzd2varzd2_295
																																																=
																																																CAR
																																																(BgL_cdrzd2659zd2_301);
																																															BgL_exp1z00_296
																																																=
																																																CAR
																																																(BgL_cdrzd2703zd2_317);
																																															{	/* Expand/if.scm 223 */
																																																bool_t
																																																	BgL_test1984z00_1313;
																																																if (PAIRP(BgL_exp1z00_296))
																																																	{
																																																		obj_t
																																																			BgL_exp1z00_379;
																																																		BgL_exp1z00_379
																																																			=
																																																			BgL_exp1z00_296;
																																																	BgL_zc3z04anonymousza31574ze3z87_380:
																																																		if (NULLP(BgL_exp1z00_379))
																																																			{	/* Expand/if.scm 226 */
																																																				BgL_test1984z00_1313
																																																					=
																																																					(
																																																					(bool_t)
																																																					1);
																																																			}
																																																		else
																																																			{	/* Expand/if.scm 228 */
																																																				bool_t
																																																					BgL_test1987z00_1318;
																																																				{	/* Expand/if.scm 228 */
																																																					obj_t
																																																						BgL_arg1576z00_384;
																																																					BgL_arg1576z00_384
																																																						=
																																																						CAR
																																																						(
																																																						((obj_t) BgL_exp1z00_379));
																																																					BgL_test1987z00_1318
																																																						=
																																																						BGl_iszd2azd2validzd2constantzf3z21zzexpand_ifz00
																																																						(BgL_arg1576z00_384);
																																																				}
																																																				if (BgL_test1987z00_1318)
																																																					{	/* Expand/if.scm 229 */
																																																						obj_t
																																																							BgL_arg1575z00_383;
																																																						BgL_arg1575z00_383
																																																							=
																																																							CDR
																																																							(
																																																							((obj_t) BgL_exp1z00_379));
																																																						{
																																																							obj_t
																																																								BgL_exp1z00_1324;
																																																							BgL_exp1z00_1324
																																																								=
																																																								BgL_arg1575z00_383;
																																																							BgL_exp1z00_379
																																																								=
																																																								BgL_exp1z00_1324;
																																																							goto
																																																								BgL_zc3z04anonymousza31574ze3z87_380;
																																																						}
																																																					}
																																																				else
																																																					{	/* Expand/if.scm 228 */
																																																						BgL_test1984z00_1313
																																																							=
																																																							(
																																																							(bool_t)
																																																							0);
																																																					}
																																																			}
																																																	}
																																																else
																																																	{	/* Expand/if.scm 223 */
																																																		BgL_test1984z00_1313
																																																			=
																																																			(
																																																			(bool_t)
																																																			0);
																																																	}
																																																if (BgL_test1984z00_1313)
																																																	{	/* Expand/if.scm 223 */
																																																		if (NULLP(BgL_varz00_268))
																																																			{
																																																				long
																																																					BgL_nbzd2clauseszd2_1329;
																																																				obj_t
																																																					BgL_expz72z72_1328;
																																																				obj_t
																																																					BgL_varz00_1327;
																																																				BgL_varz00_1327
																																																					=
																																																					BgL_newzd2varzd2_295;
																																																				BgL_expz72z72_1328
																																																					=
																																																					BgL_sinonz00_273;
																																																				BgL_nbzd2clauseszd2_1329
																																																					=
																																																					(BgL_nbzd2clauseszd2_270
																																																					+
																																																					1L);
																																																				BgL_nbzd2clauseszd2_270
																																																					=
																																																					BgL_nbzd2clauseszd2_1329;
																																																				BgL_expz72z72_269
																																																					=
																																																					BgL_expz72z72_1328;
																																																				BgL_varz00_268
																																																					=
																																																					BgL_varz00_1327;
																																																				goto
																																																					BgL_zc3z04anonymousza31326ze3z87_271;
																																																			}
																																																		else
																																																			{	/* Expand/if.scm 233 */
																																																				if ((BgL_newzd2varzd2_295 == BgL_varz00_268))
																																																					{
																																																						long
																																																							BgL_nbzd2clauseszd2_1335;
																																																						obj_t
																																																							BgL_expz72z72_1334;
																																																						obj_t
																																																							BgL_varz00_1333;
																																																						BgL_varz00_1333
																																																							=
																																																							BgL_newzd2varzd2_295;
																																																						BgL_expz72z72_1334
																																																							=
																																																							BgL_sinonz00_273;
																																																						BgL_nbzd2clauseszd2_1335
																																																							=
																																																							(BgL_nbzd2clauseszd2_270
																																																							+
																																																							1L);
																																																						BgL_nbzd2clauseszd2_270
																																																							=
																																																							BgL_nbzd2clauseszd2_1335;
																																																						BgL_expz72z72_269
																																																							=
																																																							BgL_expz72z72_1334;
																																																						BgL_varz00_268
																																																							=
																																																							BgL_varz00_1333;
																																																						goto
																																																							BgL_zc3z04anonymousza31326ze3z87_271;
																																																					}
																																																				else
																																																					{	/* Expand/if.scm 235 */
																																																						if ((BgL_nbzd2clauseszd2_270 > 3L))
																																																							{	/* Expand/if.scm 239 */
																																																								obj_t
																																																									BgL_list1563z00_370;
																																																								{	/* Expand/if.scm 239 */
																																																									obj_t
																																																										BgL_arg1564z00_371;
																																																									{	/* Expand/if.scm 239 */
																																																										obj_t
																																																											BgL_arg1565z00_372;
																																																										BgL_arg1565z00_372
																																																											=
																																																											MAKE_YOUNG_PAIR
																																																											(BgL_expz72z72_269,
																																																											BNIL);
																																																										BgL_arg1564z00_371
																																																											=
																																																											MAKE_YOUNG_PAIR
																																																											(BgL_varz00_268,
																																																											BgL_arg1565z00_372);
																																																									}
																																																									BgL_list1563z00_370
																																																										=
																																																										MAKE_YOUNG_PAIR
																																																										(BgL_expz00_14,
																																																										BgL_arg1564z00_371);
																																																								}
																																																								return
																																																									BgL_list1563z00_370;
																																																							}
																																																						else
																																																							{	/* Expand/if.scm 238 */
																																																								return
																																																									BFALSE;
																																																							}
																																																					}
																																																			}
																																																	}
																																																else
																																																	{	/* Expand/if.scm 223 */
																																																		if ((BgL_nbzd2clauseszd2_270 > 3L))
																																																			{	/* Expand/if.scm 243 */
																																																				obj_t
																																																					BgL_list1567z00_374;
																																																				{	/* Expand/if.scm 243 */
																																																					obj_t
																																																						BgL_arg1571z00_375;
																																																					{	/* Expand/if.scm 243 */
																																																						obj_t
																																																							BgL_arg1573z00_376;
																																																						BgL_arg1573z00_376
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_expz72z72_269,
																																																							BNIL);
																																																						BgL_arg1571z00_375
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_varz00_268,
																																																							BgL_arg1573z00_376);
																																																					}
																																																					BgL_list1567z00_374
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_expz00_14,
																																																						BgL_arg1571z00_375);
																																																				}
																																																				return
																																																					BgL_list1567z00_374;
																																																			}
																																																		else
																																																			{	/* Expand/if.scm 242 */
																																																				return
																																																					BFALSE;
																																																			}
																																																	}
																																															}
																																														}
																																													else
																																														{	/* Expand/if.scm 191 */
																																															return
																																																BFALSE;
																																														}
																																												}
																																											else
																																												{	/* Expand/if.scm 191 */
																																													return
																																														BFALSE;
																																												}
																																										}
																																									else
																																										{	/* Expand/if.scm 191 */
																																											return
																																												BFALSE;
																																										}
																																								}
																																							else
																																								{	/* Expand/if.scm 191 */
																																									return
																																										BFALSE;
																																								}
																																						}
																																					else
																																						{	/* Expand/if.scm 191 */
																																							return
																																								BFALSE;
																																						}
																																				}
																																			else
																																				{	/* Expand/if.scm 191 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Expand/if.scm 191 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Expand/if.scm 191 */
																																	return BFALSE;
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																			}
																		else
																			{	/* Expand/if.scm 191 */
																				return BFALSE;
																			}
																	}
																}
															else
																{	/* Expand/if.scm 191 */
																BgL_tagzd2630zd2_275:
																	if ((BgL_nbzd2clauseszd2_270 > 3L))
																		{	/* Expand/if.scm 247 */
																			obj_t BgL_list1578z00_387;

																			{	/* Expand/if.scm 247 */
																				obj_t BgL_arg1584z00_388;

																				{	/* Expand/if.scm 247 */
																					obj_t BgL_arg1585z00_389;

																					BgL_arg1585z00_389 =
																						MAKE_YOUNG_PAIR(BgL_expz72z72_269,
																						BNIL);
																					BgL_arg1584z00_388 =
																						MAKE_YOUNG_PAIR(BgL_varz00_268,
																						BgL_arg1585z00_389);
																				}
																				BgL_list1578z00_387 =
																					MAKE_YOUNG_PAIR(BgL_expz00_14,
																					BgL_arg1584z00_388);
																			}
																			return BgL_list1578z00_387;
																		}
																	else
																		{	/* Expand/if.scm 246 */
																			return BFALSE;
																		}
																}
														}
													else
														{	/* Expand/if.scm 191 */
															goto BgL_tagzd2630zd2_275;
														}
												}
											else
												{	/* Expand/if.scm 191 */
													goto BgL_tagzd2630zd2_275;
												}
										}
									else
										{	/* Expand/if.scm 191 */
											goto BgL_tagzd2630zd2_275;
										}
								}
							else
								{	/* Expand/if.scm 191 */
									goto BgL_tagzd2630zd2_275;
								}
						}
					else
						{	/* Expand/if.scm 191 */
							goto BgL_tagzd2630zd2_275;
						}
				}
			}
		}

	}



/* is-a-valid-constant? */
	bool_t BGl_iszd2azd2validzd2constantzf3z21zzexpand_ifz00(obj_t BgL_cnstz00_15)
	{
		{	/* Expand/if.scm 255 */
			{	/* Expand/if.scm 256 */
				bool_t BgL__ortest_1019z00_391;

				BgL__ortest_1019z00_391 = INTEGERP(BgL_cnstz00_15);
				if (BgL__ortest_1019z00_391)
					{	/* Expand/if.scm 256 */
						return BgL__ortest_1019z00_391;
					}
				else
					{	/* Expand/if.scm 257 */
						bool_t BgL__ortest_1020z00_392;

						BgL__ortest_1020z00_392 = CHARP(BgL_cnstz00_15);
						if (BgL__ortest_1020z00_392)
							{	/* Expand/if.scm 257 */
								return BgL__ortest_1020z00_392;
							}
						else
							{	/* Expand/if.scm 258 */
								bool_t BgL__ortest_1021z00_393;

								BgL__ortest_1021z00_393 = CNSTP(BgL_cnstz00_15);
								if (BgL__ortest_1021z00_393)
									{	/* Expand/if.scm 258 */
										return BgL__ortest_1021z00_393;
									}
								else
									{	/* Expand/if.scm 259 */
										bool_t BgL__ortest_1022z00_394;

										BgL__ortest_1022z00_394 = KEYWORDP(BgL_cnstz00_15);
										if (BgL__ortest_1022z00_394)
											{	/* Expand/if.scm 259 */
												return BgL__ortest_1022z00_394;
											}
										else
											{	/* Expand/if.scm 259 */
												if (PAIRP(BgL_cnstz00_15))
													{	/* Expand/if.scm 260 */
														if ((CAR(BgL_cnstz00_15) == CNST_TABLE_REF(13)))
															{	/* Expand/if.scm 262 */
																bool_t BgL_test1999z00_1370;

																{	/* Expand/if.scm 262 */
																	obj_t BgL_tmpz00_1371;

																	BgL_tmpz00_1371 = CDR(BgL_cnstz00_15);
																	BgL_test1999z00_1370 = PAIRP(BgL_tmpz00_1371);
																}
																if (BgL_test1999z00_1370)
																	{	/* Expand/if.scm 263 */
																		bool_t BgL_test2000z00_1374;

																		{	/* Expand/if.scm 263 */
																			obj_t BgL_tmpz00_1375;

																			BgL_tmpz00_1375 =
																				CAR(CDR(BgL_cnstz00_15));
																			BgL_test2000z00_1374 =
																				SYMBOLP(BgL_tmpz00_1375);
																		}
																		if (BgL_test2000z00_1374)
																			{	/* Expand/if.scm 263 */
																				return NULLP(CDR(CDR(BgL_cnstz00_15)));
																			}
																		else
																			{	/* Expand/if.scm 263 */
																				return ((bool_t) 0);
																			}
																	}
																else
																	{	/* Expand/if.scm 262 */
																		return ((bool_t) 0);
																	}
															}
														else
															{	/* Expand/if.scm 261 */
																return ((bool_t) 0);
															}
													}
												else
													{	/* Expand/if.scm 260 */
														return ((bool_t) 0);
													}
											}
									}
							}
					}
			}
		}

	}



/* if->case! */
	obj_t BGl_ifzd2ze3casez12z23zzexpand_ifz00(obj_t BgL_expzd2varzd2endz00_16)
	{
		{	/* Expand/if.scm 273 */
			{	/* Expand/if.scm 275 */
				obj_t BgL_expz00_403;

				BgL_expz00_403 = CAR(((obj_t) BgL_expzd2varzd2endz00_16));
				{	/* Expand/if.scm 275 */
					obj_t BgL_varz00_404;

					{	/* Expand/if.scm 276 */
						obj_t BgL_pairz00_729;

						BgL_pairz00_729 = CDR(((obj_t) BgL_expzd2varzd2endz00_16));
						BgL_varz00_404 = CAR(BgL_pairz00_729);
					}
					{	/* Expand/if.scm 276 */
						obj_t BgL_endzd2expzd2_405;

						{	/* Expand/if.scm 277 */
							obj_t BgL_pairz00_735;

							{	/* Expand/if.scm 277 */
								obj_t BgL_pairz00_734;

								BgL_pairz00_734 = CDR(((obj_t) BgL_expzd2varzd2endz00_16));
								BgL_pairz00_735 = CDR(BgL_pairz00_734);
							}
							BgL_endzd2expzd2_405 = CAR(BgL_pairz00_735);
						}
						{	/* Expand/if.scm 277 */
							obj_t BgL_newzd2expzd2_406;

							{	/* Expand/if.scm 279 */
								obj_t BgL_arg1605z00_409;

								{	/* Expand/if.scm 279 */
									obj_t BgL_arg1606z00_410;

									{	/* Expand/if.scm 279 */
										obj_t BgL_arg1609z00_411;

										{
											obj_t BgL_expz00_414;
											obj_t BgL_clausesz00_415;

											BgL_expz00_414 = BgL_expz00_403;
											BgL_clausesz00_415 = BNIL;
										BgL_zc3z04anonymousza31610ze3z87_416:
											if ((BgL_expz00_414 == BgL_endzd2expzd2_405))
												{	/* Expand/if.scm 285 */
													obj_t BgL_arg1611z00_417;

													{	/* Expand/if.scm 285 */
														obj_t BgL_arg1613z00_418;

														{	/* Expand/if.scm 285 */
															obj_t BgL_arg1615z00_419;

															BgL_arg1615z00_419 =
																MAKE_YOUNG_PAIR(BgL_endzd2expzd2_405, BNIL);
															BgL_arg1613z00_418 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																BgL_arg1615z00_419);
														}
														BgL_arg1611z00_417 =
															MAKE_YOUNG_PAIR(BgL_arg1613z00_418,
															BgL_clausesz00_415);
													}
													BgL_arg1609z00_411 =
														bgl_reverse_bang(BgL_arg1611z00_417);
												}
											else
												{	/* Expand/if.scm 284 */
													if (PAIRP(BgL_expz00_414))
														{	/* Expand/if.scm 286 */
															obj_t BgL_cdrzd2721zd2_426;

															BgL_cdrzd2721zd2_426 =
																CDR(((obj_t) BgL_expz00_414));
															if (
																(CAR(
																		((obj_t) BgL_expz00_414)) ==
																	CNST_TABLE_REF(0)))
																{	/* Expand/if.scm 286 */
																	if (PAIRP(BgL_cdrzd2721zd2_426))
																		{	/* Expand/if.scm 286 */
																			obj_t BgL_cdrzd2726zd2_430;

																			BgL_cdrzd2726zd2_430 =
																				CDR(BgL_cdrzd2721zd2_426);
																			if (PAIRP(BgL_cdrzd2726zd2_430))
																				{	/* Expand/if.scm 286 */
																					obj_t BgL_cdrzd2731zd2_432;

																					BgL_cdrzd2731zd2_432 =
																						CDR(BgL_cdrzd2726zd2_430);
																					if (PAIRP(BgL_cdrzd2731zd2_432))
																						{	/* Expand/if.scm 286 */
																							if (NULLP(CDR
																									(BgL_cdrzd2731zd2_432)))
																								{	/* Expand/if.scm 286 */
																									obj_t BgL_arg1646z00_436;
																									obj_t BgL_arg1650z00_437;
																									obj_t BgL_arg1651z00_438;

																									BgL_arg1646z00_436 =
																										CAR(BgL_cdrzd2721zd2_426);
																									BgL_arg1650z00_437 =
																										CAR(BgL_cdrzd2726zd2_430);
																									BgL_arg1651z00_438 =
																										CAR(BgL_cdrzd2731zd2_432);
																									{	/* Expand/if.scm 289 */
																										obj_t BgL_arg1663z00_746;

																										BgL_arg1663z00_746 =
																											MAKE_YOUNG_PAIR
																											(BGl_makezd2clausezd2zzexpand_ifz00
																											(BgL_varz00_404,
																												BgL_arg1646z00_436,
																												BgL_arg1650z00_437),
																											BgL_clausesz00_415);
																										{
																											obj_t BgL_clausesz00_1424;
																											obj_t BgL_expz00_1423;

																											BgL_expz00_1423 =
																												BgL_arg1651z00_438;
																											BgL_clausesz00_1424 =
																												BgL_arg1663z00_746;
																											BgL_clausesz00_415 =
																												BgL_clausesz00_1424;
																											BgL_expz00_414 =
																												BgL_expz00_1423;
																											goto
																												BgL_zc3z04anonymousza31610ze3z87_416;
																										}
																									}
																								}
																							else
																								{	/* Expand/if.scm 286 */
																									BgL_arg1609z00_411 = BFALSE;
																								}
																						}
																					else
																						{	/* Expand/if.scm 286 */
																							BgL_arg1609z00_411 = BFALSE;
																						}
																				}
																			else
																				{	/* Expand/if.scm 286 */
																					BgL_arg1609z00_411 = BFALSE;
																				}
																		}
																	else
																		{	/* Expand/if.scm 286 */
																			BgL_arg1609z00_411 = BFALSE;
																		}
																}
															else
																{	/* Expand/if.scm 286 */
																	BgL_arg1609z00_411 = BFALSE;
																}
														}
													else
														{	/* Expand/if.scm 286 */
															BgL_arg1609z00_411 = BFALSE;
														}
												}
										}
										BgL_arg1606z00_410 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1609z00_411, BNIL);
									}
									BgL_arg1605z00_409 =
										MAKE_YOUNG_PAIR(BgL_varz00_404, BgL_arg1606z00_410);
								}
								BgL_newzd2expzd2_406 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg1605z00_409);
							}
							{	/* Expand/if.scm 278 */

								{	/* Expand/if.scm 292 */
									obj_t BgL_arg1595z00_407;

									BgL_arg1595z00_407 = CAR(BgL_newzd2expzd2_406);
									{	/* Expand/if.scm 292 */
										obj_t BgL_tmpz00_1430;

										BgL_tmpz00_1430 = ((obj_t) BgL_expz00_403);
										SET_CAR(BgL_tmpz00_1430, BgL_arg1595z00_407);
									}
								}
								{	/* Expand/if.scm 293 */
									obj_t BgL_arg1602z00_408;

									BgL_arg1602z00_408 = CDR(BgL_newzd2expzd2_406);
									{	/* Expand/if.scm 293 */
										obj_t BgL_tmpz00_1434;

										BgL_tmpz00_1434 = ((obj_t) BgL_expz00_403);
										SET_CDR(BgL_tmpz00_1434, BgL_arg1602z00_408);
									}
								}
								return BgL_expz00_403;
							}
						}
					}
				}
			}
		}

	}



/* make-clause */
	obj_t BGl_makezd2clausezd2zzexpand_ifz00(obj_t BgL_varz00_17,
		obj_t BgL_testz00_18, obj_t BgL_alorsz00_19)
	{
		{	/* Expand/if.scm 300 */
			{
				obj_t BgL_exp1z00_445;
				obj_t BgL_exp2z00_446;

				if (PAIRP(BgL_testz00_18))
					{	/* Expand/if.scm 306 */
						obj_t BgL_carzd2745zd2_452;
						obj_t BgL_cdrzd2746zd2_453;

						BgL_carzd2745zd2_452 = CAR(((obj_t) BgL_testz00_18));
						BgL_cdrzd2746zd2_453 = CDR(((obj_t) BgL_testz00_18));
						{

							if ((BgL_carzd2745zd2_452 == CNST_TABLE_REF(8)))
								{	/* Expand/if.scm 306 */
								BgL_kapzd2747zd2_454:
									if (PAIRP(BgL_cdrzd2746zd2_453))
										{	/* Expand/if.scm 306 */
											obj_t BgL_cdrzd2751zd2_483;

											BgL_cdrzd2751zd2_483 = CDR(BgL_cdrzd2746zd2_453);
											if (PAIRP(BgL_cdrzd2751zd2_483))
												{	/* Expand/if.scm 306 */
													if (NULLP(CDR(BgL_cdrzd2751zd2_483)))
														{	/* Expand/if.scm 306 */
															BgL_exp1z00_445 = CAR(BgL_cdrzd2746zd2_453);
															BgL_exp2z00_446 = CAR(BgL_cdrzd2751zd2_483);
															if ((BgL_exp1z00_445 == BgL_varz00_17))
																{	/* Expand/if.scm 309 */
																	obj_t BgL_arg1733z00_490;
																	obj_t BgL_arg1734z00_491;

																	BgL_arg1733z00_490 =
																		MAKE_YOUNG_PAIR
																		(BGl_valzd2ze3constantze70zd6zzexpand_ifz00
																		(BgL_exp2z00_446), BNIL);
																	BgL_arg1734z00_491 =
																		MAKE_YOUNG_PAIR(BgL_alorsz00_19, BNIL);
																	return MAKE_YOUNG_PAIR(BgL_arg1733z00_490,
																		BgL_arg1734z00_491);
																}
															else
																{	/* Expand/if.scm 310 */
																	obj_t BgL_arg1736z00_493;
																	obj_t BgL_arg1737z00_494;

																	BgL_arg1736z00_493 =
																		MAKE_YOUNG_PAIR
																		(BGl_valzd2ze3constantze70zd6zzexpand_ifz00
																		(BgL_exp1z00_445), BNIL);
																	BgL_arg1737z00_494 =
																		MAKE_YOUNG_PAIR(BgL_alorsz00_19, BNIL);
																	return MAKE_YOUNG_PAIR(BgL_arg1736z00_493,
																		BgL_arg1737z00_494);
																}
														}
													else
														{	/* Expand/if.scm 306 */
															return BFALSE;
														}
												}
											else
												{	/* Expand/if.scm 306 */
													return BFALSE;
												}
										}
									else
										{	/* Expand/if.scm 306 */
											return BFALSE;
										}
								}
							else
								{	/* Expand/if.scm 306 */
									if ((BgL_carzd2745zd2_452 == CNST_TABLE_REF(7)))
										{	/* Expand/if.scm 306 */
											goto BgL_kapzd2747zd2_454;
										}
									else
										{	/* Expand/if.scm 306 */
											if ((BgL_carzd2745zd2_452 == CNST_TABLE_REF(11)))
												{	/* Expand/if.scm 306 */
													goto BgL_kapzd2747zd2_454;
												}
											else
												{	/* Expand/if.scm 306 */
													if ((BgL_carzd2745zd2_452 == CNST_TABLE_REF(9)))
														{	/* Expand/if.scm 306 */
															goto BgL_kapzd2747zd2_454;
														}
													else
														{	/* Expand/if.scm 306 */
															if ((BgL_carzd2745zd2_452 == CNST_TABLE_REF(10)))
																{	/* Expand/if.scm 306 */
																	goto BgL_kapzd2747zd2_454;
																}
															else
																{	/* Expand/if.scm 306 */
																	if (
																		(CAR(
																				((obj_t) BgL_testz00_18)) ==
																			CNST_TABLE_REF(12)))
																		{	/* Expand/if.scm 306 */
																			if (PAIRP(BgL_cdrzd2746zd2_453))
																				{	/* Expand/if.scm 306 */
																					obj_t BgL_cdrzd2772zd2_465;

																					BgL_cdrzd2772zd2_465 =
																						CDR(BgL_cdrzd2746zd2_453);
																					if (PAIRP(BgL_cdrzd2772zd2_465))
																						{	/* Expand/if.scm 306 */
																							obj_t BgL_carzd2774zd2_467;

																							BgL_carzd2774zd2_467 =
																								CAR(BgL_cdrzd2772zd2_465);
																							if (PAIRP(BgL_carzd2774zd2_467))
																								{	/* Expand/if.scm 306 */
																									obj_t BgL_cdrzd2778zd2_469;

																									BgL_cdrzd2778zd2_469 =
																										CDR(BgL_carzd2774zd2_467);
																									if (
																										(CAR(BgL_carzd2774zd2_467)
																											== CNST_TABLE_REF(13)))
																										{	/* Expand/if.scm 306 */
																											if (PAIRP
																												(BgL_cdrzd2778zd2_469))
																												{	/* Expand/if.scm 306 */
																													if (NULLP(CDR
																															(BgL_cdrzd2778zd2_469)))
																														{	/* Expand/if.scm 306 */
																															if (NULLP(CDR
																																	(BgL_cdrzd2772zd2_465)))
																																{	/* Expand/if.scm 306 */
																																	obj_t
																																		BgL_arg1708z00_477;
																																	BgL_arg1708z00_477
																																		=
																																		CAR
																																		(BgL_cdrzd2778zd2_469);
																																	{	/* Expand/if.scm 312 */
																																		obj_t
																																			BgL_arg1739z00_778;
																																		BgL_arg1739z00_778
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_alorsz00_19,
																																			BNIL);
																																		return
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1708z00_477,
																																			BgL_arg1739z00_778);
																																	}
																																}
																															else
																																{	/* Expand/if.scm 306 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/if.scm 306 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/if.scm 306 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/if.scm 306 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/if.scm 306 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Expand/if.scm 306 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Expand/if.scm 306 */
																					return BFALSE;
																				}
																		}
																	else
																		{	/* Expand/if.scm 306 */
																			return BFALSE;
																		}
																}
														}
												}
										}
								}
						}
					}
				else
					{	/* Expand/if.scm 306 */
						return BFALSE;
					}
			}
		}

	}



/* val->constant~0 */
	obj_t BGl_valzd2ze3constantze70zd6zzexpand_ifz00(obj_t BgL_vz00_497)
	{
		{	/* Expand/if.scm 305 */
			{	/* Expand/if.scm 302 */
				bool_t BgL_test2026z00_1507;

				if (PAIRP(BgL_vz00_497))
					{	/* Expand/if.scm 302 */
						BgL_test2026z00_1507 = (CAR(BgL_vz00_497) == CNST_TABLE_REF(13));
					}
				else
					{	/* Expand/if.scm 302 */
						BgL_test2026z00_1507 = ((bool_t) 0);
					}
				if (BgL_test2026z00_1507)
					{	/* Expand/if.scm 302 */
						return CAR(CDR(BgL_vz00_497));
					}
				else
					{	/* Expand/if.scm 302 */
						return BgL_vz00_497;
					}
			}
		}

	}



/* expand-or */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2orzd2zzexpand_ifz00(obj_t BgL_xz00_21,
		obj_t BgL_ez00_22)
	{
		{	/* Expand/if.scm 329 */
			{	/* Expand/if.scm 330 */
				obj_t BgL_resz00_507;

				{	/* Expand/if.scm 330 */
					obj_t BgL_g1028z00_509;

					BgL_g1028z00_509 = CDR(((obj_t) BgL_xz00_21));
					BgL_resz00_507 =
						BGl_loopze71ze7zzexpand_ifz00(BgL_xz00_21, BgL_g1028z00_509);
				}
				{	/* Expand/if.scm 347 */
					obj_t BgL_arg1750z00_508;

					BgL_arg1750z00_508 =
						BGL_PROCEDURE_CALL2(BgL_ez00_22, BgL_resz00_507, BgL_ez00_22);
					return
						BGl_replacez12z12zztools_miscz00(BgL_xz00_21, BgL_arg1750z00_508);
				}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zzexpand_ifz00(obj_t BgL_xz00_824, obj_t BgL_sorz00_511)
	{
		{	/* Expand/if.scm 330 */
			if (NULLP(BgL_sorz00_511))
				{	/* Expand/if.scm 332 */
					return BFALSE;
				}
			else
				{	/* Expand/if.scm 332 */
					if (PAIRP(BgL_sorz00_511))
						{	/* Expand/if.scm 334 */
							if (NULLP(CDR(BgL_sorz00_511)))
								{	/* Expand/if.scm 336 */
									return CAR(BgL_sorz00_511);
								}
							else
								{	/* Expand/if.scm 339 */
									obj_t BgL_testz00_517;

									{	/* Expand/if.scm 318 */
										obj_t BgL_symbolz00_782;

										BgL_symbolz00_782 =
											BGl_gensymz00zz__r4_symbols_6_4z00(string_append_3
											(BGl_string1885z00zzexpand_ifz00,
												BGl_string1886z00zzexpand_ifz00,
												BGl_string1887z00zzexpand_ifz00));
										BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_symbolz00_782,
											CNST_TABLE_REF(15), BTRUE);
										BgL_testz00_517 = BgL_symbolz00_782;
									}
									{	/* Expand/if.scm 339 */
										obj_t BgL_resz00_518;

										{	/* Expand/if.scm 340 */
											obj_t BgL_arg1775z00_526;

											{	/* Expand/if.scm 340 */
												obj_t BgL_arg1798z00_527;
												obj_t BgL_arg1799z00_528;

												{	/* Expand/if.scm 340 */
													obj_t BgL_arg1805z00_529;

													{	/* Expand/if.scm 340 */
														obj_t BgL_arg1806z00_530;

														BgL_arg1806z00_530 =
															MAKE_YOUNG_PAIR(CAR(BgL_sorz00_511), BNIL);
														BgL_arg1805z00_529 =
															MAKE_YOUNG_PAIR(BgL_testz00_517,
															BgL_arg1806z00_530);
													}
													BgL_arg1798z00_527 =
														MAKE_YOUNG_PAIR(BgL_arg1805z00_529, BNIL);
												}
												{	/* Expand/if.scm 341 */
													obj_t BgL_arg1812z00_532;

													{	/* Expand/if.scm 341 */
														obj_t BgL_arg1820z00_533;

														{	/* Expand/if.scm 341 */
															obj_t BgL_arg1822z00_534;

															{	/* Expand/if.scm 341 */
																obj_t BgL_arg1823z00_535;

																BgL_arg1823z00_535 =
																	MAKE_YOUNG_PAIR(BGl_loopze71ze7zzexpand_ifz00
																	(BgL_xz00_824, CDR(BgL_sorz00_511)), BNIL);
																BgL_arg1822z00_534 =
																	MAKE_YOUNG_PAIR(BgL_testz00_517,
																	BgL_arg1823z00_535);
															}
															BgL_arg1820z00_533 =
																MAKE_YOUNG_PAIR(BgL_testz00_517,
																BgL_arg1822z00_534);
														}
														BgL_arg1812z00_532 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
															BgL_arg1820z00_533);
													}
													BgL_arg1799z00_528 =
														MAKE_YOUNG_PAIR(BgL_arg1812z00_532, BNIL);
												}
												BgL_arg1775z00_526 =
													MAKE_YOUNG_PAIR(BgL_arg1798z00_527,
													BgL_arg1799z00_528);
											}
											BgL_resz00_518 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1775z00_526);
										}
										{	/* Expand/if.scm 340 */

											{	/* Expand/if.scm 343 */
												bool_t BgL_test2031z00_1551;

												{	/* Expand/if.scm 343 */
													obj_t BgL_tmpz00_1552;

													BgL_tmpz00_1552 = CAR(BgL_sorz00_511);
													BgL_test2031z00_1551 = EPAIRP(BgL_tmpz00_1552);
												}
												if (BgL_test2031z00_1551)
													{	/* Expand/if.scm 344 */
														obj_t BgL_arg1765z00_521;
														obj_t BgL_arg1767z00_522;
														obj_t BgL_arg1770z00_523;

														BgL_arg1765z00_521 = CAR(BgL_resz00_518);
														BgL_arg1767z00_522 = CDR(BgL_resz00_518);
														BgL_arg1770z00_523 = CER(CAR(BgL_sorz00_511));
														{	/* Expand/if.scm 344 */
															obj_t BgL_res1881z00_791;

															BgL_res1881z00_791 =
																MAKE_YOUNG_EPAIR(BgL_arg1765z00_521,
																BgL_arg1767z00_522, BgL_arg1770z00_523);
															return BgL_res1881z00_791;
														}
													}
												else
													{	/* Expand/if.scm 343 */
														return BgL_resz00_518;
													}
											}
										}
									}
								}
						}
					else
						{	/* Expand/if.scm 334 */
							return
								BGl_errorz00zz__errorz00(CNST_TABLE_REF(4),
								BGl_string1883z00zzexpand_ifz00, BgL_xz00_824);
						}
				}
		}

	}



/* &expand-or */
	obj_t BGl_z62expandzd2orzb0zzexpand_ifz00(obj_t BgL_envz00_814,
		obj_t BgL_xz00_815, obj_t BgL_ez00_816)
	{
		{	/* Expand/if.scm 329 */
			return BGl_expandzd2orzd2zzexpand_ifz00(BgL_xz00_815, BgL_ez00_816);
		}

	}



/* expand-and */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2andzd2zzexpand_ifz00(obj_t BgL_xz00_23,
		obj_t BgL_ez00_24)
	{
		{	/* Expand/if.scm 352 */
			{	/* Expand/if.scm 353 */
				obj_t BgL_resz00_540;

				{	/* Expand/if.scm 353 */
					obj_t BgL_g1029z00_542;

					BgL_g1029z00_542 = CDR(((obj_t) BgL_xz00_23));
					BgL_resz00_540 =
						BGl_loopze70ze7zzexpand_ifz00(BgL_xz00_23, BgL_g1029z00_542);
				}
				{	/* Expand/if.scm 370 */
					obj_t BgL_arg1834z00_541;

					BgL_arg1834z00_541 =
						BGL_PROCEDURE_CALL2(BgL_ez00_24, BgL_resz00_540, BgL_ez00_24);
					return
						BGl_replacez12z12zztools_miscz00(BgL_xz00_23, BgL_arg1834z00_541);
				}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzexpand_ifz00(obj_t BgL_xz00_823, obj_t BgL_sandz00_544)
	{
		{	/* Expand/if.scm 353 */
			if (NULLP(BgL_sandz00_544))
				{	/* Expand/if.scm 355 */
					return BTRUE;
				}
			else
				{	/* Expand/if.scm 355 */
					if (PAIRP(BgL_sandz00_544))
						{	/* Expand/if.scm 357 */
							if (NULLP(CDR(BgL_sandz00_544)))
								{	/* Expand/if.scm 359 */
									return CAR(BgL_sandz00_544);
								}
							else
								{	/* Expand/if.scm 362 */
									obj_t BgL_testz00_550;

									{	/* Expand/if.scm 318 */
										obj_t BgL_symbolz00_795;

										BgL_symbolz00_795 =
											BGl_gensymz00zz__r4_symbols_6_4z00(string_append_3
											(BGl_string1885z00zzexpand_ifz00,
												BGl_string1888z00zzexpand_ifz00,
												BGl_string1887z00zzexpand_ifz00));
										BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_symbolz00_795,
											CNST_TABLE_REF(15), BTRUE);
										BgL_testz00_550 = BgL_symbolz00_795;
									}
									{	/* Expand/if.scm 362 */
										obj_t BgL_resz00_551;

										{	/* Expand/if.scm 363 */
											obj_t BgL_arg1847z00_559;

											{	/* Expand/if.scm 363 */
												obj_t BgL_arg1848z00_560;
												obj_t BgL_arg1849z00_561;

												{	/* Expand/if.scm 363 */
													obj_t BgL_arg1850z00_562;

													{	/* Expand/if.scm 363 */
														obj_t BgL_arg1851z00_563;

														BgL_arg1851z00_563 =
															MAKE_YOUNG_PAIR(CAR(BgL_sandz00_544), BNIL);
														BgL_arg1850z00_562 =
															MAKE_YOUNG_PAIR(BgL_testz00_550,
															BgL_arg1851z00_563);
													}
													BgL_arg1848z00_560 =
														MAKE_YOUNG_PAIR(BgL_arg1850z00_562, BNIL);
												}
												{	/* Expand/if.scm 364 */
													obj_t BgL_arg1853z00_565;

													{	/* Expand/if.scm 364 */
														obj_t BgL_arg1854z00_566;

														{	/* Expand/if.scm 364 */
															obj_t BgL_arg1856z00_567;

															{	/* Expand/if.scm 364 */
																obj_t BgL_arg1857z00_568;
																obj_t BgL_arg1858z00_569;

																BgL_arg1857z00_568 =
																	BGl_loopze70ze7zzexpand_ifz00(BgL_xz00_823,
																	CDR(BgL_sandz00_544));
																BgL_arg1858z00_569 =
																	MAKE_YOUNG_PAIR(BFALSE, BNIL);
																BgL_arg1856z00_567 =
																	MAKE_YOUNG_PAIR(BgL_arg1857z00_568,
																	BgL_arg1858z00_569);
															}
															BgL_arg1854z00_566 =
																MAKE_YOUNG_PAIR(BgL_testz00_550,
																BgL_arg1856z00_567);
														}
														BgL_arg1853z00_565 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
															BgL_arg1854z00_566);
													}
													BgL_arg1849z00_561 =
														MAKE_YOUNG_PAIR(BgL_arg1853z00_565, BNIL);
												}
												BgL_arg1847z00_559 =
													MAKE_YOUNG_PAIR(BgL_arg1848z00_560,
													BgL_arg1849z00_561);
											}
											BgL_resz00_551 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1847z00_559);
										}
										{	/* Expand/if.scm 363 */

											{	/* Expand/if.scm 366 */
												bool_t BgL_test2035z00_1599;

												{	/* Expand/if.scm 366 */
													obj_t BgL_tmpz00_1600;

													BgL_tmpz00_1600 = CAR(BgL_sandz00_544);
													BgL_test2035z00_1599 = EPAIRP(BgL_tmpz00_1600);
												}
												if (BgL_test2035z00_1599)
													{	/* Expand/if.scm 367 */
														obj_t BgL_arg1842z00_554;
														obj_t BgL_arg1843z00_555;
														obj_t BgL_arg1844z00_556;

														BgL_arg1842z00_554 = CAR(BgL_resz00_551);
														BgL_arg1843z00_555 = CDR(BgL_resz00_551);
														BgL_arg1844z00_556 = CER(CAR(BgL_sandz00_544));
														{	/* Expand/if.scm 367 */
															obj_t BgL_res1882z00_804;

															BgL_res1882z00_804 =
																MAKE_YOUNG_EPAIR(BgL_arg1842z00_554,
																BgL_arg1843z00_555, BgL_arg1844z00_556);
															return BgL_res1882z00_804;
														}
													}
												else
													{	/* Expand/if.scm 366 */
														return BgL_resz00_551;
													}
											}
										}
									}
								}
						}
					else
						{	/* Expand/if.scm 357 */
							return
								BGl_errorz00zz__errorz00(CNST_TABLE_REF(5),
								BGl_string1883z00zzexpand_ifz00, BgL_xz00_823);
						}
				}
		}

	}



/* &expand-and */
	obj_t BGl_z62expandzd2andzb0zzexpand_ifz00(obj_t BgL_envz00_817,
		obj_t BgL_xz00_818, obj_t BgL_ez00_819)
	{
		{	/* Expand/if.scm 352 */
			return BGl_expandzd2andzd2zzexpand_ifz00(BgL_xz00_818, BgL_ez00_819);
		}

	}



/* expand-not */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2notzd2zzexpand_ifz00(obj_t BgL_xz00_25,
		obj_t BgL_ez00_26)
	{
		{	/* Expand/if.scm 375 */
			{
				obj_t BgL_expz00_573;

				if (PAIRP(BgL_xz00_25))
					{	/* Expand/if.scm 376 */
						obj_t BgL_cdrzd2791zd2_578;

						BgL_cdrzd2791zd2_578 = CDR(((obj_t) BgL_xz00_25));
						if (PAIRP(BgL_cdrzd2791zd2_578))
							{	/* Expand/if.scm 376 */
								if (NULLP(CDR(BgL_cdrzd2791zd2_578)))
									{	/* Expand/if.scm 376 */
										BgL_expz00_573 = CAR(BgL_cdrzd2791zd2_578);
										{	/* Expand/if.scm 378 */
											obj_t BgL_resz00_584;

											{	/* Expand/if.scm 378 */
												obj_t BgL_arg1869z00_585;

												{	/* Expand/if.scm 378 */
													obj_t BgL_arg1870z00_586;

													{	/* Expand/if.scm 378 */
														obj_t BgL_arg1872z00_587;

														{	/* Expand/if.scm 378 */
															obj_t BgL_arg1873z00_588;

															BgL_arg1873z00_588 = MAKE_YOUNG_PAIR(BTRUE, BNIL);
															BgL_arg1872z00_587 =
																MAKE_YOUNG_PAIR(BFALSE, BgL_arg1873z00_588);
														}
														BgL_arg1870z00_586 =
															MAKE_YOUNG_PAIR(BgL_expz00_573,
															BgL_arg1872z00_587);
													}
													BgL_arg1869z00_585 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
														BgL_arg1870z00_586);
												}
												BgL_resz00_584 =
													BGL_PROCEDURE_CALL2(BgL_ez00_26, BgL_arg1869z00_585,
													BgL_ez00_26);
											}
											return
												BGl_replacez12z12zztools_miscz00(BgL_xz00_25,
												BgL_resz00_584);
										}
									}
								else
									{	/* Expand/if.scm 376 */
									BgL_tagzd2786zd2_575:
										return
											BGl_errorz00zz__errorz00(BFALSE,
											BGl_string1889z00zzexpand_ifz00, BgL_xz00_25);
									}
							}
						else
							{	/* Expand/if.scm 376 */
								goto BgL_tagzd2786zd2_575;
							}
					}
				else
					{	/* Expand/if.scm 376 */
						goto BgL_tagzd2786zd2_575;
					}
			}
		}

	}



/* &expand-not */
	obj_t BGl_z62expandzd2notzb0zzexpand_ifz00(obj_t BgL_envz00_820,
		obj_t BgL_xz00_821, obj_t BgL_ez00_822)
	{
		{	/* Expand/if.scm 375 */
			return BGl_expandzd2notzd2zzexpand_ifz00(BgL_xz00_821, BgL_ez00_822);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_ifz00(void)
	{
		{	/* Expand/if.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_ifz00(void)
	{
		{	/* Expand/if.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_ifz00(void)
	{
		{	/* Expand/if.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_ifz00(void)
	{
		{	/* Expand/if.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1890z00zzexpand_ifz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1890z00zzexpand_ifz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1890z00zzexpand_ifz00));
			return
				BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string1890z00zzexpand_ifz00));
		}

	}

#ifdef __cplusplus
}
#endif
