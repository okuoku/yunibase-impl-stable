/*===========================================================================*/
/*   (Expand/lambda.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/lambda.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_LAMBDA_TYPE_DEFINITIONS
#define BGL_EXPAND_LAMBDA_TYPE_DEFINITIONS
#endif													// BGL_EXPAND_LAMBDA_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62zc3z04anonymousza31103ze3ze5zzexpand_lambdaz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_lambdaz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31115ze3ze5zzexpand_lambdaz00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzexpand_lambdaz00(void);
	static obj_t BGl_z62expandzd2argszb0zzexpand_lambdaz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_lambdaz00(void);
	static obj_t BGl_z62zc3z04anonymousza31141ze3ze5zzexpand_lambdaz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_lambdaz00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2lambdazd2zzexpand_lambdaz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31144ze3ze5zzexpand_lambdaz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31080ze3ze5zzexpand_lambdaz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzexpand_lambdaz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_lambdaz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31069ze3ze5zzexpand_lambdaz00(obj_t);
	static obj_t BGl_lambdazd2defineszd2zzexpand_lambdaz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 =
		BUNSPEC;
	extern obj_t BGl_replacez12z12zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_emapz00zztools_prognz00(obj_t, obj_t);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_withzd2lexicalzd2zzexpand_epsz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_beginzd2bindingszd2zzexpand_lambdaz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_lambdaz00(void);
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_lambdaz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_lambdaz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_lambdaz00(void);
	extern obj_t BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(obj_t);
	static obj_t BGl_z62expandzd2lambdazb0zzexpand_lambdaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62internalzd2beginzd2expanderz62zzexpand_lambdaz00(obj_t,
		obj_t);
	static obj_t BGl_loopze70ze7zzexpand_lambdaz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2argszd2zzexpand_lambdaz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2argszd2envz00zzexpand_lambdaz00,
		BgL_bgl_za762expandza7d2args1228z00,
		BGl_z62expandzd2argszb0zzexpand_lambdaz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internalzd2beginzd2expanderzd2envzd2zzexpand_lambdaz00,
		BgL_bgl_za762internalza7d2be1229z00,
		BGl_z62internalzd2beginzd2expanderz62zzexpand_lambdaz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2lambdazd2envz00zzexpand_lambdaz00,
		BgL_bgl_za762expandza7d2lamb1230z00,
		BGl_z62expandzd2lambdazb0zzexpand_lambdaz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1221z00zzexpand_lambdaz00,
		BgL_bgl_string1221za700za7za7e1231za7, "expand", 6);
	      DEFINE_STRING(BGl_string1222z00zzexpand_lambdaz00,
		BgL_bgl_string1222za700za7za7e1232za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string1223z00zzexpand_lambdaz00,
		BgL_bgl_string1223za700za7za7e1233za7, "Illegal `lambda' form", 21);
	      DEFINE_STRING(BGl_string1224z00zzexpand_lambdaz00,
		BgL_bgl_string1224za700za7za7e1234za7, "Illegal `begin' form", 20);
	      DEFINE_STRING(BGl_string1225z00zzexpand_lambdaz00,
		BgL_bgl_string1225za700za7za7e1235za7, "expand_lambda", 13);
	      DEFINE_STRING(BGl_string1226z00zzexpand_lambdaz00,
		BgL_bgl_string1226za700za7za7e1236za7, "letrec* define begin _ ", 23);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzexpand_lambdaz00));
		     ADD_ROOT((void
				*) (&BGl_internalzd2definitionzf3z21zzexpand_lambdaz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(long
		BgL_checksumz00_343, char *BgL_fromz00_344)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_lambdaz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_lambdaz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_lambdaz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_lambdaz00();
					BGl_cnstzd2initzd2zzexpand_lambdaz00();
					BGl_importedzd2moduleszd2initz00zzexpand_lambdaz00();
					return BGl_toplevelzd2initzd2zzexpand_lambdaz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_lambdaz00(void)
	{
		{	/* Expand/lambda.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_lambda");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_lambda");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"expand_lambda");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_lambda");
			BGl_modulezd2initializa7ationz75zz__prognz00(0L, "expand_lambda");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"expand_lambda");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"expand_lambda");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_lambdaz00(void)
	{
		{	/* Expand/lambda.scm 15 */
			{	/* Expand/lambda.scm 15 */
				obj_t BgL_cportz00_306;

				{	/* Expand/lambda.scm 15 */
					obj_t BgL_stringz00_313;

					BgL_stringz00_313 = BGl_string1226z00zzexpand_lambdaz00;
					{	/* Expand/lambda.scm 15 */
						obj_t BgL_startz00_314;

						BgL_startz00_314 = BINT(0L);
						{	/* Expand/lambda.scm 15 */
							obj_t BgL_endz00_315;

							BgL_endz00_315 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_313)));
							{	/* Expand/lambda.scm 15 */

								BgL_cportz00_306 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_313, BgL_startz00_314, BgL_endz00_315);
				}}}}
				{
					long BgL_iz00_307;

					BgL_iz00_307 = 3L;
				BgL_loopz00_308:
					if ((BgL_iz00_307 == -1L))
						{	/* Expand/lambda.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/lambda.scm 15 */
							{	/* Expand/lambda.scm 15 */
								obj_t BgL_arg1227z00_309;

								{	/* Expand/lambda.scm 15 */

									{	/* Expand/lambda.scm 15 */
										obj_t BgL_locationz00_311;

										BgL_locationz00_311 = BBOOL(((bool_t) 0));
										{	/* Expand/lambda.scm 15 */

											BgL_arg1227z00_309 =
												BGl_readz00zz__readerz00(BgL_cportz00_306,
												BgL_locationz00_311);
										}
									}
								}
								{	/* Expand/lambda.scm 15 */
									int BgL_tmpz00_369;

									BgL_tmpz00_369 = (int) (BgL_iz00_307);
									CNST_TABLE_SET(BgL_tmpz00_369, BgL_arg1227z00_309);
							}}
							{	/* Expand/lambda.scm 15 */
								int BgL_auxz00_312;

								BgL_auxz00_312 = (int) ((BgL_iz00_307 - 1L));
								{
									long BgL_iz00_374;

									BgL_iz00_374 = (long) (BgL_auxz00_312);
									BgL_iz00_307 = BgL_iz00_374;
									goto BgL_loopz00_308;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_lambdaz00(void)
	{
		{	/* Expand/lambda.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzexpand_lambdaz00(void)
	{
		{	/* Expand/lambda.scm 15 */
			return (BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 =
				BFALSE, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzexpand_lambdaz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_10;

				BgL_headz00_10 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_11;
					obj_t BgL_tailz00_12;

					BgL_prevz00_11 = BgL_headz00_10;
					BgL_tailz00_12 = BgL_l1z00_1;
				BgL_loopz00_13:
					if (PAIRP(BgL_tailz00_12))
						{
							obj_t BgL_newzd2prevzd2_15;

							BgL_newzd2prevzd2_15 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_12), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_11, BgL_newzd2prevzd2_15);
							{
								obj_t BgL_tailz00_384;
								obj_t BgL_prevz00_383;

								BgL_prevz00_383 = BgL_newzd2prevzd2_15;
								BgL_tailz00_384 = CDR(BgL_tailz00_12);
								BgL_tailz00_12 = BgL_tailz00_384;
								BgL_prevz00_11 = BgL_prevz00_383;
								goto BgL_loopz00_13;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_10);
				}
			}
		}

	}



/* expand-args */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2argszd2zzexpand_lambdaz00(obj_t
		BgL_argsz00_3, obj_t BgL_ez00_4)
	{
		{	/* Expand/lambda.scm 37 */
			return BGl_loopze70ze7zzexpand_lambdaz00(BgL_ez00_4, BgL_argsz00_3);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzexpand_lambdaz00(obj_t BgL_ez00_305,
		obj_t BgL_argsz00_19)
	{
		{	/* Expand/lambda.scm 38 */
			if (NULLP(BgL_argsz00_19))
				{	/* Expand/lambda.scm 40 */
					return BNIL;
				}
			else
				{	/* Expand/lambda.scm 40 */
					if (SYMBOLP(BgL_argsz00_19))
						{	/* Expand/lambda.scm 42 */
							return BgL_argsz00_19;
						}
					else
						{	/* Expand/lambda.scm 42 */
							if (PAIRP(BgL_argsz00_19))
								{	/* Expand/lambda.scm 46 */
									bool_t BgL_test1245z00_394;

									{	/* Expand/lambda.scm 46 */
										bool_t BgL_test1246z00_395;

										{	/* Expand/lambda.scm 46 */
											obj_t BgL_tmpz00_396;

											BgL_tmpz00_396 = CAR(BgL_argsz00_19);
											BgL_test1246z00_395 = PAIRP(BgL_tmpz00_396);
										}
										if (BgL_test1246z00_395)
											{	/* Expand/lambda.scm 47 */
												bool_t BgL_test1247z00_399;

												{	/* Expand/lambda.scm 47 */
													obj_t BgL_tmpz00_400;

													BgL_tmpz00_400 = CDR(CAR(BgL_argsz00_19));
													BgL_test1247z00_399 = PAIRP(BgL_tmpz00_400);
												}
												if (BgL_test1247z00_399)
													{	/* Expand/lambda.scm 48 */
														obj_t BgL_tmpz00_404;

														{	/* Expand/lambda.scm 48 */
															obj_t BgL_pairz00_223;

															BgL_pairz00_223 = CAR(BgL_argsz00_19);
															BgL_tmpz00_404 = CDR(CDR(BgL_pairz00_223));
														}
														BgL_test1245z00_394 = NULLP(BgL_tmpz00_404);
													}
												else
													{	/* Expand/lambda.scm 47 */
														BgL_test1245z00_394 = ((bool_t) 0);
													}
											}
										else
											{	/* Expand/lambda.scm 46 */
												BgL_test1245z00_394 = ((bool_t) 0);
											}
									}
									if (BgL_test1245z00_394)
										{	/* Expand/lambda.scm 51 */
											obj_t BgL_arg1040z00_35;
											obj_t BgL_arg1041z00_36;

											{	/* Expand/lambda.scm 51 */
												obj_t BgL_arg1042z00_37;
												obj_t BgL_arg1044z00_38;

												BgL_arg1042z00_37 = CAR(CAR(BgL_argsz00_19));
												{	/* Expand/lambda.scm 51 */
													obj_t BgL_arg1048z00_42;

													{	/* Expand/lambda.scm 51 */
														obj_t BgL_pairz00_230;

														BgL_pairz00_230 = CAR(BgL_argsz00_19);
														BgL_arg1048z00_42 = CAR(CDR(BgL_pairz00_230));
													}
													BgL_arg1044z00_38 =
														BGL_PROCEDURE_CALL2(BgL_ez00_305, BgL_arg1048z00_42,
														BgL_ez00_305);
												}
												{	/* Expand/lambda.scm 51 */
													obj_t BgL_list1045z00_39;

													{	/* Expand/lambda.scm 51 */
														obj_t BgL_arg1046z00_40;

														BgL_arg1046z00_40 =
															MAKE_YOUNG_PAIR(BgL_arg1044z00_38, BNIL);
														BgL_list1045z00_39 =
															MAKE_YOUNG_PAIR(BgL_arg1042z00_37,
															BgL_arg1046z00_40);
													}
													BgL_arg1040z00_35 = BgL_list1045z00_39;
												}
											}
											BgL_arg1041z00_36 =
												BGl_loopze70ze7zzexpand_lambdaz00(BgL_ez00_305,
												CDR(BgL_argsz00_19));
											return
												MAKE_YOUNG_PAIR(BgL_arg1040z00_35, BgL_arg1041z00_36);
										}
									else
										{	/* Expand/lambda.scm 46 */
											return
												MAKE_YOUNG_PAIR(CAR(BgL_argsz00_19),
												BGl_loopze70ze7zzexpand_lambdaz00(BgL_ez00_305,
													CDR(BgL_argsz00_19)));
										}
								}
							else
								{	/* Expand/lambda.scm 44 */
									return
										BGl_errorz00zz__errorz00
										(BGl_string1221z00zzexpand_lambdaz00,
										BGl_string1222z00zzexpand_lambdaz00, BgL_argsz00_19);
								}
						}
				}
		}

	}



/* &expand-args */
	obj_t BGl_z62expandzd2argszb0zzexpand_lambdaz00(obj_t BgL_envz00_270,
		obj_t BgL_argsz00_271, obj_t BgL_ez00_272)
	{
		{	/* Expand/lambda.scm 37 */
			return
				BGl_expandzd2argszd2zzexpand_lambdaz00(BgL_argsz00_271, BgL_ez00_272);
		}

	}



/* expand-lambda */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2lambdazd2zzexpand_lambdaz00(obj_t
		BgL_xz00_5, obj_t BgL_ez00_6)
	{
		{	/* Expand/lambda.scm 57 */
			{	/* Expand/lambda.scm 58 */
				obj_t BgL_oldzd2internalzd2_56;

				BgL_oldzd2internalzd2_56 =
					BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
				BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 = BTRUE;
				{	/* Expand/lambda.scm 60 */
					obj_t BgL_resz00_57;

					{
						obj_t BgL_lamz00_58;
						obj_t BgL_argsz00_59;
						obj_t BgL_bodyz00_60;

						if (PAIRP(BgL_xz00_5))
							{	/* Expand/lambda.scm 60 */
								obj_t BgL_cdrzd2369zd2_65;

								BgL_cdrzd2369zd2_65 = CDR(((obj_t) BgL_xz00_5));
								if (PAIRP(BgL_cdrzd2369zd2_65))
									{	/* Expand/lambda.scm 60 */
										obj_t BgL_cdrzd2374zd2_67;

										BgL_cdrzd2374zd2_67 = CDR(BgL_cdrzd2369zd2_65);
										if (NULLP(BgL_cdrzd2374zd2_67))
											{	/* Expand/lambda.scm 60 */
											BgL_tagzd2360zd2_62:
												BgL_resz00_57 =
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string1223z00zzexpand_lambdaz00, BgL_xz00_5);
											}
										else
											{	/* Expand/lambda.scm 60 */
												obj_t BgL_arg1062z00_69;
												obj_t BgL_arg1063z00_70;

												BgL_arg1062z00_69 = CAR(((obj_t) BgL_xz00_5));
												BgL_arg1063z00_70 = CAR(BgL_cdrzd2369zd2_65);
												BgL_lamz00_58 = BgL_arg1062z00_69;
												BgL_argsz00_59 = BgL_arg1063z00_70;
												BgL_bodyz00_60 = BgL_cdrzd2374zd2_67;
												{	/* Expand/lambda.scm 62 */
													obj_t BgL_arg1065z00_71;
													obj_t BgL_arg1066z00_72;

													BgL_arg1065z00_71 =
														BGl_argsza2zd2ze3argszd2listz41zztools_argsz00
														(BgL_argsz00_59);
													BgL_arg1066z00_72 =
														BGl_findzd2locationzd2zztools_locationz00
														(BgL_xz00_5);
													{	/* Expand/lambda.scm 66 */
														obj_t BgL_zc3z04anonymousza31069ze3z87_273;

														BgL_zc3z04anonymousza31069ze3z87_273 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31069ze3ze5zzexpand_lambdaz00,
															(int) (0L), (int) (4L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31069ze3z87_273,
															(int) (0L), BgL_ez00_6);
														PROCEDURE_SET(BgL_zc3z04anonymousza31069ze3z87_273,
															(int) (1L), BgL_argsz00_59);
														PROCEDURE_SET(BgL_zc3z04anonymousza31069ze3z87_273,
															(int) (2L), BgL_bodyz00_60);
														PROCEDURE_SET(BgL_zc3z04anonymousza31069ze3z87_273,
															(int) (3L), BgL_lamz00_58);
														BgL_resz00_57 =
															BGl_withzd2lexicalzd2zzexpand_epsz00
															(BgL_arg1065z00_71, CNST_TABLE_REF(0),
															BgL_arg1066z00_72,
															BgL_zc3z04anonymousza31069ze3z87_273);
									}}}}
								else
									{	/* Expand/lambda.scm 60 */
										goto BgL_tagzd2360zd2_62;
									}
							}
						else
							{	/* Expand/lambda.scm 60 */
								goto BgL_tagzd2360zd2_62;
							}
					}
					BGl_internalzd2definitionzf3z21zzexpand_lambdaz00 =
						BgL_oldzd2internalzd2_56;
					return BGl_replacez12z12zztools_miscz00(BgL_xz00_5, BgL_resz00_57);
				}
			}
		}

	}



/* &expand-lambda */
	obj_t BGl_z62expandzd2lambdazb0zzexpand_lambdaz00(obj_t BgL_envz00_274,
		obj_t BgL_xz00_275, obj_t BgL_ez00_276)
	{
		{	/* Expand/lambda.scm 57 */
			return
				BGl_expandzd2lambdazd2zzexpand_lambdaz00(BgL_xz00_275, BgL_ez00_276);
		}

	}



/* &<@anonymous:1069> */
	obj_t BGl_z62zc3z04anonymousza31069ze3ze5zzexpand_lambdaz00(obj_t
		BgL_envz00_277)
	{
		{	/* Expand/lambda.scm 65 */
			{	/* Expand/lambda.scm 66 */
				obj_t BgL_ez00_278;
				obj_t BgL_argsz00_279;
				obj_t BgL_bodyz00_280;
				obj_t BgL_lamz00_281;

				BgL_ez00_278 = ((obj_t) PROCEDURE_REF(BgL_envz00_277, (int) (0L)));
				BgL_argsz00_279 = PROCEDURE_REF(BgL_envz00_277, (int) (1L));
				BgL_bodyz00_280 = PROCEDURE_REF(BgL_envz00_277, (int) (2L));
				BgL_lamz00_281 = PROCEDURE_REF(BgL_envz00_277, (int) (3L));
				{	/* Expand/lambda.scm 66 */
					obj_t BgL_ez00_318;

					BgL_ez00_318 =
						BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(BgL_ez00_278);
					{	/* Expand/lambda.scm 67 */
						obj_t BgL_arg1074z00_319;

						{	/* Expand/lambda.scm 67 */
							obj_t BgL_arg1075z00_320;
							obj_t BgL_arg1076z00_321;

							BgL_arg1075z00_320 =
								BGl_loopze70ze7zzexpand_lambdaz00(BgL_ez00_318,
								BgL_argsz00_279);
							{	/* Expand/lambda.scm 68 */
								obj_t BgL_arg1078z00_323;

								{	/* Expand/lambda.scm 68 */
									obj_t BgL_arg1079z00_324;

									BgL_arg1079z00_324 =
										BGl_expandzd2prognzd2zz__prognz00(BgL_bodyz00_280);
									BgL_arg1078z00_323 =
										BGL_PROCEDURE_CALL2(BgL_ez00_318, BgL_arg1079z00_324,
										BgL_ez00_318);
								}
								BgL_arg1076z00_321 = MAKE_YOUNG_PAIR(BgL_arg1078z00_323, BNIL);
							}
							BgL_arg1074z00_319 =
								MAKE_YOUNG_PAIR(BgL_arg1075z00_320, BgL_arg1076z00_321);
						}
						return MAKE_YOUNG_PAIR(BgL_lamz00_281, BgL_arg1074z00_319);
					}
				}
			}
		}

	}



/* internal-begin-expander */
	BGL_EXPORTED_DEF obj_t
		BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(obj_t
		BgL_oldzd2expanderzd2_7)
	{
		{	/* Expand/lambda.scm 77 */
			{	/* Expand/lambda.scm 78 */
				obj_t BgL_zc3z04anonymousza31080ze3z87_286;

				BgL_zc3z04anonymousza31080ze3z87_286 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31080ze3ze5zzexpand_lambdaz00, (int) (2L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31080ze3z87_286, (int) (0L),
					BgL_oldzd2expanderzd2_7);
				return BgL_zc3z04anonymousza31080ze3z87_286;
			}
		}

	}



/* &internal-begin-expander */
	obj_t BGl_z62internalzd2beginzd2expanderz62zzexpand_lambdaz00(obj_t
		BgL_envz00_287, obj_t BgL_oldzd2expanderzd2_288)
	{
		{	/* Expand/lambda.scm 77 */
			return
				BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00
				(BgL_oldzd2expanderzd2_288);
		}

	}



/* &<@anonymous:1080> */
	obj_t BGl_z62zc3z04anonymousza31080ze3ze5zzexpand_lambdaz00(obj_t
		BgL_envz00_289, obj_t BgL_exprz00_291, obj_t BgL_expanderz00_292)
	{
		{	/* Expand/lambda.scm 78 */
			{	/* Expand/lambda.scm 78 */
				obj_t BgL_oldzd2expanderzd2_290;

				BgL_oldzd2expanderzd2_290 =
					((obj_t) PROCEDURE_REF(BgL_envz00_289, (int) (0L)));
				{
					obj_t BgL_restz00_334;

					if (PAIRP(BgL_exprz00_291))
						{	/* Expand/lambda.scm 78 */
							if ((CAR(((obj_t) BgL_exprz00_291)) == CNST_TABLE_REF(1)))
								{	/* Expand/lambda.scm 78 */
									if (NULLP(CDR(((obj_t) BgL_exprz00_291))))
										{	/* Expand/lambda.scm 78 */
											return BUNSPEC;
										}
									else
										{	/* Expand/lambda.scm 78 */
											obj_t BgL_arg1087z00_338;

											BgL_arg1087z00_338 = CDR(((obj_t) BgL_exprz00_291));
											BgL_restz00_334 = BgL_arg1087z00_338;
											if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
												(BgL_restz00_334))
												{	/* Expand/lambda.scm 86 */
													obj_t BgL_arg1092z00_335;
													obj_t BgL_arg1097z00_336;

													BgL_arg1092z00_335 =
														BGl_beginzd2bindingszd2zzexpand_lambdaz00
														(BgL_restz00_334);
													BgL_arg1097z00_336 =
														BGl_findzd2locationzd2zztools_locationz00
														(BgL_exprz00_291);
													{	/* Expand/lambda.scm 91 */
														obj_t BgL_zc3z04anonymousza31103ze3z87_337;

														BgL_zc3z04anonymousza31103ze3z87_337 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31103ze3ze5zzexpand_lambdaz00,
															(int) (0L), (int) (2L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31103ze3z87_337,
															(int) (0L), BgL_oldzd2expanderzd2_290);
														PROCEDURE_SET(BgL_zc3z04anonymousza31103ze3z87_337,
															(int) (1L), BgL_restz00_334);
														return
															BGl_withzd2lexicalzd2zzexpand_epsz00
															(BgL_arg1092z00_335, CNST_TABLE_REF(0),
															BgL_arg1097z00_336,
															BgL_zc3z04anonymousza31103ze3z87_337);
													}
												}
											else
												{	/* Expand/lambda.scm 83 */
													return
														BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
														BGl_string1224z00zzexpand_lambdaz00,
														BgL_exprz00_291);
												}
										}
								}
							else
								{	/* Expand/lambda.scm 78 */
								BgL_tagzd2380zd2_325:
									{	/* Expand/lambda.scm 93 */
										obj_t BgL_nexprz00_327;

										BgL_nexprz00_327 =
											BGL_PROCEDURE_CALL2(BgL_oldzd2expanderzd2_290,
											BgL_exprz00_291, BgL_oldzd2expanderzd2_290);
										{
											obj_t BgL_restz00_329;

											if (PAIRP(BgL_nexprz00_327))
												{	/* Expand/lambda.scm 94 */
													if (
														(CAR(
																((obj_t) BgL_nexprz00_327)) ==
															CNST_TABLE_REF(1)))
														{	/* Expand/lambda.scm 94 */
															if (NULLP(CDR(((obj_t) BgL_nexprz00_327))))
																{	/* Expand/lambda.scm 94 */
																	return BUNSPEC;
																}
															else
																{	/* Expand/lambda.scm 94 */
																	obj_t BgL_arg1127z00_333;

																	BgL_arg1127z00_333 =
																		CDR(((obj_t) BgL_nexprz00_327));
																	BgL_restz00_329 = BgL_arg1127z00_333;
																	if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_restz00_329))
																		{	/* Expand/lambda.scm 101 */
																			obj_t BgL_arg1137z00_330;
																			obj_t BgL_arg1138z00_331;

																			BgL_arg1137z00_330 =
																				BGl_beginzd2bindingszd2zzexpand_lambdaz00
																				(BgL_restz00_329);
																			BgL_arg1138z00_331 =
																				BGl_findzd2locationzd2zztools_locationz00
																				(BgL_exprz00_291);
																			{	/* Expand/lambda.scm 106 */
																				obj_t
																					BgL_zc3z04anonymousza31141ze3z87_332;
																				BgL_zc3z04anonymousza31141ze3z87_332 =
																					MAKE_FX_PROCEDURE
																					(BGl_z62zc3z04anonymousza31141ze3ze5zzexpand_lambdaz00,
																					(int) (0L), (int) (2L));
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31141ze3z87_332,
																					(int) (0L),
																					BgL_oldzd2expanderzd2_290);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31141ze3z87_332,
																					(int) (1L), BgL_restz00_329);
																				return
																					BGl_withzd2lexicalzd2zzexpand_epsz00
																					(BgL_arg1137z00_330,
																					CNST_TABLE_REF(0), BgL_arg1138z00_331,
																					BgL_zc3z04anonymousza31141ze3z87_332);
																			}
																		}
																	else
																		{	/* Expand/lambda.scm 98 */
																			return
																				BGl_errorz00zz__errorz00(CNST_TABLE_REF
																				(1),
																				BGl_string1224z00zzexpand_lambdaz00,
																				BgL_exprz00_291);
																		}
																}
														}
													else
														{	/* Expand/lambda.scm 94 */
															return BgL_nexprz00_327;
														}
												}
											else
												{	/* Expand/lambda.scm 94 */
													return BgL_nexprz00_327;
												}
										}
									}
								}
						}
					else
						{	/* Expand/lambda.scm 78 */
							goto BgL_tagzd2380zd2_325;
						}
				}
			}
		}

	}



/* &<@anonymous:1141> */
	obj_t BGl_z62zc3z04anonymousza31141ze3ze5zzexpand_lambdaz00(obj_t
		BgL_envz00_293)
	{
		{	/* Expand/lambda.scm 104 */
			{	/* Expand/lambda.scm 106 */
				obj_t BgL_oldzd2expanderzd2_294;
				obj_t BgL_restz00_295;

				BgL_oldzd2expanderzd2_294 =
					((obj_t) PROCEDURE_REF(BgL_envz00_293, (int) (0L)));
				BgL_restz00_295 = PROCEDURE_REF(BgL_envz00_293, (int) (1L));
				{	/* Expand/lambda.scm 106 */
					obj_t BgL_arg1142z00_339;

					{	/* Expand/lambda.scm 106 */
						obj_t BgL_zc3z04anonymousza31144ze3z87_340;

						BgL_zc3z04anonymousza31144ze3z87_340 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31144ze3ze5zzexpand_lambdaz00,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31144ze3z87_340, (int) (0L),
							BgL_oldzd2expanderzd2_294);
						BgL_arg1142z00_339 =
							BGl_emapz00zztools_prognz00(BgL_zc3z04anonymousza31144ze3z87_340,
							BgL_restz00_295);
					}
					return BGl_lambdazd2defineszd2zzexpand_lambdaz00(BgL_arg1142z00_339);
				}
			}
		}

	}



/* &<@anonymous:1144> */
	obj_t BGl_z62zc3z04anonymousza31144ze3ze5zzexpand_lambdaz00(obj_t
		BgL_envz00_296, obj_t BgL_xz00_298)
	{
		{	/* Expand/lambda.scm 106 */
			{	/* Expand/lambda.scm 106 */
				obj_t BgL_oldzd2expanderzd2_297;

				BgL_oldzd2expanderzd2_297 =
					((obj_t) PROCEDURE_REF(BgL_envz00_296, (int) (0L)));
				return
					BGL_PROCEDURE_CALL2(BgL_oldzd2expanderzd2_297, BgL_xz00_298,
					BgL_oldzd2expanderzd2_297);
			}
		}

	}



/* &<@anonymous:1103> */
	obj_t BGl_z62zc3z04anonymousza31103ze3ze5zzexpand_lambdaz00(obj_t
		BgL_envz00_299)
	{
		{	/* Expand/lambda.scm 89 */
			{	/* Expand/lambda.scm 91 */
				obj_t BgL_oldzd2expanderzd2_300;
				obj_t BgL_restz00_301;

				BgL_oldzd2expanderzd2_300 =
					((obj_t) PROCEDURE_REF(BgL_envz00_299, (int) (0L)));
				BgL_restz00_301 = PROCEDURE_REF(BgL_envz00_299, (int) (1L));
				{	/* Expand/lambda.scm 91 */
					obj_t BgL_arg1104z00_341;

					{	/* Expand/lambda.scm 91 */
						obj_t BgL_zc3z04anonymousza31115ze3z87_342;

						BgL_zc3z04anonymousza31115ze3z87_342 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31115ze3ze5zzexpand_lambdaz00,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31115ze3z87_342, (int) (0L),
							BgL_oldzd2expanderzd2_300);
						BgL_arg1104z00_341 =
							BGl_emapz00zztools_prognz00(BgL_zc3z04anonymousza31115ze3z87_342,
							BgL_restz00_301);
					}
					return BGl_lambdazd2defineszd2zzexpand_lambdaz00(BgL_arg1104z00_341);
				}
			}
		}

	}



/* &<@anonymous:1115> */
	obj_t BGl_z62zc3z04anonymousza31115ze3ze5zzexpand_lambdaz00(obj_t
		BgL_envz00_302, obj_t BgL_xz00_304)
	{
		{	/* Expand/lambda.scm 91 */
			{	/* Expand/lambda.scm 91 */
				obj_t BgL_oldzd2expanderzd2_303;

				BgL_oldzd2expanderzd2_303 =
					((obj_t) PROCEDURE_REF(BgL_envz00_302, (int) (0L)));
				return
					BGL_PROCEDURE_CALL2(BgL_oldzd2expanderzd2_303, BgL_xz00_304,
					BgL_oldzd2expanderzd2_303);
			}
		}

	}



/* begin-bindings */
	obj_t BGl_beginzd2bindingszd2zzexpand_lambdaz00(obj_t BgL_bodyz00_8)
	{
		{	/* Expand/lambda.scm 113 */
			{
				obj_t BgL_oldformsz00_136;
				obj_t BgL_varsz00_137;

				BgL_oldformsz00_136 = BgL_bodyz00_8;
				BgL_varsz00_137 = BNIL;
			BgL_zc3z04anonymousza31145ze3z87_138:
				if (PAIRP(BgL_oldformsz00_136))
					{	/* Expand/lambda.scm 119 */
						obj_t BgL_formz00_140;

						BgL_formz00_140 = CAR(BgL_oldformsz00_136);
						{
							obj_t BgL_varz00_143;
							obj_t BgL_varz00_141;

							if (PAIRP(BgL_formz00_140))
								{	/* Expand/lambda.scm 120 */
									obj_t BgL_cdrzd2417zd2_148;

									BgL_cdrzd2417zd2_148 = CDR(((obj_t) BgL_formz00_140));
									if ((CAR(((obj_t) BgL_formz00_140)) == CNST_TABLE_REF(2)))
										{	/* Expand/lambda.scm 120 */
											if (PAIRP(BgL_cdrzd2417zd2_148))
												{	/* Expand/lambda.scm 120 */
													obj_t BgL_carzd2419zd2_152;

													BgL_carzd2419zd2_152 = CAR(BgL_cdrzd2417zd2_148);
													if (PAIRP(BgL_carzd2419zd2_152))
														{	/* Expand/lambda.scm 120 */
															BgL_varz00_141 = CAR(BgL_carzd2419zd2_152);
															{	/* Expand/lambda.scm 122 */
																obj_t BgL_arg1158z00_158;
																obj_t BgL_arg1162z00_159;

																BgL_arg1158z00_158 =
																	CDR(((obj_t) BgL_oldformsz00_136));
																BgL_arg1162z00_159 =
																	MAKE_YOUNG_PAIR(BgL_varz00_141,
																	BgL_varsz00_137);
																{
																	obj_t BgL_varsz00_611;
																	obj_t BgL_oldformsz00_610;

																	BgL_oldformsz00_610 = BgL_arg1158z00_158;
																	BgL_varsz00_611 = BgL_arg1162z00_159;
																	BgL_varsz00_137 = BgL_varsz00_611;
																	BgL_oldformsz00_136 = BgL_oldformsz00_610;
																	goto BgL_zc3z04anonymousza31145ze3z87_138;
																}
															}
														}
													else
														{	/* Expand/lambda.scm 120 */
															obj_t BgL_arg1153z00_155;

															BgL_arg1153z00_155 =
																CAR(((obj_t) BgL_cdrzd2417zd2_148));
															BgL_varz00_143 = BgL_arg1153z00_155;
															{	/* Expand/lambda.scm 124 */
																obj_t BgL_arg1164z00_160;
																obj_t BgL_arg1166z00_161;

																BgL_arg1164z00_160 =
																	CDR(((obj_t) BgL_oldformsz00_136));
																BgL_arg1166z00_161 =
																	MAKE_YOUNG_PAIR(BgL_varz00_143,
																	BgL_varsz00_137);
																{
																	obj_t BgL_varsz00_619;
																	obj_t BgL_oldformsz00_618;

																	BgL_oldformsz00_618 = BgL_arg1164z00_160;
																	BgL_varsz00_619 = BgL_arg1166z00_161;
																	BgL_varsz00_137 = BgL_varsz00_619;
																	BgL_oldformsz00_136 = BgL_oldformsz00_618;
																	goto BgL_zc3z04anonymousza31145ze3z87_138;
																}
															}
														}
												}
											else
												{	/* Expand/lambda.scm 120 */
												BgL_tagzd2412zd2_145:
													{	/* Expand/lambda.scm 126 */
														obj_t BgL_arg1171z00_162;

														BgL_arg1171z00_162 =
															CDR(((obj_t) BgL_oldformsz00_136));
														{
															obj_t BgL_oldformsz00_622;

															BgL_oldformsz00_622 = BgL_arg1171z00_162;
															BgL_oldformsz00_136 = BgL_oldformsz00_622;
															goto BgL_zc3z04anonymousza31145ze3z87_138;
														}
													}
												}
										}
									else
										{	/* Expand/lambda.scm 120 */
											goto BgL_tagzd2412zd2_145;
										}
								}
							else
								{	/* Expand/lambda.scm 120 */
									goto BgL_tagzd2412zd2_145;
								}
						}
					}
				else
					{	/* Expand/lambda.scm 118 */
						return BgL_varsz00_137;
					}
			}
		}

	}



/* lambda-defines */
	obj_t BGl_lambdazd2defineszd2zzexpand_lambdaz00(obj_t BgL_bodyz00_9)
	{
		{	/* Expand/lambda.scm 132 */
			{
				obj_t BgL_oldformsz00_168;
				obj_t BgL_newformsz00_169;
				obj_t BgL_varsz00_170;
				obj_t BgL_declsz00_171;

				BgL_oldformsz00_168 = BgL_bodyz00_9;
				BgL_newformsz00_169 = BNIL;
				BgL_varsz00_170 = BNIL;
				BgL_declsz00_171 = BNIL;
			BgL_zc3z04anonymousza31172ze3z87_172:
				if (PAIRP(BgL_oldformsz00_168))
					{	/* Expand/lambda.scm 138 */
						obj_t BgL_formz00_174;

						BgL_formz00_174 = CAR(BgL_oldformsz00_168);
						{
							obj_t BgL_bodyz00_178;
							obj_t BgL_varz00_175;
							obj_t BgL_valz00_176;

							if (PAIRP(BgL_formz00_174))
								{	/* Expand/lambda.scm 139 */
									obj_t BgL_cdrzd2449zd2_183;

									BgL_cdrzd2449zd2_183 = CDR(((obj_t) BgL_formz00_174));
									if ((CAR(((obj_t) BgL_formz00_174)) == CNST_TABLE_REF(2)))
										{	/* Expand/lambda.scm 139 */
											if (PAIRP(BgL_cdrzd2449zd2_183))
												{	/* Expand/lambda.scm 139 */
													obj_t BgL_cdrzd2453zd2_187;

													BgL_cdrzd2453zd2_187 = CDR(BgL_cdrzd2449zd2_183);
													if (PAIRP(BgL_cdrzd2453zd2_187))
														{	/* Expand/lambda.scm 139 */
															if (NULLP(CDR(BgL_cdrzd2453zd2_187)))
																{	/* Expand/lambda.scm 139 */
																	BgL_varz00_175 = CAR(BgL_cdrzd2449zd2_183);
																	BgL_valz00_176 = CAR(BgL_cdrzd2453zd2_187);
																	{	/* Expand/lambda.scm 141 */
																		obj_t BgL_arg1199z00_199;
																		obj_t BgL_arg1200z00_200;
																		obj_t BgL_arg1201z00_201;

																		BgL_arg1199z00_199 =
																			CDR(((obj_t) BgL_oldformsz00_168));
																		BgL_arg1200z00_200 =
																			MAKE_YOUNG_PAIR(BgL_varz00_175,
																			BgL_varsz00_170);
																		{	/* Expand/lambda.scm 144 */
																			obj_t BgL_arg1202z00_202;

																			{	/* Expand/lambda.scm 144 */
																				obj_t BgL_arg1203z00_203;

																				BgL_arg1203z00_203 =
																					MAKE_YOUNG_PAIR(BgL_valz00_176, BNIL);
																				BgL_arg1202z00_202 =
																					MAKE_YOUNG_PAIR(BgL_varz00_175,
																					BgL_arg1203z00_203);
																			}
																			BgL_arg1201z00_201 =
																				MAKE_YOUNG_PAIR(BgL_arg1202z00_202,
																				BgL_declsz00_171);
																		}
																		{
																			obj_t BgL_declsz00_651;
																			obj_t BgL_varsz00_650;
																			obj_t BgL_oldformsz00_649;

																			BgL_oldformsz00_649 = BgL_arg1199z00_199;
																			BgL_varsz00_650 = BgL_arg1200z00_200;
																			BgL_declsz00_651 = BgL_arg1201z00_201;
																			BgL_declsz00_171 = BgL_declsz00_651;
																			BgL_varsz00_170 = BgL_varsz00_650;
																			BgL_oldformsz00_168 = BgL_oldformsz00_649;
																			goto BgL_zc3z04anonymousza31172ze3z87_172;
																		}
																	}
																}
															else
																{	/* Expand/lambda.scm 139 */
																BgL_tagzd2441zd2_180:
																	{	/* Expand/lambda.scm 151 */
																		obj_t BgL_arg1209z00_206;
																		obj_t BgL_arg1210z00_207;

																		BgL_arg1209z00_206 =
																			CDR(((obj_t) BgL_oldformsz00_168));
																		BgL_arg1210z00_207 =
																			MAKE_YOUNG_PAIR(BgL_formz00_174,
																			BgL_newformsz00_169);
																		{
																			obj_t BgL_newformsz00_658;
																			obj_t BgL_oldformsz00_657;

																			BgL_oldformsz00_657 = BgL_arg1209z00_206;
																			BgL_newformsz00_658 = BgL_arg1210z00_207;
																			BgL_newformsz00_169 = BgL_newformsz00_658;
																			BgL_oldformsz00_168 = BgL_oldformsz00_657;
																			goto BgL_zc3z04anonymousza31172ze3z87_172;
																		}
																	}
																}
														}
													else
														{	/* Expand/lambda.scm 139 */
															goto BgL_tagzd2441zd2_180;
														}
												}
											else
												{	/* Expand/lambda.scm 139 */
													goto BgL_tagzd2441zd2_180;
												}
										}
									else
										{	/* Expand/lambda.scm 139 */
											if ((CAR(((obj_t) BgL_formz00_174)) == CNST_TABLE_REF(1)))
												{	/* Expand/lambda.scm 139 */
													BgL_bodyz00_178 = BgL_cdrzd2449zd2_183;
													{	/* Expand/lambda.scm 146 */
														obj_t BgL_arg1206z00_204;

														{	/* Expand/lambda.scm 146 */
															obj_t BgL_arg1208z00_205;

															BgL_arg1208z00_205 =
																CDR(((obj_t) BgL_oldformsz00_168));
															BgL_arg1206z00_204 =
																BGl_appendzd221011zd2zzexpand_lambdaz00
																(BgL_bodyz00_178, BgL_arg1208z00_205);
														}
														{
															obj_t BgL_oldformsz00_667;

															BgL_oldformsz00_667 = BgL_arg1206z00_204;
															BgL_oldformsz00_168 = BgL_oldformsz00_667;
															goto BgL_zc3z04anonymousza31172ze3z87_172;
														}
													}
												}
											else
												{	/* Expand/lambda.scm 139 */
													goto BgL_tagzd2441zd2_180;
												}
										}
								}
							else
								{	/* Expand/lambda.scm 139 */
									goto BgL_tagzd2441zd2_180;
								}
						}
					}
				else
					{	/* Expand/lambda.scm 137 */
						if (NULLP(BgL_varsz00_170))
							{	/* Expand/lambda.scm 156 */
								BGL_TAIL return
									BGl_expandzd2prognzd2zz__prognz00(BgL_bodyz00_9);
							}
						else
							{	/* Expand/lambda.scm 157 */
								obj_t BgL_arg1212z00_209;

								{	/* Expand/lambda.scm 157 */
									obj_t BgL_arg1215z00_210;
									obj_t BgL_arg1216z00_211;

									BgL_arg1215z00_210 = bgl_reverse_bang(BgL_declsz00_171);
									BgL_arg1216z00_211 =
										MAKE_YOUNG_PAIR(BGl_expandzd2prognzd2zz__prognz00
										(bgl_reverse(BgL_newformsz00_169)), BNIL);
									BgL_arg1212z00_209 =
										MAKE_YOUNG_PAIR(BgL_arg1215z00_210, BgL_arg1216z00_211);
								}
								return MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1212z00_209);
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_lambdaz00(void)
	{
		{	/* Expand/lambda.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_lambdaz00(void)
	{
		{	/* Expand/lambda.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_lambdaz00(void)
	{
		{	/* Expand/lambda.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_lambdaz00(void)
	{
		{	/* Expand/lambda.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1225z00zzexpand_lambdaz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1225z00zzexpand_lambdaz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string1225z00zzexpand_lambdaz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1225z00zzexpand_lambdaz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1225z00zzexpand_lambdaz00));
			BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string1225z00zzexpand_lambdaz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1225z00zzexpand_lambdaz00));
		}

	}

#ifdef __cplusplus
}
#endif
