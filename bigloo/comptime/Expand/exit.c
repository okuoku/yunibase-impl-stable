/*===========================================================================*/
/*   (Expand/exit.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/exit.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_EXIT_TYPE_DEFINITIONS
#define BGL_EXPAND_EXIT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_EXPAND_EXIT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_exitz00 = BUNSPEC;
	static bool_t BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzexpand_exitz00(void);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_exitz00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2setzd2exitz00zzexpand_exitz00(obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2setzd2exitz62zzexpand_exitz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_exitz00(void);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2bindzd2exitz00zzexpand_exitz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_exitz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2unwindzd2protectz00zzexpand_exitz00(obj_t, obj_t);
	extern obj_t BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00(obj_t);
	static obj_t BGl_z62expandzd2withzd2handlerz62zzexpand_exitz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_bignum_sub(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static bool_t BGl_tailreczf3ze70z14zzexpand_exitz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_addzd2traceze70z35zzexpand_exitz00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2unwindzd2protectz62zzexpand_exitz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
	static obj_t BGl_z62expandzd2bindzd2exitz62zzexpand_exitz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_replacez12z12zztools_miscz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_takez00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzexpand_exitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_expanderz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2withzd2handlerz00zzexpand_exitz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2jumpzd2exitz00zzexpand_exitz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_exitz00(void);
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_exitz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_exitz00(void);
	static obj_t BGl_returnz12ze70zf5zzexpand_exitz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_exitz00(void);
	BGL_IMPORT int BGl_bigloozd2compilerzd2debugz00zz__paramz00(void);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static obj_t BGl_z62expandzd2jumpzd2exitz62zzexpand_exitz00(obj_t, obj_t,
		obj_t);
	static bool_t BGl_tracezf3ze70z14zzexpand_exitz00(void);
	static bool_t BGl_tracezf3ze71z14zzexpand_exitz00(void);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t __cnst[54];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2withzd2handlerzd2envzd2zzexpand_exitz00,
		BgL_bgl_za762expandza7d2with2601z00,
		BGl_z62expandzd2withzd2handlerz62zzexpand_exitz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2setzd2exitzd2envzd2zzexpand_exitz00,
		BgL_bgl_za762expandza7d2setza72602za7,
		BGl_z62expandzd2setzd2exitz62zzexpand_exitz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2unwindzd2protectzd2envzd2zzexpand_exitz00,
		BgL_bgl_za762expandza7d2unwi2603z00,
		BGl_z62expandzd2unwindzd2protectz62zzexpand_exitz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2bindzd2exitzd2envzd2zzexpand_exitz00,
		BgL_bgl_za762expandza7d2bind2604z00,
		BGl_z62expandzd2bindzd2exitz62zzexpand_exitz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2593z00zzexpand_exitz00,
		BgL_bgl_string2593za700za7za7e2605za7, "Illegal 'jump-exit' form", 24);
	      DEFINE_STRING(BGl_string2594z00zzexpand_exitz00,
		BgL_bgl_string2594za700za7za7e2606za7, "Illegal `set-exit' form", 23);
	      DEFINE_STRING(BGl_string2595z00zzexpand_exitz00,
		BgL_bgl_string2595za700za7za7e2607za7, "Illegal `bind-exit' form", 24);
	      DEFINE_STRING(BGl_string2596z00zzexpand_exitz00,
		BgL_bgl_string2596za700za7za7e2608za7, "Illegal `unwind-protect' form", 29);
	      DEFINE_STRING(BGl_string2597z00zzexpand_exitz00,
		BgL_bgl_string2597za700za7za7e2609za7, "Illegal `with-handler' form", 27);
	      DEFINE_STRING(BGl_string2598z00zzexpand_exitz00,
		BgL_bgl_string2598za700za7za7e2610za7, "expand_exit", 11);
	      DEFINE_STRING(BGl_string2599z00zzexpand_exitz00,
		BgL_bgl_string2599za700za7za7e2611za7,
		"cell-ref sigsetmask eq? unwind-protect $acons $env-get-error-handler $make-stack-cell errhandler handler escape cell err hds ohs exitd-push-protect! lambda protect $set-error-handler! $get-exitd-top $env-set-error-handler! exitd-pop-protect! exitd-protect-set! cons $exitd-protect exitd bind-exit if quote $set-trace-stacksp $get-trace-stacksp tmp :env letrec* letrec let* labels @ unwind-stack-until! __bexit $current-dynamic-env env begin let $env-pop-exit! $env-get-exitd-top $env-push-exit! tracesp res val an_exitd an_exit set-exit :onexit jump-exit ",
		555);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2jumpzd2exitzd2envzd2zzexpand_exitz00,
		BgL_bgl_za762expandza7d2jump2612z00,
		BGl_z62expandzd2jumpzd2exitz62zzexpand_exitz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzexpand_exitz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzexpand_exitz00(long
		BgL_checksumz00_2182, char *BgL_fromz00_2183)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_exitz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_exitz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_exitz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_exitz00();
					BGl_cnstzd2initzd2zzexpand_exitz00();
					BGl_importedzd2moduleszd2initz00zzexpand_exitz00();
					return BGl_toplevelzd2initzd2zzexpand_exitz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_exit");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_exit");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "expand_exit");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_exit");
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "expand_exit");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "expand_exit");
			BGl_modulezd2initializa7ationz75zz__prognz00(0L, "expand_exit");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "expand_exit");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"expand_exit");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_exit");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "expand_exit");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 15 */
			{	/* Expand/exit.scm 15 */
				obj_t BgL_cportz00_2171;

				{	/* Expand/exit.scm 15 */
					obj_t BgL_stringz00_2178;

					BgL_stringz00_2178 = BGl_string2599z00zzexpand_exitz00;
					{	/* Expand/exit.scm 15 */
						obj_t BgL_startz00_2179;

						BgL_startz00_2179 = BINT(0L);
						{	/* Expand/exit.scm 15 */
							obj_t BgL_endz00_2180;

							BgL_endz00_2180 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2178)));
							{	/* Expand/exit.scm 15 */

								BgL_cportz00_2171 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2178, BgL_startz00_2179, BgL_endz00_2180);
				}}}}
				{
					long BgL_iz00_2172;

					BgL_iz00_2172 = 53L;
				BgL_loopz00_2173:
					if ((BgL_iz00_2172 == -1L))
						{	/* Expand/exit.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/exit.scm 15 */
							{	/* Expand/exit.scm 15 */
								obj_t BgL_arg2600z00_2174;

								{	/* Expand/exit.scm 15 */

									{	/* Expand/exit.scm 15 */
										obj_t BgL_locationz00_2176;

										BgL_locationz00_2176 = BBOOL(((bool_t) 0));
										{	/* Expand/exit.scm 15 */

											BgL_arg2600z00_2174 =
												BGl_readz00zz__readerz00(BgL_cportz00_2171,
												BgL_locationz00_2176);
										}
									}
								}
								{	/* Expand/exit.scm 15 */
									int BgL_tmpz00_2212;

									BgL_tmpz00_2212 = (int) (BgL_iz00_2172);
									CNST_TABLE_SET(BgL_tmpz00_2212, BgL_arg2600z00_2174);
							}}
							{	/* Expand/exit.scm 15 */
								int BgL_auxz00_2177;

								BgL_auxz00_2177 = (int) ((BgL_iz00_2172 - 1L));
								{
									long BgL_iz00_2217;

									BgL_iz00_2217 = (long) (BgL_auxz00_2177);
									BgL_iz00_2172 = BgL_iz00_2217;
									goto BgL_loopz00_2173;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 15 */
			return BUNSPEC;
		}

	}



/* expand-jump-exit */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2jumpzd2exitz00zzexpand_exitz00(obj_t
		BgL_xz00_27, obj_t BgL_ez00_28)
	{
		{	/* Expand/exit.scm 39 */
			{
				obj_t BgL_exitz00_224;
				obj_t BgL_valuez00_225;

				if (PAIRP(BgL_xz00_27))
					{	/* Expand/exit.scm 40 */
						obj_t BgL_cdrzd2367zd2_230;

						BgL_cdrzd2367zd2_230 = CDR(((obj_t) BgL_xz00_27));
						if (PAIRP(BgL_cdrzd2367zd2_230))
							{	/* Expand/exit.scm 40 */
								BgL_exitz00_224 = CAR(BgL_cdrzd2367zd2_230);
								BgL_valuez00_225 = CDR(BgL_cdrzd2367zd2_230);
								{	/* Expand/exit.scm 42 */
									obj_t BgL_newz00_234;

									{	/* Expand/exit.scm 42 */
										obj_t BgL_arg1132z00_235;

										{	/* Expand/exit.scm 42 */
											obj_t BgL_arg1137z00_236;
											obj_t BgL_arg1138z00_237;

											BgL_arg1137z00_236 =
												BGL_PROCEDURE_CALL2(BgL_ez00_28, BgL_exitz00_224,
												BgL_ez00_28);
											{	/* Expand/exit.scm 42 */
												obj_t BgL_arg1140z00_238;

												{	/* Expand/exit.scm 42 */
													obj_t BgL_arg1141z00_239;

													BgL_arg1141z00_239 =
														BGl_expandzd2prognzd2zz__prognz00(BgL_valuez00_225);
													BgL_arg1140z00_238 =
														BGL_PROCEDURE_CALL2(BgL_ez00_28, BgL_arg1141z00_239,
														BgL_ez00_28);
												}
												BgL_arg1138z00_237 =
													MAKE_YOUNG_PAIR(BgL_arg1140z00_238, BNIL);
											}
											BgL_arg1132z00_235 =
												MAKE_YOUNG_PAIR(BgL_arg1137z00_236, BgL_arg1138z00_237);
										}
										BgL_newz00_234 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1132z00_235);
									}
									return
										BGl_replacez12z12zztools_miscz00(BgL_xz00_27,
										BgL_newz00_234);
								}
							}
						else
							{	/* Expand/exit.scm 40 */
							BgL_tagzd2360zd2_227:
								return
									BGl_errorz00zz__errorz00(BFALSE,
									BGl_string2593z00zzexpand_exitz00, BgL_xz00_27);
							}
					}
				else
					{	/* Expand/exit.scm 40 */
						goto BgL_tagzd2360zd2_227;
					}
			}
		}

	}



/* &expand-jump-exit */
	obj_t BGl_z62expandzd2jumpzd2exitz62zzexpand_exitz00(obj_t BgL_envz00_2154,
		obj_t BgL_xz00_2155, obj_t BgL_ez00_2156)
	{
		{	/* Expand/exit.scm 39 */
			return
				BGl_expandzd2jumpzd2exitz00zzexpand_exitz00(BgL_xz00_2155,
				BgL_ez00_2156);
		}

	}



/* expand-set-exit */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2setzd2exitz00zzexpand_exitz00(obj_t
		BgL_xz00_29, obj_t BgL_ez00_30)
	{
		{	/* Expand/exit.scm 50 */
			{
				obj_t BgL_exitz00_244;
				obj_t BgL_bodyz00_245;
				obj_t BgL_exitz00_240;
				obj_t BgL_bodyz00_241;
				obj_t BgL_onexitz00_242;

				if (PAIRP(BgL_xz00_29))
					{	/* Expand/exit.scm 51 */
						obj_t BgL_cdrzd2385zd2_250;

						BgL_cdrzd2385zd2_250 = CDR(((obj_t) BgL_xz00_29));
						if (PAIRP(BgL_cdrzd2385zd2_250))
							{	/* Expand/exit.scm 51 */
								obj_t BgL_carzd2389zd2_252;
								obj_t BgL_cdrzd2390zd2_253;

								BgL_carzd2389zd2_252 = CAR(BgL_cdrzd2385zd2_250);
								BgL_cdrzd2390zd2_253 = CDR(BgL_cdrzd2385zd2_250);
								if (PAIRP(BgL_carzd2389zd2_252))
									{	/* Expand/exit.scm 51 */
										if (NULLP(CDR(BgL_carzd2389zd2_252)))
											{	/* Expand/exit.scm 51 */
												if (PAIRP(BgL_cdrzd2390zd2_253))
													{	/* Expand/exit.scm 51 */
														obj_t BgL_cdrzd2398zd2_258;

														BgL_cdrzd2398zd2_258 = CDR(BgL_cdrzd2390zd2_253);
														if (PAIRP(BgL_cdrzd2398zd2_258))
															{	/* Expand/exit.scm 51 */
																obj_t BgL_cdrzd2402zd2_260;

																BgL_cdrzd2402zd2_260 =
																	CDR(BgL_cdrzd2398zd2_258);
																if (
																	(CAR(BgL_cdrzd2398zd2_258) ==
																		CNST_TABLE_REF(1)))
																	{	/* Expand/exit.scm 51 */
																		if (PAIRP(BgL_cdrzd2402zd2_260))
																			{	/* Expand/exit.scm 51 */
																				if (NULLP(CDR(BgL_cdrzd2402zd2_260)))
																					{	/* Expand/exit.scm 51 */
																						BgL_exitz00_240 =
																							CAR(BgL_carzd2389zd2_252);
																						BgL_bodyz00_241 =
																							CAR(BgL_cdrzd2390zd2_253);
																						BgL_onexitz00_242 =
																							CAR(BgL_cdrzd2402zd2_260);
																						{	/* Expand/exit.scm 53 */
																							obj_t BgL_newz00_280;

																							{	/* Expand/exit.scm 53 */
																								obj_t BgL_arg1188z00_281;

																								{	/* Expand/exit.scm 53 */
																									obj_t BgL_arg1189z00_282;
																									obj_t BgL_arg1190z00_283;

																									BgL_arg1189z00_282 =
																										MAKE_YOUNG_PAIR
																										(BgL_exitz00_240, BNIL);
																									{	/* Expand/exit.scm 53 */
																										obj_t BgL_arg1191z00_284;
																										obj_t BgL_arg1193z00_285;

																										BgL_arg1191z00_284 =
																											BGL_PROCEDURE_CALL2
																											(BgL_ez00_30,
																											BgL_bodyz00_241,
																											BgL_ez00_30);
																										{	/* Expand/exit.scm 53 */
																											obj_t BgL_arg1194z00_286;

																											{	/* Expand/exit.scm 53 */
																												obj_t
																													BgL_arg1196z00_287;
																												BgL_arg1196z00_287 =
																													BGL_PROCEDURE_CALL2
																													(BgL_ez00_30,
																													BgL_onexitz00_242,
																													BgL_ez00_30);
																												BgL_arg1194z00_286 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1196z00_287,
																													BNIL);
																											}
																											BgL_arg1193z00_285 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(1),
																												BgL_arg1194z00_286);
																										}
																										BgL_arg1190z00_283 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1191z00_284,
																											BgL_arg1193z00_285);
																									}
																									BgL_arg1188z00_281 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1189z00_282,
																										BgL_arg1190z00_283);
																								}
																								BgL_newz00_280 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(2), BgL_arg1188z00_281);
																							}
																							return
																								BGl_replacez12z12zztools_miscz00
																								(BgL_xz00_29, BgL_newz00_280);
																						}
																					}
																				else
																					{	/* Expand/exit.scm 51 */
																					BgL_tagzd2376zd2_247:
																						return
																							BGl_errorz00zz__errorz00(BFALSE,
																							BGl_string2594z00zzexpand_exitz00,
																							BgL_xz00_29);
																					}
																			}
																		else
																			{	/* Expand/exit.scm 51 */
																				goto BgL_tagzd2376zd2_247;
																			}
																	}
																else
																	{	/* Expand/exit.scm 51 */
																		goto BgL_tagzd2376zd2_247;
																	}
															}
														else
															{	/* Expand/exit.scm 51 */
																obj_t BgL_cdrzd2438zd2_272;

																BgL_cdrzd2438zd2_272 =
																	CDR(((obj_t) BgL_cdrzd2385zd2_250));
																if (NULLP(CDR(((obj_t) BgL_cdrzd2438zd2_272))))
																	{	/* Expand/exit.scm 51 */
																		obj_t BgL_arg1171z00_275;
																		obj_t BgL_arg1172z00_276;

																		{	/* Expand/exit.scm 51 */
																			obj_t BgL_pairz00_1797;

																			BgL_pairz00_1797 =
																				CAR(((obj_t) BgL_cdrzd2385zd2_250));
																			BgL_arg1171z00_275 =
																				CAR(BgL_pairz00_1797);
																		}
																		BgL_arg1172z00_276 =
																			CAR(((obj_t) BgL_cdrzd2438zd2_272));
																		BgL_exitz00_244 = BgL_arg1171z00_275;
																		BgL_bodyz00_245 = BgL_arg1172z00_276;
																		{	/* Expand/exit.scm 56 */
																			obj_t BgL_newz00_288;

																			{	/* Expand/exit.scm 56 */
																				obj_t BgL_arg1197z00_289;

																				{	/* Expand/exit.scm 56 */
																					obj_t BgL_arg1198z00_290;
																					obj_t BgL_arg1199z00_291;

																					BgL_arg1198z00_290 =
																						MAKE_YOUNG_PAIR(BgL_exitz00_244,
																						BNIL);
																					{	/* Expand/exit.scm 56 */
																						obj_t BgL_arg1200z00_292;

																						BgL_arg1200z00_292 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_30,
																							BgL_bodyz00_245, BgL_ez00_30);
																						BgL_arg1199z00_291 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1200z00_292, BNIL);
																					}
																					BgL_arg1197z00_289 =
																						MAKE_YOUNG_PAIR(BgL_arg1198z00_290,
																						BgL_arg1199z00_291);
																				}
																				BgL_newz00_288 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																					BgL_arg1197z00_289);
																			}
																			return
																				BGl_replacez12z12zztools_miscz00
																				(BgL_xz00_29, BgL_newz00_288);
																		}
																	}
																else
																	{	/* Expand/exit.scm 51 */
																		goto BgL_tagzd2376zd2_247;
																	}
															}
													}
												else
													{	/* Expand/exit.scm 51 */
														goto BgL_tagzd2376zd2_247;
													}
											}
										else
											{	/* Expand/exit.scm 51 */
												goto BgL_tagzd2376zd2_247;
											}
									}
								else
									{	/* Expand/exit.scm 51 */
										goto BgL_tagzd2376zd2_247;
									}
							}
						else
							{	/* Expand/exit.scm 51 */
								goto BgL_tagzd2376zd2_247;
							}
					}
				else
					{	/* Expand/exit.scm 51 */
						goto BgL_tagzd2376zd2_247;
					}
			}
		}

	}



/* &expand-set-exit */
	obj_t BGl_z62expandzd2setzd2exitz62zzexpand_exitz00(obj_t BgL_envz00_2157,
		obj_t BgL_xz00_2158, obj_t BgL_ez00_2159)
	{
		{	/* Expand/exit.scm 50 */
			return
				BGl_expandzd2setzd2exitz00zzexpand_exitz00(BgL_xz00_2158,
				BgL_ez00_2159);
		}

	}



/* expand-bind-exit */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2bindzd2exitz00zzexpand_exitz00(obj_t
		BgL_xz00_31, obj_t BgL_ez00_32)
	{
		{	/* Expand/exit.scm 64 */
			{
				obj_t BgL_exitz00_1216;
				obj_t BgL_bodyz00_1217;
				obj_t BgL_envz00_1286;
				obj_t BgL_exitz00_1287;
				obj_t BgL_bodyz00_1288;
				obj_t BgL_envz00_1329;
				obj_t BgL_exitz00_1330;
				obj_t BgL_bodyz00_1331;
				obj_t BgL_onexitz00_1332;

				{
					obj_t BgL_exitz00_305;
					obj_t BgL_exprsz00_306;
					obj_t BgL_envz00_308;
					obj_t BgL_exitz00_309;
					obj_t BgL_bodyz00_310;
					obj_t BgL_envz00_312;
					obj_t BgL_exitz00_313;
					obj_t BgL_bodyz00_314;
					obj_t BgL_onexitz00_315;

					if (PAIRP(BgL_xz00_31))
						{	/* Expand/exit.scm 197 */
							obj_t BgL_cdrzd21278zd2_320;

							BgL_cdrzd21278zd2_320 = CDR(((obj_t) BgL_xz00_31));
							if (PAIRP(BgL_cdrzd21278zd2_320))
								{	/* Expand/exit.scm 197 */
									obj_t BgL_carzd21281zd2_322;
									obj_t BgL_cdrzd21282zd2_323;

									BgL_carzd21281zd2_322 = CAR(BgL_cdrzd21278zd2_320);
									BgL_cdrzd21282zd2_323 = CDR(BgL_cdrzd21278zd2_320);
									if (PAIRP(BgL_carzd21281zd2_322))
										{	/* Expand/exit.scm 197 */
											obj_t BgL_exitz00_325;

											BgL_exitz00_325 = CAR(BgL_carzd21281zd2_322);
											if (NULLP(CDR(BgL_carzd21281zd2_322)))
												{	/* Expand/exit.scm 197 */
													if (PAIRP(BgL_cdrzd21282zd2_323))
														{	/* Expand/exit.scm 197 */
															obj_t BgL_carzd21289zd2_329;

															BgL_carzd21289zd2_329 =
																CAR(BgL_cdrzd21282zd2_323);
															if (PAIRP(BgL_carzd21289zd2_329))
																{	/* Expand/exit.scm 197 */
																	obj_t BgL_cdrzd21294zd2_331;

																	BgL_cdrzd21294zd2_331 =
																		CDR(BgL_carzd21289zd2_329);
																	if (
																		(BgL_exitz00_325 ==
																			CAR(BgL_carzd21289zd2_329)))
																		{	/* Expand/exit.scm 197 */
																			if (PAIRP(BgL_cdrzd21294zd2_331))
																				{	/* Expand/exit.scm 197 */
																					if (NULLP(CDR(BgL_cdrzd21294zd2_331)))
																						{	/* Expand/exit.scm 197 */
																							if (NULLP(CDR
																									(BgL_cdrzd21282zd2_323)))
																								{	/* Expand/exit.scm 197 */
																									obj_t BgL_arg1220z00_339;

																									BgL_arg1220z00_339 =
																										CAR(BgL_cdrzd21294zd2_331);
																									return
																										BGL_PROCEDURE_CALL2
																										(BgL_ez00_32,
																										BgL_arg1220z00_339,
																										BgL_ez00_32);
																								}
																							else
																								{	/* Expand/exit.scm 197 */
																									obj_t BgL_arg1221z00_341;
																									obj_t BgL_arg1223z00_342;

																									{	/* Expand/exit.scm 197 */
																										obj_t BgL_pairz00_2044;

																										BgL_pairz00_2044 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd21278zd2_320));
																										BgL_arg1221z00_341 =
																											CAR(BgL_pairz00_2044);
																									}
																									BgL_arg1223z00_342 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd21278zd2_320));
																									BgL_exitz00_305 =
																										BgL_arg1221z00_341;
																									BgL_exprsz00_306 =
																										BgL_arg1223z00_342;
																								BgL_tagzd21264zd2_307:
																									{	/* Expand/exit.scm 202 */
																										obj_t
																											BgL_oldzd2internalzd2_403;
																										BgL_oldzd2internalzd2_403 =
																											BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
																										BGl_internalzd2definitionzf3z21zzexpand_lambdaz00
																											= BTRUE;
																										{	/* Expand/exit.scm 204 */
																											obj_t BgL_ez00_404;

																											BgL_ez00_404 =
																												BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00
																												(BgL_ez00_32);
																											{	/* Expand/exit.scm 204 */
																												obj_t BgL_nexprsz00_405;

																												{	/* Expand/exit.scm 205 */
																													obj_t
																														BgL_arg1329z00_408;
																													{	/* Expand/exit.scm 205 */
																														obj_t
																															BgL_arg1331z00_409;
																														BgL_arg1331z00_409 =
																															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																															(BgL_exprsz00_306,
																															BNIL);
																														BgL_arg1329z00_408 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(12),
																															BgL_arg1331z00_409);
																													}
																													BgL_nexprsz00_405 =
																														BGL_PROCEDURE_CALL2
																														(BgL_ez00_404,
																														BgL_arg1329z00_408,
																														BgL_ez00_404);
																												}
																												{	/* Expand/exit.scm 205 */

																													{	/* Expand/exit.scm 206 */
																														obj_t
																															BgL_arg1327z00_406;
																														{	/* Expand/exit.scm 206 */
																															obj_t
																																BgL_pairz00_1941;
																															BgL_pairz00_1941 =
																																CDR(((obj_t)
																																	BgL_xz00_31));
																															BgL_arg1327z00_406
																																=
																																CDR
																																(BgL_pairz00_1941);
																														}
																														{	/* Expand/exit.scm 206 */
																															obj_t
																																BgL_tmpz00_2374;
																															BgL_tmpz00_2374 =
																																((obj_t)
																																BgL_arg1327z00_406);
																															SET_CAR
																																(BgL_tmpz00_2374,
																																BgL_nexprsz00_405);
																														}
																													}
																													{	/* Expand/exit.scm 207 */
																														obj_t
																															BgL_arg1328z00_407;
																														{	/* Expand/exit.scm 207 */
																															obj_t
																																BgL_pairz00_1946;
																															BgL_pairz00_1946 =
																																CDR(((obj_t)
																																	BgL_xz00_31));
																															BgL_arg1328z00_407
																																=
																																CDR
																																(BgL_pairz00_1946);
																														}
																														{	/* Expand/exit.scm 207 */
																															obj_t
																																BgL_tmpz00_2380;
																															BgL_tmpz00_2380 =
																																((obj_t)
																																BgL_arg1328z00_407);
																															SET_CDR
																																(BgL_tmpz00_2380,
																																BNIL);
																														}
																													}
																												}
																											}
																										}
																										BGl_internalzd2definitionzf3z21zzexpand_lambdaz00
																											=
																											BgL_oldzd2internalzd2_403;
																									}
																									{
																										obj_t BgL_letkz00_410;
																										obj_t BgL_bindingsz00_411;
																										obj_t BgL_bodyz00_412;
																										obj_t BgL_exitz00_413;
																										obj_t BgL_exprz00_414;
																										obj_t BgL_exitz00_416;
																										obj_t BgL_bodyz00_417;

																										if (PAIRP(BgL_xz00_31))
																											{	/* Expand/exit.scm 209 */
																												obj_t
																													BgL_cdrzd21664zd2_422;
																												BgL_cdrzd21664zd2_422 =
																													CDR(((obj_t)
																														BgL_xz00_31));
																												if (PAIRP
																													(BgL_cdrzd21664zd2_422))
																													{	/* Expand/exit.scm 209 */
																														obj_t
																															BgL_carzd21670zd2_424;
																														obj_t
																															BgL_cdrzd21671zd2_425;
																														BgL_carzd21670zd2_424
																															=
																															CAR
																															(BgL_cdrzd21664zd2_422);
																														BgL_cdrzd21671zd2_425
																															=
																															CDR
																															(BgL_cdrzd21664zd2_422);
																														if (PAIRP
																															(BgL_carzd21670zd2_424))
																															{	/* Expand/exit.scm 209 */
																																obj_t
																																	BgL_exitz00_427;
																																BgL_exitz00_427
																																	=
																																	CAR
																																	(BgL_carzd21670zd2_424);
																																if (NULLP(CDR
																																		(BgL_carzd21670zd2_424)))
																																	{	/* Expand/exit.scm 209 */
																																		if (PAIRP
																																			(BgL_cdrzd21671zd2_425))
																																			{	/* Expand/exit.scm 209 */
																																				obj_t
																																					BgL_carzd21681zd2_431;
																																				BgL_carzd21681zd2_431
																																					=
																																					CAR
																																					(BgL_cdrzd21671zd2_425);
																																				if (PAIRP(BgL_carzd21681zd2_431))
																																					{	/* Expand/exit.scm 209 */
																																						obj_t
																																							BgL_carzd21688zd2_433;
																																						obj_t
																																							BgL_cdrzd21689zd2_434;
																																						BgL_carzd21688zd2_433
																																							=
																																							CAR
																																							(BgL_carzd21681zd2_431);
																																						BgL_cdrzd21689zd2_434
																																							=
																																							CDR
																																							(BgL_carzd21681zd2_431);
																																						{

																																							if ((BgL_carzd21688zd2_433 == CNST_TABLE_REF(11)))
																																								{	/* Expand/exit.scm 209 */
																																								BgL_kapzd21692zd2_435:
																																									if (PAIRP(BgL_cdrzd21689zd2_434))
																																										{	/* Expand/exit.scm 209 */
																																											obj_t
																																												BgL_carzd21698zd2_446;
																																											obj_t
																																												BgL_cdrzd21699zd2_447;
																																											BgL_carzd21698zd2_446
																																												=
																																												CAR
																																												(BgL_cdrzd21689zd2_434);
																																											BgL_cdrzd21699zd2_447
																																												=
																																												CDR
																																												(BgL_cdrzd21689zd2_434);
																																											if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd21698zd2_446))
																																												{	/* Expand/exit.scm 209 */
																																													if (PAIRP(BgL_cdrzd21699zd2_447))
																																														{	/* Expand/exit.scm 209 */
																																															obj_t
																																																BgL_carzd21706zd2_450;
																																															BgL_carzd21706zd2_450
																																																=
																																																CAR
																																																(BgL_cdrzd21699zd2_447);
																																															if (PAIRP(BgL_carzd21706zd2_450))
																																																{	/* Expand/exit.scm 209 */
																																																	obj_t
																																																		BgL_cdrzd21717zd2_452;
																																																	BgL_cdrzd21717zd2_452
																																																		=
																																																		CDR
																																																		(BgL_carzd21706zd2_450);
																																																	if ((BgL_exitz00_427 == CAR(BgL_carzd21706zd2_450)))
																																																		{	/* Expand/exit.scm 209 */
																																																			if (PAIRP(BgL_cdrzd21717zd2_452))
																																																				{	/* Expand/exit.scm 209 */
																																																					if (NULLP(CDR(BgL_cdrzd21717zd2_452)))
																																																						{	/* Expand/exit.scm 209 */
																																																							if (NULLP(CDR(BgL_cdrzd21699zd2_447)))
																																																								{	/* Expand/exit.scm 209 */
																																																									if (NULLP(CDR(((obj_t) BgL_cdrzd21671zd2_425))))
																																																										{	/* Expand/exit.scm 209 */
																																																											BgL_letkz00_410
																																																												=
																																																												BgL_carzd21688zd2_433;
																																																											BgL_bindingsz00_411
																																																												=
																																																												BgL_carzd21698zd2_446;
																																																											BgL_bodyz00_412
																																																												=
																																																												BgL_carzd21706zd2_450;
																																																											BgL_exitz00_413
																																																												=
																																																												BgL_exitz00_427;
																																																											BgL_exprz00_414
																																																												=
																																																												CAR
																																																												(BgL_cdrzd21717zd2_452);
																																																											{	/* Expand/exit.scm 214 */
																																																												bool_t
																																																													BgL_test2653z00_2434;
																																																												{
																																																													obj_t
																																																														BgL_l1080z00_531;
																																																													BgL_l1080z00_531
																																																														=
																																																														BgL_bindingsz00_411;
																																																												BgL_zc3z04anonymousza31626ze3z87_532:
																																																													if (NULLP(BgL_l1080z00_531))
																																																														{	/* Expand/exit.scm 214 */
																																																															BgL_test2653z00_2434
																																																																=
																																																																(
																																																																(bool_t)
																																																																1);
																																																														}
																																																													else
																																																														{	/* Expand/exit.scm 214 */
																																																															bool_t
																																																																BgL_test2655z00_2437;
																																																															{	/* Expand/exit.scm 215 */
																																																																obj_t
																																																																	BgL_bz00_537;
																																																																BgL_bz00_537
																																																																	=
																																																																	CAR
																																																																	(
																																																																	((obj_t) BgL_l1080z00_531));
																																																																{	/* Expand/exit.scm 215 */
																																																																	bool_t
																																																																		BgL__ortest_1058z00_538;
																																																																	BgL__ortest_1058z00_538
																																																																		=
																																																																		SYMBOLP
																																																																		(BgL_bz00_537);
																																																																	if (BgL__ortest_1058z00_538)
																																																																		{	/* Expand/exit.scm 215 */
																																																																			BgL_test2655z00_2437
																																																																				=
																																																																				BgL__ortest_1058z00_538;
																																																																		}
																																																																	else
																																																																		{	/* Expand/exit.scm 215 */
																																																																			if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_bz00_537))
																																																																				{	/* Expand/exit.scm 216 */
																																																																					if (BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00(BgL_exitz00_413, CAR(((obj_t) BgL_bz00_537))))
																																																																						{	/* Expand/exit.scm 217 */
																																																																							BgL_test2655z00_2437
																																																																								=
																																																																								(
																																																																								(bool_t)
																																																																								0);
																																																																						}
																																																																					else
																																																																						{	/* Expand/exit.scm 217 */
																																																																							BgL_test2655z00_2437
																																																																								=
																																																																								(
																																																																								(bool_t)
																																																																								1);
																																																																						}
																																																																				}
																																																																			else
																																																																				{	/* Expand/exit.scm 216 */
																																																																					BgL_test2655z00_2437
																																																																						=
																																																																						(
																																																																						(bool_t)
																																																																						0);
																																																																				}
																																																																		}
																																																																}
																																																															}
																																																															if (BgL_test2655z00_2437)
																																																																{	/* Expand/exit.scm 214 */
																																																																	obj_t
																																																																		BgL_arg1627z00_536;
																																																																	BgL_arg1627z00_536
																																																																		=
																																																																		CDR
																																																																		(
																																																																		((obj_t) BgL_l1080z00_531));
																																																																	{
																																																																		obj_t
																																																																			BgL_l1080z00_2450;
																																																																		BgL_l1080z00_2450
																																																																			=
																																																																			BgL_arg1627z00_536;
																																																																		BgL_l1080z00_531
																																																																			=
																																																																			BgL_l1080z00_2450;
																																																																		goto
																																																																			BgL_zc3z04anonymousza31626ze3z87_532;
																																																																	}
																																																																}
																																																															else
																																																																{	/* Expand/exit.scm 214 */
																																																																	BgL_test2653z00_2434
																																																																		=
																																																																		(
																																																																		(bool_t)
																																																																		0);
																																																																}
																																																														}
																																																												}
																																																												if (BgL_test2653z00_2434)
																																																													{	/* Expand/exit.scm 219 */
																																																														obj_t
																																																															BgL_arg1615z00_527;
																																																														{	/* Expand/exit.scm 219 */
																																																															obj_t
																																																																BgL_arg1616z00_528;
																																																															{	/* Expand/exit.scm 219 */
																																																																obj_t
																																																																	BgL_arg1625z00_529;
																																																																BgL_arg1625z00_529
																																																																	=
																																																																	MAKE_YOUNG_PAIR
																																																																	(BgL_exprz00_414,
																																																																	BNIL);
																																																																BgL_arg1616z00_528
																																																																	=
																																																																	MAKE_YOUNG_PAIR
																																																																	(BgL_bindingsz00_411,
																																																																	BgL_arg1625z00_529);
																																																															}
																																																															BgL_arg1615z00_527
																																																																=
																																																																MAKE_YOUNG_PAIR
																																																																(BgL_letkz00_410,
																																																																BgL_arg1616z00_528);
																																																														}
																																																														return
																																																															BGl_replacez12z12zztools_miscz00
																																																															(BgL_xz00_31,
																																																															BgL_arg1615z00_527);
																																																													}
																																																												else
																																																													{	/* Expand/exit.scm 214 */
																																																														BgL_exitz00_1216
																																																															=
																																																															BgL_exitz00_413;
																																																														BgL_bodyz00_1217
																																																															=
																																																															BgL_bodyz00_412;
																																																													BgL_zc3z04anonymousza32099ze3z87_1218:
																																																														{	/* Expand/exit.scm 140 */
																																																															obj_t
																																																																BgL_anzd2exitzd2_1219;
																																																															obj_t
																																																																BgL_anzd2exitdzd2_1220;
																																																															obj_t
																																																																BgL_valz00_1221;
																																																															obj_t
																																																																BgL_resz00_1222;
																																																															obj_t
																																																																BgL_envz00_1223;
																																																															obj_t
																																																																BgL_tracespz00_1224;
																																																															BgL_anzd2exitzd2_1219
																																																																=
																																																																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																																																(BGl_gensymz00zz__r4_symbols_6_4z00
																																																																(CNST_TABLE_REF
																																																																	(3)));
																																																															BgL_anzd2exitdzd2_1220
																																																																=
																																																																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																																																(BGl_gensymz00zz__r4_symbols_6_4z00
																																																																(CNST_TABLE_REF
																																																																	(4)));
																																																															BgL_valz00_1221
																																																																=
																																																																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																																																(BGl_gensymz00zz__r4_symbols_6_4z00
																																																																(CNST_TABLE_REF
																																																																	(5)));
																																																															BgL_resz00_1222
																																																																=
																																																																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																																																(BGl_gensymz00zz__r4_symbols_6_4z00
																																																																(CNST_TABLE_REF
																																																																	(6)));
																																																															BgL_envz00_1223
																																																																=
																																																																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																																																(BGl_gensymz00zz__r4_symbols_6_4z00
																																																																(CNST_TABLE_REF
																																																																	(13)));
																																																															BgL_tracespz00_1224
																																																																=
																																																																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																																																(BGl_gensymz00zz__r4_symbols_6_4z00
																																																																(CNST_TABLE_REF
																																																																	(7)));
																																																															{	/* Expand/exit.scm 146 */
																																																																obj_t
																																																																	BgL_newz00_1225;
																																																																{	/* Expand/exit.scm 146 */
																																																																	obj_t
																																																																		BgL_arg2100z00_1226;
																																																																	{	/* Expand/exit.scm 146 */
																																																																		obj_t
																																																																			BgL_arg2101z00_1227;
																																																																		obj_t
																																																																			BgL_arg2102z00_1228;
																																																																		BgL_arg2101z00_1227
																																																																			=
																																																																			MAKE_YOUNG_PAIR
																																																																			(BgL_anzd2exitzd2_1219,
																																																																			BNIL);
																																																																		{	/* Expand/exit.scm 148 */
																																																																			obj_t
																																																																				BgL_arg2103z00_1229;
																																																																			{	/* Expand/exit.scm 148 */
																																																																				obj_t
																																																																					BgL_arg2104z00_1230;
																																																																				{	/* Expand/exit.scm 148 */
																																																																					obj_t
																																																																						BgL_arg2105z00_1231;
																																																																					{	/* Expand/exit.scm 148 */
																																																																						obj_t
																																																																							BgL_arg2106z00_1232;
																																																																						obj_t
																																																																							BgL_arg2107z00_1233;
																																																																						{	/* Expand/exit.scm 148 */
																																																																							obj_t
																																																																								BgL_arg2108z00_1234;
																																																																							{	/* Expand/exit.scm 148 */
																																																																								obj_t
																																																																									BgL_arg2109z00_1235;
																																																																								{	/* Expand/exit.scm 148 */
																																																																									obj_t
																																																																										BgL_arg2110z00_1236;
																																																																									BgL_arg2110z00_1236
																																																																										=
																																																																										MAKE_YOUNG_PAIR
																																																																										(CNST_TABLE_REF
																																																																										(14),
																																																																										BNIL);
																																																																									BgL_arg2109z00_1235
																																																																										=
																																																																										MAKE_YOUNG_PAIR
																																																																										(BgL_arg2110z00_1236,
																																																																										BNIL);
																																																																								}
																																																																								BgL_arg2108z00_1234
																																																																									=
																																																																									MAKE_YOUNG_PAIR
																																																																									(BgL_envz00_1223,
																																																																									BgL_arg2109z00_1235);
																																																																							}
																																																																							BgL_arg2106z00_1232
																																																																								=
																																																																								MAKE_YOUNG_PAIR
																																																																								(BgL_arg2108z00_1234,
																																																																								BNIL);
																																																																						}
																																																																						{	/* Expand/exit.scm 149 */
																																																																							obj_t
																																																																								BgL_arg2111z00_1237;
																																																																							obj_t
																																																																								BgL_arg2112z00_1238;
																																																																							{	/* Expand/exit.scm 149 */
																																																																								obj_t
																																																																									BgL_arg2113z00_1239;
																																																																								{	/* Expand/exit.scm 149 */
																																																																									obj_t
																																																																										BgL_arg2114z00_1240;
																																																																									{	/* Expand/exit.scm 149 */
																																																																										obj_t
																																																																											BgL_arg2115z00_1241;
																																																																										BgL_arg2115z00_1241
																																																																											=
																																																																											MAKE_YOUNG_PAIR
																																																																											(BINT
																																																																											(1L),
																																																																											BNIL);
																																																																										BgL_arg2114z00_1240
																																																																											=
																																																																											MAKE_YOUNG_PAIR
																																																																											(BgL_anzd2exitzd2_1219,
																																																																											BgL_arg2115z00_1241);
																																																																									}
																																																																									BgL_arg2113z00_1239
																																																																										=
																																																																										MAKE_YOUNG_PAIR
																																																																										(BgL_envz00_1223,
																																																																										BgL_arg2114z00_1240);
																																																																								}
																																																																								BgL_arg2111z00_1237
																																																																									=
																																																																									MAKE_YOUNG_PAIR
																																																																									(CNST_TABLE_REF
																																																																									(8),
																																																																									BgL_arg2113z00_1239);
																																																																							}
																																																																							{	/* Expand/exit.scm 150 */
																																																																								obj_t
																																																																									BgL_arg2116z00_1242;
																																																																								{	/* Expand/exit.scm 150 */
																																																																									obj_t
																																																																										BgL_arg2117z00_1243;
																																																																									{	/* Expand/exit.scm 150 */
																																																																										obj_t
																																																																											BgL_arg2118z00_1244;
																																																																										obj_t
																																																																											BgL_arg2119z00_1245;
																																																																										{	/* Expand/exit.scm 150 */
																																																																											obj_t
																																																																												BgL_arg2120z00_1246;
																																																																											{	/* Expand/exit.scm 150 */
																																																																												obj_t
																																																																													BgL_arg2121z00_1247;
																																																																												{	/* Expand/exit.scm 150 */
																																																																													obj_t
																																																																														BgL_arg2122z00_1248;
																																																																													{	/* Expand/exit.scm 150 */
																																																																														obj_t
																																																																															BgL_arg2123z00_1249;
																																																																														BgL_arg2123z00_1249
																																																																															=
																																																																															MAKE_YOUNG_PAIR
																																																																															(BgL_envz00_1223,
																																																																															BNIL);
																																																																														BgL_arg2122z00_1248
																																																																															=
																																																																															MAKE_YOUNG_PAIR
																																																																															(CNST_TABLE_REF
																																																																															(9),
																																																																															BgL_arg2123z00_1249);
																																																																													}
																																																																													BgL_arg2121z00_1247
																																																																														=
																																																																														MAKE_YOUNG_PAIR
																																																																														(BgL_arg2122z00_1248,
																																																																														BNIL);
																																																																												}
																																																																												BgL_arg2120z00_1246
																																																																													=
																																																																													MAKE_YOUNG_PAIR
																																																																													(BgL_anzd2exitdzd2_1220,
																																																																													BgL_arg2121z00_1247);
																																																																											}
																																																																											BgL_arg2118z00_1244
																																																																												=
																																																																												MAKE_YOUNG_PAIR
																																																																												(BgL_arg2120z00_1246,
																																																																												BNIL);
																																																																										}
																																																																										{	/* Expand/exit.scm 151 */
																																																																											obj_t
																																																																												BgL_arg2124z00_1250;
																																																																											{	/* Expand/exit.scm 151 */
																																																																												obj_t
																																																																													BgL_arg2125z00_1251;
																																																																												{	/* Expand/exit.scm 151 */
																																																																													obj_t
																																																																														BgL_arg2126z00_1252;
																																																																													obj_t
																																																																														BgL_arg2127z00_1253;
																																																																													{	/* Expand/exit.scm 151 */
																																																																														obj_t
																																																																															BgL_arg2129z00_1254;
																																																																														{	/* Expand/exit.scm 151 */
																																																																															obj_t
																																																																																BgL_arg2130z00_1255;
																																																																															{	/* Expand/exit.scm 151 */
																																																																																obj_t
																																																																																	BgL_arg2131z00_1256;
																																																																																obj_t
																																																																																	BgL_arg2132z00_1257;
																																																																																BgL_arg2131z00_1256
																																																																																	=
																																																																																	MAKE_YOUNG_PAIR
																																																																																	(BgL_valz00_1221,
																																																																																	BNIL);
																																																																																{	/* Expand/exit.scm 152 */
																																																																																	obj_t
																																																																																		BgL_arg2133z00_1258;
																																																																																	{	/* Expand/exit.scm 152 */
																																																																																		obj_t
																																																																																			BgL_arg2134z00_1259;
																																																																																		obj_t
																																																																																			BgL_arg2135z00_1260;
																																																																																		{	/* Expand/exit.scm 152 */
																																																																																			obj_t
																																																																																				BgL_arg2136z00_1261;
																																																																																			{	/* Expand/exit.scm 152 */
																																																																																				obj_t
																																																																																					BgL_arg2137z00_1262;
																																																																																				BgL_arg2137z00_1262
																																																																																					=
																																																																																					MAKE_YOUNG_PAIR
																																																																																					(CNST_TABLE_REF
																																																																																					(15),
																																																																																					BNIL);
																																																																																				BgL_arg2136z00_1261
																																																																																					=
																																																																																					MAKE_YOUNG_PAIR
																																																																																					(CNST_TABLE_REF
																																																																																					(16),
																																																																																					BgL_arg2137z00_1262);
																																																																																			}
																																																																																			BgL_arg2134z00_1259
																																																																																				=
																																																																																				MAKE_YOUNG_PAIR
																																																																																				(CNST_TABLE_REF
																																																																																				(17),
																																																																																				BgL_arg2136z00_1261);
																																																																																		}
																																																																																		{	/* Expand/exit.scm 157 */
																																																																																			obj_t
																																																																																				BgL_arg2138z00_1263;
																																																																																			{	/* Expand/exit.scm 157 */
																																																																																				obj_t
																																																																																					BgL_arg2139z00_1264;
																																																																																				{	/* Expand/exit.scm 157 */
																																																																																					obj_t
																																																																																						BgL_arg2141z00_1265;
																																																																																					{	/* Expand/exit.scm 157 */
																																																																																						obj_t
																																																																																							BgL_arg2142z00_1266;
																																																																																						{	/* Expand/exit.scm 157 */
																																																																																							obj_t
																																																																																								BgL_arg2143z00_1267;
																																																																																							if (BGl_tracezf3ze71z14zzexpand_exitz00())
																																																																																								{	/* Expand/exit.scm 157 */
																																																																																									BgL_arg2143z00_1267
																																																																																										=
																																																																																										BgL_tracespz00_1224;
																																																																																								}
																																																																																							else
																																																																																								{	/* Expand/exit.scm 157 */
																																																																																									BgL_arg2143z00_1267
																																																																																										=
																																																																																										BFALSE;
																																																																																								}
																																																																																							BgL_arg2142z00_1266
																																																																																								=
																																																																																								MAKE_YOUNG_PAIR
																																																																																								(BgL_arg2143z00_1267,
																																																																																								BNIL);
																																																																																						}
																																																																																						BgL_arg2141z00_1265
																																																																																							=
																																																																																							MAKE_YOUNG_PAIR
																																																																																							(BFALSE,
																																																																																							BgL_arg2142z00_1266);
																																																																																					}
																																																																																					BgL_arg2139z00_1264
																																																																																						=
																																																																																						MAKE_YOUNG_PAIR
																																																																																						(BgL_valz00_1221,
																																																																																						BgL_arg2141z00_1265);
																																																																																				}
																																																																																				BgL_arg2138z00_1263
																																																																																					=
																																																																																					MAKE_YOUNG_PAIR
																																																																																					(BFALSE,
																																																																																					BgL_arg2139z00_1264);
																																																																																			}
																																																																																			BgL_arg2135z00_1260
																																																																																				=
																																																																																				MAKE_YOUNG_PAIR
																																																																																				(BgL_anzd2exitdzd2_1220,
																																																																																				BgL_arg2138z00_1263);
																																																																																		}
																																																																																		BgL_arg2133z00_1258
																																																																																			=
																																																																																			MAKE_YOUNG_PAIR
																																																																																			(BgL_arg2134z00_1259,
																																																																																			BgL_arg2135z00_1260);
																																																																																	}
																																																																																	BgL_arg2132z00_1257
																																																																																		=
																																																																																		MAKE_YOUNG_PAIR
																																																																																		(BgL_arg2133z00_1258,
																																																																																		BNIL);
																																																																																}
																																																																																BgL_arg2130z00_1255
																																																																																	=
																																																																																	MAKE_YOUNG_PAIR
																																																																																	(BgL_arg2131z00_1256,
																																																																																	BgL_arg2132z00_1257);
																																																																															}
																																																																															BgL_arg2129z00_1254
																																																																																=
																																																																																MAKE_YOUNG_PAIR
																																																																																(BgL_exitz00_1216,
																																																																																BgL_arg2130z00_1255);
																																																																														}
																																																																														BgL_arg2126z00_1252
																																																																															=
																																																																															MAKE_YOUNG_PAIR
																																																																															(BgL_arg2129z00_1254,
																																																																															BNIL);
																																																																													}
																																																																													{	/* Expand/exit.scm 158 */
																																																																														obj_t
																																																																															BgL_arg2145z00_1269;
																																																																														{	/* Expand/exit.scm 158 */
																																																																															obj_t
																																																																																BgL_arg2146z00_1270;
																																																																															{	/* Expand/exit.scm 158 */
																																																																																obj_t
																																																																																	BgL_arg2147z00_1271;
																																																																																obj_t
																																																																																	BgL_arg2148z00_1272;
																																																																																{	/* Expand/exit.scm 158 */
																																																																																	obj_t
																																																																																		BgL_arg2149z00_1273;
																																																																																	{	/* Expand/exit.scm 158 */
																																																																																		obj_t
																																																																																			BgL_arg2150z00_1274;
																																																																																		{	/* Expand/exit.scm 158 */
																																																																																			obj_t
																																																																																				BgL_arg2151z00_1275;
																																																																																			{	/* Expand/exit.scm 158 */
																																																																																				obj_t
																																																																																					BgL_arg2152z00_1276;
																																																																																				BgL_arg2152z00_1276
																																																																																					=
																																																																																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																																																																					(BgL_bodyz00_1217,
																																																																																					BNIL);
																																																																																				BgL_arg2151z00_1275
																																																																																					=
																																																																																					MAKE_YOUNG_PAIR
																																																																																					(CNST_TABLE_REF
																																																																																					(12),
																																																																																					BgL_arg2152z00_1276);
																																																																																			}
																																																																																			BgL_arg2150z00_1274
																																																																																				=
																																																																																				MAKE_YOUNG_PAIR
																																																																																				(BgL_arg2151z00_1275,
																																																																																				BNIL);
																																																																																		}
																																																																																		BgL_arg2149z00_1273
																																																																																			=
																																																																																			MAKE_YOUNG_PAIR
																																																																																			(BgL_resz00_1222,
																																																																																			BgL_arg2150z00_1274);
																																																																																	}
																																																																																	BgL_arg2147z00_1271
																																																																																		=
																																																																																		MAKE_YOUNG_PAIR
																																																																																		(BgL_arg2149z00_1273,
																																																																																		BNIL);
																																																																																}
																																																																																{	/* Expand/exit.scm 159 */
																																																																																	obj_t
																																																																																		BgL_arg2154z00_1277;
																																																																																	obj_t
																																																																																		BgL_arg2155z00_1278;
																																																																																	{	/* Expand/exit.scm 159 */
																																																																																		obj_t
																																																																																			BgL_arg2156z00_1279;
																																																																																		BgL_arg2156z00_1279
																																																																																			=
																																																																																			MAKE_YOUNG_PAIR
																																																																																			(BgL_envz00_1223,
																																																																																			BNIL);
																																																																																		BgL_arg2154z00_1277
																																																																																			=
																																																																																			MAKE_YOUNG_PAIR
																																																																																			(CNST_TABLE_REF
																																																																																			(10),
																																																																																			BgL_arg2156z00_1279);
																																																																																	}
																																																																																	BgL_arg2155z00_1278
																																																																																		=
																																																																																		MAKE_YOUNG_PAIR
																																																																																		(BgL_resz00_1222,
																																																																																		BNIL);
																																																																																	BgL_arg2148z00_1272
																																																																																		=
																																																																																		MAKE_YOUNG_PAIR
																																																																																		(BgL_arg2154z00_1277,
																																																																																		BgL_arg2155z00_1278);
																																																																																}
																																																																																BgL_arg2146z00_1270
																																																																																	=
																																																																																	MAKE_YOUNG_PAIR
																																																																																	(BgL_arg2147z00_1271,
																																																																																	BgL_arg2148z00_1272);
																																																																															}
																																																																															BgL_arg2145z00_1269
																																																																																=
																																																																																MAKE_YOUNG_PAIR
																																																																																(CNST_TABLE_REF
																																																																																(11),
																																																																																BgL_arg2146z00_1270);
																																																																														}
																																																																														BgL_arg2127z00_1253
																																																																															=
																																																																															MAKE_YOUNG_PAIR
																																																																															(BgL_arg2145z00_1269,
																																																																															BNIL);
																																																																													}
																																																																													BgL_arg2125z00_1251
																																																																														=
																																																																														MAKE_YOUNG_PAIR
																																																																														(BgL_arg2126z00_1252,
																																																																														BgL_arg2127z00_1253);
																																																																												}
																																																																												BgL_arg2124z00_1250
																																																																													=
																																																																													MAKE_YOUNG_PAIR
																																																																													(CNST_TABLE_REF
																																																																													(18),
																																																																													BgL_arg2125z00_1251);
																																																																											}
																																																																											BgL_arg2119z00_1245
																																																																												=
																																																																												MAKE_YOUNG_PAIR
																																																																												(BgL_arg2124z00_1250,
																																																																												BNIL);
																																																																										}
																																																																										BgL_arg2117z00_1243
																																																																											=
																																																																											MAKE_YOUNG_PAIR
																																																																											(BgL_arg2118z00_1244,
																																																																											BgL_arg2119z00_1245);
																																																																									}
																																																																									BgL_arg2116z00_1242
																																																																										=
																																																																										MAKE_YOUNG_PAIR
																																																																										(CNST_TABLE_REF
																																																																										(11),
																																																																										BgL_arg2117z00_1243);
																																																																								}
																																																																								BgL_arg2112z00_1238
																																																																									=
																																																																									MAKE_YOUNG_PAIR
																																																																									(BgL_arg2116z00_1242,
																																																																									BNIL);
																																																																							}
																																																																							BgL_arg2107z00_1233
																																																																								=
																																																																								MAKE_YOUNG_PAIR
																																																																								(BgL_arg2111z00_1237,
																																																																								BgL_arg2112z00_1238);
																																																																						}
																																																																						BgL_arg2105z00_1231
																																																																							=
																																																																							MAKE_YOUNG_PAIR
																																																																							(BgL_arg2106z00_1232,
																																																																							BgL_arg2107z00_1233);
																																																																					}
																																																																					BgL_arg2104z00_1230
																																																																						=
																																																																						MAKE_YOUNG_PAIR
																																																																						(CNST_TABLE_REF
																																																																						(11),
																																																																						BgL_arg2105z00_1231);
																																																																				}
																																																																				BgL_arg2103z00_1229
																																																																					=
																																																																					BGl_addzd2traceze70z35zzexpand_exitz00
																																																																					(BgL_tracespz00_1224,
																																																																					BgL_arg2104z00_1230);
																																																																			}
																																																																			BgL_arg2102z00_1228
																																																																				=
																																																																				MAKE_YOUNG_PAIR
																																																																				(BgL_arg2103z00_1229,
																																																																				BNIL);
																																																																		}
																																																																		BgL_arg2100z00_1226
																																																																			=
																																																																			MAKE_YOUNG_PAIR
																																																																			(BgL_arg2101z00_1227,
																																																																			BgL_arg2102z00_1228);
																																																																	}
																																																																	BgL_newz00_1225
																																																																		=
																																																																		MAKE_YOUNG_PAIR
																																																																		(CNST_TABLE_REF
																																																																		(2),
																																																																		BgL_arg2100z00_1226);
																																																																}
																																																																return
																																																																	BGl_replacez12z12zztools_miscz00
																																																																	(BgL_xz00_31,
																																																																	BgL_newz00_1225);
																																																															}
																																																														}
																																																													}
																																																											}
																																																										}
																																																									else
																																																										{	/* Expand/exit.scm 209 */
																																																											obj_t
																																																												BgL_cdrzd21729zd2_463;
																																																											BgL_cdrzd21729zd2_463
																																																												=
																																																												CDR
																																																												(
																																																												((obj_t) BgL_xz00_31));
																																																											{	/* Expand/exit.scm 209 */
																																																												obj_t
																																																													BgL_arg1422z00_464;
																																																												obj_t
																																																													BgL_arg1434z00_465;
																																																												{	/* Expand/exit.scm 209 */
																																																													obj_t
																																																														BgL_pairz00_1981;
																																																													BgL_pairz00_1981
																																																														=
																																																														CAR
																																																														(
																																																														((obj_t) BgL_cdrzd21729zd2_463));
																																																													BgL_arg1422z00_464
																																																														=
																																																														CAR
																																																														(BgL_pairz00_1981);
																																																												}
																																																												BgL_arg1434z00_465
																																																													=
																																																													CDR
																																																													(
																																																													((obj_t) BgL_cdrzd21729zd2_463));
																																																												BgL_exitz00_416
																																																													=
																																																													BgL_arg1422z00_464;
																																																												BgL_bodyz00_417
																																																													=
																																																													BgL_arg1434z00_465;
																																																											BgL_tagzd21650zd2_418:
																																																												if (BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00(BgL_exitz00_416, BgL_bodyz00_417))
																																																													{	/* Expand/exit.scm 224 */
																																																														obj_t
																																																															BgL_headz00_544;
																																																														obj_t
																																																															BgL_tailz00_545;
																																																														{	/* Expand/exit.scm 224 */
																																																															obj_t
																																																																BgL_arg1654z00_553;
																																																															{	/* Expand/exit.scm 224 */
																																																																long
																																																																	BgL_a1083z00_554;
																																																																BgL_a1083z00_554
																																																																	=
																																																																	bgl_list_length
																																																																	(BgL_bodyz00_417);
																																																																{	/* Expand/exit.scm 224 */

																																																																	{	/* Expand/exit.scm 224 */
																																																																		obj_t
																																																																			BgL_za71za7_1951;
																																																																		obj_t
																																																																			BgL_za72za7_1952;
																																																																		BgL_za71za7_1951
																																																																			=
																																																																			BINT
																																																																			(BgL_a1083z00_554);
																																																																		BgL_za72za7_1952
																																																																			=
																																																																			BINT
																																																																			(1L);
																																																																		{	/* Expand/exit.scm 224 */
																																																																			obj_t
																																																																				BgL_tmpz00_1953;
																																																																			BgL_tmpz00_1953
																																																																				=
																																																																				BINT
																																																																				(0L);
																																																																			if (BGL_SUBFX_OV(BgL_za71za7_1951, BgL_za72za7_1952, BgL_tmpz00_1953))
																																																																				{	/* Expand/exit.scm 224 */
																																																																					BgL_arg1654z00_553
																																																																						=
																																																																						bgl_bignum_sub
																																																																						(bgl_long_to_bignum
																																																																						((long) CINT(BgL_za71za7_1951)), bgl_long_to_bignum((long) CINT(BgL_za72za7_1952)));
																																																																				}
																																																																			else
																																																																				{	/* Expand/exit.scm 224 */
																																																																					BgL_arg1654z00_553
																																																																						=
																																																																						BgL_tmpz00_1953;
																																																																				}
																																																																		}
																																																																	}
																																																																}
																																																															}
																																																															BgL_headz00_544
																																																																=
																																																																BGl_takez00zz__r4_pairs_and_lists_6_3z00
																																																																(BgL_bodyz00_417,
																																																																(long)
																																																																CINT
																																																																(BgL_arg1654z00_553));
																																																														}
																																																														BgL_tailz00_545
																																																															=
																																																															CAR
																																																															(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																																																															(BgL_bodyz00_417));
																																																														{	/* Expand/exit.scm 226 */
																																																															bool_t
																																																																BgL_test2662z00_2568;
																																																															if (BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00(BgL_exitz00_416, BgL_headz00_544))
																																																																{	/* Expand/exit.scm 226 */
																																																																	BgL_test2662z00_2568
																																																																		=
																																																																		(
																																																																		(bool_t)
																																																																		0);
																																																																}
																																																															else
																																																																{	/* Expand/exit.scm 226 */
																																																																	BgL_test2662z00_2568
																																																																		=
																																																																		BGl_tailreczf3ze70z14zzexpand_exitz00
																																																																		(BgL_exitz00_416,
																																																																		BgL_tailz00_545);
																																																																}
																																																															if (BgL_test2662z00_2568)
																																																																{	/* Expand/exit.scm 228 */
																																																																	obj_t
																																																																		BgL_arg1642z00_548;
																																																																	{	/* Expand/exit.scm 228 */
																																																																		obj_t
																																																																			BgL_arg1646z00_549;
																																																																		{	/* Expand/exit.scm 228 */
																																																																			obj_t
																																																																				BgL_arg1650z00_550;
																																																																			BgL_arg1650z00_550
																																																																				=
																																																																				MAKE_YOUNG_PAIR
																																																																				(BGl_returnz12ze70zf5zzexpand_exitz00
																																																																				(BgL_exitz00_416,
																																																																					BgL_tailz00_545),
																																																																				BNIL);
																																																																			BgL_arg1646z00_549
																																																																				=
																																																																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																																																				(BgL_headz00_544,
																																																																				BgL_arg1650z00_550);
																																																																		}
																																																																		BgL_arg1642z00_548
																																																																			=
																																																																			MAKE_YOUNG_PAIR
																																																																			(CNST_TABLE_REF
																																																																			(12),
																																																																			BgL_arg1646z00_549);
																																																																	}
																																																																	return
																																																																		BGl_replacez12z12zztools_miscz00
																																																																		(BgL_xz00_31,
																																																																		BgL_arg1642z00_548);
																																																																}
																																																															else
																																																																{
																																																																	obj_t
																																																																		BgL_bodyz00_2579;
																																																																	obj_t
																																																																		BgL_exitz00_2578;
																																																																	BgL_exitz00_2578
																																																																		=
																																																																		BgL_exitz00_416;
																																																																	BgL_bodyz00_2579
																																																																		=
																																																																		BgL_bodyz00_417;
																																																																	BgL_bodyz00_1217
																																																																		=
																																																																		BgL_bodyz00_2579;
																																																																	BgL_exitz00_1216
																																																																		=
																																																																		BgL_exitz00_2578;
																																																																	goto
																																																																		BgL_zc3z04anonymousza32099ze3z87_1218;
																																																																}
																																																														}
																																																													}
																																																												else
																																																													{	/* Expand/exit.scm 223 */
																																																														obj_t
																																																															BgL_arg1663z00_558;
																																																														{	/* Expand/exit.scm 223 */
																																																															obj_t
																																																																BgL_arg1675z00_559;
																																																															{	/* Expand/exit.scm 223 */
																																																																obj_t
																																																																	BgL_arg1678z00_560;
																																																																BgL_arg1678z00_560
																																																																	=
																																																																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																																																	(BgL_bodyz00_417,
																																																																	BNIL);
																																																																BgL_arg1675z00_559
																																																																	=
																																																																	MAKE_YOUNG_PAIR
																																																																	(CNST_TABLE_REF
																																																																	(12),
																																																																	BgL_arg1678z00_560);
																																																															}
																																																															BgL_arg1663z00_558
																																																																=
																																																																BGL_PROCEDURE_CALL2
																																																																(BgL_ez00_32,
																																																																BgL_arg1675z00_559,
																																																																BgL_ez00_32);
																																																														}
																																																														return
																																																															BGl_replacez12z12zztools_miscz00
																																																															(BgL_xz00_31,
																																																															BgL_arg1663z00_558);
																																																													}
																																																											}
																																																										}
																																																								}
																																																							else
																																																								{	/* Expand/exit.scm 209 */
																																																									obj_t
																																																										BgL_cdrzd21748zd2_468;
																																																									BgL_cdrzd21748zd2_468
																																																										=
																																																										CDR
																																																										(
																																																										((obj_t) BgL_xz00_31));
																																																									{	/* Expand/exit.scm 209 */
																																																										obj_t
																																																											BgL_arg1453z00_469;
																																																										obj_t
																																																											BgL_arg1454z00_470;
																																																										{	/* Expand/exit.scm 209 */
																																																											obj_t
																																																												BgL_pairz00_1985;
																																																											BgL_pairz00_1985
																																																												=
																																																												CAR
																																																												(
																																																												((obj_t) BgL_cdrzd21748zd2_468));
																																																											BgL_arg1453z00_469
																																																												=
																																																												CAR
																																																												(BgL_pairz00_1985);
																																																										}
																																																										BgL_arg1454z00_470
																																																											=
																																																											CDR
																																																											(
																																																											((obj_t) BgL_cdrzd21748zd2_468));
																																																										{
																																																											obj_t
																																																												BgL_bodyz00_2597;
																																																											obj_t
																																																												BgL_exitz00_2596;
																																																											BgL_exitz00_2596
																																																												=
																																																												BgL_arg1453z00_469;
																																																											BgL_bodyz00_2597
																																																												=
																																																												BgL_arg1454z00_470;
																																																											BgL_bodyz00_417
																																																												=
																																																												BgL_bodyz00_2597;
																																																											BgL_exitz00_416
																																																												=
																																																												BgL_exitz00_2596;
																																																											goto
																																																												BgL_tagzd21650zd2_418;
																																																										}
																																																									}
																																																								}
																																																						}
																																																					else
																																																						{	/* Expand/exit.scm 209 */
																																																							obj_t
																																																								BgL_cdrzd21767zd2_473;
																																																							BgL_cdrzd21767zd2_473
																																																								=
																																																								CDR
																																																								(
																																																								((obj_t) BgL_xz00_31));
																																																							{	/* Expand/exit.scm 209 */
																																																								obj_t
																																																									BgL_arg1485z00_474;
																																																								obj_t
																																																									BgL_arg1489z00_475;
																																																								{	/* Expand/exit.scm 209 */
																																																									obj_t
																																																										BgL_pairz00_1989;
																																																									BgL_pairz00_1989
																																																										=
																																																										CAR
																																																										(
																																																										((obj_t) BgL_cdrzd21767zd2_473));
																																																									BgL_arg1485z00_474
																																																										=
																																																										CAR
																																																										(BgL_pairz00_1989);
																																																								}
																																																								BgL_arg1489z00_475
																																																									=
																																																									CDR
																																																									(
																																																									((obj_t) BgL_cdrzd21767zd2_473));
																																																								{
																																																									obj_t
																																																										BgL_bodyz00_2606;
																																																									obj_t
																																																										BgL_exitz00_2605;
																																																									BgL_exitz00_2605
																																																										=
																																																										BgL_arg1485z00_474;
																																																									BgL_bodyz00_2606
																																																										=
																																																										BgL_arg1489z00_475;
																																																									BgL_bodyz00_417
																																																										=
																																																										BgL_bodyz00_2606;
																																																									BgL_exitz00_416
																																																										=
																																																										BgL_exitz00_2605;
																																																									goto
																																																										BgL_tagzd21650zd2_418;
																																																								}
																																																							}
																																																						}
																																																				}
																																																			else
																																																				{	/* Expand/exit.scm 209 */
																																																					obj_t
																																																						BgL_cdrzd21786zd2_478;
																																																					BgL_cdrzd21786zd2_478
																																																						=
																																																						CDR
																																																						(
																																																						((obj_t) BgL_xz00_31));
																																																					{	/* Expand/exit.scm 209 */
																																																						obj_t
																																																							BgL_arg1513z00_479;
																																																						obj_t
																																																							BgL_arg1514z00_480;
																																																						{	/* Expand/exit.scm 209 */
																																																							obj_t
																																																								BgL_pairz00_1993;
																																																							BgL_pairz00_1993
																																																								=
																																																								CAR
																																																								(
																																																								((obj_t) BgL_cdrzd21786zd2_478));
																																																							BgL_arg1513z00_479
																																																								=
																																																								CAR
																																																								(BgL_pairz00_1993);
																																																						}
																																																						BgL_arg1514z00_480
																																																							=
																																																							CDR
																																																							(
																																																							((obj_t) BgL_cdrzd21786zd2_478));
																																																						{
																																																							obj_t
																																																								BgL_bodyz00_2615;
																																																							obj_t
																																																								BgL_exitz00_2614;
																																																							BgL_exitz00_2614
																																																								=
																																																								BgL_arg1513z00_479;
																																																							BgL_bodyz00_2615
																																																								=
																																																								BgL_arg1514z00_480;
																																																							BgL_bodyz00_417
																																																								=
																																																								BgL_bodyz00_2615;
																																																							BgL_exitz00_416
																																																								=
																																																								BgL_exitz00_2614;
																																																							goto
																																																								BgL_tagzd21650zd2_418;
																																																						}
																																																					}
																																																				}
																																																		}
																																																	else
																																																		{	/* Expand/exit.scm 209 */
																																																			obj_t
																																																				BgL_cdrzd21805zd2_482;
																																																			BgL_cdrzd21805zd2_482
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_xz00_31));
																																																			{	/* Expand/exit.scm 209 */
																																																				obj_t
																																																					BgL_arg1535z00_483;
																																																				obj_t
																																																					BgL_arg1540z00_484;
																																																				{	/* Expand/exit.scm 209 */
																																																					obj_t
																																																						BgL_pairz00_1997;
																																																					BgL_pairz00_1997
																																																						=
																																																						CAR
																																																						(
																																																						((obj_t) BgL_cdrzd21805zd2_482));
																																																					BgL_arg1535z00_483
																																																						=
																																																						CAR
																																																						(BgL_pairz00_1997);
																																																				}
																																																				BgL_arg1540z00_484
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_cdrzd21805zd2_482));
																																																				{
																																																					obj_t
																																																						BgL_bodyz00_2624;
																																																					obj_t
																																																						BgL_exitz00_2623;
																																																					BgL_exitz00_2623
																																																						=
																																																						BgL_arg1535z00_483;
																																																					BgL_bodyz00_2624
																																																						=
																																																						BgL_arg1540z00_484;
																																																					BgL_bodyz00_417
																																																						=
																																																						BgL_bodyz00_2624;
																																																					BgL_exitz00_416
																																																						=
																																																						BgL_exitz00_2623;
																																																					goto
																																																						BgL_tagzd21650zd2_418;
																																																				}
																																																			}
																																																		}
																																																}
																																															else
																																																{	/* Expand/exit.scm 209 */
																																																	obj_t
																																																		BgL_cdrzd21824zd2_487;
																																																	BgL_cdrzd21824zd2_487
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_xz00_31));
																																																	{	/* Expand/exit.scm 209 */
																																																		obj_t
																																																			BgL_arg1552z00_488;
																																																		obj_t
																																																			BgL_arg1553z00_489;
																																																		{	/* Expand/exit.scm 209 */
																																																			obj_t
																																																				BgL_pairz00_2001;
																																																			BgL_pairz00_2001
																																																				=
																																																				CAR
																																																				(
																																																				((obj_t) BgL_cdrzd21824zd2_487));
																																																			BgL_arg1552z00_488
																																																				=
																																																				CAR
																																																				(BgL_pairz00_2001);
																																																		}
																																																		BgL_arg1553z00_489
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd21824zd2_487));
																																																		{
																																																			obj_t
																																																				BgL_bodyz00_2633;
																																																			obj_t
																																																				BgL_exitz00_2632;
																																																			BgL_exitz00_2632
																																																				=
																																																				BgL_arg1552z00_488;
																																																			BgL_bodyz00_2633
																																																				=
																																																				BgL_arg1553z00_489;
																																																			BgL_bodyz00_417
																																																				=
																																																				BgL_bodyz00_2633;
																																																			BgL_exitz00_416
																																																				=
																																																				BgL_exitz00_2632;
																																																			goto
																																																				BgL_tagzd21650zd2_418;
																																																		}
																																																	}
																																																}
																																														}
																																													else
																																														{	/* Expand/exit.scm 209 */
																																															obj_t
																																																BgL_cdrzd21843zd2_491;
																																															BgL_cdrzd21843zd2_491
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_xz00_31));
																																															{	/* Expand/exit.scm 209 */
																																																obj_t
																																																	BgL_arg1561z00_492;
																																																obj_t
																																																	BgL_arg1564z00_493;
																																																{	/* Expand/exit.scm 209 */
																																																	obj_t
																																																		BgL_pairz00_2005;
																																																	BgL_pairz00_2005
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_cdrzd21843zd2_491));
																																																	BgL_arg1561z00_492
																																																		=
																																																		CAR
																																																		(BgL_pairz00_2005);
																																																}
																																																BgL_arg1564z00_493
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd21843zd2_491));
																																																{
																																																	obj_t
																																																		BgL_bodyz00_2642;
																																																	obj_t
																																																		BgL_exitz00_2641;
																																																	BgL_exitz00_2641
																																																		=
																																																		BgL_arg1561z00_492;
																																																	BgL_bodyz00_2642
																																																		=
																																																		BgL_arg1564z00_493;
																																																	BgL_bodyz00_417
																																																		=
																																																		BgL_bodyz00_2642;
																																																	BgL_exitz00_416
																																																		=
																																																		BgL_exitz00_2641;
																																																	goto
																																																		BgL_tagzd21650zd2_418;
																																																}
																																															}
																																														}
																																												}
																																											else
																																												{	/* Expand/exit.scm 209 */
																																													obj_t
																																														BgL_cdrzd21862zd2_495;
																																													BgL_cdrzd21862zd2_495
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_xz00_31));
																																													{	/* Expand/exit.scm 209 */
																																														obj_t
																																															BgL_arg1571z00_496;
																																														obj_t
																																															BgL_arg1573z00_497;
																																														{	/* Expand/exit.scm 209 */
																																															obj_t
																																																BgL_pairz00_2009;
																																															BgL_pairz00_2009
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_cdrzd21862zd2_495));
																																															BgL_arg1571z00_496
																																																=
																																																CAR
																																																(BgL_pairz00_2009);
																																														}
																																														BgL_arg1573z00_497
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_cdrzd21862zd2_495));
																																														{
																																															obj_t
																																																BgL_bodyz00_2651;
																																															obj_t
																																																BgL_exitz00_2650;
																																															BgL_exitz00_2650
																																																=
																																																BgL_arg1571z00_496;
																																															BgL_bodyz00_2651
																																																=
																																																BgL_arg1573z00_497;
																																															BgL_bodyz00_417
																																																=
																																																BgL_bodyz00_2651;
																																															BgL_exitz00_416
																																																=
																																																BgL_exitz00_2650;
																																															goto
																																																BgL_tagzd21650zd2_418;
																																														}
																																													}
																																												}
																																										}
																																									else
																																										{	/* Expand/exit.scm 209 */
																																											obj_t
																																												BgL_cdrzd21881zd2_499;
																																											BgL_cdrzd21881zd2_499
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_xz00_31));
																																											{	/* Expand/exit.scm 209 */
																																												obj_t
																																													BgL_arg1576z00_500;
																																												obj_t
																																													BgL_arg1584z00_501;
																																												{	/* Expand/exit.scm 209 */
																																													obj_t
																																														BgL_pairz00_2013;
																																													BgL_pairz00_2013
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd21881zd2_499));
																																													BgL_arg1576z00_500
																																														=
																																														CAR
																																														(BgL_pairz00_2013);
																																												}
																																												BgL_arg1584z00_501
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cdrzd21881zd2_499));
																																												{
																																													obj_t
																																														BgL_bodyz00_2660;
																																													obj_t
																																														BgL_exitz00_2659;
																																													BgL_exitz00_2659
																																														=
																																														BgL_arg1576z00_500;
																																													BgL_bodyz00_2660
																																														=
																																														BgL_arg1584z00_501;
																																													BgL_bodyz00_417
																																														=
																																														BgL_bodyz00_2660;
																																													BgL_exitz00_416
																																														=
																																														BgL_exitz00_2659;
																																													goto
																																														BgL_tagzd21650zd2_418;
																																												}
																																											}
																																										}
																																								}
																																							else
																																								{	/* Expand/exit.scm 209 */
																																									if ((BgL_carzd21688zd2_433 == CNST_TABLE_REF(19)))
																																										{	/* Expand/exit.scm 209 */
																																											goto
																																												BgL_kapzd21692zd2_435;
																																										}
																																									else
																																										{	/* Expand/exit.scm 209 */
																																											if ((BgL_carzd21688zd2_433 == CNST_TABLE_REF(20)))
																																												{	/* Expand/exit.scm 209 */
																																													goto
																																														BgL_kapzd21692zd2_435;
																																												}
																																											else
																																												{	/* Expand/exit.scm 209 */
																																													if ((BgL_carzd21688zd2_433 == CNST_TABLE_REF(21)))
																																														{	/* Expand/exit.scm 209 */
																																															goto
																																																BgL_kapzd21692zd2_435;
																																														}
																																													else
																																														{	/* Expand/exit.scm 209 */
																																															obj_t
																																																BgL_arg1351z00_442;
																																															obj_t
																																																BgL_arg1352z00_443;
																																															{	/* Expand/exit.scm 209 */
																																																obj_t
																																																	BgL_pairz00_2021;
																																																BgL_pairz00_2021
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd21664zd2_422));
																																																BgL_arg1351z00_442
																																																	=
																																																	CAR
																																																	(BgL_pairz00_2021);
																																															}
																																															BgL_arg1352z00_443
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd21664zd2_422));
																																															{
																																																obj_t
																																																	BgL_bodyz00_2676;
																																																obj_t
																																																	BgL_exitz00_2675;
																																																BgL_exitz00_2675
																																																	=
																																																	BgL_arg1351z00_442;
																																																BgL_bodyz00_2676
																																																	=
																																																	BgL_arg1352z00_443;
																																																BgL_bodyz00_417
																																																	=
																																																	BgL_bodyz00_2676;
																																																BgL_exitz00_416
																																																	=
																																																	BgL_exitz00_2675;
																																																goto
																																																	BgL_tagzd21650zd2_418;
																																															}
																																														}
																																												}
																																										}
																																								}
																																						}
																																					}
																																				else
																																					{	/* Expand/exit.scm 209 */
																																						obj_t
																																							BgL_arg1589z00_504;
																																						obj_t
																																							BgL_arg1591z00_505;
																																						{	/* Expand/exit.scm 209 */
																																							obj_t
																																								BgL_pairz00_2025;
																																							BgL_pairz00_2025
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21664zd2_422));
																																							BgL_arg1589z00_504
																																								=
																																								CAR
																																								(BgL_pairz00_2025);
																																						}
																																						BgL_arg1591z00_505
																																							=
																																							CDR
																																							(((obj_t) BgL_cdrzd21664zd2_422));
																																						{
																																							obj_t
																																								BgL_bodyz00_2683;
																																							obj_t
																																								BgL_exitz00_2682;
																																							BgL_exitz00_2682
																																								=
																																								BgL_arg1589z00_504;
																																							BgL_bodyz00_2683
																																								=
																																								BgL_arg1591z00_505;
																																							BgL_bodyz00_417
																																								=
																																								BgL_bodyz00_2683;
																																							BgL_exitz00_416
																																								=
																																								BgL_exitz00_2682;
																																							goto
																																								BgL_tagzd21650zd2_418;
																																						}
																																					}
																																			}
																																		else
																																			{	/* Expand/exit.scm 209 */
																																				obj_t
																																					BgL_arg1594z00_508;
																																				obj_t
																																					BgL_arg1595z00_509;
																																				{	/* Expand/exit.scm 209 */
																																					obj_t
																																						BgL_pairz00_2029;
																																					BgL_pairz00_2029
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21664zd2_422));
																																					BgL_arg1594z00_508
																																						=
																																						CAR
																																						(BgL_pairz00_2029);
																																				}
																																				BgL_arg1595z00_509
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd21664zd2_422));
																																				{
																																					obj_t
																																						BgL_bodyz00_2690;
																																					obj_t
																																						BgL_exitz00_2689;
																																					BgL_exitz00_2689
																																						=
																																						BgL_arg1594z00_508;
																																					BgL_bodyz00_2690
																																						=
																																						BgL_arg1595z00_509;
																																					BgL_bodyz00_417
																																						=
																																						BgL_bodyz00_2690;
																																					BgL_exitz00_416
																																						=
																																						BgL_exitz00_2689;
																																					goto
																																						BgL_tagzd21650zd2_418;
																																				}
																																			}
																																	}
																																else
																																	{	/* Expand/exit.scm 209 */
																																	BgL_tagzd21651zd2_419:
																																		return
																																			BGl_errorz00zz__errorz00
																																			(BFALSE,
																																			BGl_string2595z00zzexpand_exitz00,
																																			BgL_xz00_31);
																																	}
																															}
																														else
																															{	/* Expand/exit.scm 209 */
																																goto
																																	BgL_tagzd21651zd2_419;
																															}
																													}
																												else
																													{	/* Expand/exit.scm 209 */
																														goto
																															BgL_tagzd21651zd2_419;
																													}
																											}
																										else
																											{	/* Expand/exit.scm 209 */
																												goto
																													BgL_tagzd21651zd2_419;
																											}
																									}
																								}
																						}
																					else
																						{	/* Expand/exit.scm 197 */
																							obj_t BgL_arg1227z00_346;
																							obj_t BgL_arg1228z00_347;

																							{	/* Expand/exit.scm 197 */
																								obj_t BgL_pairz00_2048;

																								BgL_pairz00_2048 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd21278zd2_320));
																								BgL_arg1227z00_346 =
																									CAR(BgL_pairz00_2048);
																							}
																							BgL_arg1228z00_347 =
																								CDR(
																								((obj_t)
																									BgL_cdrzd21278zd2_320));
																							{
																								obj_t BgL_exprsz00_2698;
																								obj_t BgL_exitz00_2697;

																								BgL_exitz00_2697 =
																									BgL_arg1227z00_346;
																								BgL_exprsz00_2698 =
																									BgL_arg1228z00_347;
																								BgL_exprsz00_306 =
																									BgL_exprsz00_2698;
																								BgL_exitz00_305 =
																									BgL_exitz00_2697;
																								goto BgL_tagzd21264zd2_307;
																							}
																						}
																				}
																			else
																				{	/* Expand/exit.scm 197 */
																					obj_t BgL_arg1231z00_351;
																					obj_t BgL_arg1232z00_352;

																					{	/* Expand/exit.scm 197 */
																						obj_t BgL_pairz00_2052;

																						BgL_pairz00_2052 =
																							CAR(
																							((obj_t) BgL_cdrzd21278zd2_320));
																						BgL_arg1231z00_351 =
																							CAR(BgL_pairz00_2052);
																					}
																					BgL_arg1232z00_352 =
																						CDR(
																						((obj_t) BgL_cdrzd21278zd2_320));
																					{
																						obj_t BgL_exprsz00_2705;
																						obj_t BgL_exitz00_2704;

																						BgL_exitz00_2704 =
																							BgL_arg1231z00_351;
																						BgL_exprsz00_2705 =
																							BgL_arg1232z00_352;
																						BgL_exprsz00_306 =
																							BgL_exprsz00_2705;
																						BgL_exitz00_305 = BgL_exitz00_2704;
																						goto BgL_tagzd21264zd2_307;
																					}
																				}
																		}
																	else
																		{	/* Expand/exit.scm 197 */
																			obj_t BgL_arg1234z00_355;
																			obj_t BgL_arg1236z00_356;

																			{	/* Expand/exit.scm 197 */
																				obj_t BgL_pairz00_2056;

																				BgL_pairz00_2056 =
																					CAR(((obj_t) BgL_cdrzd21278zd2_320));
																				BgL_arg1234z00_355 =
																					CAR(BgL_pairz00_2056);
																			}
																			BgL_arg1236z00_356 =
																				CDR(((obj_t) BgL_cdrzd21278zd2_320));
																			{
																				obj_t BgL_exprsz00_2712;
																				obj_t BgL_exitz00_2711;

																				BgL_exitz00_2711 = BgL_arg1234z00_355;
																				BgL_exprsz00_2712 = BgL_arg1236z00_356;
																				BgL_exprsz00_306 = BgL_exprsz00_2712;
																				BgL_exitz00_305 = BgL_exitz00_2711;
																				goto BgL_tagzd21264zd2_307;
																			}
																		}
																}
															else
																{	/* Expand/exit.scm 197 */
																	obj_t BgL_arg1242z00_360;
																	obj_t BgL_arg1244z00_361;

																	{	/* Expand/exit.scm 197 */
																		obj_t BgL_pairz00_2060;

																		BgL_pairz00_2060 =
																			CAR(((obj_t) BgL_cdrzd21278zd2_320));
																		BgL_arg1242z00_360 = CAR(BgL_pairz00_2060);
																	}
																	BgL_arg1244z00_361 =
																		CDR(((obj_t) BgL_cdrzd21278zd2_320));
																	{
																		obj_t BgL_exprsz00_2719;
																		obj_t BgL_exitz00_2718;

																		BgL_exitz00_2718 = BgL_arg1242z00_360;
																		BgL_exprsz00_2719 = BgL_arg1244z00_361;
																		BgL_exprsz00_306 = BgL_exprsz00_2719;
																		BgL_exitz00_305 = BgL_exitz00_2718;
																		goto BgL_tagzd21264zd2_307;
																	}
																}
														}
													else
														{	/* Expand/exit.scm 197 */
															obj_t BgL_arg1249z00_364;
															obj_t BgL_arg1252z00_365;

															{	/* Expand/exit.scm 197 */
																obj_t BgL_pairz00_2064;

																BgL_pairz00_2064 =
																	CAR(((obj_t) BgL_cdrzd21278zd2_320));
																BgL_arg1249z00_364 = CAR(BgL_pairz00_2064);
															}
															BgL_arg1252z00_365 =
																CDR(((obj_t) BgL_cdrzd21278zd2_320));
															{
																obj_t BgL_exprsz00_2726;
																obj_t BgL_exitz00_2725;

																BgL_exitz00_2725 = BgL_arg1249z00_364;
																BgL_exprsz00_2726 = BgL_arg1252z00_365;
																BgL_exprsz00_306 = BgL_exprsz00_2726;
																BgL_exitz00_305 = BgL_exitz00_2725;
																goto BgL_tagzd21264zd2_307;
															}
														}
												}
											else
												{	/* Expand/exit.scm 197 */
												BgL_tagzd21267zd2_317:
													return
														BGl_errorz00zz__errorz00(BFALSE,
														BGl_string2595z00zzexpand_exitz00, BgL_xz00_31);
												}
										}
									else
										{	/* Expand/exit.scm 197 */
											obj_t BgL_cdrzd21475zd2_369;

											BgL_cdrzd21475zd2_369 =
												CDR(((obj_t) BgL_cdrzd21278zd2_320));
											if (
												(CAR(
														((obj_t) BgL_cdrzd21278zd2_320)) ==
													CNST_TABLE_REF(22)))
												{	/* Expand/exit.scm 197 */
													if (PAIRP(BgL_cdrzd21475zd2_369))
														{	/* Expand/exit.scm 197 */
															obj_t BgL_cdrzd21480zd2_373;

															BgL_cdrzd21480zd2_373 =
																CDR(BgL_cdrzd21475zd2_369);
															if (PAIRP(BgL_cdrzd21480zd2_373))
																{	/* Expand/exit.scm 197 */
																	obj_t BgL_carzd21484zd2_375;
																	obj_t BgL_cdrzd21485zd2_376;

																	BgL_carzd21484zd2_375 =
																		CAR(BgL_cdrzd21480zd2_373);
																	BgL_cdrzd21485zd2_376 =
																		CDR(BgL_cdrzd21480zd2_373);
																	if (PAIRP(BgL_carzd21484zd2_375))
																		{	/* Expand/exit.scm 197 */
																			if (NULLP(CDR(BgL_carzd21484zd2_375)))
																				{	/* Expand/exit.scm 197 */
																					if (PAIRP(BgL_cdrzd21485zd2_376))
																						{	/* Expand/exit.scm 197 */
																							if (NULLP(CDR
																									(BgL_cdrzd21485zd2_376)))
																								{	/* Expand/exit.scm 197 */
																									BgL_envz00_308 =
																										CAR(BgL_cdrzd21475zd2_369);
																									BgL_exitz00_309 =
																										CAR(BgL_carzd21484zd2_375);
																									BgL_bodyz00_310 =
																										CAR(BgL_cdrzd21485zd2_376);
																									{	/* Expand/exit.scm 234 */
																										obj_t
																											BgL_oldzd2internalzd2_561;
																										BgL_oldzd2internalzd2_561 =
																											BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
																										BGl_internalzd2definitionzf3z21zzexpand_lambdaz00
																											= BTRUE;
																										{	/* Expand/exit.scm 236 */
																											obj_t BgL_ez00_562;

																											BgL_ez00_562 =
																												BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00
																												(BgL_ez00_32);
																											{	/* Expand/exit.scm 236 */
																												obj_t BgL_ebodyz00_563;

																												BgL_ebodyz00_563 =
																													BGL_PROCEDURE_CALL2
																													(BgL_ez00_562,
																													BgL_bodyz00_310,
																													BgL_ez00_562);
																												{	/* Expand/exit.scm 237 */

																													BGl_internalzd2definitionzf3z21zzexpand_lambdaz00
																														=
																														BgL_oldzd2internalzd2_561;
																													BgL_envz00_1286 =
																														BgL_envz00_308;
																													BgL_exitz00_1287 =
																														BgL_exitz00_309;
																													BgL_bodyz00_1288 =
																														BgL_ebodyz00_563;
																													{	/* Expand/exit.scm 164 */
																														obj_t
																															BgL_anzd2exitzd2_1290;
																														obj_t
																															BgL_anzd2exitdzd2_1291;
																														obj_t
																															BgL_valz00_1292;
																														obj_t
																															BgL_resz00_1293;
																														obj_t
																															BgL_tracespz00_1294;
																														BgL_anzd2exitzd2_1290
																															=
																															BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																															(BGl_gensymz00zz__r4_symbols_6_4z00
																															(CNST_TABLE_REF
																																(3)));
																														BgL_anzd2exitdzd2_1291
																															=
																															BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																															(BGl_gensymz00zz__r4_symbols_6_4z00
																															(CNST_TABLE_REF
																																(4)));
																														BgL_valz00_1292 =
																															BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																															(BGl_gensymz00zz__r4_symbols_6_4z00
																															(CNST_TABLE_REF
																																(5)));
																														BgL_resz00_1293 =
																															BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																															(BGl_gensymz00zz__r4_symbols_6_4z00
																															(CNST_TABLE_REF
																																(6)));
																														BgL_tracespz00_1294
																															=
																															BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																															(BGl_gensymz00zz__r4_symbols_6_4z00
																															(CNST_TABLE_REF
																																(7)));
																														{	/* Expand/exit.scm 169 */
																															obj_t
																																BgL_newz00_1295;
																															{	/* Expand/exit.scm 170 */
																																obj_t
																																	BgL_arg2164z00_1296;
																																{	/* Expand/exit.scm 170 */
																																	obj_t
																																		BgL_arg2165z00_1297;
																																	{	/* Expand/exit.scm 170 */
																																		obj_t
																																			BgL_arg2166z00_1298;
																																		obj_t
																																			BgL_arg2167z00_1299;
																																		BgL_arg2166z00_1298
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_anzd2exitzd2_1290,
																																			BNIL);
																																		{	/* Expand/exit.scm 172 */
																																			obj_t
																																				BgL_arg2168z00_1300;
																																			{	/* Expand/exit.scm 172 */
																																				obj_t
																																					BgL_arg2169z00_1301;
																																				{	/* Expand/exit.scm 172 */
																																					obj_t
																																						BgL_arg2170z00_1302;
																																					obj_t
																																						BgL_arg2171z00_1303;
																																					{	/* Expand/exit.scm 172 */
																																						obj_t
																																							BgL_arg2172z00_1304;
																																						{	/* Expand/exit.scm 172 */
																																							obj_t
																																								BgL_arg2173z00_1305;
																																							{	/* Expand/exit.scm 172 */
																																								obj_t
																																									BgL_arg2174z00_1306;
																																								BgL_arg2174z00_1306
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BINT
																																									(1L),
																																									BNIL);
																																								BgL_arg2173z00_1305
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_anzd2exitzd2_1290,
																																									BgL_arg2174z00_1306);
																																							}
																																							BgL_arg2172z00_1304
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_envz00_1286,
																																								BgL_arg2173z00_1305);
																																						}
																																						BgL_arg2170z00_1302
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(8),
																																							BgL_arg2172z00_1304);
																																					}
																																					{	/* Expand/exit.scm 173 */
																																						obj_t
																																							BgL_arg2175z00_1307;
																																						{	/* Expand/exit.scm 173 */
																																							obj_t
																																								BgL_arg2176z00_1308;
																																							{	/* Expand/exit.scm 173 */
																																								obj_t
																																									BgL_arg2177z00_1309;
																																								obj_t
																																									BgL_arg2178z00_1310;
																																								{	/* Expand/exit.scm 173 */
																																									obj_t
																																										BgL_arg2179z00_1311;
																																									{	/* Expand/exit.scm 173 */
																																										obj_t
																																											BgL_arg2180z00_1312;
																																										{	/* Expand/exit.scm 173 */
																																											obj_t
																																												BgL_arg2181z00_1313;
																																											{	/* Expand/exit.scm 173 */
																																												obj_t
																																													BgL_arg2182z00_1314;
																																												BgL_arg2182z00_1314
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_envz00_1286,
																																													BNIL);
																																												BgL_arg2181z00_1313
																																													=
																																													MAKE_YOUNG_PAIR
																																													(CNST_TABLE_REF
																																													(9),
																																													BgL_arg2182z00_1314);
																																											}
																																											BgL_arg2180z00_1312
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg2181z00_1313,
																																												BNIL);
																																										}
																																										BgL_arg2179z00_1311
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_exitz00_1287,
																																											BgL_arg2180z00_1312);
																																									}
																																									BgL_arg2177z00_1309
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg2179z00_1311,
																																										BNIL);
																																								}
																																								{	/* Expand/exit.scm 174 */
																																									obj_t
																																										BgL_arg2183z00_1315;
																																									{	/* Expand/exit.scm 174 */
																																										obj_t
																																											BgL_arg2184z00_1316;
																																										{	/* Expand/exit.scm 174 */
																																											obj_t
																																												BgL_arg2185z00_1317;
																																											obj_t
																																												BgL_arg2186z00_1318;
																																											{	/* Expand/exit.scm 174 */
																																												obj_t
																																													BgL_arg2187z00_1319;
																																												{	/* Expand/exit.scm 174 */
																																													obj_t
																																														BgL_arg2188z00_1320;
																																													BgL_arg2188z00_1320
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_bodyz00_1288,
																																														BNIL);
																																													BgL_arg2187z00_1319
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_resz00_1293,
																																														BgL_arg2188z00_1320);
																																												}
																																												BgL_arg2185z00_1317
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg2187z00_1319,
																																													BNIL);
																																											}
																																											{	/* Expand/exit.scm 175 */
																																												obj_t
																																													BgL_arg2189z00_1321;
																																												obj_t
																																													BgL_arg2190z00_1322;
																																												{	/* Expand/exit.scm 175 */
																																													obj_t
																																														BgL_arg2191z00_1323;
																																													BgL_arg2191z00_1323
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_envz00_1286,
																																														BNIL);
																																													BgL_arg2189z00_1321
																																														=
																																														MAKE_YOUNG_PAIR
																																														(CNST_TABLE_REF
																																														(10),
																																														BgL_arg2191z00_1323);
																																												}
																																												BgL_arg2190z00_1322
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_resz00_1293,
																																													BNIL);
																																												BgL_arg2186z00_1318
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg2189z00_1321,
																																													BgL_arg2190z00_1322);
																																											}
																																											BgL_arg2184z00_1316
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg2185z00_1317,
																																												BgL_arg2186z00_1318);
																																										}
																																										BgL_arg2183z00_1315
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(11),
																																											BgL_arg2184z00_1316);
																																									}
																																									BgL_arg2178z00_1310
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg2183z00_1315,
																																										BNIL);
																																								}
																																								BgL_arg2176z00_1308
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg2177z00_1309,
																																									BgL_arg2178z00_1310);
																																							}
																																							BgL_arg2175z00_1307
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(11),
																																								BgL_arg2176z00_1308);
																																						}
																																						BgL_arg2171z00_1303
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2175z00_1307,
																																							BNIL);
																																					}
																																					BgL_arg2169z00_1301
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2170z00_1302,
																																						BgL_arg2171z00_1303);
																																				}
																																				BgL_arg2168z00_1300
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(12),
																																					BgL_arg2169z00_1301);
																																			}
																																			BgL_arg2167z00_1299
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2168z00_1300,
																																				BNIL);
																																		}
																																		BgL_arg2165z00_1297
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2166z00_1298,
																																			BgL_arg2167z00_1299);
																																	}
																																	BgL_arg2164z00_1296
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(2),
																																		BgL_arg2165z00_1297);
																																}
																																BgL_newz00_1295
																																	=
																																	BGl_addzd2traceze70z35zzexpand_exitz00
																																	(BgL_tracespz00_1294,
																																	BgL_arg2164z00_1296);
																															}
																															return
																																BGl_replacez12z12zztools_miscz00
																																(BgL_xz00_31,
																																BgL_newz00_1295);
																														}
																													}
																												}
																											}
																										}
																									}
																								}
																							else
																								{	/* Expand/exit.scm 197 */
																									obj_t BgL_cdrzd21515zd2_386;

																									{	/* Expand/exit.scm 197 */
																										obj_t BgL_pairz00_2078;

																										BgL_pairz00_2078 =
																											CDR(
																											((obj_t) BgL_xz00_31));
																										BgL_cdrzd21515zd2_386 =
																											CDR(BgL_pairz00_2078);
																									}
																									{	/* Expand/exit.scm 197 */
																										obj_t BgL_cdrzd21524zd2_387;

																										BgL_cdrzd21524zd2_387 =
																											CDR(
																											((obj_t)
																												BgL_cdrzd21515zd2_386));
																										{	/* Expand/exit.scm 197 */
																											obj_t
																												BgL_cdrzd21533zd2_388;
																											BgL_cdrzd21533zd2_388 =
																												CDR(((obj_t)
																													BgL_cdrzd21524zd2_387));
																											{	/* Expand/exit.scm 197 */
																												obj_t
																													BgL_cdrzd21544zd2_389;
																												BgL_cdrzd21544zd2_389 =
																													CDR(((obj_t)
																														BgL_cdrzd21533zd2_388));
																												if (PAIRP
																													(BgL_cdrzd21544zd2_389))
																													{	/* Expand/exit.scm 197 */
																														if (NULLP(CDR
																																(BgL_cdrzd21544zd2_389)))
																															{	/* Expand/exit.scm 197 */
																																obj_t
																																	BgL_arg1316z00_393;
																																obj_t
																																	BgL_arg1317z00_394;
																																obj_t
																																	BgL_arg1318z00_395;
																																obj_t
																																	BgL_arg1319z00_396;
																																BgL_arg1316z00_393
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd21515zd2_386));
																																{	/* Expand/exit.scm 197 */
																																	obj_t
																																		BgL_pairz00_2085;
																																	BgL_pairz00_2085
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21524zd2_387));
																																	BgL_arg1317z00_394
																																		=
																																		CAR
																																		(BgL_pairz00_2085);
																																}
																																BgL_arg1318z00_395
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd21533zd2_388));
																																BgL_arg1319z00_396
																																	=
																																	CAR
																																	(BgL_cdrzd21544zd2_389);
																																BgL_envz00_312 =
																																	BgL_arg1316z00_393;
																																BgL_exitz00_313
																																	=
																																	BgL_arg1317z00_394;
																																BgL_bodyz00_314
																																	=
																																	BgL_arg1318z00_395;
																																BgL_onexitz00_315
																																	=
																																	BgL_arg1319z00_396;
																																{	/* Expand/exit.scm 242 */
																																	obj_t
																																		BgL_oldzd2internalzd2_564;
																																	BgL_oldzd2internalzd2_564
																																		=
																																		BGl_internalzd2definitionzf3z21zzexpand_lambdaz00;
																																	BGl_internalzd2definitionzf3z21zzexpand_lambdaz00
																																		= BTRUE;
																																	{	/* Expand/exit.scm 244 */
																																		obj_t
																																			BgL_ez00_565;
																																		BgL_ez00_565
																																			=
																																			BGl_internalzd2beginzd2expanderz00zzexpand_lambdaz00
																																			(BgL_ez00_32);
																																		{	/* Expand/exit.scm 244 */
																																			obj_t
																																				BgL_ebodyz00_566;
																																			BgL_ebodyz00_566
																																				=
																																				BGL_PROCEDURE_CALL2
																																				(BgL_ez00_565,
																																				BgL_bodyz00_314,
																																				BgL_ez00_565);
																																			{	/* Expand/exit.scm 245 */
																																				obj_t
																																					BgL_eonexitz00_567;
																																				BgL_eonexitz00_567
																																					=
																																					BGL_PROCEDURE_CALL2
																																					(BgL_ez00_565,
																																					BgL_onexitz00_315,
																																					BgL_ez00_565);
																																				{	/* Expand/exit.scm 246 */

																																					BGl_internalzd2definitionzf3z21zzexpand_lambdaz00
																																						=
																																						BgL_oldzd2internalzd2_564;
																																					BgL_envz00_1329
																																						=
																																						BgL_envz00_312;
																																					BgL_exitz00_1330
																																						=
																																						BgL_exitz00_313;
																																					BgL_bodyz00_1331
																																						=
																																						BgL_ebodyz00_566;
																																					BgL_onexitz00_1332
																																						=
																																						BgL_eonexitz00_567;
																																					{	/* Expand/exit.scm 180 */
																																						obj_t
																																							BgL_anzd2exitzd2_1334;
																																						obj_t
																																							BgL_anzd2exitdzd2_1335;
																																						obj_t
																																							BgL_valz00_1336;
																																						obj_t
																																							BgL_resz00_1337;
																																						obj_t
																																							BgL_tracespz00_1338;
																																						BgL_anzd2exitzd2_1334
																																							=
																																							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																							(BGl_gensymz00zz__r4_symbols_6_4z00
																																							(CNST_TABLE_REF
																																								(3)));
																																						BgL_anzd2exitdzd2_1335
																																							=
																																							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																							(BGl_gensymz00zz__r4_symbols_6_4z00
																																							(CNST_TABLE_REF
																																								(4)));
																																						BgL_valz00_1336
																																							=
																																							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																							(BGl_gensymz00zz__r4_symbols_6_4z00
																																							(CNST_TABLE_REF
																																								(5)));
																																						BgL_resz00_1337
																																							=
																																							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																							(BGl_gensymz00zz__r4_symbols_6_4z00
																																							(CNST_TABLE_REF
																																								(6)));
																																						BgL_tracespz00_1338
																																							=
																																							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																							(BGl_gensymz00zz__r4_symbols_6_4z00
																																							(CNST_TABLE_REF
																																								(7)));
																																						{	/* Expand/exit.scm 185 */
																																							obj_t
																																								BgL_newz00_1339;
																																							{	/* Expand/exit.scm 186 */
																																								obj_t
																																									BgL_arg2199z00_1340;
																																								{	/* Expand/exit.scm 186 */
																																									obj_t
																																										BgL_arg2200z00_1341;
																																									{	/* Expand/exit.scm 186 */
																																										obj_t
																																											BgL_arg2201z00_1342;
																																										obj_t
																																											BgL_arg2202z00_1343;
																																										BgL_arg2201z00_1342
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_anzd2exitzd2_1334,
																																											BNIL);
																																										{	/* Expand/exit.scm 188 */
																																											obj_t
																																												BgL_arg2203z00_1344;
																																											obj_t
																																												BgL_arg2204z00_1345;
																																											{	/* Expand/exit.scm 188 */
																																												obj_t
																																													BgL_arg2205z00_1346;
																																												{	/* Expand/exit.scm 188 */
																																													obj_t
																																														BgL_arg2206z00_1347;
																																													obj_t
																																														BgL_arg2207z00_1348;
																																													{	/* Expand/exit.scm 188 */
																																														obj_t
																																															BgL_arg2208z00_1349;
																																														{	/* Expand/exit.scm 188 */
																																															obj_t
																																																BgL_arg2209z00_1350;
																																															{	/* Expand/exit.scm 188 */
																																																obj_t
																																																	BgL_arg2210z00_1351;
																																																BgL_arg2210z00_1351
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BINT
																																																	(1L),
																																																	BNIL);
																																																BgL_arg2209z00_1350
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_anzd2exitzd2_1334,
																																																	BgL_arg2210z00_1351);
																																															}
																																															BgL_arg2208z00_1349
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_envz00_1329,
																																																BgL_arg2209z00_1350);
																																														}
																																														BgL_arg2206z00_1347
																																															=
																																															MAKE_YOUNG_PAIR
																																															(CNST_TABLE_REF
																																															(8),
																																															BgL_arg2208z00_1349);
																																													}
																																													{	/* Expand/exit.scm 189 */
																																														obj_t
																																															BgL_arg2211z00_1352;
																																														{	/* Expand/exit.scm 189 */
																																															obj_t
																																																BgL_arg2212z00_1353;
																																															{	/* Expand/exit.scm 189 */
																																																obj_t
																																																	BgL_arg2213z00_1354;
																																																obj_t
																																																	BgL_arg2214z00_1355;
																																																{	/* Expand/exit.scm 189 */
																																																	obj_t
																																																		BgL_arg2215z00_1356;
																																																	{	/* Expand/exit.scm 189 */
																																																		obj_t
																																																			BgL_arg2216z00_1357;
																																																		{	/* Expand/exit.scm 189 */
																																																			obj_t
																																																				BgL_arg2217z00_1358;
																																																			{	/* Expand/exit.scm 189 */
																																																				obj_t
																																																					BgL_arg2218z00_1359;
																																																				BgL_arg2218z00_1359
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_envz00_1329,
																																																					BNIL);
																																																				BgL_arg2217z00_1358
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(CNST_TABLE_REF
																																																					(9),
																																																					BgL_arg2218z00_1359);
																																																			}
																																																			BgL_arg2216z00_1357
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_arg2217z00_1358,
																																																				BNIL);
																																																		}
																																																		BgL_arg2215z00_1356
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_exitz00_1330,
																																																			BgL_arg2216z00_1357);
																																																	}
																																																	BgL_arg2213z00_1354
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg2215z00_1356,
																																																		BNIL);
																																																}
																																																{	/* Expand/exit.scm 190 */
																																																	obj_t
																																																		BgL_arg2219z00_1360;
																																																	{	/* Expand/exit.scm 190 */
																																																		obj_t
																																																			BgL_arg2220z00_1361;
																																																		{	/* Expand/exit.scm 190 */
																																																			obj_t
																																																				BgL_arg2221z00_1362;
																																																			obj_t
																																																				BgL_arg2222z00_1363;
																																																			{	/* Expand/exit.scm 190 */
																																																				obj_t
																																																					BgL_arg2223z00_1364;
																																																				{	/* Expand/exit.scm 190 */
																																																					obj_t
																																																						BgL_arg2224z00_1365;
																																																					BgL_arg2224z00_1365
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_bodyz00_1331,
																																																						BNIL);
																																																					BgL_arg2223z00_1364
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_resz00_1337,
																																																						BgL_arg2224z00_1365);
																																																				}
																																																				BgL_arg2221z00_1362
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_arg2223z00_1364,
																																																					BNIL);
																																																			}
																																																			{	/* Expand/exit.scm 191 */
																																																				obj_t
																																																					BgL_arg2225z00_1366;
																																																				obj_t
																																																					BgL_arg2226z00_1367;
																																																				{	/* Expand/exit.scm 191 */
																																																					obj_t
																																																						BgL_arg2227z00_1368;
																																																					BgL_arg2227z00_1368
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_envz00_1329,
																																																						BNIL);
																																																					BgL_arg2225z00_1366
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(CNST_TABLE_REF
																																																						(10),
																																																						BgL_arg2227z00_1368);
																																																				}
																																																				BgL_arg2226z00_1367
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_resz00_1337,
																																																					BNIL);
																																																				BgL_arg2222z00_1363
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_arg2225z00_1366,
																																																					BgL_arg2226z00_1367);
																																																			}
																																																			BgL_arg2220z00_1361
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_arg2221z00_1362,
																																																				BgL_arg2222z00_1363);
																																																		}
																																																		BgL_arg2219z00_1360
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(CNST_TABLE_REF
																																																			(11),
																																																			BgL_arg2220z00_1361);
																																																	}
																																																	BgL_arg2214z00_1355
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg2219z00_1360,
																																																		BNIL);
																																																}
																																																BgL_arg2212z00_1353
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg2213z00_1354,
																																																	BgL_arg2214z00_1355);
																																															}
																																															BgL_arg2211z00_1352
																																																=
																																																MAKE_YOUNG_PAIR
																																																(CNST_TABLE_REF
																																																(11),
																																																BgL_arg2212z00_1353);
																																														}
																																														BgL_arg2207z00_1348
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_arg2211z00_1352,
																																															BNIL);
																																													}
																																													BgL_arg2205z00_1346
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg2206z00_1347,
																																														BgL_arg2207z00_1348);
																																												}
																																												BgL_arg2203z00_1344
																																													=
																																													MAKE_YOUNG_PAIR
																																													(CNST_TABLE_REF
																																													(12),
																																													BgL_arg2205z00_1346);
																																											}
																																											{	/* Expand/exit.scm 186 */
																																												obj_t
																																													BgL_arg2228z00_1369;
																																												BgL_arg2228z00_1369
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_onexitz00_1332,
																																													BNIL);
																																												BgL_arg2204z00_1345
																																													=
																																													MAKE_YOUNG_PAIR
																																													(CNST_TABLE_REF
																																													(1),
																																													BgL_arg2228z00_1369);
																																											}
																																											BgL_arg2202z00_1343
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg2203z00_1344,
																																												BgL_arg2204z00_1345);
																																										}
																																										BgL_arg2200z00_1341
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg2201z00_1342,
																																											BgL_arg2202z00_1343);
																																									}
																																									BgL_arg2199z00_1340
																																										=
																																										MAKE_YOUNG_PAIR
																																										(CNST_TABLE_REF
																																										(2),
																																										BgL_arg2200z00_1341);
																																								}
																																								BgL_newz00_1339
																																									=
																																									BGl_addzd2traceze70z35zzexpand_exitz00
																																									(BgL_tracespz00_1338,
																																									BgL_arg2199z00_1340);
																																							}
																																							BGl_replacez12z12zztools_miscz00
																																								(BgL_xz00_31,
																																								BgL_newz00_1339);
																																							return
																																								BgL_xz00_31;
																																						}
																																					}
																																				}
																																			}
																																		}
																																	}
																																}
																															}
																														else
																															{	/* Expand/exit.scm 197 */
																																goto
																																	BgL_tagzd21267zd2_317;
																															}
																													}
																												else
																													{	/* Expand/exit.scm 197 */
																														goto
																															BgL_tagzd21267zd2_317;
																													}
																											}
																										}
																									}
																								}
																						}
																					else
																						{	/* Expand/exit.scm 197 */
																							goto BgL_tagzd21267zd2_317;
																						}
																				}
																			else
																				{	/* Expand/exit.scm 197 */
																					goto BgL_tagzd21267zd2_317;
																				}
																		}
																	else
																		{	/* Expand/exit.scm 197 */
																			goto BgL_tagzd21267zd2_317;
																		}
																}
															else
																{	/* Expand/exit.scm 197 */
																	goto BgL_tagzd21267zd2_317;
																}
														}
													else
														{	/* Expand/exit.scm 197 */
															goto BgL_tagzd21267zd2_317;
														}
												}
											else
												{	/* Expand/exit.scm 197 */
													goto BgL_tagzd21267zd2_317;
												}
										}
								}
							else
								{	/* Expand/exit.scm 197 */
									goto BgL_tagzd21267zd2_317;
								}
						}
					else
						{	/* Expand/exit.scm 197 */
							goto BgL_tagzd21267zd2_317;
						}
				}
			}
		}

	}



/* trace?~1 */
	bool_t BGl_tracezf3ze71z14zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 128 */
			{	/* Expand/exit.scm 127 */
				bool_t BgL_test2676z00_2903;

				{	/* Expand/exit.scm 127 */
					int BgL_arg2078z00_1194;

					BgL_arg2078z00_1194 = BGl_bigloozd2compilerzd2debugz00zz__paramz00();
					BgL_test2676z00_2903 = ((long) (BgL_arg2078z00_1194) > 0L);
				}
				if (BgL_test2676z00_2903)
					{	/* Expand/exit.scm 128 */
						obj_t BgL_arg2077z00_1193;

						BgL_arg2077z00_1193 = BGl_thezd2backendzd2zzbackend_backendz00();
						return
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg2077z00_1193)))->
							BgL_tracezd2supportzd2);
					}
				else
					{	/* Expand/exit.scm 127 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* add-trace~0 */
	obj_t BGl_addzd2traceze70z35zzexpand_exitz00(obj_t BgL_tracespz00_1195,
		obj_t BgL_bodyz00_1196)
	{
		{	/* Expand/exit.scm 137 */
			if (BGl_tracezf3ze71z14zzexpand_exitz00())
				{	/* Expand/exit.scm 132 */
					obj_t BgL_tmpz00_1199;

					BgL_tmpz00_1199 =
						BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
						(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(23)));
					{	/* Expand/exit.scm 133 */
						obj_t BgL_arg2081z00_1200;

						{	/* Expand/exit.scm 133 */
							obj_t BgL_arg2082z00_1201;
							obj_t BgL_arg2083z00_1202;

							{	/* Expand/exit.scm 133 */
								obj_t BgL_arg2084z00_1203;

								{	/* Expand/exit.scm 133 */
									obj_t BgL_arg2086z00_1204;

									{	/* Expand/exit.scm 133 */
										obj_t BgL_arg2087z00_1205;

										BgL_arg2087z00_1205 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(24), BNIL);
										BgL_arg2086z00_1204 =
											MAKE_YOUNG_PAIR(BgL_arg2087z00_1205, BNIL);
									}
									BgL_arg2084z00_1203 =
										MAKE_YOUNG_PAIR(BgL_tracespz00_1195, BgL_arg2086z00_1204);
								}
								BgL_arg2082z00_1201 =
									MAKE_YOUNG_PAIR(BgL_arg2084z00_1203, BNIL);
							}
							{	/* Expand/exit.scm 134 */
								obj_t BgL_arg2088z00_1206;

								{	/* Expand/exit.scm 134 */
									obj_t BgL_arg2089z00_1207;

									{	/* Expand/exit.scm 134 */
										obj_t BgL_arg2090z00_1208;
										obj_t BgL_arg2091z00_1209;

										{	/* Expand/exit.scm 134 */
											obj_t BgL_arg2093z00_1210;

											{	/* Expand/exit.scm 134 */
												obj_t BgL_arg2094z00_1211;

												BgL_arg2094z00_1211 =
													MAKE_YOUNG_PAIR(BgL_bodyz00_1196, BNIL);
												BgL_arg2093z00_1210 =
													MAKE_YOUNG_PAIR(BgL_tmpz00_1199, BgL_arg2094z00_1211);
											}
											BgL_arg2090z00_1208 =
												MAKE_YOUNG_PAIR(BgL_arg2093z00_1210, BNIL);
										}
										{	/* Expand/exit.scm 135 */
											obj_t BgL_arg2095z00_1212;
											obj_t BgL_arg2096z00_1213;

											{	/* Expand/exit.scm 135 */
												obj_t BgL_arg2097z00_1214;

												BgL_arg2097z00_1214 =
													MAKE_YOUNG_PAIR(BgL_tracespz00_1195, BNIL);
												BgL_arg2095z00_1212 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(25),
													BgL_arg2097z00_1214);
											}
											BgL_arg2096z00_1213 =
												MAKE_YOUNG_PAIR(BgL_tmpz00_1199, BNIL);
											BgL_arg2091z00_1209 =
												MAKE_YOUNG_PAIR(BgL_arg2095z00_1212,
												BgL_arg2096z00_1213);
										}
										BgL_arg2089z00_1207 =
											MAKE_YOUNG_PAIR(BgL_arg2090z00_1208, BgL_arg2091z00_1209);
									}
									BgL_arg2088z00_1206 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg2089z00_1207);
								}
								BgL_arg2083z00_1202 =
									MAKE_YOUNG_PAIR(BgL_arg2088z00_1206, BNIL);
							}
							BgL_arg2081z00_1200 =
								MAKE_YOUNG_PAIR(BgL_arg2082z00_1201, BgL_arg2083z00_1202);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg2081z00_1200);
					}
				}
			else
				{	/* Expand/exit.scm 131 */
					return BgL_bodyz00_1196;
				}
		}

	}



/* find-in-body~0 */
	bool_t BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00(obj_t BgL_kz00_568,
		obj_t BgL_bodyz00_569)
	{
		{	/* Expand/exit.scm 73 */
		BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00:
			if ((BgL_bodyz00_569 == BgL_kz00_568))
				{	/* Expand/exit.scm 68 */
					return ((bool_t) 1);
				}
			else
				{	/* Expand/exit.scm 68 */
					if (PAIRP(BgL_bodyz00_569))
						{	/* Expand/exit.scm 70 */
							if ((CAR(BgL_bodyz00_569) == CNST_TABLE_REF(26)))
								{	/* Expand/exit.scm 71 */
									return ((bool_t) 0);
								}
							else
								{	/* Expand/exit.scm 72 */
									bool_t BgL__ortest_1053z00_574;

									BgL__ortest_1053z00_574 =
										BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00(BgL_kz00_568,
										CAR(BgL_bodyz00_569));
									if (BgL__ortest_1053z00_574)
										{	/* Expand/exit.scm 72 */
											return BgL__ortest_1053z00_574;
										}
									else
										{
											obj_t BgL_bodyz00_2946;

											BgL_bodyz00_2946 = CDR(BgL_bodyz00_569);
											BgL_bodyz00_569 = BgL_bodyz00_2946;
											goto BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00;
										}
								}
						}
					else
						{	/* Expand/exit.scm 70 */
							return ((bool_t) 0);
						}
				}
		}

	}



/* tailrec?~0 */
	bool_t BGl_tailreczf3ze70z14zzexpand_exitz00(obj_t BgL_exitz00_602,
		obj_t BgL_bodyz00_603)
	{
		{	/* Expand/exit.scm 103 */
		BGl_tailreczf3ze70z14zzexpand_exitz00:
			{
				obj_t BgL_exitz00_578;
				obj_t BgL_bodyz00_579;

				{
					obj_t BgL_bindingsz00_613;
					obj_t BgL_bodyz00_614;

					if (PAIRP(BgL_bodyz00_603))
						{	/* Expand/exit.scm 103 */
							obj_t BgL_cdrzd2492zd2_625;

							BgL_cdrzd2492zd2_625 = CDR(((obj_t) BgL_bodyz00_603));
							if ((CAR(((obj_t) BgL_bodyz00_603)) == BgL_exitz00_602))
								{	/* Expand/exit.scm 103 */
									if (PAIRP(BgL_cdrzd2492zd2_625))
										{	/* Expand/exit.scm 103 */
											if (NULLP(CDR(BgL_cdrzd2492zd2_625)))
												{	/* Expand/exit.scm 103 */
													if (BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00
														(BgL_exitz00_602, CAR(BgL_cdrzd2492zd2_625)))
														{	/* Expand/exit.scm 83 */
															return ((bool_t) 0);
														}
													else
														{	/* Expand/exit.scm 83 */
															return ((bool_t) 1);
														}
												}
											else
												{	/* Expand/exit.scm 103 */
													if (
														(CAR(
																((obj_t) BgL_bodyz00_603)) ==
															CNST_TABLE_REF(12)))
														{	/* Expand/exit.scm 103 */
															if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
																(BgL_cdrzd2492zd2_625))
																{	/* Expand/exit.scm 103 */
																	BgL_exitz00_578 = BgL_exitz00_602;
																	BgL_bodyz00_579 = BgL_cdrzd2492zd2_625;
																BgL_zc3z04anonymousza31700ze3z87_580:
																	{	/* Expand/exit.scm 76 */
																		obj_t BgL_ydobz00_581;

																		BgL_ydobz00_581 =
																			bgl_reverse(BgL_bodyz00_579);
																		{	/* Expand/exit.scm 77 */
																			bool_t BgL_test2689z00_2972;

																			{
																				obj_t BgL_l1063z00_595;

																				BgL_l1063z00_595 =
																					CDR(((obj_t) BgL_ydobz00_581));
																			BgL_zc3z04anonymousza31706ze3z87_596:
																				if (NULLP(BgL_l1063z00_595))
																					{	/* Expand/exit.scm 77 */
																						BgL_test2689z00_2972 = ((bool_t) 0);
																					}
																				else
																					{	/* Expand/exit.scm 77 */
																						bool_t BgL__ortest_1066z00_598;

																						BgL__ortest_1066z00_598 =
																							BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00
																							(BgL_exitz00_578,
																							CAR(((obj_t) BgL_l1063z00_595)));
																						if (BgL__ortest_1066z00_598)
																							{	/* Expand/exit.scm 77 */
																								BgL_test2689z00_2972 =
																									BgL__ortest_1066z00_598;
																							}
																						else
																							{
																								obj_t BgL_l1063z00_2979;

																								BgL_l1063z00_2979 =
																									CDR(
																									((obj_t) BgL_l1063z00_595));
																								BgL_l1063z00_595 =
																									BgL_l1063z00_2979;
																								goto
																									BgL_zc3z04anonymousza31706ze3z87_596;
																							}
																					}
																			}
																			if (BgL_test2689z00_2972)
																				{	/* Expand/exit.scm 77 */
																					return ((bool_t) 0);
																				}
																			else
																				{	/* Expand/exit.scm 78 */
																					obj_t BgL_arg1705z00_592;

																					BgL_arg1705z00_592 =
																						CAR(((obj_t) BgL_ydobz00_581));
																					{
																						obj_t BgL_bodyz00_2987;
																						obj_t BgL_exitz00_2986;

																						BgL_exitz00_2986 = BgL_exitz00_578;
																						BgL_bodyz00_2987 =
																							BgL_arg1705z00_592;
																						BgL_bodyz00_603 = BgL_bodyz00_2987;
																						BgL_exitz00_602 = BgL_exitz00_2986;
																						goto
																							BGl_tailreczf3ze70z14zzexpand_exitz00;
																					}
																				}
																		}
																	}
																}
															else
																{	/* Expand/exit.scm 103 */
																	return ((bool_t) 0);
																}
														}
													else
														{	/* Expand/exit.scm 103 */
															obj_t BgL_cdrzd2559zd2_657;

															BgL_cdrzd2559zd2_657 =
																CDR(((obj_t) BgL_bodyz00_603));
															if (
																(CAR(
																		((obj_t) BgL_bodyz00_603)) ==
																	CNST_TABLE_REF(27)))
																{	/* Expand/exit.scm 103 */
																	obj_t BgL_cdrzd2565zd2_660;

																	BgL_cdrzd2565zd2_660 =
																		CDR(((obj_t) BgL_cdrzd2559zd2_657));
																	if (PAIRP(BgL_cdrzd2565zd2_660))
																		{	/* Expand/exit.scm 103 */
																			obj_t BgL_cdrzd2571zd2_662;

																			BgL_cdrzd2571zd2_662 =
																				CDR(BgL_cdrzd2565zd2_660);
																			if (PAIRP(BgL_cdrzd2571zd2_662))
																				{	/* Expand/exit.scm 103 */
																					if (NULLP(CDR(BgL_cdrzd2571zd2_662)))
																						{	/* Expand/exit.scm 103 */
																							obj_t BgL_arg1748z00_666;
																							obj_t BgL_arg1749z00_667;
																							obj_t BgL_arg1750z00_668;

																							BgL_arg1748z00_666 =
																								CAR(
																								((obj_t) BgL_cdrzd2559zd2_657));
																							BgL_arg1749z00_667 =
																								CAR(BgL_cdrzd2565zd2_660);
																							BgL_arg1750z00_668 =
																								CAR(BgL_cdrzd2571zd2_662);
																							if (BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00(BgL_exitz00_602, BgL_arg1748z00_666))
																								{	/* Expand/exit.scm 87 */
																									return ((bool_t) 0);
																								}
																							else
																								{	/* Expand/exit.scm 87 */
																									if (BGl_tailreczf3ze70z14zzexpand_exitz00(BgL_exitz00_602, BgL_arg1749z00_667))
																										{
																											obj_t BgL_bodyz00_3013;

																											BgL_bodyz00_3013 =
																												BgL_arg1750z00_668;
																											BgL_bodyz00_603 =
																												BgL_bodyz00_3013;
																											goto
																												BGl_tailreczf3ze70z14zzexpand_exitz00;
																										}
																									else
																										{	/* Expand/exit.scm 88 */
																											return ((bool_t) 0);
																										}
																								}
																						}
																					else
																						{	/* Expand/exit.scm 103 */
																							return ((bool_t) 0);
																						}
																				}
																			else
																				{	/* Expand/exit.scm 103 */
																					return ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Expand/exit.scm 103 */
																			return ((bool_t) 0);
																		}
																}
															else
																{	/* Expand/exit.scm 103 */
																	obj_t BgL_carzd2661zd2_733;

																	BgL_carzd2661zd2_733 =
																		CAR(((obj_t) BgL_bodyz00_603));
																	{

																		if (
																			(BgL_carzd2661zd2_733 ==
																				CNST_TABLE_REF(11)))
																			{	/* Expand/exit.scm 103 */
																			BgL_kapzd2663zd2_735:
																				{	/* Expand/exit.scm 103 */
																					obj_t BgL_carzd2667zd2_774;

																					BgL_carzd2667zd2_774 =
																						CAR(((obj_t) BgL_cdrzd2559zd2_657));
																					if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2667zd2_774))
																						{	/* Expand/exit.scm 103 */
																							obj_t BgL_arg1839z00_776;

																							BgL_arg1839z00_776 =
																								CDR(
																								((obj_t) BgL_cdrzd2559zd2_657));
																							BgL_bindingsz00_613 =
																								BgL_carzd2667zd2_774;
																							BgL_bodyz00_614 =
																								BgL_arg1839z00_776;
																						BgL_tagzd2476zd2_615:
																							{	/* Expand/exit.scm 91 */
																								bool_t BgL_test2700z00_3025;

																								{
																									obj_t BgL_l1067z00_1088;

																									BgL_l1067z00_1088 =
																										BgL_bindingsz00_613;
																								BgL_zc3z04anonymousza31999ze3z87_1089:
																									if (NULLP
																										(BgL_l1067z00_1088))
																										{	/* Expand/exit.scm 91 */
																											BgL_test2700z00_3025 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Expand/exit.scm 91 */
																											bool_t
																												BgL__ortest_1079z00_1091;
																											BgL__ortest_1079z00_1091 =
																												BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00
																												(BgL_exitz00_602,
																												CAR(((obj_t)
																														BgL_l1067z00_1088)));
																											if (BgL__ortest_1079z00_1091)
																												{	/* Expand/exit.scm 91 */
																													BgL_test2700z00_3025 =
																														BgL__ortest_1079z00_1091;
																												}
																											else
																												{
																													obj_t
																														BgL_l1067z00_3032;
																													BgL_l1067z00_3032 =
																														CDR(((obj_t)
																															BgL_l1067z00_1088));
																													BgL_l1067z00_1088 =
																														BgL_l1067z00_3032;
																													goto
																														BgL_zc3z04anonymousza31999ze3z87_1089;
																												}
																										}
																								}
																								if (BgL_test2700z00_3025)
																									{	/* Expand/exit.scm 91 */
																										return ((bool_t) 0);
																									}
																								else
																									{
																										obj_t BgL_bodyz00_3036;
																										obj_t BgL_exitz00_3035;

																										BgL_exitz00_3035 =
																											BgL_exitz00_602;
																										BgL_bodyz00_3036 =
																											BgL_bodyz00_614;
																										BgL_bodyz00_579 =
																											BgL_bodyz00_3036;
																										BgL_exitz00_578 =
																											BgL_exitz00_3035;
																										goto
																											BgL_zc3z04anonymousza31700ze3z87_580;
																									}
																							}
																						}
																					else
																						{	/* Expand/exit.scm 103 */
																							obj_t BgL_cdrzd2680zd2_777;

																							BgL_cdrzd2680zd2_777 =
																								CDR(((obj_t) BgL_bodyz00_603));
																							if (
																								(CAR(
																										((obj_t) BgL_bodyz00_603))
																									== CNST_TABLE_REF(28)))
																								{	/* Expand/exit.scm 103 */
																									obj_t BgL_carzd2683zd2_780;

																									BgL_carzd2683zd2_780 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd2680zd2_777));
																									if (PAIRP
																										(BgL_carzd2683zd2_780))
																										{	/* Expand/exit.scm 103 */
																											if (NULLP(CDR
																													(BgL_carzd2683zd2_780)))
																												{	/* Expand/exit.scm 103 */
																													obj_t
																														BgL_arg1845z00_784;
																													obj_t
																														BgL_arg1846z00_785;
																													BgL_arg1845z00_784 =
																														CAR
																														(BgL_carzd2683zd2_780);
																													BgL_arg1846z00_785 =
																														CDR(((obj_t)
																															BgL_cdrzd2680zd2_777));
																													if (
																														(BgL_arg1845z00_784
																															==
																															BgL_exitz00_602))
																														{	/* Expand/exit.scm 94 */
																															return ((bool_t)
																																0);
																														}
																													else
																														{
																															obj_t
																																BgL_bodyz00_3057;
																															obj_t
																																BgL_exitz00_3056;
																															BgL_exitz00_3056 =
																																BgL_exitz00_602;
																															BgL_bodyz00_3057 =
																																BgL_arg1846z00_785;
																															BgL_bodyz00_579 =
																																BgL_bodyz00_3057;
																															BgL_exitz00_578 =
																																BgL_exitz00_3056;
																															goto
																																BgL_zc3z04anonymousza31700ze3z87_580;
																														}
																												}
																											else
																												{	/* Expand/exit.scm 103 */
																													return ((bool_t) 0);
																												}
																										}
																									else
																										{	/* Expand/exit.scm 103 */
																											return ((bool_t) 0);
																										}
																								}
																							else
																								{	/* Expand/exit.scm 103 */
																									return
																										(CAR(
																											((obj_t) BgL_bodyz00_603))
																										== CNST_TABLE_REF(26));
																								}
																						}
																				}
																			}
																		else
																			{	/* Expand/exit.scm 103 */
																				if (
																					(BgL_carzd2661zd2_733 ==
																						CNST_TABLE_REF(19)))
																					{	/* Expand/exit.scm 103 */
																						goto BgL_kapzd2663zd2_735;
																					}
																				else
																					{	/* Expand/exit.scm 103 */
																						if (
																							(BgL_carzd2661zd2_733 ==
																								CNST_TABLE_REF(20)))
																							{	/* Expand/exit.scm 103 */
																								goto BgL_kapzd2663zd2_735;
																							}
																						else
																							{	/* Expand/exit.scm 103 */
																								if (
																									(BgL_carzd2661zd2_733 ==
																										CNST_TABLE_REF(21)))
																									{	/* Expand/exit.scm 103 */
																										goto BgL_kapzd2663zd2_735;
																									}
																								else
																									{	/* Expand/exit.scm 103 */
																										obj_t BgL_cdrzd2699zd2_736;

																										BgL_cdrzd2699zd2_736 =
																											CDR(
																											((obj_t)
																												BgL_bodyz00_603));
																										if ((CAR(((obj_t)
																														BgL_bodyz00_603)) ==
																												CNST_TABLE_REF(28)))
																											{	/* Expand/exit.scm 103 */
																												obj_t
																													BgL_carzd2702zd2_739;
																												BgL_carzd2702zd2_739 =
																													CAR(((obj_t)
																														BgL_cdrzd2699zd2_736));
																												if (PAIRP
																													(BgL_carzd2702zd2_739))
																													{	/* Expand/exit.scm 103 */
																														if (NULLP(CDR
																																(BgL_carzd2702zd2_739)))
																															{	/* Expand/exit.scm 103 */
																																obj_t
																																	BgL_arg1808z00_743;
																																obj_t
																																	BgL_arg1812z00_744;
																																BgL_arg1808z00_743
																																	=
																																	CAR
																																	(BgL_carzd2702zd2_739);
																																BgL_arg1812z00_744
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd2699zd2_736));
																																if (
																																	(BgL_arg1808z00_743
																																		==
																																		BgL_exitz00_602))
																																	{	/* Expand/exit.scm 94 */
																																		return (
																																			(bool_t)
																																			0);
																																	}
																																else
																																	{
																																		obj_t
																																			BgL_bodyz00_3091;
																																		obj_t
																																			BgL_exitz00_3090;
																																		BgL_exitz00_3090
																																			=
																																			BgL_exitz00_602;
																																		BgL_bodyz00_3091
																																			=
																																			BgL_arg1812z00_744;
																																		BgL_bodyz00_579
																																			=
																																			BgL_bodyz00_3091;
																																		BgL_exitz00_578
																																			=
																																			BgL_exitz00_3090;
																																		goto
																																			BgL_zc3z04anonymousza31700ze3z87_580;
																																	}
																															}
																														else
																															{	/* Expand/exit.scm 103 */
																																return ((bool_t)
																																	0);
																															}
																													}
																												else
																													{	/* Expand/exit.scm 103 */
																														return ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Expand/exit.scm 103 */
																												return
																													(CAR(
																														((obj_t)
																															BgL_bodyz00_603))
																													==
																													CNST_TABLE_REF(26));
																											}
																									}
																							}
																					}
																			}
																	}
																}
														}
												}
										}
									else
										{	/* Expand/exit.scm 103 */
											if (
												(CAR(((obj_t) BgL_bodyz00_603)) == CNST_TABLE_REF(12)))
												{	/* Expand/exit.scm 103 */
													if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
														(BgL_cdrzd2492zd2_625))
														{
															obj_t BgL_bodyz00_3104;
															obj_t BgL_exitz00_3103;

															BgL_exitz00_3103 = BgL_exitz00_602;
															BgL_bodyz00_3104 = BgL_cdrzd2492zd2_625;
															BgL_bodyz00_579 = BgL_bodyz00_3104;
															BgL_exitz00_578 = BgL_exitz00_3103;
															goto BgL_zc3z04anonymousza31700ze3z87_580;
														}
													else
														{	/* Expand/exit.scm 103 */
															return ((bool_t) 0);
														}
												}
											else
												{	/* Expand/exit.scm 103 */
													return
														(CAR(
															((obj_t) BgL_bodyz00_603)) == CNST_TABLE_REF(26));
												}
										}
								}
							else
								{	/* Expand/exit.scm 103 */
									if ((CAR(((obj_t) BgL_bodyz00_603)) == CNST_TABLE_REF(12)))
										{	/* Expand/exit.scm 103 */
											if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
												(BgL_cdrzd2492zd2_625))
												{
													obj_t BgL_bodyz00_3117;
													obj_t BgL_exitz00_3116;

													BgL_exitz00_3116 = BgL_exitz00_602;
													BgL_bodyz00_3117 = BgL_cdrzd2492zd2_625;
													BgL_bodyz00_579 = BgL_bodyz00_3117;
													BgL_exitz00_578 = BgL_exitz00_3116;
													goto BgL_zc3z04anonymousza31700ze3z87_580;
												}
											else
												{	/* Expand/exit.scm 103 */
													return ((bool_t) 0);
												}
										}
									else
										{	/* Expand/exit.scm 103 */
											obj_t BgL_cdrzd2822zd2_876;

											BgL_cdrzd2822zd2_876 = CDR(((obj_t) BgL_bodyz00_603));
											if (
												(CAR(((obj_t) BgL_bodyz00_603)) == CNST_TABLE_REF(27)))
												{	/* Expand/exit.scm 103 */
													if (PAIRP(BgL_cdrzd2822zd2_876))
														{	/* Expand/exit.scm 103 */
															obj_t BgL_cdrzd2827zd2_880;

															BgL_cdrzd2827zd2_880 = CDR(BgL_cdrzd2822zd2_876);
															if (PAIRP(BgL_cdrzd2827zd2_880))
																{	/* Expand/exit.scm 103 */
																	obj_t BgL_cdrzd2832zd2_882;

																	BgL_cdrzd2832zd2_882 =
																		CDR(BgL_cdrzd2827zd2_880);
																	if (PAIRP(BgL_cdrzd2832zd2_882))
																		{	/* Expand/exit.scm 103 */
																			if (NULLP(CDR(BgL_cdrzd2832zd2_882)))
																				{	/* Expand/exit.scm 103 */
																					obj_t BgL_arg1901z00_886;
																					obj_t BgL_arg1902z00_887;
																					obj_t BgL_arg1903z00_888;

																					BgL_arg1901z00_886 =
																						CAR(BgL_cdrzd2822zd2_876);
																					BgL_arg1902z00_887 =
																						CAR(BgL_cdrzd2827zd2_880);
																					BgL_arg1903z00_888 =
																						CAR(BgL_cdrzd2832zd2_882);
																					if (BGl_findzd2inzd2bodyze70ze7zzexpand_exitz00(BgL_exitz00_602, BgL_arg1901z00_886))
																						{	/* Expand/exit.scm 87 */
																							return ((bool_t) 0);
																						}
																					else
																						{	/* Expand/exit.scm 87 */
																							if (BGl_tailreczf3ze70z14zzexpand_exitz00(BgL_exitz00_602, BgL_arg1902z00_887))
																								{
																									obj_t BgL_bodyz00_3143;

																									BgL_bodyz00_3143 =
																										BgL_arg1903z00_888;
																									BgL_bodyz00_603 =
																										BgL_bodyz00_3143;
																									goto
																										BGl_tailreczf3ze70z14zzexpand_exitz00;
																								}
																							else
																								{	/* Expand/exit.scm 88 */
																									return ((bool_t) 0);
																								}
																						}
																				}
																			else
																				{	/* Expand/exit.scm 103 */
																					return ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Expand/exit.scm 103 */
																			return ((bool_t) 0);
																		}
																}
															else
																{	/* Expand/exit.scm 103 */
																	return ((bool_t) 0);
																}
														}
													else
														{	/* Expand/exit.scm 103 */
															return ((bool_t) 0);
														}
												}
											else
												{	/* Expand/exit.scm 103 */
													obj_t BgL_carzd2942zd2_961;

													BgL_carzd2942zd2_961 = CAR(((obj_t) BgL_bodyz00_603));
													{

														if ((BgL_carzd2942zd2_961 == CNST_TABLE_REF(11)))
															{	/* Expand/exit.scm 103 */
															BgL_kapzd2944zd2_963:
																if (PAIRP(BgL_cdrzd2822zd2_876))
																	{	/* Expand/exit.scm 103 */
																		obj_t BgL_carzd2947zd2_1012;

																		BgL_carzd2947zd2_1012 =
																			CAR(BgL_cdrzd2822zd2_876);
																		if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2947zd2_1012))
																			{
																				obj_t BgL_bodyz00_3155;
																				obj_t BgL_bindingsz00_3154;

																				BgL_bindingsz00_3154 =
																					BgL_carzd2947zd2_1012;
																				BgL_bodyz00_3155 =
																					CDR(BgL_cdrzd2822zd2_876);
																				BgL_bodyz00_614 = BgL_bodyz00_3155;
																				BgL_bindingsz00_613 =
																					BgL_bindingsz00_3154;
																				goto BgL_tagzd2476zd2_615;
																			}
																		else
																			{	/* Expand/exit.scm 103 */
																				obj_t BgL_cdrzd2958zd2_1015;

																				BgL_cdrzd2958zd2_1015 =
																					CDR(((obj_t) BgL_bodyz00_603));
																				if (
																					(CAR(
																							((obj_t) BgL_bodyz00_603)) ==
																						CNST_TABLE_REF(28)))
																					{	/* Expand/exit.scm 103 */
																						obj_t BgL_carzd2961zd2_1018;

																						BgL_carzd2961zd2_1018 =
																							CAR(
																							((obj_t) BgL_cdrzd2958zd2_1015));
																						if (PAIRP(BgL_carzd2961zd2_1018))
																							{	/* Expand/exit.scm 103 */
																								if (NULLP(CDR
																										(BgL_carzd2961zd2_1018)))
																									{	/* Expand/exit.scm 103 */
																										obj_t BgL_arg1967z00_1022;
																										obj_t BgL_arg1968z00_1023;

																										BgL_arg1967z00_1022 =
																											CAR
																											(BgL_carzd2961zd2_1018);
																										BgL_arg1968z00_1023 =
																											CDR(((obj_t)
																												BgL_cdrzd2958zd2_1015));
																										if ((BgL_arg1967z00_1022 ==
																												BgL_exitz00_602))
																											{	/* Expand/exit.scm 94 */
																												return ((bool_t) 0);
																											}
																										else
																											{
																												obj_t BgL_bodyz00_3177;
																												obj_t BgL_exitz00_3176;

																												BgL_exitz00_3176 =
																													BgL_exitz00_602;
																												BgL_bodyz00_3177 =
																													BgL_arg1968z00_1023;
																												BgL_bodyz00_579 =
																													BgL_bodyz00_3177;
																												BgL_exitz00_578 =
																													BgL_exitz00_3176;
																												goto
																													BgL_zc3z04anonymousza31700ze3z87_580;
																											}
																									}
																								else
																									{	/* Expand/exit.scm 103 */
																										return ((bool_t) 0);
																									}
																							}
																						else
																							{	/* Expand/exit.scm 103 */
																								return ((bool_t) 0);
																							}
																					}
																				else
																					{	/* Expand/exit.scm 103 */
																						return
																							(CAR(
																								((obj_t) BgL_bodyz00_603)) ==
																							CNST_TABLE_REF(26));
																					}
																			}
																	}
																else
																	{	/* Expand/exit.scm 103 */
																		return
																			(CAR(
																				((obj_t) BgL_bodyz00_603)) ==
																			CNST_TABLE_REF(26));
																	}
															}
														else
															{	/* Expand/exit.scm 103 */
																if (
																	(BgL_carzd2942zd2_961 == CNST_TABLE_REF(19)))
																	{	/* Expand/exit.scm 103 */
																		goto BgL_kapzd2944zd2_963;
																	}
																else
																	{	/* Expand/exit.scm 103 */
																		if (
																			(BgL_carzd2942zd2_961 ==
																				CNST_TABLE_REF(20)))
																			{	/* Expand/exit.scm 103 */
																				goto BgL_kapzd2944zd2_963;
																			}
																		else
																			{	/* Expand/exit.scm 103 */
																				if (
																					(BgL_carzd2942zd2_961 ==
																						CNST_TABLE_REF(21)))
																					{	/* Expand/exit.scm 103 */
																						goto BgL_kapzd2944zd2_963;
																					}
																				else
																					{	/* Expand/exit.scm 103 */
																						obj_t BgL_cdrzd2983zd2_964;

																						BgL_cdrzd2983zd2_964 =
																							CDR(((obj_t) BgL_bodyz00_603));
																						if (
																							(CAR(
																									((obj_t) BgL_bodyz00_603)) ==
																								CNST_TABLE_REF(28)))
																							{	/* Expand/exit.scm 103 */
																								if (PAIRP(BgL_cdrzd2983zd2_964))
																									{	/* Expand/exit.scm 103 */
																										obj_t BgL_carzd2986zd2_968;

																										BgL_carzd2986zd2_968 =
																											CAR(BgL_cdrzd2983zd2_964);
																										if (PAIRP
																											(BgL_carzd2986zd2_968))
																											{	/* Expand/exit.scm 103 */
																												if (NULLP(CDR
																														(BgL_carzd2986zd2_968)))
																													{	/* Expand/exit.scm 103 */
																														obj_t
																															BgL_arg1940z00_972;
																														obj_t
																															BgL_arg1941z00_973;
																														BgL_arg1940z00_972 =
																															CAR
																															(BgL_carzd2986zd2_968);
																														BgL_arg1941z00_973 =
																															CDR
																															(BgL_cdrzd2983zd2_964);
																														if (
																															(BgL_arg1940z00_972
																																==
																																BgL_exitz00_602))
																															{	/* Expand/exit.scm 94 */
																																return ((bool_t)
																																	0);
																															}
																														else
																															{
																																obj_t
																																	BgL_bodyz00_3215;
																																obj_t
																																	BgL_exitz00_3214;
																																BgL_exitz00_3214
																																	=
																																	BgL_exitz00_602;
																																BgL_bodyz00_3215
																																	=
																																	BgL_arg1941z00_973;
																																BgL_bodyz00_579
																																	=
																																	BgL_bodyz00_3215;
																																BgL_exitz00_578
																																	=
																																	BgL_exitz00_3214;
																																goto
																																	BgL_zc3z04anonymousza31700ze3z87_580;
																															}
																													}
																												else
																													{	/* Expand/exit.scm 103 */
																														return ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Expand/exit.scm 103 */
																												return ((bool_t) 0);
																											}
																									}
																								else
																									{	/* Expand/exit.scm 103 */
																										return ((bool_t) 0);
																									}
																							}
																						else
																							{	/* Expand/exit.scm 103 */
																								return
																									(CAR(
																										((obj_t) BgL_bodyz00_603))
																									== CNST_TABLE_REF(26));
																							}
																					}
																			}
																	}
															}
													}
												}
										}
								}
						}
					else
						{	/* Expand/exit.scm 103 */
							if ((BgL_bodyz00_603 == BgL_exitz00_602))
								{	/* Expand/exit.scm 103 */
									return ((bool_t) 0);
								}
							else
								{	/* Expand/exit.scm 103 */
									return ((bool_t) 1);
								}
						}
				}
			}
		}

	}



/* return!~0 */
	obj_t BGl_returnz12ze70zf5zzexpand_exitz00(obj_t BgL_exitz00_1095,
		obj_t BgL_bodyz00_1096)
	{
		{	/* Expand/exit.scm 124 */
			{
				obj_t BgL_exprsz00_1100;
				obj_t BgL_exprsz00_1106;
				obj_t BgL_exprsz00_1108;

				if (PAIRP(BgL_bodyz00_1096))
					{	/* Expand/exit.scm 124 */
						obj_t BgL_cdrzd21034zd2_1113;

						BgL_cdrzd21034zd2_1113 = CDR(((obj_t) BgL_bodyz00_1096));
						if ((CAR(((obj_t) BgL_bodyz00_1096)) == BgL_exitz00_1095))
							{	/* Expand/exit.scm 124 */
								if (PAIRP(BgL_cdrzd21034zd2_1113))
									{	/* Expand/exit.scm 124 */
										if (NULLP(CDR(BgL_cdrzd21034zd2_1113)))
											{	/* Expand/exit.scm 124 */
												{	/* Expand/exit.scm 108 */
													obj_t BgL_objz00_1898;

													BgL_objz00_1898 = CNST_TABLE_REF(12);
													SET_CAR(BgL_bodyz00_1096, BgL_objz00_1898);
												}
												return BgL_bodyz00_1096;
											}
										else
											{	/* Expand/exit.scm 124 */
												if (
													(CAR(
															((obj_t) BgL_bodyz00_1096)) ==
														CNST_TABLE_REF(12)))
													{	/* Expand/exit.scm 124 */
														BgL_exprsz00_1100 = BgL_cdrzd21034zd2_1113;
													BgL_tagzd21021zd2_1101:
														BGl_returnz12ze70zf5zzexpand_exitz00
															(BgL_exitz00_1095,
															CAR
															(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																(BgL_exprsz00_1100)));
														return BgL_bodyz00_1096;
													}
												else
													{	/* Expand/exit.scm 124 */
														if (
															(CAR(
																	((obj_t) BgL_bodyz00_1096)) ==
																CNST_TABLE_REF(27)))
															{	/* Expand/exit.scm 124 */
																obj_t BgL_cdrzd21062zd2_1126;

																BgL_cdrzd21062zd2_1126 =
																	CDR(((obj_t) BgL_cdrzd21034zd2_1113));
																if (PAIRP(BgL_cdrzd21062zd2_1126))
																	{	/* Expand/exit.scm 124 */
																		obj_t BgL_cdrzd21068zd2_1128;

																		BgL_cdrzd21068zd2_1128 =
																			CDR(BgL_cdrzd21062zd2_1126);
																		if (PAIRP(BgL_cdrzd21068zd2_1128))
																			{	/* Expand/exit.scm 124 */
																				if (NULLP(CDR(BgL_cdrzd21068zd2_1128)))
																					{	/* Expand/exit.scm 124 */
																						obj_t BgL_arg2019z00_1133;
																						obj_t BgL_arg2020z00_1134;

																						BgL_arg2019z00_1133 =
																							CAR(BgL_cdrzd21062zd2_1126);
																						BgL_arg2020z00_1134 =
																							CAR(BgL_cdrzd21068zd2_1128);
																						BGl_returnz12ze70zf5zzexpand_exitz00
																							(BgL_exitz00_1095,
																							BgL_arg2019z00_1133);
																						BGl_returnz12ze70zf5zzexpand_exitz00
																							(BgL_exitz00_1095,
																							BgL_arg2020z00_1134);
																						return BgL_bodyz00_1096;
																					}
																				else
																					{	/* Expand/exit.scm 124 */
																						return BgL_bodyz00_1096;
																					}
																			}
																		else
																			{	/* Expand/exit.scm 124 */
																				return BgL_bodyz00_1096;
																			}
																	}
																else
																	{	/* Expand/exit.scm 124 */
																		return BgL_bodyz00_1096;
																	}
															}
														else
															{	/* Expand/exit.scm 124 */
																obj_t BgL_carzd21115zd2_1136;
																obj_t BgL_cdrzd21116zd2_1137;

																BgL_carzd21115zd2_1136 =
																	CAR(((obj_t) BgL_bodyz00_1096));
																BgL_cdrzd21116zd2_1137 =
																	CDR(((obj_t) BgL_bodyz00_1096));
																if (
																	(BgL_carzd21115zd2_1136 ==
																		CNST_TABLE_REF(11)))
																	{	/* Expand/exit.scm 124 */
																		obj_t BgL_arg2022z00_1138;

																		BgL_arg2022z00_1138 =
																			CDR(((obj_t) BgL_cdrzd21116zd2_1137));
																		BgL_exprsz00_1106 = BgL_arg2022z00_1138;
																	BgL_tagzd21023zd2_1107:
																		BGl_returnz12ze70zf5zzexpand_exitz00
																			(BgL_exitz00_1095,
																			CAR
																			(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_exprsz00_1106)));
																		return BgL_bodyz00_1096;
																	}
																else
																	{	/* Expand/exit.scm 124 */
																		if (
																			(BgL_carzd21115zd2_1136 ==
																				CNST_TABLE_REF(19)))
																			{	/* Expand/exit.scm 124 */
																				obj_t BgL_arg2024z00_1139;

																				BgL_arg2024z00_1139 =
																					CDR(((obj_t) BgL_cdrzd21116zd2_1137));
																				{
																					obj_t BgL_exprsz00_3281;

																					BgL_exprsz00_3281 =
																						BgL_arg2024z00_1139;
																					BgL_exprsz00_1106 = BgL_exprsz00_3281;
																					goto BgL_tagzd21023zd2_1107;
																				}
																			}
																		else
																			{	/* Expand/exit.scm 124 */
																				if (
																					(BgL_carzd21115zd2_1136 ==
																						CNST_TABLE_REF(20)))
																					{	/* Expand/exit.scm 124 */
																						obj_t BgL_arg2025z00_1140;

																						BgL_arg2025z00_1140 =
																							CDR(
																							((obj_t) BgL_cdrzd21116zd2_1137));
																						{
																							obj_t BgL_exprsz00_3287;

																							BgL_exprsz00_3287 =
																								BgL_arg2025z00_1140;
																							BgL_exprsz00_1106 =
																								BgL_exprsz00_3287;
																							goto BgL_tagzd21023zd2_1107;
																						}
																					}
																				else
																					{	/* Expand/exit.scm 124 */
																						if (
																							(BgL_carzd21115zd2_1136 ==
																								CNST_TABLE_REF(21)))
																							{	/* Expand/exit.scm 124 */
																								obj_t BgL_arg2026z00_1141;

																								BgL_arg2026z00_1141 =
																									CDR(
																									((obj_t)
																										BgL_cdrzd21116zd2_1137));
																								{
																									obj_t BgL_exprsz00_3293;

																									BgL_exprsz00_3293 =
																										BgL_arg2026z00_1141;
																									BgL_exprsz00_1106 =
																										BgL_exprsz00_3293;
																									goto BgL_tagzd21023zd2_1107;
																								}
																							}
																						else
																							{	/* Expand/exit.scm 124 */
																								if (
																									(BgL_carzd21115zd2_1136 ==
																										CNST_TABLE_REF(28)))
																									{	/* Expand/exit.scm 124 */
																										obj_t BgL_arg2029z00_1144;

																										{	/* Expand/exit.scm 124 */
																											obj_t BgL_pairz00_1917;

																											BgL_pairz00_1917 =
																												CDR(
																												((obj_t)
																													BgL_bodyz00_1096));
																											BgL_arg2029z00_1144 =
																												CDR(BgL_pairz00_1917);
																										}
																										BgL_exprsz00_1108 =
																											BgL_arg2029z00_1144;
																									BgL_tagzd21024zd2_1109:
																										BGl_returnz12ze70zf5zzexpand_exitz00(BgL_exitz00_1095, CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(BgL_exprsz00_1108)));
																										return BgL_bodyz00_1096;
																									}
																								else
																									{	/* Expand/exit.scm 124 */
																										return BgL_bodyz00_1096;
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
								else
									{	/* Expand/exit.scm 124 */
										if ((CAR(((obj_t) BgL_bodyz00_1096)) == CNST_TABLE_REF(12)))
											{
												obj_t BgL_exprsz00_3308;

												BgL_exprsz00_3308 = BgL_cdrzd21034zd2_1113;
												BgL_exprsz00_1100 = BgL_exprsz00_3308;
												goto BgL_tagzd21021zd2_1101;
											}
										else
											{	/* Expand/exit.scm 124 */
												return BgL_bodyz00_1096;
											}
									}
							}
						else
							{	/* Expand/exit.scm 124 */
								if ((CAR(((obj_t) BgL_bodyz00_1096)) == CNST_TABLE_REF(12)))
									{
										obj_t BgL_exprsz00_3314;

										BgL_exprsz00_3314 = BgL_cdrzd21034zd2_1113;
										BgL_exprsz00_1100 = BgL_exprsz00_3314;
										goto BgL_tagzd21021zd2_1101;
									}
								else
									{	/* Expand/exit.scm 124 */
										if ((CAR(((obj_t) BgL_bodyz00_1096)) == CNST_TABLE_REF(27)))
											{	/* Expand/exit.scm 124 */
												if (PAIRP(BgL_cdrzd21034zd2_1113))
													{	/* Expand/exit.scm 124 */
														obj_t BgL_cdrzd21172zd2_1161;

														BgL_cdrzd21172zd2_1161 =
															CDR(BgL_cdrzd21034zd2_1113);
														if (PAIRP(BgL_cdrzd21172zd2_1161))
															{	/* Expand/exit.scm 124 */
																obj_t BgL_cdrzd21177zd2_1163;

																BgL_cdrzd21177zd2_1163 =
																	CDR(BgL_cdrzd21172zd2_1161);
																if (PAIRP(BgL_cdrzd21177zd2_1163))
																	{	/* Expand/exit.scm 124 */
																		if (NULLP(CDR(BgL_cdrzd21177zd2_1163)))
																			{	/* Expand/exit.scm 124 */
																				obj_t BgL_arg2055z00_1168;
																				obj_t BgL_arg2056z00_1169;

																				BgL_arg2055z00_1168 =
																					CAR(BgL_cdrzd21172zd2_1161);
																				BgL_arg2056z00_1169 =
																					CAR(BgL_cdrzd21177zd2_1163);
																				BGl_returnz12ze70zf5zzexpand_exitz00
																					(BgL_exitz00_1095,
																					BgL_arg2055z00_1168);
																				BGl_returnz12ze70zf5zzexpand_exitz00
																					(BgL_exitz00_1095,
																					BgL_arg2056z00_1169);
																				return BgL_bodyz00_1096;
																			}
																		else
																			{	/* Expand/exit.scm 124 */
																				return BgL_bodyz00_1096;
																			}
																	}
																else
																	{	/* Expand/exit.scm 124 */
																		return BgL_bodyz00_1096;
																	}
															}
														else
															{	/* Expand/exit.scm 124 */
																return BgL_bodyz00_1096;
															}
													}
												else
													{	/* Expand/exit.scm 124 */
														return BgL_bodyz00_1096;
													}
											}
										else
											{	/* Expand/exit.scm 124 */
												obj_t BgL_carzd21228zd2_1171;
												obj_t BgL_cdrzd21229zd2_1172;

												BgL_carzd21228zd2_1171 =
													CAR(((obj_t) BgL_bodyz00_1096));
												BgL_cdrzd21229zd2_1172 =
													CDR(((obj_t) BgL_bodyz00_1096));
												{

													if ((BgL_carzd21228zd2_1171 == CNST_TABLE_REF(11)))
														{	/* Expand/exit.scm 124 */
														BgL_kapzd21230zd2_1173:
															if (PAIRP(BgL_cdrzd21229zd2_1172))
																{
																	obj_t BgL_exprsz00_3344;

																	BgL_exprsz00_3344 =
																		CDR(BgL_cdrzd21229zd2_1172);
																	BgL_exprsz00_1106 = BgL_exprsz00_3344;
																	goto BgL_tagzd21023zd2_1107;
																}
															else
																{	/* Expand/exit.scm 124 */
																	return BgL_bodyz00_1096;
																}
														}
													else
														{	/* Expand/exit.scm 124 */
															if (
																(BgL_carzd21228zd2_1171 == CNST_TABLE_REF(19)))
																{	/* Expand/exit.scm 124 */
																	goto BgL_kapzd21230zd2_1173;
																}
															else
																{	/* Expand/exit.scm 124 */
																	if (
																		(BgL_carzd21228zd2_1171 ==
																			CNST_TABLE_REF(20)))
																		{	/* Expand/exit.scm 124 */
																			goto BgL_kapzd21230zd2_1173;
																		}
																	else
																		{	/* Expand/exit.scm 124 */
																			if (
																				(BgL_carzd21228zd2_1171 ==
																					CNST_TABLE_REF(21)))
																				{	/* Expand/exit.scm 124 */
																					goto BgL_kapzd21230zd2_1173;
																				}
																			else
																				{	/* Expand/exit.scm 124 */
																					if (
																						(CAR(
																								((obj_t) BgL_bodyz00_1096)) ==
																							CNST_TABLE_REF(28)))
																						{	/* Expand/exit.scm 124 */
																							if (PAIRP(BgL_cdrzd21229zd2_1172))
																								{
																									obj_t BgL_exprsz00_3362;

																									BgL_exprsz00_3362 =
																										CDR(BgL_cdrzd21229zd2_1172);
																									BgL_exprsz00_1108 =
																										BgL_exprsz00_3362;
																									goto BgL_tagzd21024zd2_1109;
																								}
																							else
																								{	/* Expand/exit.scm 124 */
																									return BgL_bodyz00_1096;
																								}
																						}
																					else
																						{	/* Expand/exit.scm 124 */
																							return BgL_bodyz00_1096;
																						}
																				}
																		}
																}
														}
												}
											}
									}
							}
					}
				else
					{	/* Expand/exit.scm 124 */
						return BgL_bodyz00_1096;
					}
			}
		}

	}



/* &expand-bind-exit */
	obj_t BGl_z62expandzd2bindzd2exitz62zzexpand_exitz00(obj_t BgL_envz00_2160,
		obj_t BgL_xz00_2161, obj_t BgL_ez00_2162)
	{
		{	/* Expand/exit.scm 64 */
			return
				BGl_expandzd2bindzd2exitz00zzexpand_exitz00(BgL_xz00_2161,
				BgL_ez00_2162);
		}

	}



/* expand-unwind-protect */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2unwindzd2protectz00zzexpand_exitz00(obj_t
		BgL_xz00_33, obj_t BgL_ez00_34)
	{
		{	/* Expand/exit.scm 255 */
			{
				obj_t BgL_tracespz00_1499;
				obj_t BgL_bodyz00_1500;
				obj_t BgL_expz00_1520;
				obj_t BgL_cleanupz00_1521;
				obj_t BgL_expz00_1572;
				obj_t BgL_ohsz00_1573;
				obj_t BgL_expz00_1619;
				obj_t BgL_envz00_1620;
				obj_t BgL_ohsz00_1621;

				{

					if (PAIRP(BgL_xz00_33))
						{	/* Expand/exit.scm 314 */
							obj_t BgL_cdrzd21982zd2_1402;

							BgL_cdrzd21982zd2_1402 = CDR(((obj_t) BgL_xz00_33));
							if (PAIRP(BgL_cdrzd21982zd2_1402))
								{	/* Expand/exit.scm 314 */
									obj_t BgL_cdrzd21986zd2_1404;

									BgL_cdrzd21986zd2_1404 = CDR(BgL_cdrzd21982zd2_1402);
									if (PAIRP(BgL_cdrzd21986zd2_1404))
										{	/* Expand/exit.scm 314 */
											obj_t BgL_carzd21989zd2_1406;

											BgL_carzd21989zd2_1406 = CAR(BgL_cdrzd21986zd2_1404);
											if (PAIRP(BgL_carzd21989zd2_1406))
												{	/* Expand/exit.scm 314 */
													obj_t BgL_cdrzd21993zd2_1408;

													BgL_cdrzd21993zd2_1408 = CDR(BgL_carzd21989zd2_1406);
													if (
														(CAR(BgL_carzd21989zd2_1406) == CNST_TABLE_REF(36)))
														{	/* Expand/exit.scm 314 */
															if (PAIRP(BgL_cdrzd21993zd2_1408))
																{	/* Expand/exit.scm 314 */
																	obj_t BgL_carzd21995zd2_1412;

																	BgL_carzd21995zd2_1412 =
																		CAR(BgL_cdrzd21993zd2_1408);
																	if (SYMBOLP(BgL_carzd21995zd2_1412))
																		{	/* Expand/exit.scm 314 */
																			if (NULLP(CDR(BgL_cdrzd21993zd2_1408)))
																				{	/* Expand/exit.scm 314 */
																					if (NULLP(CDR
																							(BgL_cdrzd21986zd2_1404)))
																						{	/* Expand/exit.scm 314 */
																							BgL_expz00_1572 =
																								CAR(BgL_cdrzd21982zd2_1402);
																							BgL_ohsz00_1573 =
																								BgL_carzd21995zd2_1412;
																							{	/* Expand/exit.scm 289 */
																								obj_t BgL_exitdz00_1575;
																								obj_t BgL_tmpz00_1576;
																								obj_t BgL_protectz00_1577;

																								BgL_exitdz00_1575 =
																									BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																									(BGl_gensymz00zz__r4_symbols_6_4z00
																									(CNST_TABLE_REF(29)));
																								BgL_tmpz00_1576 =
																									BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																									(BGl_gensymz00zz__r4_symbols_6_4z00
																									(CNST_TABLE_REF(23)));
																								BgL_protectz00_1577 =
																									BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																									(BGl_gensymz00zz__r4_symbols_6_4z00
																									(CNST_TABLE_REF(23)));
																								{	/* Expand/exit.scm 292 */
																									obj_t BgL_newz00_1578;

																									{	/* Expand/exit.scm 292 */
																										obj_t BgL_arg2371z00_1580;

																										{	/* Expand/exit.scm 292 */
																											obj_t BgL_arg2373z00_1581;
																											obj_t BgL_arg2374z00_1582;

																											{	/* Expand/exit.scm 292 */
																												obj_t
																													BgL_arg2375z00_1583;
																												obj_t
																													BgL_arg2376z00_1584;
																												{	/* Expand/exit.scm 292 */
																													obj_t
																														BgL_arg2377z00_1585;
																													{	/* Expand/exit.scm 292 */
																														obj_t
																															BgL_arg2378z00_1586;
																														BgL_arg2378z00_1586
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(35), BNIL);
																														BgL_arg2377z00_1585
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg2378z00_1586,
																															BNIL);
																													}
																													BgL_arg2375z00_1583 =
																														MAKE_YOUNG_PAIR
																														(BgL_exitdz00_1575,
																														BgL_arg2377z00_1585);
																												}
																												{	/* Expand/exit.scm 293 */
																													obj_t
																														BgL_arg2379z00_1587;
																													{	/* Expand/exit.scm 293 */
																														obj_t
																															BgL_arg2380z00_1588;
																														{	/* Expand/exit.scm 293 */
																															obj_t
																																BgL_arg2381z00_1589;
																															{	/* Expand/exit.scm 293 */
																																obj_t
																																	BgL_arg2382z00_1590;
																																{	/* Expand/exit.scm 293 */
																																	obj_t
																																		BgL_arg2383z00_1591;
																																	{	/* Expand/exit.scm 293 */
																																		obj_t
																																			BgL_arg2384z00_1592;
																																		{	/* Expand/exit.scm 293 */
																																			obj_t
																																				BgL_arg2385z00_1593;
																																			BgL_arg2385z00_1593
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_exitdz00_1575,
																																				BNIL);
																																			BgL_arg2384z00_1592
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(30),
																																				BgL_arg2385z00_1593);
																																		}
																																		BgL_arg2383z00_1591
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2384z00_1592,
																																			BNIL);
																																	}
																																	BgL_arg2382z00_1590
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_ohsz00_1573,
																																		BgL_arg2383z00_1591);
																																}
																																BgL_arg2381z00_1589
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(31),
																																	BgL_arg2382z00_1590);
																															}
																															BgL_arg2380z00_1588
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2381z00_1589,
																																BNIL);
																														}
																														BgL_arg2379z00_1587
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_protectz00_1577,
																															BgL_arg2380z00_1588);
																													}
																													BgL_arg2376z00_1584 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2379z00_1587,
																														BNIL);
																												}
																												BgL_arg2373z00_1581 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg2375z00_1583,
																													BgL_arg2376z00_1584);
																											}
																											{	/* Expand/exit.scm 294 */
																												obj_t
																													BgL_arg2386z00_1594;
																												obj_t
																													BgL_arg2387z00_1595;
																												{	/* Expand/exit.scm 294 */
																													obj_t
																														BgL_arg2388z00_1596;
																													obj_t
																														BgL_arg2389z00_1597;
																													{	/* Expand/exit.scm 294 */
																														obj_t
																															BgL_arg2390z00_1598;
																														{	/* Expand/exit.scm 294 */
																															obj_t
																																BgL_arg2391z00_1599;
																															BgL_arg2391z00_1599
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(15), BNIL);
																															BgL_arg2390z00_1598
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(32),
																																BgL_arg2391z00_1599);
																														}
																														BgL_arg2388z00_1596
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(17),
																															BgL_arg2390z00_1598);
																													}
																													{	/* Expand/exit.scm 294 */
																														obj_t
																															BgL_arg2392z00_1600;
																														BgL_arg2392z00_1600
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_protectz00_1577,
																															BNIL);
																														BgL_arg2389z00_1597
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_exitdz00_1575,
																															BgL_arg2392z00_1600);
																													}
																													BgL_arg2386z00_1594 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2388z00_1596,
																														BgL_arg2389z00_1597);
																												}
																												{	/* Expand/exit.scm 295 */
																													obj_t
																														BgL_arg2393z00_1601;
																													{	/* Expand/exit.scm 295 */
																														obj_t
																															BgL_arg2395z00_1602;
																														{	/* Expand/exit.scm 295 */
																															obj_t
																																BgL_arg2396z00_1603;
																															obj_t
																																BgL_arg2397z00_1604;
																															{	/* Expand/exit.scm 295 */
																																obj_t
																																	BgL_arg2398z00_1605;
																																{	/* Expand/exit.scm 295 */
																																	obj_t
																																		BgL_arg2399z00_1606;
																																	BgL_arg2399z00_1606
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_expz00_1572,
																																		BNIL);
																																	BgL_arg2398z00_1605
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_tmpz00_1576,
																																		BgL_arg2399z00_1606);
																																}
																																BgL_arg2396z00_1603
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2398z00_1605,
																																	BNIL);
																															}
																															{	/* Expand/exit.scm 296 */
																																obj_t
																																	BgL_arg2401z00_1607;
																																obj_t
																																	BgL_arg2402z00_1608;
																																{	/* Expand/exit.scm 296 */
																																	obj_t
																																		BgL_arg2403z00_1609;
																																	obj_t
																																		BgL_arg2404z00_1610;
																																	{	/* Expand/exit.scm 296 */
																																		obj_t
																																			BgL_arg2405z00_1611;
																																		{	/* Expand/exit.scm 296 */
																																			obj_t
																																				BgL_arg2407z00_1612;
																																			BgL_arg2407z00_1612
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(15),
																																				BNIL);
																																			BgL_arg2405z00_1611
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(33),
																																				BgL_arg2407z00_1612);
																																		}
																																		BgL_arg2403z00_1609
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(17),
																																			BgL_arg2405z00_1611);
																																	}
																																	BgL_arg2404z00_1610
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_exitdz00_1575,
																																		BNIL);
																																	BgL_arg2401z00_1607
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2403z00_1609,
																																		BgL_arg2404z00_1610);
																																}
																																{	/* Expand/exit.scm 297 */
																																	obj_t
																																		BgL_arg2408z00_1613;
																																	obj_t
																																		BgL_arg2410z00_1614;
																																	{	/* Expand/exit.scm 297 */
																																		obj_t
																																			BgL_arg2411z00_1615;
																																		BgL_arg2411z00_1615
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_ohsz00_1573,
																																			BNIL);
																																		BgL_arg2408z00_1613
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(36),
																																			BgL_arg2411z00_1615);
																																	}
																																	BgL_arg2410z00_1614
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_tmpz00_1576,
																																		BNIL);
																																	BgL_arg2402z00_1608
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2408z00_1613,
																																		BgL_arg2410z00_1614);
																																}
																																BgL_arg2397z00_1604
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2401z00_1607,
																																	BgL_arg2402z00_1608);
																															}
																															BgL_arg2395z00_1602
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2396z00_1603,
																																BgL_arg2397z00_1604);
																														}
																														BgL_arg2393z00_1601
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(11),
																															BgL_arg2395z00_1602);
																													}
																													BgL_arg2387z00_1595 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2393z00_1601,
																														BNIL);
																												}
																												BgL_arg2374z00_1582 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg2386z00_1594,
																													BgL_arg2387z00_1595);
																											}
																											BgL_arg2371z00_1580 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2373z00_1581,
																												BgL_arg2374z00_1582);
																										}
																										BgL_newz00_1578 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(19),
																											BgL_arg2371z00_1580);
																									}
																									{	/* Expand/exit.scm 299 */
																										obj_t BgL_arg2370z00_1579;

																										BgL_arg2370z00_1579 =
																											BGL_PROCEDURE_CALL2
																											(BgL_ez00_34,
																											BgL_newz00_1578,
																											BgL_ez00_34);
																										return
																											BGl_replacez12z12zztools_miscz00
																											(BgL_xz00_33,
																											BgL_arg2370z00_1579);
																									}
																								}
																							}
																						}
																					else
																						{	/* Expand/exit.scm 314 */
																							obj_t BgL_cdrzd22019zd2_1420;

																							BgL_cdrzd22019zd2_1420 =
																								CDR(
																								((obj_t)
																									BgL_cdrzd21982zd2_1402));
																							if (PAIRP(BgL_cdrzd22019zd2_1420))
																								{	/* Expand/exit.scm 314 */
																									obj_t BgL_arg2248z00_1422;

																									BgL_arg2248z00_1422 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd21982zd2_1402));
																									BgL_expz00_1520 =
																										BgL_arg2248z00_1422;
																									BgL_cleanupz00_1521 =
																										BgL_cdrzd22019zd2_1420;
																								BgL_zc3z04anonymousza32314ze3z87_1522:
																									{	/* Expand/exit.scm 271 */
																										obj_t BgL_exitdz00_1523;
																										obj_t BgL_protectz00_1524;
																										obj_t BgL_tmpz00_1525;
																										obj_t BgL_tracespz00_1526;

																										BgL_exitdz00_1523 =
																											BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																											(BGl_gensymz00zz__r4_symbols_6_4z00
																											(CNST_TABLE_REF(29)));
																										BgL_protectz00_1524 =
																											BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																											(BGl_gensymz00zz__r4_symbols_6_4z00
																											(CNST_TABLE_REF(37)));
																										BgL_tmpz00_1525 =
																											BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																											(BGl_gensymz00zz__r4_symbols_6_4z00
																											(CNST_TABLE_REF(23)));
																										BgL_tracespz00_1526 =
																											BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																											(BGl_gensymz00zz__r4_symbols_6_4z00
																											(CNST_TABLE_REF(7)));
																										{	/* Expand/exit.scm 275 */
																											obj_t BgL_newz00_1527;

																											{	/* Expand/exit.scm 276 */
																												obj_t
																													BgL_arg2316z00_1529;
																												{	/* Expand/exit.scm 276 */
																													obj_t
																														BgL_arg2317z00_1530;
																													{	/* Expand/exit.scm 276 */
																														obj_t
																															BgL_arg2318z00_1531;
																														obj_t
																															BgL_arg2319z00_1532;
																														{	/* Expand/exit.scm 276 */
																															obj_t
																																BgL_arg2320z00_1533;
																															obj_t
																																BgL_arg2321z00_1534;
																															{	/* Expand/exit.scm 276 */
																																obj_t
																																	BgL_arg2323z00_1535;
																																{	/* Expand/exit.scm 276 */
																																	obj_t
																																		BgL_arg2324z00_1536;
																																	BgL_arg2324z00_1536
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(35), BNIL);
																																	BgL_arg2323z00_1535
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2324z00_1536,
																																		BNIL);
																																}
																																BgL_arg2320z00_1533
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_exitdz00_1523,
																																	BgL_arg2323z00_1535);
																															}
																															{	/* Expand/exit.scm 277 */
																																obj_t
																																	BgL_arg2325z00_1537;
																																{	/* Expand/exit.scm 277 */
																																	obj_t
																																		BgL_arg2326z00_1538;
																																	{	/* Expand/exit.scm 277 */
																																		obj_t
																																			BgL_arg2327z00_1539;
																																		{	/* Expand/exit.scm 277 */
																																			obj_t
																																				BgL_arg2328z00_1540;
																																			BgL_arg2328z00_1540
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BNIL,
																																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																				(BgL_cleanupz00_1521,
																																					BNIL));
																																			BgL_arg2327z00_1539
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(38),
																																				BgL_arg2328z00_1540);
																																		}
																																		BgL_arg2326z00_1538
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2327z00_1539,
																																			BNIL);
																																	}
																																	BgL_arg2325z00_1537
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_protectz00_1524,
																																		BgL_arg2326z00_1538);
																																}
																																BgL_arg2321z00_1534
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2325z00_1537,
																																	BNIL);
																															}
																															BgL_arg2318z00_1531
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2320z00_1533,
																																BgL_arg2321z00_1534);
																														}
																														{	/* Expand/exit.scm 278 */
																															obj_t
																																BgL_arg2331z00_1542;
																															obj_t
																																BgL_arg2333z00_1543;
																															{	/* Expand/exit.scm 278 */
																																obj_t
																																	BgL_arg2335z00_1544;
																																obj_t
																																	BgL_arg2336z00_1545;
																																{	/* Expand/exit.scm 278 */
																																	obj_t
																																		BgL_arg2337z00_1546;
																																	{	/* Expand/exit.scm 278 */
																																		obj_t
																																			BgL_arg2338z00_1547;
																																		BgL_arg2338z00_1547
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(15),
																																			BNIL);
																																		BgL_arg2337z00_1546
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(39),
																																			BgL_arg2338z00_1547);
																																	}
																																	BgL_arg2335z00_1544
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(17),
																																		BgL_arg2337z00_1546);
																																}
																																{	/* Expand/exit.scm 278 */
																																	obj_t
																																		BgL_arg2339z00_1548;
																																	BgL_arg2339z00_1548
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_protectz00_1524,
																																		BNIL);
																																	BgL_arg2336z00_1545
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_exitdz00_1523,
																																		BgL_arg2339z00_1548);
																																}
																																BgL_arg2331z00_1542
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2335z00_1544,
																																	BgL_arg2336z00_1545);
																															}
																															{	/* Expand/exit.scm 279 */
																																obj_t
																																	BgL_arg2340z00_1549;
																																{	/* Expand/exit.scm 279 */
																																	obj_t
																																		BgL_arg2341z00_1550;
																																	{	/* Expand/exit.scm 279 */
																																		obj_t
																																			BgL_arg2342z00_1551;
																																		obj_t
																																			BgL_arg2345z00_1552;
																																		{	/* Expand/exit.scm 279 */
																																			obj_t
																																				BgL_arg2346z00_1553;
																																			{	/* Expand/exit.scm 279 */
																																				obj_t
																																					BgL_arg2348z00_1554;
																																				BgL_arg2348z00_1554
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_expz00_1520,
																																					BNIL);
																																				BgL_arg2346z00_1553
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_tmpz00_1525,
																																					BgL_arg2348z00_1554);
																																			}
																																			BgL_arg2342z00_1551
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2346z00_1553,
																																				BNIL);
																																		}
																																		{	/* Expand/exit.scm 280 */
																																			obj_t
																																				BgL_arg2349z00_1555;
																																			obj_t
																																				BgL_arg2350z00_1556;
																																			{	/* Expand/exit.scm 280 */
																																				obj_t
																																					BgL_arg2351z00_1557;
																																				obj_t
																																					BgL_arg2352z00_1558;
																																				{	/* Expand/exit.scm 280 */
																																					obj_t
																																						BgL_arg2353z00_1559;
																																					{	/* Expand/exit.scm 280 */
																																						obj_t
																																							BgL_arg2354z00_1560;
																																						BgL_arg2354z00_1560
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(15),
																																							BNIL);
																																						BgL_arg2353z00_1559
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(33),
																																							BgL_arg2354z00_1560);
																																					}
																																					BgL_arg2351z00_1557
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(17),
																																						BgL_arg2353z00_1559);
																																				}
																																				BgL_arg2352z00_1558
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_exitdz00_1523,
																																					BNIL);
																																				BgL_arg2349z00_1555
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2351z00_1557,
																																					BgL_arg2352z00_1558);
																																			}
																																			{	/* Expand/exit.scm 281 */
																																				obj_t
																																					BgL_arg2355z00_1561;
																																				obj_t
																																					BgL_arg2356z00_1562;
																																				if (BGl_tracezf3ze70z14zzexpand_exitz00())
																																					{	/* Expand/exit.scm 282 */
																																						obj_t
																																							BgL_arg2358z00_1564;
																																						{	/* Expand/exit.scm 282 */
																																							obj_t
																																								BgL_arg2361z00_1565;
																																							BgL_arg2361z00_1565
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_tracespz00_1526,
																																								BNIL);
																																							BgL_arg2358z00_1564
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(25),
																																								BgL_arg2361z00_1565);
																																						}
																																						BgL_arg2355z00_1561
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2358z00_1564,
																																							BNIL);
																																					}
																																				else
																																					{	/* Expand/exit.scm 281 */
																																						BgL_arg2355z00_1561
																																							=
																																							BNIL;
																																					}
																																				{	/* Expand/exit.scm 284 */
																																					obj_t
																																						BgL_arg2363z00_1566;
																																					obj_t
																																						BgL_arg2364z00_1567;
																																					BgL_arg2363z00_1566
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_protectz00_1524,
																																						BNIL);
																																					BgL_arg2364z00_1567
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_tmpz00_1525,
																																						BNIL);
																																					BgL_arg2356z00_1562
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2363z00_1566,
																																						BgL_arg2364z00_1567);
																																				}
																																				BgL_arg2350z00_1556
																																					=
																																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																					(BgL_arg2355z00_1561,
																																					BgL_arg2356z00_1562);
																																			}
																																			BgL_arg2345z00_1552
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2349z00_1555,
																																				BgL_arg2350z00_1556);
																																		}
																																		BgL_arg2341z00_1550
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2342z00_1551,
																																			BgL_arg2345z00_1552);
																																	}
																																	BgL_arg2340z00_1549
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(11),
																																		BgL_arg2341z00_1550);
																																}
																																BgL_arg2333z00_1543
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2340z00_1549,
																																	BNIL);
																															}
																															BgL_arg2319z00_1532
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2331z00_1542,
																																BgL_arg2333z00_1543);
																														}
																														BgL_arg2317z00_1530
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg2318z00_1531,
																															BgL_arg2319z00_1532);
																													}
																													BgL_arg2316z00_1529 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(11),
																														BgL_arg2317z00_1530);
																												}
																												BgL_tracespz00_1499 =
																													BgL_tracespz00_1526;
																												BgL_bodyz00_1500 =
																													BgL_arg2316z00_1529;
																												if (BGl_tracezf3ze70z14zzexpand_exitz00())
																													{	/* Expand/exit.scm 263 */
																														obj_t
																															BgL_tmpz00_1503;
																														BgL_tmpz00_1503 =
																															BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																															(BGl_gensymz00zz__r4_symbols_6_4z00
																															(CNST_TABLE_REF
																																(23)));
																														{	/* Expand/exit.scm 264 */
																															obj_t
																																BgL_arg2296z00_1504;
																															{	/* Expand/exit.scm 264 */
																																obj_t
																																	BgL_arg2297z00_1505;
																																obj_t
																																	BgL_arg2298z00_1506;
																																{	/* Expand/exit.scm 264 */
																																	obj_t
																																		BgL_arg2299z00_1507;
																																	{	/* Expand/exit.scm 264 */
																																		obj_t
																																			BgL_arg2301z00_1508;
																																		{	/* Expand/exit.scm 264 */
																																			obj_t
																																				BgL_arg2302z00_1509;
																																			BgL_arg2302z00_1509
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(24),
																																				BNIL);
																																			BgL_arg2301z00_1508
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2302z00_1509,
																																				BNIL);
																																		}
																																		BgL_arg2299z00_1507
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_tracespz00_1499,
																																			BgL_arg2301z00_1508);
																																	}
																																	BgL_arg2297z00_1505
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2299z00_1507,
																																		BNIL);
																																}
																																{	/* Expand/exit.scm 265 */
																																	obj_t
																																		BgL_arg2304z00_1510;
																																	{	/* Expand/exit.scm 265 */
																																		obj_t
																																			BgL_arg2305z00_1511;
																																		{	/* Expand/exit.scm 265 */
																																			obj_t
																																				BgL_arg2306z00_1512;
																																			obj_t
																																				BgL_arg2307z00_1513;
																																			{	/* Expand/exit.scm 265 */
																																				obj_t
																																					BgL_arg2308z00_1514;
																																				{	/* Expand/exit.scm 265 */
																																					obj_t
																																						BgL_arg2309z00_1515;
																																					BgL_arg2309z00_1515
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_bodyz00_1500,
																																						BNIL);
																																					BgL_arg2308z00_1514
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_tmpz00_1503,
																																						BgL_arg2309z00_1515);
																																				}
																																				BgL_arg2306z00_1512
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2308z00_1514,
																																					BNIL);
																																			}
																																			{	/* Expand/exit.scm 266 */
																																				obj_t
																																					BgL_arg2310z00_1516;
																																				obj_t
																																					BgL_arg2311z00_1517;
																																				{	/* Expand/exit.scm 266 */
																																					obj_t
																																						BgL_arg2312z00_1518;
																																					BgL_arg2312z00_1518
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_tracespz00_1499,
																																						BNIL);
																																					BgL_arg2310z00_1516
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(25),
																																						BgL_arg2312z00_1518);
																																				}
																																				BgL_arg2311z00_1517
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_tmpz00_1503,
																																					BNIL);
																																				BgL_arg2307z00_1513
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2310z00_1516,
																																					BgL_arg2311z00_1517);
																																			}
																																			BgL_arg2305z00_1511
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2306z00_1512,
																																				BgL_arg2307z00_1513);
																																		}
																																		BgL_arg2304z00_1510
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(11),
																																			BgL_arg2305z00_1511);
																																	}
																																	BgL_arg2298z00_1506
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2304z00_1510,
																																		BNIL);
																																}
																																BgL_arg2296z00_1504
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2297z00_1505,
																																	BgL_arg2298z00_1506);
																															}
																															BgL_newz00_1527 =
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(11),
																																BgL_arg2296z00_1504);
																														}
																													}
																												else
																													{	/* Expand/exit.scm 262 */
																														BgL_newz00_1527 =
																															BgL_bodyz00_1500;
																													}
																											}
																											{	/* Expand/exit.scm 286 */
																												obj_t
																													BgL_arg2315z00_1528;
																												BgL_arg2315z00_1528 =
																													BGL_PROCEDURE_CALL2
																													(BgL_ez00_34,
																													BgL_newz00_1527,
																													BgL_ez00_34);
																												return
																													BGl_replacez12z12zztools_miscz00
																													(BgL_xz00_33,
																													BgL_arg2315z00_1528);
																											}
																										}
																									}
																								}
																							else
																								{	/* Expand/exit.scm 314 */
																								BgL_tagzd21973zd2_1399:
																									return
																										BGl_errorz00zz__errorz00
																										(BFALSE,
																										BGl_string2596z00zzexpand_exitz00,
																										BgL_xz00_33);
																								}
																						}
																				}
																			else
																				{	/* Expand/exit.scm 314 */
																					obj_t BgL_cdrzd22044zd2_1425;

																					BgL_cdrzd22044zd2_1425 =
																						CDR(
																						((obj_t) BgL_cdrzd21982zd2_1402));
																					if (PAIRP(BgL_cdrzd22044zd2_1425))
																						{	/* Expand/exit.scm 314 */
																							obj_t BgL_arg2251z00_1427;

																							BgL_arg2251z00_1427 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd21982zd2_1402));
																							{
																								obj_t BgL_cleanupz00_3566;
																								obj_t BgL_expz00_3565;

																								BgL_expz00_3565 =
																									BgL_arg2251z00_1427;
																								BgL_cleanupz00_3566 =
																									BgL_cdrzd22044zd2_1425;
																								BgL_cleanupz00_1521 =
																									BgL_cleanupz00_3566;
																								BgL_expz00_1520 =
																									BgL_expz00_3565;
																								goto
																									BgL_zc3z04anonymousza32314ze3z87_1522;
																							}
																						}
																					else
																						{	/* Expand/exit.scm 314 */
																							goto BgL_tagzd21973zd2_1399;
																						}
																				}
																		}
																	else
																		{	/* Expand/exit.scm 314 */
																			obj_t BgL_cdrzd22069zd2_1430;

																			BgL_cdrzd22069zd2_1430 =
																				CDR(((obj_t) BgL_cdrzd21982zd2_1402));
																			if (PAIRP(BgL_cdrzd22069zd2_1430))
																				{	/* Expand/exit.scm 314 */
																					obj_t BgL_arg2254z00_1432;

																					BgL_arg2254z00_1432 =
																						CAR(
																						((obj_t) BgL_cdrzd21982zd2_1402));
																					{
																						obj_t BgL_cleanupz00_3574;
																						obj_t BgL_expz00_3573;

																						BgL_expz00_3573 =
																							BgL_arg2254z00_1432;
																						BgL_cleanupz00_3574 =
																							BgL_cdrzd22069zd2_1430;
																						BgL_cleanupz00_1521 =
																							BgL_cleanupz00_3574;
																						BgL_expz00_1520 = BgL_expz00_3573;
																						goto
																							BgL_zc3z04anonymousza32314ze3z87_1522;
																					}
																				}
																			else
																				{	/* Expand/exit.scm 314 */
																					goto BgL_tagzd21973zd2_1399;
																				}
																		}
																}
															else
																{	/* Expand/exit.scm 314 */
																	obj_t BgL_cdrzd22094zd2_1434;

																	BgL_cdrzd22094zd2_1434 =
																		CDR(((obj_t) BgL_cdrzd21982zd2_1402));
																	if (PAIRP(BgL_cdrzd22094zd2_1434))
																		{	/* Expand/exit.scm 314 */
																			obj_t BgL_arg2256z00_1436;

																			BgL_arg2256z00_1436 =
																				CAR(((obj_t) BgL_cdrzd21982zd2_1402));
																			{
																				obj_t BgL_cleanupz00_3582;
																				obj_t BgL_expz00_3581;

																				BgL_expz00_3581 = BgL_arg2256z00_1436;
																				BgL_cleanupz00_3582 =
																					BgL_cdrzd22094zd2_1434;
																				BgL_cleanupz00_1521 =
																					BgL_cleanupz00_3582;
																				BgL_expz00_1520 = BgL_expz00_3581;
																				goto
																					BgL_zc3z04anonymousza32314ze3z87_1522;
																			}
																		}
																	else
																		{	/* Expand/exit.scm 314 */
																			goto BgL_tagzd21973zd2_1399;
																		}
																}
														}
													else
														{	/* Expand/exit.scm 314 */
															obj_t BgL_cdrzd22115zd2_1438;

															BgL_cdrzd22115zd2_1438 =
																CDR(((obj_t) BgL_cdrzd21982zd2_1402));
															{	/* Expand/exit.scm 314 */
																obj_t BgL_carzd22120zd2_1439;

																BgL_carzd22120zd2_1439 =
																	CAR(((obj_t) BgL_cdrzd22115zd2_1438));
																{	/* Expand/exit.scm 314 */
																	obj_t BgL_cdrzd22125zd2_1440;

																	BgL_cdrzd22125zd2_1440 =
																		CDR(((obj_t) BgL_carzd22120zd2_1439));
																	if (
																		(CAR(
																				((obj_t) BgL_carzd22120zd2_1439)) ==
																			CNST_TABLE_REF(34)))
																		{	/* Expand/exit.scm 314 */
																			if (PAIRP(BgL_cdrzd22125zd2_1440))
																				{	/* Expand/exit.scm 314 */
																					obj_t BgL_carzd22128zd2_1444;
																					obj_t BgL_cdrzd22129zd2_1445;

																					BgL_carzd22128zd2_1444 =
																						CAR(BgL_cdrzd22125zd2_1440);
																					BgL_cdrzd22129zd2_1445 =
																						CDR(BgL_cdrzd22125zd2_1440);
																					if (SYMBOLP(BgL_carzd22128zd2_1444))
																						{	/* Expand/exit.scm 314 */
																							if (PAIRP(BgL_cdrzd22129zd2_1445))
																								{	/* Expand/exit.scm 314 */
																									obj_t BgL_carzd22134zd2_1448;

																									BgL_carzd22134zd2_1448 =
																										CAR(BgL_cdrzd22129zd2_1445);
																									if (SYMBOLP
																										(BgL_carzd22134zd2_1448))
																										{	/* Expand/exit.scm 314 */
																											if (NULLP(CDR
																													(BgL_cdrzd22129zd2_1445)))
																												{	/* Expand/exit.scm 314 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd22115zd2_1438))))
																														{	/* Expand/exit.scm 314 */
																															obj_t
																																BgL_arg2267z00_1454;
																															BgL_arg2267z00_1454
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21982zd2_1402));
																															BgL_expz00_1619 =
																																BgL_arg2267z00_1454;
																															BgL_envz00_1620 =
																																BgL_carzd22128zd2_1444;
																															BgL_ohsz00_1621 =
																																BgL_carzd22134zd2_1448;
																															{	/* Expand/exit.scm 302 */
																																obj_t
																																	BgL_exitdz00_1623;
																																obj_t
																																	BgL_tmpz00_1624;
																																obj_t
																																	BgL_protectz00_1625;
																																BgL_exitdz00_1623
																																	=
																																	BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																	(BGl_gensymz00zz__r4_symbols_6_4z00
																																	(CNST_TABLE_REF
																																		(29)));
																																BgL_tmpz00_1624
																																	=
																																	BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																	(BGl_gensymz00zz__r4_symbols_6_4z00
																																	(CNST_TABLE_REF
																																		(23)));
																																BgL_protectz00_1625
																																	=
																																	BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																																	(BGl_gensymz00zz__r4_symbols_6_4z00
																																	(CNST_TABLE_REF
																																		(23)));
																																{	/* Expand/exit.scm 305 */
																																	obj_t
																																		BgL_newz00_1626;
																																	{	/* Expand/exit.scm 305 */
																																		obj_t
																																			BgL_arg2418z00_1628;
																																		{	/* Expand/exit.scm 305 */
																																			obj_t
																																				BgL_arg2419z00_1629;
																																			obj_t
																																				BgL_arg2420z00_1630;
																																			{	/* Expand/exit.scm 305 */
																																				obj_t
																																					BgL_arg2421z00_1631;
																																				obj_t
																																					BgL_arg2423z00_1632;
																																				{	/* Expand/exit.scm 305 */
																																					obj_t
																																						BgL_arg2424z00_1633;
																																					{	/* Expand/exit.scm 305 */
																																						obj_t
																																							BgL_arg2425z00_1634;
																																						{	/* Expand/exit.scm 305 */
																																							obj_t
																																								BgL_arg2426z00_1635;
																																							BgL_arg2426z00_1635
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_envz00_1620,
																																								BNIL);
																																							BgL_arg2425z00_1634
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(9),
																																								BgL_arg2426z00_1635);
																																						}
																																						BgL_arg2424z00_1633
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2425z00_1634,
																																							BNIL);
																																					}
																																					BgL_arg2421z00_1631
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_exitdz00_1623,
																																						BgL_arg2424z00_1633);
																																				}
																																				{	/* Expand/exit.scm 306 */
																																					obj_t
																																						BgL_arg2428z00_1636;
																																					{	/* Expand/exit.scm 306 */
																																						obj_t
																																							BgL_arg2429z00_1637;
																																						{	/* Expand/exit.scm 306 */
																																							obj_t
																																								BgL_arg2430z00_1638;
																																							{	/* Expand/exit.scm 306 */
																																								obj_t
																																									BgL_arg2431z00_1639;
																																								{	/* Expand/exit.scm 306 */
																																									obj_t
																																										BgL_arg2432z00_1640;
																																									{	/* Expand/exit.scm 306 */
																																										obj_t
																																											BgL_arg2434z00_1641;
																																										{	/* Expand/exit.scm 306 */
																																											obj_t
																																												BgL_arg2435z00_1642;
																																											BgL_arg2435z00_1642
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_exitdz00_1623,
																																												BNIL);
																																											BgL_arg2434z00_1641
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(30),
																																												BgL_arg2435z00_1642);
																																										}
																																										BgL_arg2432z00_1640
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg2434z00_1641,
																																											BNIL);
																																									}
																																									BgL_arg2431z00_1639
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_ohsz00_1621,
																																										BgL_arg2432z00_1640);
																																								}
																																								BgL_arg2430z00_1638
																																									=
																																									MAKE_YOUNG_PAIR
																																									(CNST_TABLE_REF
																																									(31),
																																									BgL_arg2431z00_1639);
																																							}
																																							BgL_arg2429z00_1637
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg2430z00_1638,
																																								BNIL);
																																						}
																																						BgL_arg2428z00_1636
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_protectz00_1625,
																																							BgL_arg2429z00_1637);
																																					}
																																					BgL_arg2423z00_1632
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2428z00_1636,
																																						BNIL);
																																				}
																																				BgL_arg2419z00_1629
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2421z00_1631,
																																					BgL_arg2423z00_1632);
																																			}
																																			{	/* Expand/exit.scm 307 */
																																				obj_t
																																					BgL_arg2437z00_1643;
																																				obj_t
																																					BgL_arg2438z00_1644;
																																				{	/* Expand/exit.scm 307 */
																																					obj_t
																																						BgL_arg2439z00_1645;
																																					obj_t
																																						BgL_arg2442z00_1646;
																																					{	/* Expand/exit.scm 307 */
																																						obj_t
																																							BgL_arg2443z00_1647;
																																						{	/* Expand/exit.scm 307 */
																																							obj_t
																																								BgL_arg2444z00_1648;
																																							BgL_arg2444z00_1648
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(15),
																																								BNIL);
																																							BgL_arg2443z00_1647
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(32),
																																								BgL_arg2444z00_1648);
																																						}
																																						BgL_arg2439z00_1645
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(17),
																																							BgL_arg2443z00_1647);
																																					}
																																					{	/* Expand/exit.scm 307 */
																																						obj_t
																																							BgL_arg2445z00_1649;
																																						BgL_arg2445z00_1649
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_protectz00_1625,
																																							BNIL);
																																						BgL_arg2442z00_1646
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_exitdz00_1623,
																																							BgL_arg2445z00_1649);
																																					}
																																					BgL_arg2437z00_1643
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2439z00_1645,
																																						BgL_arg2442z00_1646);
																																				}
																																				{	/* Expand/exit.scm 308 */
																																					obj_t
																																						BgL_arg2446z00_1650;
																																					{	/* Expand/exit.scm 308 */
																																						obj_t
																																							BgL_arg2447z00_1651;
																																						{	/* Expand/exit.scm 308 */
																																							obj_t
																																								BgL_arg2449z00_1652;
																																							obj_t
																																								BgL_arg2450z00_1653;
																																							{	/* Expand/exit.scm 308 */
																																								obj_t
																																									BgL_arg2451z00_1654;
																																								{	/* Expand/exit.scm 308 */
																																									obj_t
																																										BgL_arg2452z00_1655;
																																									BgL_arg2452z00_1655
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_expz00_1619,
																																										BNIL);
																																									BgL_arg2451z00_1654
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_tmpz00_1624,
																																										BgL_arg2452z00_1655);
																																								}
																																								BgL_arg2449z00_1652
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg2451z00_1654,
																																									BNIL);
																																							}
																																							{	/* Expand/exit.scm 309 */
																																								obj_t
																																									BgL_arg2453z00_1656;
																																								obj_t
																																									BgL_arg2455z00_1657;
																																								{	/* Expand/exit.scm 309 */
																																									obj_t
																																										BgL_arg2456z00_1658;
																																									obj_t
																																										BgL_arg2457z00_1659;
																																									{	/* Expand/exit.scm 309 */
																																										obj_t
																																											BgL_arg2458z00_1660;
																																										{	/* Expand/exit.scm 309 */
																																											obj_t
																																												BgL_arg2459z00_1661;
																																											BgL_arg2459z00_1661
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(15),
																																												BNIL);
																																											BgL_arg2458z00_1660
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(33),
																																												BgL_arg2459z00_1661);
																																										}
																																										BgL_arg2456z00_1658
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(17),
																																											BgL_arg2458z00_1660);
																																									}
																																									BgL_arg2457z00_1659
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_exitdz00_1623,
																																										BNIL);
																																									BgL_arg2453z00_1656
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg2456z00_1658,
																																										BgL_arg2457z00_1659);
																																								}
																																								{	/* Expand/exit.scm 310 */
																																									obj_t
																																										BgL_arg2460z00_1662;
																																									obj_t
																																										BgL_arg2461z00_1663;
																																									{	/* Expand/exit.scm 310 */
																																										obj_t
																																											BgL_arg2462z00_1664;
																																										{	/* Expand/exit.scm 310 */
																																											obj_t
																																												BgL_arg2463z00_1665;
																																											BgL_arg2463z00_1665
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_ohsz00_1621,
																																												BNIL);
																																											BgL_arg2462z00_1664
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_envz00_1620,
																																												BgL_arg2463z00_1665);
																																										}
																																										BgL_arg2460z00_1662
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(34),
																																											BgL_arg2462z00_1664);
																																									}
																																									BgL_arg2461z00_1663
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_tmpz00_1624,
																																										BNIL);
																																									BgL_arg2455z00_1657
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg2460z00_1662,
																																										BgL_arg2461z00_1663);
																																								}
																																								BgL_arg2450z00_1653
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg2453z00_1656,
																																									BgL_arg2455z00_1657);
																																							}
																																							BgL_arg2447z00_1651
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg2449z00_1652,
																																								BgL_arg2450z00_1653);
																																						}
																																						BgL_arg2446z00_1650
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(11),
																																							BgL_arg2447z00_1651);
																																					}
																																					BgL_arg2438z00_1644
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2446z00_1650,
																																						BNIL);
																																				}
																																				BgL_arg2420z00_1630
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2437z00_1643,
																																					BgL_arg2438z00_1644);
																																			}
																																			BgL_arg2418z00_1628
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2419z00_1629,
																																				BgL_arg2420z00_1630);
																																		}
																																		BgL_newz00_1626
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(19),
																																			BgL_arg2418z00_1628);
																																	}
																																	{	/* Expand/exit.scm 312 */
																																		obj_t
																																			BgL_arg2417z00_1627;
																																		BgL_arg2417z00_1627
																																			=
																																			BGL_PROCEDURE_CALL2
																																			(BgL_ez00_34,
																																			BgL_newz00_1626,
																																			BgL_ez00_34);
																																		return
																																			BGl_replacez12z12zztools_miscz00
																																			(BgL_xz00_33,
																																			BgL_arg2417z00_1627);
																																	}
																																}
																															}
																														}
																													else
																														{	/* Expand/exit.scm 314 */
																															obj_t
																																BgL_cdrzd22145zd2_1455;
																															BgL_cdrzd22145zd2_1455
																																=
																																CDR(((obj_t)
																																	BgL_xz00_33));
																															{	/* Expand/exit.scm 314 */
																																obj_t
																																	BgL_cdrzd22150zd2_1456;
																																BgL_cdrzd22150zd2_1456
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd22145zd2_1455));
																																if (PAIRP
																																	(BgL_cdrzd22150zd2_1456))
																																	{	/* Expand/exit.scm 314 */
																																		obj_t
																																			BgL_arg2269z00_1458;
																																		BgL_arg2269z00_1458
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd22145zd2_1455));
																																		{
																																			obj_t
																																				BgL_cleanupz00_3689;
																																			obj_t
																																				BgL_expz00_3688;
																																			BgL_expz00_3688
																																				=
																																				BgL_arg2269z00_1458;
																																			BgL_cleanupz00_3689
																																				=
																																				BgL_cdrzd22150zd2_1456;
																																			BgL_cleanupz00_1521
																																				=
																																				BgL_cleanupz00_3689;
																																			BgL_expz00_1520
																																				=
																																				BgL_expz00_3688;
																																			goto
																																				BgL_zc3z04anonymousza32314ze3z87_1522;
																																		}
																																	}
																																else
																																	{	/* Expand/exit.scm 314 */
																																		goto
																																			BgL_tagzd21973zd2_1399;
																																	}
																															}
																														}
																												}
																											else
																												{	/* Expand/exit.scm 314 */
																													obj_t
																														BgL_cdrzd22162zd2_1460;
																													BgL_cdrzd22162zd2_1460
																														=
																														CDR(((obj_t)
																															BgL_xz00_33));
																													{	/* Expand/exit.scm 314 */
																														obj_t
																															BgL_cdrzd22167zd2_1461;
																														BgL_cdrzd22167zd2_1461
																															=
																															CDR(((obj_t)
																																BgL_cdrzd22162zd2_1460));
																														if (PAIRP
																															(BgL_cdrzd22167zd2_1461))
																															{	/* Expand/exit.scm 314 */
																																obj_t
																																	BgL_arg2272z00_1463;
																																BgL_arg2272z00_1463
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd22162zd2_1460));
																																{
																																	obj_t
																																		BgL_cleanupz00_3699;
																																	obj_t
																																		BgL_expz00_3698;
																																	BgL_expz00_3698
																																		=
																																		BgL_arg2272z00_1463;
																																	BgL_cleanupz00_3699
																																		=
																																		BgL_cdrzd22167zd2_1461;
																																	BgL_cleanupz00_1521
																																		=
																																		BgL_cleanupz00_3699;
																																	BgL_expz00_1520
																																		=
																																		BgL_expz00_3698;
																																	goto
																																		BgL_zc3z04anonymousza32314ze3z87_1522;
																																}
																															}
																														else
																															{	/* Expand/exit.scm 314 */
																																goto
																																	BgL_tagzd21973zd2_1399;
																															}
																													}
																												}
																										}
																									else
																										{	/* Expand/exit.scm 314 */
																											obj_t
																												BgL_cdrzd22179zd2_1465;
																											BgL_cdrzd22179zd2_1465 =
																												CDR(((obj_t)
																													BgL_xz00_33));
																											{	/* Expand/exit.scm 314 */
																												obj_t
																													BgL_cdrzd22184zd2_1466;
																												BgL_cdrzd22184zd2_1466 =
																													CDR(((obj_t)
																														BgL_cdrzd22179zd2_1465));
																												if (PAIRP
																													(BgL_cdrzd22184zd2_1466))
																													{	/* Expand/exit.scm 314 */
																														obj_t
																															BgL_arg2275z00_1468;
																														BgL_arg2275z00_1468
																															=
																															CAR(((obj_t)
																																BgL_cdrzd22179zd2_1465));
																														{
																															obj_t
																																BgL_cleanupz00_3709;
																															obj_t
																																BgL_expz00_3708;
																															BgL_expz00_3708 =
																																BgL_arg2275z00_1468;
																															BgL_cleanupz00_3709
																																=
																																BgL_cdrzd22184zd2_1466;
																															BgL_cleanupz00_1521
																																=
																																BgL_cleanupz00_3709;
																															BgL_expz00_1520 =
																																BgL_expz00_3708;
																															goto
																																BgL_zc3z04anonymousza32314ze3z87_1522;
																														}
																													}
																												else
																													{	/* Expand/exit.scm 314 */
																														goto
																															BgL_tagzd21973zd2_1399;
																													}
																											}
																										}
																								}
																							else
																								{	/* Expand/exit.scm 314 */
																									obj_t BgL_cdrzd22196zd2_1469;

																									BgL_cdrzd22196zd2_1469 =
																										CDR(((obj_t) BgL_xz00_33));
																									{	/* Expand/exit.scm 314 */
																										obj_t
																											BgL_cdrzd22201zd2_1470;
																										BgL_cdrzd22201zd2_1470 =
																											CDR(((obj_t)
																												BgL_cdrzd22196zd2_1469));
																										if (PAIRP
																											(BgL_cdrzd22201zd2_1470))
																											{	/* Expand/exit.scm 314 */
																												obj_t
																													BgL_arg2277z00_1472;
																												BgL_arg2277z00_1472 =
																													CAR(((obj_t)
																														BgL_cdrzd22196zd2_1469));
																												{
																													obj_t
																														BgL_cleanupz00_3719;
																													obj_t BgL_expz00_3718;

																													BgL_expz00_3718 =
																														BgL_arg2277z00_1472;
																													BgL_cleanupz00_3719 =
																														BgL_cdrzd22201zd2_1470;
																													BgL_cleanupz00_1521 =
																														BgL_cleanupz00_3719;
																													BgL_expz00_1520 =
																														BgL_expz00_3718;
																													goto
																														BgL_zc3z04anonymousza32314ze3z87_1522;
																												}
																											}
																										else
																											{	/* Expand/exit.scm 314 */
																												goto
																													BgL_tagzd21973zd2_1399;
																											}
																									}
																								}
																						}
																					else
																						{	/* Expand/exit.scm 314 */
																							obj_t BgL_cdrzd22213zd2_1473;

																							BgL_cdrzd22213zd2_1473 =
																								CDR(((obj_t) BgL_xz00_33));
																							{	/* Expand/exit.scm 314 */
																								obj_t BgL_cdrzd22218zd2_1474;

																								BgL_cdrzd22218zd2_1474 =
																									CDR(
																									((obj_t)
																										BgL_cdrzd22213zd2_1473));
																								if (PAIRP
																									(BgL_cdrzd22218zd2_1474))
																									{	/* Expand/exit.scm 314 */
																										obj_t BgL_arg2279z00_1476;

																										BgL_arg2279z00_1476 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd22213zd2_1473));
																										{
																											obj_t BgL_cleanupz00_3729;
																											obj_t BgL_expz00_3728;

																											BgL_expz00_3728 =
																												BgL_arg2279z00_1476;
																											BgL_cleanupz00_3729 =
																												BgL_cdrzd22218zd2_1474;
																											BgL_cleanupz00_1521 =
																												BgL_cleanupz00_3729;
																											BgL_expz00_1520 =
																												BgL_expz00_3728;
																											goto
																												BgL_zc3z04anonymousza32314ze3z87_1522;
																										}
																									}
																								else
																									{	/* Expand/exit.scm 314 */
																										goto BgL_tagzd21973zd2_1399;
																									}
																							}
																						}
																				}
																			else
																				{	/* Expand/exit.scm 314 */
																					obj_t BgL_cdrzd22230zd2_1477;

																					BgL_cdrzd22230zd2_1477 =
																						CDR(((obj_t) BgL_xz00_33));
																					{	/* Expand/exit.scm 314 */
																						obj_t BgL_cdrzd22235zd2_1478;

																						BgL_cdrzd22235zd2_1478 =
																							CDR(
																							((obj_t) BgL_cdrzd22230zd2_1477));
																						if (PAIRP(BgL_cdrzd22235zd2_1478))
																							{	/* Expand/exit.scm 314 */
																								obj_t BgL_arg2281z00_1480;

																								BgL_arg2281z00_1480 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd22230zd2_1477));
																								{
																									obj_t BgL_cleanupz00_3739;
																									obj_t BgL_expz00_3738;

																									BgL_expz00_3738 =
																										BgL_arg2281z00_1480;
																									BgL_cleanupz00_3739 =
																										BgL_cdrzd22235zd2_1478;
																									BgL_cleanupz00_1521 =
																										BgL_cleanupz00_3739;
																									BgL_expz00_1520 =
																										BgL_expz00_3738;
																									goto
																										BgL_zc3z04anonymousza32314ze3z87_1522;
																								}
																							}
																						else
																							{	/* Expand/exit.scm 314 */
																								goto BgL_tagzd21973zd2_1399;
																							}
																					}
																				}
																		}
																	else
																		{	/* Expand/exit.scm 314 */
																			obj_t BgL_cdrzd22247zd2_1481;

																			BgL_cdrzd22247zd2_1481 =
																				CDR(((obj_t) BgL_xz00_33));
																			{	/* Expand/exit.scm 314 */
																				obj_t BgL_cdrzd22252zd2_1482;

																				BgL_cdrzd22252zd2_1482 =
																					CDR(((obj_t) BgL_cdrzd22247zd2_1481));
																				if (PAIRP(BgL_cdrzd22252zd2_1482))
																					{	/* Expand/exit.scm 314 */
																						obj_t BgL_arg2283z00_1484;

																						BgL_arg2283z00_1484 =
																							CAR(
																							((obj_t) BgL_cdrzd22247zd2_1481));
																						{
																							obj_t BgL_cleanupz00_3749;
																							obj_t BgL_expz00_3748;

																							BgL_expz00_3748 =
																								BgL_arg2283z00_1484;
																							BgL_cleanupz00_3749 =
																								BgL_cdrzd22252zd2_1482;
																							BgL_cleanupz00_1521 =
																								BgL_cleanupz00_3749;
																							BgL_expz00_1520 = BgL_expz00_3748;
																							goto
																								BgL_zc3z04anonymousza32314ze3z87_1522;
																						}
																					}
																				else
																					{	/* Expand/exit.scm 314 */
																						goto BgL_tagzd21973zd2_1399;
																					}
																			}
																		}
																}
															}
														}
												}
											else
												{	/* Expand/exit.scm 314 */
													obj_t BgL_cdrzd22277zd2_1488;

													BgL_cdrzd22277zd2_1488 =
														CDR(((obj_t) BgL_cdrzd21982zd2_1402));
													if (PAIRP(BgL_cdrzd22277zd2_1488))
														{	/* Expand/exit.scm 314 */
															obj_t BgL_arg2288z00_1490;

															BgL_arg2288z00_1490 =
																CAR(((obj_t) BgL_cdrzd21982zd2_1402));
															{
																obj_t BgL_cleanupz00_3757;
																obj_t BgL_expz00_3756;

																BgL_expz00_3756 = BgL_arg2288z00_1490;
																BgL_cleanupz00_3757 = BgL_cdrzd22277zd2_1488;
																BgL_cleanupz00_1521 = BgL_cleanupz00_3757;
																BgL_expz00_1520 = BgL_expz00_3756;
																goto BgL_zc3z04anonymousza32314ze3z87_1522;
															}
														}
													else
														{	/* Expand/exit.scm 314 */
															goto BgL_tagzd21973zd2_1399;
														}
												}
										}
									else
										{	/* Expand/exit.scm 314 */
											obj_t BgL_cdrzd22302zd2_1492;

											BgL_cdrzd22302zd2_1492 =
												CDR(((obj_t) BgL_cdrzd21982zd2_1402));
											if (PAIRP(BgL_cdrzd22302zd2_1492))
												{	/* Expand/exit.scm 314 */
													obj_t BgL_arg2290z00_1494;

													BgL_arg2290z00_1494 =
														CAR(((obj_t) BgL_cdrzd21982zd2_1402));
													{
														obj_t BgL_cleanupz00_3765;
														obj_t BgL_expz00_3764;

														BgL_expz00_3764 = BgL_arg2290z00_1494;
														BgL_cleanupz00_3765 = BgL_cdrzd22302zd2_1492;
														BgL_cleanupz00_1521 = BgL_cleanupz00_3765;
														BgL_expz00_1520 = BgL_expz00_3764;
														goto BgL_zc3z04anonymousza32314ze3z87_1522;
													}
												}
											else
												{	/* Expand/exit.scm 314 */
													goto BgL_tagzd21973zd2_1399;
												}
										}
								}
							else
								{	/* Expand/exit.scm 314 */
									goto BgL_tagzd21973zd2_1399;
								}
						}
					else
						{	/* Expand/exit.scm 314 */
							goto BgL_tagzd21973zd2_1399;
						}
				}
			}
		}

	}



/* trace?~0 */
	bool_t BGl_tracezf3ze70z14zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 259 */
			{	/* Expand/exit.scm 258 */
				bool_t BgL_test2800z00_3766;

				{	/* Expand/exit.scm 258 */
					int BgL_arg2293z00_1498;

					BgL_arg2293z00_1498 = BGl_bigloozd2compilerzd2debugz00zz__paramz00();
					BgL_test2800z00_3766 = ((long) (BgL_arg2293z00_1498) > 0L);
				}
				if (BgL_test2800z00_3766)
					{	/* Expand/exit.scm 259 */
						obj_t BgL_arg2292z00_1497;

						BgL_arg2292z00_1497 = BGl_thezd2backendzd2zzbackend_backendz00();
						return
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg2292z00_1497)))->
							BgL_tracezd2supportzd2);
					}
				else
					{	/* Expand/exit.scm 258 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &expand-unwind-protect */
	obj_t BGl_z62expandzd2unwindzd2protectz62zzexpand_exitz00(obj_t
		BgL_envz00_2163, obj_t BgL_xz00_2164, obj_t BgL_ez00_2165)
	{
		{	/* Expand/exit.scm 255 */
			return
				BGl_expandzd2unwindzd2protectz00zzexpand_exitz00(BgL_xz00_2164,
				BgL_ez00_2165);
		}

	}



/* expand-with-handler */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2withzd2handlerz00zzexpand_exitz00(obj_t
		BgL_xz00_35, obj_t BgL_ez00_36)
	{
		{	/* Expand/exit.scm 327 */
			{
				obj_t BgL_handlerz00_1686;
				obj_t BgL_bodyz00_1687;

				{

					if (PAIRP(BgL_xz00_35))
						{	/* Expand/exit.scm 356 */
							obj_t BgL_cdrzd22338zd2_1681;

							BgL_cdrzd22338zd2_1681 = CDR(((obj_t) BgL_xz00_35));
							if (PAIRP(BgL_cdrzd22338zd2_1681))
								{	/* Expand/exit.scm 356 */
									obj_t BgL_arg2473z00_1683;
									obj_t BgL_arg2474z00_1684;

									BgL_arg2473z00_1683 = CAR(BgL_cdrzd22338zd2_1681);
									BgL_arg2474z00_1684 = CDR(BgL_cdrzd22338zd2_1681);
									{	/* Expand/exit.scm 358 */
										obj_t BgL_auxz00_3782;

										BgL_handlerz00_1686 = BgL_arg2473z00_1683;
										BgL_bodyz00_1687 = BgL_arg2474z00_1684;
										{	/* Expand/exit.scm 330 */
											obj_t BgL_ohsz00_1689;
											obj_t BgL_hdsz00_1690;
											obj_t BgL_errz00_1691;
											obj_t BgL_cellz00_1692;
											obj_t BgL_escapez00_1693;
											obj_t BgL_hdlz00_1694;
											obj_t BgL_ehdlz00_1695;
											obj_t BgL_valz00_1696;
											obj_t BgL_envz00_1697;

											BgL_ohsz00_1689 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(40));
											BgL_hdsz00_1690 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(41));
											BgL_errz00_1691 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(42));
											BgL_cellz00_1692 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(43));
											BgL_escapez00_1693 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(44));
											BgL_hdlz00_1694 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(45));
											BgL_ehdlz00_1695 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(46));
											BgL_valz00_1696 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(5));
											BgL_envz00_1697 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(13));
											{	/* Expand/exit.scm 339 */
												obj_t BgL_arg2479z00_1698;

												{	/* Expand/exit.scm 339 */
													obj_t BgL_arg2480z00_1699;

													{	/* Expand/exit.scm 339 */
														obj_t BgL_arg2481z00_1700;
														obj_t BgL_arg2482z00_1701;

														{	/* Expand/exit.scm 339 */
															obj_t BgL_arg2483z00_1702;
															obj_t BgL_arg2484z00_1703;

															{	/* Expand/exit.scm 339 */
																obj_t BgL_arg2486z00_1704;

																BgL_arg2486z00_1704 =
																	MAKE_YOUNG_PAIR(BgL_handlerz00_1686, BNIL);
																BgL_arg2483z00_1702 =
																	MAKE_YOUNG_PAIR(BgL_hdlz00_1694,
																	BgL_arg2486z00_1704);
															}
															{	/* Expand/exit.scm 340 */
																obj_t BgL_arg2487z00_1705;

																{	/* Expand/exit.scm 340 */
																	obj_t BgL_arg2488z00_1706;

																	{	/* Expand/exit.scm 340 */
																		obj_t BgL_arg2490z00_1707;

																		BgL_arg2490z00_1707 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BNIL);
																		BgL_arg2488z00_1706 =
																			MAKE_YOUNG_PAIR(BgL_arg2490z00_1707,
																			BNIL);
																	}
																	BgL_arg2487z00_1705 =
																		MAKE_YOUNG_PAIR(BgL_envz00_1697,
																		BgL_arg2488z00_1706);
																}
																BgL_arg2484z00_1703 =
																	MAKE_YOUNG_PAIR(BgL_arg2487z00_1705, BNIL);
															}
															BgL_arg2481z00_1700 =
																MAKE_YOUNG_PAIR(BgL_arg2483z00_1702,
																BgL_arg2484z00_1703);
														}
														{	/* Expand/exit.scm 341 */
															obj_t BgL_arg2491z00_1708;

															{	/* Expand/exit.scm 341 */
																obj_t BgL_arg2492z00_1709;

																{	/* Expand/exit.scm 341 */
																	obj_t BgL_arg2493z00_1710;
																	obj_t BgL_arg2495z00_1711;

																	{	/* Expand/exit.scm 341 */
																		obj_t BgL_arg2497z00_1712;

																		{	/* Expand/exit.scm 341 */
																			obj_t BgL_arg2500z00_1713;

																			{	/* Expand/exit.scm 341 */
																				obj_t BgL_arg2501z00_1714;

																				{	/* Expand/exit.scm 341 */
																					obj_t BgL_arg2502z00_1715;

																					BgL_arg2502z00_1715 =
																						MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
																					BgL_arg2501z00_1714 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(47),
																						BgL_arg2502z00_1715);
																				}
																				BgL_arg2500z00_1713 =
																					MAKE_YOUNG_PAIR(BgL_arg2501z00_1714,
																					BNIL);
																			}
																			BgL_arg2497z00_1712 =
																				MAKE_YOUNG_PAIR(BgL_cellz00_1692,
																				BgL_arg2500z00_1713);
																		}
																		BgL_arg2493z00_1710 =
																			MAKE_YOUNG_PAIR(BgL_arg2497z00_1712,
																			BNIL);
																	}
																	{	/* Expand/exit.scm 342 */
																		obj_t BgL_arg2503z00_1716;

																		{	/* Expand/exit.scm 342 */
																			obj_t BgL_arg2505z00_1717;

																			{	/* Expand/exit.scm 342 */
																				obj_t BgL_arg2506z00_1718;
																				obj_t BgL_arg2508z00_1719;

																				{	/* Expand/exit.scm 342 */
																					obj_t BgL_arg2509z00_1720;

																					{	/* Expand/exit.scm 342 */
																						obj_t BgL_arg2510z00_1721;

																						{	/* Expand/exit.scm 342 */
																							obj_t BgL_arg2511z00_1722;

																							{	/* Expand/exit.scm 342 */
																								obj_t BgL_arg2512z00_1723;

																								{	/* Expand/exit.scm 342 */
																									obj_t BgL_arg2513z00_1724;

																									{	/* Expand/exit.scm 342 */
																										obj_t BgL_arg2514z00_1725;

																										{	/* Expand/exit.scm 342 */
																											obj_t BgL_arg2515z00_1726;
																											obj_t BgL_arg2516z00_1727;

																											BgL_arg2515z00_1726 =
																												MAKE_YOUNG_PAIR
																												(BgL_escapez00_1693,
																												BNIL);
																											{	/* Expand/exit.scm 343 */
																												obj_t
																													BgL_arg2518z00_1728;
																												{	/* Expand/exit.scm 343 */
																													obj_t
																														BgL_arg2519z00_1729;
																													{	/* Expand/exit.scm 343 */
																														obj_t
																															BgL_arg2521z00_1730;
																														obj_t
																															BgL_arg2524z00_1731;
																														{	/* Expand/exit.scm 343 */
																															obj_t
																																BgL_arg2525z00_1732;
																															{	/* Expand/exit.scm 343 */
																																obj_t
																																	BgL_arg2526z00_1733;
																																{	/* Expand/exit.scm 343 */
																																	obj_t
																																		BgL_arg2527z00_1734;
																																	{	/* Expand/exit.scm 343 */
																																		obj_t
																																			BgL_arg2528z00_1735;
																																		BgL_arg2528z00_1735
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_envz00_1697,
																																			BNIL);
																																		BgL_arg2527z00_1734
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(48),
																																			BgL_arg2528z00_1735);
																																	}
																																	BgL_arg2526z00_1733
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2527z00_1734,
																																		BNIL);
																																}
																																BgL_arg2525z00_1732
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_ohsz00_1689,
																																	BgL_arg2526z00_1733);
																															}
																															BgL_arg2521z00_1730
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2525z00_1732,
																																BNIL);
																														}
																														{	/* Expand/exit.scm 344 */
																															obj_t
																																BgL_arg2529z00_1736;
																															{	/* Expand/exit.scm 344 */
																																obj_t
																																	BgL_arg2534z00_1737;
																																{	/* Expand/exit.scm 344 */
																																	obj_t
																																		BgL_arg2536z00_1738;
																																	obj_t
																																		BgL_arg2537z00_1739;
																																	{	/* Expand/exit.scm 344 */
																																		obj_t
																																			BgL_arg2538z00_1740;
																																		{	/* Expand/exit.scm 344 */
																																			obj_t
																																				BgL_arg2539z00_1741;
																																			{	/* Expand/exit.scm 344 */
																																				obj_t
																																					BgL_arg2540z00_1742;
																																				{	/* Expand/exit.scm 344 */
																																					obj_t
																																						BgL_arg2542z00_1743;
																																					{	/* Expand/exit.scm 344 */
																																						obj_t
																																							BgL_arg2543z00_1744;
																																						BgL_arg2543z00_1744
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_cellz00_1692,
																																							BNIL);
																																						BgL_arg2542z00_1743
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_escapez00_1693,
																																							BgL_arg2543z00_1744);
																																					}
																																					BgL_arg2540z00_1742
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(49),
																																						BgL_arg2542z00_1743);
																																				}
																																				BgL_arg2539z00_1741
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2540z00_1742,
																																					BNIL);
																																			}
																																			BgL_arg2538z00_1740
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_hdsz00_1690,
																																				BgL_arg2539z00_1741);
																																		}
																																		BgL_arg2536z00_1738
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2538z00_1740,
																																			BNIL);
																																	}
																																	{	/* Expand/exit.scm 345 */
																																		obj_t
																																			BgL_arg2545z00_1745;
																																		obj_t
																																			BgL_arg2546z00_1746;
																																		{	/* Expand/exit.scm 345 */
																																			obj_t
																																				BgL_arg2548z00_1747;
																																			{	/* Expand/exit.scm 345 */
																																				obj_t
																																					BgL_arg2549z00_1748;
																																				BgL_arg2549z00_1748
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_hdsz00_1690,
																																					BNIL);
																																				BgL_arg2548z00_1747
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_envz00_1697,
																																					BgL_arg2549z00_1748);
																																			}
																																			BgL_arg2545z00_1745
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(34),
																																				BgL_arg2548z00_1747);
																																		}
																																		{	/* Expand/exit.scm 347 */
																																			obj_t
																																				BgL_arg2551z00_1749;
																																			{	/* Expand/exit.scm 347 */
																																				obj_t
																																					BgL_arg2552z00_1750;
																																				{	/* Expand/exit.scm 347 */
																																					obj_t
																																						BgL_arg2553z00_1751;
																																					obj_t
																																						BgL_arg2554z00_1752;
																																					{	/* Expand/exit.scm 347 */
																																						obj_t
																																							BgL_arg2555z00_1753;
																																						BgL_arg2555z00_1753
																																							=
																																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																							(BgL_bodyz00_1687,
																																							BNIL);
																																						BgL_arg2553z00_1751
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(12),
																																							BgL_arg2555z00_1753);
																																					}
																																					{	/* Expand/exit.scm 348 */
																																						obj_t
																																							BgL_arg2556z00_1754;
																																						{	/* Expand/exit.scm 348 */
																																							obj_t
																																								BgL_arg2557z00_1755;
																																							{	/* Expand/exit.scm 348 */
																																								obj_t
																																									BgL_arg2560z00_1756;
																																								BgL_arg2560z00_1756
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_ohsz00_1689,
																																									BNIL);
																																								BgL_arg2557z00_1755
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_envz00_1697,
																																									BgL_arg2560z00_1756);
																																							}
																																							BgL_arg2556z00_1754
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(34),
																																								BgL_arg2557z00_1755);
																																						}
																																						BgL_arg2554z00_1752
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2556z00_1754,
																																							BNIL);
																																					}
																																					BgL_arg2552z00_1750
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2553z00_1751,
																																						BgL_arg2554z00_1752);
																																				}
																																				BgL_arg2551z00_1749
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(50),
																																					BgL_arg2552z00_1750);
																																			}
																																			BgL_arg2546z00_1746
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2551z00_1749,
																																				BNIL);
																																		}
																																		BgL_arg2537z00_1739
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2545z00_1745,
																																			BgL_arg2546z00_1746);
																																	}
																																	BgL_arg2534z00_1737
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2536z00_1738,
																																		BgL_arg2537z00_1739);
																																}
																																BgL_arg2529z00_1736
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(11),
																																	BgL_arg2534z00_1737);
																															}
																															BgL_arg2524z00_1731
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2529z00_1736,
																																BNIL);
																														}
																														BgL_arg2519z00_1729
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg2521z00_1730,
																															BgL_arg2524z00_1731);
																													}
																													BgL_arg2518z00_1728 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(11),
																														BgL_arg2519z00_1729);
																												}
																												BgL_arg2516z00_1727 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg2518z00_1728,
																													BNIL);
																											}
																											BgL_arg2514z00_1725 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2515z00_1726,
																												BgL_arg2516z00_1727);
																										}
																										BgL_arg2513z00_1724 =
																											MAKE_YOUNG_PAIR
																											(BgL_envz00_1697,
																											BgL_arg2514z00_1725);
																									}
																									BgL_arg2512z00_1723 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(22),
																										BgL_arg2513z00_1724);
																								}
																								BgL_arg2511z00_1722 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(28), BgL_arg2512z00_1723);
																							}
																							BgL_arg2510z00_1721 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2511z00_1722, BNIL);
																						}
																						BgL_arg2509z00_1720 =
																							MAKE_YOUNG_PAIR(BgL_valz00_1696,
																							BgL_arg2510z00_1721);
																					}
																					BgL_arg2506z00_1718 =
																						MAKE_YOUNG_PAIR(BgL_arg2509z00_1720,
																						BNIL);
																				}
																				{	/* Expand/exit.scm 349 */
																					obj_t BgL_arg2561z00_1757;

																					{	/* Expand/exit.scm 349 */
																						obj_t BgL_arg2562z00_1758;

																						{	/* Expand/exit.scm 349 */
																							obj_t BgL_arg2563z00_1759;
																							obj_t BgL_arg2564z00_1760;

																							{	/* Expand/exit.scm 349 */
																								obj_t BgL_arg2565z00_1761;

																								{	/* Expand/exit.scm 349 */
																									obj_t BgL_arg2566z00_1762;

																									BgL_arg2566z00_1762 =
																										MAKE_YOUNG_PAIR
																										(BgL_cellz00_1692, BNIL);
																									BgL_arg2565z00_1761 =
																										MAKE_YOUNG_PAIR
																										(BgL_valz00_1696,
																										BgL_arg2566z00_1762);
																								}
																								BgL_arg2563z00_1759 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(51), BgL_arg2565z00_1761);
																							}
																							{	/* Expand/exit.scm 351 */
																								obj_t BgL_arg2567z00_1763;
																								obj_t BgL_arg2568z00_1764;

																								{	/* Expand/exit.scm 351 */
																									obj_t BgL_arg2572z00_1765;

																									{	/* Expand/exit.scm 351 */
																										obj_t BgL_arg2578z00_1766;
																										obj_t BgL_arg2579z00_1767;

																										{	/* Expand/exit.scm 351 */
																											obj_t BgL_arg2584z00_1768;

																											BgL_arg2584z00_1768 =
																												MAKE_YOUNG_PAIR(BINT
																												(0L), BNIL);
																											BgL_arg2578z00_1766 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(52),
																												BgL_arg2584z00_1768);
																										}
																										{	/* Expand/exit.scm 352 */
																											obj_t BgL_arg2585z00_1769;

																											{	/* Expand/exit.scm 352 */
																												obj_t
																													BgL_arg2586z00_1770;
																												{	/* Expand/exit.scm 352 */
																													obj_t
																														BgL_arg2587z00_1771;
																													{	/* Expand/exit.scm 352 */
																														obj_t
																															BgL_arg2589z00_1772;
																														BgL_arg2589z00_1772
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_valz00_1696,
																															BNIL);
																														BgL_arg2587z00_1771
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(53),
																															BgL_arg2589z00_1772);
																													}
																													BgL_arg2586z00_1770 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2587z00_1771,
																														BNIL);
																												}
																												BgL_arg2585z00_1769 =
																													MAKE_YOUNG_PAIR
																													(BgL_hdlz00_1694,
																													BgL_arg2586z00_1770);
																											}
																											BgL_arg2579z00_1767 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2585z00_1769,
																												BNIL);
																										}
																										BgL_arg2572z00_1765 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2578z00_1766,
																											BgL_arg2579z00_1767);
																									}
																									BgL_arg2567z00_1763 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(12),
																										BgL_arg2572z00_1765);
																								}
																								BgL_arg2568z00_1764 =
																									MAKE_YOUNG_PAIR
																									(BgL_valz00_1696, BNIL);
																								BgL_arg2564z00_1760 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2567z00_1763,
																									BgL_arg2568z00_1764);
																							}
																							BgL_arg2562z00_1758 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2563z00_1759,
																								BgL_arg2564z00_1760);
																						}
																						BgL_arg2561z00_1757 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(27), BgL_arg2562z00_1758);
																					}
																					BgL_arg2508z00_1719 =
																						MAKE_YOUNG_PAIR(BgL_arg2561z00_1757,
																						BNIL);
																				}
																				BgL_arg2505z00_1717 =
																					MAKE_YOUNG_PAIR(BgL_arg2506z00_1718,
																					BgL_arg2508z00_1719);
																			}
																			BgL_arg2503z00_1716 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
																				BgL_arg2505z00_1717);
																		}
																		BgL_arg2495z00_1711 =
																			MAKE_YOUNG_PAIR(BgL_arg2503z00_1716,
																			BNIL);
																	}
																	BgL_arg2492z00_1709 =
																		MAKE_YOUNG_PAIR(BgL_arg2493z00_1710,
																		BgL_arg2495z00_1711);
																}
																BgL_arg2491z00_1708 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
																	BgL_arg2492z00_1709);
															}
															BgL_arg2482z00_1701 =
																MAKE_YOUNG_PAIR(BgL_arg2491z00_1708, BNIL);
														}
														BgL_arg2480z00_1699 =
															MAKE_YOUNG_PAIR(BgL_arg2481z00_1700,
															BgL_arg2482z00_1701);
													}
													BgL_arg2479z00_1698 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
														BgL_arg2480z00_1699);
												}
												BgL_auxz00_3782 =
													BGL_PROCEDURE_CALL2(BgL_ez00_36, BgL_arg2479z00_1698,
													BgL_ez00_36);
											}
										}
										return
											BGl_replacez12z12zztools_miscz00(BgL_xz00_35,
											BgL_auxz00_3782);
									}
								}
							else
								{	/* Expand/exit.scm 356 */
								BgL_tagzd22331zd2_1678:
									return
										BGl_errorz00zz__errorz00(BFALSE,
										BGl_string2597z00zzexpand_exitz00, BgL_xz00_35);
								}
						}
					else
						{	/* Expand/exit.scm 356 */
							goto BgL_tagzd22331zd2_1678;
						}
				}
			}
		}

	}



/* &expand-with-handler */
	obj_t BGl_z62expandzd2withzd2handlerz62zzexpand_exitz00(obj_t BgL_envz00_2166,
		obj_t BgL_xz00_2167, obj_t BgL_ez00_2168)
	{
		{	/* Expand/exit.scm 327 */
			return
				BGl_expandzd2withzd2handlerz00zzexpand_exitz00(BgL_xz00_2167,
				BgL_ez00_2168);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_exitz00(void)
	{
		{	/* Expand/exit.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zzexpand_expanderz00(393376L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zzexpand_lambdaz00(223654864L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2598z00zzexpand_exitz00));
		}

	}

#ifdef __cplusplus
}
#endif
