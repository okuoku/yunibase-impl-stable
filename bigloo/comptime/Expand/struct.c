/*===========================================================================*/
/*   (Expand/struct.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/struct.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_STRUCT_TYPE_DEFINITIONS
#define BGL_EXPAND_STRUCT_TYPE_DEFINITIONS
#endif													// BGL_EXPAND_STRUCT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62expandzd2structzb0zzexpand_structz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_structz00 = BUNSPEC;
	extern obj_t BGl_epairifyz00zztools_miscz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_structz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_structz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_structz00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2structzd2zzexpand_structz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_matchzd2definezd2structurez12z12zz__match_normaliza7eza7(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2unsafezd2structza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzexpand_structz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzexpand_structz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_structz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_structz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_structz00(void);
	static obj_t __cnst[34];


	   
		 
		DEFINE_STRING(BGl_string1705z00zzexpand_structz00,
		BgL_bgl_string1705za700za7za7e1711za7, "Illegal `define-struct' form", 28);
	      DEFINE_STRING(BGl_string1706z00zzexpand_structz00,
		BgL_bgl_string1706za700za7za7e1712za7, "struct-ref:not an instance of", 29);
	      DEFINE_STRING(BGl_string1707z00zzexpand_structz00,
		BgL_bgl_string1707za700za7za7e1713za7, "struct-set!:not an instance of",
		30);
	      DEFINE_STRING(BGl_string1708z00zzexpand_structz00,
		BgL_bgl_string1708za700za7za7e1714za7, "expand_struct", 13);
	      DEFINE_STRING(BGl_string1709z00zzexpand_structz00,
		BgL_bgl_string1709za700za7za7e1715za7,
		"begin u-struct-set! struct-set! v error u-struct-ref struct-ref s ((unspecified)) eq? struct-key struct? o ? let - -set! new $create-struct define-inline if car quote @ make-struct __structure apply not null? cdr pair? init make- (quote ()) ",
		241);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2structzd2envz00zzexpand_structz00,
		BgL_bgl_za762expandza7d2stru1716z00,
		BGl_z62expandzd2structzb0zzexpand_structz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzexpand_structz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzexpand_structz00(long
		BgL_checksumz00_384, char *BgL_fromz00_385)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_structz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_structz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_structz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_structz00();
					BGl_cnstzd2initzd2zzexpand_structz00();
					BGl_importedzd2moduleszd2initz00zzexpand_structz00();
					return BGl_methodzd2initzd2zzexpand_structz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_structz00(void)
	{
		{	/* Expand/struct.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_struct");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"expand_struct");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"expand_struct");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_struct");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"expand_struct");
			BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(0L,
				"expand_struct");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_struct");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_struct");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_structz00(void)
	{
		{	/* Expand/struct.scm 15 */
			{	/* Expand/struct.scm 15 */
				obj_t BgL_cportz00_373;

				{	/* Expand/struct.scm 15 */
					obj_t BgL_stringz00_380;

					BgL_stringz00_380 = BGl_string1709z00zzexpand_structz00;
					{	/* Expand/struct.scm 15 */
						obj_t BgL_startz00_381;

						BgL_startz00_381 = BINT(0L);
						{	/* Expand/struct.scm 15 */
							obj_t BgL_endz00_382;

							BgL_endz00_382 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_380)));
							{	/* Expand/struct.scm 15 */

								BgL_cportz00_373 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_380, BgL_startz00_381, BgL_endz00_382);
				}}}}
				{
					long BgL_iz00_374;

					BgL_iz00_374 = 33L;
				BgL_loopz00_375:
					if ((BgL_iz00_374 == -1L))
						{	/* Expand/struct.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/struct.scm 15 */
							{	/* Expand/struct.scm 15 */
								obj_t BgL_arg1710z00_376;

								{	/* Expand/struct.scm 15 */

									{	/* Expand/struct.scm 15 */
										obj_t BgL_locationz00_378;

										BgL_locationz00_378 = BBOOL(((bool_t) 0));
										{	/* Expand/struct.scm 15 */

											BgL_arg1710z00_376 =
												BGl_readz00zz__readerz00(BgL_cportz00_373,
												BgL_locationz00_378);
										}
									}
								}
								{	/* Expand/struct.scm 15 */
									int BgL_tmpz00_411;

									BgL_tmpz00_411 = (int) (BgL_iz00_374);
									CNST_TABLE_SET(BgL_tmpz00_411, BgL_arg1710z00_376);
							}}
							{	/* Expand/struct.scm 15 */
								int BgL_auxz00_379;

								BgL_auxz00_379 = (int) ((BgL_iz00_374 - 1L));
								{
									long BgL_iz00_416;

									BgL_iz00_416 = (long) (BgL_auxz00_379);
									BgL_iz00_374 = BgL_iz00_416;
									goto BgL_loopz00_375;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_structz00(void)
	{
		{	/* Expand/struct.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* expand-struct */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2structzd2zzexpand_structz00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Expand/struct.scm 23 */
			{
				obj_t BgL_namez00_13;
				obj_t BgL_slotsz00_14;

				if (PAIRP(BgL_xz00_3))
					{	/* Expand/struct.scm 24 */
						obj_t BgL_cdrzd2109zd2_19;

						BgL_cdrzd2109zd2_19 = CDR(((obj_t) BgL_xz00_3));
						if (PAIRP(BgL_cdrzd2109zd2_19))
							{	/* Expand/struct.scm 24 */
								BgL_namez00_13 = CAR(BgL_cdrzd2109zd2_19);
								BgL_slotsz00_14 = CDR(BgL_cdrzd2109zd2_19);
								BGl_matchzd2definezd2structurez12z12zz__match_normaliza7eza7
									(BgL_xz00_3);
								{	/* Expand/struct.scm 27 */
									long BgL_lenz00_23;

									BgL_lenz00_23 = bgl_list_length(BgL_slotsz00_14);
									{	/* Expand/struct.scm 27 */
										obj_t BgL_slotszd2namezd2_24;

										if (NULLP(BgL_slotsz00_14))
											{	/* Expand/struct.scm 28 */
												BgL_slotszd2namezd2_24 = BNIL;
											}
										else
											{	/* Expand/struct.scm 28 */
												obj_t BgL_head1016z00_302;

												BgL_head1016z00_302 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1014z00_304;
													obj_t BgL_tail1017z00_305;

													BgL_l1014z00_304 = BgL_slotsz00_14;
													BgL_tail1017z00_305 = BgL_head1016z00_302;
												BgL_zc3z04anonymousza31680ze3z87_306:
													if (NULLP(BgL_l1014z00_304))
														{	/* Expand/struct.scm 28 */
															BgL_slotszd2namezd2_24 = CDR(BgL_head1016z00_302);
														}
													else
														{	/* Expand/struct.scm 28 */
															obj_t BgL_newtail1018z00_308;

															{	/* Expand/struct.scm 28 */
																obj_t BgL_arg1689z00_310;

																{	/* Expand/struct.scm 28 */
																	obj_t BgL_sz00_311;

																	BgL_sz00_311 =
																		CAR(((obj_t) BgL_l1014z00_304));
																	{

																		if (PAIRP(BgL_sz00_311))
																			{	/* Expand/struct.scm 29 */
																				obj_t BgL_cdrzd2125zd2_319;

																				BgL_cdrzd2125zd2_319 =
																					CDR(((obj_t) BgL_sz00_311));
																				if (PAIRP(BgL_cdrzd2125zd2_319))
																					{	/* Expand/struct.scm 29 */
																						if (NULLP(CDR
																								(BgL_cdrzd2125zd2_319)))
																							{	/* Expand/struct.scm 29 */
																								BgL_arg1689z00_310 =
																									CAR(((obj_t) BgL_sz00_311));
																							}
																						else
																							{	/* Expand/struct.scm 29 */
																							BgL_tagzd2118zd2_316:
																								BgL_arg1689z00_310 =
																									BGl_errorz00zz__errorz00
																									(BFALSE,
																									BGl_string1705z00zzexpand_structz00,
																									BgL_xz00_3);
																							}
																					}
																				else
																					{	/* Expand/struct.scm 29 */
																						goto BgL_tagzd2118zd2_316;
																					}
																			}
																		else
																			{	/* Expand/struct.scm 29 */
																				if (SYMBOLP(BgL_sz00_311))
																					{	/* Expand/struct.scm 29 */
																						BgL_arg1689z00_310 = BgL_sz00_311;
																					}
																				else
																					{	/* Expand/struct.scm 29 */
																						goto BgL_tagzd2118zd2_316;
																					}
																			}
																	}
																}
																BgL_newtail1018z00_308 =
																	MAKE_YOUNG_PAIR(BgL_arg1689z00_310, BNIL);
															}
															SET_CDR(BgL_tail1017z00_305,
																BgL_newtail1018z00_308);
															{	/* Expand/struct.scm 28 */
																obj_t BgL_arg1688z00_309;

																BgL_arg1688z00_309 =
																	CDR(((obj_t) BgL_l1014z00_304));
																{
																	obj_t BgL_tail1017z00_454;
																	obj_t BgL_l1014z00_453;

																	BgL_l1014z00_453 = BgL_arg1688z00_309;
																	BgL_tail1017z00_454 = BgL_newtail1018z00_308;
																	BgL_tail1017z00_305 = BgL_tail1017z00_454;
																	BgL_l1014z00_304 = BgL_l1014z00_453;
																	goto BgL_zc3z04anonymousza31680ze3z87_306;
																}
															}
														}
												}
											}
										{	/* Expand/struct.scm 28 */
											bool_t BgL_slotszd2valzf3z21_25;

											BgL_slotszd2valzf3z21_25 = ((bool_t) 0);
											{	/* Expand/struct.scm 39 */
												obj_t BgL_slotszd2valzd2_26;

												if (NULLP(BgL_slotsz00_14))
													{	/* Expand/struct.scm 40 */
														BgL_slotszd2valzd2_26 = BNIL;
													}
												else
													{	/* Expand/struct.scm 40 */
														obj_t BgL_head1021z00_274;

														BgL_head1021z00_274 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1019z00_276;
															obj_t BgL_tail1022z00_277;

															BgL_l1019z00_276 = BgL_slotsz00_14;
															BgL_tail1022z00_277 = BgL_head1021z00_274;
														BgL_zc3z04anonymousza31648ze3z87_278:
															if (NULLP(BgL_l1019z00_276))
																{	/* Expand/struct.scm 40 */
																	BgL_slotszd2valzd2_26 =
																		CDR(BgL_head1021z00_274);
																}
															else
																{	/* Expand/struct.scm 40 */
																	obj_t BgL_newtail1023z00_280;

																	{	/* Expand/struct.scm 40 */
																		obj_t BgL_arg1651z00_282;

																		{	/* Expand/struct.scm 40 */
																			obj_t BgL_sz00_283;

																			BgL_sz00_283 =
																				CAR(((obj_t) BgL_l1019z00_276));
																			{

																				if (PAIRP(BgL_sz00_283))
																					{	/* Expand/struct.scm 41 */
																						obj_t BgL_cdrzd2138zd2_290;

																						BgL_cdrzd2138zd2_290 =
																							CDR(((obj_t) BgL_sz00_283));
																						if (PAIRP(BgL_cdrzd2138zd2_290))
																							{	/* Expand/struct.scm 41 */
																								if (NULLP(CDR
																										(BgL_cdrzd2138zd2_290)))
																									{	/* Expand/struct.scm 41 */
																										obj_t BgL_arg1661z00_294;

																										BgL_arg1661z00_294 =
																											CAR(BgL_cdrzd2138zd2_290);
																										BgL_slotszd2valzf3z21_25 =
																											((bool_t) 1);
																										BgL_arg1651z00_282 =
																											BgL_arg1661z00_294;
																									}
																								else
																									{	/* Expand/struct.scm 41 */
																									BgL_tagzd2133zd2_287:
																										BgL_arg1651z00_282 =
																											BGl_errorz00zz__errorz00
																											(BFALSE,
																											BGl_string1705z00zzexpand_structz00,
																											BgL_xz00_3);
																									}
																							}
																						else
																							{	/* Expand/struct.scm 41 */
																								goto BgL_tagzd2133zd2_287;
																							}
																					}
																				else
																					{	/* Expand/struct.scm 41 */
																						if (SYMBOLP(BgL_sz00_283))
																							{	/* Expand/struct.scm 41 */
																								BgL_arg1651z00_282 =
																									CNST_TABLE_REF(0);
																							}
																						else
																							{	/* Expand/struct.scm 41 */
																								goto BgL_tagzd2133zd2_287;
																							}
																					}
																			}
																		}
																		BgL_newtail1023z00_280 =
																			MAKE_YOUNG_PAIR(BgL_arg1651z00_282, BNIL);
																	}
																	SET_CDR(BgL_tail1022z00_277,
																		BgL_newtail1023z00_280);
																	{	/* Expand/struct.scm 40 */
																		obj_t BgL_arg1650z00_281;

																		BgL_arg1650z00_281 =
																			CDR(((obj_t) BgL_l1019z00_276));
																		{
																			obj_t BgL_tail1022z00_482;
																			obj_t BgL_l1019z00_481;

																			BgL_l1019z00_481 = BgL_arg1650z00_281;
																			BgL_tail1022z00_482 =
																				BgL_newtail1023z00_280;
																			BgL_tail1022z00_277 = BgL_tail1022z00_482;
																			BgL_l1019z00_276 = BgL_l1019z00_481;
																			goto BgL_zc3z04anonymousza31648ze3z87_278;
																		}
																	}
																}
														}
													}
												{	/* Expand/struct.scm 40 */

													{	/* Expand/struct.scm 57 */
														obj_t BgL_arg1033z00_27;

														{	/* Expand/struct.scm 57 */
															obj_t BgL_arg1035z00_28;
															obj_t BgL_arg1036z00_29;

															{	/* Expand/struct.scm 57 */
																obj_t BgL_arg1037z00_30;

																{	/* Expand/struct.scm 57 */
																	obj_t BgL_arg1038z00_31;

																	{	/* Expand/struct.scm 57 */
																		obj_t BgL_arg1039z00_32;

																		{	/* Expand/struct.scm 57 */
																			obj_t BgL_arg1040z00_33;
																			obj_t BgL_arg1041z00_34;

																			{	/* Expand/struct.scm 57 */
																				obj_t BgL_arg1042z00_35;

																				{	/* Expand/struct.scm 57 */
																					obj_t BgL_arg1044z00_36;

																					{	/* Expand/struct.scm 57 */
																						obj_t BgL_arg1045z00_37;
																						obj_t BgL_arg1046z00_38;

																						{	/* Expand/struct.scm 57 */
																							obj_t BgL_symbolz00_345;

																							BgL_symbolz00_345 =
																								CNST_TABLE_REF(1);
																							{	/* Expand/struct.scm 57 */
																								obj_t BgL_arg1455z00_346;

																								BgL_arg1455z00_346 =
																									SYMBOL_TO_STRING
																									(BgL_symbolz00_345);
																								BgL_arg1045z00_37 =
																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																									(BgL_arg1455z00_346);
																							}
																						}
																						{	/* Expand/struct.scm 57 */
																							obj_t BgL_arg1455z00_348;

																							BgL_arg1455z00_348 =
																								SYMBOL_TO_STRING(
																								((obj_t) BgL_namez00_13));
																							BgL_arg1046z00_38 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_348);
																						}
																						BgL_arg1044z00_36 =
																							string_append(BgL_arg1045z00_37,
																							BgL_arg1046z00_38);
																					}
																					BgL_arg1042z00_35 =
																						bstring_to_symbol
																						(BgL_arg1044z00_36);
																				}
																				BgL_arg1040z00_33 =
																					MAKE_YOUNG_PAIR(BgL_arg1042z00_35,
																					CNST_TABLE_REF(2));
																			}
																			{	/* Expand/struct.scm 58 */
																				obj_t BgL_arg1047z00_39;

																				if (BgL_slotszd2valzf3z21_25)
																					{	/* Expand/struct.scm 59 */
																						obj_t BgL_arg1048z00_40;

																						{	/* Expand/struct.scm 59 */
																							obj_t BgL_arg1049z00_41;
																							obj_t BgL_arg1050z00_42;

																							{	/* Expand/struct.scm 59 */
																								obj_t BgL_arg1051z00_43;

																								BgL_arg1051z00_43 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(2), BNIL);
																								BgL_arg1049z00_41 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(3), BgL_arg1051z00_43);
																							}
																							{	/* Expand/struct.scm 60 */
																								obj_t BgL_arg1052z00_44;
																								obj_t BgL_arg1053z00_45;

																								{	/* Expand/struct.scm 60 */
																									obj_t BgL_arg1054z00_46;

																									{	/* Expand/struct.scm 60 */
																										obj_t BgL_arg1055z00_47;
																										obj_t BgL_arg1056z00_48;

																										{	/* Expand/struct.scm 60 */
																											obj_t BgL_arg1057z00_49;

																											{	/* Expand/struct.scm 60 */
																												obj_t BgL_arg1058z00_50;

																												{	/* Expand/struct.scm 60 */
																													obj_t
																														BgL_arg1059z00_51;
																													{	/* Expand/struct.scm 60 */
																														obj_t
																															BgL_arg1060z00_52;
																														{	/* Expand/struct.scm 60 */
																															obj_t
																																BgL_arg1062z00_53;
																															BgL_arg1062z00_53
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(2), BNIL);
																															BgL_arg1060z00_52
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(4),
																																BgL_arg1062z00_53);
																														}
																														BgL_arg1059z00_51 =
																															MAKE_YOUNG_PAIR
																															(BgL_arg1060z00_52,
																															BNIL);
																													}
																													BgL_arg1058z00_50 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(5),
																														BgL_arg1059z00_51);
																												}
																												BgL_arg1057z00_49 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1058z00_50,
																													BNIL);
																											}
																											BgL_arg1055z00_47 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(6),
																												BgL_arg1057z00_49);
																										}
																										{	/* Expand/struct.scm 61 */
																											obj_t BgL_arg1063z00_54;
																											obj_t BgL_arg1065z00_55;

																											{	/* Expand/struct.scm 61 */
																												obj_t BgL_arg1066z00_56;

																												{	/* Expand/struct.scm 61 */
																													obj_t
																														BgL_arg1068z00_57;
																													BgL_arg1068z00_57 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(2),
																														BNIL);
																													BgL_arg1066z00_56 =
																														MAKE_YOUNG_PAIR
																														(BgL_namez00_13,
																														BgL_arg1068z00_57);
																												}
																												BgL_arg1063z00_54 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(7),
																													BgL_arg1066z00_56);
																											}
																											{	/* Expand/struct.scm 62 */
																												obj_t BgL_arg1074z00_58;

																												{	/* Expand/struct.scm 62 */
																													obj_t
																														BgL_arg1075z00_59;
																													obj_t
																														BgL_arg1076z00_60;
																													{	/* Expand/struct.scm 62 */
																														obj_t
																															BgL_arg1078z00_61;
																														{	/* Expand/struct.scm 62 */
																															obj_t
																																BgL_arg1079z00_62;
																															BgL_arg1079z00_62
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(8), BNIL);
																															BgL_arg1078z00_61
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(9),
																																BgL_arg1079z00_62);
																														}
																														BgL_arg1075z00_59 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(10),
																															BgL_arg1078z00_61);
																													}
																													{	/* Expand/struct.scm 63 */
																														obj_t
																															BgL_arg1080z00_63;
																														obj_t
																															BgL_arg1082z00_64;
																														{	/* Expand/struct.scm 63 */
																															obj_t
																																BgL_arg1083z00_65;
																															BgL_arg1083z00_65
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_namez00_13,
																																BNIL);
																															BgL_arg1080z00_63
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(11),
																																BgL_arg1083z00_65);
																														}
																														{	/* Expand/struct.scm 63 */
																															obj_t
																																BgL_arg1084z00_66;
																															{	/* Expand/struct.scm 63 */
																																obj_t
																																	BgL_arg1085z00_67;
																																{	/* Expand/struct.scm 63 */
																																	obj_t
																																		BgL_arg1087z00_68;
																																	BgL_arg1087z00_68
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(2), BNIL);
																																	BgL_arg1085z00_67
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(12),
																																		BgL_arg1087z00_68);
																																}
																																BgL_arg1084z00_66
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1085z00_67,
																																	BNIL);
																															}
																															BgL_arg1082z00_64
																																=
																																MAKE_YOUNG_PAIR
																																(BINT
																																(BgL_lenz00_23),
																																BgL_arg1084z00_66);
																														}
																														BgL_arg1076z00_60 =
																															MAKE_YOUNG_PAIR
																															(BgL_arg1080z00_63,
																															BgL_arg1082z00_64);
																													}
																													BgL_arg1074z00_58 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1075z00_59,
																														BgL_arg1076z00_60);
																												}
																												BgL_arg1065z00_55 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1074z00_58,
																													BNIL);
																											}
																											BgL_arg1056z00_48 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1063z00_54,
																												BgL_arg1065z00_55);
																										}
																										BgL_arg1054z00_46 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1055z00_47,
																											BgL_arg1056z00_48);
																									}
																									BgL_arg1052z00_44 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(13),
																										BgL_arg1054z00_46);
																								}
																								{	/* Expand/struct.scm 64 */
																									obj_t BgL_arg1088z00_69;

																									BgL_arg1088z00_69 =
																										MAKE_YOUNG_PAIR
																										(BgL_namez00_13,
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_slotszd2valzd2_26,
																											BNIL));
																									BgL_arg1053z00_45 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1088z00_69, BNIL);
																								}
																								BgL_arg1050z00_42 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1052z00_44,
																									BgL_arg1053z00_45);
																							}
																							BgL_arg1048z00_40 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1049z00_41,
																								BgL_arg1050z00_42);
																						}
																						BgL_arg1047z00_39 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(13), BgL_arg1048z00_40);
																					}
																				else
																					{	/* Expand/struct.scm 65 */
																						obj_t BgL_arg1092z00_71;

																						{	/* Expand/struct.scm 65 */
																							obj_t BgL_arg1097z00_72;
																							obj_t BgL_arg1102z00_73;

																							{	/* Expand/struct.scm 65 */
																								obj_t BgL_arg1103z00_74;

																								BgL_arg1103z00_74 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(2), BNIL);
																								BgL_arg1097z00_72 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(3), BgL_arg1103z00_74);
																							}
																							{	/* Expand/struct.scm 66 */
																								obj_t BgL_arg1104z00_75;
																								obj_t BgL_arg1114z00_76;

																								{	/* Expand/struct.scm 66 */
																									obj_t BgL_arg1115z00_77;

																									{	/* Expand/struct.scm 66 */
																										obj_t BgL_arg1122z00_78;
																										obj_t BgL_arg1123z00_79;

																										{	/* Expand/struct.scm 66 */
																											obj_t BgL_arg1125z00_80;

																											{	/* Expand/struct.scm 66 */
																												obj_t BgL_arg1126z00_81;

																												{	/* Expand/struct.scm 66 */
																													obj_t
																														BgL_arg1127z00_82;
																													{	/* Expand/struct.scm 66 */
																														obj_t
																															BgL_arg1129z00_83;
																														{	/* Expand/struct.scm 66 */
																															obj_t
																																BgL_arg1131z00_84;
																															BgL_arg1131z00_84
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(2), BNIL);
																															BgL_arg1129z00_83
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(4),
																																BgL_arg1131z00_84);
																														}
																														BgL_arg1127z00_82 =
																															MAKE_YOUNG_PAIR
																															(BgL_arg1129z00_83,
																															BNIL);
																													}
																													BgL_arg1126z00_81 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(5),
																														BgL_arg1127z00_82);
																												}
																												BgL_arg1125z00_80 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1126z00_81,
																													BNIL);
																											}
																											BgL_arg1122z00_78 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(6),
																												BgL_arg1125z00_80);
																										}
																										{	/* Expand/struct.scm 67 */
																											obj_t BgL_arg1132z00_85;
																											obj_t BgL_arg1137z00_86;

																											{	/* Expand/struct.scm 67 */
																												obj_t BgL_arg1138z00_87;

																												{	/* Expand/struct.scm 67 */
																													obj_t
																														BgL_arg1140z00_88;
																													BgL_arg1140z00_88 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(2),
																														BNIL);
																													BgL_arg1138z00_87 =
																														MAKE_YOUNG_PAIR
																														(BgL_namez00_13,
																														BgL_arg1140z00_88);
																												}
																												BgL_arg1132z00_85 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(7),
																													BgL_arg1138z00_87);
																											}
																											{	/* Expand/struct.scm 68 */
																												obj_t BgL_arg1141z00_89;

																												{	/* Expand/struct.scm 68 */
																													obj_t
																														BgL_arg1142z00_90;
																													obj_t
																														BgL_arg1143z00_91;
																													{	/* Expand/struct.scm 68 */
																														obj_t
																															BgL_arg1145z00_92;
																														{	/* Expand/struct.scm 68 */
																															obj_t
																																BgL_arg1148z00_93;
																															BgL_arg1148z00_93
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(8), BNIL);
																															BgL_arg1145z00_92
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(9),
																																BgL_arg1148z00_93);
																														}
																														BgL_arg1142z00_90 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(10),
																															BgL_arg1145z00_92);
																													}
																													{	/* Expand/struct.scm 69 */
																														obj_t
																															BgL_arg1149z00_94;
																														obj_t
																															BgL_arg1152z00_95;
																														{	/* Expand/struct.scm 69 */
																															obj_t
																																BgL_arg1153z00_96;
																															BgL_arg1153z00_96
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_namez00_13,
																																BNIL);
																															BgL_arg1149z00_94
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(11),
																																BgL_arg1153z00_96);
																														}
																														{	/* Expand/struct.scm 69 */
																															obj_t
																																BgL_arg1154z00_97;
																															{	/* Expand/struct.scm 69 */
																																obj_t
																																	BgL_arg1157z00_98;
																																{	/* Expand/struct.scm 69 */
																																	obj_t
																																		BgL_arg1158z00_99;
																																	BgL_arg1158z00_99
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(2), BNIL);
																																	BgL_arg1157z00_98
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(12),
																																		BgL_arg1158z00_99);
																																}
																																BgL_arg1154z00_97
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1157z00_98,
																																	BNIL);
																															}
																															BgL_arg1152z00_95
																																=
																																MAKE_YOUNG_PAIR
																																(BINT
																																(BgL_lenz00_23),
																																BgL_arg1154z00_97);
																														}
																														BgL_arg1143z00_91 =
																															MAKE_YOUNG_PAIR
																															(BgL_arg1149z00_94,
																															BgL_arg1152z00_95);
																													}
																													BgL_arg1141z00_89 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1142z00_90,
																														BgL_arg1143z00_91);
																												}
																												BgL_arg1137z00_86 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1141z00_89,
																													BNIL);
																											}
																											BgL_arg1123z00_79 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1132z00_85,
																												BgL_arg1137z00_86);
																										}
																										BgL_arg1115z00_77 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1122z00_78,
																											BgL_arg1123z00_79);
																									}
																									BgL_arg1104z00_75 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(13),
																										BgL_arg1115z00_77);
																								}
																								{	/* Expand/struct.scm 70 */
																									obj_t BgL_arg1162z00_100;

																									{	/* Expand/struct.scm 70 */
																										obj_t BgL_arg1164z00_101;
																										obj_t BgL_arg1166z00_102;

																										{	/* Expand/struct.scm 70 */
																											obj_t BgL_arg1171z00_103;

																											{	/* Expand/struct.scm 70 */
																												obj_t
																													BgL_arg1172z00_104;
																												BgL_arg1172z00_104 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(8),
																													BNIL);
																												BgL_arg1171z00_103 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(9),
																													BgL_arg1172z00_104);
																											}
																											BgL_arg1164z00_101 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(10),
																												BgL_arg1171z00_103);
																										}
																										{	/* Expand/struct.scm 71 */
																											obj_t BgL_arg1182z00_105;
																											obj_t BgL_arg1183z00_106;

																											{	/* Expand/struct.scm 71 */
																												obj_t
																													BgL_arg1187z00_107;
																												BgL_arg1187z00_107 =
																													MAKE_YOUNG_PAIR
																													(BgL_namez00_13,
																													BNIL);
																												BgL_arg1182z00_105 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(11),
																													BgL_arg1187z00_107);
																											}
																											{	/* Expand/struct.scm 71 */
																												obj_t
																													BgL_arg1188z00_108;
																												{	/* Expand/struct.scm 71 */
																													obj_t
																														BgL_arg1189z00_109;
																													{	/* Expand/struct.scm 71 */
																														obj_t
																															BgL_arg1190z00_110;
																														BgL_arg1190z00_110 =
																															MAKE_YOUNG_PAIR
																															(BNIL, BNIL);
																														BgL_arg1189z00_109 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(11),
																															BgL_arg1190z00_110);
																													}
																													BgL_arg1188z00_108 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1189z00_109,
																														BNIL);
																												}
																												BgL_arg1183z00_106 =
																													MAKE_YOUNG_PAIR(BINT
																													(BgL_lenz00_23),
																													BgL_arg1188z00_108);
																											}
																											BgL_arg1166z00_102 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1182z00_105,
																												BgL_arg1183z00_106);
																										}
																										BgL_arg1162z00_100 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1164z00_101,
																											BgL_arg1166z00_102);
																									}
																									BgL_arg1114z00_76 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1162z00_100, BNIL);
																								}
																								BgL_arg1102z00_73 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1104z00_75,
																									BgL_arg1114z00_76);
																							}
																							BgL_arg1092z00_71 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1097z00_72,
																								BgL_arg1102z00_73);
																						}
																						BgL_arg1047z00_39 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(13), BgL_arg1092z00_71);
																					}
																				BgL_arg1041z00_34 =
																					MAKE_YOUNG_PAIR(BgL_arg1047z00_39,
																					BNIL);
																			}
																			BgL_arg1039z00_32 =
																				MAKE_YOUNG_PAIR(BgL_arg1040z00_33,
																				BgL_arg1041z00_34);
																		}
																		BgL_arg1038z00_31 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																			BgL_arg1039z00_32);
																	}
																	BgL_arg1037z00_30 =
																		BGl_epairifyz00zztools_miscz00
																		(BgL_arg1038z00_31, BgL_xz00_3);
																}
																BgL_arg1035z00_28 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_4,
																	BgL_arg1037z00_30, BgL_ez00_4);
															}
															{	/* Expand/struct.scm 76 */
																obj_t BgL_arg1191z00_111;
																obj_t BgL_arg1193z00_112;

																{	/* Expand/struct.scm 76 */
																	obj_t BgL_arg1194z00_113;

																	{	/* Expand/struct.scm 76 */
																		obj_t BgL_arg1196z00_114;

																		{	/* Expand/struct.scm 76 */
																			obj_t BgL_arg1197z00_115;

																			{	/* Expand/struct.scm 76 */
																				obj_t BgL_arg1198z00_116;
																				obj_t BgL_arg1199z00_117;

																				BgL_arg1198z00_116 =
																					MAKE_YOUNG_PAIR(BgL_namez00_13,
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_slotszd2namezd2_24, BNIL));
																				{	/* Expand/struct.scm 77 */
																					obj_t BgL_arg1201z00_119;

																					{	/* Expand/struct.scm 77 */
																						obj_t BgL_arg1202z00_120;

																						{	/* Expand/struct.scm 77 */
																							obj_t BgL_arg1203z00_121;
																							obj_t BgL_arg1206z00_122;

																							{	/* Expand/struct.scm 77 */
																								obj_t BgL_arg1208z00_123;

																								{	/* Expand/struct.scm 77 */
																									obj_t BgL_arg1209z00_124;

																									{	/* Expand/struct.scm 77 */
																										obj_t BgL_arg1210z00_125;

																										{	/* Expand/struct.scm 77 */
																											obj_t BgL_arg1212z00_126;

																											{	/* Expand/struct.scm 77 */
																												obj_t
																													BgL_arg1215z00_127;
																												obj_t
																													BgL_arg1216z00_128;
																												{	/* Expand/struct.scm 77 */
																													obj_t
																														BgL_arg1218z00_129;
																													BgL_arg1218z00_129 =
																														MAKE_YOUNG_PAIR
																														(BgL_namez00_13,
																														BNIL);
																													BgL_arg1215z00_127 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(11),
																														BgL_arg1218z00_129);
																												}
																												BgL_arg1216z00_128 =
																													MAKE_YOUNG_PAIR(BINT
																													(BgL_lenz00_23),
																													BNIL);
																												BgL_arg1212z00_126 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1215z00_127,
																													BgL_arg1216z00_128);
																											}
																											BgL_arg1210z00_125 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(15),
																												BgL_arg1212z00_126);
																										}
																										BgL_arg1209z00_124 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1210z00_125,
																											BNIL);
																									}
																									BgL_arg1208z00_123 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(16),
																										BgL_arg1209z00_124);
																								}
																								BgL_arg1203z00_121 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1208z00_123, BNIL);
																							}
																							{	/* Expand/struct.scm 78 */
																								obj_t BgL_arg1219z00_130;
																								obj_t BgL_arg1220z00_131;

																								{
																									obj_t BgL_slotsz00_134;
																									obj_t BgL_resz00_135;

																									BgL_slotsz00_134 =
																										BgL_slotszd2namezd2_24;
																									BgL_resz00_135 = BNIL;
																								BgL_zc3z04anonymousza31221ze3z87_136:
																									if (NULLP
																										(BgL_slotsz00_134))
																										{	/* Expand/struct.scm 80 */
																											BgL_arg1219z00_130 =
																												BgL_resz00_135;
																										}
																									else
																										{	/* Expand/struct.scm 82 */
																											obj_t BgL_arg1223z00_138;
																											obj_t BgL_arg1225z00_139;

																											BgL_arg1223z00_138 =
																												CDR(
																												((obj_t)
																													BgL_slotsz00_134));
																											{	/* Expand/struct.scm 85 */
																												obj_t
																													BgL_arg1226z00_140;
																												{	/* Expand/struct.scm 85 */
																													obj_t
																														BgL_arg1227z00_141;
																													obj_t
																														BgL_arg1228z00_142;
																													{	/* Expand/struct.scm 85 */
																														obj_t
																															BgL_arg1229z00_143;
																														BgL_arg1229z00_143 =
																															CAR(((obj_t)
																																BgL_slotsz00_134));
																														{	/* Expand/struct.scm 83 */
																															obj_t
																																BgL_list1230z00_144;
																															{	/* Expand/struct.scm 83 */
																																obj_t
																																	BgL_arg1231z00_145;
																																{	/* Expand/struct.scm 83 */
																																	obj_t
																																		BgL_arg1232z00_146;
																																	{	/* Expand/struct.scm 83 */
																																		obj_t
																																			BgL_arg1233z00_147;
																																		BgL_arg1233z00_147
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(17),
																																			BNIL);
																																		BgL_arg1232z00_146
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1229z00_143,
																																			BgL_arg1233z00_147);
																																	}
																																	BgL_arg1231z00_145
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(18),
																																		BgL_arg1232z00_146);
																																}
																																BgL_list1230z00_144
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_namez00_13,
																																	BgL_arg1231z00_145);
																															}
																															BgL_arg1227z00_141
																																=
																																BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																																(BgL_list1230z00_144);
																														}
																													}
																													{	/* Expand/struct.scm 88 */
																														obj_t
																															BgL_arg1234z00_148;
																														{	/* Expand/struct.scm 88 */
																															obj_t
																																BgL_arg1236z00_149;
																															BgL_arg1236z00_149
																																=
																																CAR(((obj_t)
																																	BgL_slotsz00_134));
																															BgL_arg1234z00_148
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1236z00_149,
																																BNIL);
																														}
																														BgL_arg1228z00_142 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(16),
																															BgL_arg1234z00_148);
																													}
																													BgL_arg1226z00_140 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1227z00_141,
																														BgL_arg1228z00_142);
																												}
																												BgL_arg1225z00_139 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1226z00_140,
																													BgL_resz00_135);
																											}
																											{
																												obj_t BgL_resz00_652;
																												obj_t BgL_slotsz00_651;

																												BgL_slotsz00_651 =
																													BgL_arg1223z00_138;
																												BgL_resz00_652 =
																													BgL_arg1225z00_139;
																												BgL_resz00_135 =
																													BgL_resz00_652;
																												BgL_slotsz00_134 =
																													BgL_slotsz00_651;
																												goto
																													BgL_zc3z04anonymousza31221ze3z87_136;
																											}
																										}
																								}
																								BgL_arg1220z00_131 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(16), BNIL);
																								BgL_arg1206z00_122 =
																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																									(BgL_arg1219z00_130,
																									BgL_arg1220z00_131);
																							}
																							BgL_arg1202z00_120 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1203z00_121,
																								BgL_arg1206z00_122);
																						}
																						BgL_arg1201z00_119 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(19), BgL_arg1202z00_120);
																					}
																					BgL_arg1199z00_117 =
																						MAKE_YOUNG_PAIR(BgL_arg1201z00_119,
																						BNIL);
																				}
																				BgL_arg1197z00_115 =
																					MAKE_YOUNG_PAIR(BgL_arg1198z00_116,
																					BgL_arg1199z00_117);
																			}
																			BgL_arg1196z00_114 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																				BgL_arg1197z00_115);
																		}
																		BgL_arg1194z00_113 =
																			BGl_epairifyz00zztools_miscz00
																			(BgL_arg1196z00_114, BgL_xz00_3);
																	}
																	BgL_arg1191z00_111 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_4,
																		BgL_arg1194z00_113, BgL_ez00_4);
																}
																{	/* Expand/struct.scm 96 */
																	obj_t BgL_arg1238z00_151;
																	obj_t BgL_arg1239z00_152;

																	{	/* Expand/struct.scm 96 */
																		obj_t BgL_arg1242z00_153;

																		{	/* Expand/struct.scm 96 */
																			obj_t BgL_arg1244z00_154;

																			{	/* Expand/struct.scm 96 */
																				obj_t BgL_arg1248z00_155;

																				{	/* Expand/struct.scm 96 */
																					obj_t BgL_arg1249z00_156;
																					obj_t BgL_arg1252z00_157;

																					{	/* Expand/struct.scm 96 */
																						obj_t BgL_arg1268z00_158;
																						obj_t BgL_arg1272z00_159;

																						{	/* Expand/struct.scm 96 */
																							obj_t BgL_arg1284z00_160;

																							{	/* Expand/struct.scm 96 */
																								obj_t BgL_arg1304z00_161;
																								obj_t BgL_arg1305z00_162;

																								{	/* Expand/struct.scm 96 */
																									obj_t BgL_arg1455z00_354;

																									BgL_arg1455z00_354 =
																										SYMBOL_TO_STRING(
																										((obj_t) BgL_namez00_13));
																									BgL_arg1304z00_161 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg1455z00_354);
																								}
																								{	/* Expand/struct.scm 96 */
																									obj_t BgL_symbolz00_355;

																									BgL_symbolz00_355 =
																										CNST_TABLE_REF(20);
																									{	/* Expand/struct.scm 96 */
																										obj_t BgL_arg1455z00_356;

																										BgL_arg1455z00_356 =
																											SYMBOL_TO_STRING
																											(BgL_symbolz00_355);
																										BgL_arg1305z00_162 =
																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																											(BgL_arg1455z00_356);
																									}
																								}
																								BgL_arg1284z00_160 =
																									string_append
																									(BgL_arg1304z00_161,
																									BgL_arg1305z00_162);
																							}
																							BgL_arg1268z00_158 =
																								bstring_to_symbol
																								(BgL_arg1284z00_160);
																						}
																						BgL_arg1272z00_159 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(21), BNIL);
																						BgL_arg1249z00_156 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1268z00_158,
																							BgL_arg1272z00_159);
																					}
																					{	/* Expand/struct.scm 97 */
																						obj_t BgL_arg1306z00_163;

																						{	/* Expand/struct.scm 97 */
																							obj_t BgL_arg1307z00_164;

																							{	/* Expand/struct.scm 97 */
																								obj_t BgL_arg1308z00_165;
																								obj_t BgL_arg1310z00_166;

																								{	/* Expand/struct.scm 97 */
																									obj_t BgL_arg1311z00_167;

																									BgL_arg1311z00_167 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(21), BNIL);
																									BgL_arg1308z00_165 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(22),
																										BgL_arg1311z00_167);
																								}
																								{	/* Expand/struct.scm 98 */
																									obj_t BgL_arg1312z00_168;
																									obj_t BgL_arg1314z00_169;

																									{	/* Expand/struct.scm 98 */
																										obj_t BgL_arg1315z00_170;

																										{	/* Expand/struct.scm 98 */
																											obj_t BgL_arg1316z00_171;
																											obj_t BgL_arg1317z00_172;

																											{	/* Expand/struct.scm 98 */
																												obj_t
																													BgL_arg1318z00_173;
																												obj_t
																													BgL_arg1319z00_174;
																												{	/* Expand/struct.scm 98 */
																													obj_t
																														BgL_arg1320z00_175;
																													{	/* Expand/struct.scm 98 */
																														obj_t
																															BgL_arg1321z00_176;
																														BgL_arg1321z00_176 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(8), BNIL);
																														BgL_arg1320z00_175 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(23),
																															BgL_arg1321z00_176);
																													}
																													BgL_arg1318z00_173 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(10),
																														BgL_arg1320z00_175);
																												}
																												BgL_arg1319z00_174 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(21),
																													BNIL);
																												BgL_arg1316z00_171 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1318z00_173,
																													BgL_arg1319z00_174);
																											}
																											{	/* Expand/struct.scm 98 */
																												obj_t
																													BgL_arg1322z00_177;
																												{	/* Expand/struct.scm 98 */
																													obj_t
																														BgL_arg1323z00_178;
																													BgL_arg1323z00_178 =
																														MAKE_YOUNG_PAIR
																														(BgL_namez00_13,
																														BNIL);
																													BgL_arg1322z00_177 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(11),
																														BgL_arg1323z00_178);
																												}
																												BgL_arg1317z00_172 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1322z00_177,
																													BNIL);
																											}
																											BgL_arg1315z00_170 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1316z00_171,
																												BgL_arg1317z00_172);
																										}
																										BgL_arg1312z00_168 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(24),
																											BgL_arg1315z00_170);
																									}
																									BgL_arg1314z00_169 =
																										MAKE_YOUNG_PAIR(BFALSE,
																										BNIL);
																									BgL_arg1310z00_166 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1312z00_168,
																										BgL_arg1314z00_169);
																								}
																								BgL_arg1307z00_164 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1308z00_165,
																									BgL_arg1310z00_166);
																							}
																							BgL_arg1306z00_163 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(13), BgL_arg1307z00_164);
																						}
																						BgL_arg1252z00_157 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1306z00_163, BNIL);
																					}
																					BgL_arg1248z00_155 =
																						MAKE_YOUNG_PAIR(BgL_arg1249z00_156,
																						BgL_arg1252z00_157);
																				}
																				BgL_arg1244z00_154 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																					BgL_arg1248z00_155);
																			}
																			BgL_arg1242z00_153 =
																				BGl_epairifyz00zztools_miscz00
																				(BgL_arg1244z00_154, BgL_xz00_3);
																		}
																		BgL_arg1238z00_151 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_4,
																			BgL_arg1242z00_153, BgL_ez00_4);
																	}
																	{	/* Expand/struct.scm 103 */
																		obj_t BgL_g1013z00_179;

																		BgL_g1013z00_179 = CNST_TABLE_REF(25);
																		{
																			long BgL_iz00_181;
																			obj_t BgL_slotsz00_182;
																			obj_t BgL_resz00_183;

																			BgL_iz00_181 = 0L;
																			BgL_slotsz00_182 = BgL_slotszd2namezd2_24;
																			BgL_resz00_183 = BgL_g1013z00_179;
																		BgL_zc3z04anonymousza31324ze3z87_184:
																			if ((BgL_iz00_181 == BgL_lenz00_23))
																				{	/* Expand/struct.scm 106 */
																					BgL_arg1239z00_152 = BgL_resz00_183;
																				}
																			else
																				{	/* Expand/struct.scm 108 */
																					obj_t BgL_prz00_186;

																					BgL_prz00_186 =
																						CAR(((obj_t) BgL_slotsz00_182));
																					{	/* Expand/struct.scm 109 */
																						long BgL_arg1326z00_187;
																						obj_t BgL_arg1327z00_188;
																						obj_t BgL_arg1328z00_189;

																						BgL_arg1326z00_187 =
																							(BgL_iz00_181 + 1L);
																						BgL_arg1327z00_188 =
																							CDR(((obj_t) BgL_slotsz00_182));
																						{	/* Expand/struct.scm 116 */
																							obj_t BgL_arg1329z00_190;
																							obj_t BgL_arg1331z00_191;

																							{	/* Expand/struct.scm 116 */
																								obj_t BgL_arg1332z00_192;

																								{	/* Expand/struct.scm 116 */
																									obj_t BgL_arg1333z00_193;

																									{	/* Expand/struct.scm 116 */
																										obj_t BgL_arg1335z00_194;

																										{	/* Expand/struct.scm 116 */
																											obj_t BgL_arg1339z00_195;
																											obj_t BgL_arg1340z00_196;

																											{	/* Expand/struct.scm 116 */
																												obj_t
																													BgL_arg1342z00_197;
																												obj_t
																													BgL_arg1343z00_198;
																												{	/* Expand/struct.scm 116 */
																													obj_t
																														BgL_list1344z00_199;
																													{	/* Expand/struct.scm 116 */
																														obj_t
																															BgL_arg1346z00_200;
																														{	/* Expand/struct.scm 116 */
																															obj_t
																																BgL_arg1348z00_201;
																															BgL_arg1348z00_201
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_prz00_186,
																																BNIL);
																															BgL_arg1346z00_200
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(18),
																																BgL_arg1348z00_201);
																														}
																														BgL_list1344z00_199
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_namez00_13,
																															BgL_arg1346z00_200);
																													}
																													BgL_arg1342z00_197 =
																														BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																														(BgL_list1344z00_199);
																												}
																												BgL_arg1343z00_198 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(26),
																													BNIL);
																												BgL_arg1339z00_195 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1342z00_197,
																													BgL_arg1343z00_198);
																											}
																											{	/* Expand/struct.scm 117 */
																												obj_t
																													BgL_arg1349z00_202;
																												if (CBOOL
																													(BGl_za2unsafezd2structza2zd2zzengine_paramz00))
																													{	/* Expand/struct.scm 118 */
																														obj_t
																															BgL_arg1351z00_203;
																														obj_t
																															BgL_arg1352z00_204;
																														{	/* Expand/struct.scm 118 */
																															obj_t
																																BgL_arg1361z00_205;
																															{	/* Expand/struct.scm 118 */
																																obj_t
																																	BgL_arg1364z00_206;
																																BgL_arg1364z00_206
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(8), BNIL);
																																BgL_arg1361z00_205
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(27),
																																	BgL_arg1364z00_206);
																															}
																															BgL_arg1351z00_203
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(10),
																																BgL_arg1361z00_205);
																														}
																														{	/* Expand/struct.scm 118 */
																															obj_t
																																BgL_arg1367z00_207;
																															BgL_arg1367z00_207
																																=
																																MAKE_YOUNG_PAIR
																																(BINT
																																(BgL_iz00_181),
																																BNIL);
																															BgL_arg1352z00_204
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(26),
																																BgL_arg1367z00_207);
																														}
																														BgL_arg1349z00_202 =
																															MAKE_YOUNG_PAIR
																															(BgL_arg1351z00_203,
																															BgL_arg1352z00_204);
																													}
																												else
																													{	/* Expand/struct.scm 119 */
																														obj_t
																															BgL_arg1370z00_208;
																														{	/* Expand/struct.scm 119 */
																															obj_t
																																BgL_arg1371z00_209;
																															obj_t
																																BgL_arg1375z00_210;
																															{	/* Expand/struct.scm 119 */
																																obj_t
																																	BgL_arg1376z00_211;
																																{	/* Expand/struct.scm 119 */
																																	obj_t
																																		BgL_arg1377z00_212;
																																	obj_t
																																		BgL_arg1378z00_213;
																																	{	/* Expand/struct.scm 119 */
																																		obj_t
																																			BgL_arg1379z00_214;
																																		obj_t
																																			BgL_arg1380z00_215;
																																		{	/* Expand/struct.scm 119 */
																																			obj_t
																																				BgL_arg1408z00_216;
																																			{	/* Expand/struct.scm 119 */
																																				obj_t
																																					BgL_arg1410z00_217;
																																				BgL_arg1410z00_217
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(8),
																																					BNIL);
																																				BgL_arg1408z00_216
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(23),
																																					BgL_arg1410z00_217);
																																			}
																																			BgL_arg1379z00_214
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(10),
																																				BgL_arg1408z00_216);
																																		}
																																		BgL_arg1380z00_215
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(26),
																																			BNIL);
																																		BgL_arg1377z00_212
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1379z00_214,
																																			BgL_arg1380z00_215);
																																	}
																																	{	/* Expand/struct.scm 119 */
																																		obj_t
																																			BgL_arg1421z00_218;
																																		{	/* Expand/struct.scm 119 */
																																			obj_t
																																				BgL_arg1422z00_219;
																																			BgL_arg1422z00_219
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_namez00_13,
																																				BNIL);
																																			BgL_arg1421z00_218
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(11),
																																				BgL_arg1422z00_219);
																																		}
																																		BgL_arg1378z00_213
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1421z00_218,
																																			BNIL);
																																	}
																																	BgL_arg1376z00_211
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1377z00_212,
																																		BgL_arg1378z00_213);
																																}
																																BgL_arg1371z00_209
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(24),
																																	BgL_arg1376z00_211);
																															}
																															{	/* Expand/struct.scm 120 */
																																obj_t
																																	BgL_arg1434z00_220;
																																obj_t
																																	BgL_arg1437z00_221;
																																{	/* Expand/struct.scm 120 */
																																	obj_t
																																		BgL_arg1448z00_222;
																																	{	/* Expand/struct.scm 120 */
																																		obj_t
																																			BgL_arg1453z00_223;
																																		BgL_arg1453z00_223
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BINT
																																			(BgL_iz00_181),
																																			BNIL);
																																		BgL_arg1448z00_222
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(26),
																																			BgL_arg1453z00_223);
																																	}
																																	BgL_arg1434z00_220
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(28),
																																		BgL_arg1448z00_222);
																																}
																																{	/* Expand/struct.scm 123 */
																																	obj_t
																																		BgL_arg1454z00_224;
																																	{	/* Expand/struct.scm 123 */
																																		obj_t
																																			BgL_arg1472z00_225;
																																		{	/* Expand/struct.scm 123 */
																																			obj_t
																																				BgL_arg1473z00_226;
																																			{	/* Expand/struct.scm 123 */
																																				obj_t
																																					BgL_arg1485z00_227;
																																				obj_t
																																					BgL_arg1489z00_228;
																																				{	/* Expand/struct.scm 123 */
																																					obj_t
																																						BgL_arg1455z00_364;
																																					BgL_arg1455z00_364
																																						=
																																						SYMBOL_TO_STRING
																																						(((obj_t) BgL_namez00_13));
																																					BgL_arg1485z00_227
																																						=
																																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																						(BgL_arg1455z00_364);
																																				}
																																				BgL_arg1489z00_228
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(26),
																																					BNIL);
																																				BgL_arg1473z00_226
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1485z00_227,
																																					BgL_arg1489z00_228);
																																			}
																																			BgL_arg1472z00_225
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_string1706z00zzexpand_structz00,
																																				BgL_arg1473z00_226);
																																		}
																																		BgL_arg1454z00_224
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(29),
																																			BgL_arg1472z00_225);
																																	}
																																	BgL_arg1437z00_221
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1454z00_224,
																																		BNIL);
																																}
																																BgL_arg1375z00_210
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1434z00_220,
																																	BgL_arg1437z00_221);
																															}
																															BgL_arg1370z00_208
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1371z00_209,
																																BgL_arg1375z00_210);
																														}
																														BgL_arg1349z00_202 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(13),
																															BgL_arg1370z00_208);
																													}
																												BgL_arg1340z00_196 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1349z00_202,
																													BNIL);
																											}
																											BgL_arg1335z00_194 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1339z00_195,
																												BgL_arg1340z00_196);
																										}
																										BgL_arg1333z00_193 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(14),
																											BgL_arg1335z00_194);
																									}
																									BgL_arg1332z00_192 =
																										BGl_epairifyz00zztools_miscz00
																										(BgL_arg1333z00_193,
																										BgL_xz00_3);
																								}
																								BgL_arg1329z00_190 =
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_4,
																									BgL_arg1332z00_192,
																									BgL_ez00_4);
																							}
																							{	/* Expand/struct.scm 132 */
																								obj_t BgL_arg1502z00_229;

																								{	/* Expand/struct.scm 132 */
																									obj_t BgL_arg1509z00_230;

																									{	/* Expand/struct.scm 132 */
																										obj_t BgL_arg1513z00_231;

																										{	/* Expand/struct.scm 132 */
																											obj_t BgL_arg1514z00_232;

																											{	/* Expand/struct.scm 132 */
																												obj_t
																													BgL_arg1516z00_233;
																												obj_t
																													BgL_arg1535z00_234;
																												{	/* Expand/struct.scm 132 */
																													obj_t
																														BgL_arg1540z00_235;
																													obj_t
																														BgL_arg1544z00_236;
																													{	/* Expand/struct.scm 132 */
																														obj_t
																															BgL_list1545z00_237;
																														{	/* Expand/struct.scm 132 */
																															obj_t
																																BgL_arg1546z00_238;
																															{	/* Expand/struct.scm 132 */
																																obj_t
																																	BgL_arg1552z00_239;
																																{	/* Expand/struct.scm 132 */
																																	obj_t
																																		BgL_arg1553z00_240;
																																	BgL_arg1553z00_240
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(17), BNIL);
																																	BgL_arg1552z00_239
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_prz00_186,
																																		BgL_arg1553z00_240);
																																}
																																BgL_arg1546z00_238
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(18),
																																	BgL_arg1552z00_239);
																															}
																															BgL_list1545z00_237
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_namez00_13,
																																BgL_arg1546z00_238);
																														}
																														BgL_arg1540z00_235 =
																															BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																															(BgL_list1545z00_237);
																													}
																													{	/* Expand/struct.scm 132 */
																														obj_t
																															BgL_arg1559z00_241;
																														BgL_arg1559z00_241 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(30), BNIL);
																														BgL_arg1544z00_236 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(26),
																															BgL_arg1559z00_241);
																													}
																													BgL_arg1516z00_233 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1540z00_235,
																														BgL_arg1544z00_236);
																												}
																												{	/* Expand/struct.scm 133 */
																													obj_t
																														BgL_arg1561z00_242;
																													if (CBOOL
																														(BGl_za2unsafezd2structza2zd2zzengine_paramz00))
																														{	/* Expand/struct.scm 134 */
																															obj_t
																																BgL_arg1564z00_243;
																															obj_t
																																BgL_arg1565z00_244;
																															{	/* Expand/struct.scm 134 */
																																obj_t
																																	BgL_arg1571z00_245;
																																{	/* Expand/struct.scm 134 */
																																	obj_t
																																		BgL_arg1573z00_246;
																																	BgL_arg1573z00_246
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(8), BNIL);
																																	BgL_arg1571z00_245
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(31),
																																		BgL_arg1573z00_246);
																																}
																																BgL_arg1564z00_243
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(10),
																																	BgL_arg1571z00_245);
																															}
																															{	/* Expand/struct.scm 134 */
																																obj_t
																																	BgL_arg1575z00_247;
																																{	/* Expand/struct.scm 134 */
																																	obj_t
																																		BgL_arg1576z00_248;
																																	BgL_arg1576z00_248
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(30), BNIL);
																																	BgL_arg1575z00_247
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BINT
																																		(BgL_iz00_181),
																																		BgL_arg1576z00_248);
																																}
																																BgL_arg1565z00_244
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(26),
																																	BgL_arg1575z00_247);
																															}
																															BgL_arg1561z00_242
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1564z00_243,
																																BgL_arg1565z00_244);
																														}
																													else
																														{	/* Expand/struct.scm 135 */
																															obj_t
																																BgL_arg1584z00_249;
																															{	/* Expand/struct.scm 135 */
																																obj_t
																																	BgL_arg1585z00_250;
																																obj_t
																																	BgL_arg1589z00_251;
																																{	/* Expand/struct.scm 135 */
																																	obj_t
																																		BgL_arg1591z00_252;
																																	{	/* Expand/struct.scm 135 */
																																		obj_t
																																			BgL_arg1593z00_253;
																																		obj_t
																																			BgL_arg1594z00_254;
																																		{	/* Expand/struct.scm 135 */
																																			obj_t
																																				BgL_arg1595z00_255;
																																			obj_t
																																				BgL_arg1602z00_256;
																																			{	/* Expand/struct.scm 135 */
																																				obj_t
																																					BgL_arg1605z00_257;
																																				{	/* Expand/struct.scm 135 */
																																					obj_t
																																						BgL_arg1606z00_258;
																																					BgL_arg1606z00_258
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(8),
																																						BNIL);
																																					BgL_arg1605z00_257
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(23),
																																						BgL_arg1606z00_258);
																																				}
																																				BgL_arg1595z00_255
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(10),
																																					BgL_arg1605z00_257);
																																			}
																																			BgL_arg1602z00_256
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(26),
																																				BNIL);
																																			BgL_arg1593z00_253
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1595z00_255,
																																				BgL_arg1602z00_256);
																																		}
																																		{	/* Expand/struct.scm 135 */
																																			obj_t
																																				BgL_arg1609z00_259;
																																			{	/* Expand/struct.scm 135 */
																																				obj_t
																																					BgL_arg1611z00_260;
																																				BgL_arg1611z00_260
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_namez00_13,
																																					BNIL);
																																				BgL_arg1609z00_259
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(11),
																																					BgL_arg1611z00_260);
																																			}
																																			BgL_arg1594z00_254
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1609z00_259,
																																				BNIL);
																																		}
																																		BgL_arg1591z00_252
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1593z00_253,
																																			BgL_arg1594z00_254);
																																	}
																																	BgL_arg1585z00_250
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(24),
																																		BgL_arg1591z00_252);
																																}
																																{	/* Expand/struct.scm 136 */
																																	obj_t
																																		BgL_arg1613z00_261;
																																	obj_t
																																		BgL_arg1615z00_262;
																																	{	/* Expand/struct.scm 136 */
																																		obj_t
																																			BgL_arg1616z00_263;
																																		{	/* Expand/struct.scm 136 */
																																			obj_t
																																				BgL_arg1625z00_264;
																																			{	/* Expand/struct.scm 136 */
																																				obj_t
																																					BgL_arg1626z00_265;
																																				BgL_arg1626z00_265
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(30),
																																					BNIL);
																																				BgL_arg1625z00_264
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BINT
																																					(BgL_iz00_181),
																																					BgL_arg1626z00_265);
																																			}
																																			BgL_arg1616z00_263
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(26),
																																				BgL_arg1625z00_264);
																																		}
																																		BgL_arg1613z00_261
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(32),
																																			BgL_arg1616z00_263);
																																	}
																																	{	/* Expand/struct.scm 139 */
																																		obj_t
																																			BgL_arg1627z00_266;
																																		{	/* Expand/struct.scm 139 */
																																			obj_t
																																				BgL_arg1629z00_267;
																																			{	/* Expand/struct.scm 139 */
																																				obj_t
																																					BgL_arg1630z00_268;
																																				{	/* Expand/struct.scm 139 */
																																					obj_t
																																						BgL_arg1642z00_269;
																																					obj_t
																																						BgL_arg1646z00_270;
																																					{	/* Expand/struct.scm 139 */
																																						obj_t
																																							BgL_arg1455z00_366;
																																						BgL_arg1455z00_366
																																							=
																																							SYMBOL_TO_STRING
																																							(((obj_t) BgL_namez00_13));
																																						BgL_arg1642z00_269
																																							=
																																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																							(BgL_arg1455z00_366);
																																					}
																																					BgL_arg1646z00_270
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(26),
																																						BNIL);
																																					BgL_arg1630z00_268
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1642z00_269,
																																						BgL_arg1646z00_270);
																																				}
																																				BgL_arg1629z00_267
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_string1707z00zzexpand_structz00,
																																					BgL_arg1630z00_268);
																																			}
																																			BgL_arg1627z00_266
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(29),
																																				BgL_arg1629z00_267);
																																		}
																																		BgL_arg1615z00_262
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1627z00_266,
																																			BNIL);
																																	}
																																	BgL_arg1589z00_251
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1613z00_261,
																																		BgL_arg1615z00_262);
																																}
																																BgL_arg1584z00_249
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1585z00_250,
																																	BgL_arg1589z00_251);
																															}
																															BgL_arg1561z00_242
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(13),
																																BgL_arg1584z00_249);
																														}
																													BgL_arg1535z00_234 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1561z00_242,
																														BNIL);
																												}
																												BgL_arg1514z00_232 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1516z00_233,
																													BgL_arg1535z00_234);
																											}
																											BgL_arg1513z00_231 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(14),
																												BgL_arg1514z00_232);
																										}
																										BgL_arg1509z00_230 =
																											BGl_epairifyz00zztools_miscz00
																											(BgL_arg1513z00_231,
																											BgL_xz00_3);
																									}
																									BgL_arg1502z00_229 =
																										BGL_PROCEDURE_CALL2
																										(BgL_ez00_4,
																										BgL_arg1509z00_230,
																										BgL_ez00_4);
																								}
																								BgL_arg1331z00_191 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1502z00_229,
																									BgL_resz00_183);
																							}
																							BgL_arg1328z00_189 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1329z00_190,
																								BgL_arg1331z00_191);
																						}
																						{
																							obj_t BgL_resz00_869;
																							obj_t BgL_slotsz00_868;
																							long BgL_iz00_867;

																							BgL_iz00_867 = BgL_arg1326z00_187;
																							BgL_slotsz00_868 =
																								BgL_arg1327z00_188;
																							BgL_resz00_869 =
																								BgL_arg1328z00_189;
																							BgL_resz00_183 = BgL_resz00_869;
																							BgL_slotsz00_182 =
																								BgL_slotsz00_868;
																							BgL_iz00_181 = BgL_iz00_867;
																							goto
																								BgL_zc3z04anonymousza31324ze3z87_184;
																						}
																					}
																				}
																		}
																	}
																	BgL_arg1193z00_112 =
																		MAKE_YOUNG_PAIR(BgL_arg1238z00_151,
																		BgL_arg1239z00_152);
																}
																BgL_arg1036z00_29 =
																	MAKE_YOUNG_PAIR(BgL_arg1191z00_111,
																	BgL_arg1193z00_112);
															}
															BgL_arg1033z00_27 =
																MAKE_YOUNG_PAIR(BgL_arg1035z00_28,
																BgL_arg1036z00_29);
														}
														return
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(33),
															BgL_arg1033z00_27);
													}
												}
											}
										}
									}
								}
							}
						else
							{	/* Expand/struct.scm 24 */
							BgL_tagzd2102zd2_16:
								return
									BGl_errorz00zz__errorz00(BFALSE,
									BGl_string1705z00zzexpand_structz00, BgL_xz00_3);
							}
					}
				else
					{	/* Expand/struct.scm 24 */
						goto BgL_tagzd2102zd2_16;
					}
			}
		}

	}



/* &expand-struct */
	obj_t BGl_z62expandzd2structzb0zzexpand_structz00(obj_t BgL_envz00_370,
		obj_t BgL_xz00_371, obj_t BgL_ez00_372)
	{
		{	/* Expand/struct.scm 23 */
			return
				BGl_expandzd2structzd2zzexpand_structz00(BgL_xz00_371, BgL_ez00_372);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_structz00(void)
	{
		{	/* Expand/struct.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_structz00(void)
	{
		{	/* Expand/struct.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_structz00(void)
	{
		{	/* Expand/struct.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_structz00(void)
	{
		{	/* Expand/struct.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1708z00zzexpand_structz00));
			return
				BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1708z00zzexpand_structz00));
		}

	}

#ifdef __cplusplus
}
#endif
