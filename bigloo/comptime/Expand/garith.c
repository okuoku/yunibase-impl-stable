/*===========================================================================*/
/*   (Expand/garith.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/garith.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_GARITHMETIQUE_TYPE_DEFINITIONS
#define BGL_EXPAND_GARITHMETIQUE_TYPE_DEFINITIONS
#endif													// BGL_EXPAND_GARITHMETIQUE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2gzc3zd3zc2zzexpand_garithmetiquez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_garithmetiquez00 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2gze3zd3ze2zzexpand_garithmetiquez00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2gzc3zd3za0zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2gze3zd3z80zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2gza2z12zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2gzb2z02zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2gzd2z62zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2gzf2z42zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2gzc3z73zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2gzd3z63zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2gze3z53zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_garithmetiquez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2gminzd2zzexpand_garithmetiquez00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_garithmetiquez00(void);
	static obj_t BGl_z62expandzd2gmaxzb0zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_2zf2zf2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_fxze70ze7zzexpand_garithmetiquez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2gza2z70zzexpand_garithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2gzb2z60zzexpand_garithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2gzd2z00zzexpand_garithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2gzf2z20zzexpand_garithmetiquez00(obj_t,
		obj_t);
	static obj_t BGl_expandzd2g2zd2zzexpand_garithmetiquez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2gzc3z11zzexpand_garithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2gzd3z01zzexpand_garithmetiquez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2gze3z31zzexpand_garithmetiquez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_garithmetiquez00(void);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62expandzd2gminzb0zzexpand_garithmetiquez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzexpand_garithmetiquez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzexpand_garithmetiquez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_garithmetiquez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_garithmetiquez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_garithmetiquez00(void);
	extern obj_t BGl_za2arithmeticzd2newzd2overflowza2z00zzengine_paramz00;
	extern obj_t BGl_za2arithmeticzd2overflowza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static bool_t BGl_expandzd2gzd2numberzf3zf3zzexpand_garithmetiquez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2gmaxzd2zzexpand_garithmetiquez00(obj_t,
		obj_t);
	static obj_t __cnst[38];


	BGL_IMPORT obj_t BGl_zd2zd2envz00zz__r4_numbers_6_5z00;
	BGL_IMPORT obj_t BGl_zd3zd2envz01zz__r4_numbers_6_5z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gzb2zd2envzb2zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gza7b23792za7,
		BGl_z62expandzd2gzb2z02zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gminzd2envz00zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gmin3793z00,
		BGl_z62expandzd2gminzb0zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gmaxzd2envz00zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gmax3794z00,
		BGl_z62expandzd2gmaxzb0zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_ze3zd2envz31zz__r4_numbers_6_5z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gzc3zd2envzc3zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gza7c33795za7,
		BGl_z62expandzd2gzc3z73zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gzd2zd2envzd2zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gza7d23796za7,
		BGl_z62expandzd2gzd2z62zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gzd3zd2envzd3zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gza7d33797za7,
		BGl_z62expandzd2gzd3z63zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gze3zd2envze3zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gza7e33798za7,
		BGl_z62expandzd2gze3z53zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gzc3zd3zd2envz10zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gza7c33799za7,
		BGl_z62expandzd2gzc3zd3za0zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gzf2zd2envzf2zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gza7f23800za7,
		BGl_z62expandzd2gzf2z42zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_zc3zd3zd2envzc2zz__r4_numbers_6_5z00;
	BGL_IMPORT obj_t BGl_za2zd2envz70zz__r4_numbers_6_5z00;
	   
		 
		DEFINE_STRING(BGl_string3781z00zzexpand_garithmetiquez00,
		BgL_bgl_string3781za700za7za7e3801za7, "=", 1);
	      DEFINE_STRING(BGl_string3782z00zzexpand_garithmetiquez00,
		BgL_bgl_string3782za700za7za7e3802za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string3783z00zzexpand_garithmetiquez00,
		BgL_bgl_string3783za700za7za7e3803za7, "<", 1);
	      DEFINE_STRING(BGl_string3784z00zzexpand_garithmetiquez00,
		BgL_bgl_string3784za700za7za7e3804za7, ">", 1);
	      DEFINE_STRING(BGl_string3785z00zzexpand_garithmetiquez00,
		BgL_bgl_string3785za700za7za7e3805za7, "<=", 2);
	      DEFINE_STRING(BGl_string3786z00zzexpand_garithmetiquez00,
		BgL_bgl_string3786za700za7za7e3806za7, ">=", 2);
	      DEFINE_STRING(BGl_string3787z00zzexpand_garithmetiquez00,
		BgL_bgl_string3787za700za7za7e3807za7,
		"Incorrect number of arguments for `max'", 39);
	      DEFINE_STRING(BGl_string3788z00zzexpand_garithmetiquez00,
		BgL_bgl_string3788za700za7za7e3808za7,
		"Incorrect number of arguments for `min'", 39);
	      DEFINE_STRING(BGl_string3789z00zzexpand_garithmetiquez00,
		BgL_bgl_string3789za700za7za7e3809za7, "expand_garithmetique", 20);
	      DEFINE_STRING(BGl_string3790z00zzexpand_garithmetiquez00,
		BgL_bgl_string3790za700za7za7e3810za7,
		"min 2min max 2max x >= 2>= <= 2<= > 2> < 2< = 2= 2/ * - + fx fx-safe (+ - / *) fx/ov (+ - *) y expand-g-number? fl $flonum? if @ __r4_numbers_6_5 |2| and $fixnum? let let* b a ",
		176);
	BGL_IMPORT obj_t BGl_zb2zd2envz60zz__r4_numbers_6_5z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gze3zd3zd2envz30zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gza7e33811za7,
		BGl_z62expandzd2gze3zd3z80zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_zc3zd2envz11zz__r4_numbers_6_5z00;
	BGL_IMPORT obj_t BGl_ze3zd3zd2envze2zz__r4_numbers_6_5z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2gza2zd2envza2zzexpand_garithmetiquez00,
		BgL_bgl_za762expandza7d2gza7a23812za7,
		BGl_z62expandzd2gza2z12zzexpand_garithmetiquez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzexpand_garithmetiquez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzexpand_garithmetiquez00(long
		BgL_checksumz00_4489, char *BgL_fromz00_4490)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_garithmetiquez00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_garithmetiquez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_garithmetiquez00();
					BGl_libraryzd2moduleszd2initz00zzexpand_garithmetiquez00();
					BGl_cnstzd2initzd2zzexpand_garithmetiquez00();
					BGl_importedzd2moduleszd2initz00zzexpand_garithmetiquez00();
					return BGl_methodzd2initzd2zzexpand_garithmetiquez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_garithmetiquez00(void)
	{
		{	/* Expand/garith.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_garithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"expand_garithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"expand_garithmetique");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_garithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"expand_garithmetique");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_garithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"expand_garithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"expand_garithmetique");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_garithmetique");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_garithmetiquez00(void)
	{
		{	/* Expand/garith.scm 15 */
			{	/* Expand/garith.scm 15 */
				obj_t BgL_cportz00_4478;

				{	/* Expand/garith.scm 15 */
					obj_t BgL_stringz00_4485;

					BgL_stringz00_4485 = BGl_string3790z00zzexpand_garithmetiquez00;
					{	/* Expand/garith.scm 15 */
						obj_t BgL_startz00_4486;

						BgL_startz00_4486 = BINT(0L);
						{	/* Expand/garith.scm 15 */
							obj_t BgL_endz00_4487;

							BgL_endz00_4487 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4485)));
							{	/* Expand/garith.scm 15 */

								BgL_cportz00_4478 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4485, BgL_startz00_4486, BgL_endz00_4487);
				}}}}
				{
					long BgL_iz00_4479;

					BgL_iz00_4479 = 37L;
				BgL_loopz00_4480:
					if ((BgL_iz00_4479 == -1L))
						{	/* Expand/garith.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/garith.scm 15 */
							{	/* Expand/garith.scm 15 */
								obj_t BgL_arg3791z00_4481;

								{	/* Expand/garith.scm 15 */

									{	/* Expand/garith.scm 15 */
										obj_t BgL_locationz00_4483;

										BgL_locationz00_4483 = BBOOL(((bool_t) 0));
										{	/* Expand/garith.scm 15 */

											BgL_arg3791z00_4481 =
												BGl_readz00zz__readerz00(BgL_cportz00_4478,
												BgL_locationz00_4483);
										}
									}
								}
								{	/* Expand/garith.scm 15 */
									int BgL_tmpz00_4517;

									BgL_tmpz00_4517 = (int) (BgL_iz00_4479);
									CNST_TABLE_SET(BgL_tmpz00_4517, BgL_arg3791z00_4481);
							}}
							{	/* Expand/garith.scm 15 */
								int BgL_auxz00_4484;

								BgL_auxz00_4484 = (int) ((BgL_iz00_4479 - 1L));
								{
									long BgL_iz00_4522;

									BgL_iz00_4522 = (long) (BgL_auxz00_4484);
									BgL_iz00_4479 = BgL_iz00_4522;
									goto BgL_loopz00_4480;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_garithmetiquez00(void)
	{
		{	/* Expand/garith.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* expand-g-number? */
	bool_t BGl_expandzd2gzd2numberzf3zf3zzexpand_garithmetiquez00(obj_t
		BgL_xz00_3)
	{
		{	/* Expand/garith.scm 32 */
			{	/* Expand/garith.scm 33 */
				bool_t BgL__ortest_1012z00_37;

				BgL__ortest_1012z00_37 = INTEGERP(BgL_xz00_3);
				if (BgL__ortest_1012z00_37)
					{	/* Expand/garith.scm 33 */
						return BgL__ortest_1012z00_37;
					}
				else
					{	/* Expand/garith.scm 33 */
						bool_t BgL__ortest_1013z00_38;

						BgL__ortest_1013z00_38 = REALP(BgL_xz00_3);
						if (BgL__ortest_1013z00_38)
							{	/* Expand/garith.scm 33 */
								return BgL__ortest_1013z00_38;
							}
						else
							{	/* Expand/garith.scm 33 */
								bool_t BgL__ortest_1014z00_39;

								BgL__ortest_1014z00_39 = BGL_UINT32P(BgL_xz00_3);
								if (BgL__ortest_1014z00_39)
									{	/* Expand/garith.scm 33 */
										return BgL__ortest_1014z00_39;
									}
								else
									{	/* Expand/garith.scm 33 */
										return BGL_INT32P(BgL_xz00_3);
									}
							}
					}
			}
		}

	}



/* expand-g2 */
	obj_t BGl_expandzd2g2zd2zzexpand_garithmetiquez00(obj_t BgL_xz00_4,
		obj_t BgL_ez00_5, obj_t BgL_opz00_6)
	{
		{	/* Expand/garith.scm 38 */
			{
				obj_t BgL_idz00_43;
				obj_t BgL_az00_44;
				obj_t BgL_bz00_45;
				obj_t BgL_idz00_47;
				obj_t BgL_az00_48;
				obj_t BgL_bz00_49;
				obj_t BgL_idz00_51;
				obj_t BgL_az00_52;
				double BgL_bz00_53;
				obj_t BgL_idz00_55;
				double BgL_az00_56;
				obj_t BgL_bz00_57;
				obj_t BgL_idz00_59;
				obj_t BgL_az00_60;
				obj_t BgL_bz00_61;
				obj_t BgL_idz00_63;
				obj_t BgL_az00_64;
				obj_t BgL_bz00_65;
				obj_t BgL_idz00_67;
				obj_t BgL_az00_68;
				obj_t BgL_bz00_69;
				obj_t BgL_idz00_71;
				obj_t BgL_az00_72;
				obj_t BgL_bz00_73;

				if (PAIRP(BgL_xz00_4))
					{	/* Expand/garith.scm 49 */
						obj_t BgL_cdrzd2116zd2_77;

						BgL_cdrzd2116zd2_77 = CDR(((obj_t) BgL_xz00_4));
						if (PAIRP(BgL_cdrzd2116zd2_77))
							{	/* Expand/garith.scm 49 */
								obj_t BgL_cdrzd2119zd2_79;

								BgL_cdrzd2119zd2_79 = CDR(BgL_cdrzd2116zd2_77);
								if (BGl_expandzd2gzd2numberzf3zf3zzexpand_garithmetiquez00(CAR
										(BgL_cdrzd2116zd2_77)))
									{	/* Expand/garith.scm 49 */
										if (PAIRP(BgL_cdrzd2119zd2_79))
											{	/* Expand/garith.scm 49 */
												obj_t BgL_carzd2120zd2_83;

												BgL_carzd2120zd2_83 = CAR(BgL_cdrzd2119zd2_79);
												if (PAIRP(BgL_carzd2120zd2_83))
													{	/* Expand/garith.scm 49 */
														obj_t BgL_cdrzd2123zd2_85;

														BgL_cdrzd2123zd2_85 = CDR(BgL_carzd2120zd2_83);
														if (
															(CAR(BgL_carzd2120zd2_83) == CNST_TABLE_REF(12)))
															{	/* Expand/garith.scm 49 */
																if (PAIRP(BgL_cdrzd2123zd2_85))
																	{	/* Expand/garith.scm 49 */
																		if (
																			(CAR(BgL_cdrzd2123zd2_85) ==
																				CNST_TABLE_REF(13)))
																			{	/* Expand/garith.scm 49 */
																				if (NULLP(CDR(BgL_cdrzd2123zd2_85)))
																					{	/* Expand/garith.scm 49 */
																						if (NULLP(CDR(BgL_cdrzd2119zd2_79)))
																							{	/* Expand/garith.scm 49 */
																								return
																									apply(BgL_opz00_6,
																									BgL_xz00_4);
																							}
																						else
																							{	/* Expand/garith.scm 49 */
																								return BFALSE;
																							}
																					}
																				else
																					{	/* Expand/garith.scm 49 */
																						obj_t BgL_cdrzd2190zd2_97;

																						BgL_cdrzd2190zd2_97 =
																							CDR(((obj_t) BgL_xz00_4));
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd2195zd2_98;
																							obj_t BgL_cdrzd2196zd2_99;

																							BgL_carzd2195zd2_98 =
																								CAR(
																								((obj_t) BgL_cdrzd2190zd2_97));
																							BgL_cdrzd2196zd2_99 =
																								CDR(
																								((obj_t) BgL_cdrzd2190zd2_97));
																							if (INTEGERP(BgL_carzd2195zd2_98))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd2200zd2_101;

																									BgL_carzd2200zd2_101 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd2196zd2_99));
																									if (SYMBOLP
																										(BgL_carzd2200zd2_101))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd2196zd2_99))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg1041z00_105;
																													BgL_arg1041z00_105 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_idz00_43 =
																														BgL_arg1041z00_105;
																													BgL_az00_44 =
																														BgL_carzd2195zd2_98;
																													BgL_bz00_45 =
																														BgL_carzd2200zd2_101;
																												BgL_tagzd2102zd2_46:
																													{	/* Expand/garith.scm 53 */
																														obj_t
																															BgL_nxz00_2204;
																														{	/* Expand/garith.scm 53 */
																															obj_t
																																BgL_arg3129z00_2205;
																															{	/* Expand/garith.scm 53 */
																																obj_t
																																	BgL_arg3131z00_2206;
																																obj_t
																																	BgL_arg3132z00_2207;
																																{	/* Expand/garith.scm 53 */
																																	obj_t
																																		BgL_arg3133z00_2208;
																																	BgL_arg3133z00_2208
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_bz00_45,
																																		BNIL);
																																	BgL_arg3131z00_2206
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(4),
																																		BgL_arg3133z00_2208);
																																}
																																{	/* Expand/garith.scm 54 */
																																	obj_t
																																		BgL_arg3135z00_2209;
																																	obj_t
																																		BgL_arg3136z00_2210;
																																	{	/* Expand/garith.scm 54 */
																																		obj_t
																																			BgL_arg3137z00_2211;
																																		obj_t
																																			BgL_arg3139z00_2212;
																																		BgL_arg3137z00_2211
																																			=
																																			BGl_fxze70ze7zzexpand_garithmetiquez00
																																			(BgL_idz00_43);
																																		{	/* Expand/garith.scm 54 */
																																			obj_t
																																				BgL_arg3140z00_2213;
																																			BgL_arg3140z00_2213
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_bz00_45,
																																				BNIL);
																																			BgL_arg3139z00_2212
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_az00_44,
																																				BgL_arg3140z00_2213);
																																		}
																																		BgL_arg3135z00_2209
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg3137z00_2211,
																																			BgL_arg3139z00_2212);
																																	}
																																	{	/* Expand/garith.scm 55 */
																																		obj_t
																																			BgL_arg3141z00_2214;
																																		{	/* Expand/garith.scm 55 */
																																			obj_t
																																				BgL_arg3144z00_2215;
																																			obj_t
																																				BgL_arg3145z00_2216;
																																			{	/* Expand/garith.scm 55 */
																																				obj_t
																																					BgL_arg3146z00_2217;
																																				{	/* Expand/garith.scm 55 */
																																					obj_t
																																						BgL_arg3149z00_2218;
																																					obj_t
																																						BgL_arg3154z00_2219;
																																					{	/* Expand/garith.scm 55 */
																																						obj_t
																																							BgL_symbolz00_2822;
																																						BgL_symbolz00_2822
																																							=
																																							CNST_TABLE_REF
																																							(6);
																																						{	/* Expand/garith.scm 55 */
																																							obj_t
																																								BgL_arg1455z00_2823;
																																							BgL_arg1455z00_2823
																																								=
																																								SYMBOL_TO_STRING
																																								(BgL_symbolz00_2822);
																																							BgL_arg3149z00_2218
																																								=
																																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																								(BgL_arg1455z00_2823);
																																						}
																																					}
																																					{	/* Expand/garith.scm 55 */
																																						obj_t
																																							BgL_arg1455z00_2825;
																																						BgL_arg1455z00_2825
																																							=
																																							SYMBOL_TO_STRING
																																							(((obj_t) BgL_idz00_43));
																																						BgL_arg3154z00_2219
																																							=
																																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																							(BgL_arg1455z00_2825);
																																					}
																																					BgL_arg3146z00_2217
																																						=
																																						string_append
																																						(BgL_arg3149z00_2218,
																																						BgL_arg3154z00_2219);
																																				}
																																				BgL_arg3144z00_2215
																																					=
																																					bstring_to_symbol
																																					(BgL_arg3146z00_2217);
																																			}
																																			{	/* Expand/garith.scm 55 */
																																				obj_t
																																					BgL_arg3155z00_2220;
																																				BgL_arg3155z00_2220
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_bz00_45,
																																					BNIL);
																																				BgL_arg3145z00_2216
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_az00_44,
																																					BgL_arg3155z00_2220);
																																			}
																																			BgL_arg3141z00_2214
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg3144z00_2215,
																																				BgL_arg3145z00_2216);
																																		}
																																		BgL_arg3136z00_2210
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg3141z00_2214,
																																			BNIL);
																																	}
																																	BgL_arg3132z00_2207
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg3135z00_2209,
																																		BgL_arg3136z00_2210);
																																}
																																BgL_arg3129z00_2205
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg3131z00_2206,
																																	BgL_arg3132z00_2207);
																															}
																															BgL_nxz00_2204 =
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(9),
																																BgL_arg3129z00_2205);
																														}
																														return
																															BGL_PROCEDURE_CALL2
																															(BgL_ez00_5,
																															BgL_nxz00_2204,
																															BgL_ez00_5);
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											if (SYMBOLP
																												(BgL_carzd2195zd2_98))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_carzd2282zd2_111;
																													BgL_carzd2282zd2_111 =
																														CAR(((obj_t)
																															BgL_cdrzd2196zd2_99));
																													if (INTEGERP
																														(BgL_carzd2282zd2_111))
																														{	/* Expand/garith.scm 49 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd2196zd2_99))))
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_arg1047z00_115;
																																	BgL_arg1047z00_115
																																		=
																																		CAR(((obj_t)
																																			BgL_xz00_4));
																																	BgL_idz00_47 =
																																		BgL_arg1047z00_115;
																																	BgL_az00_48 =
																																		BgL_carzd2195zd2_98;
																																	BgL_bz00_49 =
																																		BgL_carzd2282zd2_111;
																																BgL_tagzd2103zd2_50:
																																	{	/* Expand/garith.scm 58 */
																																		obj_t
																																			BgL_nxz00_2221;
																																		{	/* Expand/garith.scm 58 */
																																			obj_t
																																				BgL_arg3156z00_2222;
																																			{	/* Expand/garith.scm 58 */
																																				obj_t
																																					BgL_arg3157z00_2223;
																																				obj_t
																																					BgL_arg3158z00_2224;
																																				{	/* Expand/garith.scm 58 */
																																					obj_t
																																						BgL_arg3160z00_2225;
																																					BgL_arg3160z00_2225
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_az00_48,
																																						BNIL);
																																					BgL_arg3157z00_2223
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(4),
																																						BgL_arg3160z00_2225);
																																				}
																																				{	/* Expand/garith.scm 59 */
																																					obj_t
																																						BgL_arg3161z00_2226;
																																					obj_t
																																						BgL_arg3162z00_2227;
																																					{	/* Expand/garith.scm 59 */
																																						obj_t
																																							BgL_arg3163z00_2228;
																																						obj_t
																																							BgL_arg3171z00_2229;
																																						BgL_arg3163z00_2228
																																							=
																																							BGl_fxze70ze7zzexpand_garithmetiquez00
																																							(BgL_idz00_47);
																																						{	/* Expand/garith.scm 59 */
																																							obj_t
																																								BgL_arg3172z00_2230;
																																							BgL_arg3172z00_2230
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_bz00_49,
																																								BNIL);
																																							BgL_arg3171z00_2229
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_az00_48,
																																								BgL_arg3172z00_2230);
																																						}
																																						BgL_arg3161z00_2226
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg3163z00_2228,
																																							BgL_arg3171z00_2229);
																																					}
																																					{	/* Expand/garith.scm 60 */
																																						obj_t
																																							BgL_arg3174z00_2231;
																																						{	/* Expand/garith.scm 60 */
																																							obj_t
																																								BgL_arg3175z00_2232;
																																							obj_t
																																								BgL_arg3177z00_2233;
																																							{	/* Expand/garith.scm 60 */
																																								obj_t
																																									BgL_arg3178z00_2234;
																																								{	/* Expand/garith.scm 60 */
																																									obj_t
																																										BgL_arg3180z00_2235;
																																									obj_t
																																										BgL_arg3181z00_2236;
																																									{	/* Expand/garith.scm 60 */
																																										obj_t
																																											BgL_symbolz00_2827;
																																										BgL_symbolz00_2827
																																											=
																																											CNST_TABLE_REF
																																											(6);
																																										{	/* Expand/garith.scm 60 */
																																											obj_t
																																												BgL_arg1455z00_2828;
																																											BgL_arg1455z00_2828
																																												=
																																												SYMBOL_TO_STRING
																																												(BgL_symbolz00_2827);
																																											BgL_arg3180z00_2235
																																												=
																																												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																												(BgL_arg1455z00_2828);
																																										}
																																									}
																																									{	/* Expand/garith.scm 60 */
																																										obj_t
																																											BgL_arg1455z00_2830;
																																										BgL_arg1455z00_2830
																																											=
																																											SYMBOL_TO_STRING
																																											(
																																											((obj_t) BgL_idz00_47));
																																										BgL_arg3181z00_2236
																																											=
																																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																											(BgL_arg1455z00_2830);
																																									}
																																									BgL_arg3178z00_2234
																																										=
																																										string_append
																																										(BgL_arg3180z00_2235,
																																										BgL_arg3181z00_2236);
																																								}
																																								BgL_arg3175z00_2232
																																									=
																																									bstring_to_symbol
																																									(BgL_arg3178z00_2234);
																																							}
																																							{	/* Expand/garith.scm 60 */
																																								obj_t
																																									BgL_arg3182z00_2237;
																																								BgL_arg3182z00_2237
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_bz00_49,
																																									BNIL);
																																								BgL_arg3177z00_2233
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_az00_48,
																																									BgL_arg3182z00_2237);
																																							}
																																							BgL_arg3174z00_2231
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg3175z00_2232,
																																								BgL_arg3177z00_2233);
																																						}
																																						BgL_arg3162z00_2227
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg3174z00_2231,
																																							BNIL);
																																					}
																																					BgL_arg3158z00_2224
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg3161z00_2226,
																																						BgL_arg3162z00_2227);
																																				}
																																				BgL_arg3156z00_2222
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg3157z00_2223,
																																					BgL_arg3158z00_2224);
																																			}
																																			BgL_nxz00_2221
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(9),
																																				BgL_arg3156z00_2222);
																																		}
																																		return
																																			BGL_PROCEDURE_CALL2
																																			(BgL_ez00_5,
																																			BgL_nxz00_2221,
																																			BgL_ez00_5);
																																	}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_cdrzd2343zd2_117;
																															BgL_cdrzd2343zd2_117
																																=
																																CDR(((obj_t)
																																	BgL_xz00_4));
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_cdrzd2350zd2_118;
																																BgL_cdrzd2350zd2_118
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd2343zd2_117));
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_carzd2354zd2_119;
																																	BgL_carzd2354zd2_119
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2350zd2_118));
																																	if (REALP
																																		(BgL_carzd2354zd2_119))
																																		{	/* Expand/garith.scm 49 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd2350zd2_118))))
																																				{	/* Expand/garith.scm 49 */
																																					obj_t
																																						BgL_arg1052z00_123;
																																					obj_t
																																						BgL_arg1053z00_124;
																																					BgL_arg1052z00_123
																																						=
																																						CAR(
																																						((obj_t) BgL_xz00_4));
																																					BgL_arg1053z00_124
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2343zd2_117));
																																					BgL_idz00_51
																																						=
																																						BgL_arg1052z00_123;
																																					BgL_az00_52
																																						=
																																						BgL_arg1053z00_124;
																																					BgL_bz00_53
																																						=
																																						REAL_TO_DOUBLE
																																						(BgL_carzd2354zd2_119);
																																				BgL_tagzd2104zd2_54:
																																					{	/* Expand/garith.scm 63 */
																																						obj_t
																																							BgL_nxz00_2238;
																																						if (SYMBOLP(BgL_az00_52))
																																							{	/* Expand/garith.scm 64 */
																																								obj_t
																																									BgL_arg3185z00_2240;
																																								{	/* Expand/garith.scm 64 */
																																									obj_t
																																										BgL_arg3186z00_2241;
																																									obj_t
																																										BgL_arg3187z00_2242;
																																									{	/* Expand/garith.scm 64 */
																																										obj_t
																																											BgL_arg3189z00_2243;
																																										BgL_arg3189z00_2243
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_az00_52,
																																											BNIL);
																																										BgL_arg3186z00_2241
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(10),
																																											BgL_arg3189z00_2243);
																																									}
																																									{	/* Expand/garith.scm 65 */
																																										obj_t
																																											BgL_arg3190z00_2244;
																																										obj_t
																																											BgL_arg3192z00_2245;
																																										{	/* Expand/garith.scm 65 */
																																											obj_t
																																												BgL_arg3195z00_2246;
																																											obj_t
																																												BgL_arg3199z00_2247;
																																											{	/* Expand/garith.scm 65 */
																																												obj_t
																																													BgL_arg3200z00_2248;
																																												{	/* Expand/garith.scm 65 */
																																													obj_t
																																														BgL_arg3201z00_2249;
																																													obj_t
																																														BgL_arg3203z00_2250;
																																													{	/* Expand/garith.scm 65 */
																																														obj_t
																																															BgL_arg1455z00_2833;
																																														BgL_arg1455z00_2833
																																															=
																																															SYMBOL_TO_STRING
																																															(
																																															((obj_t) BgL_idz00_51));
																																														BgL_arg3201z00_2249
																																															=
																																															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																															(BgL_arg1455z00_2833);
																																													}
																																													{	/* Expand/garith.scm 65 */
																																														obj_t
																																															BgL_symbolz00_2834;
																																														BgL_symbolz00_2834
																																															=
																																															CNST_TABLE_REF
																																															(11);
																																														{	/* Expand/garith.scm 65 */
																																															obj_t
																																																BgL_arg1455z00_2835;
																																															BgL_arg1455z00_2835
																																																=
																																																SYMBOL_TO_STRING
																																																(BgL_symbolz00_2834);
																																															BgL_arg3203z00_2250
																																																=
																																																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																(BgL_arg1455z00_2835);
																																														}
																																													}
																																													BgL_arg3200z00_2248
																																														=
																																														string_append
																																														(BgL_arg3201z00_2249,
																																														BgL_arg3203z00_2250);
																																												}
																																												BgL_arg3195z00_2246
																																													=
																																													bstring_to_symbol
																																													(BgL_arg3200z00_2248);
																																											}
																																											{	/* Expand/garith.scm 65 */
																																												obj_t
																																													BgL_arg3204z00_2251;
																																												BgL_arg3204z00_2251
																																													=
																																													MAKE_YOUNG_PAIR
																																													(DOUBLE_TO_REAL
																																													(BgL_bz00_53),
																																													BNIL);
																																												BgL_arg3199z00_2247
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_az00_52,
																																													BgL_arg3204z00_2251);
																																											}
																																											BgL_arg3190z00_2244
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg3195z00_2246,
																																												BgL_arg3199z00_2247);
																																										}
																																										{	/* Expand/garith.scm 66 */
																																											obj_t
																																												BgL_arg3205z00_2252;
																																											{	/* Expand/garith.scm 66 */
																																												obj_t
																																													BgL_arg3207z00_2253;
																																												obj_t
																																													BgL_arg3208z00_2254;
																																												{	/* Expand/garith.scm 66 */
																																													obj_t
																																														BgL_arg3210z00_2255;
																																													{	/* Expand/garith.scm 66 */
																																														obj_t
																																															BgL_arg3211z00_2256;
																																														obj_t
																																															BgL_arg3212z00_2257;
																																														{	/* Expand/garith.scm 66 */
																																															obj_t
																																																BgL_symbolz00_2837;
																																															BgL_symbolz00_2837
																																																=
																																																CNST_TABLE_REF
																																																(6);
																																															{	/* Expand/garith.scm 66 */
																																																obj_t
																																																	BgL_arg1455z00_2838;
																																																BgL_arg1455z00_2838
																																																	=
																																																	SYMBOL_TO_STRING
																																																	(BgL_symbolz00_2837);
																																																BgL_arg3211z00_2256
																																																	=
																																																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																	(BgL_arg1455z00_2838);
																																															}
																																														}
																																														{	/* Expand/garith.scm 66 */
																																															obj_t
																																																BgL_arg1455z00_2840;
																																															BgL_arg1455z00_2840
																																																=
																																																SYMBOL_TO_STRING
																																																(
																																																((obj_t) BgL_idz00_51));
																																															BgL_arg3212z00_2257
																																																=
																																																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																(BgL_arg1455z00_2840);
																																														}
																																														BgL_arg3210z00_2255
																																															=
																																															string_append
																																															(BgL_arg3211z00_2256,
																																															BgL_arg3212z00_2257);
																																													}
																																													BgL_arg3207z00_2253
																																														=
																																														bstring_to_symbol
																																														(BgL_arg3210z00_2255);
																																												}
																																												{	/* Expand/garith.scm 66 */
																																													obj_t
																																														BgL_arg3217z00_2258;
																																													BgL_arg3217z00_2258
																																														=
																																														MAKE_YOUNG_PAIR
																																														(DOUBLE_TO_REAL
																																														(BgL_bz00_53),
																																														BNIL);
																																													BgL_arg3208z00_2254
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_az00_52,
																																														BgL_arg3217z00_2258);
																																												}
																																												BgL_arg3205z00_2252
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg3207z00_2253,
																																													BgL_arg3208z00_2254);
																																											}
																																											BgL_arg3192z00_2245
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg3205z00_2252,
																																												BNIL);
																																										}
																																										BgL_arg3187z00_2242
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg3190z00_2244,
																																											BgL_arg3192z00_2245);
																																									}
																																									BgL_arg3185z00_2240
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg3186z00_2241,
																																										BgL_arg3187z00_2242);
																																								}
																																								BgL_nxz00_2238
																																									=
																																									MAKE_YOUNG_PAIR
																																									(CNST_TABLE_REF
																																									(9),
																																									BgL_arg3185z00_2240);
																																							}
																																						else
																																							{	/* Expand/garith.scm 67 */
																																								obj_t
																																									BgL_tmpz00_2259;
																																								BgL_tmpz00_2259
																																									=
																																									BGl_gensymz00zz__r4_symbols_6_4z00
																																									(CNST_TABLE_REF
																																									(0));
																																								{	/* Expand/garith.scm 68 */
																																									obj_t
																																										BgL_arg3218z00_2260;
																																									{	/* Expand/garith.scm 68 */
																																										obj_t
																																											BgL_arg3220z00_2261;
																																										obj_t
																																											BgL_arg3221z00_2262;
																																										{	/* Expand/garith.scm 68 */
																																											obj_t
																																												BgL_arg3222z00_2263;
																																											{	/* Expand/garith.scm 68 */
																																												obj_t
																																													BgL_arg3223z00_2264;
																																												BgL_arg3223z00_2264
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_az00_52,
																																													BNIL);
																																												BgL_arg3222z00_2263
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_tmpz00_2259,
																																													BgL_arg3223z00_2264);
																																											}
																																											BgL_arg3220z00_2261
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg3222z00_2263,
																																												BNIL);
																																										}
																																										{	/* Expand/garith.scm 68 */
																																											obj_t
																																												BgL_arg3226z00_2265;
																																											{	/* Expand/garith.scm 68 */
																																												obj_t
																																													BgL_arg3229z00_2266;
																																												{	/* Expand/garith.scm 68 */
																																													obj_t
																																														BgL_arg3230z00_2267;
																																													BgL_arg3230z00_2267
																																														=
																																														MAKE_YOUNG_PAIR
																																														(DOUBLE_TO_REAL
																																														(BgL_bz00_53),
																																														BNIL);
																																													BgL_arg3229z00_2266
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_tmpz00_2259,
																																														BgL_arg3230z00_2267);
																																												}
																																												BgL_arg3226z00_2265
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_idz00_51,
																																													BgL_arg3229z00_2266);
																																											}
																																											BgL_arg3221z00_2262
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg3226z00_2265,
																																												BNIL);
																																										}
																																										BgL_arg3218z00_2260
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg3220z00_2261,
																																											BgL_arg3221z00_2262);
																																									}
																																									BgL_nxz00_2238
																																										=
																																										MAKE_YOUNG_PAIR
																																										(CNST_TABLE_REF
																																										(3),
																																										BgL_arg3218z00_2260);
																																								}
																																							}
																																						return
																																							BGL_PROCEDURE_CALL2
																																							(BgL_ez00_5,
																																							BgL_nxz00_2238,
																																							BgL_ez00_5);
																																					}
																																				}
																																			else
																																				{	/* Expand/garith.scm 49 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_carzd2413zd2_127;
																																			BgL_carzd2413zd2_127
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2343zd2_117));
																																			if (REALP
																																				(BgL_carzd2413zd2_127))
																																				{	/* Expand/garith.scm 49 */
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd2350zd2_118))))
																																						{	/* Expand/garith.scm 49 */
																																							obj_t
																																								BgL_arg1058z00_132;
																																							obj_t
																																								BgL_arg1059z00_133;
																																							BgL_arg1058z00_132
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_xz00_4));
																																							BgL_arg1059z00_133
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2350zd2_118));
																																							BgL_idz00_55
																																								=
																																								BgL_arg1058z00_132;
																																							BgL_az00_56
																																								=
																																								REAL_TO_DOUBLE
																																								(BgL_carzd2413zd2_127);
																																							BgL_bz00_57
																																								=
																																								BgL_arg1059z00_133;
																																						BgL_tagzd2105zd2_58:
																																							{	/* Expand/garith.scm 71 */
																																								obj_t
																																									BgL_nxz00_2268;
																																								if (SYMBOLP(BgL_bz00_57))
																																									{	/* Expand/garith.scm 72 */
																																										obj_t
																																											BgL_arg3233z00_2270;
																																										{	/* Expand/garith.scm 72 */
																																											obj_t
																																												BgL_arg3234z00_2271;
																																											obj_t
																																												BgL_arg3236z00_2272;
																																											{	/* Expand/garith.scm 72 */
																																												obj_t
																																													BgL_arg3237z00_2273;
																																												BgL_arg3237z00_2273
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_bz00_57,
																																													BNIL);
																																												BgL_arg3234z00_2271
																																													=
																																													MAKE_YOUNG_PAIR
																																													(CNST_TABLE_REF
																																													(10),
																																													BgL_arg3237z00_2273);
																																											}
																																											{	/* Expand/garith.scm 73 */
																																												obj_t
																																													BgL_arg3239z00_2274;
																																												obj_t
																																													BgL_arg3240z00_2275;
																																												{	/* Expand/garith.scm 73 */
																																													obj_t
																																														BgL_arg3241z00_2276;
																																													obj_t
																																														BgL_arg3242z00_2277;
																																													{	/* Expand/garith.scm 73 */
																																														obj_t
																																															BgL_arg3245z00_2278;
																																														{	/* Expand/garith.scm 73 */
																																															obj_t
																																																BgL_arg3246z00_2279;
																																															obj_t
																																																BgL_arg3247z00_2280;
																																															{	/* Expand/garith.scm 73 */
																																																obj_t
																																																	BgL_arg1455z00_2843;
																																																BgL_arg1455z00_2843
																																																	=
																																																	SYMBOL_TO_STRING
																																																	(
																																																	((obj_t) BgL_idz00_55));
																																																BgL_arg3246z00_2279
																																																	=
																																																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																	(BgL_arg1455z00_2843);
																																															}
																																															{	/* Expand/garith.scm 73 */
																																																obj_t
																																																	BgL_symbolz00_2844;
																																																BgL_symbolz00_2844
																																																	=
																																																	CNST_TABLE_REF
																																																	(11);
																																																{	/* Expand/garith.scm 73 */
																																																	obj_t
																																																		BgL_arg1455z00_2845;
																																																	BgL_arg1455z00_2845
																																																		=
																																																		SYMBOL_TO_STRING
																																																		(BgL_symbolz00_2844);
																																																	BgL_arg3247z00_2280
																																																		=
																																																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																		(BgL_arg1455z00_2845);
																																																}
																																															}
																																															BgL_arg3245z00_2278
																																																=
																																																string_append
																																																(BgL_arg3246z00_2279,
																																																BgL_arg3247z00_2280);
																																														}
																																														BgL_arg3241z00_2276
																																															=
																																															bstring_to_symbol
																																															(BgL_arg3245z00_2278);
																																													}
																																													{	/* Expand/garith.scm 73 */
																																														obj_t
																																															BgL_arg3249z00_2281;
																																														BgL_arg3249z00_2281
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_bz00_57,
																																															BNIL);
																																														BgL_arg3242z00_2277
																																															=
																																															MAKE_YOUNG_PAIR
																																															(DOUBLE_TO_REAL
																																															(BgL_az00_56),
																																															BgL_arg3249z00_2281);
																																													}
																																													BgL_arg3239z00_2274
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg3241z00_2276,
																																														BgL_arg3242z00_2277);
																																												}
																																												{	/* Expand/garith.scm 74 */
																																													obj_t
																																														BgL_arg3250z00_2282;
																																													{	/* Expand/garith.scm 74 */
																																														obj_t
																																															BgL_arg3251z00_2283;
																																														obj_t
																																															BgL_arg3252z00_2284;
																																														{	/* Expand/garith.scm 74 */
																																															obj_t
																																																BgL_arg3254z00_2285;
																																															{	/* Expand/garith.scm 74 */
																																																obj_t
																																																	BgL_arg3255z00_2286;
																																																obj_t
																																																	BgL_arg3256z00_2287;
																																																{	/* Expand/garith.scm 74 */
																																																	obj_t
																																																		BgL_symbolz00_2847;
																																																	BgL_symbolz00_2847
																																																		=
																																																		CNST_TABLE_REF
																																																		(6);
																																																	{	/* Expand/garith.scm 74 */
																																																		obj_t
																																																			BgL_arg1455z00_2848;
																																																		BgL_arg1455z00_2848
																																																			=
																																																			SYMBOL_TO_STRING
																																																			(BgL_symbolz00_2847);
																																																		BgL_arg3255z00_2286
																																																			=
																																																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																			(BgL_arg1455z00_2848);
																																																	}
																																																}
																																																{	/* Expand/garith.scm 74 */
																																																	obj_t
																																																		BgL_arg1455z00_2850;
																																																	BgL_arg1455z00_2850
																																																		=
																																																		SYMBOL_TO_STRING
																																																		(
																																																		((obj_t) BgL_idz00_55));
																																																	BgL_arg3256z00_2287
																																																		=
																																																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																		(BgL_arg1455z00_2850);
																																																}
																																																BgL_arg3254z00_2285
																																																	=
																																																	string_append
																																																	(BgL_arg3255z00_2286,
																																																	BgL_arg3256z00_2287);
																																															}
																																															BgL_arg3251z00_2283
																																																=
																																																bstring_to_symbol
																																																(BgL_arg3254z00_2285);
																																														}
																																														{	/* Expand/garith.scm 74 */
																																															obj_t
																																																BgL_arg3259z00_2288;
																																															BgL_arg3259z00_2288
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_bz00_57,
																																																BNIL);
																																															BgL_arg3252z00_2284
																																																=
																																																MAKE_YOUNG_PAIR
																																																(DOUBLE_TO_REAL
																																																(BgL_az00_56),
																																																BgL_arg3259z00_2288);
																																														}
																																														BgL_arg3250z00_2282
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_arg3251z00_2283,
																																															BgL_arg3252z00_2284);
																																													}
																																													BgL_arg3240z00_2275
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg3250z00_2282,
																																														BNIL);
																																												}
																																												BgL_arg3236z00_2272
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg3239z00_2274,
																																													BgL_arg3240z00_2275);
																																											}
																																											BgL_arg3233z00_2270
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg3234z00_2271,
																																												BgL_arg3236z00_2272);
																																										}
																																										BgL_nxz00_2268
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(9),
																																											BgL_arg3233z00_2270);
																																									}
																																								else
																																									{	/* Expand/garith.scm 75 */
																																										obj_t
																																											BgL_tmpz00_2289;
																																										BgL_tmpz00_2289
																																											=
																																											BGl_gensymz00zz__r4_symbols_6_4z00
																																											(CNST_TABLE_REF
																																											(1));
																																										{	/* Expand/garith.scm 76 */
																																											obj_t
																																												BgL_arg3263z00_2290;
																																											{	/* Expand/garith.scm 76 */
																																												obj_t
																																													BgL_arg3264z00_2291;
																																												obj_t
																																													BgL_arg3266z00_2292;
																																												{	/* Expand/garith.scm 76 */
																																													obj_t
																																														BgL_arg3267z00_2293;
																																													{	/* Expand/garith.scm 76 */
																																														obj_t
																																															BgL_arg3268z00_2294;
																																														BgL_arg3268z00_2294
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_bz00_57,
																																															BNIL);
																																														BgL_arg3267z00_2293
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_tmpz00_2289,
																																															BgL_arg3268z00_2294);
																																													}
																																													BgL_arg3264z00_2291
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg3267z00_2293,
																																														BNIL);
																																												}
																																												{	/* Expand/garith.scm 76 */
																																													obj_t
																																														BgL_arg3269z00_2295;
																																													{	/* Expand/garith.scm 76 */
																																														obj_t
																																															BgL_arg3271z00_2296;
																																														{	/* Expand/garith.scm 76 */
																																															obj_t
																																																BgL_arg3272z00_2297;
																																															BgL_arg3272z00_2297
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_tmpz00_2289,
																																																BNIL);
																																															BgL_arg3271z00_2296
																																																=
																																																MAKE_YOUNG_PAIR
																																																(DOUBLE_TO_REAL
																																																(BgL_az00_56),
																																																BgL_arg3272z00_2297);
																																														}
																																														BgL_arg3269z00_2295
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_idz00_55,
																																															BgL_arg3271z00_2296);
																																													}
																																													BgL_arg3266z00_2292
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg3269z00_2295,
																																														BNIL);
																																												}
																																												BgL_arg3263z00_2290
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg3264z00_2291,
																																													BgL_arg3266z00_2292);
																																											}
																																											BgL_nxz00_2268
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(3),
																																												BgL_arg3263z00_2290);
																																										}
																																									}
																																								return
																																									BGL_PROCEDURE_CALL2
																																									(BgL_ez00_5,
																																									BgL_nxz00_2268,
																																									BgL_ez00_5);
																																							}
																																						}
																																					else
																																						{	/* Expand/garith.scm 49 */
																																							return
																																								BFALSE;
																																						}
																																				}
																																			else
																																				{	/* Expand/garith.scm 49 */
																																					obj_t
																																						BgL_cdrzd2464zd2_135;
																																					BgL_cdrzd2464zd2_135
																																						=
																																						CDR(
																																						((obj_t) BgL_xz00_4));
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_carzd2470zd2_136;
																																						obj_t
																																							BgL_cdrzd2471zd2_137;
																																						BgL_carzd2470zd2_136
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd2464zd2_135));
																																						BgL_cdrzd2471zd2_137
																																							=
																																							CDR
																																							(((obj_t) BgL_cdrzd2464zd2_135));
																																						if (SYMBOLP(BgL_carzd2470zd2_136))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_carzd2477zd2_139;
																																								BgL_carzd2477zd2_139
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd2471zd2_137));
																																								if (SYMBOLP(BgL_carzd2477zd2_139))
																																									{	/* Expand/garith.scm 49 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd2471zd2_137))))
																																											{	/* Expand/garith.scm 49 */
																																												obj_t
																																													BgL_arg1065z00_143;
																																												BgL_arg1065z00_143
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_xz00_4));
																																												BgL_idz00_59
																																													=
																																													BgL_arg1065z00_143;
																																												BgL_az00_60
																																													=
																																													BgL_carzd2470zd2_136;
																																												BgL_bz00_61
																																													=
																																													BgL_carzd2477zd2_139;
																																											BgL_tagzd2106zd2_62:
																																												{	/* Expand/garith.scm 79 */
																																													obj_t
																																														BgL_nxz00_2298;
																																													{	/* Expand/garith.scm 79 */
																																														obj_t
																																															BgL_arg3273z00_2299;
																																														{	/* Expand/garith.scm 79 */
																																															obj_t
																																																BgL_arg3275z00_2300;
																																															obj_t
																																																BgL_arg3276z00_2301;
																																															{	/* Expand/garith.scm 79 */
																																																obj_t
																																																	BgL_arg3281z00_2302;
																																																{	/* Expand/garith.scm 79 */
																																																	obj_t
																																																		BgL_arg3282z00_2303;
																																																	obj_t
																																																		BgL_arg3287z00_2304;
																																																	{	/* Expand/garith.scm 79 */
																																																		obj_t
																																																			BgL_arg3288z00_2305;
																																																		BgL_arg3288z00_2305
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_az00_60,
																																																			BNIL);
																																																		BgL_arg3282z00_2303
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(CNST_TABLE_REF
																																																			(4),
																																																			BgL_arg3288z00_2305);
																																																	}
																																																	{	/* Expand/garith.scm 79 */
																																																		obj_t
																																																			BgL_arg3289z00_2306;
																																																		{	/* Expand/garith.scm 79 */
																																																			obj_t
																																																				BgL_arg3290z00_2307;
																																																			BgL_arg3290z00_2307
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_bz00_61,
																																																				BNIL);
																																																			BgL_arg3289z00_2306
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(CNST_TABLE_REF
																																																				(4),
																																																				BgL_arg3290z00_2307);
																																																		}
																																																		BgL_arg3287z00_2304
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_arg3289z00_2306,
																																																			BNIL);
																																																	}
																																																	BgL_arg3281z00_2302
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg3282z00_2303,
																																																		BgL_arg3287z00_2304);
																																																}
																																																BgL_arg3275z00_2300
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(CNST_TABLE_REF
																																																	(5),
																																																	BgL_arg3281z00_2302);
																																															}
																																															{	/* Expand/garith.scm 80 */
																																																obj_t
																																																	BgL_arg3292z00_2308;
																																																obj_t
																																																	BgL_arg3294z00_2309;
																																																{	/* Expand/garith.scm 80 */
																																																	obj_t
																																																		BgL_arg3295z00_2310;
																																																	obj_t
																																																		BgL_arg3298z00_2311;
																																																	BgL_arg3295z00_2310
																																																		=
																																																		BGl_fxze70ze7zzexpand_garithmetiquez00
																																																		(BgL_idz00_59);
																																																	{	/* Expand/garith.scm 80 */
																																																		obj_t
																																																			BgL_arg3299z00_2312;
																																																		BgL_arg3299z00_2312
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_bz00_61,
																																																			BNIL);
																																																		BgL_arg3298z00_2311
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_az00_60,
																																																			BgL_arg3299z00_2312);
																																																	}
																																																	BgL_arg3292z00_2308
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg3295z00_2310,
																																																		BgL_arg3298z00_2311);
																																																}
																																																{	/* Expand/garith.scm 81 */
																																																	obj_t
																																																		BgL_arg3301z00_2313;
																																																	{	/* Expand/garith.scm 81 */
																																																		obj_t
																																																			BgL_arg3302z00_2314;
																																																		obj_t
																																																			BgL_arg3305z00_2315;
																																																		{	/* Expand/garith.scm 81 */
																																																			obj_t
																																																				BgL_arg3306z00_2316;
																																																			{	/* Expand/garith.scm 81 */
																																																				obj_t
																																																					BgL_arg3307z00_2317;
																																																				obj_t
																																																					BgL_arg3308z00_2318;
																																																				{	/* Expand/garith.scm 81 */
																																																					obj_t
																																																						BgL_arg3309z00_2319;
																																																					{	/* Expand/garith.scm 81 */
																																																						obj_t
																																																							BgL_arg3310z00_2320;
																																																						obj_t
																																																							BgL_arg3311z00_2321;
																																																						{	/* Expand/garith.scm 81 */
																																																							obj_t
																																																								BgL_symbolz00_2852;
																																																							BgL_symbolz00_2852
																																																								=
																																																								CNST_TABLE_REF
																																																								(6);
																																																							{	/* Expand/garith.scm 81 */
																																																								obj_t
																																																									BgL_arg1455z00_2853;
																																																								BgL_arg1455z00_2853
																																																									=
																																																									SYMBOL_TO_STRING
																																																									(BgL_symbolz00_2852);
																																																								BgL_arg3310z00_2320
																																																									=
																																																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																									(BgL_arg1455z00_2853);
																																																							}
																																																						}
																																																						{	/* Expand/garith.scm 81 */
																																																							obj_t
																																																								BgL_arg1455z00_2855;
																																																							BgL_arg1455z00_2855
																																																								=
																																																								SYMBOL_TO_STRING
																																																								(
																																																								((obj_t) BgL_idz00_59));
																																																							BgL_arg3311z00_2321
																																																								=
																																																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																								(BgL_arg1455z00_2855);
																																																						}
																																																						BgL_arg3309z00_2319
																																																							=
																																																							string_append
																																																							(BgL_arg3310z00_2320,
																																																							BgL_arg3311z00_2321);
																																																					}
																																																					BgL_arg3307z00_2317
																																																						=
																																																						bstring_to_symbol
																																																						(BgL_arg3309z00_2319);
																																																				}
																																																				BgL_arg3308z00_2318
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(CNST_TABLE_REF
																																																					(7),
																																																					BNIL);
																																																				BgL_arg3306z00_2316
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_arg3307z00_2317,
																																																					BgL_arg3308z00_2318);
																																																			}
																																																			BgL_arg3302z00_2314
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(CNST_TABLE_REF
																																																				(8),
																																																				BgL_arg3306z00_2316);
																																																		}
																																																		{	/* Expand/garith.scm 81 */
																																																			obj_t
																																																				BgL_arg3312z00_2322;
																																																			BgL_arg3312z00_2322
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_bz00_61,
																																																				BNIL);
																																																			BgL_arg3305z00_2315
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_az00_60,
																																																				BgL_arg3312z00_2322);
																																																		}
																																																		BgL_arg3301z00_2313
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_arg3302z00_2314,
																																																			BgL_arg3305z00_2315);
																																																	}
																																																	BgL_arg3294z00_2309
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg3301z00_2313,
																																																		BNIL);
																																																}
																																																BgL_arg3276z00_2301
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg3292z00_2308,
																																																	BgL_arg3294z00_2309);
																																															}
																																															BgL_arg3273z00_2299
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg3275z00_2300,
																																																BgL_arg3276z00_2301);
																																														}
																																														BgL_nxz00_2298
																																															=
																																															MAKE_YOUNG_PAIR
																																															(CNST_TABLE_REF
																																															(9),
																																															BgL_arg3273z00_2299);
																																													}
																																													return
																																														BGL_PROCEDURE_CALL2
																																														(BgL_ez00_5,
																																														BgL_nxz00_2298,
																																														BgL_ez00_5);
																																												}
																																											}
																																										else
																																											{	/* Expand/garith.scm 49 */
																																												return
																																													BFALSE;
																																											}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd2471zd2_137))))
																																											{	/* Expand/garith.scm 49 */
																																												obj_t
																																													BgL_arg1074z00_151;
																																												obj_t
																																													BgL_arg1075z00_152;
																																												BgL_arg1074z00_151
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_xz00_4));
																																												BgL_arg1075z00_152
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd2471zd2_137));
																																												BgL_idz00_63
																																													=
																																													BgL_arg1074z00_151;
																																												BgL_az00_64
																																													=
																																													BgL_carzd2470zd2_136;
																																												BgL_bz00_65
																																													=
																																													BgL_arg1075z00_152;
																																											BgL_tagzd2107zd2_66:
																																												{	/* Expand/garith.scm 84 */
																																													obj_t
																																														BgL_tmpz00_2323;
																																													BgL_tmpz00_2323
																																														=
																																														BGl_gensymz00zz__r4_symbols_6_4z00
																																														(CNST_TABLE_REF
																																														(1));
																																													{	/* Expand/garith.scm 84 */
																																														obj_t
																																															BgL_nxz00_2324;
																																														{	/* Expand/garith.scm 85 */
																																															obj_t
																																																BgL_arg3313z00_2325;
																																															{	/* Expand/garith.scm 85 */
																																																obj_t
																																																	BgL_arg3314z00_2326;
																																																obj_t
																																																	BgL_arg3315z00_2327;
																																																{	/* Expand/garith.scm 85 */
																																																	obj_t
																																																		BgL_arg3316z00_2328;
																																																	{	/* Expand/garith.scm 85 */
																																																		obj_t
																																																			BgL_arg3317z00_2329;
																																																		BgL_arg3317z00_2329
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_bz00_65,
																																																			BNIL);
																																																		BgL_arg3316z00_2328
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_tmpz00_2323,
																																																			BgL_arg3317z00_2329);
																																																	}
																																																	BgL_arg3314z00_2326
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg3316z00_2328,
																																																		BNIL);
																																																}
																																																{	/* Expand/garith.scm 85 */
																																																	obj_t
																																																		BgL_arg3318z00_2330;
																																																	{	/* Expand/garith.scm 85 */
																																																		obj_t
																																																			BgL_arg3321z00_2331;
																																																		{	/* Expand/garith.scm 85 */
																																																			obj_t
																																																				BgL_arg3322z00_2332;
																																																			BgL_arg3322z00_2332
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_tmpz00_2323,
																																																				BNIL);
																																																			BgL_arg3321z00_2331
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_az00_64,
																																																				BgL_arg3322z00_2332);
																																																		}
																																																		BgL_arg3318z00_2330
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_idz00_63,
																																																			BgL_arg3321z00_2331);
																																																	}
																																																	BgL_arg3315z00_2327
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg3318z00_2330,
																																																		BNIL);
																																																}
																																																BgL_arg3313z00_2325
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg3314z00_2326,
																																																	BgL_arg3315z00_2327);
																																															}
																																															BgL_nxz00_2324
																																																=
																																																MAKE_YOUNG_PAIR
																																																(CNST_TABLE_REF
																																																(3),
																																																BgL_arg3313z00_2325);
																																														}
																																														{	/* Expand/garith.scm 85 */

																																															return
																																																BGL_PROCEDURE_CALL2
																																																(BgL_ez00_5,
																																																BgL_nxz00_2324,
																																																BgL_ez00_5);
																																														}
																																													}
																																												}
																																											}
																																										else
																																											{	/* Expand/garith.scm 49 */
																																												return
																																													BFALSE;
																																											}
																																									}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_cdrzd2651zd2_180;
																																								BgL_cdrzd2651zd2_180
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								{	/* Expand/garith.scm 49 */
																																									obj_t
																																										BgL_cdrzd2658zd2_181;
																																									BgL_cdrzd2658zd2_181
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd2651zd2_180));
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_carzd2662zd2_182;
																																										BgL_carzd2662zd2_182
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd2658zd2_181));
																																										if (SYMBOLP(BgL_carzd2662zd2_182))
																																											{	/* Expand/garith.scm 49 */
																																												if (NULLP(CDR(((obj_t) BgL_cdrzd2658zd2_181))))
																																													{	/* Expand/garith.scm 49 */
																																														obj_t
																																															BgL_arg1122z00_186;
																																														obj_t
																																															BgL_arg1123z00_187;
																																														BgL_arg1122z00_186
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_xz00_4));
																																														BgL_arg1123z00_187
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd2651zd2_180));
																																														BgL_idz00_67
																																															=
																																															BgL_arg1122z00_186;
																																														BgL_az00_68
																																															=
																																															BgL_arg1123z00_187;
																																														BgL_bz00_69
																																															=
																																															BgL_carzd2662zd2_182;
																																													BgL_tagzd2108zd2_70:
																																														{	/* Expand/garith.scm 88 */
																																															obj_t
																																																BgL_tmpz00_2333;
																																															BgL_tmpz00_2333
																																																=
																																																BGl_gensymz00zz__r4_symbols_6_4z00
																																																(CNST_TABLE_REF
																																																(0));
																																															{	/* Expand/garith.scm 88 */
																																																obj_t
																																																	BgL_nxz00_2334;
																																																{	/* Expand/garith.scm 89 */
																																																	obj_t
																																																		BgL_arg3323z00_2335;
																																																	{	/* Expand/garith.scm 89 */
																																																		obj_t
																																																			BgL_arg3326z00_2336;
																																																		obj_t
																																																			BgL_arg3327z00_2337;
																																																		{	/* Expand/garith.scm 89 */
																																																			obj_t
																																																				BgL_arg3337z00_2338;
																																																			{	/* Expand/garith.scm 89 */
																																																				obj_t
																																																					BgL_arg3338z00_2339;
																																																				BgL_arg3338z00_2339
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_az00_68,
																																																					BNIL);
																																																				BgL_arg3337z00_2338
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_tmpz00_2333,
																																																					BgL_arg3338z00_2339);
																																																			}
																																																			BgL_arg3326z00_2336
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_arg3337z00_2338,
																																																				BNIL);
																																																		}
																																																		{	/* Expand/garith.scm 89 */
																																																			obj_t
																																																				BgL_arg3339z00_2340;
																																																			{	/* Expand/garith.scm 89 */
																																																				obj_t
																																																					BgL_arg3349z00_2341;
																																																				{	/* Expand/garith.scm 89 */
																																																					obj_t
																																																						BgL_arg3350z00_2342;
																																																					BgL_arg3350z00_2342
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_bz00_69,
																																																						BNIL);
																																																					BgL_arg3349z00_2341
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_tmpz00_2333,
																																																						BgL_arg3350z00_2342);
																																																				}
																																																				BgL_arg3339z00_2340
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_idz00_67,
																																																					BgL_arg3349z00_2341);
																																																			}
																																																			BgL_arg3327z00_2337
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_arg3339z00_2340,
																																																				BNIL);
																																																		}
																																																		BgL_arg3323z00_2335
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_arg3326z00_2336,
																																																			BgL_arg3327z00_2337);
																																																	}
																																																	BgL_nxz00_2334
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(CNST_TABLE_REF
																																																		(3),
																																																		BgL_arg3323z00_2335);
																																																}
																																																{	/* Expand/garith.scm 89 */

																																																	return
																																																		BGL_PROCEDURE_CALL2
																																																		(BgL_ez00_5,
																																																		BgL_nxz00_2334,
																																																		BgL_ez00_5);
																																																}
																																															}
																																														}
																																													}
																																												else
																																													{	/* Expand/garith.scm 49 */
																																														return
																																															BFALSE;
																																													}
																																											}
																																										else
																																											{	/* Expand/garith.scm 49 */
																																												if (NULLP(CDR(((obj_t) BgL_cdrzd2658zd2_181))))
																																													{	/* Expand/garith.scm 49 */
																																														obj_t
																																															BgL_arg1129z00_193;
																																														obj_t
																																															BgL_arg1131z00_194;
																																														obj_t
																																															BgL_arg1132z00_195;
																																														BgL_arg1129z00_193
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_xz00_4));
																																														BgL_arg1131z00_194
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd2651zd2_180));
																																														BgL_arg1132z00_195
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd2658zd2_181));
																																														BgL_idz00_71
																																															=
																																															BgL_arg1129z00_193;
																																														BgL_az00_72
																																															=
																																															BgL_arg1131z00_194;
																																														BgL_bz00_73
																																															=
																																															BgL_arg1132z00_195;
																																													BgL_tagzd2109zd2_74:
																																														{	/* Expand/garith.scm 93 */
																																															obj_t
																																																BgL_tmpaz00_2343;
																																															BgL_tmpaz00_2343
																																																=
																																																BGl_gensymz00zz__r4_symbols_6_4z00
																																																(CNST_TABLE_REF
																																																(0));
																																															{	/* Expand/garith.scm 93 */
																																																obj_t
																																																	BgL_tmpbz00_2344;
																																																BgL_tmpbz00_2344
																																																	=
																																																	BGl_gensymz00zz__r4_symbols_6_4z00
																																																	(CNST_TABLE_REF
																																																	(1));
																																																{	/* Expand/garith.scm 94 */
																																																	obj_t
																																																		BgL_nxz00_2345;
																																																	{	/* Expand/garith.scm 95 */
																																																		obj_t
																																																			BgL_arg3352z00_2346;
																																																		{	/* Expand/garith.scm 95 */
																																																			obj_t
																																																				BgL_arg3356z00_2347;
																																																			obj_t
																																																				BgL_arg3357z00_2348;
																																																			{	/* Expand/garith.scm 95 */
																																																				obj_t
																																																					BgL_arg3358z00_2349;
																																																				obj_t
																																																					BgL_arg3359z00_2350;
																																																				{	/* Expand/garith.scm 95 */
																																																					obj_t
																																																						BgL_arg3360z00_2351;
																																																					BgL_arg3360z00_2351
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_az00_72,
																																																						BNIL);
																																																					BgL_arg3358z00_2349
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_tmpaz00_2343,
																																																						BgL_arg3360z00_2351);
																																																				}
																																																				{	/* Expand/garith.scm 95 */
																																																					obj_t
																																																						BgL_arg3361z00_2352;
																																																					{	/* Expand/garith.scm 95 */
																																																						obj_t
																																																							BgL_arg3362z00_2353;
																																																						BgL_arg3362z00_2353
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_bz00_73,
																																																							BNIL);
																																																						BgL_arg3361z00_2352
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_tmpbz00_2344,
																																																							BgL_arg3362z00_2353);
																																																					}
																																																					BgL_arg3359z00_2350
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_arg3361z00_2352,
																																																						BNIL);
																																																				}
																																																				BgL_arg3356z00_2347
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_arg3358z00_2349,
																																																					BgL_arg3359z00_2350);
																																																			}
																																																			{	/* Expand/garith.scm 95 */
																																																				obj_t
																																																					BgL_arg3364z00_2354;
																																																				{	/* Expand/garith.scm 95 */
																																																					obj_t
																																																						BgL_arg3367z00_2355;
																																																					{	/* Expand/garith.scm 95 */
																																																						obj_t
																																																							BgL_arg3369z00_2356;
																																																						BgL_arg3369z00_2356
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_tmpbz00_2344,
																																																							BNIL);
																																																						BgL_arg3367z00_2355
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_tmpaz00_2343,
																																																							BgL_arg3369z00_2356);
																																																					}
																																																					BgL_arg3364z00_2354
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_idz00_71,
																																																						BgL_arg3367z00_2355);
																																																				}
																																																				BgL_arg3357z00_2348
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_arg3364z00_2354,
																																																					BNIL);
																																																			}
																																																			BgL_arg3352z00_2346
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_arg3356z00_2347,
																																																				BgL_arg3357z00_2348);
																																																		}
																																																		BgL_nxz00_2345
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(CNST_TABLE_REF
																																																			(2),
																																																			BgL_arg3352z00_2346);
																																																	}
																																																	{	/* Expand/garith.scm 95 */

																																																		return
																																																			BGL_PROCEDURE_CALL2
																																																			(BgL_ez00_5,
																																																			BgL_nxz00_2345,
																																																			BgL_ez00_5);
																																																	}
																																																}
																																															}
																																														}
																																													}
																																												else
																																													{	/* Expand/garith.scm 49 */
																																														return
																																															BFALSE;
																																													}
																																											}
																																									}
																																								}
																																							}
																																					}
																																				}
																																		}
																																}
																															}
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd2706zd2_197;
																													BgL_cdrzd2706zd2_197 =
																														CDR(((obj_t)
																															BgL_xz00_4));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_cdrzd2713zd2_198;
																														BgL_cdrzd2713zd2_198
																															=
																															CDR(((obj_t)
																																BgL_cdrzd2706zd2_197));
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_carzd2717zd2_199;
																															BgL_carzd2717zd2_199
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2713zd2_198));
																															if (REALP
																																(BgL_carzd2717zd2_199))
																																{	/* Expand/garith.scm 49 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd2713zd2_198))))
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_arg1142z00_203;
																																			obj_t
																																				BgL_arg1143z00_204;
																																			BgL_arg1142z00_203
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			BgL_arg1143z00_204
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2706zd2_197));
																																			{
																																				double
																																					BgL_bz00_4952;
																																				obj_t
																																					BgL_az00_4951;
																																				obj_t
																																					BgL_idz00_4950;
																																				BgL_idz00_4950
																																					=
																																					BgL_arg1142z00_203;
																																				BgL_az00_4951
																																					=
																																					BgL_arg1143z00_204;
																																				BgL_bz00_4952
																																					=
																																					REAL_TO_DOUBLE
																																					(BgL_carzd2717zd2_199);
																																				BgL_bz00_53
																																					=
																																					BgL_bz00_4952;
																																				BgL_az00_52
																																					=
																																					BgL_az00_4951;
																																				BgL_idz00_51
																																					=
																																					BgL_idz00_4950;
																																				goto
																																					BgL_tagzd2104zd2_54;
																																			}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_carzd2776zd2_207;
																																	BgL_carzd2776zd2_207
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2706zd2_197));
																																	if (REALP
																																		(BgL_carzd2776zd2_207))
																																		{	/* Expand/garith.scm 49 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd2713zd2_198))))
																																				{	/* Expand/garith.scm 49 */
																																					obj_t
																																						BgL_arg1152z00_212;
																																					obj_t
																																						BgL_arg1153z00_213;
																																					BgL_arg1152z00_212
																																						=
																																						CAR(
																																						((obj_t) BgL_xz00_4));
																																					BgL_arg1153z00_213
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2713zd2_198));
																																					{
																																						obj_t
																																							BgL_bz00_4969;
																																						double
																																							BgL_az00_4967;
																																						obj_t
																																							BgL_idz00_4966;
																																						BgL_idz00_4966
																																							=
																																							BgL_arg1152z00_212;
																																						BgL_az00_4967
																																							=
																																							REAL_TO_DOUBLE
																																							(BgL_carzd2776zd2_207);
																																						BgL_bz00_4969
																																							=
																																							BgL_arg1153z00_213;
																																						BgL_bz00_57
																																							=
																																							BgL_bz00_4969;
																																						BgL_az00_56
																																							=
																																							BgL_az00_4967;
																																						BgL_idz00_55
																																							=
																																							BgL_idz00_4966;
																																						goto
																																							BgL_tagzd2105zd2_58;
																																					}
																																				}
																																			else
																																				{	/* Expand/garith.scm 49 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_cdrzd2827zd2_215;
																																			BgL_cdrzd2827zd2_215
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd2833zd2_216;
																																				obj_t
																																					BgL_cdrzd2834zd2_217;
																																				BgL_carzd2833zd2_216
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd2827zd2_215));
																																				BgL_cdrzd2834zd2_217
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd2827zd2_215));
																																				if (SYMBOLP(BgL_carzd2833zd2_216))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_carzd2840zd2_219;
																																						BgL_carzd2840zd2_219
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd2834zd2_217));
																																						if (SYMBOLP(BgL_carzd2840zd2_219))
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd2834zd2_217))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1162z00_223;
																																										BgL_arg1162z00_223
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										{
																																											obj_t
																																												BgL_bz00_4990;
																																											obj_t
																																												BgL_az00_4989;
																																											obj_t
																																												BgL_idz00_4988;
																																											BgL_idz00_4988
																																												=
																																												BgL_arg1162z00_223;
																																											BgL_az00_4989
																																												=
																																												BgL_carzd2833zd2_216;
																																											BgL_bz00_4990
																																												=
																																												BgL_carzd2840zd2_219;
																																											BgL_bz00_61
																																												=
																																												BgL_bz00_4990;
																																											BgL_az00_60
																																												=
																																												BgL_az00_4989;
																																											BgL_idz00_59
																																												=
																																												BgL_idz00_4988;
																																											goto
																																												BgL_tagzd2106zd2_62;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd2834zd2_217))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1171z00_231;
																																										obj_t
																																											BgL_arg1172z00_232;
																																										BgL_arg1171z00_231
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg1172z00_232
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd2834zd2_217));
																																										{
																																											obj_t
																																												BgL_bz00_5001;
																																											obj_t
																																												BgL_az00_5000;
																																											obj_t
																																												BgL_idz00_4999;
																																											BgL_idz00_4999
																																												=
																																												BgL_arg1171z00_231;
																																											BgL_az00_5000
																																												=
																																												BgL_carzd2833zd2_216;
																																											BgL_bz00_5001
																																												=
																																												BgL_arg1172z00_232;
																																											BgL_bz00_65
																																												=
																																												BgL_bz00_5001;
																																											BgL_az00_64
																																												=
																																												BgL_az00_5000;
																																											BgL_idz00_63
																																												=
																																												BgL_idz00_4999;
																																											goto
																																												BgL_tagzd2107zd2_66;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_cdrzd21014zd2_260;
																																						BgL_cdrzd21014zd2_260
																																							=
																																							CDR
																																							(((obj_t) BgL_xz00_4));
																																						{	/* Expand/garith.scm 49 */
																																							obj_t
																																								BgL_cdrzd21021zd2_261;
																																							BgL_cdrzd21021zd2_261
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd21014zd2_260));
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_carzd21025zd2_262;
																																								BgL_carzd21025zd2_262
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd21021zd2_261));
																																								if (SYMBOLP(BgL_carzd21025zd2_262))
																																									{	/* Expand/garith.scm 49 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd21021zd2_261))))
																																											{	/* Expand/garith.scm 49 */
																																												obj_t
																																													BgL_arg1212z00_266;
																																												obj_t
																																													BgL_arg1215z00_267;
																																												BgL_arg1212z00_266
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_xz00_4));
																																												BgL_arg1215z00_267
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21014zd2_260));
																																												{
																																													obj_t
																																														BgL_bz00_5020;
																																													obj_t
																																														BgL_az00_5019;
																																													obj_t
																																														BgL_idz00_5018;
																																													BgL_idz00_5018
																																														=
																																														BgL_arg1212z00_266;
																																													BgL_az00_5019
																																														=
																																														BgL_arg1215z00_267;
																																													BgL_bz00_5020
																																														=
																																														BgL_carzd21025zd2_262;
																																													BgL_bz00_69
																																														=
																																														BgL_bz00_5020;
																																													BgL_az00_68
																																														=
																																														BgL_az00_5019;
																																													BgL_idz00_67
																																														=
																																														BgL_idz00_5018;
																																													goto
																																														BgL_tagzd2108zd2_70;
																																												}
																																											}
																																										else
																																											{	/* Expand/garith.scm 49 */
																																												return
																																													BFALSE;
																																											}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd21021zd2_261))))
																																											{	/* Expand/garith.scm 49 */
																																												obj_t
																																													BgL_arg1220z00_273;
																																												obj_t
																																													BgL_arg1221z00_274;
																																												obj_t
																																													BgL_arg1223z00_275;
																																												BgL_arg1220z00_273
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_xz00_4));
																																												BgL_arg1221z00_274
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21014zd2_260));
																																												BgL_arg1223z00_275
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21021zd2_261));
																																												{
																																													obj_t
																																														BgL_bz00_5033;
																																													obj_t
																																														BgL_az00_5032;
																																													obj_t
																																														BgL_idz00_5031;
																																													BgL_idz00_5031
																																														=
																																														BgL_arg1220z00_273;
																																													BgL_az00_5032
																																														=
																																														BgL_arg1221z00_274;
																																													BgL_bz00_5033
																																														=
																																														BgL_arg1223z00_275;
																																													BgL_bz00_73
																																														=
																																														BgL_bz00_5033;
																																													BgL_az00_72
																																														=
																																														BgL_az00_5032;
																																													BgL_idz00_71
																																														=
																																														BgL_idz00_5031;
																																													goto
																																														BgL_tagzd2109zd2_74;
																																												}
																																											}
																																										else
																																											{	/* Expand/garith.scm 49 */
																																												return
																																													BFALSE;
																																											}
																																									}
																																							}
																																						}
																																					}
																																			}
																																		}
																																}
																														}
																													}
																												}
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									if (SYMBOLP
																										(BgL_carzd2195zd2_98))
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd21082zd2_281;
																											BgL_carzd21082zd2_281 =
																												CAR(((obj_t)
																													BgL_cdrzd2196zd2_99));
																											if (INTEGERP
																												(BgL_carzd21082zd2_281))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd2196zd2_99))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg1230z00_285;
																															BgL_arg1230z00_285
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															{
																																obj_t
																																	BgL_bz00_5048;
																																obj_t
																																	BgL_az00_5047;
																																obj_t
																																	BgL_idz00_5046;
																																BgL_idz00_5046 =
																																	BgL_arg1230z00_285;
																																BgL_az00_5047 =
																																	BgL_carzd2195zd2_98;
																																BgL_bz00_5048 =
																																	BgL_carzd21082zd2_281;
																																BgL_bz00_49 =
																																	BgL_bz00_5048;
																																BgL_az00_48 =
																																	BgL_az00_5047;
																																BgL_idz00_47 =
																																	BgL_idz00_5046;
																																goto
																																	BgL_tagzd2103zd2_50;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd21143zd2_287;
																													BgL_cdrzd21143zd2_287
																														=
																														CDR(((obj_t)
																															BgL_xz00_4));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_cdrzd21150zd2_288;
																														BgL_cdrzd21150zd2_288
																															=
																															CDR(((obj_t)
																																BgL_cdrzd21143zd2_287));
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_carzd21154zd2_289;
																															BgL_carzd21154zd2_289
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21150zd2_288));
																															if (REALP
																																(BgL_carzd21154zd2_289))
																																{	/* Expand/garith.scm 49 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd21150zd2_288))))
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_arg1236z00_293;
																																			obj_t
																																				BgL_arg1238z00_294;
																																			BgL_arg1236z00_293
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			BgL_arg1238z00_294
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd21143zd2_287));
																																			{
																																				double
																																					BgL_bz00_5067;
																																				obj_t
																																					BgL_az00_5066;
																																				obj_t
																																					BgL_idz00_5065;
																																				BgL_idz00_5065
																																					=
																																					BgL_arg1236z00_293;
																																				BgL_az00_5066
																																					=
																																					BgL_arg1238z00_294;
																																				BgL_bz00_5067
																																					=
																																					REAL_TO_DOUBLE
																																					(BgL_carzd21154zd2_289);
																																				BgL_bz00_53
																																					=
																																					BgL_bz00_5067;
																																				BgL_az00_52
																																					=
																																					BgL_az00_5066;
																																				BgL_idz00_51
																																					=
																																					BgL_idz00_5065;
																																				goto
																																					BgL_tagzd2104zd2_54;
																																			}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_carzd21213zd2_297;
																																	BgL_carzd21213zd2_297
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21143zd2_287));
																																	if (REALP
																																		(BgL_carzd21213zd2_297))
																																		{	/* Expand/garith.scm 49 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd21150zd2_288))))
																																				{	/* Expand/garith.scm 49 */
																																					obj_t
																																						BgL_arg1244z00_302;
																																					obj_t
																																						BgL_arg1248z00_303;
																																					BgL_arg1244z00_302
																																						=
																																						CAR(
																																						((obj_t) BgL_xz00_4));
																																					BgL_arg1248z00_303
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21150zd2_288));
																																					{
																																						obj_t
																																							BgL_bz00_5084;
																																						double
																																							BgL_az00_5082;
																																						obj_t
																																							BgL_idz00_5081;
																																						BgL_idz00_5081
																																							=
																																							BgL_arg1244z00_302;
																																						BgL_az00_5082
																																							=
																																							REAL_TO_DOUBLE
																																							(BgL_carzd21213zd2_297);
																																						BgL_bz00_5084
																																							=
																																							BgL_arg1248z00_303;
																																						BgL_bz00_57
																																							=
																																							BgL_bz00_5084;
																																						BgL_az00_56
																																							=
																																							BgL_az00_5082;
																																						BgL_idz00_55
																																							=
																																							BgL_idz00_5081;
																																						goto
																																							BgL_tagzd2105zd2_58;
																																					}
																																				}
																																			else
																																				{	/* Expand/garith.scm 49 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_cdrzd21264zd2_305;
																																			BgL_cdrzd21264zd2_305
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd21270zd2_306;
																																				obj_t
																																					BgL_cdrzd21271zd2_307;
																																				BgL_carzd21270zd2_306
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd21264zd2_305));
																																				BgL_cdrzd21271zd2_307
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd21264zd2_305));
																																				if (SYMBOLP(BgL_carzd21270zd2_306))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_carzd21277zd2_309;
																																						BgL_carzd21277zd2_309
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd21271zd2_307));
																																						if (SYMBOLP(BgL_carzd21277zd2_309))
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd21271zd2_307))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1268z00_313;
																																										BgL_arg1268z00_313
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										{
																																											obj_t
																																												BgL_bz00_5105;
																																											obj_t
																																												BgL_az00_5104;
																																											obj_t
																																												BgL_idz00_5103;
																																											BgL_idz00_5103
																																												=
																																												BgL_arg1268z00_313;
																																											BgL_az00_5104
																																												=
																																												BgL_carzd21270zd2_306;
																																											BgL_bz00_5105
																																												=
																																												BgL_carzd21277zd2_309;
																																											BgL_bz00_61
																																												=
																																												BgL_bz00_5105;
																																											BgL_az00_60
																																												=
																																												BgL_az00_5104;
																																											BgL_idz00_59
																																												=
																																												BgL_idz00_5103;
																																											goto
																																												BgL_tagzd2106zd2_62;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd21271zd2_307))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1304z00_321;
																																										obj_t
																																											BgL_arg1305z00_322;
																																										BgL_arg1304z00_321
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg1305z00_322
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd21271zd2_307));
																																										{
																																											obj_t
																																												BgL_bz00_5116;
																																											obj_t
																																												BgL_az00_5115;
																																											obj_t
																																												BgL_idz00_5114;
																																											BgL_idz00_5114
																																												=
																																												BgL_arg1304z00_321;
																																											BgL_az00_5115
																																												=
																																												BgL_carzd21270zd2_306;
																																											BgL_bz00_5116
																																												=
																																												BgL_arg1305z00_322;
																																											BgL_bz00_65
																																												=
																																												BgL_bz00_5116;
																																											BgL_az00_64
																																												=
																																												BgL_az00_5115;
																																											BgL_idz00_63
																																												=
																																												BgL_idz00_5114;
																																											goto
																																												BgL_tagzd2107zd2_66;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_cdrzd21451zd2_350;
																																						BgL_cdrzd21451zd2_350
																																							=
																																							CDR
																																							(((obj_t) BgL_xz00_4));
																																						{	/* Expand/garith.scm 49 */
																																							obj_t
																																								BgL_cdrzd21458zd2_351;
																																							BgL_cdrzd21458zd2_351
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd21451zd2_350));
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_carzd21462zd2_352;
																																								BgL_carzd21462zd2_352
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd21458zd2_351));
																																								if (SYMBOLP(BgL_carzd21462zd2_352))
																																									{	/* Expand/garith.scm 49 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd21458zd2_351))))
																																											{	/* Expand/garith.scm 49 */
																																												obj_t
																																													BgL_arg1331z00_356;
																																												obj_t
																																													BgL_arg1332z00_357;
																																												BgL_arg1331z00_356
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_xz00_4));
																																												BgL_arg1332z00_357
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21451zd2_350));
																																												{
																																													obj_t
																																														BgL_bz00_5135;
																																													obj_t
																																														BgL_az00_5134;
																																													obj_t
																																														BgL_idz00_5133;
																																													BgL_idz00_5133
																																														=
																																														BgL_arg1331z00_356;
																																													BgL_az00_5134
																																														=
																																														BgL_arg1332z00_357;
																																													BgL_bz00_5135
																																														=
																																														BgL_carzd21462zd2_352;
																																													BgL_bz00_69
																																														=
																																														BgL_bz00_5135;
																																													BgL_az00_68
																																														=
																																														BgL_az00_5134;
																																													BgL_idz00_67
																																														=
																																														BgL_idz00_5133;
																																													goto
																																														BgL_tagzd2108zd2_70;
																																												}
																																											}
																																										else
																																											{	/* Expand/garith.scm 49 */
																																												return
																																													BFALSE;
																																											}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd21458zd2_351))))
																																											{	/* Expand/garith.scm 49 */
																																												obj_t
																																													BgL_arg1339z00_363;
																																												obj_t
																																													BgL_arg1340z00_364;
																																												obj_t
																																													BgL_arg1342z00_365;
																																												BgL_arg1339z00_363
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_xz00_4));
																																												BgL_arg1340z00_364
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21451zd2_350));
																																												BgL_arg1342z00_365
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21458zd2_351));
																																												{
																																													obj_t
																																														BgL_bz00_5148;
																																													obj_t
																																														BgL_az00_5147;
																																													obj_t
																																														BgL_idz00_5146;
																																													BgL_idz00_5146
																																														=
																																														BgL_arg1339z00_363;
																																													BgL_az00_5147
																																														=
																																														BgL_arg1340z00_364;
																																													BgL_bz00_5148
																																														=
																																														BgL_arg1342z00_365;
																																													BgL_bz00_73
																																														=
																																														BgL_bz00_5148;
																																													BgL_az00_72
																																														=
																																														BgL_az00_5147;
																																													BgL_idz00_71
																																														=
																																														BgL_idz00_5146;
																																													goto
																																														BgL_tagzd2109zd2_74;
																																												}
																																											}
																																										else
																																											{	/* Expand/garith.scm 49 */
																																												return
																																													BFALSE;
																																											}
																																									}
																																							}
																																						}
																																					}
																																			}
																																		}
																																}
																														}
																													}
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_cdrzd21506zd2_367;
																											BgL_cdrzd21506zd2_367 =
																												CDR(((obj_t)
																													BgL_xz00_4));
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_cdrzd21513zd2_368;
																												BgL_cdrzd21513zd2_368 =
																													CDR(((obj_t)
																														BgL_cdrzd21506zd2_367));
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_carzd21517zd2_369;
																													BgL_carzd21517zd2_369
																														=
																														CAR(((obj_t)
																															BgL_cdrzd21513zd2_368));
																													if (REALP
																														(BgL_carzd21517zd2_369))
																														{	/* Expand/garith.scm 49 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd21513zd2_368))))
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_arg1348z00_373;
																																	obj_t
																																		BgL_arg1349z00_374;
																																	BgL_arg1348z00_373
																																		=
																																		CAR(((obj_t)
																																			BgL_xz00_4));
																																	BgL_arg1349z00_374
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21506zd2_367));
																																	{
																																		double
																																			BgL_bz00_5167;
																																		obj_t
																																			BgL_az00_5166;
																																		obj_t
																																			BgL_idz00_5165;
																																		BgL_idz00_5165
																																			=
																																			BgL_arg1348z00_373;
																																		BgL_az00_5166
																																			=
																																			BgL_arg1349z00_374;
																																		BgL_bz00_5167
																																			=
																																			REAL_TO_DOUBLE
																																			(BgL_carzd21517zd2_369);
																																		BgL_bz00_53
																																			=
																																			BgL_bz00_5167;
																																		BgL_az00_52
																																			=
																																			BgL_az00_5166;
																																		BgL_idz00_51
																																			=
																																			BgL_idz00_5165;
																																		goto
																																			BgL_tagzd2104zd2_54;
																																	}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_carzd21576zd2_377;
																															BgL_carzd21576zd2_377
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21506zd2_367));
																															if (REALP
																																(BgL_carzd21576zd2_377))
																																{	/* Expand/garith.scm 49 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd21513zd2_368))))
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_arg1364z00_382;
																																			obj_t
																																				BgL_arg1367z00_383;
																																			BgL_arg1364z00_382
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			BgL_arg1367z00_383
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd21513zd2_368));
																																			{
																																				obj_t
																																					BgL_bz00_5184;
																																				double
																																					BgL_az00_5182;
																																				obj_t
																																					BgL_idz00_5181;
																																				BgL_idz00_5181
																																					=
																																					BgL_arg1364z00_382;
																																				BgL_az00_5182
																																					=
																																					REAL_TO_DOUBLE
																																					(BgL_carzd21576zd2_377);
																																				BgL_bz00_5184
																																					=
																																					BgL_arg1367z00_383;
																																				BgL_bz00_57
																																					=
																																					BgL_bz00_5184;
																																				BgL_az00_56
																																					=
																																					BgL_az00_5182;
																																				BgL_idz00_55
																																					=
																																					BgL_idz00_5181;
																																				goto
																																					BgL_tagzd2105zd2_58;
																																			}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_cdrzd21627zd2_385;
																																	BgL_cdrzd21627zd2_385
																																		=
																																		CDR(((obj_t)
																																			BgL_xz00_4));
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd21633zd2_386;
																																		obj_t
																																			BgL_cdrzd21634zd2_387;
																																		BgL_carzd21633zd2_386
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd21627zd2_385));
																																		BgL_cdrzd21634zd2_387
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd21627zd2_385));
																																		if (SYMBOLP
																																			(BgL_carzd21633zd2_386))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd21640zd2_389;
																																				BgL_carzd21640zd2_389
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd21634zd2_387));
																																				if (SYMBOLP(BgL_carzd21640zd2_389))
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd21634zd2_387))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg1408z00_393;
																																								BgL_arg1408z00_393
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								{
																																									obj_t
																																										BgL_bz00_5205;
																																									obj_t
																																										BgL_az00_5204;
																																									obj_t
																																										BgL_idz00_5203;
																																									BgL_idz00_5203
																																										=
																																										BgL_arg1408z00_393;
																																									BgL_az00_5204
																																										=
																																										BgL_carzd21633zd2_386;
																																									BgL_bz00_5205
																																										=
																																										BgL_carzd21640zd2_389;
																																									BgL_bz00_61
																																										=
																																										BgL_bz00_5205;
																																									BgL_az00_60
																																										=
																																										BgL_az00_5204;
																																									BgL_idz00_59
																																										=
																																										BgL_idz00_5203;
																																									goto
																																										BgL_tagzd2106zd2_62;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd21634zd2_387))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg1434z00_401;
																																								obj_t
																																									BgL_arg1437z00_402;
																																								BgL_arg1434z00_401
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg1437z00_402
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd21634zd2_387));
																																								{
																																									obj_t
																																										BgL_bz00_5216;
																																									obj_t
																																										BgL_az00_5215;
																																									obj_t
																																										BgL_idz00_5214;
																																									BgL_idz00_5214
																																										=
																																										BgL_arg1434z00_401;
																																									BgL_az00_5215
																																										=
																																										BgL_carzd21633zd2_386;
																																									BgL_bz00_5216
																																										=
																																										BgL_arg1437z00_402;
																																									BgL_bz00_65
																																										=
																																										BgL_bz00_5216;
																																									BgL_az00_64
																																										=
																																										BgL_az00_5215;
																																									BgL_idz00_63
																																										=
																																										BgL_idz00_5214;
																																									goto
																																										BgL_tagzd2107zd2_66;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_cdrzd21814zd2_430;
																																				BgL_cdrzd21814zd2_430
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				{	/* Expand/garith.scm 49 */
																																					obj_t
																																						BgL_cdrzd21821zd2_431;
																																					BgL_cdrzd21821zd2_431
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd21814zd2_430));
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_carzd21825zd2_432;
																																						BgL_carzd21825zd2_432
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd21821zd2_431));
																																						if (SYMBOLP(BgL_carzd21825zd2_432))
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd21821zd2_431))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1559z00_436;
																																										obj_t
																																											BgL_arg1561z00_437;
																																										BgL_arg1559z00_436
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg1561z00_437
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd21814zd2_430));
																																										{
																																											obj_t
																																												BgL_bz00_5235;
																																											obj_t
																																												BgL_az00_5234;
																																											obj_t
																																												BgL_idz00_5233;
																																											BgL_idz00_5233
																																												=
																																												BgL_arg1559z00_436;
																																											BgL_az00_5234
																																												=
																																												BgL_arg1561z00_437;
																																											BgL_bz00_5235
																																												=
																																												BgL_carzd21825zd2_432;
																																											BgL_bz00_69
																																												=
																																												BgL_bz00_5235;
																																											BgL_az00_68
																																												=
																																												BgL_az00_5234;
																																											BgL_idz00_67
																																												=
																																												BgL_idz00_5233;
																																											goto
																																												BgL_tagzd2108zd2_70;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd21821zd2_431))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1571z00_443;
																																										obj_t
																																											BgL_arg1573z00_444;
																																										obj_t
																																											BgL_arg1575z00_445;
																																										BgL_arg1571z00_443
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg1573z00_444
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd21814zd2_430));
																																										BgL_arg1575z00_445
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd21821zd2_431));
																																										{
																																											obj_t
																																												BgL_bz00_5248;
																																											obj_t
																																												BgL_az00_5247;
																																											obj_t
																																												BgL_idz00_5246;
																																											BgL_idz00_5246
																																												=
																																												BgL_arg1571z00_443;
																																											BgL_az00_5247
																																												=
																																												BgL_arg1573z00_444;
																																											BgL_bz00_5248
																																												=
																																												BgL_arg1575z00_445;
																																											BgL_bz00_73
																																												=
																																												BgL_bz00_5248;
																																											BgL_az00_72
																																												=
																																												BgL_az00_5247;
																																											BgL_idz00_71
																																												=
																																												BgL_idz00_5246;
																																											goto
																																												BgL_tagzd2109zd2_74;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																					}
																																				}
																																			}
																																	}
																																}
																														}
																												}
																											}
																										}
																								}
																						}
																					}
																			}
																		else
																			{	/* Expand/garith.scm 49 */
																				obj_t BgL_cdrzd21868zd2_448;

																				BgL_cdrzd21868zd2_448 =
																					CDR(((obj_t) BgL_xz00_4));
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_carzd21873zd2_449;
																					obj_t BgL_cdrzd21874zd2_450;

																					BgL_carzd21873zd2_449 =
																						CAR(
																						((obj_t) BgL_cdrzd21868zd2_448));
																					BgL_cdrzd21874zd2_450 =
																						CDR(
																						((obj_t) BgL_cdrzd21868zd2_448));
																					if (INTEGERP(BgL_carzd21873zd2_449))
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd21878zd2_452;

																							BgL_carzd21878zd2_452 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd21874zd2_450));
																							if (SYMBOLP
																								(BgL_carzd21878zd2_452))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd21874zd2_450))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg1593z00_456;

																											BgL_arg1593z00_456 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											{
																												obj_t BgL_bz00_5269;
																												obj_t BgL_az00_5268;
																												obj_t BgL_idz00_5267;

																												BgL_idz00_5267 =
																													BgL_arg1593z00_456;
																												BgL_az00_5268 =
																													BgL_carzd21873zd2_449;
																												BgL_bz00_5269 =
																													BgL_carzd21878zd2_452;
																												BgL_bz00_45 =
																													BgL_bz00_5269;
																												BgL_az00_44 =
																													BgL_az00_5268;
																												BgL_idz00_43 =
																													BgL_idz00_5267;
																												goto
																													BgL_tagzd2102zd2_46;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									if (SYMBOLP
																										(BgL_carzd21873zd2_449))
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd21960zd2_462;
																											BgL_carzd21960zd2_462 =
																												CAR(((obj_t)
																													BgL_cdrzd21874zd2_450));
																											if (INTEGERP
																												(BgL_carzd21960zd2_462))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd21874zd2_450))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg1605z00_466;
																															BgL_arg1605z00_466
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															{
																																obj_t
																																	BgL_bz00_5284;
																																obj_t
																																	BgL_az00_5283;
																																obj_t
																																	BgL_idz00_5282;
																																BgL_idz00_5282 =
																																	BgL_arg1605z00_466;
																																BgL_az00_5283 =
																																	BgL_carzd21873zd2_449;
																																BgL_bz00_5284 =
																																	BgL_carzd21960zd2_462;
																																BgL_bz00_49 =
																																	BgL_bz00_5284;
																																BgL_az00_48 =
																																	BgL_az00_5283;
																																BgL_idz00_47 =
																																	BgL_idz00_5282;
																																goto
																																	BgL_tagzd2103zd2_50;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd22021zd2_468;
																													BgL_cdrzd22021zd2_468
																														=
																														CDR(((obj_t)
																															BgL_xz00_4));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_cdrzd22028zd2_469;
																														BgL_cdrzd22028zd2_469
																															=
																															CDR(((obj_t)
																																BgL_cdrzd22021zd2_468));
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_carzd22032zd2_470;
																															BgL_carzd22032zd2_470
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd22028zd2_469));
																															if (REALP
																																(BgL_carzd22032zd2_470))
																																{	/* Expand/garith.scm 49 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd22028zd2_469))))
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_arg1611z00_474;
																																			obj_t
																																				BgL_arg1613z00_475;
																																			BgL_arg1611z00_474
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			BgL_arg1613z00_475
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd22021zd2_468));
																																			{
																																				double
																																					BgL_bz00_5303;
																																				obj_t
																																					BgL_az00_5302;
																																				obj_t
																																					BgL_idz00_5301;
																																				BgL_idz00_5301
																																					=
																																					BgL_arg1611z00_474;
																																				BgL_az00_5302
																																					=
																																					BgL_arg1613z00_475;
																																				BgL_bz00_5303
																																					=
																																					REAL_TO_DOUBLE
																																					(BgL_carzd22032zd2_470);
																																				BgL_bz00_53
																																					=
																																					BgL_bz00_5303;
																																				BgL_az00_52
																																					=
																																					BgL_az00_5302;
																																				BgL_idz00_51
																																					=
																																					BgL_idz00_5301;
																																				goto
																																					BgL_tagzd2104zd2_54;
																																			}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_carzd22091zd2_478;
																																	BgL_carzd22091zd2_478
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd22021zd2_468));
																																	if (REALP
																																		(BgL_carzd22091zd2_478))
																																		{	/* Expand/garith.scm 49 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd22028zd2_469))))
																																				{	/* Expand/garith.scm 49 */
																																					obj_t
																																						BgL_arg1627z00_483;
																																					obj_t
																																						BgL_arg1629z00_484;
																																					BgL_arg1627z00_483
																																						=
																																						CAR(
																																						((obj_t) BgL_xz00_4));
																																					BgL_arg1629z00_484
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd22028zd2_469));
																																					{
																																						obj_t
																																							BgL_bz00_5320;
																																						double
																																							BgL_az00_5318;
																																						obj_t
																																							BgL_idz00_5317;
																																						BgL_idz00_5317
																																							=
																																							BgL_arg1627z00_483;
																																						BgL_az00_5318
																																							=
																																							REAL_TO_DOUBLE
																																							(BgL_carzd22091zd2_478);
																																						BgL_bz00_5320
																																							=
																																							BgL_arg1629z00_484;
																																						BgL_bz00_57
																																							=
																																							BgL_bz00_5320;
																																						BgL_az00_56
																																							=
																																							BgL_az00_5318;
																																						BgL_idz00_55
																																							=
																																							BgL_idz00_5317;
																																						goto
																																							BgL_tagzd2105zd2_58;
																																					}
																																				}
																																			else
																																				{	/* Expand/garith.scm 49 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_cdrzd22142zd2_486;
																																			BgL_cdrzd22142zd2_486
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd22148zd2_487;
																																				obj_t
																																					BgL_cdrzd22149zd2_488;
																																				BgL_carzd22148zd2_487
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd22142zd2_486));
																																				BgL_cdrzd22149zd2_488
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd22142zd2_486));
																																				if (SYMBOLP(BgL_carzd22148zd2_487))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_carzd22155zd2_490;
																																						BgL_carzd22155zd2_490
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd22149zd2_488));
																																						if (SYMBOLP(BgL_carzd22155zd2_490))
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd22149zd2_488))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1646z00_494;
																																										BgL_arg1646z00_494
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										{
																																											obj_t
																																												BgL_bz00_5341;
																																											obj_t
																																												BgL_az00_5340;
																																											obj_t
																																												BgL_idz00_5339;
																																											BgL_idz00_5339
																																												=
																																												BgL_arg1646z00_494;
																																											BgL_az00_5340
																																												=
																																												BgL_carzd22148zd2_487;
																																											BgL_bz00_5341
																																												=
																																												BgL_carzd22155zd2_490;
																																											BgL_bz00_61
																																												=
																																												BgL_bz00_5341;
																																											BgL_az00_60
																																												=
																																												BgL_az00_5340;
																																											BgL_idz00_59
																																												=
																																												BgL_idz00_5339;
																																											goto
																																												BgL_tagzd2106zd2_62;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd22149zd2_488))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1661z00_502;
																																										obj_t
																																											BgL_arg1663z00_503;
																																										BgL_arg1661z00_502
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg1663z00_503
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd22149zd2_488));
																																										{
																																											obj_t
																																												BgL_bz00_5352;
																																											obj_t
																																												BgL_az00_5351;
																																											obj_t
																																												BgL_idz00_5350;
																																											BgL_idz00_5350
																																												=
																																												BgL_arg1661z00_502;
																																											BgL_az00_5351
																																												=
																																												BgL_carzd22148zd2_487;
																																											BgL_bz00_5352
																																												=
																																												BgL_arg1663z00_503;
																																											BgL_bz00_65
																																												=
																																												BgL_bz00_5352;
																																											BgL_az00_64
																																												=
																																												BgL_az00_5351;
																																											BgL_idz00_63
																																												=
																																												BgL_idz00_5350;
																																											goto
																																												BgL_tagzd2107zd2_66;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_cdrzd22329zd2_531;
																																						BgL_cdrzd22329zd2_531
																																							=
																																							CDR
																																							(((obj_t) BgL_xz00_4));
																																						{	/* Expand/garith.scm 49 */
																																							obj_t
																																								BgL_cdrzd22336zd2_532;
																																							BgL_cdrzd22336zd2_532
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd22329zd2_531));
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_carzd22340zd2_533;
																																								BgL_carzd22340zd2_533
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd22336zd2_532));
																																								if (SYMBOLP(BgL_carzd22340zd2_533))
																																									{	/* Expand/garith.scm 49 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd22336zd2_532))))
																																											{	/* Expand/garith.scm 49 */
																																												obj_t
																																													BgL_arg1717z00_537;
																																												obj_t
																																													BgL_arg1718z00_538;
																																												BgL_arg1717z00_537
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_xz00_4));
																																												BgL_arg1718z00_538
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd22329zd2_531));
																																												{
																																													obj_t
																																														BgL_bz00_5371;
																																													obj_t
																																														BgL_az00_5370;
																																													obj_t
																																														BgL_idz00_5369;
																																													BgL_idz00_5369
																																														=
																																														BgL_arg1717z00_537;
																																													BgL_az00_5370
																																														=
																																														BgL_arg1718z00_538;
																																													BgL_bz00_5371
																																														=
																																														BgL_carzd22340zd2_533;
																																													BgL_bz00_69
																																														=
																																														BgL_bz00_5371;
																																													BgL_az00_68
																																														=
																																														BgL_az00_5370;
																																													BgL_idz00_67
																																														=
																																														BgL_idz00_5369;
																																													goto
																																														BgL_tagzd2108zd2_70;
																																												}
																																											}
																																										else
																																											{	/* Expand/garith.scm 49 */
																																												return
																																													BFALSE;
																																											}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd22336zd2_532))))
																																											{	/* Expand/garith.scm 49 */
																																												obj_t
																																													BgL_arg1724z00_544;
																																												obj_t
																																													BgL_arg1733z00_545;
																																												obj_t
																																													BgL_arg1734z00_546;
																																												BgL_arg1724z00_544
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_xz00_4));
																																												BgL_arg1733z00_545
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd22329zd2_531));
																																												BgL_arg1734z00_546
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd22336zd2_532));
																																												{
																																													obj_t
																																														BgL_bz00_5384;
																																													obj_t
																																														BgL_az00_5383;
																																													obj_t
																																														BgL_idz00_5382;
																																													BgL_idz00_5382
																																														=
																																														BgL_arg1724z00_544;
																																													BgL_az00_5383
																																														=
																																														BgL_arg1733z00_545;
																																													BgL_bz00_5384
																																														=
																																														BgL_arg1734z00_546;
																																													BgL_bz00_73
																																														=
																																														BgL_bz00_5384;
																																													BgL_az00_72
																																														=
																																														BgL_az00_5383;
																																													BgL_idz00_71
																																														=
																																														BgL_idz00_5382;
																																													goto
																																														BgL_tagzd2109zd2_74;
																																												}
																																											}
																																										else
																																											{	/* Expand/garith.scm 49 */
																																												return
																																													BFALSE;
																																											}
																																									}
																																							}
																																						}
																																					}
																																			}
																																		}
																																}
																														}
																													}
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_cdrzd22384zd2_548;
																											BgL_cdrzd22384zd2_548 =
																												CDR(((obj_t)
																													BgL_xz00_4));
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_cdrzd22391zd2_549;
																												BgL_cdrzd22391zd2_549 =
																													CDR(((obj_t)
																														BgL_cdrzd22384zd2_548));
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_carzd22395zd2_550;
																													BgL_carzd22395zd2_550
																														=
																														CAR(((obj_t)
																															BgL_cdrzd22391zd2_549));
																													if (REALP
																														(BgL_carzd22395zd2_550))
																														{	/* Expand/garith.scm 49 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd22391zd2_549))))
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_arg1739z00_554;
																																	obj_t
																																		BgL_arg1740z00_555;
																																	BgL_arg1739z00_554
																																		=
																																		CAR(((obj_t)
																																			BgL_xz00_4));
																																	BgL_arg1740z00_555
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd22384zd2_548));
																																	{
																																		double
																																			BgL_bz00_5403;
																																		obj_t
																																			BgL_az00_5402;
																																		obj_t
																																			BgL_idz00_5401;
																																		BgL_idz00_5401
																																			=
																																			BgL_arg1739z00_554;
																																		BgL_az00_5402
																																			=
																																			BgL_arg1740z00_555;
																																		BgL_bz00_5403
																																			=
																																			REAL_TO_DOUBLE
																																			(BgL_carzd22395zd2_550);
																																		BgL_bz00_53
																																			=
																																			BgL_bz00_5403;
																																		BgL_az00_52
																																			=
																																			BgL_az00_5402;
																																		BgL_idz00_51
																																			=
																																			BgL_idz00_5401;
																																		goto
																																			BgL_tagzd2104zd2_54;
																																	}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_carzd22454zd2_558;
																															BgL_carzd22454zd2_558
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd22384zd2_548));
																															if (REALP
																																(BgL_carzd22454zd2_558))
																																{	/* Expand/garith.scm 49 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd22391zd2_549))))
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_arg1751z00_563;
																																			obj_t
																																				BgL_arg1752z00_564;
																																			BgL_arg1751z00_563
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			BgL_arg1752z00_564
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd22391zd2_549));
																																			{
																																				obj_t
																																					BgL_bz00_5420;
																																				double
																																					BgL_az00_5418;
																																				obj_t
																																					BgL_idz00_5417;
																																				BgL_idz00_5417
																																					=
																																					BgL_arg1751z00_563;
																																				BgL_az00_5418
																																					=
																																					REAL_TO_DOUBLE
																																					(BgL_carzd22454zd2_558);
																																				BgL_bz00_5420
																																					=
																																					BgL_arg1752z00_564;
																																				BgL_bz00_57
																																					=
																																					BgL_bz00_5420;
																																				BgL_az00_56
																																					=
																																					BgL_az00_5418;
																																				BgL_idz00_55
																																					=
																																					BgL_idz00_5417;
																																				goto
																																					BgL_tagzd2105zd2_58;
																																			}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_cdrzd22505zd2_566;
																																	BgL_cdrzd22505zd2_566
																																		=
																																		CDR(((obj_t)
																																			BgL_xz00_4));
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd22511zd2_567;
																																		obj_t
																																			BgL_cdrzd22512zd2_568;
																																		BgL_carzd22511zd2_567
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd22505zd2_566));
																																		BgL_cdrzd22512zd2_568
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd22505zd2_566));
																																		if (SYMBOLP
																																			(BgL_carzd22511zd2_567))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd22518zd2_570;
																																				BgL_carzd22518zd2_570
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd22512zd2_568));
																																				if (SYMBOLP(BgL_carzd22518zd2_570))
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd22512zd2_568))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg1765z00_574;
																																								BgL_arg1765z00_574
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								{
																																									obj_t
																																										BgL_bz00_5441;
																																									obj_t
																																										BgL_az00_5440;
																																									obj_t
																																										BgL_idz00_5439;
																																									BgL_idz00_5439
																																										=
																																										BgL_arg1765z00_574;
																																									BgL_az00_5440
																																										=
																																										BgL_carzd22511zd2_567;
																																									BgL_bz00_5441
																																										=
																																										BgL_carzd22518zd2_570;
																																									BgL_bz00_61
																																										=
																																										BgL_bz00_5441;
																																									BgL_az00_60
																																										=
																																										BgL_az00_5440;
																																									BgL_idz00_59
																																										=
																																										BgL_idz00_5439;
																																									goto
																																										BgL_tagzd2106zd2_62;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd22512zd2_568))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg1773z00_582;
																																								obj_t
																																									BgL_arg1775z00_583;
																																								BgL_arg1773z00_582
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg1775z00_583
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd22512zd2_568));
																																								{
																																									obj_t
																																										BgL_bz00_5452;
																																									obj_t
																																										BgL_az00_5451;
																																									obj_t
																																										BgL_idz00_5450;
																																									BgL_idz00_5450
																																										=
																																										BgL_arg1773z00_582;
																																									BgL_az00_5451
																																										=
																																										BgL_carzd22511zd2_567;
																																									BgL_bz00_5452
																																										=
																																										BgL_arg1775z00_583;
																																									BgL_bz00_65
																																										=
																																										BgL_bz00_5452;
																																									BgL_az00_64
																																										=
																																										BgL_az00_5451;
																																									BgL_idz00_63
																																										=
																																										BgL_idz00_5450;
																																									goto
																																										BgL_tagzd2107zd2_66;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_cdrzd22692zd2_611;
																																				BgL_cdrzd22692zd2_611
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				{	/* Expand/garith.scm 49 */
																																					obj_t
																																						BgL_cdrzd22699zd2_612;
																																					BgL_cdrzd22699zd2_612
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd22692zd2_611));
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_carzd22703zd2_613;
																																						BgL_carzd22703zd2_613
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd22699zd2_612));
																																						if (SYMBOLP(BgL_carzd22703zd2_613))
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd22699zd2_612))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1844z00_617;
																																										obj_t
																																											BgL_arg1845z00_618;
																																										BgL_arg1844z00_617
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg1845z00_618
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd22692zd2_611));
																																										{
																																											obj_t
																																												BgL_bz00_5471;
																																											obj_t
																																												BgL_az00_5470;
																																											obj_t
																																												BgL_idz00_5469;
																																											BgL_idz00_5469
																																												=
																																												BgL_arg1844z00_617;
																																											BgL_az00_5470
																																												=
																																												BgL_arg1845z00_618;
																																											BgL_bz00_5471
																																												=
																																												BgL_carzd22703zd2_613;
																																											BgL_bz00_69
																																												=
																																												BgL_bz00_5471;
																																											BgL_az00_68
																																												=
																																												BgL_az00_5470;
																																											BgL_idz00_67
																																												=
																																												BgL_idz00_5469;
																																											goto
																																												BgL_tagzd2108zd2_70;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd22699zd2_612))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1849z00_624;
																																										obj_t
																																											BgL_arg1850z00_625;
																																										obj_t
																																											BgL_arg1851z00_626;
																																										BgL_arg1849z00_624
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg1850z00_625
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd22692zd2_611));
																																										BgL_arg1851z00_626
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd22699zd2_612));
																																										{
																																											obj_t
																																												BgL_bz00_5484;
																																											obj_t
																																												BgL_az00_5483;
																																											obj_t
																																												BgL_idz00_5482;
																																											BgL_idz00_5482
																																												=
																																												BgL_arg1849z00_624;
																																											BgL_az00_5483
																																												=
																																												BgL_arg1850z00_625;
																																											BgL_bz00_5484
																																												=
																																												BgL_arg1851z00_626;
																																											BgL_bz00_73
																																												=
																																												BgL_bz00_5484;
																																											BgL_az00_72
																																												=
																																												BgL_az00_5483;
																																											BgL_idz00_71
																																												=
																																												BgL_idz00_5482;
																																											goto
																																												BgL_tagzd2109zd2_74;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																					}
																																				}
																																			}
																																	}
																																}
																														}
																												}
																											}
																										}
																								}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							if (SYMBOLP
																								(BgL_carzd21873zd2_449))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd22760zd2_632;

																									BgL_carzd22760zd2_632 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd21874zd2_450));
																									if (INTEGERP
																										(BgL_carzd22760zd2_632))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd21874zd2_450))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg1858z00_636;
																													BgL_arg1858z00_636 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													{
																														obj_t BgL_bz00_5499;
																														obj_t BgL_az00_5498;
																														obj_t
																															BgL_idz00_5497;
																														BgL_idz00_5497 =
																															BgL_arg1858z00_636;
																														BgL_az00_5498 =
																															BgL_carzd21873zd2_449;
																														BgL_bz00_5499 =
																															BgL_carzd22760zd2_632;
																														BgL_bz00_49 =
																															BgL_bz00_5499;
																														BgL_az00_48 =
																															BgL_az00_5498;
																														BgL_idz00_47 =
																															BgL_idz00_5497;
																														goto
																															BgL_tagzd2103zd2_50;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_cdrzd22821zd2_638;
																											BgL_cdrzd22821zd2_638 =
																												CDR(((obj_t)
																													BgL_xz00_4));
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_cdrzd22828zd2_639;
																												BgL_cdrzd22828zd2_639 =
																													CDR(((obj_t)
																														BgL_cdrzd22821zd2_638));
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_carzd22832zd2_640;
																													BgL_carzd22832zd2_640
																														=
																														CAR(((obj_t)
																															BgL_cdrzd22828zd2_639));
																													if (REALP
																														(BgL_carzd22832zd2_640))
																														{	/* Expand/garith.scm 49 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd22828zd2_639))))
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_arg1864z00_644;
																																	obj_t
																																		BgL_arg1866z00_645;
																																	BgL_arg1864z00_644
																																		=
																																		CAR(((obj_t)
																																			BgL_xz00_4));
																																	BgL_arg1866z00_645
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd22821zd2_638));
																																	{
																																		double
																																			BgL_bz00_5518;
																																		obj_t
																																			BgL_az00_5517;
																																		obj_t
																																			BgL_idz00_5516;
																																		BgL_idz00_5516
																																			=
																																			BgL_arg1864z00_644;
																																		BgL_az00_5517
																																			=
																																			BgL_arg1866z00_645;
																																		BgL_bz00_5518
																																			=
																																			REAL_TO_DOUBLE
																																			(BgL_carzd22832zd2_640);
																																		BgL_bz00_53
																																			=
																																			BgL_bz00_5518;
																																		BgL_az00_52
																																			=
																																			BgL_az00_5517;
																																		BgL_idz00_51
																																			=
																																			BgL_idz00_5516;
																																		goto
																																			BgL_tagzd2104zd2_54;
																																	}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_carzd22891zd2_648;
																															BgL_carzd22891zd2_648
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd22821zd2_638));
																															if (REALP
																																(BgL_carzd22891zd2_648))
																																{	/* Expand/garith.scm 49 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd22828zd2_639))))
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_arg1872z00_653;
																																			obj_t
																																				BgL_arg1873z00_654;
																																			BgL_arg1872z00_653
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			BgL_arg1873z00_654
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd22828zd2_639));
																																			{
																																				obj_t
																																					BgL_bz00_5535;
																																				double
																																					BgL_az00_5533;
																																				obj_t
																																					BgL_idz00_5532;
																																				BgL_idz00_5532
																																					=
																																					BgL_arg1872z00_653;
																																				BgL_az00_5533
																																					=
																																					REAL_TO_DOUBLE
																																					(BgL_carzd22891zd2_648);
																																				BgL_bz00_5535
																																					=
																																					BgL_arg1873z00_654;
																																				BgL_bz00_57
																																					=
																																					BgL_bz00_5535;
																																				BgL_az00_56
																																					=
																																					BgL_az00_5533;
																																				BgL_idz00_55
																																					=
																																					BgL_idz00_5532;
																																				goto
																																					BgL_tagzd2105zd2_58;
																																			}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_cdrzd22942zd2_656;
																																	BgL_cdrzd22942zd2_656
																																		=
																																		CDR(((obj_t)
																																			BgL_xz00_4));
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd22948zd2_657;
																																		obj_t
																																			BgL_cdrzd22949zd2_658;
																																		BgL_carzd22948zd2_657
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd22942zd2_656));
																																		BgL_cdrzd22949zd2_658
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd22942zd2_656));
																																		if (SYMBOLP
																																			(BgL_carzd22948zd2_657))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd22955zd2_660;
																																				BgL_carzd22955zd2_660
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd22949zd2_658));
																																				if (SYMBOLP(BgL_carzd22955zd2_660))
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd22949zd2_658))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg1879z00_664;
																																								BgL_arg1879z00_664
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								{
																																									obj_t
																																										BgL_bz00_5556;
																																									obj_t
																																										BgL_az00_5555;
																																									obj_t
																																										BgL_idz00_5554;
																																									BgL_idz00_5554
																																										=
																																										BgL_arg1879z00_664;
																																									BgL_az00_5555
																																										=
																																										BgL_carzd22948zd2_657;
																																									BgL_bz00_5556
																																										=
																																										BgL_carzd22955zd2_660;
																																									BgL_bz00_61
																																										=
																																										BgL_bz00_5556;
																																									BgL_az00_60
																																										=
																																										BgL_az00_5555;
																																									BgL_idz00_59
																																										=
																																										BgL_idz00_5554;
																																									goto
																																										BgL_tagzd2106zd2_62;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd22949zd2_658))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg1884z00_672;
																																								obj_t
																																									BgL_arg1885z00_673;
																																								BgL_arg1884z00_672
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg1885z00_673
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd22949zd2_658));
																																								{
																																									obj_t
																																										BgL_bz00_5567;
																																									obj_t
																																										BgL_az00_5566;
																																									obj_t
																																										BgL_idz00_5565;
																																									BgL_idz00_5565
																																										=
																																										BgL_arg1884z00_672;
																																									BgL_az00_5566
																																										=
																																										BgL_carzd22948zd2_657;
																																									BgL_bz00_5567
																																										=
																																										BgL_arg1885z00_673;
																																									BgL_bz00_65
																																										=
																																										BgL_bz00_5567;
																																									BgL_az00_64
																																										=
																																										BgL_az00_5566;
																																									BgL_idz00_63
																																										=
																																										BgL_idz00_5565;
																																									goto
																																										BgL_tagzd2107zd2_66;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_cdrzd23129zd2_701;
																																				BgL_cdrzd23129zd2_701
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				{	/* Expand/garith.scm 49 */
																																					obj_t
																																						BgL_cdrzd23136zd2_702;
																																					BgL_cdrzd23136zd2_702
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd23129zd2_701));
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_carzd23140zd2_703;
																																						BgL_carzd23140zd2_703
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd23136zd2_702));
																																						if (SYMBOLP(BgL_carzd23140zd2_703))
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd23136zd2_702))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1912z00_707;
																																										obj_t
																																											BgL_arg1913z00_708;
																																										BgL_arg1912z00_707
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg1913z00_708
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd23129zd2_701));
																																										{
																																											obj_t
																																												BgL_bz00_5586;
																																											obj_t
																																												BgL_az00_5585;
																																											obj_t
																																												BgL_idz00_5584;
																																											BgL_idz00_5584
																																												=
																																												BgL_arg1912z00_707;
																																											BgL_az00_5585
																																												=
																																												BgL_arg1913z00_708;
																																											BgL_bz00_5586
																																												=
																																												BgL_carzd23140zd2_703;
																																											BgL_bz00_69
																																												=
																																												BgL_bz00_5586;
																																											BgL_az00_68
																																												=
																																												BgL_az00_5585;
																																											BgL_idz00_67
																																												=
																																												BgL_idz00_5584;
																																											goto
																																												BgL_tagzd2108zd2_70;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd23136zd2_702))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg1918z00_714;
																																										obj_t
																																											BgL_arg1919z00_715;
																																										obj_t
																																											BgL_arg1920z00_716;
																																										BgL_arg1918z00_714
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg1919z00_715
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd23129zd2_701));
																																										BgL_arg1920z00_716
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd23136zd2_702));
																																										{
																																											obj_t
																																												BgL_bz00_5599;
																																											obj_t
																																												BgL_az00_5598;
																																											obj_t
																																												BgL_idz00_5597;
																																											BgL_idz00_5597
																																												=
																																												BgL_arg1918z00_714;
																																											BgL_az00_5598
																																												=
																																												BgL_arg1919z00_715;
																																											BgL_bz00_5599
																																												=
																																												BgL_arg1920z00_716;
																																											BgL_bz00_73
																																												=
																																												BgL_bz00_5599;
																																											BgL_az00_72
																																												=
																																												BgL_az00_5598;
																																											BgL_idz00_71
																																												=
																																												BgL_idz00_5597;
																																											goto
																																												BgL_tagzd2109zd2_74;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																					}
																																				}
																																			}
																																	}
																																}
																														}
																												}
																											}
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_cdrzd23184zd2_718;

																									BgL_cdrzd23184zd2_718 =
																										CDR(((obj_t) BgL_xz00_4));
																									{	/* Expand/garith.scm 49 */
																										obj_t BgL_cdrzd23191zd2_719;

																										BgL_cdrzd23191zd2_719 =
																											CDR(
																											((obj_t)
																												BgL_cdrzd23184zd2_718));
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd23195zd2_720;
																											BgL_carzd23195zd2_720 =
																												CAR(((obj_t)
																													BgL_cdrzd23191zd2_719));
																											if (REALP
																												(BgL_carzd23195zd2_720))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd23191zd2_719))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg1927z00_724;
																															obj_t
																																BgL_arg1928z00_725;
																															BgL_arg1927z00_724
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg1928z00_725
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd23184zd2_718));
																															{
																																double
																																	BgL_bz00_5618;
																																obj_t
																																	BgL_az00_5617;
																																obj_t
																																	BgL_idz00_5616;
																																BgL_idz00_5616 =
																																	BgL_arg1927z00_724;
																																BgL_az00_5617 =
																																	BgL_arg1928z00_725;
																																BgL_bz00_5618 =
																																	REAL_TO_DOUBLE
																																	(BgL_carzd23195zd2_720);
																																BgL_bz00_53 =
																																	BgL_bz00_5618;
																																BgL_az00_52 =
																																	BgL_az00_5617;
																																BgL_idz00_51 =
																																	BgL_idz00_5616;
																																goto
																																	BgL_tagzd2104zd2_54;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_carzd23254zd2_728;
																													BgL_carzd23254zd2_728
																														=
																														CAR(((obj_t)
																															BgL_cdrzd23184zd2_718));
																													if (REALP
																														(BgL_carzd23254zd2_728))
																														{	/* Expand/garith.scm 49 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd23191zd2_719))))
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_arg1933z00_733;
																																	obj_t
																																		BgL_arg1934z00_734;
																																	BgL_arg1933z00_733
																																		=
																																		CAR(((obj_t)
																																			BgL_xz00_4));
																																	BgL_arg1934z00_734
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd23191zd2_719));
																																	{
																																		obj_t
																																			BgL_bz00_5635;
																																		double
																																			BgL_az00_5633;
																																		obj_t
																																			BgL_idz00_5632;
																																		BgL_idz00_5632
																																			=
																																			BgL_arg1933z00_733;
																																		BgL_az00_5633
																																			=
																																			REAL_TO_DOUBLE
																																			(BgL_carzd23254zd2_728);
																																		BgL_bz00_5635
																																			=
																																			BgL_arg1934z00_734;
																																		BgL_bz00_57
																																			=
																																			BgL_bz00_5635;
																																		BgL_az00_56
																																			=
																																			BgL_az00_5633;
																																		BgL_idz00_55
																																			=
																																			BgL_idz00_5632;
																																		goto
																																			BgL_tagzd2105zd2_58;
																																	}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_cdrzd23305zd2_736;
																															BgL_cdrzd23305zd2_736
																																=
																																CDR(((obj_t)
																																	BgL_xz00_4));
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd23311zd2_737;
																																obj_t
																																	BgL_cdrzd23312zd2_738;
																																BgL_carzd23311zd2_737
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd23305zd2_736));
																																BgL_cdrzd23312zd2_738
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd23305zd2_736));
																																if (SYMBOLP
																																	(BgL_carzd23311zd2_737))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd23318zd2_740;
																																		BgL_carzd23318zd2_740
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd23312zd2_738));
																																		if (SYMBOLP
																																			(BgL_carzd23318zd2_740))
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd23312zd2_738))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg1940z00_744;
																																						BgL_arg1940z00_744
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						{
																																							obj_t
																																								BgL_bz00_5656;
																																							obj_t
																																								BgL_az00_5655;
																																							obj_t
																																								BgL_idz00_5654;
																																							BgL_idz00_5654
																																								=
																																								BgL_arg1940z00_744;
																																							BgL_az00_5655
																																								=
																																								BgL_carzd23311zd2_737;
																																							BgL_bz00_5656
																																								=
																																								BgL_carzd23318zd2_740;
																																							BgL_bz00_61
																																								=
																																								BgL_bz00_5656;
																																							BgL_az00_60
																																								=
																																								BgL_az00_5655;
																																							BgL_idz00_59
																																								=
																																								BgL_idz00_5654;
																																							goto
																																								BgL_tagzd2106zd2_62;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd23312zd2_738))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg1945z00_752;
																																						obj_t
																																							BgL_arg1946z00_753;
																																						BgL_arg1945z00_752
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg1946z00_753
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd23312zd2_738));
																																						{
																																							obj_t
																																								BgL_bz00_5667;
																																							obj_t
																																								BgL_az00_5666;
																																							obj_t
																																								BgL_idz00_5665;
																																							BgL_idz00_5665
																																								=
																																								BgL_arg1945z00_752;
																																							BgL_az00_5666
																																								=
																																								BgL_carzd23311zd2_737;
																																							BgL_bz00_5667
																																								=
																																								BgL_arg1946z00_753;
																																							BgL_bz00_65
																																								=
																																								BgL_bz00_5667;
																																							BgL_az00_64
																																								=
																																								BgL_az00_5666;
																																							BgL_idz00_63
																																								=
																																								BgL_idz00_5665;
																																							goto
																																								BgL_tagzd2107zd2_66;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_cdrzd23492zd2_781;
																																		BgL_cdrzd23492zd2_781
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_cdrzd23499zd2_782;
																																			BgL_cdrzd23499zd2_782
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd23492zd2_781));
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd23503zd2_783;
																																				BgL_carzd23503zd2_783
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd23499zd2_782));
																																				if (SYMBOLP(BgL_carzd23503zd2_783))
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd23499zd2_782))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg1969z00_787;
																																								obj_t
																																									BgL_arg1970z00_788;
																																								BgL_arg1969z00_787
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg1970z00_788
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd23492zd2_781));
																																								{
																																									obj_t
																																										BgL_bz00_5686;
																																									obj_t
																																										BgL_az00_5685;
																																									obj_t
																																										BgL_idz00_5684;
																																									BgL_idz00_5684
																																										=
																																										BgL_arg1969z00_787;
																																									BgL_az00_5685
																																										=
																																										BgL_arg1970z00_788;
																																									BgL_bz00_5686
																																										=
																																										BgL_carzd23503zd2_783;
																																									BgL_bz00_69
																																										=
																																										BgL_bz00_5686;
																																									BgL_az00_68
																																										=
																																										BgL_az00_5685;
																																									BgL_idz00_67
																																										=
																																										BgL_idz00_5684;
																																									goto
																																										BgL_tagzd2108zd2_70;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd23499zd2_782))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg1974z00_794;
																																								obj_t
																																									BgL_arg1975z00_795;
																																								obj_t
																																									BgL_arg1976z00_796;
																																								BgL_arg1974z00_794
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg1975z00_795
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd23492zd2_781));
																																								BgL_arg1976z00_796
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd23499zd2_782));
																																								{
																																									obj_t
																																										BgL_bz00_5699;
																																									obj_t
																																										BgL_az00_5698;
																																									obj_t
																																										BgL_idz00_5697;
																																									BgL_idz00_5697
																																										=
																																										BgL_arg1974z00_794;
																																									BgL_az00_5698
																																										=
																																										BgL_arg1975z00_795;
																																									BgL_bz00_5699
																																										=
																																										BgL_arg1976z00_796;
																																									BgL_bz00_73
																																										=
																																										BgL_bz00_5699;
																																									BgL_az00_72
																																										=
																																										BgL_az00_5698;
																																									BgL_idz00_71
																																										=
																																										BgL_idz00_5697;
																																									goto
																																										BgL_tagzd2109zd2_74;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																			}
																																		}
																																	}
																															}
																														}
																												}
																										}
																									}
																								}
																						}
																				}
																			}
																	}
																else
																	{	/* Expand/garith.scm 49 */
																		obj_t BgL_cdrzd23546zd2_799;

																		BgL_cdrzd23546zd2_799 =
																			CDR(((obj_t) BgL_xz00_4));
																		{	/* Expand/garith.scm 49 */
																			obj_t BgL_carzd23551zd2_800;
																			obj_t BgL_cdrzd23552zd2_801;

																			BgL_carzd23551zd2_800 =
																				CAR(((obj_t) BgL_cdrzd23546zd2_799));
																			BgL_cdrzd23552zd2_801 =
																				CDR(((obj_t) BgL_cdrzd23546zd2_799));
																			if (INTEGERP(BgL_carzd23551zd2_800))
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_carzd23556zd2_803;

																					BgL_carzd23556zd2_803 =
																						CAR(
																						((obj_t) BgL_cdrzd23552zd2_801));
																					if (SYMBOLP(BgL_carzd23556zd2_803))
																						{	/* Expand/garith.scm 49 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd23552zd2_801))))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_arg1983z00_807;

																									BgL_arg1983z00_807 =
																										CAR(((obj_t) BgL_xz00_4));
																									{
																										obj_t BgL_bz00_5720;
																										obj_t BgL_az00_5719;
																										obj_t BgL_idz00_5718;

																										BgL_idz00_5718 =
																											BgL_arg1983z00_807;
																										BgL_az00_5719 =
																											BgL_carzd23551zd2_800;
																										BgL_bz00_5720 =
																											BgL_carzd23556zd2_803;
																										BgL_bz00_45 = BgL_bz00_5720;
																										BgL_az00_44 = BgL_az00_5719;
																										BgL_idz00_43 =
																											BgL_idz00_5718;
																										goto BgL_tagzd2102zd2_46;
																									}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							if (SYMBOLP
																								(BgL_carzd23551zd2_800))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd23638zd2_813;

																									BgL_carzd23638zd2_813 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd23552zd2_801));
																									if (INTEGERP
																										(BgL_carzd23638zd2_813))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd23552zd2_801))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg1989z00_817;
																													BgL_arg1989z00_817 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													{
																														obj_t BgL_bz00_5735;
																														obj_t BgL_az00_5734;
																														obj_t
																															BgL_idz00_5733;
																														BgL_idz00_5733 =
																															BgL_arg1989z00_817;
																														BgL_az00_5734 =
																															BgL_carzd23551zd2_800;
																														BgL_bz00_5735 =
																															BgL_carzd23638zd2_813;
																														BgL_bz00_49 =
																															BgL_bz00_5735;
																														BgL_az00_48 =
																															BgL_az00_5734;
																														BgL_idz00_47 =
																															BgL_idz00_5733;
																														goto
																															BgL_tagzd2103zd2_50;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_cdrzd23699zd2_819;
																											BgL_cdrzd23699zd2_819 =
																												CDR(((obj_t)
																													BgL_xz00_4));
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_cdrzd23706zd2_820;
																												BgL_cdrzd23706zd2_820 =
																													CDR(((obj_t)
																														BgL_cdrzd23699zd2_819));
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_carzd23710zd2_821;
																													BgL_carzd23710zd2_821
																														=
																														CAR(((obj_t)
																															BgL_cdrzd23706zd2_820));
																													if (REALP
																														(BgL_carzd23710zd2_821))
																														{	/* Expand/garith.scm 49 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd23706zd2_820))))
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_arg1994z00_825;
																																	obj_t
																																		BgL_arg1995z00_826;
																																	BgL_arg1994z00_825
																																		=
																																		CAR(((obj_t)
																																			BgL_xz00_4));
																																	BgL_arg1995z00_826
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd23699zd2_819));
																																	{
																																		double
																																			BgL_bz00_5754;
																																		obj_t
																																			BgL_az00_5753;
																																		obj_t
																																			BgL_idz00_5752;
																																		BgL_idz00_5752
																																			=
																																			BgL_arg1994z00_825;
																																		BgL_az00_5753
																																			=
																																			BgL_arg1995z00_826;
																																		BgL_bz00_5754
																																			=
																																			REAL_TO_DOUBLE
																																			(BgL_carzd23710zd2_821);
																																		BgL_bz00_53
																																			=
																																			BgL_bz00_5754;
																																		BgL_az00_52
																																			=
																																			BgL_az00_5753;
																																		BgL_idz00_51
																																			=
																																			BgL_idz00_5752;
																																		goto
																																			BgL_tagzd2104zd2_54;
																																	}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_carzd23769zd2_829;
																															BgL_carzd23769zd2_829
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd23699zd2_819));
																															if (REALP
																																(BgL_carzd23769zd2_829))
																																{	/* Expand/garith.scm 49 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd23706zd2_820))))
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_arg2000z00_834;
																																			obj_t
																																				BgL_arg2001z00_835;
																																			BgL_arg2000z00_834
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_xz00_4));
																																			BgL_arg2001z00_835
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd23706zd2_820));
																																			{
																																				obj_t
																																					BgL_bz00_5771;
																																				double
																																					BgL_az00_5769;
																																				obj_t
																																					BgL_idz00_5768;
																																				BgL_idz00_5768
																																					=
																																					BgL_arg2000z00_834;
																																				BgL_az00_5769
																																					=
																																					REAL_TO_DOUBLE
																																					(BgL_carzd23769zd2_829);
																																				BgL_bz00_5771
																																					=
																																					BgL_arg2001z00_835;
																																				BgL_bz00_57
																																					=
																																					BgL_bz00_5771;
																																				BgL_az00_56
																																					=
																																					BgL_az00_5769;
																																				BgL_idz00_55
																																					=
																																					BgL_idz00_5768;
																																				goto
																																					BgL_tagzd2105zd2_58;
																																			}
																																		}
																																	else
																																		{	/* Expand/garith.scm 49 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_cdrzd23820zd2_837;
																																	BgL_cdrzd23820zd2_837
																																		=
																																		CDR(((obj_t)
																																			BgL_xz00_4));
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd23826zd2_838;
																																		obj_t
																																			BgL_cdrzd23827zd2_839;
																																		BgL_carzd23826zd2_838
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd23820zd2_837));
																																		BgL_cdrzd23827zd2_839
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd23820zd2_837));
																																		if (SYMBOLP
																																			(BgL_carzd23826zd2_838))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd23833zd2_841;
																																				BgL_carzd23833zd2_841
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd23827zd2_839));
																																				if (SYMBOLP(BgL_carzd23833zd2_841))
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd23827zd2_839))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg2008z00_845;
																																								BgL_arg2008z00_845
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								{
																																									obj_t
																																										BgL_bz00_5792;
																																									obj_t
																																										BgL_az00_5791;
																																									obj_t
																																										BgL_idz00_5790;
																																									BgL_idz00_5790
																																										=
																																										BgL_arg2008z00_845;
																																									BgL_az00_5791
																																										=
																																										BgL_carzd23826zd2_838;
																																									BgL_bz00_5792
																																										=
																																										BgL_carzd23833zd2_841;
																																									BgL_bz00_61
																																										=
																																										BgL_bz00_5792;
																																									BgL_az00_60
																																										=
																																										BgL_az00_5791;
																																									BgL_idz00_59
																																										=
																																										BgL_idz00_5790;
																																									goto
																																										BgL_tagzd2106zd2_62;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd23827zd2_839))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg2013z00_853;
																																								obj_t
																																									BgL_arg2014z00_854;
																																								BgL_arg2013z00_853
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg2014z00_854
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd23827zd2_839));
																																								{
																																									obj_t
																																										BgL_bz00_5803;
																																									obj_t
																																										BgL_az00_5802;
																																									obj_t
																																										BgL_idz00_5801;
																																									BgL_idz00_5801
																																										=
																																										BgL_arg2013z00_853;
																																									BgL_az00_5802
																																										=
																																										BgL_carzd23826zd2_838;
																																									BgL_bz00_5803
																																										=
																																										BgL_arg2014z00_854;
																																									BgL_bz00_65
																																										=
																																										BgL_bz00_5803;
																																									BgL_az00_64
																																										=
																																										BgL_az00_5802;
																																									BgL_idz00_63
																																										=
																																										BgL_idz00_5801;
																																									goto
																																										BgL_tagzd2107zd2_66;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_cdrzd24007zd2_882;
																																				BgL_cdrzd24007zd2_882
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				{	/* Expand/garith.scm 49 */
																																					obj_t
																																						BgL_cdrzd24014zd2_883;
																																					BgL_cdrzd24014zd2_883
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd24007zd2_882));
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_carzd24018zd2_884;
																																						BgL_carzd24018zd2_884
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd24014zd2_883));
																																						if (SYMBOLP(BgL_carzd24018zd2_884))
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd24014zd2_883))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg2038z00_888;
																																										obj_t
																																											BgL_arg2039z00_889;
																																										BgL_arg2038z00_888
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg2039z00_889
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd24007zd2_882));
																																										{
																																											obj_t
																																												BgL_bz00_5822;
																																											obj_t
																																												BgL_az00_5821;
																																											obj_t
																																												BgL_idz00_5820;
																																											BgL_idz00_5820
																																												=
																																												BgL_arg2038z00_888;
																																											BgL_az00_5821
																																												=
																																												BgL_arg2039z00_889;
																																											BgL_bz00_5822
																																												=
																																												BgL_carzd24018zd2_884;
																																											BgL_bz00_69
																																												=
																																												BgL_bz00_5822;
																																											BgL_az00_68
																																												=
																																												BgL_az00_5821;
																																											BgL_idz00_67
																																												=
																																												BgL_idz00_5820;
																																											goto
																																												BgL_tagzd2108zd2_70;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd24014zd2_883))))
																																									{	/* Expand/garith.scm 49 */
																																										obj_t
																																											BgL_arg2044z00_895;
																																										obj_t
																																											BgL_arg2045z00_896;
																																										obj_t
																																											BgL_arg2046z00_897;
																																										BgL_arg2044z00_895
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_xz00_4));
																																										BgL_arg2045z00_896
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd24007zd2_882));
																																										BgL_arg2046z00_897
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd24014zd2_883));
																																										{
																																											obj_t
																																												BgL_bz00_5835;
																																											obj_t
																																												BgL_az00_5834;
																																											obj_t
																																												BgL_idz00_5833;
																																											BgL_idz00_5833
																																												=
																																												BgL_arg2044z00_895;
																																											BgL_az00_5834
																																												=
																																												BgL_arg2045z00_896;
																																											BgL_bz00_5835
																																												=
																																												BgL_arg2046z00_897;
																																											BgL_bz00_73
																																												=
																																												BgL_bz00_5835;
																																											BgL_az00_72
																																												=
																																												BgL_az00_5834;
																																											BgL_idz00_71
																																												=
																																												BgL_idz00_5833;
																																											goto
																																												BgL_tagzd2109zd2_74;
																																										}
																																									}
																																								else
																																									{	/* Expand/garith.scm 49 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																					}
																																				}
																																			}
																																	}
																																}
																														}
																												}
																											}
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_cdrzd24062zd2_899;

																									BgL_cdrzd24062zd2_899 =
																										CDR(((obj_t) BgL_xz00_4));
																									{	/* Expand/garith.scm 49 */
																										obj_t BgL_cdrzd24069zd2_900;

																										BgL_cdrzd24069zd2_900 =
																											CDR(
																											((obj_t)
																												BgL_cdrzd24062zd2_899));
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd24073zd2_901;
																											BgL_carzd24073zd2_901 =
																												CAR(((obj_t)
																													BgL_cdrzd24069zd2_900));
																											if (REALP
																												(BgL_carzd24073zd2_901))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd24069zd2_900))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg2051z00_905;
																															obj_t
																																BgL_arg2052z00_906;
																															BgL_arg2051z00_905
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg2052z00_906
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd24062zd2_899));
																															{
																																double
																																	BgL_bz00_5854;
																																obj_t
																																	BgL_az00_5853;
																																obj_t
																																	BgL_idz00_5852;
																																BgL_idz00_5852 =
																																	BgL_arg2051z00_905;
																																BgL_az00_5853 =
																																	BgL_arg2052z00_906;
																																BgL_bz00_5854 =
																																	REAL_TO_DOUBLE
																																	(BgL_carzd24073zd2_901);
																																BgL_bz00_53 =
																																	BgL_bz00_5854;
																																BgL_az00_52 =
																																	BgL_az00_5853;
																																BgL_idz00_51 =
																																	BgL_idz00_5852;
																																goto
																																	BgL_tagzd2104zd2_54;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_carzd24132zd2_909;
																													BgL_carzd24132zd2_909
																														=
																														CAR(((obj_t)
																															BgL_cdrzd24062zd2_899));
																													if (REALP
																														(BgL_carzd24132zd2_909))
																														{	/* Expand/garith.scm 49 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd24069zd2_900))))
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_arg2059z00_914;
																																	obj_t
																																		BgL_arg2060z00_915;
																																	BgL_arg2059z00_914
																																		=
																																		CAR(((obj_t)
																																			BgL_xz00_4));
																																	BgL_arg2060z00_915
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd24069zd2_900));
																																	{
																																		obj_t
																																			BgL_bz00_5871;
																																		double
																																			BgL_az00_5869;
																																		obj_t
																																			BgL_idz00_5868;
																																		BgL_idz00_5868
																																			=
																																			BgL_arg2059z00_914;
																																		BgL_az00_5869
																																			=
																																			REAL_TO_DOUBLE
																																			(BgL_carzd24132zd2_909);
																																		BgL_bz00_5871
																																			=
																																			BgL_arg2060z00_915;
																																		BgL_bz00_57
																																			=
																																			BgL_bz00_5871;
																																		BgL_az00_56
																																			=
																																			BgL_az00_5869;
																																		BgL_idz00_55
																																			=
																																			BgL_idz00_5868;
																																		goto
																																			BgL_tagzd2105zd2_58;
																																	}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_cdrzd24183zd2_917;
																															BgL_cdrzd24183zd2_917
																																=
																																CDR(((obj_t)
																																	BgL_xz00_4));
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd24189zd2_918;
																																obj_t
																																	BgL_cdrzd24190zd2_919;
																																BgL_carzd24189zd2_918
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd24183zd2_917));
																																BgL_cdrzd24190zd2_919
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd24183zd2_917));
																																if (SYMBOLP
																																	(BgL_carzd24189zd2_918))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd24196zd2_921;
																																		BgL_carzd24196zd2_921
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd24190zd2_919));
																																		if (SYMBOLP
																																			(BgL_carzd24196zd2_921))
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd24190zd2_919))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2067z00_925;
																																						BgL_arg2067z00_925
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						{
																																							obj_t
																																								BgL_bz00_5892;
																																							obj_t
																																								BgL_az00_5891;
																																							obj_t
																																								BgL_idz00_5890;
																																							BgL_idz00_5890
																																								=
																																								BgL_arg2067z00_925;
																																							BgL_az00_5891
																																								=
																																								BgL_carzd24189zd2_918;
																																							BgL_bz00_5892
																																								=
																																								BgL_carzd24196zd2_921;
																																							BgL_bz00_61
																																								=
																																								BgL_bz00_5892;
																																							BgL_az00_60
																																								=
																																								BgL_az00_5891;
																																							BgL_idz00_59
																																								=
																																								BgL_idz00_5890;
																																							goto
																																								BgL_tagzd2106zd2_62;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd24190zd2_919))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2072z00_933;
																																						obj_t
																																							BgL_arg2074z00_934;
																																						BgL_arg2072z00_933
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2074z00_934
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd24190zd2_919));
																																						{
																																							obj_t
																																								BgL_bz00_5903;
																																							obj_t
																																								BgL_az00_5902;
																																							obj_t
																																								BgL_idz00_5901;
																																							BgL_idz00_5901
																																								=
																																								BgL_arg2072z00_933;
																																							BgL_az00_5902
																																								=
																																								BgL_carzd24189zd2_918;
																																							BgL_bz00_5903
																																								=
																																								BgL_arg2074z00_934;
																																							BgL_bz00_65
																																								=
																																								BgL_bz00_5903;
																																							BgL_az00_64
																																								=
																																								BgL_az00_5902;
																																							BgL_idz00_63
																																								=
																																								BgL_idz00_5901;
																																							goto
																																								BgL_tagzd2107zd2_66;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_cdrzd24370zd2_962;
																																		BgL_cdrzd24370zd2_962
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_cdrzd24377zd2_963;
																																			BgL_cdrzd24377zd2_963
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd24370zd2_962));
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd24381zd2_964;
																																				BgL_carzd24381zd2_964
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd24377zd2_963));
																																				if (SYMBOLP(BgL_carzd24381zd2_964))
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd24377zd2_963))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg2099z00_968;
																																								obj_t
																																									BgL_arg2100z00_969;
																																								BgL_arg2099z00_968
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg2100z00_969
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd24370zd2_962));
																																								{
																																									obj_t
																																										BgL_bz00_5922;
																																									obj_t
																																										BgL_az00_5921;
																																									obj_t
																																										BgL_idz00_5920;
																																									BgL_idz00_5920
																																										=
																																										BgL_arg2099z00_968;
																																									BgL_az00_5921
																																										=
																																										BgL_arg2100z00_969;
																																									BgL_bz00_5922
																																										=
																																										BgL_carzd24381zd2_964;
																																									BgL_bz00_69
																																										=
																																										BgL_bz00_5922;
																																									BgL_az00_68
																																										=
																																										BgL_az00_5921;
																																									BgL_idz00_67
																																										=
																																										BgL_idz00_5920;
																																									goto
																																										BgL_tagzd2108zd2_70;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd24377zd2_963))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg2104z00_975;
																																								obj_t
																																									BgL_arg2105z00_976;
																																								obj_t
																																									BgL_arg2106z00_977;
																																								BgL_arg2104z00_975
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg2105z00_976
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd24370zd2_962));
																																								BgL_arg2106z00_977
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd24377zd2_963));
																																								{
																																									obj_t
																																										BgL_bz00_5935;
																																									obj_t
																																										BgL_az00_5934;
																																									obj_t
																																										BgL_idz00_5933;
																																									BgL_idz00_5933
																																										=
																																										BgL_arg2104z00_975;
																																									BgL_az00_5934
																																										=
																																										BgL_arg2105z00_976;
																																									BgL_bz00_5935
																																										=
																																										BgL_arg2106z00_977;
																																									BgL_bz00_73
																																										=
																																										BgL_bz00_5935;
																																									BgL_az00_72
																																										=
																																										BgL_az00_5934;
																																									BgL_idz00_71
																																										=
																																										BgL_idz00_5933;
																																									goto
																																										BgL_tagzd2109zd2_74;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																			}
																																		}
																																	}
																															}
																														}
																												}
																										}
																									}
																								}
																						}
																				}
																			else
																				{	/* Expand/garith.scm 49 */
																					if (SYMBOLP(BgL_carzd23551zd2_800))
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd24438zd2_983;

																							BgL_carzd24438zd2_983 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd23552zd2_801));
																							if (INTEGERP
																								(BgL_carzd24438zd2_983))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd23552zd2_801))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg2112z00_987;

																											BgL_arg2112z00_987 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											{
																												obj_t BgL_bz00_5950;
																												obj_t BgL_az00_5949;
																												obj_t BgL_idz00_5948;

																												BgL_idz00_5948 =
																													BgL_arg2112z00_987;
																												BgL_az00_5949 =
																													BgL_carzd23551zd2_800;
																												BgL_bz00_5950 =
																													BgL_carzd24438zd2_983;
																												BgL_bz00_49 =
																													BgL_bz00_5950;
																												BgL_az00_48 =
																													BgL_az00_5949;
																												BgL_idz00_47 =
																													BgL_idz00_5948;
																												goto
																													BgL_tagzd2103zd2_50;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_cdrzd24499zd2_989;

																									BgL_cdrzd24499zd2_989 =
																										CDR(((obj_t) BgL_xz00_4));
																									{	/* Expand/garith.scm 49 */
																										obj_t BgL_cdrzd24506zd2_990;

																										BgL_cdrzd24506zd2_990 =
																											CDR(
																											((obj_t)
																												BgL_cdrzd24499zd2_989));
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd24510zd2_991;
																											BgL_carzd24510zd2_991 =
																												CAR(((obj_t)
																													BgL_cdrzd24506zd2_990));
																											if (REALP
																												(BgL_carzd24510zd2_991))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd24506zd2_990))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg2117z00_995;
																															obj_t
																																BgL_arg2118z00_996;
																															BgL_arg2117z00_995
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg2118z00_996
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd24499zd2_989));
																															{
																																double
																																	BgL_bz00_5969;
																																obj_t
																																	BgL_az00_5968;
																																obj_t
																																	BgL_idz00_5967;
																																BgL_idz00_5967 =
																																	BgL_arg2117z00_995;
																																BgL_az00_5968 =
																																	BgL_arg2118z00_996;
																																BgL_bz00_5969 =
																																	REAL_TO_DOUBLE
																																	(BgL_carzd24510zd2_991);
																																BgL_bz00_53 =
																																	BgL_bz00_5969;
																																BgL_az00_52 =
																																	BgL_az00_5968;
																																BgL_idz00_51 =
																																	BgL_idz00_5967;
																																goto
																																	BgL_tagzd2104zd2_54;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_carzd24569zd2_999;
																													BgL_carzd24569zd2_999
																														=
																														CAR(((obj_t)
																															BgL_cdrzd24499zd2_989));
																													if (REALP
																														(BgL_carzd24569zd2_999))
																														{	/* Expand/garith.scm 49 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd24506zd2_990))))
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_arg2123z00_1004;
																																	obj_t
																																		BgL_arg2124z00_1005;
																																	BgL_arg2123z00_1004
																																		=
																																		CAR(((obj_t)
																																			BgL_xz00_4));
																																	BgL_arg2124z00_1005
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd24506zd2_990));
																																	{
																																		obj_t
																																			BgL_bz00_5986;
																																		double
																																			BgL_az00_5984;
																																		obj_t
																																			BgL_idz00_5983;
																																		BgL_idz00_5983
																																			=
																																			BgL_arg2123z00_1004;
																																		BgL_az00_5984
																																			=
																																			REAL_TO_DOUBLE
																																			(BgL_carzd24569zd2_999);
																																		BgL_bz00_5986
																																			=
																																			BgL_arg2124z00_1005;
																																		BgL_bz00_57
																																			=
																																			BgL_bz00_5986;
																																		BgL_az00_56
																																			=
																																			BgL_az00_5984;
																																		BgL_idz00_55
																																			=
																																			BgL_idz00_5983;
																																		goto
																																			BgL_tagzd2105zd2_58;
																																	}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_cdrzd24620zd2_1007;
																															BgL_cdrzd24620zd2_1007
																																=
																																CDR(((obj_t)
																																	BgL_xz00_4));
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd24626zd2_1008;
																																obj_t
																																	BgL_cdrzd24627zd2_1009;
																																BgL_carzd24626zd2_1008
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd24620zd2_1007));
																																BgL_cdrzd24627zd2_1009
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd24620zd2_1007));
																																if (SYMBOLP
																																	(BgL_carzd24626zd2_1008))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd24633zd2_1011;
																																		BgL_carzd24633zd2_1011
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd24627zd2_1009));
																																		if (SYMBOLP
																																			(BgL_carzd24633zd2_1011))
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd24627zd2_1009))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2131z00_1015;
																																						BgL_arg2131z00_1015
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						{
																																							obj_t
																																								BgL_bz00_6007;
																																							obj_t
																																								BgL_az00_6006;
																																							obj_t
																																								BgL_idz00_6005;
																																							BgL_idz00_6005
																																								=
																																								BgL_arg2131z00_1015;
																																							BgL_az00_6006
																																								=
																																								BgL_carzd24626zd2_1008;
																																							BgL_bz00_6007
																																								=
																																								BgL_carzd24633zd2_1011;
																																							BgL_bz00_61
																																								=
																																								BgL_bz00_6007;
																																							BgL_az00_60
																																								=
																																								BgL_az00_6006;
																																							BgL_idz00_59
																																								=
																																								BgL_idz00_6005;
																																							goto
																																								BgL_tagzd2106zd2_62;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd24627zd2_1009))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2136z00_1023;
																																						obj_t
																																							BgL_arg2137z00_1024;
																																						BgL_arg2136z00_1023
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2137z00_1024
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd24627zd2_1009));
																																						{
																																							obj_t
																																								BgL_bz00_6018;
																																							obj_t
																																								BgL_az00_6017;
																																							obj_t
																																								BgL_idz00_6016;
																																							BgL_idz00_6016
																																								=
																																								BgL_arg2136z00_1023;
																																							BgL_az00_6017
																																								=
																																								BgL_carzd24626zd2_1008;
																																							BgL_bz00_6018
																																								=
																																								BgL_arg2137z00_1024;
																																							BgL_bz00_65
																																								=
																																								BgL_bz00_6018;
																																							BgL_az00_64
																																								=
																																								BgL_az00_6017;
																																							BgL_idz00_63
																																								=
																																								BgL_idz00_6016;
																																							goto
																																								BgL_tagzd2107zd2_66;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_cdrzd24807zd2_1052;
																																		BgL_cdrzd24807zd2_1052
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_cdrzd24814zd2_1053;
																																			BgL_cdrzd24814zd2_1053
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd24807zd2_1052));
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd24818zd2_1054;
																																				BgL_carzd24818zd2_1054
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd24814zd2_1053));
																																				if (SYMBOLP(BgL_carzd24818zd2_1054))
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd24814zd2_1053))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg2162z00_1058;
																																								obj_t
																																									BgL_arg2163z00_1059;
																																								BgL_arg2162z00_1058
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg2163z00_1059
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd24807zd2_1052));
																																								{
																																									obj_t
																																										BgL_bz00_6037;
																																									obj_t
																																										BgL_az00_6036;
																																									obj_t
																																										BgL_idz00_6035;
																																									BgL_idz00_6035
																																										=
																																										BgL_arg2162z00_1058;
																																									BgL_az00_6036
																																										=
																																										BgL_arg2163z00_1059;
																																									BgL_bz00_6037
																																										=
																																										BgL_carzd24818zd2_1054;
																																									BgL_bz00_69
																																										=
																																										BgL_bz00_6037;
																																									BgL_az00_68
																																										=
																																										BgL_az00_6036;
																																									BgL_idz00_67
																																										=
																																										BgL_idz00_6035;
																																									goto
																																										BgL_tagzd2108zd2_70;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd24814zd2_1053))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg2167z00_1065;
																																								obj_t
																																									BgL_arg2168z00_1066;
																																								obj_t
																																									BgL_arg2169z00_1067;
																																								BgL_arg2167z00_1065
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg2168z00_1066
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd24807zd2_1052));
																																								BgL_arg2169z00_1067
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd24814zd2_1053));
																																								{
																																									obj_t
																																										BgL_bz00_6050;
																																									obj_t
																																										BgL_az00_6049;
																																									obj_t
																																										BgL_idz00_6048;
																																									BgL_idz00_6048
																																										=
																																										BgL_arg2167z00_1065;
																																									BgL_az00_6049
																																										=
																																										BgL_arg2168z00_1066;
																																									BgL_bz00_6050
																																										=
																																										BgL_arg2169z00_1067;
																																									BgL_bz00_73
																																										=
																																										BgL_bz00_6050;
																																									BgL_az00_72
																																										=
																																										BgL_az00_6049;
																																									BgL_idz00_71
																																										=
																																										BgL_idz00_6048;
																																									goto
																																										BgL_tagzd2109zd2_74;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																			}
																																		}
																																	}
																															}
																														}
																												}
																										}
																									}
																								}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_cdrzd24862zd2_1069;

																							BgL_cdrzd24862zd2_1069 =
																								CDR(((obj_t) BgL_xz00_4));
																							{	/* Expand/garith.scm 49 */
																								obj_t BgL_cdrzd24869zd2_1070;

																								BgL_cdrzd24869zd2_1070 =
																									CDR(
																									((obj_t)
																										BgL_cdrzd24862zd2_1069));
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd24873zd2_1071;

																									BgL_carzd24873zd2_1071 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd24869zd2_1070));
																									if (REALP
																										(BgL_carzd24873zd2_1071))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd24869zd2_1070))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg2174z00_1075;
																													obj_t
																														BgL_arg2175z00_1076;
																													BgL_arg2174z00_1075 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_arg2175z00_1076 =
																														CAR(((obj_t)
																															BgL_cdrzd24862zd2_1069));
																													{
																														double
																															BgL_bz00_6069;
																														obj_t BgL_az00_6068;
																														obj_t
																															BgL_idz00_6067;
																														BgL_idz00_6067 =
																															BgL_arg2174z00_1075;
																														BgL_az00_6068 =
																															BgL_arg2175z00_1076;
																														BgL_bz00_6069 =
																															REAL_TO_DOUBLE
																															(BgL_carzd24873zd2_1071);
																														BgL_bz00_53 =
																															BgL_bz00_6069;
																														BgL_az00_52 =
																															BgL_az00_6068;
																														BgL_idz00_51 =
																															BgL_idz00_6067;
																														goto
																															BgL_tagzd2104zd2_54;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd24932zd2_1079;
																											BgL_carzd24932zd2_1079 =
																												CAR(((obj_t)
																													BgL_cdrzd24862zd2_1069));
																											if (REALP
																												(BgL_carzd24932zd2_1079))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd24869zd2_1070))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg2180z00_1084;
																															obj_t
																																BgL_arg2181z00_1085;
																															BgL_arg2180z00_1084
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg2181z00_1085
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd24869zd2_1070));
																															{
																																obj_t
																																	BgL_bz00_6086;
																																double
																																	BgL_az00_6084;
																																obj_t
																																	BgL_idz00_6083;
																																BgL_idz00_6083 =
																																	BgL_arg2180z00_1084;
																																BgL_az00_6084 =
																																	REAL_TO_DOUBLE
																																	(BgL_carzd24932zd2_1079);
																																BgL_bz00_6086 =
																																	BgL_arg2181z00_1085;
																																BgL_bz00_57 =
																																	BgL_bz00_6086;
																																BgL_az00_56 =
																																	BgL_az00_6084;
																																BgL_idz00_55 =
																																	BgL_idz00_6083;
																																goto
																																	BgL_tagzd2105zd2_58;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd24983zd2_1087;
																													BgL_cdrzd24983zd2_1087
																														=
																														CDR(((obj_t)
																															BgL_xz00_4));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd24989zd2_1088;
																														obj_t
																															BgL_cdrzd24990zd2_1089;
																														BgL_carzd24989zd2_1088
																															=
																															CAR(((obj_t)
																																BgL_cdrzd24983zd2_1087));
																														BgL_cdrzd24990zd2_1089
																															=
																															CDR(((obj_t)
																																BgL_cdrzd24983zd2_1087));
																														if (SYMBOLP
																															(BgL_carzd24989zd2_1088))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd24996zd2_1091;
																																BgL_carzd24996zd2_1091
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd24990zd2_1089));
																																if (SYMBOLP
																																	(BgL_carzd24996zd2_1091))
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd24990zd2_1089))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2187z00_1095;
																																				BgL_arg2187z00_1095
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				{
																																					obj_t
																																						BgL_bz00_6107;
																																					obj_t
																																						BgL_az00_6106;
																																					obj_t
																																						BgL_idz00_6105;
																																					BgL_idz00_6105
																																						=
																																						BgL_arg2187z00_1095;
																																					BgL_az00_6106
																																						=
																																						BgL_carzd24989zd2_1088;
																																					BgL_bz00_6107
																																						=
																																						BgL_carzd24996zd2_1091;
																																					BgL_bz00_61
																																						=
																																						BgL_bz00_6107;
																																					BgL_az00_60
																																						=
																																						BgL_az00_6106;
																																					BgL_idz00_59
																																						=
																																						BgL_idz00_6105;
																																					goto
																																						BgL_tagzd2106zd2_62;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd24990zd2_1089))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2192z00_1103;
																																				obj_t
																																					BgL_arg2193z00_1104;
																																				BgL_arg2192z00_1103
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2193z00_1104
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd24990zd2_1089));
																																				{
																																					obj_t
																																						BgL_bz00_6118;
																																					obj_t
																																						BgL_az00_6117;
																																					obj_t
																																						BgL_idz00_6116;
																																					BgL_idz00_6116
																																						=
																																						BgL_arg2192z00_1103;
																																					BgL_az00_6117
																																						=
																																						BgL_carzd24989zd2_1088;
																																					BgL_bz00_6118
																																						=
																																						BgL_arg2193z00_1104;
																																					BgL_bz00_65
																																						=
																																						BgL_bz00_6118;
																																					BgL_az00_64
																																						=
																																						BgL_az00_6117;
																																					BgL_idz00_63
																																						=
																																						BgL_idz00_6116;
																																					goto
																																						BgL_tagzd2107zd2_66;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_cdrzd25170zd2_1132;
																																BgL_cdrzd25170zd2_1132
																																	=
																																	CDR(((obj_t)
																																		BgL_xz00_4));
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_cdrzd25177zd2_1133;
																																	BgL_cdrzd25177zd2_1133
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd25170zd2_1132));
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd25181zd2_1134;
																																		BgL_carzd25181zd2_1134
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd25177zd2_1133));
																																		if (SYMBOLP
																																			(BgL_carzd25181zd2_1134))
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd25177zd2_1133))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2216z00_1138;
																																						obj_t
																																							BgL_arg2217z00_1139;
																																						BgL_arg2216z00_1138
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2217z00_1139
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd25170zd2_1132));
																																						{
																																							obj_t
																																								BgL_bz00_6137;
																																							obj_t
																																								BgL_az00_6136;
																																							obj_t
																																								BgL_idz00_6135;
																																							BgL_idz00_6135
																																								=
																																								BgL_arg2216z00_1138;
																																							BgL_az00_6136
																																								=
																																								BgL_arg2217z00_1139;
																																							BgL_bz00_6137
																																								=
																																								BgL_carzd25181zd2_1134;
																																							BgL_bz00_69
																																								=
																																								BgL_bz00_6137;
																																							BgL_az00_68
																																								=
																																								BgL_az00_6136;
																																							BgL_idz00_67
																																								=
																																								BgL_idz00_6135;
																																							goto
																																								BgL_tagzd2108zd2_70;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd25177zd2_1133))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2221z00_1145;
																																						obj_t
																																							BgL_arg2222z00_1146;
																																						obj_t
																																							BgL_arg2223z00_1147;
																																						BgL_arg2221z00_1145
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2222z00_1146
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd25170zd2_1132));
																																						BgL_arg2223z00_1147
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd25177zd2_1133));
																																						{
																																							obj_t
																																								BgL_bz00_6150;
																																							obj_t
																																								BgL_az00_6149;
																																							obj_t
																																								BgL_idz00_6148;
																																							BgL_idz00_6148
																																								=
																																								BgL_arg2221z00_1145;
																																							BgL_az00_6149
																																								=
																																								BgL_arg2222z00_1146;
																																							BgL_bz00_6150
																																								=
																																								BgL_arg2223z00_1147;
																																							BgL_bz00_73
																																								=
																																								BgL_bz00_6150;
																																							BgL_az00_72
																																								=
																																								BgL_az00_6149;
																																							BgL_idz00_71
																																								=
																																								BgL_idz00_6148;
																																							goto
																																								BgL_tagzd2109zd2_74;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																	}
																																}
																															}
																													}
																												}
																										}
																								}
																							}
																						}
																				}
																		}
																	}
															}
														else
															{	/* Expand/garith.scm 49 */
																obj_t BgL_cdrzd25224zd2_1149;

																BgL_cdrzd25224zd2_1149 =
																	CDR(((obj_t) BgL_xz00_4));
																{	/* Expand/garith.scm 49 */
																	obj_t BgL_carzd25229zd2_1150;
																	obj_t BgL_cdrzd25230zd2_1151;

																	BgL_carzd25229zd2_1150 =
																		CAR(((obj_t) BgL_cdrzd25224zd2_1149));
																	BgL_cdrzd25230zd2_1151 =
																		CDR(((obj_t) BgL_cdrzd25224zd2_1149));
																	if (INTEGERP(BgL_carzd25229zd2_1150))
																		{	/* Expand/garith.scm 49 */
																			obj_t BgL_carzd25234zd2_1153;

																			BgL_carzd25234zd2_1153 =
																				CAR(((obj_t) BgL_cdrzd25230zd2_1151));
																			if (SYMBOLP(BgL_carzd25234zd2_1153))
																				{	/* Expand/garith.scm 49 */
																					if (NULLP(CDR(
																								((obj_t)
																									BgL_cdrzd25230zd2_1151))))
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_arg2229z00_1157;

																							BgL_arg2229z00_1157 =
																								CAR(((obj_t) BgL_xz00_4));
																							{
																								obj_t BgL_bz00_6171;
																								obj_t BgL_az00_6170;
																								obj_t BgL_idz00_6169;

																								BgL_idz00_6169 =
																									BgL_arg2229z00_1157;
																								BgL_az00_6170 =
																									BgL_carzd25229zd2_1150;
																								BgL_bz00_6171 =
																									BgL_carzd25234zd2_1153;
																								BgL_bz00_45 = BgL_bz00_6171;
																								BgL_az00_44 = BgL_az00_6170;
																								BgL_idz00_43 = BgL_idz00_6169;
																								goto BgL_tagzd2102zd2_46;
																							}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Expand/garith.scm 49 */
																					if (SYMBOLP(BgL_carzd25229zd2_1150))
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd25316zd2_1163;

																							BgL_carzd25316zd2_1163 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd25230zd2_1151));
																							if (INTEGERP
																								(BgL_carzd25316zd2_1163))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd25230zd2_1151))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg2235z00_1167;

																											BgL_arg2235z00_1167 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											{
																												obj_t BgL_bz00_6186;
																												obj_t BgL_az00_6185;
																												obj_t BgL_idz00_6184;

																												BgL_idz00_6184 =
																													BgL_arg2235z00_1167;
																												BgL_az00_6185 =
																													BgL_carzd25229zd2_1150;
																												BgL_bz00_6186 =
																													BgL_carzd25316zd2_1163;
																												BgL_bz00_49 =
																													BgL_bz00_6186;
																												BgL_az00_48 =
																													BgL_az00_6185;
																												BgL_idz00_47 =
																													BgL_idz00_6184;
																												goto
																													BgL_tagzd2103zd2_50;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_cdrzd25377zd2_1169;

																									BgL_cdrzd25377zd2_1169 =
																										CDR(((obj_t) BgL_xz00_4));
																									{	/* Expand/garith.scm 49 */
																										obj_t
																											BgL_cdrzd25384zd2_1170;
																										BgL_cdrzd25384zd2_1170 =
																											CDR(((obj_t)
																												BgL_cdrzd25377zd2_1169));
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd25388zd2_1171;
																											BgL_carzd25388zd2_1171 =
																												CAR(((obj_t)
																													BgL_cdrzd25384zd2_1170));
																											if (REALP
																												(BgL_carzd25388zd2_1171))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd25384zd2_1170))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg2240z00_1175;
																															obj_t
																																BgL_arg2241z00_1176;
																															BgL_arg2240z00_1175
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg2241z00_1176
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd25377zd2_1169));
																															{
																																double
																																	BgL_bz00_6205;
																																obj_t
																																	BgL_az00_6204;
																																obj_t
																																	BgL_idz00_6203;
																																BgL_idz00_6203 =
																																	BgL_arg2240z00_1175;
																																BgL_az00_6204 =
																																	BgL_arg2241z00_1176;
																																BgL_bz00_6205 =
																																	REAL_TO_DOUBLE
																																	(BgL_carzd25388zd2_1171);
																																BgL_bz00_53 =
																																	BgL_bz00_6205;
																																BgL_az00_52 =
																																	BgL_az00_6204;
																																BgL_idz00_51 =
																																	BgL_idz00_6203;
																																goto
																																	BgL_tagzd2104zd2_54;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_carzd25447zd2_1179;
																													BgL_carzd25447zd2_1179
																														=
																														CAR(((obj_t)
																															BgL_cdrzd25377zd2_1169));
																													if (REALP
																														(BgL_carzd25447zd2_1179))
																														{	/* Expand/garith.scm 49 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd25384zd2_1170))))
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_arg2246z00_1184;
																																	obj_t
																																		BgL_arg2247z00_1185;
																																	BgL_arg2246z00_1184
																																		=
																																		CAR(((obj_t)
																																			BgL_xz00_4));
																																	BgL_arg2247z00_1185
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd25384zd2_1170));
																																	{
																																		obj_t
																																			BgL_bz00_6222;
																																		double
																																			BgL_az00_6220;
																																		obj_t
																																			BgL_idz00_6219;
																																		BgL_idz00_6219
																																			=
																																			BgL_arg2246z00_1184;
																																		BgL_az00_6220
																																			=
																																			REAL_TO_DOUBLE
																																			(BgL_carzd25447zd2_1179);
																																		BgL_bz00_6222
																																			=
																																			BgL_arg2247z00_1185;
																																		BgL_bz00_57
																																			=
																																			BgL_bz00_6222;
																																		BgL_az00_56
																																			=
																																			BgL_az00_6220;
																																		BgL_idz00_55
																																			=
																																			BgL_idz00_6219;
																																		goto
																																			BgL_tagzd2105zd2_58;
																																	}
																																}
																															else
																																{	/* Expand/garith.scm 49 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_cdrzd25498zd2_1187;
																															BgL_cdrzd25498zd2_1187
																																=
																																CDR(((obj_t)
																																	BgL_xz00_4));
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd25504zd2_1188;
																																obj_t
																																	BgL_cdrzd25505zd2_1189;
																																BgL_carzd25504zd2_1188
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd25498zd2_1187));
																																BgL_cdrzd25505zd2_1189
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd25498zd2_1187));
																																if (SYMBOLP
																																	(BgL_carzd25504zd2_1188))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd25511zd2_1191;
																																		BgL_carzd25511zd2_1191
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd25505zd2_1189));
																																		if (SYMBOLP
																																			(BgL_carzd25511zd2_1191))
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd25505zd2_1189))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2253z00_1195;
																																						BgL_arg2253z00_1195
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						{
																																							obj_t
																																								BgL_bz00_6243;
																																							obj_t
																																								BgL_az00_6242;
																																							obj_t
																																								BgL_idz00_6241;
																																							BgL_idz00_6241
																																								=
																																								BgL_arg2253z00_1195;
																																							BgL_az00_6242
																																								=
																																								BgL_carzd25504zd2_1188;
																																							BgL_bz00_6243
																																								=
																																								BgL_carzd25511zd2_1191;
																																							BgL_bz00_61
																																								=
																																								BgL_bz00_6243;
																																							BgL_az00_60
																																								=
																																								BgL_az00_6242;
																																							BgL_idz00_59
																																								=
																																								BgL_idz00_6241;
																																							goto
																																								BgL_tagzd2106zd2_62;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd25505zd2_1189))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2258z00_1203;
																																						obj_t
																																							BgL_arg2259z00_1204;
																																						BgL_arg2258z00_1203
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2259z00_1204
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd25505zd2_1189));
																																						{
																																							obj_t
																																								BgL_bz00_6254;
																																							obj_t
																																								BgL_az00_6253;
																																							obj_t
																																								BgL_idz00_6252;
																																							BgL_idz00_6252
																																								=
																																								BgL_arg2258z00_1203;
																																							BgL_az00_6253
																																								=
																																								BgL_carzd25504zd2_1188;
																																							BgL_bz00_6254
																																								=
																																								BgL_arg2259z00_1204;
																																							BgL_bz00_65
																																								=
																																								BgL_bz00_6254;
																																							BgL_az00_64
																																								=
																																								BgL_az00_6253;
																																							BgL_idz00_63
																																								=
																																								BgL_idz00_6252;
																																							goto
																																								BgL_tagzd2107zd2_66;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_cdrzd25685zd2_1232;
																																		BgL_cdrzd25685zd2_1232
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		{	/* Expand/garith.scm 49 */
																																			obj_t
																																				BgL_cdrzd25692zd2_1233;
																																			BgL_cdrzd25692zd2_1233
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd25685zd2_1232));
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_carzd25696zd2_1234;
																																				BgL_carzd25696zd2_1234
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd25692zd2_1233));
																																				if (SYMBOLP(BgL_carzd25696zd2_1234))
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd25692zd2_1233))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg2283z00_1238;
																																								obj_t
																																									BgL_arg2284z00_1239;
																																								BgL_arg2283z00_1238
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg2284z00_1239
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd25685zd2_1232));
																																								{
																																									obj_t
																																										BgL_bz00_6273;
																																									obj_t
																																										BgL_az00_6272;
																																									obj_t
																																										BgL_idz00_6271;
																																									BgL_idz00_6271
																																										=
																																										BgL_arg2283z00_1238;
																																									BgL_az00_6272
																																										=
																																										BgL_arg2284z00_1239;
																																									BgL_bz00_6273
																																										=
																																										BgL_carzd25696zd2_1234;
																																									BgL_bz00_69
																																										=
																																										BgL_bz00_6273;
																																									BgL_az00_68
																																										=
																																										BgL_az00_6272;
																																									BgL_idz00_67
																																										=
																																										BgL_idz00_6271;
																																									goto
																																										BgL_tagzd2108zd2_70;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd25692zd2_1233))))
																																							{	/* Expand/garith.scm 49 */
																																								obj_t
																																									BgL_arg2289z00_1245;
																																								obj_t
																																									BgL_arg2290z00_1246;
																																								obj_t
																																									BgL_arg2291z00_1247;
																																								BgL_arg2289z00_1245
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_xz00_4));
																																								BgL_arg2290z00_1246
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd25685zd2_1232));
																																								BgL_arg2291z00_1247
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd25692zd2_1233));
																																								{
																																									obj_t
																																										BgL_bz00_6286;
																																									obj_t
																																										BgL_az00_6285;
																																									obj_t
																																										BgL_idz00_6284;
																																									BgL_idz00_6284
																																										=
																																										BgL_arg2289z00_1245;
																																									BgL_az00_6285
																																										=
																																										BgL_arg2290z00_1246;
																																									BgL_bz00_6286
																																										=
																																										BgL_arg2291z00_1247;
																																									BgL_bz00_73
																																										=
																																										BgL_bz00_6286;
																																									BgL_az00_72
																																										=
																																										BgL_az00_6285;
																																									BgL_idz00_71
																																										=
																																										BgL_idz00_6284;
																																									goto
																																										BgL_tagzd2109zd2_74;
																																								}
																																							}
																																						else
																																							{	/* Expand/garith.scm 49 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																			}
																																		}
																																	}
																															}
																														}
																												}
																										}
																									}
																								}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_cdrzd25740zd2_1249;

																							BgL_cdrzd25740zd2_1249 =
																								CDR(((obj_t) BgL_xz00_4));
																							{	/* Expand/garith.scm 49 */
																								obj_t BgL_cdrzd25747zd2_1250;

																								BgL_cdrzd25747zd2_1250 =
																									CDR(
																									((obj_t)
																										BgL_cdrzd25740zd2_1249));
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd25751zd2_1251;

																									BgL_carzd25751zd2_1251 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd25747zd2_1250));
																									if (REALP
																										(BgL_carzd25751zd2_1251))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd25747zd2_1250))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg2296z00_1255;
																													obj_t
																														BgL_arg2297z00_1256;
																													BgL_arg2296z00_1255 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_arg2297z00_1256 =
																														CAR(((obj_t)
																															BgL_cdrzd25740zd2_1249));
																													{
																														double
																															BgL_bz00_6305;
																														obj_t BgL_az00_6304;
																														obj_t
																															BgL_idz00_6303;
																														BgL_idz00_6303 =
																															BgL_arg2296z00_1255;
																														BgL_az00_6304 =
																															BgL_arg2297z00_1256;
																														BgL_bz00_6305 =
																															REAL_TO_DOUBLE
																															(BgL_carzd25751zd2_1251);
																														BgL_bz00_53 =
																															BgL_bz00_6305;
																														BgL_az00_52 =
																															BgL_az00_6304;
																														BgL_idz00_51 =
																															BgL_idz00_6303;
																														goto
																															BgL_tagzd2104zd2_54;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd25810zd2_1259;
																											BgL_carzd25810zd2_1259 =
																												CAR(((obj_t)
																													BgL_cdrzd25740zd2_1249));
																											if (REALP
																												(BgL_carzd25810zd2_1259))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd25747zd2_1250))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg2304z00_1264;
																															obj_t
																																BgL_arg2305z00_1265;
																															BgL_arg2304z00_1264
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg2305z00_1265
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd25747zd2_1250));
																															{
																																obj_t
																																	BgL_bz00_6322;
																																double
																																	BgL_az00_6320;
																																obj_t
																																	BgL_idz00_6319;
																																BgL_idz00_6319 =
																																	BgL_arg2304z00_1264;
																																BgL_az00_6320 =
																																	REAL_TO_DOUBLE
																																	(BgL_carzd25810zd2_1259);
																																BgL_bz00_6322 =
																																	BgL_arg2305z00_1265;
																																BgL_bz00_57 =
																																	BgL_bz00_6322;
																																BgL_az00_56 =
																																	BgL_az00_6320;
																																BgL_idz00_55 =
																																	BgL_idz00_6319;
																																goto
																																	BgL_tagzd2105zd2_58;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd25861zd2_1267;
																													BgL_cdrzd25861zd2_1267
																														=
																														CDR(((obj_t)
																															BgL_xz00_4));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd25867zd2_1268;
																														obj_t
																															BgL_cdrzd25868zd2_1269;
																														BgL_carzd25867zd2_1268
																															=
																															CAR(((obj_t)
																																BgL_cdrzd25861zd2_1267));
																														BgL_cdrzd25868zd2_1269
																															=
																															CDR(((obj_t)
																																BgL_cdrzd25861zd2_1267));
																														if (SYMBOLP
																															(BgL_carzd25867zd2_1268))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd25874zd2_1271;
																																BgL_carzd25874zd2_1271
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd25868zd2_1269));
																																if (SYMBOLP
																																	(BgL_carzd25874zd2_1271))
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd25868zd2_1269))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2311z00_1275;
																																				BgL_arg2311z00_1275
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				{
																																					obj_t
																																						BgL_bz00_6343;
																																					obj_t
																																						BgL_az00_6342;
																																					obj_t
																																						BgL_idz00_6341;
																																					BgL_idz00_6341
																																						=
																																						BgL_arg2311z00_1275;
																																					BgL_az00_6342
																																						=
																																						BgL_carzd25867zd2_1268;
																																					BgL_bz00_6343
																																						=
																																						BgL_carzd25874zd2_1271;
																																					BgL_bz00_61
																																						=
																																						BgL_bz00_6343;
																																					BgL_az00_60
																																						=
																																						BgL_az00_6342;
																																					BgL_idz00_59
																																						=
																																						BgL_idz00_6341;
																																					goto
																																						BgL_tagzd2106zd2_62;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd25868zd2_1269))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2316z00_1283;
																																				obj_t
																																					BgL_arg2317z00_1284;
																																				BgL_arg2316z00_1283
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2317z00_1284
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd25868zd2_1269));
																																				{
																																					obj_t
																																						BgL_bz00_6354;
																																					obj_t
																																						BgL_az00_6353;
																																					obj_t
																																						BgL_idz00_6352;
																																					BgL_idz00_6352
																																						=
																																						BgL_arg2316z00_1283;
																																					BgL_az00_6353
																																						=
																																						BgL_carzd25867zd2_1268;
																																					BgL_bz00_6354
																																						=
																																						BgL_arg2317z00_1284;
																																					BgL_bz00_65
																																						=
																																						BgL_bz00_6354;
																																					BgL_az00_64
																																						=
																																						BgL_az00_6353;
																																					BgL_idz00_63
																																						=
																																						BgL_idz00_6352;
																																					goto
																																						BgL_tagzd2107zd2_66;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_cdrzd26048zd2_1312;
																																BgL_cdrzd26048zd2_1312
																																	=
																																	CDR(((obj_t)
																																		BgL_xz00_4));
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_cdrzd26055zd2_1313;
																																	BgL_cdrzd26055zd2_1313
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd26048zd2_1312));
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd26059zd2_1314;
																																		BgL_carzd26059zd2_1314
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd26055zd2_1313));
																																		if (SYMBOLP
																																			(BgL_carzd26059zd2_1314))
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd26055zd2_1313))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2345z00_1318;
																																						obj_t
																																							BgL_arg2346z00_1319;
																																						BgL_arg2345z00_1318
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2346z00_1319
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd26048zd2_1312));
																																						{
																																							obj_t
																																								BgL_bz00_6373;
																																							obj_t
																																								BgL_az00_6372;
																																							obj_t
																																								BgL_idz00_6371;
																																							BgL_idz00_6371
																																								=
																																								BgL_arg2345z00_1318;
																																							BgL_az00_6372
																																								=
																																								BgL_arg2346z00_1319;
																																							BgL_bz00_6373
																																								=
																																								BgL_carzd26059zd2_1314;
																																							BgL_bz00_69
																																								=
																																								BgL_bz00_6373;
																																							BgL_az00_68
																																								=
																																								BgL_az00_6372;
																																							BgL_idz00_67
																																								=
																																								BgL_idz00_6371;
																																							goto
																																								BgL_tagzd2108zd2_70;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd26055zd2_1313))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2351z00_1325;
																																						obj_t
																																							BgL_arg2352z00_1326;
																																						obj_t
																																							BgL_arg2353z00_1327;
																																						BgL_arg2351z00_1325
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2352z00_1326
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd26048zd2_1312));
																																						BgL_arg2353z00_1327
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd26055zd2_1313));
																																						{
																																							obj_t
																																								BgL_bz00_6386;
																																							obj_t
																																								BgL_az00_6385;
																																							obj_t
																																								BgL_idz00_6384;
																																							BgL_idz00_6384
																																								=
																																								BgL_arg2351z00_1325;
																																							BgL_az00_6385
																																								=
																																								BgL_arg2352z00_1326;
																																							BgL_bz00_6386
																																								=
																																								BgL_arg2353z00_1327;
																																							BgL_bz00_73
																																								=
																																								BgL_bz00_6386;
																																							BgL_az00_72
																																								=
																																								BgL_az00_6385;
																																							BgL_idz00_71
																																								=
																																								BgL_idz00_6384;
																																							goto
																																								BgL_tagzd2109zd2_74;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																	}
																																}
																															}
																													}
																												}
																										}
																								}
																							}
																						}
																				}
																		}
																	else
																		{	/* Expand/garith.scm 49 */
																			if (SYMBOLP(BgL_carzd25229zd2_1150))
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_carzd26116zd2_1333;

																					BgL_carzd26116zd2_1333 =
																						CAR(
																						((obj_t) BgL_cdrzd25230zd2_1151));
																					if (INTEGERP(BgL_carzd26116zd2_1333))
																						{	/* Expand/garith.scm 49 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd25230zd2_1151))))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_arg2361z00_1337;

																									BgL_arg2361z00_1337 =
																										CAR(((obj_t) BgL_xz00_4));
																									{
																										obj_t BgL_bz00_6401;
																										obj_t BgL_az00_6400;
																										obj_t BgL_idz00_6399;

																										BgL_idz00_6399 =
																											BgL_arg2361z00_1337;
																										BgL_az00_6400 =
																											BgL_carzd25229zd2_1150;
																										BgL_bz00_6401 =
																											BgL_carzd26116zd2_1333;
																										BgL_bz00_49 = BgL_bz00_6401;
																										BgL_az00_48 = BgL_az00_6400;
																										BgL_idz00_47 =
																											BgL_idz00_6399;
																										goto BgL_tagzd2103zd2_50;
																									}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_cdrzd26177zd2_1339;

																							BgL_cdrzd26177zd2_1339 =
																								CDR(((obj_t) BgL_xz00_4));
																							{	/* Expand/garith.scm 49 */
																								obj_t BgL_cdrzd26184zd2_1340;

																								BgL_cdrzd26184zd2_1340 =
																									CDR(
																									((obj_t)
																										BgL_cdrzd26177zd2_1339));
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd26188zd2_1341;

																									BgL_carzd26188zd2_1341 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd26184zd2_1340));
																									if (REALP
																										(BgL_carzd26188zd2_1341))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd26184zd2_1340))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg2367z00_1345;
																													obj_t
																														BgL_arg2368z00_1346;
																													BgL_arg2367z00_1345 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_arg2368z00_1346 =
																														CAR(((obj_t)
																															BgL_cdrzd26177zd2_1339));
																													{
																														double
																															BgL_bz00_6420;
																														obj_t BgL_az00_6419;
																														obj_t
																															BgL_idz00_6418;
																														BgL_idz00_6418 =
																															BgL_arg2367z00_1345;
																														BgL_az00_6419 =
																															BgL_arg2368z00_1346;
																														BgL_bz00_6420 =
																															REAL_TO_DOUBLE
																															(BgL_carzd26188zd2_1341);
																														BgL_bz00_53 =
																															BgL_bz00_6420;
																														BgL_az00_52 =
																															BgL_az00_6419;
																														BgL_idz00_51 =
																															BgL_idz00_6418;
																														goto
																															BgL_tagzd2104zd2_54;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd26247zd2_1349;
																											BgL_carzd26247zd2_1349 =
																												CAR(((obj_t)
																													BgL_cdrzd26177zd2_1339));
																											if (REALP
																												(BgL_carzd26247zd2_1349))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd26184zd2_1340))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg2373z00_1354;
																															obj_t
																																BgL_arg2374z00_1355;
																															BgL_arg2373z00_1354
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg2374z00_1355
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd26184zd2_1340));
																															{
																																obj_t
																																	BgL_bz00_6437;
																																double
																																	BgL_az00_6435;
																																obj_t
																																	BgL_idz00_6434;
																																BgL_idz00_6434 =
																																	BgL_arg2373z00_1354;
																																BgL_az00_6435 =
																																	REAL_TO_DOUBLE
																																	(BgL_carzd26247zd2_1349);
																																BgL_bz00_6437 =
																																	BgL_arg2374z00_1355;
																																BgL_bz00_57 =
																																	BgL_bz00_6437;
																																BgL_az00_56 =
																																	BgL_az00_6435;
																																BgL_idz00_55 =
																																	BgL_idz00_6434;
																																goto
																																	BgL_tagzd2105zd2_58;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd26298zd2_1357;
																													BgL_cdrzd26298zd2_1357
																														=
																														CDR(((obj_t)
																															BgL_xz00_4));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd26304zd2_1358;
																														obj_t
																															BgL_cdrzd26305zd2_1359;
																														BgL_carzd26304zd2_1358
																															=
																															CAR(((obj_t)
																																BgL_cdrzd26298zd2_1357));
																														BgL_cdrzd26305zd2_1359
																															=
																															CDR(((obj_t)
																																BgL_cdrzd26298zd2_1357));
																														if (SYMBOLP
																															(BgL_carzd26304zd2_1358))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd26311zd2_1361;
																																BgL_carzd26311zd2_1361
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd26305zd2_1359));
																																if (SYMBOLP
																																	(BgL_carzd26311zd2_1361))
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd26305zd2_1359))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2380z00_1365;
																																				BgL_arg2380z00_1365
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				{
																																					obj_t
																																						BgL_bz00_6458;
																																					obj_t
																																						BgL_az00_6457;
																																					obj_t
																																						BgL_idz00_6456;
																																					BgL_idz00_6456
																																						=
																																						BgL_arg2380z00_1365;
																																					BgL_az00_6457
																																						=
																																						BgL_carzd26304zd2_1358;
																																					BgL_bz00_6458
																																						=
																																						BgL_carzd26311zd2_1361;
																																					BgL_bz00_61
																																						=
																																						BgL_bz00_6458;
																																					BgL_az00_60
																																						=
																																						BgL_az00_6457;
																																					BgL_idz00_59
																																						=
																																						BgL_idz00_6456;
																																					goto
																																						BgL_tagzd2106zd2_62;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd26305zd2_1359))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2385z00_1373;
																																				obj_t
																																					BgL_arg2386z00_1374;
																																				BgL_arg2385z00_1373
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2386z00_1374
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd26305zd2_1359));
																																				{
																																					obj_t
																																						BgL_bz00_6469;
																																					obj_t
																																						BgL_az00_6468;
																																					obj_t
																																						BgL_idz00_6467;
																																					BgL_idz00_6467
																																						=
																																						BgL_arg2385z00_1373;
																																					BgL_az00_6468
																																						=
																																						BgL_carzd26304zd2_1358;
																																					BgL_bz00_6469
																																						=
																																						BgL_arg2386z00_1374;
																																					BgL_bz00_65
																																						=
																																						BgL_bz00_6469;
																																					BgL_az00_64
																																						=
																																						BgL_az00_6468;
																																					BgL_idz00_63
																																						=
																																						BgL_idz00_6467;
																																					goto
																																						BgL_tagzd2107zd2_66;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_cdrzd26485zd2_1402;
																																BgL_cdrzd26485zd2_1402
																																	=
																																	CDR(((obj_t)
																																		BgL_xz00_4));
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_cdrzd26492zd2_1403;
																																	BgL_cdrzd26492zd2_1403
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd26485zd2_1402));
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd26496zd2_1404;
																																		BgL_carzd26496zd2_1404
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd26492zd2_1403));
																																		if (SYMBOLP
																																			(BgL_carzd26496zd2_1404))
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd26492zd2_1403))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2412z00_1408;
																																						obj_t
																																							BgL_arg2414z00_1409;
																																						BgL_arg2412z00_1408
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2414z00_1409
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd26485zd2_1402));
																																						{
																																							obj_t
																																								BgL_bz00_6488;
																																							obj_t
																																								BgL_az00_6487;
																																							obj_t
																																								BgL_idz00_6486;
																																							BgL_idz00_6486
																																								=
																																								BgL_arg2412z00_1408;
																																							BgL_az00_6487
																																								=
																																								BgL_arg2414z00_1409;
																																							BgL_bz00_6488
																																								=
																																								BgL_carzd26496zd2_1404;
																																							BgL_bz00_69
																																								=
																																								BgL_bz00_6488;
																																							BgL_az00_68
																																								=
																																								BgL_az00_6487;
																																							BgL_idz00_67
																																								=
																																								BgL_idz00_6486;
																																							goto
																																								BgL_tagzd2108zd2_70;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd26492zd2_1403))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2419z00_1415;
																																						obj_t
																																							BgL_arg2420z00_1416;
																																						obj_t
																																							BgL_arg2421z00_1417;
																																						BgL_arg2419z00_1415
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2420z00_1416
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd26485zd2_1402));
																																						BgL_arg2421z00_1417
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd26492zd2_1403));
																																						{
																																							obj_t
																																								BgL_bz00_6501;
																																							obj_t
																																								BgL_az00_6500;
																																							obj_t
																																								BgL_idz00_6499;
																																							BgL_idz00_6499
																																								=
																																								BgL_arg2419z00_1415;
																																							BgL_az00_6500
																																								=
																																								BgL_arg2420z00_1416;
																																							BgL_bz00_6501
																																								=
																																								BgL_arg2421z00_1417;
																																							BgL_bz00_73
																																								=
																																								BgL_bz00_6501;
																																							BgL_az00_72
																																								=
																																								BgL_az00_6500;
																																							BgL_idz00_71
																																								=
																																								BgL_idz00_6499;
																																							goto
																																								BgL_tagzd2109zd2_74;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																	}
																																}
																															}
																													}
																												}
																										}
																								}
																							}
																						}
																				}
																			else
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_cdrzd26540zd2_1419;

																					BgL_cdrzd26540zd2_1419 =
																						CDR(((obj_t) BgL_xz00_4));
																					{	/* Expand/garith.scm 49 */
																						obj_t BgL_cdrzd26547zd2_1420;

																						BgL_cdrzd26547zd2_1420 =
																							CDR(
																							((obj_t) BgL_cdrzd26540zd2_1419));
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd26551zd2_1421;

																							BgL_carzd26551zd2_1421 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd26547zd2_1420));
																							if (REALP(BgL_carzd26551zd2_1421))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd26547zd2_1420))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg2428z00_1425;
																											obj_t BgL_arg2429z00_1426;

																											BgL_arg2428z00_1425 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											BgL_arg2429z00_1426 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd26540zd2_1419));
																											{
																												double BgL_bz00_6520;
																												obj_t BgL_az00_6519;
																												obj_t BgL_idz00_6518;

																												BgL_idz00_6518 =
																													BgL_arg2428z00_1425;
																												BgL_az00_6519 =
																													BgL_arg2429z00_1426;
																												BgL_bz00_6520 =
																													REAL_TO_DOUBLE
																													(BgL_carzd26551zd2_1421);
																												BgL_bz00_53 =
																													BgL_bz00_6520;
																												BgL_az00_52 =
																													BgL_az00_6519;
																												BgL_idz00_51 =
																													BgL_idz00_6518;
																												goto
																													BgL_tagzd2104zd2_54;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd26610zd2_1429;

																									BgL_carzd26610zd2_1429 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd26540zd2_1419));
																									if (REALP
																										(BgL_carzd26610zd2_1429))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd26547zd2_1420))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg2434z00_1434;
																													obj_t
																														BgL_arg2435z00_1435;
																													BgL_arg2434z00_1434 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_arg2435z00_1435 =
																														CAR(((obj_t)
																															BgL_cdrzd26547zd2_1420));
																													{
																														obj_t BgL_bz00_6537;
																														double
																															BgL_az00_6535;
																														obj_t
																															BgL_idz00_6534;
																														BgL_idz00_6534 =
																															BgL_arg2434z00_1434;
																														BgL_az00_6535 =
																															REAL_TO_DOUBLE
																															(BgL_carzd26610zd2_1429);
																														BgL_bz00_6537 =
																															BgL_arg2435z00_1435;
																														BgL_bz00_57 =
																															BgL_bz00_6537;
																														BgL_az00_56 =
																															BgL_az00_6535;
																														BgL_idz00_55 =
																															BgL_idz00_6534;
																														goto
																															BgL_tagzd2105zd2_58;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_cdrzd26661zd2_1437;
																											BgL_cdrzd26661zd2_1437 =
																												CDR(((obj_t)
																													BgL_xz00_4));
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_carzd26667zd2_1438;
																												obj_t
																													BgL_cdrzd26668zd2_1439;
																												BgL_carzd26667zd2_1438 =
																													CAR(((obj_t)
																														BgL_cdrzd26661zd2_1437));
																												BgL_cdrzd26668zd2_1439 =
																													CDR(((obj_t)
																														BgL_cdrzd26661zd2_1437));
																												if (SYMBOLP
																													(BgL_carzd26667zd2_1438))
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd26674zd2_1441;
																														BgL_carzd26674zd2_1441
																															=
																															CAR(((obj_t)
																																BgL_cdrzd26668zd2_1439));
																														if (SYMBOLP
																															(BgL_carzd26674zd2_1441))
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd26668zd2_1439))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2444z00_1445;
																																		BgL_arg2444z00_1445
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		{
																																			obj_t
																																				BgL_bz00_6558;
																																			obj_t
																																				BgL_az00_6557;
																																			obj_t
																																				BgL_idz00_6556;
																																			BgL_idz00_6556
																																				=
																																				BgL_arg2444z00_1445;
																																			BgL_az00_6557
																																				=
																																				BgL_carzd26667zd2_1438;
																																			BgL_bz00_6558
																																				=
																																				BgL_carzd26674zd2_1441;
																																			BgL_bz00_61
																																				=
																																				BgL_bz00_6558;
																																			BgL_az00_60
																																				=
																																				BgL_az00_6557;
																																			BgL_idz00_59
																																				=
																																				BgL_idz00_6556;
																																			goto
																																				BgL_tagzd2106zd2_62;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd26668zd2_1439))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2449z00_1453;
																																		obj_t
																																			BgL_arg2450z00_1454;
																																		BgL_arg2449z00_1453
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg2450z00_1454
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd26668zd2_1439));
																																		{
																																			obj_t
																																				BgL_bz00_6569;
																																			obj_t
																																				BgL_az00_6568;
																																			obj_t
																																				BgL_idz00_6567;
																																			BgL_idz00_6567
																																				=
																																				BgL_arg2449z00_1453;
																																			BgL_az00_6568
																																				=
																																				BgL_carzd26667zd2_1438;
																																			BgL_bz00_6569
																																				=
																																				BgL_arg2450z00_1454;
																																			BgL_bz00_65
																																				=
																																				BgL_bz00_6569;
																																			BgL_az00_64
																																				=
																																				BgL_az00_6568;
																																			BgL_idz00_63
																																				=
																																				BgL_idz00_6567;
																																			goto
																																				BgL_tagzd2107zd2_66;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																													}
																												else
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_cdrzd26848zd2_1482;
																														BgL_cdrzd26848zd2_1482
																															=
																															CDR(((obj_t)
																																BgL_xz00_4));
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_cdrzd26855zd2_1483;
																															BgL_cdrzd26855zd2_1483
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd26848zd2_1482));
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd26859zd2_1484;
																																BgL_carzd26859zd2_1484
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd26855zd2_1483));
																																if (SYMBOLP
																																	(BgL_carzd26859zd2_1484))
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd26855zd2_1483))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2475z00_1488;
																																				obj_t
																																					BgL_arg2476z00_1489;
																																				BgL_arg2475z00_1488
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2476z00_1489
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd26848zd2_1482));
																																				{
																																					obj_t
																																						BgL_bz00_6588;
																																					obj_t
																																						BgL_az00_6587;
																																					obj_t
																																						BgL_idz00_6586;
																																					BgL_idz00_6586
																																						=
																																						BgL_arg2475z00_1488;
																																					BgL_az00_6587
																																						=
																																						BgL_arg2476z00_1489;
																																					BgL_bz00_6588
																																						=
																																						BgL_carzd26859zd2_1484;
																																					BgL_bz00_69
																																						=
																																						BgL_bz00_6588;
																																					BgL_az00_68
																																						=
																																						BgL_az00_6587;
																																					BgL_idz00_67
																																						=
																																						BgL_idz00_6586;
																																					goto
																																						BgL_tagzd2108zd2_70;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd26855zd2_1483))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2482z00_1495;
																																				obj_t
																																					BgL_arg2483z00_1496;
																																				obj_t
																																					BgL_arg2484z00_1497;
																																				BgL_arg2482z00_1495
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2483z00_1496
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd26848zd2_1482));
																																				BgL_arg2484z00_1497
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd26855zd2_1483));
																																				{
																																					obj_t
																																						BgL_bz00_6601;
																																					obj_t
																																						BgL_az00_6600;
																																					obj_t
																																						BgL_idz00_6599;
																																					BgL_idz00_6599
																																						=
																																						BgL_arg2482z00_1495;
																																					BgL_az00_6600
																																						=
																																						BgL_arg2483z00_1496;
																																					BgL_bz00_6601
																																						=
																																						BgL_arg2484z00_1497;
																																					BgL_bz00_73
																																						=
																																						BgL_bz00_6601;
																																					BgL_az00_72
																																						=
																																						BgL_az00_6600;
																																					BgL_idz00_71
																																						=
																																						BgL_idz00_6599;
																																					goto
																																						BgL_tagzd2109zd2_74;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																															}
																														}
																													}
																											}
																										}
																								}
																						}
																					}
																				}
																		}
																}
															}
													}
												else
													{	/* Expand/garith.scm 49 */
														obj_t BgL_cdrzd26902zd2_1500;

														BgL_cdrzd26902zd2_1500 = CDR(((obj_t) BgL_xz00_4));
														{	/* Expand/garith.scm 49 */
															obj_t BgL_carzd26907zd2_1501;
															obj_t BgL_cdrzd26908zd2_1502;

															BgL_carzd26907zd2_1501 =
																CAR(((obj_t) BgL_cdrzd26902zd2_1500));
															BgL_cdrzd26908zd2_1502 =
																CDR(((obj_t) BgL_cdrzd26902zd2_1500));
															if (INTEGERP(BgL_carzd26907zd2_1501))
																{	/* Expand/garith.scm 49 */
																	obj_t BgL_carzd26912zd2_1504;

																	BgL_carzd26912zd2_1504 =
																		CAR(((obj_t) BgL_cdrzd26908zd2_1502));
																	if (SYMBOLP(BgL_carzd26912zd2_1504))
																		{	/* Expand/garith.scm 49 */
																			if (NULLP(CDR(
																						((obj_t) BgL_cdrzd26908zd2_1502))))
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_arg2492z00_1508;

																					BgL_arg2492z00_1508 =
																						CAR(((obj_t) BgL_xz00_4));
																					{
																						obj_t BgL_bz00_6622;
																						obj_t BgL_az00_6621;
																						obj_t BgL_idz00_6620;

																						BgL_idz00_6620 =
																							BgL_arg2492z00_1508;
																						BgL_az00_6621 =
																							BgL_carzd26907zd2_1501;
																						BgL_bz00_6622 =
																							BgL_carzd26912zd2_1504;
																						BgL_bz00_45 = BgL_bz00_6622;
																						BgL_az00_44 = BgL_az00_6621;
																						BgL_idz00_43 = BgL_idz00_6620;
																						goto BgL_tagzd2102zd2_46;
																					}
																				}
																			else
																				{	/* Expand/garith.scm 49 */
																					return BFALSE;
																				}
																		}
																	else
																		{	/* Expand/garith.scm 49 */
																			if (SYMBOLP(BgL_carzd26907zd2_1501))
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_carzd27004zd2_1514;

																					BgL_carzd27004zd2_1514 =
																						CAR(
																						((obj_t) BgL_cdrzd26908zd2_1502));
																					if (INTEGERP(BgL_carzd27004zd2_1514))
																						{	/* Expand/garith.scm 49 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd26908zd2_1502))))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_arg2500z00_1518;

																									BgL_arg2500z00_1518 =
																										CAR(((obj_t) BgL_xz00_4));
																									{
																										obj_t BgL_bz00_6637;
																										obj_t BgL_az00_6636;
																										obj_t BgL_idz00_6635;

																										BgL_idz00_6635 =
																											BgL_arg2500z00_1518;
																										BgL_az00_6636 =
																											BgL_carzd26907zd2_1501;
																										BgL_bz00_6637 =
																											BgL_carzd27004zd2_1514;
																										BgL_bz00_49 = BgL_bz00_6637;
																										BgL_az00_48 = BgL_az00_6636;
																										BgL_idz00_47 =
																											BgL_idz00_6635;
																										goto BgL_tagzd2103zd2_50;
																									}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_cdrzd27074zd2_1520;

																							BgL_cdrzd27074zd2_1520 =
																								CDR(((obj_t) BgL_xz00_4));
																							{	/* Expand/garith.scm 49 */
																								obj_t BgL_cdrzd27082zd2_1521;

																								BgL_cdrzd27082zd2_1521 =
																									CDR(
																									((obj_t)
																										BgL_cdrzd27074zd2_1520));
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd27087zd2_1522;

																									BgL_carzd27087zd2_1522 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd27082zd2_1521));
																									if (REALP
																										(BgL_carzd27087zd2_1522))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd27082zd2_1521))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg2505z00_1526;
																													obj_t
																														BgL_arg2506z00_1527;
																													BgL_arg2505z00_1526 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_arg2506z00_1527 =
																														CAR(((obj_t)
																															BgL_cdrzd27074zd2_1520));
																													{
																														double
																															BgL_bz00_6656;
																														obj_t BgL_az00_6655;
																														obj_t
																															BgL_idz00_6654;
																														BgL_idz00_6654 =
																															BgL_arg2505z00_1526;
																														BgL_az00_6655 =
																															BgL_arg2506z00_1527;
																														BgL_bz00_6656 =
																															REAL_TO_DOUBLE
																															(BgL_carzd27087zd2_1522);
																														BgL_bz00_53 =
																															BgL_bz00_6656;
																														BgL_az00_52 =
																															BgL_az00_6655;
																														BgL_idz00_51 =
																															BgL_idz00_6654;
																														goto
																															BgL_tagzd2104zd2_54;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd27155zd2_1530;
																											BgL_carzd27155zd2_1530 =
																												CAR(((obj_t)
																													BgL_cdrzd27074zd2_1520));
																											if (REALP
																												(BgL_carzd27155zd2_1530))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd27082zd2_1521))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg2512z00_1535;
																															obj_t
																																BgL_arg2513z00_1536;
																															BgL_arg2512z00_1535
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg2513z00_1536
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd27082zd2_1521));
																															{
																																obj_t
																																	BgL_bz00_6673;
																																double
																																	BgL_az00_6671;
																																obj_t
																																	BgL_idz00_6670;
																																BgL_idz00_6670 =
																																	BgL_arg2512z00_1535;
																																BgL_az00_6671 =
																																	REAL_TO_DOUBLE
																																	(BgL_carzd27155zd2_1530);
																																BgL_bz00_6673 =
																																	BgL_arg2513z00_1536;
																																BgL_bz00_57 =
																																	BgL_bz00_6673;
																																BgL_az00_56 =
																																	BgL_az00_6671;
																																BgL_idz00_55 =
																																	BgL_idz00_6670;
																																goto
																																	BgL_tagzd2105zd2_58;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd27213zd2_1538;
																													BgL_cdrzd27213zd2_1538
																														=
																														CDR(((obj_t)
																															BgL_xz00_4));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd27220zd2_1539;
																														obj_t
																															BgL_cdrzd27221zd2_1540;
																														BgL_carzd27220zd2_1539
																															=
																															CAR(((obj_t)
																																BgL_cdrzd27213zd2_1538));
																														BgL_cdrzd27221zd2_1540
																															=
																															CDR(((obj_t)
																																BgL_cdrzd27213zd2_1538));
																														if (SYMBOLP
																															(BgL_carzd27220zd2_1539))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd27228zd2_1542;
																																BgL_carzd27228zd2_1542
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd27221zd2_1540));
																																if (SYMBOLP
																																	(BgL_carzd27228zd2_1542))
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd27221zd2_1540))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2521z00_1546;
																																				BgL_arg2521z00_1546
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				{
																																					obj_t
																																						BgL_bz00_6694;
																																					obj_t
																																						BgL_az00_6693;
																																					obj_t
																																						BgL_idz00_6692;
																																					BgL_idz00_6692
																																						=
																																						BgL_arg2521z00_1546;
																																					BgL_az00_6693
																																						=
																																						BgL_carzd27220zd2_1539;
																																					BgL_bz00_6694
																																						=
																																						BgL_carzd27228zd2_1542;
																																					BgL_bz00_61
																																						=
																																						BgL_bz00_6694;
																																					BgL_az00_60
																																						=
																																						BgL_az00_6693;
																																					BgL_idz00_59
																																						=
																																						BgL_idz00_6692;
																																					goto
																																						BgL_tagzd2106zd2_62;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd27221zd2_1540))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2528z00_1554;
																																				obj_t
																																					BgL_arg2529z00_1555;
																																				BgL_arg2528z00_1554
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2529z00_1555
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd27221zd2_1540));
																																				{
																																					obj_t
																																						BgL_bz00_6705;
																																					obj_t
																																						BgL_az00_6704;
																																					obj_t
																																						BgL_idz00_6703;
																																					BgL_idz00_6703
																																						=
																																						BgL_arg2528z00_1554;
																																					BgL_az00_6704
																																						=
																																						BgL_carzd27220zd2_1539;
																																					BgL_bz00_6705
																																						=
																																						BgL_arg2529z00_1555;
																																					BgL_bz00_65
																																						=
																																						BgL_bz00_6705;
																																					BgL_az00_64
																																						=
																																						BgL_az00_6704;
																																					BgL_idz00_63
																																						=
																																						BgL_idz00_6703;
																																					goto
																																						BgL_tagzd2107zd2_66;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_cdrzd27430zd2_1583;
																																BgL_cdrzd27430zd2_1583
																																	=
																																	CDR(((obj_t)
																																		BgL_xz00_4));
																																{	/* Expand/garith.scm 49 */
																																	obj_t
																																		BgL_cdrzd27438zd2_1584;
																																	BgL_cdrzd27438zd2_1584
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd27430zd2_1583));
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_carzd27443zd2_1585;
																																		BgL_carzd27443zd2_1585
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd27438zd2_1584));
																																		if (SYMBOLP
																																			(BgL_carzd27443zd2_1585))
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd27438zd2_1584))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2560z00_1589;
																																						obj_t
																																							BgL_arg2561z00_1590;
																																						BgL_arg2560z00_1589
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2561z00_1590
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd27430zd2_1583));
																																						{
																																							obj_t
																																								BgL_bz00_6724;
																																							obj_t
																																								BgL_az00_6723;
																																							obj_t
																																								BgL_idz00_6722;
																																							BgL_idz00_6722
																																								=
																																								BgL_arg2560z00_1589;
																																							BgL_az00_6723
																																								=
																																								BgL_arg2561z00_1590;
																																							BgL_bz00_6724
																																								=
																																								BgL_carzd27443zd2_1585;
																																							BgL_bz00_69
																																								=
																																								BgL_bz00_6724;
																																							BgL_az00_68
																																								=
																																								BgL_az00_6723;
																																							BgL_idz00_67
																																								=
																																								BgL_idz00_6722;
																																							goto
																																								BgL_tagzd2108zd2_70;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd27438zd2_1584))))
																																					{	/* Expand/garith.scm 49 */
																																						obj_t
																																							BgL_arg2565z00_1596;
																																						obj_t
																																							BgL_arg2566z00_1597;
																																						obj_t
																																							BgL_arg2567z00_1598;
																																						BgL_arg2565z00_1596
																																							=
																																							CAR
																																							(((obj_t) BgL_xz00_4));
																																						BgL_arg2566z00_1597
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd27430zd2_1583));
																																						BgL_arg2567z00_1598
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd27438zd2_1584));
																																						{
																																							obj_t
																																								BgL_bz00_6737;
																																							obj_t
																																								BgL_az00_6736;
																																							obj_t
																																								BgL_idz00_6735;
																																							BgL_idz00_6735
																																								=
																																								BgL_arg2565z00_1596;
																																							BgL_az00_6736
																																								=
																																								BgL_arg2566z00_1597;
																																							BgL_bz00_6737
																																								=
																																								BgL_arg2567z00_1598;
																																							BgL_bz00_73
																																								=
																																								BgL_bz00_6737;
																																							BgL_az00_72
																																								=
																																								BgL_az00_6736;
																																							BgL_idz00_71
																																								=
																																								BgL_idz00_6735;
																																							goto
																																								BgL_tagzd2109zd2_74;
																																						}
																																					}
																																				else
																																					{	/* Expand/garith.scm 49 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																	}
																																}
																															}
																													}
																												}
																										}
																								}
																							}
																						}
																				}
																			else
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_cdrzd27495zd2_1600;

																					BgL_cdrzd27495zd2_1600 =
																						CDR(((obj_t) BgL_xz00_4));
																					{	/* Expand/garith.scm 49 */
																						obj_t BgL_cdrzd27503zd2_1601;

																						BgL_cdrzd27503zd2_1601 =
																							CDR(
																							((obj_t) BgL_cdrzd27495zd2_1600));
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd27508zd2_1602;

																							BgL_carzd27508zd2_1602 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd27503zd2_1601));
																							if (REALP(BgL_carzd27508zd2_1602))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd27503zd2_1601))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg2578z00_1606;
																											obj_t BgL_arg2579z00_1607;

																											BgL_arg2578z00_1606 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											BgL_arg2579z00_1607 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd27495zd2_1600));
																											{
																												double BgL_bz00_6756;
																												obj_t BgL_az00_6755;
																												obj_t BgL_idz00_6754;

																												BgL_idz00_6754 =
																													BgL_arg2578z00_1606;
																												BgL_az00_6755 =
																													BgL_arg2579z00_1607;
																												BgL_bz00_6756 =
																													REAL_TO_DOUBLE
																													(BgL_carzd27508zd2_1602);
																												BgL_bz00_53 =
																													BgL_bz00_6756;
																												BgL_az00_52 =
																													BgL_az00_6755;
																												BgL_idz00_51 =
																													BgL_idz00_6754;
																												goto
																													BgL_tagzd2104zd2_54;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd27576zd2_1610;

																									BgL_carzd27576zd2_1610 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd27495zd2_1600));
																									if (REALP
																										(BgL_carzd27576zd2_1610))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd27503zd2_1601))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg2589z00_1615;
																													obj_t
																														BgL_arg2590z00_1616;
																													BgL_arg2589z00_1615 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_arg2590z00_1616 =
																														CAR(((obj_t)
																															BgL_cdrzd27503zd2_1601));
																													{
																														obj_t BgL_bz00_6773;
																														double
																															BgL_az00_6771;
																														obj_t
																															BgL_idz00_6770;
																														BgL_idz00_6770 =
																															BgL_arg2589z00_1615;
																														BgL_az00_6771 =
																															REAL_TO_DOUBLE
																															(BgL_carzd27576zd2_1610);
																														BgL_bz00_6773 =
																															BgL_arg2590z00_1616;
																														BgL_bz00_57 =
																															BgL_bz00_6773;
																														BgL_az00_56 =
																															BgL_az00_6771;
																														BgL_idz00_55 =
																															BgL_idz00_6770;
																														goto
																															BgL_tagzd2105zd2_58;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_cdrzd27634zd2_1618;
																											BgL_cdrzd27634zd2_1618 =
																												CDR(((obj_t)
																													BgL_xz00_4));
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_carzd27641zd2_1619;
																												obj_t
																													BgL_cdrzd27642zd2_1620;
																												BgL_carzd27641zd2_1619 =
																													CAR(((obj_t)
																														BgL_cdrzd27634zd2_1618));
																												BgL_cdrzd27642zd2_1620 =
																													CDR(((obj_t)
																														BgL_cdrzd27634zd2_1618));
																												if (SYMBOLP
																													(BgL_carzd27641zd2_1619))
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd27649zd2_1622;
																														BgL_carzd27649zd2_1622
																															=
																															CAR(((obj_t)
																																BgL_cdrzd27642zd2_1620));
																														if (SYMBOLP
																															(BgL_carzd27649zd2_1622))
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd27642zd2_1620))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2596z00_1626;
																																		BgL_arg2596z00_1626
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		{
																																			obj_t
																																				BgL_bz00_6794;
																																			obj_t
																																				BgL_az00_6793;
																																			obj_t
																																				BgL_idz00_6792;
																																			BgL_idz00_6792
																																				=
																																				BgL_arg2596z00_1626;
																																			BgL_az00_6793
																																				=
																																				BgL_carzd27641zd2_1619;
																																			BgL_bz00_6794
																																				=
																																				BgL_carzd27649zd2_1622;
																																			BgL_bz00_61
																																				=
																																				BgL_bz00_6794;
																																			BgL_az00_60
																																				=
																																				BgL_az00_6793;
																																			BgL_idz00_59
																																				=
																																				BgL_idz00_6792;
																																			goto
																																				BgL_tagzd2106zd2_62;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd27642zd2_1620))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2601z00_1634;
																																		obj_t
																																			BgL_arg2604z00_1635;
																																		BgL_arg2601z00_1634
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg2604z00_1635
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd27642zd2_1620));
																																		{
																																			obj_t
																																				BgL_bz00_6805;
																																			obj_t
																																				BgL_az00_6804;
																																			obj_t
																																				BgL_idz00_6803;
																																			BgL_idz00_6803
																																				=
																																				BgL_arg2601z00_1634;
																																			BgL_az00_6804
																																				=
																																				BgL_carzd27641zd2_1619;
																																			BgL_bz00_6805
																																				=
																																				BgL_arg2604z00_1635;
																																			BgL_bz00_65
																																				=
																																				BgL_bz00_6805;
																																			BgL_az00_64
																																				=
																																				BgL_az00_6804;
																																			BgL_idz00_63
																																				=
																																				BgL_idz00_6803;
																																			goto
																																				BgL_tagzd2107zd2_66;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																													}
																												else
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_cdrzd27851zd2_1663;
																														BgL_cdrzd27851zd2_1663
																															=
																															CDR(((obj_t)
																																BgL_xz00_4));
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_cdrzd27859zd2_1664;
																															BgL_cdrzd27859zd2_1664
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd27851zd2_1663));
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd27864zd2_1665;
																																BgL_carzd27864zd2_1665
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd27859zd2_1664));
																																if (SYMBOLP
																																	(BgL_carzd27864zd2_1665))
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd27859zd2_1664))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2628z00_1669;
																																				obj_t
																																					BgL_arg2629z00_1670;
																																				BgL_arg2628z00_1669
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2629z00_1670
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd27851zd2_1663));
																																				{
																																					obj_t
																																						BgL_bz00_6824;
																																					obj_t
																																						BgL_az00_6823;
																																					obj_t
																																						BgL_idz00_6822;
																																					BgL_idz00_6822
																																						=
																																						BgL_arg2628z00_1669;
																																					BgL_az00_6823
																																						=
																																						BgL_arg2629z00_1670;
																																					BgL_bz00_6824
																																						=
																																						BgL_carzd27864zd2_1665;
																																					BgL_bz00_69
																																						=
																																						BgL_bz00_6824;
																																					BgL_az00_68
																																						=
																																						BgL_az00_6823;
																																					BgL_idz00_67
																																						=
																																						BgL_idz00_6822;
																																					goto
																																						BgL_tagzd2108zd2_70;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd27859zd2_1664))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2633z00_1676;
																																				obj_t
																																					BgL_arg2635z00_1677;
																																				obj_t
																																					BgL_arg2636z00_1678;
																																				BgL_arg2633z00_1676
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2635z00_1677
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd27851zd2_1663));
																																				BgL_arg2636z00_1678
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd27859zd2_1664));
																																				{
																																					obj_t
																																						BgL_bz00_6837;
																																					obj_t
																																						BgL_az00_6836;
																																					obj_t
																																						BgL_idz00_6835;
																																					BgL_idz00_6835
																																						=
																																						BgL_arg2633z00_1676;
																																					BgL_az00_6836
																																						=
																																						BgL_arg2635z00_1677;
																																					BgL_bz00_6837
																																						=
																																						BgL_arg2636z00_1678;
																																					BgL_bz00_73
																																						=
																																						BgL_bz00_6837;
																																					BgL_az00_72
																																						=
																																						BgL_az00_6836;
																																					BgL_idz00_71
																																						=
																																						BgL_idz00_6835;
																																					goto
																																						BgL_tagzd2109zd2_74;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																															}
																														}
																													}
																											}
																										}
																								}
																						}
																					}
																				}
																		}
																}
															else
																{	/* Expand/garith.scm 49 */
																	if (SYMBOLP(BgL_carzd26907zd2_1501))
																		{	/* Expand/garith.scm 49 */
																			obj_t BgL_carzd27928zd2_1684;

																			BgL_carzd27928zd2_1684 =
																				CAR(((obj_t) BgL_cdrzd26908zd2_1502));
																			if (INTEGERP(BgL_carzd27928zd2_1684))
																				{	/* Expand/garith.scm 49 */
																					if (NULLP(CDR(
																								((obj_t)
																									BgL_cdrzd26908zd2_1502))))
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_arg2642z00_1688;

																							BgL_arg2642z00_1688 =
																								CAR(((obj_t) BgL_xz00_4));
																							{
																								obj_t BgL_bz00_6852;
																								obj_t BgL_az00_6851;
																								obj_t BgL_idz00_6850;

																								BgL_idz00_6850 =
																									BgL_arg2642z00_1688;
																								BgL_az00_6851 =
																									BgL_carzd26907zd2_1501;
																								BgL_bz00_6852 =
																									BgL_carzd27928zd2_1684;
																								BgL_bz00_49 = BgL_bz00_6852;
																								BgL_az00_48 = BgL_az00_6851;
																								BgL_idz00_47 = BgL_idz00_6850;
																								goto BgL_tagzd2103zd2_50;
																							}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_cdrzd27996zd2_1690;

																					BgL_cdrzd27996zd2_1690 =
																						CDR(((obj_t) BgL_xz00_4));
																					{	/* Expand/garith.scm 49 */
																						obj_t BgL_cdrzd28004zd2_1691;

																						BgL_cdrzd28004zd2_1691 =
																							CDR(
																							((obj_t) BgL_cdrzd27996zd2_1690));
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd28009zd2_1692;

																							BgL_carzd28009zd2_1692 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd28004zd2_1691));
																							if (REALP(BgL_carzd28009zd2_1692))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd28004zd2_1691))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg2647z00_1696;
																											obj_t BgL_arg2648z00_1697;

																											BgL_arg2647z00_1696 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											BgL_arg2648z00_1697 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd27996zd2_1690));
																											{
																												double BgL_bz00_6871;
																												obj_t BgL_az00_6870;
																												obj_t BgL_idz00_6869;

																												BgL_idz00_6869 =
																													BgL_arg2647z00_1696;
																												BgL_az00_6870 =
																													BgL_arg2648z00_1697;
																												BgL_bz00_6871 =
																													REAL_TO_DOUBLE
																													(BgL_carzd28009zd2_1692);
																												BgL_bz00_53 =
																													BgL_bz00_6871;
																												BgL_az00_52 =
																													BgL_az00_6870;
																												BgL_idz00_51 =
																													BgL_idz00_6869;
																												goto
																													BgL_tagzd2104zd2_54;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd28077zd2_1700;

																									BgL_carzd28077zd2_1700 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd27996zd2_1690));
																									if (REALP
																										(BgL_carzd28077zd2_1700))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd28004zd2_1691))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg2653z00_1705;
																													obj_t
																														BgL_arg2654z00_1706;
																													BgL_arg2653z00_1705 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_arg2654z00_1706 =
																														CAR(((obj_t)
																															BgL_cdrzd28004zd2_1691));
																													{
																														obj_t BgL_bz00_6888;
																														double
																															BgL_az00_6886;
																														obj_t
																															BgL_idz00_6885;
																														BgL_idz00_6885 =
																															BgL_arg2653z00_1705;
																														BgL_az00_6886 =
																															REAL_TO_DOUBLE
																															(BgL_carzd28077zd2_1700);
																														BgL_bz00_6888 =
																															BgL_arg2654z00_1706;
																														BgL_bz00_57 =
																															BgL_bz00_6888;
																														BgL_az00_56 =
																															BgL_az00_6886;
																														BgL_idz00_55 =
																															BgL_idz00_6885;
																														goto
																															BgL_tagzd2105zd2_58;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_cdrzd28135zd2_1708;
																											BgL_cdrzd28135zd2_1708 =
																												CDR(((obj_t)
																													BgL_xz00_4));
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_carzd28142zd2_1709;
																												obj_t
																													BgL_cdrzd28143zd2_1710;
																												BgL_carzd28142zd2_1709 =
																													CAR(((obj_t)
																														BgL_cdrzd28135zd2_1708));
																												BgL_cdrzd28143zd2_1710 =
																													CDR(((obj_t)
																														BgL_cdrzd28135zd2_1708));
																												if (SYMBOLP
																													(BgL_carzd28142zd2_1709))
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd28150zd2_1712;
																														BgL_carzd28150zd2_1712
																															=
																															CAR(((obj_t)
																																BgL_cdrzd28143zd2_1710));
																														if (SYMBOLP
																															(BgL_carzd28150zd2_1712))
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd28143zd2_1710))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2662z00_1716;
																																		BgL_arg2662z00_1716
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		{
																																			obj_t
																																				BgL_bz00_6909;
																																			obj_t
																																				BgL_az00_6908;
																																			obj_t
																																				BgL_idz00_6907;
																																			BgL_idz00_6907
																																				=
																																				BgL_arg2662z00_1716;
																																			BgL_az00_6908
																																				=
																																				BgL_carzd28142zd2_1709;
																																			BgL_bz00_6909
																																				=
																																				BgL_carzd28150zd2_1712;
																																			BgL_bz00_61
																																				=
																																				BgL_bz00_6909;
																																			BgL_az00_60
																																				=
																																				BgL_az00_6908;
																																			BgL_idz00_59
																																				=
																																				BgL_idz00_6907;
																																			goto
																																				BgL_tagzd2106zd2_62;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd28143zd2_1710))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2669z00_1724;
																																		obj_t
																																			BgL_arg2670z00_1725;
																																		BgL_arg2669z00_1724
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg2670z00_1725
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd28143zd2_1710));
																																		{
																																			obj_t
																																				BgL_bz00_6920;
																																			obj_t
																																				BgL_az00_6919;
																																			obj_t
																																				BgL_idz00_6918;
																																			BgL_idz00_6918
																																				=
																																				BgL_arg2669z00_1724;
																																			BgL_az00_6919
																																				=
																																				BgL_carzd28142zd2_1709;
																																			BgL_bz00_6920
																																				=
																																				BgL_arg2670z00_1725;
																																			BgL_bz00_65
																																				=
																																				BgL_bz00_6920;
																																			BgL_az00_64
																																				=
																																				BgL_az00_6919;
																																			BgL_idz00_63
																																				=
																																				BgL_idz00_6918;
																																			goto
																																				BgL_tagzd2107zd2_66;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																													}
																												else
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_cdrzd28352zd2_1753;
																														BgL_cdrzd28352zd2_1753
																															=
																															CDR(((obj_t)
																																BgL_xz00_4));
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_cdrzd28360zd2_1754;
																															BgL_cdrzd28360zd2_1754
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd28352zd2_1753));
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd28365zd2_1755;
																																BgL_carzd28365zd2_1755
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd28360zd2_1754));
																																if (SYMBOLP
																																	(BgL_carzd28365zd2_1755))
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd28360zd2_1754))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2697z00_1759;
																																				obj_t
																																					BgL_arg2698z00_1760;
																																				BgL_arg2697z00_1759
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2698z00_1760
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd28352zd2_1753));
																																				{
																																					obj_t
																																						BgL_bz00_6939;
																																					obj_t
																																						BgL_az00_6938;
																																					obj_t
																																						BgL_idz00_6937;
																																					BgL_idz00_6937
																																						=
																																						BgL_arg2697z00_1759;
																																					BgL_az00_6938
																																						=
																																						BgL_arg2698z00_1760;
																																					BgL_bz00_6939
																																						=
																																						BgL_carzd28365zd2_1755;
																																					BgL_bz00_69
																																						=
																																						BgL_bz00_6939;
																																					BgL_az00_68
																																						=
																																						BgL_az00_6938;
																																					BgL_idz00_67
																																						=
																																						BgL_idz00_6937;
																																					goto
																																						BgL_tagzd2108zd2_70;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd28360zd2_1754))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2702z00_1766;
																																				obj_t
																																					BgL_arg2703z00_1767;
																																				obj_t
																																					BgL_arg2704z00_1768;
																																				BgL_arg2702z00_1766
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2703z00_1767
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd28352zd2_1753));
																																				BgL_arg2704z00_1768
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd28360zd2_1754));
																																				{
																																					obj_t
																																						BgL_bz00_6952;
																																					obj_t
																																						BgL_az00_6951;
																																					obj_t
																																						BgL_idz00_6950;
																																					BgL_idz00_6950
																																						=
																																						BgL_arg2702z00_1766;
																																					BgL_az00_6951
																																						=
																																						BgL_arg2703z00_1767;
																																					BgL_bz00_6952
																																						=
																																						BgL_arg2704z00_1768;
																																					BgL_bz00_73
																																						=
																																						BgL_bz00_6952;
																																					BgL_az00_72
																																						=
																																						BgL_az00_6951;
																																					BgL_idz00_71
																																						=
																																						BgL_idz00_6950;
																																					goto
																																						BgL_tagzd2109zd2_74;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																															}
																														}
																													}
																											}
																										}
																								}
																						}
																					}
																				}
																		}
																	else
																		{	/* Expand/garith.scm 49 */
																			obj_t BgL_cdrzd28416zd2_1770;

																			BgL_cdrzd28416zd2_1770 =
																				CDR(((obj_t) BgL_xz00_4));
																			{	/* Expand/garith.scm 49 */
																				obj_t BgL_cdrzd28423zd2_1771;

																				BgL_cdrzd28423zd2_1771 =
																					CDR(((obj_t) BgL_cdrzd28416zd2_1770));
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_carzd28427zd2_1772;

																					BgL_carzd28427zd2_1772 =
																						CAR(
																						((obj_t) BgL_cdrzd28423zd2_1771));
																					if (REALP(BgL_carzd28427zd2_1772))
																						{	/* Expand/garith.scm 49 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd28423zd2_1771))))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_arg2709z00_1776;
																									obj_t BgL_arg2710z00_1777;

																									BgL_arg2709z00_1776 =
																										CAR(((obj_t) BgL_xz00_4));
																									BgL_arg2710z00_1777 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd28416zd2_1770));
																									{
																										double BgL_bz00_6971;
																										obj_t BgL_az00_6970;
																										obj_t BgL_idz00_6969;

																										BgL_idz00_6969 =
																											BgL_arg2709z00_1776;
																										BgL_az00_6970 =
																											BgL_arg2710z00_1777;
																										BgL_bz00_6971 =
																											REAL_TO_DOUBLE
																											(BgL_carzd28427zd2_1772);
																										BgL_bz00_53 = BgL_bz00_6971;
																										BgL_az00_52 = BgL_az00_6970;
																										BgL_idz00_51 =
																											BgL_idz00_6969;
																										goto BgL_tagzd2104zd2_54;
																									}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd28493zd2_1780;

																							BgL_carzd28493zd2_1780 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd28416zd2_1770));
																							if (REALP(BgL_carzd28493zd2_1780))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd28423zd2_1771))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg2716z00_1785;
																											obj_t BgL_arg2717z00_1786;

																											BgL_arg2716z00_1785 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											BgL_arg2717z00_1786 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd28423zd2_1771));
																											{
																												obj_t BgL_bz00_6988;
																												double BgL_az00_6986;
																												obj_t BgL_idz00_6985;

																												BgL_idz00_6985 =
																													BgL_arg2716z00_1785;
																												BgL_az00_6986 =
																													REAL_TO_DOUBLE
																													(BgL_carzd28493zd2_1780);
																												BgL_bz00_6988 =
																													BgL_arg2717z00_1786;
																												BgL_bz00_57 =
																													BgL_bz00_6988;
																												BgL_az00_56 =
																													BgL_az00_6986;
																												BgL_idz00_55 =
																													BgL_idz00_6985;
																												goto
																													BgL_tagzd2105zd2_58;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_cdrzd28551zd2_1788;

																									BgL_cdrzd28551zd2_1788 =
																										CDR(((obj_t) BgL_xz00_4));
																									{	/* Expand/garith.scm 49 */
																										obj_t
																											BgL_carzd28558zd2_1789;
																										obj_t
																											BgL_cdrzd28559zd2_1790;
																										BgL_carzd28558zd2_1789 =
																											CAR(((obj_t)
																												BgL_cdrzd28551zd2_1788));
																										BgL_cdrzd28559zd2_1790 =
																											CDR(((obj_t)
																												BgL_cdrzd28551zd2_1788));
																										if (SYMBOLP
																											(BgL_carzd28558zd2_1789))
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_carzd28566zd2_1792;
																												BgL_carzd28566zd2_1792 =
																													CAR(((obj_t)
																														BgL_cdrzd28559zd2_1790));
																												if (SYMBOLP
																													(BgL_carzd28566zd2_1792))
																													{	/* Expand/garith.scm 49 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_cdrzd28559zd2_1790))))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_arg2724z00_1796;
																																BgL_arg2724z00_1796
																																	=
																																	CAR(((obj_t)
																																		BgL_xz00_4));
																																{
																																	obj_t
																																		BgL_bz00_7009;
																																	obj_t
																																		BgL_az00_7008;
																																	obj_t
																																		BgL_idz00_7007;
																																	BgL_idz00_7007
																																		=
																																		BgL_arg2724z00_1796;
																																	BgL_az00_7008
																																		=
																																		BgL_carzd28558zd2_1789;
																																	BgL_bz00_7009
																																		=
																																		BgL_carzd28566zd2_1792;
																																	BgL_bz00_61 =
																																		BgL_bz00_7009;
																																	BgL_az00_60 =
																																		BgL_az00_7008;
																																	BgL_idz00_59 =
																																		BgL_idz00_7007;
																																	goto
																																		BgL_tagzd2106zd2_62;
																																}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																return BFALSE;
																															}
																													}
																												else
																													{	/* Expand/garith.scm 49 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_cdrzd28559zd2_1790))))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_arg2729z00_1804;
																																obj_t
																																	BgL_arg2731z00_1805;
																																BgL_arg2729z00_1804
																																	=
																																	CAR(((obj_t)
																																		BgL_xz00_4));
																																BgL_arg2731z00_1805
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd28559zd2_1790));
																																{
																																	obj_t
																																		BgL_bz00_7020;
																																	obj_t
																																		BgL_az00_7019;
																																	obj_t
																																		BgL_idz00_7018;
																																	BgL_idz00_7018
																																		=
																																		BgL_arg2729z00_1804;
																																	BgL_az00_7019
																																		=
																																		BgL_carzd28558zd2_1789;
																																	BgL_bz00_7020
																																		=
																																		BgL_arg2731z00_1805;
																																	BgL_bz00_65 =
																																		BgL_bz00_7020;
																																	BgL_az00_64 =
																																		BgL_az00_7019;
																																	BgL_idz00_63 =
																																		BgL_idz00_7018;
																																	goto
																																		BgL_tagzd2107zd2_66;
																																}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																return BFALSE;
																															}
																													}
																											}
																										else
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_cdrzd28768zd2_1833;
																												BgL_cdrzd28768zd2_1833 =
																													CDR(((obj_t)
																														BgL_xz00_4));
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd28776zd2_1834;
																													BgL_cdrzd28776zd2_1834
																														=
																														CDR(((obj_t)
																															BgL_cdrzd28768zd2_1833));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd28781zd2_1835;
																														BgL_carzd28781zd2_1835
																															=
																															CAR(((obj_t)
																																BgL_cdrzd28776zd2_1834));
																														if (SYMBOLP
																															(BgL_carzd28781zd2_1835))
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd28776zd2_1834))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2758z00_1839;
																																		obj_t
																																			BgL_arg2759z00_1840;
																																		BgL_arg2758z00_1839
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg2759z00_1840
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd28768zd2_1833));
																																		{
																																			obj_t
																																				BgL_bz00_7039;
																																			obj_t
																																				BgL_az00_7038;
																																			obj_t
																																				BgL_idz00_7037;
																																			BgL_idz00_7037
																																				=
																																				BgL_arg2758z00_1839;
																																			BgL_az00_7038
																																				=
																																				BgL_arg2759z00_1840;
																																			BgL_bz00_7039
																																				=
																																				BgL_carzd28781zd2_1835;
																																			BgL_bz00_69
																																				=
																																				BgL_bz00_7039;
																																			BgL_az00_68
																																				=
																																				BgL_az00_7038;
																																			BgL_idz00_67
																																				=
																																				BgL_idz00_7037;
																																			goto
																																				BgL_tagzd2108zd2_70;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd28776zd2_1834))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2764z00_1846;
																																		obj_t
																																			BgL_arg2765z00_1847;
																																		obj_t
																																			BgL_arg2766z00_1848;
																																		BgL_arg2764z00_1846
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg2765z00_1847
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd28768zd2_1833));
																																		BgL_arg2766z00_1848
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd28776zd2_1834));
																																		{
																																			obj_t
																																				BgL_bz00_7052;
																																			obj_t
																																				BgL_az00_7051;
																																			obj_t
																																				BgL_idz00_7050;
																																			BgL_idz00_7050
																																				=
																																				BgL_arg2764z00_1846;
																																			BgL_az00_7051
																																				=
																																				BgL_arg2765z00_1847;
																																			BgL_bz00_7052
																																				=
																																				BgL_arg2766z00_1848;
																																			BgL_bz00_73
																																				=
																																				BgL_bz00_7052;
																																			BgL_az00_72
																																				=
																																				BgL_az00_7051;
																																			BgL_idz00_71
																																				=
																																				BgL_idz00_7050;
																																			goto
																																				BgL_tagzd2109zd2_74;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																													}
																												}
																											}
																									}
																								}
																						}
																				}
																			}
																		}
																}
														}
													}
											}
										else
											{	/* Expand/garith.scm 49 */
												return BFALSE;
											}
									}
								else
									{	/* Expand/garith.scm 49 */
										obj_t BgL_cdrzd28887zd2_1850;

										BgL_cdrzd28887zd2_1850 = CDR(((obj_t) BgL_xz00_4));
										{	/* Expand/garith.scm 49 */
											obj_t BgL_carzd28892zd2_1851;
											obj_t BgL_cdrzd28893zd2_1852;

											BgL_carzd28892zd2_1851 =
												CAR(((obj_t) BgL_cdrzd28887zd2_1850));
											BgL_cdrzd28893zd2_1852 =
												CDR(((obj_t) BgL_cdrzd28887zd2_1850));
											if (INTEGERP(BgL_carzd28892zd2_1851))
												{	/* Expand/garith.scm 49 */
													if (PAIRP(BgL_cdrzd28893zd2_1852))
														{	/* Expand/garith.scm 49 */
															obj_t BgL_carzd28897zd2_1855;

															BgL_carzd28897zd2_1855 =
																CAR(BgL_cdrzd28893zd2_1852);
															if (SYMBOLP(BgL_carzd28897zd2_1855))
																{	/* Expand/garith.scm 49 */
																	if (NULLP(CDR(BgL_cdrzd28893zd2_1852)))
																		{	/* Expand/garith.scm 49 */
																			obj_t BgL_arg2773z00_1859;

																			BgL_arg2773z00_1859 =
																				CAR(((obj_t) BgL_xz00_4));
																			{
																				obj_t BgL_bz00_7073;
																				obj_t BgL_az00_7072;
																				obj_t BgL_idz00_7071;

																				BgL_idz00_7071 = BgL_arg2773z00_1859;
																				BgL_az00_7072 = BgL_carzd28892zd2_1851;
																				BgL_bz00_7073 = BgL_carzd28897zd2_1855;
																				BgL_bz00_45 = BgL_bz00_7073;
																				BgL_az00_44 = BgL_az00_7072;
																				BgL_idz00_43 = BgL_idz00_7071;
																				goto BgL_tagzd2102zd2_46;
																			}
																		}
																	else
																		{	/* Expand/garith.scm 49 */
																			return BFALSE;
																		}
																}
															else
																{	/* Expand/garith.scm 49 */
																	if (SYMBOLP(BgL_carzd28892zd2_1851))
																		{	/* Expand/garith.scm 49 */
																			obj_t BgL_carzd28989zd2_1865;

																			BgL_carzd28989zd2_1865 =
																				CAR(((obj_t) BgL_cdrzd28893zd2_1852));
																			if (INTEGERP(BgL_carzd28989zd2_1865))
																				{	/* Expand/garith.scm 49 */
																					if (NULLP(CDR(
																								((obj_t)
																									BgL_cdrzd28893zd2_1852))))
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_arg2780z00_1869;

																							BgL_arg2780z00_1869 =
																								CAR(((obj_t) BgL_xz00_4));
																							{
																								obj_t BgL_bz00_7088;
																								obj_t BgL_az00_7087;
																								obj_t BgL_idz00_7086;

																								BgL_idz00_7086 =
																									BgL_arg2780z00_1869;
																								BgL_az00_7087 =
																									BgL_carzd28892zd2_1851;
																								BgL_bz00_7088 =
																									BgL_carzd28989zd2_1865;
																								BgL_bz00_49 = BgL_bz00_7088;
																								BgL_az00_48 = BgL_az00_7087;
																								BgL_idz00_47 = BgL_idz00_7086;
																								goto BgL_tagzd2103zd2_50;
																							}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_cdrzd29059zd2_1871;

																					BgL_cdrzd29059zd2_1871 =
																						CDR(((obj_t) BgL_xz00_4));
																					{	/* Expand/garith.scm 49 */
																						obj_t BgL_cdrzd29067zd2_1872;

																						BgL_cdrzd29067zd2_1872 =
																							CDR(
																							((obj_t) BgL_cdrzd29059zd2_1871));
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd29072zd2_1873;

																							BgL_carzd29072zd2_1873 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd29067zd2_1872));
																							if (REALP(BgL_carzd29072zd2_1873))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd29067zd2_1872))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg2786z00_1877;
																											obj_t BgL_arg2787z00_1878;

																											BgL_arg2786z00_1877 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											BgL_arg2787z00_1878 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd29059zd2_1871));
																											{
																												double BgL_bz00_7107;
																												obj_t BgL_az00_7106;
																												obj_t BgL_idz00_7105;

																												BgL_idz00_7105 =
																													BgL_arg2786z00_1877;
																												BgL_az00_7106 =
																													BgL_arg2787z00_1878;
																												BgL_bz00_7107 =
																													REAL_TO_DOUBLE
																													(BgL_carzd29072zd2_1873);
																												BgL_bz00_53 =
																													BgL_bz00_7107;
																												BgL_az00_52 =
																													BgL_az00_7106;
																												BgL_idz00_51 =
																													BgL_idz00_7105;
																												goto
																													BgL_tagzd2104zd2_54;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd29140zd2_1881;

																									BgL_carzd29140zd2_1881 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd29059zd2_1871));
																									if (REALP
																										(BgL_carzd29140zd2_1881))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd29067zd2_1872))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg2799z00_1886;
																													obj_t
																														BgL_arg2800z00_1887;
																													BgL_arg2799z00_1886 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_arg2800z00_1887 =
																														CAR(((obj_t)
																															BgL_cdrzd29067zd2_1872));
																													{
																														obj_t BgL_bz00_7124;
																														double
																															BgL_az00_7122;
																														obj_t
																															BgL_idz00_7121;
																														BgL_idz00_7121 =
																															BgL_arg2799z00_1886;
																														BgL_az00_7122 =
																															REAL_TO_DOUBLE
																															(BgL_carzd29140zd2_1881);
																														BgL_bz00_7124 =
																															BgL_arg2800z00_1887;
																														BgL_bz00_57 =
																															BgL_bz00_7124;
																														BgL_az00_56 =
																															BgL_az00_7122;
																														BgL_idz00_55 =
																															BgL_idz00_7121;
																														goto
																															BgL_tagzd2105zd2_58;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_cdrzd29198zd2_1889;
																											BgL_cdrzd29198zd2_1889 =
																												CDR(((obj_t)
																													BgL_xz00_4));
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_carzd29205zd2_1890;
																												obj_t
																													BgL_cdrzd29206zd2_1891;
																												BgL_carzd29205zd2_1890 =
																													CAR(((obj_t)
																														BgL_cdrzd29198zd2_1889));
																												BgL_cdrzd29206zd2_1891 =
																													CDR(((obj_t)
																														BgL_cdrzd29198zd2_1889));
																												if (SYMBOLP
																													(BgL_carzd29205zd2_1890))
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd29213zd2_1893;
																														BgL_carzd29213zd2_1893
																															=
																															CAR(((obj_t)
																																BgL_cdrzd29206zd2_1891));
																														if (SYMBOLP
																															(BgL_carzd29213zd2_1893))
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd29206zd2_1891))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2811z00_1897;
																																		BgL_arg2811z00_1897
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		{
																																			obj_t
																																				BgL_bz00_7145;
																																			obj_t
																																				BgL_az00_7144;
																																			obj_t
																																				BgL_idz00_7143;
																																			BgL_idz00_7143
																																				=
																																				BgL_arg2811z00_1897;
																																			BgL_az00_7144
																																				=
																																				BgL_carzd29205zd2_1890;
																																			BgL_bz00_7145
																																				=
																																				BgL_carzd29213zd2_1893;
																																			BgL_bz00_61
																																				=
																																				BgL_bz00_7145;
																																			BgL_az00_60
																																				=
																																				BgL_az00_7144;
																																			BgL_idz00_59
																																				=
																																				BgL_idz00_7143;
																																			goto
																																				BgL_tagzd2106zd2_62;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd29206zd2_1891))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2818z00_1905;
																																		obj_t
																																			BgL_arg2820z00_1906;
																																		BgL_arg2818z00_1905
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg2820z00_1906
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd29206zd2_1891));
																																		{
																																			obj_t
																																				BgL_bz00_7156;
																																			obj_t
																																				BgL_az00_7155;
																																			obj_t
																																				BgL_idz00_7154;
																																			BgL_idz00_7154
																																				=
																																				BgL_arg2818z00_1905;
																																			BgL_az00_7155
																																				=
																																				BgL_carzd29205zd2_1890;
																																			BgL_bz00_7156
																																				=
																																				BgL_arg2820z00_1906;
																																			BgL_bz00_65
																																				=
																																				BgL_bz00_7156;
																																			BgL_az00_64
																																				=
																																				BgL_az00_7155;
																																			BgL_idz00_63
																																				=
																																				BgL_idz00_7154;
																																			goto
																																				BgL_tagzd2107zd2_66;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																													}
																												else
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_cdrzd29415zd2_1934;
																														BgL_cdrzd29415zd2_1934
																															=
																															CDR(((obj_t)
																																BgL_xz00_4));
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_cdrzd29423zd2_1935;
																															BgL_cdrzd29423zd2_1935
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd29415zd2_1934));
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_carzd29428zd2_1936;
																																BgL_carzd29428zd2_1936
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd29423zd2_1935));
																																if (SYMBOLP
																																	(BgL_carzd29428zd2_1936))
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd29423zd2_1935))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2848z00_1940;
																																				obj_t
																																					BgL_arg2856z00_1941;
																																				BgL_arg2848z00_1940
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2856z00_1941
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd29415zd2_1934));
																																				{
																																					obj_t
																																						BgL_bz00_7175;
																																					obj_t
																																						BgL_az00_7174;
																																					obj_t
																																						BgL_idz00_7173;
																																					BgL_idz00_7173
																																						=
																																						BgL_arg2848z00_1940;
																																					BgL_az00_7174
																																						=
																																						BgL_arg2856z00_1941;
																																					BgL_bz00_7175
																																						=
																																						BgL_carzd29428zd2_1936;
																																					BgL_bz00_69
																																						=
																																						BgL_bz00_7175;
																																					BgL_az00_68
																																						=
																																						BgL_az00_7174;
																																					BgL_idz00_67
																																						=
																																						BgL_idz00_7173;
																																					goto
																																						BgL_tagzd2108zd2_70;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd29423zd2_1935))))
																																			{	/* Expand/garith.scm 49 */
																																				obj_t
																																					BgL_arg2864z00_1947;
																																				obj_t
																																					BgL_arg2870z00_1948;
																																				obj_t
																																					BgL_arg2871z00_1949;
																																				BgL_arg2864z00_1947
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_xz00_4));
																																				BgL_arg2870z00_1948
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd29415zd2_1934));
																																				BgL_arg2871z00_1949
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd29423zd2_1935));
																																				{
																																					obj_t
																																						BgL_bz00_7188;
																																					obj_t
																																						BgL_az00_7187;
																																					obj_t
																																						BgL_idz00_7186;
																																					BgL_idz00_7186
																																						=
																																						BgL_arg2864z00_1947;
																																					BgL_az00_7187
																																						=
																																						BgL_arg2870z00_1948;
																																					BgL_bz00_7188
																																						=
																																						BgL_arg2871z00_1949;
																																					BgL_bz00_73
																																						=
																																						BgL_bz00_7188;
																																					BgL_az00_72
																																						=
																																						BgL_az00_7187;
																																					BgL_idz00_71
																																						=
																																						BgL_idz00_7186;
																																					goto
																																						BgL_tagzd2109zd2_74;
																																				}
																																			}
																																		else
																																			{	/* Expand/garith.scm 49 */
																																				return
																																					BFALSE;
																																			}
																																	}
																															}
																														}
																													}
																											}
																										}
																								}
																						}
																					}
																				}
																		}
																	else
																		{	/* Expand/garith.scm 49 */
																			obj_t BgL_cdrzd29480zd2_1951;

																			BgL_cdrzd29480zd2_1951 =
																				CDR(((obj_t) BgL_xz00_4));
																			{	/* Expand/garith.scm 49 */
																				obj_t BgL_cdrzd29488zd2_1952;

																				BgL_cdrzd29488zd2_1952 =
																					CDR(((obj_t) BgL_cdrzd29480zd2_1951));
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_carzd29493zd2_1953;

																					BgL_carzd29493zd2_1953 =
																						CAR(
																						((obj_t) BgL_cdrzd29488zd2_1952));
																					if (REALP(BgL_carzd29493zd2_1953))
																						{	/* Expand/garith.scm 49 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd29488zd2_1952))))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_arg2876z00_1957;
																									obj_t BgL_arg2877z00_1958;

																									BgL_arg2876z00_1957 =
																										CAR(((obj_t) BgL_xz00_4));
																									BgL_arg2877z00_1958 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd29480zd2_1951));
																									{
																										double BgL_bz00_7207;
																										obj_t BgL_az00_7206;
																										obj_t BgL_idz00_7205;

																										BgL_idz00_7205 =
																											BgL_arg2876z00_1957;
																										BgL_az00_7206 =
																											BgL_arg2877z00_1958;
																										BgL_bz00_7207 =
																											REAL_TO_DOUBLE
																											(BgL_carzd29493zd2_1953);
																										BgL_bz00_53 = BgL_bz00_7207;
																										BgL_az00_52 = BgL_az00_7206;
																										BgL_idz00_51 =
																											BgL_idz00_7205;
																										goto BgL_tagzd2104zd2_54;
																									}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd29561zd2_1961;

																							BgL_carzd29561zd2_1961 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd29480zd2_1951));
																							if (REALP(BgL_carzd29561zd2_1961))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd29488zd2_1952))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg2882z00_1966;
																											obj_t BgL_arg2883z00_1967;

																											BgL_arg2882z00_1966 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											BgL_arg2883z00_1967 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd29488zd2_1952));
																											{
																												obj_t BgL_bz00_7224;
																												double BgL_az00_7222;
																												obj_t BgL_idz00_7221;

																												BgL_idz00_7221 =
																													BgL_arg2882z00_1966;
																												BgL_az00_7222 =
																													REAL_TO_DOUBLE
																													(BgL_carzd29561zd2_1961);
																												BgL_bz00_7224 =
																													BgL_arg2883z00_1967;
																												BgL_bz00_57 =
																													BgL_bz00_7224;
																												BgL_az00_56 =
																													BgL_az00_7222;
																												BgL_idz00_55 =
																													BgL_idz00_7221;
																												goto
																													BgL_tagzd2105zd2_58;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_cdrzd29619zd2_1969;

																									BgL_cdrzd29619zd2_1969 =
																										CDR(((obj_t) BgL_xz00_4));
																									{	/* Expand/garith.scm 49 */
																										obj_t
																											BgL_carzd29626zd2_1970;
																										obj_t
																											BgL_cdrzd29627zd2_1971;
																										BgL_carzd29626zd2_1970 =
																											CAR(((obj_t)
																												BgL_cdrzd29619zd2_1969));
																										BgL_cdrzd29627zd2_1971 =
																											CDR(((obj_t)
																												BgL_cdrzd29619zd2_1969));
																										if (SYMBOLP
																											(BgL_carzd29626zd2_1970))
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_carzd29634zd2_1973;
																												BgL_carzd29634zd2_1973 =
																													CAR(((obj_t)
																														BgL_cdrzd29627zd2_1971));
																												if (SYMBOLP
																													(BgL_carzd29634zd2_1973))
																													{	/* Expand/garith.scm 49 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_cdrzd29627zd2_1971))))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_arg2892z00_1977;
																																BgL_arg2892z00_1977
																																	=
																																	CAR(((obj_t)
																																		BgL_xz00_4));
																																{
																																	obj_t
																																		BgL_bz00_7245;
																																	obj_t
																																		BgL_az00_7244;
																																	obj_t
																																		BgL_idz00_7243;
																																	BgL_idz00_7243
																																		=
																																		BgL_arg2892z00_1977;
																																	BgL_az00_7244
																																		=
																																		BgL_carzd29626zd2_1970;
																																	BgL_bz00_7245
																																		=
																																		BgL_carzd29634zd2_1973;
																																	BgL_bz00_61 =
																																		BgL_bz00_7245;
																																	BgL_az00_60 =
																																		BgL_az00_7244;
																																	BgL_idz00_59 =
																																		BgL_idz00_7243;
																																	goto
																																		BgL_tagzd2106zd2_62;
																																}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																return BFALSE;
																															}
																													}
																												else
																													{	/* Expand/garith.scm 49 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_cdrzd29627zd2_1971))))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_arg2897z00_1985;
																																obj_t
																																	BgL_arg2898z00_1986;
																																BgL_arg2897z00_1985
																																	=
																																	CAR(((obj_t)
																																		BgL_xz00_4));
																																BgL_arg2898z00_1986
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd29627zd2_1971));
																																{
																																	obj_t
																																		BgL_bz00_7256;
																																	obj_t
																																		BgL_az00_7255;
																																	obj_t
																																		BgL_idz00_7254;
																																	BgL_idz00_7254
																																		=
																																		BgL_arg2897z00_1985;
																																	BgL_az00_7255
																																		=
																																		BgL_carzd29626zd2_1970;
																																	BgL_bz00_7256
																																		=
																																		BgL_arg2898z00_1986;
																																	BgL_bz00_65 =
																																		BgL_bz00_7256;
																																	BgL_az00_64 =
																																		BgL_az00_7255;
																																	BgL_idz00_63 =
																																		BgL_idz00_7254;
																																	goto
																																		BgL_tagzd2107zd2_66;
																																}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																return BFALSE;
																															}
																													}
																											}
																										else
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_cdrzd29836zd2_2014;
																												BgL_cdrzd29836zd2_2014 =
																													CDR(((obj_t)
																														BgL_xz00_4));
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd29844zd2_2015;
																													BgL_cdrzd29844zd2_2015
																														=
																														CDR(((obj_t)
																															BgL_cdrzd29836zd2_2014));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd29849zd2_2016;
																														BgL_carzd29849zd2_2016
																															=
																															CAR(((obj_t)
																																BgL_cdrzd29844zd2_2015));
																														if (SYMBOLP
																															(BgL_carzd29849zd2_2016))
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd29844zd2_2015))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2927z00_2020;
																																		obj_t
																																			BgL_arg2928z00_2021;
																																		BgL_arg2927z00_2020
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg2928z00_2021
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd29836zd2_2014));
																																		{
																																			obj_t
																																				BgL_bz00_7275;
																																			obj_t
																																				BgL_az00_7274;
																																			obj_t
																																				BgL_idz00_7273;
																																			BgL_idz00_7273
																																				=
																																				BgL_arg2927z00_2020;
																																			BgL_az00_7274
																																				=
																																				BgL_arg2928z00_2021;
																																			BgL_bz00_7275
																																				=
																																				BgL_carzd29849zd2_2016;
																																			BgL_bz00_69
																																				=
																																				BgL_bz00_7275;
																																			BgL_az00_68
																																				=
																																				BgL_az00_7274;
																																			BgL_idz00_67
																																				=
																																				BgL_idz00_7273;
																																			goto
																																				BgL_tagzd2108zd2_70;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd29844zd2_2015))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg2932z00_2027;
																																		obj_t
																																			BgL_arg2933z00_2028;
																																		obj_t
																																			BgL_arg2934z00_2029;
																																		BgL_arg2932z00_2027
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg2933z00_2028
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd29836zd2_2014));
																																		BgL_arg2934z00_2029
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd29844zd2_2015));
																																		{
																																			obj_t
																																				BgL_bz00_7288;
																																			obj_t
																																				BgL_az00_7287;
																																			obj_t
																																				BgL_idz00_7286;
																																			BgL_idz00_7286
																																				=
																																				BgL_arg2932z00_2027;
																																			BgL_az00_7287
																																				=
																																				BgL_arg2933z00_2028;
																																			BgL_bz00_7288
																																				=
																																				BgL_arg2934z00_2029;
																																			BgL_bz00_73
																																				=
																																				BgL_bz00_7288;
																																			BgL_az00_72
																																				=
																																				BgL_az00_7287;
																																			BgL_idz00_71
																																				=
																																				BgL_idz00_7286;
																																			goto
																																				BgL_tagzd2109zd2_74;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																													}
																												}
																											}
																									}
																								}
																						}
																				}
																			}
																		}
																}
														}
													else
														{	/* Expand/garith.scm 49 */
															return BFALSE;
														}
												}
											else
												{	/* Expand/garith.scm 49 */
													if (SYMBOLP(BgL_carzd28892zd2_1851))
														{	/* Expand/garith.scm 49 */
															if (PAIRP(BgL_cdrzd28893zd2_1852))
																{	/* Expand/garith.scm 49 */
																	obj_t BgL_carzd29969zd2_2036;

																	BgL_carzd29969zd2_2036 =
																		CAR(BgL_cdrzd28893zd2_1852);
																	if (INTEGERP(BgL_carzd29969zd2_2036))
																		{	/* Expand/garith.scm 49 */
																			if (NULLP(CDR(BgL_cdrzd28893zd2_1852)))
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_arg2942z00_2040;

																					BgL_arg2942z00_2040 =
																						CAR(((obj_t) BgL_xz00_4));
																					{
																						obj_t BgL_bz00_7303;
																						obj_t BgL_az00_7302;
																						obj_t BgL_idz00_7301;

																						BgL_idz00_7301 =
																							BgL_arg2942z00_2040;
																						BgL_az00_7302 =
																							BgL_carzd28892zd2_1851;
																						BgL_bz00_7303 =
																							BgL_carzd29969zd2_2036;
																						BgL_bz00_49 = BgL_bz00_7303;
																						BgL_az00_48 = BgL_az00_7302;
																						BgL_idz00_47 = BgL_idz00_7301;
																						goto BgL_tagzd2103zd2_50;
																					}
																				}
																			else
																				{	/* Expand/garith.scm 49 */
																					return BFALSE;
																				}
																		}
																	else
																		{	/* Expand/garith.scm 49 */
																			obj_t BgL_cdrzd210037zd2_2042;

																			BgL_cdrzd210037zd2_2042 =
																				CDR(((obj_t) BgL_xz00_4));
																			{	/* Expand/garith.scm 49 */
																				obj_t BgL_cdrzd210045zd2_2043;

																				BgL_cdrzd210045zd2_2043 =
																					CDR(
																					((obj_t) BgL_cdrzd210037zd2_2042));
																				{	/* Expand/garith.scm 49 */
																					obj_t BgL_carzd210050zd2_2044;

																					BgL_carzd210050zd2_2044 =
																						CAR(
																						((obj_t) BgL_cdrzd210045zd2_2043));
																					if (REALP(BgL_carzd210050zd2_2044))
																						{	/* Expand/garith.scm 49 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd210045zd2_2043))))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_arg2956z00_2048;
																									obj_t BgL_arg2966z00_2049;

																									BgL_arg2956z00_2048 =
																										CAR(((obj_t) BgL_xz00_4));
																									BgL_arg2966z00_2049 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd210037zd2_2042));
																									{
																										double BgL_bz00_7322;
																										obj_t BgL_az00_7321;
																										obj_t BgL_idz00_7320;

																										BgL_idz00_7320 =
																											BgL_arg2956z00_2048;
																										BgL_az00_7321 =
																											BgL_arg2966z00_2049;
																										BgL_bz00_7322 =
																											REAL_TO_DOUBLE
																											(BgL_carzd210050zd2_2044);
																										BgL_bz00_53 = BgL_bz00_7322;
																										BgL_az00_52 = BgL_az00_7321;
																										BgL_idz00_51 =
																											BgL_idz00_7320;
																										goto BgL_tagzd2104zd2_54;
																									}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd210118zd2_2052;

																							BgL_carzd210118zd2_2052 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd210037zd2_2042));
																							if (REALP
																								(BgL_carzd210118zd2_2052))
																								{	/* Expand/garith.scm 49 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd210045zd2_2043))))
																										{	/* Expand/garith.scm 49 */
																											obj_t BgL_arg2974z00_2057;
																											obj_t BgL_arg2975z00_2058;

																											BgL_arg2974z00_2057 =
																												CAR(
																												((obj_t) BgL_xz00_4));
																											BgL_arg2975z00_2058 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd210045zd2_2043));
																											{
																												obj_t BgL_bz00_7339;
																												double BgL_az00_7337;
																												obj_t BgL_idz00_7336;

																												BgL_idz00_7336 =
																													BgL_arg2974z00_2057;
																												BgL_az00_7337 =
																													REAL_TO_DOUBLE
																													(BgL_carzd210118zd2_2052);
																												BgL_bz00_7339 =
																													BgL_arg2975z00_2058;
																												BgL_bz00_57 =
																													BgL_bz00_7339;
																												BgL_az00_56 =
																													BgL_az00_7337;
																												BgL_idz00_55 =
																													BgL_idz00_7336;
																												goto
																													BgL_tagzd2105zd2_58;
																											}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_cdrzd210176zd2_2060;

																									BgL_cdrzd210176zd2_2060 =
																										CDR(((obj_t) BgL_xz00_4));
																									{	/* Expand/garith.scm 49 */
																										obj_t
																											BgL_carzd210183zd2_2061;
																										obj_t
																											BgL_cdrzd210184zd2_2062;
																										BgL_carzd210183zd2_2061 =
																											CAR(((obj_t)
																												BgL_cdrzd210176zd2_2060));
																										BgL_cdrzd210184zd2_2062 =
																											CDR(((obj_t)
																												BgL_cdrzd210176zd2_2060));
																										if (SYMBOLP
																											(BgL_carzd210183zd2_2061))
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_carzd210191zd2_2064;
																												BgL_carzd210191zd2_2064
																													=
																													CAR(((obj_t)
																														BgL_cdrzd210184zd2_2062));
																												if (SYMBOLP
																													(BgL_carzd210191zd2_2064))
																													{	/* Expand/garith.scm 49 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_cdrzd210184zd2_2062))))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_arg2981z00_2068;
																																BgL_arg2981z00_2068
																																	=
																																	CAR(((obj_t)
																																		BgL_xz00_4));
																																{
																																	obj_t
																																		BgL_bz00_7360;
																																	obj_t
																																		BgL_az00_7359;
																																	obj_t
																																		BgL_idz00_7358;
																																	BgL_idz00_7358
																																		=
																																		BgL_arg2981z00_2068;
																																	BgL_az00_7359
																																		=
																																		BgL_carzd210183zd2_2061;
																																	BgL_bz00_7360
																																		=
																																		BgL_carzd210191zd2_2064;
																																	BgL_bz00_61 =
																																		BgL_bz00_7360;
																																	BgL_az00_60 =
																																		BgL_az00_7359;
																																	BgL_idz00_59 =
																																		BgL_idz00_7358;
																																	goto
																																		BgL_tagzd2106zd2_62;
																																}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																return BFALSE;
																															}
																													}
																												else
																													{	/* Expand/garith.scm 49 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_cdrzd210184zd2_2062))))
																															{	/* Expand/garith.scm 49 */
																																obj_t
																																	BgL_arg2989z00_2076;
																																obj_t
																																	BgL_arg2990z00_2077;
																																BgL_arg2989z00_2076
																																	=
																																	CAR(((obj_t)
																																		BgL_xz00_4));
																																BgL_arg2990z00_2077
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd210184zd2_2062));
																																{
																																	obj_t
																																		BgL_bz00_7371;
																																	obj_t
																																		BgL_az00_7370;
																																	obj_t
																																		BgL_idz00_7369;
																																	BgL_idz00_7369
																																		=
																																		BgL_arg2989z00_2076;
																																	BgL_az00_7370
																																		=
																																		BgL_carzd210183zd2_2061;
																																	BgL_bz00_7371
																																		=
																																		BgL_arg2990z00_2077;
																																	BgL_bz00_65 =
																																		BgL_bz00_7371;
																																	BgL_az00_64 =
																																		BgL_az00_7370;
																																	BgL_idz00_63 =
																																		BgL_idz00_7369;
																																	goto
																																		BgL_tagzd2107zd2_66;
																																}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																return BFALSE;
																															}
																													}
																											}
																										else
																											{	/* Expand/garith.scm 49 */
																												obj_t
																													BgL_cdrzd210393zd2_2105;
																												BgL_cdrzd210393zd2_2105
																													=
																													CDR(((obj_t)
																														BgL_xz00_4));
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_cdrzd210401zd2_2106;
																													BgL_cdrzd210401zd2_2106
																														=
																														CDR(((obj_t)
																															BgL_cdrzd210393zd2_2105));
																													{	/* Expand/garith.scm 49 */
																														obj_t
																															BgL_carzd210406zd2_2107;
																														BgL_carzd210406zd2_2107
																															=
																															CAR(((obj_t)
																																BgL_cdrzd210401zd2_2106));
																														if (SYMBOLP
																															(BgL_carzd210406zd2_2107))
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd210401zd2_2106))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg3020z00_2111;
																																		obj_t
																																			BgL_arg3021z00_2112;
																																		BgL_arg3020z00_2111
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg3021z00_2112
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd210393zd2_2105));
																																		{
																																			obj_t
																																				BgL_bz00_7390;
																																			obj_t
																																				BgL_az00_7389;
																																			obj_t
																																				BgL_idz00_7388;
																																			BgL_idz00_7388
																																				=
																																				BgL_arg3020z00_2111;
																																			BgL_az00_7389
																																				=
																																				BgL_arg3021z00_2112;
																																			BgL_bz00_7390
																																				=
																																				BgL_carzd210406zd2_2107;
																																			BgL_bz00_69
																																				=
																																				BgL_bz00_7390;
																																			BgL_az00_68
																																				=
																																				BgL_az00_7389;
																																			BgL_idz00_67
																																				=
																																				BgL_idz00_7388;
																																			goto
																																				BgL_tagzd2108zd2_70;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																														else
																															{	/* Expand/garith.scm 49 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd210401zd2_2106))))
																																	{	/* Expand/garith.scm 49 */
																																		obj_t
																																			BgL_arg3026z00_2118;
																																		obj_t
																																			BgL_arg3027z00_2119;
																																		obj_t
																																			BgL_arg3029z00_2120;
																																		BgL_arg3026z00_2118
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_xz00_4));
																																		BgL_arg3027z00_2119
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd210393zd2_2105));
																																		BgL_arg3029z00_2120
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd210401zd2_2106));
																																		{
																																			obj_t
																																				BgL_bz00_7403;
																																			obj_t
																																				BgL_az00_7402;
																																			obj_t
																																				BgL_idz00_7401;
																																			BgL_idz00_7401
																																				=
																																				BgL_arg3026z00_2118;
																																			BgL_az00_7402
																																				=
																																				BgL_arg3027z00_2119;
																																			BgL_bz00_7403
																																				=
																																				BgL_arg3029z00_2120;
																																			BgL_bz00_73
																																				=
																																				BgL_bz00_7403;
																																			BgL_az00_72
																																				=
																																				BgL_az00_7402;
																																			BgL_idz00_71
																																				=
																																				BgL_idz00_7401;
																																			goto
																																				BgL_tagzd2109zd2_74;
																																		}
																																	}
																																else
																																	{	/* Expand/garith.scm 49 */
																																		return
																																			BFALSE;
																																	}
																															}
																													}
																												}
																											}
																									}
																								}
																						}
																				}
																			}
																		}
																}
															else
																{	/* Expand/garith.scm 49 */
																	return BFALSE;
																}
														}
													else
														{	/* Expand/garith.scm 49 */
															obj_t BgL_cdrzd210505zd2_2122;

															BgL_cdrzd210505zd2_2122 =
																CDR(((obj_t) BgL_xz00_4));
															{	/* Expand/garith.scm 49 */
																obj_t BgL_cdrzd210512zd2_2123;

																BgL_cdrzd210512zd2_2123 =
																	CDR(((obj_t) BgL_cdrzd210505zd2_2122));
																if (PAIRP(BgL_cdrzd210512zd2_2123))
																	{	/* Expand/garith.scm 49 */
																		obj_t BgL_carzd210516zd2_2125;

																		BgL_carzd210516zd2_2125 =
																			CAR(BgL_cdrzd210512zd2_2123);
																		if (REALP(BgL_carzd210516zd2_2125))
																			{	/* Expand/garith.scm 49 */
																				if (NULLP(CDR(BgL_cdrzd210512zd2_2123)))
																					{	/* Expand/garith.scm 49 */
																						obj_t BgL_arg3043z00_2129;
																						obj_t BgL_arg3044z00_2130;

																						BgL_arg3043z00_2129 =
																							CAR(((obj_t) BgL_xz00_4));
																						BgL_arg3044z00_2130 =
																							CAR(
																							((obj_t)
																								BgL_cdrzd210505zd2_2122));
																						{
																							double BgL_bz00_7422;
																							obj_t BgL_az00_7421;
																							obj_t BgL_idz00_7420;

																							BgL_idz00_7420 =
																								BgL_arg3043z00_2129;
																							BgL_az00_7421 =
																								BgL_arg3044z00_2130;
																							BgL_bz00_7422 =
																								REAL_TO_DOUBLE
																								(BgL_carzd210516zd2_2125);
																							BgL_bz00_53 = BgL_bz00_7422;
																							BgL_az00_52 = BgL_az00_7421;
																							BgL_idz00_51 = BgL_idz00_7420;
																							goto BgL_tagzd2104zd2_54;
																						}
																					}
																				else
																					{	/* Expand/garith.scm 49 */
																						return BFALSE;
																					}
																			}
																		else
																			{	/* Expand/garith.scm 49 */
																				obj_t BgL_carzd210582zd2_2133;

																				BgL_carzd210582zd2_2133 =
																					CAR(
																					((obj_t) BgL_cdrzd210505zd2_2122));
																				if (REALP(BgL_carzd210582zd2_2133))
																					{	/* Expand/garith.scm 49 */
																						if (NULLP(CDR(
																									((obj_t)
																										BgL_cdrzd210512zd2_2123))))
																							{	/* Expand/garith.scm 49 */
																								obj_t BgL_arg3058z00_2138;
																								obj_t BgL_arg3059z00_2139;

																								BgL_arg3058z00_2138 =
																									CAR(((obj_t) BgL_xz00_4));
																								BgL_arg3059z00_2139 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd210512zd2_2123));
																								{
																									obj_t BgL_bz00_7439;
																									double BgL_az00_7437;
																									obj_t BgL_idz00_7436;

																									BgL_idz00_7436 =
																										BgL_arg3058z00_2138;
																									BgL_az00_7437 =
																										REAL_TO_DOUBLE
																										(BgL_carzd210582zd2_2133);
																									BgL_bz00_7439 =
																										BgL_arg3059z00_2139;
																									BgL_bz00_57 = BgL_bz00_7439;
																									BgL_az00_56 = BgL_az00_7437;
																									BgL_idz00_55 = BgL_idz00_7436;
																									goto BgL_tagzd2105zd2_58;
																								}
																							}
																						else
																							{	/* Expand/garith.scm 49 */
																								return BFALSE;
																							}
																					}
																				else
																					{	/* Expand/garith.scm 49 */
																						obj_t BgL_cdrzd210640zd2_2141;

																						BgL_cdrzd210640zd2_2141 =
																							CDR(((obj_t) BgL_xz00_4));
																						{	/* Expand/garith.scm 49 */
																							obj_t BgL_carzd210647zd2_2142;
																							obj_t BgL_cdrzd210648zd2_2143;

																							BgL_carzd210647zd2_2142 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd210640zd2_2141));
																							BgL_cdrzd210648zd2_2143 =
																								CDR(((obj_t)
																									BgL_cdrzd210640zd2_2141));
																							if (SYMBOLP
																								(BgL_carzd210647zd2_2142))
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_carzd210655zd2_2145;

																									BgL_carzd210655zd2_2145 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd210648zd2_2143));
																									if (SYMBOLP
																										(BgL_carzd210655zd2_2145))
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd210648zd2_2143))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg3070z00_2149;
																													BgL_arg3070z00_2149 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													{
																														obj_t BgL_bz00_7460;
																														obj_t BgL_az00_7459;
																														obj_t
																															BgL_idz00_7458;
																														BgL_idz00_7458 =
																															BgL_arg3070z00_2149;
																														BgL_az00_7459 =
																															BgL_carzd210647zd2_2142;
																														BgL_bz00_7460 =
																															BgL_carzd210655zd2_2145;
																														BgL_bz00_61 =
																															BgL_bz00_7460;
																														BgL_az00_60 =
																															BgL_az00_7459;
																														BgL_idz00_59 =
																															BgL_idz00_7458;
																														goto
																															BgL_tagzd2106zd2_62;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Expand/garith.scm 49 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd210648zd2_2143))))
																												{	/* Expand/garith.scm 49 */
																													obj_t
																														BgL_arg3075z00_2157;
																													obj_t
																														BgL_arg3077z00_2158;
																													BgL_arg3075z00_2157 =
																														CAR(((obj_t)
																															BgL_xz00_4));
																													BgL_arg3077z00_2158 =
																														CAR(((obj_t)
																															BgL_cdrzd210648zd2_2143));
																													{
																														obj_t BgL_bz00_7471;
																														obj_t BgL_az00_7470;
																														obj_t
																															BgL_idz00_7469;
																														BgL_idz00_7469 =
																															BgL_arg3075z00_2157;
																														BgL_az00_7470 =
																															BgL_carzd210647zd2_2142;
																														BgL_bz00_7471 =
																															BgL_arg3077z00_2158;
																														BgL_bz00_65 =
																															BgL_bz00_7471;
																														BgL_az00_64 =
																															BgL_az00_7470;
																														BgL_idz00_63 =
																															BgL_idz00_7469;
																														goto
																															BgL_tagzd2107zd2_66;
																													}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													return BFALSE;
																												}
																										}
																								}
																							else
																								{	/* Expand/garith.scm 49 */
																									obj_t BgL_cdrzd210857zd2_2186;

																									BgL_cdrzd210857zd2_2186 =
																										CDR(((obj_t) BgL_xz00_4));
																									{	/* Expand/garith.scm 49 */
																										obj_t
																											BgL_cdrzd210865zd2_2187;
																										BgL_cdrzd210865zd2_2187 =
																											CDR(((obj_t)
																												BgL_cdrzd210857zd2_2186));
																										{	/* Expand/garith.scm 49 */
																											obj_t
																												BgL_carzd210870zd2_2188;
																											BgL_carzd210870zd2_2188 =
																												CAR(((obj_t)
																													BgL_cdrzd210865zd2_2187));
																											if (SYMBOLP
																												(BgL_carzd210870zd2_2188))
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd210865zd2_2187))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg3116z00_2192;
																															obj_t
																																BgL_arg3118z00_2193;
																															BgL_arg3116z00_2192
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg3118z00_2193
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd210857zd2_2186));
																															{
																																obj_t
																																	BgL_bz00_7490;
																																obj_t
																																	BgL_az00_7489;
																																obj_t
																																	BgL_idz00_7488;
																																BgL_idz00_7488 =
																																	BgL_arg3116z00_2192;
																																BgL_az00_7489 =
																																	BgL_arg3118z00_2193;
																																BgL_bz00_7490 =
																																	BgL_carzd210870zd2_2188;
																																BgL_bz00_69 =
																																	BgL_bz00_7490;
																																BgL_az00_68 =
																																	BgL_az00_7489;
																																BgL_idz00_67 =
																																	BgL_idz00_7488;
																																goto
																																	BgL_tagzd2108zd2_70;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Expand/garith.scm 49 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd210865zd2_2187))))
																														{	/* Expand/garith.scm 49 */
																															obj_t
																																BgL_arg3124z00_2199;
																															obj_t
																																BgL_arg3125z00_2200;
																															obj_t
																																BgL_arg3126z00_2201;
																															BgL_arg3124z00_2199
																																=
																																CAR(((obj_t)
																																	BgL_xz00_4));
																															BgL_arg3125z00_2200
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd210857zd2_2186));
																															BgL_arg3126z00_2201
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd210865zd2_2187));
																															{
																																obj_t
																																	BgL_bz00_7503;
																																obj_t
																																	BgL_az00_7502;
																																obj_t
																																	BgL_idz00_7501;
																																BgL_idz00_7501 =
																																	BgL_arg3124z00_2199;
																																BgL_az00_7502 =
																																	BgL_arg3125z00_2200;
																																BgL_bz00_7503 =
																																	BgL_arg3126z00_2201;
																																BgL_bz00_73 =
																																	BgL_bz00_7503;
																																BgL_az00_72 =
																																	BgL_az00_7502;
																																BgL_idz00_71 =
																																	BgL_idz00_7501;
																																goto
																																	BgL_tagzd2109zd2_74;
																															}
																														}
																													else
																														{	/* Expand/garith.scm 49 */
																															return BFALSE;
																														}
																												}
																										}
																									}
																								}
																						}
																					}
																			}
																	}
																else
																	{	/* Expand/garith.scm 49 */
																		return BFALSE;
																	}
															}
														}
												}
										}
									}
							}
						else
							{	/* Expand/garith.scm 49 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/garith.scm 49 */
						return BFALSE;
					}
			}
		}

	}



/* fx~0 */
	obj_t BGl_fxze70ze7zzexpand_garithmetiquez00(obj_t BgL_idz00_2357)
	{
		{	/* Expand/garith.scm 47 */
			{	/* Expand/garith.scm 42 */
				bool_t BgL_test4151z00_7504;

				if (CBOOL(BGl_za2arithmeticzd2overflowza2zd2zzengine_paramz00))
					{	/* Expand/garith.scm 42 */
						if (CBOOL
							(BGl_za2arithmeticzd2newzd2overflowza2z00zzengine_paramz00))
							{	/* Expand/garith.scm 42 */
								BgL_test4151z00_7504 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_2357,
										CNST_TABLE_REF(14)));
							}
						else
							{	/* Expand/garith.scm 42 */
								BgL_test4151z00_7504 = ((bool_t) 0);
							}
					}
				else
					{	/* Expand/garith.scm 42 */
						BgL_test4151z00_7504 = ((bool_t) 0);
					}
				if (BgL_test4151z00_7504)
					{	/* Expand/garith.scm 43 */
						obj_t BgL_arg3372z00_2360;

						{	/* Expand/garith.scm 43 */
							obj_t BgL_arg3373z00_2361;
							obj_t BgL_arg3375z00_2362;

							{	/* Expand/garith.scm 43 */
								obj_t BgL_arg1455z00_2808;

								BgL_arg1455z00_2808 =
									SYMBOL_TO_STRING(((obj_t) BgL_idz00_2357));
								BgL_arg3373z00_2361 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_2808);
							}
							{	/* Expand/garith.scm 43 */
								obj_t BgL_symbolz00_2809;

								BgL_symbolz00_2809 = CNST_TABLE_REF(15);
								{	/* Expand/garith.scm 43 */
									obj_t BgL_arg1455z00_2810;

									BgL_arg1455z00_2810 = SYMBOL_TO_STRING(BgL_symbolz00_2809);
									BgL_arg3375z00_2362 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2810);
								}
							}
							BgL_arg3372z00_2360 =
								string_append(BgL_arg3373z00_2361, BgL_arg3375z00_2362);
						}
						return bstring_to_symbol(BgL_arg3372z00_2360);
					}
				else
					{	/* Expand/garith.scm 44 */
						bool_t BgL_test4154z00_7520;

						if (CBOOL(BGl_za2arithmeticzd2overflowza2zd2zzengine_paramz00))
							{	/* Expand/garith.scm 44 */
								BgL_test4154z00_7520 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_2357,
										CNST_TABLE_REF(16)));
							}
						else
							{	/* Expand/garith.scm 44 */
								BgL_test4154z00_7520 = ((bool_t) 0);
							}
						if (BgL_test4154z00_7520)
							{	/* Expand/garith.scm 45 */
								obj_t BgL_arg3379z00_2364;

								{	/* Expand/garith.scm 45 */
									obj_t BgL_arg3381z00_2365;
									obj_t BgL_arg3382z00_2366;

									{	/* Expand/garith.scm 45 */
										obj_t BgL_arg1455z00_2813;

										BgL_arg1455z00_2813 =
											SYMBOL_TO_STRING(((obj_t) BgL_idz00_2357));
										BgL_arg3381z00_2365 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2813);
									}
									{	/* Expand/garith.scm 45 */
										obj_t BgL_symbolz00_2814;

										BgL_symbolz00_2814 = CNST_TABLE_REF(17);
										{	/* Expand/garith.scm 45 */
											obj_t BgL_arg1455z00_2815;

											BgL_arg1455z00_2815 =
												SYMBOL_TO_STRING(BgL_symbolz00_2814);
											BgL_arg3382z00_2366 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2815);
										}
									}
									BgL_arg3379z00_2364 =
										string_append(BgL_arg3381z00_2365, BgL_arg3382z00_2366);
								}
								return bstring_to_symbol(BgL_arg3379z00_2364);
							}
						else
							{	/* Expand/garith.scm 47 */
								obj_t BgL_arg3383z00_2367;

								{	/* Expand/garith.scm 47 */
									obj_t BgL_arg3384z00_2368;
									obj_t BgL_arg3385z00_2369;

									{	/* Expand/garith.scm 47 */
										obj_t BgL_arg1455z00_2818;

										BgL_arg1455z00_2818 =
											SYMBOL_TO_STRING(((obj_t) BgL_idz00_2357));
										BgL_arg3384z00_2368 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2818);
									}
									{	/* Expand/garith.scm 47 */
										obj_t BgL_symbolz00_2819;

										BgL_symbolz00_2819 = CNST_TABLE_REF(18);
										{	/* Expand/garith.scm 47 */
											obj_t BgL_arg1455z00_2820;

											BgL_arg1455z00_2820 =
												SYMBOL_TO_STRING(BgL_symbolz00_2819);
											BgL_arg3385z00_2369 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2820);
										}
									}
									BgL_arg3383z00_2367 =
										string_append(BgL_arg3384z00_2368, BgL_arg3385z00_2369);
								}
								return bstring_to_symbol(BgL_arg3383z00_2367);
							}
					}
			}
		}

	}



/* expand-g+ */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gzb2z60zzexpand_garithmetiquez00(obj_t
		BgL_xz00_7, obj_t BgL_ez00_8)
	{
		{	/* Expand/garith.scm 101 */
			{
				obj_t BgL_xz00_2375;
				obj_t BgL_yz00_2376;

				if (PAIRP(BgL_xz00_7))
					{	/* Expand/garith.scm 102 */
						if (NULLP(CDR(((obj_t) BgL_xz00_7))))
							{	/* Expand/garith.scm 102 */
								return BINT(0L);
							}
						else
							{	/* Expand/garith.scm 102 */
								obj_t BgL_cdrzd211069zd2_2382;

								BgL_cdrzd211069zd2_2382 = CDR(((obj_t) BgL_xz00_7));
								if (PAIRP(BgL_cdrzd211069zd2_2382))
									{	/* Expand/garith.scm 102 */
										if (NULLP(CDR(BgL_cdrzd211069zd2_2382)))
											{	/* Expand/garith.scm 102 */
												obj_t BgL_arg3393z00_2386;

												BgL_arg3393z00_2386 = CAR(BgL_cdrzd211069zd2_2382);
												return
													BGL_PROCEDURE_CALL2(BgL_ez00_8, BgL_arg3393z00_2386,
													BgL_ez00_8);
											}
										else
											{	/* Expand/garith.scm 102 */
												obj_t BgL_cdrzd211079zd2_2387;

												BgL_cdrzd211079zd2_2387 =
													CDR(((obj_t) BgL_cdrzd211069zd2_2382));
												if (PAIRP(BgL_cdrzd211079zd2_2387))
													{	/* Expand/garith.scm 102 */
														if (NULLP(CDR(BgL_cdrzd211079zd2_2387)))
															{	/* Expand/garith.scm 102 */
																return
																	BGl_expandzd2g2zd2zzexpand_garithmetiquez00
																	(BgL_xz00_7, BgL_ez00_8,
																	BGl_zb2zd2envz60zz__r4_numbers_6_5z00);
															}
														else
															{	/* Expand/garith.scm 102 */
																obj_t BgL_arg3397z00_2392;
																obj_t BgL_arg3398z00_2393;

																BgL_arg3397z00_2392 =
																	CAR(((obj_t) BgL_cdrzd211069zd2_2382));
																BgL_arg3398z00_2393 =
																	CDR(((obj_t) BgL_cdrzd211069zd2_2382));
																BgL_xz00_2375 = BgL_arg3397z00_2392;
																BgL_yz00_2376 = BgL_arg3398z00_2393;
															BgL_tagzd211059zd2_2377:
																{	/* Expand/garith.scm 110 */
																	obj_t BgL_arg3405z00_2401;

																	{	/* Expand/garith.scm 110 */
																		obj_t BgL_arg3406z00_2402;

																		{	/* Expand/garith.scm 110 */
																			obj_t BgL_arg3407z00_2403;

																			{	/* Expand/garith.scm 110 */
																				obj_t BgL_arg3410z00_2404;

																				{	/* Expand/garith.scm 110 */
																					obj_t BgL_arg3413z00_2405;

																					{	/* Expand/garith.scm 110 */
																						obj_t BgL_arg3416z00_2406;

																						BgL_arg3416z00_2406 =
																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_yz00_2376, BNIL);
																						BgL_arg3413z00_2405 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(19), BgL_arg3416z00_2406);
																					}
																					BgL_arg3410z00_2404 =
																						BGL_PROCEDURE_CALL2(BgL_ez00_8,
																						BgL_arg3413z00_2405, BgL_ez00_8);
																				}
																				BgL_arg3407z00_2403 =
																					MAKE_YOUNG_PAIR(BgL_arg3410z00_2404,
																					BNIL);
																			}
																			BgL_arg3406z00_2402 =
																				MAKE_YOUNG_PAIR(BgL_xz00_2375,
																				BgL_arg3407z00_2403);
																		}
																		BgL_arg3405z00_2401 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																			BgL_arg3406z00_2402);
																	}
																	return
																		BGl_expandzd2g2zd2zzexpand_garithmetiquez00
																		(BgL_arg3405z00_2401, BgL_ez00_8,
																		BGl_zb2zd2envz60zz__r4_numbers_6_5z00);
																}
															}
													}
												else
													{	/* Expand/garith.scm 102 */
														obj_t BgL_arg3400z00_2396;
														obj_t BgL_arg3401z00_2397;

														BgL_arg3400z00_2396 =
															CAR(((obj_t) BgL_cdrzd211069zd2_2382));
														BgL_arg3401z00_2397 =
															CDR(((obj_t) BgL_cdrzd211069zd2_2382));
														{
															obj_t BgL_yz00_7592;
															obj_t BgL_xz00_7591;

															BgL_xz00_7591 = BgL_arg3400z00_2396;
															BgL_yz00_7592 = BgL_arg3401z00_2397;
															BgL_yz00_2376 = BgL_yz00_7592;
															BgL_xz00_2375 = BgL_xz00_7591;
															goto BgL_tagzd211059zd2_2377;
														}
													}
											}
									}
								else
									{	/* Expand/garith.scm 102 */
										return BFALSE;
									}
							}
					}
				else
					{	/* Expand/garith.scm 102 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-g+ */
	obj_t BGl_z62expandzd2gzb2z02zzexpand_garithmetiquez00(obj_t BgL_envz00_4418,
		obj_t BgL_xz00_4419, obj_t BgL_ez00_4420)
	{
		{	/* Expand/garith.scm 101 */
			return
				BGl_expandzd2gzb2z60zzexpand_garithmetiquez00(BgL_xz00_4419,
				BgL_ez00_4420);
		}

	}



/* expand-g- */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gzd2z00zzexpand_garithmetiquez00(obj_t
		BgL_xz00_9, obj_t BgL_ez00_10)
	{
		{	/* Expand/garith.scm 115 */
			{
				obj_t BgL_xz00_2410;
				obj_t BgL_yz00_2411;
				obj_t BgL_xz00_2407;

				if (PAIRP(BgL_xz00_9))
					{	/* Expand/garith.scm 116 */
						obj_t BgL_cdrzd211135zd2_2415;

						BgL_cdrzd211135zd2_2415 = CDR(((obj_t) BgL_xz00_9));
						if (PAIRP(BgL_cdrzd211135zd2_2415))
							{	/* Expand/garith.scm 116 */
								if (NULLP(CDR(BgL_cdrzd211135zd2_2415)))
									{	/* Expand/garith.scm 116 */
										BgL_xz00_2407 = CAR(BgL_cdrzd211135zd2_2415);
										if (INTEGERP(BgL_xz00_2407))
											{	/* Expand/garith.scm 119 */
												return BINT(NEG((long) CINT(BgL_xz00_2407)));
											}
										else
											{	/* Expand/garith.scm 119 */
												if (REALP(BgL_xz00_2407))
													{	/* Expand/garith.scm 121 */
														return
															DOUBLE_TO_REAL(NEG(REAL_TO_DOUBLE
																(BgL_xz00_2407)));
													}
												else
													{	/* Expand/garith.scm 124 */
														obj_t BgL_arg3445z00_2435;

														{	/* Expand/garith.scm 124 */
															obj_t BgL_arg3446z00_2436;

															BgL_arg3446z00_2436 =
																BGL_PROCEDURE_CALL2(BgL_ez00_10, BgL_xz00_2407,
																BgL_ez00_10);
															BgL_arg3445z00_2435 =
																MAKE_YOUNG_PAIR(BgL_arg3446z00_2436, BNIL);
														}
														return
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
															BgL_arg3445z00_2435);
													}
											}
									}
								else
									{	/* Expand/garith.scm 116 */
										obj_t BgL_cdrzd211145zd2_2420;

										BgL_cdrzd211145zd2_2420 =
											CDR(((obj_t) BgL_cdrzd211135zd2_2415));
										if (PAIRP(BgL_cdrzd211145zd2_2420))
											{	/* Expand/garith.scm 116 */
												if (NULLP(CDR(BgL_cdrzd211145zd2_2420)))
													{	/* Expand/garith.scm 116 */
														return
															BGl_expandzd2g2zd2zzexpand_garithmetiquez00
															(BgL_xz00_9, BgL_ez00_10,
															BGl_zd2zd2envz00zz__r4_numbers_6_5z00);
													}
												else
													{	/* Expand/garith.scm 116 */
														obj_t BgL_arg3432z00_2425;
														obj_t BgL_arg3433z00_2426;

														BgL_arg3432z00_2425 =
															CAR(((obj_t) BgL_cdrzd211135zd2_2415));
														BgL_arg3433z00_2426 =
															CDR(((obj_t) BgL_cdrzd211135zd2_2415));
														BgL_xz00_2410 = BgL_arg3432z00_2425;
														BgL_yz00_2411 = BgL_arg3433z00_2426;
													BgL_tagzd211129zd2_2412:
														{	/* Expand/garith.scm 128 */
															obj_t BgL_arg3448z00_2437;

															{	/* Expand/garith.scm 128 */
																obj_t BgL_arg3449z00_2438;

																{	/* Expand/garith.scm 128 */
																	obj_t BgL_arg3451z00_2439;

																	{	/* Expand/garith.scm 128 */
																		obj_t BgL_arg3452z00_2440;

																		{	/* Expand/garith.scm 128 */
																			obj_t BgL_arg3453z00_2441;

																			{	/* Expand/garith.scm 128 */
																				obj_t BgL_arg3454z00_2442;

																				BgL_arg3454z00_2442 =
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_yz00_2411, BNIL);
																				BgL_arg3453z00_2441 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																					BgL_arg3454z00_2442);
																			}
																			BgL_arg3452z00_2440 =
																				BGL_PROCEDURE_CALL2(BgL_ez00_10,
																				BgL_arg3453z00_2441, BgL_ez00_10);
																		}
																		BgL_arg3451z00_2439 =
																			MAKE_YOUNG_PAIR(BgL_arg3452z00_2440,
																			BNIL);
																	}
																	BgL_arg3449z00_2438 =
																		MAKE_YOUNG_PAIR(BgL_xz00_2410,
																		BgL_arg3451z00_2439);
																}
																BgL_arg3448z00_2437 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
																	BgL_arg3449z00_2438);
															}
															return
																BGl_expandzd2g2zd2zzexpand_garithmetiquez00
																(BgL_arg3448z00_2437, BgL_ez00_10,
																BGl_zd2zd2envz00zz__r4_numbers_6_5z00);
														}
													}
											}
										else
											{	/* Expand/garith.scm 116 */
												obj_t BgL_arg3436z00_2429;
												obj_t BgL_arg3439z00_2430;

												BgL_arg3436z00_2429 =
													CAR(((obj_t) BgL_cdrzd211135zd2_2415));
												BgL_arg3439z00_2430 =
													CDR(((obj_t) BgL_cdrzd211135zd2_2415));
												{
													obj_t BgL_yz00_7652;
													obj_t BgL_xz00_7651;

													BgL_xz00_7651 = BgL_arg3436z00_2429;
													BgL_yz00_7652 = BgL_arg3439z00_2430;
													BgL_yz00_2411 = BgL_yz00_7652;
													BgL_xz00_2410 = BgL_xz00_7651;
													goto BgL_tagzd211129zd2_2412;
												}
											}
									}
							}
						else
							{	/* Expand/garith.scm 116 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/garith.scm 116 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-g- */
	obj_t BGl_z62expandzd2gzd2z62zzexpand_garithmetiquez00(obj_t BgL_envz00_4423,
		obj_t BgL_xz00_4424, obj_t BgL_ez00_4425)
	{
		{	/* Expand/garith.scm 115 */
			return
				BGl_expandzd2gzd2z00zzexpand_garithmetiquez00(BgL_xz00_4424,
				BgL_ez00_4425);
		}

	}



/* expand-g* */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gza2z70zzexpand_garithmetiquez00(obj_t
		BgL_xz00_11, obj_t BgL_ez00_12)
	{
		{	/* Expand/garith.scm 133 */
			{
				obj_t BgL_xz00_2447;
				obj_t BgL_yz00_2448;

				if (PAIRP(BgL_xz00_11))
					{	/* Expand/garith.scm 134 */
						if (NULLP(CDR(((obj_t) BgL_xz00_11))))
							{	/* Expand/garith.scm 134 */
								return BINT(1L);
							}
						else
							{	/* Expand/garith.scm 134 */
								obj_t BgL_cdrzd211203zd2_2454;

								BgL_cdrzd211203zd2_2454 = CDR(((obj_t) BgL_xz00_11));
								if (PAIRP(BgL_cdrzd211203zd2_2454))
									{	/* Expand/garith.scm 134 */
										if (NULLP(CDR(BgL_cdrzd211203zd2_2454)))
											{	/* Expand/garith.scm 134 */
												obj_t BgL_arg3462z00_2458;

												BgL_arg3462z00_2458 = CAR(BgL_cdrzd211203zd2_2454);
												return
													BGL_PROCEDURE_CALL2(BgL_ez00_12, BgL_arg3462z00_2458,
													BgL_ez00_12);
											}
										else
											{	/* Expand/garith.scm 134 */
												obj_t BgL_cdrzd211213zd2_2459;

												BgL_cdrzd211213zd2_2459 =
													CDR(((obj_t) BgL_cdrzd211203zd2_2454));
												if (PAIRP(BgL_cdrzd211213zd2_2459))
													{	/* Expand/garith.scm 134 */
														if (NULLP(CDR(BgL_cdrzd211213zd2_2459)))
															{	/* Expand/garith.scm 134 */
																return
																	BGl_expandzd2g2zd2zzexpand_garithmetiquez00
																	(BgL_xz00_11, BgL_ez00_12,
																	BGl_za2zd2envz70zz__r4_numbers_6_5z00);
															}
														else
															{	/* Expand/garith.scm 134 */
																obj_t BgL_arg3466z00_2464;
																obj_t BgL_arg3468z00_2465;

																BgL_arg3466z00_2464 =
																	CAR(((obj_t) BgL_cdrzd211203zd2_2454));
																BgL_arg3468z00_2465 =
																	CDR(((obj_t) BgL_cdrzd211203zd2_2454));
																BgL_xz00_2447 = BgL_arg3466z00_2464;
																BgL_yz00_2448 = BgL_arg3468z00_2465;
															BgL_tagzd211193zd2_2449:
																{	/* Expand/garith.scm 142 */
																	obj_t BgL_arg3476z00_2473;

																	{	/* Expand/garith.scm 142 */
																		obj_t BgL_arg3477z00_2474;

																		{	/* Expand/garith.scm 142 */
																			obj_t BgL_arg3479z00_2475;

																			{	/* Expand/garith.scm 142 */
																				obj_t BgL_arg3480z00_2476;

																				{	/* Expand/garith.scm 142 */
																					obj_t BgL_arg3481z00_2477;

																					{	/* Expand/garith.scm 142 */
																						obj_t BgL_arg3482z00_2478;

																						BgL_arg3482z00_2478 =
																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_yz00_2448, BNIL);
																						BgL_arg3481z00_2477 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(21), BgL_arg3482z00_2478);
																					}
																					BgL_arg3480z00_2476 =
																						BGL_PROCEDURE_CALL2(BgL_ez00_12,
																						BgL_arg3481z00_2477, BgL_ez00_12);
																				}
																				BgL_arg3479z00_2475 =
																					MAKE_YOUNG_PAIR(BgL_arg3480z00_2476,
																					BNIL);
																			}
																			BgL_arg3477z00_2474 =
																				MAKE_YOUNG_PAIR(BgL_xz00_2447,
																				BgL_arg3479z00_2475);
																		}
																		BgL_arg3476z00_2473 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
																			BgL_arg3477z00_2474);
																	}
																	return
																		BGl_expandzd2g2zd2zzexpand_garithmetiquez00
																		(BgL_arg3476z00_2473, BgL_ez00_12,
																		BGl_za2zd2envz70zz__r4_numbers_6_5z00);
																}
															}
													}
												else
													{	/* Expand/garith.scm 134 */
														obj_t BgL_arg3470z00_2468;
														obj_t BgL_arg3471z00_2469;

														BgL_arg3470z00_2468 =
															CAR(((obj_t) BgL_cdrzd211203zd2_2454));
														BgL_arg3471z00_2469 =
															CDR(((obj_t) BgL_cdrzd211203zd2_2454));
														{
															obj_t BgL_yz00_7704;
															obj_t BgL_xz00_7703;

															BgL_xz00_7703 = BgL_arg3470z00_2468;
															BgL_yz00_7704 = BgL_arg3471z00_2469;
															BgL_yz00_2448 = BgL_yz00_7704;
															BgL_xz00_2447 = BgL_xz00_7703;
															goto BgL_tagzd211193zd2_2449;
														}
													}
											}
									}
								else
									{	/* Expand/garith.scm 134 */
										return BFALSE;
									}
							}
					}
				else
					{	/* Expand/garith.scm 134 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-g* */
	obj_t BGl_z62expandzd2gza2z12zzexpand_garithmetiquez00(obj_t BgL_envz00_4429,
		obj_t BgL_xz00_4430, obj_t BgL_ez00_4431)
	{
		{	/* Expand/garith.scm 133 */
			return
				BGl_expandzd2gza2z70zzexpand_garithmetiquez00(BgL_xz00_4430,
				BgL_ez00_4431);
		}

	}



/* expand-g/ */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gzf2z20zzexpand_garithmetiquez00(obj_t
		BgL_xz00_13, obj_t BgL_ez00_14)
	{
		{	/* Expand/garith.scm 147 */
			{
				obj_t BgL_az00_2484;
				obj_t BgL_bz00_2485;
				obj_t BgL_az00_2481;
				obj_t BgL_bz00_2482;
				obj_t BgL_xz00_2479;

				if (PAIRP(BgL_xz00_13))
					{	/* Expand/garith.scm 148 */
						obj_t BgL_cdrzd211270zd2_2489;

						BgL_cdrzd211270zd2_2489 = CDR(((obj_t) BgL_xz00_13));
						if (PAIRP(BgL_cdrzd211270zd2_2489))
							{	/* Expand/garith.scm 148 */
								if (NULLP(CDR(BgL_cdrzd211270zd2_2489)))
									{	/* Expand/garith.scm 148 */
										BgL_xz00_2479 = CAR(BgL_cdrzd211270zd2_2489);
										{	/* Expand/garith.scm 150 */
											obj_t BgL_arg3504z00_2509;

											{	/* Expand/garith.scm 150 */
												obj_t BgL_arg3506z00_2510;

												{	/* Expand/garith.scm 150 */
													obj_t BgL_arg3508z00_2511;

													BgL_arg3508z00_2511 =
														BGL_PROCEDURE_CALL2(BgL_ez00_14, BgL_xz00_2479,
														BgL_ez00_14);
													BgL_arg3506z00_2510 =
														MAKE_YOUNG_PAIR(BgL_arg3508z00_2511, BNIL);
												}
												BgL_arg3504z00_2509 =
													MAKE_YOUNG_PAIR(BINT(1L), BgL_arg3506z00_2510);
											}
											return
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
												BgL_arg3504z00_2509);
										}
									}
								else
									{	/* Expand/garith.scm 148 */
										obj_t BgL_cdrzd211286zd2_2495;

										BgL_cdrzd211286zd2_2495 =
											CDR(((obj_t) BgL_cdrzd211270zd2_2489));
										if (PAIRP(BgL_cdrzd211286zd2_2495))
											{	/* Expand/garith.scm 148 */
												if (NULLP(CDR(BgL_cdrzd211286zd2_2495)))
													{	/* Expand/garith.scm 148 */
														obj_t BgL_arg3492z00_2499;
														obj_t BgL_arg3493z00_2500;

														BgL_arg3492z00_2499 =
															CAR(((obj_t) BgL_cdrzd211270zd2_2489));
														BgL_arg3493z00_2500 = CAR(BgL_cdrzd211286zd2_2495);
														BgL_az00_2481 = BgL_arg3492z00_2499;
														BgL_bz00_2482 = BgL_arg3493z00_2500;
														{	/* Expand/garith.scm 153 */
															bool_t BgL_test4180z00_7736;

															if (BGl_expandzd2gzd2numberzf3zf3zzexpand_garithmetiquez00(BgL_az00_2481))
																{	/* Expand/garith.scm 153 */
																	BgL_test4180z00_7736 =
																		BGl_expandzd2gzd2numberzf3zf3zzexpand_garithmetiquez00
																		(BgL_bz00_2482);
																}
															else
																{	/* Expand/garith.scm 153 */
																	BgL_test4180z00_7736 = ((bool_t) 0);
																}
															if (BgL_test4180z00_7736)
																{	/* Expand/garith.scm 154 */
																	bool_t BgL_test4182z00_7740;

																	if (INTEGERP(BgL_bz00_2482))
																		{	/* Expand/garith.scm 154 */
																			BgL_test4182z00_7740 =
																				((long) CINT(BgL_bz00_2482) == 0L);
																		}
																	else
																		{	/* Expand/garith.scm 154 */
																			BgL_test4182z00_7740 =
																				BGl_2zd3zd3zz__r4_numbers_6_5z00
																				(BgL_bz00_2482, BINT(0L));
																		}
																	if (BgL_test4182z00_7740)
																		{	/* Expand/garith.scm 155 */
																			obj_t BgL_arg3512z00_2515;

																			{	/* Expand/garith.scm 155 */
																				obj_t BgL_arg3513z00_2516;

																				{	/* Expand/garith.scm 155 */
																					obj_t BgL_arg3514z00_2517;

																					BgL_arg3514z00_2517 =
																						MAKE_YOUNG_PAIR(BgL_bz00_2482,
																						BNIL);
																					BgL_arg3513z00_2516 =
																						MAKE_YOUNG_PAIR(BgL_az00_2481,
																						BgL_arg3514z00_2517);
																				}
																				BgL_arg3512z00_2515 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
																					BgL_arg3513z00_2516);
																			}
																			return
																				BGL_PROCEDURE_CALL2(BgL_ez00_14,
																				BgL_arg3512z00_2515, BgL_ez00_14);
																		}
																	else
																		{	/* Expand/garith.scm 154 */
																			return
																				BGl_2zf2zf2zz__r4_numbers_6_5z00
																				(BgL_az00_2481, BgL_bz00_2482);
																		}
																}
															else
																{	/* Expand/garith.scm 158 */
																	obj_t BgL_arg3515z00_2518;

																	{	/* Expand/garith.scm 158 */
																		obj_t BgL_arg3517z00_2519;

																		{	/* Expand/garith.scm 158 */
																			obj_t BgL_arg3518z00_2520;

																			BgL_arg3518z00_2520 =
																				MAKE_YOUNG_PAIR(BgL_bz00_2482, BNIL);
																			BgL_arg3517z00_2519 =
																				MAKE_YOUNG_PAIR(BgL_az00_2481,
																				BgL_arg3518z00_2520);
																		}
																		BgL_arg3515z00_2518 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
																			BgL_arg3517z00_2519);
																	}
																	return
																		BGL_PROCEDURE_CALL2(BgL_ez00_14,
																		BgL_arg3515z00_2518, BgL_ez00_14);
																}
														}
													}
												else
													{	/* Expand/garith.scm 148 */
														obj_t BgL_arg3494z00_2502;
														obj_t BgL_arg3495z00_2503;

														BgL_arg3494z00_2502 =
															CAR(((obj_t) BgL_cdrzd211270zd2_2489));
														BgL_arg3495z00_2503 =
															CDR(((obj_t) BgL_cdrzd211270zd2_2489));
														BgL_az00_2484 = BgL_arg3494z00_2502;
														BgL_bz00_2485 = BgL_arg3495z00_2503;
													BgL_tagzd211263zd2_2486:
														{	/* Expand/garith.scm 160 */
															obj_t BgL_arg3520z00_2522;

															{	/* Expand/garith.scm 160 */
																obj_t BgL_arg3521z00_2523;

																{	/* Expand/garith.scm 160 */
																	obj_t BgL_arg3522z00_2524;

																	{	/* Expand/garith.scm 160 */
																		obj_t BgL_arg3523z00_2525;

																		{	/* Expand/garith.scm 160 */
																			obj_t BgL_arg3524z00_2526;

																			BgL_arg3524z00_2526 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_bz00_2485, BNIL);
																			BgL_arg3523z00_2525 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
																				BgL_arg3524z00_2526);
																		}
																		BgL_arg3522z00_2524 =
																			MAKE_YOUNG_PAIR(BgL_arg3523z00_2525,
																			BNIL);
																	}
																	BgL_arg3521z00_2523 =
																		MAKE_YOUNG_PAIR(BgL_az00_2484,
																		BgL_arg3522z00_2524);
																}
																BgL_arg3520z00_2522 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
																	BgL_arg3521z00_2523);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_14,
																BgL_arg3520z00_2522, BgL_ez00_14);
														}
													}
											}
										else
											{	/* Expand/garith.scm 148 */
												obj_t BgL_arg3500z00_2506;
												obj_t BgL_arg3502z00_2507;

												BgL_arg3500z00_2506 =
													CAR(((obj_t) BgL_cdrzd211270zd2_2489));
												BgL_arg3502z00_2507 =
													CDR(((obj_t) BgL_cdrzd211270zd2_2489));
												{
													obj_t BgL_bz00_7787;
													obj_t BgL_az00_7786;

													BgL_az00_7786 = BgL_arg3500z00_2506;
													BgL_bz00_7787 = BgL_arg3502z00_2507;
													BgL_bz00_2485 = BgL_bz00_7787;
													BgL_az00_2484 = BgL_az00_7786;
													goto BgL_tagzd211263zd2_2486;
												}
											}
									}
							}
						else
							{	/* Expand/garith.scm 148 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/garith.scm 148 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-g/ */
	obj_t BGl_z62expandzd2gzf2z42zzexpand_garithmetiquez00(obj_t BgL_envz00_4434,
		obj_t BgL_xz00_4435, obj_t BgL_ez00_4436)
	{
		{	/* Expand/garith.scm 147 */
			return
				BGl_expandzd2gzf2z20zzexpand_garithmetiquez00(BgL_xz00_4435,
				BgL_ez00_4436);
		}

	}



/* expand-g= */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gzd3z01zzexpand_garithmetiquez00(obj_t
		BgL_xz00_15, obj_t BgL_ez00_16)
	{
		{	/* Expand/garith.scm 165 */
			{
				obj_t BgL_xz00_2529;
				obj_t BgL_yz00_2530;

				if (PAIRP(BgL_xz00_15))
					{	/* Expand/garith.scm 166 */
						obj_t BgL_cdrzd211340zd2_2534;

						BgL_cdrzd211340zd2_2534 = CDR(((obj_t) BgL_xz00_15));
						if (PAIRP(BgL_cdrzd211340zd2_2534))
							{	/* Expand/garith.scm 166 */
								obj_t BgL_cdrzd211342zd2_2536;

								BgL_cdrzd211342zd2_2536 = CDR(BgL_cdrzd211340zd2_2534);
								if (PAIRP(BgL_cdrzd211342zd2_2536))
									{	/* Expand/garith.scm 166 */
										if (NULLP(CDR(BgL_cdrzd211342zd2_2536)))
											{	/* Expand/garith.scm 166 */
												return
													BGl_expandzd2g2zd2zzexpand_garithmetiquez00
													(BgL_xz00_15, BgL_ez00_16,
													BGl_zd3zd2envz01zz__r4_numbers_6_5z00);
											}
										else
											{	/* Expand/garith.scm 166 */
												obj_t BgL_arg3531z00_2541;
												obj_t BgL_arg3532z00_2542;

												BgL_arg3531z00_2541 =
													CAR(((obj_t) BgL_cdrzd211340zd2_2534));
												BgL_arg3532z00_2542 =
													CDR(((obj_t) BgL_cdrzd211340zd2_2534));
												BgL_xz00_2529 = BgL_arg3531z00_2541;
												BgL_yz00_2530 = BgL_arg3532z00_2542;
											BgL_tagzd211335zd2_2531:
												{	/* Expand/garith.scm 172 */
													obj_t BgL_arg3542z00_2552;

													{	/* Expand/garith.scm 172 */
														obj_t BgL_arg3544z00_2553;

														{	/* Expand/garith.scm 172 */
															obj_t BgL_arg3545z00_2554;
															obj_t BgL_arg3546z00_2555;

															{	/* Expand/garith.scm 172 */
																obj_t BgL_arg3548z00_2556;

																{	/* Expand/garith.scm 172 */
																	obj_t BgL_arg3549z00_2557;

																	{	/* Expand/garith.scm 172 */
																		obj_t BgL_arg3551z00_2558;

																		BgL_arg3551z00_2558 =
																			CAR(((obj_t) BgL_yz00_2530));
																		BgL_arg3549z00_2557 =
																			MAKE_YOUNG_PAIR(BgL_arg3551z00_2558,
																			BNIL);
																	}
																	BgL_arg3548z00_2556 =
																		MAKE_YOUNG_PAIR(BgL_xz00_2529,
																		BgL_arg3549z00_2557);
																}
																BgL_arg3545z00_2554 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(23),
																	BgL_arg3548z00_2556);
															}
															{	/* Expand/garith.scm 172 */
																obj_t BgL_arg3552z00_2559;

																{	/* Expand/garith.scm 172 */
																	obj_t BgL_arg3553z00_2560;

																	BgL_arg3553z00_2560 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_2530, BNIL);
																	BgL_arg3552z00_2559 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
																		BgL_arg3553z00_2560);
																}
																BgL_arg3546z00_2555 =
																	MAKE_YOUNG_PAIR(BgL_arg3552z00_2559, BNIL);
															}
															BgL_arg3544z00_2553 =
																MAKE_YOUNG_PAIR(BgL_arg3545z00_2554,
																BgL_arg3546z00_2555);
														}
														BgL_arg3542z00_2552 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
															BgL_arg3544z00_2553);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_16,
														BgL_arg3542z00_2552, BgL_ez00_16);
												}
											}
									}
								else
									{	/* Expand/garith.scm 166 */
										if (NULLP(CDR(((obj_t) BgL_cdrzd211340zd2_2534))))
											{	/* Expand/garith.scm 166 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string3781z00zzexpand_garithmetiquez00,
													BGl_string3782z00zzexpand_garithmetiquez00,
													BgL_xz00_15);
											}
										else
											{	/* Expand/garith.scm 166 */
												obj_t BgL_arg3538z00_2548;
												obj_t BgL_arg3539z00_2549;

												BgL_arg3538z00_2548 =
													CAR(((obj_t) BgL_cdrzd211340zd2_2534));
												BgL_arg3539z00_2549 =
													CDR(((obj_t) BgL_cdrzd211340zd2_2534));
												{
													obj_t BgL_yz00_7834;
													obj_t BgL_xz00_7833;

													BgL_xz00_7833 = BgL_arg3538z00_2548;
													BgL_yz00_7834 = BgL_arg3539z00_2549;
													BgL_yz00_2530 = BgL_yz00_7834;
													BgL_xz00_2529 = BgL_xz00_7833;
													goto BgL_tagzd211335zd2_2531;
												}
											}
									}
							}
						else
							{	/* Expand/garith.scm 166 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/garith.scm 166 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-g= */
	obj_t BGl_z62expandzd2gzd3z63zzexpand_garithmetiquez00(obj_t BgL_envz00_4437,
		obj_t BgL_xz00_4438, obj_t BgL_ez00_4439)
	{
		{	/* Expand/garith.scm 165 */
			return
				BGl_expandzd2gzd3z01zzexpand_garithmetiquez00(BgL_xz00_4438,
				BgL_ez00_4439);
		}

	}



/* expand-g< */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gzc3z11zzexpand_garithmetiquez00(obj_t
		BgL_xz00_17, obj_t BgL_ez00_18)
	{
		{	/* Expand/garith.scm 177 */
			{
				obj_t BgL_xz00_2563;
				obj_t BgL_yz00_2564;

				if (PAIRP(BgL_xz00_17))
					{	/* Expand/garith.scm 178 */
						obj_t BgL_cdrzd211396zd2_2568;

						BgL_cdrzd211396zd2_2568 = CDR(((obj_t) BgL_xz00_17));
						if (PAIRP(BgL_cdrzd211396zd2_2568))
							{	/* Expand/garith.scm 178 */
								obj_t BgL_cdrzd211398zd2_2570;

								BgL_cdrzd211398zd2_2570 = CDR(BgL_cdrzd211396zd2_2568);
								if (PAIRP(BgL_cdrzd211398zd2_2570))
									{	/* Expand/garith.scm 178 */
										if (NULLP(CDR(BgL_cdrzd211398zd2_2570)))
											{	/* Expand/garith.scm 178 */
												return
													BGl_expandzd2g2zd2zzexpand_garithmetiquez00
													(BgL_xz00_17, BgL_ez00_18,
													BGl_zc3zd2envz11zz__r4_numbers_6_5z00);
											}
										else
											{	/* Expand/garith.scm 178 */
												obj_t BgL_arg3560z00_2575;
												obj_t BgL_arg3561z00_2576;

												BgL_arg3560z00_2575 =
													CAR(((obj_t) BgL_cdrzd211396zd2_2568));
												BgL_arg3561z00_2576 =
													CDR(((obj_t) BgL_cdrzd211396zd2_2568));
												BgL_xz00_2563 = BgL_arg3560z00_2575;
												BgL_yz00_2564 = BgL_arg3561z00_2576;
											BgL_tagzd211391zd2_2565:
												{	/* Expand/garith.scm 184 */
													obj_t BgL_arg3572z00_2586;

													{	/* Expand/garith.scm 184 */
														obj_t BgL_arg3573z00_2587;

														{	/* Expand/garith.scm 184 */
															obj_t BgL_arg3574z00_2588;
															obj_t BgL_arg3575z00_2589;

															{	/* Expand/garith.scm 184 */
																obj_t BgL_arg3576z00_2590;

																{	/* Expand/garith.scm 184 */
																	obj_t BgL_arg3577z00_2591;

																	{	/* Expand/garith.scm 184 */
																		obj_t BgL_arg3578z00_2592;

																		BgL_arg3578z00_2592 =
																			CAR(((obj_t) BgL_yz00_2564));
																		BgL_arg3577z00_2591 =
																			MAKE_YOUNG_PAIR(BgL_arg3578z00_2592,
																			BNIL);
																	}
																	BgL_arg3576z00_2590 =
																		MAKE_YOUNG_PAIR(BgL_xz00_2563,
																		BgL_arg3577z00_2591);
																}
																BgL_arg3574z00_2588 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(25),
																	BgL_arg3576z00_2590);
															}
															{	/* Expand/garith.scm 184 */
																obj_t BgL_arg3580z00_2593;

																{	/* Expand/garith.scm 184 */
																	obj_t BgL_arg3581z00_2594;

																	BgL_arg3581z00_2594 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_2564, BNIL);
																	BgL_arg3580z00_2593 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(26),
																		BgL_arg3581z00_2594);
																}
																BgL_arg3575z00_2589 =
																	MAKE_YOUNG_PAIR(BgL_arg3580z00_2593, BNIL);
															}
															BgL_arg3573z00_2587 =
																MAKE_YOUNG_PAIR(BgL_arg3574z00_2588,
																BgL_arg3575z00_2589);
														}
														BgL_arg3572z00_2586 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
															BgL_arg3573z00_2587);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_18,
														BgL_arg3572z00_2586, BgL_ez00_18);
												}
											}
									}
								else
									{	/* Expand/garith.scm 178 */
										if (NULLP(CDR(((obj_t) BgL_cdrzd211396zd2_2568))))
											{	/* Expand/garith.scm 178 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string3783z00zzexpand_garithmetiquez00,
													BGl_string3782z00zzexpand_garithmetiquez00,
													BgL_xz00_17);
											}
										else
											{	/* Expand/garith.scm 178 */
												obj_t BgL_arg3567z00_2582;
												obj_t BgL_arg3568z00_2583;

												BgL_arg3567z00_2582 =
													CAR(((obj_t) BgL_cdrzd211396zd2_2568));
												BgL_arg3568z00_2583 =
													CDR(((obj_t) BgL_cdrzd211396zd2_2568));
												{
													obj_t BgL_yz00_7881;
													obj_t BgL_xz00_7880;

													BgL_xz00_7880 = BgL_arg3567z00_2582;
													BgL_yz00_7881 = BgL_arg3568z00_2583;
													BgL_yz00_2564 = BgL_yz00_7881;
													BgL_xz00_2563 = BgL_xz00_7880;
													goto BgL_tagzd211391zd2_2565;
												}
											}
									}
							}
						else
							{	/* Expand/garith.scm 178 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/garith.scm 178 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-g< */
	obj_t BGl_z62expandzd2gzc3z73zzexpand_garithmetiquez00(obj_t BgL_envz00_4444,
		obj_t BgL_xz00_4445, obj_t BgL_ez00_4446)
	{
		{	/* Expand/garith.scm 177 */
			return
				BGl_expandzd2gzc3z11zzexpand_garithmetiquez00(BgL_xz00_4445,
				BgL_ez00_4446);
		}

	}



/* expand-g> */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gze3z31zzexpand_garithmetiquez00(obj_t
		BgL_xz00_19, obj_t BgL_ez00_20)
	{
		{	/* Expand/garith.scm 189 */
			{
				obj_t BgL_xz00_2597;
				obj_t BgL_yz00_2598;

				if (PAIRP(BgL_xz00_19))
					{	/* Expand/garith.scm 190 */
						obj_t BgL_cdrzd211452zd2_2602;

						BgL_cdrzd211452zd2_2602 = CDR(((obj_t) BgL_xz00_19));
						if (PAIRP(BgL_cdrzd211452zd2_2602))
							{	/* Expand/garith.scm 190 */
								obj_t BgL_cdrzd211454zd2_2604;

								BgL_cdrzd211454zd2_2604 = CDR(BgL_cdrzd211452zd2_2602);
								if (PAIRP(BgL_cdrzd211454zd2_2604))
									{	/* Expand/garith.scm 190 */
										if (NULLP(CDR(BgL_cdrzd211454zd2_2604)))
											{	/* Expand/garith.scm 190 */
												return
													BGl_expandzd2g2zd2zzexpand_garithmetiquez00
													(BgL_xz00_19, BgL_ez00_20,
													BGl_ze3zd2envz31zz__r4_numbers_6_5z00);
											}
										else
											{	/* Expand/garith.scm 190 */
												obj_t BgL_arg3587z00_2609;
												obj_t BgL_arg3589z00_2610;

												BgL_arg3587z00_2609 =
													CAR(((obj_t) BgL_cdrzd211452zd2_2602));
												BgL_arg3589z00_2610 =
													CDR(((obj_t) BgL_cdrzd211452zd2_2602));
												BgL_xz00_2597 = BgL_arg3587z00_2609;
												BgL_yz00_2598 = BgL_arg3589z00_2610;
											BgL_tagzd211447zd2_2599:
												{	/* Expand/garith.scm 196 */
													obj_t BgL_arg3599z00_2620;

													{	/* Expand/garith.scm 196 */
														obj_t BgL_arg3600z00_2621;

														{	/* Expand/garith.scm 196 */
															obj_t BgL_arg3601z00_2622;
															obj_t BgL_arg3603z00_2623;

															{	/* Expand/garith.scm 196 */
																obj_t BgL_arg3605z00_2624;

																{	/* Expand/garith.scm 196 */
																	obj_t BgL_arg3606z00_2625;

																	{	/* Expand/garith.scm 196 */
																		obj_t BgL_arg3607z00_2626;

																		BgL_arg3607z00_2626 =
																			CAR(((obj_t) BgL_yz00_2598));
																		BgL_arg3606z00_2625 =
																			MAKE_YOUNG_PAIR(BgL_arg3607z00_2626,
																			BNIL);
																	}
																	BgL_arg3605z00_2624 =
																		MAKE_YOUNG_PAIR(BgL_xz00_2597,
																		BgL_arg3606z00_2625);
																}
																BgL_arg3601z00_2622 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(27),
																	BgL_arg3605z00_2624);
															}
															{	/* Expand/garith.scm 196 */
																obj_t BgL_arg3609z00_2627;

																{	/* Expand/garith.scm 196 */
																	obj_t BgL_arg3610z00_2628;

																	BgL_arg3610z00_2628 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_2598, BNIL);
																	BgL_arg3609z00_2627 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(28),
																		BgL_arg3610z00_2628);
																}
																BgL_arg3603z00_2623 =
																	MAKE_YOUNG_PAIR(BgL_arg3609z00_2627, BNIL);
															}
															BgL_arg3600z00_2621 =
																MAKE_YOUNG_PAIR(BgL_arg3601z00_2622,
																BgL_arg3603z00_2623);
														}
														BgL_arg3599z00_2620 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
															BgL_arg3600z00_2621);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_20,
														BgL_arg3599z00_2620, BgL_ez00_20);
												}
											}
									}
								else
									{	/* Expand/garith.scm 190 */
										if (NULLP(CDR(((obj_t) BgL_cdrzd211452zd2_2602))))
											{	/* Expand/garith.scm 190 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string3784z00zzexpand_garithmetiquez00,
													BGl_string3782z00zzexpand_garithmetiquez00,
													BgL_xz00_19);
											}
										else
											{	/* Expand/garith.scm 190 */
												obj_t BgL_arg3595z00_2616;
												obj_t BgL_arg3596z00_2617;

												BgL_arg3595z00_2616 =
													CAR(((obj_t) BgL_cdrzd211452zd2_2602));
												BgL_arg3596z00_2617 =
													CDR(((obj_t) BgL_cdrzd211452zd2_2602));
												{
													obj_t BgL_yz00_7928;
													obj_t BgL_xz00_7927;

													BgL_xz00_7927 = BgL_arg3595z00_2616;
													BgL_yz00_7928 = BgL_arg3596z00_2617;
													BgL_yz00_2598 = BgL_yz00_7928;
													BgL_xz00_2597 = BgL_xz00_7927;
													goto BgL_tagzd211447zd2_2599;
												}
											}
									}
							}
						else
							{	/* Expand/garith.scm 190 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/garith.scm 190 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-g> */
	obj_t BGl_z62expandzd2gze3z53zzexpand_garithmetiquez00(obj_t BgL_envz00_4451,
		obj_t BgL_xz00_4452, obj_t BgL_ez00_4453)
	{
		{	/* Expand/garith.scm 189 */
			return
				BGl_expandzd2gze3z31zzexpand_garithmetiquez00(BgL_xz00_4452,
				BgL_ez00_4453);
		}

	}



/* expand-g<= */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gzc3zd3zc2zzexpand_garithmetiquez00(obj_t
		BgL_xz00_21, obj_t BgL_ez00_22)
	{
		{	/* Expand/garith.scm 201 */
			{
				obj_t BgL_xz00_2631;
				obj_t BgL_yz00_2632;

				if (PAIRP(BgL_xz00_21))
					{	/* Expand/garith.scm 202 */
						obj_t BgL_cdrzd211508zd2_2636;

						BgL_cdrzd211508zd2_2636 = CDR(((obj_t) BgL_xz00_21));
						if (PAIRP(BgL_cdrzd211508zd2_2636))
							{	/* Expand/garith.scm 202 */
								obj_t BgL_cdrzd211510zd2_2638;

								BgL_cdrzd211510zd2_2638 = CDR(BgL_cdrzd211508zd2_2636);
								if (PAIRP(BgL_cdrzd211510zd2_2638))
									{	/* Expand/garith.scm 202 */
										if (NULLP(CDR(BgL_cdrzd211510zd2_2638)))
											{	/* Expand/garith.scm 202 */
												return
													BGl_expandzd2g2zd2zzexpand_garithmetiquez00
													(BgL_xz00_21, BgL_ez00_22,
													BGl_zc3zd3zd2envzc2zz__r4_numbers_6_5z00);
											}
										else
											{	/* Expand/garith.scm 202 */
												obj_t BgL_arg3616z00_2643;
												obj_t BgL_arg3618z00_2644;

												BgL_arg3616z00_2643 =
													CAR(((obj_t) BgL_cdrzd211508zd2_2636));
												BgL_arg3618z00_2644 =
													CDR(((obj_t) BgL_cdrzd211508zd2_2636));
												BgL_xz00_2631 = BgL_arg3616z00_2643;
												BgL_yz00_2632 = BgL_arg3618z00_2644;
											BgL_tagzd211503zd2_2633:
												{	/* Expand/garith.scm 208 */
													obj_t BgL_arg3629z00_2654;

													{	/* Expand/garith.scm 208 */
														obj_t BgL_arg3630z00_2655;

														{	/* Expand/garith.scm 208 */
															obj_t BgL_arg3631z00_2656;
															obj_t BgL_arg3632z00_2657;

															{	/* Expand/garith.scm 208 */
																obj_t BgL_arg3634z00_2658;

																{	/* Expand/garith.scm 208 */
																	obj_t BgL_arg3635z00_2659;

																	{	/* Expand/garith.scm 208 */
																		obj_t BgL_arg3636z00_2660;

																		BgL_arg3636z00_2660 =
																			CAR(((obj_t) BgL_yz00_2632));
																		BgL_arg3635z00_2659 =
																			MAKE_YOUNG_PAIR(BgL_arg3636z00_2660,
																			BNIL);
																	}
																	BgL_arg3634z00_2658 =
																		MAKE_YOUNG_PAIR(BgL_xz00_2631,
																		BgL_arg3635z00_2659);
																}
																BgL_arg3631z00_2656 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(29),
																	BgL_arg3634z00_2658);
															}
															{	/* Expand/garith.scm 208 */
																obj_t BgL_arg3637z00_2661;

																{	/* Expand/garith.scm 208 */
																	obj_t BgL_arg3639z00_2662;

																	BgL_arg3639z00_2662 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_2632, BNIL);
																	BgL_arg3637z00_2661 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(30),
																		BgL_arg3639z00_2662);
																}
																BgL_arg3632z00_2657 =
																	MAKE_YOUNG_PAIR(BgL_arg3637z00_2661, BNIL);
															}
															BgL_arg3630z00_2655 =
																MAKE_YOUNG_PAIR(BgL_arg3631z00_2656,
																BgL_arg3632z00_2657);
														}
														BgL_arg3629z00_2654 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
															BgL_arg3630z00_2655);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_22,
														BgL_arg3629z00_2654, BgL_ez00_22);
												}
											}
									}
								else
									{	/* Expand/garith.scm 202 */
										if (NULLP(CDR(((obj_t) BgL_cdrzd211508zd2_2636))))
											{	/* Expand/garith.scm 202 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string3785z00zzexpand_garithmetiquez00,
													BGl_string3782z00zzexpand_garithmetiquez00,
													BgL_xz00_21);
											}
										else
											{	/* Expand/garith.scm 202 */
												obj_t BgL_arg3624z00_2650;
												obj_t BgL_arg3625z00_2651;

												BgL_arg3624z00_2650 =
													CAR(((obj_t) BgL_cdrzd211508zd2_2636));
												BgL_arg3625z00_2651 =
													CDR(((obj_t) BgL_cdrzd211508zd2_2636));
												{
													obj_t BgL_yz00_7975;
													obj_t BgL_xz00_7974;

													BgL_xz00_7974 = BgL_arg3624z00_2650;
													BgL_yz00_7975 = BgL_arg3625z00_2651;
													BgL_yz00_2632 = BgL_yz00_7975;
													BgL_xz00_2631 = BgL_xz00_7974;
													goto BgL_tagzd211503zd2_2633;
												}
											}
									}
							}
						else
							{	/* Expand/garith.scm 202 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/garith.scm 202 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-g<= */
	obj_t BGl_z62expandzd2gzc3zd3za0zzexpand_garithmetiquez00(obj_t
		BgL_envz00_4458, obj_t BgL_xz00_4459, obj_t BgL_ez00_4460)
	{
		{	/* Expand/garith.scm 201 */
			return
				BGl_expandzd2gzc3zd3zc2zzexpand_garithmetiquez00(BgL_xz00_4459,
				BgL_ez00_4460);
		}

	}



/* expand-g>= */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gze3zd3ze2zzexpand_garithmetiquez00(obj_t
		BgL_xz00_23, obj_t BgL_ez00_24)
	{
		{	/* Expand/garith.scm 213 */
			{
				obj_t BgL_xz00_2665;
				obj_t BgL_yz00_2666;

				if (PAIRP(BgL_xz00_23))
					{	/* Expand/garith.scm 214 */
						obj_t BgL_cdrzd211564zd2_2670;

						BgL_cdrzd211564zd2_2670 = CDR(((obj_t) BgL_xz00_23));
						if (PAIRP(BgL_cdrzd211564zd2_2670))
							{	/* Expand/garith.scm 214 */
								obj_t BgL_cdrzd211566zd2_2672;

								BgL_cdrzd211566zd2_2672 = CDR(BgL_cdrzd211564zd2_2670);
								if (PAIRP(BgL_cdrzd211566zd2_2672))
									{	/* Expand/garith.scm 214 */
										if (NULLP(CDR(BgL_cdrzd211566zd2_2672)))
											{	/* Expand/garith.scm 214 */
												return
													BGl_expandzd2g2zd2zzexpand_garithmetiquez00
													(BgL_xz00_23, BgL_ez00_24,
													BGl_ze3zd3zd2envze2zz__r4_numbers_6_5z00);
											}
										else
											{	/* Expand/garith.scm 214 */
												obj_t BgL_arg3645z00_2677;
												obj_t BgL_arg3646z00_2678;

												BgL_arg3645z00_2677 =
													CAR(((obj_t) BgL_cdrzd211564zd2_2670));
												BgL_arg3646z00_2678 =
													CDR(((obj_t) BgL_cdrzd211564zd2_2670));
												BgL_xz00_2665 = BgL_arg3645z00_2677;
												BgL_yz00_2666 = BgL_arg3646z00_2678;
											BgL_tagzd211559zd2_2667:
												{	/* Expand/garith.scm 220 */
													obj_t BgL_arg3658z00_2688;

													{	/* Expand/garith.scm 220 */
														obj_t BgL_arg3659z00_2689;

														{	/* Expand/garith.scm 220 */
															obj_t BgL_arg3660z00_2690;
															obj_t BgL_arg3661z00_2691;

															{	/* Expand/garith.scm 220 */
																obj_t BgL_arg3662z00_2692;

																{	/* Expand/garith.scm 220 */
																	obj_t BgL_arg3664z00_2693;

																	{	/* Expand/garith.scm 220 */
																		obj_t BgL_arg3665z00_2694;

																		BgL_arg3665z00_2694 =
																			CAR(((obj_t) BgL_yz00_2666));
																		BgL_arg3664z00_2693 =
																			MAKE_YOUNG_PAIR(BgL_arg3665z00_2694,
																			BNIL);
																	}
																	BgL_arg3662z00_2692 =
																		MAKE_YOUNG_PAIR(BgL_xz00_2665,
																		BgL_arg3664z00_2693);
																}
																BgL_arg3660z00_2690 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(31),
																	BgL_arg3662z00_2692);
															}
															{	/* Expand/garith.scm 220 */
																obj_t BgL_arg3668z00_2695;

																{	/* Expand/garith.scm 220 */
																	obj_t BgL_arg3669z00_2696;

																	BgL_arg3669z00_2696 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_yz00_2666, BNIL);
																	BgL_arg3668z00_2695 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(32),
																		BgL_arg3669z00_2696);
																}
																BgL_arg3661z00_2691 =
																	MAKE_YOUNG_PAIR(BgL_arg3668z00_2695, BNIL);
															}
															BgL_arg3659z00_2689 =
																MAKE_YOUNG_PAIR(BgL_arg3660z00_2690,
																BgL_arg3661z00_2691);
														}
														BgL_arg3658z00_2688 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
															BgL_arg3659z00_2689);
													}
													return
														BGL_PROCEDURE_CALL2(BgL_ez00_24,
														BgL_arg3658z00_2688, BgL_ez00_24);
												}
											}
									}
								else
									{	/* Expand/garith.scm 214 */
										if (NULLP(CDR(((obj_t) BgL_cdrzd211564zd2_2670))))
											{	/* Expand/garith.scm 214 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string3786z00zzexpand_garithmetiquez00,
													BGl_string3782z00zzexpand_garithmetiquez00,
													BgL_xz00_23);
											}
										else
											{	/* Expand/garith.scm 214 */
												obj_t BgL_arg3653z00_2684;
												obj_t BgL_arg3654z00_2685;

												BgL_arg3653z00_2684 =
													CAR(((obj_t) BgL_cdrzd211564zd2_2670));
												BgL_arg3654z00_2685 =
													CDR(((obj_t) BgL_cdrzd211564zd2_2670));
												{
													obj_t BgL_yz00_8022;
													obj_t BgL_xz00_8021;

													BgL_xz00_8021 = BgL_arg3653z00_2684;
													BgL_yz00_8022 = BgL_arg3654z00_2685;
													BgL_yz00_2666 = BgL_yz00_8022;
													BgL_xz00_2665 = BgL_xz00_8021;
													goto BgL_tagzd211559zd2_2667;
												}
											}
									}
							}
						else
							{	/* Expand/garith.scm 214 */
								return BFALSE;
							}
					}
				else
					{	/* Expand/garith.scm 214 */
						return BFALSE;
					}
			}
		}

	}



/* &expand-g>= */
	obj_t BGl_z62expandzd2gze3zd3z80zzexpand_garithmetiquez00(obj_t
		BgL_envz00_4465, obj_t BgL_xz00_4466, obj_t BgL_ez00_4467)
	{
		{	/* Expand/garith.scm 213 */
			return
				BGl_expandzd2gze3zd3ze2zzexpand_garithmetiquez00(BgL_xz00_4466,
				BgL_ez00_4467);
		}

	}



/* expand-gmax */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gmaxzd2zzexpand_garithmetiquez00(obj_t
		BgL_xz00_25, obj_t BgL_ez00_26)
	{
		{	/* Expand/garith.scm 225 */
			{
				obj_t BgL_xz00_2702;
				obj_t BgL_yz00_2703;
				obj_t BgL_xz00_2699;
				obj_t BgL_yz00_2700;

				if (PAIRP(BgL_xz00_25))
					{	/* Expand/garith.scm 226 */
						obj_t BgL_cdrzd211622zd2_2708;

						BgL_cdrzd211622zd2_2708 = CDR(((obj_t) BgL_xz00_25));
						if (PAIRP(BgL_cdrzd211622zd2_2708))
							{	/* Expand/garith.scm 226 */
								if (NULLP(CDR(BgL_cdrzd211622zd2_2708)))
									{	/* Expand/garith.scm 226 */
										obj_t BgL_arg3674z00_2712;

										BgL_arg3674z00_2712 = CAR(BgL_cdrzd211622zd2_2708);
										return
											BGL_PROCEDURE_CALL2(BgL_ez00_26, BgL_arg3674z00_2712,
											BgL_ez00_26);
									}
								else
									{	/* Expand/garith.scm 226 */
										obj_t BgL_cdrzd211638zd2_2714;

										BgL_cdrzd211638zd2_2714 =
											CDR(((obj_t) BgL_cdrzd211622zd2_2708));
										if (PAIRP(BgL_cdrzd211638zd2_2714))
											{	/* Expand/garith.scm 226 */
												if (NULLP(CDR(BgL_cdrzd211638zd2_2714)))
													{	/* Expand/garith.scm 226 */
														obj_t BgL_arg3680z00_2718;
														obj_t BgL_arg3681z00_2719;

														BgL_arg3680z00_2718 =
															CAR(((obj_t) BgL_cdrzd211622zd2_2708));
														BgL_arg3681z00_2719 = CAR(BgL_cdrzd211638zd2_2714);
														BgL_xz00_2699 = BgL_arg3680z00_2718;
														BgL_yz00_2700 = BgL_arg3681z00_2719;
														{	/* Expand/garith.scm 231 */
															bool_t BgL_test4214z00_8049;

															if (INTEGERP(BgL_xz00_2699))
																{	/* Expand/garith.scm 231 */
																	BgL_test4214z00_8049 =
																		INTEGERP(BgL_yz00_2700);
																}
															else
																{	/* Expand/garith.scm 231 */
																	BgL_test4214z00_8049 = ((bool_t) 0);
																}
															if (BgL_test4214z00_8049)
																{	/* Expand/garith.scm 231 */
																	if (
																		((long) CINT(BgL_xz00_2699) >
																			(long) CINT(BgL_yz00_2700)))
																		{	/* Expand/garith.scm 232 */
																			return BgL_xz00_2699;
																		}
																	else
																		{	/* Expand/garith.scm 232 */
																			return BgL_yz00_2700;
																		}
																}
															else
																{	/* Expand/garith.scm 234 */
																	obj_t BgL_arg3694z00_2733;

																	{	/* Expand/garith.scm 234 */
																		obj_t BgL_arg3695z00_2734;
																		obj_t BgL_arg3696z00_2735;

																		{	/* Expand/garith.scm 234 */
																			obj_t BgL_arg3697z00_2736;
																			obj_t BgL_arg3699z00_2737;

																			{	/* Expand/garith.scm 234 */
																				obj_t BgL_arg3700z00_2738;

																				{	/* Expand/garith.scm 234 */
																					obj_t BgL_arg3703z00_2739;

																					BgL_arg3703z00_2739 =
																						BGL_PROCEDURE_CALL2(BgL_ez00_26,
																						BgL_xz00_2699, BgL_ez00_26);
																					BgL_arg3700z00_2738 =
																						MAKE_YOUNG_PAIR(BgL_arg3703z00_2739,
																						BNIL);
																				}
																				BgL_arg3697z00_2736 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(33),
																					BgL_arg3700z00_2738);
																			}
																			{	/* Expand/garith.scm 235 */
																				obj_t BgL_arg3704z00_2740;

																				{	/* Expand/garith.scm 235 */
																					obj_t BgL_arg3705z00_2741;

																					{	/* Expand/garith.scm 235 */
																						obj_t BgL_arg3706z00_2742;

																						BgL_arg3706z00_2742 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_26,
																							BgL_yz00_2700, BgL_ez00_26);
																						BgL_arg3705z00_2741 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg3706z00_2742, BNIL);
																					}
																					BgL_arg3704z00_2740 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																						BgL_arg3705z00_2741);
																				}
																				BgL_arg3699z00_2737 =
																					MAKE_YOUNG_PAIR(BgL_arg3704z00_2740,
																					BNIL);
																			}
																			BgL_arg3695z00_2734 =
																				MAKE_YOUNG_PAIR(BgL_arg3697z00_2736,
																				BgL_arg3699z00_2737);
																		}
																		{	/* Expand/garith.scm 236 */
																			obj_t BgL_arg3707z00_2743;

																			{	/* Expand/garith.scm 236 */
																				obj_t BgL_arg3708z00_2744;

																				{	/* Expand/garith.scm 236 */
																					obj_t BgL_arg3710z00_2745;

																					BgL_arg3710z00_2745 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																						BNIL);
																					BgL_arg3708z00_2744 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(33),
																						BgL_arg3710z00_2745);
																				}
																				BgL_arg3707z00_2743 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(34),
																					BgL_arg3708z00_2744);
																			}
																			BgL_arg3696z00_2735 =
																				MAKE_YOUNG_PAIR(BgL_arg3707z00_2743,
																				BNIL);
																		}
																		BgL_arg3694z00_2733 =
																			MAKE_YOUNG_PAIR(BgL_arg3695z00_2734,
																			BgL_arg3696z00_2735);
																	}
																	return
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																		BgL_arg3694z00_2733);
																}
														}
													}
												else
													{	/* Expand/garith.scm 226 */
														obj_t BgL_arg3683z00_2721;
														obj_t BgL_arg3684z00_2722;

														BgL_arg3683z00_2721 =
															CAR(((obj_t) BgL_cdrzd211622zd2_2708));
														BgL_arg3684z00_2722 =
															CDR(((obj_t) BgL_cdrzd211622zd2_2708));
														BgL_xz00_2702 = BgL_arg3683z00_2721;
														BgL_yz00_2703 = BgL_arg3684z00_2722;
													BgL_tagzd211615zd2_2704:
														{	/* Expand/garith.scm 238 */
															obj_t BgL_arg3713z00_2747;

															{	/* Expand/garith.scm 238 */
																obj_t BgL_arg3714z00_2748;

																{	/* Expand/garith.scm 238 */
																	obj_t BgL_arg3715z00_2749;

																	{	/* Expand/garith.scm 238 */
																		obj_t BgL_arg3716z00_2750;

																		{	/* Expand/garith.scm 238 */
																			obj_t BgL_arg3717z00_2751;

																			BgL_arg3717z00_2751 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_yz00_2703, BNIL);
																			BgL_arg3716z00_2750 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(35),
																				BgL_arg3717z00_2751);
																		}
																		BgL_arg3715z00_2749 =
																			MAKE_YOUNG_PAIR(BgL_arg3716z00_2750,
																			BNIL);
																	}
																	BgL_arg3714z00_2748 =
																		MAKE_YOUNG_PAIR(BgL_xz00_2702,
																		BgL_arg3715z00_2749);
																}
																BgL_arg3713z00_2747 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(34),
																	BgL_arg3714z00_2748);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_26,
																BgL_arg3713z00_2747, BgL_ez00_26);
														}
													}
											}
										else
											{	/* Expand/garith.scm 226 */
												obj_t BgL_arg3687z00_2725;
												obj_t BgL_arg3688z00_2726;

												BgL_arg3687z00_2725 =
													CAR(((obj_t) BgL_cdrzd211622zd2_2708));
												BgL_arg3688z00_2726 =
													CDR(((obj_t) BgL_cdrzd211622zd2_2708));
												{
													obj_t BgL_yz00_8106;
													obj_t BgL_xz00_8105;

													BgL_xz00_8105 = BgL_arg3687z00_2725;
													BgL_yz00_8106 = BgL_arg3688z00_2726;
													BgL_yz00_2703 = BgL_yz00_8106;
													BgL_xz00_2702 = BgL_xz00_8105;
													goto BgL_tagzd211615zd2_2704;
												}
											}
									}
							}
						else
							{	/* Expand/garith.scm 226 */
							BgL_tagzd211616zd2_2705:
								return
									BGl_errorz00zz__errorz00(BFALSE,
									BGl_string3787z00zzexpand_garithmetiquez00, BgL_xz00_25);
							}
					}
				else
					{	/* Expand/garith.scm 226 */
						goto BgL_tagzd211616zd2_2705;
					}
			}
		}

	}



/* &expand-gmax */
	obj_t BGl_z62expandzd2gmaxzb0zzexpand_garithmetiquez00(obj_t BgL_envz00_4472,
		obj_t BgL_xz00_4473, obj_t BgL_ez00_4474)
	{
		{	/* Expand/garith.scm 225 */
			return
				BGl_expandzd2gmaxzd2zzexpand_garithmetiquez00(BgL_xz00_4473,
				BgL_ez00_4474);
		}

	}



/* expand-gmin */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2gminzd2zzexpand_garithmetiquez00(obj_t
		BgL_xz00_27, obj_t BgL_ez00_28)
	{
		{	/* Expand/garith.scm 245 */
			{
				obj_t BgL_xz00_2757;
				obj_t BgL_yz00_2758;
				obj_t BgL_xz00_2754;
				obj_t BgL_yz00_2755;

				if (PAIRP(BgL_xz00_27))
					{	/* Expand/garith.scm 246 */
						obj_t BgL_cdrzd211700zd2_2763;

						BgL_cdrzd211700zd2_2763 = CDR(((obj_t) BgL_xz00_27));
						if (PAIRP(BgL_cdrzd211700zd2_2763))
							{	/* Expand/garith.scm 246 */
								if (NULLP(CDR(BgL_cdrzd211700zd2_2763)))
									{	/* Expand/garith.scm 246 */
										obj_t BgL_arg3722z00_2767;

										BgL_arg3722z00_2767 = CAR(BgL_cdrzd211700zd2_2763);
										return
											BGL_PROCEDURE_CALL2(BgL_ez00_28, BgL_arg3722z00_2767,
											BgL_ez00_28);
									}
								else
									{	/* Expand/garith.scm 246 */
										obj_t BgL_cdrzd211716zd2_2769;

										BgL_cdrzd211716zd2_2769 =
											CDR(((obj_t) BgL_cdrzd211700zd2_2763));
										if (PAIRP(BgL_cdrzd211716zd2_2769))
											{	/* Expand/garith.scm 246 */
												if (NULLP(CDR(BgL_cdrzd211716zd2_2769)))
													{	/* Expand/garith.scm 246 */
														obj_t BgL_arg3727z00_2773;
														obj_t BgL_arg3728z00_2774;

														BgL_arg3727z00_2773 =
															CAR(((obj_t) BgL_cdrzd211700zd2_2763));
														BgL_arg3728z00_2774 = CAR(BgL_cdrzd211716zd2_2769);
														BgL_xz00_2754 = BgL_arg3727z00_2773;
														BgL_yz00_2755 = BgL_arg3728z00_2774;
														{	/* Expand/garith.scm 251 */
															bool_t BgL_test4222z00_8134;

															if (INTEGERP(BgL_xz00_2754))
																{	/* Expand/garith.scm 251 */
																	BgL_test4222z00_8134 =
																		INTEGERP(BgL_yz00_2755);
																}
															else
																{	/* Expand/garith.scm 251 */
																	BgL_test4222z00_8134 = ((bool_t) 0);
																}
															if (BgL_test4222z00_8134)
																{	/* Expand/garith.scm 251 */
																	if (
																		((long) CINT(BgL_xz00_2754) <
																			(long) CINT(BgL_yz00_2755)))
																		{	/* Expand/garith.scm 252 */
																			return BgL_xz00_2754;
																		}
																	else
																		{	/* Expand/garith.scm 252 */
																			return BgL_yz00_2755;
																		}
																}
															else
																{	/* Expand/garith.scm 254 */
																	obj_t BgL_arg3740z00_2788;

																	{	/* Expand/garith.scm 254 */
																		obj_t BgL_arg3741z00_2789;
																		obj_t BgL_arg3743z00_2790;

																		{	/* Expand/garith.scm 254 */
																			obj_t BgL_arg3744z00_2791;
																			obj_t BgL_arg3745z00_2792;

																			{	/* Expand/garith.scm 254 */
																				obj_t BgL_arg3747z00_2793;

																				{	/* Expand/garith.scm 254 */
																					obj_t BgL_arg3748z00_2794;

																					BgL_arg3748z00_2794 =
																						BGL_PROCEDURE_CALL2(BgL_ez00_28,
																						BgL_xz00_2754, BgL_ez00_28);
																					BgL_arg3747z00_2793 =
																						MAKE_YOUNG_PAIR(BgL_arg3748z00_2794,
																						BNIL);
																				}
																				BgL_arg3744z00_2791 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(33),
																					BgL_arg3747z00_2793);
																			}
																			{	/* Expand/garith.scm 255 */
																				obj_t BgL_arg3749z00_2795;

																				{	/* Expand/garith.scm 255 */
																					obj_t BgL_arg3750z00_2796;

																					{	/* Expand/garith.scm 255 */
																						obj_t BgL_arg3751z00_2797;

																						BgL_arg3751z00_2797 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_28,
																							BgL_yz00_2755, BgL_ez00_28);
																						BgL_arg3750z00_2796 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg3751z00_2797, BNIL);
																					}
																					BgL_arg3749z00_2795 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																						BgL_arg3750z00_2796);
																				}
																				BgL_arg3745z00_2792 =
																					MAKE_YOUNG_PAIR(BgL_arg3749z00_2795,
																					BNIL);
																			}
																			BgL_arg3741z00_2789 =
																				MAKE_YOUNG_PAIR(BgL_arg3744z00_2791,
																				BgL_arg3745z00_2792);
																		}
																		{	/* Expand/garith.scm 256 */
																			obj_t BgL_arg3752z00_2798;

																			{	/* Expand/garith.scm 256 */
																				obj_t BgL_arg3753z00_2799;

																				{	/* Expand/garith.scm 256 */
																					obj_t BgL_arg3755z00_2800;

																					BgL_arg3755z00_2800 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																						BNIL);
																					BgL_arg3753z00_2799 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(33),
																						BgL_arg3755z00_2800);
																				}
																				BgL_arg3752z00_2798 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(36),
																					BgL_arg3753z00_2799);
																			}
																			BgL_arg3743z00_2790 =
																				MAKE_YOUNG_PAIR(BgL_arg3752z00_2798,
																				BNIL);
																		}
																		BgL_arg3740z00_2788 =
																			MAKE_YOUNG_PAIR(BgL_arg3741z00_2789,
																			BgL_arg3743z00_2790);
																	}
																	return
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																		BgL_arg3740z00_2788);
																}
														}
													}
												else
													{	/* Expand/garith.scm 246 */
														obj_t BgL_arg3729z00_2776;
														obj_t BgL_arg3730z00_2777;

														BgL_arg3729z00_2776 =
															CAR(((obj_t) BgL_cdrzd211700zd2_2763));
														BgL_arg3730z00_2777 =
															CDR(((obj_t) BgL_cdrzd211700zd2_2763));
														BgL_xz00_2757 = BgL_arg3729z00_2776;
														BgL_yz00_2758 = BgL_arg3730z00_2777;
													BgL_tagzd211693zd2_2759:
														{	/* Expand/garith.scm 258 */
															obj_t BgL_arg3756z00_2802;

															{	/* Expand/garith.scm 258 */
																obj_t BgL_arg3757z00_2803;

																{	/* Expand/garith.scm 258 */
																	obj_t BgL_arg3759z00_2804;

																	{	/* Expand/garith.scm 258 */
																		obj_t BgL_arg3760z00_2805;

																		{	/* Expand/garith.scm 258 */
																			obj_t BgL_arg3761z00_2806;

																			BgL_arg3761z00_2806 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_yz00_2758, BNIL);
																			BgL_arg3760z00_2805 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(37),
																				BgL_arg3761z00_2806);
																		}
																		BgL_arg3759z00_2804 =
																			MAKE_YOUNG_PAIR(BgL_arg3760z00_2805,
																			BNIL);
																	}
																	BgL_arg3757z00_2803 =
																		MAKE_YOUNG_PAIR(BgL_xz00_2757,
																		BgL_arg3759z00_2804);
																}
																BgL_arg3756z00_2802 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(36),
																	BgL_arg3757z00_2803);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_ez00_28,
																BgL_arg3756z00_2802, BgL_ez00_28);
														}
													}
											}
										else
											{	/* Expand/garith.scm 246 */
												obj_t BgL_arg3733z00_2780;
												obj_t BgL_arg3734z00_2781;

												BgL_arg3733z00_2780 =
													CAR(((obj_t) BgL_cdrzd211700zd2_2763));
												BgL_arg3734z00_2781 =
													CDR(((obj_t) BgL_cdrzd211700zd2_2763));
												{
													obj_t BgL_yz00_8191;
													obj_t BgL_xz00_8190;

													BgL_xz00_8190 = BgL_arg3733z00_2780;
													BgL_yz00_8191 = BgL_arg3734z00_2781;
													BgL_yz00_2758 = BgL_yz00_8191;
													BgL_xz00_2757 = BgL_xz00_8190;
													goto BgL_tagzd211693zd2_2759;
												}
											}
									}
							}
						else
							{	/* Expand/garith.scm 246 */
							BgL_tagzd211694zd2_2760:
								return
									BGl_errorz00zz__errorz00(BFALSE,
									BGl_string3788z00zzexpand_garithmetiquez00, BgL_xz00_27);
							}
					}
				else
					{	/* Expand/garith.scm 246 */
						goto BgL_tagzd211694zd2_2760;
					}
			}
		}

	}



/* &expand-gmin */
	obj_t BGl_z62expandzd2gminzb0zzexpand_garithmetiquez00(obj_t BgL_envz00_4475,
		obj_t BgL_xz00_4476, obj_t BgL_ez00_4477)
	{
		{	/* Expand/garith.scm 245 */
			return
				BGl_expandzd2gminzd2zzexpand_garithmetiquez00(BgL_xz00_4476,
				BgL_ez00_4477);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_garithmetiquez00(void)
	{
		{	/* Expand/garith.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_garithmetiquez00(void)
	{
		{	/* Expand/garith.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_garithmetiquez00(void)
	{
		{	/* Expand/garith.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_garithmetiquez00(void)
	{
		{	/* Expand/garith.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string3789z00zzexpand_garithmetiquez00));
		}

	}

#ifdef __cplusplus
}
#endif
