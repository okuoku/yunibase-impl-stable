/*===========================================================================*/
/*   (Expand/case.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/case.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_CASE_TYPE_DEFINITIONS
#define BGL_EXPAND_CASE_TYPE_DEFINITIONS
#endif													// BGL_EXPAND_CASE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_dozd2cnstzd2casez00zzexpand_casez00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzexpand_casez00 = BUNSPEC;
	extern obj_t BGl_epairifyz00zztools_miscz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_casez00(void);
	static obj_t BGl_objectzd2initzd2zzexpand_casez00(void);
	static obj_t BGl_typezd2testzd2zzexpand_casez00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzexpand_casez00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2casezd2zzexpand_casez00(obj_t, obj_t);
	static bool_t BGl_typezd2matchzf3ze70zc6zzexpand_casez00(obj_t, obj_t);
	static obj_t BGl_casezd2typezd2zzexpand_casez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_dozd2genericzd2casez00zzexpand_casez00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_dozd2genericzd2symbolzf2keywordzd2casez20zzexpand_casez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzexpand_casez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_z62expandzd2casezb0zzexpand_casez00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzexpand_casez00(void);
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_casez00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_casez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_casez00(void);
	static obj_t BGl_dozd2typedzd2casez00zzexpand_casez00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_epairifyzd2reczd2zztools_miscz00(obj_t, obj_t);
	static obj_t
		BGl_dozd2optimzd2symbolzf2keywordzd2casez20zzexpand_casez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_generalze70ze7zzexpand_casez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static obj_t BGl_loopze70ze7zzexpand_casez00(obj_t);
	static obj_t BGl_loopze71ze7zzexpand_casez00(obj_t, obj_t);
	static obj_t BGl_loopze72ze7zzexpand_casez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2optimzd2symbolzd2caseza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t __cnst[30];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2casezd2envz00zzexpand_casez00,
		BgL_bgl_za762expandza7d2case2015z00,
		BGl_z62expandzd2casezb0zzexpand_casez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2006z00zzexpand_casez00,
		BgL_bgl_string2006za700za7za7e2016za7, "case", 4);
	      DEFINE_STRING(BGl_string2007z00zzexpand_casez00,
		BgL_bgl_string2007za700za7za7e2017za7, "Illegal `case' form", 19);
	      DEFINE_STRING(BGl_string2008z00zzexpand_casez00,
		BgL_bgl_string2008za700za7za7e2018za7, "case_else", 9);
	      DEFINE_STRING(BGl_string2009z00zzexpand_casez00,
		BgL_bgl_string2009za700za7za7e2019za7, "Illegal `case' clause", 21);
	      DEFINE_STRING(BGl_string2010z00zzexpand_casez00,
		BgL_bgl_string2010za700za7za7e2020za7, "type-test", 9);
	      DEFINE_STRING(BGl_string2011z00zzexpand_casez00,
		BgL_bgl_string2011za700za7za7e2021za7, "Unknown `case' type", 19);
	      DEFINE_STRING(BGl_string2012z00zzexpand_casez00,
		BgL_bgl_string2012za700za7za7e2022za7, "expand_case", 11);
	      DEFINE_STRING(BGl_string2013z00zzexpand_casez00,
		BgL_bgl_string2013za700za7za7e2023za7,
		"c-keyword? c-symbol? int32? uint32? $fixnum? labels c-char? memv or c-eq? eqv? quote case-value let if cnst->integer cnst? aux else etherogeneous case fail-type keyword symbol cnst char int32 uint32 long integer ",
		212);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzexpand_casez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzexpand_casez00(long
		BgL_checksumz00_894, char *BgL_fromz00_895)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_casez00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_casez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_casez00();
					BGl_libraryzd2moduleszd2initz00zzexpand_casez00();
					BGl_cnstzd2initzd2zzexpand_casez00();
					BGl_importedzd2moduleszd2initz00zzexpand_casez00();
					return BGl_methodzd2initzd2zzexpand_casez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_casez00(void)
	{
		{	/* Expand/case.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_case");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_case");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "expand_case");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_case");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "expand_case");
			BGl_modulezd2initializa7ationz75zz__prognz00(0L, "expand_case");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "expand_case");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "expand_case");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"expand_case");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_case");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"expand_case");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_casez00(void)
	{
		{	/* Expand/case.scm 15 */
			{	/* Expand/case.scm 15 */
				obj_t BgL_cportz00_883;

				{	/* Expand/case.scm 15 */
					obj_t BgL_stringz00_890;

					BgL_stringz00_890 = BGl_string2013z00zzexpand_casez00;
					{	/* Expand/case.scm 15 */
						obj_t BgL_startz00_891;

						BgL_startz00_891 = BINT(0L);
						{	/* Expand/case.scm 15 */
							obj_t BgL_endz00_892;

							BgL_endz00_892 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_890)));
							{	/* Expand/case.scm 15 */

								BgL_cportz00_883 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_890, BgL_startz00_891, BgL_endz00_892);
				}}}}
				{
					long BgL_iz00_884;

					BgL_iz00_884 = 29L;
				BgL_loopz00_885:
					if ((BgL_iz00_884 == -1L))
						{	/* Expand/case.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/case.scm 15 */
							{	/* Expand/case.scm 15 */
								obj_t BgL_arg2014z00_886;

								{	/* Expand/case.scm 15 */

									{	/* Expand/case.scm 15 */
										obj_t BgL_locationz00_888;

										BgL_locationz00_888 = BBOOL(((bool_t) 0));
										{	/* Expand/case.scm 15 */

											BgL_arg2014z00_886 =
												BGl_readz00zz__readerz00(BgL_cportz00_883,
												BgL_locationz00_888);
										}
									}
								}
								{	/* Expand/case.scm 15 */
									int BgL_tmpz00_924;

									BgL_tmpz00_924 = (int) (BgL_iz00_884);
									CNST_TABLE_SET(BgL_tmpz00_924, BgL_arg2014z00_886);
							}}
							{	/* Expand/case.scm 15 */
								int BgL_auxz00_889;

								BgL_auxz00_889 = (int) ((BgL_iz00_884 - 1L));
								{
									long BgL_iz00_929;

									BgL_iz00_929 = (long) (BgL_auxz00_889);
									BgL_iz00_884 = BgL_iz00_929;
									goto BgL_loopz00_885;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_casez00(void)
	{
		{	/* Expand/case.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* expand-case */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2casezd2zzexpand_casez00(obj_t BgL_xz00_3,
		obj_t BgL_ez00_4)
	{
		{	/* Expand/case.scm 34 */
			{
				obj_t BgL_valuez00_102;
				obj_t BgL_clausesz00_103;

				if (PAIRP(BgL_xz00_3))
					{	/* Expand/case.scm 36 */
						obj_t BgL_cdrzd2367zd2_108;

						BgL_cdrzd2367zd2_108 = CDR(((obj_t) BgL_xz00_3));
						if (PAIRP(BgL_cdrzd2367zd2_108))
							{	/* Expand/case.scm 36 */
								BgL_valuez00_102 = CAR(BgL_cdrzd2367zd2_108);
								BgL_clausesz00_103 = CDR(BgL_cdrzd2367zd2_108);
								{	/* Expand/case.scm 38 */
									obj_t BgL_casezd2valuezd2_112;

									BgL_casezd2valuezd2_112 =
										BGl_casezd2typezd2zzexpand_casez00(BgL_xz00_3,
										BgL_clausesz00_103);
									if ((BgL_casezd2valuezd2_112 == CNST_TABLE_REF(0)))
										{	/* Expand/case.scm 38 */
											return
												BGl_dozd2typedzd2casez00zzexpand_casez00(CNST_TABLE_REF
												(1), BgL_valuez00_102, BgL_clausesz00_103, BgL_ez00_4);
										}
									else
										{	/* Expand/case.scm 38 */
											if ((BgL_casezd2valuezd2_112 == CNST_TABLE_REF(2)))
												{	/* Expand/case.scm 38 */
													return
														BGl_dozd2typedzd2casez00zzexpand_casez00
														(CNST_TABLE_REF(2), BgL_valuez00_102,
														BgL_clausesz00_103, BgL_ez00_4);
												}
											else
												{	/* Expand/case.scm 38 */
													if ((BgL_casezd2valuezd2_112 == CNST_TABLE_REF(3)))
														{	/* Expand/case.scm 38 */
															return
																BGl_dozd2typedzd2casez00zzexpand_casez00
																(CNST_TABLE_REF(3), BgL_valuez00_102,
																BgL_clausesz00_103, BgL_ez00_4);
														}
													else
														{	/* Expand/case.scm 38 */
															if (
																(BgL_casezd2valuezd2_112 == CNST_TABLE_REF(4)))
																{	/* Expand/case.scm 38 */
																	return
																		BGl_dozd2typedzd2casez00zzexpand_casez00
																		(CNST_TABLE_REF(4), BgL_valuez00_102,
																		BgL_clausesz00_103, BgL_ez00_4);
																}
															else
																{	/* Expand/case.scm 38 */
																	if (
																		(BgL_casezd2valuezd2_112 ==
																			CNST_TABLE_REF(5)))
																		{	/* Expand/case.scm 38 */
																			return
																				BGl_dozd2cnstzd2casez00zzexpand_casez00
																				(BgL_valuez00_102, BgL_clausesz00_103,
																				BgL_ez00_4);
																		}
																	else
																		{	/* Expand/case.scm 38 */
																			if (
																				(BgL_casezd2valuezd2_112 ==
																					CNST_TABLE_REF(6)))
																				{	/* Expand/case.scm 38 */
																					{	/* Expand/case.scm 56 */
																						obj_t BgL_typez00_736;

																						BgL_typez00_736 = CNST_TABLE_REF(6);
																						if (CBOOL
																							(BGl_za2optimzd2symbolzd2caseza2z00zzengine_paramz00))
																							{	/* Expand/case.scm 212 */
																								return
																									BGl_dozd2optimzd2symbolzf2keywordzd2casez20zzexpand_casez00
																									(BgL_typez00_736,
																									BgL_valuez00_102,
																									BgL_clausesz00_103,
																									BgL_ez00_4);
																							}
																						else
																							{	/* Expand/case.scm 212 */
																								return
																									BGl_dozd2genericzd2symbolzf2keywordzd2casez20zzexpand_casez00
																									(BgL_valuez00_102,
																									BgL_clausesz00_103,
																									BgL_ez00_4);
																							}
																					}
																				}
																			else
																				{	/* Expand/case.scm 38 */
																					if (
																						(BgL_casezd2valuezd2_112 ==
																							CNST_TABLE_REF(7)))
																						{	/* Expand/case.scm 38 */
																							{	/* Expand/case.scm 59 */
																								obj_t BgL_typez00_738;

																								BgL_typez00_738 =
																									CNST_TABLE_REF(7);
																								if (CBOOL
																									(BGl_za2optimzd2symbolzd2caseza2z00zzengine_paramz00))
																									{	/* Expand/case.scm 212 */
																										return
																											BGl_dozd2optimzd2symbolzf2keywordzd2casez20zzexpand_casez00
																											(BgL_typez00_738,
																											BgL_valuez00_102,
																											BgL_clausesz00_103,
																											BgL_ez00_4);
																									}
																								else
																									{	/* Expand/case.scm 212 */
																										return
																											BGl_dozd2genericzd2symbolzf2keywordzd2casez20zzexpand_casez00
																											(BgL_valuez00_102,
																											BgL_clausesz00_103,
																											BgL_ez00_4);
																									}
																							}
																						}
																					else
																						{	/* Expand/case.scm 38 */
																							return
																								BGl_dozd2genericzd2casez00zzexpand_casez00
																								(BgL_valuez00_102,
																								BgL_clausesz00_103, BgL_ez00_4);
																						}
																				}
																		}
																}
														}
												}
										}
								}
							}
						else
							{	/* Expand/case.scm 36 */
							BgL_tagzd2360zd2_105:
								return
									BGl_errorz00zz__errorz00(BGl_string2006z00zzexpand_casez00,
									BGl_string2007z00zzexpand_casez00, BgL_xz00_3);
							}
					}
				else
					{	/* Expand/case.scm 36 */
						goto BgL_tagzd2360zd2_105;
					}
			}
		}

	}



/* &expand-case */
	obj_t BGl_z62expandzd2casezb0zzexpand_casez00(obj_t BgL_envz00_877,
		obj_t BgL_xz00_878, obj_t BgL_ez00_879)
	{
		{	/* Expand/case.scm 34 */
			return BGl_expandzd2casezd2zzexpand_casez00(BgL_xz00_878, BgL_ez00_879);
		}

	}



/* case-type */
	obj_t BGl_casezd2typezd2zzexpand_casez00(obj_t BgL_xz00_5,
		obj_t BgL_clausesz00_6)
	{
		{	/* Expand/case.scm 77 */
			{
				obj_t BgL_datumz00_171;
				obj_t BgL_datumsz00_180;

				{
					obj_t BgL_clausesz00_126;
					obj_t BgL_typez00_127;

					BgL_clausesz00_126 = BgL_clausesz00_6;
					BgL_typez00_127 = BNIL;
				BgL_zc3z04anonymousza31110ze3z87_128:
					if (NULLP(BgL_clausesz00_126))
						{	/* Expand/case.scm 122 */
							return BgL_typez00_127;
						}
					else
						{
							obj_t BgL_expsz00_130;
							obj_t BgL_datumz00_132;
							obj_t BgL_expsz00_133;

							{	/* Expand/case.scm 124 */
								obj_t BgL_ezd2377zd2_136;

								BgL_ezd2377zd2_136 = CAR(((obj_t) BgL_clausesz00_126));
								if (PAIRP(BgL_ezd2377zd2_136))
									{	/* Expand/case.scm 124 */
										if ((CAR(BgL_ezd2377zd2_136) == CNST_TABLE_REF(11)))
											{	/* Expand/case.scm 124 */
												BgL_expsz00_130 = CDR(BgL_ezd2377zd2_136);
												{	/* Expand/case.scm 126 */
													bool_t BgL_test2040z00_994;

													if (NULLP(CDR(((obj_t) BgL_clausesz00_126))))
														{	/* Expand/case.scm 126 */
															BgL_test2040z00_994 = NULLP(BgL_expsz00_130);
														}
													else
														{	/* Expand/case.scm 126 */
															BgL_test2040z00_994 = ((bool_t) 1);
														}
													if (BgL_test2040z00_994)
														{	/* Expand/case.scm 126 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string2006z00zzexpand_casez00,
																BGl_string2007z00zzexpand_casez00, BgL_xz00_5);
														}
													else
														{	/* Expand/case.scm 126 */
															return BgL_typez00_127;
														}
												}
											}
										else
											{	/* Expand/case.scm 124 */
												obj_t BgL_carzd2388zd2_141;

												BgL_carzd2388zd2_141 = CAR(BgL_ezd2377zd2_136);
												if (NULLP(BgL_carzd2388zd2_141))
													{	/* Expand/case.scm 124 */
													BgL_tagzd2376zd2_135:
														return
															BGl_errorz00zz__errorz00
															(BGl_string2006z00zzexpand_casez00,
															BGl_string2007z00zzexpand_casez00, BgL_xz00_5);
													}
												else
													{	/* Expand/case.scm 124 */
														BgL_datumz00_132 = BgL_carzd2388zd2_141;
														BgL_expsz00_133 = CDR(BgL_ezd2377zd2_136);
														if (NULLP(BgL_expsz00_133))
															{	/* Expand/case.scm 131 */
																return
																	BGl_errorz00zz__errorz00
																	(BGl_string2006z00zzexpand_casez00,
																	BGl_string2007z00zzexpand_casez00,
																	BgL_xz00_5);
															}
														else
															{	/* Expand/case.scm 133 */
																obj_t BgL_dtypez00_152;

																BgL_datumsz00_180 = BgL_datumz00_132;
																{
																	obj_t BgL_datumsz00_184;
																	obj_t BgL_typez00_185;

																	BgL_datumsz00_184 = BgL_datumsz00_180;
																	BgL_typez00_185 = BNIL;
																BgL_zc3z04anonymousza31151ze3z87_186:
																	if (NULLP(BgL_datumsz00_184))
																		{	/* Expand/case.scm 109 */
																			BgL_dtypez00_152 = BgL_typez00_185;
																		}
																	else
																		{	/* Expand/case.scm 109 */
																			if (PAIRP(BgL_datumsz00_184))
																				{	/* Expand/case.scm 115 */
																					obj_t BgL_dtypez00_189;

																					BgL_datumz00_171 =
																						CAR(BgL_datumsz00_184);
																					if (INTEGERP(BgL_datumz00_171))
																						{	/* Expand/case.scm 96 */
																							BgL_dtypez00_189 =
																								CNST_TABLE_REF(0);
																						}
																					else
																						{	/* Expand/case.scm 96 */
																							if (BGL_UINT32P(BgL_datumz00_171))
																								{	/* Expand/case.scm 97 */
																									BgL_dtypez00_189 =
																										CNST_TABLE_REF(2);
																								}
																							else
																								{	/* Expand/case.scm 97 */
																									if (BGL_INT32P
																										(BgL_datumz00_171))
																										{	/* Expand/case.scm 98 */
																											BgL_dtypez00_189 =
																												CNST_TABLE_REF(3);
																										}
																									else
																										{	/* Expand/case.scm 98 */
																											if (CHARP
																												(BgL_datumz00_171))
																												{	/* Expand/case.scm 99 */
																													BgL_dtypez00_189 =
																														CNST_TABLE_REF(4);
																												}
																											else
																												{	/* Expand/case.scm 99 */
																													if (CNSTP
																														(BgL_datumz00_171))
																														{	/* Expand/case.scm 100 */
																															BgL_dtypez00_189 =
																																CNST_TABLE_REF
																																(5);
																														}
																													else
																														{	/* Expand/case.scm 100 */
																															if (SYMBOLP
																																(BgL_datumz00_171))
																																{	/* Expand/case.scm 101 */
																																	BgL_dtypez00_189
																																		=
																																		CNST_TABLE_REF
																																		(6);
																																}
																															else
																																{	/* Expand/case.scm 101 */
																																	if (KEYWORDP
																																		(BgL_datumz00_171))
																																		{	/* Expand/case.scm 102 */
																																			BgL_dtypez00_189
																																				=
																																				CNST_TABLE_REF
																																				(7);
																																		}
																																	else
																																		{	/* Expand/case.scm 102 */
																																			BgL_dtypez00_189
																																				=
																																				CNST_TABLE_REF
																																				(8);
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																					if (BGl_typezd2matchzf3ze70zc6zzexpand_casez00(BgL_dtypez00_189, BgL_typez00_185))
																						{
																							obj_t BgL_typez00_1040;
																							obj_t BgL_datumsz00_1038;

																							BgL_datumsz00_1038 =
																								CDR(BgL_datumsz00_184);
																							BgL_typez00_1040 =
																								BGl_generalze70ze7zzexpand_casez00
																								(BgL_dtypez00_189,
																								BgL_typez00_185);
																							BgL_typez00_185 =
																								BgL_typez00_1040;
																							BgL_datumsz00_184 =
																								BgL_datumsz00_1038;
																							goto
																								BgL_zc3z04anonymousza31151ze3z87_186;
																						}
																					else
																						{	/* Expand/case.scm 116 */
																							BgL_dtypez00_152 =
																								CNST_TABLE_REF(8);
																						}
																				}
																			else
																				{	/* Expand/case.scm 111 */
																					BGl_errorz00zz__errorz00
																						(CNST_TABLE_REF(9),
																						BGl_string2007z00zzexpand_casez00,
																						BgL_xz00_5);
																					BgL_dtypez00_152 = BFALSE;
																				}
																		}
																}
																if (BGl_typezd2matchzf3ze70zc6zzexpand_casez00
																	(BgL_dtypez00_152, BgL_typez00_127))
																	{	/* Expand/case.scm 135 */
																		obj_t BgL_arg1137z00_154;
																		obj_t BgL_arg1138z00_155;

																		BgL_arg1137z00_154 =
																			CDR(((obj_t) BgL_clausesz00_126));
																		BgL_arg1138z00_155 =
																			BGl_generalze70ze7zzexpand_casez00
																			(BgL_dtypez00_152, BgL_typez00_127);
																		{
																			obj_t BgL_typez00_1051;
																			obj_t BgL_clausesz00_1050;

																			BgL_clausesz00_1050 = BgL_arg1137z00_154;
																			BgL_typez00_1051 = BgL_arg1138z00_155;
																			BgL_typez00_127 = BgL_typez00_1051;
																			BgL_clausesz00_126 = BgL_clausesz00_1050;
																			goto BgL_zc3z04anonymousza31110ze3z87_128;
																		}
																	}
																else
																	{	/* Expand/case.scm 134 */
																		return CNST_TABLE_REF(10);
																	}
															}
													}
											}
									}
								else
									{	/* Expand/case.scm 124 */
										goto BgL_tagzd2376zd2_135;
									}
							}
						}
				}
			}
		}

	}



/* type-match?~0 */
	bool_t BGl_typezd2matchzf3ze70zc6zzexpand_casez00(obj_t BgL_type1z00_157,
		obj_t BgL_type2z00_158)
	{
		{	/* Expand/case.scm 85 */
			{	/* Expand/case.scm 80 */
				bool_t BgL__ortest_1052z00_160;

				BgL__ortest_1052z00_160 = NULLP(BgL_type1z00_157);
				if (BgL__ortest_1052z00_160)
					{	/* Expand/case.scm 80 */
						return BgL__ortest_1052z00_160;
					}
				else
					{	/* Expand/case.scm 81 */
						bool_t BgL__ortest_1053z00_161;

						BgL__ortest_1053z00_161 = NULLP(BgL_type2z00_158);
						if (BgL__ortest_1053z00_161)
							{	/* Expand/case.scm 81 */
								return BgL__ortest_1053z00_161;
							}
						else
							{	/* Expand/case.scm 81 */
								if ((BgL_type1z00_157 == CNST_TABLE_REF(8)))
									{	/* Expand/case.scm 82 */
										return ((bool_t) 0);
									}
								else
									{	/* Expand/case.scm 83 */
										bool_t BgL__ortest_1055z00_163;

										BgL__ortest_1055z00_163 =
											(BgL_type1z00_157 == BgL_type2z00_158);
										if (BgL__ortest_1055z00_163)
											{	/* Expand/case.scm 83 */
												return BgL__ortest_1055z00_163;
											}
										else
											{	/* Expand/case.scm 84 */
												bool_t BgL__ortest_1056z00_164;

												if ((BgL_type1z00_157 == CNST_TABLE_REF(5)))
													{	/* Expand/case.scm 84 */
														BgL__ortest_1056z00_164 =
															(BgL_type2z00_158 == CNST_TABLE_REF(4));
													}
												else
													{	/* Expand/case.scm 84 */
														BgL__ortest_1056z00_164 = ((bool_t) 0);
													}
												if (BgL__ortest_1056z00_164)
													{	/* Expand/case.scm 84 */
														return BgL__ortest_1056z00_164;
													}
												else
													{	/* Expand/case.scm 84 */
														if ((BgL_type1z00_157 == CNST_TABLE_REF(4)))
															{	/* Expand/case.scm 85 */
																return (BgL_type2z00_158 == CNST_TABLE_REF(5));
															}
														else
															{	/* Expand/case.scm 85 */
																return ((bool_t) 0);
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* general~0 */
	obj_t BGl_generalze70ze7zzexpand_casez00(obj_t BgL_type1z00_167,
		obj_t BgL_type2z00_168)
	{
		{	/* Expand/case.scm 92 */
			if ((BgL_type1z00_167 == BgL_type2z00_168))
				{	/* Expand/case.scm 89 */
					return BgL_type1z00_167;
				}
			else
				{	/* Expand/case.scm 89 */
					if ((BgL_type1z00_167 == CNST_TABLE_REF(5)))
						{	/* Expand/case.scm 90 */
							return BgL_type1z00_167;
						}
					else
						{	/* Expand/case.scm 90 */
							if (NULLP(BgL_type2z00_168))
								{	/* Expand/case.scm 91 */
									return BgL_type1z00_167;
								}
							else
								{	/* Expand/case.scm 91 */
									return BgL_type2z00_168;
								}
						}
				}
		}

	}



/* do-typed-case */
	obj_t BGl_dozd2typedzd2casez00zzexpand_casez00(obj_t BgL_typez00_7,
		obj_t BgL_valuez00_8, obj_t BgL_clausesz00_9, obj_t BgL_ez00_10)
	{
		{	/* Expand/case.scm 144 */
			{	/* Expand/case.scm 145 */
				obj_t BgL_elsezd2bodyzd2_199;

				{
					obj_t BgL_clausesz00_260;

					BgL_clausesz00_260 = BgL_clausesz00_9;
				BgL_zc3z04anonymousza31220ze3z87_261:
					if (NULLP(BgL_clausesz00_260))
						{	/* Expand/case.scm 147 */
							obj_t BgL_list1222z00_263;

							BgL_list1222z00_263 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
							BgL_elsezd2bodyzd2_199 = BgL_list1222z00_263;
						}
					else
						{
							obj_t BgL_bodyz00_265;

							{	/* Expand/case.scm 148 */
								obj_t BgL_ezd2401zd2_268;

								BgL_ezd2401zd2_268 = CAR(((obj_t) BgL_clausesz00_260));
								if (NULLP(BgL_ezd2401zd2_268))
									{	/* Expand/case.scm 148 */
										BgL_elsezd2bodyzd2_199 = BUNSPEC;
									}
								else
									{	/* Expand/case.scm 148 */
										if (PAIRP(BgL_ezd2401zd2_268))
											{	/* Expand/case.scm 148 */
												if ((CAR(BgL_ezd2401zd2_268) == CNST_TABLE_REF(11)))
													{	/* Expand/case.scm 148 */
														BgL_bodyz00_265 = CDR(BgL_ezd2401zd2_268);
														if (NULLP(BgL_bodyz00_265))
															{	/* Expand/case.scm 152 */
																BgL_elsezd2bodyzd2_199 = BNIL;
															}
														else
															{	/* Expand/case.scm 152 */
																obj_t BgL_head1065z00_277;

																BgL_head1065z00_277 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1063z00_279;
																	obj_t BgL_tail1066z00_280;

																	BgL_l1063z00_279 = BgL_bodyz00_265;
																	BgL_tail1066z00_280 = BgL_head1065z00_277;
																BgL_zc3z04anonymousza31230ze3z87_281:
																	if (NULLP(BgL_l1063z00_279))
																		{	/* Expand/case.scm 152 */
																			BgL_elsezd2bodyzd2_199 =
																				CDR(BgL_head1065z00_277);
																		}
																	else
																		{	/* Expand/case.scm 152 */
																			obj_t BgL_newtail1067z00_283;

																			{	/* Expand/case.scm 152 */
																				obj_t BgL_arg1233z00_285;

																				{	/* Expand/case.scm 152 */
																					obj_t BgL_xz00_286;

																					BgL_xz00_286 =
																						CAR(((obj_t) BgL_l1063z00_279));
																					BgL_arg1233z00_285 =
																						BGL_PROCEDURE_CALL2(BgL_ez00_10,
																						BgL_xz00_286, BgL_ez00_10);
																				}
																				BgL_newtail1067z00_283 =
																					MAKE_YOUNG_PAIR(BgL_arg1233z00_285,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1066z00_280,
																				BgL_newtail1067z00_283);
																			{	/* Expand/case.scm 152 */
																				obj_t BgL_arg1232z00_284;

																				BgL_arg1232z00_284 =
																					CDR(((obj_t) BgL_l1063z00_279));
																				{
																					obj_t BgL_tail1066z00_1112;
																					obj_t BgL_l1063z00_1111;

																					BgL_l1063z00_1111 =
																						BgL_arg1232z00_284;
																					BgL_tail1066z00_1112 =
																						BgL_newtail1067z00_283;
																					BgL_tail1066z00_280 =
																						BgL_tail1066z00_1112;
																					BgL_l1063z00_279 = BgL_l1063z00_1111;
																					goto
																						BgL_zc3z04anonymousza31230ze3z87_281;
																				}
																			}
																		}
																}
															}
													}
												else
													{	/* Expand/case.scm 148 */
													BgL_tagzd2400zd2_267:
														{	/* Expand/case.scm 154 */
															obj_t BgL_arg1234z00_288;

															BgL_arg1234z00_288 =
																CDR(((obj_t) BgL_clausesz00_260));
															{
																obj_t BgL_clausesz00_1116;

																BgL_clausesz00_1116 = BgL_arg1234z00_288;
																BgL_clausesz00_260 = BgL_clausesz00_1116;
																goto BgL_zc3z04anonymousza31220ze3z87_261;
															}
														}
													}
											}
										else
											{	/* Expand/case.scm 148 */
												goto BgL_tagzd2400zd2_267;
											}
									}
							}
						}
				}
				{	/* Expand/case.scm 145 */
					obj_t BgL_elsezd2namezd2_200;

					BgL_elsezd2namezd2_200 =
						BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
						(BGl_gensymz00zz__r4_symbols_6_4z00
						(BGl_string2008z00zzexpand_casez00));
					{	/* Expand/case.scm 155 */
						obj_t BgL_auxz00_201;

						BgL_auxz00_201 =
							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
							(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(12)));
						{	/* Expand/case.scm 156 */

							{	/* Expand/case.scm 157 */
								obj_t BgL_casez00_202;

								{	/* Expand/case.scm 159 */
									obj_t BgL_arg1164z00_203;

									BgL_arg1164z00_203 =
										MAKE_YOUNG_PAIR(BgL_auxz00_201,
										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
										(BGl_loopze72ze7zzexpand_casez00(BgL_elsezd2namezd2_200,
												BgL_ez00_10, BgL_clausesz00_9), BNIL));
									BgL_casez00_202 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1164z00_203);
								}
								return
									BGl_typezd2testzd2zzexpand_casez00(BgL_auxz00_201,
									BgL_typez00_7, BgL_valuez00_8, BgL_casez00_202,
									BgL_elsezd2bodyzd2_199, BgL_elsezd2namezd2_200, BgL_ez00_10);
							}
						}
					}
				}
			}
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zzexpand_casez00(obj_t BgL_elsezd2namezd2_882,
		obj_t BgL_ez00_881, obj_t BgL_clausesz00_207)
	{
		{	/* Expand/case.scm 158 */
			if (NULLP(BgL_clausesz00_207))
				{	/* Expand/case.scm 161 */
					obj_t BgL_arg1182z00_210;

					{	/* Expand/case.scm 161 */
						obj_t BgL_arg1183z00_211;

						BgL_arg1183z00_211 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
						BgL_arg1182z00_210 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg1183z00_211);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1182z00_210, BNIL);
				}
			else
				{
					obj_t BgL_bodyz00_213;
					obj_t BgL_datumsz00_215;
					obj_t BgL_bodyz00_216;

					{	/* Expand/case.scm 162 */
						obj_t BgL_ezd2412zd2_219;

						BgL_ezd2412zd2_219 = CAR(((obj_t) BgL_clausesz00_207));
						if (NULLP(BgL_ezd2412zd2_219))
							{	/* Expand/case.scm 162 */
								{	/* Expand/case.scm 164 */
									obj_t BgL_arg1194z00_228;

									{	/* Expand/case.scm 164 */
										obj_t BgL_arg1196z00_229;

										BgL_arg1196z00_229 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
										BgL_arg1194z00_228 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg1196z00_229);
									}
									return MAKE_YOUNG_PAIR(BgL_arg1194z00_228, BNIL);
								}
							}
						else
							{	/* Expand/case.scm 162 */
								if (PAIRP(BgL_ezd2412zd2_219))
									{	/* Expand/case.scm 162 */
										if ((CAR(BgL_ezd2412zd2_219) == CNST_TABLE_REF(11)))
											{	/* Expand/case.scm 162 */
												BgL_bodyz00_213 = CDR(BgL_ezd2412zd2_219);
												{	/* Expand/case.scm 166 */
													obj_t BgL_arg1197z00_230;

													{	/* Expand/case.scm 166 */
														obj_t BgL_arg1198z00_231;

														{	/* Expand/case.scm 166 */
															obj_t BgL_arg1199z00_232;

															BgL_arg1199z00_232 =
																MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_882, BNIL);
															BgL_arg1198z00_231 =
																MAKE_YOUNG_PAIR(BgL_arg1199z00_232, BNIL);
														}
														BgL_arg1197z00_230 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
															BgL_arg1198z00_231);
													}
													return MAKE_YOUNG_PAIR(BgL_arg1197z00_230, BNIL);
												}
											}
										else
											{	/* Expand/case.scm 162 */
												BgL_datumsz00_215 = CAR(BgL_ezd2412zd2_219);
												BgL_bodyz00_216 = CDR(BgL_ezd2412zd2_219);
												if (NULLP(BgL_bodyz00_216))
													{	/* Expand/case.scm 171 */
														obj_t BgL_arg1201z00_234;

														BgL_arg1201z00_234 =
															CAR(((obj_t) BgL_clausesz00_207));
														return
															BGl_errorz00zz__errorz00
															(BGl_string2006z00zzexpand_casez00,
															BGl_string2009z00zzexpand_casez00,
															BgL_arg1201z00_234);
													}
												else
													{	/* Expand/case.scm 172 */
														obj_t BgL_nbodyz00_235;

														{	/* Expand/case.scm 172 */
															obj_t BgL_head1070z00_245;

															BgL_head1070z00_245 = MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_l1068z00_247;
																obj_t BgL_tail1071z00_248;

																BgL_l1068z00_247 = BgL_bodyz00_216;
																BgL_tail1071z00_248 = BgL_head1070z00_245;
															BgL_zc3z04anonymousza31211ze3z87_249:
																if (NULLP(BgL_l1068z00_247))
																	{	/* Expand/case.scm 172 */
																		BgL_nbodyz00_235 = CDR(BgL_head1070z00_245);
																	}
																else
																	{	/* Expand/case.scm 172 */
																		obj_t BgL_newtail1072z00_251;

																		{	/* Expand/case.scm 172 */
																			obj_t BgL_arg1216z00_253;

																			{	/* Expand/case.scm 172 */
																				obj_t BgL_xz00_254;

																				BgL_xz00_254 =
																					CAR(((obj_t) BgL_l1068z00_247));
																				BgL_arg1216z00_253 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_881,
																					BgL_xz00_254, BgL_ez00_881);
																			}
																			BgL_newtail1072z00_251 =
																				MAKE_YOUNG_PAIR(BgL_arg1216z00_253,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1071z00_248,
																			BgL_newtail1072z00_251);
																		{	/* Expand/case.scm 172 */
																			obj_t BgL_arg1215z00_252;

																			BgL_arg1215z00_252 =
																				CDR(((obj_t) BgL_l1068z00_247));
																			{
																				obj_t BgL_tail1071z00_1175;
																				obj_t BgL_l1068z00_1174;

																				BgL_l1068z00_1174 = BgL_arg1215z00_252;
																				BgL_tail1071z00_1175 =
																					BgL_newtail1072z00_251;
																				BgL_tail1071z00_248 =
																					BgL_tail1071z00_1175;
																				BgL_l1068z00_247 = BgL_l1068z00_1174;
																				goto
																					BgL_zc3z04anonymousza31211ze3z87_249;
																			}
																		}
																	}
															}
														}
														{	/* Expand/case.scm 172 */
															obj_t BgL_ebodyz00_236;

															BgL_ebodyz00_236 =
																BGl_epairifyzd2reczd2zztools_miscz00
																(BgL_nbodyz00_235, BgL_bodyz00_216);
															{	/* Expand/case.scm 174 */
																obj_t BgL_nclausez00_237;

																BgL_nclausez00_237 =
																	MAKE_YOUNG_PAIR(BgL_datumsz00_215,
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_ebodyz00_236, BNIL));
																{	/* Expand/case.scm 175 */

																	{	/* Expand/case.scm 176 */
																		obj_t BgL_arg1202z00_238;
																		obj_t BgL_arg1203z00_239;

																		{	/* Expand/case.scm 176 */
																			obj_t BgL_arg1206z00_240;

																			BgL_arg1206z00_240 =
																				CAR(((obj_t) BgL_clausesz00_207));
																			BgL_arg1202z00_238 =
																				BGl_epairifyz00zztools_miscz00
																				(BgL_nclausez00_237,
																				BgL_arg1206z00_240);
																		}
																		{	/* Expand/case.scm 177 */
																			obj_t BgL_arg1208z00_241;

																			BgL_arg1208z00_241 =
																				CDR(((obj_t) BgL_clausesz00_207));
																			BgL_arg1203z00_239 =
																				BGl_loopze72ze7zzexpand_casez00
																				(BgL_elsezd2namezd2_882, BgL_ez00_881,
																				BgL_arg1208z00_241);
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_arg1202z00_238,
																			BgL_arg1203z00_239);
																	}
																}
															}
														}
													}
											}
									}
								else
									{	/* Expand/case.scm 162 */
										return
											BGl_errorz00zz__errorz00
											(BGl_string2006z00zzexpand_casez00,
											BGl_string2007z00zzexpand_casez00, BgL_clausesz00_207);
									}
							}
					}
				}
		}

	}



/* do-cnst-case */
	obj_t BGl_dozd2cnstzd2casez00zzexpand_casez00(obj_t BgL_valuez00_11,
		obj_t BgL_clausesz00_12, obj_t BgL_ez00_13)
	{
		{	/* Expand/case.scm 191 */
			{	/* Expand/case.scm 192 */
				obj_t BgL_auxz00_290;

				BgL_auxz00_290 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(12)));
				{	/* Expand/case.scm 192 */
					obj_t BgL_valuez00_291;

					{	/* Expand/case.scm 193 */
						obj_t BgL_arg1305z00_319;

						{	/* Expand/case.scm 193 */
							obj_t BgL_arg1306z00_320;
							obj_t BgL_arg1307z00_321;

							{	/* Expand/case.scm 193 */
								obj_t BgL_arg1308z00_322;

								{	/* Expand/case.scm 193 */
									obj_t BgL_arg1310z00_323;

									BgL_arg1310z00_323 = MAKE_YOUNG_PAIR(BgL_valuez00_11, BNIL);
									BgL_arg1308z00_322 =
										MAKE_YOUNG_PAIR(BgL_auxz00_290, BgL_arg1310z00_323);
								}
								BgL_arg1306z00_320 = MAKE_YOUNG_PAIR(BgL_arg1308z00_322, BNIL);
							}
							{	/* Expand/case.scm 194 */
								obj_t BgL_arg1311z00_324;

								{	/* Expand/case.scm 194 */
									obj_t BgL_arg1312z00_325;

									{	/* Expand/case.scm 194 */
										obj_t BgL_arg1314z00_326;
										obj_t BgL_arg1315z00_327;

										{	/* Expand/case.scm 194 */
											obj_t BgL_arg1316z00_328;

											BgL_arg1316z00_328 =
												MAKE_YOUNG_PAIR(BgL_auxz00_290, BNIL);
											BgL_arg1314z00_326 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(13), BgL_arg1316z00_328);
										}
										{	/* Expand/case.scm 195 */
											obj_t BgL_arg1317z00_329;
											obj_t BgL_arg1318z00_330;

											{	/* Expand/case.scm 195 */
												obj_t BgL_arg1319z00_331;

												BgL_arg1319z00_331 =
													MAKE_YOUNG_PAIR(BgL_auxz00_290, BNIL);
												BgL_arg1317z00_329 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
													BgL_arg1319z00_331);
											}
											BgL_arg1318z00_330 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
											BgL_arg1315z00_327 =
												MAKE_YOUNG_PAIR(BgL_arg1317z00_329, BgL_arg1318z00_330);
										}
										BgL_arg1312z00_325 =
											MAKE_YOUNG_PAIR(BgL_arg1314z00_326, BgL_arg1315z00_327);
									}
									BgL_arg1311z00_324 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(15), BgL_arg1312z00_325);
								}
								BgL_arg1307z00_321 = MAKE_YOUNG_PAIR(BgL_arg1311z00_324, BNIL);
							}
							BgL_arg1305z00_319 =
								MAKE_YOUNG_PAIR(BgL_arg1306z00_320, BgL_arg1307z00_321);
						}
						BgL_valuez00_291 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1305z00_319);
					}
					{	/* Expand/case.scm 193 */

						{
							obj_t BgL_cz00_293;

							BgL_cz00_293 = BgL_clausesz00_12;
						BgL_zc3z04anonymousza31235ze3z87_294:
							if (NULLP(BgL_cz00_293))
								{	/* Expand/case.scm 200 */
									return
										BGl_dozd2typedzd2casez00zzexpand_casez00(CNST_TABLE_REF(1),
										BgL_valuez00_291, BgL_clausesz00_12, BgL_ez00_13);
								}
							else
								{	/* Expand/case.scm 202 */
									obj_t BgL_clausez00_296;

									BgL_clausez00_296 = CAR(((obj_t) BgL_cz00_293));
									if ((CAR(((obj_t) BgL_clausez00_296)) == CNST_TABLE_REF(11)))
										{	/* Expand/case.scm 203 */
											BFALSE;
										}
									else
										{	/* Expand/case.scm 204 */
											obj_t BgL_arg1242z00_299;

											{	/* Expand/case.scm 204 */
												obj_t BgL_l1073z00_300;

												BgL_l1073z00_300 = CAR(((obj_t) BgL_clausez00_296));
												if (NULLP(BgL_l1073z00_300))
													{	/* Expand/case.scm 204 */
														BgL_arg1242z00_299 = BNIL;
													}
												else
													{	/* Expand/case.scm 204 */
														obj_t BgL_head1075z00_302;

														{	/* Expand/case.scm 204 */
															long BgL_arg1268z00_314;

															{	/* Expand/case.scm 204 */
																obj_t BgL_arg1272z00_315;

																BgL_arg1272z00_315 =
																	CAR(((obj_t) BgL_l1073z00_300));
																BgL_arg1268z00_314 = CCNST(BgL_arg1272z00_315);
															}
															BgL_head1075z00_302 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1268z00_314), BNIL);
														}
														{	/* Expand/case.scm 204 */
															obj_t BgL_g1078z00_303;

															BgL_g1078z00_303 =
																CDR(((obj_t) BgL_l1073z00_300));
															{
																obj_t BgL_l1073z00_305;
																obj_t BgL_tail1076z00_306;

																BgL_l1073z00_305 = BgL_g1078z00_303;
																BgL_tail1076z00_306 = BgL_head1075z00_302;
															BgL_zc3z04anonymousza31244ze3z87_307:
																if (NULLP(BgL_l1073z00_305))
																	{	/* Expand/case.scm 204 */
																		BgL_arg1242z00_299 = BgL_head1075z00_302;
																	}
																else
																	{	/* Expand/case.scm 204 */
																		obj_t BgL_newtail1077z00_309;

																		{	/* Expand/case.scm 204 */
																			long BgL_arg1249z00_311;

																			{	/* Expand/case.scm 204 */
																				obj_t BgL_arg1252z00_312;

																				BgL_arg1252z00_312 =
																					CAR(((obj_t) BgL_l1073z00_305));
																				BgL_arg1249z00_311 =
																					CCNST(BgL_arg1252z00_312);
																			}
																			BgL_newtail1077z00_309 =
																				MAKE_YOUNG_PAIR(BINT
																				(BgL_arg1249z00_311), BNIL);
																		}
																		SET_CDR(BgL_tail1076z00_306,
																			BgL_newtail1077z00_309);
																		{	/* Expand/case.scm 204 */
																			obj_t BgL_arg1248z00_310;

																			BgL_arg1248z00_310 =
																				CDR(((obj_t) BgL_l1073z00_305));
																			{
																				obj_t BgL_tail1076z00_1244;
																				obj_t BgL_l1073z00_1243;

																				BgL_l1073z00_1243 = BgL_arg1248z00_310;
																				BgL_tail1076z00_1244 =
																					BgL_newtail1077z00_309;
																				BgL_tail1076z00_306 =
																					BgL_tail1076z00_1244;
																				BgL_l1073z00_305 = BgL_l1073z00_1243;
																				goto
																					BgL_zc3z04anonymousza31244ze3z87_307;
																			}
																		}
																	}
															}
														}
													}
											}
											{	/* Expand/case.scm 204 */
												obj_t BgL_tmpz00_1245;

												BgL_tmpz00_1245 = ((obj_t) BgL_clausez00_296);
												SET_CAR(BgL_tmpz00_1245, BgL_arg1242z00_299);
											}
										}
									{	/* Expand/case.scm 205 */
										obj_t BgL_arg1304z00_317;

										BgL_arg1304z00_317 = CDR(((obj_t) BgL_cz00_293));
										{
											obj_t BgL_cz00_1250;

											BgL_cz00_1250 = BgL_arg1304z00_317;
											BgL_cz00_293 = BgL_cz00_1250;
											goto BgL_zc3z04anonymousza31235ze3z87_294;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* do-optim-symbol/keyword-case */
	obj_t BGl_dozd2optimzd2symbolzf2keywordzd2casez20zzexpand_casez00(obj_t
		BgL_typez00_18, obj_t BgL_valuez00_19, obj_t BgL_clausesz00_20,
		obj_t BgL_ez00_21)
	{
		{	/* Expand/case.scm 219 */
			{
				obj_t BgL_clausesz00_336;

				{	/* Expand/case.scm 234 */
					obj_t BgL_numzd2elszd2_334;

					BgL_clausesz00_336 = BgL_clausesz00_20;
					{
						obj_t BgL_clausesz00_339;
						long BgL_resz00_340;

						BgL_clausesz00_339 = BgL_clausesz00_336;
						BgL_resz00_340 = 0L;
					BgL_zc3z04anonymousza31323ze3z87_341:
						if (NULLP(BgL_clausesz00_339))
							{	/* Expand/case.scm 223 */
								BgL_numzd2elszd2_334 = BINT(BgL_resz00_340);
							}
						else
							{	/* Expand/case.scm 225 */
								obj_t BgL_ezd2437zd2_352;

								BgL_ezd2437zd2_352 = CAR(((obj_t) BgL_clausesz00_339));
								if (NULLP(BgL_ezd2437zd2_352))
									{	/* Expand/case.scm 225 */
										BgL_numzd2elszd2_334 = BINT(BgL_resz00_340);
									}
								else
									{	/* Expand/case.scm 225 */
										if (PAIRP(BgL_ezd2437zd2_352))
											{	/* Expand/case.scm 225 */
												if ((CAR(BgL_ezd2437zd2_352) == CNST_TABLE_REF(11)))
													{	/* Expand/case.scm 225 */
														BgL_numzd2elszd2_334 = BINT(BgL_resz00_340);
													}
												else
													{	/* Expand/case.scm 225 */
														obj_t BgL_carzd2450zd2_358;

														BgL_carzd2450zd2_358 = CAR(BgL_ezd2437zd2_352);
														if (PAIRP(BgL_carzd2450zd2_358))
															{	/* Expand/case.scm 225 */
																bool_t BgL_test2086z00_1269;

																{	/* Expand/case.scm 225 */
																	obj_t BgL_tmpz00_1270;

																	BgL_tmpz00_1270 = CDR(BgL_carzd2450zd2_358);
																	BgL_test2086z00_1269 = PAIRP(BgL_tmpz00_1270);
																}
																if (BgL_test2086z00_1269)
																	{
																		long BgL_resz00_1276;
																		obj_t BgL_clausesz00_1273;

																		BgL_clausesz00_1273 =
																			CDR(((obj_t) BgL_clausesz00_339));
																		BgL_resz00_1276 =
																			(bgl_list_length(BgL_carzd2450zd2_358) +
																			BgL_resz00_340);
																		BgL_resz00_340 = BgL_resz00_1276;
																		BgL_clausesz00_339 = BgL_clausesz00_1273;
																		goto BgL_zc3z04anonymousza31323ze3z87_341;
																	}
																else
																	{	/* Expand/case.scm 225 */
																		if (NULLP(CDR(
																					((obj_t) BgL_carzd2450zd2_358))))
																			{
																				long BgL_resz00_1286;
																				obj_t BgL_clausesz00_1283;

																				BgL_clausesz00_1283 =
																					CDR(((obj_t) BgL_clausesz00_339));
																				BgL_resz00_1286 = (1L + BgL_resz00_340);
																				BgL_resz00_340 = BgL_resz00_1286;
																				BgL_clausesz00_339 =
																					BgL_clausesz00_1283;
																				goto
																					BgL_zc3z04anonymousza31323ze3z87_341;
																			}
																		else
																			{	/* Expand/case.scm 225 */
																				BgL_numzd2elszd2_334 = BFALSE;
																			}
																	}
															}
														else
															{	/* Expand/case.scm 225 */
																BgL_numzd2elszd2_334 = BFALSE;
															}
													}
											}
										else
											{	/* Expand/case.scm 225 */
												BgL_numzd2elszd2_334 = BFALSE;
											}
									}
							}
					}
					if (((long) CINT(BgL_numzd2elszd2_334) < 10L))
						{	/* Expand/case.scm 235 */
							return
								BGl_dozd2genericzd2symbolzf2keywordzd2casez20zzexpand_casez00
								(BgL_valuez00_19, BgL_clausesz00_20, BgL_ez00_21);
						}
					else
						{	/* Expand/case.scm 235 */
							return
								BGl_dozd2genericzd2symbolzf2keywordzd2casez20zzexpand_casez00
								(BgL_valuez00_19, BgL_clausesz00_20, BgL_ez00_21);
						}
				}
			}
		}

	}



/* do-generic-symbol/keyword-case */
	obj_t BGl_dozd2genericzd2symbolzf2keywordzd2casez20zzexpand_casez00(obj_t
		BgL_valuez00_22, obj_t BgL_clausesz00_23, obj_t BgL_ez00_24)
	{
		{	/* Expand/case.scm 244 */
			{	/* Expand/case.scm 245 */
				obj_t BgL_arg1375z00_385;

				{	/* Expand/case.scm 245 */
					obj_t BgL_arg1376z00_386;
					obj_t BgL_arg1377z00_387;

					{	/* Expand/case.scm 245 */
						obj_t BgL_arg1378z00_388;

						{	/* Expand/case.scm 245 */
							obj_t BgL_arg1379z00_389;

							{	/* Expand/case.scm 245 */
								obj_t BgL_arg1380z00_390;

								BgL_arg1380z00_390 =
									BGL_PROCEDURE_CALL2(BgL_ez00_24, BgL_valuez00_22,
									BgL_ez00_24);
								BgL_arg1379z00_389 = MAKE_YOUNG_PAIR(BgL_arg1380z00_390, BNIL);
							}
							BgL_arg1378z00_388 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg1379z00_389);
						}
						BgL_arg1376z00_386 = MAKE_YOUNG_PAIR(BgL_arg1378z00_388, BNIL);
					}
					BgL_arg1377z00_387 =
						MAKE_YOUNG_PAIR(BGl_loopze71ze7zzexpand_casez00(BgL_ez00_24,
							BgL_clausesz00_23), BNIL);
					BgL_arg1375z00_385 =
						MAKE_YOUNG_PAIR(BgL_arg1376z00_386, BgL_arg1377z00_387);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1375z00_385);
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zzexpand_casez00(obj_t BgL_ez00_880,
		obj_t BgL_clausesz00_393)
	{
		{	/* Expand/case.scm 246 */
			if (NULLP(BgL_clausesz00_393))
				{	/* Expand/case.scm 247 */
					return BUNSPEC;
				}
			else
				{
					obj_t BgL_bodyz00_397;
					obj_t BgL_datumsz00_399;
					obj_t BgL_bodyz00_400;
					obj_t BgL_datumz00_402;
					obj_t BgL_bodyz00_403;

					{	/* Expand/case.scm 249 */
						obj_t BgL_ezd2493zd2_405;

						BgL_ezd2493zd2_405 = CAR(((obj_t) BgL_clausesz00_393));
						if (NULLP(BgL_ezd2493zd2_405))
							{	/* Expand/case.scm 249 */
								return BUNSPEC;
							}
						else
							{	/* Expand/case.scm 249 */
								if (PAIRP(BgL_ezd2493zd2_405))
									{	/* Expand/case.scm 249 */
										if ((CAR(BgL_ezd2493zd2_405) == CNST_TABLE_REF(11)))
											{	/* Expand/case.scm 249 */
												BgL_bodyz00_397 = CDR(BgL_ezd2493zd2_405);
												if (NULLP(BgL_bodyz00_397))
													{	/* Expand/case.scm 253 */
														return BFALSE;
													}
												else
													{	/* Expand/case.scm 255 */
														obj_t BgL_arg1535z00_432;

														BgL_arg1535z00_432 =
															BGl_expandzd2prognzd2zz__prognz00
															(BgL_bodyz00_397);
														return BGL_PROCEDURE_CALL2(BgL_ez00_880,
															BgL_arg1535z00_432, BgL_ez00_880);
													}
											}
										else
											{	/* Expand/case.scm 249 */
												obj_t BgL_carzd2509zd2_411;

												BgL_carzd2509zd2_411 = CAR(BgL_ezd2493zd2_405);
												if (PAIRP(BgL_carzd2509zd2_411))
													{	/* Expand/case.scm 249 */
														bool_t BgL_test2095z00_1331;

														{	/* Expand/case.scm 249 */
															obj_t BgL_tmpz00_1332;

															BgL_tmpz00_1332 = CDR(BgL_carzd2509zd2_411);
															BgL_test2095z00_1331 = PAIRP(BgL_tmpz00_1332);
														}
														if (BgL_test2095z00_1331)
															{	/* Expand/case.scm 249 */
																BgL_datumsz00_399 = BgL_carzd2509zd2_411;
																BgL_bodyz00_400 = CDR(BgL_ezd2493zd2_405);
																if (NULLP(BgL_bodyz00_400))
																	{	/* Expand/case.scm 259 */
																		obj_t BgL_arg1540z00_434;

																		BgL_arg1540z00_434 =
																			CAR(((obj_t) BgL_clausesz00_393));
																		return
																			BGl_errorz00zz__errorz00
																			(BGl_string2006z00zzexpand_casez00,
																			BGl_string2009z00zzexpand_casez00,
																			BgL_arg1540z00_434);
																	}
																else
																	{	/* Expand/case.scm 258 */
																		if (
																			(bgl_list_length(BgL_datumsz00_399) <
																				10L))
																			{	/* Expand/case.scm 261 */
																				obj_t BgL_arg1552z00_437;

																				{	/* Expand/case.scm 261 */
																					obj_t BgL_arg1553z00_438;
																					obj_t BgL_arg1559z00_439;

																					{	/* Expand/case.scm 261 */
																						obj_t BgL_arg1561z00_440;

																						{	/* Expand/case.scm 261 */
																							obj_t BgL_arg1564z00_441;

																							{	/* Expand/case.scm 261 */
																								obj_t BgL_arg1565z00_442;

																								{	/* Expand/case.scm 261 */
																									obj_t BgL_head1081z00_445;

																									BgL_head1081z00_445 =
																										MAKE_YOUNG_PAIR(BNIL, BNIL);
																									{
																										obj_t BgL_l1079z00_447;
																										obj_t BgL_tail1082z00_448;

																										BgL_l1079z00_447 =
																											BgL_datumsz00_399;
																										BgL_tail1082z00_448 =
																											BgL_head1081z00_445;
																									BgL_zc3z04anonymousza31567ze3z87_449:
																										if (NULLP
																											(BgL_l1079z00_447))
																											{	/* Expand/case.scm 261 */
																												BgL_arg1565z00_442 =
																													CDR
																													(BgL_head1081z00_445);
																											}
																										else
																											{	/* Expand/case.scm 261 */
																												obj_t
																													BgL_newtail1083z00_451;
																												{	/* Expand/case.scm 261 */
																													obj_t
																														BgL_arg1573z00_453;
																													{	/* Expand/case.scm 261 */
																														obj_t BgL_dz00_454;

																														BgL_dz00_454 =
																															CAR(
																															((obj_t)
																																BgL_l1079z00_447));
																														{	/* Expand/case.scm 261 */
																															obj_t
																																BgL_arg1575z00_455;
																															{	/* Expand/case.scm 261 */
																																obj_t
																																	BgL_arg1576z00_456;
																																{	/* Expand/case.scm 261 */
																																	obj_t
																																		BgL_arg1584z00_457;
																																	{	/* Expand/case.scm 261 */
																																		obj_t
																																			BgL_arg1585z00_458;
																																		BgL_arg1585z00_458
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_dz00_454,
																																			BNIL);
																																		BgL_arg1584z00_457
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(18),
																																			BgL_arg1585z00_458);
																																	}
																																	BgL_arg1576z00_456
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1584z00_457,
																																		BNIL);
																																}
																																BgL_arg1575z00_455
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(17),
																																	BgL_arg1576z00_456);
																															}
																															BgL_arg1573z00_453
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(20),
																																BgL_arg1575z00_455);
																														}
																													}
																													BgL_newtail1083z00_451
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg1573z00_453,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1082z00_448,
																													BgL_newtail1083z00_451);
																												{	/* Expand/case.scm 261 */
																													obj_t
																														BgL_arg1571z00_452;
																													BgL_arg1571z00_452 =
																														CDR(((obj_t)
																															BgL_l1079z00_447));
																													{
																														obj_t
																															BgL_tail1082z00_1362;
																														obj_t
																															BgL_l1079z00_1361;
																														BgL_l1079z00_1361 =
																															BgL_arg1571z00_452;
																														BgL_tail1082z00_1362
																															=
																															BgL_newtail1083z00_451;
																														BgL_tail1082z00_448
																															=
																															BgL_tail1082z00_1362;
																														BgL_l1079z00_447 =
																															BgL_l1079z00_1361;
																														goto
																															BgL_zc3z04anonymousza31567ze3z87_449;
																													}
																												}
																											}
																									}
																								}
																								BgL_arg1564z00_441 =
																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																									(BgL_arg1565z00_442, BNIL);
																							}
																							BgL_arg1561z00_440 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(21), BgL_arg1564z00_441);
																						}
																						BgL_arg1553z00_438 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_880,
																							BgL_arg1561z00_440, BgL_ez00_880);
																					}
																					{	/* Expand/case.scm 262 */
																						obj_t BgL_arg1589z00_460;
																						obj_t BgL_arg1591z00_461;

																						{	/* Expand/case.scm 262 */
																							obj_t BgL_arg1593z00_462;

																							BgL_arg1593z00_462 =
																								BGl_expandzd2prognzd2zz__prognz00
																								(BgL_bodyz00_400);
																							BgL_arg1589z00_460 =
																								BGL_PROCEDURE_CALL2
																								(BgL_ez00_880,
																								BgL_arg1593z00_462,
																								BgL_ez00_880);
																						}
																						{	/* Expand/case.scm 263 */
																							obj_t BgL_arg1594z00_463;

																							{	/* Expand/case.scm 263 */
																								obj_t BgL_arg1595z00_464;

																								BgL_arg1595z00_464 =
																									CDR(
																									((obj_t) BgL_clausesz00_393));
																								BgL_arg1594z00_463 =
																									BGl_loopze71ze7zzexpand_casez00
																									(BgL_ez00_880,
																									BgL_arg1595z00_464);
																							}
																							BgL_arg1591z00_461 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1594z00_463, BNIL);
																						}
																						BgL_arg1559z00_439 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1589z00_460,
																							BgL_arg1591z00_461);
																					}
																					BgL_arg1552z00_437 =
																						MAKE_YOUNG_PAIR(BgL_arg1553z00_438,
																						BgL_arg1559z00_439);
																				}
																				return
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																					BgL_arg1552z00_437);
																			}
																		else
																			{	/* Expand/case.scm 265 */
																				obj_t BgL_arg1602z00_465;

																				{	/* Expand/case.scm 265 */
																					obj_t BgL_arg1605z00_466;
																					obj_t BgL_arg1606z00_467;

																					{	/* Expand/case.scm 265 */
																						obj_t BgL_arg1609z00_468;

																						{	/* Expand/case.scm 265 */
																							obj_t BgL_arg1611z00_469;

																							{	/* Expand/case.scm 265 */
																								obj_t BgL_arg1613z00_470;

																								{	/* Expand/case.scm 265 */
																									obj_t BgL_arg1615z00_471;

																									{	/* Expand/case.scm 265 */
																										obj_t BgL_arg1616z00_472;

																										BgL_arg1616z00_472 =
																											MAKE_YOUNG_PAIR
																											(BgL_datumsz00_399, BNIL);
																										BgL_arg1615z00_471 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(18),
																											BgL_arg1616z00_472);
																									}
																									BgL_arg1613z00_470 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1615z00_471, BNIL);
																								}
																								BgL_arg1611z00_469 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(17), BgL_arg1613z00_470);
																							}
																							BgL_arg1609z00_468 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(22), BgL_arg1611z00_469);
																						}
																						BgL_arg1605z00_466 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_880,
																							BgL_arg1609z00_468, BgL_ez00_880);
																					}
																					{	/* Expand/case.scm 266 */
																						obj_t BgL_arg1625z00_473;
																						obj_t BgL_arg1626z00_474;

																						{	/* Expand/case.scm 266 */
																							obj_t BgL_arg1627z00_475;

																							BgL_arg1627z00_475 =
																								BGl_expandzd2prognzd2zz__prognz00
																								(BgL_bodyz00_400);
																							BgL_arg1625z00_473 =
																								BGL_PROCEDURE_CALL2
																								(BgL_ez00_880,
																								BgL_arg1627z00_475,
																								BgL_ez00_880);
																						}
																						{	/* Expand/case.scm 267 */
																							obj_t BgL_arg1629z00_476;

																							{	/* Expand/case.scm 267 */
																								obj_t BgL_arg1630z00_477;

																								BgL_arg1630z00_477 =
																									CDR(
																									((obj_t) BgL_clausesz00_393));
																								BgL_arg1629z00_476 =
																									BGl_loopze71ze7zzexpand_casez00
																									(BgL_ez00_880,
																									BgL_arg1630z00_477);
																							}
																							BgL_arg1626z00_474 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1629z00_476, BNIL);
																						}
																						BgL_arg1606z00_467 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1625z00_473,
																							BgL_arg1626z00_474);
																					}
																					BgL_arg1602z00_465 =
																						MAKE_YOUNG_PAIR(BgL_arg1605z00_466,
																						BgL_arg1606z00_467);
																				}
																				return
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																					BgL_arg1602z00_465);
																			}
																	}
															}
														else
															{	/* Expand/case.scm 249 */
																if (NULLP(CDR(((obj_t) BgL_carzd2509zd2_411))))
																	{	/* Expand/case.scm 249 */
																		obj_t BgL_arg1472z00_419;
																		obj_t BgL_arg1473z00_420;

																		BgL_arg1472z00_419 =
																			CAR(((obj_t) BgL_carzd2509zd2_411));
																		BgL_arg1473z00_420 =
																			CDR(BgL_ezd2493zd2_405);
																		BgL_datumz00_402 = BgL_arg1472z00_419;
																		BgL_bodyz00_403 = BgL_arg1473z00_420;
																		if (NULLP(BgL_bodyz00_403))
																			{	/* Expand/case.scm 270 */
																				obj_t BgL_arg1646z00_480;

																				BgL_arg1646z00_480 =
																					CAR(((obj_t) BgL_clausesz00_393));
																				return
																					BGl_errorz00zz__errorz00
																					(BGl_string2006z00zzexpand_casez00,
																					BGl_string2009z00zzexpand_casez00,
																					BgL_arg1646z00_480);
																			}
																		else
																			{	/* Expand/case.scm 271 */
																				obj_t BgL_arg1650z00_481;

																				{	/* Expand/case.scm 271 */
																					obj_t BgL_arg1651z00_482;
																					obj_t BgL_arg1654z00_483;

																					{	/* Expand/case.scm 271 */
																						obj_t BgL_arg1661z00_484;

																						{	/* Expand/case.scm 271 */
																							obj_t BgL_arg1663z00_485;

																							{	/* Expand/case.scm 271 */
																								obj_t BgL_arg1675z00_486;

																								{	/* Expand/case.scm 271 */
																									obj_t BgL_arg1678z00_487;

																									{	/* Expand/case.scm 271 */
																										obj_t BgL_arg1681z00_488;

																										BgL_arg1681z00_488 =
																											MAKE_YOUNG_PAIR
																											(BgL_datumz00_402, BNIL);
																										BgL_arg1678z00_487 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(18),
																											BgL_arg1681z00_488);
																									}
																									BgL_arg1675z00_486 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1678z00_487, BNIL);
																								}
																								BgL_arg1663z00_485 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(17), BgL_arg1675z00_486);
																							}
																							BgL_arg1661z00_484 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(19), BgL_arg1663z00_485);
																						}
																						BgL_arg1651z00_482 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_880,
																							BgL_arg1661z00_484, BgL_ez00_880);
																					}
																					{	/* Expand/case.scm 272 */
																						obj_t BgL_arg1688z00_489;
																						obj_t BgL_arg1689z00_490;

																						{	/* Expand/case.scm 272 */
																							obj_t BgL_arg1691z00_491;

																							BgL_arg1691z00_491 =
																								BGl_expandzd2prognzd2zz__prognz00
																								(BgL_bodyz00_403);
																							BgL_arg1688z00_489 =
																								BGL_PROCEDURE_CALL2
																								(BgL_ez00_880,
																								BgL_arg1691z00_491,
																								BgL_ez00_880);
																						}
																						{	/* Expand/case.scm 273 */
																							obj_t BgL_arg1692z00_492;

																							{	/* Expand/case.scm 273 */
																								obj_t BgL_arg1699z00_493;

																								BgL_arg1699z00_493 =
																									CDR(
																									((obj_t) BgL_clausesz00_393));
																								BgL_arg1692z00_492 =
																									BGl_loopze71ze7zzexpand_casez00
																									(BgL_ez00_880,
																									BgL_arg1699z00_493);
																							}
																							BgL_arg1689z00_490 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1692z00_492, BNIL);
																						}
																						BgL_arg1654z00_483 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1688z00_489,
																							BgL_arg1689z00_490);
																					}
																					BgL_arg1650z00_481 =
																						MAKE_YOUNG_PAIR(BgL_arg1651z00_482,
																						BgL_arg1654z00_483);
																				}
																				return
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																					BgL_arg1650z00_481);
																			}
																	}
																else
																	{	/* Expand/case.scm 249 */
																		return BFALSE;
																	}
															}
													}
												else
													{	/* Expand/case.scm 249 */
														return BFALSE;
													}
											}
									}
								else
									{	/* Expand/case.scm 249 */
										return BFALSE;
									}
							}
					}
				}
		}

	}



/* type-test */
	obj_t BGl_typezd2testzd2zzexpand_casez00(obj_t BgL_auxz00_25,
		obj_t BgL_typez00_26, obj_t BgL_valuez00_27, obj_t BgL_casezd2formzd2_28,
		obj_t BgL_elsezd2bodyzd2_29, obj_t BgL_elsezd2namezd2_30, obj_t BgL_ez00_31)
	{
		{	/* Expand/case.scm 278 */
			if ((BgL_typez00_26 == CNST_TABLE_REF(4)))
				{	/* Expand/case.scm 281 */
					obj_t BgL_arg1701z00_497;

					{	/* Expand/case.scm 281 */
						obj_t BgL_arg1702z00_498;
						obj_t BgL_arg1703z00_499;

						{	/* Expand/case.scm 281 */
							obj_t BgL_arg1705z00_500;

							{	/* Expand/case.scm 281 */
								obj_t BgL_arg1708z00_501;

								BgL_arg1708z00_501 =
									MAKE_YOUNG_PAIR(BNIL,
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_elsezd2bodyzd2_29, BNIL));
								BgL_arg1705z00_500 =
									MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30, BgL_arg1708z00_501);
							}
							BgL_arg1702z00_498 = MAKE_YOUNG_PAIR(BgL_arg1705z00_500, BNIL);
						}
						{	/* Expand/case.scm 282 */
							obj_t BgL_arg1710z00_503;

							{	/* Expand/case.scm 282 */
								obj_t BgL_arg1711z00_504;

								{	/* Expand/case.scm 282 */
									obj_t BgL_arg1714z00_505;
									obj_t BgL_arg1717z00_506;

									{	/* Expand/case.scm 282 */
										obj_t BgL_arg1718z00_507;

										{	/* Expand/case.scm 282 */
											obj_t BgL_arg1720z00_508;

											{	/* Expand/case.scm 282 */
												obj_t BgL_arg1722z00_509;

												BgL_arg1722z00_509 =
													BGL_PROCEDURE_CALL2(BgL_ez00_31, BgL_valuez00_27,
													BgL_ez00_31);
												BgL_arg1720z00_508 =
													MAKE_YOUNG_PAIR(BgL_arg1722z00_509, BNIL);
											}
											BgL_arg1718z00_507 =
												MAKE_YOUNG_PAIR(BgL_auxz00_25, BgL_arg1720z00_508);
										}
										BgL_arg1714z00_505 =
											MAKE_YOUNG_PAIR(BgL_arg1718z00_507, BNIL);
									}
									{	/* Expand/case.scm 283 */
										obj_t BgL_arg1724z00_510;

										{	/* Expand/case.scm 283 */
											obj_t BgL_arg1733z00_511;

											{	/* Expand/case.scm 283 */
												obj_t BgL_arg1734z00_512;
												obj_t BgL_arg1735z00_513;

												{	/* Expand/case.scm 283 */
													obj_t BgL_arg1736z00_514;

													BgL_arg1736z00_514 =
														MAKE_YOUNG_PAIR(BgL_auxz00_25, BNIL);
													BgL_arg1734z00_512 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(23),
														BgL_arg1736z00_514);
												}
												{	/* Expand/case.scm 285 */
													obj_t BgL_arg1737z00_515;

													{	/* Expand/case.scm 285 */
														obj_t BgL_arg1738z00_516;

														BgL_arg1738z00_516 =
															MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30, BNIL);
														BgL_arg1737z00_515 =
															MAKE_YOUNG_PAIR(BgL_arg1738z00_516, BNIL);
													}
													BgL_arg1735z00_513 =
														MAKE_YOUNG_PAIR(BgL_casezd2formzd2_28,
														BgL_arg1737z00_515);
												}
												BgL_arg1733z00_511 =
													MAKE_YOUNG_PAIR(BgL_arg1734z00_512,
													BgL_arg1735z00_513);
											}
											BgL_arg1724z00_510 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(15), BgL_arg1733z00_511);
										}
										BgL_arg1717z00_506 =
											MAKE_YOUNG_PAIR(BgL_arg1724z00_510, BNIL);
									}
									BgL_arg1711z00_504 =
										MAKE_YOUNG_PAIR(BgL_arg1714z00_505, BgL_arg1717z00_506);
								}
								BgL_arg1710z00_503 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1711z00_504);
							}
							BgL_arg1703z00_499 = MAKE_YOUNG_PAIR(BgL_arg1710z00_503, BNIL);
						}
						BgL_arg1701z00_497 =
							MAKE_YOUNG_PAIR(BgL_arg1702z00_498, BgL_arg1703z00_499);
					}
					return MAKE_YOUNG_PAIR(CNST_TABLE_REF(24), BgL_arg1701z00_497);
				}
			else
				{	/* Expand/case.scm 280 */
					if ((BgL_typez00_26 == CNST_TABLE_REF(1)))
						{	/* Expand/case.scm 287 */
							obj_t BgL_arg1740z00_518;

							{	/* Expand/case.scm 287 */
								obj_t BgL_arg1746z00_519;
								obj_t BgL_arg1747z00_520;

								{	/* Expand/case.scm 287 */
									obj_t BgL_arg1748z00_521;

									{	/* Expand/case.scm 287 */
										obj_t BgL_arg1749z00_522;

										BgL_arg1749z00_522 =
											MAKE_YOUNG_PAIR(BNIL,
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_elsezd2bodyzd2_29, BNIL));
										BgL_arg1748z00_521 =
											MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30,
											BgL_arg1749z00_522);
									}
									BgL_arg1746z00_519 =
										MAKE_YOUNG_PAIR(BgL_arg1748z00_521, BNIL);
								}
								{	/* Expand/case.scm 288 */
									obj_t BgL_arg1751z00_524;

									{	/* Expand/case.scm 288 */
										obj_t BgL_arg1752z00_525;

										{	/* Expand/case.scm 288 */
											obj_t BgL_arg1753z00_526;
											obj_t BgL_arg1754z00_527;

											{	/* Expand/case.scm 288 */
												obj_t BgL_arg1755z00_528;

												{	/* Expand/case.scm 288 */
													obj_t BgL_arg1761z00_529;

													{	/* Expand/case.scm 288 */
														obj_t BgL_arg1762z00_530;

														BgL_arg1762z00_530 =
															BGL_PROCEDURE_CALL2(BgL_ez00_31, BgL_valuez00_27,
															BgL_ez00_31);
														BgL_arg1761z00_529 =
															MAKE_YOUNG_PAIR(BgL_arg1762z00_530, BNIL);
													}
													BgL_arg1755z00_528 =
														MAKE_YOUNG_PAIR(BgL_auxz00_25, BgL_arg1761z00_529);
												}
												BgL_arg1753z00_526 =
													MAKE_YOUNG_PAIR(BgL_arg1755z00_528, BNIL);
											}
											{	/* Expand/case.scm 289 */
												obj_t BgL_arg1765z00_531;

												{	/* Expand/case.scm 289 */
													obj_t BgL_arg1767z00_532;

													{	/* Expand/case.scm 289 */
														obj_t BgL_arg1770z00_533;
														obj_t BgL_arg1771z00_534;

														{	/* Expand/case.scm 289 */
															obj_t BgL_arg1773z00_535;

															BgL_arg1773z00_535 =
																MAKE_YOUNG_PAIR(BgL_auxz00_25, BNIL);
															BgL_arg1770z00_533 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(25),
																BgL_arg1773z00_535);
														}
														{	/* Expand/case.scm 291 */
															obj_t BgL_arg1775z00_536;

															{	/* Expand/case.scm 291 */
																obj_t BgL_arg1798z00_537;

																BgL_arg1798z00_537 =
																	MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30, BNIL);
																BgL_arg1775z00_536 =
																	MAKE_YOUNG_PAIR(BgL_arg1798z00_537, BNIL);
															}
															BgL_arg1771z00_534 =
																MAKE_YOUNG_PAIR(BgL_casezd2formzd2_28,
																BgL_arg1775z00_536);
														}
														BgL_arg1767z00_532 =
															MAKE_YOUNG_PAIR(BgL_arg1770z00_533,
															BgL_arg1771z00_534);
													}
													BgL_arg1765z00_531 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
														BgL_arg1767z00_532);
												}
												BgL_arg1754z00_527 =
													MAKE_YOUNG_PAIR(BgL_arg1765z00_531, BNIL);
											}
											BgL_arg1752z00_525 =
												MAKE_YOUNG_PAIR(BgL_arg1753z00_526, BgL_arg1754z00_527);
										}
										BgL_arg1751z00_524 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1752z00_525);
									}
									BgL_arg1747z00_520 =
										MAKE_YOUNG_PAIR(BgL_arg1751z00_524, BNIL);
								}
								BgL_arg1740z00_518 =
									MAKE_YOUNG_PAIR(BgL_arg1746z00_519, BgL_arg1747z00_520);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(24), BgL_arg1740z00_518);
						}
					else
						{	/* Expand/case.scm 280 */
							if ((BgL_typez00_26 == CNST_TABLE_REF(2)))
								{	/* Expand/case.scm 293 */
									obj_t BgL_arg1805z00_539;

									{	/* Expand/case.scm 293 */
										obj_t BgL_arg1806z00_540;
										obj_t BgL_arg1808z00_541;

										{	/* Expand/case.scm 293 */
											obj_t BgL_arg1812z00_542;

											{	/* Expand/case.scm 293 */
												obj_t BgL_arg1820z00_543;

												BgL_arg1820z00_543 =
													MAKE_YOUNG_PAIR(BNIL,
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_elsezd2bodyzd2_29, BNIL));
												BgL_arg1812z00_542 =
													MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30,
													BgL_arg1820z00_543);
											}
											BgL_arg1806z00_540 =
												MAKE_YOUNG_PAIR(BgL_arg1812z00_542, BNIL);
										}
										{	/* Expand/case.scm 294 */
											obj_t BgL_arg1823z00_545;

											{	/* Expand/case.scm 294 */
												obj_t BgL_arg1831z00_546;

												{	/* Expand/case.scm 294 */
													obj_t BgL_arg1832z00_547;
													obj_t BgL_arg1833z00_548;

													{	/* Expand/case.scm 294 */
														obj_t BgL_arg1834z00_549;

														{	/* Expand/case.scm 294 */
															obj_t BgL_arg1835z00_550;

															{	/* Expand/case.scm 294 */
																obj_t BgL_arg1836z00_551;

																BgL_arg1836z00_551 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_31,
																	BgL_valuez00_27, BgL_ez00_31);
																BgL_arg1835z00_550 =
																	MAKE_YOUNG_PAIR(BgL_arg1836z00_551, BNIL);
															}
															BgL_arg1834z00_549 =
																MAKE_YOUNG_PAIR(BgL_auxz00_25,
																BgL_arg1835z00_550);
														}
														BgL_arg1832z00_547 =
															MAKE_YOUNG_PAIR(BgL_arg1834z00_549, BNIL);
													}
													{	/* Expand/case.scm 295 */
														obj_t BgL_arg1837z00_552;

														{	/* Expand/case.scm 295 */
															obj_t BgL_arg1838z00_553;

															{	/* Expand/case.scm 295 */
																obj_t BgL_arg1839z00_554;
																obj_t BgL_arg1840z00_555;

																{	/* Expand/case.scm 295 */
																	obj_t BgL_arg1842z00_556;

																	BgL_arg1842z00_556 =
																		MAKE_YOUNG_PAIR(BgL_auxz00_25, BNIL);
																	BgL_arg1839z00_554 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(26),
																		BgL_arg1842z00_556);
																}
																{	/* Expand/case.scm 297 */
																	obj_t BgL_arg1843z00_557;

																	{	/* Expand/case.scm 297 */
																		obj_t BgL_arg1844z00_558;

																		BgL_arg1844z00_558 =
																			MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30,
																			BNIL);
																		BgL_arg1843z00_557 =
																			MAKE_YOUNG_PAIR(BgL_arg1844z00_558, BNIL);
																	}
																	BgL_arg1840z00_555 =
																		MAKE_YOUNG_PAIR(BgL_casezd2formzd2_28,
																		BgL_arg1843z00_557);
																}
																BgL_arg1838z00_553 =
																	MAKE_YOUNG_PAIR(BgL_arg1839z00_554,
																	BgL_arg1840z00_555);
															}
															BgL_arg1837z00_552 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																BgL_arg1838z00_553);
														}
														BgL_arg1833z00_548 =
															MAKE_YOUNG_PAIR(BgL_arg1837z00_552, BNIL);
													}
													BgL_arg1831z00_546 =
														MAKE_YOUNG_PAIR(BgL_arg1832z00_547,
														BgL_arg1833z00_548);
												}
												BgL_arg1823z00_545 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
													BgL_arg1831z00_546);
											}
											BgL_arg1808z00_541 =
												MAKE_YOUNG_PAIR(BgL_arg1823z00_545, BNIL);
										}
										BgL_arg1805z00_539 =
											MAKE_YOUNG_PAIR(BgL_arg1806z00_540, BgL_arg1808z00_541);
									}
									return
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(24), BgL_arg1805z00_539);
								}
							else
								{	/* Expand/case.scm 280 */
									if ((BgL_typez00_26 == CNST_TABLE_REF(3)))
										{	/* Expand/case.scm 299 */
											obj_t BgL_arg1846z00_560;

											{	/* Expand/case.scm 299 */
												obj_t BgL_arg1847z00_561;
												obj_t BgL_arg1848z00_562;

												{	/* Expand/case.scm 299 */
													obj_t BgL_arg1849z00_563;

													{	/* Expand/case.scm 299 */
														obj_t BgL_arg1850z00_564;

														BgL_arg1850z00_564 =
															MAKE_YOUNG_PAIR(BNIL,
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_elsezd2bodyzd2_29, BNIL));
														BgL_arg1849z00_563 =
															MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30,
															BgL_arg1850z00_564);
													}
													BgL_arg1847z00_561 =
														MAKE_YOUNG_PAIR(BgL_arg1849z00_563, BNIL);
												}
												{	/* Expand/case.scm 300 */
													obj_t BgL_arg1852z00_566;

													{	/* Expand/case.scm 300 */
														obj_t BgL_arg1853z00_567;

														{	/* Expand/case.scm 300 */
															obj_t BgL_arg1854z00_568;
															obj_t BgL_arg1856z00_569;

															{	/* Expand/case.scm 300 */
																obj_t BgL_arg1857z00_570;

																{	/* Expand/case.scm 300 */
																	obj_t BgL_arg1858z00_571;

																	{	/* Expand/case.scm 300 */
																		obj_t BgL_arg1859z00_572;

																		BgL_arg1859z00_572 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_31,
																			BgL_valuez00_27, BgL_ez00_31);
																		BgL_arg1858z00_571 =
																			MAKE_YOUNG_PAIR(BgL_arg1859z00_572, BNIL);
																	}
																	BgL_arg1857z00_570 =
																		MAKE_YOUNG_PAIR(BgL_auxz00_25,
																		BgL_arg1858z00_571);
																}
																BgL_arg1854z00_568 =
																	MAKE_YOUNG_PAIR(BgL_arg1857z00_570, BNIL);
															}
															{	/* Expand/case.scm 301 */
																obj_t BgL_arg1860z00_573;

																{	/* Expand/case.scm 301 */
																	obj_t BgL_arg1862z00_574;

																	{	/* Expand/case.scm 301 */
																		obj_t BgL_arg1863z00_575;
																		obj_t BgL_arg1864z00_576;

																		{	/* Expand/case.scm 301 */
																			obj_t BgL_arg1866z00_577;

																			BgL_arg1866z00_577 =
																				MAKE_YOUNG_PAIR(BgL_auxz00_25, BNIL);
																			BgL_arg1863z00_575 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(27),
																				BgL_arg1866z00_577);
																		}
																		{	/* Expand/case.scm 303 */
																			obj_t BgL_arg1868z00_578;

																			{	/* Expand/case.scm 303 */
																				obj_t BgL_arg1869z00_579;

																				BgL_arg1869z00_579 =
																					MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30,
																					BNIL);
																				BgL_arg1868z00_578 =
																					MAKE_YOUNG_PAIR(BgL_arg1869z00_579,
																					BNIL);
																			}
																			BgL_arg1864z00_576 =
																				MAKE_YOUNG_PAIR(BgL_casezd2formzd2_28,
																				BgL_arg1868z00_578);
																		}
																		BgL_arg1862z00_574 =
																			MAKE_YOUNG_PAIR(BgL_arg1863z00_575,
																			BgL_arg1864z00_576);
																	}
																	BgL_arg1860z00_573 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																		BgL_arg1862z00_574);
																}
																BgL_arg1856z00_569 =
																	MAKE_YOUNG_PAIR(BgL_arg1860z00_573, BNIL);
															}
															BgL_arg1853z00_567 =
																MAKE_YOUNG_PAIR(BgL_arg1854z00_568,
																BgL_arg1856z00_569);
														}
														BgL_arg1852z00_566 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
															BgL_arg1853z00_567);
													}
													BgL_arg1848z00_562 =
														MAKE_YOUNG_PAIR(BgL_arg1852z00_566, BNIL);
												}
												BgL_arg1846z00_560 =
													MAKE_YOUNG_PAIR(BgL_arg1847z00_561,
													BgL_arg1848z00_562);
											}
											return
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(24), BgL_arg1846z00_560);
										}
									else
										{	/* Expand/case.scm 280 */
											if ((BgL_typez00_26 == CNST_TABLE_REF(6)))
												{	/* Expand/case.scm 305 */
													obj_t BgL_arg1872z00_581;

													{	/* Expand/case.scm 305 */
														obj_t BgL_arg1873z00_582;
														obj_t BgL_arg1874z00_583;

														{	/* Expand/case.scm 305 */
															obj_t BgL_arg1875z00_584;

															{	/* Expand/case.scm 305 */
																obj_t BgL_arg1876z00_585;

																BgL_arg1876z00_585 =
																	MAKE_YOUNG_PAIR(BNIL,
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_elsezd2bodyzd2_29, BNIL));
																BgL_arg1875z00_584 =
																	MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30,
																	BgL_arg1876z00_585);
															}
															BgL_arg1873z00_582 =
																MAKE_YOUNG_PAIR(BgL_arg1875z00_584, BNIL);
														}
														{	/* Expand/case.scm 306 */
															obj_t BgL_arg1878z00_587;

															{	/* Expand/case.scm 306 */
																obj_t BgL_arg1879z00_588;

																{	/* Expand/case.scm 306 */
																	obj_t BgL_arg1880z00_589;
																	obj_t BgL_arg1882z00_590;

																	{	/* Expand/case.scm 306 */
																		obj_t BgL_arg1883z00_591;

																		{	/* Expand/case.scm 306 */
																			obj_t BgL_arg1884z00_592;

																			{	/* Expand/case.scm 306 */
																				obj_t BgL_arg1885z00_593;

																				BgL_arg1885z00_593 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_31,
																					BgL_valuez00_27, BgL_ez00_31);
																				BgL_arg1884z00_592 =
																					MAKE_YOUNG_PAIR(BgL_arg1885z00_593,
																					BNIL);
																			}
																			BgL_arg1883z00_591 =
																				MAKE_YOUNG_PAIR(BgL_auxz00_25,
																				BgL_arg1884z00_592);
																		}
																		BgL_arg1880z00_589 =
																			MAKE_YOUNG_PAIR(BgL_arg1883z00_591, BNIL);
																	}
																	{	/* Expand/case.scm 307 */
																		obj_t BgL_arg1887z00_594;

																		{	/* Expand/case.scm 307 */
																			obj_t BgL_arg1888z00_595;

																			{	/* Expand/case.scm 307 */
																				obj_t BgL_arg1889z00_596;
																				obj_t BgL_arg1890z00_597;

																				{	/* Expand/case.scm 307 */
																					obj_t BgL_arg1891z00_598;

																					BgL_arg1891z00_598 =
																						MAKE_YOUNG_PAIR(BgL_auxz00_25,
																						BNIL);
																					BgL_arg1889z00_596 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(28),
																						BgL_arg1891z00_598);
																				}
																				{	/* Expand/case.scm 309 */
																					obj_t BgL_arg1892z00_599;

																					{	/* Expand/case.scm 309 */
																						obj_t BgL_arg1893z00_600;

																						BgL_arg1893z00_600 =
																							MAKE_YOUNG_PAIR
																							(BgL_elsezd2namezd2_30, BNIL);
																						BgL_arg1892z00_599 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1893z00_600, BNIL);
																					}
																					BgL_arg1890z00_597 =
																						MAKE_YOUNG_PAIR
																						(BgL_casezd2formzd2_28,
																						BgL_arg1892z00_599);
																				}
																				BgL_arg1888z00_595 =
																					MAKE_YOUNG_PAIR(BgL_arg1889z00_596,
																					BgL_arg1890z00_597);
																			}
																			BgL_arg1887z00_594 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																				BgL_arg1888z00_595);
																		}
																		BgL_arg1882z00_590 =
																			MAKE_YOUNG_PAIR(BgL_arg1887z00_594, BNIL);
																	}
																	BgL_arg1879z00_588 =
																		MAKE_YOUNG_PAIR(BgL_arg1880z00_589,
																		BgL_arg1882z00_590);
																}
																BgL_arg1878z00_587 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																	BgL_arg1879z00_588);
															}
															BgL_arg1874z00_583 =
																MAKE_YOUNG_PAIR(BgL_arg1878z00_587, BNIL);
														}
														BgL_arg1872z00_581 =
															MAKE_YOUNG_PAIR(BgL_arg1873z00_582,
															BgL_arg1874z00_583);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
														BgL_arg1872z00_581);
												}
											else
												{	/* Expand/case.scm 280 */
													if ((BgL_typez00_26 == CNST_TABLE_REF(7)))
														{	/* Expand/case.scm 311 */
															obj_t BgL_arg1896z00_602;

															{	/* Expand/case.scm 311 */
																obj_t BgL_arg1897z00_603;
																obj_t BgL_arg1898z00_604;

																{	/* Expand/case.scm 311 */
																	obj_t BgL_arg1899z00_605;

																	{	/* Expand/case.scm 311 */
																		obj_t BgL_arg1901z00_606;

																		BgL_arg1901z00_606 =
																			MAKE_YOUNG_PAIR(BNIL,
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_elsezd2bodyzd2_29, BNIL));
																		BgL_arg1899z00_605 =
																			MAKE_YOUNG_PAIR(BgL_elsezd2namezd2_30,
																			BgL_arg1901z00_606);
																	}
																	BgL_arg1897z00_603 =
																		MAKE_YOUNG_PAIR(BgL_arg1899z00_605, BNIL);
																}
																{	/* Expand/case.scm 312 */
																	obj_t BgL_arg1903z00_608;

																	{	/* Expand/case.scm 312 */
																		obj_t BgL_arg1904z00_609;

																		{	/* Expand/case.scm 312 */
																			obj_t BgL_arg1906z00_610;
																			obj_t BgL_arg1910z00_611;

																			{	/* Expand/case.scm 312 */
																				obj_t BgL_arg1911z00_612;

																				{	/* Expand/case.scm 312 */
																					obj_t BgL_arg1912z00_613;

																					{	/* Expand/case.scm 312 */
																						obj_t BgL_arg1913z00_614;

																						BgL_arg1913z00_614 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_31,
																							BgL_valuez00_27, BgL_ez00_31);
																						BgL_arg1912z00_613 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1913z00_614, BNIL);
																					}
																					BgL_arg1911z00_612 =
																						MAKE_YOUNG_PAIR(BgL_auxz00_25,
																						BgL_arg1912z00_613);
																				}
																				BgL_arg1906z00_610 =
																					MAKE_YOUNG_PAIR(BgL_arg1911z00_612,
																					BNIL);
																			}
																			{	/* Expand/case.scm 313 */
																				obj_t BgL_arg1914z00_615;

																				{	/* Expand/case.scm 313 */
																					obj_t BgL_arg1916z00_616;

																					{	/* Expand/case.scm 313 */
																						obj_t BgL_arg1917z00_617;
																						obj_t BgL_arg1918z00_618;

																						{	/* Expand/case.scm 313 */
																							obj_t BgL_arg1919z00_619;

																							BgL_arg1919z00_619 =
																								MAKE_YOUNG_PAIR(BgL_auxz00_25,
																								BNIL);
																							BgL_arg1917z00_617 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(29), BgL_arg1919z00_619);
																						}
																						{	/* Expand/case.scm 315 */
																							obj_t BgL_arg1920z00_620;

																							{	/* Expand/case.scm 315 */
																								obj_t BgL_arg1923z00_621;

																								BgL_arg1923z00_621 =
																									MAKE_YOUNG_PAIR
																									(BgL_elsezd2namezd2_30, BNIL);
																								BgL_arg1920z00_620 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1923z00_621, BNIL);
																							}
																							BgL_arg1918z00_618 =
																								MAKE_YOUNG_PAIR
																								(BgL_casezd2formzd2_28,
																								BgL_arg1920z00_620);
																						}
																						BgL_arg1916z00_616 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1917z00_617,
																							BgL_arg1918z00_618);
																					}
																					BgL_arg1914z00_615 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																						BgL_arg1916z00_616);
																				}
																				BgL_arg1910z00_611 =
																					MAKE_YOUNG_PAIR(BgL_arg1914z00_615,
																					BNIL);
																			}
																			BgL_arg1904z00_609 =
																				MAKE_YOUNG_PAIR(BgL_arg1906z00_610,
																				BgL_arg1910z00_611);
																		}
																		BgL_arg1903z00_608 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																			BgL_arg1904z00_609);
																	}
																	BgL_arg1898z00_604 =
																		MAKE_YOUNG_PAIR(BgL_arg1903z00_608, BNIL);
																}
																BgL_arg1896z00_602 =
																	MAKE_YOUNG_PAIR(BgL_arg1897z00_603,
																	BgL_arg1898z00_604);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
																BgL_arg1896z00_602);
														}
													else
														{	/* Expand/case.scm 280 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string2010z00zzexpand_casez00,
																BGl_string2011z00zzexpand_casez00,
																BgL_typez00_26);
														}
												}
										}
								}
						}
				}
		}

	}



/* do-generic-case */
	obj_t BGl_dozd2genericzd2casez00zzexpand_casez00(obj_t BgL_valuez00_32,
		obj_t BgL_clausesz00_33, obj_t BgL_ez00_34)
	{
		{	/* Expand/case.scm 323 */
			{	/* Expand/case.scm 324 */
				obj_t BgL_arg1924z00_622;

				{	/* Expand/case.scm 324 */
					obj_t BgL_arg1925z00_623;

					{	/* Expand/case.scm 324 */
						obj_t BgL_arg1926z00_624;
						obj_t BgL_arg1927z00_625;

						{	/* Expand/case.scm 324 */
							obj_t BgL_arg1928z00_626;

							{	/* Expand/case.scm 324 */
								obj_t BgL_arg1929z00_627;

								BgL_arg1929z00_627 = MAKE_YOUNG_PAIR(BgL_valuez00_32, BNIL);
								BgL_arg1928z00_626 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg1929z00_627);
							}
							BgL_arg1926z00_624 = MAKE_YOUNG_PAIR(BgL_arg1928z00_626, BNIL);
						}
						BgL_arg1927z00_625 =
							MAKE_YOUNG_PAIR(BGl_loopze70ze7zzexpand_casez00
							(BgL_clausesz00_33), BNIL);
						BgL_arg1925z00_623 =
							MAKE_YOUNG_PAIR(BgL_arg1926z00_624, BgL_arg1927z00_625);
					}
					BgL_arg1924z00_622 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1925z00_623);
				}
				return
					BGL_PROCEDURE_CALL2(BgL_ez00_34, BgL_arg1924z00_622, BgL_ez00_34);
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzexpand_casez00(obj_t BgL_clausesz00_630)
	{
		{	/* Expand/case.scm 325 */
			if (NULLP(BgL_clausesz00_630))
				{	/* Expand/case.scm 326 */
					return BUNSPEC;
				}
			else
				{
					obj_t BgL_datumsz00_636;
					obj_t BgL_bodyz00_637;
					obj_t BgL_datumz00_639;
					obj_t BgL_bodyz00_640;

					{	/* Expand/case.scm 328 */
						obj_t BgL_ezd2553zd2_642;

						BgL_ezd2553zd2_642 = CAR(((obj_t) BgL_clausesz00_630));
						if (NULLP(BgL_ezd2553zd2_642))
							{	/* Expand/case.scm 328 */
								return BUNSPEC;
							}
						else
							{	/* Expand/case.scm 328 */
								if (PAIRP(BgL_ezd2553zd2_642))
									{	/* Expand/case.scm 328 */
										if ((CAR(BgL_ezd2553zd2_642) == CNST_TABLE_REF(11)))
											{	/* Expand/case.scm 328 */
												obj_t BgL_arg1937z00_647;

												BgL_arg1937z00_647 = CDR(BgL_ezd2553zd2_642);
												if (NULLP(BgL_arg1937z00_647))
													{	/* Expand/case.scm 332 */
														return BFALSE;
													}
												else
													{	/* Expand/case.scm 332 */
														return
															BGl_expandzd2prognzd2zz__prognz00
															(BgL_arg1937z00_647);
													}
											}
										else
											{	/* Expand/case.scm 328 */
												obj_t BgL_carzd2569zd2_648;

												BgL_carzd2569zd2_648 = CAR(BgL_ezd2553zd2_642);
												if (PAIRP(BgL_carzd2569zd2_648))
													{	/* Expand/case.scm 328 */
														bool_t BgL_test2113z00_1678;

														{	/* Expand/case.scm 328 */
															obj_t BgL_tmpz00_1679;

															BgL_tmpz00_1679 = CDR(BgL_carzd2569zd2_648);
															BgL_test2113z00_1678 = PAIRP(BgL_tmpz00_1679);
														}
														if (BgL_test2113z00_1678)
															{	/* Expand/case.scm 328 */
																BgL_datumsz00_636 = BgL_carzd2569zd2_648;
																BgL_bodyz00_637 = CDR(BgL_ezd2553zd2_642);
																if (NULLP(BgL_bodyz00_637))
																	{	/* Expand/case.scm 338 */
																		obj_t BgL_arg1957z00_670;

																		BgL_arg1957z00_670 =
																			CAR(((obj_t) BgL_clausesz00_630));
																		return
																			BGl_errorz00zz__errorz00
																			(BGl_string2006z00zzexpand_casez00,
																			BGl_string2009z00zzexpand_casez00,
																			BgL_arg1957z00_670);
																	}
																else
																	{	/* Expand/case.scm 339 */
																		bool_t BgL_test2115z00_1687;

																		if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_datumsz00_636))
																			{	/* Expand/case.scm 339 */
																				BgL_test2115z00_1687 =
																					(bgl_list_length(BgL_datumsz00_636) <
																					10L);
																			}
																		else
																			{	/* Expand/case.scm 339 */
																				BgL_test2115z00_1687 = ((bool_t) 0);
																			}
																		if (BgL_test2115z00_1687)
																			{	/* Expand/case.scm 341 */
																				obj_t BgL_arg1961z00_674;

																				{	/* Expand/case.scm 341 */
																					obj_t BgL_arg1962z00_675;
																					obj_t BgL_arg1963z00_676;

																					{	/* Expand/case.scm 341 */
																						obj_t BgL_arg1964z00_677;

																						{	/* Expand/case.scm 341 */
																							obj_t BgL_arg1965z00_678;

																							{	/* Expand/case.scm 341 */
																								obj_t BgL_head1086z00_681;

																								BgL_head1086z00_681 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1084z00_683;
																									obj_t BgL_tail1087z00_684;

																									BgL_l1084z00_683 =
																										BgL_datumsz00_636;
																									BgL_tail1087z00_684 =
																										BgL_head1086z00_681;
																								BgL_zc3z04anonymousza31967ze3z87_685:
																									if (NULLP
																										(BgL_l1084z00_683))
																										{	/* Expand/case.scm 341 */
																											BgL_arg1965z00_678 =
																												CDR
																												(BgL_head1086z00_681);
																										}
																									else
																										{	/* Expand/case.scm 341 */
																											obj_t
																												BgL_newtail1088z00_687;
																											{	/* Expand/case.scm 341 */
																												obj_t
																													BgL_arg1970z00_689;
																												{	/* Expand/case.scm 341 */
																													obj_t BgL_dz00_690;

																													BgL_dz00_690 =
																														CAR(
																														((obj_t)
																															BgL_l1084z00_683));
																													{	/* Expand/case.scm 341 */
																														obj_t
																															BgL_arg1971z00_691;
																														{	/* Expand/case.scm 341 */
																															obj_t
																																BgL_arg1972z00_692;
																															{	/* Expand/case.scm 341 */
																																obj_t
																																	BgL_arg1973z00_693;
																																{	/* Expand/case.scm 341 */
																																	obj_t
																																		BgL_arg1974z00_694;
																																	BgL_arg1974z00_694
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_dz00_690,
																																		BNIL);
																																	BgL_arg1973z00_693
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(18),
																																		BgL_arg1974z00_694);
																																}
																																BgL_arg1972z00_692
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1973z00_693,
																																	BNIL);
																															}
																															BgL_arg1971z00_691
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(17),
																																BgL_arg1972z00_692);
																														}
																														BgL_arg1970z00_689 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(19),
																															BgL_arg1971z00_691);
																													}
																												}
																												BgL_newtail1088z00_687 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1970z00_689,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1087z00_684,
																												BgL_newtail1088z00_687);
																											{	/* Expand/case.scm 341 */
																												obj_t
																													BgL_arg1969z00_688;
																												BgL_arg1969z00_688 =
																													CDR(((obj_t)
																														BgL_l1084z00_683));
																												{
																													obj_t
																														BgL_tail1087z00_1711;
																													obj_t
																														BgL_l1084z00_1710;
																													BgL_l1084z00_1710 =
																														BgL_arg1969z00_688;
																													BgL_tail1087z00_1711 =
																														BgL_newtail1088z00_687;
																													BgL_tail1087z00_684 =
																														BgL_tail1087z00_1711;
																													BgL_l1084z00_683 =
																														BgL_l1084z00_1710;
																													goto
																														BgL_zc3z04anonymousza31967ze3z87_685;
																												}
																											}
																										}
																								}
																							}
																							BgL_arg1964z00_677 =
																								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																								(BgL_arg1965z00_678, BNIL);
																						}
																						BgL_arg1962z00_675 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(21), BgL_arg1964z00_677);
																					}
																					{	/* Expand/case.scm 343 */
																						obj_t BgL_arg1975z00_696;
																						obj_t BgL_arg1976z00_697;

																						BgL_arg1975z00_696 =
																							BGl_expandzd2prognzd2zz__prognz00
																							(BgL_bodyz00_637);
																						{	/* Expand/case.scm 344 */
																							obj_t BgL_arg1977z00_698;

																							{	/* Expand/case.scm 344 */
																								obj_t BgL_arg1978z00_699;

																								BgL_arg1978z00_699 =
																									CDR(
																									((obj_t) BgL_clausesz00_630));
																								BgL_arg1977z00_698 =
																									BGl_loopze70ze7zzexpand_casez00
																									(BgL_arg1978z00_699);
																							}
																							BgL_arg1976z00_697 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1977z00_698, BNIL);
																						}
																						BgL_arg1963z00_676 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1975z00_696,
																							BgL_arg1976z00_697);
																					}
																					BgL_arg1961z00_674 =
																						MAKE_YOUNG_PAIR(BgL_arg1962z00_675,
																						BgL_arg1963z00_676);
																				}
																				return
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																					BgL_arg1961z00_674);
																			}
																		else
																			{	/* Expand/case.scm 346 */
																				obj_t BgL_arg1979z00_700;

																				{	/* Expand/case.scm 346 */
																					obj_t BgL_arg1980z00_701;
																					obj_t BgL_arg1981z00_702;

																					{	/* Expand/case.scm 346 */
																						obj_t BgL_arg1982z00_703;

																						{	/* Expand/case.scm 346 */
																							obj_t BgL_arg1983z00_704;

																							{	/* Expand/case.scm 346 */
																								obj_t BgL_arg1984z00_705;

																								{	/* Expand/case.scm 346 */
																									obj_t BgL_arg1985z00_706;

																									BgL_arg1985z00_706 =
																										MAKE_YOUNG_PAIR
																										(BgL_datumsz00_636, BNIL);
																									BgL_arg1984z00_705 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(18),
																										BgL_arg1985z00_706);
																								}
																								BgL_arg1983z00_704 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1984z00_705, BNIL);
																							}
																							BgL_arg1982z00_703 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(17), BgL_arg1983z00_704);
																						}
																						BgL_arg1980z00_701 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(22), BgL_arg1982z00_703);
																					}
																					{	/* Expand/case.scm 347 */
																						obj_t BgL_arg1986z00_707;
																						obj_t BgL_arg1987z00_708;

																						BgL_arg1986z00_707 =
																							BGl_expandzd2prognzd2zz__prognz00
																							(BgL_bodyz00_637);
																						{	/* Expand/case.scm 348 */
																							obj_t BgL_arg1988z00_709;

																							{	/* Expand/case.scm 348 */
																								obj_t BgL_arg1989z00_710;

																								BgL_arg1989z00_710 =
																									CDR(
																									((obj_t) BgL_clausesz00_630));
																								BgL_arg1988z00_709 =
																									BGl_loopze70ze7zzexpand_casez00
																									(BgL_arg1989z00_710);
																							}
																							BgL_arg1987z00_708 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1988z00_709, BNIL);
																						}
																						BgL_arg1981z00_702 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1986z00_707,
																							BgL_arg1987z00_708);
																					}
																					BgL_arg1979z00_700 =
																						MAKE_YOUNG_PAIR(BgL_arg1980z00_701,
																						BgL_arg1981z00_702);
																				}
																				return
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																					BgL_arg1979z00_700);
																			}
																	}
															}
														else
															{	/* Expand/case.scm 328 */
																if (NULLP(CDR(((obj_t) BgL_carzd2569zd2_648))))
																	{	/* Expand/case.scm 328 */
																		obj_t BgL_arg1944z00_656;
																		obj_t BgL_arg1945z00_657;

																		BgL_arg1944z00_656 =
																			CAR(((obj_t) BgL_carzd2569zd2_648));
																		BgL_arg1945z00_657 =
																			CDR(BgL_ezd2553zd2_642);
																		BgL_datumz00_639 = BgL_arg1944z00_656;
																		BgL_bodyz00_640 = BgL_arg1945z00_657;
																		if (NULLP(BgL_bodyz00_640))
																			{	/* Expand/case.scm 351 */
																				obj_t BgL_arg1992z00_714;

																				BgL_arg1992z00_714 =
																					CAR(((obj_t) BgL_clausesz00_630));
																				return
																					BGl_errorz00zz__errorz00
																					(BGl_string2006z00zzexpand_casez00,
																					BGl_string2009z00zzexpand_casez00,
																					BgL_arg1992z00_714);
																			}
																		else
																			{	/* Expand/case.scm 352 */
																				obj_t BgL_arg1993z00_715;

																				{	/* Expand/case.scm 352 */
																					obj_t BgL_arg1994z00_716;
																					obj_t BgL_arg1995z00_717;

																					{	/* Expand/case.scm 352 */
																						obj_t BgL_arg1996z00_718;

																						{	/* Expand/case.scm 352 */
																							obj_t BgL_arg1997z00_719;

																							{	/* Expand/case.scm 352 */
																								obj_t BgL_arg1998z00_720;

																								{	/* Expand/case.scm 352 */
																									obj_t BgL_arg1999z00_721;

																									BgL_arg1999z00_721 =
																										MAKE_YOUNG_PAIR
																										(BgL_datumz00_639, BNIL);
																									BgL_arg1998z00_720 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(18),
																										BgL_arg1999z00_721);
																								}
																								BgL_arg1997z00_719 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1998z00_720, BNIL);
																							}
																							BgL_arg1996z00_718 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(17), BgL_arg1997z00_719);
																						}
																						BgL_arg1994z00_716 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(19), BgL_arg1996z00_718);
																					}
																					{	/* Expand/case.scm 353 */
																						obj_t BgL_arg2000z00_722;
																						obj_t BgL_arg2001z00_723;

																						BgL_arg2000z00_722 =
																							BGl_expandzd2prognzd2zz__prognz00
																							(BgL_bodyz00_640);
																						{	/* Expand/case.scm 354 */
																							obj_t BgL_arg2002z00_724;

																							{	/* Expand/case.scm 354 */
																								obj_t BgL_arg2003z00_725;

																								BgL_arg2003z00_725 =
																									CDR(
																									((obj_t) BgL_clausesz00_630));
																								BgL_arg2002z00_724 =
																									BGl_loopze70ze7zzexpand_casez00
																									(BgL_arg2003z00_725);
																							}
																							BgL_arg2001z00_723 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2002z00_724, BNIL);
																						}
																						BgL_arg1995z00_717 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2000z00_722,
																							BgL_arg2001z00_723);
																					}
																					BgL_arg1993z00_715 =
																						MAKE_YOUNG_PAIR(BgL_arg1994z00_716,
																						BgL_arg1995z00_717);
																				}
																				return
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																					BgL_arg1993z00_715);
																			}
																	}
																else
																	{	/* Expand/case.scm 328 */
																		return BFALSE;
																	}
															}
													}
												else
													{	/* Expand/case.scm 328 */
														return BFALSE;
													}
											}
									}
								else
									{	/* Expand/case.scm 328 */
										return BFALSE;
									}
							}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_casez00(void)
	{
		{	/* Expand/case.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_casez00(void)
	{
		{	/* Expand/case.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_casez00(void)
	{
		{	/* Expand/case.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_casez00(void)
	{
		{	/* Expand/case.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2012z00zzexpand_casez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2012z00zzexpand_casez00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2012z00zzexpand_casez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2012z00zzexpand_casez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2012z00zzexpand_casez00));
			return
				BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2012z00zzexpand_casez00));
		}

	}

#ifdef __cplusplus
}
#endif
