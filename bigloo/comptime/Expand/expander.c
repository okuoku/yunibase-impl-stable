/*===========================================================================*/
/*   (Expand/expander.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Expand/expander.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_EXPAND_EXPANDER_TYPE_DEFINITIONS
#define BGL_EXPAND_EXPANDER_TYPE_DEFINITIONS
#endif													// BGL_EXPAND_EXPANDER_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzexpand_expanderz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2Ozd2macrozd2toplevelzd2zzexpand_expanderz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_findzd2Gzd2expanderz00zzexpand_expanderz00(obj_t);
	static obj_t BGl_z62findzd2Gzd2expanderz62zzexpand_expanderz00(obj_t, obj_t);
	static obj_t BGl_za2Genvza2z00zzexpand_expanderz00 = BUNSPEC;
	static obj_t BGl_za2Oenvza2z00zzexpand_expanderz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzexpand_expanderz00(void);
	BGL_IMPORT obj_t BGl_getzd2compilerzd2expanderz00zz__macroz00(obj_t);
	extern obj_t BGl_epairifyz00zztools_miscz00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62checkzd2tozd2bezd2macroszb0zzexpand_expanderz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzexpand_expanderz00(void);
	static obj_t BGl_z62zc3z04anonymousza31036ze3ze5zzexpand_expanderz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzexpand_expanderz00(void);
	BGL_IMPORT bool_t BGl_hashtablezd2removez12zc0zz__hashz00(obj_t, obj_t);
	static obj_t BGl_za2tozd2bezd2macrosza2z00zzexpand_expanderz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t
		BGl_z62installzd2Ozd2comptimezd2expanderzb0zzexpand_expanderz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2optimzd2Ozd2macrozf3za2zf3zzengine_paramz00;
	static obj_t BGl_methodzd2initzd2zzexpand_expanderz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_initializa7ezd2Genvz12z67zzexpand_expanderz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_initializa7ezd2Oenvz12z67zzexpand_expanderz00(void);
	BGL_EXPORTED_DECL obj_t BGl_findzd2Ozd2expanderz00zzexpand_expanderz00(obj_t);
	static obj_t BGl_z62findzd2Ozd2expanderz62zzexpand_expanderz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_unbindzd2Ozd2expanderz12z12zzexpand_expanderz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tozd2bezd2macroz12z12zzexpand_expanderz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_addzd2Ozd2macrozd2toplevelz12zc0zzexpand_expanderz00(obj_t);
	static obj_t BGl_z62unbindzd2Gzd2expanderz12z70zzexpand_expanderz00(obj_t,
		obj_t);
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62initializa7ezd2Genvz12z05zzexpand_expanderz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00(obj_t, obj_t);
	static obj_t BGl_z62initializa7ezd2Oenvz12z05zzexpand_expanderz00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzexpand_expanderz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t
		BGl_z62addzd2Ozd2macrozd2toplevelz12za2zzexpand_expanderz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_checkzd2tozd2bezd2macroszd2zzexpand_expanderz00(void);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zzexpand_expanderz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzexpand_expanderz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzexpand_expanderz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzexpand_expanderz00(void);
	static obj_t
		BGl_z62installzd2Gzd2comptimezd2expanderzb0zzexpand_expanderz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	static obj_t BGl_za2Ozd2macrozd2toplevelza2z00zzexpand_expanderz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_unbindzd2Gzd2expanderz12z12zzexpand_expanderz00(obj_t);
	static obj_t BGl_z62tozd2bezd2macroz12z70zzexpand_expanderz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62unbindzd2Ozd2expanderz12z70zzexpand_expanderz00(obj_t,
		obj_t);
	static obj_t BGl_z62getzd2Ozd2macrozd2toplevelzb0zzexpand_expanderz00(obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1081z00zzexpand_expanderz00,
		BgL_bgl_string1081za700za7za7e1091za7, "install-O-comptime-expander", 27);
	      DEFINE_STRING(BGl_string1082z00zzexpand_expanderz00,
		BgL_bgl_string1082za700za7za7e1092za7,
		"Illegal re-installation of O-expander", 37);
	      DEFINE_STRING(BGl_string1083z00zzexpand_expanderz00,
		BgL_bgl_string1083za700za7za7e1093za7, "install-G-comptime-expander", 27);
	      DEFINE_STRING(BGl_string1084z00zzexpand_expanderz00,
		BgL_bgl_string1084za700za7za7e1094za7,
		"Illegal re-installation of G-expander", 37);
	      DEFINE_STRING(BGl_string1085z00zzexpand_expanderz00,
		BgL_bgl_string1085za700za7za7e1095za7, "expand", 6);
	      DEFINE_STRING(BGl_string1086z00zzexpand_expanderz00,
		BgL_bgl_string1086za700za7za7e1096za7, "Can't find syntax definition", 28);
	      DEFINE_STRING(BGl_string1087z00zzexpand_expanderz00,
		BgL_bgl_string1087za700za7za7e1097za7, "Can't find macro definition", 27);
	      DEFINE_STRING(BGl_string1088z00zzexpand_expanderz00,
		BgL_bgl_string1088za700za7za7e1098za7, "expand_expander", 15);
	      DEFINE_STRING(BGl_string1089z00zzexpand_expanderz00,
		BgL_bgl_string1089za700za7za7e1099za7, "syntax expander ", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2Ozd2macrozd2toplevelz12zd2envz12zzexpand_expanderz00,
		BgL_bgl_za762addza7d2oza7d2mac1100za7,
		BGl_z62addzd2Ozd2macrozd2toplevelz12za2zzexpand_expanderz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_checkzd2tozd2bezd2macroszd2envz00zzexpand_expanderz00,
		BgL_bgl_za762checkza7d2toza7d21101za7,
		BGl_z62checkzd2tozd2bezd2macroszb0zzexpand_expanderz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2Gzd2expanderzd2envzd2zzexpand_expanderz00,
		BgL_bgl_za762findza7d2gza7d2ex1102za7,
		BGl_z62findzd2Gzd2expanderz62zzexpand_expanderz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_installzd2Ozd2comptimezd2expanderzd2envz00zzexpand_expanderz00,
		BgL_bgl_za762installza7d2oza7d1103za7,
		BGl_z62installzd2Ozd2comptimezd2expanderzb0zzexpand_expanderz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tozd2bezd2macroz12zd2envzc0zzexpand_expanderz00,
		BgL_bgl_za762toza7d2beza7d2mac1104za7,
		BGl_z62tozd2bezd2macroz12z70zzexpand_expanderz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initializa7ezd2Genvz12zd2envzb5zzexpand_expanderz00,
		BgL_bgl_za762initializa7a7eza71105za7,
		BGl_z62initializa7ezd2Genvz12z05zzexpand_expanderz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initializa7ezd2Oenvz12zd2envzb5zzexpand_expanderz00,
		BgL_bgl_za762initializa7a7eza71106za7,
		BGl_z62initializa7ezd2Oenvz12z05zzexpand_expanderz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2Ozd2macrozd2toplevelzd2envz00zzexpand_expanderz00,
		BgL_bgl_za762getza7d2oza7d2mac1107za7,
		BGl_z62getzd2Ozd2macrozd2toplevelzb0zzexpand_expanderz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2Ozd2expanderzd2envzd2zzexpand_expanderz00,
		BgL_bgl_za762findza7d2oza7d2ex1108za7,
		BGl_z62findzd2Ozd2expanderz62zzexpand_expanderz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unbindzd2Ozd2expanderz12zd2envzc0zzexpand_expanderz00,
		BgL_bgl_za762unbindza7d2oza7d21109za7,
		BGl_z62unbindzd2Ozd2expanderz12z70zzexpand_expanderz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_installzd2Gzd2comptimezd2expanderzd2envz00zzexpand_expanderz00,
		BgL_bgl_za762installza7d2gza7d1110za7,
		BGl_z62installzd2Gzd2comptimezd2expanderzb0zzexpand_expanderz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unbindzd2Gzd2expanderz12zd2envzc0zzexpand_expanderz00,
		BgL_bgl_za762unbindza7d2gza7d21111za7,
		BGl_z62unbindzd2Gzd2expanderz12z70zzexpand_expanderz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzexpand_expanderz00));
		     ADD_ROOT((void *) (&BGl_za2Genvza2z00zzexpand_expanderz00));
		     ADD_ROOT((void *) (&BGl_za2Oenvza2z00zzexpand_expanderz00));
		   
			 ADD_ROOT((void *) (&BGl_za2tozd2bezd2macrosza2z00zzexpand_expanderz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2Ozd2macrozd2toplevelza2z00zzexpand_expanderz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzexpand_expanderz00(long
		BgL_checksumz00_186, char *BgL_fromz00_187)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzexpand_expanderz00))
				{
					BGl_requirezd2initializa7ationz75zzexpand_expanderz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzexpand_expanderz00();
					BGl_libraryzd2moduleszd2initz00zzexpand_expanderz00();
					BGl_cnstzd2initzd2zzexpand_expanderz00();
					BGl_importedzd2moduleszd2initz00zzexpand_expanderz00();
					return BGl_toplevelzd2initzd2zzexpand_expanderz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 15 */
			BGl_modulezd2initializa7ationz75zz__macroz00(0L, "expand_expander");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"expand_expander");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "expand_expander");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "expand_expander");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"expand_expander");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "expand_expander");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"expand_expander");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"expand_expander");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"expand_expander");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "expand_expander");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "expand_expander");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 15 */
			{	/* Expand/expander.scm 15 */
				obj_t BgL_cportz00_174;

				{	/* Expand/expander.scm 15 */
					obj_t BgL_stringz00_181;

					BgL_stringz00_181 = BGl_string1089z00zzexpand_expanderz00;
					{	/* Expand/expander.scm 15 */
						obj_t BgL_startz00_182;

						BgL_startz00_182 = BINT(0L);
						{	/* Expand/expander.scm 15 */
							obj_t BgL_endz00_183;

							BgL_endz00_183 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_181)));
							{	/* Expand/expander.scm 15 */

								BgL_cportz00_174 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_181, BgL_startz00_182, BgL_endz00_183);
				}}}}
				{
					long BgL_iz00_175;

					BgL_iz00_175 = 1L;
				BgL_loopz00_176:
					if ((BgL_iz00_175 == -1L))
						{	/* Expand/expander.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Expand/expander.scm 15 */
							{	/* Expand/expander.scm 15 */
								obj_t BgL_arg1090z00_177;

								{	/* Expand/expander.scm 15 */

									{	/* Expand/expander.scm 15 */
										obj_t BgL_locationz00_179;

										BgL_locationz00_179 = BBOOL(((bool_t) 0));
										{	/* Expand/expander.scm 15 */

											BgL_arg1090z00_177 =
												BGl_readz00zz__readerz00(BgL_cportz00_174,
												BgL_locationz00_179);
										}
									}
								}
								{	/* Expand/expander.scm 15 */
									int BgL_tmpz00_216;

									BgL_tmpz00_216 = (int) (BgL_iz00_175);
									CNST_TABLE_SET(BgL_tmpz00_216, BgL_arg1090z00_177);
							}}
							{	/* Expand/expander.scm 15 */
								int BgL_auxz00_180;

								BgL_auxz00_180 = (int) ((BgL_iz00_175 - 1L));
								{
									long BgL_iz00_221;

									BgL_iz00_221 = (long) (BgL_auxz00_180);
									BgL_iz00_175 = BgL_iz00_221;
									goto BgL_loopz00_176;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 15 */
			BGl_za2Oenvza2z00zzexpand_expanderz00 = BNIL;
			BGl_za2Ozd2macrozd2toplevelza2z00zzexpand_expanderz00 = BNIL;
			BGl_za2Genvza2z00zzexpand_expanderz00 = BNIL;
			return (BGl_za2tozd2bezd2macrosza2z00zzexpand_expanderz00 =
				BNIL, BUNSPEC);
		}

	}



/* initialize-Oenv! */
	BGL_EXPORTED_DEF obj_t BGl_initializa7ezd2Oenvz12z67zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 41 */
			return (BGl_za2Oenvza2z00zzexpand_expanderz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL), BUNSPEC);
		}

	}



/* &initialize-Oenv! */
	obj_t BGl_z62initializa7ezd2Oenvz12z05zzexpand_expanderz00(obj_t
		BgL_envz00_145)
	{
		{	/* Expand/expander.scm 41 */
			return BGl_initializa7ezd2Oenvz12z67zzexpand_expanderz00();
		}

	}



/* add-O-macro-toplevel! */
	BGL_EXPORTED_DEF obj_t
		BGl_addzd2Ozd2macrozd2toplevelz12zc0zzexpand_expanderz00(obj_t
		BgL_expz00_13)
	{
		{	/* Expand/expander.scm 52 */
			return (BGl_za2Ozd2macrozd2toplevelza2z00zzexpand_expanderz00 =
				MAKE_YOUNG_PAIR(BgL_expz00_13,
					BGl_za2Ozd2macrozd2toplevelza2z00zzexpand_expanderz00), BUNSPEC);
		}

	}



/* &add-O-macro-toplevel! */
	obj_t BGl_z62addzd2Ozd2macrozd2toplevelz12za2zzexpand_expanderz00(obj_t
		BgL_envz00_146, obj_t BgL_expz00_147)
	{
		{	/* Expand/expander.scm 52 */
			return
				BGl_addzd2Ozd2macrozd2toplevelz12zc0zzexpand_expanderz00
				(BgL_expz00_147);
		}

	}



/* get-O-macro-toplevel */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2Ozd2macrozd2toplevelzd2zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 58 */
			{	/* Expand/expander.scm 59 */
				obj_t BgL_vz00_44;

				BgL_vz00_44 = BGl_za2Ozd2macrozd2toplevelza2z00zzexpand_expanderz00;
				BGl_za2Ozd2macrozd2toplevelza2z00zzexpand_expanderz00 = BNIL;
				return BgL_vz00_44;
			}
		}

	}



/* &get-O-macro-toplevel */
	obj_t BGl_z62getzd2Ozd2macrozd2toplevelzb0zzexpand_expanderz00(obj_t
		BgL_envz00_148)
	{
		{	/* Expand/expander.scm 58 */
			return BGl_getzd2Ozd2macrozd2toplevelzd2zzexpand_expanderz00();
		}

	}



/* install-O-comptime-expander */
	BGL_EXPORTED_DEF obj_t
		BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00(obj_t
		BgL_keywordz00_14, obj_t BgL_functionz00_15)
	{
		{	/* Expand/expander.scm 70 */
			{	/* Expand/expander.scm 71 */
				bool_t BgL_test1114z00_229;

				{	/* Expand/expander.scm 71 */
					obj_t BgL_arg1038z00_55;

					{	/* Expand/expander.scm 90 */
						obj_t BgL__andtest_1012z00_115;

						BgL__andtest_1012z00_115 =
							BGl_za2optimzd2Ozd2macrozf3za2zf3zzengine_paramz00;
						if (CBOOL(BgL__andtest_1012z00_115))
							{	/* Expand/expander.scm 90 */
								BgL_arg1038z00_55 =
									BGl_hashtablezd2getzd2zz__hashz00
									(BGl_za2Oenvza2z00zzexpand_expanderz00, BgL_keywordz00_14);
							}
						else
							{	/* Expand/expander.scm 90 */
								BgL_arg1038z00_55 = BFALSE;
							}
					}
					if (STRUCTP(BgL_arg1038z00_55))
						{	/* Expand/expander.scm 71 */
							BgL_test1114z00_229 =
								(STRUCT_KEY(BgL_arg1038z00_55) == CNST_TABLE_REF(0));
						}
					else
						{	/* Expand/expander.scm 71 */
							BgL_test1114z00_229 = ((bool_t) 0);
						}
				}
				if (BgL_test1114z00_229)
					{	/* Expand/expander.scm 71 */
						return
							BGl_internalzd2errorzd2zztools_errorz00
							(BGl_string1081z00zzexpand_expanderz00,
							BGl_string1082z00zzexpand_expanderz00, BgL_keywordz00_14);
					}
				else
					{	/* Expand/expander.scm 75 */
						obj_t BgL_newz00_47;

						{	/* Expand/expander.scm 76 */
							obj_t BgL_zc3z04anonymousza31036ze3z87_149;

							BgL_zc3z04anonymousza31036ze3z87_149 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31036ze3ze5zzexpand_expanderz00,
								(int) (2L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31036ze3z87_149, (int) (0L),
								BgL_functionz00_15);
							{	/* Expand/expander.scm 75 */
								obj_t BgL_newz00_120;

								BgL_newz00_120 = create_struct(CNST_TABLE_REF(0), (int) (2L));
								{	/* Expand/expander.scm 75 */
									int BgL_tmpz00_247;

									BgL_tmpz00_247 = (int) (1L);
									STRUCT_SET(BgL_newz00_120, BgL_tmpz00_247,
										BgL_zc3z04anonymousza31036ze3z87_149);
								}
								{	/* Expand/expander.scm 75 */
									int BgL_tmpz00_250;

									BgL_tmpz00_250 = (int) (0L);
									STRUCT_SET(BgL_newz00_120, BgL_tmpz00_250, BgL_keywordz00_14);
								}
								BgL_newz00_47 = BgL_newz00_120;
						}}
						BGl_hashtablezd2putz12zc0zz__hashz00
							(BGl_za2Oenvza2z00zzexpand_expanderz00, BgL_keywordz00_14,
							BgL_newz00_47);
						return BgL_newz00_47;
					}
			}
		}

	}



/* &install-O-comptime-expander */
	obj_t BGl_z62installzd2Ozd2comptimezd2expanderzb0zzexpand_expanderz00(obj_t
		BgL_envz00_150, obj_t BgL_keywordz00_151, obj_t BgL_functionz00_152)
	{
		{	/* Expand/expander.scm 70 */
			return
				BGl_installzd2Ozd2comptimezd2expanderzd2zzexpand_expanderz00
				(BgL_keywordz00_151, BgL_functionz00_152);
		}

	}



/* &<@anonymous:1036> */
	obj_t BGl_z62zc3z04anonymousza31036ze3ze5zzexpand_expanderz00(obj_t
		BgL_envz00_153, obj_t BgL_xz00_155, obj_t BgL_ez00_156)
	{
		{	/* Expand/expander.scm 75 */
			{	/* Expand/expander.scm 76 */
				obj_t BgL_functionz00_154;

				BgL_functionz00_154 =
					((obj_t) PROCEDURE_REF(BgL_envz00_153, (int) (0L)));
				{	/* Expand/expander.scm 76 */
					obj_t BgL_newxz00_185;

					BgL_newxz00_185 =
						BGL_PROCEDURE_CALL2(BgL_functionz00_154, BgL_xz00_155,
						BgL_ez00_156);
					if (PAIRP(BgL_newxz00_185))
						{	/* Expand/expander.scm 77 */
							return
								BGl_epairifyz00zztools_miscz00(BgL_newxz00_185, BgL_xz00_155);
						}
					else
						{	/* Expand/expander.scm 77 */
							return BgL_newxz00_185;
						}
				}
			}
		}

	}



/* find-O-expander */
	BGL_EXPORTED_DEF obj_t BGl_findzd2Ozd2expanderz00zzexpand_expanderz00(obj_t
		BgL_symbolz00_16)
	{
		{	/* Expand/expander.scm 89 */
			{	/* Expand/expander.scm 90 */
				obj_t BgL__andtest_1012z00_123;

				BgL__andtest_1012z00_123 =
					BGl_za2optimzd2Ozd2macrozf3za2zf3zzengine_paramz00;
				if (CBOOL(BgL__andtest_1012z00_123))
					{	/* Expand/expander.scm 90 */
						return
							BGl_hashtablezd2getzd2zz__hashz00
							(BGl_za2Oenvza2z00zzexpand_expanderz00, BgL_symbolz00_16);
					}
				else
					{	/* Expand/expander.scm 90 */
						return BFALSE;
					}
			}
		}

	}



/* &find-O-expander */
	obj_t BGl_z62findzd2Ozd2expanderz62zzexpand_expanderz00(obj_t BgL_envz00_157,
		obj_t BgL_symbolz00_158)
	{
		{	/* Expand/expander.scm 89 */
			return BGl_findzd2Ozd2expanderz00zzexpand_expanderz00(BgL_symbolz00_158);
		}

	}



/* unbind-O-expander! */
	BGL_EXPORTED_DEF obj_t
		BGl_unbindzd2Ozd2expanderz12z12zzexpand_expanderz00(obj_t BgL_symbolz00_17)
	{
		{	/* Expand/expander.scm 95 */
			return
				BBOOL(BGl_hashtablezd2removez12zc0zz__hashz00
				(BGl_za2Oenvza2z00zzexpand_expanderz00, BgL_symbolz00_17));
		}

	}



/* &unbind-O-expander! */
	obj_t BGl_z62unbindzd2Ozd2expanderz12z70zzexpand_expanderz00(obj_t
		BgL_envz00_159, obj_t BgL_symbolz00_160)
	{
		{	/* Expand/expander.scm 95 */
			return
				BGl_unbindzd2Ozd2expanderz12z12zzexpand_expanderz00(BgL_symbolz00_160);
		}

	}



/* initialize-Genv! */
	BGL_EXPORTED_DEF obj_t BGl_initializa7ezd2Genvz12z67zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 106 */
			return (BGl_za2Genvza2z00zzexpand_expanderz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL), BUNSPEC);
		}

	}



/* &initialize-Genv! */
	obj_t BGl_z62initializa7ezd2Genvz12z05zzexpand_expanderz00(obj_t
		BgL_envz00_161)
	{
		{	/* Expand/expander.scm 106 */
			return BGl_initializa7ezd2Genvz12z67zzexpand_expanderz00();
		}

	}



/* install-G-comptime-expander */
	BGL_EXPORTED_DEF obj_t
		BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00(obj_t
		BgL_keywordz00_18, obj_t BgL_functionz00_19)
	{
		{	/* Expand/expander.scm 116 */
			{	/* Expand/expander.scm 117 */
				bool_t BgL_test1119z00_275;

				{	/* Expand/expander.scm 117 */
					obj_t BgL_arg1042z00_61;

					BgL_arg1042z00_61 =
						BGl_findzd2Gzd2expanderz00zzexpand_expanderz00(BgL_keywordz00_18);
					if (STRUCTP(BgL_arg1042z00_61))
						{	/* Expand/expander.scm 117 */
							BgL_test1119z00_275 =
								(STRUCT_KEY(BgL_arg1042z00_61) == CNST_TABLE_REF(0));
						}
					else
						{	/* Expand/expander.scm 117 */
							BgL_test1119z00_275 = ((bool_t) 0);
						}
				}
				if (BgL_test1119z00_275)
					{	/* Expand/expander.scm 117 */
						return
							BGl_internalzd2errorzd2zztools_errorz00
							(BGl_string1083z00zzexpand_expanderz00,
							BGl_string1084z00zzexpand_expanderz00, BgL_keywordz00_18);
					}
				else
					{	/* Expand/expander.scm 121 */
						obj_t BgL_newz00_60;

						{	/* Expand/expander.scm 121 */
							obj_t BgL_newz00_128;

							BgL_newz00_128 = create_struct(CNST_TABLE_REF(0), (int) (2L));
							{	/* Expand/expander.scm 121 */
								int BgL_tmpz00_286;

								BgL_tmpz00_286 = (int) (1L);
								STRUCT_SET(BgL_newz00_128, BgL_tmpz00_286, BgL_functionz00_19);
							}
							{	/* Expand/expander.scm 121 */
								int BgL_tmpz00_289;

								BgL_tmpz00_289 = (int) (0L);
								STRUCT_SET(BgL_newz00_128, BgL_tmpz00_289, BgL_keywordz00_18);
							}
							BgL_newz00_60 = BgL_newz00_128;
						}
						BGl_hashtablezd2putz12zc0zz__hashz00
							(BGl_za2Genvza2z00zzexpand_expanderz00, BgL_keywordz00_18,
							BgL_newz00_60);
						return BgL_newz00_60;
					}
			}
		}

	}



/* &install-G-comptime-expander */
	obj_t BGl_z62installzd2Gzd2comptimezd2expanderzb0zzexpand_expanderz00(obj_t
		BgL_envz00_162, obj_t BgL_keywordz00_163, obj_t BgL_functionz00_164)
	{
		{	/* Expand/expander.scm 116 */
			return
				BGl_installzd2Gzd2comptimezd2expanderzd2zzexpand_expanderz00
				(BgL_keywordz00_163, BgL_functionz00_164);
		}

	}



/* find-G-expander */
	BGL_EXPORTED_DEF obj_t BGl_findzd2Gzd2expanderz00zzexpand_expanderz00(obj_t
		BgL_symbolz00_20)
	{
		{	/* Expand/expander.scm 131 */
			if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
				(BGl_za2compilerzd2debugza2zd2zzengine_paramz00))
				{	/* Expand/expander.scm 132 */
					bool_t BgL_test1122z00_296;

					if (INTEGERP(BGl_za2compilerzd2debugza2zd2zzengine_paramz00))
						{	/* Expand/expander.scm 132 */
							BgL_test1122z00_296 =
								(
								(long) CINT(BGl_za2compilerzd2debugza2zd2zzengine_paramz00) >=
								1L);
						}
					else
						{	/* Expand/expander.scm 132 */
							BgL_test1122z00_296 =
								BGl_2ze3zd3z30zz__r4_numbers_6_5z00
								(BGl_za2compilerzd2debugza2zd2zzengine_paramz00, BINT(1L));
						}
					if (BgL_test1122z00_296)
						{	/* Expand/expander.scm 132 */
							return
								BGl_hashtablezd2getzd2zz__hashz00
								(BGl_za2Genvza2z00zzexpand_expanderz00, BgL_symbolz00_20);
						}
					else
						{	/* Expand/expander.scm 132 */
							return BFALSE;
						}
				}
			else
				{	/* Expand/expander.scm 132 */
					return BFALSE;
				}
		}

	}



/* &find-G-expander */
	obj_t BGl_z62findzd2Gzd2expanderz62zzexpand_expanderz00(obj_t BgL_envz00_165,
		obj_t BgL_symbolz00_166)
	{
		{	/* Expand/expander.scm 131 */
			return BGl_findzd2Gzd2expanderz00zzexpand_expanderz00(BgL_symbolz00_166);
		}

	}



/* unbind-G-expander! */
	BGL_EXPORTED_DEF obj_t
		BGl_unbindzd2Gzd2expanderz12z12zzexpand_expanderz00(obj_t BgL_symbolz00_21)
	{
		{	/* Expand/expander.scm 138 */
			return
				BBOOL(BGl_hashtablezd2removez12zc0zz__hashz00
				(BGl_za2Genvza2z00zzexpand_expanderz00, BgL_symbolz00_21));
		}

	}



/* &unbind-G-expander! */
	obj_t BGl_z62unbindzd2Gzd2expanderz12z70zzexpand_expanderz00(obj_t
		BgL_envz00_167, obj_t BgL_symbolz00_168)
	{
		{	/* Expand/expander.scm 138 */
			return
				BGl_unbindzd2Gzd2expanderz12z12zzexpand_expanderz00(BgL_symbolz00_168);
		}

	}



/* to-be-macro! */
	BGL_EXPORTED_DEF obj_t BGl_tozd2bezd2macroz12z12zzexpand_expanderz00(obj_t
		BgL_idz00_22, obj_t BgL_srcz00_23)
	{
		{	/* Expand/expander.scm 149 */
			if (CBOOL(BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_22,
						BGl_za2tozd2bezd2macrosza2z00zzexpand_expanderz00)))
				{	/* Expand/expander.scm 150 */
					return BFALSE;
				}
			else
				{	/* Expand/expander.scm 151 */
					obj_t BgL_arg1044z00_65;

					BgL_arg1044z00_65 = MAKE_YOUNG_PAIR(BgL_idz00_22, BgL_srcz00_23);
					return (BGl_za2tozd2bezd2macrosza2z00zzexpand_expanderz00 =
						MAKE_YOUNG_PAIR(BgL_arg1044z00_65,
							BGl_za2tozd2bezd2macrosza2z00zzexpand_expanderz00), BUNSPEC);
				}
		}

	}



/* &to-be-macro! */
	obj_t BGl_z62tozd2bezd2macroz12z70zzexpand_expanderz00(obj_t BgL_envz00_169,
		obj_t BgL_idz00_170, obj_t BgL_srcz00_171)
	{
		{	/* Expand/expander.scm 149 */
			return
				BGl_tozd2bezd2macroz12z12zzexpand_expanderz00(BgL_idz00_170,
				BgL_srcz00_171);
		}

	}



/* check-to-be-macros */
	BGL_EXPORTED_DEF obj_t
		BGl_checkzd2tozd2bezd2macroszd2zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 156 */
			{
				obj_t BgL_l1015z00_67;

				{	/* Expand/expander.scm 157 */
					bool_t BgL_tmpz00_314;

					BgL_l1015z00_67 = BGl_za2tozd2bezd2macrosza2z00zzexpand_expanderz00;
				BgL_zc3z04anonymousza31045ze3z87_68:
					if (PAIRP(BgL_l1015z00_67))
						{	/* Expand/expander.scm 157 */
							{	/* Expand/expander.scm 158 */
								obj_t BgL_mz00_70;

								BgL_mz00_70 = CAR(BgL_l1015z00_67);
								{
									obj_t BgL_idz00_73;
									obj_t BgL_idz00_71;

									{	/* Expand/expander.scm 158 */
										obj_t BgL_ezd2104zd2_76;

										BgL_ezd2104zd2_76 = CDR(((obj_t) BgL_mz00_70));
										if (PAIRP(BgL_ezd2104zd2_76))
											{	/* Expand/expander.scm 158 */
												obj_t BgL_cdrzd2108zd2_78;

												BgL_cdrzd2108zd2_78 = CDR(BgL_ezd2104zd2_76);
												if ((CAR(BgL_ezd2104zd2_76) == CNST_TABLE_REF(1)))
													{	/* Expand/expander.scm 158 */
														if (PAIRP(BgL_cdrzd2108zd2_78))
															{	/* Expand/expander.scm 158 */
																if (NULLP(CDR(BgL_cdrzd2108zd2_78)))
																	{	/* Expand/expander.scm 158 */
																		BgL_idz00_71 = CAR(BgL_cdrzd2108zd2_78);
																		if (CBOOL
																			(BGl_getzd2compilerzd2expanderz00zz__macroz00
																				(BgL_idz00_71)))
																			{	/* Expand/expander.scm 160 */
																				BFALSE;
																			}
																		else
																			{	/* Expand/expander.scm 160 */
																				BGl_errorz00zz__errorz00
																					(BGl_string1085z00zzexpand_expanderz00,
																					BGl_string1086z00zzexpand_expanderz00,
																					BgL_idz00_71);
																			}
																	}
																else
																	{	/* Expand/expander.scm 158 */
																	BgL_tagzd2103zd2_75:
																		{	/* Expand/expander.scm 170 */
																			bool_t BgL_test1131z00_337;

																			{	/* Expand/expander.scm 170 */
																				obj_t BgL_arg1075z00_101;

																				BgL_arg1075z00_101 =
																					CAR(((obj_t) BgL_mz00_70));
																				BgL_test1131z00_337 =
																					CBOOL
																					(BGl_getzd2compilerzd2expanderz00zz__macroz00
																					(BgL_arg1075z00_101));
																			}
																			if (BgL_test1131z00_337)
																				{	/* Expand/expander.scm 170 */
																					BFALSE;
																				}
																			else
																				{	/* Expand/expander.scm 173 */
																					obj_t BgL_arg1074z00_100;

																					BgL_arg1074z00_100 =
																						CDR(((obj_t) BgL_mz00_70));
																					BGl_errorz00zz__errorz00
																						(BGl_string1085z00zzexpand_expanderz00,
																						BGl_string1087z00zzexpand_expanderz00,
																						BgL_arg1074z00_100);
																				}
																		}
																	}
															}
														else
															{	/* Expand/expander.scm 158 */
																goto BgL_tagzd2103zd2_75;
															}
													}
												else
													{	/* Expand/expander.scm 158 */
														if ((CAR(BgL_ezd2104zd2_76) == CNST_TABLE_REF(0)))
															{	/* Expand/expander.scm 158 */
																if (PAIRP(BgL_cdrzd2108zd2_78))
																	{	/* Expand/expander.scm 158 */
																		if (NULLP(CDR(BgL_cdrzd2108zd2_78)))
																			{	/* Expand/expander.scm 158 */
																				BgL_idz00_73 = CAR(BgL_cdrzd2108zd2_78);
																				if (CBOOL
																					(BGl_getzd2compilerzd2expanderz00zz__macroz00
																						(BgL_idz00_73)))
																					{	/* Expand/expander.scm 165 */
																						BFALSE;
																					}
																				else
																					{	/* Expand/expander.scm 165 */
																						BGl_errorz00zz__errorz00
																							(BGl_string1085z00zzexpand_expanderz00,
																							BGl_string1086z00zzexpand_expanderz00,
																							BgL_idz00_73);
																					}
																			}
																		else
																			{	/* Expand/expander.scm 158 */
																				goto BgL_tagzd2103zd2_75;
																			}
																	}
																else
																	{	/* Expand/expander.scm 158 */
																		goto BgL_tagzd2103zd2_75;
																	}
															}
														else
															{	/* Expand/expander.scm 158 */
																goto BgL_tagzd2103zd2_75;
															}
													}
											}
										else
											{	/* Expand/expander.scm 158 */
												goto BgL_tagzd2103zd2_75;
											}
									}
								}
							}
							{
								obj_t BgL_l1015z00_359;

								BgL_l1015z00_359 = CDR(BgL_l1015z00_67);
								BgL_l1015z00_67 = BgL_l1015z00_359;
								goto BgL_zc3z04anonymousza31045ze3z87_68;
							}
						}
					else
						{	/* Expand/expander.scm 157 */
							BgL_tmpz00_314 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_314);
				}
			}
		}

	}



/* &check-to-be-macros */
	obj_t BGl_z62checkzd2tozd2bezd2macroszb0zzexpand_expanderz00(obj_t
		BgL_envz00_172)
	{
		{	/* Expand/expander.scm 156 */
			return BGl_checkzd2tozd2bezd2macroszd2zzexpand_expanderz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzexpand_expanderz00(void)
	{
		{	/* Expand/expander.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1088z00zzexpand_expanderz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1088z00zzexpand_expanderz00));
			return
				BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1088z00zzexpand_expanderz00));
		}

	}

#ifdef __cplusplus
}
#endif
