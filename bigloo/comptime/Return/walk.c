/*===========================================================================*/
/*   (Return/walk.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Return/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_RETURN_WALK_TYPE_DEFINITIONS
#define BGL_RETURN_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;


#endif													// BGL_RETURN_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt
		BGl_z62returnzd2gotozd2funsz12zd2le1342za2zzreturn_walkz00(obj_t, obj_t);
	static obj_t BGl_z62returnzd2walkz12za2zzreturn_walkz00(obj_t, obj_t);
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzreturn_walkz00 = BUNSPEC;
	static obj_t BGl_step6bze70ze7zzreturn_walkz00(BgL_nodez00_bglt,
		BgL_localz00_bglt, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_z62iszd2returnzf31327z43zzreturn_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62clearzd2returnzd2cachez12z70zzreturn_walkz00(obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_iszd2returnzf3z21zzreturn_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t);
	static obj_t BGl_za2popzd2exitz12za2zc0zzreturn_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_step6ze70ze7zzreturn_walkz00(BgL_nodez00_bglt,
		BgL_localz00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzreturn_walkz00(void);
	BGL_IMPORT obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_returnzd2funz12zc0zzreturn_walkz00(obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_z62iszd2returnzf3z43zzreturn_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_za2envzd2pushzd2exitz12za2z12zzreturn_walkz00 = BUNSPEC;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_iszd2unwindzd2untilzf3zf3zzreturn_walkz00(BgL_variablez00_bglt);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzreturn_walkz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static BgL_nodez00_bglt BGl_z62returnz12zd2app1338za2zzreturn_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzreturn_walkz00(void);
	static BgL_nodez00_bglt BGl_returnz12z12zzreturn_walkz00(BgL_nodez00_bglt,
		BgL_localz00_bglt, BgL_retblockz00_bglt);
	static obj_t BGl_z62iszd2returnzf3zd2var1330z91zzreturn_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_za2envzd2popzd2exitz12za2z12zzreturn_walkz00 = BUNSPEC;
	static obj_t BGl_step8ze70ze7zzreturn_walkz00(BgL_nodez00_bglt,
		BgL_localz00_bglt);
	static obj_t BGl_z62returnzd2gotozd2funsz121339z70zzreturn_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62returnzd2gotozd2funsz12z70zzreturn_walkz00(obj_t, obj_t);
	static obj_t BGl_z62iszd2getzd2exitdzd2topzf3z43zzreturn_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2pushzd2exitz12za2zc0zzreturn_walkz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zzreturn_walkz00(void);
	BGL_EXPORTED_DECL bool_t
		BGl_iszd2exitzd2returnzf3zf3zzreturn_walkz00(BgL_nodez00_bglt,
		BgL_localz00_bglt);
	static obj_t BGl_z62iszd2exitzd2returnzf3z91zzreturn_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62iszd2unwindzd2untilzf3z91zzreturn_walkz00(obj_t, obj_t);
	static obj_t BGl_z62iszd2returnzf3zd2app1334z91zzreturn_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_za2currentzd2dynamiczd2envza2z00zzreturn_walkz00 = BUNSPEC;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_functionzd2exitzd2nodez00zzreturn_walkz00(BgL_nodez00_bglt);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62abortz62zzreturn_walkz00(obj_t, obj_t);
	static obj_t BGl_z62initzd2returnzd2cachez12z70zzreturn_walkz00(obj_t);
	static obj_t BGl_za2envzd2getzd2exitdzd2topza2zd2zzreturn_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_returnzd2walkz12zc0zzreturn_walkz00(obj_t);
	extern obj_t BGl_walk2z00zzast_walkz00(BgL_nodez00_bglt, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzreturn_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_z62iszd2returnzf3zd2letzd2var1332z43zzreturn_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	static obj_t BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00 = BUNSPEC;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zzreturn_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzreturn_walkz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzreturn_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzreturn_walkz00(void);
	extern BgL_nodez00_bglt BGl_walk0z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t);
	static BgL_nodez00_bglt BGl_z62returnz12z70zzreturn_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_walk2z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62returnz121335z70zzreturn_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2unwindzd2untilz12za2zc0zzreturn_walkz00 = BUNSPEC;
	static obj_t BGl_zc3z04exitza31982ze3ze70z60zzreturn_walkz00(BgL_nodez00_bglt,
		BgL_localz00_bglt);
	static obj_t BGl_za2exitdzd2protectzd2setz12za2z12zzreturn_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_returnzd2gotozd2funsz12z12zzreturn_walkz00(BgL_nodez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_iszd2getzd2exitdzd2topzf3z21zzreturn_walkz00(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_initzd2returnzd2cachez12z12zzreturn_walkz00(void);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[15];


	   
		 
		DEFINE_STRING(BGl_string2200z00zzreturn_walkz00,
		BgL_bgl_string2200za700za7za7r2221za7, "s", 1);
	      DEFINE_STRING(BGl_string2201z00zzreturn_walkz00,
		BgL_bgl_string2201za700za7za7r2222za7, "", 0);
	      DEFINE_STRING(BGl_string2202z00zzreturn_walkz00,
		BgL_bgl_string2202za700za7za7r2223za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string2203z00zzreturn_walkz00,
		BgL_bgl_string2203za700za7za7r2224za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string2205z00zzreturn_walkz00,
		BgL_bgl_string2205za700za7za7r2225za7, "is-return?1327", 14);
	      DEFINE_STRING(BGl_string2207z00zzreturn_walkz00,
		BgL_bgl_string2207za700za7za7r2226za7, "return!1335", 11);
	      DEFINE_STRING(BGl_string2209z00zzreturn_walkz00,
		BgL_bgl_string2209za700za7za7r2227za7, "return-goto-funs!1339", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2204z00zzreturn_walkz00,
		BgL_bgl_za762isza7d2returnza7f2228za7,
		BGl_z62iszd2returnzf31327z43zzreturn_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2211z00zzreturn_walkz00,
		BgL_bgl_string2211za700za7za7r2229za7, "is-return?", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2206z00zzreturn_walkz00,
		BgL_bgl_za762returnza71213352230z00,
		BGl_z62returnz121335z70zzreturn_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2208z00zzreturn_walkz00,
		BgL_bgl_za762returnza7d2goto2231z00,
		BGl_z62returnzd2gotozd2funsz121339z70zzreturn_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2215z00zzreturn_walkz00,
		BgL_bgl_string2215za700za7za7r2232za7, "return!::node", 13);
	      DEFINE_STRING(BGl_string2217z00zzreturn_walkz00,
		BgL_bgl_string2217za700za7za7r2233za7, "return-goto-funs!::node", 23);
	      DEFINE_STRING(BGl_string2218z00zzreturn_walkz00,
		BgL_bgl_string2218za700za7za7r2234za7, "return_walk", 11);
	      DEFINE_STRING(BGl_string2219z00zzreturn_walkz00,
		BgL_bgl_string2219za700za7za7r2235za7,
		"sfun $env-push-exit! push-exit! $env-pop-exit! pop-exit! unwind-stack-until! __bexit $env-get-exitd-top $exitd-protect-set! $get-exitd-top $current-dynamic-env foreign (clear-return-cache!) pass-started (init-return-cache!) ",
		224);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_clearzd2returnzd2cachez12zd2envzc0zzreturn_walkz00,
		BgL_bgl_za762clearza7d2retur2236z00,
		BGl_z62clearzd2returnzd2cachez12z70zzreturn_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2210z00zzreturn_walkz00,
		BgL_bgl_za762isza7d2returnza7f2237za7,
		BGl_z62iszd2returnzf3zd2var1330z91zzreturn_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2212z00zzreturn_walkz00,
		BgL_bgl_za762isza7d2returnza7f2238za7,
		BGl_z62iszd2returnzf3zd2letzd2var1332z43zzreturn_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2213z00zzreturn_walkz00,
		BgL_bgl_za762isza7d2returnza7f2239za7,
		BGl_z62iszd2returnzf3zd2app1334z91zzreturn_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2214z00zzreturn_walkz00,
		BgL_bgl_za762returnza712za7d2a2240za7,
		BGl_z62returnz12zd2app1338za2zzreturn_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_GENERIC(BGl_iszd2returnzf3zd2envzf3zzreturn_walkz00,
		BgL_bgl_za762isza7d2returnza7f2241za7,
		BGl_z62iszd2returnzf3z43zzreturn_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2216z00zzreturn_walkz00,
		BgL_bgl_za762returnza7d2goto2242z00,
		BGl_z62returnzd2gotozd2funsz12zd2le1342za2zzreturn_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_returnzd2walkz12zd2envz12zzreturn_walkz00,
		BgL_bgl_za762returnza7d2walk2243z00,
		BGl_z62returnzd2walkz12za2zzreturn_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_iszd2exitzd2returnzf3zd2envz21zzreturn_walkz00,
		BgL_bgl_za762isza7d2exitza7d2r2244za7,
		BGl_z62iszd2exitzd2returnzf3z91zzreturn_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2196z00zzreturn_walkz00,
		BgL_bgl_string2196za700za7za7r2245za7, "Return", 6);
	      DEFINE_STRING(BGl_string2197z00zzreturn_walkz00,
		BgL_bgl_string2197za700za7za7r2246za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2198z00zzreturn_walkz00,
		BgL_bgl_string2198za700za7za7r2247za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2199z00zzreturn_walkz00,
		BgL_bgl_string2199za700za7za7r2248za7, " error", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_iszd2getzd2exitdzd2topzf3zd2envzf3zzreturn_walkz00,
		BgL_bgl_za762isza7d2getza7d2ex2249za7,
		BGl_z62iszd2getzd2exitdzd2topzf3z43zzreturn_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initzd2returnzd2cachez12zd2envzc0zzreturn_walkz00,
		BgL_bgl_za762initza7d2return2250z00,
		BGl_z62initzd2returnzd2cachez12z70zzreturn_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_GENERIC(BGl_returnz12zd2envzc0zzreturn_walkz00,
		BgL_bgl_za762returnza712za770za72251z00,
		BGl_z62returnz12z70zzreturn_walkz00, 0L, BUNSPEC, 3);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_returnzd2gotozd2funsz12zd2envzc0zzreturn_walkz00,
		BgL_bgl_za762returnza7d2goto2252z00,
		BGl_z62returnzd2gotozd2funsz12z70zzreturn_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_iszd2unwindzd2untilzf3zd2envz21zzreturn_walkz00,
		BgL_bgl_za762isza7d2unwindza7d2253za7,
		BGl_z62iszd2unwindzd2untilzf3z91zzreturn_walkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzreturn_walkz00));
		     ADD_ROOT((void *) (&BGl_za2popzd2exitz12za2zc0zzreturn_walkz00));
		   
			 ADD_ROOT((void *) (&BGl_za2envzd2pushzd2exitz12za2z12zzreturn_walkz00));
		     ADD_ROOT((void *) (&BGl_za2envzd2popzd2exitz12za2z12zzreturn_walkz00));
		     ADD_ROOT((void *) (&BGl_za2pushzd2exitz12za2zc0zzreturn_walkz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2currentzd2dynamiczd2envza2z00zzreturn_walkz00));
		     ADD_ROOT((void
				*) (&BGl_za2envzd2getzd2exitdzd2topza2zd2zzreturn_walkz00));
		     ADD_ROOT((void *) (&BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00));
		     ADD_ROOT((void *) (&BGl_za2unwindzd2untilz12za2zc0zzreturn_walkz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2exitdzd2protectzd2setz12za2z12zzreturn_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzreturn_walkz00(long
		BgL_checksumz00_3895, char *BgL_fromz00_3896)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzreturn_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzreturn_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzreturn_walkz00();
					BGl_libraryzd2moduleszd2initz00zzreturn_walkz00();
					BGl_cnstzd2initzd2zzreturn_walkz00();
					BGl_importedzd2moduleszd2initz00zzreturn_walkz00();
					BGl_genericzd2initzd2zzreturn_walkz00();
					BGl_methodzd2initzd2zzreturn_walkz00();
					return BGl_toplevelzd2initzd2zzreturn_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzreturn_walkz00(void)
	{
		{	/* Return/walk.scm 16 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"return_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "return_walk");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "return_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"return_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"return_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "return_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "return_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "return_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "return_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "return_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "return_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"return_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "return_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "return_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzreturn_walkz00(void)
	{
		{	/* Return/walk.scm 16 */
			{	/* Return/walk.scm 16 */
				obj_t BgL_cportz00_3784;

				{	/* Return/walk.scm 16 */
					obj_t BgL_stringz00_3791;

					BgL_stringz00_3791 = BGl_string2219z00zzreturn_walkz00;
					{	/* Return/walk.scm 16 */
						obj_t BgL_startz00_3792;

						BgL_startz00_3792 = BINT(0L);
						{	/* Return/walk.scm 16 */
							obj_t BgL_endz00_3793;

							BgL_endz00_3793 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3791)));
							{	/* Return/walk.scm 16 */

								BgL_cportz00_3784 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3791, BgL_startz00_3792, BgL_endz00_3793);
				}}}}
				{
					long BgL_iz00_3785;

					BgL_iz00_3785 = 14L;
				BgL_loopz00_3786:
					if ((BgL_iz00_3785 == -1L))
						{	/* Return/walk.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Return/walk.scm 16 */
							{	/* Return/walk.scm 16 */
								obj_t BgL_arg2220z00_3787;

								{	/* Return/walk.scm 16 */

									{	/* Return/walk.scm 16 */
										obj_t BgL_locationz00_3789;

										BgL_locationz00_3789 = BBOOL(((bool_t) 0));
										{	/* Return/walk.scm 16 */

											BgL_arg2220z00_3787 =
												BGl_readz00zz__readerz00(BgL_cportz00_3784,
												BgL_locationz00_3789);
										}
									}
								}
								{	/* Return/walk.scm 16 */
									int BgL_tmpz00_3930;

									BgL_tmpz00_3930 = (int) (BgL_iz00_3785);
									CNST_TABLE_SET(BgL_tmpz00_3930, BgL_arg2220z00_3787);
							}}
							{	/* Return/walk.scm 16 */
								int BgL_auxz00_3790;

								BgL_auxz00_3790 = (int) ((BgL_iz00_3785 - 1L));
								{
									long BgL_iz00_3935;

									BgL_iz00_3935 = (long) (BgL_auxz00_3790);
									BgL_iz00_3785 = BgL_iz00_3935;
									goto BgL_loopz00_3786;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzreturn_walkz00(void)
	{
		{	/* Return/walk.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzreturn_walkz00(void)
	{
		{	/* Return/walk.scm 16 */
			BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00 = BUNSPEC;
			BGl_za2envzd2getzd2exitdzd2topza2zd2zzreturn_walkz00 = BUNSPEC;
			BGl_za2exitdzd2protectzd2setz12za2z12zzreturn_walkz00 = BUNSPEC;
			BGl_za2unwindzd2untilz12za2zc0zzreturn_walkz00 = BUNSPEC;
			BGl_za2pushzd2exitz12za2zc0zzreturn_walkz00 = BUNSPEC;
			BGl_za2envzd2pushzd2exitz12za2z12zzreturn_walkz00 = BUNSPEC;
			BGl_za2popzd2exitz12za2zc0zzreturn_walkz00 = BUNSPEC;
			BGl_za2envzd2popzd2exitz12za2z12zzreturn_walkz00 = BUNSPEC;
			BGl_za2currentzd2dynamiczd2envza2z00zzreturn_walkz00 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* return-walk! */
	BGL_EXPORTED_DEF obj_t BGl_returnzd2walkz12zc0zzreturn_walkz00(obj_t
		BgL_globalsz00_17)
	{
		{	/* Return/walk.scm 43 */
			{	/* Return/walk.scm 44 */
				obj_t BgL_list1371z00_1502;

				{	/* Return/walk.scm 44 */
					obj_t BgL_arg1375z00_1503;

					{	/* Return/walk.scm 44 */
						obj_t BgL_arg1376z00_1504;

						BgL_arg1376z00_1504 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1375z00_1503 =
							MAKE_YOUNG_PAIR(BGl_string2196z00zzreturn_walkz00,
							BgL_arg1376z00_1504);
					}
					BgL_list1371z00_1502 =
						MAKE_YOUNG_PAIR(BGl_string2197z00zzreturn_walkz00,
						BgL_arg1375z00_1503);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1371z00_1502);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string2196z00zzreturn_walkz00;
			{	/* Return/walk.scm 44 */
				obj_t BgL_g1117z00_1505;
				obj_t BgL_g1118z00_1506;

				{	/* Return/walk.scm 44 */
					obj_t BgL_list1424z00_1519;

					BgL_list1424z00_1519 =
						MAKE_YOUNG_PAIR
						(BGl_initzd2returnzd2cachez12zd2envzc0zzreturn_walkz00, BNIL);
					BgL_g1117z00_1505 = BgL_list1424z00_1519;
				}
				BgL_g1118z00_1506 = CNST_TABLE_REF(0);
				{
					obj_t BgL_hooksz00_1508;
					obj_t BgL_hnamesz00_1509;

					BgL_hooksz00_1508 = BgL_g1117z00_1505;
					BgL_hnamesz00_1509 = BgL_g1118z00_1506;
				BgL_zc3z04anonymousza31377ze3z87_1510:
					if (NULLP(BgL_hooksz00_1508))
						{	/* Return/walk.scm 44 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Return/walk.scm 44 */
							bool_t BgL_test2257z00_3950;

							{	/* Return/walk.scm 44 */
								obj_t BgL_fun1423z00_1517;

								BgL_fun1423z00_1517 = CAR(((obj_t) BgL_hooksz00_1508));
								BgL_test2257z00_3950 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1423z00_1517));
							}
							if (BgL_test2257z00_3950)
								{	/* Return/walk.scm 44 */
									obj_t BgL_arg1408z00_1514;
									obj_t BgL_arg1410z00_1515;

									BgL_arg1408z00_1514 = CDR(((obj_t) BgL_hooksz00_1508));
									BgL_arg1410z00_1515 = CDR(((obj_t) BgL_hnamesz00_1509));
									{
										obj_t BgL_hnamesz00_3962;
										obj_t BgL_hooksz00_3961;

										BgL_hooksz00_3961 = BgL_arg1408z00_1514;
										BgL_hnamesz00_3962 = BgL_arg1410z00_1515;
										BgL_hnamesz00_1509 = BgL_hnamesz00_3962;
										BgL_hooksz00_1508 = BgL_hooksz00_3961;
										goto BgL_zc3z04anonymousza31377ze3z87_1510;
									}
								}
							else
								{	/* Return/walk.scm 44 */
									obj_t BgL_arg1421z00_1516;

									BgL_arg1421z00_1516 = CAR(((obj_t) BgL_hnamesz00_1509));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2196z00zzreturn_walkz00,
										BGl_string2198z00zzreturn_walkz00, BgL_arg1421z00_1516);
								}
						}
				}
			}
			{
				obj_t BgL_l1317z00_1521;

				BgL_l1317z00_1521 = BgL_globalsz00_17;
			BgL_zc3z04anonymousza31425ze3z87_1522:
				if (PAIRP(BgL_l1317z00_1521))
					{	/* Return/walk.scm 45 */
						BGl_returnzd2funz12zc0zzreturn_walkz00(CAR(BgL_l1317z00_1521));
						{
							obj_t BgL_l1317z00_3970;

							BgL_l1317z00_3970 = CDR(BgL_l1317z00_1521);
							BgL_l1317z00_1521 = BgL_l1317z00_3970;
							goto BgL_zc3z04anonymousza31425ze3z87_1522;
						}
					}
				else
					{	/* Return/walk.scm 45 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Return/walk.scm 46 */
					{	/* Return/walk.scm 46 */
						obj_t BgL_port1319z00_1529;

						{	/* Return/walk.scm 46 */
							obj_t BgL_tmpz00_3975;

							BgL_tmpz00_3975 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1319z00_1529 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3975);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1319z00_1529);
						bgl_display_string(BGl_string2199z00zzreturn_walkz00,
							BgL_port1319z00_1529);
						{	/* Return/walk.scm 46 */
							obj_t BgL_arg1448z00_1530;

							{	/* Return/walk.scm 46 */
								bool_t BgL_test2260z00_3980;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Return/walk.scm 46 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Return/walk.scm 46 */
												BgL_test2260z00_3980 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Return/walk.scm 46 */
												BgL_test2260z00_3980 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Return/walk.scm 46 */
										BgL_test2260z00_3980 = ((bool_t) 0);
									}
								if (BgL_test2260z00_3980)
									{	/* Return/walk.scm 46 */
										BgL_arg1448z00_1530 = BGl_string2200z00zzreturn_walkz00;
									}
								else
									{	/* Return/walk.scm 46 */
										BgL_arg1448z00_1530 = BGl_string2201z00zzreturn_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1448z00_1530, BgL_port1319z00_1529);
						}
						bgl_display_string(BGl_string2202z00zzreturn_walkz00,
							BgL_port1319z00_1529);
						bgl_display_char(((unsigned char) 10), BgL_port1319z00_1529);
					}
					{	/* Return/walk.scm 46 */
						obj_t BgL_list1451z00_1534;

						BgL_list1451z00_1534 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1451z00_1534);
					}
				}
			else
				{	/* Return/walk.scm 46 */
					obj_t BgL_g1119z00_1535;
					obj_t BgL_g1120z00_1536;

					{	/* Return/walk.scm 46 */
						obj_t BgL_list1488z00_1549;

						BgL_list1488z00_1549 =
							MAKE_YOUNG_PAIR
							(BGl_clearzd2returnzd2cachez12zd2envzc0zzreturn_walkz00, BNIL);
						BgL_g1119z00_1535 = BgL_list1488z00_1549;
					}
					BgL_g1120z00_1536 = CNST_TABLE_REF(2);
					{
						obj_t BgL_hooksz00_1538;
						obj_t BgL_hnamesz00_1539;

						BgL_hooksz00_1538 = BgL_g1119z00_1535;
						BgL_hnamesz00_1539 = BgL_g1120z00_1536;
					BgL_zc3z04anonymousza31452ze3z87_1540:
						if (NULLP(BgL_hooksz00_1538))
							{	/* Return/walk.scm 46 */
								return BgL_globalsz00_17;
							}
						else
							{	/* Return/walk.scm 46 */
								bool_t BgL_test2264z00_3999;

								{	/* Return/walk.scm 46 */
									obj_t BgL_fun1487z00_1547;

									BgL_fun1487z00_1547 = CAR(((obj_t) BgL_hooksz00_1538));
									BgL_test2264z00_3999 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1487z00_1547));
								}
								if (BgL_test2264z00_3999)
									{	/* Return/walk.scm 46 */
										obj_t BgL_arg1472z00_1544;
										obj_t BgL_arg1473z00_1545;

										BgL_arg1472z00_1544 = CDR(((obj_t) BgL_hooksz00_1538));
										BgL_arg1473z00_1545 = CDR(((obj_t) BgL_hnamesz00_1539));
										{
											obj_t BgL_hnamesz00_4011;
											obj_t BgL_hooksz00_4010;

											BgL_hooksz00_4010 = BgL_arg1472z00_1544;
											BgL_hnamesz00_4011 = BgL_arg1473z00_1545;
											BgL_hnamesz00_1539 = BgL_hnamesz00_4011;
											BgL_hooksz00_1538 = BgL_hooksz00_4010;
											goto BgL_zc3z04anonymousza31452ze3z87_1540;
										}
									}
								else
									{	/* Return/walk.scm 46 */
										obj_t BgL_arg1485z00_1546;

										BgL_arg1485z00_1546 = CAR(((obj_t) BgL_hnamesz00_1539));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string2203z00zzreturn_walkz00, BgL_arg1485z00_1546);
									}
							}
					}
				}
		}

	}



/* &return-walk! */
	obj_t BGl_z62returnzd2walkz12za2zzreturn_walkz00(obj_t BgL_envz00_3720,
		obj_t BgL_globalsz00_3721)
	{
		{	/* Return/walk.scm 43 */
			return BGl_returnzd2walkz12zc0zzreturn_walkz00(BgL_globalsz00_3721);
		}

	}



/* init-return-cache! */
	BGL_EXPORTED_DEF obj_t BGl_initzd2returnzd2cachez12z12zzreturn_walkz00(void)
	{
		{	/* Return/walk.scm 64 */
			{	/* Return/walk.scm 65 */
				bool_t BgL_test2265z00_4016;

				{	/* Return/walk.scm 65 */
					obj_t BgL_objz00_2430;

					BgL_objz00_2430 = BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00;
					{	/* Return/walk.scm 65 */
						obj_t BgL_classz00_2431;

						BgL_classz00_2431 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_objz00_2430))
							{	/* Return/walk.scm 65 */
								BgL_objectz00_bglt BgL_arg1807z00_2433;

								BgL_arg1807z00_2433 = (BgL_objectz00_bglt) (BgL_objz00_2430);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Return/walk.scm 65 */
										long BgL_idxz00_2439;

										BgL_idxz00_2439 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2433);
										BgL_test2265z00_4016 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2439 + 2L)) == BgL_classz00_2431);
									}
								else
									{	/* Return/walk.scm 65 */
										bool_t BgL_res2149z00_2464;

										{	/* Return/walk.scm 65 */
											obj_t BgL_oclassz00_2447;

											{	/* Return/walk.scm 65 */
												obj_t BgL_arg1815z00_2455;
												long BgL_arg1816z00_2456;

												BgL_arg1815z00_2455 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Return/walk.scm 65 */
													long BgL_arg1817z00_2457;

													BgL_arg1817z00_2457 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2433);
													BgL_arg1816z00_2456 =
														(BgL_arg1817z00_2457 - OBJECT_TYPE);
												}
												BgL_oclassz00_2447 =
													VECTOR_REF(BgL_arg1815z00_2455, BgL_arg1816z00_2456);
											}
											{	/* Return/walk.scm 65 */
												bool_t BgL__ortest_1115z00_2448;

												BgL__ortest_1115z00_2448 =
													(BgL_classz00_2431 == BgL_oclassz00_2447);
												if (BgL__ortest_1115z00_2448)
													{	/* Return/walk.scm 65 */
														BgL_res2149z00_2464 = BgL__ortest_1115z00_2448;
													}
												else
													{	/* Return/walk.scm 65 */
														long BgL_odepthz00_2449;

														{	/* Return/walk.scm 65 */
															obj_t BgL_arg1804z00_2450;

															BgL_arg1804z00_2450 = (BgL_oclassz00_2447);
															BgL_odepthz00_2449 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2450);
														}
														if ((2L < BgL_odepthz00_2449))
															{	/* Return/walk.scm 65 */
																obj_t BgL_arg1802z00_2452;

																{	/* Return/walk.scm 65 */
																	obj_t BgL_arg1803z00_2453;

																	BgL_arg1803z00_2453 = (BgL_oclassz00_2447);
																	BgL_arg1802z00_2452 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2453,
																		2L);
																}
																BgL_res2149z00_2464 =
																	(BgL_arg1802z00_2452 == BgL_classz00_2431);
															}
														else
															{	/* Return/walk.scm 65 */
																BgL_res2149z00_2464 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2265z00_4016 = BgL_res2149z00_2464;
									}
							}
						else
							{	/* Return/walk.scm 65 */
								BgL_test2265z00_4016 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2265z00_4016)
					{	/* Return/walk.scm 65 */
						BFALSE;
					}
				else
					{	/* Return/walk.scm 65 */
						{	/* Return/walk.scm 66 */
							obj_t BgL_list1490z00_1551;

							BgL_list1490z00_1551 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
							BGl_za2currentzd2dynamiczd2envza2z00zzreturn_walkz00 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(4),
								BgL_list1490z00_1551);
						}
						{	/* Return/walk.scm 67 */
							obj_t BgL_list1491z00_1552;

							BgL_list1491z00_1552 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
							BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(5),
								BgL_list1491z00_1552);
						}
						{	/* Return/walk.scm 68 */
							obj_t BgL_list1492z00_1553;

							BgL_list1492z00_1553 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
							BGl_za2exitdzd2protectzd2setz12za2z12zzreturn_walkz00 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(6),
								BgL_list1492z00_1553);
						}
						{	/* Return/walk.scm 69 */
							obj_t BgL_list1493z00_1554;

							BgL_list1493z00_1554 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
							BGl_za2envzd2getzd2exitdzd2topza2zd2zzreturn_walkz00 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(7),
								BgL_list1493z00_1554);
						}
						{	/* Return/walk.scm 70 */
							obj_t BgL_list1494z00_1555;

							BgL_list1494z00_1555 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
							BGl_za2unwindzd2untilz12za2zc0zzreturn_walkz00 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(9),
								BgL_list1494z00_1555);
						}
						{	/* Return/walk.scm 71 */
							obj_t BgL_list1495z00_1556;

							BgL_list1495z00_1556 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
							BGl_za2popzd2exitz12za2zc0zzreturn_walkz00 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(10),
								BgL_list1495z00_1556);
						}
						{	/* Return/walk.scm 72 */
							obj_t BgL_list1496z00_1557;

							BgL_list1496z00_1557 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
							BGl_za2envzd2popzd2exitz12za2z12zzreturn_walkz00 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(11),
								BgL_list1496z00_1557);
						}
						{	/* Return/walk.scm 73 */
							obj_t BgL_list1497z00_1558;

							BgL_list1497z00_1558 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
							BGl_za2pushzd2exitz12za2zc0zzreturn_walkz00 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(12),
								BgL_list1497z00_1558);
						}
						{	/* Return/walk.scm 74 */
							obj_t BgL_list1498z00_1559;

							BgL_list1498z00_1559 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
							BGl_za2envzd2pushzd2exitz12za2z12zzreturn_walkz00 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(13),
								BgL_list1498z00_1559);
						}
					}
			}
			return BUNSPEC;
		}

	}



/* &init-return-cache! */
	obj_t BGl_z62initzd2returnzd2cachez12z70zzreturn_walkz00(obj_t
		BgL_envz00_3722)
	{
		{	/* Return/walk.scm 64 */
			return BGl_initzd2returnzd2cachez12z12zzreturn_walkz00();
		}

	}



/* &clear-return-cache! */
	obj_t BGl_z62clearzd2returnzd2cachez12z70zzreturn_walkz00(obj_t
		BgL_envz00_3723)
	{
		{	/* Return/walk.scm 80 */
			BGl_za2currentzd2dynamiczd2envza2z00zzreturn_walkz00 = BFALSE;
			BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00 = BFALSE;
			BGl_za2envzd2getzd2exitdzd2topza2zd2zzreturn_walkz00 = BFALSE;
			BGl_za2unwindzd2untilz12za2zc0zzreturn_walkz00 = BFALSE;
			BGl_za2pushzd2exitz12za2zc0zzreturn_walkz00 = BFALSE;
			BGl_za2envzd2pushzd2exitz12za2z12zzreturn_walkz00 = BFALSE;
			BGl_za2popzd2exitz12za2zc0zzreturn_walkz00 = BFALSE;
			return (BGl_za2envzd2popzd2exitz12za2z12zzreturn_walkz00 =
				BFALSE, BUNSPEC);
		}

	}



/* return-fun! */
	obj_t BGl_returnzd2funz12zc0zzreturn_walkz00(obj_t BgL_varz00_18)
	{
		{	/* Return/walk.scm 93 */
			{	/* Return/walk.scm 94 */
				obj_t BgL_arg1502z00_1560;

				BgL_arg1502z00_1560 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_idz00);
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1502z00_1560);
			}
			{	/* Return/walk.scm 95 */
				BgL_valuez00_bglt BgL_funz00_1561;

				BgL_funz00_1561 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_valuez00);
				{	/* Return/walk.scm 95 */
					obj_t BgL_bodyz00_1562;

					BgL_bodyz00_1562 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1561)))->BgL_bodyz00);
					{	/* Return/walk.scm 96 */
						obj_t BgL_exitz00_1563;

						BgL_exitz00_1563 =
							BGl_functionzd2exitzd2nodez00zzreturn_walkz00(
							((BgL_nodez00_bglt) BgL_bodyz00_1562));
						{	/* Return/walk.scm 97 */

							if (PAIRP(BgL_exitz00_1563))
								{	/* Return/walk.scm 99 */
									obj_t BgL_exitvarz00_1565;
									obj_t BgL_exitnodez00_1566;

									BgL_exitvarz00_1565 = CAR(BgL_exitz00_1563);
									BgL_exitnodez00_1566 = CDR(BgL_exitz00_1563);
									if (BGl_iszd2exitzd2returnzf3zf3zzreturn_walkz00(
											((BgL_nodez00_bglt) BgL_exitnodez00_1566),
											((BgL_localz00_bglt) BgL_exitvarz00_1565)))
										{	/* Return/walk.scm 102 */
											BgL_retblockz00_bglt BgL_rblockz00_1568;

											{	/* Return/walk.scm 102 */
												BgL_retblockz00_bglt BgL_new1123z00_1570;

												{	/* Return/walk.scm 103 */
													BgL_retblockz00_bglt BgL_new1122z00_1571;

													BgL_new1122z00_1571 =
														((BgL_retblockz00_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_retblockz00_bgl))));
													{	/* Return/walk.scm 103 */
														long BgL_arg1509z00_1572;

														BgL_arg1509z00_1572 =
															BGL_CLASS_NUM(BGl_retblockz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1122z00_1571),
															BgL_arg1509z00_1572);
													}
													{	/* Return/walk.scm 103 */
														BgL_objectz00_bglt BgL_tmpz00_4097;

														BgL_tmpz00_4097 =
															((BgL_objectz00_bglt) BgL_new1122z00_1571);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4097, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1122z00_1571);
													BgL_new1123z00_1570 = BgL_new1122z00_1571;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1123z00_1570)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt)
																		BgL_bodyz00_1562)))->BgL_locz00)), BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1123z00_1570)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt)
																		BgL_exitnodez00_1566)))->BgL_typez00)),
													BUNSPEC);
												((((BgL_retblockz00_bglt)
															COBJECT(BgL_new1123z00_1570))->BgL_bodyz00) =
													((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
															BgL_bodyz00_1562)), BUNSPEC);
												BgL_rblockz00_1568 = BgL_new1123z00_1570;
											}
											((((BgL_retblockz00_bglt) COBJECT(BgL_rblockz00_1568))->
													BgL_bodyz00) =
												((BgL_nodez00_bglt)
													BGl_returnz12z12zzreturn_walkz00(((BgL_nodez00_bglt)
															BgL_exitnodez00_1566),
														((BgL_localz00_bglt) BgL_exitvarz00_1565),
														BgL_rblockz00_1568)), BUNSPEC);
											{	/* Return/walk.scm 108 */
												obj_t BgL_vz00_2477;

												BgL_vz00_2477 = CNST_TABLE_REF(14);
												((((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_funz00_1561)))->
														BgL_classz00) = ((obj_t) BgL_vz00_2477), BUNSPEC);
											}
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1561)))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_rblockz00_1568)), BUNSPEC);
										}
									else
										{	/* Return/walk.scm 101 */
											BFALSE;
										}
								}
							else
								{	/* Return/walk.scm 98 */
									BFALSE;
								}
							if (CBOOL(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00))
								{	/* Return/walk.scm 110 */
									((obj_t)
										BGl_returnzd2gotozd2funsz12z12zzreturn_walkz00(
											((BgL_nodez00_bglt) BgL_bodyz00_1562)));
								}
							else
								{	/* Return/walk.scm 110 */
									BFALSE;
								}
							BGl_leavezd2functionzd2zztools_errorz00();
							return BgL_varz00_18;
						}
					}
				}
			}
		}

	}



/* function-exit-node */
	obj_t BGl_functionzd2exitzd2nodez00zzreturn_walkz00(BgL_nodez00_bglt
		BgL_nodez00_19)
	{
		{	/* Return/walk.scm 145 */
			{
				BgL_nodez00_bglt BgL_nodez00_1700;
				BgL_localz00_bglt BgL_varz00_1701;
				BgL_nodez00_bglt BgL_nodez00_1728;
				BgL_localz00_bglt BgL_varz00_1729;
				obj_t BgL_envz00_1730;
				BgL_nodez00_bglt BgL_nodez00_1757;
				BgL_nodez00_bglt BgL_nodez00_1784;
				obj_t BgL_envz00_1785;
				BgL_nodez00_bglt BgL_nodez00_1829;
				BgL_localz00_bglt BgL_exitvarz00_1830;
				BgL_nodez00_bglt BgL_nodez00_1851;
				BgL_localz00_bglt BgL_exitvarz00_1852;
				obj_t BgL_envz00_1853;
				obj_t BgL_nodesz00_1884;
				BgL_localz00_bglt BgL_varz00_1885;
				obj_t BgL_nodesz00_1902;
				BgL_localz00_bglt BgL_varz00_1903;
				obj_t BgL_envz00_1904;
				BgL_nodez00_bglt BgL_bodyz00_1921;
				BgL_varz00_bglt BgL_varz00_1922;
				BgL_nodez00_bglt BgL_bodyz00_1938;
				BgL_varz00_bglt BgL_varz00_1939;

				{	/* Return/walk.scm 320 */
					bool_t BgL_test2273z00_4127;

					{	/* Return/walk.scm 320 */
						obj_t BgL_classz00_3422;

						BgL_classz00_3422 = BGl_setzd2exzd2itz00zzast_nodez00;
						{	/* Return/walk.scm 320 */
							BgL_objectz00_bglt BgL_arg1807z00_3424;

							{	/* Return/walk.scm 320 */
								obj_t BgL_tmpz00_4128;

								BgL_tmpz00_4128 = ((obj_t) BgL_nodez00_19);
								BgL_arg1807z00_3424 = (BgL_objectz00_bglt) (BgL_tmpz00_4128);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Return/walk.scm 320 */
									long BgL_idxz00_3430;

									BgL_idxz00_3430 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3424);
									BgL_test2273z00_4127 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3430 + 2L)) == BgL_classz00_3422);
								}
							else
								{	/* Return/walk.scm 320 */
									bool_t BgL_res2175z00_3455;

									{	/* Return/walk.scm 320 */
										obj_t BgL_oclassz00_3438;

										{	/* Return/walk.scm 320 */
											obj_t BgL_arg1815z00_3446;
											long BgL_arg1816z00_3447;

											BgL_arg1815z00_3446 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Return/walk.scm 320 */
												long BgL_arg1817z00_3448;

												BgL_arg1817z00_3448 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3424);
												BgL_arg1816z00_3447 =
													(BgL_arg1817z00_3448 - OBJECT_TYPE);
											}
											BgL_oclassz00_3438 =
												VECTOR_REF(BgL_arg1815z00_3446, BgL_arg1816z00_3447);
										}
										{	/* Return/walk.scm 320 */
											bool_t BgL__ortest_1115z00_3439;

											BgL__ortest_1115z00_3439 =
												(BgL_classz00_3422 == BgL_oclassz00_3438);
											if (BgL__ortest_1115z00_3439)
												{	/* Return/walk.scm 320 */
													BgL_res2175z00_3455 = BgL__ortest_1115z00_3439;
												}
											else
												{	/* Return/walk.scm 320 */
													long BgL_odepthz00_3440;

													{	/* Return/walk.scm 320 */
														obj_t BgL_arg1804z00_3441;

														BgL_arg1804z00_3441 = (BgL_oclassz00_3438);
														BgL_odepthz00_3440 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3441);
													}
													if ((2L < BgL_odepthz00_3440))
														{	/* Return/walk.scm 320 */
															obj_t BgL_arg1802z00_3443;

															{	/* Return/walk.scm 320 */
																obj_t BgL_arg1803z00_3444;

																BgL_arg1803z00_3444 = (BgL_oclassz00_3438);
																BgL_arg1802z00_3443 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3444,
																	2L);
															}
															BgL_res2175z00_3455 =
																(BgL_arg1802z00_3443 == BgL_classz00_3422);
														}
													else
														{	/* Return/walk.scm 320 */
															BgL_res2175z00_3455 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2273z00_4127 = BgL_res2175z00_3455;
								}
						}
					}
					if (BgL_test2273z00_4127)
						{	/* Return/walk.scm 322 */
							obj_t BgL__ortest_1178z00_1590;

							{	/* Return/walk.scm 322 */
								BgL_nodez00_bglt BgL_arg1516z00_1593;
								BgL_varz00_bglt BgL_arg1535z00_1594;

								BgL_arg1516z00_1593 =
									(((BgL_setzd2exzd2itz00_bglt) COBJECT(
											((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_19)))->
									BgL_bodyz00);
								BgL_arg1535z00_1594 =
									(((BgL_setzd2exzd2itz00_bglt)
										COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_19)))->
									BgL_varz00);
								BgL_bodyz00_1921 = BgL_arg1516z00_1593;
								BgL_varz00_1922 = BgL_arg1535z00_1594;
								{	/* Return/walk.scm 298 */
									bool_t BgL_test2277z00_4154;

									{	/* Return/walk.scm 298 */
										obj_t BgL_classz00_3239;

										BgL_classz00_3239 = BGl_letzd2varzd2zzast_nodez00;
										{	/* Return/walk.scm 298 */
											BgL_objectz00_bglt BgL_arg1807z00_3241;

											{	/* Return/walk.scm 298 */
												obj_t BgL_tmpz00_4155;

												BgL_tmpz00_4155 = ((obj_t) BgL_bodyz00_1921);
												BgL_arg1807z00_3241 =
													(BgL_objectz00_bglt) (BgL_tmpz00_4155);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Return/walk.scm 298 */
													long BgL_idxz00_3247;

													BgL_idxz00_3247 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3241);
													BgL_test2277z00_4154 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3247 + 3L)) == BgL_classz00_3239);
												}
											else
												{	/* Return/walk.scm 298 */
													bool_t BgL_res2170z00_3272;

													{	/* Return/walk.scm 298 */
														obj_t BgL_oclassz00_3255;

														{	/* Return/walk.scm 298 */
															obj_t BgL_arg1815z00_3263;
															long BgL_arg1816z00_3264;

															BgL_arg1815z00_3263 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Return/walk.scm 298 */
																long BgL_arg1817z00_3265;

																BgL_arg1817z00_3265 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3241);
																BgL_arg1816z00_3264 =
																	(BgL_arg1817z00_3265 - OBJECT_TYPE);
															}
															BgL_oclassz00_3255 =
																VECTOR_REF(BgL_arg1815z00_3263,
																BgL_arg1816z00_3264);
														}
														{	/* Return/walk.scm 298 */
															bool_t BgL__ortest_1115z00_3256;

															BgL__ortest_1115z00_3256 =
																(BgL_classz00_3239 == BgL_oclassz00_3255);
															if (BgL__ortest_1115z00_3256)
																{	/* Return/walk.scm 298 */
																	BgL_res2170z00_3272 =
																		BgL__ortest_1115z00_3256;
																}
															else
																{	/* Return/walk.scm 298 */
																	long BgL_odepthz00_3257;

																	{	/* Return/walk.scm 298 */
																		obj_t BgL_arg1804z00_3258;

																		BgL_arg1804z00_3258 = (BgL_oclassz00_3255);
																		BgL_odepthz00_3257 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3258);
																	}
																	if ((3L < BgL_odepthz00_3257))
																		{	/* Return/walk.scm 298 */
																			obj_t BgL_arg1802z00_3260;

																			{	/* Return/walk.scm 298 */
																				obj_t BgL_arg1803z00_3261;

																				BgL_arg1803z00_3261 =
																					(BgL_oclassz00_3255);
																				BgL_arg1802z00_3260 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3261, 3L);
																			}
																			BgL_res2170z00_3272 =
																				(BgL_arg1802z00_3260 ==
																				BgL_classz00_3239);
																		}
																	else
																		{	/* Return/walk.scm 298 */
																			BgL_res2170z00_3272 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2277z00_4154 = BgL_res2170z00_3272;
												}
										}
									}
									if (BgL_test2277z00_4154)
										{	/* Return/walk.scm 300 */
											bool_t BgL_test2281z00_4177;

											{	/* Return/walk.scm 300 */
												bool_t BgL_test2282z00_4178;

												{	/* Return/walk.scm 300 */
													BgL_nodez00_bglt BgL_arg1953z00_1937;

													BgL_arg1953z00_1937 =
														(((BgL_letzd2varzd2_bglt) COBJECT(
																((BgL_letzd2varzd2_bglt) BgL_bodyz00_1921)))->
														BgL_bodyz00);
													{	/* Return/walk.scm 300 */
														obj_t BgL_classz00_3273;

														BgL_classz00_3273 = BGl_sequencez00zzast_nodez00;
														{	/* Return/walk.scm 300 */
															BgL_objectz00_bglt BgL_arg1807z00_3275;

															{	/* Return/walk.scm 300 */
																obj_t BgL_tmpz00_4181;

																BgL_tmpz00_4181 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_arg1953z00_1937));
																BgL_arg1807z00_3275 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4181);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Return/walk.scm 300 */
																	long BgL_idxz00_3281;

																	BgL_idxz00_3281 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3275);
																	BgL_test2282z00_4178 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3281 + 3L)) ==
																		BgL_classz00_3273);
																}
															else
																{	/* Return/walk.scm 300 */
																	bool_t BgL_res2171z00_3306;

																	{	/* Return/walk.scm 300 */
																		obj_t BgL_oclassz00_3289;

																		{	/* Return/walk.scm 300 */
																			obj_t BgL_arg1815z00_3297;
																			long BgL_arg1816z00_3298;

																			BgL_arg1815z00_3297 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Return/walk.scm 300 */
																				long BgL_arg1817z00_3299;

																				BgL_arg1817z00_3299 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3275);
																				BgL_arg1816z00_3298 =
																					(BgL_arg1817z00_3299 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3289 =
																				VECTOR_REF(BgL_arg1815z00_3297,
																				BgL_arg1816z00_3298);
																		}
																		{	/* Return/walk.scm 300 */
																			bool_t BgL__ortest_1115z00_3290;

																			BgL__ortest_1115z00_3290 =
																				(BgL_classz00_3273 ==
																				BgL_oclassz00_3289);
																			if (BgL__ortest_1115z00_3290)
																				{	/* Return/walk.scm 300 */
																					BgL_res2171z00_3306 =
																						BgL__ortest_1115z00_3290;
																				}
																			else
																				{	/* Return/walk.scm 300 */
																					long BgL_odepthz00_3291;

																					{	/* Return/walk.scm 300 */
																						obj_t BgL_arg1804z00_3292;

																						BgL_arg1804z00_3292 =
																							(BgL_oclassz00_3289);
																						BgL_odepthz00_3291 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3292);
																					}
																					if ((3L < BgL_odepthz00_3291))
																						{	/* Return/walk.scm 300 */
																							obj_t BgL_arg1802z00_3294;

																							{	/* Return/walk.scm 300 */
																								obj_t BgL_arg1803z00_3295;

																								BgL_arg1803z00_3295 =
																									(BgL_oclassz00_3289);
																								BgL_arg1802z00_3294 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3295, 3L);
																							}
																							BgL_res2171z00_3306 =
																								(BgL_arg1802z00_3294 ==
																								BgL_classz00_3273);
																						}
																					else
																						{	/* Return/walk.scm 300 */
																							BgL_res2171z00_3306 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2282z00_4178 = BgL_res2171z00_3306;
																}
														}
													}
												}
												if (BgL_test2282z00_4178)
													{	/* Return/walk.scm 300 */
														BgL_test2281z00_4177 =
															NULLP(
															(((BgL_letzd2varzd2_bglt) COBJECT(
																		((BgL_letzd2varzd2_bglt)
																			BgL_bodyz00_1921)))->BgL_bindingsz00));
													}
												else
													{	/* Return/walk.scm 300 */
														BgL_test2281z00_4177 = ((bool_t) 0);
													}
											}
											if (BgL_test2281z00_4177)
												{	/* Return/walk.scm 301 */
													BgL_sequencez00_bglt BgL_i1170z00_1931;

													BgL_i1170z00_1931 =
														((BgL_sequencez00_bglt)
														(((BgL_letzd2varzd2_bglt) COBJECT(
																	((BgL_letzd2varzd2_bglt) BgL_bodyz00_1921)))->
															BgL_bodyz00));
													{	/* Return/walk.scm 303 */
														obj_t BgL_arg1950z00_1933;
														BgL_variablez00_bglt BgL_arg1951z00_1934;

														BgL_arg1950z00_1933 =
															(((BgL_sequencez00_bglt)
																COBJECT(BgL_i1170z00_1931))->BgL_nodesz00);
														BgL_arg1951z00_1934 =
															(((BgL_varz00_bglt) COBJECT(BgL_varz00_1922))->
															BgL_variablez00);
														BgL_nodesz00_1884 = BgL_arg1950z00_1933;
														BgL_varz00_1885 =
															((BgL_localz00_bglt) BgL_arg1951z00_1934);
														{	/* Return/walk.scm 280 */
															bool_t BgL_test2286z00_4212;

															if ((bgl_list_length(BgL_nodesz00_1884) == 2L))
																{	/* Return/walk.scm 280 */
																	BgL_nodez00_1829 =
																		((BgL_nodez00_bglt)
																		CAR(((obj_t) BgL_nodesz00_1884)));
																	BgL_exitvarz00_1830 = BgL_varz00_1885;
																	{	/* Return/walk.scm 247 */
																		bool_t BgL_test2288z00_4216;

																		{	/* Return/walk.scm 247 */
																			obj_t BgL_classz00_3031;

																			BgL_classz00_3031 =
																				BGl_appz00zzast_nodez00;
																			{	/* Return/walk.scm 247 */
																				BgL_objectz00_bglt BgL_arg1807z00_3033;

																				{	/* Return/walk.scm 247 */
																					obj_t BgL_tmpz00_4217;

																					BgL_tmpz00_4217 =
																						((obj_t) BgL_nodez00_1829);
																					BgL_arg1807z00_3033 =
																						(BgL_objectz00_bglt)
																						(BgL_tmpz00_4217);
																				}
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Return/walk.scm 247 */
																						long BgL_idxz00_3039;

																						BgL_idxz00_3039 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_3033);
																						BgL_test2288z00_4216 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_3039 + 3L)) ==
																							BgL_classz00_3031);
																					}
																				else
																					{	/* Return/walk.scm 247 */
																						bool_t BgL_res2165z00_3064;

																						{	/* Return/walk.scm 247 */
																							obj_t BgL_oclassz00_3047;

																							{	/* Return/walk.scm 247 */
																								obj_t BgL_arg1815z00_3055;
																								long BgL_arg1816z00_3056;

																								BgL_arg1815z00_3055 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Return/walk.scm 247 */
																									long BgL_arg1817z00_3057;

																									BgL_arg1817z00_3057 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_3033);
																									BgL_arg1816z00_3056 =
																										(BgL_arg1817z00_3057 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_3047 =
																									VECTOR_REF
																									(BgL_arg1815z00_3055,
																									BgL_arg1816z00_3056);
																							}
																							{	/* Return/walk.scm 247 */
																								bool_t BgL__ortest_1115z00_3048;

																								BgL__ortest_1115z00_3048 =
																									(BgL_classz00_3031 ==
																									BgL_oclassz00_3047);
																								if (BgL__ortest_1115z00_3048)
																									{	/* Return/walk.scm 247 */
																										BgL_res2165z00_3064 =
																											BgL__ortest_1115z00_3048;
																									}
																								else
																									{	/* Return/walk.scm 247 */
																										long BgL_odepthz00_3049;

																										{	/* Return/walk.scm 247 */
																											obj_t BgL_arg1804z00_3050;

																											BgL_arg1804z00_3050 =
																												(BgL_oclassz00_3047);
																											BgL_odepthz00_3049 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_3050);
																										}
																										if (
																											(3L < BgL_odepthz00_3049))
																											{	/* Return/walk.scm 247 */
																												obj_t
																													BgL_arg1802z00_3052;
																												{	/* Return/walk.scm 247 */
																													obj_t
																														BgL_arg1803z00_3053;
																													BgL_arg1803z00_3053 =
																														(BgL_oclassz00_3047);
																													BgL_arg1802z00_3052 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_3053,
																														3L);
																												}
																												BgL_res2165z00_3064 =
																													(BgL_arg1802z00_3052
																													== BgL_classz00_3031);
																											}
																										else
																											{	/* Return/walk.scm 247 */
																												BgL_res2165z00_3064 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2288z00_4216 =
																							BgL_res2165z00_3064;
																					}
																			}
																		}
																		if (BgL_test2288z00_4216)
																			{	/* Return/walk.scm 249 */
																				BgL_varz00_bglt BgL_i1161z00_1834;

																				BgL_i1161z00_1834 =
																					(((BgL_appz00_bglt) COBJECT(
																							((BgL_appz00_bglt)
																								BgL_nodez00_1829)))->
																					BgL_funz00);
																				{	/* Return/walk.scm 251 */
																					bool_t BgL_test2292z00_4241;

																					{	/* Return/walk.scm 251 */
																						BgL_variablez00_bglt
																							BgL_arg1885z00_1850;
																						BgL_arg1885z00_1850 =
																							(((BgL_varz00_bglt)
																								COBJECT(BgL_i1161z00_1834))->
																							BgL_variablez00);
																						BgL_test2292z00_4241 =
																							(((obj_t) BgL_arg1885z00_1850) ==
																							BGl_za2pushzd2exitz12za2zc0zzreturn_walkz00);
																					}
																					if (BgL_test2292z00_4241)
																						{	/* Return/walk.scm 251 */
																							if (
																								(bgl_list_length(
																										(((BgL_appz00_bglt) COBJECT(
																													((BgL_appz00_bglt)
																														BgL_nodez00_1829)))->
																											BgL_argsz00)) == 2L))
																								{	/* Return/walk.scm 255 */
																									bool_t BgL_test2294z00_4250;

																									{	/* Return/walk.scm 255 */
																										obj_t BgL_arg1880z00_1846;

																										{	/* Return/walk.scm 255 */
																											obj_t BgL_pairz00_3066;

																											BgL_pairz00_3066 =
																												(((BgL_appz00_bglt)
																													COBJECT((
																															(BgL_appz00_bglt)
																															BgL_nodez00_1829)))->
																												BgL_argsz00);
																											BgL_arg1880z00_1846 =
																												CAR(BgL_pairz00_3066);
																										}
																										{	/* Return/walk.scm 255 */
																											obj_t BgL_classz00_3067;

																											BgL_classz00_3067 =
																												BGl_varz00zzast_nodez00;
																											if (BGL_OBJECTP
																												(BgL_arg1880z00_1846))
																												{	/* Return/walk.scm 255 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_3069;
																													BgL_arg1807z00_3069 =
																														(BgL_objectz00_bglt)
																														(BgL_arg1880z00_1846);
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Return/walk.scm 255 */
																															long
																																BgL_idxz00_3075;
																															BgL_idxz00_3075 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_3069);
																															BgL_test2294z00_4250
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_3075
																																		+ 2L)) ==
																																BgL_classz00_3067);
																														}
																													else
																														{	/* Return/walk.scm 255 */
																															bool_t
																																BgL_res2166z00_3100;
																															{	/* Return/walk.scm 255 */
																																obj_t
																																	BgL_oclassz00_3083;
																																{	/* Return/walk.scm 255 */
																																	obj_t
																																		BgL_arg1815z00_3091;
																																	long
																																		BgL_arg1816z00_3092;
																																	BgL_arg1815z00_3091
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Return/walk.scm 255 */
																																		long
																																			BgL_arg1817z00_3093;
																																		BgL_arg1817z00_3093
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_3069);
																																		BgL_arg1816z00_3092
																																			=
																																			(BgL_arg1817z00_3093
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_3083
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_3091,
																																		BgL_arg1816z00_3092);
																																}
																																{	/* Return/walk.scm 255 */
																																	bool_t
																																		BgL__ortest_1115z00_3084;
																																	BgL__ortest_1115z00_3084
																																		=
																																		(BgL_classz00_3067
																																		==
																																		BgL_oclassz00_3083);
																																	if (BgL__ortest_1115z00_3084)
																																		{	/* Return/walk.scm 255 */
																																			BgL_res2166z00_3100
																																				=
																																				BgL__ortest_1115z00_3084;
																																		}
																																	else
																																		{	/* Return/walk.scm 255 */
																																			long
																																				BgL_odepthz00_3085;
																																			{	/* Return/walk.scm 255 */
																																				obj_t
																																					BgL_arg1804z00_3086;
																																				BgL_arg1804z00_3086
																																					=
																																					(BgL_oclassz00_3083);
																																				BgL_odepthz00_3085
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_3086);
																																			}
																																			if (
																																				(2L <
																																					BgL_odepthz00_3085))
																																				{	/* Return/walk.scm 255 */
																																					obj_t
																																						BgL_arg1802z00_3088;
																																					{	/* Return/walk.scm 255 */
																																						obj_t
																																							BgL_arg1803z00_3089;
																																						BgL_arg1803z00_3089
																																							=
																																							(BgL_oclassz00_3083);
																																						BgL_arg1802z00_3088
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_3089,
																																							2L);
																																					}
																																					BgL_res2166z00_3100
																																						=
																																						(BgL_arg1802z00_3088
																																						==
																																						BgL_classz00_3067);
																																				}
																																			else
																																				{	/* Return/walk.scm 255 */
																																					BgL_res2166z00_3100
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test2294z00_4250
																																=
																																BgL_res2166z00_3100;
																														}
																												}
																											else
																												{	/* Return/walk.scm 255 */
																													BgL_test2294z00_4250 =
																														((bool_t) 0);
																												}
																										}
																									}
																									if (BgL_test2294z00_4250)
																										{	/* Return/walk.scm 258 */
																											obj_t BgL_tmpz00_4276;

																											{
																												BgL_variablez00_bglt
																													BgL_auxz00_4277;
																												{
																													BgL_varz00_bglt
																														BgL_auxz00_4278;
																													{	/* Return/walk.scm 256 */
																														obj_t
																															BgL_pairz00_3101;
																														BgL_pairz00_3101 =
																															(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nodez00_1829)))->BgL_argsz00);
																														BgL_auxz00_4278 =
																															((BgL_varz00_bglt)
																															CAR
																															(BgL_pairz00_3101));
																													}
																													BgL_auxz00_4277 =
																														(((BgL_varz00_bglt)
																															COBJECT
																															(BgL_auxz00_4278))->
																														BgL_variablez00);
																												}
																												BgL_tmpz00_4276 =
																													((obj_t)
																													BgL_auxz00_4277);
																											}
																											BgL_test2286z00_4212 =
																												(BgL_tmpz00_4276 ==
																												((obj_t)
																													BgL_exitvarz00_1830));
																										}
																									else
																										{	/* Return/walk.scm 255 */
																											BgL_test2286z00_4212 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* Return/walk.scm 253 */
																									BgL_test2286z00_4212 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Return/walk.scm 251 */
																							BgL_test2286z00_4212 =
																								((bool_t) 0);
																						}
																				}
																			}
																		else
																			{	/* Return/walk.scm 247 */
																				BgL_test2286z00_4212 = ((bool_t) 0);
																			}
																	}
																}
															else
																{	/* Return/walk.scm 280 */
																	BgL_test2286z00_4212 = ((bool_t) 0);
																}
															if (BgL_test2286z00_4212)
																{	/* Return/walk.scm 281 */
																	obj_t BgL_bindingz00_1892;

																	{	/* Return/walk.scm 281 */
																		obj_t BgL_pairz00_3220;

																		BgL_pairz00_3220 =
																			CDR(((obj_t) BgL_nodesz00_1884));
																		BgL_nodez00_1757 =
																			((BgL_nodez00_bglt)
																			CAR(BgL_pairz00_3220));
																	}
																	{	/* Return/walk.scm 216 */
																		bool_t BgL_test2299z00_4290;

																		{	/* Return/walk.scm 216 */
																			obj_t BgL_classz00_2850;

																			BgL_classz00_2850 =
																				BGl_letzd2varzd2zzast_nodez00;
																			{	/* Return/walk.scm 216 */
																				BgL_objectz00_bglt BgL_arg1807z00_2852;

																				{	/* Return/walk.scm 216 */
																					obj_t BgL_tmpz00_4291;

																					BgL_tmpz00_4291 =
																						((obj_t) BgL_nodez00_1757);
																					BgL_arg1807z00_2852 =
																						(BgL_objectz00_bglt)
																						(BgL_tmpz00_4291);
																				}
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Return/walk.scm 216 */
																						long BgL_idxz00_2858;

																						BgL_idxz00_2858 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_2852);
																						BgL_test2299z00_4290 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_2858 + 3L)) ==
																							BgL_classz00_2850);
																					}
																				else
																					{	/* Return/walk.scm 216 */
																						bool_t BgL_res2160z00_2883;

																						{	/* Return/walk.scm 216 */
																							obj_t BgL_oclassz00_2866;

																							{	/* Return/walk.scm 216 */
																								obj_t BgL_arg1815z00_2874;
																								long BgL_arg1816z00_2875;

																								BgL_arg1815z00_2874 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Return/walk.scm 216 */
																									long BgL_arg1817z00_2876;

																									BgL_arg1817z00_2876 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_2852);
																									BgL_arg1816z00_2875 =
																										(BgL_arg1817z00_2876 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_2866 =
																									VECTOR_REF
																									(BgL_arg1815z00_2874,
																									BgL_arg1816z00_2875);
																							}
																							{	/* Return/walk.scm 216 */
																								bool_t BgL__ortest_1115z00_2867;

																								BgL__ortest_1115z00_2867 =
																									(BgL_classz00_2850 ==
																									BgL_oclassz00_2866);
																								if (BgL__ortest_1115z00_2867)
																									{	/* Return/walk.scm 216 */
																										BgL_res2160z00_2883 =
																											BgL__ortest_1115z00_2867;
																									}
																								else
																									{	/* Return/walk.scm 216 */
																										long BgL_odepthz00_2868;

																										{	/* Return/walk.scm 216 */
																											obj_t BgL_arg1804z00_2869;

																											BgL_arg1804z00_2869 =
																												(BgL_oclassz00_2866);
																											BgL_odepthz00_2868 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_2869);
																										}
																										if (
																											(3L < BgL_odepthz00_2868))
																											{	/* Return/walk.scm 216 */
																												obj_t
																													BgL_arg1802z00_2871;
																												{	/* Return/walk.scm 216 */
																													obj_t
																														BgL_arg1803z00_2872;
																													BgL_arg1803z00_2872 =
																														(BgL_oclassz00_2866);
																													BgL_arg1802z00_2871 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_2872,
																														3L);
																												}
																												BgL_res2160z00_2883 =
																													(BgL_arg1802z00_2871
																													== BgL_classz00_2850);
																											}
																										else
																											{	/* Return/walk.scm 216 */
																												BgL_res2160z00_2883 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2299z00_4290 =
																							BgL_res2160z00_2883;
																					}
																			}
																		}
																		if (BgL_test2299z00_4290)
																			{	/* Return/walk.scm 218 */
																				bool_t BgL_test2303z00_4313;

																				{	/* Return/walk.scm 218 */
																					bool_t BgL_test2304z00_4314;

																					{	/* Return/walk.scm 218 */
																						obj_t BgL_tmpz00_4315;

																						BgL_tmpz00_4315 =
																							(((BgL_letzd2varzd2_bglt) COBJECT(
																									((BgL_letzd2varzd2_bglt)
																										BgL_nodez00_1757)))->
																							BgL_bindingsz00);
																						BgL_test2304z00_4314 =
																							PAIRP(BgL_tmpz00_4315);
																					}
																					if (BgL_test2304z00_4314)
																						{	/* Return/walk.scm 218 */
																							obj_t BgL_tmpz00_4319;

																							{	/* Return/walk.scm 218 */
																								obj_t BgL_pairz00_2884;

																								BgL_pairz00_2884 =
																									(((BgL_letzd2varzd2_bglt)
																										COBJECT((
																												(BgL_letzd2varzd2_bglt)
																												BgL_nodez00_1757)))->
																									BgL_bindingsz00);
																								BgL_tmpz00_4319 =
																									CDR(BgL_pairz00_2884);
																							}
																							BgL_test2303z00_4313 =
																								NULLP(BgL_tmpz00_4319);
																						}
																					else
																						{	/* Return/walk.scm 218 */
																							BgL_test2303z00_4313 =
																								((bool_t) 0);
																						}
																				}
																				if (BgL_test2303z00_4313)
																					{	/* Return/walk.scm 219 */
																						obj_t BgL_bindingz00_1767;

																						{	/* Return/walk.scm 219 */
																							obj_t BgL_pairz00_2885;

																							BgL_pairz00_2885 =
																								(((BgL_letzd2varzd2_bglt)
																									COBJECT((
																											(BgL_letzd2varzd2_bglt)
																											BgL_nodez00_1757)))->
																								BgL_bindingsz00);
																							BgL_bindingz00_1767 =
																								CAR(BgL_pairz00_2885);
																						}
																						{	/* Return/walk.scm 220 */
																							obj_t BgL_exprz00_1769;

																							BgL_exprz00_1769 =
																								CDR(
																								((obj_t) BgL_bindingz00_1767));
																							{	/* Return/walk.scm 221 */

																								{	/* Return/walk.scm 222 */
																									bool_t BgL_test2305z00_4329;

																									{	/* Return/walk.scm 222 */
																										obj_t BgL_classz00_2888;

																										BgL_classz00_2888 =
																											BGl_appz00zzast_nodez00;
																										if (BGL_OBJECTP
																											(BgL_exprz00_1769))
																											{	/* Return/walk.scm 222 */
																												BgL_objectz00_bglt
																													BgL_arg1807z00_2890;
																												BgL_arg1807z00_2890 =
																													(BgL_objectz00_bglt)
																													(BgL_exprz00_1769);
																												if (BGL_CONDEXPAND_ISA_ARCH64())
																													{	/* Return/walk.scm 222 */
																														long
																															BgL_idxz00_2896;
																														BgL_idxz00_2896 =
																															BGL_OBJECT_INHERITANCE_NUM
																															(BgL_arg1807z00_2890);
																														BgL_test2305z00_4329
																															=
																															(VECTOR_REF
																															(BGl_za2inheritancesza2z00zz__objectz00,
																																(BgL_idxz00_2896
																																	+ 3L)) ==
																															BgL_classz00_2888);
																													}
																												else
																													{	/* Return/walk.scm 222 */
																														bool_t
																															BgL_res2161z00_2921;
																														{	/* Return/walk.scm 222 */
																															obj_t
																																BgL_oclassz00_2904;
																															{	/* Return/walk.scm 222 */
																																obj_t
																																	BgL_arg1815z00_2912;
																																long
																																	BgL_arg1816z00_2913;
																																BgL_arg1815z00_2912
																																	=
																																	(BGl_za2classesza2z00zz__objectz00);
																																{	/* Return/walk.scm 222 */
																																	long
																																		BgL_arg1817z00_2914;
																																	BgL_arg1817z00_2914
																																		=
																																		BGL_OBJECT_CLASS_NUM
																																		(BgL_arg1807z00_2890);
																																	BgL_arg1816z00_2913
																																		=
																																		(BgL_arg1817z00_2914
																																		-
																																		OBJECT_TYPE);
																																}
																																BgL_oclassz00_2904
																																	=
																																	VECTOR_REF
																																	(BgL_arg1815z00_2912,
																																	BgL_arg1816z00_2913);
																															}
																															{	/* Return/walk.scm 222 */
																																bool_t
																																	BgL__ortest_1115z00_2905;
																																BgL__ortest_1115z00_2905
																																	=
																																	(BgL_classz00_2888
																																	==
																																	BgL_oclassz00_2904);
																																if (BgL__ortest_1115z00_2905)
																																	{	/* Return/walk.scm 222 */
																																		BgL_res2161z00_2921
																																			=
																																			BgL__ortest_1115z00_2905;
																																	}
																																else
																																	{	/* Return/walk.scm 222 */
																																		long
																																			BgL_odepthz00_2906;
																																		{	/* Return/walk.scm 222 */
																																			obj_t
																																				BgL_arg1804z00_2907;
																																			BgL_arg1804z00_2907
																																				=
																																				(BgL_oclassz00_2904);
																																			BgL_odepthz00_2906
																																				=
																																				BGL_CLASS_DEPTH
																																				(BgL_arg1804z00_2907);
																																		}
																																		if (
																																			(3L <
																																				BgL_odepthz00_2906))
																																			{	/* Return/walk.scm 222 */
																																				obj_t
																																					BgL_arg1802z00_2909;
																																				{	/* Return/walk.scm 222 */
																																					obj_t
																																						BgL_arg1803z00_2910;
																																					BgL_arg1803z00_2910
																																						=
																																						(BgL_oclassz00_2904);
																																					BgL_arg1802z00_2909
																																						=
																																						BGL_CLASS_ANCESTORS_REF
																																						(BgL_arg1803z00_2910,
																																						3L);
																																				}
																																				BgL_res2161z00_2921
																																					=
																																					(BgL_arg1802z00_2909
																																					==
																																					BgL_classz00_2888);
																																			}
																																		else
																																			{	/* Return/walk.scm 222 */
																																				BgL_res2161z00_2921
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																															}
																														}
																														BgL_test2305z00_4329
																															=
																															BgL_res2161z00_2921;
																													}
																											}
																										else
																											{	/* Return/walk.scm 222 */
																												BgL_test2305z00_4329 =
																													((bool_t) 0);
																											}
																									}
																									if (BgL_test2305z00_4329)
																										{	/* Return/walk.scm 222 */
																											if (NULLP(
																													(((BgL_appz00_bglt)
																															COBJECT((
																																	(BgL_appz00_bglt)
																																	BgL_exprz00_1769)))->
																														BgL_argsz00)))
																												{	/* Return/walk.scm 226 */
																													bool_t
																														BgL_test2311z00_4356;
																													{	/* Return/walk.scm 226 */
																														BgL_variablez00_bglt
																															BgL_arg1805z00_1777;
																														BgL_arg1805z00_1777
																															=
																															(((BgL_varz00_bglt) COBJECT((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_exprz00_1769)))->BgL_funz00)))->BgL_variablez00);
																														BgL_test2311z00_4356
																															=
																															(((obj_t)
																																BgL_arg1805z00_1777)
																															==
																															BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00);
																													}
																													if (BgL_test2311z00_4356)
																														{	/* Return/walk.scm 226 */
																															BgL_bindingz00_1892
																																=
																																BgL_bindingz00_1767;
																														}
																													else
																														{	/* Return/walk.scm 226 */
																															BgL_bindingz00_1892
																																= BFALSE;
																														}
																												}
																											else
																												{	/* Return/walk.scm 224 */
																													BgL_bindingz00_1892 =
																														BFALSE;
																												}
																										}
																									else
																										{	/* Return/walk.scm 222 */
																											BgL_bindingz00_1892 =
																												BFALSE;
																										}
																								}
																							}
																						}
																					}
																				else
																					{	/* Return/walk.scm 218 */
																						BgL_bindingz00_1892 = BFALSE;
																					}
																			}
																		else
																			{	/* Return/walk.scm 216 */
																				BgL_bindingz00_1892 = BFALSE;
																			}
																	}
																	if (CBOOL(BgL_bindingz00_1892))
																		{	/* Return/walk.scm 283 */
																			BgL_letzd2varzd2_bglt BgL_i1167z00_1893;

																			{	/* Return/walk.scm 283 */
																				obj_t BgL_pairz00_3224;

																				BgL_pairz00_3224 =
																					CDR(((obj_t) BgL_nodesz00_1884));
																				BgL_i1167z00_1893 =
																					((BgL_letzd2varzd2_bglt)
																					CAR(BgL_pairz00_3224));
																			}
																			{	/* Return/walk.scm 284 */
																				obj_t BgL_exitnodez00_1894;

																				{	/* Return/walk.scm 284 */
																					BgL_nodez00_bglt BgL_arg1926z00_1896;
																					obj_t BgL_arg1927z00_1897;

																					BgL_arg1926z00_1896 =
																						(((BgL_letzd2varzd2_bglt)
																							COBJECT(BgL_i1167z00_1893))->
																						BgL_bodyz00);
																					BgL_arg1927z00_1897 =
																						CAR(((obj_t) BgL_bindingz00_1892));
																					BgL_nodez00_1700 =
																						BgL_arg1926z00_1896;
																					BgL_varz00_1701 =
																						((BgL_localz00_bglt)
																						BgL_arg1927z00_1897);
																					{	/* Return/walk.scm 196 */
																						bool_t BgL_test2313z00_4375;

																						{	/* Return/walk.scm 196 */
																							obj_t BgL_classz00_2768;

																							BgL_classz00_2768 =
																								BGl_letzd2varzd2zzast_nodez00;
																							{	/* Return/walk.scm 196 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_2770;
																								{	/* Return/walk.scm 196 */
																									obj_t BgL_tmpz00_4376;

																									BgL_tmpz00_4376 =
																										((obj_t) BgL_nodez00_1700);
																									BgL_arg1807z00_2770 =
																										(BgL_objectz00_bglt)
																										(BgL_tmpz00_4376);
																								}
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* Return/walk.scm 196 */
																										long BgL_idxz00_2776;

																										BgL_idxz00_2776 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_2770);
																										BgL_test2313z00_4375 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_2776 +
																													3L)) ==
																											BgL_classz00_2768);
																									}
																								else
																									{	/* Return/walk.scm 196 */
																										bool_t BgL_res2158z00_2801;

																										{	/* Return/walk.scm 196 */
																											obj_t BgL_oclassz00_2784;

																											{	/* Return/walk.scm 196 */
																												obj_t
																													BgL_arg1815z00_2792;
																												long
																													BgL_arg1816z00_2793;
																												BgL_arg1815z00_2792 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* Return/walk.scm 196 */
																													long
																														BgL_arg1817z00_2794;
																													BgL_arg1817z00_2794 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_2770);
																													BgL_arg1816z00_2793 =
																														(BgL_arg1817z00_2794
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_2784 =
																													VECTOR_REF
																													(BgL_arg1815z00_2792,
																													BgL_arg1816z00_2793);
																											}
																											{	/* Return/walk.scm 196 */
																												bool_t
																													BgL__ortest_1115z00_2785;
																												BgL__ortest_1115z00_2785
																													=
																													(BgL_classz00_2768 ==
																													BgL_oclassz00_2784);
																												if (BgL__ortest_1115z00_2785)
																													{	/* Return/walk.scm 196 */
																														BgL_res2158z00_2801
																															=
																															BgL__ortest_1115z00_2785;
																													}
																												else
																													{	/* Return/walk.scm 196 */
																														long
																															BgL_odepthz00_2786;
																														{	/* Return/walk.scm 196 */
																															obj_t
																																BgL_arg1804z00_2787;
																															BgL_arg1804z00_2787
																																=
																																(BgL_oclassz00_2784);
																															BgL_odepthz00_2786
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_2787);
																														}
																														if (
																															(3L <
																																BgL_odepthz00_2786))
																															{	/* Return/walk.scm 196 */
																																obj_t
																																	BgL_arg1802z00_2789;
																																{	/* Return/walk.scm 196 */
																																	obj_t
																																		BgL_arg1803z00_2790;
																																	BgL_arg1803z00_2790
																																		=
																																		(BgL_oclassz00_2784);
																																	BgL_arg1802z00_2789
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_2790,
																																		3L);
																																}
																																BgL_res2158z00_2801
																																	=
																																	(BgL_arg1802z00_2789
																																	==
																																	BgL_classz00_2768);
																															}
																														else
																															{	/* Return/walk.scm 196 */
																																BgL_res2158z00_2801
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test2313z00_4375 =
																											BgL_res2158z00_2801;
																									}
																							}
																						}
																						if (BgL_test2313z00_4375)
																							{	/* Return/walk.scm 198 */
																								bool_t BgL_test2317z00_4398;

																								{	/* Return/walk.scm 198 */
																									bool_t BgL_test2318z00_4399;

																									{	/* Return/walk.scm 198 */
																										obj_t BgL_tmpz00_4400;

																										BgL_tmpz00_4400 =
																											(((BgL_letzd2varzd2_bglt)
																												COBJECT((
																														(BgL_letzd2varzd2_bglt)
																														BgL_nodez00_1700)))->
																											BgL_bindingsz00);
																										BgL_test2318z00_4399 =
																											PAIRP(BgL_tmpz00_4400);
																									}
																									if (BgL_test2318z00_4399)
																										{	/* Return/walk.scm 198 */
																											obj_t BgL_tmpz00_4404;

																											{	/* Return/walk.scm 198 */
																												obj_t BgL_pairz00_2802;

																												BgL_pairz00_2802 =
																													(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1700)))->BgL_bindingsz00);
																												BgL_tmpz00_4404 =
																													CDR(BgL_pairz00_2802);
																											}
																											BgL_test2317z00_4398 =
																												NULLP(BgL_tmpz00_4404);
																										}
																									else
																										{	/* Return/walk.scm 198 */
																											BgL_test2317z00_4398 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test2317z00_4398)
																									{	/* Return/walk.scm 199 */
																										obj_t BgL_bindingz00_1711;

																										{	/* Return/walk.scm 199 */
																											obj_t BgL_pairz00_2803;

																											BgL_pairz00_2803 =
																												(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1700)))->BgL_bindingsz00);
																											BgL_bindingz00_1711 =
																												CAR(BgL_pairz00_2803);
																										}
																										{	/* Return/walk.scm 199 */
																											obj_t BgL_nbodyz00_1712;

																											{	/* Return/walk.scm 200 */
																												BgL_nodez00_bglt
																													BgL_arg1722z00_1721;
																												obj_t
																													BgL_arg1724z00_1722;
																												BgL_arg1722z00_1721 =
																													(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1700)))->BgL_bodyz00);
																												BgL_arg1724z00_1722 =
																													CAR(((obj_t)
																														BgL_bindingz00_1711));
																												BgL_nbodyz00_1712 =
																													BGl_step6ze70ze7zzreturn_walkz00
																													(BgL_arg1722z00_1721,
																													((BgL_localz00_bglt)
																														BgL_arg1724z00_1722));
																											}
																											{	/* Return/walk.scm 200 */

																												if (CBOOL
																													(BgL_nbodyz00_1712))
																													{	/* Return/walk.scm 202 */
																														BgL_letzd2varzd2_bglt
																															BgL_new1136z00_1714;
																														{	/* Return/walk.scm 202 */
																															BgL_letzd2varzd2_bglt
																																BgL_new1143z00_1719;
																															BgL_new1143z00_1719
																																=
																																(
																																(BgL_letzd2varzd2_bglt)
																																BOBJECT
																																(GC_MALLOC
																																	(sizeof(struct
																																			BgL_letzd2varzd2_bgl))));
																															{	/* Return/walk.scm 202 */
																																long
																																	BgL_arg1720z00_1720;
																																BgL_arg1720z00_1720
																																	=
																																	BGL_CLASS_NUM
																																	(BGl_letzd2varzd2zzast_nodez00);
																																BGL_OBJECT_CLASS_NUM_SET
																																	(((BgL_objectz00_bglt) BgL_new1143z00_1719), BgL_arg1720z00_1720);
																															}
																															{	/* Return/walk.scm 202 */
																																BgL_objectz00_bglt
																																	BgL_tmpz00_4424;
																																BgL_tmpz00_4424
																																	=
																																	(
																																	(BgL_objectz00_bglt)
																																	BgL_new1143z00_1719);
																																BGL_OBJECT_WIDENING_SET
																																	(BgL_tmpz00_4424,
																																	BFALSE);
																															}
																															((BgL_objectz00_bglt) BgL_new1143z00_1719);
																															BgL_new1136z00_1714
																																=
																																BgL_new1143z00_1719;
																														}
																														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1136z00_1714)))->BgL_locz00) = ((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_1700))->BgL_locz00)), BUNSPEC);
																														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1136z00_1714)))->BgL_typez00) = ((BgL_typez00_bglt) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_1700))->BgL_typez00)), BUNSPEC);
																														((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1136z00_1714)))->BgL_sidezd2effectzd2) = ((obj_t) (((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_nodez00_1700)))->BgL_sidezd2effectzd2)), BUNSPEC);
																														((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1136z00_1714)))->BgL_keyz00) = ((obj_t) (((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_nodez00_1700)))->BgL_keyz00)), BUNSPEC);
																														((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1136z00_1714))->BgL_bindingsz00) = ((obj_t) (((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1700)))->BgL_bindingsz00)), BUNSPEC);
																														((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1136z00_1714))->BgL_bodyz00) = ((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_1712)), BUNSPEC);
																														((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1136z00_1714))->BgL_removablezf3zf3) = ((bool_t) (((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1700)))->BgL_removablezf3zf3)), BUNSPEC);
																														BgL_exitnodez00_1894
																															=
																															((obj_t)
																															BgL_new1136z00_1714);
																													}
																												else
																													{	/* Return/walk.scm 201 */
																														BgL_exitnodez00_1894
																															= BFALSE;
																													}
																											}
																										}
																									}
																								else
																									{	/* Return/walk.scm 198 */
																										BgL_exitnodez00_1894 =
																											BFALSE;
																									}
																							}
																						else
																							{	/* Return/walk.scm 196 */
																								BgL_exitnodez00_1894 = BFALSE;
																							}
																					}
																				}
																				if (CBOOL(BgL_exitnodez00_1894))
																					{	/* Return/walk.scm 286 */
																						obj_t BgL_arg1925z00_1895;

																						BgL_arg1925z00_1895 =
																							CAR(
																							((obj_t) BgL_bindingz00_1892));
																						BgL__ortest_1178z00_1590 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1925z00_1895,
																							BgL_exitnodez00_1894);
																					}
																				else
																					{	/* Return/walk.scm 285 */
																						BgL__ortest_1178z00_1590 = BFALSE;
																					}
																			}
																		}
																	else
																		{	/* Return/walk.scm 282 */
																			BgL__ortest_1178z00_1590 = BFALSE;
																		}
																}
															else
																{	/* Return/walk.scm 280 */
																	BgL__ortest_1178z00_1590 = BFALSE;
																}
														}
													}
												}
											else
												{	/* Return/walk.scm 300 */
													BgL__ortest_1178z00_1590 = BFALSE;
												}
										}
									else
										{	/* Return/walk.scm 298 */
											BgL__ortest_1178z00_1590 = BFALSE;
										}
								}
							}
							if (CBOOL(BgL__ortest_1178z00_1590))
								{	/* Return/walk.scm 322 */
									return BgL__ortest_1178z00_1590;
								}
							else
								{	/* Return/walk.scm 323 */
									BgL_nodez00_bglt BgL_arg1513z00_1591;
									BgL_varz00_bglt BgL_arg1514z00_1592;

									BgL_arg1513z00_1591 =
										(((BgL_setzd2exzd2itz00_bglt) COBJECT(
												((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_19)))->
										BgL_bodyz00);
									BgL_arg1514z00_1592 =
										(((BgL_setzd2exzd2itz00_bglt)
											COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_19)))->
										BgL_varz00);
									BgL_bodyz00_1938 = BgL_arg1513z00_1591;
									BgL_varz00_1939 = BgL_arg1514z00_1592;
									{	/* Return/walk.scm 307 */
										bool_t BgL_test2322z00_4464;

										{	/* Return/walk.scm 307 */
											obj_t BgL_classz00_3307;

											BgL_classz00_3307 = BGl_letzd2varzd2zzast_nodez00;
											{	/* Return/walk.scm 307 */
												BgL_objectz00_bglt BgL_arg1807z00_3309;

												{	/* Return/walk.scm 307 */
													obj_t BgL_tmpz00_4465;

													BgL_tmpz00_4465 = ((obj_t) BgL_bodyz00_1938);
													BgL_arg1807z00_3309 =
														(BgL_objectz00_bglt) (BgL_tmpz00_4465);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Return/walk.scm 307 */
														long BgL_idxz00_3315;

														BgL_idxz00_3315 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3309);
														BgL_test2322z00_4464 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3315 + 3L)) == BgL_classz00_3307);
													}
												else
													{	/* Return/walk.scm 307 */
														bool_t BgL_res2172z00_3340;

														{	/* Return/walk.scm 307 */
															obj_t BgL_oclassz00_3323;

															{	/* Return/walk.scm 307 */
																obj_t BgL_arg1815z00_3331;
																long BgL_arg1816z00_3332;

																BgL_arg1815z00_3331 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Return/walk.scm 307 */
																	long BgL_arg1817z00_3333;

																	BgL_arg1817z00_3333 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3309);
																	BgL_arg1816z00_3332 =
																		(BgL_arg1817z00_3333 - OBJECT_TYPE);
																}
																BgL_oclassz00_3323 =
																	VECTOR_REF(BgL_arg1815z00_3331,
																	BgL_arg1816z00_3332);
															}
															{	/* Return/walk.scm 307 */
																bool_t BgL__ortest_1115z00_3324;

																BgL__ortest_1115z00_3324 =
																	(BgL_classz00_3307 == BgL_oclassz00_3323);
																if (BgL__ortest_1115z00_3324)
																	{	/* Return/walk.scm 307 */
																		BgL_res2172z00_3340 =
																			BgL__ortest_1115z00_3324;
																	}
																else
																	{	/* Return/walk.scm 307 */
																		long BgL_odepthz00_3325;

																		{	/* Return/walk.scm 307 */
																			obj_t BgL_arg1804z00_3326;

																			BgL_arg1804z00_3326 =
																				(BgL_oclassz00_3323);
																			BgL_odepthz00_3325 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3326);
																		}
																		if ((3L < BgL_odepthz00_3325))
																			{	/* Return/walk.scm 307 */
																				obj_t BgL_arg1802z00_3328;

																				{	/* Return/walk.scm 307 */
																					obj_t BgL_arg1803z00_3329;

																					BgL_arg1803z00_3329 =
																						(BgL_oclassz00_3323);
																					BgL_arg1802z00_3328 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3329, 3L);
																				}
																				BgL_res2172z00_3340 =
																					(BgL_arg1802z00_3328 ==
																					BgL_classz00_3307);
																			}
																		else
																			{	/* Return/walk.scm 307 */
																				BgL_res2172z00_3340 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2322z00_4464 = BgL_res2172z00_3340;
													}
											}
										}
										if (BgL_test2322z00_4464)
											{	/* Return/walk.scm 309 */
												bool_t BgL_test2326z00_4487;

												{	/* Return/walk.scm 309 */
													bool_t BgL_test2327z00_4488;

													{	/* Return/walk.scm 309 */
														BgL_nodez00_bglt BgL_arg1981z00_1974;

														BgL_arg1981z00_1974 =
															(((BgL_letzd2varzd2_bglt) COBJECT(
																	((BgL_letzd2varzd2_bglt) BgL_bodyz00_1938)))->
															BgL_bodyz00);
														{	/* Return/walk.scm 309 */
															obj_t BgL_classz00_3341;

															BgL_classz00_3341 = BGl_sequencez00zzast_nodez00;
															{	/* Return/walk.scm 309 */
																BgL_objectz00_bglt BgL_arg1807z00_3343;

																{	/* Return/walk.scm 309 */
																	obj_t BgL_tmpz00_4491;

																	BgL_tmpz00_4491 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1981z00_1974));
																	BgL_arg1807z00_3343 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_4491);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Return/walk.scm 309 */
																		long BgL_idxz00_3349;

																		BgL_idxz00_3349 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3343);
																		BgL_test2327z00_4488 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3349 + 3L)) ==
																			BgL_classz00_3341);
																	}
																else
																	{	/* Return/walk.scm 309 */
																		bool_t BgL_res2173z00_3374;

																		{	/* Return/walk.scm 309 */
																			obj_t BgL_oclassz00_3357;

																			{	/* Return/walk.scm 309 */
																				obj_t BgL_arg1815z00_3365;
																				long BgL_arg1816z00_3366;

																				BgL_arg1815z00_3365 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Return/walk.scm 309 */
																					long BgL_arg1817z00_3367;

																					BgL_arg1817z00_3367 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3343);
																					BgL_arg1816z00_3366 =
																						(BgL_arg1817z00_3367 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3357 =
																					VECTOR_REF(BgL_arg1815z00_3365,
																					BgL_arg1816z00_3366);
																			}
																			{	/* Return/walk.scm 309 */
																				bool_t BgL__ortest_1115z00_3358;

																				BgL__ortest_1115z00_3358 =
																					(BgL_classz00_3341 ==
																					BgL_oclassz00_3357);
																				if (BgL__ortest_1115z00_3358)
																					{	/* Return/walk.scm 309 */
																						BgL_res2173z00_3374 =
																							BgL__ortest_1115z00_3358;
																					}
																				else
																					{	/* Return/walk.scm 309 */
																						long BgL_odepthz00_3359;

																						{	/* Return/walk.scm 309 */
																							obj_t BgL_arg1804z00_3360;

																							BgL_arg1804z00_3360 =
																								(BgL_oclassz00_3357);
																							BgL_odepthz00_3359 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3360);
																						}
																						if ((3L < BgL_odepthz00_3359))
																							{	/* Return/walk.scm 309 */
																								obj_t BgL_arg1802z00_3362;

																								{	/* Return/walk.scm 309 */
																									obj_t BgL_arg1803z00_3363;

																									BgL_arg1803z00_3363 =
																										(BgL_oclassz00_3357);
																									BgL_arg1802z00_3362 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3363, 3L);
																								}
																								BgL_res2173z00_3374 =
																									(BgL_arg1802z00_3362 ==
																									BgL_classz00_3341);
																							}
																						else
																							{	/* Return/walk.scm 309 */
																								BgL_res2173z00_3374 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2327z00_4488 = BgL_res2173z00_3374;
																	}
															}
														}
													}
													if (BgL_test2327z00_4488)
														{	/* Return/walk.scm 310 */
															bool_t BgL_test2331z00_4514;

															{	/* Return/walk.scm 310 */
																obj_t BgL_tmpz00_4515;

																BgL_tmpz00_4515 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_bodyz00_1938)))->BgL_bindingsz00);
																BgL_test2331z00_4514 = PAIRP(BgL_tmpz00_4515);
															}
															if (BgL_test2331z00_4514)
																{	/* Return/walk.scm 311 */
																	obj_t BgL_tmpz00_4519;

																	{	/* Return/walk.scm 311 */
																		obj_t BgL_pairz00_3375;

																		BgL_pairz00_3375 =
																			(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_bodyz00_1938)))->
																			BgL_bindingsz00);
																		BgL_tmpz00_4519 = CDR(BgL_pairz00_3375);
																	}
																	BgL_test2326z00_4487 = NULLP(BgL_tmpz00_4519);
																}
															else
																{	/* Return/walk.scm 310 */
																	BgL_test2326z00_4487 = ((bool_t) 0);
																}
														}
													else
														{	/* Return/walk.scm 309 */
															BgL_test2326z00_4487 = ((bool_t) 0);
														}
												}
												if (BgL_test2326z00_4487)
													{	/* Return/walk.scm 312 */
														bool_t BgL_test2332z00_4524;

														{	/* Return/walk.scm 312 */
															obj_t BgL_arg1976z00_1967;

															{	/* Return/walk.scm 312 */
																obj_t BgL_pairz00_3376;

																BgL_pairz00_3376 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_bodyz00_1938)))->BgL_bindingsz00);
																BgL_arg1976z00_1967 =
																	CDR(CAR(BgL_pairz00_3376));
															}
															{	/* Return/walk.scm 312 */
																obj_t BgL_classz00_3380;

																BgL_classz00_3380 = BGl_appz00zzast_nodez00;
																if (BGL_OBJECTP(BgL_arg1976z00_1967))
																	{	/* Return/walk.scm 312 */
																		BgL_objectz00_bglt BgL_arg1807z00_3382;

																		BgL_arg1807z00_3382 =
																			(BgL_objectz00_bglt)
																			(BgL_arg1976z00_1967);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Return/walk.scm 312 */
																				long BgL_idxz00_3388;

																				BgL_idxz00_3388 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_3382);
																				BgL_test2332z00_4524 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_3388 + 3L)) ==
																					BgL_classz00_3380);
																			}
																		else
																			{	/* Return/walk.scm 312 */
																				bool_t BgL_res2174z00_3413;

																				{	/* Return/walk.scm 312 */
																					obj_t BgL_oclassz00_3396;

																					{	/* Return/walk.scm 312 */
																						obj_t BgL_arg1815z00_3404;
																						long BgL_arg1816z00_3405;

																						BgL_arg1815z00_3404 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Return/walk.scm 312 */
																							long BgL_arg1817z00_3406;

																							BgL_arg1817z00_3406 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_3382);
																							BgL_arg1816z00_3405 =
																								(BgL_arg1817z00_3406 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_3396 =
																							VECTOR_REF(BgL_arg1815z00_3404,
																							BgL_arg1816z00_3405);
																					}
																					{	/* Return/walk.scm 312 */
																						bool_t BgL__ortest_1115z00_3397;

																						BgL__ortest_1115z00_3397 =
																							(BgL_classz00_3380 ==
																							BgL_oclassz00_3396);
																						if (BgL__ortest_1115z00_3397)
																							{	/* Return/walk.scm 312 */
																								BgL_res2174z00_3413 =
																									BgL__ortest_1115z00_3397;
																							}
																						else
																							{	/* Return/walk.scm 312 */
																								long BgL_odepthz00_3398;

																								{	/* Return/walk.scm 312 */
																									obj_t BgL_arg1804z00_3399;

																									BgL_arg1804z00_3399 =
																										(BgL_oclassz00_3396);
																									BgL_odepthz00_3398 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_3399);
																								}
																								if ((3L < BgL_odepthz00_3398))
																									{	/* Return/walk.scm 312 */
																										obj_t BgL_arg1802z00_3401;

																										{	/* Return/walk.scm 312 */
																											obj_t BgL_arg1803z00_3402;

																											BgL_arg1803z00_3402 =
																												(BgL_oclassz00_3396);
																											BgL_arg1802z00_3401 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_3402,
																												3L);
																										}
																										BgL_res2174z00_3413 =
																											(BgL_arg1802z00_3401 ==
																											BgL_classz00_3380);
																									}
																								else
																									{	/* Return/walk.scm 312 */
																										BgL_res2174z00_3413 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2332z00_4524 =
																					BgL_res2174z00_3413;
																			}
																	}
																else
																	{	/* Return/walk.scm 312 */
																		BgL_test2332z00_4524 = ((bool_t) 0);
																	}
															}
														}
														if (BgL_test2332z00_4524)
															{	/* Return/walk.scm 313 */
																BgL_appz00_bglt BgL_i1173z00_1955;

																{	/* Return/walk.scm 313 */
																	obj_t BgL_pairz00_3414;

																	BgL_pairz00_3414 =
																		(((BgL_letzd2varzd2_bglt) COBJECT(
																				((BgL_letzd2varzd2_bglt)
																					BgL_bodyz00_1938)))->BgL_bindingsz00);
																	{	/* Return/walk.scm 313 */
																		obj_t BgL_pairz00_3417;

																		BgL_pairz00_3417 = CAR(BgL_pairz00_3414);
																		BgL_i1173z00_1955 =
																			((BgL_appz00_bglt) CDR(BgL_pairz00_3417));
																	}
																}
																{	/* Return/walk.scm 314 */
																	BgL_varz00_bglt BgL_i1174z00_1956;

																	BgL_i1174z00_1956 =
																		(((BgL_appz00_bglt)
																			COBJECT(BgL_i1173z00_1955))->BgL_funz00);
																	{	/* Return/walk.scm 315 */
																		bool_t BgL_test2337z00_4557;

																		{	/* Return/walk.scm 315 */
																			BgL_variablez00_bglt BgL_arg1974z00_1965;

																			BgL_arg1974z00_1965 =
																				(((BgL_varz00_bglt)
																					COBJECT(BgL_i1174z00_1956))->
																				BgL_variablez00);
																			BgL_test2337z00_4557 =
																				(((obj_t) BgL_arg1974z00_1965) ==
																				BGl_za2currentzd2dynamiczd2envza2z00zzreturn_walkz00);
																		}
																		if (BgL_test2337z00_4557)
																			{	/* Return/walk.scm 316 */
																				BgL_sequencez00_bglt BgL_i1175z00_1959;

																				BgL_i1175z00_1959 =
																					((BgL_sequencez00_bglt)
																					(((BgL_letzd2varzd2_bglt) COBJECT(
																								((BgL_letzd2varzd2_bglt)
																									BgL_bodyz00_1938)))->
																						BgL_bodyz00));
																				{	/* Return/walk.scm 318 */
																					obj_t BgL_arg1970z00_1961;
																					BgL_variablez00_bglt
																						BgL_arg1971z00_1962;
																					obj_t BgL_arg1972z00_1963;

																					BgL_arg1970z00_1961 =
																						(((BgL_sequencez00_bglt)
																							COBJECT(BgL_i1175z00_1959))->
																						BgL_nodesz00);
																					BgL_arg1971z00_1962 =
																						(((BgL_varz00_bglt)
																							COBJECT(BgL_varz00_1939))->
																						BgL_variablez00);
																					{	/* Return/walk.scm 318 */
																						obj_t BgL_pairz00_3418;

																						BgL_pairz00_3418 =
																							(((BgL_letzd2varzd2_bglt) COBJECT(
																									((BgL_letzd2varzd2_bglt)
																										BgL_bodyz00_1938)))->
																							BgL_bindingsz00);
																						BgL_arg1972z00_1963 =
																							CAR(CAR(BgL_pairz00_3418));
																					}
																					BgL_nodesz00_1902 =
																						BgL_arg1970z00_1961;
																					BgL_varz00_1903 =
																						((BgL_localz00_bglt)
																						BgL_arg1971z00_1962);
																					BgL_envz00_1904 = BgL_arg1972z00_1963;
																					{	/* Return/walk.scm 289 */
																						bool_t BgL_test2338z00_4570;

																						if (
																							(bgl_list_length
																								(BgL_nodesz00_1902) == 2L))
																							{	/* Return/walk.scm 289 */
																								obj_t BgL_arg1941z00_1919;

																								BgL_arg1941z00_1919 =
																									CAR(
																									((obj_t) BgL_nodesz00_1902));
																								BgL_nodez00_1851 =
																									((BgL_nodez00_bglt)
																									BgL_arg1941z00_1919);
																								BgL_exitvarz00_1852 =
																									BgL_varz00_1903;
																								BgL_envz00_1853 =
																									BgL_envz00_1904;
																								{	/* Return/walk.scm 261 */
																									bool_t BgL_test2340z00_4576;

																									{	/* Return/walk.scm 261 */
																										obj_t BgL_classz00_3102;

																										BgL_classz00_3102 =
																											BGl_appz00zzast_nodez00;
																										{	/* Return/walk.scm 261 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_3104;
																											{	/* Return/walk.scm 261 */
																												obj_t BgL_tmpz00_4577;

																												BgL_tmpz00_4577 =
																													((obj_t)
																													BgL_nodez00_1851);
																												BgL_arg1807z00_3104 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_4577);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Return/walk.scm 261 */
																													long BgL_idxz00_3110;

																													BgL_idxz00_3110 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_3104);
																													BgL_test2340z00_4576 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_3110 +
																																3L)) ==
																														BgL_classz00_3102);
																												}
																											else
																												{	/* Return/walk.scm 261 */
																													bool_t
																														BgL_res2167z00_3135;
																													{	/* Return/walk.scm 261 */
																														obj_t
																															BgL_oclassz00_3118;
																														{	/* Return/walk.scm 261 */
																															obj_t
																																BgL_arg1815z00_3126;
																															long
																																BgL_arg1816z00_3127;
																															BgL_arg1815z00_3126
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Return/walk.scm 261 */
																																long
																																	BgL_arg1817z00_3128;
																																BgL_arg1817z00_3128
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_3104);
																																BgL_arg1816z00_3127
																																	=
																																	(BgL_arg1817z00_3128
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_3118
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_3126,
																																BgL_arg1816z00_3127);
																														}
																														{	/* Return/walk.scm 261 */
																															bool_t
																																BgL__ortest_1115z00_3119;
																															BgL__ortest_1115z00_3119
																																=
																																(BgL_classz00_3102
																																==
																																BgL_oclassz00_3118);
																															if (BgL__ortest_1115z00_3119)
																																{	/* Return/walk.scm 261 */
																																	BgL_res2167z00_3135
																																		=
																																		BgL__ortest_1115z00_3119;
																																}
																															else
																																{	/* Return/walk.scm 261 */
																																	long
																																		BgL_odepthz00_3120;
																																	{	/* Return/walk.scm 261 */
																																		obj_t
																																			BgL_arg1804z00_3121;
																																		BgL_arg1804z00_3121
																																			=
																																			(BgL_oclassz00_3118);
																																		BgL_odepthz00_3120
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_3121);
																																	}
																																	if (
																																		(3L <
																																			BgL_odepthz00_3120))
																																		{	/* Return/walk.scm 261 */
																																			obj_t
																																				BgL_arg1802z00_3123;
																																			{	/* Return/walk.scm 261 */
																																				obj_t
																																					BgL_arg1803z00_3124;
																																				BgL_arg1803z00_3124
																																					=
																																					(BgL_oclassz00_3118);
																																				BgL_arg1802z00_3123
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_3124,
																																					3L);
																																			}
																																			BgL_res2167z00_3135
																																				=
																																				(BgL_arg1802z00_3123
																																				==
																																				BgL_classz00_3102);
																																		}
																																	else
																																		{	/* Return/walk.scm 261 */
																																			BgL_res2167z00_3135
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test2340z00_4576 =
																														BgL_res2167z00_3135;
																												}
																										}
																									}
																									if (BgL_test2340z00_4576)
																										{	/* Return/walk.scm 263 */
																											BgL_varz00_bglt
																												BgL_i1164z00_1857;
																											BgL_i1164z00_1857 =
																												(((BgL_appz00_bglt)
																													COBJECT((
																															(BgL_appz00_bglt)
																															BgL_nodez00_1851)))->
																												BgL_funz00);
																											{	/* Return/walk.scm 265 */
																												bool_t
																													BgL_test2344z00_4601;
																												{	/* Return/walk.scm 265 */
																													BgL_variablez00_bglt
																														BgL_arg1916z00_1883;
																													BgL_arg1916z00_1883 =
																														(((BgL_varz00_bglt)
																															COBJECT
																															(BgL_i1164z00_1857))->
																														BgL_variablez00);
																													BgL_test2344z00_4601 =
																														(((obj_t)
																															BgL_arg1916z00_1883)
																														==
																														BGl_za2envzd2pushzd2exitz12za2z12zzreturn_walkz00);
																												}
																												if (BgL_test2344z00_4601)
																													{	/* Return/walk.scm 265 */
																														if (
																															(bgl_list_length(
																																	(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nodez00_1851)))->BgL_argsz00)) == 3L))
																															{	/* Return/walk.scm 269 */
																																bool_t
																																	BgL_test2346z00_4610;
																																{	/* Return/walk.scm 269 */
																																	obj_t
																																		BgL_arg1911z00_1879;
																																	{	/* Return/walk.scm 269 */
																																		obj_t
																																			BgL_pairz00_3137;
																																		BgL_pairz00_3137
																																			=
																																			(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nodez00_1851)))->BgL_argsz00);
																																		BgL_arg1911z00_1879
																																			=
																																			CAR(CDR
																																			(BgL_pairz00_3137));
																																	}
																																	{	/* Return/walk.scm 269 */
																																		obj_t
																																			BgL_classz00_3141;
																																		BgL_classz00_3141
																																			=
																																			BGl_varz00zzast_nodez00;
																																		if (BGL_OBJECTP(BgL_arg1911z00_1879))
																																			{	/* Return/walk.scm 269 */
																																				BgL_objectz00_bglt
																																					BgL_arg1807z00_3143;
																																				BgL_arg1807z00_3143
																																					=
																																					(BgL_objectz00_bglt)
																																					(BgL_arg1911z00_1879);
																																				if (BGL_CONDEXPAND_ISA_ARCH64())
																																					{	/* Return/walk.scm 269 */
																																						long
																																							BgL_idxz00_3149;
																																						BgL_idxz00_3149
																																							=
																																							BGL_OBJECT_INHERITANCE_NUM
																																							(BgL_arg1807z00_3143);
																																						BgL_test2346z00_4610
																																							=
																																							(VECTOR_REF
																																							(BGl_za2inheritancesza2z00zz__objectz00,
																																								(BgL_idxz00_3149
																																									+
																																									2L))
																																							==
																																							BgL_classz00_3141);
																																					}
																																				else
																																					{	/* Return/walk.scm 269 */
																																						bool_t
																																							BgL_res2168z00_3174;
																																						{	/* Return/walk.scm 269 */
																																							obj_t
																																								BgL_oclassz00_3157;
																																							{	/* Return/walk.scm 269 */
																																								obj_t
																																									BgL_arg1815z00_3165;
																																								long
																																									BgL_arg1816z00_3166;
																																								BgL_arg1815z00_3165
																																									=
																																									(BGl_za2classesza2z00zz__objectz00);
																																								{	/* Return/walk.scm 269 */
																																									long
																																										BgL_arg1817z00_3167;
																																									BgL_arg1817z00_3167
																																										=
																																										BGL_OBJECT_CLASS_NUM
																																										(BgL_arg1807z00_3143);
																																									BgL_arg1816z00_3166
																																										=
																																										(BgL_arg1817z00_3167
																																										-
																																										OBJECT_TYPE);
																																								}
																																								BgL_oclassz00_3157
																																									=
																																									VECTOR_REF
																																									(BgL_arg1815z00_3165,
																																									BgL_arg1816z00_3166);
																																							}
																																							{	/* Return/walk.scm 269 */
																																								bool_t
																																									BgL__ortest_1115z00_3158;
																																								BgL__ortest_1115z00_3158
																																									=
																																									(BgL_classz00_3141
																																									==
																																									BgL_oclassz00_3157);
																																								if (BgL__ortest_1115z00_3158)
																																									{	/* Return/walk.scm 269 */
																																										BgL_res2168z00_3174
																																											=
																																											BgL__ortest_1115z00_3158;
																																									}
																																								else
																																									{	/* Return/walk.scm 269 */
																																										long
																																											BgL_odepthz00_3159;
																																										{	/* Return/walk.scm 269 */
																																											obj_t
																																												BgL_arg1804z00_3160;
																																											BgL_arg1804z00_3160
																																												=
																																												(BgL_oclassz00_3157);
																																											BgL_odepthz00_3159
																																												=
																																												BGL_CLASS_DEPTH
																																												(BgL_arg1804z00_3160);
																																										}
																																										if ((2L < BgL_odepthz00_3159))
																																											{	/* Return/walk.scm 269 */
																																												obj_t
																																													BgL_arg1802z00_3162;
																																												{	/* Return/walk.scm 269 */
																																													obj_t
																																														BgL_arg1803z00_3163;
																																													BgL_arg1803z00_3163
																																														=
																																														(BgL_oclassz00_3157);
																																													BgL_arg1802z00_3162
																																														=
																																														BGL_CLASS_ANCESTORS_REF
																																														(BgL_arg1803z00_3163,
																																														2L);
																																												}
																																												BgL_res2168z00_3174
																																													=
																																													(BgL_arg1802z00_3162
																																													==
																																													BgL_classz00_3141);
																																											}
																																										else
																																											{	/* Return/walk.scm 269 */
																																												BgL_res2168z00_3174
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																							}
																																						}
																																						BgL_test2346z00_4610
																																							=
																																							BgL_res2168z00_3174;
																																					}
																																			}
																																		else
																																			{	/* Return/walk.scm 269 */
																																				BgL_test2346z00_4610
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																}
																																if (BgL_test2346z00_4610)
																																	{	/* Return/walk.scm 270 */
																																		BgL_varz00_bglt
																																			BgL_i1165z00_1866;
																																		{	/* Return/walk.scm 270 */
																																			obj_t
																																				BgL_pairz00_3175;
																																			BgL_pairz00_3175
																																				=
																																				(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nodez00_1851)))->BgL_argsz00);
																																			{	/* Return/walk.scm 270 */
																																				obj_t
																																					BgL_pairz00_3178;
																																				BgL_pairz00_3178
																																					=
																																					CDR
																																					(BgL_pairz00_3175);
																																				BgL_i1165z00_1866
																																					=
																																					(
																																					(BgL_varz00_bglt)
																																					CAR
																																					(BgL_pairz00_3178));
																																			}
																																		}
																																		if (
																																			(((obj_t)
																																					(((BgL_varz00_bglt) COBJECT(BgL_i1165z00_1866))->BgL_variablez00)) == ((obj_t) BgL_exitvarz00_1852)))
																																			{	/* Return/walk.scm 274 */
																																				bool_t
																																					BgL_test2352z00_4647;
																																				{	/* Return/walk.scm 274 */
																																					obj_t
																																						BgL_arg1903z00_1875;
																																					{	/* Return/walk.scm 274 */
																																						obj_t
																																							BgL_pairz00_3179;
																																						BgL_pairz00_3179
																																							=
																																							(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nodez00_1851)))->BgL_argsz00);
																																						BgL_arg1903z00_1875
																																							=
																																							CAR
																																							(BgL_pairz00_3179);
																																					}
																																					{	/* Return/walk.scm 274 */
																																						obj_t
																																							BgL_classz00_3180;
																																						BgL_classz00_3180
																																							=
																																							BGl_varz00zzast_nodez00;
																																						if (BGL_OBJECTP(BgL_arg1903z00_1875))
																																							{	/* Return/walk.scm 274 */
																																								BgL_objectz00_bglt
																																									BgL_arg1807z00_3182;
																																								BgL_arg1807z00_3182
																																									=
																																									(BgL_objectz00_bglt)
																																									(BgL_arg1903z00_1875);
																																								if (BGL_CONDEXPAND_ISA_ARCH64())
																																									{	/* Return/walk.scm 274 */
																																										long
																																											BgL_idxz00_3188;
																																										BgL_idxz00_3188
																																											=
																																											BGL_OBJECT_INHERITANCE_NUM
																																											(BgL_arg1807z00_3182);
																																										BgL_test2352z00_4647
																																											=
																																											(VECTOR_REF
																																											(BGl_za2inheritancesza2z00zz__objectz00,
																																												(BgL_idxz00_3188
																																													+
																																													2L))
																																											==
																																											BgL_classz00_3180);
																																									}
																																								else
																																									{	/* Return/walk.scm 274 */
																																										bool_t
																																											BgL_res2169z00_3213;
																																										{	/* Return/walk.scm 274 */
																																											obj_t
																																												BgL_oclassz00_3196;
																																											{	/* Return/walk.scm 274 */
																																												obj_t
																																													BgL_arg1815z00_3204;
																																												long
																																													BgL_arg1816z00_3205;
																																												BgL_arg1815z00_3204
																																													=
																																													(BGl_za2classesza2z00zz__objectz00);
																																												{	/* Return/walk.scm 274 */
																																													long
																																														BgL_arg1817z00_3206;
																																													BgL_arg1817z00_3206
																																														=
																																														BGL_OBJECT_CLASS_NUM
																																														(BgL_arg1807z00_3182);
																																													BgL_arg1816z00_3205
																																														=
																																														(BgL_arg1817z00_3206
																																														-
																																														OBJECT_TYPE);
																																												}
																																												BgL_oclassz00_3196
																																													=
																																													VECTOR_REF
																																													(BgL_arg1815z00_3204,
																																													BgL_arg1816z00_3205);
																																											}
																																											{	/* Return/walk.scm 274 */
																																												bool_t
																																													BgL__ortest_1115z00_3197;
																																												BgL__ortest_1115z00_3197
																																													=
																																													(BgL_classz00_3180
																																													==
																																													BgL_oclassz00_3196);
																																												if (BgL__ortest_1115z00_3197)
																																													{	/* Return/walk.scm 274 */
																																														BgL_res2169z00_3213
																																															=
																																															BgL__ortest_1115z00_3197;
																																													}
																																												else
																																													{	/* Return/walk.scm 274 */
																																														long
																																															BgL_odepthz00_3198;
																																														{	/* Return/walk.scm 274 */
																																															obj_t
																																																BgL_arg1804z00_3199;
																																															BgL_arg1804z00_3199
																																																=
																																																(BgL_oclassz00_3196);
																																															BgL_odepthz00_3198
																																																=
																																																BGL_CLASS_DEPTH
																																																(BgL_arg1804z00_3199);
																																														}
																																														if ((2L < BgL_odepthz00_3198))
																																															{	/* Return/walk.scm 274 */
																																																obj_t
																																																	BgL_arg1802z00_3201;
																																																{	/* Return/walk.scm 274 */
																																																	obj_t
																																																		BgL_arg1803z00_3202;
																																																	BgL_arg1803z00_3202
																																																		=
																																																		(BgL_oclassz00_3196);
																																																	BgL_arg1802z00_3201
																																																		=
																																																		BGL_CLASS_ANCESTORS_REF
																																																		(BgL_arg1803z00_3202,
																																																		2L);
																																																}
																																																BgL_res2169z00_3213
																																																	=
																																																	(BgL_arg1802z00_3201
																																																	==
																																																	BgL_classz00_3180);
																																															}
																																														else
																																															{	/* Return/walk.scm 274 */
																																																BgL_res2169z00_3213
																																																	=
																																																	(
																																																	(bool_t)
																																																	0);
																																															}
																																													}
																																											}
																																										}
																																										BgL_test2352z00_4647
																																											=
																																											BgL_res2169z00_3213;
																																									}
																																							}
																																						else
																																							{	/* Return/walk.scm 274 */
																																								BgL_test2352z00_4647
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																				}
																																				if (BgL_test2352z00_4647)
																																					{	/* Return/walk.scm 277 */
																																						obj_t
																																							BgL_tmpz00_4673;
																																						{
																																							BgL_variablez00_bglt
																																								BgL_auxz00_4674;
																																							{
																																								BgL_varz00_bglt
																																									BgL_auxz00_4675;
																																								{	/* Return/walk.scm 275 */
																																									obj_t
																																										BgL_pairz00_3214;
																																									BgL_pairz00_3214
																																										=
																																										(
																																										((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nodez00_1851)))->BgL_argsz00);
																																									BgL_auxz00_4675
																																										=
																																										(
																																										(BgL_varz00_bglt)
																																										CAR
																																										(BgL_pairz00_3214));
																																								}
																																								BgL_auxz00_4674
																																									=
																																									(
																																									((BgL_varz00_bglt) COBJECT(BgL_auxz00_4675))->BgL_variablez00);
																																							}
																																							BgL_tmpz00_4673
																																								=
																																								(
																																								(obj_t)
																																								BgL_auxz00_4674);
																																						}
																																						BgL_test2338z00_4570
																																							=
																																							(BgL_tmpz00_4673
																																							==
																																							BgL_envz00_1853);
																																					}
																																				else
																																					{	/* Return/walk.scm 274 */
																																						BgL_test2338z00_4570
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		else
																																			{	/* Return/walk.scm 272 */
																																				BgL_test2338z00_4570
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																else
																																	{	/* Return/walk.scm 269 */
																																		BgL_test2338z00_4570
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																														else
																															{	/* Return/walk.scm 267 */
																																BgL_test2338z00_4570
																																	=
																																	((bool_t) 0);
																															}
																													}
																												else
																													{	/* Return/walk.scm 265 */
																														BgL_test2338z00_4570
																															= ((bool_t) 0);
																													}
																											}
																										}
																									else
																										{	/* Return/walk.scm 261 */
																											BgL_test2338z00_4570 =
																												((bool_t) 0);
																										}
																								}
																							}
																						else
																							{	/* Return/walk.scm 289 */
																								BgL_test2338z00_4570 =
																									((bool_t) 0);
																							}
																						if (BgL_test2338z00_4570)
																							{	/* Return/walk.scm 290 */
																								obj_t BgL_bindingz00_1911;

																								{	/* Return/walk.scm 290 */
																									obj_t BgL_pairz00_3232;

																									BgL_pairz00_3232 =
																										CDR(
																										((obj_t)
																											BgL_nodesz00_1902));
																									BgL_nodez00_1784 =
																										((BgL_nodez00_bglt)
																										CAR(BgL_pairz00_3232));
																								}
																								BgL_envz00_1785 =
																									BgL_envz00_1904;
																								{	/* Return/walk.scm 230 */
																									bool_t BgL_test2357z00_4684;

																									{	/* Return/walk.scm 230 */
																										obj_t BgL_classz00_2922;

																										BgL_classz00_2922 =
																											BGl_letzd2varzd2zzast_nodez00;
																										{	/* Return/walk.scm 230 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_2924;
																											{	/* Return/walk.scm 230 */
																												obj_t BgL_tmpz00_4685;

																												BgL_tmpz00_4685 =
																													((obj_t)
																													BgL_nodez00_1784);
																												BgL_arg1807z00_2924 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_4685);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Return/walk.scm 230 */
																													long BgL_idxz00_2930;

																													BgL_idxz00_2930 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_2924);
																													BgL_test2357z00_4684 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_2930 +
																																3L)) ==
																														BgL_classz00_2922);
																												}
																											else
																												{	/* Return/walk.scm 230 */
																													bool_t
																														BgL_res2162z00_2955;
																													{	/* Return/walk.scm 230 */
																														obj_t
																															BgL_oclassz00_2938;
																														{	/* Return/walk.scm 230 */
																															obj_t
																																BgL_arg1815z00_2946;
																															long
																																BgL_arg1816z00_2947;
																															BgL_arg1815z00_2946
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Return/walk.scm 230 */
																																long
																																	BgL_arg1817z00_2948;
																																BgL_arg1817z00_2948
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_2924);
																																BgL_arg1816z00_2947
																																	=
																																	(BgL_arg1817z00_2948
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_2938
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_2946,
																																BgL_arg1816z00_2947);
																														}
																														{	/* Return/walk.scm 230 */
																															bool_t
																																BgL__ortest_1115z00_2939;
																															BgL__ortest_1115z00_2939
																																=
																																(BgL_classz00_2922
																																==
																																BgL_oclassz00_2938);
																															if (BgL__ortest_1115z00_2939)
																																{	/* Return/walk.scm 230 */
																																	BgL_res2162z00_2955
																																		=
																																		BgL__ortest_1115z00_2939;
																																}
																															else
																																{	/* Return/walk.scm 230 */
																																	long
																																		BgL_odepthz00_2940;
																																	{	/* Return/walk.scm 230 */
																																		obj_t
																																			BgL_arg1804z00_2941;
																																		BgL_arg1804z00_2941
																																			=
																																			(BgL_oclassz00_2938);
																																		BgL_odepthz00_2940
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_2941);
																																	}
																																	if (
																																		(3L <
																																			BgL_odepthz00_2940))
																																		{	/* Return/walk.scm 230 */
																																			obj_t
																																				BgL_arg1802z00_2943;
																																			{	/* Return/walk.scm 230 */
																																				obj_t
																																					BgL_arg1803z00_2944;
																																				BgL_arg1803z00_2944
																																					=
																																					(BgL_oclassz00_2938);
																																				BgL_arg1802z00_2943
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_2944,
																																					3L);
																																			}
																																			BgL_res2162z00_2955
																																				=
																																				(BgL_arg1802z00_2943
																																				==
																																				BgL_classz00_2922);
																																		}
																																	else
																																		{	/* Return/walk.scm 230 */
																																			BgL_res2162z00_2955
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test2357z00_4684 =
																														BgL_res2162z00_2955;
																												}
																										}
																									}
																									if (BgL_test2357z00_4684)
																										{	/* Return/walk.scm 232 */
																											bool_t
																												BgL_test2361z00_4707;
																											{	/* Return/walk.scm 232 */
																												bool_t
																													BgL_test2362z00_4708;
																												{	/* Return/walk.scm 232 */
																													obj_t BgL_tmpz00_4709;

																													BgL_tmpz00_4709 =
																														(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1784)))->BgL_bindingsz00);
																													BgL_test2362z00_4708 =
																														PAIRP
																														(BgL_tmpz00_4709);
																												}
																												if (BgL_test2362z00_4708)
																													{	/* Return/walk.scm 232 */
																														obj_t
																															BgL_tmpz00_4713;
																														{	/* Return/walk.scm 232 */
																															obj_t
																																BgL_pairz00_2956;
																															BgL_pairz00_2956 =
																																(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1784)))->BgL_bindingsz00);
																															BgL_tmpz00_4713 =
																																CDR
																																(BgL_pairz00_2956);
																														}
																														BgL_test2361z00_4707
																															=
																															NULLP
																															(BgL_tmpz00_4713);
																													}
																												else
																													{	/* Return/walk.scm 232 */
																														BgL_test2361z00_4707
																															= ((bool_t) 0);
																													}
																											}
																											if (BgL_test2361z00_4707)
																												{	/* Return/walk.scm 233 */
																													obj_t
																														BgL_bindingz00_1795;
																													{	/* Return/walk.scm 233 */
																														obj_t
																															BgL_pairz00_2957;
																														BgL_pairz00_2957 =
																															(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1784)))->BgL_bindingsz00);
																														BgL_bindingz00_1795
																															=
																															CAR
																															(BgL_pairz00_2957);
																													}
																													{	/* Return/walk.scm 234 */
																														obj_t
																															BgL_exprz00_1797;
																														BgL_exprz00_1797 =
																															CDR(((obj_t)
																																BgL_bindingz00_1795));
																														{	/* Return/walk.scm 235 */

																															{	/* Return/walk.scm 236 */
																																bool_t
																																	BgL_test2363z00_4723;
																																{	/* Return/walk.scm 236 */
																																	obj_t
																																		BgL_classz00_2960;
																																	BgL_classz00_2960
																																		=
																																		BGl_appz00zzast_nodez00;
																																	if (BGL_OBJECTP(BgL_exprz00_1797))
																																		{	/* Return/walk.scm 236 */
																																			BgL_objectz00_bglt
																																				BgL_arg1807z00_2962;
																																			BgL_arg1807z00_2962
																																				=
																																				(BgL_objectz00_bglt)
																																				(BgL_exprz00_1797);
																																			if (BGL_CONDEXPAND_ISA_ARCH64())
																																				{	/* Return/walk.scm 236 */
																																					long
																																						BgL_idxz00_2968;
																																					BgL_idxz00_2968
																																						=
																																						BGL_OBJECT_INHERITANCE_NUM
																																						(BgL_arg1807z00_2962);
																																					BgL_test2363z00_4723
																																						=
																																						(VECTOR_REF
																																						(BGl_za2inheritancesza2z00zz__objectz00,
																																							(BgL_idxz00_2968
																																								+
																																								3L))
																																						==
																																						BgL_classz00_2960);
																																				}
																																			else
																																				{	/* Return/walk.scm 236 */
																																					bool_t
																																						BgL_res2163z00_2993;
																																					{	/* Return/walk.scm 236 */
																																						obj_t
																																							BgL_oclassz00_2976;
																																						{	/* Return/walk.scm 236 */
																																							obj_t
																																								BgL_arg1815z00_2984;
																																							long
																																								BgL_arg1816z00_2985;
																																							BgL_arg1815z00_2984
																																								=
																																								(BGl_za2classesza2z00zz__objectz00);
																																							{	/* Return/walk.scm 236 */
																																								long
																																									BgL_arg1817z00_2986;
																																								BgL_arg1817z00_2986
																																									=
																																									BGL_OBJECT_CLASS_NUM
																																									(BgL_arg1807z00_2962);
																																								BgL_arg1816z00_2985
																																									=
																																									(BgL_arg1817z00_2986
																																									-
																																									OBJECT_TYPE);
																																							}
																																							BgL_oclassz00_2976
																																								=
																																								VECTOR_REF
																																								(BgL_arg1815z00_2984,
																																								BgL_arg1816z00_2985);
																																						}
																																						{	/* Return/walk.scm 236 */
																																							bool_t
																																								BgL__ortest_1115z00_2977;
																																							BgL__ortest_1115z00_2977
																																								=
																																								(BgL_classz00_2960
																																								==
																																								BgL_oclassz00_2976);
																																							if (BgL__ortest_1115z00_2977)
																																								{	/* Return/walk.scm 236 */
																																									BgL_res2163z00_2993
																																										=
																																										BgL__ortest_1115z00_2977;
																																								}
																																							else
																																								{	/* Return/walk.scm 236 */
																																									long
																																										BgL_odepthz00_2978;
																																									{	/* Return/walk.scm 236 */
																																										obj_t
																																											BgL_arg1804z00_2979;
																																										BgL_arg1804z00_2979
																																											=
																																											(BgL_oclassz00_2976);
																																										BgL_odepthz00_2978
																																											=
																																											BGL_CLASS_DEPTH
																																											(BgL_arg1804z00_2979);
																																									}
																																									if ((3L < BgL_odepthz00_2978))
																																										{	/* Return/walk.scm 236 */
																																											obj_t
																																												BgL_arg1802z00_2981;
																																											{	/* Return/walk.scm 236 */
																																												obj_t
																																													BgL_arg1803z00_2982;
																																												BgL_arg1803z00_2982
																																													=
																																													(BgL_oclassz00_2976);
																																												BgL_arg1802z00_2981
																																													=
																																													BGL_CLASS_ANCESTORS_REF
																																													(BgL_arg1803z00_2982,
																																													3L);
																																											}
																																											BgL_res2163z00_2993
																																												=
																																												(BgL_arg1802z00_2981
																																												==
																																												BgL_classz00_2960);
																																										}
																																									else
																																										{	/* Return/walk.scm 236 */
																																											BgL_res2163z00_2993
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																						}
																																					}
																																					BgL_test2363z00_4723
																																						=
																																						BgL_res2163z00_2993;
																																				}
																																		}
																																	else
																																		{	/* Return/walk.scm 236 */
																																			BgL_test2363z00_4723
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																if (BgL_test2363z00_4723)
																																	{	/* Return/walk.scm 238 */
																																		bool_t
																																			BgL_test2368z00_4746;
																																		{	/* Return/walk.scm 238 */
																																			bool_t
																																				BgL_test2369z00_4747;
																																			{	/* Return/walk.scm 238 */
																																				obj_t
																																					BgL_tmpz00_4748;
																																				BgL_tmpz00_4748
																																					=
																																					(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_exprz00_1797)))->BgL_argsz00);
																																				BgL_test2369z00_4747
																																					=
																																					PAIRP
																																					(BgL_tmpz00_4748);
																																			}
																																			if (BgL_test2369z00_4747)
																																				{	/* Return/walk.scm 238 */
																																					obj_t
																																						BgL_tmpz00_4752;
																																					{	/* Return/walk.scm 238 */
																																						obj_t
																																							BgL_pairz00_2994;
																																						BgL_pairz00_2994
																																							=
																																							(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_exprz00_1797)))->BgL_argsz00);
																																						BgL_tmpz00_4752
																																							=
																																							CDR
																																							(BgL_pairz00_2994);
																																					}
																																					BgL_test2368z00_4746
																																						=
																																						NULLP
																																						(BgL_tmpz00_4752);
																																				}
																																			else
																																				{	/* Return/walk.scm 238 */
																																					BgL_test2368z00_4746
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																		if (BgL_test2368z00_4746)
																																			{	/* Return/walk.scm 239 */
																																				BgL_varz00_bglt
																																					BgL_i1158z00_1806;
																																				BgL_i1158z00_1806
																																					=
																																					(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_exprz00_1797)))->BgL_funz00);
																																				{	/* Return/walk.scm 240 */
																																					bool_t
																																						BgL_test2370z00_4759;
																																					{	/* Return/walk.scm 240 */
																																						BgL_variablez00_bglt
																																							BgL_arg1857z00_1819;
																																						BgL_arg1857z00_1819
																																							=
																																							(((BgL_varz00_bglt) COBJECT(BgL_i1158z00_1806))->BgL_variablez00);
																																						BgL_test2370z00_4759
																																							=
																																							(((obj_t) BgL_arg1857z00_1819) == BGl_za2envzd2getzd2exitdzd2topza2zd2zzreturn_walkz00);
																																					}
																																					if (BgL_test2370z00_4759)
																																						{	/* Return/walk.scm 241 */
																																							bool_t
																																								BgL_test2371z00_4763;
																																							{	/* Return/walk.scm 241 */
																																								obj_t
																																									BgL_arg1854z00_1817;
																																								{	/* Return/walk.scm 241 */
																																									obj_t
																																										BgL_pairz00_2995;
																																									BgL_pairz00_2995
																																										=
																																										(
																																										((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_exprz00_1797)))->BgL_argsz00);
																																									BgL_arg1854z00_1817
																																										=
																																										CAR
																																										(BgL_pairz00_2995);
																																								}
																																								{	/* Return/walk.scm 241 */
																																									obj_t
																																										BgL_classz00_2996;
																																									BgL_classz00_2996
																																										=
																																										BGl_varz00zzast_nodez00;
																																									if (BGL_OBJECTP(BgL_arg1854z00_1817))
																																										{	/* Return/walk.scm 241 */
																																											BgL_objectz00_bglt
																																												BgL_arg1807z00_2998;
																																											BgL_arg1807z00_2998
																																												=
																																												(BgL_objectz00_bglt)
																																												(BgL_arg1854z00_1817);
																																											if (BGL_CONDEXPAND_ISA_ARCH64())
																																												{	/* Return/walk.scm 241 */
																																													long
																																														BgL_idxz00_3004;
																																													BgL_idxz00_3004
																																														=
																																														BGL_OBJECT_INHERITANCE_NUM
																																														(BgL_arg1807z00_2998);
																																													BgL_test2371z00_4763
																																														=
																																														(VECTOR_REF
																																														(BGl_za2inheritancesza2z00zz__objectz00,
																																															(BgL_idxz00_3004
																																																+
																																																2L))
																																														==
																																														BgL_classz00_2996);
																																												}
																																											else
																																												{	/* Return/walk.scm 241 */
																																													bool_t
																																														BgL_res2164z00_3029;
																																													{	/* Return/walk.scm 241 */
																																														obj_t
																																															BgL_oclassz00_3012;
																																														{	/* Return/walk.scm 241 */
																																															obj_t
																																																BgL_arg1815z00_3020;
																																															long
																																																BgL_arg1816z00_3021;
																																															BgL_arg1815z00_3020
																																																=
																																																(BGl_za2classesza2z00zz__objectz00);
																																															{	/* Return/walk.scm 241 */
																																																long
																																																	BgL_arg1817z00_3022;
																																																BgL_arg1817z00_3022
																																																	=
																																																	BGL_OBJECT_CLASS_NUM
																																																	(BgL_arg1807z00_2998);
																																																BgL_arg1816z00_3021
																																																	=
																																																	(BgL_arg1817z00_3022
																																																	-
																																																	OBJECT_TYPE);
																																															}
																																															BgL_oclassz00_3012
																																																=
																																																VECTOR_REF
																																																(BgL_arg1815z00_3020,
																																																BgL_arg1816z00_3021);
																																														}
																																														{	/* Return/walk.scm 241 */
																																															bool_t
																																																BgL__ortest_1115z00_3013;
																																															BgL__ortest_1115z00_3013
																																																=
																																																(BgL_classz00_2996
																																																==
																																																BgL_oclassz00_3012);
																																															if (BgL__ortest_1115z00_3013)
																																																{	/* Return/walk.scm 241 */
																																																	BgL_res2164z00_3029
																																																		=
																																																		BgL__ortest_1115z00_3013;
																																																}
																																															else
																																																{	/* Return/walk.scm 241 */
																																																	long
																																																		BgL_odepthz00_3014;
																																																	{	/* Return/walk.scm 241 */
																																																		obj_t
																																																			BgL_arg1804z00_3015;
																																																		BgL_arg1804z00_3015
																																																			=
																																																			(BgL_oclassz00_3012);
																																																		BgL_odepthz00_3014
																																																			=
																																																			BGL_CLASS_DEPTH
																																																			(BgL_arg1804z00_3015);
																																																	}
																																																	if ((2L < BgL_odepthz00_3014))
																																																		{	/* Return/walk.scm 241 */
																																																			obj_t
																																																				BgL_arg1802z00_3017;
																																																			{	/* Return/walk.scm 241 */
																																																				obj_t
																																																					BgL_arg1803z00_3018;
																																																				BgL_arg1803z00_3018
																																																					=
																																																					(BgL_oclassz00_3012);
																																																				BgL_arg1802z00_3017
																																																					=
																																																					BGL_CLASS_ANCESTORS_REF
																																																					(BgL_arg1803z00_3018,
																																																					2L);
																																																			}
																																																			BgL_res2164z00_3029
																																																				=
																																																				(BgL_arg1802z00_3017
																																																				==
																																																				BgL_classz00_2996);
																																																		}
																																																	else
																																																		{	/* Return/walk.scm 241 */
																																																			BgL_res2164z00_3029
																																																				=
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																														}
																																													}
																																													BgL_test2371z00_4763
																																														=
																																														BgL_res2164z00_3029;
																																												}
																																										}
																																									else
																																										{	/* Return/walk.scm 241 */
																																											BgL_test2371z00_4763
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							}
																																							if (BgL_test2371z00_4763)
																																								{	/* Return/walk.scm 243 */
																																									bool_t
																																										BgL_test2376z00_4789;
																																									{	/* Return/walk.scm 243 */
																																										obj_t
																																											BgL_tmpz00_4790;
																																										{
																																											BgL_variablez00_bglt
																																												BgL_auxz00_4791;
																																											{
																																												BgL_varz00_bglt
																																													BgL_auxz00_4792;
																																												{	/* Return/walk.scm 242 */
																																													obj_t
																																														BgL_pairz00_3030;
																																													BgL_pairz00_3030
																																														=
																																														(
																																														((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_exprz00_1797)))->BgL_argsz00);
																																													BgL_auxz00_4792
																																														=
																																														(
																																														(BgL_varz00_bglt)
																																														CAR
																																														(BgL_pairz00_3030));
																																												}
																																												BgL_auxz00_4791
																																													=
																																													(
																																													((BgL_varz00_bglt) COBJECT(BgL_auxz00_4792))->BgL_variablez00);
																																											}
																																											BgL_tmpz00_4790
																																												=
																																												(
																																												(obj_t)
																																												BgL_auxz00_4791);
																																										}
																																										BgL_test2376z00_4789
																																											=
																																											(BgL_tmpz00_4790
																																											==
																																											BgL_envz00_1785);
																																									}
																																									if (BgL_test2376z00_4789)
																																										{	/* Return/walk.scm 243 */
																																											BgL_bindingz00_1911
																																												=
																																												BgL_bindingz00_1795;
																																										}
																																									else
																																										{	/* Return/walk.scm 243 */
																																											BgL_bindingz00_1911
																																												=
																																												BFALSE;
																																										}
																																								}
																																							else
																																								{	/* Return/walk.scm 241 */
																																									BgL_bindingz00_1911
																																										=
																																										BFALSE;
																																								}
																																						}
																																					else
																																						{	/* Return/walk.scm 240 */
																																							BgL_bindingz00_1911
																																								=
																																								BFALSE;
																																						}
																																				}
																																			}
																																		else
																																			{	/* Return/walk.scm 238 */
																																				BgL_bindingz00_1911
																																					=
																																					BFALSE;
																																			}
																																	}
																																else
																																	{	/* Return/walk.scm 236 */
																																		BgL_bindingz00_1911
																																			= BFALSE;
																																	}
																															}
																														}
																													}
																												}
																											else
																												{	/* Return/walk.scm 232 */
																													BgL_bindingz00_1911 =
																														BFALSE;
																												}
																										}
																									else
																										{	/* Return/walk.scm 230 */
																											BgL_bindingz00_1911 =
																												BFALSE;
																										}
																								}
																								if (CBOOL(BgL_bindingz00_1911))
																									{	/* Return/walk.scm 292 */
																										BgL_letzd2varzd2_bglt
																											BgL_i1168z00_1912;
																										{	/* Return/walk.scm 292 */
																											obj_t BgL_pairz00_3236;

																											BgL_pairz00_3236 =
																												CDR(
																												((obj_t)
																													BgL_nodesz00_1902));
																											BgL_i1168z00_1912 =
																												((BgL_letzd2varzd2_bglt)
																												CAR(BgL_pairz00_3236));
																										}
																										{	/* Return/walk.scm 293 */
																											obj_t
																												BgL_exitnodez00_1913;
																											{	/* Return/walk.scm 293 */
																												BgL_nodez00_bglt
																													BgL_arg1938z00_1915;
																												obj_t
																													BgL_arg1939z00_1916;
																												BgL_arg1938z00_1915 =
																													(((BgL_letzd2varzd2_bglt) COBJECT(BgL_i1168z00_1912))->BgL_bodyz00);
																												BgL_arg1939z00_1916 =
																													CAR(((obj_t)
																														BgL_bindingz00_1911));
																												BgL_nodez00_1728 =
																													BgL_arg1938z00_1915;
																												BgL_varz00_1729 =
																													((BgL_localz00_bglt)
																													BgL_arg1939z00_1916);
																												BgL_envz00_1730 =
																													BgL_envz00_1904;
																												{	/* Return/walk.scm 206 */
																													bool_t
																														BgL_test2378z00_4813;
																													{	/* Return/walk.scm 206 */
																														obj_t
																															BgL_classz00_2809;
																														BgL_classz00_2809 =
																															BGl_letzd2varzd2zzast_nodez00;
																														{	/* Return/walk.scm 206 */
																															BgL_objectz00_bglt
																																BgL_arg1807z00_2811;
																															{	/* Return/walk.scm 206 */
																																obj_t
																																	BgL_tmpz00_4814;
																																BgL_tmpz00_4814
																																	=
																																	((obj_t)
																																	BgL_nodez00_1728);
																																BgL_arg1807z00_2811
																																	=
																																	(BgL_objectz00_bglt)
																																	(BgL_tmpz00_4814);
																															}
																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																{	/* Return/walk.scm 206 */
																																	long
																																		BgL_idxz00_2817;
																																	BgL_idxz00_2817
																																		=
																																		BGL_OBJECT_INHERITANCE_NUM
																																		(BgL_arg1807z00_2811);
																																	BgL_test2378z00_4813
																																		=
																																		(VECTOR_REF
																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																			(BgL_idxz00_2817
																																				+
																																				3L)) ==
																																		BgL_classz00_2809);
																																}
																															else
																																{	/* Return/walk.scm 206 */
																																	bool_t
																																		BgL_res2159z00_2842;
																																	{	/* Return/walk.scm 206 */
																																		obj_t
																																			BgL_oclassz00_2825;
																																		{	/* Return/walk.scm 206 */
																																			obj_t
																																				BgL_arg1815z00_2833;
																																			long
																																				BgL_arg1816z00_2834;
																																			BgL_arg1815z00_2833
																																				=
																																				(BGl_za2classesza2z00zz__objectz00);
																																			{	/* Return/walk.scm 206 */
																																				long
																																					BgL_arg1817z00_2835;
																																				BgL_arg1817z00_2835
																																					=
																																					BGL_OBJECT_CLASS_NUM
																																					(BgL_arg1807z00_2811);
																																				BgL_arg1816z00_2834
																																					=
																																					(BgL_arg1817z00_2835
																																					-
																																					OBJECT_TYPE);
																																			}
																																			BgL_oclassz00_2825
																																				=
																																				VECTOR_REF
																																				(BgL_arg1815z00_2833,
																																				BgL_arg1816z00_2834);
																																		}
																																		{	/* Return/walk.scm 206 */
																																			bool_t
																																				BgL__ortest_1115z00_2826;
																																			BgL__ortest_1115z00_2826
																																				=
																																				(BgL_classz00_2809
																																				==
																																				BgL_oclassz00_2825);
																																			if (BgL__ortest_1115z00_2826)
																																				{	/* Return/walk.scm 206 */
																																					BgL_res2159z00_2842
																																						=
																																						BgL__ortest_1115z00_2826;
																																				}
																																			else
																																				{	/* Return/walk.scm 206 */
																																					long
																																						BgL_odepthz00_2827;
																																					{	/* Return/walk.scm 206 */
																																						obj_t
																																							BgL_arg1804z00_2828;
																																						BgL_arg1804z00_2828
																																							=
																																							(BgL_oclassz00_2825);
																																						BgL_odepthz00_2827
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_arg1804z00_2828);
																																					}
																																					if (
																																						(3L
																																							<
																																							BgL_odepthz00_2827))
																																						{	/* Return/walk.scm 206 */
																																							obj_t
																																								BgL_arg1802z00_2830;
																																							{	/* Return/walk.scm 206 */
																																								obj_t
																																									BgL_arg1803z00_2831;
																																								BgL_arg1803z00_2831
																																									=
																																									(BgL_oclassz00_2825);
																																								BgL_arg1802z00_2830
																																									=
																																									BGL_CLASS_ANCESTORS_REF
																																									(BgL_arg1803z00_2831,
																																									3L);
																																							}
																																							BgL_res2159z00_2842
																																								=
																																								(BgL_arg1802z00_2830
																																								==
																																								BgL_classz00_2809);
																																						}
																																					else
																																						{	/* Return/walk.scm 206 */
																																							BgL_res2159z00_2842
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																	BgL_test2378z00_4813
																																		=
																																		BgL_res2159z00_2842;
																																}
																														}
																													}
																													if (BgL_test2378z00_4813)
																														{	/* Return/walk.scm 208 */
																															bool_t
																																BgL_test2382z00_4836;
																															{	/* Return/walk.scm 208 */
																																bool_t
																																	BgL_test2383z00_4837;
																																{	/* Return/walk.scm 208 */
																																	obj_t
																																		BgL_tmpz00_4838;
																																	BgL_tmpz00_4838
																																		=
																																		(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1728)))->BgL_bindingsz00);
																																	BgL_test2383z00_4837
																																		=
																																		PAIRP
																																		(BgL_tmpz00_4838);
																																}
																																if (BgL_test2383z00_4837)
																																	{	/* Return/walk.scm 208 */
																																		obj_t
																																			BgL_tmpz00_4842;
																																		{	/* Return/walk.scm 208 */
																																			obj_t
																																				BgL_pairz00_2843;
																																			BgL_pairz00_2843
																																				=
																																				(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1728)))->BgL_bindingsz00);
																																			BgL_tmpz00_4842
																																				=
																																				CDR
																																				(BgL_pairz00_2843);
																																		}
																																		BgL_test2382z00_4836
																																			=
																																			NULLP
																																			(BgL_tmpz00_4842);
																																	}
																																else
																																	{	/* Return/walk.scm 208 */
																																		BgL_test2382z00_4836
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																															if (BgL_test2382z00_4836)
																																{	/* Return/walk.scm 209 */
																																	obj_t
																																		BgL_bindingz00_1740;
																																	{	/* Return/walk.scm 209 */
																																		obj_t
																																			BgL_pairz00_2844;
																																		BgL_pairz00_2844
																																			=
																																			(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1728)))->BgL_bindingsz00);
																																		BgL_bindingz00_1740
																																			=
																																			CAR
																																			(BgL_pairz00_2844);
																																	}
																																	{	/* Return/walk.scm 209 */
																																		obj_t
																																			BgL_nbodyz00_1741;
																																		{	/* Return/walk.scm 210 */
																																			BgL_nodez00_bglt
																																				BgL_arg1752z00_1750;
																																			obj_t
																																				BgL_arg1753z00_1751;
																																			BgL_arg1752z00_1750
																																				=
																																				(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1728)))->BgL_bodyz00);
																																			BgL_arg1753z00_1751
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_bindingz00_1740));
																																			BgL_nbodyz00_1741
																																				=
																																				BGl_step6bze70ze7zzreturn_walkz00
																																				(BgL_arg1752z00_1750,
																																				((BgL_localz00_bglt) BgL_arg1753z00_1751), BgL_envz00_1730);
																																		}
																																		{	/* Return/walk.scm 210 */

																																			if (CBOOL
																																				(BgL_nbodyz00_1741))
																																				{	/* Return/walk.scm 212 */
																																					BgL_letzd2varzd2_bglt
																																						BgL_new1145z00_1743;
																																					{	/* Return/walk.scm 212 */
																																						BgL_letzd2varzd2_bglt
																																							BgL_new1152z00_1748;
																																						BgL_new1152z00_1748
																																							=
																																							(
																																							(BgL_letzd2varzd2_bglt)
																																							BOBJECT
																																							(GC_MALLOC
																																								(sizeof
																																									(struct
																																										BgL_letzd2varzd2_bgl))));
																																						{	/* Return/walk.scm 212 */
																																							long
																																								BgL_arg1751z00_1749;
																																							BgL_arg1751z00_1749
																																								=
																																								BGL_CLASS_NUM
																																								(BGl_letzd2varzd2zzast_nodez00);
																																							BGL_OBJECT_CLASS_NUM_SET
																																								(
																																								((BgL_objectz00_bglt) BgL_new1152z00_1748), BgL_arg1751z00_1749);
																																						}
																																						{	/* Return/walk.scm 212 */
																																							BgL_objectz00_bglt
																																								BgL_tmpz00_4862;
																																							BgL_tmpz00_4862
																																								=
																																								(
																																								(BgL_objectz00_bglt)
																																								BgL_new1152z00_1748);
																																							BGL_OBJECT_WIDENING_SET
																																								(BgL_tmpz00_4862,
																																								BFALSE);
																																						}
																																						((BgL_objectz00_bglt) BgL_new1152z00_1748);
																																						BgL_new1145z00_1743
																																							=
																																							BgL_new1152z00_1748;
																																					}
																																					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1145z00_1743)))->BgL_locz00) = ((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_1728))->BgL_locz00)), BUNSPEC);
																																					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1145z00_1743)))->BgL_typez00) = ((BgL_typez00_bglt) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_1728))->BgL_typez00)), BUNSPEC);
																																					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1145z00_1743)))->BgL_sidezd2effectzd2) = ((obj_t) (((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_nodez00_1728)))->BgL_sidezd2effectzd2)), BUNSPEC);
																																					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1145z00_1743)))->BgL_keyz00) = ((obj_t) (((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_nodez00_1728)))->BgL_keyz00)), BUNSPEC);
																																					((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1145z00_1743))->BgL_bindingsz00) = ((obj_t) (((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1728)))->BgL_bindingsz00)), BUNSPEC);
																																					((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1145z00_1743))->BgL_bodyz00) = ((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nbodyz00_1741)), BUNSPEC);
																																					((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1145z00_1743))->BgL_removablezf3zf3) = ((bool_t) (((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_1728)))->BgL_removablezf3zf3)), BUNSPEC);
																																					BgL_exitnodez00_1913
																																						=
																																						(
																																						(obj_t)
																																						BgL_new1145z00_1743);
																																				}
																																			else
																																				{	/* Return/walk.scm 211 */
																																					BgL_exitnodez00_1913
																																						=
																																						BFALSE;
																																				}
																																		}
																																	}
																																}
																															else
																																{	/* Return/walk.scm 208 */
																																	BgL_exitnodez00_1913
																																		= BFALSE;
																																}
																														}
																													else
																														{	/* Return/walk.scm 206 */
																															BgL_exitnodez00_1913
																																= BFALSE;
																														}
																												}
																											}
																											if (CBOOL
																												(BgL_exitnodez00_1913))
																												{	/* Return/walk.scm 295 */
																													obj_t
																														BgL_arg1937z00_1914;
																													BgL_arg1937z00_1914 =
																														CAR(((obj_t)
																															BgL_bindingz00_1911));
																													return
																														MAKE_YOUNG_PAIR
																														(BgL_arg1937z00_1914,
																														BgL_exitnodez00_1913);
																												}
																											else
																												{	/* Return/walk.scm 294 */
																													return BFALSE;
																												}
																										}
																									}
																								else
																									{	/* Return/walk.scm 291 */
																										return BFALSE;
																									}
																							}
																						else
																							{	/* Return/walk.scm 289 */
																								return BFALSE;
																							}
																					}
																				}
																			}
																		else
																			{	/* Return/walk.scm 315 */
																				return BFALSE;
																			}
																	}
																}
															}
														else
															{	/* Return/walk.scm 312 */
																return BFALSE;
															}
													}
												else
													{	/* Return/walk.scm 309 */
														return BFALSE;
													}
											}
										else
											{	/* Return/walk.scm 307 */
												return BFALSE;
											}
									}
								}
						}
					else
						{	/* Return/walk.scm 320 */
							return BFALSE;
						}
				}
			}
		}

	}



/* step8~0 */
	obj_t BGl_step8ze70ze7zzreturn_walkz00(BgL_nodez00_bglt BgL_nodez00_1595,
		BgL_localz00_bglt BgL_resz00_1596)
	{
		{	/* Return/walk.scm 162 */
			{	/* Return/walk.scm 149 */
				bool_t BgL_test2386z00_4896;

				{	/* Return/walk.scm 149 */
					obj_t BgL_classz00_2479;

					BgL_classz00_2479 = BGl_varz00zzast_nodez00;
					{	/* Return/walk.scm 149 */
						BgL_objectz00_bglt BgL_arg1807z00_2481;

						{	/* Return/walk.scm 149 */
							obj_t BgL_tmpz00_4897;

							BgL_tmpz00_4897 = ((obj_t) BgL_nodez00_1595);
							BgL_arg1807z00_2481 = (BgL_objectz00_bglt) (BgL_tmpz00_4897);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Return/walk.scm 149 */
								long BgL_idxz00_2487;

								BgL_idxz00_2487 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2481);
								BgL_test2386z00_4896 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2487 + 2L)) == BgL_classz00_2479);
							}
						else
							{	/* Return/walk.scm 149 */
								bool_t BgL_res2150z00_2512;

								{	/* Return/walk.scm 149 */
									obj_t BgL_oclassz00_2495;

									{	/* Return/walk.scm 149 */
										obj_t BgL_arg1815z00_2503;
										long BgL_arg1816z00_2504;

										BgL_arg1815z00_2503 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Return/walk.scm 149 */
											long BgL_arg1817z00_2505;

											BgL_arg1817z00_2505 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2481);
											BgL_arg1816z00_2504 = (BgL_arg1817z00_2505 - OBJECT_TYPE);
										}
										BgL_oclassz00_2495 =
											VECTOR_REF(BgL_arg1815z00_2503, BgL_arg1816z00_2504);
									}
									{	/* Return/walk.scm 149 */
										bool_t BgL__ortest_1115z00_2496;

										BgL__ortest_1115z00_2496 =
											(BgL_classz00_2479 == BgL_oclassz00_2495);
										if (BgL__ortest_1115z00_2496)
											{	/* Return/walk.scm 149 */
												BgL_res2150z00_2512 = BgL__ortest_1115z00_2496;
											}
										else
											{	/* Return/walk.scm 149 */
												long BgL_odepthz00_2497;

												{	/* Return/walk.scm 149 */
													obj_t BgL_arg1804z00_2498;

													BgL_arg1804z00_2498 = (BgL_oclassz00_2495);
													BgL_odepthz00_2497 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2498);
												}
												if ((2L < BgL_odepthz00_2497))
													{	/* Return/walk.scm 149 */
														obj_t BgL_arg1802z00_2500;

														{	/* Return/walk.scm 149 */
															obj_t BgL_arg1803z00_2501;

															BgL_arg1803z00_2501 = (BgL_oclassz00_2495);
															BgL_arg1802z00_2500 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2501,
																2L);
														}
														BgL_res2150z00_2512 =
															(BgL_arg1802z00_2500 == BgL_classz00_2479);
													}
												else
													{	/* Return/walk.scm 149 */
														BgL_res2150z00_2512 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2386z00_4896 = BgL_res2150z00_2512;
							}
					}
				}
				if (BgL_test2386z00_4896)
					{	/* Return/walk.scm 149 */
						if (
							(((obj_t)
									(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_1595)))->
										BgL_variablez00)) == ((obj_t) BgL_resz00_1596)))
							{	/* Return/walk.scm 151 */
								return ((obj_t) BgL_nodez00_1595);
							}
						else
							{	/* Return/walk.scm 151 */
								return BFALSE;
							}
					}
				else
					{	/* Return/walk.scm 153 */
						bool_t BgL_test2391z00_4926;

						{	/* Return/walk.scm 153 */
							obj_t BgL_classz00_2513;

							BgL_classz00_2513 = BGl_appz00zzast_nodez00;
							{	/* Return/walk.scm 153 */
								BgL_objectz00_bglt BgL_arg1807z00_2515;

								{	/* Return/walk.scm 153 */
									obj_t BgL_tmpz00_4927;

									BgL_tmpz00_4927 = ((obj_t) BgL_nodez00_1595);
									BgL_arg1807z00_2515 = (BgL_objectz00_bglt) (BgL_tmpz00_4927);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Return/walk.scm 153 */
										long BgL_idxz00_2521;

										BgL_idxz00_2521 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2515);
										BgL_test2391z00_4926 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2521 + 3L)) == BgL_classz00_2513);
									}
								else
									{	/* Return/walk.scm 153 */
										bool_t BgL_res2151z00_2546;

										{	/* Return/walk.scm 153 */
											obj_t BgL_oclassz00_2529;

											{	/* Return/walk.scm 153 */
												obj_t BgL_arg1815z00_2537;
												long BgL_arg1816z00_2538;

												BgL_arg1815z00_2537 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Return/walk.scm 153 */
													long BgL_arg1817z00_2539;

													BgL_arg1817z00_2539 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2515);
													BgL_arg1816z00_2538 =
														(BgL_arg1817z00_2539 - OBJECT_TYPE);
												}
												BgL_oclassz00_2529 =
													VECTOR_REF(BgL_arg1815z00_2537, BgL_arg1816z00_2538);
											}
											{	/* Return/walk.scm 153 */
												bool_t BgL__ortest_1115z00_2530;

												BgL__ortest_1115z00_2530 =
													(BgL_classz00_2513 == BgL_oclassz00_2529);
												if (BgL__ortest_1115z00_2530)
													{	/* Return/walk.scm 153 */
														BgL_res2151z00_2546 = BgL__ortest_1115z00_2530;
													}
												else
													{	/* Return/walk.scm 153 */
														long BgL_odepthz00_2531;

														{	/* Return/walk.scm 153 */
															obj_t BgL_arg1804z00_2532;

															BgL_arg1804z00_2532 = (BgL_oclassz00_2529);
															BgL_odepthz00_2531 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2532);
														}
														if ((3L < BgL_odepthz00_2531))
															{	/* Return/walk.scm 153 */
																obj_t BgL_arg1802z00_2534;

																{	/* Return/walk.scm 153 */
																	obj_t BgL_arg1803z00_2535;

																	BgL_arg1803z00_2535 = (BgL_oclassz00_2529);
																	BgL_arg1802z00_2534 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2535,
																		3L);
																}
																BgL_res2151z00_2546 =
																	(BgL_arg1802z00_2534 == BgL_classz00_2513);
															}
														else
															{	/* Return/walk.scm 153 */
																BgL_res2151z00_2546 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2391z00_4926 = BgL_res2151z00_2546;
									}
							}
						}
						if (BgL_test2391z00_4926)
							{	/* Return/walk.scm 155 */
								bool_t BgL_test2395z00_4949;

								{
									obj_t BgL_l1320z00_1617;

									{	/* Return/walk.scm 155 */
										obj_t BgL_tmpz00_4950;

										BgL_l1320z00_1617 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_1595)))->BgL_argsz00);
									BgL_zc3z04anonymousza31555ze3z87_1618:
										if (NULLP(BgL_l1320z00_1617))
											{	/* Return/walk.scm 155 */
												BgL_tmpz00_4950 = BFALSE;
											}
										else
											{	/* Return/walk.scm 155 */
												obj_t BgL__ortest_1323z00_1620;

												BgL__ortest_1323z00_1620 =
													BGl_step8ze70ze7zzreturn_walkz00(
													((BgL_nodez00_bglt)
														CAR(((obj_t) BgL_l1320z00_1617))), BgL_resz00_1596);
												if (CBOOL(BgL__ortest_1323z00_1620))
													{	/* Return/walk.scm 155 */
														BgL_tmpz00_4950 = BgL__ortest_1323z00_1620;
													}
												else
													{
														obj_t BgL_l1320z00_4959;

														BgL_l1320z00_4959 =
															CDR(((obj_t) BgL_l1320z00_1617));
														BgL_l1320z00_1617 = BgL_l1320z00_4959;
														goto BgL_zc3z04anonymousza31555ze3z87_1618;
													}
											}
										BgL_test2395z00_4949 = CBOOL(BgL_tmpz00_4950);
									}
								}
								if (BgL_test2395z00_4949)
									{	/* Return/walk.scm 155 */
										return ((obj_t) BgL_nodez00_1595);
									}
								else
									{	/* Return/walk.scm 155 */
										return BFALSE;
									}
							}
						else
							{	/* Return/walk.scm 157 */
								bool_t BgL_test2398z00_4966;

								{	/* Return/walk.scm 157 */
									obj_t BgL_classz00_2549;

									BgL_classz00_2549 = BGl_castz00zzast_nodez00;
									{	/* Return/walk.scm 157 */
										BgL_objectz00_bglt BgL_arg1807z00_2551;

										{	/* Return/walk.scm 157 */
											obj_t BgL_tmpz00_4967;

											BgL_tmpz00_4967 = ((obj_t) BgL_nodez00_1595);
											BgL_arg1807z00_2551 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4967);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Return/walk.scm 157 */
												long BgL_idxz00_2557;

												BgL_idxz00_2557 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2551);
												BgL_test2398z00_4966 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2557 + 2L)) == BgL_classz00_2549);
											}
										else
											{	/* Return/walk.scm 157 */
												bool_t BgL_res2152z00_2582;

												{	/* Return/walk.scm 157 */
													obj_t BgL_oclassz00_2565;

													{	/* Return/walk.scm 157 */
														obj_t BgL_arg1815z00_2573;
														long BgL_arg1816z00_2574;

														BgL_arg1815z00_2573 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Return/walk.scm 157 */
															long BgL_arg1817z00_2575;

															BgL_arg1817z00_2575 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2551);
															BgL_arg1816z00_2574 =
																(BgL_arg1817z00_2575 - OBJECT_TYPE);
														}
														BgL_oclassz00_2565 =
															VECTOR_REF(BgL_arg1815z00_2573,
															BgL_arg1816z00_2574);
													}
													{	/* Return/walk.scm 157 */
														bool_t BgL__ortest_1115z00_2566;

														BgL__ortest_1115z00_2566 =
															(BgL_classz00_2549 == BgL_oclassz00_2565);
														if (BgL__ortest_1115z00_2566)
															{	/* Return/walk.scm 157 */
																BgL_res2152z00_2582 = BgL__ortest_1115z00_2566;
															}
														else
															{	/* Return/walk.scm 157 */
																long BgL_odepthz00_2567;

																{	/* Return/walk.scm 157 */
																	obj_t BgL_arg1804z00_2568;

																	BgL_arg1804z00_2568 = (BgL_oclassz00_2565);
																	BgL_odepthz00_2567 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2568);
																}
																if ((2L < BgL_odepthz00_2567))
																	{	/* Return/walk.scm 157 */
																		obj_t BgL_arg1802z00_2570;

																		{	/* Return/walk.scm 157 */
																			obj_t BgL_arg1803z00_2571;

																			BgL_arg1803z00_2571 =
																				(BgL_oclassz00_2565);
																			BgL_arg1802z00_2570 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2571, 2L);
																		}
																		BgL_res2152z00_2582 =
																			(BgL_arg1802z00_2570 ==
																			BgL_classz00_2549);
																	}
																else
																	{	/* Return/walk.scm 157 */
																		BgL_res2152z00_2582 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2398z00_4966 = BgL_res2152z00_2582;
											}
									}
								}
								if (BgL_test2398z00_4966)
									{	/* Return/walk.scm 157 */
										if (CBOOL(BGl_step8ze70ze7zzreturn_walkz00(
													(((BgL_castz00_bglt) COBJECT(
																((BgL_castz00_bglt) BgL_nodez00_1595)))->
														BgL_argz00), BgL_resz00_1596)))
											{	/* Return/walk.scm 159 */
												return ((obj_t) BgL_nodez00_1595);
											}
										else
											{	/* Return/walk.scm 159 */
												return BFALSE;
											}
									}
								else
									{	/* Return/walk.scm 157 */
										return BFALSE;
									}
							}
					}
			}
		}

	}



/* step6~0 */
	obj_t BGl_step6ze70ze7zzreturn_walkz00(BgL_nodez00_bglt BgL_nodez00_1665,
		BgL_localz00_bglt BgL_varz00_1666)
	{
		{	/* Return/walk.scm 186 */
			{
				BgL_nodez00_bglt BgL_nodez00_1629;

				{	/* Return/walk.scm 182 */
					bool_t BgL_test2403z00_4995;

					{	/* Return/walk.scm 182 */
						obj_t BgL_classz00_2688;

						BgL_classz00_2688 = BGl_sequencez00zzast_nodez00;
						{	/* Return/walk.scm 182 */
							BgL_objectz00_bglt BgL_arg1807z00_2690;

							{	/* Return/walk.scm 182 */
								obj_t BgL_tmpz00_4996;

								BgL_tmpz00_4996 = ((obj_t) BgL_nodez00_1665);
								BgL_arg1807z00_2690 = (BgL_objectz00_bglt) (BgL_tmpz00_4996);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Return/walk.scm 182 */
									long BgL_idxz00_2696;

									BgL_idxz00_2696 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2690);
									BgL_test2403z00_4995 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2696 + 3L)) == BgL_classz00_2688);
								}
							else
								{	/* Return/walk.scm 182 */
									bool_t BgL_res2156z00_2721;

									{	/* Return/walk.scm 182 */
										obj_t BgL_oclassz00_2704;

										{	/* Return/walk.scm 182 */
											obj_t BgL_arg1815z00_2712;
											long BgL_arg1816z00_2713;

											BgL_arg1815z00_2712 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Return/walk.scm 182 */
												long BgL_arg1817z00_2714;

												BgL_arg1817z00_2714 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2690);
												BgL_arg1816z00_2713 =
													(BgL_arg1817z00_2714 - OBJECT_TYPE);
											}
											BgL_oclassz00_2704 =
												VECTOR_REF(BgL_arg1815z00_2712, BgL_arg1816z00_2713);
										}
										{	/* Return/walk.scm 182 */
											bool_t BgL__ortest_1115z00_2705;

											BgL__ortest_1115z00_2705 =
												(BgL_classz00_2688 == BgL_oclassz00_2704);
											if (BgL__ortest_1115z00_2705)
												{	/* Return/walk.scm 182 */
													BgL_res2156z00_2721 = BgL__ortest_1115z00_2705;
												}
											else
												{	/* Return/walk.scm 182 */
													long BgL_odepthz00_2706;

													{	/* Return/walk.scm 182 */
														obj_t BgL_arg1804z00_2707;

														BgL_arg1804z00_2707 = (BgL_oclassz00_2704);
														BgL_odepthz00_2706 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2707);
													}
													if ((3L < BgL_odepthz00_2706))
														{	/* Return/walk.scm 182 */
															obj_t BgL_arg1802z00_2709;

															{	/* Return/walk.scm 182 */
																obj_t BgL_arg1803z00_2710;

																BgL_arg1803z00_2710 = (BgL_oclassz00_2704);
																BgL_arg1802z00_2709 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2710,
																	3L);
															}
															BgL_res2156z00_2721 =
																(BgL_arg1802z00_2709 == BgL_classz00_2688);
														}
													else
														{	/* Return/walk.scm 182 */
															BgL_res2156z00_2721 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2403z00_4995 = BgL_res2156z00_2721;
								}
						}
					}
					if (BgL_test2403z00_4995)
						{	/* Return/walk.scm 182 */
							if (
								(bgl_list_length(
										(((BgL_sequencez00_bglt) COBJECT(
													((BgL_sequencez00_bglt) BgL_nodez00_1665)))->
											BgL_nodesz00)) == 2L))
								{	/* Return/walk.scm 185 */
									bool_t BgL_test2408z00_5023;

									{	/* Return/walk.scm 185 */
										obj_t BgL_pairz00_2723;

										BgL_pairz00_2723 =
											(((BgL_sequencez00_bglt) COBJECT(
													((BgL_sequencez00_bglt) BgL_nodez00_1665)))->
											BgL_nodesz00);
										BgL_nodez00_1629 =
											((BgL_nodez00_bglt) CAR(BgL_pairz00_2723));
									}
									{	/* Return/walk.scm 165 */
										bool_t BgL_test2409z00_5024;

										{	/* Return/walk.scm 165 */
											obj_t BgL_classz00_2583;

											BgL_classz00_2583 = BGl_appz00zzast_nodez00;
											{	/* Return/walk.scm 165 */
												BgL_objectz00_bglt BgL_arg1807z00_2585;

												{	/* Return/walk.scm 165 */
													obj_t BgL_tmpz00_5025;

													BgL_tmpz00_5025 = ((obj_t) BgL_nodez00_1629);
													BgL_arg1807z00_2585 =
														(BgL_objectz00_bglt) (BgL_tmpz00_5025);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Return/walk.scm 165 */
														long BgL_idxz00_2591;

														BgL_idxz00_2591 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2585);
														BgL_test2409z00_5024 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2591 + 3L)) == BgL_classz00_2583);
													}
												else
													{	/* Return/walk.scm 165 */
														bool_t BgL_res2153z00_2616;

														{	/* Return/walk.scm 165 */
															obj_t BgL_oclassz00_2599;

															{	/* Return/walk.scm 165 */
																obj_t BgL_arg1815z00_2607;
																long BgL_arg1816z00_2608;

																BgL_arg1815z00_2607 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Return/walk.scm 165 */
																	long BgL_arg1817z00_2609;

																	BgL_arg1817z00_2609 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2585);
																	BgL_arg1816z00_2608 =
																		(BgL_arg1817z00_2609 - OBJECT_TYPE);
																}
																BgL_oclassz00_2599 =
																	VECTOR_REF(BgL_arg1815z00_2607,
																	BgL_arg1816z00_2608);
															}
															{	/* Return/walk.scm 165 */
																bool_t BgL__ortest_1115z00_2600;

																BgL__ortest_1115z00_2600 =
																	(BgL_classz00_2583 == BgL_oclassz00_2599);
																if (BgL__ortest_1115z00_2600)
																	{	/* Return/walk.scm 165 */
																		BgL_res2153z00_2616 =
																			BgL__ortest_1115z00_2600;
																	}
																else
																	{	/* Return/walk.scm 165 */
																		long BgL_odepthz00_2601;

																		{	/* Return/walk.scm 165 */
																			obj_t BgL_arg1804z00_2602;

																			BgL_arg1804z00_2602 =
																				(BgL_oclassz00_2599);
																			BgL_odepthz00_2601 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2602);
																		}
																		if ((3L < BgL_odepthz00_2601))
																			{	/* Return/walk.scm 165 */
																				obj_t BgL_arg1802z00_2604;

																				{	/* Return/walk.scm 165 */
																					obj_t BgL_arg1803z00_2605;

																					BgL_arg1803z00_2605 =
																						(BgL_oclassz00_2599);
																					BgL_arg1802z00_2604 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2605, 3L);
																				}
																				BgL_res2153z00_2616 =
																					(BgL_arg1802z00_2604 ==
																					BgL_classz00_2583);
																			}
																		else
																			{	/* Return/walk.scm 165 */
																				BgL_res2153z00_2616 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2409z00_5024 = BgL_res2153z00_2616;
													}
											}
										}
										if (BgL_test2409z00_5024)
											{	/* Return/walk.scm 165 */
												if (NULLP(
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_1629)))->
															BgL_argsz00)))
													{	/* Return/walk.scm 168 */
														BgL_varz00_bglt BgL_i1129z00_1635;

														BgL_i1129z00_1635 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_1629)))->
															BgL_funz00);
														{	/* Return/walk.scm 169 */
															BgL_variablez00_bglt BgL_arg1584z00_1636;

															BgL_arg1584z00_1636 =
																(((BgL_varz00_bglt)
																	COBJECT(BgL_i1129z00_1635))->BgL_variablez00);
															BgL_test2408z00_5023 =
																(((obj_t) BgL_arg1584z00_1636) ==
																BGl_za2popzd2exitz12za2zc0zzreturn_walkz00);
														}
													}
												else
													{	/* Return/walk.scm 167 */
														BgL_test2408z00_5023 = ((bool_t) 0);
													}
											}
										else
											{	/* Return/walk.scm 165 */
												BgL_test2408z00_5023 = ((bool_t) 0);
											}
									}
									if (BgL_test2408z00_5023)
										{	/* Return/walk.scm 186 */
											obj_t BgL_arg1654z00_1676;

											{	/* Return/walk.scm 186 */
												obj_t BgL_pairz00_2724;

												BgL_pairz00_2724 =
													(((BgL_sequencez00_bglt) COBJECT(
															((BgL_sequencez00_bglt) BgL_nodez00_1665)))->
													BgL_nodesz00);
												BgL_arg1654z00_1676 = CAR(CDR(BgL_pairz00_2724));
											}
											BGL_TAIL return
												BGl_step8ze70ze7zzreturn_walkz00(
												((BgL_nodez00_bglt) BgL_arg1654z00_1676),
												BgL_varz00_1666);
										}
									else
										{	/* Return/walk.scm 185 */
											return BFALSE;
										}
								}
							else
								{	/* Return/walk.scm 184 */
									return BFALSE;
								}
						}
					else
						{	/* Return/walk.scm 182 */
							return BFALSE;
						}
				}
			}
		}

	}



/* step6b~0 */
	obj_t BGl_step6bze70ze7zzreturn_walkz00(BgL_nodez00_bglt BgL_nodez00_1682,
		BgL_localz00_bglt BgL_varz00_1683, obj_t BgL_envz00_1684)
	{
		{	/* Return/walk.scm 193 */
			{
				BgL_nodez00_bglt BgL_nodez00_1638;
				obj_t BgL_envz00_1639;

				{	/* Return/walk.scm 189 */
					bool_t BgL_test2414z00_5066;

					{	/* Return/walk.scm 189 */
						obj_t BgL_classz00_2728;

						BgL_classz00_2728 = BGl_sequencez00zzast_nodez00;
						{	/* Return/walk.scm 189 */
							BgL_objectz00_bglt BgL_arg1807z00_2730;

							{	/* Return/walk.scm 189 */
								obj_t BgL_tmpz00_5067;

								BgL_tmpz00_5067 = ((obj_t) BgL_nodez00_1682);
								BgL_arg1807z00_2730 = (BgL_objectz00_bglt) (BgL_tmpz00_5067);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Return/walk.scm 189 */
									long BgL_idxz00_2736;

									BgL_idxz00_2736 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2730);
									BgL_test2414z00_5066 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2736 + 3L)) == BgL_classz00_2728);
								}
							else
								{	/* Return/walk.scm 189 */
									bool_t BgL_res2157z00_2761;

									{	/* Return/walk.scm 189 */
										obj_t BgL_oclassz00_2744;

										{	/* Return/walk.scm 189 */
											obj_t BgL_arg1815z00_2752;
											long BgL_arg1816z00_2753;

											BgL_arg1815z00_2752 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Return/walk.scm 189 */
												long BgL_arg1817z00_2754;

												BgL_arg1817z00_2754 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2730);
												BgL_arg1816z00_2753 =
													(BgL_arg1817z00_2754 - OBJECT_TYPE);
											}
											BgL_oclassz00_2744 =
												VECTOR_REF(BgL_arg1815z00_2752, BgL_arg1816z00_2753);
										}
										{	/* Return/walk.scm 189 */
											bool_t BgL__ortest_1115z00_2745;

											BgL__ortest_1115z00_2745 =
												(BgL_classz00_2728 == BgL_oclassz00_2744);
											if (BgL__ortest_1115z00_2745)
												{	/* Return/walk.scm 189 */
													BgL_res2157z00_2761 = BgL__ortest_1115z00_2745;
												}
											else
												{	/* Return/walk.scm 189 */
													long BgL_odepthz00_2746;

													{	/* Return/walk.scm 189 */
														obj_t BgL_arg1804z00_2747;

														BgL_arg1804z00_2747 = (BgL_oclassz00_2744);
														BgL_odepthz00_2746 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2747);
													}
													if ((3L < BgL_odepthz00_2746))
														{	/* Return/walk.scm 189 */
															obj_t BgL_arg1802z00_2749;

															{	/* Return/walk.scm 189 */
																obj_t BgL_arg1803z00_2750;

																BgL_arg1803z00_2750 = (BgL_oclassz00_2744);
																BgL_arg1802z00_2749 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2750,
																	3L);
															}
															BgL_res2157z00_2761 =
																(BgL_arg1802z00_2749 == BgL_classz00_2728);
														}
													else
														{	/* Return/walk.scm 189 */
															BgL_res2157z00_2761 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2414z00_5066 = BgL_res2157z00_2761;
								}
						}
					}
					if (BgL_test2414z00_5066)
						{	/* Return/walk.scm 189 */
							if (
								(bgl_list_length(
										(((BgL_sequencez00_bglt) COBJECT(
													((BgL_sequencez00_bglt) BgL_nodez00_1682)))->
											BgL_nodesz00)) == 2L))
								{	/* Return/walk.scm 192 */
									bool_t BgL_test2419z00_5094;

									{	/* Return/walk.scm 192 */
										obj_t BgL_pairz00_2763;

										BgL_pairz00_2763 =
											(((BgL_sequencez00_bglt) COBJECT(
													((BgL_sequencez00_bglt) BgL_nodez00_1682)))->
											BgL_nodesz00);
										BgL_nodez00_1638 =
											((BgL_nodez00_bglt) CAR(BgL_pairz00_2763));
									}
									BgL_envz00_1639 = BgL_envz00_1684;
									{	/* Return/walk.scm 172 */
										bool_t BgL_test2420z00_5095;

										{	/* Return/walk.scm 172 */
											obj_t BgL_classz00_2617;

											BgL_classz00_2617 = BGl_appz00zzast_nodez00;
											{	/* Return/walk.scm 172 */
												BgL_objectz00_bglt BgL_arg1807z00_2619;

												{	/* Return/walk.scm 172 */
													obj_t BgL_tmpz00_5096;

													BgL_tmpz00_5096 = ((obj_t) BgL_nodez00_1638);
													BgL_arg1807z00_2619 =
														(BgL_objectz00_bglt) (BgL_tmpz00_5096);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Return/walk.scm 172 */
														long BgL_idxz00_2625;

														BgL_idxz00_2625 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2619);
														BgL_test2420z00_5095 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2625 + 3L)) == BgL_classz00_2617);
													}
												else
													{	/* Return/walk.scm 172 */
														bool_t BgL_res2154z00_2650;

														{	/* Return/walk.scm 172 */
															obj_t BgL_oclassz00_2633;

															{	/* Return/walk.scm 172 */
																obj_t BgL_arg1815z00_2641;
																long BgL_arg1816z00_2642;

																BgL_arg1815z00_2641 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Return/walk.scm 172 */
																	long BgL_arg1817z00_2643;

																	BgL_arg1817z00_2643 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2619);
																	BgL_arg1816z00_2642 =
																		(BgL_arg1817z00_2643 - OBJECT_TYPE);
																}
																BgL_oclassz00_2633 =
																	VECTOR_REF(BgL_arg1815z00_2641,
																	BgL_arg1816z00_2642);
															}
															{	/* Return/walk.scm 172 */
																bool_t BgL__ortest_1115z00_2634;

																BgL__ortest_1115z00_2634 =
																	(BgL_classz00_2617 == BgL_oclassz00_2633);
																if (BgL__ortest_1115z00_2634)
																	{	/* Return/walk.scm 172 */
																		BgL_res2154z00_2650 =
																			BgL__ortest_1115z00_2634;
																	}
																else
																	{	/* Return/walk.scm 172 */
																		long BgL_odepthz00_2635;

																		{	/* Return/walk.scm 172 */
																			obj_t BgL_arg1804z00_2636;

																			BgL_arg1804z00_2636 =
																				(BgL_oclassz00_2633);
																			BgL_odepthz00_2635 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2636);
																		}
																		if ((3L < BgL_odepthz00_2635))
																			{	/* Return/walk.scm 172 */
																				obj_t BgL_arg1802z00_2638;

																				{	/* Return/walk.scm 172 */
																					obj_t BgL_arg1803z00_2639;

																					BgL_arg1803z00_2639 =
																						(BgL_oclassz00_2633);
																					BgL_arg1802z00_2638 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2639, 3L);
																				}
																				BgL_res2154z00_2650 =
																					(BgL_arg1802z00_2638 ==
																					BgL_classz00_2617);
																			}
																		else
																			{	/* Return/walk.scm 172 */
																				BgL_res2154z00_2650 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2420z00_5095 = BgL_res2154z00_2650;
													}
											}
										}
										if (BgL_test2420z00_5095)
											{	/* Return/walk.scm 174 */
												bool_t BgL_test2424z00_5118;

												{	/* Return/walk.scm 174 */
													bool_t BgL_test2425z00_5119;

													{	/* Return/walk.scm 174 */
														obj_t BgL_tmpz00_5120;

														BgL_tmpz00_5120 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_1638)))->
															BgL_argsz00);
														BgL_test2425z00_5119 = PAIRP(BgL_tmpz00_5120);
													}
													if (BgL_test2425z00_5119)
														{	/* Return/walk.scm 174 */
															obj_t BgL_tmpz00_5124;

															{	/* Return/walk.scm 174 */
																obj_t BgL_pairz00_2651;

																BgL_pairz00_2651 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_1638)))->
																	BgL_argsz00);
																BgL_tmpz00_5124 = CDR(BgL_pairz00_2651);
															}
															BgL_test2424z00_5118 = NULLP(BgL_tmpz00_5124);
														}
													else
														{	/* Return/walk.scm 174 */
															BgL_test2424z00_5118 = ((bool_t) 0);
														}
												}
												if (BgL_test2424z00_5118)
													{	/* Return/walk.scm 175 */
														BgL_varz00_bglt BgL_i1131z00_1649;

														BgL_i1131z00_1649 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_1638)))->
															BgL_funz00);
														{	/* Return/walk.scm 176 */
															bool_t BgL_test2426z00_5131;

															{	/* Return/walk.scm 176 */
																BgL_variablez00_bglt BgL_arg1616z00_1660;

																BgL_arg1616z00_1660 =
																	(((BgL_varz00_bglt)
																		COBJECT(BgL_i1131z00_1649))->
																	BgL_variablez00);
																BgL_test2426z00_5131 =
																	(((obj_t) BgL_arg1616z00_1660) ==
																	BGl_za2envzd2popzd2exitz12za2z12zzreturn_walkz00);
															}
															if (BgL_test2426z00_5131)
																{	/* Return/walk.scm 177 */
																	bool_t BgL_test2427z00_5135;

																	{	/* Return/walk.scm 177 */
																		obj_t BgL_arg1613z00_1658;

																		{	/* Return/walk.scm 177 */
																			obj_t BgL_pairz00_2652;

																			BgL_pairz00_2652 =
																				(((BgL_appz00_bglt) COBJECT(
																						((BgL_appz00_bglt)
																							BgL_nodez00_1638)))->BgL_argsz00);
																			BgL_arg1613z00_1658 =
																				CAR(BgL_pairz00_2652);
																		}
																		{	/* Return/walk.scm 177 */
																			obj_t BgL_classz00_2653;

																			BgL_classz00_2653 =
																				BGl_varz00zzast_nodez00;
																			if (BGL_OBJECTP(BgL_arg1613z00_1658))
																				{	/* Return/walk.scm 177 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_2655;
																					BgL_arg1807z00_2655 =
																						(BgL_objectz00_bglt)
																						(BgL_arg1613z00_1658);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Return/walk.scm 177 */
																							long BgL_idxz00_2661;

																							BgL_idxz00_2661 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_2655);
																							BgL_test2427z00_5135 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_2661 + 2L)) ==
																								BgL_classz00_2653);
																						}
																					else
																						{	/* Return/walk.scm 177 */
																							bool_t BgL_res2155z00_2686;

																							{	/* Return/walk.scm 177 */
																								obj_t BgL_oclassz00_2669;

																								{	/* Return/walk.scm 177 */
																									obj_t BgL_arg1815z00_2677;
																									long BgL_arg1816z00_2678;

																									BgL_arg1815z00_2677 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Return/walk.scm 177 */
																										long BgL_arg1817z00_2679;

																										BgL_arg1817z00_2679 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_2655);
																										BgL_arg1816z00_2678 =
																											(BgL_arg1817z00_2679 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_2669 =
																										VECTOR_REF
																										(BgL_arg1815z00_2677,
																										BgL_arg1816z00_2678);
																								}
																								{	/* Return/walk.scm 177 */
																									bool_t
																										BgL__ortest_1115z00_2670;
																									BgL__ortest_1115z00_2670 =
																										(BgL_classz00_2653 ==
																										BgL_oclassz00_2669);
																									if (BgL__ortest_1115z00_2670)
																										{	/* Return/walk.scm 177 */
																											BgL_res2155z00_2686 =
																												BgL__ortest_1115z00_2670;
																										}
																									else
																										{	/* Return/walk.scm 177 */
																											long BgL_odepthz00_2671;

																											{	/* Return/walk.scm 177 */
																												obj_t
																													BgL_arg1804z00_2672;
																												BgL_arg1804z00_2672 =
																													(BgL_oclassz00_2669);
																												BgL_odepthz00_2671 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_2672);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_2671))
																												{	/* Return/walk.scm 177 */
																													obj_t
																														BgL_arg1802z00_2674;
																													{	/* Return/walk.scm 177 */
																														obj_t
																															BgL_arg1803z00_2675;
																														BgL_arg1803z00_2675
																															=
																															(BgL_oclassz00_2669);
																														BgL_arg1802z00_2674
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_2675,
																															2L);
																													}
																													BgL_res2155z00_2686 =
																														(BgL_arg1802z00_2674
																														==
																														BgL_classz00_2653);
																												}
																											else
																												{	/* Return/walk.scm 177 */
																													BgL_res2155z00_2686 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2427z00_5135 =
																								BgL_res2155z00_2686;
																						}
																				}
																			else
																				{	/* Return/walk.scm 177 */
																					BgL_test2427z00_5135 = ((bool_t) 0);
																				}
																		}
																	}
																	if (BgL_test2427z00_5135)
																		{	/* Return/walk.scm 179 */
																			obj_t BgL_tmpz00_5161;

																			{
																				BgL_variablez00_bglt BgL_auxz00_5162;

																				{
																					BgL_varz00_bglt BgL_auxz00_5163;

																					{	/* Return/walk.scm 178 */
																						obj_t BgL_pairz00_2687;

																						BgL_pairz00_2687 =
																							(((BgL_appz00_bglt) COBJECT(
																									((BgL_appz00_bglt)
																										BgL_nodez00_1638)))->
																							BgL_argsz00);
																						BgL_auxz00_5163 =
																							((BgL_varz00_bglt)
																							CAR(BgL_pairz00_2687));
																					}
																					BgL_auxz00_5162 =
																						(((BgL_varz00_bglt)
																							COBJECT(BgL_auxz00_5163))->
																						BgL_variablez00);
																				}
																				BgL_tmpz00_5161 =
																					((obj_t) BgL_auxz00_5162);
																			}
																			BgL_test2419z00_5094 =
																				(BgL_tmpz00_5161 == BgL_envz00_1639);
																		}
																	else
																		{	/* Return/walk.scm 177 */
																			BgL_test2419z00_5094 = ((bool_t) 0);
																		}
																}
															else
																{	/* Return/walk.scm 176 */
																	BgL_test2419z00_5094 = ((bool_t) 0);
																}
														}
													}
												else
													{	/* Return/walk.scm 174 */
														BgL_test2419z00_5094 = ((bool_t) 0);
													}
											}
										else
											{	/* Return/walk.scm 172 */
												BgL_test2419z00_5094 = ((bool_t) 0);
											}
									}
									if (BgL_test2419z00_5094)
										{	/* Return/walk.scm 193 */
											obj_t BgL_arg1699z00_1694;

											{	/* Return/walk.scm 193 */
												obj_t BgL_pairz00_2764;

												BgL_pairz00_2764 =
													(((BgL_sequencez00_bglt) COBJECT(
															((BgL_sequencez00_bglt) BgL_nodez00_1682)))->
													BgL_nodesz00);
												BgL_arg1699z00_1694 = CAR(CDR(BgL_pairz00_2764));
											}
											return
												BGl_step8ze70ze7zzreturn_walkz00(
												((BgL_nodez00_bglt) BgL_arg1699z00_1694),
												BgL_varz00_1683);
										}
									else
										{	/* Return/walk.scm 192 */
											return BFALSE;
										}
								}
							else
								{	/* Return/walk.scm 191 */
									return BFALSE;
								}
						}
					else
						{	/* Return/walk.scm 189 */
							return BFALSE;
						}
				}
			}
		}

	}



/* is-unwind-until? */
	BGL_EXPORTED_DEF bool_t
		BGl_iszd2unwindzd2untilzf3zf3zzreturn_walkz00(BgL_variablez00_bglt
		BgL_vz00_20)
	{
		{	/* Return/walk.scm 328 */
			return
				(
				((obj_t) BgL_vz00_20) ==
				BGl_za2unwindzd2untilz12za2zc0zzreturn_walkz00);
		}

	}



/* &is-unwind-until? */
	obj_t BGl_z62iszd2unwindzd2untilzf3z91zzreturn_walkz00(obj_t BgL_envz00_3724,
		obj_t BgL_vz00_3725)
	{
		{	/* Return/walk.scm 328 */
			return
				BBOOL(BGl_iszd2unwindzd2untilzf3zf3zzreturn_walkz00(
					((BgL_variablez00_bglt) BgL_vz00_3725)));
		}

	}



/* is-get-exitd-top? */
	BGL_EXPORTED_DEF bool_t
		BGl_iszd2getzd2exitdzd2topzf3z21zzreturn_walkz00(BgL_variablez00_bglt
		BgL_vz00_21)
	{
		{	/* Return/walk.scm 334 */
			return
				(
				((obj_t) BgL_vz00_21) ==
				BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00);
		}

	}



/* &is-get-exitd-top? */
	obj_t BGl_z62iszd2getzd2exitdzd2topzf3z43zzreturn_walkz00(obj_t
		BgL_envz00_3726, obj_t BgL_vz00_3727)
	{
		{	/* Return/walk.scm 334 */
			return
				BBOOL(BGl_iszd2getzd2exitdzd2topzf3z21zzreturn_walkz00(
					((BgL_variablez00_bglt) BgL_vz00_3727)));
		}

	}



/* is-exit-return? */
	BGL_EXPORTED_DEF bool_t
		BGl_iszd2exitzd2returnzf3zf3zzreturn_walkz00(BgL_nodez00_bglt
		BgL_nodez00_22, BgL_localz00_bglt BgL_exitvarz00_23)
	{
		{	/* Return/walk.scm 342 */
			return
				CBOOL(BGl_zc3z04exitza31982ze3ze70z60zzreturn_walkz00(BgL_nodez00_22,
					BgL_exitvarz00_23));
		}

	}



/* <@exit:1982>~0 */
	obj_t BGl_zc3z04exitza31982ze3ze70z60zzreturn_walkz00(BgL_nodez00_bglt
		BgL_nodez00_3782, BgL_localz00_bglt BgL_exitvarz00_3781)
	{
		{	/* Return/walk.scm 343 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1179z00_1991;

			if (SET_EXIT(BgL_an_exit1179z00_1991))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1179z00_1991 = (void *) jmpbuf;
					{	/* Return/walk.scm 343 */
						obj_t BgL_env1183z00_1992;

						BgL_env1183z00_1992 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1183z00_1992, BgL_an_exit1179z00_1991, 1L);
						{	/* Return/walk.scm 343 */
							obj_t BgL_an_exitd1180z00_1993;

							BgL_an_exitd1180z00_1993 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1183z00_1992);
							{	/* Return/walk.scm 343 */
								obj_t BgL_abortz00_3728;

								BgL_abortz00_3728 =
									MAKE_FX_PROCEDURE(BGl_z62abortz62zzreturn_walkz00,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL_abortz00_3728,
									(int) (0L), BgL_an_exitd1180z00_1993);
								{	/* Return/walk.scm 343 */
									bool_t BgL_res1182z00_1996;

									{	/* Return/walk.scm 344 */
										obj_t BgL_arg1983z00_1997;

										{	/* Return/walk.scm 344 */
											obj_t BgL_list1984z00_1998;

											BgL_list1984z00_1998 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_exitvarz00_3781), BNIL);
											BgL_arg1983z00_1997 = BgL_list1984z00_1998;
										}
										BGl_iszd2returnzf3z21zzreturn_walkz00(BgL_nodez00_3782,
											BgL_arg1983z00_1997, BgL_abortz00_3728);
									}
									BgL_res1182z00_1996 = ((bool_t) 1);
									POP_ENV_EXIT(BgL_env1183z00_1992);
									return BBOOL(BgL_res1182z00_1996);
								}
							}
						}
					}
				}
		}

	}



/* &is-exit-return? */
	obj_t BGl_z62iszd2exitzd2returnzf3z91zzreturn_walkz00(obj_t BgL_envz00_3729,
		obj_t BgL_nodez00_3730, obj_t BgL_exitvarz00_3731)
	{
		{	/* Return/walk.scm 342 */
			return
				BBOOL(BGl_iszd2exitzd2returnzf3zf3zzreturn_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_3730),
					((BgL_localz00_bglt) BgL_exitvarz00_3731)));
		}

	}



/* &abort */
	obj_t BGl_z62abortz62zzreturn_walkz00(obj_t BgL_envz00_3732,
		obj_t BgL_val1181z00_3734)
	{
		{	/* Return/walk.scm 343 */
			return
				unwind_stack_until(PROCEDURE_REF(BgL_envz00_3732,
					(int) (0L)), BFALSE, BgL_val1181z00_3734, BFALSE, BFALSE);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzreturn_walkz00(void)
	{
		{	/* Return/walk.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzreturn_walkz00(void)
	{
		{	/* Return/walk.scm 16 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_iszd2returnzf3zd2envzf3zzreturn_walkz00,
				BGl_proc2204z00zzreturn_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2205z00zzreturn_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_returnz12zd2envzc0zzreturn_walkz00,
				BGl_proc2206z00zzreturn_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2207z00zzreturn_walkz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_returnzd2gotozd2funsz12zd2envzc0zzreturn_walkz00,
				BGl_proc2208z00zzreturn_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2209z00zzreturn_walkz00);
		}

	}



/* &return-goto-funs!1339 */
	obj_t BGl_z62returnzd2gotozd2funsz121339z70zzreturn_walkz00(obj_t
		BgL_envz00_3748, obj_t BgL_nodez00_3749)
	{
		{	/* Return/walk.scm 435 */
			return
				((obj_t)
				BGl_walk0z12z12zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_3749),
					BGl_returnzd2gotozd2funsz12zd2envzc0zzreturn_walkz00));
		}

	}



/* &return!1335 */
	obj_t BGl_z62returnz121335z70zzreturn_walkz00(obj_t BgL_envz00_3750,
		obj_t BgL_nodez00_3751, obj_t BgL_exitvarz00_3752, obj_t BgL_rblockz00_3753)
	{
		{	/* Return/walk.scm 409 */
			{
				BgL_nodez00_bglt BgL_auxz00_5221;

				{

					BgL_auxz00_5221 =
						BGl_walk2z12z12zzast_walkz00(
						((BgL_nodez00_bglt) BgL_nodez00_3751),
						BGl_returnz12zd2envzc0zzreturn_walkz00,
						((obj_t) ((BgL_localz00_bglt) BgL_exitvarz00_3752)),
						((obj_t) ((BgL_retblockz00_bglt) BgL_rblockz00_3753)));
				}
				return ((obj_t) BgL_auxz00_5221);
			}
		}

	}



/* &is-return?1327 */
	obj_t BGl_z62iszd2returnzf31327z43zzreturn_walkz00(obj_t BgL_envz00_3754,
		obj_t BgL_nodez00_3755, obj_t BgL_exitvarz00_3756, obj_t BgL_abortz00_3757)
	{
		{	/* Return/walk.scm 350 */
			{

				return
					BGl_walk2z00zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_3755),
					BGl_iszd2returnzf3zd2envzf3zzreturn_walkz00,
					((obj_t) BgL_exitvarz00_3756), BgL_abortz00_3757);
			}
		}

	}



/* is-return? */
	obj_t BGl_iszd2returnzf3z21zzreturn_walkz00(BgL_nodez00_bglt BgL_nodez00_24,
		obj_t BgL_exitvarz00_25, obj_t BgL_abortz00_26)
	{
		{	/* Return/walk.scm 350 */
			{	/* Return/walk.scm 350 */
				obj_t BgL_method1328z00_2056;

				{	/* Return/walk.scm 350 */
					obj_t BgL_res2181z00_3490;

					{	/* Return/walk.scm 350 */
						long BgL_objzd2classzd2numz00_3461;

						BgL_objzd2classzd2numz00_3461 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_24));
						{	/* Return/walk.scm 350 */
							obj_t BgL_arg1811z00_3462;

							BgL_arg1811z00_3462 =
								PROCEDURE_REF(BGl_iszd2returnzf3zd2envzf3zzreturn_walkz00,
								(int) (1L));
							{	/* Return/walk.scm 350 */
								int BgL_offsetz00_3465;

								BgL_offsetz00_3465 = (int) (BgL_objzd2classzd2numz00_3461);
								{	/* Return/walk.scm 350 */
									long BgL_offsetz00_3466;

									BgL_offsetz00_3466 =
										((long) (BgL_offsetz00_3465) - OBJECT_TYPE);
									{	/* Return/walk.scm 350 */
										long BgL_modz00_3467;

										BgL_modz00_3467 =
											(BgL_offsetz00_3466 >> (int) ((long) ((int) (4L))));
										{	/* Return/walk.scm 350 */
											long BgL_restz00_3469;

											BgL_restz00_3469 =
												(BgL_offsetz00_3466 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Return/walk.scm 350 */

												{	/* Return/walk.scm 350 */
													obj_t BgL_bucketz00_3471;

													BgL_bucketz00_3471 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3462), BgL_modz00_3467);
													BgL_res2181z00_3490 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3471), BgL_restz00_3469);
					}}}}}}}}
					BgL_method1328z00_2056 = BgL_res2181z00_3490;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1328z00_2056,
					((obj_t) BgL_nodez00_24), BgL_exitvarz00_25, BgL_abortz00_26);
			}
		}

	}



/* &is-return? */
	obj_t BGl_z62iszd2returnzf3z43zzreturn_walkz00(obj_t BgL_envz00_3744,
		obj_t BgL_nodez00_3745, obj_t BgL_exitvarz00_3746, obj_t BgL_abortz00_3747)
	{
		{	/* Return/walk.scm 350 */
			return
				BGl_iszd2returnzf3z21zzreturn_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3745), BgL_exitvarz00_3746,
				BgL_abortz00_3747);
		}

	}



/* return! */
	BgL_nodez00_bglt BGl_returnz12z12zzreturn_walkz00(BgL_nodez00_bglt
		BgL_nodez00_36, BgL_localz00_bglt BgL_exitvarz00_37,
		BgL_retblockz00_bglt BgL_rblockz00_38)
	{
		{	/* Return/walk.scm 409 */
			{	/* Return/walk.scm 409 */
				obj_t BgL_method1336z00_2057;

				{	/* Return/walk.scm 409 */
					obj_t BgL_res2186z00_3521;

					{	/* Return/walk.scm 409 */
						long BgL_objzd2classzd2numz00_3492;

						BgL_objzd2classzd2numz00_3492 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_36));
						{	/* Return/walk.scm 409 */
							obj_t BgL_arg1811z00_3493;

							BgL_arg1811z00_3493 =
								PROCEDURE_REF(BGl_returnz12zd2envzc0zzreturn_walkz00,
								(int) (1L));
							{	/* Return/walk.scm 409 */
								int BgL_offsetz00_3496;

								BgL_offsetz00_3496 = (int) (BgL_objzd2classzd2numz00_3492);
								{	/* Return/walk.scm 409 */
									long BgL_offsetz00_3497;

									BgL_offsetz00_3497 =
										((long) (BgL_offsetz00_3496) - OBJECT_TYPE);
									{	/* Return/walk.scm 409 */
										long BgL_modz00_3498;

										BgL_modz00_3498 =
											(BgL_offsetz00_3497 >> (int) ((long) ((int) (4L))));
										{	/* Return/walk.scm 409 */
											long BgL_restz00_3500;

											BgL_restz00_3500 =
												(BgL_offsetz00_3497 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Return/walk.scm 409 */

												{	/* Return/walk.scm 409 */
													obj_t BgL_bucketz00_3502;

													BgL_bucketz00_3502 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3493), BgL_modz00_3498);
													BgL_res2186z00_3521 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3502), BgL_restz00_3500);
					}}}}}}}}
					BgL_method1336z00_2057 = BgL_res2186z00_3521;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL3(BgL_method1336z00_2057,
						((obj_t) BgL_nodez00_36),
						((obj_t) BgL_exitvarz00_37), ((obj_t) BgL_rblockz00_38)));
			}
		}

	}



/* &return! */
	BgL_nodez00_bglt BGl_z62returnz12z70zzreturn_walkz00(obj_t BgL_envz00_3739,
		obj_t BgL_nodez00_3740, obj_t BgL_exitvarz00_3741, obj_t BgL_rblockz00_3742)
	{
		{	/* Return/walk.scm 409 */
			return
				BGl_returnz12z12zzreturn_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3740),
				((BgL_localz00_bglt) BgL_exitvarz00_3741),
				((BgL_retblockz00_bglt) BgL_rblockz00_3742));
		}

	}



/* return-goto-funs! */
	BgL_nodez00_bglt
		BGl_returnzd2gotozd2funsz12z12zzreturn_walkz00(BgL_nodez00_bglt
		BgL_nodez00_42)
	{
		{	/* Return/walk.scm 435 */
			{	/* Return/walk.scm 435 */
				obj_t BgL_method1340z00_2058;

				{	/* Return/walk.scm 435 */
					obj_t BgL_res2191z00_3552;

					{	/* Return/walk.scm 435 */
						long BgL_objzd2classzd2numz00_3523;

						BgL_objzd2classzd2numz00_3523 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_42));
						{	/* Return/walk.scm 435 */
							obj_t BgL_arg1811z00_3524;

							BgL_arg1811z00_3524 =
								PROCEDURE_REF
								(BGl_returnzd2gotozd2funsz12zd2envzc0zzreturn_walkz00,
								(int) (1L));
							{	/* Return/walk.scm 435 */
								int BgL_offsetz00_3527;

								BgL_offsetz00_3527 = (int) (BgL_objzd2classzd2numz00_3523);
								{	/* Return/walk.scm 435 */
									long BgL_offsetz00_3528;

									BgL_offsetz00_3528 =
										((long) (BgL_offsetz00_3527) - OBJECT_TYPE);
									{	/* Return/walk.scm 435 */
										long BgL_modz00_3529;

										BgL_modz00_3529 =
											(BgL_offsetz00_3528 >> (int) ((long) ((int) (4L))));
										{	/* Return/walk.scm 435 */
											long BgL_restz00_3531;

											BgL_restz00_3531 =
												(BgL_offsetz00_3528 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Return/walk.scm 435 */

												{	/* Return/walk.scm 435 */
													obj_t BgL_bucketz00_3533;

													BgL_bucketz00_3533 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3524), BgL_modz00_3529);
													BgL_res2191z00_3552 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3533), BgL_restz00_3531);
					}}}}}}}}
					BgL_method1340z00_2058 = BgL_res2191z00_3552;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1340z00_2058,
						((obj_t) BgL_nodez00_42)));
			}
		}

	}



/* &return-goto-funs! */
	BgL_nodez00_bglt BGl_z62returnzd2gotozd2funsz12z70zzreturn_walkz00(obj_t
		BgL_envz00_3736, obj_t BgL_nodez00_3737)
	{
		{	/* Return/walk.scm 435 */
			return
				BGl_returnzd2gotozd2funsz12z12zzreturn_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3737));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzreturn_walkz00(void)
	{
		{	/* Return/walk.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_iszd2returnzf3zd2envzf3zzreturn_walkz00, BGl_varz00zzast_nodez00,
				BGl_proc2210z00zzreturn_walkz00, BGl_string2211z00zzreturn_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_iszd2returnzf3zd2envzf3zzreturn_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2212z00zzreturn_walkz00,
				BGl_string2211z00zzreturn_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_iszd2returnzf3zd2envzf3zzreturn_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2213z00zzreturn_walkz00, BGl_string2211z00zzreturn_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_returnz12zd2envzc0zzreturn_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2214z00zzreturn_walkz00, BGl_string2215z00zzreturn_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_returnzd2gotozd2funsz12zd2envzc0zzreturn_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2216z00zzreturn_walkz00,
				BGl_string2217z00zzreturn_walkz00);
		}

	}



/* &return-goto-funs!-le1342 */
	BgL_nodez00_bglt
		BGl_z62returnzd2gotozd2funsz12zd2le1342za2zzreturn_walkz00(obj_t
		BgL_envz00_3763, obj_t BgL_nodez00_3764)
	{
		{	/* Return/walk.scm 441 */
			{	/* Return/walk.scm 443 */
				obj_t BgL_g1326z00_3804;

				BgL_g1326z00_3804 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_3764)))->BgL_localsz00);
				{
					obj_t BgL_l1324z00_3806;

					BgL_l1324z00_3806 = BgL_g1326z00_3804;
				BgL_zc3z04anonymousza32081ze3z87_3805:
					if (PAIRP(BgL_l1324z00_3806))
						{	/* Return/walk.scm 443 */
							BGl_returnzd2funz12zc0zzreturn_walkz00(CAR(BgL_l1324z00_3806));
							{
								obj_t BgL_l1324z00_5349;

								BgL_l1324z00_5349 = CDR(BgL_l1324z00_3806);
								BgL_l1324z00_3806 = BgL_l1324z00_5349;
								goto BgL_zc3z04anonymousza32081ze3z87_3805;
							}
						}
					else
						{	/* Return/walk.scm 443 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5351;

				{	/* Return/walk.scm 444 */
					BgL_nodez00_bglt BgL_arg2086z00_3807;

					BgL_arg2086z00_3807 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3764)))->BgL_bodyz00);
					BgL_auxz00_5351 =
						BGl_returnzd2gotozd2funsz12z12zzreturn_walkz00(BgL_arg2086z00_3807);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3764)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5351), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_3764));
		}

	}



/* &return!-app1338 */
	BgL_nodez00_bglt BGl_z62returnz12zd2app1338za2zzreturn_walkz00(obj_t
		BgL_envz00_3765, obj_t BgL_nodez00_3766, obj_t BgL_exitvarz00_3767,
		obj_t BgL_rblockz00_3768)
	{
		{	/* Return/walk.scm 415 */
			{

				{	/* Return/walk.scm 417 */
					BgL_varz00_bglt BgL_i1196z00_3812;

					BgL_i1196z00_3812 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_3766)))->BgL_funz00);
					{	/* Return/walk.scm 418 */
						bool_t BgL_test2433z00_5361;

						{	/* Return/walk.scm 418 */
							BgL_variablez00_bglt BgL_arg2077z00_3813;

							BgL_arg2077z00_3813 =
								(((BgL_varz00_bglt) COBJECT(BgL_i1196z00_3812))->
								BgL_variablez00);
							BgL_test2433z00_5361 =
								(((obj_t) BgL_arg2077z00_3813) ==
								BGl_za2unwindzd2untilz12za2zc0zzreturn_walkz00);
						}
						if (BgL_test2433z00_5361)
							{	/* Return/walk.scm 420 */
								bool_t BgL_test2434z00_5365;

								{	/* Return/walk.scm 420 */
									obj_t BgL_arg2075z00_3814;

									BgL_arg2075z00_3814 =
										CAR(
										(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_3766)))->BgL_argsz00));
									{	/* Return/walk.scm 420 */
										obj_t BgL_classz00_3815;

										BgL_classz00_3815 = BGl_varz00zzast_nodez00;
										if (BGL_OBJECTP(BgL_arg2075z00_3814))
											{	/* Return/walk.scm 420 */
												BgL_objectz00_bglt BgL_arg1807z00_3816;

												BgL_arg1807z00_3816 =
													(BgL_objectz00_bglt) (BgL_arg2075z00_3814);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Return/walk.scm 420 */
														long BgL_idxz00_3817;

														BgL_idxz00_3817 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3816);
														BgL_test2434z00_5365 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3817 + 2L)) == BgL_classz00_3815);
													}
												else
													{	/* Return/walk.scm 420 */
														bool_t BgL_res2194z00_3820;

														{	/* Return/walk.scm 420 */
															obj_t BgL_oclassz00_3821;

															{	/* Return/walk.scm 420 */
																obj_t BgL_arg1815z00_3822;
																long BgL_arg1816z00_3823;

																BgL_arg1815z00_3822 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Return/walk.scm 420 */
																	long BgL_arg1817z00_3824;

																	BgL_arg1817z00_3824 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3816);
																	BgL_arg1816z00_3823 =
																		(BgL_arg1817z00_3824 - OBJECT_TYPE);
																}
																BgL_oclassz00_3821 =
																	VECTOR_REF(BgL_arg1815z00_3822,
																	BgL_arg1816z00_3823);
															}
															{	/* Return/walk.scm 420 */
																bool_t BgL__ortest_1115z00_3825;

																BgL__ortest_1115z00_3825 =
																	(BgL_classz00_3815 == BgL_oclassz00_3821);
																if (BgL__ortest_1115z00_3825)
																	{	/* Return/walk.scm 420 */
																		BgL_res2194z00_3820 =
																			BgL__ortest_1115z00_3825;
																	}
																else
																	{	/* Return/walk.scm 420 */
																		long BgL_odepthz00_3826;

																		{	/* Return/walk.scm 420 */
																			obj_t BgL_arg1804z00_3827;

																			BgL_arg1804z00_3827 =
																				(BgL_oclassz00_3821);
																			BgL_odepthz00_3826 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3827);
																		}
																		if ((2L < BgL_odepthz00_3826))
																			{	/* Return/walk.scm 420 */
																				obj_t BgL_arg1802z00_3828;

																				{	/* Return/walk.scm 420 */
																					obj_t BgL_arg1803z00_3829;

																					BgL_arg1803z00_3829 =
																						(BgL_oclassz00_3821);
																					BgL_arg1802z00_3828 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3829, 2L);
																				}
																				BgL_res2194z00_3820 =
																					(BgL_arg1802z00_3828 ==
																					BgL_classz00_3815);
																			}
																		else
																			{	/* Return/walk.scm 420 */
																				BgL_res2194z00_3820 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2434z00_5365 = BgL_res2194z00_3820;
													}
											}
										else
											{	/* Return/walk.scm 420 */
												BgL_test2434z00_5365 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2434z00_5365)
									{	/* Return/walk.scm 421 */
										BgL_varz00_bglt BgL_i1197z00_3830;

										{	/* Return/walk.scm 421 */
											obj_t BgL_pairz00_3831;

											BgL_pairz00_3831 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_3766)))->
												BgL_argsz00);
											BgL_i1197z00_3830 =
												((BgL_varz00_bglt) CAR(BgL_pairz00_3831));
										}
										if (
											(((obj_t)
													(((BgL_varz00_bglt) COBJECT(BgL_i1197z00_3830))->
														BgL_variablez00)) ==
												((obj_t) ((BgL_localz00_bglt) BgL_exitvarz00_3767))))
											{	/* Return/walk.scm 423 */
												BgL_returnz00_bglt BgL_new1200z00_3832;

												{	/* Return/walk.scm 424 */
													BgL_returnz00_bglt BgL_new1198z00_3833;

													BgL_new1198z00_3833 =
														((BgL_returnz00_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_returnz00_bgl))));
													{	/* Return/walk.scm 424 */
														long BgL_arg2070z00_3834;

														BgL_arg2070z00_3834 =
															BGL_CLASS_NUM(BGl_returnz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1198z00_3833),
															BgL_arg2070z00_3834);
													}
													{	/* Return/walk.scm 424 */
														BgL_objectz00_bglt BgL_tmpz00_5405;

														BgL_tmpz00_5405 =
															((BgL_objectz00_bglt) BgL_new1198z00_3833);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5405, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1198z00_3833);
													BgL_new1200z00_3832 = BgL_new1198z00_3833;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1200z00_3832)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
																			BgL_nodez00_3766))))->BgL_locz00)),
													BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1200z00_3832)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) (
																			(BgL_retblockz00_bglt)
																			BgL_rblockz00_3768))))->BgL_typez00)),
													BUNSPEC);
												((((BgL_returnz00_bglt) COBJECT(BgL_new1200z00_3832))->
														BgL_blockz00) =
													((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt)
															BgL_rblockz00_3768)), BUNSPEC);
												{
													BgL_nodez00_bglt BgL_auxz00_5421;

													{	/* Return/walk.scm 427 */
														obj_t BgL_pairz00_3835;

														BgL_pairz00_3835 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_3766)))->
															BgL_argsz00);
														{	/* Return/walk.scm 427 */
															obj_t BgL_pairz00_3836;

															{	/* Return/walk.scm 427 */
																obj_t BgL_pairz00_3837;

																BgL_pairz00_3837 = CDR(BgL_pairz00_3835);
																BgL_pairz00_3836 = CDR(BgL_pairz00_3837);
															}
															BgL_auxz00_5421 =
																((BgL_nodez00_bglt) CAR(BgL_pairz00_3836));
													}}
													((((BgL_returnz00_bglt)
																COBJECT(BgL_new1200z00_3832))->BgL_valuez00) =
														((BgL_nodez00_bglt) BgL_auxz00_5421), BUNSPEC);
												}
												return ((BgL_nodez00_bglt) BgL_new1200z00_3832);
											}
										else
											{	/* Return/walk.scm 422 */
											BgL_callzd2defaultzd2walkerz00_3811:
												return
													BGl_walk2z12z12zzast_walkz00(
													((BgL_nodez00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_3766)),
													BGl_returnz12zd2envzc0zzreturn_walkz00,
													((obj_t) ((BgL_localz00_bglt) BgL_exitvarz00_3767)),
													((obj_t) ((BgL_retblockz00_bglt)
															BgL_rblockz00_3768)));
											}
									}
								else
									{	/* Return/walk.scm 420 */
										goto BgL_callzd2defaultzd2walkerz00_3811;
									}
							}
						else
							{	/* Return/walk.scm 418 */
								goto BgL_callzd2defaultzd2walkerz00_3811;
							}
					}
				}
			}
		}

	}



/* &is-return?-app1334 */
	obj_t BGl_z62iszd2returnzf3zd2app1334z91zzreturn_walkz00(obj_t
		BgL_envz00_3769, obj_t BgL_nodez00_3770, obj_t BgL_exitvarz00_3771,
		obj_t BgL_abortz00_3772)
	{
		{	/* Return/walk.scm 388 */
			{

				{	/* Return/walk.scm 390 */
					BgL_varz00_bglt BgL_i1190z00_3840;

					BgL_i1190z00_3840 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_3770)))->BgL_funz00);
					{	/* Return/walk.scm 392 */
						bool_t BgL_test2440z00_5439;

						{	/* Return/walk.scm 392 */
							BgL_variablez00_bglt BgL_arg2058z00_3841;

							BgL_arg2058z00_3841 =
								(((BgL_varz00_bglt) COBJECT(BgL_i1190z00_3840))->
								BgL_variablez00);
							BgL_test2440z00_5439 =
								(((obj_t) BgL_arg2058z00_3841) ==
								BGl_za2unwindzd2untilz12za2zc0zzreturn_walkz00);
						}
						if (BgL_test2440z00_5439)
							{	/* Return/walk.scm 393 */
								bool_t BgL_test2441z00_5443;

								{	/* Return/walk.scm 393 */
									obj_t BgL_arg2044z00_3842;

									BgL_arg2044z00_3842 =
										CAR(
										(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_3770)))->BgL_argsz00));
									{	/* Return/walk.scm 393 */
										obj_t BgL_classz00_3843;

										BgL_classz00_3843 = BGl_varz00zzast_nodez00;
										if (BGL_OBJECTP(BgL_arg2044z00_3842))
											{	/* Return/walk.scm 393 */
												BgL_objectz00_bglt BgL_arg1807z00_3844;

												BgL_arg1807z00_3844 =
													(BgL_objectz00_bglt) (BgL_arg2044z00_3842);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Return/walk.scm 393 */
														long BgL_idxz00_3845;

														BgL_idxz00_3845 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3844);
														BgL_test2441z00_5443 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3845 + 2L)) == BgL_classz00_3843);
													}
												else
													{	/* Return/walk.scm 393 */
														bool_t BgL_res2193z00_3848;

														{	/* Return/walk.scm 393 */
															obj_t BgL_oclassz00_3849;

															{	/* Return/walk.scm 393 */
																obj_t BgL_arg1815z00_3850;
																long BgL_arg1816z00_3851;

																BgL_arg1815z00_3850 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Return/walk.scm 393 */
																	long BgL_arg1817z00_3852;

																	BgL_arg1817z00_3852 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3844);
																	BgL_arg1816z00_3851 =
																		(BgL_arg1817z00_3852 - OBJECT_TYPE);
																}
																BgL_oclassz00_3849 =
																	VECTOR_REF(BgL_arg1815z00_3850,
																	BgL_arg1816z00_3851);
															}
															{	/* Return/walk.scm 393 */
																bool_t BgL__ortest_1115z00_3853;

																BgL__ortest_1115z00_3853 =
																	(BgL_classz00_3843 == BgL_oclassz00_3849);
																if (BgL__ortest_1115z00_3853)
																	{	/* Return/walk.scm 393 */
																		BgL_res2193z00_3848 =
																			BgL__ortest_1115z00_3853;
																	}
																else
																	{	/* Return/walk.scm 393 */
																		long BgL_odepthz00_3854;

																		{	/* Return/walk.scm 393 */
																			obj_t BgL_arg1804z00_3855;

																			BgL_arg1804z00_3855 =
																				(BgL_oclassz00_3849);
																			BgL_odepthz00_3854 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3855);
																		}
																		if ((2L < BgL_odepthz00_3854))
																			{	/* Return/walk.scm 393 */
																				obj_t BgL_arg1802z00_3856;

																				{	/* Return/walk.scm 393 */
																					obj_t BgL_arg1803z00_3857;

																					BgL_arg1803z00_3857 =
																						(BgL_oclassz00_3849);
																					BgL_arg1802z00_3856 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3857, 2L);
																				}
																				BgL_res2193z00_3848 =
																					(BgL_arg1802z00_3856 ==
																					BgL_classz00_3843);
																			}
																		else
																			{	/* Return/walk.scm 393 */
																				BgL_res2193z00_3848 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2441z00_5443 = BgL_res2193z00_3848;
													}
											}
										else
											{	/* Return/walk.scm 393 */
												BgL_test2441z00_5443 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2441z00_5443)
									{	/* Return/walk.scm 394 */
										BgL_varz00_bglt BgL_i1191z00_3858;

										{	/* Return/walk.scm 394 */
											obj_t BgL_pairz00_3859;

											BgL_pairz00_3859 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_3770)))->
												BgL_argsz00);
											BgL_i1191z00_3858 =
												((BgL_varz00_bglt) CAR(BgL_pairz00_3859));
										}
										{	/* Return/walk.scm 395 */
											obj_t BgL__ortest_1192z00_3860;

											if (PAIRP(BgL_exitvarz00_3771))
												{	/* Return/walk.scm 395 */
													BgL__ortest_1192z00_3860 =
														BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
														((obj_t)
															(((BgL_varz00_bglt) COBJECT(BgL_i1191z00_3858))->
																BgL_variablez00)), BgL_exitvarz00_3771);
												}
											else
												{	/* Return/walk.scm 395 */
													BgL__ortest_1192z00_3860 = BFALSE;
												}
											if (CBOOL(BgL__ortest_1192z00_3860))
												{	/* Return/walk.scm 395 */
													return BgL__ortest_1192z00_3860;
												}
											else
												{	/* Return/walk.scm 396 */
													obj_t BgL_arg2039z00_3861;

													BgL_arg2039z00_3861 =
														CAR(
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_3770)))->
															BgL_argsz00));
													return
														BGl_iszd2returnzf3z21zzreturn_walkz00((
															(BgL_nodez00_bglt) BgL_arg2039z00_3861),
														BgL_exitvarz00_3771, BgL_abortz00_3772);
												}
										}
									}
								else
									{	/* Return/walk.scm 393 */
									BgL_callzd2defaultzd2walkerz00_3839:
										return
											BGl_walk2z00zzast_walkz00(
											((BgL_nodez00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_3770)),
											BGl_iszd2returnzf3zd2envzf3zzreturn_walkz00,
											BgL_exitvarz00_3771, BgL_abortz00_3772);
									}
							}
						else
							{	/* Return/walk.scm 398 */
								bool_t BgL_test2448z00_5488;

								{	/* Return/walk.scm 398 */
									bool_t BgL_test2449z00_5489;

									{	/* Return/walk.scm 398 */
										BgL_variablez00_bglt BgL_arg2057z00_3862;

										BgL_arg2057z00_3862 =
											(((BgL_varz00_bglt) COBJECT(BgL_i1190z00_3840))->
											BgL_variablez00);
										BgL_test2449z00_5489 =
											(((obj_t) BgL_arg2057z00_3862) ==
											BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00);
									}
									if (BgL_test2449z00_5489)
										{	/* Return/walk.scm 398 */
											BgL_test2448z00_5488 = ((bool_t) 1);
										}
									else
										{	/* Return/walk.scm 399 */
											BgL_variablez00_bglt BgL_arg2056z00_3863;

											BgL_arg2056z00_3863 =
												(((BgL_varz00_bglt) COBJECT(BgL_i1190z00_3840))->
												BgL_variablez00);
											BgL_test2448z00_5488 =
												(((obj_t) BgL_arg2056z00_3863) ==
												BGl_za2envzd2getzd2exitdzd2topza2zd2zzreturn_walkz00);
										}
								}
								if (BgL_test2448z00_5488)
									{	/* Return/walk.scm 400 */
										obj_t BgL__ortest_1194z00_3864;

										BgL__ortest_1194z00_3864 =
											BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00;
										if (CBOOL(BgL__ortest_1194z00_3864))
											{	/* Return/walk.scm 400 */
												return BgL__ortest_1194z00_3864;
											}
										else
											{	/* Return/walk.scm 400 */
												return BGL_PROCEDURE_CALL1(BgL_abortz00_3772, BFALSE);
											}
									}
								else
									{	/* Return/walk.scm 401 */
										bool_t BgL_test2451z00_5502;

										{	/* Return/walk.scm 401 */
											BgL_variablez00_bglt BgL_arg2055z00_3865;

											BgL_arg2055z00_3865 =
												(((BgL_varz00_bglt) COBJECT(BgL_i1190z00_3840))->
												BgL_variablez00);
											BgL_test2451z00_5502 =
												(((obj_t) BgL_arg2055z00_3865) ==
												BGl_za2exitdzd2protectzd2setz12za2z12zzreturn_walkz00);
										}
										if (BgL_test2451z00_5502)
											{	/* Return/walk.scm 401 */
												return BGL_PROCEDURE_CALL1(BgL_abortz00_3772, BFALSE);
											}
										else
											{	/* Return/walk.scm 401 */
												goto BgL_callzd2defaultzd2walkerz00_3839;
											}
									}
							}
					}
				}
			}
		}

	}



/* &is-return?-let-var1332 */
	obj_t BGl_z62iszd2returnzf3zd2letzd2var1332z43zzreturn_walkz00(obj_t
		BgL_envz00_3773, obj_t BgL_nodez00_3774, obj_t BgL_exitvarz00_3775,
		obj_t BgL_abortz00_3776)
	{
		{	/* Return/walk.scm 364 */
			{

				if (CBOOL(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00))
					{	/* Return/walk.scm 369 */
						bool_t BgL_test2453z00_5512;

						{	/* Return/walk.scm 369 */
							bool_t BgL_test2454z00_5513;

							{	/* Return/walk.scm 369 */
								obj_t BgL_tmpz00_5514;

								BgL_tmpz00_5514 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_3774)))->
									BgL_bindingsz00);
								BgL_test2454z00_5513 = PAIRP(BgL_tmpz00_5514);
							}
							if (BgL_test2454z00_5513)
								{	/* Return/walk.scm 369 */
									BgL_test2453z00_5512 =
										NULLP(CDR(
											(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_3774)))->
												BgL_bindingsz00)));
								}
							else
								{	/* Return/walk.scm 369 */
									BgL_test2453z00_5512 = ((bool_t) 0);
								}
						}
						if (BgL_test2453z00_5512)
							{	/* Return/walk.scm 370 */
								obj_t BgL_bnodez00_3868;

								{	/* Return/walk.scm 370 */
									obj_t BgL_pairz00_3869;

									BgL_pairz00_3869 =
										(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_3774)))->
										BgL_bindingsz00);
									BgL_bnodez00_3868 = CDR(CAR(BgL_pairz00_3869));
								}
								{	/* Return/walk.scm 371 */
									bool_t BgL_test2455z00_5526;

									{	/* Return/walk.scm 371 */
										obj_t BgL_classz00_3870;

										BgL_classz00_3870 = BGl_appz00zzast_nodez00;
										if (BGL_OBJECTP(BgL_bnodez00_3868))
											{	/* Return/walk.scm 371 */
												BgL_objectz00_bglt BgL_arg1807z00_3871;

												BgL_arg1807z00_3871 =
													(BgL_objectz00_bglt) (BgL_bnodez00_3868);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Return/walk.scm 371 */
														long BgL_idxz00_3872;

														BgL_idxz00_3872 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3871);
														BgL_test2455z00_5526 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3872 + 3L)) == BgL_classz00_3870);
													}
												else
													{	/* Return/walk.scm 371 */
														bool_t BgL_res2192z00_3875;

														{	/* Return/walk.scm 371 */
															obj_t BgL_oclassz00_3876;

															{	/* Return/walk.scm 371 */
																obj_t BgL_arg1815z00_3877;
																long BgL_arg1816z00_3878;

																BgL_arg1815z00_3877 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Return/walk.scm 371 */
																	long BgL_arg1817z00_3879;

																	BgL_arg1817z00_3879 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3871);
																	BgL_arg1816z00_3878 =
																		(BgL_arg1817z00_3879 - OBJECT_TYPE);
																}
																BgL_oclassz00_3876 =
																	VECTOR_REF(BgL_arg1815z00_3877,
																	BgL_arg1816z00_3878);
															}
															{	/* Return/walk.scm 371 */
																bool_t BgL__ortest_1115z00_3880;

																BgL__ortest_1115z00_3880 =
																	(BgL_classz00_3870 == BgL_oclassz00_3876);
																if (BgL__ortest_1115z00_3880)
																	{	/* Return/walk.scm 371 */
																		BgL_res2192z00_3875 =
																			BgL__ortest_1115z00_3880;
																	}
																else
																	{	/* Return/walk.scm 371 */
																		long BgL_odepthz00_3881;

																		{	/* Return/walk.scm 371 */
																			obj_t BgL_arg1804z00_3882;

																			BgL_arg1804z00_3882 =
																				(BgL_oclassz00_3876);
																			BgL_odepthz00_3881 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3882);
																		}
																		if ((3L < BgL_odepthz00_3881))
																			{	/* Return/walk.scm 371 */
																				obj_t BgL_arg1802z00_3883;

																				{	/* Return/walk.scm 371 */
																					obj_t BgL_arg1803z00_3884;

																					BgL_arg1803z00_3884 =
																						(BgL_oclassz00_3876);
																					BgL_arg1802z00_3883 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3884, 3L);
																				}
																				BgL_res2192z00_3875 =
																					(BgL_arg1802z00_3883 ==
																					BgL_classz00_3870);
																			}
																		else
																			{	/* Return/walk.scm 371 */
																				BgL_res2192z00_3875 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2455z00_5526 = BgL_res2192z00_3875;
													}
											}
										else
											{	/* Return/walk.scm 371 */
												BgL_test2455z00_5526 = ((bool_t) 0);
											}
									}
									if (BgL_test2455z00_5526)
										{	/* Return/walk.scm 373 */
											BgL_varz00_bglt BgL_i1188z00_3885;

											BgL_i1188z00_3885 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_bnodez00_3868)))->
												BgL_funz00);
											{	/* Return/walk.scm 375 */
												bool_t BgL_test2460z00_5551;

												{	/* Return/walk.scm 375 */
													BgL_variablez00_bglt BgL_arg2025z00_3886;

													BgL_arg2025z00_3886 =
														(((BgL_varz00_bglt) COBJECT(BgL_i1188z00_3885))->
														BgL_variablez00);
													BgL_test2460z00_5551 =
														(((obj_t) BgL_arg2025z00_3886) ==
														BGl_za2envzd2getzd2exitdzd2topza2zd2zzreturn_walkz00);
												}
												if (BgL_test2460z00_5551)
													{	/* Return/walk.scm 376 */
														BgL_nodez00_bglt BgL_arg2013z00_3887;
														obj_t BgL_arg2014z00_3888;

														BgL_arg2013z00_3887 =
															(((BgL_letzd2varzd2_bglt) COBJECT(
																	((BgL_letzd2varzd2_bglt) BgL_nodez00_3774)))->
															BgL_bodyz00);
														{	/* Return/walk.scm 376 */
															obj_t BgL_arg2015z00_3889;

															{	/* Return/walk.scm 376 */
																obj_t BgL_pairz00_3890;

																BgL_pairz00_3890 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_nodez00_3774)))->BgL_bindingsz00);
																BgL_arg2015z00_3889 =
																	CAR(CAR(BgL_pairz00_3890));
															}
															BgL_arg2014z00_3888 =
																MAKE_YOUNG_PAIR(BgL_arg2015z00_3889,
																BgL_exitvarz00_3775);
														}
														return
															BGl_iszd2returnzf3z21zzreturn_walkz00
															(BgL_arg2013z00_3887, BgL_arg2014z00_3888,
															BgL_abortz00_3776);
													}
												else
													{	/* Return/walk.scm 377 */
														bool_t BgL_test2461z00_5563;

														{	/* Return/walk.scm 377 */
															BgL_variablez00_bglt BgL_arg2024z00_3891;

															BgL_arg2024z00_3891 =
																(((BgL_varz00_bglt)
																	COBJECT(BgL_i1188z00_3885))->BgL_variablez00);
															BgL_test2461z00_5563 =
																(((obj_t) BgL_arg2024z00_3891) ==
																BGl_za2getzd2exitdzd2topza2z00zzreturn_walkz00);
														}
														if (BgL_test2461z00_5563)
															{	/* Return/walk.scm 378 */
																BgL_nodez00_bglt BgL_arg2019z00_3892;
																obj_t BgL_arg2020z00_3893;

																BgL_arg2019z00_3892 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_nodez00_3774)))->BgL_bodyz00);
																BgL_arg2020z00_3893 =
																	MAKE_YOUNG_PAIR(CAR((((BgL_letzd2varzd2_bglt)
																				COBJECT(((BgL_letzd2varzd2_bglt)
																						BgL_nodez00_3774)))->
																			BgL_bindingsz00)), BgL_exitvarz00_3775);
																return
																	BGl_iszd2returnzf3z21zzreturn_walkz00
																	(BgL_arg2019z00_3892, BgL_arg2020z00_3893,
																	BgL_abortz00_3776);
															}
														else
															{	/* Return/walk.scm 377 */
															BgL_callzd2defaultzd2walkerz00_3867:
																return
																	BGl_walk2z00zzast_walkz00(
																	((BgL_nodez00_bglt)
																		((BgL_letzd2varzd2_bglt) BgL_nodez00_3774)),
																	BGl_iszd2returnzf3zd2envzf3zzreturn_walkz00,
																	BgL_exitvarz00_3775, BgL_abortz00_3776);
															}
													}
											}
										}
									else
										{	/* Return/walk.scm 371 */
											goto BgL_callzd2defaultzd2walkerz00_3867;
										}
								}
							}
						else
							{	/* Return/walk.scm 369 */
								goto BgL_callzd2defaultzd2walkerz00_3867;
							}
					}
				else
					{	/* Return/walk.scm 367 */
						goto BgL_callzd2defaultzd2walkerz00_3867;
					}
			}
		}

	}



/* &is-return?-var1330 */
	obj_t BGl_z62iszd2returnzf3zd2var1330z91zzreturn_walkz00(obj_t
		BgL_envz00_3777, obj_t BgL_nodez00_3778, obj_t BgL_exitvarz00_3779,
		obj_t BgL_abortz00_3780)
	{
		{	/* Return/walk.scm 356 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
						((obj_t)
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_3778)))->BgL_variablez00)),
						BgL_exitvarz00_3779)))
				{	/* Return/walk.scm 358 */
					return BGL_PROCEDURE_CALL1(BgL_abortz00_3780, BFALSE);
				}
			else
				{	/* Return/walk.scm 358 */
					return BFALSE;
				}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzreturn_walkz00(void)
	{
		{	/* Return/walk.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzast_walkz00(343174225L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2218z00zzreturn_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
