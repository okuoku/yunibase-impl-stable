/*===========================================================================*/
/*   (BackEnd/jvm.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/jvm.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_JVM_TYPE_DEFINITIONS
#define BGL_BACKEND_JVM_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_jvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
		obj_t BgL_qnamez00;
		obj_t BgL_classesz00;
		obj_t BgL_currentzd2classzd2;
		obj_t BgL_declarationsz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_codez00;
		obj_t BgL_lightzd2funcallszd2;
		obj_t BgL_inlinez00;
	}             *BgL_jvmz00_bglt;


#endif													// BGL_BACKEND_JVM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_zc3z04exitza31750ze3ze70z60zzbackend_jvmz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31692ze3ze5zzbackend_jvmz00(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2replacezd2zz__r4_strings_6_7z00(obj_t,
		unsigned char, unsigned char);
	BGL_IMPORT obj_t BGl_za2ppzd2widthza2zd2zz__ppz00;
	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	BGL_IMPORT bool_t BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_findzd2librarieszd2zzengine_linkz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbackend_jvmz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_raisez00zz__errorz00(obj_t);
	extern obj_t BGl_za2autozd2linkzd2mainza2z00zzengine_paramz00;
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31865ze3ze5zzbackend_jvmz00(obj_t);
	extern obj_t BGl_addzd2evalzd2libraryz12z12zzmodule_evalz00(obj_t);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzbackend_jvmz00(void);
	extern obj_t BGl_jvmzd2aszd2zzjas_asz00(obj_t, obj_t);
	extern obj_t BGl_za2restzd2argsza2zd2zzengine_paramz00;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31689ze3ze5zzbackend_jvmz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzbackend_jvmz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzbackend_jvmz00(void);
	static obj_t BGl_emitze70ze7zzbackend_jvmz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62backendzd2cnstzd2tablezd2n1438zb0zzbackend_jvmz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2ppzd2caseza2zd2zz__ppz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_jasnamez00zzbackend_jvmz00(obj_t);
	BGL_IMPORT obj_t BGl_dirnamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern bool_t BGl_subzd2typezf3z21zztype_envz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00;
	static obj_t BGl_addsuffixz00zzbackend_jvmz00(obj_t);
	BGL_IMPORT obj_t BGl_ppz00zz__ppz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	static obj_t BGl_makezd2tmpzd2filezd2namezd2zzbackend_jvmz00(void);
	BGL_IMPORT obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_basenamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_makezd2tmpzd2mainz00zzengine_linkz00(obj_t, bool_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzbackend_jvmz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_jvmz00(void);
	extern obj_t BGl_jvmz00zzbackend_jvm_classz00;
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_registerzd2backendz12zc0zzbackend_backendz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	extern obj_t BGl_findzd2mainzd2zzengine_linkz00(obj_t);
	BGL_IMPORT obj_t close_binary_port(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_checkzd2jvmzd2inlinesz00zzsaw_jvm_inlinez00(void);
	static obj_t BGl_z62startzd2jvmzd2emissionz12z70zzbackend_jvmz00(obj_t);
	static obj_t BGl_z62backendzd2linkzd2objects1440z62zzbackend_jvmz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_jvmzd2ldzd2zzsaw_jvm_ldz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31700ze3ze5zzbackend_jvmz00(obj_t);
	extern obj_t BGl_saw_jvmzd2compilezd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt);
	extern obj_t BGl_makezd2bigloozd2mainz00zzbackend_c_mainz00(void);
	static obj_t BGl_jvmzd2checkzd2packagez00zzbackend_jvmz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_compilerz00zzengine_compilerz00(void);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzbackend_jvmz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_setrcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_inlinez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_ldz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_compilez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_peepz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_asz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_mainz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_bvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_evalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_compilerz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_linkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62backendzd2compilezd2jvm1434z62zzbackend_jvmz00(obj_t,
		obj_t);
	extern obj_t BGl_za2mcozd2suffixza2zd2zzengine_paramz00;
	BGL_IMPORT bool_t fexists(char *);
	static obj_t BGl_z62buildzd2jvmzd2backendz62zzbackend_jvmz00(obj_t);
	BGL_IMPORT obj_t BGl_suffixz00zz__osz00(obj_t);
	static obj_t BGl_jvmzd2dirnamezd2zzbackend_jvmz00(obj_t);
	extern obj_t BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00(void);
	static obj_t BGl_z62backendzd2subtypezf3zd2jvm1444z91zzbackend_jvmz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzbackend_jvmz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_jvmz00(void);
	extern obj_t BGl_compilerzd2readzd2zzread_readerz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_jvmz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzbackend_jvmz00(void);
	static obj_t BGl_z62backendzd2linkzd2jvm1436z62zzbackend_jvmz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2ozd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_za2jvmzd2directoryza2zd2zzengine_paramz00;
	static obj_t BGl_z62backendzd2checkzd2inline1442z62zzbackend_jvmz00(obj_t,
		obj_t);
	BGL_IMPORT bool_t bgl_directoryp(char *);
	static obj_t BGl_jvmzd2filenamezd2zzbackend_jvmz00(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_z62zc3z04anonymousza31751ze3ze5zzbackend_jvmz00(obj_t);
	static bool_t BGl_z62zc3z04anonymousza31832ze3ze5zzbackend_jvmz00(obj_t,
		obj_t);
	extern obj_t BGl_readzd2jfilezd2zzread_jvmz00(void);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_jvmzd2classzd2sanszd2directoryzd2zzread_jvmz00(obj_t);
	extern obj_t BGl_iszd2subtypezf3z21zztype_typeofz00(obj_t, obj_t);
	extern obj_t BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_stopzd2onzd2passz00zzengine_passz00(obj_t, obj_t);
	static obj_t BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00 = BUNSPEC;
	extern obj_t BGl_usezd2libraryz12zc0zzmodule_alibraryz00(obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2mainza2z00zzmodule_modulez00;
	static obj_t BGl_jvmasdumpz00zzbackend_jvmz00(obj_t, obj_t);
	static obj_t __cnst[21];


	   
		 
		DEFINE_STRING(BGl_string2040z00zzbackend_jvmz00,
		BgL_bgl_string2040za700za7za7b2046za7, "        class: ", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2034z00zzbackend_jvmz00,
		BgL_bgl_za762za7c3za704anonymo2047za7,
		BGl_z62zc3z04anonymousza31689ze3ze5zzbackend_jvmz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2041z00zzbackend_jvmz00,
		BgL_bgl_string2041za700za7za7b2048za7, "jvm-dump", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2035z00zzbackend_jvmz00,
		BgL_bgl_za762za7c3za704anonymo2049za7,
		BGl_z62zc3z04anonymousza31692ze3ze5zzbackend_jvmz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2042z00zzbackend_jvmz00,
		BgL_bgl_string2042za700za7za7b2050za7, "Can't open file for output", 26);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2036z00zzbackend_jvmz00,
		BgL_bgl_za762za7c3za704anonymo2051za7,
		BGl_z62zc3z04anonymousza31700ze3ze5zzbackend_jvmz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2043z00zzbackend_jvmz00,
		BgL_bgl_string2043za700za7za7b2052za7, "backend_jvm", 11);
	      DEFINE_STRING(BGl_string2044z00zzbackend_jvmz00,
		BgL_bgl_string2044za700za7za7b2053za7,
		"done cc (ld distrib) pass-started (start-jvm-emission!) module eval library java.lang.Object object ld class jvmas jast lower unamed (jvm) (java) native bigloo-jvm jvm ",
		168);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_buildzd2jvmzd2backendzd2envzd2zzbackend_jvmz00,
		BgL_bgl_za762buildza7d2jvmza7d2054za7,
		BGl_z62buildzd2jvmzd2backendz62zzbackend_jvmz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_backendzd2compilezd2envz00zzbackend_backendz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_startzd2jvmzd2emissionz12zd2envzc0zzbackend_jvmz00,
		BgL_bgl_za762startza7d2jvmza7d2055za7,
		BGl_z62startzd2jvmzd2emissionz12z70zzbackend_jvmz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_backendzd2linkzd2envz00zzbackend_backendz00;
	extern obj_t BGl_backendzd2checkzd2inlineszd2envzd2zzbackend_backendz00;
	extern obj_t BGl_backendzd2linkzd2objectszd2envzd2zzbackend_backendz00;
	extern obj_t BGl_backendzd2cnstzd2tablezd2namezd2envz00zzbackend_backendz00;
	   
		 
		DEFINE_STRING(BGl_string1993z00zzbackend_jvmz00,
		BgL_bgl_string1993za700za7za7b2056za7, ".", 1);
	      DEFINE_STRING(BGl_string1994z00zzbackend_jvmz00,
		BgL_bgl_string1994za700za7za7b2057za7, "dummy", 5);
	      DEFINE_STRING(BGl_string1995z00zzbackend_jvmz00,
		BgL_bgl_string1995za700za7za7b2058za7, "jheap", 5);
	      DEFINE_STRING(BGl_string1996z00zzbackend_jvmz00,
		BgL_bgl_string1996za700za7za7b2059za7, "", 0);
	      DEFINE_STRING(BGl_string1997z00zzbackend_jvmz00,
		BgL_bgl_string1997za700za7za7b2060za7, "'.", 2);
	      DEFINE_STRING(BGl_string1998z00zzbackend_jvmz00,
		BgL_bgl_string1998za700za7za7b2061za7, "', class path is `", 18);
	      DEFINE_STRING(BGl_string1999z00zzbackend_jvmz00,
		BgL_bgl_string1999za700za7za7b2062za7, " is `", 5);
	extern obj_t BGl_backendzd2subtypezf3zd2envzf3zzbackend_backendz00;
	   
		 
		DEFINE_STRING(BGl_string2000z00zzbackend_jvmz00,
		BgL_bgl_string2000za700za7za7b2063za7, "Package name for module ", 24);
	      DEFINE_STRING(BGl_string2001z00zzbackend_jvmz00,
		BgL_bgl_string2001za700za7za7b2064za7,
		"Incompatible package name and class path.", 41);
	      DEFINE_STRING(BGl_string2002z00zzbackend_jvmz00,
		BgL_bgl_string2002za700za7za7b2065za7, ".jast", 5);
	      DEFINE_STRING(BGl_string2003z00zzbackend_jvmz00,
		BgL_bgl_string2003za700za7za7b2066za7, ".jas", 4);
	      DEFINE_STRING(BGl_string2004z00zzbackend_jvmz00,
		BgL_bgl_string2004za700za7za7b2067za7, ".class", 6);
	      DEFINE_STRING(BGl_string2005z00zzbackend_jvmz00,
		BgL_bgl_string2005za700za7za7b2068za7, "start-jvm-emission!", 19);
	      DEFINE_STRING(BGl_string2006z00zzbackend_jvmz00,
		BgL_bgl_string2006za700za7za7b2069za7,
		"Can't write dest file because directory doesn't exist", 53);
	      DEFINE_STRING(BGl_string2007z00zzbackend_jvmz00,
		BgL_bgl_string2007za700za7za7b2070za7, "JVMMAIN", 7);
	      DEFINE_STRING(BGl_string2008z00zzbackend_jvmz00,
		BgL_bgl_string2008za700za7za7b2071za7, ".bgl", 4);
	      DEFINE_STRING(BGl_string2010z00zzbackend_jvmz00,
		BgL_bgl_string2010za700za7za7b2072za7, "backend-compile", 15);
	      DEFINE_STRING(BGl_string2012z00zzbackend_jvmz00,
		BgL_bgl_string2012za700za7za7b2073za7, "backend-link", 12);
	      DEFINE_STRING(BGl_string2014z00zzbackend_jvmz00,
		BgL_bgl_string2014za700za7za7b2074za7, "backend-cnst-table-name", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2009z00zzbackend_jvmz00,
		BgL_bgl_za762backendza7d2com2075z00,
		BGl_z62backendzd2compilezd2jvm1434z62zzbackend_jvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2016z00zzbackend_jvmz00,
		BgL_bgl_string2016za700za7za7b2076za7, "backend-link-objects", 20);
	      DEFINE_STRING(BGl_string2018z00zzbackend_jvmz00,
		BgL_bgl_string2018za700za7za7b2077za7, "backend-check-inlines", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2011z00zzbackend_jvmz00,
		BgL_bgl_za762backendza7d2lin2078z00,
		BGl_z62backendzd2linkzd2jvm1436z62zzbackend_jvmz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2013z00zzbackend_jvmz00,
		BgL_bgl_za762backendza7d2cns2079z00,
		BGl_z62backendzd2cnstzd2tablezd2n1438zb0zzbackend_jvmz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2020z00zzbackend_jvmz00,
		BgL_bgl_string2020za700za7za7b2080za7, "backend-subtype?", 16);
	      DEFINE_STRING(BGl_string2021z00zzbackend_jvmz00,
		BgL_bgl_string2021za700za7za7b2081za7, " -- ", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2015z00zzbackend_jvmz00,
		BgL_bgl_za762backendza7d2lin2082z00,
		BGl_z62backendzd2linkzd2objects1440z62zzbackend_jvmz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2022z00zzbackend_jvmz00,
		BgL_bgl_string2022za700za7za7b2083za7, "No source file found", 20);
	      DEFINE_STRING(BGl_string2023z00zzbackend_jvmz00,
		BgL_bgl_string2023za700za7za7b2084za7, "link", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2017z00zzbackend_jvmz00,
		BgL_bgl_za762backendza7d2che2085z00,
		BGl_z62backendzd2checkzd2inline1442z62zzbackend_jvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2024z00zzbackend_jvmz00,
		BgL_bgl_string2024za700za7za7b2086za7, "\"", 1);
	      DEFINE_STRING(BGl_string2025z00zzbackend_jvmz00,
		BgL_bgl_string2025za700za7za7b2087za7, "Illegal file", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2019z00zzbackend_jvmz00,
		BgL_bgl_za762backendza7d2sub2088z00,
		BGl_z62backendzd2subtypezf3zd2jvm1444z91zzbackend_jvmz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2026z00zzbackend_jvmz00,
		BgL_bgl_string2026za700za7za7b2089za7, "__cnst", 6);
	      DEFINE_STRING(BGl_string2027z00zzbackend_jvmz00,
		BgL_bgl_string2027za700za7za7b2090za7, "Jvm", 3);
	      DEFINE_STRING(BGl_string2028z00zzbackend_jvmz00,
		BgL_bgl_string2028za700za7za7b2091za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2029z00zzbackend_jvmz00,
		BgL_bgl_string2029za700za7za7b2092za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2030z00zzbackend_jvmz00,
		BgL_bgl_string2030za700za7za7b2093za7, "]", 1);
	      DEFINE_STRING(BGl_string2031z00zzbackend_jvmz00,
		BgL_bgl_string2031za700za7za7b2094za7, " qualified type name: ", 22);
	      DEFINE_STRING(BGl_string2032z00zzbackend_jvmz00,
		BgL_bgl_string2032za700za7za7b2095za7, "      [module: ", 15);
	      DEFINE_STRING(BGl_string2033z00zzbackend_jvmz00,
		BgL_bgl_string2033za700za7za7b2096za7, "a.class", 7);
	      DEFINE_STRING(BGl_string2037z00zzbackend_jvmz00,
		BgL_bgl_string2037za700za7za7b2097za7, "/", 1);
	      DEFINE_STRING(BGl_string2038z00zzbackend_jvmz00,
		BgL_bgl_string2038za700za7za7b2098za7, "/a.class", 8);
	      DEFINE_STRING(BGl_string2039z00zzbackend_jvmz00,
		BgL_bgl_string2039za700za7za7b2099za7, "\n", 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzbackend_jvmz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzbackend_jvmz00(long
		BgL_checksumz00_3206, char *BgL_fromz00_3207)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_jvmz00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_jvmz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_jvmz00();
					BGl_libraryzd2moduleszd2initz00zzbackend_jvmz00();
					BGl_cnstzd2initzd2zzbackend_jvmz00();
					BGl_importedzd2moduleszd2initz00zzbackend_jvmz00();
					BGl_methodzd2initzd2zzbackend_jvmz00();
					return BGl_toplevelzd2initzd2zzbackend_jvmz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_jvmz00(void)
	{
		{	/* BackEnd/jvm.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__ppz00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"backend_jvm");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__binaryz00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"backend_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "backend_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"backend_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_jvm");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_jvmz00(void)
	{
		{	/* BackEnd/jvm.scm 15 */
			{	/* BackEnd/jvm.scm 15 */
				obj_t BgL_cportz00_3027;

				{	/* BackEnd/jvm.scm 15 */
					obj_t BgL_stringz00_3034;

					BgL_stringz00_3034 = BGl_string2044z00zzbackend_jvmz00;
					{	/* BackEnd/jvm.scm 15 */
						obj_t BgL_startz00_3035;

						BgL_startz00_3035 = BINT(0L);
						{	/* BackEnd/jvm.scm 15 */
							obj_t BgL_endz00_3036;

							BgL_endz00_3036 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3034)));
							{	/* BackEnd/jvm.scm 15 */

								BgL_cportz00_3027 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3034, BgL_startz00_3035, BgL_endz00_3036);
				}}}}
				{
					long BgL_iz00_3028;

					BgL_iz00_3028 = 20L;
				BgL_loopz00_3029:
					if ((BgL_iz00_3028 == -1L))
						{	/* BackEnd/jvm.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/jvm.scm 15 */
							{	/* BackEnd/jvm.scm 15 */
								obj_t BgL_arg2045z00_3030;

								{	/* BackEnd/jvm.scm 15 */

									{	/* BackEnd/jvm.scm 15 */
										obj_t BgL_locationz00_3032;

										BgL_locationz00_3032 = BBOOL(((bool_t) 0));
										{	/* BackEnd/jvm.scm 15 */

											BgL_arg2045z00_3030 =
												BGl_readz00zz__readerz00(BgL_cportz00_3027,
												BgL_locationz00_3032);
										}
									}
								}
								{	/* BackEnd/jvm.scm 15 */
									int BgL_tmpz00_3242;

									BgL_tmpz00_3242 = (int) (BgL_iz00_3028);
									CNST_TABLE_SET(BgL_tmpz00_3242, BgL_arg2045z00_3030);
							}}
							{	/* BackEnd/jvm.scm 15 */
								int BgL_auxz00_3033;

								BgL_auxz00_3033 = (int) ((BgL_iz00_3028 - 1L));
								{
									long BgL_iz00_3247;

									BgL_iz00_3247 = (long) (BgL_auxz00_3033);
									BgL_iz00_3028 = BgL_iz00_3247;
									goto BgL_loopz00_3029;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_jvmz00(void)
	{
		{	/* BackEnd/jvm.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbackend_jvmz00(void)
	{
		{	/* BackEnd/jvm.scm 15 */
			BGl_registerzd2backendz12zc0zzbackend_backendz00(CNST_TABLE_REF(0),
				BGl_buildzd2jvmzd2backendzd2envzd2zzbackend_jvmz00);
			BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00 =
				BGl_string1993z00zzbackend_jvmz00;
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzbackend_jvmz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1939;

				BgL_headz00_1939 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1940;
					obj_t BgL_tailz00_1941;

					BgL_prevz00_1940 = BgL_headz00_1939;
					BgL_tailz00_1941 = BgL_l1z00_1;
				BgL_loopz00_1942:
					if (PAIRP(BgL_tailz00_1941))
						{
							obj_t BgL_newzd2prevzd2_1944;

							BgL_newzd2prevzd2_1944 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1941), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1940, BgL_newzd2prevzd2_1944);
							{
								obj_t BgL_tailz00_3259;
								obj_t BgL_prevz00_3258;

								BgL_prevz00_3258 = BgL_newzd2prevzd2_1944;
								BgL_tailz00_3259 = CDR(BgL_tailz00_1941);
								BgL_tailz00_1941 = BgL_tailz00_3259;
								BgL_prevz00_1940 = BgL_prevz00_3258;
								goto BgL_loopz00_1942;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1939);
				}
			}
		}

	}



/* &build-jvm-backend */
	obj_t BGl_z62buildzd2jvmzd2backendz62zzbackend_jvmz00(obj_t BgL_envz00_2966)
	{
		{	/* BackEnd/jvm.scm 52 */
			{
				BgL_jvmz00_bglt BgL_auxz00_3262;

				{	/* BackEnd/jvm.scm 53 */
					BgL_jvmz00_bglt BgL_new1114z00_3038;

					{	/* BackEnd/jvm.scm 54 */
						BgL_jvmz00_bglt BgL_new1113z00_3039;

						BgL_new1113z00_3039 =
							((BgL_jvmz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_jvmz00_bgl))));
						{	/* BackEnd/jvm.scm 54 */
							long BgL_arg1472z00_3040;

							BgL_arg1472z00_3040 =
								BGL_CLASS_NUM(BGl_jvmz00zzbackend_jvm_classz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1113z00_3039),
								BgL_arg1472z00_3040);
						}
						BgL_new1114z00_3038 = BgL_new1113z00_3039;
					}
					((((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_new1114z00_3038)))->
							BgL_languagez00) = ((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_srfi0z00) =
						((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_namez00) =
						((obj_t) BGl_string1994z00zzbackend_jvmz00), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_externzd2variableszd2) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_externzd2functionszd2) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_externzd2typeszd2) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_variablesz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_functionsz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_typesz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_typedz00) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_heapzd2suffixzd2) =
						((obj_t) BGl_string1995z00zzbackend_jvmz00), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_heapzd2compatiblezd2) =
						((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_callccz00) =
						((bool_t) ((bool_t) 1)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_qualifiedzd2typeszd2) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_effectzb2zb2) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_removezd2emptyzd2letz00) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_foreignzd2closurezd2) =
						((bool_t) ((bool_t) 1)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_typedzd2eqzd2) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_tracezd2supportzd2) =
						((bool_t) ((bool_t) 1)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_foreignzd2clausezd2supportz00) =
						((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_debugzd2supportzd2) =
						((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_pragmazd2supportzd2) =
						((bool_t) ((bool_t) 1)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_tvectorzd2descrzd2supportz00) =
						((bool_t) ((bool_t) 1)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_requirezd2tailczd2) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_registersz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_pregistersz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_boundzd2checkzd2) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_typezd2checkzd2) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_typedzd2funcallzd2) =
						((bool_t) ((bool_t) 1)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_strictzd2typezd2castz00) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->
							BgL_forcezd2registerzd2gczd2rootszd2) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1114z00_3038)))->BgL_stringzd2literalzd2supportz00) =
						((bool_t) ((bool_t) 1)), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->BgL_qnamez00) =
						((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->BgL_classesz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->
							BgL_currentzd2classzd2) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->
							BgL_declarationsz00) = ((obj_t) BNIL), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->BgL_fieldsz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->BgL_methodsz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->
							BgL_currentzd2methodzd2) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->BgL_codez00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->
							BgL_lightzd2funcallszd2) = ((obj_t) BNIL), BUNSPEC);
					((((BgL_jvmz00_bglt) COBJECT(BgL_new1114z00_3038))->BgL_inlinez00) =
						((obj_t) BTRUE), BUNSPEC);
					{	/* BackEnd/jvm.scm 53 */
						obj_t BgL_fun1454z00_3041;

						BgL_fun1454z00_3041 =
							BGl_classzd2constructorzd2zz__objectz00
							(BGl_jvmz00zzbackend_jvm_classz00);
						BGL_PROCEDURE_CALL1(BgL_fun1454z00_3041,
							((obj_t) BgL_new1114z00_3038));
					}
					BgL_auxz00_3262 = BgL_new1114z00_3038;
				}
				return ((obj_t) BgL_auxz00_3262);
			}
		}

	}



/* jvm-check-package */
	obj_t BGl_jvmzd2checkzd2packagez00zzbackend_jvmz00(obj_t BgL_modulez00_4,
		obj_t BgL_pathz00_5)
	{
		{	/* BackEnd/jvm.scm 128 */
			{
				obj_t BgL_basez00_1968;
				obj_t BgL_pathz00_1969;

				{	/* BackEnd/jvm.scm 145 */
					obj_t BgL_qtypez00_1952;

					BgL_qtypez00_1952 =
						BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(BgL_modulez00_4);
					{	/* BackEnd/jvm.scm 145 */
						obj_t BgL_basez00_1953;

						{	/* BackEnd/jvm.scm 146 */
							obj_t BgL_prez00_1965;

							BgL_prez00_1965 = BGl_prefixz00zz__osz00(BgL_qtypez00_1952);
							{	/* BackEnd/jvm.scm 148 */
								bool_t BgL_test2103z00_3356;

								{	/* BackEnd/jvm.scm 148 */
									long BgL_l1z00_2696;

									BgL_l1z00_2696 = STRING_LENGTH(BgL_prez00_1965);
									if ((BgL_l1z00_2696 == 0L))
										{	/* BackEnd/jvm.scm 148 */
											int BgL_arg1282z00_2699;

											{	/* BackEnd/jvm.scm 148 */
												char *BgL_auxz00_3362;
												char *BgL_tmpz00_3360;

												BgL_auxz00_3362 =
													BSTRING_TO_STRING(BGl_string1996z00zzbackend_jvmz00);
												BgL_tmpz00_3360 = BSTRING_TO_STRING(BgL_prez00_1965);
												BgL_arg1282z00_2699 =
													memcmp(BgL_tmpz00_3360, BgL_auxz00_3362,
													BgL_l1z00_2696);
											}
											BgL_test2103z00_3356 =
												((long) (BgL_arg1282z00_2699) == 0L);
										}
									else
										{	/* BackEnd/jvm.scm 148 */
											BgL_test2103z00_3356 = ((bool_t) 0);
										}
								}
								if (BgL_test2103z00_3356)
									{	/* BackEnd/jvm.scm 148 */
										BgL_basez00_1953 = BGl_string1993z00zzbackend_jvmz00;
									}
								else
									{	/* BackEnd/jvm.scm 150 */
										bool_t BgL_test2105z00_3367;

										{	/* BackEnd/jvm.scm 150 */
											long BgL_l1z00_2707;

											BgL_l1z00_2707 = STRING_LENGTH(BgL_prez00_1965);
											if ((BgL_l1z00_2707 == STRING_LENGTH(BgL_qtypez00_1952)))
												{	/* BackEnd/jvm.scm 150 */
													int BgL_arg1282z00_2710;

													{	/* BackEnd/jvm.scm 150 */
														char *BgL_auxz00_3374;
														char *BgL_tmpz00_3372;

														BgL_auxz00_3374 =
															BSTRING_TO_STRING(BgL_qtypez00_1952);
														BgL_tmpz00_3372 =
															BSTRING_TO_STRING(BgL_prez00_1965);
														BgL_arg1282z00_2710 =
															memcmp(BgL_tmpz00_3372, BgL_auxz00_3374,
															BgL_l1z00_2707);
													}
													BgL_test2105z00_3367 =
														((long) (BgL_arg1282z00_2710) == 0L);
												}
											else
												{	/* BackEnd/jvm.scm 150 */
													BgL_test2105z00_3367 = ((bool_t) 0);
												}
										}
										if (BgL_test2105z00_3367)
											{	/* BackEnd/jvm.scm 150 */
												BgL_basez00_1953 = BGl_string1993z00zzbackend_jvmz00;
											}
										else
											{	/* BackEnd/jvm.scm 150 */
												BgL_basez00_1953 = BgL_prez00_1965;
											}
									}
							}
						}
						{	/* BackEnd/jvm.scm 146 */

							{	/* BackEnd/jvm.scm 154 */
								bool_t BgL_test2107z00_3379;

								BgL_basez00_1968 =
									BGl_jvmzd2filenamezd2zzbackend_jvmz00(BgL_basez00_1953);
								BgL_pathz00_1969 = BgL_pathz00_5;
								{	/* BackEnd/jvm.scm 130 */
									long BgL_lbasez00_1971;
									long BgL_lpathz00_1972;

									BgL_lbasez00_1971 = STRING_LENGTH(BgL_basez00_1968);
									BgL_lpathz00_1972 = STRING_LENGTH(BgL_pathz00_1969);
									if ((BgL_lpathz00_1972 < BgL_lbasez00_1971))
										{	/* BackEnd/jvm.scm 132 */
											BgL_test2107z00_3379 = ((bool_t) 0);
										}
									else
										{
											long BgL_rpathz00_1978;
											long BgL_rbasez00_1979;

											BgL_rpathz00_1978 = (BgL_lpathz00_1972 - 1L);
											BgL_rbasez00_1979 = (BgL_lbasez00_1971 - 1L);
										BgL_zc3z04anonymousza31542ze3z87_1980:
											if ((BgL_rbasez00_1979 == -1L))
												{	/* BackEnd/jvm.scm 136 */
													BgL_test2107z00_3379 = ((bool_t) 1);
												}
											else
												{	/* BackEnd/jvm.scm 138 */
													unsigned char BgL_cbasez00_1982;
													unsigned char BgL_cpathz00_1983;

													BgL_cbasez00_1982 =
														STRING_REF(BgL_basez00_1968, BgL_rbasez00_1979);
													BgL_cpathz00_1983 =
														STRING_REF(BgL_pathz00_1969, BgL_rpathz00_1978);
													{	/* BackEnd/jvm.scm 140 */
														bool_t BgL_test2110z00_3388;

														if ((BgL_cbasez00_1982 == BgL_cpathz00_1983))
															{	/* BackEnd/jvm.scm 140 */
																BgL_test2110z00_3388 = ((bool_t) 1);
															}
														else
															{	/* BackEnd/jvm.scm 140 */
																if (
																	(BgL_cpathz00_1983 == ((unsigned char) '/')))
																	{	/* BackEnd/jvm.scm 141 */
																		BgL_test2110z00_3388 =
																			(BgL_cbasez00_1982 ==
																			((unsigned char) '.'));
																	}
																else
																	{	/* BackEnd/jvm.scm 141 */
																		BgL_test2110z00_3388 = ((bool_t) 0);
																	}
															}
														if (BgL_test2110z00_3388)
															{
																long BgL_rbasez00_3396;
																long BgL_rpathz00_3394;

																BgL_rpathz00_3394 = (BgL_rpathz00_1978 - 1L);
																BgL_rbasez00_3396 = (BgL_rbasez00_1979 - 1L);
																BgL_rbasez00_1979 = BgL_rbasez00_3396;
																BgL_rpathz00_1978 = BgL_rpathz00_3394;
																goto BgL_zc3z04anonymousza31542ze3z87_1980;
															}
														else
															{	/* BackEnd/jvm.scm 140 */
																BgL_test2107z00_3379 = ((bool_t) 0);
															}
													}
												}
										}
								}
								if (BgL_test2107z00_3379)
									{	/* BackEnd/jvm.scm 154 */
										return BFALSE;
									}
								else
									{	/* BackEnd/jvm.scm 155 */
										obj_t BgL_list1475z00_1956;

										{	/* BackEnd/jvm.scm 155 */
											obj_t BgL_arg1485z00_1957;

											{	/* BackEnd/jvm.scm 155 */
												obj_t BgL_arg1489z00_1958;

												{	/* BackEnd/jvm.scm 155 */
													obj_t BgL_arg1502z00_1959;

													{	/* BackEnd/jvm.scm 155 */
														obj_t BgL_arg1509z00_1960;

														{	/* BackEnd/jvm.scm 155 */
															obj_t BgL_arg1513z00_1961;

															{	/* BackEnd/jvm.scm 155 */
																obj_t BgL_arg1514z00_1962;

																{	/* BackEnd/jvm.scm 155 */
																	obj_t BgL_arg1516z00_1963;

																	BgL_arg1516z00_1963 =
																		MAKE_YOUNG_PAIR
																		(BGl_string1997z00zzbackend_jvmz00, BNIL);
																	BgL_arg1514z00_1962 =
																		MAKE_YOUNG_PAIR(BgL_pathz00_5,
																		BgL_arg1516z00_1963);
																}
																BgL_arg1513z00_1961 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1998z00zzbackend_jvmz00,
																	BgL_arg1514z00_1962);
															}
															BgL_arg1509z00_1960 =
																MAKE_YOUNG_PAIR(BgL_basez00_1953,
																BgL_arg1513z00_1961);
														}
														BgL_arg1502z00_1959 =
															MAKE_YOUNG_PAIR(BGl_string1999z00zzbackend_jvmz00,
															BgL_arg1509z00_1960);
													}
													BgL_arg1489z00_1958 =
														MAKE_YOUNG_PAIR
														(BGl_za2moduleza2z00zzmodule_modulez00,
														BgL_arg1502z00_1959);
												}
												BgL_arg1485z00_1957 =
													MAKE_YOUNG_PAIR(BGl_string2000z00zzbackend_jvmz00,
													BgL_arg1489z00_1958);
											}
											BgL_list1475z00_1956 =
												MAKE_YOUNG_PAIR(BGl_string2001z00zzbackend_jvmz00,
												BgL_arg1485z00_1957);
										}
										return BGl_warningz00zz__errorz00(BgL_list1475z00_1956);
									}
							}
						}
					}
				}
			}
		}

	}



/* jvmasdump */
	obj_t BGl_jvmasdumpz00zzbackend_jvmz00(obj_t BgL_classfilez00_6,
		obj_t BgL_portz00_7)
	{
		{	/* BackEnd/jvm.scm 161 */
			{	/* BackEnd/jvm.scm 162 */
				obj_t BgL_owz00_1994;
				obj_t BgL_ocz00_1995;

				BgL_owz00_1994 = BGl_za2ppzd2widthza2zd2zz__ppz00;
				BgL_ocz00_1995 = BGl_za2ppzd2caseza2zd2zz__ppz00;
				BGl_za2ppzd2widthza2zd2zz__ppz00 = BINT(10240L);
				BGl_za2ppzd2caseza2zd2zz__ppz00 = CNST_TABLE_REF(6);
				{	/* BackEnd/jvm.scm 165 */
					obj_t BgL_list1554z00_1996;

					BgL_list1554z00_1996 = MAKE_YOUNG_PAIR(BgL_portz00_7, BNIL);
					BGl_ppz00zz__ppz00(BgL_classfilez00_6, BgL_list1554z00_1996);
				}
				BGl_za2ppzd2caseza2zd2zz__ppz00 = BgL_ocz00_1995;
				return (BGl_za2ppzd2widthza2zd2zz__ppz00 = BgL_owz00_1994, BUNSPEC);
			}
		}

	}



/* addsuffix */
	obj_t BGl_addsuffixz00zzbackend_jvmz00(obj_t BgL_namez00_8)
	{
		{	/* BackEnd/jvm.scm 169 */
			{	/* BackEnd/jvm.scm 171 */
				obj_t BgL_arg1559z00_1997;

				{	/* BackEnd/jvm.scm 171 */
					obj_t BgL_casezd2valuezd2_1998;

					BgL_casezd2valuezd2_1998 = BGl_za2passza2z00zzengine_paramz00;
					if ((BgL_casezd2valuezd2_1998 == CNST_TABLE_REF(7)))
						{	/* BackEnd/jvm.scm 171 */
							BgL_arg1559z00_1997 = BGl_string2002z00zzbackend_jvmz00;
						}
					else
						{	/* BackEnd/jvm.scm 171 */
							if ((BgL_casezd2valuezd2_1998 == CNST_TABLE_REF(8)))
								{	/* BackEnd/jvm.scm 171 */
									BgL_arg1559z00_1997 = BGl_string2003z00zzbackend_jvmz00;
								}
							else
								{	/* BackEnd/jvm.scm 171 */
									BgL_arg1559z00_1997 = BGl_string2004z00zzbackend_jvmz00;
								}
						}
				}
				return string_append(BgL_namez00_8, BgL_arg1559z00_1997);
			}
		}

	}



/* jasname */
	obj_t BGl_jasnamez00zzbackend_jvmz00(obj_t BgL_cfz00_9)
	{
		{	/* BackEnd/jvm.scm 179 */
			if (PAIRP(BgL_cfz00_9))
				{	/* BackEnd/jvm.scm 180 */
					obj_t BgL_carzd2139zd2_2003;

					BgL_carzd2139zd2_2003 = CAR(((obj_t) BgL_cfz00_9));
					if (PAIRP(BgL_carzd2139zd2_2003))
						{	/* BackEnd/jvm.scm 180 */
							obj_t BgL_cdrzd2143zd2_2005;

							BgL_cdrzd2143zd2_2005 = CDR(BgL_carzd2139zd2_2003);
							if ((CAR(BgL_carzd2139zd2_2003) == CNST_TABLE_REF(9)))
								{	/* BackEnd/jvm.scm 180 */
									if (PAIRP(BgL_cdrzd2143zd2_2005))
										{	/* BackEnd/jvm.scm 180 */
											if (NULLP(CDR(BgL_cdrzd2143zd2_2005)))
												{	/* BackEnd/jvm.scm 180 */
													obj_t BgL_arg1575z00_2011;

													BgL_arg1575z00_2011 = CAR(BgL_cdrzd2143zd2_2005);
													{	/* BackEnd/jvm.scm 182 */
														obj_t BgL_arg1585z00_2724;

														{	/* BackEnd/jvm.scm 182 */
															obj_t BgL_arg1455z00_2726;

															BgL_arg1455z00_2726 =
																SYMBOL_TO_STRING(((obj_t) BgL_arg1575z00_2011));
															BgL_arg1585z00_2724 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_2726);
														}
														return
															BGl_addsuffixz00zzbackend_jvmz00
															(BgL_arg1585z00_2724);
													}
												}
											else
												{	/* BackEnd/jvm.scm 180 */
													return BFALSE;
												}
										}
									else
										{	/* BackEnd/jvm.scm 180 */
											return BFALSE;
										}
								}
							else
								{	/* BackEnd/jvm.scm 180 */
									return BFALSE;
								}
						}
					else
						{	/* BackEnd/jvm.scm 180 */
							return BFALSE;
						}
				}
			else
				{	/* BackEnd/jvm.scm 180 */
					return BFALSE;
				}
		}

	}



/* jvm-filename */
	obj_t BGl_jvmzd2filenamezd2zzbackend_jvmz00(obj_t BgL_namez00_10)
	{
		{	/* BackEnd/jvm.scm 187 */
			if (STRINGP(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00))
				{	/* BackEnd/jvm.scm 189 */
					bool_t BgL_test2121z00_3444;

					{	/* BackEnd/jvm.scm 189 */
						long BgL_l1z00_2730;

						BgL_l1z00_2730 = STRING_LENGTH(BgL_namez00_10);
						if ((BgL_l1z00_2730 == 1L))
							{	/* BackEnd/jvm.scm 189 */
								int BgL_arg1282z00_2733;

								{	/* BackEnd/jvm.scm 189 */
									char *BgL_auxz00_3450;
									char *BgL_tmpz00_3448;

									BgL_auxz00_3450 =
										BSTRING_TO_STRING(BGl_string1993z00zzbackend_jvmz00);
									BgL_tmpz00_3448 = BSTRING_TO_STRING(BgL_namez00_10);
									BgL_arg1282z00_2733 =
										memcmp(BgL_tmpz00_3448, BgL_auxz00_3450, BgL_l1z00_2730);
								}
								BgL_test2121z00_3444 = ((long) (BgL_arg1282z00_2733) == 0L);
							}
						else
							{	/* BackEnd/jvm.scm 189 */
								BgL_test2121z00_3444 = ((bool_t) 0);
							}
					}
					if (BgL_test2121z00_3444)
						{	/* BackEnd/jvm.scm 189 */
							return BGl_za2jvmzd2directoryza2zd2zzengine_paramz00;
						}
					else
						{	/* BackEnd/jvm.scm 189 */
							return
								BGl_makezd2filezd2namez00zz__osz00
								(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00, BgL_namez00_10);
						}
				}
			else
				{	/* BackEnd/jvm.scm 188 */
					return BgL_namez00_10;
				}
		}

	}



/* jvm-dirname */
	obj_t BGl_jvmzd2dirnamezd2zzbackend_jvmz00(obj_t BgL_filez00_11)
	{
		{	/* BackEnd/jvm.scm 197 */
			{	/* BackEnd/jvm.scm 198 */
				obj_t BgL_dfilez00_2017;

				BgL_dfilez00_2017 = BGl_dirnamez00zz__osz00(BgL_filez00_11);
				{	/* BackEnd/jvm.scm 198 */
					obj_t BgL_dirz00_2018;

					BgL_dirz00_2018 =
						BGl_jvmzd2filenamezd2zzbackend_jvmz00(BgL_dfilez00_2017);
					{	/* BackEnd/jvm.scm 199 */

						{	/* BackEnd/jvm.scm 200 */
							bool_t BgL_test2123z00_3458;

							{	/* BackEnd/jvm.scm 200 */
								bool_t BgL_test2124z00_3459;

								{	/* BackEnd/jvm.scm 200 */
									long BgL_l1z00_2741;

									BgL_l1z00_2741 = STRING_LENGTH(BgL_dfilez00_2017);
									if ((BgL_l1z00_2741 == 0L))
										{	/* BackEnd/jvm.scm 200 */
											int BgL_arg1282z00_2744;

											{	/* BackEnd/jvm.scm 200 */
												char *BgL_auxz00_3465;
												char *BgL_tmpz00_3463;

												BgL_auxz00_3465 =
													BSTRING_TO_STRING(BGl_string1996z00zzbackend_jvmz00);
												BgL_tmpz00_3463 = BSTRING_TO_STRING(BgL_dfilez00_2017);
												BgL_arg1282z00_2744 =
													memcmp(BgL_tmpz00_3463, BgL_auxz00_3465,
													BgL_l1z00_2741);
											}
											BgL_test2124z00_3459 =
												((long) (BgL_arg1282z00_2744) == 0L);
										}
									else
										{	/* BackEnd/jvm.scm 200 */
											BgL_test2124z00_3459 = ((bool_t) 0);
										}
								}
								if (BgL_test2124z00_3459)
									{	/* BackEnd/jvm.scm 200 */
										BgL_test2123z00_3458 = ((bool_t) 0);
									}
								else
									{	/* BackEnd/jvm.scm 200 */
										if (bgl_directoryp(BSTRING_TO_STRING(BgL_dfilez00_2017)))
											{	/* BackEnd/jvm.scm 201 */
												BgL_test2123z00_3458 = ((bool_t) 0);
											}
										else
											{	/* BackEnd/jvm.scm 201 */
												if (fexists(BSTRING_TO_STRING(BgL_dfilez00_2017)))
													{	/* BackEnd/jvm.scm 202 */
														BgL_test2123z00_3458 = ((bool_t) 0);
													}
												else
													{	/* BackEnd/jvm.scm 203 */
														bool_t BgL__ortest_1141z00_2028;

														if (STRINGP
															(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00))
															{	/* BackEnd/jvm.scm 203 */
																BgL__ortest_1141z00_2028 = ((bool_t) 0);
															}
														else
															{	/* BackEnd/jvm.scm 203 */
																BgL__ortest_1141z00_2028 = ((bool_t) 1);
															}
														if (BgL__ortest_1141z00_2028)
															{	/* BackEnd/jvm.scm 203 */
																BgL_test2123z00_3458 = BgL__ortest_1141z00_2028;
															}
														else
															{	/* BackEnd/jvm.scm 203 */
																BgL_test2123z00_3458 =
																	bgl_directoryp(BSTRING_TO_STRING
																	(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00));
															}
													}
											}
									}
							}
							if (BgL_test2123z00_3458)
								{	/* BackEnd/jvm.scm 200 */
									BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00
										(BgL_dirz00_2018);
								}
							else
								{	/* BackEnd/jvm.scm 200 */
									((bool_t) 0);
								}
						}
						return BgL_dirz00_2018;
					}
				}
			}
		}

	}



/* &start-jvm-emission! */
	obj_t BGl_z62startzd2jvmzd2emissionz12z70zzbackend_jvmz00(obj_t
		BgL_envz00_2967)
	{
		{	/* BackEnd/jvm.scm 212 */
			if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
				{	/* BackEnd/jvm.scm 215 */
					obj_t BgL_dnamez00_3042;

					BgL_dnamez00_3042 =
						BGl_dirnamez00zz__osz00(BGl_za2destza2z00zzengine_paramz00);
					{	/* BackEnd/jvm.scm 216 */
						bool_t BgL_test2131z00_3485;

						{	/* BackEnd/jvm.scm 216 */
							long BgL_l1z00_3043;

							BgL_l1z00_3043 = STRING_LENGTH(BgL_dnamez00_3042);
							if ((BgL_l1z00_3043 == 0L))
								{	/* BackEnd/jvm.scm 216 */
									int BgL_arg1282z00_3044;

									{	/* BackEnd/jvm.scm 216 */
										char *BgL_auxz00_3491;
										char *BgL_tmpz00_3489;

										BgL_auxz00_3491 =
											BSTRING_TO_STRING(BGl_string1996z00zzbackend_jvmz00);
										BgL_tmpz00_3489 = BSTRING_TO_STRING(BgL_dnamez00_3042);
										BgL_arg1282z00_3044 =
											memcmp(BgL_tmpz00_3489, BgL_auxz00_3491, BgL_l1z00_3043);
									}
									BgL_test2131z00_3485 = ((long) (BgL_arg1282z00_3044) == 0L);
								}
							else
								{	/* BackEnd/jvm.scm 216 */
									BgL_test2131z00_3485 = ((bool_t) 0);
								}
						}
						if (BgL_test2131z00_3485)
							{	/* BackEnd/jvm.scm 216 */
								BFALSE;
							}
						else
							{	/* BackEnd/jvm.scm 216 */
								BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00 =
									BGl_jvmzd2dirnamezd2zzbackend_jvmz00
									(BGl_za2destza2z00zzengine_paramz00);
							}
					}
				}
			else
				{	/* BackEnd/jvm.scm 214 */
					if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(10)))
						{	/* BackEnd/jvm.scm 218 */
							if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
								{	/* BackEnd/jvm.scm 219 */
									BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00 =
										BGl_jvmzd2dirnamezd2zzbackend_jvmz00(CAR
										(BGl_za2srczd2filesza2zd2zzengine_paramz00));
								}
							else
								{	/* BackEnd/jvm.scm 219 */
									BFALSE;
								}
						}
					else
						{	/* BackEnd/jvm.scm 218 */
							BFALSE;
						}
				}
			{	/* BackEnd/jvm.scm 221 */
				bool_t BgL_test2135z00_3504;

				if (fexists(BSTRING_TO_STRING
						(BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00)))
					{	/* BackEnd/jvm.scm 221 */
						BgL_test2135z00_3504 =
							bgl_directoryp(BSTRING_TO_STRING
							(BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00));
					}
				else
					{	/* BackEnd/jvm.scm 221 */
						BgL_test2135z00_3504 = ((bool_t) 0);
					}
				if (BgL_test2135z00_3504)
					{	/* BackEnd/jvm.scm 221 */
						return BTRUE;
					}
				else
					{	/* BackEnd/jvm.scm 221 */
						return
							BGl_errorz00zz__errorz00(BGl_string2005z00zzbackend_jvmz00,
							BGl_string2006z00zzbackend_jvmz00,
							BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00);
					}
			}
		}

	}



/* make-tmp-file-name */
	obj_t BGl_makezd2tmpzd2filezd2namezd2zzbackend_jvmz00(void)
	{
		{	/* BackEnd/jvm.scm 256 */
			return
				string_append(BGl_string2007z00zzbackend_jvmz00,
				BGl_string2008z00zzbackend_jvmz00);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_jvmz00(void)
	{
		{	/* BackEnd/jvm.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_jvmz00(void)
	{
		{	/* BackEnd/jvm.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_jvmz00(void)
	{
		{	/* BackEnd/jvm.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2compilezd2envz00zzbackend_backendz00,
				BGl_jvmz00zzbackend_jvm_classz00, BGl_proc2009z00zzbackend_jvmz00,
				BGl_string2010z00zzbackend_jvmz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2linkzd2envz00zzbackend_backendz00,
				BGl_jvmz00zzbackend_jvm_classz00, BGl_proc2011z00zzbackend_jvmz00,
				BGl_string2012z00zzbackend_jvmz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2cnstzd2tablezd2namezd2envz00zzbackend_backendz00,
				BGl_jvmz00zzbackend_jvm_classz00, BGl_proc2013z00zzbackend_jvmz00,
				BGl_string2014z00zzbackend_jvmz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2linkzd2objectszd2envzd2zzbackend_backendz00,
				BGl_jvmz00zzbackend_jvm_classz00, BGl_proc2015z00zzbackend_jvmz00,
				BGl_string2016z00zzbackend_jvmz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2checkzd2inlineszd2envzd2zzbackend_backendz00,
				BGl_jvmz00zzbackend_jvm_classz00, BGl_proc2017z00zzbackend_jvmz00,
				BGl_string2018z00zzbackend_jvmz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2subtypezf3zd2envzf3zzbackend_backendz00,
				BGl_jvmz00zzbackend_jvm_classz00, BGl_proc2019z00zzbackend_jvmz00,
				BGl_string2020z00zzbackend_jvmz00);
		}

	}



/* &backend-subtype?-jvm1444 */
	obj_t BGl_z62backendzd2subtypezf3zd2jvm1444z91zzbackend_jvmz00(obj_t
		BgL_envz00_2980, obj_t BgL_bz00_2981, obj_t BgL_t1z00_2982,
		obj_t BgL_t2z00_2983)
	{
		{	/* BackEnd/jvm.scm 368 */
			{	/* BackEnd/jvm.scm 369 */
				bool_t BgL__ortest_1150z00_3046;

				BgL__ortest_1150z00_3046 = (BgL_t1z00_2982 == BgL_t2z00_2983);
				if (BgL__ortest_1150z00_3046)
					{	/* BackEnd/jvm.scm 369 */
						return BBOOL(BgL__ortest_1150z00_3046);
					}
				else
					{	/* BackEnd/jvm.scm 370 */
						bool_t BgL__ortest_1151z00_3047;

						BgL__ortest_1151z00_3047 =
							(
							(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_t1z00_2982)))->BgL_idz00) ==
							(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_t2z00_2983)))->BgL_idz00));
						if (BgL__ortest_1151z00_3047)
							{	/* BackEnd/jvm.scm 370 */
								return BBOOL(BgL__ortest_1151z00_3047);
							}
						else
							{	/* BackEnd/jvm.scm 371 */
								bool_t BgL__ortest_1152z00_3048;

								{	/* BackEnd/jvm.scm 371 */
									bool_t BgL_test2139z00_3528;

									{	/* BackEnd/jvm.scm 371 */
										obj_t BgL_classz00_3049;

										BgL_classz00_3049 = BGl_tclassz00zzobject_classz00;
										if (BGL_OBJECTP(BgL_t1z00_2982))
											{	/* BackEnd/jvm.scm 371 */
												BgL_objectz00_bglt BgL_arg1807z00_3050;

												BgL_arg1807z00_3050 =
													(BgL_objectz00_bglt) (BgL_t1z00_2982);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* BackEnd/jvm.scm 371 */
														long BgL_idxz00_3051;

														BgL_idxz00_3051 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3050);
														BgL_test2139z00_3528 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3051 + 2L)) == BgL_classz00_3049);
													}
												else
													{	/* BackEnd/jvm.scm 371 */
														bool_t BgL_res1980z00_3054;

														{	/* BackEnd/jvm.scm 371 */
															obj_t BgL_oclassz00_3055;

															{	/* BackEnd/jvm.scm 371 */
																obj_t BgL_arg1815z00_3056;
																long BgL_arg1816z00_3057;

																BgL_arg1815z00_3056 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* BackEnd/jvm.scm 371 */
																	long BgL_arg1817z00_3058;

																	BgL_arg1817z00_3058 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3050);
																	BgL_arg1816z00_3057 =
																		(BgL_arg1817z00_3058 - OBJECT_TYPE);
																}
																BgL_oclassz00_3055 =
																	VECTOR_REF(BgL_arg1815z00_3056,
																	BgL_arg1816z00_3057);
															}
															{	/* BackEnd/jvm.scm 371 */
																bool_t BgL__ortest_1115z00_3059;

																BgL__ortest_1115z00_3059 =
																	(BgL_classz00_3049 == BgL_oclassz00_3055);
																if (BgL__ortest_1115z00_3059)
																	{	/* BackEnd/jvm.scm 371 */
																		BgL_res1980z00_3054 =
																			BgL__ortest_1115z00_3059;
																	}
																else
																	{	/* BackEnd/jvm.scm 371 */
																		long BgL_odepthz00_3060;

																		{	/* BackEnd/jvm.scm 371 */
																			obj_t BgL_arg1804z00_3061;

																			BgL_arg1804z00_3061 =
																				(BgL_oclassz00_3055);
																			BgL_odepthz00_3060 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3061);
																		}
																		if ((2L < BgL_odepthz00_3060))
																			{	/* BackEnd/jvm.scm 371 */
																				obj_t BgL_arg1802z00_3062;

																				{	/* BackEnd/jvm.scm 371 */
																					obj_t BgL_arg1803z00_3063;

																					BgL_arg1803z00_3063 =
																						(BgL_oclassz00_3055);
																					BgL_arg1802z00_3062 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3063, 2L);
																				}
																				BgL_res1980z00_3054 =
																					(BgL_arg1802z00_3062 ==
																					BgL_classz00_3049);
																			}
																		else
																			{	/* BackEnd/jvm.scm 371 */
																				BgL_res1980z00_3054 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2139z00_3528 = BgL_res1980z00_3054;
													}
											}
										else
											{	/* BackEnd/jvm.scm 371 */
												BgL_test2139z00_3528 = ((bool_t) 0);
											}
									}
									if (BgL_test2139z00_3528)
										{	/* BackEnd/jvm.scm 371 */
											BgL__ortest_1152z00_3048 =
												(
												(((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt) BgL_t2z00_2983)))->
													BgL_idz00) == CNST_TABLE_REF(11));
										}
									else
										{	/* BackEnd/jvm.scm 371 */
											BgL__ortest_1152z00_3048 = ((bool_t) 0);
										}
								}
								if (BgL__ortest_1152z00_3048)
									{	/* BackEnd/jvm.scm 371 */
										return BBOOL(BgL__ortest_1152z00_3048);
									}
								else
									{	/* BackEnd/jvm.scm 372 */
										bool_t BgL__ortest_1153z00_3064;

										BgL__ortest_1153z00_3064 =
											(
											(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_t2z00_2983)))->
												BgL_namez00) == CNST_TABLE_REF(12));
										if (BgL__ortest_1153z00_3064)
											{	/* BackEnd/jvm.scm 372 */
												return BBOOL(BgL__ortest_1153z00_3064);
											}
										else
											{	/* BackEnd/jvm.scm 373 */
												bool_t BgL__ortest_1154z00_3065;

												BgL__ortest_1154z00_3065 =
													(
													(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_t1z00_2982)))->
														BgL_namez00) ==
													(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
																	BgL_t2z00_2983)))->BgL_namez00));
												if (BgL__ortest_1154z00_3065)
													{	/* BackEnd/jvm.scm 373 */
														return BBOOL(BgL__ortest_1154z00_3065);
													}
												else
													{	/* BackEnd/jvm.scm 374 */
														obj_t BgL__ortest_1155z00_3066;

														BgL__ortest_1155z00_3066 =
															BGl_iszd2subtypezf3z21zztype_typeofz00
															(BgL_t1z00_2982, BgL_t2z00_2983);
														if (CBOOL(BgL__ortest_1155z00_3066))
															{	/* BackEnd/jvm.scm 374 */
																return BgL__ortest_1155z00_3066;
															}
														else
															{	/* BackEnd/jvm.scm 374 */
																return
																	BBOOL(BGl_subzd2typezf3z21zztype_envz00(
																		((BgL_typez00_bglt) BgL_t1z00_2982),
																		((BgL_typez00_bglt) BgL_t2z00_2983)));
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &backend-check-inline1442 */
	obj_t BGl_z62backendzd2checkzd2inline1442z62zzbackend_jvmz00(obj_t
		BgL_envz00_2984, obj_t BgL_mez00_2985)
	{
		{	/* BackEnd/jvm.scm 362 */
			if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
				{	/* BackEnd/jvm.scm 363 */
					return BFALSE;
				}
			else
				{	/* BackEnd/jvm.scm 363 */
					return BGl_checkzd2jvmzd2inlinesz00zzsaw_jvm_inlinez00();
				}
		}

	}



/* &backend-link-objects1440 */
	obj_t BGl_z62backendzd2linkzd2objects1440z62zzbackend_jvmz00(obj_t
		BgL_envz00_2986, obj_t BgL_mez00_2987, obj_t BgL_sourcesz00_2988)
	{
		{	/* BackEnd/jvm.scm 262 */
			if (NULLP(BgL_sourcesz00_2988))
				{	/* BackEnd/jvm.scm 267 */
					obj_t BgL_firstz00_3069;

					BgL_firstz00_3069 =
						BGl_prefixz00zz__osz00(CAR
						(BGl_za2ozd2filesza2zd2zzengine_paramz00));
					{	/* BackEnd/jvm.scm 268 */
						obj_t BgL_list1757z00_3070;

						{	/* BackEnd/jvm.scm 268 */
							obj_t BgL_arg1761z00_3071;

							{	/* BackEnd/jvm.scm 268 */
								obj_t BgL_arg1762z00_3072;

								{	/* BackEnd/jvm.scm 268 */
									obj_t BgL_arg1765z00_3073;

									BgL_arg1765z00_3073 =
										MAKE_YOUNG_PAIR(BGl_za2ozd2filesza2zd2zzengine_paramz00,
										BNIL);
									BgL_arg1762z00_3072 =
										MAKE_YOUNG_PAIR(BGl_string2021z00zzbackend_jvmz00,
										BgL_arg1765z00_3073);
								}
								BgL_arg1761z00_3071 =
									MAKE_YOUNG_PAIR(BGl_string2022z00zzbackend_jvmz00,
									BgL_arg1762z00_3072);
							}
							BgL_list1757z00_3070 =
								MAKE_YOUNG_PAIR(BGl_string2023z00zzbackend_jvmz00,
								BgL_arg1761z00_3071);
						}
						BGl_warningz00zz__errorz00(BgL_list1757z00_3070);
					}
					BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00();
					BGl_readzd2jfilezd2zzread_jvmz00();
					return BGl_jvmzd2ldzd2zzsaw_jvm_ldz00(BFALSE);
				}
			else
				{
					obj_t BgL_sourcesz00_3075;
					obj_t BgL_clsz00_3076;
					obj_t BgL_mainzd2modulezd2_3077;
					obj_t BgL_mainz00_3078;
					obj_t BgL_fmainz00_3079;
					obj_t BgL_librariesz00_3080;

					BgL_sourcesz00_3075 = BgL_sourcesz00_2988;
					BgL_clsz00_3076 = BNIL;
					BgL_mainzd2modulezd2_3077 = BFALSE;
					BgL_mainz00_3078 = BFALSE;
					BgL_fmainz00_3079 = BGl_string1996z00zzbackend_jvmz00;
					BgL_librariesz00_3080 = BNIL;
				BgL_loopz00_3074:
					if (NULLP(BgL_sourcesz00_3075))
						{	/* BackEnd/jvm.scm 279 */
							if (CBOOL(BgL_mainz00_3078))
								{	/* BackEnd/jvm.scm 283 */
									obj_t BgL_firstz00_3081;

									BgL_firstz00_3081 =
										BGl_prefixz00zz__osz00(CAR
										(BGl_za2ozd2filesza2zd2zzengine_paramz00));
									{
										obj_t BgL_l1431z00_3083;

										BgL_l1431z00_3083 = BgL_librariesz00_3080;
									BgL_zc3z04anonymousza31770ze3z87_3082:
										if (PAIRP(BgL_l1431z00_3083))
											{	/* BackEnd/jvm.scm 286 */
												{	/* BackEnd/jvm.scm 287 */
													obj_t BgL_libz00_3084;

													BgL_libz00_3084 = CAR(BgL_l1431z00_3083);
													{
														obj_t BgL_clausesz00_3090;
														obj_t BgL_libsz00_3087;

														if (PAIRP(BgL_libz00_3084))
															{	/* BackEnd/jvm.scm 287 */
																if (
																	(CAR(
																			((obj_t) BgL_libz00_3084)) ==
																		CNST_TABLE_REF(13)))
																	{	/* BackEnd/jvm.scm 287 */
																		obj_t BgL_arg1775z00_3096;

																		BgL_arg1775z00_3096 =
																			CDR(((obj_t) BgL_libz00_3084));
																		BgL_libsz00_3087 = BgL_arg1775z00_3096;
																		{
																			obj_t BgL_l1422z00_3089;

																			BgL_l1422z00_3089 = BgL_libsz00_3087;
																		BgL_zc3z04anonymousza31809ze3z87_3088:
																			if (PAIRP(BgL_l1422z00_3089))
																				{	/* BackEnd/jvm.scm 289 */
																					BGl_usezd2libraryz12zc0zzmodule_alibraryz00
																						(CAR(BgL_l1422z00_3089));
																					{
																						obj_t BgL_l1422z00_3614;

																						BgL_l1422z00_3614 =
																							CDR(BgL_l1422z00_3089);
																						BgL_l1422z00_3089 =
																							BgL_l1422z00_3614;
																						goto
																							BgL_zc3z04anonymousza31809ze3z87_3088;
																					}
																				}
																			else
																				{	/* BackEnd/jvm.scm 289 */
																					((bool_t) 1);
																				}
																		}
																	}
																else
																	{	/* BackEnd/jvm.scm 287 */
																		if (
																			(CAR(
																					((obj_t) BgL_libz00_3084)) ==
																				CNST_TABLE_REF(14)))
																			{	/* BackEnd/jvm.scm 287 */
																				obj_t BgL_arg1805z00_3097;

																				BgL_arg1805z00_3097 =
																					CDR(((obj_t) BgL_libz00_3084));
																				BgL_clausesz00_3090 =
																					BgL_arg1805z00_3097;
																				{	/* BackEnd/jvm.scm 291 */
																					obj_t BgL_fun1430z00_3091;

																					{	/* BackEnd/jvm.scm 291 */
																						obj_t
																							BgL_zc3z04anonymousza31832ze3z87_3092;
																						{	/* BackEnd/jvm.scm 287 */
																							int BgL_tmpz00_3623;

																							BgL_tmpz00_3623 = (int) (0L);
																							BgL_zc3z04anonymousza31832ze3z87_3092
																								=
																								MAKE_EL_PROCEDURE
																								(BgL_tmpz00_3623);
																						}
																						BgL_fun1430z00_3091 =
																							BgL_zc3z04anonymousza31832ze3z87_3092;
																					}
																					{
																						obj_t BgL_l1428z00_3094;

																						BgL_l1428z00_3094 =
																							BgL_clausesz00_3090;
																					BgL_zc3z04anonymousza31821ze3z87_3093:
																						if (PAIRP
																							(BgL_l1428z00_3094))
																							{	/* BackEnd/jvm.scm 291 */
																								{	/* BackEnd/jvm.scm 291 */
																									obj_t BgL_arg1823z00_3095;

																									BgL_arg1823z00_3095 =
																										CAR(BgL_l1428z00_3094);
																									BGl_z62zc3z04anonymousza31832ze3ze5zzbackend_jvmz00
																										(BgL_fun1430z00_3091,
																										BgL_arg1823z00_3095);
																								}
																								{
																									obj_t BgL_l1428z00_3633;

																									BgL_l1428z00_3633 =
																										CDR(BgL_l1428z00_3094);
																									BgL_l1428z00_3094 =
																										BgL_l1428z00_3633;
																									goto
																										BgL_zc3z04anonymousza31821ze3z87_3093;
																								}
																							}
																						else
																							{	/* BackEnd/jvm.scm 291 */
																								((bool_t) 1);
																							}
																					}
																				}
																			}
																		else
																			{	/* BackEnd/jvm.scm 287 */
																				((bool_t) 0);
																			}
																	}
															}
														else
															{	/* BackEnd/jvm.scm 287 */
																((bool_t) 0);
															}
													}
												}
												{
													obj_t BgL_l1431z00_3635;

													BgL_l1431z00_3635 = CDR(BgL_l1431z00_3083);
													BgL_l1431z00_3083 = BgL_l1431z00_3635;
													goto BgL_zc3z04anonymousza31770ze3z87_3082;
												}
											}
										else
											{	/* BackEnd/jvm.scm 286 */
												((bool_t) 1);
											}
									}
									BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00();
									{	/* BackEnd/jvm.scm 299 */
										obj_t BgL_list1848z00_3098;

										BgL_list1848z00_3098 =
											MAKE_YOUNG_PAIR(BgL_fmainz00_3079, BNIL);
										BGl_za2srczd2filesza2zd2zzengine_paramz00 =
											BgL_list1848z00_3098;
									}
									BGl_readzd2jfilezd2zzread_jvmz00();
									return
										BGl_jvmzd2ldzd2zzsaw_jvm_ldz00(BgL_mainzd2modulezd2_3077);
								}
							else
								{	/* BackEnd/jvm.scm 302 */
									obj_t BgL_tmpz00_3099;

									BgL_tmpz00_3099 =
										BGl_makezd2tmpzd2filezd2namezd2zzbackend_jvmz00();
									BGl_makezd2tmpzd2mainz00zzengine_linkz00(BgL_tmpz00_3099,
										CBOOL(BgL_mainz00_3078),
										bstring_to_symbol(BGl_string2007z00zzbackend_jvmz00),
										BgL_clsz00_3076, BgL_librariesz00_3080);
									{	/* BackEnd/jvm.scm 304 */
										obj_t BgL_list1851z00_3100;

										BgL_list1851z00_3100 =
											MAKE_YOUNG_PAIR(BgL_tmpz00_3099, BNIL);
										BGl_za2srczd2filesza2zd2zzengine_paramz00 =
											BgL_list1851z00_3100;
									}
									{
										obj_t BgL_raz00_3102;
										obj_t BgL_resz00_3103;

										BgL_raz00_3102 = BGl_za2restzd2argsza2zd2zzengine_paramz00;
										BgL_resz00_3103 = BNIL;
									BgL_liipz00_3101:
										if (NULLP(BgL_raz00_3102))
											{	/* BackEnd/jvm.scm 310 */
												BGl_za2restzd2argsza2zd2zzengine_paramz00 =
													bgl_reverse_bang(BgL_resz00_3103);
											}
										else
											{	/* BackEnd/jvm.scm 312 */
												bool_t BgL_test2159z00_3649;

												{	/* BackEnd/jvm.scm 312 */
													obj_t BgL_arg1863z00_3104;

													{	/* BackEnd/jvm.scm 312 */
														obj_t BgL_arg1864z00_3105;

														BgL_arg1864z00_3105 = CAR(((obj_t) BgL_raz00_3102));
														BgL_arg1863z00_3104 =
															BGl_suffixz00zz__osz00(BgL_arg1864z00_3105);
													}
													BgL_test2159z00_3649 =
														CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1863z00_3104,
															BGl_za2mcozd2suffixza2zd2zzengine_paramz00));
												}
												if (BgL_test2159z00_3649)
													{	/* BackEnd/jvm.scm 313 */
														obj_t BgL_arg1858z00_3106;

														BgL_arg1858z00_3106 = CDR(((obj_t) BgL_raz00_3102));
														{
															obj_t BgL_raz00_3657;

															BgL_raz00_3657 = BgL_arg1858z00_3106;
															BgL_raz00_3102 = BgL_raz00_3657;
															goto BgL_liipz00_3101;
														}
													}
												else
													{	/* BackEnd/jvm.scm 315 */
														obj_t BgL_arg1859z00_3107;
														obj_t BgL_arg1860z00_3108;

														BgL_arg1859z00_3107 = CDR(((obj_t) BgL_raz00_3102));
														{	/* BackEnd/jvm.scm 315 */
															obj_t BgL_arg1862z00_3109;

															BgL_arg1862z00_3109 =
																CAR(((obj_t) BgL_raz00_3102));
															BgL_arg1860z00_3108 =
																MAKE_YOUNG_PAIR(BgL_arg1862z00_3109,
																BgL_resz00_3103);
														}
														{
															obj_t BgL_resz00_3664;
															obj_t BgL_raz00_3663;

															BgL_raz00_3663 = BgL_arg1859z00_3107;
															BgL_resz00_3664 = BgL_arg1860z00_3108;
															BgL_resz00_3103 = BgL_resz00_3664;
															BgL_raz00_3102 = BgL_raz00_3663;
															goto BgL_liipz00_3101;
														}
													}
											}
									}
									{	/* BackEnd/jvm.scm 316 */
										obj_t BgL_exitd1145z00_3110;

										BgL_exitd1145z00_3110 = BGL_EXITD_TOP_AS_OBJ();
										{	/* BackEnd/jvm.scm 319 */
											obj_t BgL_zc3z04anonymousza31865ze3z87_3111;

											BgL_zc3z04anonymousza31865ze3z87_3111 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31865ze3ze5zzbackend_jvmz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31865ze3z87_3111,
												(int) (0L), BgL_tmpz00_3099);
											{	/* BackEnd/jvm.scm 316 */
												obj_t BgL_arg1828z00_3112;

												{	/* BackEnd/jvm.scm 316 */
													obj_t BgL_arg1829z00_3113;

													BgL_arg1829z00_3113 =
														BGL_EXITD_PROTECT(BgL_exitd1145z00_3110);
													BgL_arg1828z00_3112 =
														MAKE_YOUNG_PAIR
														(BgL_zc3z04anonymousza31865ze3z87_3111,
														BgL_arg1829z00_3113);
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1145z00_3110,
													BgL_arg1828z00_3112);
												BUNSPEC;
											}
											{	/* BackEnd/jvm.scm 317 */
												obj_t BgL_tmp1147z00_3114;

												BgL_tmp1147z00_3114 =
													BGl_compilerz00zzengine_compilerz00();
												{	/* BackEnd/jvm.scm 316 */
													bool_t BgL_test2160z00_3675;

													{	/* BackEnd/jvm.scm 316 */
														obj_t BgL_arg1827z00_3115;

														BgL_arg1827z00_3115 =
															BGL_EXITD_PROTECT(BgL_exitd1145z00_3110);
														BgL_test2160z00_3675 = PAIRP(BgL_arg1827z00_3115);
													}
													if (BgL_test2160z00_3675)
														{	/* BackEnd/jvm.scm 316 */
															obj_t BgL_arg1825z00_3116;

															{	/* BackEnd/jvm.scm 316 */
																obj_t BgL_arg1826z00_3117;

																BgL_arg1826z00_3117 =
																	BGL_EXITD_PROTECT(BgL_exitd1145z00_3110);
																BgL_arg1825z00_3116 =
																	CDR(((obj_t) BgL_arg1826z00_3117));
															}
															BGL_EXITD_PROTECT_SET(BgL_exitd1145z00_3110,
																BgL_arg1825z00_3116);
															BUNSPEC;
														}
													else
														{	/* BackEnd/jvm.scm 316 */
															BFALSE;
														}
												}
												BGl_z62zc3z04anonymousza31865ze3ze5zzbackend_jvmz00
													(BgL_zc3z04anonymousza31865ze3z87_3111);
												BgL_tmp1147z00_3114;
											}
										}
									}
									return BINT(0L);
								}
						}
					else
						{	/* BackEnd/jvm.scm 325 */
							obj_t BgL_portz00_3118;

							{	/* BackEnd/jvm.scm 325 */
								obj_t BgL_arg1899z00_3119;

								{	/* BackEnd/jvm.scm 325 */
									obj_t BgL_pairz00_3120;

									BgL_pairz00_3120 = CAR(((obj_t) BgL_sourcesz00_3075));
									BgL_arg1899z00_3119 = CAR(BgL_pairz00_3120);
								}
								{	/* BackEnd/jvm.scm 325 */

									BgL_portz00_3118 =
										BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
										(BgL_arg1899z00_3119, BTRUE, BINT(5000000L));
								}
							}
							if (INPUT_PORTP(BgL_portz00_3118))
								{	/* BackEnd/jvm.scm 328 */
									obj_t BgL_expz00_3121;

									{	/* BackEnd/jvm.scm 328 */
										obj_t BgL_list1897z00_3122;

										BgL_list1897z00_3122 =
											MAKE_YOUNG_PAIR(BgL_portz00_3118, BNIL);
										BgL_expz00_3121 =
											BGl_compilerzd2readzd2zzread_readerz00
											(BgL_list1897z00_3122);
									}
									bgl_close_input_port(BgL_portz00_3118);
									{
										obj_t BgL_namez00_3125;

										if (PAIRP(BgL_expz00_3121))
											{	/* BackEnd/jvm.scm 330 */
												obj_t BgL_cdrzd2177zd2_3151;

												BgL_cdrzd2177zd2_3151 = CDR(((obj_t) BgL_expz00_3121));
												if (
													(CAR(
															((obj_t) BgL_expz00_3121)) == CNST_TABLE_REF(15)))
													{	/* BackEnd/jvm.scm 330 */
														if (PAIRP(BgL_cdrzd2177zd2_3151))
															{	/* BackEnd/jvm.scm 330 */
																BgL_namez00_3125 = CAR(BgL_cdrzd2177zd2_3151);
																{	/* BackEnd/jvm.scm 332 */
																	obj_t BgL_libsz00_3126;
																	obj_t BgL_nmainz00_3127;

																	{	/* BackEnd/jvm.scm 332 */
																		obj_t BgL_arg1893z00_3128;

																		{	/* BackEnd/jvm.scm 332 */
																			obj_t BgL_pairz00_3129;

																			BgL_pairz00_3129 =
																				CDR(((obj_t) BgL_expz00_3121));
																			BgL_arg1893z00_3128 =
																				CDR(BgL_pairz00_3129);
																		}
																		BgL_libsz00_3126 =
																			BGl_findzd2librarieszd2zzengine_linkz00
																			(BgL_arg1893z00_3128);
																	}
																	{	/* BackEnd/jvm.scm 333 */
																		obj_t BgL_arg1894z00_3130;

																		{	/* BackEnd/jvm.scm 333 */
																			obj_t BgL_pairz00_3131;

																			BgL_pairz00_3131 =
																				CDR(((obj_t) BgL_expz00_3121));
																			BgL_arg1894z00_3130 =
																				CDR(BgL_pairz00_3131);
																		}
																		BgL_nmainz00_3127 =
																			BGl_findzd2mainzd2zzengine_linkz00
																			(BgL_arg1894z00_3130);
																	}
																	{	/* BackEnd/jvm.scm 337 */
																		obj_t BgL_arg1874z00_3132;

																		{	/* BackEnd/jvm.scm 337 */
																			obj_t BgL_arg1876z00_3133;

																			{	/* BackEnd/jvm.scm 337 */
																				obj_t BgL_arg1878z00_3134;

																				{	/* BackEnd/jvm.scm 337 */
																					obj_t BgL_arg1879z00_3135;

																					{	/* BackEnd/jvm.scm 337 */
																						obj_t BgL_pairz00_3136;

																						BgL_pairz00_3136 =
																							CAR(
																							((obj_t) BgL_sourcesz00_3075));
																						BgL_arg1879z00_3135 =
																							CDR(BgL_pairz00_3136);
																					}
																					BgL_arg1878z00_3134 =
																						BGl_prefixz00zz__osz00
																						(BgL_arg1879z00_3135);
																				}
																				BgL_arg1876z00_3133 =
																					BGl_jvmzd2classzd2sanszd2directoryzd2zzread_jvmz00
																					(BgL_arg1878z00_3134);
																			}
																			BgL_arg1874z00_3132 =
																				BGl_stringzd2replacezd2zz__r4_strings_6_7z00
																				(BgL_arg1876z00_3133, FILE_SEPARATOR,
																				(char) (((unsigned char) '.')));
																		}
																		BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00
																			(BgL_namez00_3125, BgL_arg1874z00_3132,
																			BNIL);
																	}
																	{	/* BackEnd/jvm.scm 340 */
																		obj_t BgL_arg1880z00_3137;
																		obj_t BgL_arg1882z00_3138;
																		obj_t BgL_arg1883z00_3139;
																		obj_t BgL_arg1884z00_3140;
																		obj_t BgL_arg1885z00_3141;
																		obj_t BgL_arg1887z00_3142;

																		BgL_arg1880z00_3137 =
																			CDR(((obj_t) BgL_sourcesz00_3075));
																		{	/* BackEnd/jvm.scm 343 */
																			obj_t BgL_arg1888z00_3143;

																			{	/* BackEnd/jvm.scm 343 */
																				obj_t BgL_arg1889z00_3144;

																				{	/* BackEnd/jvm.scm 343 */
																					obj_t BgL_arg1892z00_3145;

																					{	/* BackEnd/jvm.scm 343 */
																						obj_t BgL_pairz00_3146;

																						BgL_pairz00_3146 =
																							CAR(
																							((obj_t) BgL_sourcesz00_3075));
																						BgL_arg1892z00_3145 =
																							CAR(BgL_pairz00_3146);
																					}
																					BgL_arg1889z00_3144 =
																						string_append_3
																						(BGl_string2024z00zzbackend_jvmz00,
																						BgL_arg1892z00_3145,
																						BGl_string2024z00zzbackend_jvmz00);
																				}
																				{	/* BackEnd/jvm.scm 341 */
																					obj_t BgL_list1890z00_3147;

																					{	/* BackEnd/jvm.scm 341 */
																						obj_t BgL_arg1891z00_3148;

																						BgL_arg1891z00_3148 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1889z00_3144, BNIL);
																						BgL_list1890z00_3147 =
																							MAKE_YOUNG_PAIR(BgL_namez00_3125,
																							BgL_arg1891z00_3148);
																					}
																					BgL_arg1888z00_3143 =
																						BgL_list1890z00_3147;
																			}}
																			BgL_arg1882z00_3138 =
																				MAKE_YOUNG_PAIR(BgL_arg1888z00_3143,
																				BgL_clsz00_3076);
																		}
																		if (CBOOL(BgL_nmainz00_3127))
																			{	/* BackEnd/jvm.scm 345 */
																				BgL_arg1883z00_3139 = BgL_namez00_3125;
																			}
																		else
																			{	/* BackEnd/jvm.scm 345 */
																				BgL_arg1883z00_3139 =
																					BgL_mainzd2modulezd2_3077;
																			}
																		if (CBOOL(BgL_nmainz00_3127))
																			{	/* BackEnd/jvm.scm 346 */
																				BgL_arg1884z00_3140 = BgL_nmainz00_3127;
																			}
																		else
																			{	/* BackEnd/jvm.scm 346 */
																				BgL_arg1884z00_3140 = BgL_mainz00_3078;
																			}
																		if (CBOOL(BgL_nmainz00_3127))
																			{	/* BackEnd/jvm.scm 347 */
																				obj_t BgL_pairz00_3149;

																				BgL_pairz00_3149 =
																					CAR(((obj_t) BgL_sourcesz00_3075));
																				BgL_arg1885z00_3141 =
																					CAR(BgL_pairz00_3149);
																			}
																		else
																			{	/* BackEnd/jvm.scm 347 */
																				BgL_arg1885z00_3141 = BgL_fmainz00_3079;
																			}
																		BgL_arg1887z00_3142 =
																			BGl_appendzd221011zd2zzbackend_jvmz00
																			(BgL_libsz00_3126, BgL_librariesz00_3080);
																		{
																			obj_t BgL_librariesz00_3745;
																			obj_t BgL_fmainz00_3744;
																			obj_t BgL_mainz00_3743;
																			obj_t BgL_mainzd2modulezd2_3742;
																			obj_t BgL_clsz00_3741;
																			obj_t BgL_sourcesz00_3740;

																			BgL_sourcesz00_3740 = BgL_arg1880z00_3137;
																			BgL_clsz00_3741 = BgL_arg1882z00_3138;
																			BgL_mainzd2modulezd2_3742 =
																				BgL_arg1883z00_3139;
																			BgL_mainz00_3743 = BgL_arg1884z00_3140;
																			BgL_fmainz00_3744 = BgL_arg1885z00_3141;
																			BgL_librariesz00_3745 =
																				BgL_arg1887z00_3142;
																			BgL_librariesz00_3080 =
																				BgL_librariesz00_3745;
																			BgL_fmainz00_3079 = BgL_fmainz00_3744;
																			BgL_mainz00_3078 = BgL_mainz00_3743;
																			BgL_mainzd2modulezd2_3077 =
																				BgL_mainzd2modulezd2_3742;
																			BgL_clsz00_3076 = BgL_clsz00_3741;
																			BgL_sourcesz00_3075 = BgL_sourcesz00_3740;
																			goto BgL_loopz00_3074;
																		}
																	}
																}
															}
														else
															{	/* BackEnd/jvm.scm 330 */
															BgL_tagzd2172zd2_3124:
																{	/* BackEnd/jvm.scm 352 */
																	obj_t BgL_arg1896z00_3150;

																	BgL_arg1896z00_3150 =
																		CDR(((obj_t) BgL_sourcesz00_3075));
																	{
																		obj_t BgL_sourcesz00_3749;

																		BgL_sourcesz00_3749 = BgL_arg1896z00_3150;
																		BgL_sourcesz00_3075 = BgL_sourcesz00_3749;
																		goto BgL_loopz00_3074;
																	}
																}
															}
													}
												else
													{	/* BackEnd/jvm.scm 330 */
														goto BgL_tagzd2172zd2_3124;
													}
											}
										else
											{	/* BackEnd/jvm.scm 330 */
												goto BgL_tagzd2172zd2_3124;
											}
									}
								}
							else
								{	/* BackEnd/jvm.scm 327 */
									obj_t BgL_arg1898z00_3152;

									{	/* BackEnd/jvm.scm 327 */
										obj_t BgL_pairz00_3153;

										BgL_pairz00_3153 = CAR(((obj_t) BgL_sourcesz00_3075));
										BgL_arg1898z00_3152 = CAR(BgL_pairz00_3153);
									}
									return
										BGl_errorz00zz__errorz00(BGl_string1996z00zzbackend_jvmz00,
										BGl_string2025z00zzbackend_jvmz00, BgL_arg1898z00_3152);
								}
						}
				}
		}

	}



/* &<@anonymous:1832> */
	bool_t BGl_z62zc3z04anonymousza31832ze3ze5zzbackend_jvmz00(obj_t
		BgL_envz00_2989, obj_t BgL_ezd2165zd2_2990)
	{
		{	/* BackEnd/jvm.scm 291 */
			{
				obj_t BgL_libsz00_3155;

				if (PAIRP(BgL_ezd2165zd2_2990))
					{	/* BackEnd/jvm.scm 291 */
						if ((CAR(BgL_ezd2165zd2_2990) == CNST_TABLE_REF(13)))
							{	/* BackEnd/jvm.scm 291 */
								BgL_libsz00_3155 = CDR(BgL_ezd2165zd2_2990);
								{
									obj_t BgL_l1424z00_3157;

									BgL_l1424z00_3157 = BgL_libsz00_3155;
								BgL_zc3z04anonymousza31838ze3z87_3156:
									if (PAIRP(BgL_l1424z00_3157))
										{	/* BackEnd/jvm.scm 293 */
											BGl_usezd2libraryz12zc0zzmodule_alibraryz00(CAR
												(BgL_l1424z00_3157));
											{
												obj_t BgL_l1424z00_3764;

												BgL_l1424z00_3764 = CDR(BgL_l1424z00_3157);
												BgL_l1424z00_3157 = BgL_l1424z00_3764;
												goto BgL_zc3z04anonymousza31838ze3z87_3156;
											}
										}
									else
										{	/* BackEnd/jvm.scm 293 */
											((bool_t) 1);
										}
								}
								{
									obj_t BgL_l1426z00_3159;

									BgL_l1426z00_3159 = BgL_libsz00_3155;
								BgL_zc3z04anonymousza31843ze3z87_3158:
									if (PAIRP(BgL_l1426z00_3159))
										{	/* BackEnd/jvm.scm 294 */
											BGl_addzd2evalzd2libraryz12z12zzmodule_evalz00(CAR
												(BgL_l1426z00_3159));
											{
												obj_t BgL_l1426z00_3770;

												BgL_l1426z00_3770 = CDR(BgL_l1426z00_3159);
												BgL_l1426z00_3159 = BgL_l1426z00_3770;
												goto BgL_zc3z04anonymousza31843ze3z87_3158;
											}
										}
									else
										{	/* BackEnd/jvm.scm 294 */
											return ((bool_t) 1);
										}
								}
							}
						else
							{	/* BackEnd/jvm.scm 291 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* BackEnd/jvm.scm 291 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &<@anonymous:1865> */
	obj_t BGl_z62zc3z04anonymousza31865ze3ze5zzbackend_jvmz00(obj_t
		BgL_envz00_2991)
	{
		{	/* BackEnd/jvm.scm 316 */
			{	/* BackEnd/jvm.scm 319 */
				obj_t BgL_tmpz00_2992;

				BgL_tmpz00_2992 = ((obj_t) PROCEDURE_REF(BgL_envz00_2991, (int) (0L)));
				{	/* BackEnd/jvm.scm 319 */
					bool_t BgL_tmpz00_3776;

					BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00();
					{	/* BackEnd/jvm.scm 320 */
						obj_t BgL_prez00_3160;

						BgL_prez00_3160 = BGl_prefixz00zz__osz00(BgL_tmpz00_2992);
						{	/* BackEnd/jvm.scm 320 */
							obj_t BgL_classzd2filezd2_3161;

							BgL_classzd2filezd2_3161 =
								string_append(BgL_prez00_3160,
								BGl_string2004z00zzbackend_jvmz00);
							{	/* BackEnd/jvm.scm 321 */

								if (fexists(BSTRING_TO_STRING(BgL_tmpz00_2992)))
									{	/* BackEnd/jvm.scm 323 */
										char *BgL_stringz00_3162;

										BgL_stringz00_3162 = BSTRING_TO_STRING(BgL_tmpz00_2992);
										if (unlink(BgL_stringz00_3162))
											{	/* BackEnd/jvm.scm 323 */
												BgL_tmpz00_3776 = ((bool_t) 0);
											}
										else
											{	/* BackEnd/jvm.scm 323 */
												BgL_tmpz00_3776 = ((bool_t) 1);
											}
									}
								else
									{	/* BackEnd/jvm.scm 322 */
										BgL_tmpz00_3776 = ((bool_t) 0);
									}
							}
						}
					}
					return BBOOL(BgL_tmpz00_3776);
				}
			}
		}

	}



/* &backend-cnst-table-n1438 */
	obj_t BGl_z62backendzd2cnstzd2tablezd2n1438zb0zzbackend_jvmz00(obj_t
		BgL_envz00_2993, obj_t BgL_mez00_2994, obj_t BgL_offsetz00_2995)
	{
		{	/* BackEnd/jvm.scm 238 */
			return BGl_string2026z00zzbackend_jvmz00;
		}

	}



/* &backend-link-jvm1436 */
	obj_t BGl_z62backendzd2linkzd2jvm1436z62zzbackend_jvmz00(obj_t
		BgL_envz00_2996, obj_t BgL_mez00_2997, obj_t BgL_resultz00_2998)
	{
		{	/* BackEnd/jvm.scm 231 */
			return BGl_jvmzd2ldzd2zzsaw_jvm_ldz00(BFALSE);
		}

	}



/* &backend-compile-jvm1434 */
	obj_t BGl_z62backendzd2compilezd2jvm1434z62zzbackend_jvmz00(obj_t
		BgL_envz00_2999, obj_t BgL_mez00_3000)
	{
		{	/* BackEnd/jvm.scm 66 */
			{	/* BackEnd/jvm.scm 68 */
				obj_t BgL_list1607z00_3166;

				{	/* BackEnd/jvm.scm 68 */
					obj_t BgL_arg1609z00_3167;

					{	/* BackEnd/jvm.scm 68 */
						obj_t BgL_arg1611z00_3168;

						BgL_arg1611z00_3168 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1609z00_3167 =
							MAKE_YOUNG_PAIR(BGl_string2027z00zzbackend_jvmz00,
							BgL_arg1611z00_3168);
					}
					BgL_list1607z00_3166 =
						MAKE_YOUNG_PAIR(BGl_string2028z00zzbackend_jvmz00,
						BgL_arg1609z00_3167);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1607z00_3166);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string2027z00zzbackend_jvmz00;
			{	/* BackEnd/jvm.scm 68 */
				obj_t BgL_g1115z00_3169;
				obj_t BgL_g1116z00_3170;

				{	/* BackEnd/jvm.scm 68 */
					obj_t BgL_list1629z00_3171;

					BgL_list1629z00_3171 =
						MAKE_YOUNG_PAIR
						(BGl_startzd2jvmzd2emissionz12zd2envzc0zzbackend_jvmz00, BNIL);
					BgL_g1115z00_3169 = BgL_list1629z00_3171;
				}
				BgL_g1116z00_3170 = CNST_TABLE_REF(16);
				{
					obj_t BgL_hooksz00_3173;
					obj_t BgL_hnamesz00_3174;

					BgL_hooksz00_3173 = BgL_g1115z00_3169;
					BgL_hnamesz00_3174 = BgL_g1116z00_3170;
				BgL_loopz00_3172:
					if (NULLP(BgL_hooksz00_3173))
						{	/* BackEnd/jvm.scm 68 */
							CNST_TABLE_REF(17);
						}
					else
						{	/* BackEnd/jvm.scm 68 */
							bool_t BgL_test2175z00_3800;

							{	/* BackEnd/jvm.scm 68 */
								obj_t BgL_fun1628z00_3175;

								BgL_fun1628z00_3175 = CAR(((obj_t) BgL_hooksz00_3173));
								BgL_test2175z00_3800 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1628z00_3175));
							}
							if (BgL_test2175z00_3800)
								{	/* BackEnd/jvm.scm 68 */
									obj_t BgL_arg1625z00_3176;
									obj_t BgL_arg1626z00_3177;

									BgL_arg1625z00_3176 = CDR(((obj_t) BgL_hooksz00_3173));
									BgL_arg1626z00_3177 = CDR(((obj_t) BgL_hnamesz00_3174));
									{
										obj_t BgL_hnamesz00_3812;
										obj_t BgL_hooksz00_3811;

										BgL_hooksz00_3811 = BgL_arg1625z00_3176;
										BgL_hnamesz00_3812 = BgL_arg1626z00_3177;
										BgL_hnamesz00_3174 = BgL_hnamesz00_3812;
										BgL_hooksz00_3173 = BgL_hooksz00_3811;
										goto BgL_loopz00_3172;
									}
								}
							else
								{	/* BackEnd/jvm.scm 68 */
									obj_t BgL_arg1627z00_3178;

									BgL_arg1627z00_3178 = CAR(((obj_t) BgL_hnamesz00_3174));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2027z00zzbackend_jvmz00,
										BGl_string2029z00zzbackend_jvmz00, BgL_arg1627z00_3178);
								}
						}
				}
			}
			{	/* BackEnd/jvm.scm 70 */
				obj_t BgL_arg1630z00_3179;

				BgL_arg1630z00_3179 =
					BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00
					(BGl_za2moduleza2z00zzmodule_modulez00);
				{	/* BackEnd/jvm.scm 69 */
					obj_t BgL_list1631z00_3180;

					{	/* BackEnd/jvm.scm 69 */
						obj_t BgL_arg1642z00_3181;

						{	/* BackEnd/jvm.scm 69 */
							obj_t BgL_arg1646z00_3182;

							{	/* BackEnd/jvm.scm 69 */
								obj_t BgL_arg1650z00_3183;

								{	/* BackEnd/jvm.scm 69 */
									obj_t BgL_arg1651z00_3184;

									{	/* BackEnd/jvm.scm 69 */
										obj_t BgL_arg1654z00_3185;

										BgL_arg1654z00_3185 =
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
										BgL_arg1651z00_3184 =
											MAKE_YOUNG_PAIR(BGl_string2030z00zzbackend_jvmz00,
											BgL_arg1654z00_3185);
									}
									BgL_arg1650z00_3183 =
										MAKE_YOUNG_PAIR(BgL_arg1630z00_3179, BgL_arg1651z00_3184);
								}
								BgL_arg1646z00_3182 =
									MAKE_YOUNG_PAIR(BGl_string2031z00zzbackend_jvmz00,
									BgL_arg1650z00_3183);
							}
							BgL_arg1642z00_3181 =
								MAKE_YOUNG_PAIR(BGl_za2moduleza2z00zzmodule_modulez00,
								BgL_arg1646z00_3182);
						}
						BgL_list1631z00_3180 =
							MAKE_YOUNG_PAIR(BGl_string2032z00zzbackend_jvmz00,
							BgL_arg1642z00_3181);
					}
					BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1631z00_3180);
			}}
			((((BgL_jvmz00_bglt) COBJECT(
							((BgL_jvmz00_bglt) BgL_mez00_3000)))->BgL_qnamez00) = ((obj_t)
					bstring_to_symbol(BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00
						(BGl_za2moduleza2z00zzmodule_modulez00))), BUNSPEC);
			{	/* BackEnd/jvm.scm 75 */
				bool_t BgL_test2176z00_3830;

				if (CBOOL(BGl_za2mainza2z00zzmodule_modulez00))
					{	/* BackEnd/jvm.scm 75 */
						BgL_test2176z00_3830 = ((bool_t) 0);
					}
				else
					{	/* BackEnd/jvm.scm 75 */
						if (CBOOL(BGl_za2autozd2linkzd2mainza2z00zzengine_paramz00))
							{	/* BackEnd/jvm.scm 75 */
								BgL_test2176z00_3830 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
									(BGl_za2passza2z00zzengine_paramz00, CNST_TABLE_REF(18)));
							}
						else
							{	/* BackEnd/jvm.scm 75 */
								BgL_test2176z00_3830 = ((bool_t) 0);
							}
					}
				if (BgL_test2176z00_3830)
					{	/* BackEnd/jvm.scm 75 */
						BGl_za2mainza2z00zzmodule_modulez00 =
							BGl_makezd2bigloozd2mainz00zzbackend_c_mainz00();
					}
				else
					{	/* BackEnd/jvm.scm 75 */
						BFALSE;
					}
			}
			BGl_jvmzd2checkzd2packagez00zzbackend_jvmz00
				(BGl_za2moduleza2z00zzmodule_modulez00,
				BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00);
			{	/* BackEnd/jvm.scm 103 */
				obj_t BgL_lza2za2_3186;
				obj_t BgL_bnamez00_3187;

				BgL_lza2za2_3186 =
					BGl_saw_jvmzd2compilezd2zzsaw_jvm_compilez00(
					((BgL_jvmz00_bglt) BgL_mez00_3000));
				if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(10)))
					{	/* BackEnd/jvm.scm 106 */
						bool_t BgL_test2180z00_3845;

						{	/* BackEnd/jvm.scm 106 */
							obj_t BgL_tmpz00_3846;

							BgL_tmpz00_3846 =
								(((BgL_jvmz00_bglt) COBJECT(
										((BgL_jvmz00_bglt) BgL_mez00_3000)))->BgL_qnamez00);
							BgL_test2180z00_3845 = SYMBOLP(BgL_tmpz00_3846);
						}
						if (BgL_test2180z00_3845)
							{	/* BackEnd/jvm.scm 107 */
								obj_t BgL_arg1703z00_3188;

								{	/* BackEnd/jvm.scm 107 */
									obj_t BgL_arg1705z00_3189;

									{	/* BackEnd/jvm.scm 107 */
										obj_t BgL_arg1708z00_3190;

										{	/* BackEnd/jvm.scm 107 */
											obj_t BgL_arg1709z00_3191;

											BgL_arg1709z00_3191 =
												(((BgL_jvmz00_bglt) COBJECT(
														((BgL_jvmz00_bglt) BgL_mez00_3000)))->BgL_qnamez00);
											{	/* BackEnd/jvm.scm 107 */
												obj_t BgL_arg1455z00_3192;

												BgL_arg1455z00_3192 =
													SYMBOL_TO_STRING(((obj_t) BgL_arg1709z00_3191));
												BgL_arg1708z00_3190 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_3192);
											}
										}
										BgL_arg1705z00_3189 =
											BGl_basenamez00zz__osz00(BgL_arg1708z00_3190);
									}
									BgL_arg1703z00_3188 =
										BGl_prefixz00zz__osz00(BgL_arg1705z00_3189);
								}
								BgL_bnamez00_3187 =
									BGl_addsuffixz00zzbackend_jvmz00(BgL_arg1703z00_3188);
							}
						else
							{	/* BackEnd/jvm.scm 106 */
								BgL_bnamez00_3187 = BGl_string2033z00zzbackend_jvmz00;
							}
					}
				else
					{	/* BackEnd/jvm.scm 105 */
						if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
							{	/* BackEnd/jvm.scm 109 */
								BgL_bnamez00_3187 =
									BGl_addsuffixz00zzbackend_jvmz00(BGl_prefixz00zz__osz00
									(BGl_basenamez00zz__osz00
										(BGl_za2destza2z00zzengine_paramz00)));
							}
						else
							{	/* BackEnd/jvm.scm 110 */
								bool_t BgL_test2182z00_3863;

								{	/* BackEnd/jvm.scm 110 */
									obj_t BgL_tmpz00_3864;

									BgL_tmpz00_3864 =
										(((BgL_jvmz00_bglt) COBJECT(
												((BgL_jvmz00_bglt) BgL_mez00_3000)))->BgL_qnamez00);
									BgL_test2182z00_3863 = SYMBOLP(BgL_tmpz00_3864);
								}
								if (BgL_test2182z00_3863)
									{	/* BackEnd/jvm.scm 111 */
										obj_t BgL_arg1720z00_3193;

										{	/* BackEnd/jvm.scm 111 */
											obj_t BgL_arg1722z00_3194;

											{	/* BackEnd/jvm.scm 111 */
												obj_t BgL_arg1724z00_3195;

												{	/* BackEnd/jvm.scm 111 */
													obj_t BgL_arg1733z00_3196;

													BgL_arg1733z00_3196 =
														(((BgL_jvmz00_bglt) COBJECT(
																((BgL_jvmz00_bglt) BgL_mez00_3000)))->
														BgL_qnamez00);
													{	/* BackEnd/jvm.scm 111 */
														obj_t BgL_arg1455z00_3197;

														BgL_arg1455z00_3197 =
															SYMBOL_TO_STRING(((obj_t) BgL_arg1733z00_3196));
														BgL_arg1724z00_3195 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_3197);
													}
												}
												BgL_arg1722z00_3194 =
													BGl_basenamez00zz__osz00(BgL_arg1724z00_3195);
											}
											BgL_arg1720z00_3193 =
												BGl_prefixz00zz__osz00(BgL_arg1722z00_3194);
										}
										BgL_bnamez00_3187 =
											BGl_addsuffixz00zzbackend_jvmz00(BgL_arg1720z00_3193);
									}
								else
									{	/* BackEnd/jvm.scm 110 */
										BgL_bnamez00_3187 = BFALSE;
									}
							}
					}
				{	/* BackEnd/jvm.scm 115 */
					obj_t BgL_arg1675z00_3198;

					BgL_arg1675z00_3198 = CAR(((obj_t) BgL_lza2za2_3186));
					BGl_emitze70ze7zzbackend_jvmz00(BgL_arg1675z00_3198,
						BgL_bnamez00_3187);
				}
				{	/* BackEnd/jvm.scm 116 */
					obj_t BgL_g1421z00_3199;

					BgL_g1421z00_3199 = CDR(((obj_t) BgL_lza2za2_3186));
					{
						obj_t BgL_l1419z00_3201;

						BgL_l1419z00_3201 = BgL_g1421z00_3199;
					BgL_zc3z04anonymousza31676ze3z87_3200:
						if (PAIRP(BgL_l1419z00_3201))
							{	/* BackEnd/jvm.scm 117 */
								{	/* BackEnd/jvm.scm 116 */
									obj_t BgL_cfz00_3202;

									BgL_cfz00_3202 = CAR(BgL_l1419z00_3201);
									BGl_emitze70ze7zzbackend_jvmz00(BgL_cfz00_3202,
										BGl_jasnamez00zzbackend_jvmz00(BgL_cfz00_3202));
								}
								{
									obj_t BgL_l1419z00_3886;

									BgL_l1419z00_3886 = CDR(BgL_l1419z00_3201);
									BgL_l1419z00_3201 = BgL_l1419z00_3886;
									goto BgL_zc3z04anonymousza31676ze3z87_3200;
								}
							}
						else
							{	/* BackEnd/jvm.scm 117 */
								((bool_t) 1);
							}
					}
				}
				BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(19),
					BGl_proc2034z00zzbackend_jvmz00);
				BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(8),
					BGl_proc2035z00zzbackend_jvmz00);
				return BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(7),
					BGl_proc2036z00zzbackend_jvmz00);
			}
		}

	}



/* <@exit:1750>~0 */
	obj_t BGl_zc3z04exitza31750ze3ze70z60zzbackend_jvmz00(obj_t
		BgL_classfilez00_3026, obj_t BgL_portz00_3025, obj_t BgL_cell1120z00_3024,
		obj_t BgL_env1125z00_3023)
	{
		{	/* BackEnd/jvm.scm 95 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1133z00_2134;

			if (SET_EXIT(BgL_an_exit1133z00_2134))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1133z00_2134 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1125z00_3023, BgL_an_exit1133z00_2134, 1L);
					{	/* BackEnd/jvm.scm 95 */
						obj_t BgL_escape1121z00_2135;

						BgL_escape1121z00_2135 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1125z00_3023);
						{	/* BackEnd/jvm.scm 95 */
							obj_t BgL_res1136z00_2136;

							{	/* BackEnd/jvm.scm 95 */
								obj_t BgL_ohs1117z00_2137;

								BgL_ohs1117z00_2137 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1125z00_3023);
								{	/* BackEnd/jvm.scm 95 */
									obj_t BgL_hds1118z00_2138;

									BgL_hds1118z00_2138 =
										MAKE_STACK_PAIR(BgL_escape1121z00_2135,
										BgL_cell1120z00_3024);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1125z00_3023,
										BgL_hds1118z00_2138);
									BUNSPEC;
									{	/* BackEnd/jvm.scm 95 */
										obj_t BgL_exitd1126z00_2139;

										BgL_exitd1126z00_2139 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1125z00_3023);
										{	/* BackEnd/jvm.scm 95 */
											obj_t BgL_tmp1128z00_2140;

											{	/* BackEnd/jvm.scm 95 */
												obj_t BgL_arg1752z00_2147;

												BgL_arg1752z00_2147 =
													BGL_EXITD_PROTECT(BgL_exitd1126z00_2139);
												BgL_tmp1128z00_2140 =
													MAKE_YOUNG_PAIR(BgL_ohs1117z00_2137,
													BgL_arg1752z00_2147);
											}
											{	/* BackEnd/jvm.scm 95 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1126z00_2139,
													BgL_tmp1128z00_2140);
												BUNSPEC;
												{	/* BackEnd/jvm.scm 99 */
													obj_t BgL_tmp1127z00_2141;

													{	/* BackEnd/jvm.scm 99 */
														obj_t BgL_exitd1129z00_2142;

														BgL_exitd1129z00_2142 = BGL_EXITD_TOP_AS_OBJ();
														{	/* BackEnd/jvm.scm 101 */
															obj_t BgL_zc3z04anonymousza31751ze3z87_2978;

															BgL_zc3z04anonymousza31751ze3z87_2978 =
																MAKE_FX_PROCEDURE
																(BGl_z62zc3z04anonymousza31751ze3ze5zzbackend_jvmz00,
																(int) (0L), (int) (1L));
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31751ze3z87_2978,
																(int) (0L), BgL_portz00_3025);
															{	/* BackEnd/jvm.scm 99 */
																obj_t BgL_arg1828z00_2778;

																{	/* BackEnd/jvm.scm 99 */
																	obj_t BgL_arg1829z00_2779;

																	BgL_arg1829z00_2779 =
																		BGL_EXITD_PROTECT(BgL_exitd1129z00_2142);
																	BgL_arg1828z00_2778 =
																		MAKE_YOUNG_PAIR
																		(BgL_zc3z04anonymousza31751ze3z87_2978,
																		BgL_arg1829z00_2779);
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1129z00_2142,
																	BgL_arg1828z00_2778);
																BUNSPEC;
															}
															{	/* BackEnd/jvm.scm 100 */
																obj_t BgL_tmp1131z00_2144;

																BgL_tmp1131z00_2144 =
																	BGl_jvmzd2aszd2zzjas_asz00
																	(BgL_classfilez00_3026, BgL_portz00_3025);
																{	/* BackEnd/jvm.scm 99 */
																	bool_t BgL_test2184z00_3915;

																	{	/* BackEnd/jvm.scm 99 */
																		obj_t BgL_arg1827z00_2781;

																		BgL_arg1827z00_2781 =
																			BGL_EXITD_PROTECT(BgL_exitd1129z00_2142);
																		BgL_test2184z00_3915 =
																			PAIRP(BgL_arg1827z00_2781);
																	}
																	if (BgL_test2184z00_3915)
																		{	/* BackEnd/jvm.scm 99 */
																			obj_t BgL_arg1825z00_2782;

																			{	/* BackEnd/jvm.scm 99 */
																				obj_t BgL_arg1826z00_2783;

																				BgL_arg1826z00_2783 =
																					BGL_EXITD_PROTECT
																					(BgL_exitd1129z00_2142);
																				BgL_arg1825z00_2782 =
																					CDR(((obj_t) BgL_arg1826z00_2783));
																			}
																			BGL_EXITD_PROTECT_SET
																				(BgL_exitd1129z00_2142,
																				BgL_arg1825z00_2782);
																			BUNSPEC;
																		}
																	else
																		{	/* BackEnd/jvm.scm 99 */
																			BFALSE;
																		}
																}
																close_binary_port(((obj_t) BgL_portz00_3025));
																BgL_tmp1127z00_2141 = BgL_tmp1131z00_2144;
															}
														}
													}
													{	/* BackEnd/jvm.scm 95 */
														bool_t BgL_test2185z00_3924;

														{	/* BackEnd/jvm.scm 95 */
															obj_t BgL_arg1827z00_2787;

															BgL_arg1827z00_2787 =
																BGL_EXITD_PROTECT(BgL_exitd1126z00_2139);
															BgL_test2185z00_3924 = PAIRP(BgL_arg1827z00_2787);
														}
														if (BgL_test2185z00_3924)
															{	/* BackEnd/jvm.scm 95 */
																obj_t BgL_arg1825z00_2788;

																{	/* BackEnd/jvm.scm 95 */
																	obj_t BgL_arg1826z00_2789;

																	BgL_arg1826z00_2789 =
																		BGL_EXITD_PROTECT(BgL_exitd1126z00_2139);
																	BgL_arg1825z00_2788 =
																		CDR(((obj_t) BgL_arg1826z00_2789));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1126z00_2139,
																	BgL_arg1825z00_2788);
																BUNSPEC;
															}
														else
															{	/* BackEnd/jvm.scm 95 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1125z00_3023,
														BgL_ohs1117z00_2137);
													BUNSPEC;
													BgL_res1136z00_2136 = BgL_tmp1127z00_2141;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1125z00_3023);
							return BgL_res1136z00_2136;
						}
					}
				}
		}

	}



/* emit~0 */
	obj_t BGl_emitze70ze7zzbackend_jvmz00(obj_t BgL_classfilez00_2110,
		obj_t BgL_destz00_2111)
	{
		{	/* BackEnd/jvm.scm 101 */
			{	/* BackEnd/jvm.scm 79 */
				obj_t BgL_dirz00_2113;

				BgL_dirz00_2113 = BGl_za2jvmzd2dirzd2nameza2z00zzbackend_jvmz00;
				if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(8)))
					{	/* BackEnd/jvm.scm 81 */
						obj_t BgL_portz00_2114;

						if (STRINGP(BgL_destz00_2111))
							{	/* BackEnd/jvm.scm 84 */
								obj_t BgL_arg1740z00_2119;

								BgL_arg1740z00_2119 =
									string_append_3(BgL_dirz00_2113,
									BGl_string2037z00zzbackend_jvmz00, BgL_destz00_2111);
								{	/* BackEnd/jvm.scm 83 */

									BgL_portz00_2114 =
										BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00
										(BgL_arg1740z00_2119, BTRUE);
								}
							}
						else
							{	/* BackEnd/jvm.scm 82 */
								obj_t BgL_tmpz00_3940;

								BgL_tmpz00_3940 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_portz00_2114 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3940);
							}
						BGl_jvmasdumpz00zzbackend_jvmz00(BgL_classfilez00_2110,
							BgL_portz00_2114);
						{	/* BackEnd/jvm.scm 86 */
							bool_t BgL_test2188z00_3944;

							{	/* BackEnd/jvm.scm 86 */
								obj_t BgL_arg1738z00_2117;

								{	/* BackEnd/jvm.scm 86 */
									obj_t BgL_tmpz00_3945;

									BgL_tmpz00_3945 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1738z00_2117 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3945);
								}
								BgL_test2188z00_3944 =
									(BgL_portz00_2114 == BgL_arg1738z00_2117);
							}
							if (BgL_test2188z00_3944)
								{	/* BackEnd/jvm.scm 86 */
									return BFALSE;
								}
							else
								{	/* BackEnd/jvm.scm 86 */
									return bgl_close_output_port(((obj_t) BgL_portz00_2114));
								}
						}
					}
				else
					{	/* BackEnd/jvm.scm 88 */
						obj_t BgL_cnamez00_2122;

						if (STRINGP(BgL_destz00_2111))
							{	/* BackEnd/jvm.scm 88 */
								BgL_cnamez00_2122 =
									string_append_3(BgL_dirz00_2113,
									BGl_string2037z00zzbackend_jvmz00, BgL_destz00_2111);
							}
						else
							{	/* BackEnd/jvm.scm 88 */
								BgL_cnamez00_2122 =
									string_append(BgL_dirz00_2113,
									BGl_string2038z00zzbackend_jvmz00);
							}
						{	/* BackEnd/jvm.scm 88 */
							obj_t BgL_portz00_2123;

							BgL_portz00_2123 =
								BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00
								(BgL_cnamez00_2122);
							{	/* BackEnd/jvm.scm 91 */

								{	/* BackEnd/jvm.scm 92 */
									obj_t BgL_list1741z00_2124;

									{	/* BackEnd/jvm.scm 92 */
										obj_t BgL_arg1746z00_2125;

										{	/* BackEnd/jvm.scm 92 */
											obj_t BgL_arg1747z00_2126;

											BgL_arg1747z00_2126 =
												MAKE_YOUNG_PAIR(BGl_string2039z00zzbackend_jvmz00,
												BNIL);
											BgL_arg1746z00_2125 =
												MAKE_YOUNG_PAIR(BgL_cnamez00_2122, BgL_arg1747z00_2126);
										}
										BgL_list1741z00_2124 =
											MAKE_YOUNG_PAIR(BGl_string2040z00zzbackend_jvmz00,
											BgL_arg1746z00_2125);
									}
									BGl_verbosez00zztools_speekz00(BINT(2L),
										BgL_list1741z00_2124);
								}
								if (BINARY_PORTP(BgL_portz00_2123))
									{	/* BackEnd/jvm.scm 93 */
										BFALSE;
									}
								else
									{	/* BackEnd/jvm.scm 93 */
										BGl_errorz00zz__errorz00(BGl_string2041z00zzbackend_jvmz00,
											BGl_string2042z00zzbackend_jvmz00, BgL_cnamez00_2122);
									}
								{	/* BackEnd/jvm.scm 95 */
									obj_t BgL_env1125z00_2129;

									BgL_env1125z00_2129 = BGL_CURRENT_DYNAMIC_ENV();
									{	/* BackEnd/jvm.scm 95 */
										obj_t BgL_cell1120z00_2130;

										BgL_cell1120z00_2130 = MAKE_STACK_CELL(BUNSPEC);
										{	/* BackEnd/jvm.scm 95 */
											obj_t BgL_val1124z00_2131;

											BgL_val1124z00_2131 =
												BGl_zc3z04exitza31750ze3ze70z60zzbackend_jvmz00
												(BgL_classfilez00_2110, BgL_portz00_2123,
												BgL_cell1120z00_2130, BgL_env1125z00_2129);
											if ((BgL_val1124z00_2131 == BgL_cell1120z00_2130))
												{	/* BackEnd/jvm.scm 95 */
													{	/* BackEnd/jvm.scm 95 */
														int BgL_tmpz00_3969;

														BgL_tmpz00_3969 = (int) (0L);
														BGL_SIGSETMASK(BgL_tmpz00_3969);
													}
													{	/* BackEnd/jvm.scm 95 */
														obj_t BgL_arg1749z00_2132;

														BgL_arg1749z00_2132 =
															CELL_REF(((obj_t) BgL_val1124z00_2131));
														{	/* BackEnd/jvm.scm 97 */
															char *BgL_stringz00_2792;

															BgL_stringz00_2792 =
																BSTRING_TO_STRING(BgL_cnamez00_2122);
															if (unlink(BgL_stringz00_2792))
																{	/* BackEnd/jvm.scm 97 */
																	((bool_t) 0);
																}
															else
																{	/* BackEnd/jvm.scm 97 */
																	((bool_t) 1);
																}
														}
														return
															BGl_raisez00zz__errorz00(BgL_arg1749z00_2132);
													}
												}
											else
												{	/* BackEnd/jvm.scm 95 */
													return BgL_val1124z00_2131;
												}
										}
									}
								}
							}
						}
					}
			}
		}

	}



/* &<@anonymous:1700> */
	obj_t BGl_z62zc3z04anonymousza31700ze3ze5zzbackend_jvmz00(obj_t
		BgL_envz00_3001)
	{
		{	/* BackEnd/jvm.scm 120 */
			return CNST_TABLE_REF(20);
		}

	}



/* &<@anonymous:1692> */
	obj_t BGl_z62zc3z04anonymousza31692ze3ze5zzbackend_jvmz00(obj_t
		BgL_envz00_3002)
	{
		{	/* BackEnd/jvm.scm 119 */
			return CNST_TABLE_REF(20);
		}

	}



/* &<@anonymous:1689> */
	obj_t BGl_z62zc3z04anonymousza31689ze3ze5zzbackend_jvmz00(obj_t
		BgL_envz00_3003)
	{
		{	/* BackEnd/jvm.scm 118 */
			return CNST_TABLE_REF(20);
		}

	}



/* &<@anonymous:1751> */
	obj_t BGl_z62zc3z04anonymousza31751ze3ze5zzbackend_jvmz00(obj_t
		BgL_envz00_3004)
	{
		{	/* BackEnd/jvm.scm 99 */
			{	/* BackEnd/jvm.scm 101 */
				obj_t BgL_portz00_3005;

				BgL_portz00_3005 = PROCEDURE_REF(BgL_envz00_3004, (int) (0L));
				return close_binary_port(((obj_t) BgL_portz00_3005));
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_jvmz00(void)
	{
		{	/* BackEnd/jvm.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzengine_linkz00(117219619L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzengine_compilerz00(412406770L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(316727058L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzmodule_evalz00(428236825L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzbackend_bvmz00(336068337L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(0L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_mainz00(65542503L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzjas_asz00(476839586L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzjas_peepz00(121595967L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_compilez00(100353792L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_ldz00(14468792L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_inlinez00(252555369L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			BGl_modulezd2initializa7ationz75zzinit_setrcz00(32737986L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
			return
				BGl_modulezd2initializa7ationz75zzread_readerz00(95801752L,
				BSTRING_TO_STRING(BGl_string2043z00zzbackend_jvmz00));
		}

	}

#ifdef __cplusplus
}
#endif
