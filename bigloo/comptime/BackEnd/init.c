/*===========================================================================*/
/*   (BackEnd/init.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/init.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_INIT_TYPE_DEFINITIONS
#define BGL_BACKEND_INIT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_BACKEND_INIT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbackend_initz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzbackend_initz00(void);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static BgL_globalz00_bglt BGl_makezd2modulezd2initz00zzbackend_initz00(void);
	extern obj_t BGl_occurzd2nodezd2inz12z12zzast_occurz00(BgL_nodez00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_bglt);
	static obj_t BGl_genericzd2initzd2zzbackend_initz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzbackend_initz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern obj_t BGl_za2debugzd2moduleza2zd2zzengine_paramz00;
	extern obj_t BGl_za2stringza2z00zztype_cachez00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_initz00(void);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern BgL_nodez00_bglt BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t, BgL_typez00_bglt, bool_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzbackend_initz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_unitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_za2dlopenzd2initzd2gcza2z00zzengine_paramz00;
	extern obj_t BGl_unitzd2initzd2callsz00zzast_unitz00(void);
	BGL_EXPORTED_DECL obj_t BGl_getzd2modulezd2initz00zzbackend_initz00(void);
	static obj_t BGl_z62getzd2modulezd2initz62zzbackend_initz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzbackend_initz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_initz00(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_initz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzbackend_initz00(void);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2modulezd2checksumza2zd2zzmodule_modulez00;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t
		BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_variablez00_bglt);
	extern obj_t BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00(obj_t);
	extern obj_t
		BGl_backendzd2gczd2initz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_za2modulezd2initza2zd2zzbackend_initz00 = BUNSPEC;
	extern obj_t BGl_za2unsafezd2versionza2zd2zzengine_paramz00;
	static obj_t __cnst[24];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2modulezd2initzd2envzd2zzbackend_initz00,
		BgL_bgl_za762getza7d2moduleza71750za7,
		BGl_z62getzd2modulezd2initz62zzbackend_initz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1743z00zzbackend_initz00,
		BgL_bgl_string1743za700za7za7b1751za7, "bgl_init_module_debug_start(\"",
		29);
	      DEFINE_STRING(BGl_string1744z00zzbackend_initz00,
		BgL_bgl_string1744za700za7za7b1752za7, "\")", 2);
	      DEFINE_STRING(BGl_string1745z00zzbackend_initz00,
		BgL_bgl_string1745za700za7za7b1753za7, "bgl_init_module_debug_end(\"", 27);
	      DEFINE_STRING(BGl_string1746z00zzbackend_initz00,
		BgL_bgl_string1746za700za7za7b1754za7, "~s", 2);
	      DEFINE_STRING(BGl_string1747z00zzbackend_initz00,
		BgL_bgl_string1747za700za7za7b1755za7, "backend_init", 12);
	      DEFINE_STRING(BGl_string1748z00zzbackend_initz00,
		BgL_bgl_string1748za700za7za7b1756za7,
		"export module-initialization sfun (checksum from) value module-init-error s from s::string pragma::string =fx bit-and checksum module now module-initalization require-initialization::obj let pragma tmp if begin set! require-initialization ",
		239);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzbackend_initz00));
		     ADD_ROOT((void *) (&BGl_za2modulezd2initza2zd2zzbackend_initz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzbackend_initz00(long
		BgL_checksumz00_2029, char *BgL_fromz00_2030)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_initz00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_initz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_initz00();
					BGl_libraryzd2moduleszd2initz00zzbackend_initz00();
					BGl_cnstzd2initzd2zzbackend_initz00();
					BGl_importedzd2moduleszd2initz00zzbackend_initz00();
					return BGl_toplevelzd2initzd2zzbackend_initz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"backend_init");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"backend_init");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "backend_init");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_init");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"backend_init");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "backend_init");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_init");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 15 */
			{	/* BackEnd/init.scm 15 */
				obj_t BgL_cportz00_2018;

				{	/* BackEnd/init.scm 15 */
					obj_t BgL_stringz00_2025;

					BgL_stringz00_2025 = BGl_string1748z00zzbackend_initz00;
					{	/* BackEnd/init.scm 15 */
						obj_t BgL_startz00_2026;

						BgL_startz00_2026 = BINT(0L);
						{	/* BackEnd/init.scm 15 */
							obj_t BgL_endz00_2027;

							BgL_endz00_2027 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2025)));
							{	/* BackEnd/init.scm 15 */

								BgL_cportz00_2018 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2025, BgL_startz00_2026, BgL_endz00_2027);
				}}}}
				{
					long BgL_iz00_2019;

					BgL_iz00_2019 = 23L;
				BgL_loopz00_2020:
					if ((BgL_iz00_2019 == -1L))
						{	/* BackEnd/init.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/init.scm 15 */
							{	/* BackEnd/init.scm 15 */
								obj_t BgL_arg1749z00_2021;

								{	/* BackEnd/init.scm 15 */

									{	/* BackEnd/init.scm 15 */
										obj_t BgL_locationz00_2023;

										BgL_locationz00_2023 = BBOOL(((bool_t) 0));
										{	/* BackEnd/init.scm 15 */

											BgL_arg1749z00_2021 =
												BGl_readz00zz__readerz00(BgL_cportz00_2018,
												BgL_locationz00_2023);
										}
									}
								}
								{	/* BackEnd/init.scm 15 */
									int BgL_tmpz00_2055;

									BgL_tmpz00_2055 = (int) (BgL_iz00_2019);
									CNST_TABLE_SET(BgL_tmpz00_2055, BgL_arg1749z00_2021);
							}}
							{	/* BackEnd/init.scm 15 */
								int BgL_auxz00_2024;

								BgL_auxz00_2024 = (int) ((BgL_iz00_2019 - 1L));
								{
									long BgL_iz00_2060;

									BgL_iz00_2060 = (long) (BgL_auxz00_2024);
									BgL_iz00_2019 = BgL_iz00_2060;
									goto BgL_loopz00_2020;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 15 */
			return (BGl_za2modulezd2initza2zd2zzbackend_initz00 = BUNSPEC, BUNSPEC);
		}

	}



/* get-module-init */
	BGL_EXPORTED_DEF obj_t BGl_getzd2modulezd2initz00zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 45 */
			if ((BGl_za2modulezd2initza2zd2zzbackend_initz00 == BUNSPEC))
				{	/* BackEnd/init.scm 46 */
					BGl_za2modulezd2initza2zd2zzbackend_initz00 =
						((obj_t) BGl_makezd2modulezd2initz00zzbackend_initz00());
				}
			else
				{	/* BackEnd/init.scm 46 */
					BFALSE;
				}
			return BGl_za2modulezd2initza2zd2zzbackend_initz00;
		}

	}



/* &get-module-init */
	obj_t BGl_z62getzd2modulezd2initz62zzbackend_initz00(obj_t BgL_envz00_2017)
	{
		{	/* BackEnd/init.scm 45 */
			return BGl_getzd2modulezd2initz00zzbackend_initz00();
		}

	}



/* make-module-init */
	BgL_globalz00_bglt BGl_makezd2modulezd2initz00zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 53 */
			{

				{	/* BackEnd/init.scm 81 */
					BgL_globalz00_bglt BgL_reqz00_1718;

					BgL_reqz00_1718 =
						BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2(CNST_TABLE_REF(7),
						BGl_za2moduleza2z00zzmodule_modulez00, CNST_TABLE_REF(8),
						CNST_TABLE_REF(9));
					{	/* BackEnd/init.scm 81 */
						obj_t BgL_bcz00_1719;

						BgL_bcz00_1719 = BGl_thezd2backendzd2zzbackend_backendz00();
						{	/* BackEnd/init.scm 85 */
							bool_t BgL_dbgz00_1720;

							if (
								((long) CINT(BGl_za2debugzd2moduleza2zd2zzengine_paramz00) >
									0L))
								{	/* BackEnd/init.scm 86 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(CNST_TABLE_REF(10),
												(((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
																BgL_bcz00_1719)))->BgL_debugzd2supportzd2))))
										{	/* BackEnd/init.scm 87 */
											BgL_dbgz00_1720 =
												(((BgL_backendz00_bglt) COBJECT(
														((BgL_backendz00_bglt) BgL_bcz00_1719)))->
												BgL_pragmazd2supportzd2);
										}
									else
										{	/* BackEnd/init.scm 87 */
											BgL_dbgz00_1720 = ((bool_t) 0);
										}
								}
							else
								{	/* BackEnd/init.scm 86 */
									BgL_dbgz00_1720 = ((bool_t) 0);
								}
							{	/* BackEnd/init.scm 86 */
								obj_t BgL_ubodyz00_1721;

								if (BgL_dbgz00_1720)
									{	/* BackEnd/init.scm 89 */
										{	/* BackEnd/init.scm 64 */
											obj_t BgL_tmpz00_1782;

											BgL_tmpz00_1782 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(4));
											{	/* BackEnd/init.scm 67 */
												obj_t BgL_arg1379z00_1783;

												{	/* BackEnd/init.scm 67 */
													obj_t BgL_arg1380z00_1784;

													{	/* BackEnd/init.scm 67 */
														obj_t BgL_arg1408z00_1785;
														obj_t BgL_arg1410z00_1786;

														{	/* BackEnd/init.scm 67 */
															obj_t BgL_arg1421z00_1787;

															{	/* BackEnd/init.scm 67 */
																obj_t BgL_arg1422z00_1788;
																obj_t BgL_arg1434z00_1789;

																{	/* BackEnd/init.scm 67 */
																	obj_t BgL_arg1437z00_1790;

																	{	/* BackEnd/init.scm 67 */
																		obj_t BgL_arg1448z00_1791;

																		BgL_arg1448z00_1791 =
																			MAKE_YOUNG_PAIR(BFALSE, BNIL);
																		BgL_arg1437z00_1790 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																			BgL_arg1448z00_1791);
																	}
																	BgL_arg1422z00_1788 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																		BgL_arg1437z00_1790);
																}
																{	/* BackEnd/init.scm 68 */
																	obj_t BgL_arg1453z00_1792;
																	obj_t BgL_arg1454z00_1793;

																	if (CBOOL
																		(BGl_za2dlopenzd2initzd2gcza2z00zzengine_paramz00))
																		{	/* BackEnd/init.scm 68 */
																			obj_t BgL_arg1472z00_1794;

																			BgL_arg1472z00_1794 =
																				BGl_thezd2backendzd2zzbackend_backendz00
																				();
																			BgL_arg1453z00_1792 =
																				BGl_backendzd2gczd2initz00zzbackend_backendz00
																				(((BgL_backendz00_bglt)
																					BgL_arg1472z00_1794));
																		}
																	else
																		{	/* BackEnd/init.scm 68 */
																			BgL_arg1453z00_1792 = BFALSE;
																		}
																	{	/* BackEnd/init.scm 71 */
																		obj_t BgL_arg1473z00_1795;
																		obj_t BgL_arg1485z00_1796;

																		{	/* BackEnd/init.scm 71 */
																			obj_t BgL_arg1489z00_1797;

																			{	/* BackEnd/init.scm 71 */
																				obj_t BgL_arg1502z00_1798;

																				{	/* BackEnd/init.scm 71 */
																					obj_t BgL_arg1509z00_1799;

																					{	/* BackEnd/init.scm 71 */
																						obj_t BgL_symbolz00_1999;

																						BgL_symbolz00_1999 =
																							BGl_za2moduleza2z00zzmodule_modulez00;
																						{	/* BackEnd/init.scm 71 */
																							obj_t BgL_arg1455z00_2000;

																							BgL_arg1455z00_2000 =
																								SYMBOL_TO_STRING
																								(BgL_symbolz00_1999);
																							BgL_arg1509z00_1799 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_2000);
																						}
																					}
																					BgL_arg1502z00_1798 =
																						string_append_3
																						(BGl_string1743z00zzbackend_initz00,
																						BgL_arg1509z00_1799,
																						BGl_string1744z00zzbackend_initz00);
																				}
																				BgL_arg1489z00_1797 =
																					MAKE_YOUNG_PAIR(BgL_arg1502z00_1798,
																					BNIL);
																			}
																			BgL_arg1473z00_1795 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																				BgL_arg1489z00_1797);
																		}
																		{	/* BackEnd/init.scm 73 */
																			obj_t BgL_arg1513z00_1800;

																			{	/* BackEnd/init.scm 73 */
																				obj_t BgL_arg1514z00_1801;

																				{	/* BackEnd/init.scm 73 */
																					obj_t BgL_arg1516z00_1802;
																					obj_t BgL_arg1535z00_1803;

																					{	/* BackEnd/init.scm 73 */
																						obj_t BgL_arg1540z00_1804;

																						{	/* BackEnd/init.scm 73 */
																							obj_t BgL_arg1544z00_1805;

																							{	/* BackEnd/init.scm 73 */
																								obj_t BgL_arg1546z00_1806;

																								{	/* BackEnd/init.scm 73 */
																									obj_t BgL_arg1552z00_1807;

																									BgL_arg1552z00_1807 =
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BGl_unitzd2initzd2callsz00zzast_unitz00
																										(), BNIL);
																									BgL_arg1546z00_1806 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(2),
																										BgL_arg1552z00_1807);
																								}
																								BgL_arg1544z00_1805 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1546z00_1806, BNIL);
																							}
																							BgL_arg1540z00_1804 =
																								MAKE_YOUNG_PAIR(BgL_tmpz00_1782,
																								BgL_arg1544z00_1805);
																						}
																						BgL_arg1516z00_1802 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1540z00_1804, BNIL);
																					}
																					{	/* BackEnd/init.scm 76 */
																						obj_t BgL_arg1559z00_1809;
																						obj_t BgL_arg1561z00_1810;

																						{	/* BackEnd/init.scm 76 */
																							obj_t BgL_arg1564z00_1811;

																							{	/* BackEnd/init.scm 76 */
																								obj_t BgL_arg1565z00_1812;

																								{	/* BackEnd/init.scm 76 */
																									obj_t BgL_arg1571z00_1813;

																									{	/* BackEnd/init.scm 76 */
																										obj_t BgL_symbolz00_2001;

																										BgL_symbolz00_2001 =
																											BGl_za2moduleza2z00zzmodule_modulez00;
																										{	/* BackEnd/init.scm 76 */
																											obj_t BgL_arg1455z00_2002;

																											BgL_arg1455z00_2002 =
																												SYMBOL_TO_STRING
																												(BgL_symbolz00_2001);
																											BgL_arg1571z00_1813 =
																												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																												(BgL_arg1455z00_2002);
																										}
																									}
																									BgL_arg1565z00_1812 =
																										string_append_3
																										(BGl_string1745z00zzbackend_initz00,
																										BgL_arg1571z00_1813,
																										BGl_string1744z00zzbackend_initz00);
																								}
																								BgL_arg1564z00_1811 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1565z00_1812, BNIL);
																							}
																							BgL_arg1559z00_1809 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(5), BgL_arg1564z00_1811);
																						}
																						BgL_arg1561z00_1810 =
																							MAKE_YOUNG_PAIR(BgL_tmpz00_1782,
																							BNIL);
																						BgL_arg1535z00_1803 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1559z00_1809,
																							BgL_arg1561z00_1810);
																					}
																					BgL_arg1514z00_1801 =
																						MAKE_YOUNG_PAIR(BgL_arg1516z00_1802,
																						BgL_arg1535z00_1803);
																				}
																				BgL_arg1513z00_1800 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																					BgL_arg1514z00_1801);
																			}
																			BgL_arg1485z00_1796 =
																				MAKE_YOUNG_PAIR(BgL_arg1513z00_1800,
																				BNIL);
																		}
																		BgL_arg1454z00_1793 =
																			MAKE_YOUNG_PAIR(BgL_arg1473z00_1795,
																			BgL_arg1485z00_1796);
																	}
																	BgL_arg1434z00_1789 =
																		MAKE_YOUNG_PAIR(BgL_arg1453z00_1792,
																		BgL_arg1454z00_1793);
																}
																BgL_arg1421z00_1787 =
																	MAKE_YOUNG_PAIR(BgL_arg1422z00_1788,
																	BgL_arg1434z00_1789);
															}
															BgL_arg1408z00_1785 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																BgL_arg1421z00_1787);
														}
														BgL_arg1410z00_1786 =
															MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
														BgL_arg1380z00_1784 =
															MAKE_YOUNG_PAIR(BgL_arg1408z00_1785,
															BgL_arg1410z00_1786);
													}
													BgL_arg1379z00_1783 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
														BgL_arg1380z00_1784);
												}
												BgL_ubodyz00_1721 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
													BgL_arg1379z00_1783);
											}
										}
									}
								else
									{	/* BackEnd/init.scm 89 */
										{	/* BackEnd/init.scm 58 */
											obj_t BgL_arg1346z00_1768;

											{	/* BackEnd/init.scm 58 */
												obj_t BgL_arg1348z00_1769;

												{	/* BackEnd/init.scm 58 */
													obj_t BgL_arg1349z00_1770;
													obj_t BgL_arg1351z00_1771;

													{	/* BackEnd/init.scm 58 */
														obj_t BgL_arg1352z00_1772;

														{	/* BackEnd/init.scm 58 */
															obj_t BgL_arg1361z00_1773;
															obj_t BgL_arg1364z00_1774;

															{	/* BackEnd/init.scm 58 */
																obj_t BgL_arg1367z00_1775;

																{	/* BackEnd/init.scm 58 */
																	obj_t BgL_arg1370z00_1776;

																	BgL_arg1370z00_1776 =
																		MAKE_YOUNG_PAIR(BFALSE, BNIL);
																	BgL_arg1367z00_1775 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																		BgL_arg1370z00_1776);
																}
																BgL_arg1361z00_1773 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																	BgL_arg1367z00_1775);
															}
															{	/* BackEnd/init.scm 59 */
																obj_t BgL_arg1371z00_1777;
																obj_t BgL_arg1375z00_1778;

																if (CBOOL
																	(BGl_za2dlopenzd2initzd2gcza2z00zzengine_paramz00))
																	{	/* BackEnd/init.scm 59 */
																		obj_t BgL_arg1376z00_1779;

																		BgL_arg1376z00_1779 =
																			BGl_thezd2backendzd2zzbackend_backendz00
																			();
																		BgL_arg1371z00_1777 =
																			BGl_backendzd2gczd2initz00zzbackend_backendz00
																			(((BgL_backendz00_bglt)
																				BgL_arg1376z00_1779));
																	}
																else
																	{	/* BackEnd/init.scm 59 */
																		BgL_arg1371z00_1777 = BFALSE;
																	}
																BgL_arg1375z00_1778 =
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BGl_unitzd2initzd2callsz00zzast_unitz00(),
																	BNIL);
																BgL_arg1364z00_1774 =
																	MAKE_YOUNG_PAIR(BgL_arg1371z00_1777,
																	BgL_arg1375z00_1778);
															}
															BgL_arg1352z00_1772 =
																MAKE_YOUNG_PAIR(BgL_arg1361z00_1773,
																BgL_arg1364z00_1774);
														}
														BgL_arg1349z00_1770 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
															BgL_arg1352z00_1772);
													}
													BgL_arg1351z00_1771 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
													BgL_arg1348z00_1769 =
														MAKE_YOUNG_PAIR(BgL_arg1349z00_1770,
														BgL_arg1351z00_1771);
												}
												BgL_arg1346z00_1768 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
													BgL_arg1348z00_1769);
											}
											BgL_ubodyz00_1721 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1346z00_1768);
										}
									}
								{	/* BackEnd/init.scm 89 */
									obj_t BgL_bodyz00_1722;

									if (CBOOL(BGl_za2unsafezd2versionza2zd2zzengine_paramz00))
										{	/* BackEnd/init.scm 90 */
											BgL_bodyz00_1722 = BgL_ubodyz00_1721;
										}
									else
										{	/* BackEnd/init.scm 92 */
											obj_t BgL_arg1305z00_1735;

											{	/* BackEnd/init.scm 92 */
												obj_t BgL_arg1306z00_1736;
												obj_t BgL_arg1307z00_1737;

												{	/* BackEnd/init.scm 92 */
													obj_t BgL_arg1308z00_1738;

													{	/* BackEnd/init.scm 92 */
														obj_t BgL_arg1310z00_1739;
														obj_t BgL_arg1311z00_1740;

														{	/* BackEnd/init.scm 92 */
															obj_t BgL_arg1312z00_1741;

															{	/* BackEnd/init.scm 92 */
																obj_t BgL_arg1314z00_1742;

																BgL_arg1314z00_1742 =
																	MAKE_YOUNG_PAIR
																	(BGl_za2modulezd2checksumza2zd2zzmodule_modulez00,
																	BNIL);
																BgL_arg1312z00_1741 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
																	BgL_arg1314z00_1742);
															}
															BgL_arg1310z00_1739 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																BgL_arg1312z00_1741);
														}
														BgL_arg1311z00_1740 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BNIL);
														BgL_arg1308z00_1738 =
															MAKE_YOUNG_PAIR(BgL_arg1310z00_1739,
															BgL_arg1311z00_1740);
													}
													BgL_arg1306z00_1736 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
														BgL_arg1308z00_1738);
												}
												{	/* BackEnd/init.scm 94 */
													obj_t BgL_arg1315z00_1743;

													{	/* BackEnd/init.scm 94 */
														obj_t BgL_arg1316z00_1744;

														{	/* BackEnd/init.scm 94 */
															bool_t BgL_test1767z00_2167;

															{	/* BackEnd/init.scm 94 */
																obj_t BgL_arg1342z00_1763;

																BgL_arg1342z00_1763 =
																	BGl_thezd2backendzd2zzbackend_backendz00();
																BgL_test1767z00_2167 =
																	(((BgL_backendz00_bglt) COBJECT(
																			((BgL_backendz00_bglt)
																				BgL_arg1342z00_1763)))->
																	BgL_pragmazd2supportzd2);
															}
															if (BgL_test1767z00_2167)
																{	/* BackEnd/init.scm 97 */
																	obj_t BgL_arg1319z00_1747;

																	{	/* BackEnd/init.scm 97 */
																		obj_t BgL_arg1320z00_1748;
																		obj_t BgL_arg1321z00_1749;

																		{	/* BackEnd/init.scm 97 */
																			obj_t BgL_arg1322z00_1750;

																			{	/* BackEnd/init.scm 97 */
																				obj_t BgL_arg1323z00_1751;

																				{	/* BackEnd/init.scm 97 */
																					obj_t BgL_arg1325z00_1752;

																					{	/* BackEnd/init.scm 97 */
																						obj_t BgL_arg1326z00_1753;

																						{	/* BackEnd/init.scm 97 */
																							obj_t BgL_arg1327z00_1754;

																							{	/* BackEnd/init.scm 97 */
																								obj_t BgL_arg1328z00_1755;

																								{	/* BackEnd/init.scm 97 */
																									obj_t BgL_symbolz00_2007;

																									BgL_symbolz00_2007 =
																										BGl_za2moduleza2z00zzmodule_modulez00;
																									{	/* BackEnd/init.scm 97 */
																										obj_t BgL_arg1455z00_2008;

																										BgL_arg1455z00_2008 =
																											SYMBOL_TO_STRING
																											(BgL_symbolz00_2007);
																										BgL_arg1328z00_1755 =
																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																											(BgL_arg1455z00_2008);
																									}
																								}
																								{	/* BackEnd/init.scm 97 */
																									obj_t BgL_list1329z00_1756;

																									BgL_list1329z00_1756 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1328z00_1755, BNIL);
																									BgL_arg1327z00_1754 =
																										BGl_formatz00zz__r4_output_6_10_3z00
																										(BGl_string1746z00zzbackend_initz00,
																										BgL_list1329z00_1756);
																								}
																							}
																							BgL_arg1326z00_1753 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1327z00_1754, BNIL);
																						}
																						BgL_arg1325z00_1752 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(14), BgL_arg1326z00_1753);
																					}
																					BgL_arg1323z00_1751 =
																						MAKE_YOUNG_PAIR(BgL_arg1325z00_1752,
																						BNIL);
																				}
																				BgL_arg1322z00_1750 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																					BgL_arg1323z00_1751);
																			}
																			BgL_arg1320z00_1748 =
																				MAKE_YOUNG_PAIR(BgL_arg1322z00_1750,
																				BNIL);
																		}
																		{	/* BackEnd/init.scm 98 */
																			obj_t BgL_arg1331z00_1757;

																			{	/* BackEnd/init.scm 98 */
																				obj_t BgL_arg1332z00_1758;

																				{	/* BackEnd/init.scm 98 */
																					obj_t BgL_arg1333z00_1759;

																					BgL_arg1333z00_1759 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																						BNIL);
																					BgL_arg1332z00_1758 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(17),
																						BgL_arg1333z00_1759);
																				}
																				BgL_arg1331z00_1757 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																					BgL_arg1332z00_1758);
																			}
																			BgL_arg1321z00_1749 =
																				MAKE_YOUNG_PAIR(BgL_arg1331z00_1757,
																				BNIL);
																		}
																		BgL_arg1319z00_1747 =
																			MAKE_YOUNG_PAIR(BgL_arg1320z00_1748,
																			BgL_arg1321z00_1749);
																	}
																	BgL_arg1316z00_1744 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																		BgL_arg1319z00_1747);
																}
															else
																{	/* BackEnd/init.scm 99 */
																	obj_t BgL_arg1335z00_1760;

																	{	/* BackEnd/init.scm 99 */
																		obj_t BgL_arg1339z00_1761;
																		obj_t BgL_arg1340z00_1762;

																		{	/* BackEnd/init.scm 99 */
																			obj_t BgL_symbolz00_2009;

																			BgL_symbolz00_2009 =
																				BGl_za2moduleza2z00zzmodule_modulez00;
																			{	/* BackEnd/init.scm 99 */
																				obj_t BgL_arg1455z00_2010;

																				BgL_arg1455z00_2010 =
																					SYMBOL_TO_STRING(BgL_symbolz00_2009);
																				BgL_arg1339z00_1761 =
																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																					(BgL_arg1455z00_2010);
																			}
																		}
																		BgL_arg1340z00_1762 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BNIL);
																		BgL_arg1335z00_1760 =
																			MAKE_YOUNG_PAIR(BgL_arg1339z00_1761,
																			BgL_arg1340z00_1762);
																	}
																	BgL_arg1316z00_1744 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																		BgL_arg1335z00_1760);
																}
														}
														BgL_arg1315z00_1743 =
															MAKE_YOUNG_PAIR(BgL_arg1316z00_1744, BNIL);
													}
													BgL_arg1307z00_1737 =
														MAKE_YOUNG_PAIR(BgL_ubodyz00_1721,
														BgL_arg1315z00_1743);
												}
												BgL_arg1305z00_1735 =
													MAKE_YOUNG_PAIR(BgL_arg1306z00_1736,
													BgL_arg1307z00_1737);
											}
											BgL_bodyz00_1722 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1305z00_1735);
										}
									{	/* BackEnd/init.scm 90 */
										BgL_localz00_bglt BgL_cvarz00_1723;

										BgL_cvarz00_1723 =
											BGl_makezd2localzd2svarz00zzast_localz00(CNST_TABLE_REF
											(11),
											((BgL_typez00_bglt) BGl_za2longza2z00zztype_cachez00));
										{	/* BackEnd/init.scm 101 */
											BgL_localz00_bglt BgL_nvarz00_1724;

											BgL_nvarz00_1724 =
												BGl_makezd2localzd2svarz00zzast_localz00(CNST_TABLE_REF
												(16),
												((BgL_typez00_bglt)
													BGl_za2stringza2z00zztype_cachez00));
											{	/* BackEnd/init.scm 102 */
												BgL_nodez00_bglt BgL_nodez00_1725;

												{	/* BackEnd/init.scm 103 */
													BgL_nodez00_bglt BgL_nodez00_1731;

													{	/* BackEnd/init.scm 103 */
														obj_t BgL_arg1284z00_1732;

														{	/* BackEnd/init.scm 103 */
															obj_t BgL_list1285z00_1733;

															{	/* BackEnd/init.scm 103 */
																obj_t BgL_arg1304z00_1734;

																BgL_arg1304z00_1734 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_nvarz00_1724), BNIL);
																BgL_list1285z00_1733 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_cvarz00_1723),
																	BgL_arg1304z00_1734);
															}
															BgL_arg1284z00_1732 = BgL_list1285z00_1733;
														}
														BgL_nodez00_1731 =
															BGl_sexpzd2ze3nodez31zzast_sexpz00
															(BgL_bodyz00_1722, BgL_arg1284z00_1732, BNIL,
															CNST_TABLE_REF(19));
													}
													BGl_lvtypezd2nodez12zc0zzast_lvtypez00
														(BgL_nodez00_1731);
													BgL_nodez00_1725 =
														BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_1731,
														((obj_t) BgL_reqz00_1718),
														((BgL_typez00_bglt)
															BGl_za2objza2z00zztype_cachez00), ((bool_t) 0));
												}
												{	/* BackEnd/init.scm 103 */
													BgL_globalz00_bglt BgL_initz00_1726;

													{	/* BackEnd/init.scm 107 */
														obj_t BgL_arg1252z00_1727;
														obj_t BgL_arg1268z00_1728;

														BgL_arg1252z00_1727 =
															BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00
															(BGl_za2moduleza2z00zzmodule_modulez00);
														{	/* BackEnd/init.scm 109 */
															obj_t BgL_list1269z00_1729;

															{	/* BackEnd/init.scm 109 */
																obj_t BgL_arg1272z00_1730;

																BgL_arg1272z00_1730 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_nvarz00_1724), BNIL);
																BgL_list1269z00_1729 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_cvarz00_1723),
																	BgL_arg1272z00_1730);
															}
															BgL_arg1268z00_1728 = BgL_list1269z00_1729;
														}
														BgL_initz00_1726 =
															BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2
															(BgL_arg1252z00_1727, CNST_TABLE_REF(20),
															BgL_arg1268z00_1728,
															BGl_za2moduleza2z00zzmodule_modulez00,
															CNST_TABLE_REF(21), CNST_TABLE_REF(22),
															CNST_TABLE_REF(9), ((obj_t) BgL_nodez00_1725));
													}
													{	/* BackEnd/init.scm 106 */

														BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
															((BgL_variablez00_bglt) BgL_reqz00_1718));
														{	/* BackEnd/init.scm 116 */
															obj_t BgL_vz00_2014;

															BgL_vz00_2014 = CNST_TABLE_REF(23);
															((((BgL_globalz00_bglt)
																		COBJECT(BgL_initz00_1726))->BgL_importz00) =
																((obj_t) BgL_vz00_2014), BUNSPEC);
														}
														{	/* BackEnd/init.scm 117 */
															BgL_typez00_bglt BgL_vz00_2016;

															BgL_vz00_2016 =
																((BgL_typez00_bglt)
																BGl_za2objza2z00zztype_cachez00);
															((((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_initz00_1726)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_vz00_2016), BUNSPEC);
														}
														BGl_occurzd2nodezd2inz12z12zzast_occurz00
															(BgL_nodez00_1725, BgL_initz00_1726);
														return BgL_initz00_1726;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_initz00(void)
	{
		{	/* BackEnd/init.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzast_unitz00(234044111L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string1747z00zzbackend_initz00));
		}

	}

#ifdef __cplusplus
}
#endif
