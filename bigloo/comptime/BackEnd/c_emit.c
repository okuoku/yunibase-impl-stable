/*===========================================================================*/
/*   (BackEnd/c_emit.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/c_emit.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_C_EMIT_TYPE_DEFINITIONS
#define BGL_BACKEND_C_EMIT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_BACKEND_C_EMIT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62stopzd2emissionz12za2zzbackend_c_emitz00(obj_t);
	BGL_IMPORT obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzbackend_c_emitz00 = BUNSPEC;
	extern obj_t BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_EXPORTED_DEF obj_t BGl_za2czd2portza2zd2zzbackend_c_emitz00 = BUNSPEC;
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_z62emitzd2atomzd2valuez62zzbackend_c_emitz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzbackend_c_emitz00(void);
	BGL_IMPORT obj_t BGl_commandzd2linezd2zz__osz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_emitzd2unsafezd2activationz00zzbackend_c_emitz00(void);
	BGL_EXPORTED_DECL obj_t BGl_stopzd2emissionz12zc0zzbackend_c_emitz00(void);
	static obj_t BGl_genericzd2initzd2zzbackend_c_emitz00(void);
	BGL_IMPORT obj_t
		BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BGL_LONGLONG_T, obj_t);
	static obj_t BGl_objectzd2initzd2zzbackend_c_emitz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_llongzd2ze3czd2isoze3zzbackend_c_emitz00(BGL_LONGLONG_T);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_za2sawza2z00zzengine_paramz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bigloozd2librarieszd2czd2setupza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_emitzd2includezd2zzbackend_c_emitz00(void);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_startzd2emissionz12zc0zzbackend_c_emitz00(obj_t);
	static obj_t BGl_z62startzd2emissionz12za2zzbackend_c_emitz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_c_emitz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_emitzd2garbagezd2collectorzd2selectionzd2zzbackend_c_emitz00(void);
	extern obj_t BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00;
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_emitzd2debugzd2activationz00zzbackend_c_emitz00(void);
	BGL_IMPORT obj_t make_string(long, unsigned char);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62emitzd2commentzb0zzbackend_c_emitz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bigloozd2licensingzf3za2z21zzengine_paramz00;
	static obj_t BGl_emitzd2licensezd2zzbackend_c_emitz00(void);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	BGL_IMPORT bool_t BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t);
	static obj_t BGl_z62emitzd2unsafezd2activationz62zzbackend_c_emitz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_positiveze70ze7zzbackend_c_emitz00(BGL_LONGLONG_T);
	static obj_t BGl_z62emitzd2mainzb0zzbackend_c_emitz00(obj_t);
	static obj_t BGl_z62emitzd2debugzd2activationz62zzbackend_c_emitz00(obj_t);
	static obj_t
		BGl_z62emitzd2garbagezd2collectorzd2selectionzb0zzbackend_c_emitz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_untrigraphz00zzbackend_c_emitz00(obj_t);
	BGL_IMPORT obj_t string_for_read(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_emitzd2copzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_copz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_licensez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_configurez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__ucs2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_za2bigloozd2nameza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_emitzd2dlopenzd2initz00zzbackend_c_emitz00(BgL_globalz00_bglt, obj_t);
	extern obj_t BGl_za2elongza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_emitzd2mainzd2zzbackend_c_emitz00(void);
	static obj_t BGl_z62untrigraphz62zzbackend_c_emitz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_za2destzd2prefixza2zd2zzbackend_c_emitz00 = BUNSPEC;
	extern obj_t BGl_za2userzd2heapzd2siza7eza2za7zzengine_paramz00;
	static obj_t BGl_z62llongzd2ze3czd2isoz81zzbackend_c_emitz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzbackend_c_emitz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_c_emitz00(void);
	BGL_EXPORTED_DECL obj_t BGl_emitzd2headerzd2zzbackend_c_emitz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_c_emitz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzbackend_c_emitz00(void);
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	static obj_t BGl_z62emitzd2includezb0zzbackend_c_emitz00(obj_t);
	extern obj_t BGl_za2bigloozd2authorza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_emitzd2tracezd2activationz00zzbackend_c_emitz00(void);
	extern obj_t BGl_za2bigloozd2dateza2zd2zzengine_paramz00;
	static obj_t BGl_z62emitzd2dlopenzd2initz62zzbackend_c_emitz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	static obj_t BGl_loopze70ze7zzbackend_c_emitz00(BGL_LONGLONG_T, obj_t, long,
		BGL_LONGLONG_T, BGL_LONGLONG_T);
	static obj_t BGl_z62emitzd2tracezd2activationz62zzbackend_c_emitz00(obj_t);
	extern obj_t BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00;
	static long BGl_za2maxzd2colza2zd2zzbackend_c_emitz00 = 0L;
	static obj_t BGl_z62emitzd2headerzb0zzbackend_c_emitz00(obj_t);
	extern obj_t BGl_bigloozd2licensezd2zztools_licensez00(void);
	extern obj_t BGl_za2includezd2foreignza2zd2zzengine_paramz00;
	extern obj_t BGl_za2garbagezd2collectorza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_emitzd2commentzd2zzbackend_c_emitz00(obj_t,
		unsigned char);
	extern obj_t BGl_za2charza2z00zztype_cachez00;
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_untrigraphzd2envzd2zzbackend_c_emitz00,
		BgL_bgl_za762untrigraphza7622034z00,
		BGl_z62untrigraphz62zzbackend_c_emitz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_emitzd2includezd2envz00zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2includ2035z00,
		BGl_z62emitzd2includezb0zzbackend_c_emitz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_emitzd2headerzd2envz00zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2header2036z00,
		BGl_z62emitzd2headerzb0zzbackend_c_emitz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2dlopenzd2initzd2envzd2zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2dlopen2037z00,
		BGl_z62emitzd2dlopenzd2initz62zzbackend_c_emitz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_llongzd2ze3czd2isozd2envz31zzbackend_c_emitz00,
		BgL_bgl_za762llongza7d2za7e3cza72038z00,
		BGl_z62llongzd2ze3czd2isoz81zzbackend_c_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1953z00zzbackend_c_emitz00,
		BgL_bgl_string1953za700za7za7b2039za7, "Can't open file for output", 26);
	      DEFINE_STRING(BGl_string1954z00zzbackend_c_emitz00,
		BgL_bgl_string1954za700za7za7b2040za7, "/*", 2);
	      DEFINE_STRING(BGl_string1955z00zzbackend_c_emitz00,
		BgL_bgl_string1955za700za7za7b2041za7, "*/", 2);
	      DEFINE_STRING(BGl_string1956z00zzbackend_c_emitz00,
		BgL_bgl_string1956za700za7za7b2042za7, "", 0);
	      DEFINE_STRING(BGl_string1957z00zzbackend_c_emitz00,
		BgL_bgl_string1957za700za7za7b2043za7, " (c)      ", 10);
	      DEFINE_STRING(BGl_string1958z00zzbackend_c_emitz00,
		BgL_bgl_string1958za700za7za7b2044za7, "/* COMPILATION: ", 16);
	      DEFINE_STRING(BGl_string1959z00zzbackend_c_emitz00,
		BgL_bgl_string1959za700za7za7b2045za7, " */", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stopzd2emissionz12zd2envz12zzbackend_c_emitz00,
		BgL_bgl_za762stopza7d2emissi2046z00,
		BGl_z62stopzd2emissionz12za2zzbackend_c_emitz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1960z00zzbackend_c_emitz00,
		BgL_bgl_string1960za700za7za7b2047za7, "\n/* SAW compilation */\n", 23);
	      DEFINE_STRING(BGl_string1961z00zzbackend_c_emitz00,
		BgL_bgl_string1961za700za7za7b2048za7, "#define BGL_SAW 1\n", 18);
	      DEFINE_STRING(BGl_string1962z00zzbackend_c_emitz00,
		BgL_bgl_string1962za700za7za7b2049za7, "/* GC selection */", 18);
	      DEFINE_STRING(BGl_string1963z00zzbackend_c_emitz00,
		BgL_bgl_string1963za700za7za7b2050za7, "#define THE_GC BOEHM_GC\n", 24);
	      DEFINE_STRING(BGl_string1964z00zzbackend_c_emitz00,
		BgL_bgl_string1964za700za7za7b2051za7, "#define THE_GC BOEHM_GC", 23);
	      DEFINE_STRING(BGl_string1965z00zzbackend_c_emitz00,
		BgL_bgl_string1965za700za7za7b2052za7, "#define BUMPY_GC\n", 17);
	      DEFINE_STRING(BGl_string1966z00zzbackend_c_emitz00,
		BgL_bgl_string1966za700za7za7b2053za7, "emit-garbage-collector-selection",
		32);
	      DEFINE_STRING(BGl_string1967z00zzbackend_c_emitz00,
		BgL_bgl_string1967za700za7za7b2054za7, "Can't emit code for gc", 22);
	      DEFINE_STRING(BGl_string1968z00zzbackend_c_emitz00,
		BgL_bgl_string1968za700za7za7b2055za7, "/* standard Bigloo include */", 29);
	      DEFINE_STRING(BGl_string1969z00zzbackend_c_emitz00,
		BgL_bgl_string1969za700za7za7b2056za7, "#include <", 10);
	      DEFINE_STRING(BGl_string1970z00zzbackend_c_emitz00,
		BgL_bgl_string1970za700za7za7b2057za7, ">", 1);
	      DEFINE_STRING(BGl_string1971z00zzbackend_c_emitz00,
		BgL_bgl_string1971za700za7za7b2058za7, "/* debug mode */", 16);
	      DEFINE_STRING(BGl_string1972z00zzbackend_c_emitz00,
		BgL_bgl_string1972za700za7za7b2059za7, "#define BIGLOO_DEBUG 1", 22);
	      DEFINE_STRING(BGl_string1973z00zzbackend_c_emitz00,
		BgL_bgl_string1973za700za7za7b2060za7, "/* unsafe mode */", 17);
	      DEFINE_STRING(BGl_string1974z00zzbackend_c_emitz00,
		BgL_bgl_string1974za700za7za7b2061za7, "#define BIGLOO_UNSAFE 1", 23);
	      DEFINE_STRING(BGl_string1975z00zzbackend_c_emitz00,
		BgL_bgl_string1975za700za7za7b2062za7, "/* traces mode */", 17);
	      DEFINE_STRING(BGl_string1976z00zzbackend_c_emitz00,
		BgL_bgl_string1976za700za7za7b2063za7, "#define BIGLOO_TRACE ", 21);
	      DEFINE_STRING(BGl_string1977z00zzbackend_c_emitz00,
		BgL_bgl_string1977za700za7za7b2064za7, "/* Libraries setup imports */", 29);
	      DEFINE_STRING(BGl_string1978z00zzbackend_c_emitz00,
		BgL_bgl_string1978za700za7za7b2065za7, "BGL_IMPORT void ", 16);
	      DEFINE_STRING(BGl_string1979z00zzbackend_c_emitz00,
		BgL_bgl_string1979za700za7za7b2066za7, "(int, char *[], char *[]);", 26);
	      DEFINE_STRING(BGl_string1980z00zzbackend_c_emitz00,
		BgL_bgl_string1980za700za7za7b2067za7, "/* Libraries setup */", 21);
	      DEFINE_STRING(BGl_string1981z00zzbackend_c_emitz00,
		BgL_bgl_string1981za700za7za7b2068za7,
		"static int bigloo_libinit( int argc, char *argv[], char *env[] ) {", 66);
	      DEFINE_STRING(BGl_string1982z00zzbackend_c_emitz00,
		BgL_bgl_string1982za700za7za7b2069za7, "(argc, argv, env);\n", 19);
	      DEFINE_STRING(BGl_string1983z00zzbackend_c_emitz00,
		BgL_bgl_string1983za700za7za7b2070za7, "return 0; }\n\n", 13);
	      DEFINE_STRING(BGl_string1984z00zzbackend_c_emitz00,
		BgL_bgl_string1984za700za7za7b2071za7,
		"long bigloo_abort(long n) { return n; }", 39);
	      DEFINE_STRING(BGl_string1985z00zzbackend_c_emitz00,
		BgL_bgl_string1985za700za7za7b2072za7,
		"int BIGLOO_MAIN(int argc, char *argv[], char *env[]) { ", 55);
	      DEFINE_STRING(BGl_string1986z00zzbackend_c_emitz00,
		BgL_bgl_string1986za700za7za7b2073za7,
		"return _bigloo_main(argc, argv, env, &bigloo_main, &bigloo_libinit, ~a);}",
		73);
	      DEFINE_STRING(BGl_string1987z00zzbackend_c_emitz00,
		BgL_bgl_string1987za700za7za7b2074za7, "BGL_EXPORTED_DEF obj_t ", 23);
	      DEFINE_STRING(BGl_string1988z00zzbackend_c_emitz00,
		BgL_bgl_string1988za700za7za7b2075za7, "() {", 4);
	      DEFINE_STRING(BGl_string1989z00zzbackend_c_emitz00,
		BgL_bgl_string1989za700za7za7b2076za7, "obj_t res = ", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2debugzd2activationzd2envzd2zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2debugza72077za7,
		BGl_z62emitzd2debugzd2activationz62zzbackend_c_emitz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1990z00zzbackend_c_emitz00,
		BgL_bgl_string1990za700za7za7b2078za7, "(0, \"", 5);
	      DEFINE_STRING(BGl_string1991z00zzbackend_c_emitz00,
		BgL_bgl_string1991za700za7za7b2079za7, "\");", 3);
	      DEFINE_STRING(BGl_string1992z00zzbackend_c_emitz00,
		BgL_bgl_string1992za700za7za7b2080za7, "BGL_MVALUES_NUMBER_SET(2);", 26);
	      DEFINE_STRING(BGl_string1993z00zzbackend_c_emitz00,
		BgL_bgl_string1993za700za7za7b2081za7,
		"BGL_MVALUES_VAL_SET(1,string_to_bstring(\"", 41);
	      DEFINE_STRING(BGl_string1994z00zzbackend_c_emitz00,
		BgL_bgl_string1994za700za7za7b2082za7, "\"));", 4);
	      DEFINE_STRING(BGl_string1995z00zzbackend_c_emitz00,
		BgL_bgl_string1995za700za7za7b2083za7, "return res;", 11);
	      DEFINE_STRING(BGl_string1996z00zzbackend_c_emitz00,
		BgL_bgl_string1996za700za7za7b2084za7, "}", 1);
	      DEFINE_STRING(BGl_string1997z00zzbackend_c_emitz00,
		BgL_bgl_string1997za700za7za7b2085za7, "(-", 2);
	      DEFINE_STRING(BGl_string1998z00zzbackend_c_emitz00,
		BgL_bgl_string1998za700za7za7b2086za7, ")", 1);
	      DEFINE_STRING(BGl_string1999z00zzbackend_c_emitz00,
		BgL_bgl_string1999za700za7za7b2087za7, ")))", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_startzd2emissionz12zd2envz12zzbackend_c_emitz00,
		BgL_bgl_za762startza7d2emiss2088z00,
		BGl_z62startzd2emissionz12za2zzbackend_c_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_emitzd2mainzd2envz00zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2mainza7b2089za7,
		BGl_z62emitzd2mainzb0zzbackend_c_emitz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2unsafezd2activationzd2envzd2zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2unsafe2090z00,
		BGl_z62emitzd2unsafezd2activationz62zzbackend_c_emitz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2garbagezd2collectorzd2selectionzd2envz00zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2garbag2091z00,
		BGl_z62emitzd2garbagezd2collectorzd2selectionzb0zzbackend_c_emitz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_emitzd2commentzd2envz00zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2commen2092z00,
		BGl_z62emitzd2commentzb0zzbackend_c_emitz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2000z00zzbackend_c_emitz00,
		BgL_bgl_string2000za700za7za7b2093za7, " * (", 4);
	      DEFINE_STRING(BGl_string2001z00zzbackend_c_emitz00,
		BgL_bgl_string2001za700za7za7b2094za7, " + ((BGL_LONGLONG_T)", 20);
	      DEFINE_STRING(BGl_string2002z00zzbackend_c_emitz00,
		BgL_bgl_string2002za700za7za7b2095za7, "(", 1);
	      DEFINE_STRING(BGl_string2003z00zzbackend_c_emitz00,
		BgL_bgl_string2003za700za7za7b2096za7, "((BGL_LONGLONG_T)", 17);
	      DEFINE_STRING(BGl_string2004z00zzbackend_c_emitz00,
		BgL_bgl_string2004za700za7za7b2097za7, "((", 2);
	      DEFINE_STRING(BGl_string2005z00zzbackend_c_emitz00,
		BgL_bgl_string2005za700za7za7b2098za7, "BNIL", 4);
	      DEFINE_STRING(BGl_string2006z00zzbackend_c_emitz00,
		BgL_bgl_string2006za700za7za7b2099za7, "'\\000'", 6);
	      DEFINE_STRING(BGl_string2007z00zzbackend_c_emitz00,
		BgL_bgl_string2007za700za7za7b2100za7, "'\\''", 4);
	      DEFINE_STRING(BGl_string2008z00zzbackend_c_emitz00,
		BgL_bgl_string2008za700za7za7b2101za7, "'\\\\'", 4);
	      DEFINE_STRING(BGl_string2009z00zzbackend_c_emitz00,
		BgL_bgl_string2009za700za7za7b2102za7, "(int8_t)(", 9);
	      DEFINE_STRING(BGl_string2010z00zzbackend_c_emitz00,
		BgL_bgl_string2010za700za7za7b2103za7, "(uint8_t)(", 10);
	      DEFINE_STRING(BGl_string2011z00zzbackend_c_emitz00,
		BgL_bgl_string2011za700za7za7b2104za7, "(int16_t)(", 10);
	      DEFINE_STRING(BGl_string2012z00zzbackend_c_emitz00,
		BgL_bgl_string2012za700za7za7b2105za7, "(uint16_t)(", 11);
	      DEFINE_STRING(BGl_string2013z00zzbackend_c_emitz00,
		BgL_bgl_string2013za700za7za7b2106za7, "(int32_t)(", 10);
	      DEFINE_STRING(BGl_string2014z00zzbackend_c_emitz00,
		BgL_bgl_string2014za700za7za7b2107za7, "(uint32_t)(", 11);
	      DEFINE_STRING(BGl_string2015z00zzbackend_c_emitz00,
		BgL_bgl_string2015za700za7za7b2108za7, "(int64_t)(", 10);
	      DEFINE_STRING(BGl_string2016z00zzbackend_c_emitz00,
		BgL_bgl_string2016za700za7za7b2109za7, "(uint64_t)(", 11);
	      DEFINE_STRING(BGl_string2017z00zzbackend_c_emitz00,
		BgL_bgl_string2017za700za7za7b2110za7, "BUCS2(", 6);
	      DEFINE_STRING(BGl_string2018z00zzbackend_c_emitz00,
		BgL_bgl_string2018za700za7za7b2111za7, "BUNSPEC", 7);
	      DEFINE_STRING(BGl_string2019z00zzbackend_c_emitz00,
		BgL_bgl_string2019za700za7za7b2112za7, "L", 1);
	      DEFINE_STRING(BGl_string2020z00zzbackend_c_emitz00,
		BgL_bgl_string2020za700za7za7b2113za7, "BGL_NAN", 7);
	      DEFINE_STRING(BGl_string2021z00zzbackend_c_emitz00,
		BgL_bgl_string2021za700za7za7b2114za7, "BGL_INFINITY", 12);
	      DEFINE_STRING(BGl_string2022z00zzbackend_c_emitz00,
		BgL_bgl_string2022za700za7za7b2115za7, "(-BGL_INFINITY)", 15);
	      DEFINE_STRING(BGl_string2023z00zzbackend_c_emitz00,
		BgL_bgl_string2023za700za7za7b2116za7, "(BEOF)", 6);
	      DEFINE_STRING(BGl_string2024z00zzbackend_c_emitz00,
		BgL_bgl_string2024za700za7za7b2117za7, "(BOPTIONAL)", 11);
	      DEFINE_STRING(BGl_string2025z00zzbackend_c_emitz00,
		BgL_bgl_string2025za700za7za7b2118za7, "(BKEY)", 6);
	      DEFINE_STRING(BGl_string2026z00zzbackend_c_emitz00,
		BgL_bgl_string2026za700za7za7b2119za7, "(BREST)", 7);
	      DEFINE_STRING(BGl_string2027z00zzbackend_c_emitz00,
		BgL_bgl_string2027za700za7za7b2120za7, "(BEOA)", 6);
	      DEFINE_STRING(BGl_string2028z00zzbackend_c_emitz00,
		BgL_bgl_string2028za700za7za7b2121za7, "BCNST(", 6);
	      DEFINE_STRING(BGl_string2029z00zzbackend_c_emitz00,
		BgL_bgl_string2029za700za7za7b2122za7, "(bgl_string_to_bignum( \"", 24);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2atomzd2valuezd2envzd2zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2atomza7d2123za7,
		BGl_z62emitzd2atomzd2valuez62zzbackend_c_emitz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2030z00zzbackend_c_emitz00,
		BgL_bgl_string2030za700za7za7b2124za7, "\", 16 ))", 8);
	      DEFINE_STRING(BGl_string2031z00zzbackend_c_emitz00,
		BgL_bgl_string2031za700za7za7b2125za7, "backend_c_emit", 14);
	      DEFINE_STRING(BGl_string2032z00zzbackend_c_emitz00,
		BgL_bgl_string2032za700za7za7b2126za7,
		"bumpy boehm #z8 --to-stdout so ld (cgen distrib cc cindent hgen) ", 65);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2tracezd2activationzd2envzd2zzbackend_c_emitz00,
		BgL_bgl_za762emitza7d2traceza72127za7,
		BGl_z62emitzd2tracezd2activationz62zzbackend_c_emitz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzbackend_c_emitz00));
		     ADD_ROOT((void *) (&BGl_za2czd2portza2zd2zzbackend_c_emitz00));
		     ADD_ROOT((void *) (&BGl_za2destzd2prefixza2zd2zzbackend_c_emitz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(long
		BgL_checksumz00_2728, char *BgL_fromz00_2729)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_c_emitz00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_c_emitz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_c_emitz00();
					BGl_libraryzd2moduleszd2initz00zzbackend_c_emitz00();
					BGl_cnstzd2initzd2zzbackend_c_emitz00();
					BGl_importedzd2moduleszd2initz00zzbackend_c_emitz00();
					return BGl_toplevelzd2initzd2zzbackend_c_emitz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__ucs2z00(0L, "backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L,
				"backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "backend_c_emit");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"backend_c_emit");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 15 */
			{	/* BackEnd/c_emit.scm 15 */
				obj_t BgL_cportz00_2717;

				{	/* BackEnd/c_emit.scm 15 */
					obj_t BgL_stringz00_2724;

					BgL_stringz00_2724 = BGl_string2032z00zzbackend_c_emitz00;
					{	/* BackEnd/c_emit.scm 15 */
						obj_t BgL_startz00_2725;

						BgL_startz00_2725 = BINT(0L);
						{	/* BackEnd/c_emit.scm 15 */
							obj_t BgL_endz00_2726;

							BgL_endz00_2726 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2724)));
							{	/* BackEnd/c_emit.scm 15 */

								BgL_cportz00_2717 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2724, BgL_startz00_2725, BgL_endz00_2726);
				}}}}
				{
					long BgL_iz00_2718;

					BgL_iz00_2718 = 6L;
				BgL_loopz00_2719:
					if ((BgL_iz00_2718 == -1L))
						{	/* BackEnd/c_emit.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/c_emit.scm 15 */
							{	/* BackEnd/c_emit.scm 15 */
								obj_t BgL_arg2033z00_2720;

								{	/* BackEnd/c_emit.scm 15 */

									{	/* BackEnd/c_emit.scm 15 */
										obj_t BgL_locationz00_2722;

										BgL_locationz00_2722 = BBOOL(((bool_t) 0));
										{	/* BackEnd/c_emit.scm 15 */

											BgL_arg2033z00_2720 =
												BGl_readz00zz__readerz00(BgL_cportz00_2717,
												BgL_locationz00_2722);
										}
									}
								}
								{	/* BackEnd/c_emit.scm 15 */
									int BgL_tmpz00_2764;

									BgL_tmpz00_2764 = (int) (BgL_iz00_2718);
									CNST_TABLE_SET(BgL_tmpz00_2764, BgL_arg2033z00_2720);
							}}
							{	/* BackEnd/c_emit.scm 15 */
								int BgL_auxz00_2723;

								BgL_auxz00_2723 = (int) ((BgL_iz00_2718 - 1L));
								{
									long BgL_iz00_2769;

									BgL_iz00_2769 = (long) (BgL_auxz00_2723);
									BgL_iz00_2718 = BgL_iz00_2769;
									goto BgL_loopz00_2719;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 15 */
			BGl_za2destzd2prefixza2zd2zzbackend_c_emitz00 = BFALSE;
			BGl_za2czd2portza2zd2zzbackend_c_emitz00 = BFALSE;
			return (BGl_za2maxzd2colza2zd2zzbackend_c_emitz00 = 79L, BUNSPEC);
		}

	}



/* start-emission! */
	BGL_EXPORTED_DEF obj_t BGl_startzd2emissionz12zc0zzbackend_c_emitz00(obj_t
		BgL_suffixz00_3)
	{
		{	/* BackEnd/c_emit.scm 51 */
			{	/* BackEnd/c_emit.scm 52 */
				obj_t BgL_prefixz00_1733;

				{	/* BackEnd/c_emit.scm 53 */
					bool_t BgL_test2130z00_2772;

					if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
						{	/* BackEnd/c_emit.scm 53 */
							BgL_test2130z00_2772 =
								CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
								(BGl_za2passza2z00zzengine_paramz00, CNST_TABLE_REF(0)));
						}
					else
						{	/* BackEnd/c_emit.scm 53 */
							BgL_test2130z00_2772 = ((bool_t) 0);
						}
					if (BgL_test2130z00_2772)
						{	/* BackEnd/c_emit.scm 53 */
							BgL_prefixz00_1733 =
								BGl_prefixz00zz__osz00(BGl_za2destza2z00zzengine_paramz00);
						}
					else
						{	/* BackEnd/c_emit.scm 56 */
							bool_t BgL_test2132z00_2779;

							if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
								{	/* BackEnd/c_emit.scm 56 */
									obj_t BgL_tmpz00_2782;

									BgL_tmpz00_2782 =
										CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
									BgL_test2132z00_2779 = STRINGP(BgL_tmpz00_2782);
								}
							else
								{	/* BackEnd/c_emit.scm 56 */
									BgL_test2132z00_2779 = ((bool_t) 0);
								}
							if (BgL_test2132z00_2779)
								{	/* BackEnd/c_emit.scm 56 */
									BgL_prefixz00_1733 =
										BGl_prefixz00zz__osz00(CAR
										(BGl_za2srczd2filesza2zd2zzengine_paramz00));
								}
							else
								{	/* BackEnd/c_emit.scm 58 */
									bool_t BgL_test2134z00_2787;

									if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
										{	/* BackEnd/c_emit.scm 58 */
											BgL_test2134z00_2787 =
												(BGl_za2passza2z00zzengine_paramz00 ==
												CNST_TABLE_REF(1));
										}
									else
										{	/* BackEnd/c_emit.scm 58 */
											BgL_test2134z00_2787 = ((bool_t) 0);
										}
									if (BgL_test2134z00_2787)
										{	/* BackEnd/c_emit.scm 58 */
											BgL_prefixz00_1733 =
												BGl_prefixz00zz__osz00
												(BGl_za2destza2z00zzengine_paramz00);
										}
									else
										{	/* BackEnd/c_emit.scm 60 */
											bool_t BgL_test2136z00_2793;

											if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
												{	/* BackEnd/c_emit.scm 60 */
													BgL_test2136z00_2793 =
														(BGl_za2passza2z00zzengine_paramz00 ==
														CNST_TABLE_REF(2));
												}
											else
												{	/* BackEnd/c_emit.scm 60 */
													BgL_test2136z00_2793 = ((bool_t) 0);
												}
											if (BgL_test2136z00_2793)
												{	/* BackEnd/c_emit.scm 60 */
													BgL_prefixz00_1733 =
														BGl_prefixz00zz__osz00
														(BGl_za2destza2z00zzengine_paramz00);
												}
											else
												{	/* BackEnd/c_emit.scm 60 */
													BgL_prefixz00_1733 = BFALSE;
												}
										}
								}
						}
				}
				{	/* BackEnd/c_emit.scm 52 */

					{	/* BackEnd/c_emit.scm 64 */
						bool_t BgL_test2138z00_2799;

						if ((BGl_za2destza2z00zzengine_paramz00 == CNST_TABLE_REF(3)))
							{	/* BackEnd/c_emit.scm 64 */
								BgL_test2138z00_2799 = ((bool_t) 1);
							}
						else
							{	/* BackEnd/c_emit.scm 64 */
								if (STRINGP(BgL_prefixz00_1733))
									{	/* BackEnd/c_emit.scm 64 */
										BgL_test2138z00_2799 = ((bool_t) 0);
									}
								else
									{	/* BackEnd/c_emit.scm 64 */
										BgL_test2138z00_2799 = ((bool_t) 1);
									}
							}
						if (BgL_test2138z00_2799)
							{	/* BackEnd/c_emit.scm 65 */
								obj_t BgL_tmpz00_2805;

								BgL_tmpz00_2805 = BGL_CURRENT_DYNAMIC_ENV();
								return (BGl_za2czd2portza2zd2zzbackend_c_emitz00 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2805), BUNSPEC);
							}
						else
							{	/* BackEnd/c_emit.scm 66 */
								obj_t BgL_fzd2namezd2_1736;

								BgL_fzd2namezd2_1736 =
									string_append(BgL_prefixz00_1733, BgL_suffixz00_3);
								BGl_za2destzd2prefixza2zd2zzbackend_c_emitz00 =
									BgL_prefixz00_1733;
								{	/* BackEnd/c_emit.scm 68 */

									BGl_za2czd2portza2zd2zzbackend_c_emitz00 =
										BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00
										(BgL_fzd2namezd2_1736, BTRUE);
								}
								if (OUTPUT_PORTP(BGl_za2czd2portza2zd2zzbackend_c_emitz00))
									{	/* BackEnd/c_emit.scm 69 */
										return BUNSPEC;
									}
								else
									{	/* BackEnd/c_emit.scm 69 */
										return
											BGl_errorz00zz__errorz00
											(BGl_za2bigloozd2nameza2zd2zzengine_paramz00,
											BGl_string1953z00zzbackend_c_emitz00,
											BgL_fzd2namezd2_1736);
									}
							}
					}
				}
			}
		}

	}



/* &start-emission! */
	obj_t BGl_z62startzd2emissionz12za2zzbackend_c_emitz00(obj_t BgL_envz00_2690,
		obj_t BgL_suffixz00_2691)
	{
		{	/* BackEnd/c_emit.scm 51 */
			return BGl_startzd2emissionz12zc0zzbackend_c_emitz00(BgL_suffixz00_2691);
		}

	}



/* stop-emission! */
	BGL_EXPORTED_DEF obj_t BGl_stopzd2emissionz12zc0zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 76 */
			if (OUTPUT_PORTP(BGl_za2czd2portza2zd2zzbackend_c_emitz00))
				{	/* BackEnd/c_emit.scm 80 */
					bool_t BgL_test2143z00_2816;

					{	/* BackEnd/c_emit.scm 80 */
						obj_t BgL_arg1408z00_1759;

						{	/* BackEnd/c_emit.scm 80 */
							obj_t BgL_tmpz00_2817;

							BgL_tmpz00_2817 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1408z00_1759 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2817);
						}
						BgL_test2143z00_2816 =
							(BGl_za2czd2portza2zd2zzbackend_c_emitz00 == BgL_arg1408z00_1759);
					}
					if (BgL_test2143z00_2816)
						{	/* BackEnd/c_emit.scm 80 */
							return BFALSE;
						}
					else
						{	/* BackEnd/c_emit.scm 80 */
							bgl_flush_output_port(BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							bgl_close_output_port(BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							BGl_za2czd2portza2zd2zzbackend_c_emitz00 = BFALSE;
							return BGl_za2destzd2prefixza2zd2zzbackend_c_emitz00;
						}
				}
			else
				{	/* BackEnd/c_emit.scm 78 */
					return BFALSE;
				}
		}

	}



/* &stop-emission! */
	obj_t BGl_z62stopzd2emissionz12za2zzbackend_c_emitz00(obj_t BgL_envz00_2692)
	{
		{	/* BackEnd/c_emit.scm 76 */
			return BGl_stopzd2emissionz12zc0zzbackend_c_emitz00();
		}

	}



/* emit-comment */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2commentzd2zzbackend_c_emitz00(obj_t
		BgL_stringz00_4, unsigned char BgL_fillz00_5)
	{
		{	/* BackEnd/c_emit.scm 96 */
			{	/* BackEnd/c_emit.scm 97 */
				obj_t BgL_stringz00_1760;

				if ((STRING_LENGTH(BgL_stringz00_4) > (79L - 8L)))
					{	/* BackEnd/c_emit.scm 97 */
						BgL_stringz00_1760 = c_substring(BgL_stringz00_4, 0L, (79L - 9L));
					}
				else
					{	/* BackEnd/c_emit.scm 97 */
						BgL_stringz00_1760 = BgL_stringz00_4;
					}
				bgl_display_string(BGl_string1954z00zzbackend_c_emitz00,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				{	/* BackEnd/c_emit.scm 101 */
					long BgL_lenz00_1761;

					BgL_lenz00_1761 = STRING_LENGTH(BgL_stringz00_1760);
					if ((BgL_lenz00_1761 == 0L))
						{	/* BackEnd/c_emit.scm 103 */
							obj_t BgL_port1308z00_1763;

							BgL_port1308z00_1763 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
							bgl_display_obj(make_string(
									(79L - 4L),
									(unsigned char) (BgL_fillz00_5)), BgL_port1308z00_1763);
							{	/* BackEnd/c_emit.scm 103 */
								obj_t BgL_tmpz00_2838;

								BgL_tmpz00_2838 = ((obj_t) BgL_port1308z00_1763);
								bgl_display_string(BGl_string1955z00zzbackend_c_emitz00,
									BgL_tmpz00_2838);
							}
							{	/* BackEnd/c_emit.scm 103 */
								obj_t BgL_tmpz00_2841;

								BgL_tmpz00_2841 = ((obj_t) BgL_port1308z00_1763);
								return bgl_display_char(((unsigned char) 10), BgL_tmpz00_2841);
						}}
					else
						{	/* BackEnd/c_emit.scm 102 */
							{	/* BackEnd/c_emit.scm 105 */
								obj_t BgL_arg1434z00_1766;

								BgL_arg1434z00_1766 =
									make_string(2L, (unsigned char) (BgL_fillz00_5));
								bgl_display_obj(BgL_arg1434z00_1766,
									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							}
							bgl_display_char(((unsigned char) ' '),
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							bgl_display_obj(BgL_stringz00_1760,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							bgl_display_char(((unsigned char) ' '),
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							{	/* BackEnd/c_emit.scm 109 */
								obj_t BgL_port1309z00_1767;

								BgL_port1309z00_1767 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
								{	/* BackEnd/c_emit.scm 110 */
									obj_t BgL_arg1437z00_1768;

									{	/* BackEnd/c_emit.scm 110 */
										long BgL_arg1448z00_1769;

										{	/* BackEnd/c_emit.scm 110 */
											obj_t BgL_arg1453z00_1770;

											{	/* BackEnd/c_emit.scm 110 */
												obj_t BgL_za72za7_2298;

												BgL_za72za7_2298 = BINT(BgL_lenz00_1761);
												{	/* BackEnd/c_emit.scm 110 */
													obj_t BgL_tmpz00_2299;

													BgL_tmpz00_2299 = BINT(0L);
													{	/* BackEnd/c_emit.scm 110 */
														bool_t BgL_test2146z00_2852;

														{	/* BackEnd/c_emit.scm 110 */
															obj_t BgL_tmpz00_2853;

															BgL_tmpz00_2853 = BINT(8L);
															BgL_test2146z00_2852 =
																BGL_ADDFX_OV(BgL_tmpz00_2853, BgL_za72za7_2298,
																BgL_tmpz00_2299);
														}
														if (BgL_test2146z00_2852)
															{	/* BackEnd/c_emit.scm 110 */
																BgL_arg1453z00_1770 =
																	bgl_bignum_add(CNST_TABLE_REF(4),
																	bgl_long_to_bignum(
																		(long) CINT(BgL_za72za7_2298)));
															}
														else
															{	/* BackEnd/c_emit.scm 110 */
																BgL_arg1453z00_1770 = BgL_tmpz00_2299;
															}
													}
												}
											}
											BgL_arg1448z00_1769 =
												(79L - (long) CINT(BgL_arg1453z00_1770));
										}
										BgL_arg1437z00_1768 =
											make_string(BgL_arg1448z00_1769,
											(unsigned char) (BgL_fillz00_5));
									}
									bgl_display_obj(BgL_arg1437z00_1768, BgL_port1309z00_1767);
								}
								{	/* BackEnd/c_emit.scm 109 */
									obj_t BgL_tmpz00_2865;

									BgL_tmpz00_2865 = ((obj_t) BgL_port1309z00_1767);
									bgl_display_string(BGl_string1955z00zzbackend_c_emitz00,
										BgL_tmpz00_2865);
								}
								{	/* BackEnd/c_emit.scm 109 */
									obj_t BgL_tmpz00_2868;

									BgL_tmpz00_2868 = ((obj_t) BgL_port1309z00_1767);
									return
										bgl_display_char(((unsigned char) 10), BgL_tmpz00_2868);
		}}}}}}

	}



/* &emit-comment */
	obj_t BGl_z62emitzd2commentzb0zzbackend_c_emitz00(obj_t BgL_envz00_2693,
		obj_t BgL_stringz00_2694, obj_t BgL_fillz00_2695)
	{
		{	/* BackEnd/c_emit.scm 96 */
			return
				BGl_emitzd2commentzd2zzbackend_c_emitz00(BgL_stringz00_2694,
				CCHAR(BgL_fillz00_2695));
		}

	}



/* emit-license */
	obj_t BGl_emitzd2licensezd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 116 */
			{	/* BackEnd/c_emit.scm 117 */
				obj_t BgL_inz00_1777;

				{	/* BackEnd/c_emit.scm 117 */
					obj_t BgL_arg1513z00_1785;

					BgL_arg1513z00_1785 = BGl_bigloozd2licensezd2zztools_licensez00();
					{	/* BackEnd/c_emit.scm 117 */
						long BgL_endz00_1788;

						BgL_endz00_1788 = STRING_LENGTH(((obj_t) BgL_arg1513z00_1785));
						{	/* BackEnd/c_emit.scm 117 */

							BgL_inz00_1777 =
								BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
								(BgL_arg1513z00_1785, BINT(0L), BINT(BgL_endz00_1788));
				}}}
				{	/* BackEnd/c_emit.scm 118 */
					obj_t BgL_g1130z00_1778;

					BgL_g1130z00_1778 =
						BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_inz00_1777);
					{
						obj_t BgL_strz00_1780;

						BgL_strz00_1780 = BgL_g1130z00_1778;
					BgL_zc3z04anonymousza31503ze3z87_1781:
						if (EOF_OBJECTP(BgL_strz00_1780))
							{	/* BackEnd/c_emit.scm 119 */
								return bgl_close_input_port(BgL_inz00_1777);
							}
						else
							{	/* BackEnd/c_emit.scm 119 */
								BGl_emitzd2commentzd2zzbackend_c_emitz00(BgL_strz00_1780,
									(char) (((unsigned char) ' ')));
								{
									obj_t BgL_strz00_2885;

									BgL_strz00_2885 =
										BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_inz00_1777);
									BgL_strz00_1780 = BgL_strz00_2885;
									goto BgL_zc3z04anonymousza31503ze3z87_1781;
								}
							}
					}
				}
			}
		}

	}



/* emit-header */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2headerzd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 128 */
			BGl_emitzd2commentzd2zzbackend_c_emitz00
				(BGl_string1956z00zzbackend_c_emitz00, (char) (((unsigned char) '=')));
			{	/* BackEnd/c_emit.scm 130 */
				obj_t BgL_arg1514z00_1789;

				{	/* BackEnd/c_emit.scm 130 */
					obj_t BgL_pz00_1790;

					{	/* BackEnd/c_emit.scm 130 */

						{	/* BackEnd/c_emit.scm 130 */

							BgL_pz00_1790 =
								BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
					}}
					bgl_display_obj(BGl_za2srczd2filesza2zd2zzengine_paramz00,
						BgL_pz00_1790);
					BgL_arg1514z00_1789 = bgl_close_output_port(BgL_pz00_1790);
				}
				BGl_emitzd2commentzd2zzbackend_c_emitz00(BgL_arg1514z00_1789,
					(char) (((unsigned char) ' ')));
			}
			BGl_emitzd2commentzd2zzbackend_c_emitz00
				(BGl_za2bigloozd2nameza2zd2zzengine_paramz00,
				(char) (((unsigned char) ' ')));
			BGl_emitzd2commentzd2zzbackend_c_emitz00(string_append_3
				(BGl_za2bigloozd2authorza2zd2zzengine_paramz00,
					BGl_string1957z00zzbackend_c_emitz00,
					BGl_za2bigloozd2dateza2zd2zzengine_paramz00),
				(char) (((unsigned char) ' ')));
			if (CBOOL(BGl_za2bigloozd2licensingzf3za2z21zzengine_paramz00))
				{	/* BackEnd/c_emit.scm 137 */
					BGl_emitzd2licensezd2zzbackend_c_emitz00();
				}
			else
				{	/* BackEnd/c_emit.scm 137 */
					BFALSE;
				}
			BGl_emitzd2commentzd2zzbackend_c_emitz00
				(BGl_string1956z00zzbackend_c_emitz00, (char) (((unsigned char) '=')));
			bgl_display_string(BGl_string1958z00zzbackend_c_emitz00,
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			{	/* BackEnd/c_emit.scm 140 */
				obj_t BgL_arg1535z00_1793;

				BgL_arg1535z00_1793 = BGl_commandzd2linezd2zz__osz00();
				bgl_display_obj(BgL_arg1535z00_1793,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			}
			bgl_display_string(BGl_string1959z00zzbackend_c_emitz00,
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			bgl_display_char(((unsigned char) 10),
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			if (CBOOL(BGl_za2sawza2z00zzengine_paramz00))
				{	/* BackEnd/c_emit.scm 143 */
					bgl_display_string(BGl_string1960z00zzbackend_c_emitz00,
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					return bgl_display_string(BGl_string1961z00zzbackend_c_emitz00,
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
			else
				{	/* BackEnd/c_emit.scm 143 */
					return BFALSE;
				}
		}

	}



/* &emit-header */
	obj_t BGl_z62emitzd2headerzb0zzbackend_c_emitz00(obj_t BgL_envz00_2696)
	{
		{	/* BackEnd/c_emit.scm 128 */
			return BGl_emitzd2headerzd2zzbackend_c_emitz00();
		}

	}



/* emit-garbage-collector-selection */
	BGL_EXPORTED_DEF obj_t
		BGl_emitzd2garbagezd2collectorzd2selectionzd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 152 */
			{	/* BackEnd/c_emit.scm 153 */
				obj_t BgL_port1310z00_1794;

				BgL_port1310z00_1794 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 153 */
					obj_t BgL_tmpz00_2914;

					BgL_tmpz00_2914 = ((obj_t) BgL_port1310z00_1794);
					bgl_display_string(BGl_string1962z00zzbackend_c_emitz00,
						BgL_tmpz00_2914);
				}
				{	/* BackEnd/c_emit.scm 153 */
					obj_t BgL_tmpz00_2917;

					BgL_tmpz00_2917 = ((obj_t) BgL_port1310z00_1794);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_2917);
			}}
			{	/* BackEnd/c_emit.scm 154 */
				obj_t BgL_casezd2valuezd2_1795;

				BgL_casezd2valuezd2_1795 =
					BGl_za2garbagezd2collectorza2zd2zzengine_paramz00;
				if ((BgL_casezd2valuezd2_1795 == CNST_TABLE_REF(5)))
					{	/* BackEnd/c_emit.scm 156 */
						obj_t BgL_port1311z00_1797;

						BgL_port1311z00_1797 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
						{	/* BackEnd/c_emit.scm 156 */
							obj_t BgL_tmpz00_2923;

							BgL_tmpz00_2923 = ((obj_t) BgL_port1311z00_1797);
							bgl_display_string(BGl_string1963z00zzbackend_c_emitz00,
								BgL_tmpz00_2923);
						}
						{	/* BackEnd/c_emit.scm 156 */
							obj_t BgL_tmpz00_2926;

							BgL_tmpz00_2926 = ((obj_t) BgL_port1311z00_1797);
							return bgl_display_char(((unsigned char) 10), BgL_tmpz00_2926);
					}}
				else
					{	/* BackEnd/c_emit.scm 154 */
						if ((BgL_casezd2valuezd2_1795 == CNST_TABLE_REF(6)))
							{	/* BackEnd/c_emit.scm 154 */
								{	/* BackEnd/c_emit.scm 158 */
									obj_t BgL_port1312z00_1799;

									BgL_port1312z00_1799 =
										BGl_za2czd2portza2zd2zzbackend_c_emitz00;
									{	/* BackEnd/c_emit.scm 158 */
										obj_t BgL_tmpz00_2932;

										BgL_tmpz00_2932 = ((obj_t) BgL_port1312z00_1799);
										bgl_display_string(BGl_string1964z00zzbackend_c_emitz00,
											BgL_tmpz00_2932);
									}
									{	/* BackEnd/c_emit.scm 158 */
										obj_t BgL_tmpz00_2935;

										BgL_tmpz00_2935 = ((obj_t) BgL_port1312z00_1799);
										bgl_display_char(((unsigned char) 10), BgL_tmpz00_2935);
								}}
								{	/* BackEnd/c_emit.scm 159 */
									obj_t BgL_port1313z00_1800;

									BgL_port1313z00_1800 =
										BGl_za2czd2portza2zd2zzbackend_c_emitz00;
									{	/* BackEnd/c_emit.scm 159 */
										obj_t BgL_tmpz00_2938;

										BgL_tmpz00_2938 = ((obj_t) BgL_port1313z00_1800);
										bgl_display_string(BGl_string1965z00zzbackend_c_emitz00,
											BgL_tmpz00_2938);
									}
									{	/* BackEnd/c_emit.scm 159 */
										obj_t BgL_tmpz00_2941;

										BgL_tmpz00_2941 = ((obj_t) BgL_port1313z00_1800);
										return
											bgl_display_char(((unsigned char) 10), BgL_tmpz00_2941);
							}}}
						else
							{	/* BackEnd/c_emit.scm 154 */
								return
									BGl_errorz00zz__errorz00(BGl_string1966z00zzbackend_c_emitz00,
									BGl_string1967z00zzbackend_c_emitz00,
									BGl_za2garbagezd2collectorza2zd2zzengine_paramz00);
							}
					}
			}
		}

	}



/* &emit-garbage-collector-selection */
	obj_t
		BGl_z62emitzd2garbagezd2collectorzd2selectionzb0zzbackend_c_emitz00(obj_t
		BgL_envz00_2697)
	{
		{	/* BackEnd/c_emit.scm 152 */
			return BGl_emitzd2garbagezd2collectorzd2selectionzd2zzbackend_c_emitz00();
		}

	}



/* emit-include */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2includezd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 168 */
			{	/* BackEnd/c_emit.scm 169 */
				obj_t BgL_port1314z00_1801;

				BgL_port1314z00_1801 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 169 */
					obj_t BgL_tmpz00_2946;

					BgL_tmpz00_2946 = ((obj_t) BgL_port1314z00_1801);
					bgl_display_string(BGl_string1968z00zzbackend_c_emitz00,
						BgL_tmpz00_2946);
				}
				{	/* BackEnd/c_emit.scm 169 */
					obj_t BgL_tmpz00_2949;

					BgL_tmpz00_2949 = ((obj_t) BgL_port1314z00_1801);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_2949);
			}}
			{	/* BackEnd/c_emit.scm 171 */
				obj_t BgL_g1318z00_1802;

				BgL_g1318z00_1802 =
					bgl_reverse_bang(BGl_za2includezd2foreignza2zd2zzengine_paramz00);
				{
					obj_t BgL_l1316z00_1804;

					BgL_l1316z00_1804 = BgL_g1318z00_1802;
				BgL_zc3z04anonymousza31538ze3z87_1805:
					if (PAIRP(BgL_l1316z00_1804))
						{	/* BackEnd/c_emit.scm 172 */
							{	/* BackEnd/c_emit.scm 171 */
								obj_t BgL_iz00_1807;

								BgL_iz00_1807 = CAR(BgL_l1316z00_1804);
								{	/* BackEnd/c_emit.scm 171 */
									obj_t BgL_port1315z00_1808;

									BgL_port1315z00_1808 =
										BGl_za2czd2portza2zd2zzbackend_c_emitz00;
									{	/* BackEnd/c_emit.scm 171 */
										obj_t BgL_tmpz00_2956;

										BgL_tmpz00_2956 = ((obj_t) BgL_port1315z00_1808);
										bgl_display_string(BGl_string1969z00zzbackend_c_emitz00,
											BgL_tmpz00_2956);
									}
									bgl_display_obj(BgL_iz00_1807, BgL_port1315z00_1808);
									{	/* BackEnd/c_emit.scm 171 */
										obj_t BgL_tmpz00_2960;

										BgL_tmpz00_2960 = ((obj_t) BgL_port1315z00_1808);
										bgl_display_string(BGl_string1970z00zzbackend_c_emitz00,
											BgL_tmpz00_2960);
									}
									{	/* BackEnd/c_emit.scm 171 */
										obj_t BgL_tmpz00_2963;

										BgL_tmpz00_2963 = ((obj_t) BgL_port1315z00_1808);
										bgl_display_char(((unsigned char) 10), BgL_tmpz00_2963);
							}}}
							{
								obj_t BgL_l1316z00_2966;

								BgL_l1316z00_2966 = CDR(BgL_l1316z00_1804);
								BgL_l1316z00_1804 = BgL_l1316z00_2966;
								goto BgL_zc3z04anonymousza31538ze3z87_1805;
							}
						}
					else
						{	/* BackEnd/c_emit.scm 172 */
							((bool_t) 1);
						}
				}
			}
			{	/* BackEnd/c_emit.scm 174 */
				obj_t BgL_g1322z00_1811;

				BgL_g1322z00_1811 =
					bgl_reverse_bang
					(BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00);
				{
					obj_t BgL_l1320z00_1813;

					BgL_l1320z00_1813 = BgL_g1322z00_1811;
				BgL_zc3z04anonymousza31541ze3z87_1814:
					if (PAIRP(BgL_l1320z00_1813))
						{	/* BackEnd/c_emit.scm 175 */
							{	/* BackEnd/c_emit.scm 174 */
								obj_t BgL_iz00_1816;

								BgL_iz00_1816 = CAR(BgL_l1320z00_1813);
								{	/* BackEnd/c_emit.scm 174 */
									obj_t BgL_port1319z00_1817;

									BgL_port1319z00_1817 =
										BGl_za2czd2portza2zd2zzbackend_c_emitz00;
									{	/* BackEnd/c_emit.scm 174 */
										obj_t BgL_tmpz00_2972;

										BgL_tmpz00_2972 = ((obj_t) BgL_port1319z00_1817);
										bgl_display_string(BGl_string1969z00zzbackend_c_emitz00,
											BgL_tmpz00_2972);
									}
									bgl_display_obj(BgL_iz00_1816, BgL_port1319z00_1817);
									{	/* BackEnd/c_emit.scm 174 */
										obj_t BgL_tmpz00_2976;

										BgL_tmpz00_2976 = ((obj_t) BgL_port1319z00_1817);
										bgl_display_string(BGl_string1970z00zzbackend_c_emitz00,
											BgL_tmpz00_2976);
									}
									{	/* BackEnd/c_emit.scm 174 */
										obj_t BgL_tmpz00_2979;

										BgL_tmpz00_2979 = ((obj_t) BgL_port1319z00_1817);
										bgl_display_char(((unsigned char) 10), BgL_tmpz00_2979);
							}}}
							{
								obj_t BgL_l1320z00_2982;

								BgL_l1320z00_2982 = CDR(BgL_l1320z00_1813);
								BgL_l1320z00_1813 = BgL_l1320z00_2982;
								goto BgL_zc3z04anonymousza31541ze3z87_1814;
							}
						}
					else
						{	/* BackEnd/c_emit.scm 175 */
							((bool_t) 1);
						}
				}
			}
			return
				bgl_display_char(((unsigned char) 10),
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
		}

	}



/* &emit-include */
	obj_t BGl_z62emitzd2includezb0zzbackend_c_emitz00(obj_t BgL_envz00_2698)
	{
		{	/* BackEnd/c_emit.scm 168 */
			return BGl_emitzd2includezd2zzbackend_c_emitz00();
		}

	}



/* emit-debug-activation */
	BGL_EXPORTED_DEF obj_t
		BGl_emitzd2debugzd2activationz00zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 181 */
			{	/* BackEnd/c_emit.scm 182 */
				obj_t BgL_port1323z00_1820;

				BgL_port1323z00_1820 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 182 */
					obj_t BgL_tmpz00_2986;

					BgL_tmpz00_2986 = ((obj_t) BgL_port1323z00_1820);
					bgl_display_string(BGl_string1971z00zzbackend_c_emitz00,
						BgL_tmpz00_2986);
				}
				{	/* BackEnd/c_emit.scm 182 */
					obj_t BgL_tmpz00_2989;

					BgL_tmpz00_2989 = ((obj_t) BgL_port1323z00_1820);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_2989);
			}}
			{	/* BackEnd/c_emit.scm 183 */
				obj_t BgL_port1324z00_1821;

				BgL_port1324z00_1821 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 183 */
					obj_t BgL_tmpz00_2992;

					BgL_tmpz00_2992 = ((obj_t) BgL_port1324z00_1821);
					bgl_display_string(BGl_string1972z00zzbackend_c_emitz00,
						BgL_tmpz00_2992);
				}
				{	/* BackEnd/c_emit.scm 183 */
					obj_t BgL_tmpz00_2995;

					BgL_tmpz00_2995 = ((obj_t) BgL_port1324z00_1821);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_2995);
			}}
			return
				bgl_display_char(((unsigned char) 10),
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
		}

	}



/* &emit-debug-activation */
	obj_t BGl_z62emitzd2debugzd2activationz62zzbackend_c_emitz00(obj_t
		BgL_envz00_2699)
	{
		{	/* BackEnd/c_emit.scm 181 */
			return BGl_emitzd2debugzd2activationz00zzbackend_c_emitz00();
		}

	}



/* emit-unsafe-activation */
	BGL_EXPORTED_DEF obj_t
		BGl_emitzd2unsafezd2activationz00zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 189 */
			{	/* BackEnd/c_emit.scm 190 */
				obj_t BgL_port1325z00_1822;

				BgL_port1325z00_1822 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 190 */
					obj_t BgL_tmpz00_3000;

					BgL_tmpz00_3000 = ((obj_t) BgL_port1325z00_1822);
					bgl_display_string(BGl_string1973z00zzbackend_c_emitz00,
						BgL_tmpz00_3000);
				}
				{	/* BackEnd/c_emit.scm 190 */
					obj_t BgL_tmpz00_3003;

					BgL_tmpz00_3003 = ((obj_t) BgL_port1325z00_1822);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_3003);
			}}
			{	/* BackEnd/c_emit.scm 191 */
				obj_t BgL_port1326z00_1823;

				BgL_port1326z00_1823 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 191 */
					obj_t BgL_tmpz00_3006;

					BgL_tmpz00_3006 = ((obj_t) BgL_port1326z00_1823);
					bgl_display_string(BGl_string1974z00zzbackend_c_emitz00,
						BgL_tmpz00_3006);
				}
				{	/* BackEnd/c_emit.scm 191 */
					obj_t BgL_tmpz00_3009;

					BgL_tmpz00_3009 = ((obj_t) BgL_port1326z00_1823);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_3009);
			}}
			return
				bgl_display_char(((unsigned char) 10),
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
		}

	}



/* &emit-unsafe-activation */
	obj_t BGl_z62emitzd2unsafezd2activationz62zzbackend_c_emitz00(obj_t
		BgL_envz00_2700)
	{
		{	/* BackEnd/c_emit.scm 189 */
			return BGl_emitzd2unsafezd2activationz00zzbackend_c_emitz00();
		}

	}



/* emit-trace-activation */
	BGL_EXPORTED_DEF obj_t
		BGl_emitzd2tracezd2activationz00zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 197 */
			{	/* BackEnd/c_emit.scm 198 */
				obj_t BgL_port1327z00_1824;

				BgL_port1327z00_1824 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 198 */
					obj_t BgL_tmpz00_3014;

					BgL_tmpz00_3014 = ((obj_t) BgL_port1327z00_1824);
					bgl_display_string(BGl_string1975z00zzbackend_c_emitz00,
						BgL_tmpz00_3014);
				}
				{	/* BackEnd/c_emit.scm 198 */
					obj_t BgL_tmpz00_3017;

					BgL_tmpz00_3017 = ((obj_t) BgL_port1327z00_1824);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_3017);
			}}
			{	/* BackEnd/c_emit.scm 199 */
				obj_t BgL_port1328z00_1825;

				BgL_port1328z00_1825 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 199 */
					obj_t BgL_tmpz00_3020;

					BgL_tmpz00_3020 = ((obj_t) BgL_port1328z00_1825);
					bgl_display_string(BGl_string1976z00zzbackend_c_emitz00,
						BgL_tmpz00_3020);
				}
				bgl_display_obj(BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00,
					BgL_port1328z00_1825);
				{	/* BackEnd/c_emit.scm 199 */
					obj_t BgL_tmpz00_3024;

					BgL_tmpz00_3024 = ((obj_t) BgL_port1328z00_1825);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_3024);
			}}
			return
				bgl_display_char(((unsigned char) 10),
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
		}

	}



/* &emit-trace-activation */
	obj_t BGl_z62emitzd2tracezd2activationz62zzbackend_c_emitz00(obj_t
		BgL_envz00_2701)
	{
		{	/* BackEnd/c_emit.scm 197 */
			return BGl_emitzd2tracezd2activationz00zzbackend_c_emitz00();
		}

	}



/* emit-main */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2mainzd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 205 */
			BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BUNSPEC);
			if (PAIRP(BGl_za2bigloozd2librarieszd2czd2setupza2zd2zzengine_paramz00))
				{	/* BackEnd/c_emit.scm 208 */
					{	/* BackEnd/c_emit.scm 209 */
						obj_t BgL_port1329z00_1827;

						BgL_port1329z00_1827 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
						{	/* BackEnd/c_emit.scm 209 */
							obj_t BgL_tmpz00_3032;

							BgL_tmpz00_3032 = ((obj_t) BgL_port1329z00_1827);
							bgl_display_string(BGl_string1977z00zzbackend_c_emitz00,
								BgL_tmpz00_3032);
						}
						{	/* BackEnd/c_emit.scm 209 */
							obj_t BgL_tmpz00_3035;

							BgL_tmpz00_3035 = ((obj_t) BgL_port1329z00_1827);
							bgl_display_char(((unsigned char) 10), BgL_tmpz00_3035);
					}}
					{
						obj_t BgL_l1331z00_1829;

						BgL_l1331z00_1829 =
							BGl_za2bigloozd2librarieszd2czd2setupza2zd2zzengine_paramz00;
					BgL_zc3z04anonymousza31547ze3z87_1830:
						if (PAIRP(BgL_l1331z00_1829))
							{	/* BackEnd/c_emit.scm 210 */
								{	/* BackEnd/c_emit.scm 211 */
									obj_t BgL_fz00_1832;

									BgL_fz00_1832 = CAR(BgL_l1331z00_1829);
									{	/* BackEnd/c_emit.scm 211 */
										obj_t BgL_port1330z00_1833;

										BgL_port1330z00_1833 =
											BGl_za2czd2portza2zd2zzbackend_c_emitz00;
										{	/* BackEnd/c_emit.scm 211 */
											obj_t BgL_tmpz00_3041;

											BgL_tmpz00_3041 = ((obj_t) BgL_port1330z00_1833);
											bgl_display_string(BGl_string1978z00zzbackend_c_emitz00,
												BgL_tmpz00_3041);
										}
										bgl_display_obj(BgL_fz00_1832, BgL_port1330z00_1833);
										{	/* BackEnd/c_emit.scm 211 */
											obj_t BgL_tmpz00_3045;

											BgL_tmpz00_3045 = ((obj_t) BgL_port1330z00_1833);
											bgl_display_string(BGl_string1979z00zzbackend_c_emitz00,
												BgL_tmpz00_3045);
										}
										{	/* BackEnd/c_emit.scm 211 */
											obj_t BgL_tmpz00_3048;

											BgL_tmpz00_3048 = ((obj_t) BgL_port1330z00_1833);
											bgl_display_char(((unsigned char) 10), BgL_tmpz00_3048);
								}}}
								{
									obj_t BgL_l1331z00_3051;

									BgL_l1331z00_3051 = CDR(BgL_l1331z00_1829);
									BgL_l1331z00_1829 = BgL_l1331z00_3051;
									goto BgL_zc3z04anonymousza31547ze3z87_1830;
								}
							}
						else
							{	/* BackEnd/c_emit.scm 210 */
								((bool_t) 1);
							}
					}
					bgl_display_char(((unsigned char) 10),
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
			else
				{	/* BackEnd/c_emit.scm 208 */
					BFALSE;
				}
			{	/* BackEnd/c_emit.scm 217 */
				obj_t BgL_port1333z00_1836;

				BgL_port1333z00_1836 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 217 */
					obj_t BgL_tmpz00_3054;

					BgL_tmpz00_3054 = ((obj_t) BgL_port1333z00_1836);
					bgl_display_string(BGl_string1980z00zzbackend_c_emitz00,
						BgL_tmpz00_3054);
				}
				{	/* BackEnd/c_emit.scm 217 */
					obj_t BgL_tmpz00_3057;

					BgL_tmpz00_3057 = ((obj_t) BgL_port1333z00_1836);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_3057);
			}}
			{	/* BackEnd/c_emit.scm 218 */
				obj_t BgL_port1334z00_1837;

				BgL_port1334z00_1837 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 218 */
					obj_t BgL_tmpz00_3060;

					BgL_tmpz00_3060 = ((obj_t) BgL_port1334z00_1837);
					bgl_display_string(BGl_string1981z00zzbackend_c_emitz00,
						BgL_tmpz00_3060);
				}
				{	/* BackEnd/c_emit.scm 218 */
					obj_t BgL_tmpz00_3063;

					BgL_tmpz00_3063 = ((obj_t) BgL_port1334z00_1837);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_3063);
			}}
			{
				obj_t BgL_l1336z00_1839;

				BgL_l1336z00_1839 =
					BGl_za2bigloozd2librarieszd2czd2setupza2zd2zzengine_paramz00;
			BgL_zc3z04anonymousza31553ze3z87_1840:
				if (PAIRP(BgL_l1336z00_1839))
					{	/* BackEnd/c_emit.scm 219 */
						{	/* BackEnd/c_emit.scm 220 */
							obj_t BgL_fz00_1842;

							BgL_fz00_1842 = CAR(BgL_l1336z00_1839);
							{	/* BackEnd/c_emit.scm 220 */
								obj_t BgL_port1335z00_1843;

								BgL_port1335z00_1843 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
								bgl_display_obj(BgL_fz00_1842, BgL_port1335z00_1843);
								{	/* BackEnd/c_emit.scm 220 */
									obj_t BgL_tmpz00_3070;

									BgL_tmpz00_3070 = ((obj_t) BgL_port1335z00_1843);
									bgl_display_string(BGl_string1982z00zzbackend_c_emitz00,
										BgL_tmpz00_3070);
								}
								{	/* BackEnd/c_emit.scm 220 */
									obj_t BgL_tmpz00_3073;

									BgL_tmpz00_3073 = ((obj_t) BgL_port1335z00_1843);
									bgl_display_char(((unsigned char) 10), BgL_tmpz00_3073);
						}}}
						{
							obj_t BgL_l1336z00_3076;

							BgL_l1336z00_3076 = CDR(BgL_l1336z00_1839);
							BgL_l1336z00_1839 = BgL_l1336z00_3076;
							goto BgL_zc3z04anonymousza31553ze3z87_1840;
						}
					}
				else
					{	/* BackEnd/c_emit.scm 219 */
						((bool_t) 1);
					}
			}
			{	/* BackEnd/c_emit.scm 222 */
				obj_t BgL_port1338z00_1846;

				BgL_port1338z00_1846 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 222 */
					obj_t BgL_tmpz00_3078;

					BgL_tmpz00_3078 = ((obj_t) BgL_port1338z00_1846);
					bgl_display_string(BGl_string1983z00zzbackend_c_emitz00,
						BgL_tmpz00_3078);
				}
				{	/* BackEnd/c_emit.scm 222 */
					obj_t BgL_tmpz00_3081;

					BgL_tmpz00_3081 = ((obj_t) BgL_port1338z00_1846);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_3081);
			}}
			{	/* BackEnd/c_emit.scm 228 */
				obj_t BgL_port1339z00_1847;

				BgL_port1339z00_1847 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 228 */
					obj_t BgL_tmpz00_3084;

					BgL_tmpz00_3084 = ((obj_t) BgL_port1339z00_1847);
					bgl_display_string(BGl_string1984z00zzbackend_c_emitz00,
						BgL_tmpz00_3084);
				}
				{	/* BackEnd/c_emit.scm 228 */
					obj_t BgL_tmpz00_3087;

					BgL_tmpz00_3087 = ((obj_t) BgL_port1339z00_1847);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_3087);
			}}
			BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BUNSPEC);
			{	/* BackEnd/c_emit.scm 230 */
				obj_t BgL_port1340z00_1848;

				BgL_port1340z00_1848 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_emit.scm 230 */
					obj_t BgL_tmpz00_3091;

					BgL_tmpz00_3091 = ((obj_t) BgL_port1340z00_1848);
					bgl_display_string(BGl_string1985z00zzbackend_c_emitz00,
						BgL_tmpz00_3091);
				}
				{	/* BackEnd/c_emit.scm 230 */
					obj_t BgL_tmpz00_3094;

					BgL_tmpz00_3094 = ((obj_t) BgL_port1340z00_1848);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_3094);
			}}
			{	/* BackEnd/c_emit.scm 232 */
				obj_t BgL_list1560z00_1849;

				BgL_list1560z00_1849 =
					MAKE_YOUNG_PAIR(BGl_za2userzd2heapzd2siza7eza2za7zzengine_paramz00,
					BNIL);
				BGl_fprintfz00zz__r4_output_6_10_3z00
					(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
					BGl_string1986z00zzbackend_c_emitz00, BgL_list1560z00_1849);
			}
			return
				bgl_display_char(((unsigned char) 10),
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
		}

	}



/* &emit-main */
	obj_t BGl_z62emitzd2mainzb0zzbackend_c_emitz00(obj_t BgL_envz00_2702)
	{
		{	/* BackEnd/c_emit.scm 205 */
			return BGl_emitzd2mainzd2zzbackend_c_emitz00();
		}

	}



/* emit-dlopen-init */
	BGL_EXPORTED_DEF obj_t
		BGl_emitzd2dlopenzd2initz00zzbackend_c_emitz00(BgL_globalz00_bglt
		BgL_globalz00_6, obj_t BgL_initz00_7)
	{
		{	/* BackEnd/c_emit.scm 239 */
			{	/* BackEnd/c_emit.scm 240 */
				obj_t BgL_symz00_1850;

				if (BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(BgL_initz00_7))
					{	/* BackEnd/c_emit.scm 240 */
						BgL_symz00_1850 = bigloo_mangle(BgL_initz00_7);
					}
				else
					{	/* BackEnd/c_emit.scm 240 */
						BgL_symz00_1850 = BgL_initz00_7;
					}
				BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BUNSPEC);
				{	/* BackEnd/c_emit.scm 244 */
					obj_t BgL_port1341z00_1851;

					BgL_port1341z00_1851 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_emit.scm 244 */
						obj_t BgL_tmpz00_3105;

						BgL_tmpz00_3105 = ((obj_t) BgL_port1341z00_1851);
						bgl_display_string(BGl_string1987z00zzbackend_c_emitz00,
							BgL_tmpz00_3105);
					}
					bgl_display_obj(BgL_symz00_1850, BgL_port1341z00_1851);
					{	/* BackEnd/c_emit.scm 244 */
						obj_t BgL_tmpz00_3109;

						BgL_tmpz00_3109 = ((obj_t) BgL_port1341z00_1851);
						bgl_display_string(BGl_string1988z00zzbackend_c_emitz00,
							BgL_tmpz00_3109);
					}
					{	/* BackEnd/c_emit.scm 244 */
						obj_t BgL_tmpz00_3112;

						BgL_tmpz00_3112 = ((obj_t) BgL_port1341z00_1851);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_3112);
				}}
				BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BUNSPEC);
				{	/* BackEnd/c_emit.scm 246 */
					obj_t BgL_port1342z00_1852;

					BgL_port1342z00_1852 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_emit.scm 246 */
						obj_t BgL_tmpz00_3116;

						BgL_tmpz00_3116 = ((obj_t) BgL_port1342z00_1852);
						bgl_display_string(BGl_string1989z00zzbackend_c_emitz00,
							BgL_tmpz00_3116);
					}
					{	/* BackEnd/c_emit.scm 246 */
						obj_t BgL_arg1561z00_1853;

						BgL_arg1561z00_1853 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_globalz00_6)))->BgL_namez00);
						bgl_display_obj(BgL_arg1561z00_1853, BgL_port1342z00_1852);
					}
					{	/* BackEnd/c_emit.scm 246 */
						obj_t BgL_tmpz00_3122;

						BgL_tmpz00_3122 = ((obj_t) BgL_port1342z00_1852);
						bgl_display_string(BGl_string1990z00zzbackend_c_emitz00,
							BgL_tmpz00_3122);
					}
					bgl_display_obj(CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00),
						BgL_port1342z00_1852);
					{	/* BackEnd/c_emit.scm 246 */
						obj_t BgL_tmpz00_3127;

						BgL_tmpz00_3127 = ((obj_t) BgL_port1342z00_1852);
						bgl_display_string(BGl_string1991z00zzbackend_c_emitz00,
							BgL_tmpz00_3127);
					}
					{	/* BackEnd/c_emit.scm 246 */
						obj_t BgL_tmpz00_3130;

						BgL_tmpz00_3130 = ((obj_t) BgL_port1342z00_1852);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_3130);
				}}
				{	/* BackEnd/c_emit.scm 248 */
					obj_t BgL_port1343z00_1855;

					BgL_port1343z00_1855 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_emit.scm 248 */
						obj_t BgL_tmpz00_3133;

						BgL_tmpz00_3133 = ((obj_t) BgL_port1343z00_1855);
						bgl_display_string(BGl_string1992z00zzbackend_c_emitz00,
							BgL_tmpz00_3133);
					}
					{	/* BackEnd/c_emit.scm 248 */
						obj_t BgL_tmpz00_3136;

						BgL_tmpz00_3136 = ((obj_t) BgL_port1343z00_1855);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_3136);
				}}
				{	/* BackEnd/c_emit.scm 249 */
					obj_t BgL_port1344z00_1856;

					BgL_port1344z00_1856 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_emit.scm 249 */
						obj_t BgL_tmpz00_3139;

						BgL_tmpz00_3139 = ((obj_t) BgL_port1344z00_1856);
						bgl_display_string(BGl_string1993z00zzbackend_c_emitz00,
							BgL_tmpz00_3139);
					}
					bgl_display_obj(
						(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_6))->BgL_modulez00),
						BgL_port1344z00_1856);
					{	/* BackEnd/c_emit.scm 249 */
						obj_t BgL_tmpz00_3144;

						BgL_tmpz00_3144 = ((obj_t) BgL_port1344z00_1856);
						bgl_display_string(BGl_string1994z00zzbackend_c_emitz00,
							BgL_tmpz00_3144);
					}
					{	/* BackEnd/c_emit.scm 249 */
						obj_t BgL_tmpz00_3147;

						BgL_tmpz00_3147 = ((obj_t) BgL_port1344z00_1856);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_3147);
				}}
				{	/* BackEnd/c_emit.scm 251 */
					obj_t BgL_port1345z00_1858;

					BgL_port1345z00_1858 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_emit.scm 251 */
						obj_t BgL_tmpz00_3150;

						BgL_tmpz00_3150 = ((obj_t) BgL_port1345z00_1858);
						bgl_display_string(BGl_string1995z00zzbackend_c_emitz00,
							BgL_tmpz00_3150);
					}
					{	/* BackEnd/c_emit.scm 251 */
						obj_t BgL_tmpz00_3153;

						BgL_tmpz00_3153 = ((obj_t) BgL_port1345z00_1858);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_3153);
				}}
				BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BUNSPEC);
				{	/* BackEnd/c_emit.scm 253 */
					obj_t BgL_port1346z00_1859;

					BgL_port1346z00_1859 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_emit.scm 253 */
						obj_t BgL_tmpz00_3157;

						BgL_tmpz00_3157 = ((obj_t) BgL_port1346z00_1859);
						bgl_display_string(BGl_string1996z00zzbackend_c_emitz00,
							BgL_tmpz00_3157);
					}
					{	/* BackEnd/c_emit.scm 253 */
						obj_t BgL_tmpz00_3160;

						BgL_tmpz00_3160 = ((obj_t) BgL_port1346z00_1859);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_3160);
				}}
				return
					bgl_display_char(((unsigned char) 10),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
		}}

	}



/* &emit-dlopen-init */
	obj_t BGl_z62emitzd2dlopenzd2initz62zzbackend_c_emitz00(obj_t BgL_envz00_2703,
		obj_t BgL_globalz00_2704, obj_t BgL_initz00_2705)
	{
		{	/* BackEnd/c_emit.scm 239 */
			return
				BGl_emitzd2dlopenzd2initz00zzbackend_c_emitz00(
				((BgL_globalz00_bglt) BgL_globalz00_2704), BgL_initz00_2705);
		}

	}



/* llong->c-iso */
	BGL_EXPORTED_DEF obj_t
		BGl_llongzd2ze3czd2isoze3zzbackend_c_emitz00(BGL_LONGLONG_T BgL_llongz00_8)
	{
		{	/* BackEnd/c_emit.scm 259 */
			if ((BgL_llongz00_8 >= ((BGL_LONGLONG_T) 0)))
				{	/* BackEnd/c_emit.scm 274 */
					BGL_TAIL return
						BGl_positiveze70ze7zzbackend_c_emitz00(BgL_llongz00_8);
				}
			else
				{	/* BackEnd/c_emit.scm 276 */
					obj_t BgL_arg1571z00_1863;

					{	/* BackEnd/c_emit.scm 276 */
						BGL_LONGLONG_T BgL_arg1573z00_1864;

						if ((BgL_llongz00_8 < ((BGL_LONGLONG_T) 0)))
							{	/* BackEnd/c_emit.scm 276 */
								BgL_arg1573z00_1864 = NEG(BgL_llongz00_8);
							}
						else
							{	/* BackEnd/c_emit.scm 276 */
								BgL_arg1573z00_1864 = BgL_llongz00_8;
							}
						BgL_arg1571z00_1863 =
							BGl_positiveze70ze7zzbackend_c_emitz00(BgL_arg1573z00_1864);
					}
					return
						string_append_3(BGl_string1997z00zzbackend_c_emitz00,
						BgL_arg1571z00_1863, BGl_string1998z00zzbackend_c_emitz00);
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzbackend_c_emitz00(BGL_LONGLONG_T BgL_shiftz00_2716,
		obj_t BgL_sshiftz00_2715, long BgL_bitsz00_2714,
		BGL_LONGLONG_T BgL_maskz00_2713, BGL_LONGLONG_T BgL_allongz00_1872)
	{
		{	/* BackEnd/c_emit.scm 265 */
			if ((BgL_allongz00_1872 >= BgL_shiftz00_2716))
				{	/* BackEnd/c_emit.scm 269 */
					obj_t BgL_arg1584z00_1875;
					obj_t BgL_arg1585z00_1876;

					BgL_arg1584z00_1875 =
						BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
						(BgL_allongz00_1872 & BgL_maskz00_2713), BNIL);
					BgL_arg1585z00_1876 =
						BGl_loopze70ze7zzbackend_c_emitz00(BgL_shiftz00_2716,
						BgL_sshiftz00_2715, BgL_bitsz00_2714, BgL_maskz00_2713,
						(BgL_allongz00_1872 >> (int) (BgL_bitsz00_2714)));
					{	/* BackEnd/c_emit.scm 267 */
						obj_t BgL_list1586z00_1877;

						{	/* BackEnd/c_emit.scm 267 */
							obj_t BgL_arg1589z00_1878;

							{	/* BackEnd/c_emit.scm 267 */
								obj_t BgL_arg1591z00_1879;

								{	/* BackEnd/c_emit.scm 267 */
									obj_t BgL_arg1593z00_1880;

									{	/* BackEnd/c_emit.scm 267 */
										obj_t BgL_arg1594z00_1881;

										{	/* BackEnd/c_emit.scm 267 */
											obj_t BgL_arg1595z00_1882;

											{	/* BackEnd/c_emit.scm 267 */
												obj_t BgL_arg1602z00_1883;

												BgL_arg1602z00_1883 =
													MAKE_YOUNG_PAIR(BGl_string1999z00zzbackend_c_emitz00,
													BNIL);
												BgL_arg1595z00_1882 =
													MAKE_YOUNG_PAIR(BgL_arg1585z00_1876,
													BgL_arg1602z00_1883);
											}
											BgL_arg1594z00_1881 =
												MAKE_YOUNG_PAIR(BGl_string2000z00zzbackend_c_emitz00,
												BgL_arg1595z00_1882);
										}
										BgL_arg1593z00_1880 =
											MAKE_YOUNG_PAIR(BgL_sshiftz00_2715, BgL_arg1594z00_1881);
									}
									BgL_arg1591z00_1879 =
										MAKE_YOUNG_PAIR(BGl_string2001z00zzbackend_c_emitz00,
										BgL_arg1593z00_1880);
								}
								BgL_arg1589z00_1878 =
									MAKE_YOUNG_PAIR(BgL_arg1584z00_1875, BgL_arg1591z00_1879);
							}
							BgL_list1586z00_1877 =
								MAKE_YOUNG_PAIR(BGl_string2002z00zzbackend_c_emitz00,
								BgL_arg1589z00_1878);
						}
						return
							BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1586z00_1877);
					}
				}
			else
				{	/* BackEnd/c_emit.scm 266 */
					return
						string_append_3(BGl_string2003z00zzbackend_c_emitz00,
						BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
						(BgL_allongz00_1872, BNIL), BGl_string1998z00zzbackend_c_emitz00);
				}
		}

	}



/* positive~0 */
	obj_t BGl_positiveze70ze7zzbackend_c_emitz00(BGL_LONGLONG_T
		BgL_allongz00_1865)
	{
		{	/* BackEnd/c_emit.scm 273 */
			{	/* BackEnd/c_emit.scm 261 */
				BGL_LONGLONG_T BgL_shiftz00_1868;

				BgL_shiftz00_1868 = (((BGL_LONGLONG_T) 1) << (int) (16L));
				{	/* BackEnd/c_emit.scm 262 */
					obj_t BgL_sshiftz00_1869;

					BgL_sshiftz00_1869 =
						BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
						(BgL_shiftz00_1868, BNIL);
					{	/* BackEnd/c_emit.scm 263 */
						BGL_LONGLONG_T BgL_maskz00_1870;

						BgL_maskz00_1870 = (BgL_shiftz00_1868 - ((BGL_LONGLONG_T) 1));
						{	/* BackEnd/c_emit.scm 264 */

							return
								BGl_loopze70ze7zzbackend_c_emitz00(BgL_shiftz00_1868,
								BgL_sshiftz00_1869, 16L, BgL_maskz00_1870, BgL_allongz00_1865);
						}
					}
				}
			}
		}

	}



/* &llong->c-iso */
	obj_t BGl_z62llongzd2ze3czd2isoz81zzbackend_c_emitz00(obj_t BgL_envz00_2706,
		obj_t BgL_llongz00_2707)
	{
		{	/* BackEnd/c_emit.scm 259 */
			return
				BGl_llongzd2ze3czd2isoze3zzbackend_c_emitz00(BLLONG_TO_LLONG
				(BgL_llongz00_2707));
		}

	}



/* emit-atom-value */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00(obj_t
		BgL_valuez00_9, BgL_typez00_bglt BgL_typez00_10)
	{
		{	/* BackEnd/c_emit.scm 281 */
			if (BOOLEANP(BgL_valuez00_9))
				{	/* BackEnd/c_emit.scm 283 */
					bgl_display_string(BGl_string2004z00zzbackend_c_emitz00,
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					{	/* BackEnd/c_emit.scm 285 */
						obj_t BgL_arg1615z00_1893;

						{	/* BackEnd/c_emit.scm 285 */
							obj_t BgL_arg1616z00_1894;

							{	/* BackEnd/c_emit.scm 285 */
								BgL_typez00_bglt BgL_oz00_2459;

								BgL_oz00_2459 =
									((BgL_typez00_bglt) BGl_za2boolza2z00zztype_cachez00);
								BgL_arg1616z00_1894 =
									(((BgL_typez00_bglt) COBJECT(BgL_oz00_2459))->BgL_namez00);
							}
							BgL_arg1615z00_1893 =
								BGl_stringzd2sanszd2z42z42zztype_toolsz00(BgL_arg1616z00_1894);
						}
						bgl_display_obj(BgL_arg1615z00_1893,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
					bgl_display_char(((unsigned char) ')'),
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					{	/* BackEnd/c_emit.scm 287 */
						long BgL_arg1625z00_1895;

						if (CBOOL(BgL_valuez00_9))
							{	/* BackEnd/c_emit.scm 287 */
								BgL_arg1625z00_1895 = 1L;
							}
						else
							{	/* BackEnd/c_emit.scm 287 */
								BgL_arg1625z00_1895 = 0L;
							}
						bgl_display_obj(BINT(BgL_arg1625z00_1895),
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
					return
						bgl_display_char(((unsigned char) ')'),
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
			else
				{	/* BackEnd/c_emit.scm 283 */
					if (NULLP(BgL_valuez00_9))
						{	/* BackEnd/c_emit.scm 289 */
							return
								bgl_display_string(BGl_string2005z00zzbackend_c_emitz00,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
					else
						{	/* BackEnd/c_emit.scm 289 */
							if (CHARP(BgL_valuez00_9))
								{	/* BackEnd/c_emit.scm 291 */
									bgl_display_string(BGl_string2004z00zzbackend_c_emitz00,
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									{	/* BackEnd/c_emit.scm 293 */
										obj_t BgL_arg1629z00_1898;

										{	/* BackEnd/c_emit.scm 293 */
											obj_t BgL_arg1630z00_1899;

											{	/* BackEnd/c_emit.scm 293 */
												BgL_typez00_bglt BgL_oz00_2468;

												BgL_oz00_2468 =
													((BgL_typez00_bglt) BGl_za2charza2z00zztype_cachez00);
												BgL_arg1630z00_1899 =
													(((BgL_typez00_bglt) COBJECT(BgL_oz00_2468))->
													BgL_namez00);
											}
											BgL_arg1629z00_1898 =
												BGl_stringzd2sanszd2z42z42zztype_toolsz00
												(BgL_arg1630z00_1899);
										}
										bgl_display_obj(BgL_arg1629z00_1898,
											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									}
									bgl_display_string(BGl_string1998z00zzbackend_c_emitz00,
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									if (((CCHAR(BgL_valuez00_9)) == 0L))
										{	/* BackEnd/c_emit.scm 296 */
											bgl_display_string(BGl_string2006z00zzbackend_c_emitz00,
												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										}
									else
										{	/* BackEnd/c_emit.scm 296 */
											if ((CCHAR(BgL_valuez00_9) == ((unsigned char) '\'')))
												{	/* BackEnd/c_emit.scm 298 */
													bgl_display_string
														(BGl_string2007z00zzbackend_c_emitz00,
														BGl_za2czd2portza2zd2zzbackend_c_emitz00);
												}
											else
												{	/* BackEnd/c_emit.scm 298 */
													if ((CCHAR(BgL_valuez00_9) == ((unsigned char) '\\')))
														{	/* BackEnd/c_emit.scm 300 */
															bgl_display_string
																(BGl_string2008z00zzbackend_c_emitz00,
																BGl_za2czd2portza2zd2zzbackend_c_emitz00);
														}
													else
														{	/* BackEnd/c_emit.scm 302 */
															bool_t BgL_test2168z00_3235;

															if (((CCHAR(BgL_valuez00_9)) >= 32L))
																{	/* BackEnd/c_emit.scm 302 */
																	BgL_test2168z00_3235 =
																		((CCHAR(BgL_valuez00_9)) < 128L);
																}
															else
																{	/* BackEnd/c_emit.scm 302 */
																	BgL_test2168z00_3235 = ((bool_t) 0);
																}
															if (BgL_test2168z00_3235)
																{	/* BackEnd/c_emit.scm 302 */
																	bgl_display_char(((unsigned char) '\''),
																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																	{	/* BackEnd/c_emit.scm 304 */
																		unsigned char BgL_tmpz00_3244;

																		BgL_tmpz00_3244 = CCHAR(BgL_valuez00_9);
																		bgl_display_char(BgL_tmpz00_3244,
																			BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																	}
																	bgl_display_char(((unsigned char) '\''),
																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																}
															else
																{	/* BackEnd/c_emit.scm 307 */
																	obj_t BgL_arg1654z00_1909;

																	{	/* BackEnd/c_emit.scm 307 */

																		BgL_arg1654z00_1909 =
																			BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																			((CCHAR(BgL_valuez00_9)), 10L);
																	}
																	bgl_display_obj(BgL_arg1654z00_1909,
																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																}
														}
												}
										}
									return
										bgl_display_char(((unsigned char) ')'),
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
								}
							else
								{	/* BackEnd/c_emit.scm 291 */
									if (BGL_INT8P(BgL_valuez00_9))
										{	/* BackEnd/c_emit.scm 309 */
											bgl_display_string(BGl_string2009z00zzbackend_c_emitz00,
												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
											{	/* BackEnd/c_emit.scm 311 */
												long BgL_arg1681z00_1918;

												{	/* BackEnd/c_emit.scm 311 */
													int8_t BgL_xz00_2498;

													BgL_xz00_2498 = BGL_BINT8_TO_INT8(BgL_valuez00_9);
													{	/* BackEnd/c_emit.scm 311 */
														long BgL_arg1451z00_2499;

														BgL_arg1451z00_2499 = (long) (BgL_xz00_2498);
														BgL_arg1681z00_1918 = (long) (BgL_arg1451z00_2499);
												}}
												bgl_display_obj(BINT(BgL_arg1681z00_1918),
													BGl_za2czd2portza2zd2zzbackend_c_emitz00);
											}
											return
												bgl_display_char(((unsigned char) ')'),
												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										}
									else
										{	/* BackEnd/c_emit.scm 309 */
											if (BGL_UINT8P(BgL_valuez00_9))
												{	/* BackEnd/c_emit.scm 313 */
													bgl_display_string
														(BGl_string2010z00zzbackend_c_emitz00,
														BGl_za2czd2portza2zd2zzbackend_c_emitz00);
													{	/* BackEnd/c_emit.scm 315 */
														long BgL_arg1688z00_1920;

														{	/* BackEnd/c_emit.scm 315 */
															uint8_t BgL_xz00_2505;

															BgL_xz00_2505 =
																BGL_BUINT8_TO_UINT8(BgL_valuez00_9);
															{	/* BackEnd/c_emit.scm 315 */
																long BgL_arg1450z00_2506;

																BgL_arg1450z00_2506 = (long) (BgL_xz00_2505);
																BgL_arg1688z00_1920 =
																	(long) (BgL_arg1450z00_2506);
														}}
														bgl_display_obj(BINT(BgL_arg1688z00_1920),
															BGl_za2czd2portza2zd2zzbackend_c_emitz00);
													}
													return
														bgl_display_char(((unsigned char) ')'),
														BGl_za2czd2portza2zd2zzbackend_c_emitz00);
												}
											else
												{	/* BackEnd/c_emit.scm 313 */
													if (BGL_INT16P(BgL_valuez00_9))
														{	/* BackEnd/c_emit.scm 317 */
															bgl_display_string
																(BGl_string2011z00zzbackend_c_emitz00,
																BGl_za2czd2portza2zd2zzbackend_c_emitz00);
															{	/* BackEnd/c_emit.scm 319 */
																long BgL_arg1691z00_1922;

																{	/* BackEnd/c_emit.scm 319 */
																	int16_t BgL_xz00_2512;

																	BgL_xz00_2512 =
																		BGL_BINT16_TO_INT16(BgL_valuez00_9);
																	{	/* BackEnd/c_emit.scm 319 */
																		long BgL_arg1449z00_2513;

																		BgL_arg1449z00_2513 =
																			(long) (BgL_xz00_2512);
																		BgL_arg1691z00_1922 =
																			(long) (BgL_arg1449z00_2513);
																}}
																bgl_display_obj(BINT(BgL_arg1691z00_1922),
																	BGl_za2czd2portza2zd2zzbackend_c_emitz00);
															}
															return
																bgl_display_char(((unsigned char) ')'),
																BGl_za2czd2portza2zd2zzbackend_c_emitz00);
														}
													else
														{	/* BackEnd/c_emit.scm 317 */
															if (BGL_UINT16P(BgL_valuez00_9))
																{	/* BackEnd/c_emit.scm 321 */
																	bgl_display_string
																		(BGl_string2012z00zzbackend_c_emitz00,
																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																	{	/* BackEnd/c_emit.scm 323 */
																		long BgL_arg1699z00_1924;

																		{	/* BackEnd/c_emit.scm 323 */
																			uint16_t BgL_xz00_2519;

																			BgL_xz00_2519 =
																				BGL_BUINT16_TO_UINT16(BgL_valuez00_9);
																			{	/* BackEnd/c_emit.scm 323 */
																				long BgL_arg1447z00_2520;

																				BgL_arg1447z00_2520 =
																					(long) (BgL_xz00_2519);
																				BgL_arg1699z00_1924 =
																					(long) (BgL_arg1447z00_2520);
																		}}
																		bgl_display_obj(BINT(BgL_arg1699z00_1924),
																			BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																	}
																	return
																		bgl_display_char(((unsigned char) ')'),
																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																}
															else
																{	/* BackEnd/c_emit.scm 321 */
																	if (BGL_INT32P(BgL_valuez00_9))
																		{	/* BackEnd/c_emit.scm 325 */
																			bgl_display_string
																				(BGl_string2013z00zzbackend_c_emitz00,
																				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																			{	/* BackEnd/c_emit.scm 327 */
																				BGL_LONGLONG_T BgL_arg1701z00_1926;

																				{	/* BackEnd/c_emit.scm 327 */
																					int32_t BgL_nz00_2526;

																					BgL_nz00_2526 =
																						BGL_BINT32_TO_INT32(BgL_valuez00_9);
																					BgL_arg1701z00_1926 =
																						(BGL_LONGLONG_T) (BgL_nz00_2526);
																				}
																				bgl_display_obj(make_bllong
																					(BgL_arg1701z00_1926),
																					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																			}
																			return
																				bgl_display_char(((unsigned char) ')'),
																				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																		}
																	else
																		{	/* BackEnd/c_emit.scm 325 */
																			if (BGL_UINT32P(BgL_valuez00_9))
																				{	/* BackEnd/c_emit.scm 329 */
																					bgl_display_string
																						(BGl_string2014z00zzbackend_c_emitz00,
																						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																					{	/* BackEnd/c_emit.scm 331 */
																						BGL_LONGLONG_T BgL_arg1703z00_1928;

																						{	/* BackEnd/c_emit.scm 331 */
																							uint32_t BgL_nz00_2531;

																							BgL_nz00_2531 =
																								BGL_BUINT32_TO_UINT32
																								(BgL_valuez00_9);
																							BgL_arg1703z00_1928 =
																								(BGL_LONGLONG_T)
																								(BgL_nz00_2531);
																						}
																						bgl_display_obj(make_bllong
																							(BgL_arg1703z00_1928),
																							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																					}
																					return
																						bgl_display_char(((unsigned char)
																							')'),
																						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																				}
																			else
																				{	/* BackEnd/c_emit.scm 329 */
																					if (BGL_INT64P(BgL_valuez00_9))
																						{	/* BackEnd/c_emit.scm 333 */
																							bgl_display_string
																								(BGl_string2015z00zzbackend_c_emitz00,
																								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																							{	/* BackEnd/c_emit.scm 335 */
																								BGL_LONGLONG_T
																									BgL_arg1705z00_1930;
																								{	/* BackEnd/c_emit.scm 335 */
																									int64_t BgL_nz00_2536;

																									BgL_nz00_2536 =
																										BGL_BINT64_TO_INT64
																										(BgL_valuez00_9);
																									BgL_arg1705z00_1930 =
																										(BGL_LONGLONG_T)
																										(BgL_nz00_2536);
																								}
																								bgl_display_obj(make_bllong
																									(BgL_arg1705z00_1930),
																									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																							}
																							return
																								bgl_display_char(((unsigned
																										char) ')'),
																								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																						}
																					else
																						{	/* BackEnd/c_emit.scm 333 */
																							if (BGL_UINT64P(BgL_valuez00_9))
																								{	/* BackEnd/c_emit.scm 337 */
																									bgl_display_string
																										(BGl_string2016z00zzbackend_c_emitz00,
																										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																									{	/* BackEnd/c_emit.scm 339 */
																										BGL_LONGLONG_T
																											BgL_arg1708z00_1932;
																										{	/* BackEnd/c_emit.scm 339 */
																											uint64_t BgL_nz00_2541;

																											BgL_nz00_2541 =
																												BGL_BINT64_TO_INT64
																												(BgL_valuez00_9);
																											BgL_arg1708z00_1932 =
																												(BGL_LONGLONG_T)
																												(BgL_nz00_2541);
																										}
																										bgl_display_obj(make_bllong
																											(BgL_arg1708z00_1932),
																											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																									}
																									return
																										bgl_display_char(((unsigned
																												char) ')'),
																										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																								}
																							else
																								{	/* BackEnd/c_emit.scm 337 */
																									if (UCS2P(BgL_valuez00_9))
																										{	/* BackEnd/c_emit.scm 341 */
																											bgl_display_string
																												(BGl_string2017z00zzbackend_c_emitz00,
																												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																											{	/* BackEnd/c_emit.scm 343 */
																												int BgL_arg1714z00_1934;

																												{	/* BackEnd/c_emit.scm 343 */
																													ucs2_t
																														BgL_ucs2z00_2546;
																													BgL_ucs2z00_2546 =
																														CUCS2
																														(BgL_valuez00_9);
																													{	/* BackEnd/c_emit.scm 343 */
																														obj_t
																															BgL_tmpz00_3325;
																														BgL_tmpz00_3325 =
																															BUCS2
																															(BgL_ucs2z00_2546);
																														BgL_arg1714z00_1934
																															=
																															CUCS2
																															(BgL_tmpz00_3325);
																												}}
																												bgl_display_obj(BINT
																													(BgL_arg1714z00_1934),
																													BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																											}
																											return
																												bgl_display_char((
																													(unsigned char) ')'),
																												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																										}
																									else
																										{	/* BackEnd/c_emit.scm 341 */
																											if (
																												(BgL_valuez00_9 ==
																													BUNSPEC))
																												{	/* BackEnd/c_emit.scm 345 */
																													return
																														bgl_display_string
																														(BGl_string2018z00zzbackend_c_emitz00,
																														BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																												}
																											else
																												{	/* BackEnd/c_emit.scm 345 */
																													if (STRINGP
																														(BgL_valuez00_9))
																														{	/* BackEnd/c_emit.scm 347 */
																															bgl_display_char((
																																	(unsigned
																																		char) '"'),
																																BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																															{	/* BackEnd/c_emit.scm 349 */
																																obj_t
																																	BgL_arg1717z00_1936;
																																BgL_arg1717z00_1936
																																	=
																																	BGl_untrigraphz00zzbackend_c_emitz00
																																	(string_for_read
																																	(BgL_valuez00_9));
																																bgl_display_obj
																																	(BgL_arg1717z00_1936,
																																	BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																															}
																															return
																																bgl_display_char
																																(((unsigned
																																		char) '"'),
																																BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																														}
																													else
																														{	/* BackEnd/c_emit.scm 347 */
																															if (INTEGERP
																																(BgL_valuez00_9))
																																{	/* BackEnd/c_emit.scm 351 */
																																	bgl_display_obj
																																		(BgL_valuez00_9,
																																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																	if ((((obj_t)
																																				BgL_typez00_10)
																																			==
																																			BGl_za2intza2z00zztype_cachez00))
																																		{	/* BackEnd/c_emit.scm 353 */
																																			return
																																				BFALSE;
																																		}
																																	else
																																		{	/* BackEnd/c_emit.scm 353 */
																																			return
																																				bgl_display_string
																																				(BGl_string2019z00zzbackend_c_emitz00,
																																				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																		}
																																}
																															else
																																{	/* BackEnd/c_emit.scm 351 */
																																	if (REALP
																																		(BgL_valuez00_9))
																																		{	/* BackEnd/c_emit.scm 360 */
																																			if (BGL_IS_NAN(REAL_TO_DOUBLE(BgL_valuez00_9)))
																																				{	/* BackEnd/c_emit.scm 362 */
																																					return
																																						bgl_display_string
																																						(BGl_string2020z00zzbackend_c_emitz00,
																																						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																				}
																																			else
																																				{	/* BackEnd/c_emit.scm 364 */
																																					bool_t
																																						BgL_test2185z00_3354;
																																					if (BGL_IS_INF(REAL_TO_DOUBLE(BgL_valuez00_9)))
																																						{	/* BackEnd/c_emit.scm 364 */
																																							BgL_test2185z00_3354
																																								=
																																								(REAL_TO_DOUBLE
																																								(BgL_valuez00_9)
																																								>
																																								((double) 0.0));
																																						}
																																					else
																																						{	/* BackEnd/c_emit.scm 364 */
																																							BgL_test2185z00_3354
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																					if (BgL_test2185z00_3354)
																																						{	/* BackEnd/c_emit.scm 364 */
																																							return
																																								bgl_display_string
																																								(BGl_string2021z00zzbackend_c_emitz00,
																																								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																						}
																																					else
																																						{	/* BackEnd/c_emit.scm 364 */
																																							if (BGL_IS_INF(REAL_TO_DOUBLE(BgL_valuez00_9)))
																																								{	/* BackEnd/c_emit.scm 366 */
																																									return
																																										bgl_display_string
																																										(BGl_string2022z00zzbackend_c_emitz00,
																																										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																								}
																																							else
																																								{	/* BackEnd/c_emit.scm 366 */
																																									bgl_display_string
																																										(BGl_string2004z00zzbackend_c_emitz00,
																																										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																									{	/* BackEnd/c_emit.scm 370 */
																																										obj_t
																																											BgL_arg1733z00_1944;
																																										{	/* BackEnd/c_emit.scm 370 */
																																											obj_t
																																												BgL_arg1734z00_1945;
																																											{	/* BackEnd/c_emit.scm 370 */
																																												BgL_typez00_bglt
																																													BgL_oz00_2571;
																																												BgL_oz00_2571
																																													=
																																													(
																																													(BgL_typez00_bglt)
																																													BGl_za2realza2z00zztype_cachez00);
																																												BgL_arg1734z00_1945
																																													=
																																													(
																																													((BgL_typez00_bglt) COBJECT(BgL_oz00_2571))->BgL_namez00);
																																											}
																																											BgL_arg1733z00_1944
																																												=
																																												BGl_stringzd2sanszd2z42z42zztype_toolsz00
																																												(BgL_arg1734z00_1945);
																																										}
																																										bgl_display_obj
																																											(BgL_arg1733z00_1944,
																																											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																									}
																																									bgl_display_string
																																										(BGl_string1998z00zzbackend_c_emitz00,
																																										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																									bgl_display_obj
																																										(BgL_valuez00_9,
																																										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																									return
																																										bgl_display_string
																																										(BGl_string1998z00zzbackend_c_emitz00,
																																										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																								}
																																						}
																																				}
																																		}
																																	else
																																		{	/* BackEnd/c_emit.scm 360 */
																																			if (ELONGP
																																				(BgL_valuez00_9))
																																				{	/* BackEnd/c_emit.scm 374 */
																																					bgl_display_string
																																						(BGl_string2004z00zzbackend_c_emitz00,
																																						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																					{	/* BackEnd/c_emit.scm 376 */
																																						obj_t
																																							BgL_arg1737z00_1948;
																																						{	/* BackEnd/c_emit.scm 376 */
																																							obj_t
																																								BgL_arg1738z00_1949;
																																							{	/* BackEnd/c_emit.scm 376 */
																																								BgL_typez00_bglt
																																									BgL_oz00_2578;
																																								BgL_oz00_2578
																																									=
																																									(
																																									(BgL_typez00_bglt)
																																									BGl_za2elongza2z00zztype_cachez00);
																																								BgL_arg1738z00_1949
																																									=
																																									(
																																									((BgL_typez00_bglt) COBJECT(BgL_oz00_2578))->BgL_namez00);
																																							}
																																							BgL_arg1737z00_1948
																																								=
																																								BGl_stringzd2sanszd2z42z42zztype_toolsz00
																																								(BgL_arg1738z00_1949);
																																						}
																																						bgl_display_obj
																																							(BgL_arg1737z00_1948,
																																							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																					}
																																					bgl_display_string
																																						(BGl_string1998z00zzbackend_c_emitz00,
																																						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																					{	/* BackEnd/c_emit.scm 378 */
																																						obj_t
																																							BgL_arg1739z00_1950;
																																						BgL_arg1739z00_1950
																																							=
																																							BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																							(BELONG_TO_LONG
																																							(BgL_valuez00_9),
																																							BNIL);
																																						bgl_display_obj
																																							(BgL_arg1739z00_1950,
																																							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																					}
																																					return
																																						bgl_display_string
																																						(BGl_string1998z00zzbackend_c_emitz00,
																																						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																				}
																																			else
																																				{	/* BackEnd/c_emit.scm 374 */
																																					if (LLONGP(BgL_valuez00_9))
																																						{	/* BackEnd/c_emit.scm 381 */
																																							obj_t
																																								BgL_arg1746z00_1953;
																																							BgL_arg1746z00_1953
																																								=
																																								BGl_llongzd2ze3czd2isoze3zzbackend_c_emitz00
																																								(BLLONG_TO_LLONG
																																								(BgL_valuez00_9));
																																							return
																																								bgl_display_obj
																																								(BgL_arg1746z00_1953,
																																								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																						}
																																					else
																																						{	/* BackEnd/c_emit.scm 380 */
																																							if (CNSTP(BgL_valuez00_9))
																																								{	/* BackEnd/c_emit.scm 382 */
																																									if (EOF_OBJECTP(BgL_valuez00_9))
																																										{	/* BackEnd/c_emit.scm 384 */
																																											return
																																												bgl_display_string
																																												(BGl_string2023z00zzbackend_c_emitz00,
																																												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																										}
																																									else
																																										{	/* BackEnd/c_emit.scm 384 */
																																											if ((BgL_valuez00_9 == BOPTIONAL))
																																												{	/* BackEnd/c_emit.scm 386 */
																																													return
																																														bgl_display_string
																																														(BGl_string2024z00zzbackend_c_emitz00,
																																														BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																												}
																																											else
																																												{	/* BackEnd/c_emit.scm 386 */
																																													if ((BgL_valuez00_9 == BKEY))
																																														{	/* BackEnd/c_emit.scm 388 */
																																															return
																																																bgl_display_string
																																																(BGl_string2025z00zzbackend_c_emitz00,
																																																BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																														}
																																													else
																																														{	/* BackEnd/c_emit.scm 388 */
																																															if ((BgL_valuez00_9 == BREST))
																																																{	/* BackEnd/c_emit.scm 390 */
																																																	return
																																																		bgl_display_string
																																																		(BGl_string2026z00zzbackend_c_emitz00,
																																																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																																}
																																															else
																																																{	/* BackEnd/c_emit.scm 390 */
																																																	if ((BgL_valuez00_9 == BEOA))
																																																		{	/* BackEnd/c_emit.scm 392 */
																																																			return
																																																				bgl_display_string
																																																				(BGl_string2027z00zzbackend_c_emitz00,
																																																				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																																		}
																																																	else
																																																		{	/* BackEnd/c_emit.scm 392 */
																																																			bgl_display_string
																																																				(BGl_string2028z00zzbackend_c_emitz00,
																																																				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																																			{	/* BackEnd/c_emit.scm 396 */
																																																				long
																																																					BgL_arg1749z00_1956;
																																																				BgL_arg1749z00_1956
																																																					=
																																																					CCNST
																																																					(BgL_valuez00_9);
																																																				bgl_display_obj
																																																					(BINT
																																																					(BgL_arg1749z00_1956),
																																																					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																																			}
																																																			return
																																																				bgl_display_char
																																																				(
																																																				((unsigned char) ')'), BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																								}}}}}}
																																							else
																																								{	/* BackEnd/c_emit.scm 382 */
																																									if (BIGNUMP(BgL_valuez00_9))
																																										{	/* BackEnd/c_emit.scm 398 */
																																											bgl_display_string
																																												(BGl_string2029z00zzbackend_c_emitz00,
																																												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																											{	/* BackEnd/c_emit.scm 400 */
																																												obj_t
																																													BgL_arg1751z00_1958;
																																												BgL_arg1751z00_1958
																																													=
																																													BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																																													(BgL_valuez00_9,
																																													BINT
																																													(16L));
																																												bgl_display_obj
																																													(BgL_arg1751z00_1958,
																																													BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																											}
																																											return
																																												bgl_display_string
																																												(BGl_string2030z00zzbackend_c_emitz00,
																																												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																										}
																																									else
																																										{	/* BackEnd/c_emit.scm 398 */
																																											return
																																												bgl_display_obj
																																												(BgL_valuez00_9,
																																												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &emit-atom-value */
	obj_t BGl_z62emitzd2atomzd2valuez62zzbackend_c_emitz00(obj_t BgL_envz00_2708,
		obj_t BgL_valuez00_2709, obj_t BgL_typez00_2710)
	{
		{	/* BackEnd/c_emit.scm 281 */
			return
				BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00(BgL_valuez00_2709,
				((BgL_typez00_bglt) BgL_typez00_2710));
		}

	}



/* untrigraph */
	BGL_EXPORTED_DEF obj_t BGl_untrigraphz00zzbackend_c_emitz00(obj_t
		BgL_fromz00_11)
	{
		{	/* BackEnd/c_emit.scm 411 */
			{	/* BackEnd/c_emit.scm 412 */
				long BgL_lenz00_1959;

				BgL_lenz00_1959 = STRING_LENGTH(BgL_fromz00_11);
				{	/* BackEnd/c_emit.scm 412 */
					long BgL_lenzd22zd2_1960;

					BgL_lenzd22zd2_1960 = (BgL_lenz00_1959 - 2L);
					{	/* BackEnd/c_emit.scm 413 */

						{	/* BackEnd/c_emit.scm 415 */
							long BgL_nbzd2colzd2_1961;

							{
								long BgL_iz00_2005;
								long BgL_nbzd2colzd2_2006;

								BgL_iz00_2005 = 0L;
								BgL_nbzd2colzd2_2006 = 0L;
							BgL_zc3z04anonymousza31841ze3z87_2007:
								if ((BgL_iz00_2005 > BgL_lenzd22zd2_1960))
									{	/* BackEnd/c_emit.scm 418 */
										BgL_nbzd2colzd2_1961 = BgL_nbzd2colzd2_2006;
									}
								else
									{	/* BackEnd/c_emit.scm 418 */
										if (
											(STRING_REF(BgL_fromz00_11,
													BgL_iz00_2005) == ((unsigned char) '?')))
											{	/* BackEnd/c_emit.scm 420 */
												if (
													(STRING_REF(BgL_fromz00_11,
															(BgL_iz00_2005 + 1L)) == ((unsigned char) '?')))
													{
														long BgL_nbzd2colzd2_3435;
														long BgL_iz00_3433;

														BgL_iz00_3433 = (BgL_iz00_2005 + 2L);
														BgL_nbzd2colzd2_3435 = (BgL_nbzd2colzd2_2006 + 1L);
														BgL_nbzd2colzd2_2006 = BgL_nbzd2colzd2_3435;
														BgL_iz00_2005 = BgL_iz00_3433;
														goto BgL_zc3z04anonymousza31841ze3z87_2007;
													}
												else
													{
														long BgL_iz00_3437;

														BgL_iz00_3437 = (BgL_iz00_2005 + 2L);
														BgL_iz00_2005 = BgL_iz00_3437;
														goto BgL_zc3z04anonymousza31841ze3z87_2007;
													}
											}
										else
											{
												long BgL_iz00_3439;

												BgL_iz00_3439 = (BgL_iz00_2005 + 1L);
												BgL_iz00_2005 = BgL_iz00_3439;
												goto BgL_zc3z04anonymousza31841ze3z87_2007;
											}
									}
							}
							if ((BgL_nbzd2colzd2_1961 == 0L))
								{	/* BackEnd/c_emit.scm 427 */
									return BgL_fromz00_11;
								}
							else
								{	/* BackEnd/c_emit.scm 432 */
									obj_t BgL_resz00_1963;

									{	/* BackEnd/c_emit.scm 432 */
										long BgL_arg1838z00_1999;

										BgL_arg1838z00_1999 =
											(BgL_lenz00_1959 + (3L * (BgL_nbzd2colzd2_1961 * 2L)));
										{	/* BackEnd/c_emit.scm 432 */

											BgL_resz00_1963 =
												make_string(BgL_arg1838z00_1999, ((unsigned char) ' '));
									}}
									{
										long BgL_rz00_1966;
										long BgL_wz00_1967;

										BgL_rz00_1966 = 0L;
										BgL_wz00_1967 = 0L;
									BgL_zc3z04anonymousza31753ze3z87_1968:
										if ((BgL_rz00_1966 == BgL_lenz00_1959))
											{	/* BackEnd/c_emit.scm 437 */
												return BgL_resz00_1963;
											}
										else
											{	/* BackEnd/c_emit.scm 439 */
												bool_t BgL_test2202z00_3449;

												if (
													(STRING_REF(BgL_fromz00_11,
															BgL_rz00_1966) == ((unsigned char) '?')))
													{	/* BackEnd/c_emit.scm 439 */
														BgL_test2202z00_3449 =
															(BgL_rz00_1966 > BgL_lenzd22zd2_1960);
													}
												else
													{	/* BackEnd/c_emit.scm 439 */
														BgL_test2202z00_3449 = ((bool_t) 1);
													}
												if (BgL_test2202z00_3449)
													{	/* BackEnd/c_emit.scm 439 */
														{	/* BackEnd/c_emit.scm 441 */
															unsigned char BgL_tmpz00_3454;

															BgL_tmpz00_3454 =
																STRING_REF(BgL_fromz00_11, BgL_rz00_1966);
															STRING_SET(BgL_resz00_1963, BgL_wz00_1967,
																BgL_tmpz00_3454);
														}
														{
															long BgL_wz00_3459;
															long BgL_rz00_3457;

															BgL_rz00_3457 = (BgL_rz00_1966 + 1L);
															BgL_wz00_3459 = (BgL_wz00_1967 + 1L);
															BgL_wz00_1967 = BgL_wz00_3459;
															BgL_rz00_1966 = BgL_rz00_3457;
															goto BgL_zc3z04anonymousza31753ze3z87_1968;
														}
													}
												else
													{	/* BackEnd/c_emit.scm 439 */
														if (
															(STRING_REF(BgL_fromz00_11,
																	(BgL_rz00_1966 + 1L)) ==
																((unsigned char) '?')))
															{	/* BackEnd/c_emit.scm 443 */
																STRING_SET(BgL_resz00_1963, BgL_wz00_1967,
																	((unsigned char) '\\'));
																{	/* BackEnd/c_emit.scm 450 */
																	long BgL_tmpz00_3466;

																	BgL_tmpz00_3466 = (BgL_wz00_1967 + 1L);
																	STRING_SET(BgL_resz00_1963, BgL_tmpz00_3466,
																		((unsigned char) '0'));
																}
																{	/* BackEnd/c_emit.scm 451 */
																	long BgL_tmpz00_3469;

																	BgL_tmpz00_3469 = (BgL_wz00_1967 + 2L);
																	STRING_SET(BgL_resz00_1963, BgL_tmpz00_3469,
																		((unsigned char) '7'));
																}
																{	/* BackEnd/c_emit.scm 452 */
																	long BgL_tmpz00_3472;

																	BgL_tmpz00_3472 = (BgL_wz00_1967 + 3L);
																	STRING_SET(BgL_resz00_1963, BgL_tmpz00_3472,
																		((unsigned char) '7'));
																}
																{	/* BackEnd/c_emit.scm 453 */
																	long BgL_tmpz00_3475;

																	BgL_tmpz00_3475 = (BgL_wz00_1967 + 4L);
																	STRING_SET(BgL_resz00_1963, BgL_tmpz00_3475,
																		((unsigned char) '\\'));
																}
																{	/* BackEnd/c_emit.scm 454 */
																	long BgL_tmpz00_3478;

																	BgL_tmpz00_3478 = (BgL_wz00_1967 + 5L);
																	STRING_SET(BgL_resz00_1963, BgL_tmpz00_3478,
																		((unsigned char) '0'));
																}
																{	/* BackEnd/c_emit.scm 455 */
																	long BgL_tmpz00_3481;

																	BgL_tmpz00_3481 = (BgL_wz00_1967 + 6L);
																	STRING_SET(BgL_resz00_1963, BgL_tmpz00_3481,
																		((unsigned char) '7'));
																}
																{	/* BackEnd/c_emit.scm 456 */
																	long BgL_tmpz00_3484;

																	BgL_tmpz00_3484 = (BgL_wz00_1967 + 7L);
																	STRING_SET(BgL_resz00_1963, BgL_tmpz00_3484,
																		((unsigned char) '7'));
																}
																{
																	long BgL_wz00_3489;
																	long BgL_rz00_3487;

																	BgL_rz00_3487 = (BgL_rz00_1966 + 2L);
																	BgL_wz00_3489 = (BgL_wz00_1967 + 8L);
																	BgL_wz00_1967 = BgL_wz00_3489;
																	BgL_rz00_1966 = BgL_rz00_3487;
																	goto BgL_zc3z04anonymousza31753ze3z87_1968;
																}
															}
														else
															{	/* BackEnd/c_emit.scm 443 */
																STRING_SET(BgL_resz00_1963, BgL_wz00_1967,
																	((unsigned char) '?'));
																{	/* BackEnd/c_emit.scm 445 */
																	unsigned char BgL_auxz00_3494;
																	long BgL_tmpz00_3492;

																	BgL_auxz00_3494 =
																		STRING_REF(BgL_fromz00_11,
																		(BgL_rz00_1966 + 1L));
																	BgL_tmpz00_3492 = (BgL_wz00_1967 + 1L);
																	STRING_SET(BgL_resz00_1963, BgL_tmpz00_3492,
																		BgL_auxz00_3494);
																}
																{
																	long BgL_wz00_3500;
																	long BgL_rz00_3498;

																	BgL_rz00_3498 = (BgL_rz00_1966 + 2L);
																	BgL_wz00_3500 = (BgL_wz00_1967 + 2L);
																	BgL_wz00_1967 = BgL_wz00_3500;
																	BgL_rz00_1966 = BgL_rz00_3498;
																	goto BgL_zc3z04anonymousza31753ze3z87_1968;
																}
															}
													}
											}
									}
								}
						}
					}
				}
			}
		}

	}



/* &untrigraph */
	obj_t BGl_z62untrigraphz62zzbackend_c_emitz00(obj_t BgL_envz00_2711,
		obj_t BgL_fromz00_2712)
	{
		{	/* BackEnd/c_emit.scm 411 */
			return BGl_untrigraphz00zzbackend_c_emitz00(BgL_fromz00_2712);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_c_emitz00(void)
	{
		{	/* BackEnd/c_emit.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
			BGl_modulezd2initializa7ationz75zzengine_configurez00(272817175L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
			BGl_modulezd2initializa7ationz75zztools_licensez00(110617375L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
			BGl_modulezd2initializa7ationz75zzcgen_copz00(529595144L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
			BGl_modulezd2initializa7ationz75zzcgen_emitzd2copzd2(371489745L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
			return
				BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2031z00zzbackend_c_emitz00));
		}

	}

#ifdef __cplusplus
}
#endif
