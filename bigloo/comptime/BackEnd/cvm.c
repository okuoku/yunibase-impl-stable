/*===========================================================================*/
/*   (BackEnd/cvm.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/cvm.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_CVM_TYPE_DEFINITIONS
#define BGL_BACKEND_CVM_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_cvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}             *BgL_cvmz00_bglt;

	typedef struct BgL_sawcz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}              *BgL_sawcz00_bglt;

	typedef struct BgL_cgenz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}              *BgL_cgenz00_bglt;


#endif													// BGL_BACKEND_CVM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62sawczd2removezd2emptyzd2letzb0zzbackend_cvmz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62cgenzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2debugzd2supportz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2functionszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2externzd2variableszd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		obj_t);
	static obj_t BGl_z62sawczd2requirezd2tailczd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sawczd2typedzd2funcallzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_cgenz00_bglt, bool_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2typedzd2funcallz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2typedzd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_cvmz00_bglt BGl_cvmzd2nilzd2zzbackend_cvmz00(void);
	static obj_t
		BGl_z62cgenzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sawcz00_bglt BGl_makezd2sawczd2zzbackend_cvmz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, obj_t,
		obj_t, bool_t, bool_t, bool_t, bool_t, bool_t, bool_t, bool_t, obj_t, obj_t,
		bool_t, bool_t, bool_t, obj_t, obj_t, bool_t, bool_t, bool_t);
	static obj_t BGl_z62cvmzd2callcczb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62sawczd2externzd2variablesz62zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2boundzd2checkz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2requirezd2tailcz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2typedzd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62cgenzd2foreignzd2closurez62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2debugzd2supportzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2pragmazd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2foreignzd2clausezd2supportzd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62cgenzd2tracezd2supportz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2heapzd2suffixzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt, obj_t);
	static obj_t
		BGl_z62sawczd2foreignzd2closurezd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sawczd2tvectorzd2descrzd2supportzb0zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sawczd2typeszd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2boundzd2checkzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		bool_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbackend_cvmz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2effectzb2z60zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62sawczd2qualifiedzd2typesz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t
		BGl_z62sawczd2removezd2emptyzd2letzd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2srfi0zd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, obj_t);
	static obj_t BGl_z62sawczd2pragmazd2supportzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sawczd2srfi0zd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2typedzd2eqz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62sawczd2pragmazd2supportz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_sawcz00_bglt, bool_t);
	static obj_t BGl_z62sawczd2languagezb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62sawczd2typezd2checkzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62sawczd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2typeszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, obj_t);
	static obj_t BGl_z62sawczd2typeszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2registerszb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2boundzd2checkz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t
		BGl_z62sawczd2externzd2variableszd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2boundzd2checkz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2foreignzd2closurez00zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2effectzb2z60zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62cgenzd2functionszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2qualifiedzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2variableszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, obj_t);
	static obj_t BGl_z62sawczd2typedzb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2effectzb2z60zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62cvmzd2foreignzd2closurez62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2requirezd2tailcz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2qualifiedzd2typesz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62sawczd2typeszb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2externzd2functionsz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62cgenzd2debugzd2supportzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cvmzd2srfi0zb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2variableszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2typezd2checkzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2variableszd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2removezd2emptyzd2letzd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62cvmzd2requirezd2tailczd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cvmzd2typedzd2funcallzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzbackend_cvmz00(void);
	BGL_EXPORTED_DECL BgL_cgenz00_bglt BGl_cgenzd2nilzd2zzbackend_cvmz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2namezd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2typedzd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62cgenzd2boundzd2checkz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62sawczd2registerszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2variableszd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2heapzd2compatiblezd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cgenz00zzbackend_cvmz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_cgenzd2typeszd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2externzd2variablesz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62cgenzd2functionszb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2foreignzd2closurezd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2qualifiedzd2typesz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2typedzd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, bool_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2tracezd2supportz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62cgenzd2typedzd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62sawczd2qualifiedzd2typeszd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2requirezd2tailcz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62cvmzd2srfi0zd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sawczd2functionszb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62sawczd2tracezd2supportz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2namezb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2typezd2checkzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt, bool_t);
	static obj_t BGl_z62cvmzd2pragmazd2supportz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t
		BGl_z62cgenzd2foreignzd2closurezd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzbackend_cvmz00(void);
	static obj_t
		BGl_z62cvmzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cgenzd2externzd2typesz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2typeszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static BgL_cgenz00_bglt BGl_z62makezd2cgenzb0zzbackend_cvmz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62cgenzd2namezb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2typedzb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzbackend_cvmz00(void);
	static obj_t
		BGl_z62sawczd2foreignzd2clausezd2supportzb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2requirezd2tailczd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cgenzd2typedzd2funcallzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sawczd2boundzd2checkz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2typeszb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t
		BGl_z62sawczd2heapzd2compatiblezd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2externzd2typesz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2callcczd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62cgenzd2languagezd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2externzd2functionszd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62cgenzd2pregisterszd2setz12z70zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sawczd2externzd2typeszd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sawczd2foreignzd2closurez62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2heapzd2suffixzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2languagezd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_cvmzd2srfi0zd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2boundzd2checkzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		bool_t);
	static obj_t BGl_z62sawczd2externzd2functionsz62zzbackend_cvmz00(obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2registerszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzf3z91zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2removezd2emptyzd2letzb0zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2tracezd2supportzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2foreignzd2clausezd2supportzd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_cgenzf3zf3zzbackend_cvmz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31330ze3ze5zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2qualifiedzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		bool_t);
	static obj_t BGl_z62cgenzd2externzd2variablesz62zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2pregisterszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, obj_t);
	static obj_t
		BGl_z62cgenzd2foreignzd2clausezd2supportzb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2foreignzd2closurez00zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t
		BGl_z62sawczd2externzd2functionszd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2foreignzd2closurezd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		bool_t);
	static obj_t BGl_z62cvmzd2pregisterszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2pragmazd2supportz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t
		BGl_z62cgenzd2qualifiedzd2typeszd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cgenzd2effectzb2zd2setz12zc2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cvmzf3zf3zzbackend_cvmz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2functionszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_cgenz00_bglt BGl_makezd2cgenzd2zzbackend_cvmz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, obj_t,
		obj_t, bool_t, bool_t, bool_t, bool_t, bool_t, bool_t, bool_t, obj_t, obj_t,
		bool_t, bool_t, bool_t, obj_t, obj_t, bool_t, bool_t, bool_t);
	BGL_EXPORTED_DECL obj_t BGl_cgenzd2namezd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62cvmzd2variableszb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2typedzd2eqz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cvmzd2foreignzd2clausezd2supportzb0zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2functionszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2heapzd2compatiblezd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_cvmz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2functionszd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2effectzb2zd2setz12za0zzbackend_cvmz00(BgL_cvmz00_bglt, bool_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2foreignzd2closurez00zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2typezd2checkz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62sawczd2typezd2checkz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2effectzb2z02zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2languagezd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2srfi0zd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2functionszd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t
		BGl_z62cgenzd2heapzd2compatiblezd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2pragmazd2supportzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		bool_t);
	static obj_t BGl_z62sawczd2heapzd2suffixzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_cvmz00_bglt, bool_t);
	static obj_t BGl_z62sawczd2boundzd2checkzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sawczd2languagezd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2typeszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2externzd2functionsz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62cgenzd2namezd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2registerszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, obj_t);
	static obj_t BGl_z62cgenzd2pragmazd2supportz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2typedzd2eqzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2foreignzd2clausezd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_sawcz00_bglt, obj_t);
	static obj_t BGl_z62sawczd2namezd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cgenzf3z91zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2debugzd2supportzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		obj_t);
	static obj_t BGl_z62cvmzd2tvectorzd2descrzd2supportzb0zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2heapzd2suffixz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t
		BGl_z62cvmzd2removezd2emptyzd2letzd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2pragmazd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		bool_t);
	static obj_t BGl_z62cvmzd2externzd2typesz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2externzd2typesz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2qualifiedzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2languagezd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62cgenzd2debugzd2supportz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62sawczd2pregisterszd2setz12z70zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cgenzd2typedzd2funcallz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62sawczd2typedzd2eqz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_cvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2registerszd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t
		BGl_z62cvmzd2externzd2variableszd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62cvmzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2tracezd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		bool_t);
	static obj_t BGl_z62cvmzd2typedzb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2registerszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2tracezd2supportz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2externzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		obj_t);
	static obj_t BGl_z62cvmzd2typezd2checkz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2typeszb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2heapzd2compatiblezd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		obj_t);
	static obj_t BGl_z62cgenzd2callcczb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cvmzd2namezd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62sawczd2effectzb2z02zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2heapzd2suffixz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2pregisterszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2pregisterszd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2heapzd2suffixzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2boundzd2checkzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2srfi0zd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, obj_t);
	static obj_t BGl_z62sawczd2pregisterszb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2externzd2functionsz62zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2requirezd2tailczd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2typedzd2funcallzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL bool_t BGl_sawczf3zf3zzbackend_cvmz00(obj_t);
	static obj_t BGl_z62cgenzd2srfi0zd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2typedzd2eqzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2typeszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, obj_t);
	static BgL_cvmz00_bglt BGl_z62cvmzd2nilzb0zzbackend_cvmz00(obj_t);
	static obj_t BGl_z62cgenzd2typeszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2tvectorzd2descrzd2supportzd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62sawczd2typedzd2eqzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cgenzd2effectzb2z02zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2removezd2emptyzd2letzd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt,
		bool_t);
	BGL_IMPORT obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	static obj_t BGl_z62cvmzd2effectzb2zd2setz12zc2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2effectzb2zd2setz12za0zzbackend_cvmz00(BgL_sawcz00_bglt, bool_t);
	static obj_t BGl_z62cvmzd2functionszb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2tracezd2supportz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2heapzd2suffixz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62sawczd2variableszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2pregisterszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, obj_t);
	static obj_t BGl_z62cvmzd2heapzd2suffixz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62sawczd2namezb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2debugzd2supportz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2namezd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2tracezd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2externzd2variableszd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2typedzd2funcallz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31347ze3ze5zzbackend_cvmz00(obj_t,
		obj_t);
	static obj_t BGl_z62sawczd2debugzd2supportz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2languagezb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2namezd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, obj_t);
	static obj_t
		BGl_z62sawczd2foreignzd2clausezd2supportzd2setz12z70zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sawczd2typedzd2funcallz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2tvectorzd2descrzd2supportzd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_cvmzd2typedzd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzbackend_cvmz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2removezd2emptyzd2letzd2setz12z12zzbackend_cvmz00
		(BgL_cgenz00_bglt, bool_t);
	static obj_t BGl_z62cgenzd2requirezd2tailcz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cvmzd2typeszd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62cvmzd2externzd2typeszd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cgenzd2heapzd2suffixz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2tracezd2supportz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	static BgL_cvmz00_bglt BGl_z62lambda1328z62zzbackend_cvmz00(obj_t);
	static obj_t BGl_z62cvmzd2removezd2emptyzd2letzb0zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2debugzd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2pragmazd2supportz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62cgenzd2heapzd2compatiblez62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2typedzd2eqz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2externzd2variableszd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		obj_t);
	static obj_t BGl_z62cvmzd2pregisterszb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2callcczd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, bool_t);
	static obj_t BGl_z62cgenzd2heapzd2suffixzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2pragmazd2supportz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62cgenzd2boundzd2checkzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sawczf3z91zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2externzd2variablesz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2externzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2callcczd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62cgenzd2tvectorzd2descrzd2supportzb0zzbackend_cvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2typezd2checkz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	static BgL_sawcz00_bglt BGl_z62sawczd2nilzb0zzbackend_cvmz00(obj_t);
	static obj_t
		BGl_z62cgenzd2removezd2emptyzd2letzd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2foreignzd2closurezd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2languagezd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, obj_t);
	static obj_t BGl_z62cvmzd2variableszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static BgL_sawcz00_bglt BGl_z62lambda1340z62zzbackend_cvmz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62cvmzd2externzd2functionszd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2removezd2emptyzd2letzd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	static BgL_sawcz00_bglt BGl_z62lambda1343z62zzbackend_cvmz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2heapzd2compatiblez00zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_cnstzd2initzd2zzbackend_cvmz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_cvmz00(void);
	BGL_EXPORTED_DECL obj_t BGl_sawczd2namezd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62cvmzd2languagezb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sawczd2srfi0zd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62sawczd2heapzd2suffixz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2heapzd2compatiblez62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2registerszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, obj_t);
	static obj_t
		BGl_z62cgenzd2externzd2variableszd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cgenzd2registerszb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_cvmz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2variableszd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2externzd2variablesz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_gczd2rootszd2initz00zzbackend_cvmz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2callcczd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, bool_t);
	static obj_t BGl_z62sawczd2registerszb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2requirezd2tailczd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2typedzd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2registerszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2typedzd2funcallzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2debugzd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		obj_t);
	static obj_t BGl_z62sawczd2typedzd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_backendz00zzbackend_backendz00;
	BGL_EXPORTED_DECL bool_t
		BGl_cvmzd2requirezd2tailcz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62cgenzd2pragmazd2supportzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static BgL_cgenz00_bglt BGl_z62lambda1362z62zzbackend_cvmz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31368ze3ze5zzbackend_cvmz00(obj_t,
		obj_t);
	static BgL_cgenz00_bglt BGl_z62lambda1365z62zzbackend_cvmz00(obj_t);
	static obj_t BGl_z62cvmzd2languagezd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sawczd2requirezd2tailcz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62sawczd2srfi0zb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2callcczd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt, bool_t);
	static obj_t BGl_z62sawczd2functionszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sawczd2tracezd2supportzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cvmzd2namezd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2callcczd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2qualifiedzd2typeszd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2externzd2functionszd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cgenzd2srfi0zd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2variableszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2typezd2checkz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62sawczd2externzd2typesz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2languagezd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2pregisterszd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62cgenzd2typezd2checkz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2externzd2typesz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_cgenz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2typedzd2eqz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2requirezd2tailczd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2typedzd2funcallzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		bool_t);
	static obj_t BGl_z62cgenzd2callcczd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sawczd2heapzd2compatiblez62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2typedzd2eqzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt, bool_t);
	static BgL_cgenz00_bglt BGl_z62cgenzd2nilzb0zzbackend_cvmz00(obj_t);
	static obj_t BGl_z62cvmzd2typedzd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2heapzd2compatiblezd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2externzd2functionszd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		obj_t);
	static obj_t BGl_z62cgenzd2qualifiedzd2typesz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_sawcz00zzbackend_cvmz00 = BUNSPEC;
	static obj_t BGl_z62cgenzd2srfi0zb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2externzd2functionsz62zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2typezd2checkzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2removezd2emptyzd2letzd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2debugzd2supportz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2registerszd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2typedzd2funcallz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62cgenzd2variableszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2heapzd2compatiblez00zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62cvmzd2functionszd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2tracezd2supportzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2registerszd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2externzd2variablesz00zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62cvmzd2typedzd2eqzd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sawczd2callcczd2setz12z70zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static BgL_sawcz00_bglt BGl_z62makezd2sawczb0zzbackend_cvmz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2qualifiedzd2typesz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62sawczd2callcczb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t
		BGl_z62cgenzd2externzd2functionszd2setz12za2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cvmz00zzbackend_cvmz00 = BUNSPEC;
	static obj_t BGl_z62sawczd2debugzd2supportzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2functionszd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2externzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt,
		obj_t);
	static obj_t BGl_z62cvmzd2qualifiedzd2typesz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2externzd2functionsz00zzbackend_cvmz00(BgL_cvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2typezd2checkzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt,
		bool_t);
	static obj_t BGl_z62sawczd2effectzb2zd2setz12zc2zzbackend_cvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cvmzd2pragmazd2supportzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2effectzb2zd2setz12za0zzbackend_cvmz00(BgL_cgenz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2heapzd2compatiblez00zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62cvmzd2typezd2checkzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cgenzd2variableszb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cvmzd2debugzd2supportz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cgenzd2boundzd2checkz00zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2languagezd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	static obj_t BGl_z62cvmzd2typedzd2funcallz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2pregisterszd2zzbackend_cvmz00(BgL_cgenz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_cvmzd2callcczd2zzbackend_cvmz00(BgL_cvmz00_bglt);
	static obj_t BGl_z62cgenzd2pregisterszb0zzbackend_cvmz00(obj_t, obj_t);
	static obj_t BGl_z62cgenzd2typedzd2eqz62zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2foreignzd2clausezd2supportzd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	static obj_t BGl_z62sawczd2variableszb0zzbackend_cvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2typedzd2eqzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_sawcz00_bglt BGl_sawczd2nilzd2zzbackend_cvmz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cvmzd2foreignzd2closurezd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt,
		bool_t);
	static obj_t BGl_z62cgenzd2externzd2typeszd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cgenzd2tracezd2supportzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sawczd2tvectorzd2descrzd2supportzd2zzbackend_cvmz00(BgL_sawcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sawczd2removezd2emptyzd2letzd2setz12z12zzbackend_cvmz00
		(BgL_sawcz00_bglt, bool_t);
	static obj_t BGl_z62cvmzd2heapzd2suffixzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cvmzd2boundzd2checkzd2setz12za2zzbackend_cvmz00(obj_t,
		obj_t, obj_t);
	static obj_t __cnst[5];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2foreignzd2clausezd2supportzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2foreig1409z00,
		BGl_z62cgenzd2foreignzd2clausezd2supportzb0zzbackend_cvmz00, 0L, BUNSPEC,
		1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2typeszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typesza71410za7,
		BGl_z62sawczd2typeszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2boundzd2checkzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2boundza7d1411za7,
		BGl_z62cvmzd2boundzd2checkzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2typezd2checkzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typeza7d1412za7,
		BGl_z62cgenzd2typezd2checkzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2externzd2typeszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2extern1413z00,
		BGl_z62cgenzd2externzd2typesz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2foreignzd2clausezd2supportzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2foreig1414z00,
		BGl_z62cgenzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_cvmz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2debugzd2supportzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2debugza7d1415za7,
		BGl_z62cvmzd2debugzd2supportz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2functionszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2functio1416z00,
		BGl_z62cvmzd2functionszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2foreignzd2clausezd2supportzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2foreig1417z00,
		BGl_z62sawczd2foreignzd2clausezd2supportzb0zzbackend_cvmz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2requirezd2tailczd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2requir1418z00,
		BGl_z62cgenzd2requirezd2tailczd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2typedzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typedza71419za7,
		BGl_z62sawczd2typedzd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2tracezd2supportzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2traceza71420za7,
		BGl_z62cgenzd2tracezd2supportzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2foreignzd2closurezd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2foreign1421z00,
		BGl_z62cvmzd2foreignzd2closurezd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2externzd2typeszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2externza71422za7,
		BGl_z62cvmzd2externzd2typeszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2removezd2emptyzd2letzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2remove1423z00,
		BGl_z62sawczd2removezd2emptyzd2letzd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2tracezd2supportzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2traceza71424za7,
		BGl_z62sawczd2tracezd2supportz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2languagezd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2languag1425z00,
		BGl_z62cvmzd2languagezb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2srfi0zd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2srfi0za7b1426za7,
		BGl_z62cvmzd2srfi0zb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2typedzd2eqzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typedza7d1427za7,
		BGl_z62cvmzd2typedzd2eqz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2pregisterszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2pregis1428z00,
		BGl_z62sawczd2pregisterszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2registerszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2regist1429z00,
		BGl_z62cgenzd2registerszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2typedzd2funcallzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typedza71430za7,
		BGl_z62cgenzd2typedzd2funcallz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2externzd2functionszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2extern1431z00,
		BGl_z62cgenzd2externzd2functionsz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2pragmazd2supportzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2pragma1432z00,
		BGl_z62cgenzd2pragmazd2supportz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2typedzd2funcallzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typedza71433za7,
		BGl_z62sawczd2typedzd2funcallzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczf3zd2envz21zzbackend_cvmz00,
		BgL_bgl_za762sawcza7f3za791za7za7b1434za7,
		BGl_z62sawczf3z91zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2typedzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typedza71435za7,
		BGl_z62cgenzd2typedzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2externzd2variableszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2extern1436z00,
		BGl_z62sawczd2externzd2variableszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2heapzd2compatiblezd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2heapza7d21437za7,
		BGl_z62cvmzd2heapzd2compatiblezd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2sawczd2envz00zzbackend_cvmz00,
		BgL_bgl_za762makeza7d2sawcza7b1438za7,
		BGl_z62makezd2sawczb0zzbackend_cvmz00, 0L, BUNSPEC, 29);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2externzd2typeszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2extern1439z00,
		BGl_z62cgenzd2externzd2typeszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2requirezd2tailczd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2requir1440z00,
		BGl_z62cgenzd2requirezd2tailcz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2typedzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typedza71441za7,
		BGl_z62sawczd2typedzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2debugzd2supportzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2debugza71442za7,
		BGl_z62sawczd2debugzd2supportzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2effectzb2zd2setz12zd2envz72zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2effect1443z00,
		BGl_z62sawczd2effectzb2zd2setz12zc2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2foreignzd2closurezd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2foreig1444z00,
		BGl_z62sawczd2foreignzd2closurez62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2pregisterszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2pregist1445z00,
		BGl_z62cvmzd2pregisterszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2srfi0zd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2srfi0za71446za7,
		BGl_z62sawczd2srfi0zd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2pregisterszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2pregist1447z00,
		BGl_z62cvmzd2pregisterszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2namezd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2nameza7b1448za7,
		BGl_z62sawczd2namezb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2namezd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2nameza7d1449za7,
		BGl_z62cgenzd2namezd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2variableszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2variabl1450z00,
		BGl_z62cvmzd2variableszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2pregisterszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2pregis1451z00,
		BGl_z62cgenzd2pregisterszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2registerszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2regist1452z00,
		BGl_z62cgenzd2registerszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2typedzd2funcallzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typedza71453za7,
		BGl_z62cgenzd2typedzd2funcallzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2foreignzd2closurezd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2foreign1454z00,
		BGl_z62cvmzd2foreignzd2closurez62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2tvectorzd2descrzd2supportzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2tvecto1455z00,
		BGl_z62sawczd2tvectorzd2descrzd2supportzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2qualifiedzd2typeszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2qualif1456z00,
		BGl_z62sawczd2qualifiedzd2typesz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2debugzd2supportzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2debugza71457za7,
		BGl_z62cgenzd2debugzd2supportzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2pragmazd2supportzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2pragmaza71458za7,
		BGl_z62cvmzd2pragmazd2supportz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2pragmazd2supportzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2pragma1459z00,
		BGl_z62sawczd2pragmazd2supportzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2boundzd2checkzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2boundza71460za7,
		BGl_z62sawczd2boundzd2checkzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2debugzd2supportzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2debugza71461za7,
		BGl_z62cgenzd2debugzd2supportz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1375z00zzbackend_cvmz00,
		BgL_bgl_za762za7c3za704anonymo1462za7,
		BGl_z62zc3z04anonymousza31330ze3ze5zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1376z00zzbackend_cvmz00,
		BgL_bgl_za762lambda1328za7621463z00, BGl_z62lambda1328z62zzbackend_cvmz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1383z00zzbackend_cvmz00,
		BgL_bgl_string1383za700za7za7b1464za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1377z00zzbackend_cvmz00,
		BgL_bgl_za762za7c3za704anonymo1465za7,
		BGl_z62zc3z04anonymousza31347ze3ze5zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1384z00zzbackend_cvmz00,
		BgL_bgl_string1384za700za7za7b1466za7, "backend_cvm", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1378z00zzbackend_cvmz00,
		BgL_bgl_za762lambda1343za7621467z00, BGl_z62lambda1343z62zzbackend_cvmz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1385z00zzbackend_cvmz00,
		BgL_bgl_string1385za700za7za7b1468za7, "_ cgen sawc backend_cvm cvm ", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1379z00zzbackend_cvmz00,
		BgL_bgl_za762lambda1340za7621469z00, BGl_z62lambda1340z62zzbackend_cvmz00,
		0L, BUNSPEC, 32);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2registerszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2regist1470z00,
		BGl_z62sawczd2registerszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2typedzd2eqzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typedza7d1471za7,
		BGl_z62cvmzd2typedzd2eqzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2callcczd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2callcc1472z00,
		BGl_z62sawczd2callcczb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2foreignzd2closurezd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2foreig1473z00,
		BGl_z62sawczd2foreignzd2closurezd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2qualifiedzd2typeszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2qualifi1474z00,
		BGl_z62cvmzd2qualifiedzd2typesz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1380z00zzbackend_cvmz00,
		BgL_bgl_za762za7c3za704anonymo1475za7,
		BGl_z62zc3z04anonymousza31368ze3ze5zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1381z00zzbackend_cvmz00,
		BgL_bgl_za762lambda1365za7621476z00, BGl_z62lambda1365z62zzbackend_cvmz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1382z00zzbackend_cvmz00,
		BgL_bgl_za762lambda1362za7621477z00, BGl_z62lambda1362z62zzbackend_cvmz00,
		0L, BUNSPEC, 32);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2pragmazd2supportzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2pragmaza71478za7,
		BGl_z62cvmzd2pragmazd2supportzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2heapzd2compatiblezd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2heapza7d1479za7,
		BGl_z62cgenzd2heapzd2compatiblez62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2foreignzd2closurezd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2foreig1480z00,
		BGl_z62cgenzd2foreignzd2closurezd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2languagezd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2langua1481z00,
		BGl_z62sawczd2languagezb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2typedzd2eqzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typedza71482za7,
		BGl_z62sawczd2typedzd2eqz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2requirezd2tailczd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2require1483z00,
		BGl_z62cvmzd2requirezd2tailczd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2registerszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2registe1484z00,
		BGl_z62cvmzd2registerszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2tracezd2supportzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2traceza7d1485za7,
		BGl_z62cvmzd2tracezd2supportzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cgenzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762makeza7d2cgenza7b1486za7,
		BGl_z62makezd2cgenzb0zzbackend_cvmz00, 0L, BUNSPEC, 29);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2typeszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typesza71487za7,
		BGl_z62sawczd2typeszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2namezd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2nameza7d1488za7,
		BGl_z62sawczd2namezd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2heapzd2compatiblezd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2heapza7d1489za7,
		BGl_z62sawczd2heapzd2compatiblezd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2effectzb2zd2envzb2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2effect1490z00,
		BGl_z62cgenzd2effectzb2z02zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2typezd2checkzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typeza7d1491za7,
		BGl_z62sawczd2typezd2checkz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2heapzd2compatiblezd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2heapza7d1492za7,
		BGl_z62cgenzd2heapzd2compatiblezd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2externzd2functionszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2extern1493z00,
		BGl_z62sawczd2externzd2functionszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2typezd2checkzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typeza7d1494za7,
		BGl_z62sawczd2typezd2checkzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2callcczd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2callcc1495z00,
		BGl_z62cgenzd2callcczd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2boundzd2checkzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2boundza71496za7,
		BGl_z62cgenzd2boundzd2checkzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2foreignzd2clausezd2supportzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2foreign1497z00,
		BGl_z62cvmzd2foreignzd2clausezd2supportzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2tvectorzd2descrzd2supportzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2tvecto1498z00,
		BGl_z62cgenzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_cvmz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2typedzd2funcallzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typedza71499za7,
		BGl_z62sawczd2typedzd2funcallz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2typedzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typedza7d1500za7,
		BGl_z62cvmzd2typedzd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2typedzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typedza71501za7,
		BGl_z62cgenzd2typedzd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2functionszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2functio1502z00,
		BGl_z62cvmzd2functionszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2requirezd2tailczd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2requir1503z00,
		BGl_z62sawczd2requirezd2tailcz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2effectzb2zd2envzb2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2effect1504z00,
		BGl_z62sawczd2effectzb2z02zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2typezd2checkzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typeza7d21505za7,
		BGl_z62cvmzd2typezd2checkzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2effectzb2zd2setz12zd2envz72zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2effect1506z00,
		BGl_z62cgenzd2effectzb2zd2setz12zc2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2typedzd2funcallzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typedza7d1507za7,
		BGl_z62cvmzd2typedzd2funcallzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2typedzd2eqzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typedza71508za7,
		BGl_z62cgenzd2typedzd2eqzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2debugzd2supportzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2debugza7d1509za7,
		BGl_z62cvmzd2debugzd2supportzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2qualifiedzd2typeszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2qualifi1510z00,
		BGl_z62cvmzd2qualifiedzd2typeszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2effectzb2zd2envzb2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2effectza71511za7,
		BGl_z62cvmzd2effectzb2z02zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2languagezd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2languag1512z00,
		BGl_z62cvmzd2languagezd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2srfi0zd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2srfi0za7d1513za7,
		BGl_z62cvmzd2srfi0zd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2variableszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2variab1514z00,
		BGl_z62cgenzd2variableszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2srfi0zd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2srfi0za71515za7,
		BGl_z62cgenzd2srfi0zd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2debugzd2supportzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2debugza71516za7,
		BGl_z62sawczd2debugzd2supportz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2externzd2typeszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2extern1517z00,
		BGl_z62sawczd2externzd2typeszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2srfi0zd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2srfi0za71518za7,
		BGl_z62cgenzd2srfi0zb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2variableszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2variab1519z00,
		BGl_z62sawczd2variableszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2removezd2emptyzd2letzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2removeza71520za7,
		BGl_z62cvmzd2removezd2emptyzd2letzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2srfi0zd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2srfi0za71521za7,
		BGl_z62sawczd2srfi0zb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2tvectorzd2descrzd2supportzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2tvecto1522z00,
		BGl_z62sawczd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_cvmz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2boundzd2checkzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2boundza71523za7,
		BGl_z62cgenzd2boundzd2checkz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2externzd2variableszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2externza71524za7,
		BGl_z62cvmzd2externzd2variablesz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2callcczd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2callccza71525za7,
		BGl_z62cvmzd2callcczb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2callcczd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2callcc1526z00,
		BGl_z62sawczd2callcczd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2removezd2emptyzd2letzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2remove1527z00,
		BGl_z62cgenzd2removezd2emptyzd2letzd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2typeszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typesza7b1528za7,
		BGl_z62cvmzd2typeszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2externzd2typeszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2externza71529za7,
		BGl_z62cvmzd2externzd2typesz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2externzd2variableszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2extern1530z00,
		BGl_z62cgenzd2externzd2variableszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2typeszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typesza7d1531za7,
		BGl_z62cvmzd2typeszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2heapzd2compatiblezd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2heapza7d1532za7,
		BGl_z62sawczd2heapzd2compatiblez62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2callcczd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2callccza71533za7,
		BGl_z62cvmzd2callcczd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2variableszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2variab1534z00,
		BGl_z62sawczd2variableszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2typeszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typesza71535za7,
		BGl_z62cgenzd2typeszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2typezd2checkzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typeza7d21536za7,
		BGl_z62cvmzd2typezd2checkz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2languagezd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2langua1537z00,
		BGl_z62cgenzd2languagezd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2removezd2emptyzd2letzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2removeza71538za7,
		BGl_z62cvmzd2removezd2emptyzd2letzd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2heapzd2suffixzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2heapza7d1539za7,
		BGl_z62cgenzd2heapzd2suffixz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2qualifiedzd2typeszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2qualif1540z00,
		BGl_z62sawczd2qualifiedzd2typeszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2tracezd2supportzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2traceza7d1541za7,
		BGl_z62cvmzd2tracezd2supportz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2heapzd2suffixzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2heapza7d21542za7,
		BGl_z62cvmzd2heapzd2suffixzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2typezd2checkzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typeza7d1543za7,
		BGl_z62cgenzd2typezd2checkz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2qualifiedzd2typeszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2qualif1544z00,
		BGl_z62cgenzd2qualifiedzd2typeszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2heapzd2compatiblezd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2heapza7d21545za7,
		BGl_z62cvmzd2heapzd2compatiblez62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2externzd2variableszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2externza71546za7,
		BGl_z62cvmzd2externzd2variableszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2namezd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2nameza7b1547za7,
		BGl_z62cgenzd2namezb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2languagezd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2langua1548z00,
		BGl_z62cgenzd2languagezb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2typedzd2eqzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typedza71549za7,
		BGl_z62cgenzd2typedzd2eqz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2tvectorzd2descrzd2supportzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2tvecto1550z00,
		BGl_z62cgenzd2tvectorzd2descrzd2supportzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2typedzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typedza7b1551za7,
		BGl_z62cvmzd2typedzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2typedzd2eqzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2typedza71552za7,
		BGl_z62sawczd2typedzd2eqzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2nilzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2nilza7b01553za7, BGl_z62sawczd2nilzb0zzbackend_cvmz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2functionszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2functi1554z00,
		BGl_z62cgenzd2functionszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2callcczd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2callcc1555z00,
		BGl_z62cgenzd2callcczb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2removezd2emptyzd2letzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2remove1556z00,
		BGl_z62sawczd2removezd2emptyzd2letzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2variableszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2variab1557z00,
		BGl_z62cgenzd2variableszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2nilzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2nilza7b01558za7, BGl_z62cgenzd2nilzb0zzbackend_cvmz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2functionszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2functi1559z00,
		BGl_z62sawczd2functionszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2foreignzd2clausezd2supportzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2foreig1560z00,
		BGl_z62sawczd2foreignzd2clausezd2supportzd2setz12z70zzbackend_cvmz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2nilzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2nilza7b0za71561z00,
		BGl_z62cvmzd2nilzb0zzbackend_cvmz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2externzd2variableszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2extern1562z00,
		BGl_z62sawczd2externzd2variablesz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzf3zd2envz21zzbackend_cvmz00,
		BgL_bgl_za762cgenza7f3za791za7za7b1563za7,
		BGl_z62cgenzf3z91zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2boundzd2checkzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2boundza71564za7,
		BGl_z62sawczd2boundzd2checkz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2tvectorzd2descrzd2supportzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2tvector1565z00,
		BGl_z62cvmzd2tvectorzd2descrzd2supportzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2pragmazd2supportzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2pragma1566z00,
		BGl_z62cgenzd2pragmazd2supportzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2pregisterszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2pregis1567z00,
		BGl_z62cgenzd2pregisterszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2externzd2functionszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2externza71568za7,
		BGl_z62cvmzd2externzd2functionsz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2externzd2typeszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2extern1569z00,
		BGl_z62sawczd2externzd2typesz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2foreignzd2clausezd2supportzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2foreign1570z00,
		BGl_z62cvmzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_cvmz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2registerszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2registe1571z00,
		BGl_z62cvmzd2registerszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2boundzd2checkzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2boundza7d1572za7,
		BGl_z62cvmzd2boundzd2checkz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2foreignzd2closurezd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2foreig1573z00,
		BGl_z62cgenzd2foreignzd2closurez62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2externzd2functionszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2extern1574z00,
		BGl_z62cgenzd2externzd2functionszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2tracezd2supportzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2traceza71575za7,
		BGl_z62cgenzd2tracezd2supportz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2variableszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2variabl1576z00,
		BGl_z62cvmzd2variableszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2functionszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2functi1577z00,
		BGl_z62sawczd2functionszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2heapzd2suffixzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2heapza7d1578za7,
		BGl_z62sawczd2heapzd2suffixzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2heapzd2suffixzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2heapza7d1579za7,
		BGl_z62sawczd2heapzd2suffixz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2tvectorzd2descrzd2supportzd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2tvector1580z00,
		BGl_z62cvmzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_cvmz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawczd2pregisterszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2pregis1581z00,
		BGl_z62sawczd2pregisterszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2removezd2emptyzd2letzd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2remove1582z00,
		BGl_z62cgenzd2removezd2emptyzd2letzb0zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2namezd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2nameza7d21583za7,
		BGl_z62cvmzd2namezd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2qualifiedzd2typeszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2qualif1584z00,
		BGl_z62cgenzd2qualifiedzd2typesz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2externzd2variableszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2extern1585z00,
		BGl_z62cgenzd2externzd2variablesz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2externzd2functionszd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2externza71586za7,
		BGl_z62cvmzd2externzd2functionszd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2heapzd2suffixzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2heapza7d21587za7,
		BGl_z62cvmzd2heapzd2suffixz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2languagezd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2langua1588z00,
		BGl_z62sawczd2languagezd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2pragmazd2supportzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2pragma1589z00,
		BGl_z62sawczd2pragmazd2supportz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzf3zd2envz21zzbackend_cvmz00,
		BgL_bgl_za762cvmza7f3za791za7za7ba1590za7, BGl_z62cvmzf3z91zzbackend_cvmz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2typedzd2funcallzd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2typedza7d1591za7,
		BGl_z62cvmzd2typedzd2funcallz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2functionszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2functi1592z00,
		BGl_z62cgenzd2functionszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2effectzb2zd2setz12zd2envz72zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2effectza71593za7,
		BGl_z62cvmzd2effectzb2zd2setz12zc2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvmzd2requirezd2tailczd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2require1594z00,
		BGl_z62cvmzd2requirezd2tailcz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvmzd2namezd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cvmza7d2nameza7b01595za7, BGl_z62cvmzd2namezb0zzbackend_cvmz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2externzd2functionszd2envzd2zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2extern1596z00,
		BGl_z62sawczd2externzd2functionsz62zzbackend_cvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cgenzd2heapzd2suffixzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2heapza7d1597za7,
		BGl_z62cgenzd2heapzd2suffixzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2requirezd2tailczd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2requir1598z00,
		BGl_z62sawczd2requirezd2tailczd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2registerszd2setz12zd2envzc0zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2regist1599z00,
		BGl_z62sawczd2registerszd2setz12z70zzbackend_cvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sawczd2tracezd2supportzd2setz12zd2envz12zzbackend_cvmz00,
		BgL_bgl_za762sawcza7d2traceza71600za7,
		BGl_z62sawczd2tracezd2supportzd2setz12za2zzbackend_cvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2typeszd2envz00zzbackend_cvmz00,
		BgL_bgl_za762cgenza7d2typesza71601za7,
		BGl_z62cgenzd2typeszb0zzbackend_cvmz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzbackend_cvmz00));
		     ADD_ROOT((void *) (&BGl_cgenz00zzbackend_cvmz00));
		     ADD_ROOT((void *) (&BGl_sawcz00zzbackend_cvmz00));
		     ADD_ROOT((void *) (&BGl_cvmz00zzbackend_cvmz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzbackend_cvmz00(long
		BgL_checksumz00_1832, char *BgL_fromz00_1833)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_cvmz00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_cvmz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_cvmz00();
					BGl_libraryzd2moduleszd2initz00zzbackend_cvmz00();
					BGl_cnstzd2initzd2zzbackend_cvmz00();
					BGl_importedzd2moduleszd2initz00zzbackend_cvmz00();
					BGl_objectzd2initzd2zzbackend_cvmz00();
					return BGl_toplevelzd2initzd2zzbackend_cvmz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "backend_cvm");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_cvm");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "backend_cvm");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "backend_cvm");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"backend_cvm");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "backend_cvm");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_cvm");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.scm 15 */
			{	/* BackEnd/cvm.scm 15 */
				obj_t BgL_cportz00_1699;

				{	/* BackEnd/cvm.scm 15 */
					obj_t BgL_stringz00_1706;

					BgL_stringz00_1706 = BGl_string1385z00zzbackend_cvmz00;
					{	/* BackEnd/cvm.scm 15 */
						obj_t BgL_startz00_1707;

						BgL_startz00_1707 = BINT(0L);
						{	/* BackEnd/cvm.scm 15 */
							obj_t BgL_endz00_1708;

							BgL_endz00_1708 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1706)));
							{	/* BackEnd/cvm.scm 15 */

								BgL_cportz00_1699 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1706, BgL_startz00_1707, BgL_endz00_1708);
				}}}}
				{
					long BgL_iz00_1700;

					BgL_iz00_1700 = 4L;
				BgL_loopz00_1701:
					if ((BgL_iz00_1700 == -1L))
						{	/* BackEnd/cvm.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/cvm.scm 15 */
							{	/* BackEnd/cvm.scm 15 */
								obj_t BgL_arg1408z00_1702;

								{	/* BackEnd/cvm.scm 15 */

									{	/* BackEnd/cvm.scm 15 */
										obj_t BgL_locationz00_1704;

										BgL_locationz00_1704 = BBOOL(((bool_t) 0));
										{	/* BackEnd/cvm.scm 15 */

											BgL_arg1408z00_1702 =
												BGl_readz00zz__readerz00(BgL_cportz00_1699,
												BgL_locationz00_1704);
										}
									}
								}
								{	/* BackEnd/cvm.scm 15 */
									int BgL_tmpz00_1859;

									BgL_tmpz00_1859 = (int) (BgL_iz00_1700);
									CNST_TABLE_SET(BgL_tmpz00_1859, BgL_arg1408z00_1702);
							}}
							{	/* BackEnd/cvm.scm 15 */
								int BgL_auxz00_1705;

								BgL_auxz00_1705 = (int) ((BgL_iz00_1700 - 1L));
								{
									long BgL_iz00_1864;

									BgL_iz00_1864 = (long) (BgL_auxz00_1705);
									BgL_iz00_1700 = BgL_iz00_1864;
									goto BgL_loopz00_1701;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.scm 15 */
			return BUNSPEC;
		}

	}



/* cvm? */
	BGL_EXPORTED_DEF bool_t BGl_cvmzf3zf3zzbackend_cvmz00(obj_t BgL_objz00_3)
	{
		{	/* BackEnd/cvm.sch 208 */
			{	/* BackEnd/cvm.sch 208 */
				obj_t BgL_classz00_1710;

				BgL_classz00_1710 = BGl_cvmz00zzbackend_cvmz00;
				if (BGL_OBJECTP(BgL_objz00_3))
					{	/* BackEnd/cvm.sch 208 */
						BgL_objectz00_bglt BgL_arg1807z00_1711;

						BgL_arg1807z00_1711 = (BgL_objectz00_bglt) (BgL_objz00_3);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* BackEnd/cvm.sch 208 */
								long BgL_idxz00_1712;

								BgL_idxz00_1712 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1711);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_1712 + 2L)) == BgL_classz00_1710);
							}
						else
							{	/* BackEnd/cvm.sch 208 */
								bool_t BgL_res1371z00_1715;

								{	/* BackEnd/cvm.sch 208 */
									obj_t BgL_oclassz00_1716;

									{	/* BackEnd/cvm.sch 208 */
										obj_t BgL_arg1815z00_1717;
										long BgL_arg1816z00_1718;

										BgL_arg1815z00_1717 = (BGl_za2classesza2z00zz__objectz00);
										{	/* BackEnd/cvm.sch 208 */
											long BgL_arg1817z00_1719;

											BgL_arg1817z00_1719 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1711);
											BgL_arg1816z00_1718 = (BgL_arg1817z00_1719 - OBJECT_TYPE);
										}
										BgL_oclassz00_1716 =
											VECTOR_REF(BgL_arg1815z00_1717, BgL_arg1816z00_1718);
									}
									{	/* BackEnd/cvm.sch 208 */
										bool_t BgL__ortest_1115z00_1720;

										BgL__ortest_1115z00_1720 =
											(BgL_classz00_1710 == BgL_oclassz00_1716);
										if (BgL__ortest_1115z00_1720)
											{	/* BackEnd/cvm.sch 208 */
												BgL_res1371z00_1715 = BgL__ortest_1115z00_1720;
											}
										else
											{	/* BackEnd/cvm.sch 208 */
												long BgL_odepthz00_1721;

												{	/* BackEnd/cvm.sch 208 */
													obj_t BgL_arg1804z00_1722;

													BgL_arg1804z00_1722 = (BgL_oclassz00_1716);
													BgL_odepthz00_1721 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_1722);
												}
												if ((2L < BgL_odepthz00_1721))
													{	/* BackEnd/cvm.sch 208 */
														obj_t BgL_arg1802z00_1723;

														{	/* BackEnd/cvm.sch 208 */
															obj_t BgL_arg1803z00_1724;

															BgL_arg1803z00_1724 = (BgL_oclassz00_1716);
															BgL_arg1802z00_1723 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1724,
																2L);
														}
														BgL_res1371z00_1715 =
															(BgL_arg1802z00_1723 == BgL_classz00_1710);
													}
												else
													{	/* BackEnd/cvm.sch 208 */
														BgL_res1371z00_1715 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1371z00_1715;
							}
					}
				else
					{	/* BackEnd/cvm.sch 208 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cvm? */
	obj_t BGl_z62cvmzf3z91zzbackend_cvmz00(obj_t BgL_envz00_1110,
		obj_t BgL_objz00_1111)
	{
		{	/* BackEnd/cvm.sch 208 */
			return BBOOL(BGl_cvmzf3zf3zzbackend_cvmz00(BgL_objz00_1111));
		}

	}



/* cvm-nil */
	BGL_EXPORTED_DEF BgL_cvmz00_bglt BGl_cvmzd2nilzd2zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.sch 209 */
			{	/* BackEnd/cvm.sch 209 */
				obj_t BgL_classz00_825;

				BgL_classz00_825 = BGl_cvmz00zzbackend_cvmz00;
				{	/* BackEnd/cvm.sch 209 */
					obj_t BgL__ortest_1117z00_826;

					BgL__ortest_1117z00_826 = BGL_CLASS_NIL(BgL_classz00_825);
					if (CBOOL(BgL__ortest_1117z00_826))
						{	/* BackEnd/cvm.sch 209 */
							return ((BgL_cvmz00_bglt) BgL__ortest_1117z00_826);
						}
					else
						{	/* BackEnd/cvm.sch 209 */
							return
								((BgL_cvmz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_825));
						}
				}
			}
		}

	}



/* &cvm-nil */
	BgL_cvmz00_bglt BGl_z62cvmzd2nilzb0zzbackend_cvmz00(obj_t BgL_envz00_1112)
	{
		{	/* BackEnd/cvm.sch 209 */
			return BGl_cvmzd2nilzd2zzbackend_cvmz00();
		}

	}



/* cvm-typed-funcall */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2typedzd2funcallz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_4)
	{
		{	/* BackEnd/cvm.sch 210 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_4)))->BgL_typedzd2funcallzd2);
		}

	}



/* &cvm-typed-funcall */
	obj_t BGl_z62cvmzd2typedzd2funcallz62zzbackend_cvmz00(obj_t BgL_envz00_1113,
		obj_t BgL_oz00_1114)
	{
		{	/* BackEnd/cvm.sch 210 */
			return
				BBOOL(BGl_cvmzd2typedzd2funcallz00zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1114)));
		}

	}



/* cvm-typed-funcall-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2typedzd2funcallzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_5, bool_t BgL_vz00_6)
	{
		{	/* BackEnd/cvm.sch 211 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_5)))->BgL_typedzd2funcallzd2) =
				((bool_t) BgL_vz00_6), BUNSPEC);
		}

	}



/* &cvm-typed-funcall-set! */
	obj_t BGl_z62cvmzd2typedzd2funcallzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1115, obj_t BgL_oz00_1116, obj_t BgL_vz00_1117)
	{
		{	/* BackEnd/cvm.sch 211 */
			return
				BGl_cvmzd2typedzd2funcallzd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1116), CBOOL(BgL_vz00_1117));
		}

	}



/* cvm-type-check */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2typezd2checkz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_7)
	{
		{	/* BackEnd/cvm.sch 212 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_7)))->BgL_typezd2checkzd2);
		}

	}



/* &cvm-type-check */
	obj_t BGl_z62cvmzd2typezd2checkz62zzbackend_cvmz00(obj_t BgL_envz00_1118,
		obj_t BgL_oz00_1119)
	{
		{	/* BackEnd/cvm.sch 212 */
			return
				BBOOL(BGl_cvmzd2typezd2checkz00zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1119)));
		}

	}



/* cvm-type-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2typezd2checkzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_8, bool_t BgL_vz00_9)
	{
		{	/* BackEnd/cvm.sch 213 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_8)))->BgL_typezd2checkzd2) =
				((bool_t) BgL_vz00_9), BUNSPEC);
		}

	}



/* &cvm-type-check-set! */
	obj_t BGl_z62cvmzd2typezd2checkzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1120, obj_t BgL_oz00_1121, obj_t BgL_vz00_1122)
	{
		{	/* BackEnd/cvm.sch 213 */
			return
				BGl_cvmzd2typezd2checkzd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1121), CBOOL(BgL_vz00_1122));
		}

	}



/* cvm-bound-check */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2boundzd2checkz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_10)
	{
		{	/* BackEnd/cvm.sch 214 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_10)))->BgL_boundzd2checkzd2);
		}

	}



/* &cvm-bound-check */
	obj_t BGl_z62cvmzd2boundzd2checkz62zzbackend_cvmz00(obj_t BgL_envz00_1123,
		obj_t BgL_oz00_1124)
	{
		{	/* BackEnd/cvm.sch 214 */
			return
				BBOOL(BGl_cvmzd2boundzd2checkz00zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1124)));
		}

	}



/* cvm-bound-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2boundzd2checkzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_11, bool_t BgL_vz00_12)
	{
		{	/* BackEnd/cvm.sch 215 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_11)))->BgL_boundzd2checkzd2) =
				((bool_t) BgL_vz00_12), BUNSPEC);
		}

	}



/* &cvm-bound-check-set! */
	obj_t BGl_z62cvmzd2boundzd2checkzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1125, obj_t BgL_oz00_1126, obj_t BgL_vz00_1127)
	{
		{	/* BackEnd/cvm.sch 215 */
			return
				BGl_cvmzd2boundzd2checkzd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1126), CBOOL(BgL_vz00_1127));
		}

	}



/* cvm-pregisters */
	BGL_EXPORTED_DEF obj_t BGl_cvmzd2pregisterszd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_13)
	{
		{	/* BackEnd/cvm.sch 216 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_13)))->BgL_pregistersz00);
		}

	}



/* &cvm-pregisters */
	obj_t BGl_z62cvmzd2pregisterszb0zzbackend_cvmz00(obj_t BgL_envz00_1128,
		obj_t BgL_oz00_1129)
	{
		{	/* BackEnd/cvm.sch 216 */
			return
				BGl_cvmzd2pregisterszd2zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1129));
		}

	}



/* cvm-pregisters-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2pregisterszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_14, obj_t BgL_vz00_15)
	{
		{	/* BackEnd/cvm.sch 217 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_14)))->BgL_pregistersz00) =
				((obj_t) BgL_vz00_15), BUNSPEC);
		}

	}



/* &cvm-pregisters-set! */
	obj_t BGl_z62cvmzd2pregisterszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1130, obj_t BgL_oz00_1131, obj_t BgL_vz00_1132)
	{
		{	/* BackEnd/cvm.sch 217 */
			return
				BGl_cvmzd2pregisterszd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1131), BgL_vz00_1132);
		}

	}



/* cvm-registers */
	BGL_EXPORTED_DEF obj_t BGl_cvmzd2registerszd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_16)
	{
		{	/* BackEnd/cvm.sch 218 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_16)))->BgL_registersz00);
		}

	}



/* &cvm-registers */
	obj_t BGl_z62cvmzd2registerszb0zzbackend_cvmz00(obj_t BgL_envz00_1133,
		obj_t BgL_oz00_1134)
	{
		{	/* BackEnd/cvm.sch 218 */
			return
				BGl_cvmzd2registerszd2zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1134));
		}

	}



/* cvm-registers-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2registerszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_17,
		obj_t BgL_vz00_18)
	{
		{	/* BackEnd/cvm.sch 219 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_17)))->BgL_registersz00) =
				((obj_t) BgL_vz00_18), BUNSPEC);
		}

	}



/* &cvm-registers-set! */
	obj_t BGl_z62cvmzd2registerszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1135, obj_t BgL_oz00_1136, obj_t BgL_vz00_1137)
	{
		{	/* BackEnd/cvm.sch 219 */
			return
				BGl_cvmzd2registerszd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1136), BgL_vz00_1137);
		}

	}



/* cvm-require-tailc */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2requirezd2tailcz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_19)
	{
		{	/* BackEnd/cvm.sch 220 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_19)))->BgL_requirezd2tailczd2);
		}

	}



/* &cvm-require-tailc */
	obj_t BGl_z62cvmzd2requirezd2tailcz62zzbackend_cvmz00(obj_t BgL_envz00_1138,
		obj_t BgL_oz00_1139)
	{
		{	/* BackEnd/cvm.sch 220 */
			return
				BBOOL(BGl_cvmzd2requirezd2tailcz00zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1139)));
		}

	}



/* cvm-require-tailc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2requirezd2tailczd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_20, bool_t BgL_vz00_21)
	{
		{	/* BackEnd/cvm.sch 221 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_20)))->BgL_requirezd2tailczd2) =
				((bool_t) BgL_vz00_21), BUNSPEC);
		}

	}



/* &cvm-require-tailc-set! */
	obj_t BGl_z62cvmzd2requirezd2tailczd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1140, obj_t BgL_oz00_1141, obj_t BgL_vz00_1142)
	{
		{	/* BackEnd/cvm.sch 221 */
			return
				BGl_cvmzd2requirezd2tailczd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1141), CBOOL(BgL_vz00_1142));
		}

	}



/* cvm-tvector-descr-support */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2tvectorzd2descrzd2supportzd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_22)
	{
		{	/* BackEnd/cvm.sch 222 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_22)))->
				BgL_tvectorzd2descrzd2supportz00);
		}

	}



/* &cvm-tvector-descr-support */
	obj_t BGl_z62cvmzd2tvectorzd2descrzd2supportzb0zzbackend_cvmz00(obj_t
		BgL_envz00_1143, obj_t BgL_oz00_1144)
	{
		{	/* BackEnd/cvm.sch 222 */
			return
				BBOOL(BGl_cvmzd2tvectorzd2descrzd2supportzd2zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1144)));
		}

	}



/* cvm-tvector-descr-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_cvmz00_bglt BgL_oz00_23, bool_t BgL_vz00_24)
	{
		{	/* BackEnd/cvm.sch 223 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_23)))->
					BgL_tvectorzd2descrzd2supportz00) = ((bool_t) BgL_vz00_24), BUNSPEC);
		}

	}



/* &cvm-tvector-descr-support-set! */
	obj_t BGl_z62cvmzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1145, obj_t BgL_oz00_1146, obj_t BgL_vz00_1147)
	{
		{	/* BackEnd/cvm.sch 223 */
			return
				BGl_cvmzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1146), CBOOL(BgL_vz00_1147));
		}

	}



/* cvm-pragma-support */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2pragmazd2supportz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_25)
	{
		{	/* BackEnd/cvm.sch 224 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_25)))->BgL_pragmazd2supportzd2);
		}

	}



/* &cvm-pragma-support */
	obj_t BGl_z62cvmzd2pragmazd2supportz62zzbackend_cvmz00(obj_t BgL_envz00_1148,
		obj_t BgL_oz00_1149)
	{
		{	/* BackEnd/cvm.sch 224 */
			return
				BBOOL(BGl_cvmzd2pragmazd2supportz00zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1149)));
		}

	}



/* cvm-pragma-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2pragmazd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_26, bool_t BgL_vz00_27)
	{
		{	/* BackEnd/cvm.sch 225 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_26)))->BgL_pragmazd2supportzd2) =
				((bool_t) BgL_vz00_27), BUNSPEC);
		}

	}



/* &cvm-pragma-support-set! */
	obj_t BGl_z62cvmzd2pragmazd2supportzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1150, obj_t BgL_oz00_1151, obj_t BgL_vz00_1152)
	{
		{	/* BackEnd/cvm.sch 225 */
			return
				BGl_cvmzd2pragmazd2supportzd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1151), CBOOL(BgL_vz00_1152));
		}

	}



/* cvm-debug-support */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2debugzd2supportz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_28)
	{
		{	/* BackEnd/cvm.sch 226 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_28)))->BgL_debugzd2supportzd2);
		}

	}



/* &cvm-debug-support */
	obj_t BGl_z62cvmzd2debugzd2supportz62zzbackend_cvmz00(obj_t BgL_envz00_1153,
		obj_t BgL_oz00_1154)
	{
		{	/* BackEnd/cvm.sch 226 */
			return
				BGl_cvmzd2debugzd2supportz00zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1154));
		}

	}



/* cvm-debug-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2debugzd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* BackEnd/cvm.sch 227 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_29)))->BgL_debugzd2supportzd2) =
				((obj_t) BgL_vz00_30), BUNSPEC);
		}

	}



/* &cvm-debug-support-set! */
	obj_t BGl_z62cvmzd2debugzd2supportzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1155, obj_t BgL_oz00_1156, obj_t BgL_vz00_1157)
	{
		{	/* BackEnd/cvm.sch 227 */
			return
				BGl_cvmzd2debugzd2supportzd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1156), BgL_vz00_1157);
		}

	}



/* cvm-foreign-clause-support */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2foreignzd2clausezd2supportzd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_31)
	{
		{	/* BackEnd/cvm.sch 228 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_31)))->
				BgL_foreignzd2clausezd2supportz00);
		}

	}



/* &cvm-foreign-clause-support */
	obj_t BGl_z62cvmzd2foreignzd2clausezd2supportzb0zzbackend_cvmz00(obj_t
		BgL_envz00_1158, obj_t BgL_oz00_1159)
	{
		{	/* BackEnd/cvm.sch 228 */
			return
				BGl_cvmzd2foreignzd2clausezd2supportzd2zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1159));
		}

	}



/* cvm-foreign-clause-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_cvmz00_bglt BgL_oz00_32, obj_t BgL_vz00_33)
	{
		{	/* BackEnd/cvm.sch 229 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_32)))->
					BgL_foreignzd2clausezd2supportz00) = ((obj_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &cvm-foreign-clause-support-set! */
	obj_t
		BGl_z62cvmzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1160, obj_t BgL_oz00_1161, obj_t BgL_vz00_1162)
	{
		{	/* BackEnd/cvm.sch 229 */
			return
				BGl_cvmzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1161), BgL_vz00_1162);
		}

	}



/* cvm-trace-support */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2tracezd2supportz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_34)
	{
		{	/* BackEnd/cvm.sch 230 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_34)))->BgL_tracezd2supportzd2);
		}

	}



/* &cvm-trace-support */
	obj_t BGl_z62cvmzd2tracezd2supportz62zzbackend_cvmz00(obj_t BgL_envz00_1163,
		obj_t BgL_oz00_1164)
	{
		{	/* BackEnd/cvm.sch 230 */
			return
				BBOOL(BGl_cvmzd2tracezd2supportz00zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1164)));
		}

	}



/* cvm-trace-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2tracezd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_35, bool_t BgL_vz00_36)
	{
		{	/* BackEnd/cvm.sch 231 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_35)))->BgL_tracezd2supportzd2) =
				((bool_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &cvm-trace-support-set! */
	obj_t BGl_z62cvmzd2tracezd2supportzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1165, obj_t BgL_oz00_1166, obj_t BgL_vz00_1167)
	{
		{	/* BackEnd/cvm.sch 231 */
			return
				BGl_cvmzd2tracezd2supportzd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1166), CBOOL(BgL_vz00_1167));
		}

	}



/* cvm-typed-eq */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2typedzd2eqz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_37)
	{
		{	/* BackEnd/cvm.sch 232 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_37)))->BgL_typedzd2eqzd2);
		}

	}



/* &cvm-typed-eq */
	obj_t BGl_z62cvmzd2typedzd2eqz62zzbackend_cvmz00(obj_t BgL_envz00_1168,
		obj_t BgL_oz00_1169)
	{
		{	/* BackEnd/cvm.sch 232 */
			return
				BBOOL(BGl_cvmzd2typedzd2eqz00zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1169)));
		}

	}



/* cvm-typed-eq-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2typedzd2eqzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_38, bool_t BgL_vz00_39)
	{
		{	/* BackEnd/cvm.sch 233 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_38)))->BgL_typedzd2eqzd2) =
				((bool_t) BgL_vz00_39), BUNSPEC);
		}

	}



/* &cvm-typed-eq-set! */
	obj_t BGl_z62cvmzd2typedzd2eqzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1170, obj_t BgL_oz00_1171, obj_t BgL_vz00_1172)
	{
		{	/* BackEnd/cvm.sch 233 */
			return
				BGl_cvmzd2typedzd2eqzd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1171), CBOOL(BgL_vz00_1172));
		}

	}



/* cvm-foreign-closure */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2foreignzd2closurez00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_40)
	{
		{	/* BackEnd/cvm.sch 234 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_40)))->BgL_foreignzd2closurezd2);
		}

	}



/* &cvm-foreign-closure */
	obj_t BGl_z62cvmzd2foreignzd2closurez62zzbackend_cvmz00(obj_t BgL_envz00_1173,
		obj_t BgL_oz00_1174)
	{
		{	/* BackEnd/cvm.sch 234 */
			return
				BBOOL(BGl_cvmzd2foreignzd2closurez00zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1174)));
		}

	}



/* cvm-foreign-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2foreignzd2closurezd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_41, bool_t BgL_vz00_42)
	{
		{	/* BackEnd/cvm.sch 235 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_41)))->BgL_foreignzd2closurezd2) =
				((bool_t) BgL_vz00_42), BUNSPEC);
		}

	}



/* &cvm-foreign-closure-set! */
	obj_t BGl_z62cvmzd2foreignzd2closurezd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1175, obj_t BgL_oz00_1176, obj_t BgL_vz00_1177)
	{
		{	/* BackEnd/cvm.sch 235 */
			return
				BGl_cvmzd2foreignzd2closurezd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1176), CBOOL(BgL_vz00_1177));
		}

	}



/* cvm-remove-empty-let */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2removezd2emptyzd2letzd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_43)
	{
		{	/* BackEnd/cvm.sch 236 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_43)))->BgL_removezd2emptyzd2letz00);
		}

	}



/* &cvm-remove-empty-let */
	obj_t BGl_z62cvmzd2removezd2emptyzd2letzb0zzbackend_cvmz00(obj_t
		BgL_envz00_1178, obj_t BgL_oz00_1179)
	{
		{	/* BackEnd/cvm.sch 236 */
			return
				BBOOL(BGl_cvmzd2removezd2emptyzd2letzd2zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1179)));
		}

	}



/* cvm-remove-empty-let-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2removezd2emptyzd2letzd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_44, bool_t BgL_vz00_45)
	{
		{	/* BackEnd/cvm.sch 237 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_44)))->
					BgL_removezd2emptyzd2letz00) = ((bool_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &cvm-remove-empty-let-set! */
	obj_t BGl_z62cvmzd2removezd2emptyzd2letzd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1180, obj_t BgL_oz00_1181, obj_t BgL_vz00_1182)
	{
		{	/* BackEnd/cvm.sch 237 */
			return
				BGl_cvmzd2removezd2emptyzd2letzd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1181), CBOOL(BgL_vz00_1182));
		}

	}



/* cvm-effect+ */
	BGL_EXPORTED_DEF bool_t BGl_cvmzd2effectzb2z60zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_46)
	{
		{	/* BackEnd/cvm.sch 238 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_46)))->BgL_effectzb2zb2);
		}

	}



/* &cvm-effect+ */
	obj_t BGl_z62cvmzd2effectzb2z02zzbackend_cvmz00(obj_t BgL_envz00_1183,
		obj_t BgL_oz00_1184)
	{
		{	/* BackEnd/cvm.sch 238 */
			return
				BBOOL(BGl_cvmzd2effectzb2z60zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1184)));
		}

	}



/* cvm-effect+-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2effectzb2zd2setz12za0zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_47,
		bool_t BgL_vz00_48)
	{
		{	/* BackEnd/cvm.sch 239 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_47)))->BgL_effectzb2zb2) =
				((bool_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &cvm-effect+-set! */
	obj_t BGl_z62cvmzd2effectzb2zd2setz12zc2zzbackend_cvmz00(obj_t
		BgL_envz00_1185, obj_t BgL_oz00_1186, obj_t BgL_vz00_1187)
	{
		{	/* BackEnd/cvm.sch 239 */
			return
				BGl_cvmzd2effectzb2zd2setz12za0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1186), CBOOL(BgL_vz00_1187));
		}

	}



/* cvm-qualified-types */
	BGL_EXPORTED_DEF bool_t
		BGl_cvmzd2qualifiedzd2typesz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_49)
	{
		{	/* BackEnd/cvm.sch 240 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_49)))->BgL_qualifiedzd2typeszd2);
		}

	}



/* &cvm-qualified-types */
	obj_t BGl_z62cvmzd2qualifiedzd2typesz62zzbackend_cvmz00(obj_t BgL_envz00_1188,
		obj_t BgL_oz00_1189)
	{
		{	/* BackEnd/cvm.sch 240 */
			return
				BBOOL(BGl_cvmzd2qualifiedzd2typesz00zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1189)));
		}

	}



/* cvm-qualified-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2qualifiedzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_50, bool_t BgL_vz00_51)
	{
		{	/* BackEnd/cvm.sch 241 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_50)))->BgL_qualifiedzd2typeszd2) =
				((bool_t) BgL_vz00_51), BUNSPEC);
		}

	}



/* &cvm-qualified-types-set! */
	obj_t BGl_z62cvmzd2qualifiedzd2typeszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1190, obj_t BgL_oz00_1191, obj_t BgL_vz00_1192)
	{
		{	/* BackEnd/cvm.sch 241 */
			return
				BGl_cvmzd2qualifiedzd2typeszd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1191), CBOOL(BgL_vz00_1192));
		}

	}



/* cvm-callcc */
	BGL_EXPORTED_DEF bool_t BGl_cvmzd2callcczd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_52)
	{
		{	/* BackEnd/cvm.sch 242 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_52)))->BgL_callccz00);
		}

	}



/* &cvm-callcc */
	obj_t BGl_z62cvmzd2callcczb0zzbackend_cvmz00(obj_t BgL_envz00_1193,
		obj_t BgL_oz00_1194)
	{
		{	/* BackEnd/cvm.sch 242 */
			return
				BBOOL(BGl_cvmzd2callcczd2zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1194)));
		}

	}



/* cvm-callcc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2callcczd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_53,
		bool_t BgL_vz00_54)
	{
		{	/* BackEnd/cvm.sch 243 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_53)))->BgL_callccz00) =
				((bool_t) BgL_vz00_54), BUNSPEC);
		}

	}



/* &cvm-callcc-set! */
	obj_t BGl_z62cvmzd2callcczd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1195,
		obj_t BgL_oz00_1196, obj_t BgL_vz00_1197)
	{
		{	/* BackEnd/cvm.sch 243 */
			return
				BGl_cvmzd2callcczd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1196), CBOOL(BgL_vz00_1197));
		}

	}



/* cvm-heap-compatible */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2heapzd2compatiblez00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_55)
	{
		{	/* BackEnd/cvm.sch 244 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_55)))->BgL_heapzd2compatiblezd2);
		}

	}



/* &cvm-heap-compatible */
	obj_t BGl_z62cvmzd2heapzd2compatiblez62zzbackend_cvmz00(obj_t BgL_envz00_1198,
		obj_t BgL_oz00_1199)
	{
		{	/* BackEnd/cvm.sch 244 */
			return
				BGl_cvmzd2heapzd2compatiblez00zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1199));
		}

	}



/* cvm-heap-compatible-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2heapzd2compatiblezd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_56, obj_t BgL_vz00_57)
	{
		{	/* BackEnd/cvm.sch 245 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_56)))->BgL_heapzd2compatiblezd2) =
				((obj_t) BgL_vz00_57), BUNSPEC);
		}

	}



/* &cvm-heap-compatible-set! */
	obj_t BGl_z62cvmzd2heapzd2compatiblezd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1200, obj_t BgL_oz00_1201, obj_t BgL_vz00_1202)
	{
		{	/* BackEnd/cvm.sch 245 */
			return
				BGl_cvmzd2heapzd2compatiblezd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1201), BgL_vz00_1202);
		}

	}



/* cvm-heap-suffix */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2heapzd2suffixz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_58)
	{
		{	/* BackEnd/cvm.sch 246 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_58)))->BgL_heapzd2suffixzd2);
		}

	}



/* &cvm-heap-suffix */
	obj_t BGl_z62cvmzd2heapzd2suffixz62zzbackend_cvmz00(obj_t BgL_envz00_1203,
		obj_t BgL_oz00_1204)
	{
		{	/* BackEnd/cvm.sch 246 */
			return
				BGl_cvmzd2heapzd2suffixz00zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1204));
		}

	}



/* cvm-heap-suffix-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2heapzd2suffixzd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* BackEnd/cvm.sch 247 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_59)))->BgL_heapzd2suffixzd2) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &cvm-heap-suffix-set! */
	obj_t BGl_z62cvmzd2heapzd2suffixzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1205, obj_t BgL_oz00_1206, obj_t BgL_vz00_1207)
	{
		{	/* BackEnd/cvm.sch 247 */
			return
				BGl_cvmzd2heapzd2suffixzd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1206), BgL_vz00_1207);
		}

	}



/* cvm-typed */
	BGL_EXPORTED_DEF bool_t BGl_cvmzd2typedzd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_61)
	{
		{	/* BackEnd/cvm.sch 248 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_61)))->BgL_typedz00);
		}

	}



/* &cvm-typed */
	obj_t BGl_z62cvmzd2typedzb0zzbackend_cvmz00(obj_t BgL_envz00_1208,
		obj_t BgL_oz00_1209)
	{
		{	/* BackEnd/cvm.sch 248 */
			return
				BBOOL(BGl_cvmzd2typedzd2zzbackend_cvmz00(
					((BgL_cvmz00_bglt) BgL_oz00_1209)));
		}

	}



/* cvm-typed-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2typedzd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_62,
		bool_t BgL_vz00_63)
	{
		{	/* BackEnd/cvm.sch 249 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_62)))->BgL_typedz00) =
				((bool_t) BgL_vz00_63), BUNSPEC);
		}

	}



/* &cvm-typed-set! */
	obj_t BGl_z62cvmzd2typedzd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1210,
		obj_t BgL_oz00_1211, obj_t BgL_vz00_1212)
	{
		{	/* BackEnd/cvm.sch 249 */
			return
				BGl_cvmzd2typedzd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1211), CBOOL(BgL_vz00_1212));
		}

	}



/* cvm-types */
	BGL_EXPORTED_DEF obj_t BGl_cvmzd2typeszd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_64)
	{
		{	/* BackEnd/cvm.sch 250 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_64)))->BgL_typesz00);
		}

	}



/* &cvm-types */
	obj_t BGl_z62cvmzd2typeszb0zzbackend_cvmz00(obj_t BgL_envz00_1213,
		obj_t BgL_oz00_1214)
	{
		{	/* BackEnd/cvm.sch 250 */
			return
				BGl_cvmzd2typeszd2zzbackend_cvmz00(((BgL_cvmz00_bglt) BgL_oz00_1214));
		}

	}



/* cvm-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2typeszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_65,
		obj_t BgL_vz00_66)
	{
		{	/* BackEnd/cvm.sch 251 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_65)))->BgL_typesz00) =
				((obj_t) BgL_vz00_66), BUNSPEC);
		}

	}



/* &cvm-types-set! */
	obj_t BGl_z62cvmzd2typeszd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1215,
		obj_t BgL_oz00_1216, obj_t BgL_vz00_1217)
	{
		{	/* BackEnd/cvm.sch 251 */
			return
				BGl_cvmzd2typeszd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1216), BgL_vz00_1217);
		}

	}



/* cvm-functions */
	BGL_EXPORTED_DEF obj_t BGl_cvmzd2functionszd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_67)
	{
		{	/* BackEnd/cvm.sch 252 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_67)))->BgL_functionsz00);
		}

	}



/* &cvm-functions */
	obj_t BGl_z62cvmzd2functionszb0zzbackend_cvmz00(obj_t BgL_envz00_1218,
		obj_t BgL_oz00_1219)
	{
		{	/* BackEnd/cvm.sch 252 */
			return
				BGl_cvmzd2functionszd2zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1219));
		}

	}



/* cvm-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2functionszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_68,
		obj_t BgL_vz00_69)
	{
		{	/* BackEnd/cvm.sch 253 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_68)))->BgL_functionsz00) =
				((obj_t) BgL_vz00_69), BUNSPEC);
		}

	}



/* &cvm-functions-set! */
	obj_t BGl_z62cvmzd2functionszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1220, obj_t BgL_oz00_1221, obj_t BgL_vz00_1222)
	{
		{	/* BackEnd/cvm.sch 253 */
			return
				BGl_cvmzd2functionszd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1221), BgL_vz00_1222);
		}

	}



/* cvm-variables */
	BGL_EXPORTED_DEF obj_t BGl_cvmzd2variableszd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_70)
	{
		{	/* BackEnd/cvm.sch 254 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_70)))->BgL_variablesz00);
		}

	}



/* &cvm-variables */
	obj_t BGl_z62cvmzd2variableszb0zzbackend_cvmz00(obj_t BgL_envz00_1223,
		obj_t BgL_oz00_1224)
	{
		{	/* BackEnd/cvm.sch 254 */
			return
				BGl_cvmzd2variableszd2zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1224));
		}

	}



/* cvm-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2variableszd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_71,
		obj_t BgL_vz00_72)
	{
		{	/* BackEnd/cvm.sch 255 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_71)))->BgL_variablesz00) =
				((obj_t) BgL_vz00_72), BUNSPEC);
		}

	}



/* &cvm-variables-set! */
	obj_t BGl_z62cvmzd2variableszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1225, obj_t BgL_oz00_1226, obj_t BgL_vz00_1227)
	{
		{	/* BackEnd/cvm.sch 255 */
			return
				BGl_cvmzd2variableszd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1226), BgL_vz00_1227);
		}

	}



/* cvm-extern-types */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2externzd2typesz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_73)
	{
		{	/* BackEnd/cvm.sch 256 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_73)))->BgL_externzd2typeszd2);
		}

	}



/* &cvm-extern-types */
	obj_t BGl_z62cvmzd2externzd2typesz62zzbackend_cvmz00(obj_t BgL_envz00_1228,
		obj_t BgL_oz00_1229)
	{
		{	/* BackEnd/cvm.sch 256 */
			return
				BGl_cvmzd2externzd2typesz00zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1229));
		}

	}



/* cvm-extern-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2externzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_74, obj_t BgL_vz00_75)
	{
		{	/* BackEnd/cvm.sch 257 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_74)))->BgL_externzd2typeszd2) =
				((obj_t) BgL_vz00_75), BUNSPEC);
		}

	}



/* &cvm-extern-types-set! */
	obj_t BGl_z62cvmzd2externzd2typeszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1230, obj_t BgL_oz00_1231, obj_t BgL_vz00_1232)
	{
		{	/* BackEnd/cvm.sch 257 */
			return
				BGl_cvmzd2externzd2typeszd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1231), BgL_vz00_1232);
		}

	}



/* cvm-extern-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2externzd2functionsz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_76)
	{
		{	/* BackEnd/cvm.sch 258 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_76)))->BgL_externzd2functionszd2);
		}

	}



/* &cvm-extern-functions */
	obj_t BGl_z62cvmzd2externzd2functionsz62zzbackend_cvmz00(obj_t
		BgL_envz00_1233, obj_t BgL_oz00_1234)
	{
		{	/* BackEnd/cvm.sch 258 */
			return
				BGl_cvmzd2externzd2functionsz00zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1234));
		}

	}



/* cvm-extern-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2externzd2functionszd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_77, obj_t BgL_vz00_78)
	{
		{	/* BackEnd/cvm.sch 259 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_77)))->
					BgL_externzd2functionszd2) = ((obj_t) BgL_vz00_78), BUNSPEC);
		}

	}



/* &cvm-extern-functions-set! */
	obj_t BGl_z62cvmzd2externzd2functionszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1235, obj_t BgL_oz00_1236, obj_t BgL_vz00_1237)
	{
		{	/* BackEnd/cvm.sch 259 */
			return
				BGl_cvmzd2externzd2functionszd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1236), BgL_vz00_1237);
		}

	}



/* cvm-extern-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2externzd2variablesz00zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_79)
	{
		{	/* BackEnd/cvm.sch 260 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_79)))->BgL_externzd2variableszd2);
		}

	}



/* &cvm-extern-variables */
	obj_t BGl_z62cvmzd2externzd2variablesz62zzbackend_cvmz00(obj_t
		BgL_envz00_1238, obj_t BgL_oz00_1239)
	{
		{	/* BackEnd/cvm.sch 260 */
			return
				BGl_cvmzd2externzd2variablesz00zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1239));
		}

	}



/* cvm-extern-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2externzd2variableszd2setz12zc0zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_80, obj_t BgL_vz00_81)
	{
		{	/* BackEnd/cvm.sch 261 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_80)))->
					BgL_externzd2variableszd2) = ((obj_t) BgL_vz00_81), BUNSPEC);
		}

	}



/* &cvm-extern-variables-set! */
	obj_t BGl_z62cvmzd2externzd2variableszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1240, obj_t BgL_oz00_1241, obj_t BgL_vz00_1242)
	{
		{	/* BackEnd/cvm.sch 261 */
			return
				BGl_cvmzd2externzd2variableszd2setz12zc0zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1241), BgL_vz00_1242);
		}

	}



/* cvm-name */
	BGL_EXPORTED_DEF obj_t BGl_cvmzd2namezd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_82)
	{
		{	/* BackEnd/cvm.sch 262 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_82)))->BgL_namez00);
		}

	}



/* &cvm-name */
	obj_t BGl_z62cvmzd2namezb0zzbackend_cvmz00(obj_t BgL_envz00_1243,
		obj_t BgL_oz00_1244)
	{
		{	/* BackEnd/cvm.sch 262 */
			return
				BGl_cvmzd2namezd2zzbackend_cvmz00(((BgL_cvmz00_bglt) BgL_oz00_1244));
		}

	}



/* cvm-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2namezd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_83,
		obj_t BgL_vz00_84)
	{
		{	/* BackEnd/cvm.sch 263 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_83)))->BgL_namez00) =
				((obj_t) BgL_vz00_84), BUNSPEC);
		}

	}



/* &cvm-name-set! */
	obj_t BGl_z62cvmzd2namezd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1245,
		obj_t BgL_oz00_1246, obj_t BgL_vz00_1247)
	{
		{	/* BackEnd/cvm.sch 263 */
			return
				BGl_cvmzd2namezd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1246), BgL_vz00_1247);
		}

	}



/* cvm-srfi0 */
	BGL_EXPORTED_DEF obj_t BGl_cvmzd2srfi0zd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_85)
	{
		{	/* BackEnd/cvm.sch 264 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_85)))->BgL_srfi0z00);
		}

	}



/* &cvm-srfi0 */
	obj_t BGl_z62cvmzd2srfi0zb0zzbackend_cvmz00(obj_t BgL_envz00_1248,
		obj_t BgL_oz00_1249)
	{
		{	/* BackEnd/cvm.sch 264 */
			return
				BGl_cvmzd2srfi0zd2zzbackend_cvmz00(((BgL_cvmz00_bglt) BgL_oz00_1249));
		}

	}



/* cvm-srfi0-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2srfi0zd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_86,
		obj_t BgL_vz00_87)
	{
		{	/* BackEnd/cvm.sch 265 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_86)))->BgL_srfi0z00) =
				((obj_t) BgL_vz00_87), BUNSPEC);
		}

	}



/* &cvm-srfi0-set! */
	obj_t BGl_z62cvmzd2srfi0zd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1250,
		obj_t BgL_oz00_1251, obj_t BgL_vz00_1252)
	{
		{	/* BackEnd/cvm.sch 265 */
			return
				BGl_cvmzd2srfi0zd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1251), BgL_vz00_1252);
		}

	}



/* cvm-language */
	BGL_EXPORTED_DEF obj_t BGl_cvmzd2languagezd2zzbackend_cvmz00(BgL_cvmz00_bglt
		BgL_oz00_88)
	{
		{	/* BackEnd/cvm.sch 266 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_88)))->BgL_languagez00);
		}

	}



/* &cvm-language */
	obj_t BGl_z62cvmzd2languagezb0zzbackend_cvmz00(obj_t BgL_envz00_1253,
		obj_t BgL_oz00_1254)
	{
		{	/* BackEnd/cvm.sch 266 */
			return
				BGl_cvmzd2languagezd2zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1254));
		}

	}



/* cvm-language-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvmzd2languagezd2setz12z12zzbackend_cvmz00(BgL_cvmz00_bglt BgL_oz00_89,
		obj_t BgL_vz00_90)
	{
		{	/* BackEnd/cvm.sch 267 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_89)))->BgL_languagez00) =
				((obj_t) BgL_vz00_90), BUNSPEC);
		}

	}



/* &cvm-language-set! */
	obj_t BGl_z62cvmzd2languagezd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1255,
		obj_t BgL_oz00_1256, obj_t BgL_vz00_1257)
	{
		{	/* BackEnd/cvm.sch 267 */
			return
				BGl_cvmzd2languagezd2setz12z12zzbackend_cvmz00(
				((BgL_cvmz00_bglt) BgL_oz00_1256), BgL_vz00_1257);
		}

	}



/* make-sawc */
	BGL_EXPORTED_DEF BgL_sawcz00_bglt BGl_makezd2sawczd2zzbackend_cvmz00(obj_t
		BgL_language1194z00_91, obj_t BgL_srfi01195z00_92, obj_t BgL_name1196z00_93,
		obj_t BgL_externzd2variables1197zd2_94,
		obj_t BgL_externzd2functions1198zd2_95, obj_t BgL_externzd2types1199zd2_96,
		obj_t BgL_variables1200z00_97, obj_t BgL_functions1201z00_98,
		obj_t BgL_types1202z00_99, bool_t BgL_typed1203z00_100,
		obj_t BgL_heapzd2suffix1204zd2_101, obj_t BgL_heapzd2compatible1205zd2_102,
		bool_t BgL_callcc1206z00_103, bool_t BgL_qualifiedzd2types1207zd2_104,
		bool_t BgL_effectzb21208zb2_105, bool_t BgL_removezd2emptyzd2let1209z00_106,
		bool_t BgL_foreignzd2closure1210zd2_107, bool_t BgL_typedzd2eq1211zd2_108,
		bool_t BgL_tracezd2support1212zd2_109,
		obj_t BgL_foreignzd2clausezd2suppo1213z00_110,
		obj_t BgL_debugzd2support1214zd2_111,
		bool_t BgL_pragmazd2support1215zd2_112,
		bool_t BgL_tvectorzd2descrzd2suppor1216z00_113,
		bool_t BgL_requirezd2tailc1217zd2_114, obj_t BgL_registers1218z00_115,
		obj_t BgL_pregisters1219z00_116, bool_t BgL_boundzd2check1220zd2_117,
		bool_t BgL_typezd2check1221zd2_118, bool_t BgL_typedzd2funcall1222zd2_119)
	{
		{	/* BackEnd/cvm.sch 270 */
			{	/* BackEnd/cvm.sch 270 */
				BgL_sawcz00_bglt BgL_new1183z00_1725;

				{	/* BackEnd/cvm.sch 270 */
					BgL_sawcz00_bglt BgL_new1182z00_1726;

					BgL_new1182z00_1726 =
						((BgL_sawcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sawcz00_bgl))));
					{	/* BackEnd/cvm.sch 270 */
						long BgL_arg1320z00_1727;

						BgL_arg1320z00_1727 = BGL_CLASS_NUM(BGl_sawcz00zzbackend_cvmz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1182z00_1726), BgL_arg1320z00_1727);
					}
					BgL_new1183z00_1725 = BgL_new1182z00_1726;
				}
				((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_new1183z00_1725)))->
						BgL_languagez00) = ((obj_t) BgL_language1194z00_91), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_srfi0z00) =
					((obj_t) BgL_srfi01195z00_92), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_namez00) =
					((obj_t) BgL_name1196z00_93), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_externzd2variableszd2) =
					((obj_t) BgL_externzd2variables1197zd2_94), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_externzd2functionszd2) =
					((obj_t) BgL_externzd2functions1198zd2_95), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_externzd2typeszd2) =
					((obj_t) BgL_externzd2types1199zd2_96), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_variablesz00) =
					((obj_t) BgL_variables1200z00_97), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_functionsz00) =
					((obj_t) BgL_functions1201z00_98), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_typesz00) =
					((obj_t) BgL_types1202z00_99), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_typedz00) =
					((bool_t) BgL_typed1203z00_100), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_heapzd2suffixzd2) =
					((obj_t) BgL_heapzd2suffix1204zd2_101), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_heapzd2compatiblezd2) =
					((obj_t) BgL_heapzd2compatible1205zd2_102), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_callccz00) =
					((bool_t) BgL_callcc1206z00_103), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_qualifiedzd2typeszd2) =
					((bool_t) BgL_qualifiedzd2types1207zd2_104), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_effectzb2zb2) =
					((bool_t) BgL_effectzb21208zb2_105), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_removezd2emptyzd2letz00) =
					((bool_t) BgL_removezd2emptyzd2let1209z00_106), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_foreignzd2closurezd2) =
					((bool_t) BgL_foreignzd2closure1210zd2_107), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_typedzd2eqzd2) =
					((bool_t) BgL_typedzd2eq1211zd2_108), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_tracezd2supportzd2) =
					((bool_t) BgL_tracezd2support1212zd2_109), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_foreignzd2clausezd2supportz00) =
					((obj_t) BgL_foreignzd2clausezd2suppo1213z00_110), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_debugzd2supportzd2) =
					((obj_t) BgL_debugzd2support1214zd2_111), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_pragmazd2supportzd2) =
					((bool_t) BgL_pragmazd2support1215zd2_112), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) BgL_tvectorzd2descrzd2suppor1216z00_113), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_requirezd2tailczd2) =
					((bool_t) BgL_requirezd2tailc1217zd2_114), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_registersz00) =
					((obj_t) BgL_registers1218z00_115), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_pregistersz00) =
					((obj_t) BgL_pregisters1219z00_116), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_boundzd2checkzd2) =
					((bool_t) BgL_boundzd2check1220zd2_117), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_typezd2checkzd2) =
					((bool_t) BgL_typezd2check1221zd2_118), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_typedzd2funcallzd2) =
					((bool_t) BgL_typedzd2funcall1222zd2_119), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_strictzd2typezd2castz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1183z00_1725)))->BgL_stringzd2literalzd2supportz00) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				{	/* BackEnd/cvm.sch 270 */
					obj_t BgL_fun1319z00_1728;

					BgL_fun1319z00_1728 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_sawcz00zzbackend_cvmz00);
					BGL_PROCEDURE_CALL1(BgL_fun1319z00_1728,
						((obj_t) BgL_new1183z00_1725));
				}
				return BgL_new1183z00_1725;
			}
		}

	}



/* &make-sawc */
	BgL_sawcz00_bglt BGl_z62makezd2sawczb0zzbackend_cvmz00(obj_t BgL_envz00_1258,
		obj_t BgL_language1194z00_1259, obj_t BgL_srfi01195z00_1260,
		obj_t BgL_name1196z00_1261, obj_t BgL_externzd2variables1197zd2_1262,
		obj_t BgL_externzd2functions1198zd2_1263,
		obj_t BgL_externzd2types1199zd2_1264, obj_t BgL_variables1200z00_1265,
		obj_t BgL_functions1201z00_1266, obj_t BgL_types1202z00_1267,
		obj_t BgL_typed1203z00_1268, obj_t BgL_heapzd2suffix1204zd2_1269,
		obj_t BgL_heapzd2compatible1205zd2_1270, obj_t BgL_callcc1206z00_1271,
		obj_t BgL_qualifiedzd2types1207zd2_1272, obj_t BgL_effectzb21208zb2_1273,
		obj_t BgL_removezd2emptyzd2let1209z00_1274,
		obj_t BgL_foreignzd2closure1210zd2_1275, obj_t BgL_typedzd2eq1211zd2_1276,
		obj_t BgL_tracezd2support1212zd2_1277,
		obj_t BgL_foreignzd2clausezd2suppo1213z00_1278,
		obj_t BgL_debugzd2support1214zd2_1279,
		obj_t BgL_pragmazd2support1215zd2_1280,
		obj_t BgL_tvectorzd2descrzd2suppor1216z00_1281,
		obj_t BgL_requirezd2tailc1217zd2_1282, obj_t BgL_registers1218z00_1283,
		obj_t BgL_pregisters1219z00_1284, obj_t BgL_boundzd2check1220zd2_1285,
		obj_t BgL_typezd2check1221zd2_1286, obj_t BgL_typedzd2funcall1222zd2_1287)
	{
		{	/* BackEnd/cvm.sch 270 */
			return
				BGl_makezd2sawczd2zzbackend_cvmz00(BgL_language1194z00_1259,
				BgL_srfi01195z00_1260, BgL_name1196z00_1261,
				BgL_externzd2variables1197zd2_1262, BgL_externzd2functions1198zd2_1263,
				BgL_externzd2types1199zd2_1264, BgL_variables1200z00_1265,
				BgL_functions1201z00_1266, BgL_types1202z00_1267,
				CBOOL(BgL_typed1203z00_1268), BgL_heapzd2suffix1204zd2_1269,
				BgL_heapzd2compatible1205zd2_1270, CBOOL(BgL_callcc1206z00_1271),
				CBOOL(BgL_qualifiedzd2types1207zd2_1272),
				CBOOL(BgL_effectzb21208zb2_1273),
				CBOOL(BgL_removezd2emptyzd2let1209z00_1274),
				CBOOL(BgL_foreignzd2closure1210zd2_1275),
				CBOOL(BgL_typedzd2eq1211zd2_1276),
				CBOOL(BgL_tracezd2support1212zd2_1277),
				BgL_foreignzd2clausezd2suppo1213z00_1278,
				BgL_debugzd2support1214zd2_1279,
				CBOOL(BgL_pragmazd2support1215zd2_1280),
				CBOOL(BgL_tvectorzd2descrzd2suppor1216z00_1281),
				CBOOL(BgL_requirezd2tailc1217zd2_1282), BgL_registers1218z00_1283,
				BgL_pregisters1219z00_1284, CBOOL(BgL_boundzd2check1220zd2_1285),
				CBOOL(BgL_typezd2check1221zd2_1286),
				CBOOL(BgL_typedzd2funcall1222zd2_1287));
		}

	}



/* sawc? */
	BGL_EXPORTED_DEF bool_t BGl_sawczf3zf3zzbackend_cvmz00(obj_t BgL_objz00_120)
	{
		{	/* BackEnd/cvm.sch 271 */
			{	/* BackEnd/cvm.sch 271 */
				obj_t BgL_classz00_1729;

				BgL_classz00_1729 = BGl_sawcz00zzbackend_cvmz00;
				if (BGL_OBJECTP(BgL_objz00_120))
					{	/* BackEnd/cvm.sch 271 */
						BgL_objectz00_bglt BgL_arg1807z00_1730;

						BgL_arg1807z00_1730 = (BgL_objectz00_bglt) (BgL_objz00_120);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* BackEnd/cvm.sch 271 */
								long BgL_idxz00_1731;

								BgL_idxz00_1731 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1730);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_1731 + 3L)) == BgL_classz00_1729);
							}
						else
							{	/* BackEnd/cvm.sch 271 */
								bool_t BgL_res1372z00_1734;

								{	/* BackEnd/cvm.sch 271 */
									obj_t BgL_oclassz00_1735;

									{	/* BackEnd/cvm.sch 271 */
										obj_t BgL_arg1815z00_1736;
										long BgL_arg1816z00_1737;

										BgL_arg1815z00_1736 = (BGl_za2classesza2z00zz__objectz00);
										{	/* BackEnd/cvm.sch 271 */
											long BgL_arg1817z00_1738;

											BgL_arg1817z00_1738 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1730);
											BgL_arg1816z00_1737 = (BgL_arg1817z00_1738 - OBJECT_TYPE);
										}
										BgL_oclassz00_1735 =
											VECTOR_REF(BgL_arg1815z00_1736, BgL_arg1816z00_1737);
									}
									{	/* BackEnd/cvm.sch 271 */
										bool_t BgL__ortest_1115z00_1739;

										BgL__ortest_1115z00_1739 =
											(BgL_classz00_1729 == BgL_oclassz00_1735);
										if (BgL__ortest_1115z00_1739)
											{	/* BackEnd/cvm.sch 271 */
												BgL_res1372z00_1734 = BgL__ortest_1115z00_1739;
											}
										else
											{	/* BackEnd/cvm.sch 271 */
												long BgL_odepthz00_1740;

												{	/* BackEnd/cvm.sch 271 */
													obj_t BgL_arg1804z00_1741;

													BgL_arg1804z00_1741 = (BgL_oclassz00_1735);
													BgL_odepthz00_1740 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_1741);
												}
												if ((3L < BgL_odepthz00_1740))
													{	/* BackEnd/cvm.sch 271 */
														obj_t BgL_arg1802z00_1742;

														{	/* BackEnd/cvm.sch 271 */
															obj_t BgL_arg1803z00_1743;

															BgL_arg1803z00_1743 = (BgL_oclassz00_1735);
															BgL_arg1802z00_1742 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1743,
																3L);
														}
														BgL_res1372z00_1734 =
															(BgL_arg1802z00_1742 == BgL_classz00_1729);
													}
												else
													{	/* BackEnd/cvm.sch 271 */
														BgL_res1372z00_1734 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1372z00_1734;
							}
					}
				else
					{	/* BackEnd/cvm.sch 271 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sawc? */
	obj_t BGl_z62sawczf3z91zzbackend_cvmz00(obj_t BgL_envz00_1288,
		obj_t BgL_objz00_1289)
	{
		{	/* BackEnd/cvm.sch 271 */
			return BBOOL(BGl_sawczf3zf3zzbackend_cvmz00(BgL_objz00_1289));
		}

	}



/* sawc-nil */
	BGL_EXPORTED_DEF BgL_sawcz00_bglt BGl_sawczd2nilzd2zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.sch 272 */
			{	/* BackEnd/cvm.sch 272 */
				obj_t BgL_classz00_926;

				BgL_classz00_926 = BGl_sawcz00zzbackend_cvmz00;
				{	/* BackEnd/cvm.sch 272 */
					obj_t BgL__ortest_1117z00_927;

					BgL__ortest_1117z00_927 = BGL_CLASS_NIL(BgL_classz00_926);
					if (CBOOL(BgL__ortest_1117z00_927))
						{	/* BackEnd/cvm.sch 272 */
							return ((BgL_sawcz00_bglt) BgL__ortest_1117z00_927);
						}
					else
						{	/* BackEnd/cvm.sch 272 */
							return
								((BgL_sawcz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_926));
						}
				}
			}
		}

	}



/* &sawc-nil */
	BgL_sawcz00_bglt BGl_z62sawczd2nilzb0zzbackend_cvmz00(obj_t BgL_envz00_1290)
	{
		{	/* BackEnd/cvm.sch 272 */
			return BGl_sawczd2nilzd2zzbackend_cvmz00();
		}

	}



/* sawc-typed-funcall */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2typedzd2funcallz00zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_121)
	{
		{	/* BackEnd/cvm.sch 273 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_121)))->BgL_typedzd2funcallzd2);
		}

	}



/* &sawc-typed-funcall */
	obj_t BGl_z62sawczd2typedzd2funcallz62zzbackend_cvmz00(obj_t BgL_envz00_1291,
		obj_t BgL_oz00_1292)
	{
		{	/* BackEnd/cvm.sch 273 */
			return
				BBOOL(BGl_sawczd2typedzd2funcallz00zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1292)));
		}

	}



/* sawc-typed-funcall-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2typedzd2funcallzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_122, bool_t BgL_vz00_123)
	{
		{	/* BackEnd/cvm.sch 274 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_122)))->BgL_typedzd2funcallzd2) =
				((bool_t) BgL_vz00_123), BUNSPEC);
		}

	}



/* &sawc-typed-funcall-set! */
	obj_t BGl_z62sawczd2typedzd2funcallzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1293, obj_t BgL_oz00_1294, obj_t BgL_vz00_1295)
	{
		{	/* BackEnd/cvm.sch 274 */
			return
				BGl_sawczd2typedzd2funcallzd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1294), CBOOL(BgL_vz00_1295));
		}

	}



/* sawc-type-check */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2typezd2checkz00zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_124)
	{
		{	/* BackEnd/cvm.sch 275 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_124)))->BgL_typezd2checkzd2);
		}

	}



/* &sawc-type-check */
	obj_t BGl_z62sawczd2typezd2checkz62zzbackend_cvmz00(obj_t BgL_envz00_1296,
		obj_t BgL_oz00_1297)
	{
		{	/* BackEnd/cvm.sch 275 */
			return
				BBOOL(BGl_sawczd2typezd2checkz00zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1297)));
		}

	}



/* sawc-type-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2typezd2checkzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_125, bool_t BgL_vz00_126)
	{
		{	/* BackEnd/cvm.sch 276 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_125)))->BgL_typezd2checkzd2) =
				((bool_t) BgL_vz00_126), BUNSPEC);
		}

	}



/* &sawc-type-check-set! */
	obj_t BGl_z62sawczd2typezd2checkzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1298, obj_t BgL_oz00_1299, obj_t BgL_vz00_1300)
	{
		{	/* BackEnd/cvm.sch 276 */
			return
				BGl_sawczd2typezd2checkzd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1299), CBOOL(BgL_vz00_1300));
		}

	}



/* sawc-bound-check */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2boundzd2checkz00zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_127)
	{
		{	/* BackEnd/cvm.sch 277 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_127)))->BgL_boundzd2checkzd2);
		}

	}



/* &sawc-bound-check */
	obj_t BGl_z62sawczd2boundzd2checkz62zzbackend_cvmz00(obj_t BgL_envz00_1301,
		obj_t BgL_oz00_1302)
	{
		{	/* BackEnd/cvm.sch 277 */
			return
				BBOOL(BGl_sawczd2boundzd2checkz00zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1302)));
		}

	}



/* sawc-bound-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2boundzd2checkzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_128, bool_t BgL_vz00_129)
	{
		{	/* BackEnd/cvm.sch 278 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_128)))->BgL_boundzd2checkzd2) =
				((bool_t) BgL_vz00_129), BUNSPEC);
		}

	}



/* &sawc-bound-check-set! */
	obj_t BGl_z62sawczd2boundzd2checkzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1303, obj_t BgL_oz00_1304, obj_t BgL_vz00_1305)
	{
		{	/* BackEnd/cvm.sch 278 */
			return
				BGl_sawczd2boundzd2checkzd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1304), CBOOL(BgL_vz00_1305));
		}

	}



/* sawc-pregisters */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2pregisterszd2zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_130)
	{
		{	/* BackEnd/cvm.sch 279 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_130)))->BgL_pregistersz00);
		}

	}



/* &sawc-pregisters */
	obj_t BGl_z62sawczd2pregisterszb0zzbackend_cvmz00(obj_t BgL_envz00_1306,
		obj_t BgL_oz00_1307)
	{
		{	/* BackEnd/cvm.sch 279 */
			return
				BGl_sawczd2pregisterszd2zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1307));
		}

	}



/* sawc-pregisters-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2pregisterszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_131, obj_t BgL_vz00_132)
	{
		{	/* BackEnd/cvm.sch 280 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_131)))->BgL_pregistersz00) =
				((obj_t) BgL_vz00_132), BUNSPEC);
		}

	}



/* &sawc-pregisters-set! */
	obj_t BGl_z62sawczd2pregisterszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1308, obj_t BgL_oz00_1309, obj_t BgL_vz00_1310)
	{
		{	/* BackEnd/cvm.sch 280 */
			return
				BGl_sawczd2pregisterszd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1309), BgL_vz00_1310);
		}

	}



/* sawc-registers */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2registerszd2zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_133)
	{
		{	/* BackEnd/cvm.sch 281 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_133)))->BgL_registersz00);
		}

	}



/* &sawc-registers */
	obj_t BGl_z62sawczd2registerszb0zzbackend_cvmz00(obj_t BgL_envz00_1311,
		obj_t BgL_oz00_1312)
	{
		{	/* BackEnd/cvm.sch 281 */
			return
				BGl_sawczd2registerszd2zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1312));
		}

	}



/* sawc-registers-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2registerszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_134, obj_t BgL_vz00_135)
	{
		{	/* BackEnd/cvm.sch 282 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_134)))->BgL_registersz00) =
				((obj_t) BgL_vz00_135), BUNSPEC);
		}

	}



/* &sawc-registers-set! */
	obj_t BGl_z62sawczd2registerszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1313, obj_t BgL_oz00_1314, obj_t BgL_vz00_1315)
	{
		{	/* BackEnd/cvm.sch 282 */
			return
				BGl_sawczd2registerszd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1314), BgL_vz00_1315);
		}

	}



/* sawc-require-tailc */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2requirezd2tailcz00zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_136)
	{
		{	/* BackEnd/cvm.sch 283 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_136)))->BgL_requirezd2tailczd2);
		}

	}



/* &sawc-require-tailc */
	obj_t BGl_z62sawczd2requirezd2tailcz62zzbackend_cvmz00(obj_t BgL_envz00_1316,
		obj_t BgL_oz00_1317)
	{
		{	/* BackEnd/cvm.sch 283 */
			return
				BBOOL(BGl_sawczd2requirezd2tailcz00zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1317)));
		}

	}



/* sawc-require-tailc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2requirezd2tailczd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_137, bool_t BgL_vz00_138)
	{
		{	/* BackEnd/cvm.sch 284 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_137)))->BgL_requirezd2tailczd2) =
				((bool_t) BgL_vz00_138), BUNSPEC);
		}

	}



/* &sawc-require-tailc-set! */
	obj_t BGl_z62sawczd2requirezd2tailczd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1318, obj_t BgL_oz00_1319, obj_t BgL_vz00_1320)
	{
		{	/* BackEnd/cvm.sch 284 */
			return
				BGl_sawczd2requirezd2tailczd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1319), CBOOL(BgL_vz00_1320));
		}

	}



/* sawc-tvector-descr-support */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2tvectorzd2descrzd2supportzd2zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_139)
	{
		{	/* BackEnd/cvm.sch 285 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_139)))->
				BgL_tvectorzd2descrzd2supportz00);
		}

	}



/* &sawc-tvector-descr-support */
	obj_t BGl_z62sawczd2tvectorzd2descrzd2supportzb0zzbackend_cvmz00(obj_t
		BgL_envz00_1321, obj_t BgL_oz00_1322)
	{
		{	/* BackEnd/cvm.sch 285 */
			return
				BBOOL(BGl_sawczd2tvectorzd2descrzd2supportzd2zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1322)));
		}

	}



/* sawc-tvector-descr-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_sawcz00_bglt BgL_oz00_140, bool_t BgL_vz00_141)
	{
		{	/* BackEnd/cvm.sch 286 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_140)))->
					BgL_tvectorzd2descrzd2supportz00) = ((bool_t) BgL_vz00_141), BUNSPEC);
		}

	}



/* &sawc-tvector-descr-support-set! */
	obj_t
		BGl_z62sawczd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1323, obj_t BgL_oz00_1324, obj_t BgL_vz00_1325)
	{
		{	/* BackEnd/cvm.sch 286 */
			return
				BGl_sawczd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1324), CBOOL(BgL_vz00_1325));
		}

	}



/* sawc-pragma-support */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2pragmazd2supportz00zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_142)
	{
		{	/* BackEnd/cvm.sch 287 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_142)))->BgL_pragmazd2supportzd2);
		}

	}



/* &sawc-pragma-support */
	obj_t BGl_z62sawczd2pragmazd2supportz62zzbackend_cvmz00(obj_t BgL_envz00_1326,
		obj_t BgL_oz00_1327)
	{
		{	/* BackEnd/cvm.sch 287 */
			return
				BBOOL(BGl_sawczd2pragmazd2supportz00zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1327)));
		}

	}



/* sawc-pragma-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2pragmazd2supportzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_143, bool_t BgL_vz00_144)
	{
		{	/* BackEnd/cvm.sch 288 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_143)))->BgL_pragmazd2supportzd2) =
				((bool_t) BgL_vz00_144), BUNSPEC);
		}

	}



/* &sawc-pragma-support-set! */
	obj_t BGl_z62sawczd2pragmazd2supportzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1328, obj_t BgL_oz00_1329, obj_t BgL_vz00_1330)
	{
		{	/* BackEnd/cvm.sch 288 */
			return
				BGl_sawczd2pragmazd2supportzd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1329), CBOOL(BgL_vz00_1330));
		}

	}



/* sawc-debug-support */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2debugzd2supportz00zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_145)
	{
		{	/* BackEnd/cvm.sch 289 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_145)))->BgL_debugzd2supportzd2);
		}

	}



/* &sawc-debug-support */
	obj_t BGl_z62sawczd2debugzd2supportz62zzbackend_cvmz00(obj_t BgL_envz00_1331,
		obj_t BgL_oz00_1332)
	{
		{	/* BackEnd/cvm.sch 289 */
			return
				BGl_sawczd2debugzd2supportz00zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1332));
		}

	}



/* sawc-debug-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2debugzd2supportzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_146, obj_t BgL_vz00_147)
	{
		{	/* BackEnd/cvm.sch 290 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_146)))->BgL_debugzd2supportzd2) =
				((obj_t) BgL_vz00_147), BUNSPEC);
		}

	}



/* &sawc-debug-support-set! */
	obj_t BGl_z62sawczd2debugzd2supportzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1333, obj_t BgL_oz00_1334, obj_t BgL_vz00_1335)
	{
		{	/* BackEnd/cvm.sch 290 */
			return
				BGl_sawczd2debugzd2supportzd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1334), BgL_vz00_1335);
		}

	}



/* sawc-foreign-clause-support */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2foreignzd2clausezd2supportzd2zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_148)
	{
		{	/* BackEnd/cvm.sch 291 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_148)))->
				BgL_foreignzd2clausezd2supportz00);
		}

	}



/* &sawc-foreign-clause-support */
	obj_t BGl_z62sawczd2foreignzd2clausezd2supportzb0zzbackend_cvmz00(obj_t
		BgL_envz00_1336, obj_t BgL_oz00_1337)
	{
		{	/* BackEnd/cvm.sch 291 */
			return
				BGl_sawczd2foreignzd2clausezd2supportzd2zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1337));
		}

	}



/* sawc-foreign-clause-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2foreignzd2clausezd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_sawcz00_bglt BgL_oz00_149, obj_t BgL_vz00_150)
	{
		{	/* BackEnd/cvm.sch 292 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_149)))->
					BgL_foreignzd2clausezd2supportz00) = ((obj_t) BgL_vz00_150), BUNSPEC);
		}

	}



/* &sawc-foreign-clause-support-set! */
	obj_t
		BGl_z62sawczd2foreignzd2clausezd2supportzd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1338, obj_t BgL_oz00_1339, obj_t BgL_vz00_1340)
	{
		{	/* BackEnd/cvm.sch 292 */
			return
				BGl_sawczd2foreignzd2clausezd2supportzd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1339), BgL_vz00_1340);
		}

	}



/* sawc-trace-support */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2tracezd2supportz00zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_151)
	{
		{	/* BackEnd/cvm.sch 293 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_151)))->BgL_tracezd2supportzd2);
		}

	}



/* &sawc-trace-support */
	obj_t BGl_z62sawczd2tracezd2supportz62zzbackend_cvmz00(obj_t BgL_envz00_1341,
		obj_t BgL_oz00_1342)
	{
		{	/* BackEnd/cvm.sch 293 */
			return
				BBOOL(BGl_sawczd2tracezd2supportz00zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1342)));
		}

	}



/* sawc-trace-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2tracezd2supportzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_152, bool_t BgL_vz00_153)
	{
		{	/* BackEnd/cvm.sch 294 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_152)))->BgL_tracezd2supportzd2) =
				((bool_t) BgL_vz00_153), BUNSPEC);
		}

	}



/* &sawc-trace-support-set! */
	obj_t BGl_z62sawczd2tracezd2supportzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1343, obj_t BgL_oz00_1344, obj_t BgL_vz00_1345)
	{
		{	/* BackEnd/cvm.sch 294 */
			return
				BGl_sawczd2tracezd2supportzd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1344), CBOOL(BgL_vz00_1345));
		}

	}



/* sawc-typed-eq */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2typedzd2eqz00zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_154)
	{
		{	/* BackEnd/cvm.sch 295 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_154)))->BgL_typedzd2eqzd2);
		}

	}



/* &sawc-typed-eq */
	obj_t BGl_z62sawczd2typedzd2eqz62zzbackend_cvmz00(obj_t BgL_envz00_1346,
		obj_t BgL_oz00_1347)
	{
		{	/* BackEnd/cvm.sch 295 */
			return
				BBOOL(BGl_sawczd2typedzd2eqz00zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1347)));
		}

	}



/* sawc-typed-eq-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2typedzd2eqzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_155, bool_t BgL_vz00_156)
	{
		{	/* BackEnd/cvm.sch 296 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_155)))->BgL_typedzd2eqzd2) =
				((bool_t) BgL_vz00_156), BUNSPEC);
		}

	}



/* &sawc-typed-eq-set! */
	obj_t BGl_z62sawczd2typedzd2eqzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1348, obj_t BgL_oz00_1349, obj_t BgL_vz00_1350)
	{
		{	/* BackEnd/cvm.sch 296 */
			return
				BGl_sawczd2typedzd2eqzd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1349), CBOOL(BgL_vz00_1350));
		}

	}



/* sawc-foreign-closure */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2foreignzd2closurez00zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_157)
	{
		{	/* BackEnd/cvm.sch 297 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_157)))->BgL_foreignzd2closurezd2);
		}

	}



/* &sawc-foreign-closure */
	obj_t BGl_z62sawczd2foreignzd2closurez62zzbackend_cvmz00(obj_t
		BgL_envz00_1351, obj_t BgL_oz00_1352)
	{
		{	/* BackEnd/cvm.sch 297 */
			return
				BBOOL(BGl_sawczd2foreignzd2closurez00zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1352)));
		}

	}



/* sawc-foreign-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2foreignzd2closurezd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_158, bool_t BgL_vz00_159)
	{
		{	/* BackEnd/cvm.sch 298 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_158)))->
					BgL_foreignzd2closurezd2) = ((bool_t) BgL_vz00_159), BUNSPEC);
		}

	}



/* &sawc-foreign-closure-set! */
	obj_t BGl_z62sawczd2foreignzd2closurezd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1353, obj_t BgL_oz00_1354, obj_t BgL_vz00_1355)
	{
		{	/* BackEnd/cvm.sch 298 */
			return
				BGl_sawczd2foreignzd2closurezd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1354), CBOOL(BgL_vz00_1355));
		}

	}



/* sawc-remove-empty-let */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2removezd2emptyzd2letzd2zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_160)
	{
		{	/* BackEnd/cvm.sch 299 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_160)))->
				BgL_removezd2emptyzd2letz00);
		}

	}



/* &sawc-remove-empty-let */
	obj_t BGl_z62sawczd2removezd2emptyzd2letzb0zzbackend_cvmz00(obj_t
		BgL_envz00_1356, obj_t BgL_oz00_1357)
	{
		{	/* BackEnd/cvm.sch 299 */
			return
				BBOOL(BGl_sawczd2removezd2emptyzd2letzd2zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1357)));
		}

	}



/* sawc-remove-empty-let-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2removezd2emptyzd2letzd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_161, bool_t BgL_vz00_162)
	{
		{	/* BackEnd/cvm.sch 300 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_161)))->
					BgL_removezd2emptyzd2letz00) = ((bool_t) BgL_vz00_162), BUNSPEC);
		}

	}



/* &sawc-remove-empty-let-set! */
	obj_t BGl_z62sawczd2removezd2emptyzd2letzd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1358, obj_t BgL_oz00_1359, obj_t BgL_vz00_1360)
	{
		{	/* BackEnd/cvm.sch 300 */
			return
				BGl_sawczd2removezd2emptyzd2letzd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1359), CBOOL(BgL_vz00_1360));
		}

	}



/* sawc-effect+ */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2effectzb2z60zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_163)
	{
		{	/* BackEnd/cvm.sch 301 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_163)))->BgL_effectzb2zb2);
		}

	}



/* &sawc-effect+ */
	obj_t BGl_z62sawczd2effectzb2z02zzbackend_cvmz00(obj_t BgL_envz00_1361,
		obj_t BgL_oz00_1362)
	{
		{	/* BackEnd/cvm.sch 301 */
			return
				BBOOL(BGl_sawczd2effectzb2z60zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1362)));
		}

	}



/* sawc-effect+-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2effectzb2zd2setz12za0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_164, bool_t BgL_vz00_165)
	{
		{	/* BackEnd/cvm.sch 302 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_164)))->BgL_effectzb2zb2) =
				((bool_t) BgL_vz00_165), BUNSPEC);
		}

	}



/* &sawc-effect+-set! */
	obj_t BGl_z62sawczd2effectzb2zd2setz12zc2zzbackend_cvmz00(obj_t
		BgL_envz00_1363, obj_t BgL_oz00_1364, obj_t BgL_vz00_1365)
	{
		{	/* BackEnd/cvm.sch 302 */
			return
				BGl_sawczd2effectzb2zd2setz12za0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1364), CBOOL(BgL_vz00_1365));
		}

	}



/* sawc-qualified-types */
	BGL_EXPORTED_DEF bool_t
		BGl_sawczd2qualifiedzd2typesz00zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_166)
	{
		{	/* BackEnd/cvm.sch 303 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_166)))->BgL_qualifiedzd2typeszd2);
		}

	}



/* &sawc-qualified-types */
	obj_t BGl_z62sawczd2qualifiedzd2typesz62zzbackend_cvmz00(obj_t
		BgL_envz00_1366, obj_t BgL_oz00_1367)
	{
		{	/* BackEnd/cvm.sch 303 */
			return
				BBOOL(BGl_sawczd2qualifiedzd2typesz00zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1367)));
		}

	}



/* sawc-qualified-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2qualifiedzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_167, bool_t BgL_vz00_168)
	{
		{	/* BackEnd/cvm.sch 304 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_167)))->
					BgL_qualifiedzd2typeszd2) = ((bool_t) BgL_vz00_168), BUNSPEC);
		}

	}



/* &sawc-qualified-types-set! */
	obj_t BGl_z62sawczd2qualifiedzd2typeszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1368, obj_t BgL_oz00_1369, obj_t BgL_vz00_1370)
	{
		{	/* BackEnd/cvm.sch 304 */
			return
				BGl_sawczd2qualifiedzd2typeszd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1369), CBOOL(BgL_vz00_1370));
		}

	}



/* sawc-callcc */
	BGL_EXPORTED_DEF bool_t BGl_sawczd2callcczd2zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_169)
	{
		{	/* BackEnd/cvm.sch 305 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_169)))->BgL_callccz00);
		}

	}



/* &sawc-callcc */
	obj_t BGl_z62sawczd2callcczb0zzbackend_cvmz00(obj_t BgL_envz00_1371,
		obj_t BgL_oz00_1372)
	{
		{	/* BackEnd/cvm.sch 305 */
			return
				BBOOL(BGl_sawczd2callcczd2zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1372)));
		}

	}



/* sawc-callcc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2callcczd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_170,
		bool_t BgL_vz00_171)
	{
		{	/* BackEnd/cvm.sch 306 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_170)))->BgL_callccz00) =
				((bool_t) BgL_vz00_171), BUNSPEC);
		}

	}



/* &sawc-callcc-set! */
	obj_t BGl_z62sawczd2callcczd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1373,
		obj_t BgL_oz00_1374, obj_t BgL_vz00_1375)
	{
		{	/* BackEnd/cvm.sch 306 */
			return
				BGl_sawczd2callcczd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1374), CBOOL(BgL_vz00_1375));
		}

	}



/* sawc-heap-compatible */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2heapzd2compatiblez00zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_172)
	{
		{	/* BackEnd/cvm.sch 307 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_172)))->BgL_heapzd2compatiblezd2);
		}

	}



/* &sawc-heap-compatible */
	obj_t BGl_z62sawczd2heapzd2compatiblez62zzbackend_cvmz00(obj_t
		BgL_envz00_1376, obj_t BgL_oz00_1377)
	{
		{	/* BackEnd/cvm.sch 307 */
			return
				BGl_sawczd2heapzd2compatiblez00zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1377));
		}

	}



/* sawc-heap-compatible-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2heapzd2compatiblezd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_173, obj_t BgL_vz00_174)
	{
		{	/* BackEnd/cvm.sch 308 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_173)))->
					BgL_heapzd2compatiblezd2) = ((obj_t) BgL_vz00_174), BUNSPEC);
		}

	}



/* &sawc-heap-compatible-set! */
	obj_t BGl_z62sawczd2heapzd2compatiblezd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1378, obj_t BgL_oz00_1379, obj_t BgL_vz00_1380)
	{
		{	/* BackEnd/cvm.sch 308 */
			return
				BGl_sawczd2heapzd2compatiblezd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1379), BgL_vz00_1380);
		}

	}



/* sawc-heap-suffix */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2heapzd2suffixz00zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_175)
	{
		{	/* BackEnd/cvm.sch 309 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_175)))->BgL_heapzd2suffixzd2);
		}

	}



/* &sawc-heap-suffix */
	obj_t BGl_z62sawczd2heapzd2suffixz62zzbackend_cvmz00(obj_t BgL_envz00_1381,
		obj_t BgL_oz00_1382)
	{
		{	/* BackEnd/cvm.sch 309 */
			return
				BGl_sawczd2heapzd2suffixz00zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1382));
		}

	}



/* sawc-heap-suffix-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2heapzd2suffixzd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_176, obj_t BgL_vz00_177)
	{
		{	/* BackEnd/cvm.sch 310 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_176)))->BgL_heapzd2suffixzd2) =
				((obj_t) BgL_vz00_177), BUNSPEC);
		}

	}



/* &sawc-heap-suffix-set! */
	obj_t BGl_z62sawczd2heapzd2suffixzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1383, obj_t BgL_oz00_1384, obj_t BgL_vz00_1385)
	{
		{	/* BackEnd/cvm.sch 310 */
			return
				BGl_sawczd2heapzd2suffixzd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1384), BgL_vz00_1385);
		}

	}



/* sawc-typed */
	BGL_EXPORTED_DEF bool_t BGl_sawczd2typedzd2zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_178)
	{
		{	/* BackEnd/cvm.sch 311 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_178)))->BgL_typedz00);
		}

	}



/* &sawc-typed */
	obj_t BGl_z62sawczd2typedzb0zzbackend_cvmz00(obj_t BgL_envz00_1386,
		obj_t BgL_oz00_1387)
	{
		{	/* BackEnd/cvm.sch 311 */
			return
				BBOOL(BGl_sawczd2typedzd2zzbackend_cvmz00(
					((BgL_sawcz00_bglt) BgL_oz00_1387)));
		}

	}



/* sawc-typed-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2typedzd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_179,
		bool_t BgL_vz00_180)
	{
		{	/* BackEnd/cvm.sch 312 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_179)))->BgL_typedz00) =
				((bool_t) BgL_vz00_180), BUNSPEC);
		}

	}



/* &sawc-typed-set! */
	obj_t BGl_z62sawczd2typedzd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1388,
		obj_t BgL_oz00_1389, obj_t BgL_vz00_1390)
	{
		{	/* BackEnd/cvm.sch 312 */
			return
				BGl_sawczd2typedzd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1389), CBOOL(BgL_vz00_1390));
		}

	}



/* sawc-types */
	BGL_EXPORTED_DEF obj_t BGl_sawczd2typeszd2zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_181)
	{
		{	/* BackEnd/cvm.sch 313 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_181)))->BgL_typesz00);
		}

	}



/* &sawc-types */
	obj_t BGl_z62sawczd2typeszb0zzbackend_cvmz00(obj_t BgL_envz00_1391,
		obj_t BgL_oz00_1392)
	{
		{	/* BackEnd/cvm.sch 313 */
			return
				BGl_sawczd2typeszd2zzbackend_cvmz00(((BgL_sawcz00_bglt) BgL_oz00_1392));
		}

	}



/* sawc-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2typeszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_182,
		obj_t BgL_vz00_183)
	{
		{	/* BackEnd/cvm.sch 314 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_182)))->BgL_typesz00) =
				((obj_t) BgL_vz00_183), BUNSPEC);
		}

	}



/* &sawc-types-set! */
	obj_t BGl_z62sawczd2typeszd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1393,
		obj_t BgL_oz00_1394, obj_t BgL_vz00_1395)
	{
		{	/* BackEnd/cvm.sch 314 */
			return
				BGl_sawczd2typeszd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1394), BgL_vz00_1395);
		}

	}



/* sawc-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2functionszd2zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_184)
	{
		{	/* BackEnd/cvm.sch 315 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_184)))->BgL_functionsz00);
		}

	}



/* &sawc-functions */
	obj_t BGl_z62sawczd2functionszb0zzbackend_cvmz00(obj_t BgL_envz00_1396,
		obj_t BgL_oz00_1397)
	{
		{	/* BackEnd/cvm.sch 315 */
			return
				BGl_sawczd2functionszd2zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1397));
		}

	}



/* sawc-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2functionszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_185, obj_t BgL_vz00_186)
	{
		{	/* BackEnd/cvm.sch 316 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_185)))->BgL_functionsz00) =
				((obj_t) BgL_vz00_186), BUNSPEC);
		}

	}



/* &sawc-functions-set! */
	obj_t BGl_z62sawczd2functionszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1398, obj_t BgL_oz00_1399, obj_t BgL_vz00_1400)
	{
		{	/* BackEnd/cvm.sch 316 */
			return
				BGl_sawczd2functionszd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1399), BgL_vz00_1400);
		}

	}



/* sawc-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2variableszd2zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_187)
	{
		{	/* BackEnd/cvm.sch 317 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_187)))->BgL_variablesz00);
		}

	}



/* &sawc-variables */
	obj_t BGl_z62sawczd2variableszb0zzbackend_cvmz00(obj_t BgL_envz00_1401,
		obj_t BgL_oz00_1402)
	{
		{	/* BackEnd/cvm.sch 317 */
			return
				BGl_sawczd2variableszd2zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1402));
		}

	}



/* sawc-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2variableszd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_188, obj_t BgL_vz00_189)
	{
		{	/* BackEnd/cvm.sch 318 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_188)))->BgL_variablesz00) =
				((obj_t) BgL_vz00_189), BUNSPEC);
		}

	}



/* &sawc-variables-set! */
	obj_t BGl_z62sawczd2variableszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1403, obj_t BgL_oz00_1404, obj_t BgL_vz00_1405)
	{
		{	/* BackEnd/cvm.sch 318 */
			return
				BGl_sawczd2variableszd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1404), BgL_vz00_1405);
		}

	}



/* sawc-extern-types */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2externzd2typesz00zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_190)
	{
		{	/* BackEnd/cvm.sch 319 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_190)))->BgL_externzd2typeszd2);
		}

	}



/* &sawc-extern-types */
	obj_t BGl_z62sawczd2externzd2typesz62zzbackend_cvmz00(obj_t BgL_envz00_1406,
		obj_t BgL_oz00_1407)
	{
		{	/* BackEnd/cvm.sch 319 */
			return
				BGl_sawczd2externzd2typesz00zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1407));
		}

	}



/* sawc-extern-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2externzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_191, obj_t BgL_vz00_192)
	{
		{	/* BackEnd/cvm.sch 320 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_191)))->BgL_externzd2typeszd2) =
				((obj_t) BgL_vz00_192), BUNSPEC);
		}

	}



/* &sawc-extern-types-set! */
	obj_t BGl_z62sawczd2externzd2typeszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1408, obj_t BgL_oz00_1409, obj_t BgL_vz00_1410)
	{
		{	/* BackEnd/cvm.sch 320 */
			return
				BGl_sawczd2externzd2typeszd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1409), BgL_vz00_1410);
		}

	}



/* sawc-extern-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2externzd2functionsz00zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_193)
	{
		{	/* BackEnd/cvm.sch 321 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_193)))->BgL_externzd2functionszd2);
		}

	}



/* &sawc-extern-functions */
	obj_t BGl_z62sawczd2externzd2functionsz62zzbackend_cvmz00(obj_t
		BgL_envz00_1411, obj_t BgL_oz00_1412)
	{
		{	/* BackEnd/cvm.sch 321 */
			return
				BGl_sawczd2externzd2functionsz00zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1412));
		}

	}



/* sawc-extern-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2externzd2functionszd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_194, obj_t BgL_vz00_195)
	{
		{	/* BackEnd/cvm.sch 322 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_194)))->
					BgL_externzd2functionszd2) = ((obj_t) BgL_vz00_195), BUNSPEC);
		}

	}



/* &sawc-extern-functions-set! */
	obj_t BGl_z62sawczd2externzd2functionszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1413, obj_t BgL_oz00_1414, obj_t BgL_vz00_1415)
	{
		{	/* BackEnd/cvm.sch 322 */
			return
				BGl_sawczd2externzd2functionszd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1414), BgL_vz00_1415);
		}

	}



/* sawc-extern-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2externzd2variablesz00zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_196)
	{
		{	/* BackEnd/cvm.sch 323 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_196)))->BgL_externzd2variableszd2);
		}

	}



/* &sawc-extern-variables */
	obj_t BGl_z62sawczd2externzd2variablesz62zzbackend_cvmz00(obj_t
		BgL_envz00_1416, obj_t BgL_oz00_1417)
	{
		{	/* BackEnd/cvm.sch 323 */
			return
				BGl_sawczd2externzd2variablesz00zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1417));
		}

	}



/* sawc-extern-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2externzd2variableszd2setz12zc0zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_197, obj_t BgL_vz00_198)
	{
		{	/* BackEnd/cvm.sch 324 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_197)))->
					BgL_externzd2variableszd2) = ((obj_t) BgL_vz00_198), BUNSPEC);
		}

	}



/* &sawc-extern-variables-set! */
	obj_t BGl_z62sawczd2externzd2variableszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1418, obj_t BgL_oz00_1419, obj_t BgL_vz00_1420)
	{
		{	/* BackEnd/cvm.sch 324 */
			return
				BGl_sawczd2externzd2variableszd2setz12zc0zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1419), BgL_vz00_1420);
		}

	}



/* sawc-name */
	BGL_EXPORTED_DEF obj_t BGl_sawczd2namezd2zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_199)
	{
		{	/* BackEnd/cvm.sch 325 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_199)))->BgL_namez00);
		}

	}



/* &sawc-name */
	obj_t BGl_z62sawczd2namezb0zzbackend_cvmz00(obj_t BgL_envz00_1421,
		obj_t BgL_oz00_1422)
	{
		{	/* BackEnd/cvm.sch 325 */
			return
				BGl_sawczd2namezd2zzbackend_cvmz00(((BgL_sawcz00_bglt) BgL_oz00_1422));
		}

	}



/* sawc-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2namezd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_200,
		obj_t BgL_vz00_201)
	{
		{	/* BackEnd/cvm.sch 326 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_200)))->BgL_namez00) =
				((obj_t) BgL_vz00_201), BUNSPEC);
		}

	}



/* &sawc-name-set! */
	obj_t BGl_z62sawczd2namezd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1423,
		obj_t BgL_oz00_1424, obj_t BgL_vz00_1425)
	{
		{	/* BackEnd/cvm.sch 326 */
			return
				BGl_sawczd2namezd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1424), BgL_vz00_1425);
		}

	}



/* sawc-srfi0 */
	BGL_EXPORTED_DEF obj_t BGl_sawczd2srfi0zd2zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_202)
	{
		{	/* BackEnd/cvm.sch 327 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_202)))->BgL_srfi0z00);
		}

	}



/* &sawc-srfi0 */
	obj_t BGl_z62sawczd2srfi0zb0zzbackend_cvmz00(obj_t BgL_envz00_1426,
		obj_t BgL_oz00_1427)
	{
		{	/* BackEnd/cvm.sch 327 */
			return
				BGl_sawczd2srfi0zd2zzbackend_cvmz00(((BgL_sawcz00_bglt) BgL_oz00_1427));
		}

	}



/* sawc-srfi0-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2srfi0zd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt BgL_oz00_203,
		obj_t BgL_vz00_204)
	{
		{	/* BackEnd/cvm.sch 328 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_203)))->BgL_srfi0z00) =
				((obj_t) BgL_vz00_204), BUNSPEC);
		}

	}



/* &sawc-srfi0-set! */
	obj_t BGl_z62sawczd2srfi0zd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1428,
		obj_t BgL_oz00_1429, obj_t BgL_vz00_1430)
	{
		{	/* BackEnd/cvm.sch 328 */
			return
				BGl_sawczd2srfi0zd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1429), BgL_vz00_1430);
		}

	}



/* sawc-language */
	BGL_EXPORTED_DEF obj_t BGl_sawczd2languagezd2zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_205)
	{
		{	/* BackEnd/cvm.sch 329 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_205)))->BgL_languagez00);
		}

	}



/* &sawc-language */
	obj_t BGl_z62sawczd2languagezb0zzbackend_cvmz00(obj_t BgL_envz00_1431,
		obj_t BgL_oz00_1432)
	{
		{	/* BackEnd/cvm.sch 329 */
			return
				BGl_sawczd2languagezd2zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1432));
		}

	}



/* sawc-language-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sawczd2languagezd2setz12z12zzbackend_cvmz00(BgL_sawcz00_bglt
		BgL_oz00_206, obj_t BgL_vz00_207)
	{
		{	/* BackEnd/cvm.sch 330 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_206)))->BgL_languagez00) =
				((obj_t) BgL_vz00_207), BUNSPEC);
		}

	}



/* &sawc-language-set! */
	obj_t BGl_z62sawczd2languagezd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1433, obj_t BgL_oz00_1434, obj_t BgL_vz00_1435)
	{
		{	/* BackEnd/cvm.sch 330 */
			return
				BGl_sawczd2languagezd2setz12z12zzbackend_cvmz00(
				((BgL_sawcz00_bglt) BgL_oz00_1434), BgL_vz00_1435);
		}

	}



/* make-cgen */
	BGL_EXPORTED_DEF BgL_cgenz00_bglt BGl_makezd2cgenzd2zzbackend_cvmz00(obj_t
		BgL_language1164z00_208, obj_t BgL_srfi01165z00_209,
		obj_t BgL_name1166z00_210, obj_t BgL_externzd2variables1167zd2_211,
		obj_t BgL_externzd2functions1168zd2_212,
		obj_t BgL_externzd2types1169zd2_213, obj_t BgL_variables1170z00_214,
		obj_t BgL_functions1171z00_215, obj_t BgL_types1172z00_216,
		bool_t BgL_typed1173z00_217, obj_t BgL_heapzd2suffix1174zd2_218,
		obj_t BgL_heapzd2compatible1175zd2_219, bool_t BgL_callcc1176z00_220,
		bool_t BgL_qualifiedzd2types1177zd2_221, bool_t BgL_effectzb21178zb2_222,
		bool_t BgL_removezd2emptyzd2let1179z00_223,
		bool_t BgL_foreignzd2closure1180zd2_224, bool_t BgL_typedzd2eq1181zd2_225,
		bool_t BgL_tracezd2support1182zd2_226,
		obj_t BgL_foreignzd2clausezd2suppo1183z00_227,
		obj_t BgL_debugzd2support1184zd2_228,
		bool_t BgL_pragmazd2support1185zd2_229,
		bool_t BgL_tvectorzd2descrzd2suppor1186z00_230,
		bool_t BgL_requirezd2tailc1187zd2_231, obj_t BgL_registers1188z00_232,
		obj_t BgL_pregisters1189z00_233, bool_t BgL_boundzd2check1190zd2_234,
		bool_t BgL_typezd2check1191zd2_235, bool_t BgL_typedzd2funcall1192zd2_236)
	{
		{	/* BackEnd/cvm.sch 333 */
			{	/* BackEnd/cvm.sch 333 */
				BgL_cgenz00_bglt BgL_new1243z00_1744;

				{	/* BackEnd/cvm.sch 333 */
					BgL_cgenz00_bglt BgL_new1242z00_1745;

					BgL_new1242z00_1745 =
						((BgL_cgenz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cgenz00_bgl))));
					{	/* BackEnd/cvm.sch 333 */
						long BgL_arg1322z00_1746;

						BgL_arg1322z00_1746 = BGL_CLASS_NUM(BGl_cgenz00zzbackend_cvmz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1242z00_1745), BgL_arg1322z00_1746);
					}
					BgL_new1243z00_1744 = BgL_new1242z00_1745;
				}
				((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_new1243z00_1744)))->
						BgL_languagez00) = ((obj_t) BgL_language1164z00_208), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_srfi0z00) =
					((obj_t) BgL_srfi01165z00_209), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_namez00) =
					((obj_t) BgL_name1166z00_210), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_externzd2variableszd2) =
					((obj_t) BgL_externzd2variables1167zd2_211), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_externzd2functionszd2) =
					((obj_t) BgL_externzd2functions1168zd2_212), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_externzd2typeszd2) =
					((obj_t) BgL_externzd2types1169zd2_213), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_variablesz00) =
					((obj_t) BgL_variables1170z00_214), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_functionsz00) =
					((obj_t) BgL_functions1171z00_215), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_typesz00) =
					((obj_t) BgL_types1172z00_216), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_typedz00) =
					((bool_t) BgL_typed1173z00_217), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_heapzd2suffixzd2) =
					((obj_t) BgL_heapzd2suffix1174zd2_218), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_heapzd2compatiblezd2) =
					((obj_t) BgL_heapzd2compatible1175zd2_219), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_callccz00) =
					((bool_t) BgL_callcc1176z00_220), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_qualifiedzd2typeszd2) =
					((bool_t) BgL_qualifiedzd2types1177zd2_221), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_effectzb2zb2) =
					((bool_t) BgL_effectzb21178zb2_222), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_removezd2emptyzd2letz00) =
					((bool_t) BgL_removezd2emptyzd2let1179z00_223), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_foreignzd2closurezd2) =
					((bool_t) BgL_foreignzd2closure1180zd2_224), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_typedzd2eqzd2) =
					((bool_t) BgL_typedzd2eq1181zd2_225), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_tracezd2supportzd2) =
					((bool_t) BgL_tracezd2support1182zd2_226), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_foreignzd2clausezd2supportz00) =
					((obj_t) BgL_foreignzd2clausezd2suppo1183z00_227), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_debugzd2supportzd2) =
					((obj_t) BgL_debugzd2support1184zd2_228), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_pragmazd2supportzd2) =
					((bool_t) BgL_pragmazd2support1185zd2_229), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) BgL_tvectorzd2descrzd2suppor1186z00_230), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_requirezd2tailczd2) =
					((bool_t) BgL_requirezd2tailc1187zd2_231), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_registersz00) =
					((obj_t) BgL_registers1188z00_232), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_pregistersz00) =
					((obj_t) BgL_pregisters1189z00_233), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_boundzd2checkzd2) =
					((bool_t) BgL_boundzd2check1190zd2_234), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_typezd2checkzd2) =
					((bool_t) BgL_typezd2check1191zd2_235), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_typedzd2funcallzd2) =
					((bool_t) BgL_typedzd2funcall1192zd2_236), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_strictzd2typezd2castz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1243z00_1744)))->BgL_stringzd2literalzd2supportz00) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				{	/* BackEnd/cvm.sch 333 */
					obj_t BgL_fun1321z00_1747;

					BgL_fun1321z00_1747 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_cgenz00zzbackend_cvmz00);
					BGL_PROCEDURE_CALL1(BgL_fun1321z00_1747,
						((obj_t) BgL_new1243z00_1744));
				}
				return BgL_new1243z00_1744;
			}
		}

	}



/* &make-cgen */
	BgL_cgenz00_bglt BGl_z62makezd2cgenzb0zzbackend_cvmz00(obj_t BgL_envz00_1436,
		obj_t BgL_language1164z00_1437, obj_t BgL_srfi01165z00_1438,
		obj_t BgL_name1166z00_1439, obj_t BgL_externzd2variables1167zd2_1440,
		obj_t BgL_externzd2functions1168zd2_1441,
		obj_t BgL_externzd2types1169zd2_1442, obj_t BgL_variables1170z00_1443,
		obj_t BgL_functions1171z00_1444, obj_t BgL_types1172z00_1445,
		obj_t BgL_typed1173z00_1446, obj_t BgL_heapzd2suffix1174zd2_1447,
		obj_t BgL_heapzd2compatible1175zd2_1448, obj_t BgL_callcc1176z00_1449,
		obj_t BgL_qualifiedzd2types1177zd2_1450, obj_t BgL_effectzb21178zb2_1451,
		obj_t BgL_removezd2emptyzd2let1179z00_1452,
		obj_t BgL_foreignzd2closure1180zd2_1453, obj_t BgL_typedzd2eq1181zd2_1454,
		obj_t BgL_tracezd2support1182zd2_1455,
		obj_t BgL_foreignzd2clausezd2suppo1183z00_1456,
		obj_t BgL_debugzd2support1184zd2_1457,
		obj_t BgL_pragmazd2support1185zd2_1458,
		obj_t BgL_tvectorzd2descrzd2suppor1186z00_1459,
		obj_t BgL_requirezd2tailc1187zd2_1460, obj_t BgL_registers1188z00_1461,
		obj_t BgL_pregisters1189z00_1462, obj_t BgL_boundzd2check1190zd2_1463,
		obj_t BgL_typezd2check1191zd2_1464, obj_t BgL_typedzd2funcall1192zd2_1465)
	{
		{	/* BackEnd/cvm.sch 333 */
			return
				BGl_makezd2cgenzd2zzbackend_cvmz00(BgL_language1164z00_1437,
				BgL_srfi01165z00_1438, BgL_name1166z00_1439,
				BgL_externzd2variables1167zd2_1440, BgL_externzd2functions1168zd2_1441,
				BgL_externzd2types1169zd2_1442, BgL_variables1170z00_1443,
				BgL_functions1171z00_1444, BgL_types1172z00_1445,
				CBOOL(BgL_typed1173z00_1446), BgL_heapzd2suffix1174zd2_1447,
				BgL_heapzd2compatible1175zd2_1448, CBOOL(BgL_callcc1176z00_1449),
				CBOOL(BgL_qualifiedzd2types1177zd2_1450),
				CBOOL(BgL_effectzb21178zb2_1451),
				CBOOL(BgL_removezd2emptyzd2let1179z00_1452),
				CBOOL(BgL_foreignzd2closure1180zd2_1453),
				CBOOL(BgL_typedzd2eq1181zd2_1454),
				CBOOL(BgL_tracezd2support1182zd2_1455),
				BgL_foreignzd2clausezd2suppo1183z00_1456,
				BgL_debugzd2support1184zd2_1457,
				CBOOL(BgL_pragmazd2support1185zd2_1458),
				CBOOL(BgL_tvectorzd2descrzd2suppor1186z00_1459),
				CBOOL(BgL_requirezd2tailc1187zd2_1460), BgL_registers1188z00_1461,
				BgL_pregisters1189z00_1462, CBOOL(BgL_boundzd2check1190zd2_1463),
				CBOOL(BgL_typezd2check1191zd2_1464),
				CBOOL(BgL_typedzd2funcall1192zd2_1465));
		}

	}



/* cgen? */
	BGL_EXPORTED_DEF bool_t BGl_cgenzf3zf3zzbackend_cvmz00(obj_t BgL_objz00_237)
	{
		{	/* BackEnd/cvm.sch 334 */
			{	/* BackEnd/cvm.sch 334 */
				obj_t BgL_classz00_1748;

				BgL_classz00_1748 = BGl_cgenz00zzbackend_cvmz00;
				if (BGL_OBJECTP(BgL_objz00_237))
					{	/* BackEnd/cvm.sch 334 */
						BgL_objectz00_bglt BgL_arg1807z00_1749;

						BgL_arg1807z00_1749 = (BgL_objectz00_bglt) (BgL_objz00_237);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* BackEnd/cvm.sch 334 */
								long BgL_idxz00_1750;

								BgL_idxz00_1750 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1749);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_1750 + 3L)) == BgL_classz00_1748);
							}
						else
							{	/* BackEnd/cvm.sch 334 */
								bool_t BgL_res1373z00_1753;

								{	/* BackEnd/cvm.sch 334 */
									obj_t BgL_oclassz00_1754;

									{	/* BackEnd/cvm.sch 334 */
										obj_t BgL_arg1815z00_1755;
										long BgL_arg1816z00_1756;

										BgL_arg1815z00_1755 = (BGl_za2classesza2z00zz__objectz00);
										{	/* BackEnd/cvm.sch 334 */
											long BgL_arg1817z00_1757;

											BgL_arg1817z00_1757 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1749);
											BgL_arg1816z00_1756 = (BgL_arg1817z00_1757 - OBJECT_TYPE);
										}
										BgL_oclassz00_1754 =
											VECTOR_REF(BgL_arg1815z00_1755, BgL_arg1816z00_1756);
									}
									{	/* BackEnd/cvm.sch 334 */
										bool_t BgL__ortest_1115z00_1758;

										BgL__ortest_1115z00_1758 =
											(BgL_classz00_1748 == BgL_oclassz00_1754);
										if (BgL__ortest_1115z00_1758)
											{	/* BackEnd/cvm.sch 334 */
												BgL_res1373z00_1753 = BgL__ortest_1115z00_1758;
											}
										else
											{	/* BackEnd/cvm.sch 334 */
												long BgL_odepthz00_1759;

												{	/* BackEnd/cvm.sch 334 */
													obj_t BgL_arg1804z00_1760;

													BgL_arg1804z00_1760 = (BgL_oclassz00_1754);
													BgL_odepthz00_1759 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_1760);
												}
												if ((3L < BgL_odepthz00_1759))
													{	/* BackEnd/cvm.sch 334 */
														obj_t BgL_arg1802z00_1761;

														{	/* BackEnd/cvm.sch 334 */
															obj_t BgL_arg1803z00_1762;

															BgL_arg1803z00_1762 = (BgL_oclassz00_1754);
															BgL_arg1802z00_1761 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1762,
																3L);
														}
														BgL_res1373z00_1753 =
															(BgL_arg1802z00_1761 == BgL_classz00_1748);
													}
												else
													{	/* BackEnd/cvm.sch 334 */
														BgL_res1373z00_1753 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1373z00_1753;
							}
					}
				else
					{	/* BackEnd/cvm.sch 334 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cgen? */
	obj_t BGl_z62cgenzf3z91zzbackend_cvmz00(obj_t BgL_envz00_1466,
		obj_t BgL_objz00_1467)
	{
		{	/* BackEnd/cvm.sch 334 */
			return BBOOL(BGl_cgenzf3zf3zzbackend_cvmz00(BgL_objz00_1467));
		}

	}



/* cgen-nil */
	BGL_EXPORTED_DEF BgL_cgenz00_bglt BGl_cgenzd2nilzd2zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.sch 335 */
			{	/* BackEnd/cvm.sch 335 */
				obj_t BgL_classz00_1027;

				BgL_classz00_1027 = BGl_cgenz00zzbackend_cvmz00;
				{	/* BackEnd/cvm.sch 335 */
					obj_t BgL__ortest_1117z00_1028;

					BgL__ortest_1117z00_1028 = BGL_CLASS_NIL(BgL_classz00_1027);
					if (CBOOL(BgL__ortest_1117z00_1028))
						{	/* BackEnd/cvm.sch 335 */
							return ((BgL_cgenz00_bglt) BgL__ortest_1117z00_1028);
						}
					else
						{	/* BackEnd/cvm.sch 335 */
							return
								((BgL_cgenz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1027));
						}
				}
			}
		}

	}



/* &cgen-nil */
	BgL_cgenz00_bglt BGl_z62cgenzd2nilzb0zzbackend_cvmz00(obj_t BgL_envz00_1468)
	{
		{	/* BackEnd/cvm.sch 335 */
			return BGl_cgenzd2nilzd2zzbackend_cvmz00();
		}

	}



/* cgen-typed-funcall */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2typedzd2funcallz00zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_238)
	{
		{	/* BackEnd/cvm.sch 336 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_238)))->BgL_typedzd2funcallzd2);
		}

	}



/* &cgen-typed-funcall */
	obj_t BGl_z62cgenzd2typedzd2funcallz62zzbackend_cvmz00(obj_t BgL_envz00_1469,
		obj_t BgL_oz00_1470)
	{
		{	/* BackEnd/cvm.sch 336 */
			return
				BBOOL(BGl_cgenzd2typedzd2funcallz00zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1470)));
		}

	}



/* cgen-typed-funcall-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2typedzd2funcallzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_239, bool_t BgL_vz00_240)
	{
		{	/* BackEnd/cvm.sch 337 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_239)))->BgL_typedzd2funcallzd2) =
				((bool_t) BgL_vz00_240), BUNSPEC);
		}

	}



/* &cgen-typed-funcall-set! */
	obj_t BGl_z62cgenzd2typedzd2funcallzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1471, obj_t BgL_oz00_1472, obj_t BgL_vz00_1473)
	{
		{	/* BackEnd/cvm.sch 337 */
			return
				BGl_cgenzd2typedzd2funcallzd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1472), CBOOL(BgL_vz00_1473));
		}

	}



/* cgen-type-check */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2typezd2checkz00zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_241)
	{
		{	/* BackEnd/cvm.sch 338 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_241)))->BgL_typezd2checkzd2);
		}

	}



/* &cgen-type-check */
	obj_t BGl_z62cgenzd2typezd2checkz62zzbackend_cvmz00(obj_t BgL_envz00_1474,
		obj_t BgL_oz00_1475)
	{
		{	/* BackEnd/cvm.sch 338 */
			return
				BBOOL(BGl_cgenzd2typezd2checkz00zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1475)));
		}

	}



/* cgen-type-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2typezd2checkzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_242, bool_t BgL_vz00_243)
	{
		{	/* BackEnd/cvm.sch 339 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_242)))->BgL_typezd2checkzd2) =
				((bool_t) BgL_vz00_243), BUNSPEC);
		}

	}



/* &cgen-type-check-set! */
	obj_t BGl_z62cgenzd2typezd2checkzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1476, obj_t BgL_oz00_1477, obj_t BgL_vz00_1478)
	{
		{	/* BackEnd/cvm.sch 339 */
			return
				BGl_cgenzd2typezd2checkzd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1477), CBOOL(BgL_vz00_1478));
		}

	}



/* cgen-bound-check */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2boundzd2checkz00zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_244)
	{
		{	/* BackEnd/cvm.sch 340 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_244)))->BgL_boundzd2checkzd2);
		}

	}



/* &cgen-bound-check */
	obj_t BGl_z62cgenzd2boundzd2checkz62zzbackend_cvmz00(obj_t BgL_envz00_1479,
		obj_t BgL_oz00_1480)
	{
		{	/* BackEnd/cvm.sch 340 */
			return
				BBOOL(BGl_cgenzd2boundzd2checkz00zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1480)));
		}

	}



/* cgen-bound-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2boundzd2checkzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_245, bool_t BgL_vz00_246)
	{
		{	/* BackEnd/cvm.sch 341 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_245)))->BgL_boundzd2checkzd2) =
				((bool_t) BgL_vz00_246), BUNSPEC);
		}

	}



/* &cgen-bound-check-set! */
	obj_t BGl_z62cgenzd2boundzd2checkzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1481, obj_t BgL_oz00_1482, obj_t BgL_vz00_1483)
	{
		{	/* BackEnd/cvm.sch 341 */
			return
				BGl_cgenzd2boundzd2checkzd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1482), CBOOL(BgL_vz00_1483));
		}

	}



/* cgen-pregisters */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2pregisterszd2zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_247)
	{
		{	/* BackEnd/cvm.sch 342 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_247)))->BgL_pregistersz00);
		}

	}



/* &cgen-pregisters */
	obj_t BGl_z62cgenzd2pregisterszb0zzbackend_cvmz00(obj_t BgL_envz00_1484,
		obj_t BgL_oz00_1485)
	{
		{	/* BackEnd/cvm.sch 342 */
			return
				BGl_cgenzd2pregisterszd2zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1485));
		}

	}



/* cgen-pregisters-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2pregisterszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_248, obj_t BgL_vz00_249)
	{
		{	/* BackEnd/cvm.sch 343 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_248)))->BgL_pregistersz00) =
				((obj_t) BgL_vz00_249), BUNSPEC);
		}

	}



/* &cgen-pregisters-set! */
	obj_t BGl_z62cgenzd2pregisterszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1486, obj_t BgL_oz00_1487, obj_t BgL_vz00_1488)
	{
		{	/* BackEnd/cvm.sch 343 */
			return
				BGl_cgenzd2pregisterszd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1487), BgL_vz00_1488);
		}

	}



/* cgen-registers */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2registerszd2zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_250)
	{
		{	/* BackEnd/cvm.sch 344 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_250)))->BgL_registersz00);
		}

	}



/* &cgen-registers */
	obj_t BGl_z62cgenzd2registerszb0zzbackend_cvmz00(obj_t BgL_envz00_1489,
		obj_t BgL_oz00_1490)
	{
		{	/* BackEnd/cvm.sch 344 */
			return
				BGl_cgenzd2registerszd2zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1490));
		}

	}



/* cgen-registers-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2registerszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_251, obj_t BgL_vz00_252)
	{
		{	/* BackEnd/cvm.sch 345 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_251)))->BgL_registersz00) =
				((obj_t) BgL_vz00_252), BUNSPEC);
		}

	}



/* &cgen-registers-set! */
	obj_t BGl_z62cgenzd2registerszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1491, obj_t BgL_oz00_1492, obj_t BgL_vz00_1493)
	{
		{	/* BackEnd/cvm.sch 345 */
			return
				BGl_cgenzd2registerszd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1492), BgL_vz00_1493);
		}

	}



/* cgen-require-tailc */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2requirezd2tailcz00zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_253)
	{
		{	/* BackEnd/cvm.sch 346 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_253)))->BgL_requirezd2tailczd2);
		}

	}



/* &cgen-require-tailc */
	obj_t BGl_z62cgenzd2requirezd2tailcz62zzbackend_cvmz00(obj_t BgL_envz00_1494,
		obj_t BgL_oz00_1495)
	{
		{	/* BackEnd/cvm.sch 346 */
			return
				BBOOL(BGl_cgenzd2requirezd2tailcz00zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1495)));
		}

	}



/* cgen-require-tailc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2requirezd2tailczd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_254, bool_t BgL_vz00_255)
	{
		{	/* BackEnd/cvm.sch 347 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_254)))->BgL_requirezd2tailczd2) =
				((bool_t) BgL_vz00_255), BUNSPEC);
		}

	}



/* &cgen-require-tailc-set! */
	obj_t BGl_z62cgenzd2requirezd2tailczd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1496, obj_t BgL_oz00_1497, obj_t BgL_vz00_1498)
	{
		{	/* BackEnd/cvm.sch 347 */
			return
				BGl_cgenzd2requirezd2tailczd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1497), CBOOL(BgL_vz00_1498));
		}

	}



/* cgen-tvector-descr-support */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2tvectorzd2descrzd2supportzd2zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_256)
	{
		{	/* BackEnd/cvm.sch 348 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_256)))->
				BgL_tvectorzd2descrzd2supportz00);
		}

	}



/* &cgen-tvector-descr-support */
	obj_t BGl_z62cgenzd2tvectorzd2descrzd2supportzb0zzbackend_cvmz00(obj_t
		BgL_envz00_1499, obj_t BgL_oz00_1500)
	{
		{	/* BackEnd/cvm.sch 348 */
			return
				BBOOL(BGl_cgenzd2tvectorzd2descrzd2supportzd2zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1500)));
		}

	}



/* cgen-tvector-descr-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_cgenz00_bglt BgL_oz00_257, bool_t BgL_vz00_258)
	{
		{	/* BackEnd/cvm.sch 349 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_257)))->
					BgL_tvectorzd2descrzd2supportz00) = ((bool_t) BgL_vz00_258), BUNSPEC);
		}

	}



/* &cgen-tvector-descr-support-set! */
	obj_t
		BGl_z62cgenzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1501, obj_t BgL_oz00_1502, obj_t BgL_vz00_1503)
	{
		{	/* BackEnd/cvm.sch 349 */
			return
				BGl_cgenzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1502), CBOOL(BgL_vz00_1503));
		}

	}



/* cgen-pragma-support */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2pragmazd2supportz00zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_259)
	{
		{	/* BackEnd/cvm.sch 350 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_259)))->BgL_pragmazd2supportzd2);
		}

	}



/* &cgen-pragma-support */
	obj_t BGl_z62cgenzd2pragmazd2supportz62zzbackend_cvmz00(obj_t BgL_envz00_1504,
		obj_t BgL_oz00_1505)
	{
		{	/* BackEnd/cvm.sch 350 */
			return
				BBOOL(BGl_cgenzd2pragmazd2supportz00zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1505)));
		}

	}



/* cgen-pragma-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2pragmazd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_260, bool_t BgL_vz00_261)
	{
		{	/* BackEnd/cvm.sch 351 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_260)))->BgL_pragmazd2supportzd2) =
				((bool_t) BgL_vz00_261), BUNSPEC);
		}

	}



/* &cgen-pragma-support-set! */
	obj_t BGl_z62cgenzd2pragmazd2supportzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1506, obj_t BgL_oz00_1507, obj_t BgL_vz00_1508)
	{
		{	/* BackEnd/cvm.sch 351 */
			return
				BGl_cgenzd2pragmazd2supportzd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1507), CBOOL(BgL_vz00_1508));
		}

	}



/* cgen-debug-support */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2debugzd2supportz00zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_262)
	{
		{	/* BackEnd/cvm.sch 352 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_262)))->BgL_debugzd2supportzd2);
		}

	}



/* &cgen-debug-support */
	obj_t BGl_z62cgenzd2debugzd2supportz62zzbackend_cvmz00(obj_t BgL_envz00_1509,
		obj_t BgL_oz00_1510)
	{
		{	/* BackEnd/cvm.sch 352 */
			return
				BGl_cgenzd2debugzd2supportz00zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1510));
		}

	}



/* cgen-debug-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2debugzd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_263, obj_t BgL_vz00_264)
	{
		{	/* BackEnd/cvm.sch 353 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_263)))->BgL_debugzd2supportzd2) =
				((obj_t) BgL_vz00_264), BUNSPEC);
		}

	}



/* &cgen-debug-support-set! */
	obj_t BGl_z62cgenzd2debugzd2supportzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1511, obj_t BgL_oz00_1512, obj_t BgL_vz00_1513)
	{
		{	/* BackEnd/cvm.sch 353 */
			return
				BGl_cgenzd2debugzd2supportzd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1512), BgL_vz00_1513);
		}

	}



/* cgen-foreign-clause-support */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2foreignzd2clausezd2supportzd2zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_265)
	{
		{	/* BackEnd/cvm.sch 354 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_265)))->
				BgL_foreignzd2clausezd2supportz00);
		}

	}



/* &cgen-foreign-clause-support */
	obj_t BGl_z62cgenzd2foreignzd2clausezd2supportzb0zzbackend_cvmz00(obj_t
		BgL_envz00_1514, obj_t BgL_oz00_1515)
	{
		{	/* BackEnd/cvm.sch 354 */
			return
				BGl_cgenzd2foreignzd2clausezd2supportzd2zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1515));
		}

	}



/* cgen-foreign-clause-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_cvmz00
		(BgL_cgenz00_bglt BgL_oz00_266, obj_t BgL_vz00_267)
	{
		{	/* BackEnd/cvm.sch 355 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_266)))->
					BgL_foreignzd2clausezd2supportz00) = ((obj_t) BgL_vz00_267), BUNSPEC);
		}

	}



/* &cgen-foreign-clause-support-set! */
	obj_t
		BGl_z62cgenzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1516, obj_t BgL_oz00_1517, obj_t BgL_vz00_1518)
	{
		{	/* BackEnd/cvm.sch 355 */
			return
				BGl_cgenzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1517), BgL_vz00_1518);
		}

	}



/* cgen-trace-support */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2tracezd2supportz00zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_268)
	{
		{	/* BackEnd/cvm.sch 356 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_268)))->BgL_tracezd2supportzd2);
		}

	}



/* &cgen-trace-support */
	obj_t BGl_z62cgenzd2tracezd2supportz62zzbackend_cvmz00(obj_t BgL_envz00_1519,
		obj_t BgL_oz00_1520)
	{
		{	/* BackEnd/cvm.sch 356 */
			return
				BBOOL(BGl_cgenzd2tracezd2supportz00zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1520)));
		}

	}



/* cgen-trace-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2tracezd2supportzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_269, bool_t BgL_vz00_270)
	{
		{	/* BackEnd/cvm.sch 357 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_269)))->BgL_tracezd2supportzd2) =
				((bool_t) BgL_vz00_270), BUNSPEC);
		}

	}



/* &cgen-trace-support-set! */
	obj_t BGl_z62cgenzd2tracezd2supportzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1521, obj_t BgL_oz00_1522, obj_t BgL_vz00_1523)
	{
		{	/* BackEnd/cvm.sch 357 */
			return
				BGl_cgenzd2tracezd2supportzd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1522), CBOOL(BgL_vz00_1523));
		}

	}



/* cgen-typed-eq */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2typedzd2eqz00zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_271)
	{
		{	/* BackEnd/cvm.sch 358 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_271)))->BgL_typedzd2eqzd2);
		}

	}



/* &cgen-typed-eq */
	obj_t BGl_z62cgenzd2typedzd2eqz62zzbackend_cvmz00(obj_t BgL_envz00_1524,
		obj_t BgL_oz00_1525)
	{
		{	/* BackEnd/cvm.sch 358 */
			return
				BBOOL(BGl_cgenzd2typedzd2eqz00zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1525)));
		}

	}



/* cgen-typed-eq-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2typedzd2eqzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_272, bool_t BgL_vz00_273)
	{
		{	/* BackEnd/cvm.sch 359 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_272)))->BgL_typedzd2eqzd2) =
				((bool_t) BgL_vz00_273), BUNSPEC);
		}

	}



/* &cgen-typed-eq-set! */
	obj_t BGl_z62cgenzd2typedzd2eqzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1526, obj_t BgL_oz00_1527, obj_t BgL_vz00_1528)
	{
		{	/* BackEnd/cvm.sch 359 */
			return
				BGl_cgenzd2typedzd2eqzd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1527), CBOOL(BgL_vz00_1528));
		}

	}



/* cgen-foreign-closure */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2foreignzd2closurez00zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_274)
	{
		{	/* BackEnd/cvm.sch 360 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_274)))->BgL_foreignzd2closurezd2);
		}

	}



/* &cgen-foreign-closure */
	obj_t BGl_z62cgenzd2foreignzd2closurez62zzbackend_cvmz00(obj_t
		BgL_envz00_1529, obj_t BgL_oz00_1530)
	{
		{	/* BackEnd/cvm.sch 360 */
			return
				BBOOL(BGl_cgenzd2foreignzd2closurez00zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1530)));
		}

	}



/* cgen-foreign-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2foreignzd2closurezd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_275, bool_t BgL_vz00_276)
	{
		{	/* BackEnd/cvm.sch 361 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_275)))->
					BgL_foreignzd2closurezd2) = ((bool_t) BgL_vz00_276), BUNSPEC);
		}

	}



/* &cgen-foreign-closure-set! */
	obj_t BGl_z62cgenzd2foreignzd2closurezd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1531, obj_t BgL_oz00_1532, obj_t BgL_vz00_1533)
	{
		{	/* BackEnd/cvm.sch 361 */
			return
				BGl_cgenzd2foreignzd2closurezd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1532), CBOOL(BgL_vz00_1533));
		}

	}



/* cgen-remove-empty-let */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2removezd2emptyzd2letzd2zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_277)
	{
		{	/* BackEnd/cvm.sch 362 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_277)))->
				BgL_removezd2emptyzd2letz00);
		}

	}



/* &cgen-remove-empty-let */
	obj_t BGl_z62cgenzd2removezd2emptyzd2letzb0zzbackend_cvmz00(obj_t
		BgL_envz00_1534, obj_t BgL_oz00_1535)
	{
		{	/* BackEnd/cvm.sch 362 */
			return
				BBOOL(BGl_cgenzd2removezd2emptyzd2letzd2zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1535)));
		}

	}



/* cgen-remove-empty-let-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2removezd2emptyzd2letzd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_278, bool_t BgL_vz00_279)
	{
		{	/* BackEnd/cvm.sch 363 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_278)))->
					BgL_removezd2emptyzd2letz00) = ((bool_t) BgL_vz00_279), BUNSPEC);
		}

	}



/* &cgen-remove-empty-let-set! */
	obj_t BGl_z62cgenzd2removezd2emptyzd2letzd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1536, obj_t BgL_oz00_1537, obj_t BgL_vz00_1538)
	{
		{	/* BackEnd/cvm.sch 363 */
			return
				BGl_cgenzd2removezd2emptyzd2letzd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1537), CBOOL(BgL_vz00_1538));
		}

	}



/* cgen-effect+ */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2effectzb2z60zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_280)
	{
		{	/* BackEnd/cvm.sch 364 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_280)))->BgL_effectzb2zb2);
		}

	}



/* &cgen-effect+ */
	obj_t BGl_z62cgenzd2effectzb2z02zzbackend_cvmz00(obj_t BgL_envz00_1539,
		obj_t BgL_oz00_1540)
	{
		{	/* BackEnd/cvm.sch 364 */
			return
				BBOOL(BGl_cgenzd2effectzb2z60zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1540)));
		}

	}



/* cgen-effect+-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2effectzb2zd2setz12za0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_281, bool_t BgL_vz00_282)
	{
		{	/* BackEnd/cvm.sch 365 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_281)))->BgL_effectzb2zb2) =
				((bool_t) BgL_vz00_282), BUNSPEC);
		}

	}



/* &cgen-effect+-set! */
	obj_t BGl_z62cgenzd2effectzb2zd2setz12zc2zzbackend_cvmz00(obj_t
		BgL_envz00_1541, obj_t BgL_oz00_1542, obj_t BgL_vz00_1543)
	{
		{	/* BackEnd/cvm.sch 365 */
			return
				BGl_cgenzd2effectzb2zd2setz12za0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1542), CBOOL(BgL_vz00_1543));
		}

	}



/* cgen-qualified-types */
	BGL_EXPORTED_DEF bool_t
		BGl_cgenzd2qualifiedzd2typesz00zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_283)
	{
		{	/* BackEnd/cvm.sch 366 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_283)))->BgL_qualifiedzd2typeszd2);
		}

	}



/* &cgen-qualified-types */
	obj_t BGl_z62cgenzd2qualifiedzd2typesz62zzbackend_cvmz00(obj_t
		BgL_envz00_1544, obj_t BgL_oz00_1545)
	{
		{	/* BackEnd/cvm.sch 366 */
			return
				BBOOL(BGl_cgenzd2qualifiedzd2typesz00zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1545)));
		}

	}



/* cgen-qualified-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2qualifiedzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_284, bool_t BgL_vz00_285)
	{
		{	/* BackEnd/cvm.sch 367 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_284)))->
					BgL_qualifiedzd2typeszd2) = ((bool_t) BgL_vz00_285), BUNSPEC);
		}

	}



/* &cgen-qualified-types-set! */
	obj_t BGl_z62cgenzd2qualifiedzd2typeszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1546, obj_t BgL_oz00_1547, obj_t BgL_vz00_1548)
	{
		{	/* BackEnd/cvm.sch 367 */
			return
				BGl_cgenzd2qualifiedzd2typeszd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1547), CBOOL(BgL_vz00_1548));
		}

	}



/* cgen-callcc */
	BGL_EXPORTED_DEF bool_t BGl_cgenzd2callcczd2zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_286)
	{
		{	/* BackEnd/cvm.sch 368 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_286)))->BgL_callccz00);
		}

	}



/* &cgen-callcc */
	obj_t BGl_z62cgenzd2callcczb0zzbackend_cvmz00(obj_t BgL_envz00_1549,
		obj_t BgL_oz00_1550)
	{
		{	/* BackEnd/cvm.sch 368 */
			return
				BBOOL(BGl_cgenzd2callcczd2zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1550)));
		}

	}



/* cgen-callcc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2callcczd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_287,
		bool_t BgL_vz00_288)
	{
		{	/* BackEnd/cvm.sch 369 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_287)))->BgL_callccz00) =
				((bool_t) BgL_vz00_288), BUNSPEC);
		}

	}



/* &cgen-callcc-set! */
	obj_t BGl_z62cgenzd2callcczd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1551,
		obj_t BgL_oz00_1552, obj_t BgL_vz00_1553)
	{
		{	/* BackEnd/cvm.sch 369 */
			return
				BGl_cgenzd2callcczd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1552), CBOOL(BgL_vz00_1553));
		}

	}



/* cgen-heap-compatible */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2heapzd2compatiblez00zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_289)
	{
		{	/* BackEnd/cvm.sch 370 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_289)))->BgL_heapzd2compatiblezd2);
		}

	}



/* &cgen-heap-compatible */
	obj_t BGl_z62cgenzd2heapzd2compatiblez62zzbackend_cvmz00(obj_t
		BgL_envz00_1554, obj_t BgL_oz00_1555)
	{
		{	/* BackEnd/cvm.sch 370 */
			return
				BGl_cgenzd2heapzd2compatiblez00zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1555));
		}

	}



/* cgen-heap-compatible-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2heapzd2compatiblezd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_290, obj_t BgL_vz00_291)
	{
		{	/* BackEnd/cvm.sch 371 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_290)))->
					BgL_heapzd2compatiblezd2) = ((obj_t) BgL_vz00_291), BUNSPEC);
		}

	}



/* &cgen-heap-compatible-set! */
	obj_t BGl_z62cgenzd2heapzd2compatiblezd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1556, obj_t BgL_oz00_1557, obj_t BgL_vz00_1558)
	{
		{	/* BackEnd/cvm.sch 371 */
			return
				BGl_cgenzd2heapzd2compatiblezd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1557), BgL_vz00_1558);
		}

	}



/* cgen-heap-suffix */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2heapzd2suffixz00zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_292)
	{
		{	/* BackEnd/cvm.sch 372 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_292)))->BgL_heapzd2suffixzd2);
		}

	}



/* &cgen-heap-suffix */
	obj_t BGl_z62cgenzd2heapzd2suffixz62zzbackend_cvmz00(obj_t BgL_envz00_1559,
		obj_t BgL_oz00_1560)
	{
		{	/* BackEnd/cvm.sch 372 */
			return
				BGl_cgenzd2heapzd2suffixz00zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1560));
		}

	}



/* cgen-heap-suffix-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2heapzd2suffixzd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_293, obj_t BgL_vz00_294)
	{
		{	/* BackEnd/cvm.sch 373 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_293)))->BgL_heapzd2suffixzd2) =
				((obj_t) BgL_vz00_294), BUNSPEC);
		}

	}



/* &cgen-heap-suffix-set! */
	obj_t BGl_z62cgenzd2heapzd2suffixzd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1561, obj_t BgL_oz00_1562, obj_t BgL_vz00_1563)
	{
		{	/* BackEnd/cvm.sch 373 */
			return
				BGl_cgenzd2heapzd2suffixzd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1562), BgL_vz00_1563);
		}

	}



/* cgen-typed */
	BGL_EXPORTED_DEF bool_t BGl_cgenzd2typedzd2zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_295)
	{
		{	/* BackEnd/cvm.sch 374 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_295)))->BgL_typedz00);
		}

	}



/* &cgen-typed */
	obj_t BGl_z62cgenzd2typedzb0zzbackend_cvmz00(obj_t BgL_envz00_1564,
		obj_t BgL_oz00_1565)
	{
		{	/* BackEnd/cvm.sch 374 */
			return
				BBOOL(BGl_cgenzd2typedzd2zzbackend_cvmz00(
					((BgL_cgenz00_bglt) BgL_oz00_1565)));
		}

	}



/* cgen-typed-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2typedzd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_296,
		bool_t BgL_vz00_297)
	{
		{	/* BackEnd/cvm.sch 375 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_296)))->BgL_typedz00) =
				((bool_t) BgL_vz00_297), BUNSPEC);
		}

	}



/* &cgen-typed-set! */
	obj_t BGl_z62cgenzd2typedzd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1566,
		obj_t BgL_oz00_1567, obj_t BgL_vz00_1568)
	{
		{	/* BackEnd/cvm.sch 375 */
			return
				BGl_cgenzd2typedzd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1567), CBOOL(BgL_vz00_1568));
		}

	}



/* cgen-types */
	BGL_EXPORTED_DEF obj_t BGl_cgenzd2typeszd2zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_298)
	{
		{	/* BackEnd/cvm.sch 376 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_298)))->BgL_typesz00);
		}

	}



/* &cgen-types */
	obj_t BGl_z62cgenzd2typeszb0zzbackend_cvmz00(obj_t BgL_envz00_1569,
		obj_t BgL_oz00_1570)
	{
		{	/* BackEnd/cvm.sch 376 */
			return
				BGl_cgenzd2typeszd2zzbackend_cvmz00(((BgL_cgenz00_bglt) BgL_oz00_1570));
		}

	}



/* cgen-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2typeszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_299,
		obj_t BgL_vz00_300)
	{
		{	/* BackEnd/cvm.sch 377 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_299)))->BgL_typesz00) =
				((obj_t) BgL_vz00_300), BUNSPEC);
		}

	}



/* &cgen-types-set! */
	obj_t BGl_z62cgenzd2typeszd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1571,
		obj_t BgL_oz00_1572, obj_t BgL_vz00_1573)
	{
		{	/* BackEnd/cvm.sch 377 */
			return
				BGl_cgenzd2typeszd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1572), BgL_vz00_1573);
		}

	}



/* cgen-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2functionszd2zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_301)
	{
		{	/* BackEnd/cvm.sch 378 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_301)))->BgL_functionsz00);
		}

	}



/* &cgen-functions */
	obj_t BGl_z62cgenzd2functionszb0zzbackend_cvmz00(obj_t BgL_envz00_1574,
		obj_t BgL_oz00_1575)
	{
		{	/* BackEnd/cvm.sch 378 */
			return
				BGl_cgenzd2functionszd2zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1575));
		}

	}



/* cgen-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2functionszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_302, obj_t BgL_vz00_303)
	{
		{	/* BackEnd/cvm.sch 379 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_302)))->BgL_functionsz00) =
				((obj_t) BgL_vz00_303), BUNSPEC);
		}

	}



/* &cgen-functions-set! */
	obj_t BGl_z62cgenzd2functionszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1576, obj_t BgL_oz00_1577, obj_t BgL_vz00_1578)
	{
		{	/* BackEnd/cvm.sch 379 */
			return
				BGl_cgenzd2functionszd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1577), BgL_vz00_1578);
		}

	}



/* cgen-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2variableszd2zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_304)
	{
		{	/* BackEnd/cvm.sch 380 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_304)))->BgL_variablesz00);
		}

	}



/* &cgen-variables */
	obj_t BGl_z62cgenzd2variableszb0zzbackend_cvmz00(obj_t BgL_envz00_1579,
		obj_t BgL_oz00_1580)
	{
		{	/* BackEnd/cvm.sch 380 */
			return
				BGl_cgenzd2variableszd2zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1580));
		}

	}



/* cgen-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2variableszd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_305, obj_t BgL_vz00_306)
	{
		{	/* BackEnd/cvm.sch 381 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_305)))->BgL_variablesz00) =
				((obj_t) BgL_vz00_306), BUNSPEC);
		}

	}



/* &cgen-variables-set! */
	obj_t BGl_z62cgenzd2variableszd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1581, obj_t BgL_oz00_1582, obj_t BgL_vz00_1583)
	{
		{	/* BackEnd/cvm.sch 381 */
			return
				BGl_cgenzd2variableszd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1582), BgL_vz00_1583);
		}

	}



/* cgen-extern-types */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2externzd2typesz00zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_307)
	{
		{	/* BackEnd/cvm.sch 382 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_307)))->BgL_externzd2typeszd2);
		}

	}



/* &cgen-extern-types */
	obj_t BGl_z62cgenzd2externzd2typesz62zzbackend_cvmz00(obj_t BgL_envz00_1584,
		obj_t BgL_oz00_1585)
	{
		{	/* BackEnd/cvm.sch 382 */
			return
				BGl_cgenzd2externzd2typesz00zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1585));
		}

	}



/* cgen-extern-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2externzd2typeszd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_308, obj_t BgL_vz00_309)
	{
		{	/* BackEnd/cvm.sch 383 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_308)))->BgL_externzd2typeszd2) =
				((obj_t) BgL_vz00_309), BUNSPEC);
		}

	}



/* &cgen-extern-types-set! */
	obj_t BGl_z62cgenzd2externzd2typeszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1586, obj_t BgL_oz00_1587, obj_t BgL_vz00_1588)
	{
		{	/* BackEnd/cvm.sch 383 */
			return
				BGl_cgenzd2externzd2typeszd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1587), BgL_vz00_1588);
		}

	}



/* cgen-extern-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2externzd2functionsz00zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_310)
	{
		{	/* BackEnd/cvm.sch 384 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_310)))->BgL_externzd2functionszd2);
		}

	}



/* &cgen-extern-functions */
	obj_t BGl_z62cgenzd2externzd2functionsz62zzbackend_cvmz00(obj_t
		BgL_envz00_1589, obj_t BgL_oz00_1590)
	{
		{	/* BackEnd/cvm.sch 384 */
			return
				BGl_cgenzd2externzd2functionsz00zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1590));
		}

	}



/* cgen-extern-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2externzd2functionszd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_311, obj_t BgL_vz00_312)
	{
		{	/* BackEnd/cvm.sch 385 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_311)))->
					BgL_externzd2functionszd2) = ((obj_t) BgL_vz00_312), BUNSPEC);
		}

	}



/* &cgen-extern-functions-set! */
	obj_t BGl_z62cgenzd2externzd2functionszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1591, obj_t BgL_oz00_1592, obj_t BgL_vz00_1593)
	{
		{	/* BackEnd/cvm.sch 385 */
			return
				BGl_cgenzd2externzd2functionszd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1592), BgL_vz00_1593);
		}

	}



/* cgen-extern-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2externzd2variablesz00zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_313)
	{
		{	/* BackEnd/cvm.sch 386 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_313)))->BgL_externzd2variableszd2);
		}

	}



/* &cgen-extern-variables */
	obj_t BGl_z62cgenzd2externzd2variablesz62zzbackend_cvmz00(obj_t
		BgL_envz00_1594, obj_t BgL_oz00_1595)
	{
		{	/* BackEnd/cvm.sch 386 */
			return
				BGl_cgenzd2externzd2variablesz00zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1595));
		}

	}



/* cgen-extern-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2externzd2variableszd2setz12zc0zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_314, obj_t BgL_vz00_315)
	{
		{	/* BackEnd/cvm.sch 387 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_314)))->
					BgL_externzd2variableszd2) = ((obj_t) BgL_vz00_315), BUNSPEC);
		}

	}



/* &cgen-extern-variables-set! */
	obj_t BGl_z62cgenzd2externzd2variableszd2setz12za2zzbackend_cvmz00(obj_t
		BgL_envz00_1596, obj_t BgL_oz00_1597, obj_t BgL_vz00_1598)
	{
		{	/* BackEnd/cvm.sch 387 */
			return
				BGl_cgenzd2externzd2variableszd2setz12zc0zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1597), BgL_vz00_1598);
		}

	}



/* cgen-name */
	BGL_EXPORTED_DEF obj_t BGl_cgenzd2namezd2zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_316)
	{
		{	/* BackEnd/cvm.sch 388 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_316)))->BgL_namez00);
		}

	}



/* &cgen-name */
	obj_t BGl_z62cgenzd2namezb0zzbackend_cvmz00(obj_t BgL_envz00_1599,
		obj_t BgL_oz00_1600)
	{
		{	/* BackEnd/cvm.sch 388 */
			return
				BGl_cgenzd2namezd2zzbackend_cvmz00(((BgL_cgenz00_bglt) BgL_oz00_1600));
		}

	}



/* cgen-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2namezd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_317,
		obj_t BgL_vz00_318)
	{
		{	/* BackEnd/cvm.sch 389 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_317)))->BgL_namez00) =
				((obj_t) BgL_vz00_318), BUNSPEC);
		}

	}



/* &cgen-name-set! */
	obj_t BGl_z62cgenzd2namezd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1601,
		obj_t BgL_oz00_1602, obj_t BgL_vz00_1603)
	{
		{	/* BackEnd/cvm.sch 389 */
			return
				BGl_cgenzd2namezd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1602), BgL_vz00_1603);
		}

	}



/* cgen-srfi0 */
	BGL_EXPORTED_DEF obj_t BGl_cgenzd2srfi0zd2zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_319)
	{
		{	/* BackEnd/cvm.sch 390 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_319)))->BgL_srfi0z00);
		}

	}



/* &cgen-srfi0 */
	obj_t BGl_z62cgenzd2srfi0zb0zzbackend_cvmz00(obj_t BgL_envz00_1604,
		obj_t BgL_oz00_1605)
	{
		{	/* BackEnd/cvm.sch 390 */
			return
				BGl_cgenzd2srfi0zd2zzbackend_cvmz00(((BgL_cgenz00_bglt) BgL_oz00_1605));
		}

	}



/* cgen-srfi0-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2srfi0zd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt BgL_oz00_320,
		obj_t BgL_vz00_321)
	{
		{	/* BackEnd/cvm.sch 391 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_320)))->BgL_srfi0z00) =
				((obj_t) BgL_vz00_321), BUNSPEC);
		}

	}



/* &cgen-srfi0-set! */
	obj_t BGl_z62cgenzd2srfi0zd2setz12z70zzbackend_cvmz00(obj_t BgL_envz00_1606,
		obj_t BgL_oz00_1607, obj_t BgL_vz00_1608)
	{
		{	/* BackEnd/cvm.sch 391 */
			return
				BGl_cgenzd2srfi0zd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1607), BgL_vz00_1608);
		}

	}



/* cgen-language */
	BGL_EXPORTED_DEF obj_t BGl_cgenzd2languagezd2zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_322)
	{
		{	/* BackEnd/cvm.sch 392 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_322)))->BgL_languagez00);
		}

	}



/* &cgen-language */
	obj_t BGl_z62cgenzd2languagezb0zzbackend_cvmz00(obj_t BgL_envz00_1609,
		obj_t BgL_oz00_1610)
	{
		{	/* BackEnd/cvm.sch 392 */
			return
				BGl_cgenzd2languagezd2zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1610));
		}

	}



/* cgen-language-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgenzd2languagezd2setz12z12zzbackend_cvmz00(BgL_cgenz00_bglt
		BgL_oz00_323, obj_t BgL_vz00_324)
	{
		{	/* BackEnd/cvm.sch 393 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_323)))->BgL_languagez00) =
				((obj_t) BgL_vz00_324), BUNSPEC);
		}

	}



/* &cgen-language-set! */
	obj_t BGl_z62cgenzd2languagezd2setz12z70zzbackend_cvmz00(obj_t
		BgL_envz00_1611, obj_t BgL_oz00_1612, obj_t BgL_vz00_1613)
	{
		{	/* BackEnd/cvm.sch 393 */
			return
				BGl_cgenzd2languagezd2setz12z12zzbackend_cvmz00(
				((BgL_cgenz00_bglt) BgL_oz00_1612), BgL_vz00_1613);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.scm 15 */
			{	/* BackEnd/cvm.scm 19 */
				obj_t BgL_arg1326z00_673;
				obj_t BgL_arg1327z00_674;

				{	/* BackEnd/cvm.scm 19 */
					obj_t BgL_v1310z00_680;

					BgL_v1310z00_680 = create_vector(0L);
					BgL_arg1326z00_673 = BgL_v1310z00_680;
				}
				{	/* BackEnd/cvm.scm 19 */
					obj_t BgL_v1311z00_681;

					BgL_v1311z00_681 = create_vector(0L);
					BgL_arg1327z00_674 = BgL_v1311z00_681;
				}
				BGl_cvmz00zzbackend_cvmz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(0),
					CNST_TABLE_REF(1), BGl_backendz00zzbackend_backendz00, 50908L, BFALSE,
					BGl_proc1376z00zzbackend_cvmz00,
					BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00,
					BGl_proc1375z00zzbackend_cvmz00, BFALSE, BgL_arg1326z00_673,
					BgL_arg1327z00_674);
			}
			{	/* BackEnd/cvm.scm 20 */
				obj_t BgL_arg1335z00_687;
				obj_t BgL_arg1339z00_688;

				{	/* BackEnd/cvm.scm 20 */
					obj_t BgL_v1312z00_731;

					BgL_v1312z00_731 = create_vector(0L);
					BgL_arg1335z00_687 = BgL_v1312z00_731;
				}
				{	/* BackEnd/cvm.scm 20 */
					obj_t BgL_v1313z00_732;

					BgL_v1313z00_732 = create_vector(0L);
					BgL_arg1339z00_688 = BgL_v1313z00_732;
				}
				BGl_sawcz00zzbackend_cvmz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(2),
					CNST_TABLE_REF(1), BGl_cvmz00zzbackend_cvmz00, 41787L,
					BGl_proc1379z00zzbackend_cvmz00, BGl_proc1378z00zzbackend_cvmz00,
					BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00,
					BGl_proc1377z00zzbackend_cvmz00, BFALSE, BgL_arg1335z00_687,
					BgL_arg1339z00_688);
			}
			{	/* BackEnd/cvm.scm 21 */
				obj_t BgL_arg1352z00_739;
				obj_t BgL_arg1361z00_740;

				{	/* BackEnd/cvm.scm 21 */
					obj_t BgL_v1314z00_783;

					BgL_v1314z00_783 = create_vector(0L);
					BgL_arg1352z00_739 = BgL_v1314z00_783;
				}
				{	/* BackEnd/cvm.scm 21 */
					obj_t BgL_v1315z00_784;

					BgL_v1315z00_784 = create_vector(0L);
					BgL_arg1361z00_740 = BgL_v1315z00_784;
				}
				return (BGl_cgenz00zzbackend_cvmz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(3),
						CNST_TABLE_REF(1), BGl_cvmz00zzbackend_cvmz00, 14760L,
						BGl_proc1382z00zzbackend_cvmz00, BGl_proc1381z00zzbackend_cvmz00,
						BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00,
						BGl_proc1380z00zzbackend_cvmz00, BFALSE, BgL_arg1352z00_739,
						BgL_arg1361z00_740), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1368> */
	obj_t BGl_z62zc3z04anonymousza31368ze3ze5zzbackend_cvmz00(obj_t
		BgL_envz00_1622, obj_t BgL_new1122z00_1623)
	{
		{	/* BackEnd/cvm.scm 21 */
			{
				BgL_cgenz00_bglt BgL_auxz00_2933;

				((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt)
									((BgL_cgenz00_bglt) BgL_new1122z00_1623))))->
						BgL_languagez00) = ((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_namez00) =
					((obj_t) BGl_string1383z00zzbackend_cvmz00), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_externzd2variableszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_externzd2functionszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_externzd2typeszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_variablesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_functionsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_typesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_typedz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_heapzd2suffixzd2) =
					((obj_t) BGl_string1383z00zzbackend_cvmz00), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_heapzd2compatiblezd2) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_callccz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_qualifiedzd2typeszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_effectzb2zb2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_removezd2emptyzd2letz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_foreignzd2closurezd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_typedzd2eqzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_tracezd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->
						BgL_foreignzd2clausezd2supportz00) = ((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_debugzd2supportzd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_pragmazd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_requirezd2tailczd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_registersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_pregistersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_boundzd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_typezd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_typedzd2funcallzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->BgL_strictzd2typezd2castz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cgenz00_bglt)
										BgL_new1122z00_1623))))->
						BgL_stringzd2literalzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_2933 = ((BgL_cgenz00_bglt) BgL_new1122z00_1623);
				return ((obj_t) BgL_auxz00_2933);
			}
		}

	}



/* &lambda1365 */
	BgL_cgenz00_bglt BGl_z62lambda1365z62zzbackend_cvmz00(obj_t BgL_envz00_1624)
	{
		{	/* BackEnd/cvm.scm 21 */
			{	/* BackEnd/cvm.scm 21 */
				BgL_cgenz00_bglt BgL_new1120z00_1764;

				BgL_new1120z00_1764 =
					((BgL_cgenz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cgenz00_bgl))));
				{	/* BackEnd/cvm.scm 21 */
					long BgL_arg1367z00_1765;

					BgL_arg1367z00_1765 = BGL_CLASS_NUM(BGl_cgenz00zzbackend_cvmz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1120z00_1764), BgL_arg1367z00_1765);
				}
				return BgL_new1120z00_1764;
			}
		}

	}



/* &lambda1362 */
	BgL_cgenz00_bglt BGl_z62lambda1362z62zzbackend_cvmz00(obj_t BgL_envz00_1625,
		obj_t BgL_language1088z00_1626, obj_t BgL_srfi01089z00_1627,
		obj_t BgL_name1090z00_1628, obj_t BgL_externzd2variables1091zd2_1629,
		obj_t BgL_externzd2functions1092zd2_1630,
		obj_t BgL_externzd2types1093zd2_1631, obj_t BgL_variables1094z00_1632,
		obj_t BgL_functions1095z00_1633, obj_t BgL_types1096z00_1634,
		obj_t BgL_typed1097z00_1635, obj_t BgL_heapzd2suffix1098zd2_1636,
		obj_t BgL_heapzd2compatible1099zd2_1637, obj_t BgL_callcc1100z00_1638,
		obj_t BgL_qualifiedzd2types1101zd2_1639, obj_t BgL_effectzb21102zb2_1640,
		obj_t BgL_removezd2emptyzd2let1103z00_1641,
		obj_t BgL_foreignzd2closure1104zd2_1642, obj_t BgL_typedzd2eq1105zd2_1643,
		obj_t BgL_tracezd2support1106zd2_1644,
		obj_t BgL_foreignzd2clausezd2suppo1107z00_1645,
		obj_t BgL_debugzd2support1108zd2_1646,
		obj_t BgL_pragmazd2support1109zd2_1647,
		obj_t BgL_tvectorzd2descrzd2suppor1110z00_1648,
		obj_t BgL_requirezd2tailc1111zd2_1649, obj_t BgL_registers1112z00_1650,
		obj_t BgL_pregisters1113z00_1651, obj_t BgL_boundzd2check1114zd2_1652,
		obj_t BgL_typezd2check1115zd2_1653, obj_t BgL_typedzd2funcall1116zd2_1654,
		obj_t BgL_strictzd2typezd2cast1117z00_1655,
		obj_t BgL_forcezd2registerzd2gczd2ro1118zd2_1656,
		obj_t BgL_stringzd2literalzd2suppo1119z00_1657)
	{
		{	/* BackEnd/cvm.scm 21 */
			{	/* BackEnd/cvm.scm 21 */
				bool_t BgL_typed1097z00_1769;
				bool_t BgL_callcc1100z00_1772;
				bool_t BgL_qualifiedzd2types1101zd2_1773;
				bool_t BgL_effectzb21102zb2_1774;
				bool_t BgL_removezd2emptyzd2let1103z00_1775;
				bool_t BgL_foreignzd2closure1104zd2_1776;
				bool_t BgL_typedzd2eq1105zd2_1777;
				bool_t BgL_tracezd2support1106zd2_1778;
				bool_t BgL_pragmazd2support1109zd2_1781;
				bool_t BgL_tvectorzd2descrzd2suppor1110z00_1782;
				bool_t BgL_requirezd2tailc1111zd2_1783;
				bool_t BgL_boundzd2check1114zd2_1786;
				bool_t BgL_typezd2check1115zd2_1787;
				bool_t BgL_typedzd2funcall1116zd2_1788;
				bool_t BgL_strictzd2typezd2cast1117z00_1789;
				bool_t BgL_forcezd2registerzd2gczd2ro1118zd2_1790;
				bool_t BgL_stringzd2literalzd2suppo1119z00_1791;

				BgL_typed1097z00_1769 = CBOOL(BgL_typed1097z00_1635);
				BgL_callcc1100z00_1772 = CBOOL(BgL_callcc1100z00_1638);
				BgL_qualifiedzd2types1101zd2_1773 =
					CBOOL(BgL_qualifiedzd2types1101zd2_1639);
				BgL_effectzb21102zb2_1774 = CBOOL(BgL_effectzb21102zb2_1640);
				BgL_removezd2emptyzd2let1103z00_1775 =
					CBOOL(BgL_removezd2emptyzd2let1103z00_1641);
				BgL_foreignzd2closure1104zd2_1776 =
					CBOOL(BgL_foreignzd2closure1104zd2_1642);
				BgL_typedzd2eq1105zd2_1777 = CBOOL(BgL_typedzd2eq1105zd2_1643);
				BgL_tracezd2support1106zd2_1778 =
					CBOOL(BgL_tracezd2support1106zd2_1644);
				BgL_pragmazd2support1109zd2_1781 =
					CBOOL(BgL_pragmazd2support1109zd2_1647);
				BgL_tvectorzd2descrzd2suppor1110z00_1782 =
					CBOOL(BgL_tvectorzd2descrzd2suppor1110z00_1648);
				BgL_requirezd2tailc1111zd2_1783 =
					CBOOL(BgL_requirezd2tailc1111zd2_1649);
				BgL_boundzd2check1114zd2_1786 = CBOOL(BgL_boundzd2check1114zd2_1652);
				BgL_typezd2check1115zd2_1787 = CBOOL(BgL_typezd2check1115zd2_1653);
				BgL_typedzd2funcall1116zd2_1788 =
					CBOOL(BgL_typedzd2funcall1116zd2_1654);
				BgL_strictzd2typezd2cast1117z00_1789 =
					CBOOL(BgL_strictzd2typezd2cast1117z00_1655);
				BgL_forcezd2registerzd2gczd2ro1118zd2_1790 =
					CBOOL(BgL_forcezd2registerzd2gczd2ro1118zd2_1656);
				BgL_stringzd2literalzd2suppo1119z00_1791 =
					CBOOL(BgL_stringzd2literalzd2suppo1119z00_1657);
				{	/* BackEnd/cvm.scm 21 */
					BgL_cgenz00_bglt BgL_new1307z00_1792;

					{	/* BackEnd/cvm.scm 21 */
						BgL_cgenz00_bglt BgL_new1305z00_1793;

						BgL_new1305z00_1793 =
							((BgL_cgenz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_cgenz00_bgl))));
						{	/* BackEnd/cvm.scm 21 */
							long BgL_arg1364z00_1794;

							BgL_arg1364z00_1794 = BGL_CLASS_NUM(BGl_cgenz00zzbackend_cvmz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1305z00_1793),
								BgL_arg1364z00_1794);
						}
						BgL_new1307z00_1792 = BgL_new1305z00_1793;
					}
					((((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_new1307z00_1792)))->
							BgL_languagez00) =
						((obj_t) ((obj_t) BgL_language1088z00_1626)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_srfi0z00) =
						((obj_t) ((obj_t) BgL_srfi01089z00_1627)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_namez00) =
						((obj_t) ((obj_t) BgL_name1090z00_1628)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_externzd2variableszd2) =
						((obj_t) BgL_externzd2variables1091zd2_1629), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_externzd2functionszd2) =
						((obj_t) BgL_externzd2functions1092zd2_1630), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_externzd2typeszd2) =
						((obj_t) BgL_externzd2types1093zd2_1631), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_variablesz00) =
						((obj_t) BgL_variables1094z00_1632), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_functionsz00) =
						((obj_t) BgL_functions1095z00_1633), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_typesz00) =
						((obj_t) BgL_types1096z00_1634), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_typedz00) =
						((bool_t) BgL_typed1097z00_1769), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_heapzd2suffixzd2) =
						((obj_t) ((obj_t) BgL_heapzd2suffix1098zd2_1636)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_heapzd2compatiblezd2) =
						((obj_t) ((obj_t) BgL_heapzd2compatible1099zd2_1637)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_callccz00) =
						((bool_t) BgL_callcc1100z00_1772), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_qualifiedzd2typeszd2) =
						((bool_t) BgL_qualifiedzd2types1101zd2_1773), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_effectzb2zb2) =
						((bool_t) BgL_effectzb21102zb2_1774), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_removezd2emptyzd2letz00) =
						((bool_t) BgL_removezd2emptyzd2let1103z00_1775), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_foreignzd2closurezd2) =
						((bool_t) BgL_foreignzd2closure1104zd2_1776), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_typedzd2eqzd2) =
						((bool_t) BgL_typedzd2eq1105zd2_1777), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_tracezd2supportzd2) =
						((bool_t) BgL_tracezd2support1106zd2_1778), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_foreignzd2clausezd2supportz00) =
						((obj_t) ((obj_t) BgL_foreignzd2clausezd2suppo1107z00_1645)),
						BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_debugzd2supportzd2) =
						((obj_t) ((obj_t) BgL_debugzd2support1108zd2_1646)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_pragmazd2supportzd2) =
						((bool_t) BgL_pragmazd2support1109zd2_1781), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_tvectorzd2descrzd2supportz00) =
						((bool_t) BgL_tvectorzd2descrzd2suppor1110z00_1782), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_requirezd2tailczd2) =
						((bool_t) BgL_requirezd2tailc1111zd2_1783), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_registersz00) =
						((obj_t) ((obj_t) BgL_registers1112z00_1650)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_pregistersz00) =
						((obj_t) ((obj_t) BgL_pregisters1113z00_1651)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_boundzd2checkzd2) =
						((bool_t) BgL_boundzd2check1114zd2_1786), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_typezd2checkzd2) =
						((bool_t) BgL_typezd2check1115zd2_1787), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_typedzd2funcallzd2) =
						((bool_t) BgL_typedzd2funcall1116zd2_1788), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_strictzd2typezd2castz00) =
						((bool_t) BgL_strictzd2typezd2cast1117z00_1789), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->
							BgL_forcezd2registerzd2gczd2rootszd2) =
						((bool_t) BgL_forcezd2registerzd2gczd2ro1118zd2_1790), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1307z00_1792)))->BgL_stringzd2literalzd2supportz00) =
						((bool_t) BgL_stringzd2literalzd2suppo1119z00_1791), BUNSPEC);
					{	/* BackEnd/cvm.scm 21 */
						obj_t BgL_fun1363z00_1795;

						BgL_fun1363z00_1795 =
							BGl_classzd2constructorzd2zz__objectz00
							(BGl_cgenz00zzbackend_cvmz00);
						BGL_PROCEDURE_CALL1(BgL_fun1363z00_1795,
							((obj_t) BgL_new1307z00_1792));
					}
					return BgL_new1307z00_1792;
				}
			}
		}

	}



/* &<@anonymous:1347> */
	obj_t BGl_z62zc3z04anonymousza31347ze3ze5zzbackend_cvmz00(obj_t
		BgL_envz00_1658, obj_t BgL_new1086z00_1659)
	{
		{	/* BackEnd/cvm.scm 20 */
			{
				BgL_sawcz00_bglt BgL_auxz00_3139;

				((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt)
									((BgL_sawcz00_bglt) BgL_new1086z00_1659))))->
						BgL_languagez00) = ((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_namez00) =
					((obj_t) BGl_string1383z00zzbackend_cvmz00), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_externzd2variableszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_externzd2functionszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_externzd2typeszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_variablesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_functionsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_typesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_typedz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_heapzd2suffixzd2) =
					((obj_t) BGl_string1383z00zzbackend_cvmz00), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_heapzd2compatiblezd2) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_callccz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_qualifiedzd2typeszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_effectzb2zb2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_removezd2emptyzd2letz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_foreignzd2closurezd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_typedzd2eqzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_tracezd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->
						BgL_foreignzd2clausezd2supportz00) = ((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_debugzd2supportzd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_pragmazd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_requirezd2tailczd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_registersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_pregistersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_boundzd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_typezd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_typedzd2funcallzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->BgL_strictzd2typezd2castz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_sawcz00_bglt)
										BgL_new1086z00_1659))))->
						BgL_stringzd2literalzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_3139 = ((BgL_sawcz00_bglt) BgL_new1086z00_1659);
				return ((obj_t) BgL_auxz00_3139);
			}
		}

	}



/* &lambda1343 */
	BgL_sawcz00_bglt BGl_z62lambda1343z62zzbackend_cvmz00(obj_t BgL_envz00_1660)
	{
		{	/* BackEnd/cvm.scm 20 */
			{	/* BackEnd/cvm.scm 20 */
				BgL_sawcz00_bglt BgL_new1085z00_1797;

				BgL_new1085z00_1797 =
					((BgL_sawcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sawcz00_bgl))));
				{	/* BackEnd/cvm.scm 20 */
					long BgL_arg1346z00_1798;

					BgL_arg1346z00_1798 = BGL_CLASS_NUM(BGl_sawcz00zzbackend_cvmz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1085z00_1797), BgL_arg1346z00_1798);
				}
				return BgL_new1085z00_1797;
			}
		}

	}



/* &lambda1340 */
	BgL_sawcz00_bglt BGl_z62lambda1340z62zzbackend_cvmz00(obj_t BgL_envz00_1661,
		obj_t BgL_language1053z00_1662, obj_t BgL_srfi01054z00_1663,
		obj_t BgL_name1055z00_1664, obj_t BgL_externzd2variables1056zd2_1665,
		obj_t BgL_externzd2functions1057zd2_1666,
		obj_t BgL_externzd2types1058zd2_1667, obj_t BgL_variables1059z00_1668,
		obj_t BgL_functions1060z00_1669, obj_t BgL_types1061z00_1670,
		obj_t BgL_typed1062z00_1671, obj_t BgL_heapzd2suffix1063zd2_1672,
		obj_t BgL_heapzd2compatible1064zd2_1673, obj_t BgL_callcc1065z00_1674,
		obj_t BgL_qualifiedzd2types1066zd2_1675, obj_t BgL_effectzb21067zb2_1676,
		obj_t BgL_removezd2emptyzd2let1068z00_1677,
		obj_t BgL_foreignzd2closure1069zd2_1678, obj_t BgL_typedzd2eq1070zd2_1679,
		obj_t BgL_tracezd2support1071zd2_1680,
		obj_t BgL_foreignzd2clausezd2suppo1072z00_1681,
		obj_t BgL_debugzd2support1073zd2_1682,
		obj_t BgL_pragmazd2support1074zd2_1683,
		obj_t BgL_tvectorzd2descrzd2suppor1075z00_1684,
		obj_t BgL_requirezd2tailc1076zd2_1685, obj_t BgL_registers1077z00_1686,
		obj_t BgL_pregisters1078z00_1687, obj_t BgL_boundzd2check1079zd2_1688,
		obj_t BgL_typezd2check1080zd2_1689, obj_t BgL_typedzd2funcall1081zd2_1690,
		obj_t BgL_strictzd2typezd2cast1082z00_1691,
		obj_t BgL_forcezd2registerzd2gczd2ro1083zd2_1692,
		obj_t BgL_stringzd2literalzd2suppo1084z00_1693)
	{
		{	/* BackEnd/cvm.scm 20 */
			{	/* BackEnd/cvm.scm 20 */
				bool_t BgL_typed1062z00_1802;
				bool_t BgL_callcc1065z00_1805;
				bool_t BgL_qualifiedzd2types1066zd2_1806;
				bool_t BgL_effectzb21067zb2_1807;
				bool_t BgL_removezd2emptyzd2let1068z00_1808;
				bool_t BgL_foreignzd2closure1069zd2_1809;
				bool_t BgL_typedzd2eq1070zd2_1810;
				bool_t BgL_tracezd2support1071zd2_1811;
				bool_t BgL_pragmazd2support1074zd2_1814;
				bool_t BgL_tvectorzd2descrzd2suppor1075z00_1815;
				bool_t BgL_requirezd2tailc1076zd2_1816;
				bool_t BgL_boundzd2check1079zd2_1819;
				bool_t BgL_typezd2check1080zd2_1820;
				bool_t BgL_typedzd2funcall1081zd2_1821;
				bool_t BgL_strictzd2typezd2cast1082z00_1822;
				bool_t BgL_forcezd2registerzd2gczd2ro1083zd2_1823;
				bool_t BgL_stringzd2literalzd2suppo1084z00_1824;

				BgL_typed1062z00_1802 = CBOOL(BgL_typed1062z00_1671);
				BgL_callcc1065z00_1805 = CBOOL(BgL_callcc1065z00_1674);
				BgL_qualifiedzd2types1066zd2_1806 =
					CBOOL(BgL_qualifiedzd2types1066zd2_1675);
				BgL_effectzb21067zb2_1807 = CBOOL(BgL_effectzb21067zb2_1676);
				BgL_removezd2emptyzd2let1068z00_1808 =
					CBOOL(BgL_removezd2emptyzd2let1068z00_1677);
				BgL_foreignzd2closure1069zd2_1809 =
					CBOOL(BgL_foreignzd2closure1069zd2_1678);
				BgL_typedzd2eq1070zd2_1810 = CBOOL(BgL_typedzd2eq1070zd2_1679);
				BgL_tracezd2support1071zd2_1811 =
					CBOOL(BgL_tracezd2support1071zd2_1680);
				BgL_pragmazd2support1074zd2_1814 =
					CBOOL(BgL_pragmazd2support1074zd2_1683);
				BgL_tvectorzd2descrzd2suppor1075z00_1815 =
					CBOOL(BgL_tvectorzd2descrzd2suppor1075z00_1684);
				BgL_requirezd2tailc1076zd2_1816 =
					CBOOL(BgL_requirezd2tailc1076zd2_1685);
				BgL_boundzd2check1079zd2_1819 = CBOOL(BgL_boundzd2check1079zd2_1688);
				BgL_typezd2check1080zd2_1820 = CBOOL(BgL_typezd2check1080zd2_1689);
				BgL_typedzd2funcall1081zd2_1821 =
					CBOOL(BgL_typedzd2funcall1081zd2_1690);
				BgL_strictzd2typezd2cast1082z00_1822 =
					CBOOL(BgL_strictzd2typezd2cast1082z00_1691);
				BgL_forcezd2registerzd2gczd2ro1083zd2_1823 =
					CBOOL(BgL_forcezd2registerzd2gczd2ro1083zd2_1692);
				BgL_stringzd2literalzd2suppo1084z00_1824 =
					CBOOL(BgL_stringzd2literalzd2suppo1084z00_1693);
				{	/* BackEnd/cvm.scm 20 */
					BgL_sawcz00_bglt BgL_new1304z00_1825;

					{	/* BackEnd/cvm.scm 20 */
						BgL_sawcz00_bglt BgL_new1303z00_1826;

						BgL_new1303z00_1826 =
							((BgL_sawcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sawcz00_bgl))));
						{	/* BackEnd/cvm.scm 20 */
							long BgL_arg1342z00_1827;

							BgL_arg1342z00_1827 = BGL_CLASS_NUM(BGl_sawcz00zzbackend_cvmz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1303z00_1826),
								BgL_arg1342z00_1827);
						}
						BgL_new1304z00_1825 = BgL_new1303z00_1826;
					}
					((((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_new1304z00_1825)))->
							BgL_languagez00) =
						((obj_t) ((obj_t) BgL_language1053z00_1662)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_srfi0z00) =
						((obj_t) ((obj_t) BgL_srfi01054z00_1663)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_namez00) =
						((obj_t) ((obj_t) BgL_name1055z00_1664)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_externzd2variableszd2) =
						((obj_t) BgL_externzd2variables1056zd2_1665), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_externzd2functionszd2) =
						((obj_t) BgL_externzd2functions1057zd2_1666), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_externzd2typeszd2) =
						((obj_t) BgL_externzd2types1058zd2_1667), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_variablesz00) =
						((obj_t) BgL_variables1059z00_1668), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_functionsz00) =
						((obj_t) BgL_functions1060z00_1669), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_typesz00) =
						((obj_t) BgL_types1061z00_1670), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_typedz00) =
						((bool_t) BgL_typed1062z00_1802), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_heapzd2suffixzd2) =
						((obj_t) ((obj_t) BgL_heapzd2suffix1063zd2_1672)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_heapzd2compatiblezd2) =
						((obj_t) ((obj_t) BgL_heapzd2compatible1064zd2_1673)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_callccz00) =
						((bool_t) BgL_callcc1065z00_1805), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_qualifiedzd2typeszd2) =
						((bool_t) BgL_qualifiedzd2types1066zd2_1806), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_effectzb2zb2) =
						((bool_t) BgL_effectzb21067zb2_1807), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_removezd2emptyzd2letz00) =
						((bool_t) BgL_removezd2emptyzd2let1068z00_1808), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_foreignzd2closurezd2) =
						((bool_t) BgL_foreignzd2closure1069zd2_1809), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_typedzd2eqzd2) =
						((bool_t) BgL_typedzd2eq1070zd2_1810), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_tracezd2supportzd2) =
						((bool_t) BgL_tracezd2support1071zd2_1811), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_foreignzd2clausezd2supportz00) =
						((obj_t) ((obj_t) BgL_foreignzd2clausezd2suppo1072z00_1681)),
						BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_debugzd2supportzd2) =
						((obj_t) ((obj_t) BgL_debugzd2support1073zd2_1682)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_pragmazd2supportzd2) =
						((bool_t) BgL_pragmazd2support1074zd2_1814), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_tvectorzd2descrzd2supportz00) =
						((bool_t) BgL_tvectorzd2descrzd2suppor1075z00_1815), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_requirezd2tailczd2) =
						((bool_t) BgL_requirezd2tailc1076zd2_1816), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_registersz00) =
						((obj_t) ((obj_t) BgL_registers1077z00_1686)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_pregistersz00) =
						((obj_t) ((obj_t) BgL_pregisters1078z00_1687)), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_boundzd2checkzd2) =
						((bool_t) BgL_boundzd2check1079zd2_1819), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_typezd2checkzd2) =
						((bool_t) BgL_typezd2check1080zd2_1820), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_typedzd2funcallzd2) =
						((bool_t) BgL_typedzd2funcall1081zd2_1821), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_strictzd2typezd2castz00) =
						((bool_t) BgL_strictzd2typezd2cast1082z00_1822), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->
							BgL_forcezd2registerzd2gczd2rootszd2) =
						((bool_t) BgL_forcezd2registerzd2gczd2ro1083zd2_1823), BUNSPEC);
					((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
										BgL_new1304z00_1825)))->BgL_stringzd2literalzd2supportz00) =
						((bool_t) BgL_stringzd2literalzd2suppo1084z00_1824), BUNSPEC);
					{	/* BackEnd/cvm.scm 20 */
						obj_t BgL_fun1341z00_1828;

						BgL_fun1341z00_1828 =
							BGl_classzd2constructorzd2zz__objectz00
							(BGl_sawcz00zzbackend_cvmz00);
						BGL_PROCEDURE_CALL1(BgL_fun1341z00_1828,
							((obj_t) BgL_new1304z00_1825));
					}
					return BgL_new1304z00_1825;
				}
			}
		}

	}



/* &<@anonymous:1330> */
	obj_t BGl_z62zc3z04anonymousza31330ze3ze5zzbackend_cvmz00(obj_t
		BgL_envz00_1694, obj_t BgL_new1051z00_1695)
	{
		{	/* BackEnd/cvm.scm 19 */
			{
				BgL_cvmz00_bglt BgL_auxz00_3345;

				((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt)
									((BgL_cvmz00_bglt) BgL_new1051z00_1695))))->BgL_languagez00) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_namez00) =
					((obj_t) BGl_string1383z00zzbackend_cvmz00), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_externzd2variableszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_externzd2functionszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_externzd2typeszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_variablesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_functionsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_typesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_typedz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_heapzd2suffixzd2) =
					((obj_t) BGl_string1383z00zzbackend_cvmz00), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_heapzd2compatiblezd2) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_callccz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_qualifiedzd2typeszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_effectzb2zb2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_removezd2emptyzd2letz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_foreignzd2closurezd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_typedzd2eqzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_tracezd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->
						BgL_foreignzd2clausezd2supportz00) = ((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_debugzd2supportzd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_pragmazd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_requirezd2tailczd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_registersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_pregistersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_boundzd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_typezd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_typedzd2funcallzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->BgL_strictzd2typezd2castz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_cvmz00_bglt)
										BgL_new1051z00_1695))))->
						BgL_stringzd2literalzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_3345 = ((BgL_cvmz00_bglt) BgL_new1051z00_1695);
				return ((obj_t) BgL_auxz00_3345);
			}
		}

	}



/* &lambda1328 */
	BgL_cvmz00_bglt BGl_z62lambda1328z62zzbackend_cvmz00(obj_t BgL_envz00_1696)
	{
		{	/* BackEnd/cvm.scm 19 */
			{	/* BackEnd/cvm.scm 19 */
				BgL_cvmz00_bglt BgL_new1050z00_1830;

				BgL_new1050z00_1830 =
					((BgL_cvmz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct BgL_cvmz00_bgl))));
				{	/* BackEnd/cvm.scm 19 */
					long BgL_arg1329z00_1831;

					BgL_arg1329z00_1831 = BGL_CLASS_NUM(BGl_cvmz00zzbackend_cvmz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1050z00_1830), BgL_arg1329z00_1831);
				}
				return BgL_new1050z00_1830;
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_cvmz00(void)
	{
		{	/* BackEnd/cvm.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1384z00zzbackend_cvmz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1384z00zzbackend_cvmz00));
		}

	}

#ifdef __cplusplus
}
#endif
