/*===========================================================================*/
/*   (BackEnd/cplib.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/cplib.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_CPLIB_TYPE_DEFINITIONS
#define BGL_BACKEND_CPLIB_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;

	typedef struct BgL_wclassz00_bgl
	{
		obj_t BgL_itszd2classzd2;
	}                *BgL_wclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;


#endif													// BGL_BACKEND_CPLIB_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t);
	static obj_t BGl_z62getzd2declaredzd2classesz62zzbackend_cplibz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbackend_cplibz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_qualifiedzd2wclasszd2namez00zzbackend_cplibz00(BgL_typez00_bglt);
	static obj_t
		BGl_z62getzd2declaredzd2globalzd2variableszb0zzbackend_cplibz00(obj_t,
		obj_t);
	static obj_t BGl_z62widezd2ze3chunkz53zzbackend_cplibz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2declaredzd2classesz00zzbackend_cplibz00(void);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2declaredzd2fieldsz00zzbackend_cplibz00(BgL_typez00_bglt);
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern bool_t BGl_widezd2classzf3z21zzobject_classz00(obj_t);
	static obj_t BGl_z62qualifiedzd2jclasszd2namez62zzbackend_cplibz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_qualifiedzd2tclasszd2namez00zzbackend_cplibz00(BgL_typez00_bglt);
	extern obj_t BGl_sfunz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2globalzd2variableszd2tozd2bezd2initializa7edz75zzbackend_cplibz00
		(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzbackend_cplibz00(void);
	static obj_t BGl_z62globalzd2arityzb0zzbackend_cplibz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62getzd2itszd2superz62zzbackend_cplibz00(obj_t,
		obj_t);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t bigloo_module_mangle(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzbackend_cplibz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62qualifiedzd2wclasszd2namez62zzbackend_cplibz00(obj_t,
		obj_t);
	extern obj_t BGl_getzd2objectzd2typez00zztype_cachez00(void);
	static obj_t BGl_objectzd2initzd2zzbackend_cplibz00(void);
	extern obj_t BGl_widezd2chunkzd2classzd2idzd2zzobject_classz00(obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_findzd2javazd2classz00zzmodule_javaz00(obj_t);
	static obj_t BGl_z62qualifiedzd2tclasszd2namez62zzbackend_cplibz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_cplibz00(void);
	BGL_IMPORT obj_t bgl_bignum_sub(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2declaredzd2globalzd2variableszd2zzbackend_cplibz00(obj_t);
	static obj_t
		BGl_z62forzd2eachzd2declaredzd2classeszb0zzbackend_cplibz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_globalzd2entryzd2zzbackend_cplibz00(BgL_globalz00_bglt);
	static obj_t
		BGl_z62getzd2globalzd2variableszd2tozd2bezd2initializa7edz17zzbackend_cplibz00
		(obj_t, obj_t);
	static obj_t BGl_z62setzd2variablezd2namez12z70zzbackend_cplibz00(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t);
	static obj_t BGl_z62resetzd2globalz12za2zzbackend_cplibz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_qualifiedzd2realzd2tclasszd2namezd2zzbackend_cplibz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2itszd2superz00zzbackend_cplibz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_forzd2eachzd2declaredzd2classeszd2zzbackend_cplibz00(obj_t);
	extern obj_t BGl_wclassz00zzobject_classz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_javaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_cfunz00zzast_varz00;
	extern bool_t BGl_slotzd2virtualzf3z21zzobject_slotsz00(BgL_slotz00_bglt);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzbackend_cplibz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_cplibz00(void);
	extern bool_t
		BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(BgL_globalz00_bglt);
	static obj_t BGl_typezd2ze3classz31zzbackend_cplibz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2arityzd2zzbackend_cplibz00(BgL_globalz00_bglt);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_cplibz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzbackend_cplibz00(void);
	static obj_t BGl_z62classzd2idzd2ze3typezd2namez53zzbackend_cplibz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62qualifiedzd2typezd2namez62zzbackend_cplibz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	static obj_t BGl_z62getzd2fieldzd2typez62zzbackend_cplibz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31716ze3ze5zzbackend_cplibz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_widezd2ze3chunkz31zzbackend_cplibz00(BgL_typez00_bglt);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_resetzd2globalz12zc0zzbackend_cplibz00(BgL_globalz00_bglt);
	extern obj_t BGl_za2jvmzd2foreignzd2classzd2nameza2zd2zzengine_paramz00;
	static obj_t BGl_z62getzd2declaredzd2fieldsz62zzbackend_cplibz00(obj_t,
		obj_t);
	extern obj_t BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_variablez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31680ze3ze5zzbackend_cplibz00(obj_t,
		obj_t);
	static BgL_globalz00_bglt BGl_z62globalzd2entryzb0zzbackend_cplibz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2fieldzd2typez00zzbackend_cplibz00(BgL_slotz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_classzd2idzd2ze3typezd2namez31zzbackend_cplibz00(obj_t, obj_t);
	extern obj_t BGl_getzd2classzd2listz00zzobject_classz00(void);
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_qualifiedzd2typezd2namez00zzbackend_cplibz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_qualifiedzd2jclasszd2namez00zzbackend_cplibz00(BgL_typez00_bglt);
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2declaredzd2globalzd2variableszd2envz00zzbackend_cplibz00,
		BgL_bgl_za762getza7d2declare1894z00,
		BGl_z62getzd2declaredzd2globalzd2variableszb0zzbackend_cplibz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_qualifiedzd2typezd2namezd2envzd2zzbackend_cplibz00,
		BgL_bgl_za762qualifiedza7d2t1895z00,
		BGl_z62qualifiedzd2typezd2namez62zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_resetzd2globalz12zd2envz12zzbackend_cplibz00,
		BgL_bgl_za762resetza7d2globa1896z00,
		BGl_z62resetzd2globalz12za2zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_qualifiedzd2tclasszd2namezd2envzd2zzbackend_cplibz00,
		BgL_bgl_za762qualifiedza7d2t1897z00,
		BGl_z62qualifiedzd2tclasszd2namez62zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2idzd2ze3typezd2namezd2envze3zzbackend_cplibz00,
		BgL_bgl_za762classza7d2idza7d21898za7,
		BGl_z62classzd2idzd2ze3typezd2namez53zzbackend_cplibz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_widezd2ze3chunkzd2envze3zzbackend_cplibz00,
		BgL_bgl_za762wideza7d2za7e3chu1899za7,
		BGl_z62widezd2ze3chunkz53zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2entryzd2envz00zzbackend_cplibz00,
		BgL_bgl_za762globalza7d2entr1900z00,
		BGl_z62globalzd2entryzb0zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2globalzd2variableszd2tozd2bezd2initializa7edzd2envza7zzbackend_cplibz00,
		BgL_bgl_za762getza7d2globalza71901za7,
		BGl_z62getzd2globalzd2variableszd2tozd2bezd2initializa7edz17zzbackend_cplibz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1879z00zzbackend_cplibz00,
		BgL_bgl_string1879za700za7za7b1902za7, "_K", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_qualifiedzd2wclasszd2namezd2envzd2zzbackend_cplibz00,
		BgL_bgl_za762qualifiedza7d2w1903z00,
		BGl_z62qualifiedzd2wclasszd2namez62zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2arityzd2envz00zzbackend_cplibz00,
		BgL_bgl_za762globalza7d2arit1904z00,
		BGl_z62globalzd2arityzb0zzbackend_cplibz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1880z00zzbackend_cplibz00,
		BgL_bgl_string1880za700za7za7b1905za7, "object", 6);
	      DEFINE_STRING(BGl_string1881z00zzbackend_cplibz00,
		BgL_bgl_string1881za700za7za7b1906za7, "", 0);
	      DEFINE_STRING(BGl_string1882z00zzbackend_cplibz00,
		BgL_bgl_string1882za700za7za7b1907za7, "bigloo", 6);
	      DEFINE_STRING(BGl_string1883z00zzbackend_cplibz00,
		BgL_bgl_string1883za700za7za7b1908za7, ".", 1);
	      DEFINE_STRING(BGl_string1884z00zzbackend_cplibz00,
		BgL_bgl_string1884za700za7za7b1909za7, "bigloo.", 7);
	      DEFINE_STRING(BGl_string1885z00zzbackend_cplibz00,
		BgL_bgl_string1885za700za7za7b1910za7, "_", 1);
	      DEFINE_STRING(BGl_string1886z00zzbackend_cplibz00,
		BgL_bgl_string1886za700za7za7b1911za7, "set-variable-name!", 18);
	      DEFINE_STRING(BGl_string1887z00zzbackend_cplibz00,
		BgL_bgl_string1887za700za7za7b1912za7, "Unknown variable king", 21);
	      DEFINE_STRING(BGl_string1888z00zzbackend_cplibz00,
		BgL_bgl_string1888za700za7za7b1913za7, "Illegal type", 12);
	      DEFINE_STRING(BGl_string1889z00zzbackend_cplibz00,
		BgL_bgl_string1889za700za7za7b1914za7, "wide-chunk", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2declaredzd2classeszd2envzd2zzbackend_cplibz00,
		BgL_bgl_za762getza7d2declare1915z00,
		BGl_z62getzd2declaredzd2classesz62zzbackend_cplibz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1890z00zzbackend_cplibz00,
		BgL_bgl_string1890za700za7za7b1916za7, "internal error", 14);
	      DEFINE_STRING(BGl_string1891z00zzbackend_cplibz00,
		BgL_bgl_string1891za700za7za7b1917za7, "backend_cplib", 13);
	      DEFINE_STRING(BGl_string1892z00zzbackend_cplibz00,
		BgL_bgl_string1892za700za7za7b1918za7,
		"__cnsts_table get-declared-fields import #z1 stvector foreign obj ", 66);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2variablezd2namez12zd2envzc0zzbackend_cplibz00,
		BgL_bgl_za762setza7d2variabl1919z00,
		BGl_z62setzd2variablezd2namez12z70zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2itszd2superzd2envzd2zzbackend_cplibz00,
		BgL_bgl_za762getza7d2itsza7d2s1920za7,
		BGl_z62getzd2itszd2superz62zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_forzd2eachzd2declaredzd2classeszd2envz00zzbackend_cplibz00,
		BgL_bgl_za762forza7d2eachza7d21921za7,
		BGl_z62forzd2eachzd2declaredzd2classeszb0zzbackend_cplibz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2declaredzd2fieldszd2envzd2zzbackend_cplibz00,
		BgL_bgl_za762getza7d2declare1922z00,
		BGl_z62getzd2declaredzd2fieldsz62zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_qualifiedzd2jclasszd2namezd2envzd2zzbackend_cplibz00,
		BgL_bgl_za762qualifiedza7d2j1923z00,
		BGl_z62qualifiedzd2jclasszd2namez62zzbackend_cplibz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2fieldzd2typezd2envzd2zzbackend_cplibz00,
		BgL_bgl_za762getza7d2fieldza7d1924za7,
		BGl_z62getzd2fieldzd2typez62zzbackend_cplibz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzbackend_cplibz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long
		BgL_checksumz00_2982, char *BgL_fromz00_2983)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_cplibz00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_cplibz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_cplibz00();
					BGl_libraryzd2moduleszd2initz00zzbackend_cplibz00();
					BGl_cnstzd2initzd2zzbackend_cplibz00();
					BGl_importedzd2moduleszd2initz00zzbackend_cplibz00();
					return BGl_toplevelzd2initzd2zzbackend_cplibz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_cplibz00(void)
	{
		{	/* BackEnd/cplib.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "backend_cplib");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "backend_cplib");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"backend_cplib");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_cplib");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"backend_cplib");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "backend_cplib");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_cplib");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "backend_cplib");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"backend_cplib");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"backend_cplib");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"backend_cplib");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"backend_cplib");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "backend_cplib");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_cplibz00(void)
	{
		{	/* BackEnd/cplib.scm 15 */
			{	/* BackEnd/cplib.scm 15 */
				obj_t BgL_cportz00_2921;

				{	/* BackEnd/cplib.scm 15 */
					obj_t BgL_stringz00_2928;

					BgL_stringz00_2928 = BGl_string1892z00zzbackend_cplibz00;
					{	/* BackEnd/cplib.scm 15 */
						obj_t BgL_startz00_2929;

						BgL_startz00_2929 = BINT(0L);
						{	/* BackEnd/cplib.scm 15 */
							obj_t BgL_endz00_2930;

							BgL_endz00_2930 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2928)));
							{	/* BackEnd/cplib.scm 15 */

								BgL_cportz00_2921 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2928, BgL_startz00_2929, BgL_endz00_2930);
				}}}}
				{
					long BgL_iz00_2922;

					BgL_iz00_2922 = 6L;
				BgL_loopz00_2923:
					if ((BgL_iz00_2922 == -1L))
						{	/* BackEnd/cplib.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/cplib.scm 15 */
							{	/* BackEnd/cplib.scm 15 */
								obj_t BgL_arg1893z00_2924;

								{	/* BackEnd/cplib.scm 15 */

									{	/* BackEnd/cplib.scm 15 */
										obj_t BgL_locationz00_2926;

										BgL_locationz00_2926 = BBOOL(((bool_t) 0));
										{	/* BackEnd/cplib.scm 15 */

											BgL_arg1893z00_2924 =
												BGl_readz00zz__readerz00(BgL_cportz00_2921,
												BgL_locationz00_2926);
										}
									}
								}
								{	/* BackEnd/cplib.scm 15 */
									int BgL_tmpz00_3014;

									BgL_tmpz00_3014 = (int) (BgL_iz00_2922);
									CNST_TABLE_SET(BgL_tmpz00_3014, BgL_arg1893z00_2924);
							}}
							{	/* BackEnd/cplib.scm 15 */
								int BgL_auxz00_2927;

								BgL_auxz00_2927 = (int) ((BgL_iz00_2922 - 1L));
								{
									long BgL_iz00_3019;

									BgL_iz00_3019 = (long) (BgL_auxz00_2927);
									BgL_iz00_2922 = BgL_iz00_3019;
									goto BgL_loopz00_2923;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_cplibz00(void)
	{
		{	/* BackEnd/cplib.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbackend_cplibz00(void)
	{
		{	/* BackEnd/cplib.scm 15 */
			return BUNSPEC;
		}

	}



/* class-id->type-name */
	BGL_EXPORTED_DEF obj_t
		BGl_classzd2idzd2ze3typezd2namez31zzbackend_cplibz00(obj_t BgL_cidz00_13,
		obj_t BgL_midz00_14)
	{
		{	/* BackEnd/cplib.scm 56 */
			{	/* BackEnd/cplib.scm 57 */
				obj_t BgL_idz00_2059;

				{	/* BackEnd/cplib.scm 57 */
					obj_t BgL_arg1311z00_2060;
					obj_t BgL_arg1312z00_2061;

					{	/* BackEnd/cplib.scm 57 */
						obj_t BgL_arg1455z00_2063;

						BgL_arg1455z00_2063 = SYMBOL_TO_STRING(BgL_cidz00_13);
						BgL_arg1311z00_2060 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2063);
					}
					{	/* BackEnd/cplib.scm 57 */
						obj_t BgL_arg1455z00_2065;

						BgL_arg1455z00_2065 = SYMBOL_TO_STRING(BgL_midz00_14);
						BgL_arg1312z00_2061 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2065);
					}
					BgL_idz00_2059 =
						bigloo_module_mangle(BgL_arg1311z00_2060, BgL_arg1312z00_2061);
				}
				return
					string_append(BGl_string1879z00zzbackend_cplibz00, BgL_idz00_2059);
			}
		}

	}



/* &class-id->type-name */
	obj_t BGl_z62classzd2idzd2ze3typezd2namez53zzbackend_cplibz00(obj_t
		BgL_envz00_2870, obj_t BgL_cidz00_2871, obj_t BgL_midz00_2872)
	{
		{	/* BackEnd/cplib.scm 56 */
			return
				BGl_classzd2idzd2ze3typezd2namez31zzbackend_cplibz00(BgL_cidz00_2871,
				BgL_midz00_2872);
		}

	}



/* qualified-real-tclass-name */
	obj_t
		BGl_qualifiedzd2realzd2tclasszd2namezd2zzbackend_cplibz00(BgL_typez00_bglt
		BgL_classz00_15)
	{
		{	/* BackEnd/cplib.scm 63 */
			{
				BgL_typez00_bglt BgL_superz00_1677;
				obj_t BgL_namez00_1672;
				BgL_typez00_bglt BgL_classz00_1663;

				{	/* BackEnd/cplib.scm 97 */
					obj_t BgL_arg1314z00_1657;
					obj_t BgL_arg1315z00_1658;

					BgL_superz00_1677 = BgL_classz00_15;
					{	/* BackEnd/cplib.scm 87 */
						bool_t BgL_test1927z00_3029;

						{	/* BackEnd/cplib.scm 87 */
							bool_t BgL_test1928z00_3030;

							{	/* BackEnd/cplib.scm 87 */
								obj_t BgL_arg1361z00_1703;

								{
									BgL_tclassz00_bglt BgL_auxz00_3031;

									{
										obj_t BgL_auxz00_3032;

										{	/* BackEnd/cplib.scm 87 */
											BgL_objectz00_bglt BgL_tmpz00_3033;

											BgL_tmpz00_3033 =
												((BgL_objectz00_bglt) BgL_superz00_1677);
											BgL_auxz00_3032 = BGL_OBJECT_WIDENING(BgL_tmpz00_3033);
										}
										BgL_auxz00_3031 = ((BgL_tclassz00_bglt) BgL_auxz00_3032);
									}
									BgL_arg1361z00_1703 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3031))->
										BgL_itszd2superzd2);
								}
								{	/* BackEnd/cplib.scm 87 */
									obj_t BgL_classz00_2101;

									BgL_classz00_2101 = BGl_tclassz00zzobject_classz00;
									if (BGL_OBJECTP(BgL_arg1361z00_1703))
										{	/* BackEnd/cplib.scm 87 */
											BgL_objectz00_bglt BgL_arg1807z00_2103;

											BgL_arg1807z00_2103 =
												(BgL_objectz00_bglt) (BgL_arg1361z00_1703);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* BackEnd/cplib.scm 87 */
													long BgL_idxz00_2109;

													BgL_idxz00_2109 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2103);
													BgL_test1928z00_3030 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2109 + 2L)) == BgL_classz00_2101);
												}
											else
												{	/* BackEnd/cplib.scm 87 */
													bool_t BgL_res1860z00_2134;

													{	/* BackEnd/cplib.scm 87 */
														obj_t BgL_oclassz00_2117;

														{	/* BackEnd/cplib.scm 87 */
															obj_t BgL_arg1815z00_2125;
															long BgL_arg1816z00_2126;

															BgL_arg1815z00_2125 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* BackEnd/cplib.scm 87 */
																long BgL_arg1817z00_2127;

																BgL_arg1817z00_2127 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2103);
																BgL_arg1816z00_2126 =
																	(BgL_arg1817z00_2127 - OBJECT_TYPE);
															}
															BgL_oclassz00_2117 =
																VECTOR_REF(BgL_arg1815z00_2125,
																BgL_arg1816z00_2126);
														}
														{	/* BackEnd/cplib.scm 87 */
															bool_t BgL__ortest_1115z00_2118;

															BgL__ortest_1115z00_2118 =
																(BgL_classz00_2101 == BgL_oclassz00_2117);
															if (BgL__ortest_1115z00_2118)
																{	/* BackEnd/cplib.scm 87 */
																	BgL_res1860z00_2134 =
																		BgL__ortest_1115z00_2118;
																}
															else
																{	/* BackEnd/cplib.scm 87 */
																	long BgL_odepthz00_2119;

																	{	/* BackEnd/cplib.scm 87 */
																		obj_t BgL_arg1804z00_2120;

																		BgL_arg1804z00_2120 = (BgL_oclassz00_2117);
																		BgL_odepthz00_2119 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2120);
																	}
																	if ((2L < BgL_odepthz00_2119))
																		{	/* BackEnd/cplib.scm 87 */
																			obj_t BgL_arg1802z00_2122;

																			{	/* BackEnd/cplib.scm 87 */
																				obj_t BgL_arg1803z00_2123;

																				BgL_arg1803z00_2123 =
																					(BgL_oclassz00_2117);
																				BgL_arg1802z00_2122 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2123, 2L);
																			}
																			BgL_res1860z00_2134 =
																				(BgL_arg1802z00_2122 ==
																				BgL_classz00_2101);
																		}
																	else
																		{	/* BackEnd/cplib.scm 87 */
																			BgL_res1860z00_2134 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1928z00_3030 = BgL_res1860z00_2134;
												}
										}
									else
										{	/* BackEnd/cplib.scm 87 */
											BgL_test1928z00_3030 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test1928z00_3030)
								{	/* BackEnd/cplib.scm 88 */
									bool_t BgL_test1933z00_3060;

									{	/* BackEnd/cplib.scm 88 */
										obj_t BgL_arg1352z00_1702;

										{
											BgL_tclassz00_bglt BgL_auxz00_3061;

											{
												obj_t BgL_auxz00_3062;

												{	/* BackEnd/cplib.scm 88 */
													BgL_objectz00_bglt BgL_tmpz00_3063;

													BgL_tmpz00_3063 =
														((BgL_objectz00_bglt) BgL_superz00_1677);
													BgL_auxz00_3062 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3063);
												}
												BgL_auxz00_3061 =
													((BgL_tclassz00_bglt) BgL_auxz00_3062);
											}
											BgL_arg1352z00_1702 =
												(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3061))->
												BgL_itszd2superzd2);
										}
										BgL_test1933z00_3060 =
											(((obj_t) BgL_superz00_1677) == BgL_arg1352z00_1702);
									}
									if (BgL_test1933z00_3060)
										{	/* BackEnd/cplib.scm 89 */
											bool_t BgL_test1934z00_3070;

											{	/* BackEnd/cplib.scm 89 */
												obj_t BgL_arg1349z00_1700;

												{	/* BackEnd/cplib.scm 89 */
													obj_t BgL_arg1351z00_1701;

													{
														BgL_tclassz00_bglt BgL_auxz00_3071;

														{
															obj_t BgL_auxz00_3072;

															{	/* BackEnd/cplib.scm 89 */
																BgL_objectz00_bglt BgL_tmpz00_3073;

																BgL_tmpz00_3073 =
																	((BgL_objectz00_bglt) BgL_superz00_1677);
																BgL_auxz00_3072 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3073);
															}
															BgL_auxz00_3071 =
																((BgL_tclassz00_bglt) BgL_auxz00_3072);
														}
														BgL_arg1351z00_1701 =
															(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3071))->
															BgL_itszd2superzd2);
													}
													BgL_arg1349z00_1700 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_arg1351z00_1701)))->
														BgL_idz00);
												}
												BgL_test1934z00_3070 =
													(BgL_arg1349z00_1700 == CNST_TABLE_REF(0));
											}
											if (BgL_test1934z00_3070)
												{	/* BackEnd/cplib.scm 89 */
													BgL_test1927z00_3029 = ((bool_t) 0);
												}
											else
												{	/* BackEnd/cplib.scm 89 */
													BgL_test1927z00_3029 = ((bool_t) 1);
												}
										}
									else
										{	/* BackEnd/cplib.scm 88 */
											BgL_test1927z00_3029 = ((bool_t) 0);
										}
								}
							else
								{	/* BackEnd/cplib.scm 87 */
									BgL_test1927z00_3029 = ((bool_t) 1);
								}
						}
						if (BgL_test1927z00_3029)
							{	/* BackEnd/cplib.scm 87 */
								BgL_arg1314z00_1657 = BGl_string1882z00zzbackend_cplibz00;
							}
						else
							{	/* BackEnd/cplib.scm 91 */
								BgL_globalz00_bglt BgL_holderz00_1691;

								{
									BgL_tclassz00_bglt BgL_auxz00_3082;

									{
										obj_t BgL_auxz00_3083;

										{	/* BackEnd/cplib.scm 91 */
											BgL_objectz00_bglt BgL_tmpz00_3084;

											BgL_tmpz00_3084 =
												((BgL_objectz00_bglt) BgL_superz00_1677);
											BgL_auxz00_3083 = BGL_OBJECT_WIDENING(BgL_tmpz00_3084);
										}
										BgL_auxz00_3082 = ((BgL_tclassz00_bglt) BgL_auxz00_3083);
									}
									BgL_holderz00_1691 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3082))->
										BgL_holderz00);
								}
								{	/* BackEnd/cplib.scm 92 */
									obj_t BgL_arg1342z00_1692;
									obj_t BgL_arg1343z00_1693;
									obj_t BgL_arg1346z00_1694;

									BgL_arg1342z00_1692 =
										(((BgL_globalz00_bglt) COBJECT(BgL_holderz00_1691))->
										BgL_modulez00);
									BgL_arg1343z00_1693 =
										(((BgL_globalz00_bglt) COBJECT(BgL_holderz00_1691))->
										BgL_jvmzd2typezd2namez00);
									BgL_arg1346z00_1694 =
										(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
													BgL_superz00_1677)))->BgL_idz00);
									{	/* BackEnd/cplib.scm 92 */
										obj_t BgL_list1347z00_1695;

										BgL_list1347z00_1695 =
											MAKE_YOUNG_PAIR(BgL_arg1346z00_1694, BNIL);
										BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00
											(BgL_arg1342z00_1692, BgL_arg1343z00_1693,
											BgL_list1347z00_1695);
									}
								}
								BgL_namez00_1672 =
									(((BgL_globalz00_bglt) COBJECT(BgL_holderz00_1691))->
									BgL_modulez00);
								{	/* BackEnd/cplib.scm 79 */
									obj_t BgL_sz00_1674;

									BgL_sz00_1674 =
										BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00
										(BgL_namez00_1672);
									{	/* BackEnd/cplib.scm 79 */
										obj_t BgL_prefz00_1675;

										BgL_prefz00_1675 = BGl_prefixz00zz__osz00(BgL_sz00_1674);
										{	/* BackEnd/cplib.scm 80 */

											{	/* BackEnd/cplib.scm 81 */
												bool_t BgL_test1935z00_3097;

												{	/* BackEnd/cplib.scm 81 */
													long BgL_l1z00_2091;

													BgL_l1z00_2091 = STRING_LENGTH(BgL_prefz00_1675);
													if ((BgL_l1z00_2091 == STRING_LENGTH(BgL_sz00_1674)))
														{	/* BackEnd/cplib.scm 81 */
															int BgL_arg1282z00_2094;

															{	/* BackEnd/cplib.scm 81 */
																char *BgL_auxz00_3104;
																char *BgL_tmpz00_3102;

																BgL_auxz00_3104 =
																	BSTRING_TO_STRING(BgL_sz00_1674);
																BgL_tmpz00_3102 =
																	BSTRING_TO_STRING(BgL_prefz00_1675);
																BgL_arg1282z00_2094 =
																	memcmp(BgL_tmpz00_3102, BgL_auxz00_3104,
																	BgL_l1z00_2091);
															}
															BgL_test1935z00_3097 =
																((long) (BgL_arg1282z00_2094) == 0L);
														}
													else
														{	/* BackEnd/cplib.scm 81 */
															BgL_test1935z00_3097 = ((bool_t) 0);
														}
												}
												if (BgL_test1935z00_3097)
													{	/* BackEnd/cplib.scm 81 */
														BgL_arg1314z00_1657 =
															BGl_string1881z00zzbackend_cplibz00;
													}
												else
													{	/* BackEnd/cplib.scm 81 */
														BgL_arg1314z00_1657 = BgL_prefz00_1675;
													}
											}
										}
									}
								}
							}
					}
					BgL_classz00_1663 = BgL_classz00_15;
					{	/* BackEnd/cplib.scm 72 */
						bool_t BgL_test1937z00_3110;

						{	/* BackEnd/cplib.scm 72 */
							obj_t BgL_arg1323z00_1671;

							BgL_arg1323z00_1671 = BGl_getzd2objectzd2typez00zztype_cachez00();
							BgL_test1937z00_3110 =
								(((obj_t) BgL_classz00_1663) == BgL_arg1323z00_1671);
						}
						if (BgL_test1937z00_3110)
							{	/* BackEnd/cplib.scm 72 */
								BgL_arg1315z00_1658 = BGl_string1880z00zzbackend_cplibz00;
							}
						else
							{	/* BackEnd/cplib.scm 75 */
								obj_t BgL_modz00_1668;

								{	/* BackEnd/cplib.scm 75 */
									BgL_globalz00_bglt BgL_arg1322z00_1670;

									{
										BgL_tclassz00_bglt BgL_auxz00_3114;

										{
											obj_t BgL_auxz00_3115;

											{	/* BackEnd/cplib.scm 75 */
												BgL_objectz00_bglt BgL_tmpz00_3116;

												BgL_tmpz00_3116 =
													((BgL_objectz00_bglt) BgL_classz00_1663);
												BgL_auxz00_3115 = BGL_OBJECT_WIDENING(BgL_tmpz00_3116);
											}
											BgL_auxz00_3114 = ((BgL_tclassz00_bglt) BgL_auxz00_3115);
										}
										BgL_arg1322z00_1670 =
											(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3114))->
											BgL_holderz00);
									}
									BgL_modz00_1668 =
										(((BgL_globalz00_bglt) COBJECT(BgL_arg1322z00_1670))->
										BgL_modulez00);
								}
								{	/* BackEnd/cplib.scm 76 */
									obj_t BgL_arg1321z00_1669;

									BgL_arg1321z00_1669 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_classz00_1663)))->BgL_idz00);
									{	/* BackEnd/cplib.scm 57 */
										obj_t BgL_idz00_2082;

										{	/* BackEnd/cplib.scm 57 */
											obj_t BgL_arg1311z00_2083;
											obj_t BgL_arg1312z00_2084;

											{	/* BackEnd/cplib.scm 57 */
												obj_t BgL_arg1455z00_2086;

												BgL_arg1455z00_2086 =
													SYMBOL_TO_STRING(BgL_arg1321z00_1669);
												BgL_arg1311z00_2083 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_2086);
											}
											{	/* BackEnd/cplib.scm 57 */
												obj_t BgL_arg1455z00_2088;

												BgL_arg1455z00_2088 = SYMBOL_TO_STRING(BgL_modz00_1668);
												BgL_arg1312z00_2084 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_2088);
											}
											BgL_idz00_2082 =
												bigloo_module_mangle(BgL_arg1311z00_2083,
												BgL_arg1312z00_2084);
										}
										BgL_arg1315z00_1658 =
											string_append(BGl_string1879z00zzbackend_cplibz00,
											BgL_idz00_2082);
									}
								}
							}
					}
					{	/* BackEnd/cplib.scm 67 */
						bool_t BgL_test1938z00_3130;

						{	/* BackEnd/cplib.scm 67 */
							long BgL_l1z00_2146;

							BgL_l1z00_2146 = STRING_LENGTH(BgL_arg1314z00_1657);
							if ((BgL_l1z00_2146 == 0L))
								{	/* BackEnd/cplib.scm 67 */
									int BgL_arg1282z00_2149;

									{	/* BackEnd/cplib.scm 67 */
										char *BgL_auxz00_3136;
										char *BgL_tmpz00_3134;

										BgL_auxz00_3136 =
											BSTRING_TO_STRING(BGl_string1881z00zzbackend_cplibz00);
										BgL_tmpz00_3134 = BSTRING_TO_STRING(BgL_arg1314z00_1657);
										BgL_arg1282z00_2149 =
											memcmp(BgL_tmpz00_3134, BgL_auxz00_3136, BgL_l1z00_2146);
									}
									BgL_test1938z00_3130 = ((long) (BgL_arg1282z00_2149) == 0L);
								}
							else
								{	/* BackEnd/cplib.scm 67 */
									BgL_test1938z00_3130 = ((bool_t) 0);
								}
						}
						if (BgL_test1938z00_3130)
							{	/* BackEnd/cplib.scm 67 */
								return BgL_arg1315z00_1658;
							}
						else
							{	/* BackEnd/cplib.scm 67 */
								return
									string_append_3(BgL_arg1314z00_1657,
									BGl_string1883z00zzbackend_cplibz00, BgL_arg1315z00_1658);
							}
					}
				}
			}
		}

	}



/* qualified-tclass-name */
	BGL_EXPORTED_DEF obj_t
		BGl_qualifiedzd2tclasszd2namez00zzbackend_cplibz00(BgL_typez00_bglt
		BgL_classz00_16)
	{
		{	/* BackEnd/cplib.scm 102 */
			{	/* BackEnd/cplib.scm 103 */
				obj_t BgL_cz00_1708;

				if (BGl_widezd2classzf3z21zzobject_classz00(((obj_t) BgL_classz00_16)))
					{
						BgL_tclassz00_bglt BgL_auxz00_3145;

						{
							obj_t BgL_auxz00_3146;

							{	/* BackEnd/cplib.scm 103 */
								BgL_objectz00_bglt BgL_tmpz00_3147;

								BgL_tmpz00_3147 = ((BgL_objectz00_bglt) BgL_classz00_16);
								BgL_auxz00_3146 = BGL_OBJECT_WIDENING(BgL_tmpz00_3147);
							}
							BgL_auxz00_3145 = ((BgL_tclassz00_bglt) BgL_auxz00_3146);
						}
						BgL_cz00_1708 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3145))->
							BgL_itszd2superzd2);
					}
				else
					{	/* BackEnd/cplib.scm 103 */
						BgL_cz00_1708 = ((obj_t) BgL_classz00_16);
					}
				BGL_TAIL return
					BGl_qualifiedzd2realzd2tclasszd2namezd2zzbackend_cplibz00(
					((BgL_typez00_bglt) BgL_cz00_1708));
			}
		}

	}



/* &qualified-tclass-name */
	obj_t BGl_z62qualifiedzd2tclasszd2namez62zzbackend_cplibz00(obj_t
		BgL_envz00_2873, obj_t BgL_classz00_2874)
	{
		{	/* BackEnd/cplib.scm 102 */
			return
				BGl_qualifiedzd2tclasszd2namez00zzbackend_cplibz00(
				((BgL_typez00_bglt) BgL_classz00_2874));
		}

	}



/* qualified-wclass-name */
	BGL_EXPORTED_DEF obj_t
		BGl_qualifiedzd2wclasszd2namez00zzbackend_cplibz00(BgL_typez00_bglt
		BgL_classz00_17)
	{
		{	/* BackEnd/cplib.scm 109 */
			{	/* BackEnd/cplib.scm 110 */
				obj_t BgL_arg1364z00_2157;

				{
					BgL_wclassz00_bglt BgL_auxz00_3157;

					{
						obj_t BgL_auxz00_3158;

						{	/* BackEnd/cplib.scm 110 */
							BgL_objectz00_bglt BgL_tmpz00_3159;

							BgL_tmpz00_3159 = ((BgL_objectz00_bglt) BgL_classz00_17);
							BgL_auxz00_3158 = BGL_OBJECT_WIDENING(BgL_tmpz00_3159);
						}
						BgL_auxz00_3157 = ((BgL_wclassz00_bglt) BgL_auxz00_3158);
					}
					BgL_arg1364z00_2157 =
						(((BgL_wclassz00_bglt) COBJECT(BgL_auxz00_3157))->
						BgL_itszd2classzd2);
				}
				return
					BGl_qualifiedzd2realzd2tclasszd2namezd2zzbackend_cplibz00(
					((BgL_typez00_bglt) BgL_arg1364z00_2157));
			}
		}

	}



/* &qualified-wclass-name */
	obj_t BGl_z62qualifiedzd2wclasszd2namez62zzbackend_cplibz00(obj_t
		BgL_envz00_2875, obj_t BgL_classz00_2876)
	{
		{	/* BackEnd/cplib.scm 109 */
			return
				BGl_qualifiedzd2wclasszd2namez00zzbackend_cplibz00(
				((BgL_typez00_bglt) BgL_classz00_2876));
		}

	}



/* qualified-jclass-name */
	BGL_EXPORTED_DEF obj_t
		BGl_qualifiedzd2jclasszd2namez00zzbackend_cplibz00(BgL_typez00_bglt
		BgL_classz00_18)
	{
		{	/* BackEnd/cplib.scm 115 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_classz00_18)))->BgL_namez00);
		}

	}



/* &qualified-jclass-name */
	obj_t BGl_z62qualifiedzd2jclasszd2namez62zzbackend_cplibz00(obj_t
		BgL_envz00_2877, obj_t BgL_classz00_2878)
	{
		{	/* BackEnd/cplib.scm 115 */
			return
				BGl_qualifiedzd2jclasszd2namez00zzbackend_cplibz00(
				((BgL_typez00_bglt) BgL_classz00_2878));
		}

	}



/* qualified-type-name */
	BGL_EXPORTED_DEF obj_t
		BGl_qualifiedzd2typezd2namez00zzbackend_cplibz00(BgL_typez00_bglt
		BgL_typez00_19)
	{
		{	/* BackEnd/cplib.scm 121 */
			{	/* BackEnd/cplib.scm 122 */
				obj_t BgL_idz00_1711;

				BgL_idz00_1711 =
					(((BgL_typez00_bglt) COBJECT(BgL_typez00_19))->BgL_idz00);
				if ((BgL_idz00_1711 == CNST_TABLE_REF(1)))
					{	/* BackEnd/cplib.scm 123 */
						return BGl_za2jvmzd2foreignzd2classzd2nameza2zd2zzengine_paramz00;
					}
				else
					{	/* BackEnd/cplib.scm 125 */
						obj_t BgL_javazd2classzd2_1712;

						BgL_javazd2classzd2_1712 =
							BGl_findzd2javazd2classz00zzmodule_javaz00(BgL_idz00_1711);
						if (STRINGP(BgL_javazd2classzd2_1712))
							{	/* BackEnd/cplib.scm 126 */
								return BgL_javazd2classzd2_1712;
							}
						else
							{	/* BackEnd/cplib.scm 128 */
								obj_t BgL_arg1367z00_1714;

								{	/* BackEnd/cplib.scm 128 */
									obj_t BgL_arg1455z00_2163;

									BgL_arg1455z00_2163 = SYMBOL_TO_STRING(BgL_idz00_1711);
									BgL_arg1367z00_1714 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2163);
								}
								return
									string_append(BGl_string1884z00zzbackend_cplibz00,
									BgL_arg1367z00_1714);
							}
					}
			}
		}

	}



/* &qualified-type-name */
	obj_t BGl_z62qualifiedzd2typezd2namez62zzbackend_cplibz00(obj_t
		BgL_envz00_2879, obj_t BgL_typez00_2880)
	{
		{	/* BackEnd/cplib.scm 121 */
			return
				BGl_qualifiedzd2typezd2namez00zzbackend_cplibz00(
				((BgL_typez00_bglt) BgL_typez00_2880));
		}

	}



/* reset-global! */
	BGL_EXPORTED_DEF obj_t
		BGl_resetzd2globalz12zc0zzbackend_cplibz00(BgL_globalz00_bglt BgL_varz00_20)
	{
		{	/* BackEnd/cplib.scm 134 */
			{	/* BackEnd/cplib.scm 136 */
				obj_t BgL_namez00_1715;

				BgL_namez00_1715 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_20)))->BgL_namez00);
				if (STRINGP(BgL_namez00_1715))
					{	/* BackEnd/cplib.scm 137 */
						BgL_namez00_1715;
					}
				else
					{	/* BackEnd/cplib.scm 139 */
						obj_t BgL_namez00_1717;

						{	/* BackEnd/cplib.scm 139 */
							obj_t BgL_arg1375z00_1720;

							{	/* BackEnd/cplib.scm 139 */
								obj_t BgL__ortest_1109z00_1721;

								BgL__ortest_1109z00_1721 =
									(((BgL_globalz00_bglt) COBJECT(BgL_varz00_20))->BgL_aliasz00);
								if (CBOOL(BgL__ortest_1109z00_1721))
									{	/* BackEnd/cplib.scm 139 */
										BgL_arg1375z00_1720 = BgL__ortest_1109z00_1721;
									}
								else
									{	/* BackEnd/cplib.scm 139 */
										BgL_arg1375z00_1720 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_varz00_20)))->BgL_idz00);
									}
							}
							{	/* BackEnd/cplib.scm 139 */
								obj_t BgL_arg1455z00_2168;

								BgL_arg1455z00_2168 =
									SYMBOL_TO_STRING(((obj_t) BgL_arg1375z00_1720));
								BgL_namez00_1717 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_2168);
							}
						}
						{	/* BackEnd/cplib.scm 141 */
							obj_t BgL_arg1370z00_1718;

							if (BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00
								(BgL_namez00_1717))
								{	/* BackEnd/cplib.scm 141 */
									BgL_arg1370z00_1718 = bigloo_mangle(BgL_namez00_1717);
								}
							else
								{	/* BackEnd/cplib.scm 141 */
									BgL_arg1370z00_1718 = BgL_namez00_1717;
								}
							((((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_varz00_20)))->BgL_namez00) =
								((obj_t) BgL_arg1370z00_1718), BUNSPEC);
						}
					}
			}
			{	/* BackEnd/cplib.scm 145 */
				BgL_valuez00_bglt BgL_valuez00_1722;

				BgL_valuez00_1722 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_20)))->BgL_valuez00);
				{	/* BackEnd/cplib.scm 146 */
					bool_t BgL_test1946z00_3203;

					{	/* BackEnd/cplib.scm 146 */
						bool_t BgL_test1947z00_3204;

						{	/* BackEnd/cplib.scm 146 */
							obj_t BgL_classz00_2171;

							BgL_classz00_2171 = BGl_scnstz00zzast_varz00;
							{	/* BackEnd/cplib.scm 146 */
								BgL_objectz00_bglt BgL_arg1807z00_2173;

								{	/* BackEnd/cplib.scm 146 */
									obj_t BgL_tmpz00_3205;

									BgL_tmpz00_3205 =
										((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_1722));
									BgL_arg1807z00_2173 = (BgL_objectz00_bglt) (BgL_tmpz00_3205);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* BackEnd/cplib.scm 146 */
										long BgL_idxz00_2179;

										BgL_idxz00_2179 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2173);
										BgL_test1947z00_3204 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2179 + 2L)) == BgL_classz00_2171);
									}
								else
									{	/* BackEnd/cplib.scm 146 */
										bool_t BgL_res1861z00_2204;

										{	/* BackEnd/cplib.scm 146 */
											obj_t BgL_oclassz00_2187;

											{	/* BackEnd/cplib.scm 146 */
												obj_t BgL_arg1815z00_2195;
												long BgL_arg1816z00_2196;

												BgL_arg1815z00_2195 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* BackEnd/cplib.scm 146 */
													long BgL_arg1817z00_2197;

													BgL_arg1817z00_2197 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2173);
													BgL_arg1816z00_2196 =
														(BgL_arg1817z00_2197 - OBJECT_TYPE);
												}
												BgL_oclassz00_2187 =
													VECTOR_REF(BgL_arg1815z00_2195, BgL_arg1816z00_2196);
											}
											{	/* BackEnd/cplib.scm 146 */
												bool_t BgL__ortest_1115z00_2188;

												BgL__ortest_1115z00_2188 =
													(BgL_classz00_2171 == BgL_oclassz00_2187);
												if (BgL__ortest_1115z00_2188)
													{	/* BackEnd/cplib.scm 146 */
														BgL_res1861z00_2204 = BgL__ortest_1115z00_2188;
													}
												else
													{	/* BackEnd/cplib.scm 146 */
														long BgL_odepthz00_2189;

														{	/* BackEnd/cplib.scm 146 */
															obj_t BgL_arg1804z00_2190;

															BgL_arg1804z00_2190 = (BgL_oclassz00_2187);
															BgL_odepthz00_2189 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2190);
														}
														if ((2L < BgL_odepthz00_2189))
															{	/* BackEnd/cplib.scm 146 */
																obj_t BgL_arg1802z00_2192;

																{	/* BackEnd/cplib.scm 146 */
																	obj_t BgL_arg1803z00_2193;

																	BgL_arg1803z00_2193 = (BgL_oclassz00_2187);
																	BgL_arg1802z00_2192 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2193,
																		2L);
																}
																BgL_res1861z00_2204 =
																	(BgL_arg1802z00_2192 == BgL_classz00_2171);
															}
														else
															{	/* BackEnd/cplib.scm 146 */
																BgL_res1861z00_2204 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1947z00_3204 = BgL_res1861z00_2204;
									}
							}
						}
						if (BgL_test1947z00_3204)
							{	/* BackEnd/cplib.scm 146 */
								BgL_test1946z00_3203 =
									(
									(((BgL_scnstz00_bglt) COBJECT(
												((BgL_scnstz00_bglt) BgL_valuez00_1722)))->
										BgL_classz00) == CNST_TABLE_REF(2));
							}
						else
							{	/* BackEnd/cplib.scm 146 */
								BgL_test1946z00_3203 = ((bool_t) 0);
							}
					}
					if (BgL_test1946z00_3203)
						{	/* BackEnd/cplib.scm 147 */
							obj_t BgL_arg1421z00_1726;

							{	/* BackEnd/cplib.scm 147 */
								obj_t BgL_sz00_2207;

								BgL_sz00_2207 =
									(((BgL_scnstz00_bglt) COBJECT(
											((BgL_scnstz00_bglt) BgL_valuez00_1722)))->BgL_nodez00);
								BgL_arg1421z00_1726 = STRUCT_REF(BgL_sz00_2207, (int) (0L));
							}
							return
								((((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_varz00_20)))->BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_arg1421z00_1726)),
								BUNSPEC);
						}
					else
						{	/* BackEnd/cplib.scm 146 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &reset-global! */
	obj_t BGl_z62resetzd2globalz12za2zzbackend_cplibz00(obj_t BgL_envz00_2881,
		obj_t BgL_varz00_2882)
	{
		{	/* BackEnd/cplib.scm 134 */
			return
				BGl_resetzd2globalz12zc0zzbackend_cplibz00(
				((BgL_globalz00_bglt) BgL_varz00_2882));
		}

	}



/* set-variable-name! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_variablez00_bglt
		BgL_variablez00_21)
	{
		{	/* BackEnd/cplib.scm 152 */
			{	/* BackEnd/cplib.scm 154 */
				bool_t BgL_test1951z00_3241;

				{	/* BackEnd/cplib.scm 154 */
					obj_t BgL_tmpz00_3242;

					BgL_tmpz00_3242 =
						(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_21))->BgL_namez00);
					BgL_test1951z00_3241 = STRINGP(BgL_tmpz00_3242);
				}
				if (BgL_test1951z00_3241)
					{	/* BackEnd/cplib.scm 154 */
						return
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_21))->
							BgL_namez00);
					}
				else
					{	/* BackEnd/cplib.scm 156 */
						obj_t BgL_nz00_1733;

						{	/* BackEnd/cplib.scm 157 */
							bool_t BgL_test1952z00_3246;

							{	/* BackEnd/cplib.scm 157 */
								obj_t BgL_classz00_2210;

								BgL_classz00_2210 = BGl_globalz00zzast_varz00;
								{	/* BackEnd/cplib.scm 157 */
									BgL_objectz00_bglt BgL_arg1807z00_2212;

									{	/* BackEnd/cplib.scm 157 */
										obj_t BgL_tmpz00_3247;

										BgL_tmpz00_3247 = ((obj_t) BgL_variablez00_21);
										BgL_arg1807z00_2212 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3247);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* BackEnd/cplib.scm 157 */
											long BgL_idxz00_2218;

											BgL_idxz00_2218 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2212);
											BgL_test1952z00_3246 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2218 + 2L)) == BgL_classz00_2210);
										}
									else
										{	/* BackEnd/cplib.scm 157 */
											bool_t BgL_res1862z00_2243;

											{	/* BackEnd/cplib.scm 157 */
												obj_t BgL_oclassz00_2226;

												{	/* BackEnd/cplib.scm 157 */
													obj_t BgL_arg1815z00_2234;
													long BgL_arg1816z00_2235;

													BgL_arg1815z00_2234 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* BackEnd/cplib.scm 157 */
														long BgL_arg1817z00_2236;

														BgL_arg1817z00_2236 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2212);
														BgL_arg1816z00_2235 =
															(BgL_arg1817z00_2236 - OBJECT_TYPE);
													}
													BgL_oclassz00_2226 =
														VECTOR_REF(BgL_arg1815z00_2234,
														BgL_arg1816z00_2235);
												}
												{	/* BackEnd/cplib.scm 157 */
													bool_t BgL__ortest_1115z00_2227;

													BgL__ortest_1115z00_2227 =
														(BgL_classz00_2210 == BgL_oclassz00_2226);
													if (BgL__ortest_1115z00_2227)
														{	/* BackEnd/cplib.scm 157 */
															BgL_res1862z00_2243 = BgL__ortest_1115z00_2227;
														}
													else
														{	/* BackEnd/cplib.scm 157 */
															long BgL_odepthz00_2228;

															{	/* BackEnd/cplib.scm 157 */
																obj_t BgL_arg1804z00_2229;

																BgL_arg1804z00_2229 = (BgL_oclassz00_2226);
																BgL_odepthz00_2228 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2229);
															}
															if ((2L < BgL_odepthz00_2228))
																{	/* BackEnd/cplib.scm 157 */
																	obj_t BgL_arg1802z00_2231;

																	{	/* BackEnd/cplib.scm 157 */
																		obj_t BgL_arg1803z00_2232;

																		BgL_arg1803z00_2232 = (BgL_oclassz00_2226);
																		BgL_arg1802z00_2231 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2232, 2L);
																	}
																	BgL_res1862z00_2243 =
																		(BgL_arg1802z00_2231 == BgL_classz00_2210);
																}
															else
																{	/* BackEnd/cplib.scm 157 */
																	BgL_res1862z00_2243 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1952z00_3246 = BgL_res1862z00_2243;
										}
								}
							}
							if (BgL_test1952z00_3246)
								{	/* BackEnd/cplib.scm 159 */
									obj_t BgL_arg1448z00_1736;
									obj_t BgL_arg1453z00_1737;

									{	/* BackEnd/cplib.scm 159 */
										obj_t BgL_arg1454z00_1738;

										{	/* BackEnd/cplib.scm 159 */
											obj_t BgL__ortest_1112z00_1739;

											BgL__ortest_1112z00_1739 =
												(((BgL_globalz00_bglt) COBJECT(
														((BgL_globalz00_bglt) BgL_variablez00_21)))->
												BgL_aliasz00);
											if (CBOOL(BgL__ortest_1112z00_1739))
												{	/* BackEnd/cplib.scm 159 */
													BgL_arg1454z00_1738 = BgL__ortest_1112z00_1739;
												}
											else
												{	/* BackEnd/cplib.scm 159 */
													BgL_arg1454z00_1738 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt) BgL_variablez00_21))))->
														BgL_idz00);
												}
										}
										{	/* BackEnd/cplib.scm 159 */
											obj_t BgL_arg1455z00_2245;

											BgL_arg1455z00_2245 =
												SYMBOL_TO_STRING(((obj_t) BgL_arg1454z00_1738));
											BgL_arg1448z00_1736 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2245);
										}
									}
									{	/* BackEnd/cplib.scm 160 */
										obj_t BgL_arg1472z00_1740;

										BgL_arg1472z00_1740 =
											(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_variablez00_21)))->
											BgL_modulez00);
										{	/* BackEnd/cplib.scm 160 */
											obj_t BgL_arg1455z00_2247;

											BgL_arg1455z00_2247 =
												SYMBOL_TO_STRING(BgL_arg1472z00_1740);
											BgL_arg1453z00_1737 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2247);
										}
									}
									BgL_nz00_1733 =
										bigloo_module_mangle(BgL_arg1448z00_1736,
										BgL_arg1453z00_1737);
								}
							else
								{	/* BackEnd/cplib.scm 161 */
									bool_t BgL_test1957z00_3284;

									{	/* BackEnd/cplib.scm 161 */
										obj_t BgL_classz00_2248;

										BgL_classz00_2248 = BGl_localz00zzast_varz00;
										{	/* BackEnd/cplib.scm 161 */
											BgL_objectz00_bglt BgL_arg1807z00_2250;

											{	/* BackEnd/cplib.scm 161 */
												obj_t BgL_tmpz00_3285;

												BgL_tmpz00_3285 = ((obj_t) BgL_variablez00_21);
												BgL_arg1807z00_2250 =
													(BgL_objectz00_bglt) (BgL_tmpz00_3285);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* BackEnd/cplib.scm 161 */
													long BgL_idxz00_2256;

													BgL_idxz00_2256 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2250);
													BgL_test1957z00_3284 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2256 + 2L)) == BgL_classz00_2248);
												}
											else
												{	/* BackEnd/cplib.scm 161 */
													bool_t BgL_res1863z00_2281;

													{	/* BackEnd/cplib.scm 161 */
														obj_t BgL_oclassz00_2264;

														{	/* BackEnd/cplib.scm 161 */
															obj_t BgL_arg1815z00_2272;
															long BgL_arg1816z00_2273;

															BgL_arg1815z00_2272 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* BackEnd/cplib.scm 161 */
																long BgL_arg1817z00_2274;

																BgL_arg1817z00_2274 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2250);
																BgL_arg1816z00_2273 =
																	(BgL_arg1817z00_2274 - OBJECT_TYPE);
															}
															BgL_oclassz00_2264 =
																VECTOR_REF(BgL_arg1815z00_2272,
																BgL_arg1816z00_2273);
														}
														{	/* BackEnd/cplib.scm 161 */
															bool_t BgL__ortest_1115z00_2265;

															BgL__ortest_1115z00_2265 =
																(BgL_classz00_2248 == BgL_oclassz00_2264);
															if (BgL__ortest_1115z00_2265)
																{	/* BackEnd/cplib.scm 161 */
																	BgL_res1863z00_2281 =
																		BgL__ortest_1115z00_2265;
																}
															else
																{	/* BackEnd/cplib.scm 161 */
																	long BgL_odepthz00_2266;

																	{	/* BackEnd/cplib.scm 161 */
																		obj_t BgL_arg1804z00_2267;

																		BgL_arg1804z00_2267 = (BgL_oclassz00_2264);
																		BgL_odepthz00_2266 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2267);
																	}
																	if ((2L < BgL_odepthz00_2266))
																		{	/* BackEnd/cplib.scm 161 */
																			obj_t BgL_arg1802z00_2269;

																			{	/* BackEnd/cplib.scm 161 */
																				obj_t BgL_arg1803z00_2270;

																				BgL_arg1803z00_2270 =
																					(BgL_oclassz00_2264);
																				BgL_arg1802z00_2269 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2270, 2L);
																			}
																			BgL_res1863z00_2281 =
																				(BgL_arg1802z00_2269 ==
																				BgL_classz00_2248);
																		}
																	else
																		{	/* BackEnd/cplib.scm 161 */
																			BgL_res1863z00_2281 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1957z00_3284 = BgL_res1863z00_2281;
												}
										}
									}
									if (BgL_test1957z00_3284)
										{	/* BackEnd/cplib.scm 165 */
											obj_t BgL_arg1485z00_1743;

											{	/* BackEnd/cplib.scm 165 */
												obj_t BgL_arg1489z00_1744;
												obj_t BgL_arg1502z00_1745;

												{	/* BackEnd/cplib.scm 165 */
													obj_t BgL_arg1509z00_1746;

													BgL_arg1509z00_1746 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_variablez00_21))))->
														BgL_idz00);
													{	/* BackEnd/cplib.scm 165 */
														obj_t BgL_arg1455z00_2283;

														BgL_arg1455z00_2283 =
															SYMBOL_TO_STRING(BgL_arg1509z00_1746);
														BgL_arg1489z00_1744 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_2283);
													}
												}
												{	/* BackEnd/cplib.scm 165 */

													BgL_arg1502z00_1745 =
														BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
														((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
																		BgL_variablez00_21)))->BgL_keyz00), 10L);
												}
												BgL_arg1485z00_1743 =
													string_append_3(BgL_arg1489z00_1744,
													BGl_string1885z00zzbackend_cplibz00,
													BgL_arg1502z00_1745);
											}
											BgL_nz00_1733 = bigloo_mangle(BgL_arg1485z00_1743);
										}
									else
										{	/* BackEnd/cplib.scm 169 */
											obj_t BgL_arg1514z00_1750;

											BgL_arg1514z00_1750 =
												BGl_shapez00zztools_shapez00(
												((obj_t) BgL_variablez00_21));
											BgL_nz00_1733 =
												BGl_internalzd2errorzd2zztools_errorz00
												(BGl_string1886z00zzbackend_cplibz00,
												BGl_string1887z00zzbackend_cplibz00,
												BgL_arg1514z00_1750);
										}
								}
						}
						((((BgL_variablez00_bglt) COBJECT(BgL_variablez00_21))->
								BgL_namez00) = ((obj_t) BgL_nz00_1733), BUNSPEC);
						return BgL_nz00_1733;
					}
			}
		}

	}



/* &set-variable-name! */
	obj_t BGl_z62setzd2variablezd2namez12z70zzbackend_cplibz00(obj_t
		BgL_envz00_2883, obj_t BgL_variablez00_2884)
	{
		{	/* BackEnd/cplib.scm 152 */
			return
				BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_variablez00_2884));
		}

	}



/* global-entry */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_globalzd2entryzd2zzbackend_cplibz00(BgL_globalz00_bglt BgL_varz00_22)
	{
		{	/* BackEnd/cplib.scm 176 */
			{	/* BackEnd/cplib.scm 178 */
				BgL_valuez00_bglt BgL_valuez00_1752;

				BgL_valuez00_1752 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_22)))->BgL_valuez00);
				{	/* BackEnd/cplib.scm 179 */
					bool_t BgL_test1961z00_3325;

					{	/* BackEnd/cplib.scm 179 */
						obj_t BgL_classz00_2285;

						BgL_classz00_2285 = BGl_scnstz00zzast_varz00;
						{	/* BackEnd/cplib.scm 179 */
							BgL_objectz00_bglt BgL_arg1807z00_2287;

							{	/* BackEnd/cplib.scm 179 */
								obj_t BgL_tmpz00_3326;

								BgL_tmpz00_3326 =
									((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_1752));
								BgL_arg1807z00_2287 = (BgL_objectz00_bglt) (BgL_tmpz00_3326);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* BackEnd/cplib.scm 179 */
									long BgL_idxz00_2293;

									BgL_idxz00_2293 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2287);
									BgL_test1961z00_3325 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2293 + 2L)) == BgL_classz00_2285);
								}
							else
								{	/* BackEnd/cplib.scm 179 */
									bool_t BgL_res1864z00_2318;

									{	/* BackEnd/cplib.scm 179 */
										obj_t BgL_oclassz00_2301;

										{	/* BackEnd/cplib.scm 179 */
											obj_t BgL_arg1815z00_2309;
											long BgL_arg1816z00_2310;

											BgL_arg1815z00_2309 = (BGl_za2classesza2z00zz__objectz00);
											{	/* BackEnd/cplib.scm 179 */
												long BgL_arg1817z00_2311;

												BgL_arg1817z00_2311 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2287);
												BgL_arg1816z00_2310 =
													(BgL_arg1817z00_2311 - OBJECT_TYPE);
											}
											BgL_oclassz00_2301 =
												VECTOR_REF(BgL_arg1815z00_2309, BgL_arg1816z00_2310);
										}
										{	/* BackEnd/cplib.scm 179 */
											bool_t BgL__ortest_1115z00_2302;

											BgL__ortest_1115z00_2302 =
												(BgL_classz00_2285 == BgL_oclassz00_2301);
											if (BgL__ortest_1115z00_2302)
												{	/* BackEnd/cplib.scm 179 */
													BgL_res1864z00_2318 = BgL__ortest_1115z00_2302;
												}
											else
												{	/* BackEnd/cplib.scm 179 */
													long BgL_odepthz00_2303;

													{	/* BackEnd/cplib.scm 179 */
														obj_t BgL_arg1804z00_2304;

														BgL_arg1804z00_2304 = (BgL_oclassz00_2301);
														BgL_odepthz00_2303 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2304);
													}
													if ((2L < BgL_odepthz00_2303))
														{	/* BackEnd/cplib.scm 179 */
															obj_t BgL_arg1802z00_2306;

															{	/* BackEnd/cplib.scm 179 */
																obj_t BgL_arg1803z00_2307;

																BgL_arg1803z00_2307 = (BgL_oclassz00_2301);
																BgL_arg1802z00_2306 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2307,
																	2L);
															}
															BgL_res1864z00_2318 =
																(BgL_arg1802z00_2306 == BgL_classz00_2285);
														}
													else
														{	/* BackEnd/cplib.scm 179 */
															BgL_res1864z00_2318 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1961z00_3325 = BgL_res1864z00_2318;
								}
						}
					}
					if (BgL_test1961z00_3325)
						{
							BgL_variablez00_bglt BgL_auxz00_3349;

							{
								BgL_varz00_bglt BgL_auxz00_3350;

								{	/* BackEnd/cplib.scm 180 */
									obj_t BgL_pairz00_2321;

									BgL_pairz00_2321 =
										(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt)
													(((BgL_scnstz00_bglt) COBJECT(
																((BgL_scnstz00_bglt) BgL_valuez00_1752)))->
														BgL_nodez00))))->BgL_argsz00);
									BgL_auxz00_3350 = ((BgL_varz00_bglt) CAR(BgL_pairz00_2321));
								}
								BgL_auxz00_3349 =
									(((BgL_varz00_bglt) COBJECT(BgL_auxz00_3350))->
									BgL_variablez00);
							}
							return ((BgL_globalz00_bglt) BgL_auxz00_3349);
						}
					else
						{	/* BackEnd/cplib.scm 179 */
							return BgL_varz00_22;
						}
				}
			}
		}

	}



/* &global-entry */
	BgL_globalz00_bglt BGl_z62globalzd2entryzb0zzbackend_cplibz00(obj_t
		BgL_envz00_2885, obj_t BgL_varz00_2886)
	{
		{	/* BackEnd/cplib.scm 176 */
			return
				BGl_globalzd2entryzd2zzbackend_cplibz00(
				((BgL_globalz00_bglt) BgL_varz00_2886));
		}

	}



/* global-arity */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2arityzd2zzbackend_cplibz00(BgL_globalz00_bglt BgL_varz00_23)
	{
		{	/* BackEnd/cplib.scm 186 */
			{	/* BackEnd/cplib.scm 188 */
				BgL_valuez00_bglt BgL_valuez00_1757;

				BgL_valuez00_1757 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_23)))->BgL_valuez00);
				{	/* BackEnd/cplib.scm 190 */
					bool_t BgL_test1965z00_3363;

					{	/* BackEnd/cplib.scm 190 */
						obj_t BgL_classz00_2324;

						BgL_classz00_2324 = BGl_scnstz00zzast_varz00;
						{	/* BackEnd/cplib.scm 190 */
							BgL_objectz00_bglt BgL_arg1807z00_2326;

							{	/* BackEnd/cplib.scm 190 */
								obj_t BgL_tmpz00_3364;

								BgL_tmpz00_3364 =
									((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_1757));
								BgL_arg1807z00_2326 = (BgL_objectz00_bglt) (BgL_tmpz00_3364);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* BackEnd/cplib.scm 190 */
									long BgL_idxz00_2332;

									BgL_idxz00_2332 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2326);
									BgL_test1965z00_3363 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2332 + 2L)) == BgL_classz00_2324);
								}
							else
								{	/* BackEnd/cplib.scm 190 */
									bool_t BgL_res1865z00_2357;

									{	/* BackEnd/cplib.scm 190 */
										obj_t BgL_oclassz00_2340;

										{	/* BackEnd/cplib.scm 190 */
											obj_t BgL_arg1815z00_2348;
											long BgL_arg1816z00_2349;

											BgL_arg1815z00_2348 = (BGl_za2classesza2z00zz__objectz00);
											{	/* BackEnd/cplib.scm 190 */
												long BgL_arg1817z00_2350;

												BgL_arg1817z00_2350 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2326);
												BgL_arg1816z00_2349 =
													(BgL_arg1817z00_2350 - OBJECT_TYPE);
											}
											BgL_oclassz00_2340 =
												VECTOR_REF(BgL_arg1815z00_2348, BgL_arg1816z00_2349);
										}
										{	/* BackEnd/cplib.scm 190 */
											bool_t BgL__ortest_1115z00_2341;

											BgL__ortest_1115z00_2341 =
												(BgL_classz00_2324 == BgL_oclassz00_2340);
											if (BgL__ortest_1115z00_2341)
												{	/* BackEnd/cplib.scm 190 */
													BgL_res1865z00_2357 = BgL__ortest_1115z00_2341;
												}
											else
												{	/* BackEnd/cplib.scm 190 */
													long BgL_odepthz00_2342;

													{	/* BackEnd/cplib.scm 190 */
														obj_t BgL_arg1804z00_2343;

														BgL_arg1804z00_2343 = (BgL_oclassz00_2340);
														BgL_odepthz00_2342 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2343);
													}
													if ((2L < BgL_odepthz00_2342))
														{	/* BackEnd/cplib.scm 190 */
															obj_t BgL_arg1802z00_2345;

															{	/* BackEnd/cplib.scm 190 */
																obj_t BgL_arg1803z00_2346;

																BgL_arg1803z00_2346 = (BgL_oclassz00_2340);
																BgL_arg1802z00_2345 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2346,
																	2L);
															}
															BgL_res1865z00_2357 =
																(BgL_arg1802z00_2345 == BgL_classz00_2324);
														}
													else
														{	/* BackEnd/cplib.scm 190 */
															BgL_res1865z00_2357 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1965z00_3363 = BgL_res1865z00_2357;
								}
						}
					}
					if (BgL_test1965z00_3363)
						{	/* BackEnd/cplib.scm 193 */

							{	/* BackEnd/cplib.scm 194 */
								long BgL_tmpz00_3387;

								{
									BgL_funz00_bglt BgL_auxz00_3388;

									{
										BgL_sfunz00_bglt BgL_auxz00_3389;

										{
											BgL_valuez00_bglt BgL_auxz00_3390;

											{
												BgL_variablez00_bglt BgL_auxz00_3391;

												{
													obj_t BgL_auxz00_3392;

													{
														BgL_sfunz00_bglt BgL_auxz00_3393;

														{
															BgL_valuez00_bglt BgL_auxz00_3394;

															{
																BgL_variablez00_bglt BgL_auxz00_3395;

																{
																	BgL_globalz00_bglt BgL_auxz00_3396;

																	{
																		BgL_variablez00_bglt BgL_auxz00_3397;

																		{
																			BgL_varz00_bglt BgL_auxz00_3398;

																			{	/* BackEnd/cplib.scm 192 */
																				obj_t BgL_pairz00_2360;

																				BgL_pairz00_2360 =
																					(((BgL_appz00_bglt) COBJECT(
																							((BgL_appz00_bglt)
																								(((BgL_scnstz00_bglt) COBJECT(
																											((BgL_scnstz00_bglt)
																												BgL_valuez00_1757)))->
																									BgL_nodez00))))->BgL_argsz00);
																				BgL_auxz00_3398 =
																					((BgL_varz00_bglt)
																					CAR(BgL_pairz00_2360));
																			}
																			BgL_auxz00_3397 =
																				(((BgL_varz00_bglt)
																					COBJECT(BgL_auxz00_3398))->
																				BgL_variablez00);
																		}
																		BgL_auxz00_3396 =
																			((BgL_globalz00_bglt) BgL_auxz00_3397);
																	}
																	BgL_auxz00_3395 =
																		((BgL_variablez00_bglt) BgL_auxz00_3396);
																}
																BgL_auxz00_3394 =
																	(((BgL_variablez00_bglt)
																		COBJECT(BgL_auxz00_3395))->BgL_valuez00);
															}
															BgL_auxz00_3393 =
																((BgL_sfunz00_bglt) BgL_auxz00_3394);
														}
														BgL_auxz00_3392 =
															(((BgL_sfunz00_bglt) COBJECT(BgL_auxz00_3393))->
															BgL_thezd2closurezd2globalz00);
													}
													BgL_auxz00_3391 =
														((BgL_variablez00_bglt) BgL_auxz00_3392);
												}
												BgL_auxz00_3390 =
													(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_3391))->
													BgL_valuez00);
											}
											BgL_auxz00_3389 = ((BgL_sfunz00_bglt) BgL_auxz00_3390);
										}
										BgL_auxz00_3388 = ((BgL_funz00_bglt) BgL_auxz00_3389);
									}
									BgL_tmpz00_3387 =
										(((BgL_funz00_bglt) COBJECT(BgL_auxz00_3388))->
										BgL_arityz00);
								}
								return BINT(BgL_tmpz00_3387);
							}
						}
					else
						{	/* BackEnd/cplib.scm 196 */
							bool_t BgL_test1969z00_3417;

							{	/* BackEnd/cplib.scm 196 */
								obj_t BgL_classz00_2366;

								BgL_classz00_2366 = BGl_sfunz00zzast_varz00;
								{	/* BackEnd/cplib.scm 196 */
									BgL_objectz00_bglt BgL_arg1807z00_2368;

									{	/* BackEnd/cplib.scm 196 */
										obj_t BgL_tmpz00_3418;

										BgL_tmpz00_3418 =
											((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_1757));
										BgL_arg1807z00_2368 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3418);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* BackEnd/cplib.scm 196 */
											long BgL_idxz00_2374;

											BgL_idxz00_2374 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2368);
											BgL_test1969z00_3417 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2374 + 3L)) == BgL_classz00_2366);
										}
									else
										{	/* BackEnd/cplib.scm 196 */
											bool_t BgL_res1866z00_2399;

											{	/* BackEnd/cplib.scm 196 */
												obj_t BgL_oclassz00_2382;

												{	/* BackEnd/cplib.scm 196 */
													obj_t BgL_arg1815z00_2390;
													long BgL_arg1816z00_2391;

													BgL_arg1815z00_2390 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* BackEnd/cplib.scm 196 */
														long BgL_arg1817z00_2392;

														BgL_arg1817z00_2392 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2368);
														BgL_arg1816z00_2391 =
															(BgL_arg1817z00_2392 - OBJECT_TYPE);
													}
													BgL_oclassz00_2382 =
														VECTOR_REF(BgL_arg1815z00_2390,
														BgL_arg1816z00_2391);
												}
												{	/* BackEnd/cplib.scm 196 */
													bool_t BgL__ortest_1115z00_2383;

													BgL__ortest_1115z00_2383 =
														(BgL_classz00_2366 == BgL_oclassz00_2382);
													if (BgL__ortest_1115z00_2383)
														{	/* BackEnd/cplib.scm 196 */
															BgL_res1866z00_2399 = BgL__ortest_1115z00_2383;
														}
													else
														{	/* BackEnd/cplib.scm 196 */
															long BgL_odepthz00_2384;

															{	/* BackEnd/cplib.scm 196 */
																obj_t BgL_arg1804z00_2385;

																BgL_arg1804z00_2385 = (BgL_oclassz00_2382);
																BgL_odepthz00_2384 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2385);
															}
															if ((3L < BgL_odepthz00_2384))
																{	/* BackEnd/cplib.scm 196 */
																	obj_t BgL_arg1802z00_2387;

																	{	/* BackEnd/cplib.scm 196 */
																		obj_t BgL_arg1803z00_2388;

																		BgL_arg1803z00_2388 = (BgL_oclassz00_2382);
																		BgL_arg1802z00_2387 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2388, 3L);
																	}
																	BgL_res1866z00_2399 =
																		(BgL_arg1802z00_2387 == BgL_classz00_2366);
																}
															else
																{	/* BackEnd/cplib.scm 196 */
																	BgL_res1866z00_2399 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1969z00_3417 = BgL_res1866z00_2399;
										}
								}
							}
							if (BgL_test1969z00_3417)
								{	/* BackEnd/cplib.scm 197 */
									long BgL_xz00_1767;

									BgL_xz00_1767 =
										(((BgL_funz00_bglt) COBJECT(
												((BgL_funz00_bglt)
													((BgL_sfunz00_bglt) BgL_valuez00_1757))))->
										BgL_arityz00);
									if ((BgL_xz00_1767 >= 0L))
										{	/* BackEnd/cplib.scm 198 */
											obj_t BgL_za71za7_2402;

											BgL_za71za7_2402 = BINT(BgL_xz00_1767);
											{	/* BackEnd/cplib.scm 198 */
												obj_t BgL_tmpz00_2404;

												BgL_tmpz00_2404 = BINT(0L);
												{	/* BackEnd/cplib.scm 198 */
													bool_t BgL_test1974z00_3448;

													{	/* BackEnd/cplib.scm 198 */
														obj_t BgL_tmpz00_3449;

														BgL_tmpz00_3449 = BINT(1L);
														BgL_test1974z00_3448 =
															BGL_SUBFX_OV(BgL_za71za7_2402, BgL_tmpz00_3449,
															BgL_tmpz00_2404);
													}
													if (BgL_test1974z00_3448)
														{	/* BackEnd/cplib.scm 198 */
															return
																bgl_bignum_sub(bgl_long_to_bignum(
																	(long) CINT(BgL_za71za7_2402)),
																CNST_TABLE_REF(3));
														}
													else
														{	/* BackEnd/cplib.scm 198 */
															return BgL_tmpz00_2404;
														}
												}
											}
										}
									else
										{	/* BackEnd/cplib.scm 198 */
											obj_t BgL_za71za7_2412;

											BgL_za71za7_2412 = BINT(BgL_xz00_1767);
											{	/* BackEnd/cplib.scm 198 */
												obj_t BgL_tmpz00_2414;

												BgL_tmpz00_2414 = BINT(0L);
												{	/* BackEnd/cplib.scm 198 */
													bool_t BgL_test1975z00_3458;

													{	/* BackEnd/cplib.scm 198 */
														obj_t BgL_tmpz00_3459;

														BgL_tmpz00_3459 = BINT(1L);
														BgL_test1975z00_3458 =
															BGL_ADDFX_OV(BgL_za71za7_2412, BgL_tmpz00_3459,
															BgL_tmpz00_2414);
													}
													if (BgL_test1975z00_3458)
														{	/* BackEnd/cplib.scm 198 */
															return
																bgl_bignum_add(bgl_long_to_bignum(
																	(long) CINT(BgL_za71za7_2412)),
																CNST_TABLE_REF(3));
														}
													else
														{	/* BackEnd/cplib.scm 198 */
															return BgL_tmpz00_2414;
														}
												}
											}
										}
								}
							else
								{	/* BackEnd/cplib.scm 196 */
									return BFALSE;
								}
						}
				}
			}
		}

	}



/* &global-arity */
	obj_t BGl_z62globalzd2arityzb0zzbackend_cplibz00(obj_t BgL_envz00_2887,
		obj_t BgL_varz00_2888)
	{
		{	/* BackEnd/cplib.scm 186 */
			return
				BGl_globalzd2arityzd2zzbackend_cplibz00(
				((BgL_globalz00_bglt) BgL_varz00_2888));
		}

	}



/* for-each-declared-classes */
	BGL_EXPORTED_DEF obj_t
		BGl_forzd2eachzd2declaredzd2classeszd2zzbackend_cplibz00(obj_t
		BgL_funz00_24)
	{
		{	/* BackEnd/cplib.scm 205 */
			{	/* BackEnd/cplib.scm 206 */
				obj_t BgL_g1249z00_1769;

				BgL_g1249z00_1769 = BGl_getzd2declaredzd2classesz00zzbackend_cplibz00();
				{
					obj_t BgL_l1247z00_1771;

					{	/* BackEnd/cplib.scm 215 */
						bool_t BgL_tmpz00_3469;

						BgL_l1247z00_1771 = BgL_g1249z00_1769;
					BgL_zc3z04anonymousza31564ze3z87_1772:
						if (PAIRP(BgL_l1247z00_1771))
							{	/* BackEnd/cplib.scm 215 */
								{	/* BackEnd/cplib.scm 207 */
									obj_t BgL_cz00_1774;

									BgL_cz00_1774 = CAR(BgL_l1247z00_1771);
									{	/* BackEnd/cplib.scm 207 */
										bool_t BgL_widezf3zf3_1775;

										BgL_widezf3zf3_1775 =
											BGl_widezd2classzf3z21zzobject_classz00(BgL_cz00_1774);
										{	/* BackEnd/cplib.scm 212 */
											obj_t BgL_arg1571z00_1776;
											obj_t BgL_arg1573z00_1777;

											if (BgL_widezf3zf3_1775)
												{	/* BackEnd/cplib.scm 212 */
													BgL_arg1571z00_1776 =
														BGl_widezd2ze3chunkz31zzbackend_cplibz00(
														((BgL_typez00_bglt) BgL_cz00_1774));
												}
											else
												{	/* BackEnd/cplib.scm 212 */
													BgL_arg1571z00_1776 = BgL_cz00_1774;
												}
											if (BgL_widezf3zf3_1775)
												{	/* BackEnd/cplib.scm 214 */
													BgL_arg1573z00_1777 =
														((obj_t)
														BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(0)));
												}
											else
												{
													BgL_tclassz00_bglt BgL_auxz00_3481;

													{
														obj_t BgL_auxz00_3482;

														{	/* BackEnd/cplib.scm 214 */
															BgL_objectz00_bglt BgL_tmpz00_3483;

															BgL_tmpz00_3483 =
																((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_cz00_1774));
															BgL_auxz00_3482 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3483);
														}
														BgL_auxz00_3481 =
															((BgL_tclassz00_bglt) BgL_auxz00_3482);
													}
													BgL_arg1573z00_1777 =
														(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3481))->
														BgL_itszd2superzd2);
												}
											BGL_PROCEDURE_CALL3(BgL_funz00_24, BgL_cz00_1774,
												BgL_arg1571z00_1776, BgL_arg1573z00_1777);
										}
									}
								}
								{
									obj_t BgL_l1247z00_3495;

									BgL_l1247z00_3495 = CDR(BgL_l1247z00_1771);
									BgL_l1247z00_1771 = BgL_l1247z00_3495;
									goto BgL_zc3z04anonymousza31564ze3z87_1772;
								}
							}
						else
							{	/* BackEnd/cplib.scm 215 */
								BgL_tmpz00_3469 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_3469);
					}
				}
			}
		}

	}



/* &for-each-declared-classes */
	obj_t BGl_z62forzd2eachzd2declaredzd2classeszb0zzbackend_cplibz00(obj_t
		BgL_envz00_2889, obj_t BgL_funz00_2890)
	{
		{	/* BackEnd/cplib.scm 205 */
			return
				BGl_forzd2eachzd2declaredzd2classeszd2zzbackend_cplibz00
				(BgL_funz00_2890);
		}

	}



/* get-declared-classes */
	BGL_EXPORTED_DEF obj_t BGl_getzd2declaredzd2classesz00zzbackend_cplibz00(void)
	{
		{	/* BackEnd/cplib.scm 220 */
			{	/* BackEnd/cplib.scm 223 */
				obj_t BgL_rz00_1780;

				BgL_rz00_1780 = BNIL;
				{	/* BackEnd/cplib.scm 224 */
					obj_t BgL_g1252z00_1781;

					BgL_g1252z00_1781 = BGl_getzd2classzd2listz00zzobject_classz00();
					{
						obj_t BgL_l1250z00_1783;

						BgL_l1250z00_1783 = BgL_g1252z00_1781;
					BgL_zc3z04anonymousza31576ze3z87_1784:
						if (PAIRP(BgL_l1250z00_1783))
							{	/* BackEnd/cplib.scm 229 */
								{	/* BackEnd/cplib.scm 225 */
									obj_t BgL_cz00_1786;

									BgL_cz00_1786 = CAR(BgL_l1250z00_1783);
									{	/* BackEnd/cplib.scm 225 */
										bool_t BgL_test1980z00_3503;

										{	/* BackEnd/cplib.scm 225 */
											bool_t BgL_test1981z00_3504;

											{	/* BackEnd/cplib.scm 225 */
												obj_t BgL_arg1605z00_1798;

												{	/* BackEnd/cplib.scm 225 */
													BgL_globalz00_bglt BgL_arg1606z00_1799;

													{
														BgL_tclassz00_bglt BgL_auxz00_3505;

														{
															obj_t BgL_auxz00_3506;

															{	/* BackEnd/cplib.scm 225 */
																BgL_objectz00_bglt BgL_tmpz00_3507;

																BgL_tmpz00_3507 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_cz00_1786));
																BgL_auxz00_3506 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3507);
															}
															BgL_auxz00_3505 =
																((BgL_tclassz00_bglt) BgL_auxz00_3506);
														}
														BgL_arg1606z00_1799 =
															(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3505))->
															BgL_holderz00);
													}
													BgL_arg1605z00_1798 =
														(((BgL_globalz00_bglt)
															COBJECT(BgL_arg1606z00_1799))->BgL_importz00);
												}
												BgL_test1981z00_3504 =
													(BgL_arg1605z00_1798 == CNST_TABLE_REF(4));
											}
											if (BgL_test1981z00_3504)
												{	/* BackEnd/cplib.scm 225 */
													BgL_test1980z00_3503 = ((bool_t) 1);
												}
											else
												{	/* BackEnd/cplib.scm 225 */
													BgL_test1980z00_3503 =
														(BgL_cz00_1786 ==
														BGl_getzd2objectzd2typez00zztype_cachez00());
												}
										}
										if (BgL_test1980z00_3503)
											{	/* BackEnd/cplib.scm 225 */
												BFALSE;
											}
										else
											{	/* BackEnd/cplib.scm 227 */
												obj_t BgL_arg1595z00_1794;

												if (BGl_widezd2classzf3z21zzobject_classz00
													(BgL_cz00_1786))
													{	/* BackEnd/cplib.scm 227 */
														BgL_arg1595z00_1794 =
															BGl_widezd2ze3chunkz31zzbackend_cplibz00(
															((BgL_typez00_bglt) BgL_cz00_1786));
													}
												else
													{	/* BackEnd/cplib.scm 227 */
														BgL_arg1595z00_1794 = BgL_cz00_1786;
													}
												BgL_rz00_1780 =
													MAKE_YOUNG_PAIR(BgL_arg1595z00_1794, BgL_rz00_1780);
											}
									}
								}
								{
									obj_t BgL_l1250z00_3523;

									BgL_l1250z00_3523 = CDR(BgL_l1250z00_1783);
									BgL_l1250z00_1783 = BgL_l1250z00_3523;
									goto BgL_zc3z04anonymousza31576ze3z87_1784;
								}
							}
						else
							{	/* BackEnd/cplib.scm 229 */
								((bool_t) 1);
							}
					}
				}
				return BgL_rz00_1780;
			}
		}

	}



/* &get-declared-classes */
	obj_t BGl_z62getzd2declaredzd2classesz62zzbackend_cplibz00(obj_t
		BgL_envz00_2891)
	{
		{	/* BackEnd/cplib.scm 220 */
			return BGl_getzd2declaredzd2classesz00zzbackend_cplibz00();
		}

	}



/* type->class */
	obj_t BGl_typezd2ze3classz31zzbackend_cplibz00(BgL_typez00_bglt
		BgL_typez00_25)
	{
		{	/* BackEnd/cplib.scm 235 */
			{	/* BackEnd/cplib.scm 237 */
				bool_t BgL_test1983z00_3526;

				{	/* BackEnd/cplib.scm 237 */
					obj_t BgL_classz00_2431;

					BgL_classz00_2431 = BGl_tclassz00zzobject_classz00;
					{	/* BackEnd/cplib.scm 237 */
						BgL_objectz00_bglt BgL_arg1807z00_2433;

						{	/* BackEnd/cplib.scm 237 */
							obj_t BgL_tmpz00_3527;

							BgL_tmpz00_3527 = ((obj_t) ((BgL_objectz00_bglt) BgL_typez00_25));
							BgL_arg1807z00_2433 = (BgL_objectz00_bglt) (BgL_tmpz00_3527);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* BackEnd/cplib.scm 237 */
								long BgL_idxz00_2439;

								BgL_idxz00_2439 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2433);
								BgL_test1983z00_3526 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2439 + 2L)) == BgL_classz00_2431);
							}
						else
							{	/* BackEnd/cplib.scm 237 */
								bool_t BgL_res1867z00_2464;

								{	/* BackEnd/cplib.scm 237 */
									obj_t BgL_oclassz00_2447;

									{	/* BackEnd/cplib.scm 237 */
										obj_t BgL_arg1815z00_2455;
										long BgL_arg1816z00_2456;

										BgL_arg1815z00_2455 = (BGl_za2classesza2z00zz__objectz00);
										{	/* BackEnd/cplib.scm 237 */
											long BgL_arg1817z00_2457;

											BgL_arg1817z00_2457 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2433);
											BgL_arg1816z00_2456 = (BgL_arg1817z00_2457 - OBJECT_TYPE);
										}
										BgL_oclassz00_2447 =
											VECTOR_REF(BgL_arg1815z00_2455, BgL_arg1816z00_2456);
									}
									{	/* BackEnd/cplib.scm 237 */
										bool_t BgL__ortest_1115z00_2448;

										BgL__ortest_1115z00_2448 =
											(BgL_classz00_2431 == BgL_oclassz00_2447);
										if (BgL__ortest_1115z00_2448)
											{	/* BackEnd/cplib.scm 237 */
												BgL_res1867z00_2464 = BgL__ortest_1115z00_2448;
											}
										else
											{	/* BackEnd/cplib.scm 237 */
												long BgL_odepthz00_2449;

												{	/* BackEnd/cplib.scm 237 */
													obj_t BgL_arg1804z00_2450;

													BgL_arg1804z00_2450 = (BgL_oclassz00_2447);
													BgL_odepthz00_2449 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2450);
												}
												if ((2L < BgL_odepthz00_2449))
													{	/* BackEnd/cplib.scm 237 */
														obj_t BgL_arg1802z00_2452;

														{	/* BackEnd/cplib.scm 237 */
															obj_t BgL_arg1803z00_2453;

															BgL_arg1803z00_2453 = (BgL_oclassz00_2447);
															BgL_arg1802z00_2452 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2453,
																2L);
														}
														BgL_res1867z00_2464 =
															(BgL_arg1802z00_2452 == BgL_classz00_2431);
													}
												else
													{	/* BackEnd/cplib.scm 237 */
														BgL_res1867z00_2464 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test1983z00_3526 = BgL_res1867z00_2464;
							}
					}
				}
				if (BgL_test1983z00_3526)
					{	/* BackEnd/cplib.scm 237 */
						return ((obj_t) BgL_typez00_25);
					}
				else
					{	/* BackEnd/cplib.scm 239 */
						bool_t BgL_test1987z00_3551;

						{	/* BackEnd/cplib.scm 239 */
							obj_t BgL_classz00_2465;

							BgL_classz00_2465 = BGl_wclassz00zzobject_classz00;
							{	/* BackEnd/cplib.scm 239 */
								BgL_objectz00_bglt BgL_arg1807z00_2467;

								{	/* BackEnd/cplib.scm 239 */
									obj_t BgL_tmpz00_3552;

									BgL_tmpz00_3552 =
										((obj_t) ((BgL_objectz00_bglt) BgL_typez00_25));
									BgL_arg1807z00_2467 = (BgL_objectz00_bglt) (BgL_tmpz00_3552);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* BackEnd/cplib.scm 239 */
										long BgL_idxz00_2473;

										BgL_idxz00_2473 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2467);
										BgL_test1987z00_3551 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2473 + 2L)) == BgL_classz00_2465);
									}
								else
									{	/* BackEnd/cplib.scm 239 */
										bool_t BgL_res1868z00_2498;

										{	/* BackEnd/cplib.scm 239 */
											obj_t BgL_oclassz00_2481;

											{	/* BackEnd/cplib.scm 239 */
												obj_t BgL_arg1815z00_2489;
												long BgL_arg1816z00_2490;

												BgL_arg1815z00_2489 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* BackEnd/cplib.scm 239 */
													long BgL_arg1817z00_2491;

													BgL_arg1817z00_2491 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2467);
													BgL_arg1816z00_2490 =
														(BgL_arg1817z00_2491 - OBJECT_TYPE);
												}
												BgL_oclassz00_2481 =
													VECTOR_REF(BgL_arg1815z00_2489, BgL_arg1816z00_2490);
											}
											{	/* BackEnd/cplib.scm 239 */
												bool_t BgL__ortest_1115z00_2482;

												BgL__ortest_1115z00_2482 =
													(BgL_classz00_2465 == BgL_oclassz00_2481);
												if (BgL__ortest_1115z00_2482)
													{	/* BackEnd/cplib.scm 239 */
														BgL_res1868z00_2498 = BgL__ortest_1115z00_2482;
													}
												else
													{	/* BackEnd/cplib.scm 239 */
														long BgL_odepthz00_2483;

														{	/* BackEnd/cplib.scm 239 */
															obj_t BgL_arg1804z00_2484;

															BgL_arg1804z00_2484 = (BgL_oclassz00_2481);
															BgL_odepthz00_2483 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2484);
														}
														if ((2L < BgL_odepthz00_2483))
															{	/* BackEnd/cplib.scm 239 */
																obj_t BgL_arg1802z00_2486;

																{	/* BackEnd/cplib.scm 239 */
																	obj_t BgL_arg1803z00_2487;

																	BgL_arg1803z00_2487 = (BgL_oclassz00_2481);
																	BgL_arg1802z00_2486 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2487,
																		2L);
																}
																BgL_res1868z00_2498 =
																	(BgL_arg1802z00_2486 == BgL_classz00_2465);
															}
														else
															{	/* BackEnd/cplib.scm 239 */
																BgL_res1868z00_2498 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1987z00_3551 = BgL_res1868z00_2498;
									}
							}
						}
						if (BgL_test1987z00_3551)
							{
								BgL_wclassz00_bglt BgL_auxz00_3575;

								{
									obj_t BgL_auxz00_3576;

									{	/* BackEnd/cplib.scm 240 */
										BgL_objectz00_bglt BgL_tmpz00_3577;

										BgL_tmpz00_3577 =
											((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_typez00_25));
										BgL_auxz00_3576 = BGL_OBJECT_WIDENING(BgL_tmpz00_3577);
									}
									BgL_auxz00_3575 = ((BgL_wclassz00_bglt) BgL_auxz00_3576);
								}
								return
									(((BgL_wclassz00_bglt) COBJECT(BgL_auxz00_3575))->
									BgL_itszd2classzd2);
							}
						else
							{	/* BackEnd/cplib.scm 239 */
								return
									BGl_internalzd2errorzd2zztools_errorz00(CNST_TABLE_REF(5),
									BGl_string1888z00zzbackend_cplibz00,
									((obj_t) BgL_typez00_25));
							}
					}
			}
		}

	}



/* get-its-super */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2itszd2superz00zzbackend_cplibz00(BgL_typez00_bglt BgL_typez00_26)
	{
		{	/* BackEnd/cplib.scm 247 */
			{	/* BackEnd/cplib.scm 248 */
				obj_t BgL_arg1613z00_2501;

				BgL_arg1613z00_2501 =
					BGl_typezd2ze3classz31zzbackend_cplibz00(BgL_typez00_26);
				{
					obj_t BgL_auxz00_3587;

					{
						BgL_tclassz00_bglt BgL_auxz00_3588;

						{
							obj_t BgL_auxz00_3589;

							{	/* BackEnd/cplib.scm 248 */
								BgL_objectz00_bglt BgL_tmpz00_3590;

								BgL_tmpz00_3590 =
									((BgL_objectz00_bglt)
									((BgL_typez00_bglt) BgL_arg1613z00_2501));
								BgL_auxz00_3589 = BGL_OBJECT_WIDENING(BgL_tmpz00_3590);
							}
							BgL_auxz00_3588 = ((BgL_tclassz00_bglt) BgL_auxz00_3589);
						}
						BgL_auxz00_3587 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3588))->
							BgL_itszd2superzd2);
					}
					return ((BgL_typez00_bglt) BgL_auxz00_3587);
				}
			}
		}

	}



/* &get-its-super */
	BgL_typez00_bglt BGl_z62getzd2itszd2superz62zzbackend_cplibz00(obj_t
		BgL_envz00_2892, obj_t BgL_typez00_2893)
	{
		{	/* BackEnd/cplib.scm 247 */
			return
				BGl_getzd2itszd2superz00zzbackend_cplibz00(
				((BgL_typez00_bglt) BgL_typez00_2893));
		}

	}



/* get-declared-fields */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2declaredzd2fieldsz00zzbackend_cplibz00(BgL_typez00_bglt
		BgL_typez00_27)
	{
		{	/* BackEnd/cplib.scm 253 */
			{	/* BackEnd/cplib.scm 255 */
				obj_t BgL_classz00_1805;

				BgL_classz00_1805 =
					BGl_typezd2ze3classz31zzbackend_cplibz00(BgL_typez00_27);
				{	/* BackEnd/cplib.scm 256 */
					obj_t BgL_hook1257z00_1806;

					BgL_hook1257z00_1806 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
					{	/* BackEnd/cplib.scm 259 */
						obj_t BgL_g1258z00_1807;

						{
							BgL_tclassz00_bglt BgL_auxz00_3601;

							{
								obj_t BgL_auxz00_3602;

								{	/* BackEnd/cplib.scm 259 */
									BgL_objectz00_bglt BgL_tmpz00_3603;

									BgL_tmpz00_3603 =
										((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_classz00_1805));
									BgL_auxz00_3602 = BGL_OBJECT_WIDENING(BgL_tmpz00_3603);
								}
								BgL_auxz00_3601 = ((BgL_tclassz00_bglt) BgL_auxz00_3602);
							}
							BgL_g1258z00_1807 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3601))->BgL_slotsz00);
						}
						{
							obj_t BgL_l1254z00_1809;
							obj_t BgL_h1255z00_1810;

							BgL_l1254z00_1809 = BgL_g1258z00_1807;
							BgL_h1255z00_1810 = BgL_hook1257z00_1806;
						BgL_zc3z04anonymousza31614ze3z87_1811:
							if (NULLP(BgL_l1254z00_1809))
								{	/* BackEnd/cplib.scm 259 */
									return CDR(BgL_hook1257z00_1806);
								}
							else
								{	/* BackEnd/cplib.scm 259 */
									bool_t BgL_test1992z00_3612;

									{	/* BackEnd/cplib.scm 257 */
										BgL_slotz00_bglt BgL_slotz00_1821;

										BgL_slotz00_1821 =
											((BgL_slotz00_bglt) CAR(((obj_t) BgL_l1254z00_1809)));
										if (BGl_slotzd2virtualzf3z21zzobject_slotsz00
											(BgL_slotz00_1821))
											{	/* BackEnd/cplib.scm 257 */
												BgL_test1992z00_3612 = ((bool_t) 0);
											}
										else
											{	/* BackEnd/cplib.scm 257 */
												BgL_test1992z00_3612 =
													(BgL_classz00_1805 ==
													(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_1821))->
														BgL_classzd2ownerzd2));
											}
									}
									if (BgL_test1992z00_3612)
										{	/* BackEnd/cplib.scm 259 */
											obj_t BgL_nh1256z00_1817;

											{	/* BackEnd/cplib.scm 259 */
												obj_t BgL_arg1626z00_1819;

												BgL_arg1626z00_1819 = CAR(((obj_t) BgL_l1254z00_1809));
												BgL_nh1256z00_1817 =
													MAKE_YOUNG_PAIR(BgL_arg1626z00_1819, BNIL);
											}
											SET_CDR(BgL_h1255z00_1810, BgL_nh1256z00_1817);
											{	/* BackEnd/cplib.scm 259 */
												obj_t BgL_arg1625z00_1818;

												BgL_arg1625z00_1818 = CDR(((obj_t) BgL_l1254z00_1809));
												{
													obj_t BgL_h1255z00_3627;
													obj_t BgL_l1254z00_3626;

													BgL_l1254z00_3626 = BgL_arg1625z00_1818;
													BgL_h1255z00_3627 = BgL_nh1256z00_1817;
													BgL_h1255z00_1810 = BgL_h1255z00_3627;
													BgL_l1254z00_1809 = BgL_l1254z00_3626;
													goto BgL_zc3z04anonymousza31614ze3z87_1811;
												}
											}
										}
									else
										{	/* BackEnd/cplib.scm 259 */
											obj_t BgL_arg1627z00_1820;

											BgL_arg1627z00_1820 = CDR(((obj_t) BgL_l1254z00_1809));
											{
												obj_t BgL_l1254z00_3630;

												BgL_l1254z00_3630 = BgL_arg1627z00_1820;
												BgL_l1254z00_1809 = BgL_l1254z00_3630;
												goto BgL_zc3z04anonymousza31614ze3z87_1811;
											}
										}
								}
						}
					}
				}
			}
		}

	}



/* &get-declared-fields */
	obj_t BGl_z62getzd2declaredzd2fieldsz62zzbackend_cplibz00(obj_t
		BgL_envz00_2894, obj_t BgL_typez00_2895)
	{
		{	/* BackEnd/cplib.scm 253 */
			return
				BGl_getzd2declaredzd2fieldsz00zzbackend_cplibz00(
				((BgL_typez00_bglt) BgL_typez00_2895));
		}

	}



/* get-field-type */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2fieldzd2typez00zzbackend_cplibz00(BgL_slotz00_bglt BgL_slotz00_28)
	{
		{	/* BackEnd/cplib.scm 264 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_slotz00_28))->BgL_typez00);
		}

	}



/* &get-field-type */
	obj_t BGl_z62getzd2fieldzd2typez62zzbackend_cplibz00(obj_t BgL_envz00_2896,
		obj_t BgL_slotz00_2897)
	{
		{	/* BackEnd/cplib.scm 264 */
			return
				BGl_getzd2fieldzd2typez00zzbackend_cplibz00(
				((BgL_slotz00_bglt) BgL_slotz00_2897));
		}

	}



/* wide->chunk */
	BGL_EXPORTED_DEF obj_t
		BGl_widezd2ze3chunkz31zzbackend_cplibz00(BgL_typez00_bglt BgL_cz00_29)
	{
		{	/* BackEnd/cplib.scm 271 */
			{	/* BackEnd/cplib.scm 275 */
				bool_t BgL_test1994z00_3636;

				{	/* BackEnd/cplib.scm 275 */
					BgL_typez00_bglt BgL_arg1654z00_1830;
					obj_t BgL_arg1661z00_1831;

					{	/* BackEnd/cplib.scm 275 */
						obj_t BgL_arg1663z00_1832;

						{	/* BackEnd/cplib.scm 275 */
							obj_t BgL_arg1675z00_1833;

							BgL_arg1675z00_1833 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_cz00_29)))->BgL_idz00);
							BgL_arg1663z00_1832 =
								BGl_widezd2chunkzd2classzd2idzd2zzobject_classz00
								(BgL_arg1675z00_1833);
						}
						BgL_arg1654z00_1830 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1663z00_1832);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_3641;

						{
							obj_t BgL_auxz00_3642;

							{	/* BackEnd/cplib.scm 276 */
								BgL_objectz00_bglt BgL_tmpz00_3643;

								BgL_tmpz00_3643 = ((BgL_objectz00_bglt) BgL_cz00_29);
								BgL_auxz00_3642 = BGL_OBJECT_WIDENING(BgL_tmpz00_3643);
							}
							BgL_auxz00_3641 = ((BgL_tclassz00_bglt) BgL_auxz00_3642);
						}
						BgL_arg1661z00_1831 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3641))->
							BgL_widezd2typezd2);
					}
					BgL_test1994z00_3636 =
						(((obj_t) BgL_arg1654z00_1830) == BgL_arg1661z00_1831);
				}
				if (BgL_test1994z00_3636)
					{
						BgL_tclassz00_bglt BgL_auxz00_3650;

						{
							obj_t BgL_auxz00_3651;

							{	/* BackEnd/cplib.scm 278 */
								BgL_objectz00_bglt BgL_tmpz00_3652;

								BgL_tmpz00_3652 = ((BgL_objectz00_bglt) BgL_cz00_29);
								BgL_auxz00_3651 = BGL_OBJECT_WIDENING(BgL_tmpz00_3652);
							}
							BgL_auxz00_3650 = ((BgL_tclassz00_bglt) BgL_auxz00_3651);
						}
						return
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3650))->
							BgL_widezd2typezd2);
					}
				else
					{	/* BackEnd/cplib.scm 275 */
						return
							BGl_errorz00zz__errorz00(BGl_string1889z00zzbackend_cplibz00,
							BGl_string1890z00zzbackend_cplibz00, ((obj_t) BgL_cz00_29));
					}
			}
		}

	}



/* &wide->chunk */
	obj_t BGl_z62widezd2ze3chunkz53zzbackend_cplibz00(obj_t BgL_envz00_2898,
		obj_t BgL_cz00_2899)
	{
		{	/* BackEnd/cplib.scm 271 */
			return
				BGl_widezd2ze3chunkz31zzbackend_cplibz00(
				((BgL_typez00_bglt) BgL_cz00_2899));
		}

	}



/* get-declared-global-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2declaredzd2globalzd2variableszd2zzbackend_cplibz00(obj_t
		BgL_modulez00_30)
	{
		{	/* BackEnd/cplib.scm 283 */
			{	/* BackEnd/cplib.scm 285 */
				obj_t BgL_rz00_2908;

				BgL_rz00_2908 = MAKE_CELL(BNIL);
				{	/* BackEnd/cplib.scm 287 */
					obj_t BgL_zc3z04anonymousza31680ze3z87_2900;

					BgL_zc3z04anonymousza31680ze3z87_2900 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31680ze3ze5zzbackend_cplibz00, (int) (1L),
						(int) (2L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31680ze3z87_2900, (int) (0L),
						((obj_t) BgL_rz00_2908));
					PROCEDURE_SET(BgL_zc3z04anonymousza31680ze3z87_2900, (int) (1L),
						BgL_modulez00_30);
					BGl_forzd2eachzd2globalz12z12zzast_envz00
						(BgL_zc3z04anonymousza31680ze3z87_2900, BNIL);
				}
				return CELL_REF(BgL_rz00_2908);
			}
		}

	}



/* &get-declared-global-variables */
	obj_t BGl_z62getzd2declaredzd2globalzd2variableszb0zzbackend_cplibz00(obj_t
		BgL_envz00_2901, obj_t BgL_modulez00_2902)
	{
		{	/* BackEnd/cplib.scm 283 */
			return
				BGl_getzd2declaredzd2globalzd2variableszd2zzbackend_cplibz00
				(BgL_modulez00_2902);
		}

	}



/* &<@anonymous:1680> */
	obj_t BGl_z62zc3z04anonymousza31680ze3ze5zzbackend_cplibz00(obj_t
		BgL_envz00_2903, obj_t BgL_vz00_2906)
	{
		{	/* BackEnd/cplib.scm 286 */
			{	/* BackEnd/cplib.scm 287 */
				obj_t BgL_rz00_2904;
				obj_t BgL_modulez00_2905;

				BgL_rz00_2904 = PROCEDURE_REF(BgL_envz00_2903, (int) (0L));
				BgL_modulez00_2905 = PROCEDURE_REF(BgL_envz00_2903, (int) (1L));
				{	/* BackEnd/cplib.scm 287 */
					bool_t BgL_test1995z00_3675;

					if (
						((((BgL_globalz00_bglt) COBJECT(
										((BgL_globalz00_bglt) BgL_vz00_2906)))->BgL_modulez00) ==
							BgL_modulez00_2905))
						{	/* BackEnd/cplib.scm 288 */
							bool_t BgL_test1997z00_3680;

							{	/* BackEnd/cplib.scm 288 */
								BgL_valuez00_bglt BgL_arg1710z00_2932;

								BgL_arg1710z00_2932 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_vz00_2906))))->BgL_valuez00);
								{	/* BackEnd/cplib.scm 288 */
									obj_t BgL_classz00_2933;

									BgL_classz00_2933 = BGl_cfunz00zzast_varz00;
									{	/* BackEnd/cplib.scm 288 */
										BgL_objectz00_bglt BgL_arg1807z00_2934;

										{	/* BackEnd/cplib.scm 288 */
											obj_t BgL_tmpz00_3684;

											BgL_tmpz00_3684 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1710z00_2932));
											BgL_arg1807z00_2934 =
												(BgL_objectz00_bglt) (BgL_tmpz00_3684);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* BackEnd/cplib.scm 288 */
												long BgL_idxz00_2935;

												BgL_idxz00_2935 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2934);
												BgL_test1997z00_3680 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2935 + 3L)) == BgL_classz00_2933);
											}
										else
											{	/* BackEnd/cplib.scm 288 */
												bool_t BgL_res1869z00_2938;

												{	/* BackEnd/cplib.scm 288 */
													obj_t BgL_oclassz00_2939;

													{	/* BackEnd/cplib.scm 288 */
														obj_t BgL_arg1815z00_2940;
														long BgL_arg1816z00_2941;

														BgL_arg1815z00_2940 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* BackEnd/cplib.scm 288 */
															long BgL_arg1817z00_2942;

															BgL_arg1817z00_2942 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2934);
															BgL_arg1816z00_2941 =
																(BgL_arg1817z00_2942 - OBJECT_TYPE);
														}
														BgL_oclassz00_2939 =
															VECTOR_REF(BgL_arg1815z00_2940,
															BgL_arg1816z00_2941);
													}
													{	/* BackEnd/cplib.scm 288 */
														bool_t BgL__ortest_1115z00_2943;

														BgL__ortest_1115z00_2943 =
															(BgL_classz00_2933 == BgL_oclassz00_2939);
														if (BgL__ortest_1115z00_2943)
															{	/* BackEnd/cplib.scm 288 */
																BgL_res1869z00_2938 = BgL__ortest_1115z00_2943;
															}
														else
															{	/* BackEnd/cplib.scm 288 */
																long BgL_odepthz00_2944;

																{	/* BackEnd/cplib.scm 288 */
																	obj_t BgL_arg1804z00_2945;

																	BgL_arg1804z00_2945 = (BgL_oclassz00_2939);
																	BgL_odepthz00_2944 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2945);
																}
																if ((3L < BgL_odepthz00_2944))
																	{	/* BackEnd/cplib.scm 288 */
																		obj_t BgL_arg1802z00_2946;

																		{	/* BackEnd/cplib.scm 288 */
																			obj_t BgL_arg1803z00_2947;

																			BgL_arg1803z00_2947 =
																				(BgL_oclassz00_2939);
																			BgL_arg1802z00_2946 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2947, 3L);
																		}
																		BgL_res1869z00_2938 =
																			(BgL_arg1802z00_2946 ==
																			BgL_classz00_2933);
																	}
																else
																	{	/* BackEnd/cplib.scm 288 */
																		BgL_res1869z00_2938 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1997z00_3680 = BgL_res1869z00_2938;
											}
									}
								}
							}
							if (BgL_test1997z00_3680)
								{	/* BackEnd/cplib.scm 288 */
									BgL_test1995z00_3675 = ((bool_t) 0);
								}
							else
								{	/* BackEnd/cplib.scm 289 */
									bool_t BgL_test2001z00_3707;

									{	/* BackEnd/cplib.scm 289 */
										BgL_valuez00_bglt BgL_arg1709z00_2948;

										BgL_arg1709z00_2948 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_vz00_2906))))->
											BgL_valuez00);
										{	/* BackEnd/cplib.scm 289 */
											obj_t BgL_classz00_2949;

											BgL_classz00_2949 = BGl_sfunz00zzast_varz00;
											{	/* BackEnd/cplib.scm 289 */
												BgL_objectz00_bglt BgL_arg1807z00_2950;

												{	/* BackEnd/cplib.scm 289 */
													obj_t BgL_tmpz00_3711;

													BgL_tmpz00_3711 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1709z00_2948));
													BgL_arg1807z00_2950 =
														(BgL_objectz00_bglt) (BgL_tmpz00_3711);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* BackEnd/cplib.scm 289 */
														long BgL_idxz00_2951;

														BgL_idxz00_2951 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2950);
														BgL_test2001z00_3707 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2951 + 3L)) == BgL_classz00_2949);
													}
												else
													{	/* BackEnd/cplib.scm 289 */
														bool_t BgL_res1870z00_2954;

														{	/* BackEnd/cplib.scm 289 */
															obj_t BgL_oclassz00_2955;

															{	/* BackEnd/cplib.scm 289 */
																obj_t BgL_arg1815z00_2956;
																long BgL_arg1816z00_2957;

																BgL_arg1815z00_2956 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* BackEnd/cplib.scm 289 */
																	long BgL_arg1817z00_2958;

																	BgL_arg1817z00_2958 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2950);
																	BgL_arg1816z00_2957 =
																		(BgL_arg1817z00_2958 - OBJECT_TYPE);
																}
																BgL_oclassz00_2955 =
																	VECTOR_REF(BgL_arg1815z00_2956,
																	BgL_arg1816z00_2957);
															}
															{	/* BackEnd/cplib.scm 289 */
																bool_t BgL__ortest_1115z00_2959;

																BgL__ortest_1115z00_2959 =
																	(BgL_classz00_2949 == BgL_oclassz00_2955);
																if (BgL__ortest_1115z00_2959)
																	{	/* BackEnd/cplib.scm 289 */
																		BgL_res1870z00_2954 =
																			BgL__ortest_1115z00_2959;
																	}
																else
																	{	/* BackEnd/cplib.scm 289 */
																		long BgL_odepthz00_2960;

																		{	/* BackEnd/cplib.scm 289 */
																			obj_t BgL_arg1804z00_2961;

																			BgL_arg1804z00_2961 =
																				(BgL_oclassz00_2955);
																			BgL_odepthz00_2960 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2961);
																		}
																		if ((3L < BgL_odepthz00_2960))
																			{	/* BackEnd/cplib.scm 289 */
																				obj_t BgL_arg1802z00_2962;

																				{	/* BackEnd/cplib.scm 289 */
																					obj_t BgL_arg1803z00_2963;

																					BgL_arg1803z00_2963 =
																						(BgL_oclassz00_2955);
																					BgL_arg1802z00_2962 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2963, 3L);
																				}
																				BgL_res1870z00_2954 =
																					(BgL_arg1802z00_2962 ==
																					BgL_classz00_2949);
																			}
																		else
																			{	/* BackEnd/cplib.scm 289 */
																				BgL_res1870z00_2954 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2001z00_3707 = BgL_res1870z00_2954;
													}
											}
										}
									}
									if (BgL_test2001z00_3707)
										{	/* BackEnd/cplib.scm 289 */
											BgL_test1995z00_3675 = ((bool_t) 0);
										}
									else
										{	/* BackEnd/cplib.scm 289 */
											if (
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt) BgL_vz00_2906))))->
														BgL_idz00) == CNST_TABLE_REF(6)))
												{	/* BackEnd/cplib.scm 290 */
													BgL_test1995z00_3675 = ((bool_t) 0);
												}
											else
												{	/* BackEnd/cplib.scm 290 */
													BgL_test1995z00_3675 = ((bool_t) 1);
												}
										}
								}
						}
					else
						{	/* BackEnd/cplib.scm 287 */
							BgL_test1995z00_3675 = ((bool_t) 0);
						}
					if (BgL_test1995z00_3675)
						{	/* BackEnd/cplib.scm 291 */
							obj_t BgL_auxz00_2964;

							BgL_auxz00_2964 =
								MAKE_YOUNG_PAIR(BgL_vz00_2906, CELL_REF(BgL_rz00_2904));
							return CELL_SET(BgL_rz00_2904, BgL_auxz00_2964);
						}
					else
						{	/* BackEnd/cplib.scm 287 */
							return BFALSE;
						}
				}
			}
		}

	}



/* get-global-variables-to-be-initialized */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2globalzd2variableszd2tozd2bezd2initializa7edz75zzbackend_cplibz00
		(obj_t BgL_modulez00_31)
	{
		{	/* BackEnd/cplib.scm 297 */
			{	/* BackEnd/cplib.scm 299 */
				obj_t BgL_rz00_2918;

				BgL_rz00_2918 = MAKE_CELL(BNIL);
				{	/* BackEnd/cplib.scm 301 */
					obj_t BgL_zc3z04anonymousza31716ze3z87_2910;

					BgL_zc3z04anonymousza31716ze3z87_2910 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31716ze3ze5zzbackend_cplibz00, (int) (1L),
						(int) (2L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31716ze3z87_2910, (int) (0L),
						((obj_t) BgL_rz00_2918));
					PROCEDURE_SET(BgL_zc3z04anonymousza31716ze3z87_2910, (int) (1L),
						BgL_modulez00_31);
					BGl_forzd2eachzd2globalz12z12zzast_envz00
						(BgL_zc3z04anonymousza31716ze3z87_2910, BNIL);
				}
				return CELL_REF(BgL_rz00_2918);
			}
		}

	}



/* &get-global-variables-to-be-initialized */
	obj_t
		BGl_z62getzd2globalzd2variableszd2tozd2bezd2initializa7edz17zzbackend_cplibz00
		(obj_t BgL_envz00_2911, obj_t BgL_modulez00_2912)
	{
		{	/* BackEnd/cplib.scm 297 */
			return
				BGl_getzd2globalzd2variableszd2tozd2bezd2initializa7edz75zzbackend_cplibz00
				(BgL_modulez00_2912);
		}

	}



/* &<@anonymous:1716> */
	obj_t BGl_z62zc3z04anonymousza31716ze3ze5zzbackend_cplibz00(obj_t
		BgL_envz00_2913, obj_t BgL_globalz00_2916)
	{
		{	/* BackEnd/cplib.scm 300 */
			{	/* BackEnd/cplib.scm 301 */
				obj_t BgL_rz00_2914;
				obj_t BgL_modulez00_2915;

				BgL_rz00_2914 = PROCEDURE_REF(BgL_envz00_2913, (int) (0L));
				BgL_modulez00_2915 = PROCEDURE_REF(BgL_envz00_2913, (int) (1L));
				{	/* BackEnd/cplib.scm 301 */
					bool_t BgL_test2006z00_3755;

					if (
						((((BgL_globalz00_bglt) COBJECT(
										((BgL_globalz00_bglt) BgL_globalz00_2916)))->
								BgL_modulez00) == BgL_modulez00_2915))
						{	/* BackEnd/cplib.scm 301 */
							if (BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(
									((BgL_globalz00_bglt) BgL_globalz00_2916)))
								{	/* BackEnd/cplib.scm 304 */
									BgL_valuez00_bglt BgL_arg1724z00_2965;

									BgL_arg1724z00_2965 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_globalz00_2916))))->
										BgL_valuez00);
									{	/* BackEnd/cplib.scm 304 */
										obj_t BgL_classz00_2966;

										BgL_classz00_2966 = BGl_scnstz00zzast_varz00;
										{	/* BackEnd/cplib.scm 304 */
											BgL_objectz00_bglt BgL_arg1807z00_2967;

											{	/* BackEnd/cplib.scm 304 */
												obj_t BgL_tmpz00_3766;

												BgL_tmpz00_3766 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1724z00_2965));
												BgL_arg1807z00_2967 =
													(BgL_objectz00_bglt) (BgL_tmpz00_3766);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* BackEnd/cplib.scm 304 */
													long BgL_idxz00_2968;

													BgL_idxz00_2968 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2967);
													BgL_test2006z00_3755 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2968 + 2L)) == BgL_classz00_2966);
												}
											else
												{	/* BackEnd/cplib.scm 304 */
													bool_t BgL_res1871z00_2971;

													{	/* BackEnd/cplib.scm 304 */
														obj_t BgL_oclassz00_2972;

														{	/* BackEnd/cplib.scm 304 */
															obj_t BgL_arg1815z00_2973;
															long BgL_arg1816z00_2974;

															BgL_arg1815z00_2973 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* BackEnd/cplib.scm 304 */
																long BgL_arg1817z00_2975;

																BgL_arg1817z00_2975 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2967);
																BgL_arg1816z00_2974 =
																	(BgL_arg1817z00_2975 - OBJECT_TYPE);
															}
															BgL_oclassz00_2972 =
																VECTOR_REF(BgL_arg1815z00_2973,
																BgL_arg1816z00_2974);
														}
														{	/* BackEnd/cplib.scm 304 */
															bool_t BgL__ortest_1115z00_2976;

															BgL__ortest_1115z00_2976 =
																(BgL_classz00_2966 == BgL_oclassz00_2972);
															if (BgL__ortest_1115z00_2976)
																{	/* BackEnd/cplib.scm 304 */
																	BgL_res1871z00_2971 =
																		BgL__ortest_1115z00_2976;
																}
															else
																{	/* BackEnd/cplib.scm 304 */
																	long BgL_odepthz00_2977;

																	{	/* BackEnd/cplib.scm 304 */
																		obj_t BgL_arg1804z00_2978;

																		BgL_arg1804z00_2978 = (BgL_oclassz00_2972);
																		BgL_odepthz00_2977 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2978);
																	}
																	if ((2L < BgL_odepthz00_2977))
																		{	/* BackEnd/cplib.scm 304 */
																			obj_t BgL_arg1802z00_2979;

																			{	/* BackEnd/cplib.scm 304 */
																				obj_t BgL_arg1803z00_2980;

																				BgL_arg1803z00_2980 =
																					(BgL_oclassz00_2972);
																				BgL_arg1802z00_2979 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2980, 2L);
																			}
																			BgL_res1871z00_2971 =
																				(BgL_arg1802z00_2979 ==
																				BgL_classz00_2966);
																		}
																	else
																		{	/* BackEnd/cplib.scm 304 */
																			BgL_res1871z00_2971 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2006z00_3755 = BgL_res1871z00_2971;
												}
										}
									}
								}
							else
								{	/* BackEnd/cplib.scm 303 */
									BgL_test2006z00_3755 = ((bool_t) 0);
								}
						}
					else
						{	/* BackEnd/cplib.scm 301 */
							BgL_test2006z00_3755 = ((bool_t) 0);
						}
					if (BgL_test2006z00_3755)
						{	/* BackEnd/cplib.scm 305 */
							obj_t BgL_auxz00_2981;

							BgL_auxz00_2981 =
								MAKE_YOUNG_PAIR(BgL_globalz00_2916, CELL_REF(BgL_rz00_2914));
							return CELL_SET(BgL_rz00_2914, BgL_auxz00_2981);
						}
					else
						{	/* BackEnd/cplib.scm 301 */
							return BFALSE;
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_cplibz00(void)
	{
		{	/* BackEnd/cplib.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_cplibz00(void)
	{
		{	/* BackEnd/cplib.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_cplibz00(void)
	{
		{	/* BackEnd/cplib.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_cplibz00(void)
	{
		{	/* BackEnd/cplib.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzcnst_nodez00(89013043L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzmodule_javaz00(93941037L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(364917963L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
			return
				BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1891z00zzbackend_cplibz00));
		}

	}

#ifdef __cplusplus
}
#endif
