/*===========================================================================*/
/*   (BackEnd/backend.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/backend.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_BACKEND_TYPE_DEFINITIONS
#define BGL_BACKEND_BACKEND_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_BACKEND_BACKEND_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2typezd2checkzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2typedzd2eqz00zzbackend_backendz00(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	static obj_t BGl_z62backendzd2callcczb0zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31201ze3ze5zzbackend_backendz00(obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbackend_backendz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2languagezd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt,
		obj_t);
	static obj_t BGl_z62backendzd2namezb0zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2srfi0zb0zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2compilezd2functionsz00zzbackend_backendz00
		(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2functionszd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt,
		obj_t);
	static obj_t
		BGl_z62backendzd2cnstzd2tablezd2n1067zb0zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62backendzd2callcczd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_backendz00_bglt
		BGl_backendzd2nilzd2zzbackend_backendz00(void);
	static obj_t BGl_z62backendzd2typedzd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2foreignzd2closurezd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	static obj_t BGl_z62backendzd2compilezd2func1061z62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62backendzd2subtypezf3z43zzbackend_backendz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31211ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31106ze3ze5zzbackend_backendz00(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2forcezd2registerzd2gczd2rootsz00zzbackend_backendz00
		(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2heapzd2suffixz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t
		BGl_z62backendzd2cnstzd2tablezd2namezb0zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62backendzd2forcezd2registerzd2gczd2rootsz62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62backendzd2initializa7ez121057z05zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2backendz12zc0zzbackend_backendz00(obj_t);
	static obj_t BGl_za2thezd2backendza2zd2zzbackend_backendz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzbackend_backendz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2variableszd2zzbackend_backendz00(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2checkzd2inlinesz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62backendzd2compile1059zb0zzbackend_backendz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2typezd2checkz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31221ze3ze5zzbackend_backendz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2foreignzd2closurez00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62backendzd2variableszd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2languagezd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzbackend_backendz00(void);
	static obj_t BGl_z62backendzd2linkzd2objectsz62zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62backendzd2typedzd2eqz62zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_backendzf3zf3zzbackend_backendz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31230ze3ze5zzbackend_backendz00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2callcczd2zzbackend_backendz00(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2typedzd2eqzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	static obj_t BGl_objectzd2initzd2zzbackend_backendz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2qualifiedzd2typeszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	static obj_t BGl_z62zc3z04anonymousza31126ze3ze5zzbackend_backendz00(obj_t);
	static obj_t
		BGl_z62backendzd2forcezd2registerzd2gczd2rootszd2setz12za2zzbackend_backendz00
		(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t
		BGl_z62backendzd2removezd2emptyzd2letzb0zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2linkzd2objectsz00zzbackend_backendz00(BgL_backendz00_bglt,
		obj_t);
	static obj_t BGl_z62backendzd2heapzd2compatiblez62zzbackend_backendz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2debugzd2supportz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31135ze3ze5zzbackend_backendz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2typedzd2funcallz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62backendzd2registerszb0zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2externzd2variablesz62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62backendzd2checkzd2inlinesz62zzbackend_backendz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2heapzd2compatiblezd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2forcezd2registerzd2gczd2rootszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	static obj_t BGl_z62zc3z04anonymousza31144ze3ze5zzbackend_backendz00(obj_t);
	static obj_t
		BGl_z62backendzd2externzd2typeszd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62backendzd2initializa7ez12z05zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_za2backendsza2z00zzbackend_backendz00 = BUNSPEC;
	static obj_t BGl_z62backendzd2linkzd2objects1069z62zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2strictzd2typezd2castzd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2pregisterszd2zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31331ze3ze5zzbackend_backendz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2callcczd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt,
		bool_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2foreignzd2clausezd2supportzb0zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62backendzd2typedzd2eqzd2setz12za2zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	static BgL_backendz00_bglt BGl_z62backendzd2nilzb0zzbackend_backendz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_backendz00(void);
	static obj_t
		BGl_z62backendzd2tvectorzd2descrzd2supportzb0zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31324ze3ze5zzbackend_backendz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2functionszd2zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31308ze3ze5zzbackend_backendz00(obj_t);
	static obj_t
		BGl_z62backendzd2removezd2emptyzd2letzd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2pragmazd2supportz00zzbackend_backendz00(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2pregisterszd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	static obj_t BGl_z62backendzd2debugzd2supportz62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62backendzd2functionszd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31155ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31317ze3ze5zzbackend_backendz00(obj_t);
	static obj_t
		BGl_z62backendzd2externzd2variableszd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62backendzd2typedzd2funcallz62zzbackend_backendz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2backendz12zc0zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2boundzd2checkz62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62backendzd2typedzb0zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2externzd2typeszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	static obj_t BGl_z62backendzd2linkzb0zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2requirezd2tailcz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1104z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31342ze3ze5zzbackend_backendz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2effectzb2z60zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1105z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31091ze3ze5zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31237ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62backendzd2typeszb0zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2effectzb2z02zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2srfi0zd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2srfi0zd2zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t
		BGl_z62backendzd2instrzd2resetzd2registerszb0zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_backendz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2registerszd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt,
		obj_t);
	static obj_t BGl_z62backendzd2typeszd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1200z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31271ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31352ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62lambda1124z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31425ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62lambda1125z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31247ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62backendzd2qualifiedzd2typesz62zzbackend_backendz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2pragmazd2supportzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	static obj_t BGl_z62lambda1209z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2externzd2functionsz62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62backendzd2typezd2checkzd2setz12za2zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1210z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1133z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1134z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2instrzd2resetzd21071zb0zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31167ze3ze5zzbackend_backendz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2foreignzd2clausezd2supportzd2zzbackend_backendz00
		(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1219z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2variableszb0zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2typedzd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2initializa7ez12z67zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t
		BGl_z62backendzd2foreignzd2closurezd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1220z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1142z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1143z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2stringzd2literalzd2supportzd2setz12z70zzbackend_backendz00
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1306z62zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2removezd2emptyzd2letzd2zzbackend_backendz00
		(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1307z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62backendzd2requirezd2tailcz62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1228z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1229z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2strictzd2typezd2castzb0zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62thezd2backendzb0zzbackend_backendz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2compilezd2zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62backendzd2compilezb0zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62setzd2backendz12za2zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2tracezd2supportzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2namezd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda1153z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31193ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62lambda1315z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31517ze3ze5zzbackend_backendz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2externzd2variablesz00zzbackend_backendz00
		(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1154z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1235z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1316z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1236z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2stringzd2literalzd2supportzb0zzbackend_backendz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzbackend_backendz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62lambda1322z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2checkzd2inline1073z62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1323z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2cnstzd2tablezd2namezd2zzbackend_backendz00
		(BgL_backendz00_bglt, int);
	static obj_t
		BGl_z62backendzd2externzd2functionszd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31186ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62lambda1245z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1165z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1246z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1166z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2heapzd2suffixzd2setz12za2zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1329z62zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2namezd2zzbackend_backendz00(BgL_backendz00_bglt);
	static BgL_backendz00_bglt BGl_z62lambda1088z62zzbackend_backendz00(obj_t);
	static obj_t
		BGl_z62backendzd2boundzd2checkzd2setz12za2zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2languagezd2zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1330z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2tracezd2supportzd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31381ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31373ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62backendzd2link1063zb0zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2boundzd2checkz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t
		BGl_z62backendzd2qualifiedzd2typeszd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1340z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2foreignzd2closurez62zzbackend_backendz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2strictzd2typezd2castzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	static obj_t BGl_z62lambda1341z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1423z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1424z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31609ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62lambda1184z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1185z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzbackend_backendz00(void);
	static obj_t BGl_z62lambda1269z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_backendz00(void);
	static obj_t BGl_z62backendzd2pragmazd2supportz62zzbackend_backendz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_backendz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzbackend_backendz00(void);
	static obj_t BGl_z62lambda1350z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1270z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1351z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1191z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31456ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62lambda1515z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1192z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1516z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1199z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2heapzd2compatiblezd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2debugzd2supportzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2tvectorzd2descrzd2supportzd2zzbackend_backendz00
		(BgL_backendz00_bglt);
	static obj_t BGl_z62backendzd2typezd2checkz62zzbackend_backendz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_backendz00zzbackend_backendz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2removezd2emptyzd2letzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	static obj_t BGl_z62backendzd2functionszb0zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1607z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1608z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2pragmazd2supportzd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2heapzd2compatiblez00zzbackend_backendz00(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2externzd2variableszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	static obj_t BGl_z62backendzd2registerszd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2stringzd2literalzd2supportzd2zzbackend_backendz00
		(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1371z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_backendz00
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1372z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1454z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1455z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1379z62zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2typedzd2zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62registerzd2backendz12za2zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62backendzd2debugzd2supportzd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2typeszd2zzbackend_backendz00(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2stringzd2literalzd2supportzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2externzd2functionsz00zzbackend_backendz00
		(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1380z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31564ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62backendzd2externzd2typesz62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1547z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1548z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2instrzd2resetzd2registerszd2zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	static obj_t BGl_z62backendzd2languagezb0zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2requirezd2tailczd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2typedzd2funcallzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	static obj_t BGl_z62backendzd2gczd2init1075z62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31492ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31549ze3ze5zzbackend_backendz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2variableszd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2externzd2typesz00zzbackend_backendz00(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2tracezd2supportz00zzbackend_backendz00(BgL_backendz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2srfi0zd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda1562z62zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2strictzd2typezd2castzd2zzbackend_backendz00
		(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1563z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62backendzd2heapzd2suffixz62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62backendzd2pregisterszd2setz12z70zzbackend_backendz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	static obj_t BGl_z62backendzd2pregisterszb0zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2typeszd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt,
		obj_t);
	static obj_t
		BGl_z62backendzd2requirezd2tailczd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2typedzd2funcallzd2setz12za2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62backendzf3z91zzbackend_backendz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2registerszd2zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62lambda1490z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1491z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1576z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1577z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2subtypezf3z21zzbackend_backendz00(BgL_backendz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62backendzd2effectzb2zd2setz12zc2zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2heapzd2suffixzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2boundzd2checkzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2gczd2initz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62backendzd2gczd2initz62zzbackend_backendz00(obj_t, obj_t);
	static obj_t
		BGl_z62backendzd2compilezd2functionsz62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2subtypezf31065z43zzbackend_backendz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62backendzd2namezd2setz12z70zzbackend_backendz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1592z62zzbackend_backendz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1593z62zzbackend_backendz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2linkzd2zzbackend_backendz00(BgL_backendz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2effectzb2zd2setz12za0zzbackend_backendz00(BgL_backendz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_backendzd2externzd2functionszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_backendzd2qualifiedzd2typesz00zzbackend_backendz00(BgL_backendz00_bglt);
	static obj_t BGl_z62backendzd2tracezd2supportz62zzbackend_backendz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31594ze3ze5zzbackend_backendz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31578ze3ze5zzbackend_backendz00(obj_t);
	static obj_t __cnst[50];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typeszd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ1821z00,
		BGl_z62backendzd2typeszb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2stringzd2literalzd2supportzd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2str1822z00,
		BGl_z62backendzd2stringzd2literalzd2supportzd2setz12z70zzbackend_backendz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2effectzb2zd2envzb2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2eff1823z00,
		BGl_z62backendzd2effectzb2z02zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_thezd2backendzd2envz00zzbackend_backendz00,
		BgL_bgl_za762theza7d2backend1824z00,
		BGl_z62thezd2backendzb0zzbackend_backendz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_backendzd2instrzd2resetzd2registerszd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ins1825z00,
		BGl_z62backendzd2instrzd2resetzd2registerszb0zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_backendzf3zd2envz21zzbackend_backendz00,
		BgL_bgl_za762backendza7f3za7911826za7,
		BGl_z62backendzf3z91zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2functionszd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2fun1827z00,
		BGl_z62backendzd2functionszd2setz12z70zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2callcczd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2cal1828z00,
		BGl_z62backendzd2callcczb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_backendzd2nilzd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2nil1829z00,
		BGl_z62backendzd2nilzb0zzbackend_backendz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2removezd2emptyzd2letzd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2rem1830z00,
		BGl_z62backendzd2removezd2emptyzd2letzd2setz12z70zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2functionszd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2fun1831z00,
		BGl_z62backendzd2functionszb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2externzd2variableszd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ext1832z00,
		BGl_z62backendzd2externzd2variableszd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typedzd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ1833z00,
		BGl_z62backendzd2typedzd2setz12z70zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typedzd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ1834z00,
		BGl_z62backendzd2typedzb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2boundzd2checkzd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2bou1835z00,
		BGl_z62backendzd2boundzd2checkzd2setz12za2zzbackend_backendz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typedzd2eqzd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ1836z00,
		BGl_z62backendzd2typedzd2eqzd2setz12za2zzbackend_backendz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2tvectorzd2descrzd2supportzd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2tve1837z00,
		BGl_z62backendzd2tvectorzd2descrzd2supportzb0zzbackend_backendz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2forcezd2registerzd2gczd2rootszd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2for1838z00,
		BGl_z62backendzd2forcezd2registerzd2gczd2rootsz62zzbackend_backendz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2srfi0zd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2srf1839z00,
		BGl_z62backendzd2srfi0zd2setz12z70zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2pragmazd2supportzd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2pra1840z00,
		BGl_z62backendzd2pragmazd2supportz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2pregisterszd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2pre1841z00,
		BGl_z62backendzd2pregisterszd2setz12z70zzbackend_backendz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1700z00zzbackend_backendz00,
		BgL_bgl_za762lambda1125za7621842z00,
		BGl_z62lambda1125z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1701z00zzbackend_backendz00,
		BgL_bgl_za762lambda1124za7621843z00,
		BGl_z62lambda1124z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2externzd2typeszd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ext1844z00,
		BGl_z62backendzd2externzd2typeszd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1702z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1845za7,
		BGl_z62zc3z04anonymousza31135ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1703z00zzbackend_backendz00,
		BgL_bgl_za762lambda1134za7621846z00,
		BGl_z62lambda1134z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2foreignzd2closurezd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2for1847z00,
		BGl_z62backendzd2foreignzd2closurez62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1704z00zzbackend_backendz00,
		BgL_bgl_za762lambda1133za7621848z00,
		BGl_z62lambda1133z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1705z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1849za7,
		BGl_z62zc3z04anonymousza31144ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1706z00zzbackend_backendz00,
		BgL_bgl_za762lambda1143za7621850z00,
		BGl_z62lambda1143z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1707z00zzbackend_backendz00,
		BgL_bgl_za762lambda1142za7621851z00,
		BGl_z62lambda1142z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1708z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1852za7,
		BGl_z62zc3z04anonymousza31155ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1709z00zzbackend_backendz00,
		BgL_bgl_za762lambda1154za7621853z00,
		BGl_z62lambda1154z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_backendzd2compilezd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2com1854z00,
		BGl_z62backendzd2compilezb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1710z00zzbackend_backendz00,
		BgL_bgl_za762lambda1153za7621855z00,
		BGl_z62lambda1153z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2registerszd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2reg1856z00,
		BGl_z62backendzd2registerszd2setz12z70zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1711z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1857za7,
		BGl_z62zc3z04anonymousza31167ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1712z00zzbackend_backendz00,
		BgL_bgl_za762lambda1166za7621858z00,
		BGl_z62lambda1166z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1800z00zzbackend_backendz00,
		BgL_bgl_string1800za700za7za7b1859za7, "backend-compile1059", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1713z00zzbackend_backendz00,
		BgL_bgl_za762lambda1165za7621860z00,
		BGl_z62lambda1165z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1714z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1861za7,
		BGl_z62zc3z04anonymousza31186ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1802z00zzbackend_backendz00,
		BgL_bgl_string1802za700za7za7b1862za7, "backend-compile-func1061", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1715z00zzbackend_backendz00,
		BgL_bgl_za762lambda1185za7621863z00,
		BGl_z62lambda1185z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1716z00zzbackend_backendz00,
		BgL_bgl_za762lambda1184za7621864z00,
		BGl_z62lambda1184z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1804z00zzbackend_backendz00,
		BgL_bgl_string1804za700za7za7b1865za7, "backend-link1063", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1717z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1866za7,
		BGl_z62zc3z04anonymousza31193ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1718z00zzbackend_backendz00,
		BgL_bgl_za762lambda1192za7621867z00,
		BGl_z62lambda1192z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1806z00zzbackend_backendz00,
		BgL_bgl_string1806za700za7za7b1868za7, "backend-subtype?1065", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1719z00zzbackend_backendz00,
		BgL_bgl_za762lambda1191za7621869z00,
		BGl_z62lambda1191z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1808z00zzbackend_backendz00,
		BgL_bgl_string1808za700za7za7b1870za7, "backend-cnst-table-n1067", 24);
	     
		DEFINE_EXPORT_BGL_GENERIC(BGl_backendzd2linkzd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2lin1871z00,
		BGl_z62backendzd2linkzb0zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1801z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2com1872z00,
		BGl_z62backendzd2compilezd2func1061z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1720z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1873za7,
		BGl_z62zc3z04anonymousza31201ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1721z00zzbackend_backendz00,
		BgL_bgl_za762lambda1200za7621874z00,
		BGl_z62lambda1200z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1803z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2lin1875z00,
		BGl_z62backendzd2link1063zb0zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1722z00zzbackend_backendz00,
		BgL_bgl_za762lambda1199za7621876z00,
		BGl_z62lambda1199z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1810z00zzbackend_backendz00,
		BgL_bgl_string1810za700za7za7b1877za7, "backend-link-objects1069", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1723z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1878za7,
		BGl_z62zc3z04anonymousza31211ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2externzd2functionszd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ext1879z00,
		BGl_z62backendzd2externzd2functionszd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2qualifiedzd2typeszd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2qua1880z00,
		BGl_z62backendzd2qualifiedzd2typesz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1805z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2sub1881z00,
		BGl_z62backendzd2subtypezf31065z43zzbackend_backendz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1724z00zzbackend_backendz00,
		BgL_bgl_za762lambda1210za7621882z00,
		BGl_z62lambda1210z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1812z00zzbackend_backendz00,
		BgL_bgl_string1812za700za7za7b1883za7, "backend-instr-reset-1071", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1725z00zzbackend_backendz00,
		BgL_bgl_za762lambda1209za7621884z00,
		BGl_z62lambda1209z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1807z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2cns1885z00,
		BGl_z62backendzd2cnstzd2tablezd2n1067zb0zzbackend_backendz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1726z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1886za7,
		BGl_z62zc3z04anonymousza31221ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1814z00zzbackend_backendz00,
		BgL_bgl_string1814za700za7za7b1887za7, "backend-check-inline1073", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1727z00zzbackend_backendz00,
		BgL_bgl_za762lambda1220za7621888z00,
		BGl_z62lambda1220z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1809z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2lin1889z00,
		BGl_z62backendzd2linkzd2objects1069z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1728z00zzbackend_backendz00,
		BgL_bgl_za762lambda1219za7621890z00,
		BGl_z62lambda1219z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1816z00zzbackend_backendz00,
		BgL_bgl_string1816za700za7za7b1891za7, "backend-gc-init1075", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1729z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1892za7,
		BGl_z62zc3z04anonymousza31230ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1817z00zzbackend_backendz00,
		BgL_bgl_string1817za700za7za7b1893za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1818z00zzbackend_backendz00,
		BgL_bgl_string1818za700za7za7b1894za7, "backend_backend", 15);
	      DEFINE_STRING(BGl_string1819z00zzbackend_backendz00,
		BgL_bgl_string1819za700za7za7b1895za7,
		"backend-compile1059 backend-compile-func1061 backend-link1063 backend-subtype?1065 backend-cnst-table-n1067 backend-link-objects1069 none native (foreign extern) (c bdb module) _ backend_backend backend string-literal-support force-register-gc-roots strict-type-cast typed-funcall type-check bound-check pregisters registers require-tailc tvector-descr-support pragma-support debug-support pair-nil foreign-clause-support trace-support typed-eq foreign-closure remove-empty-let effect+ qualified-types callcc heap-compatible heap-suffix bool typed types functions variables extern-types extern-functions obj extern-variables bstring name srfi0 symbol language ",
		660);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_backendzd2namezd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2nam1896z00,
		BGl_z62backendzd2namezb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2backendz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762setza7d2backend1897z00,
		BGl_z62setzd2backendz12za2zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typeszd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ1898z00,
		BGl_z62backendzd2typeszd2setz12z70zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1811z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ins1899z00,
		BGl_z62backendzd2instrzd2resetzd21071zb0zzbackend_backendz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1730z00zzbackend_backendz00,
		BgL_bgl_za762lambda1229za7621900z00,
		BGl_z62lambda1229z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1731z00zzbackend_backendz00,
		BgL_bgl_za762lambda1228za7621901z00,
		BGl_z62lambda1228z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1813z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2che1902z00,
		BGl_z62backendzd2checkzd2inline1073z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1732z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1903za7,
		BGl_z62zc3z04anonymousza31237ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1733z00zzbackend_backendz00,
		BgL_bgl_za762lambda1236za7621904z00,
		BGl_z62lambda1236z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1815z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2gcza71905za7,
		BGl_z62backendzd2gczd2init1075z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1734z00zzbackend_backendz00,
		BgL_bgl_za762lambda1235za7621906z00,
		BGl_z62lambda1235z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1735z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1907za7,
		BGl_z62zc3z04anonymousza31247ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1736z00zzbackend_backendz00,
		BgL_bgl_za762lambda1246za7621908z00,
		BGl_z62lambda1246z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1737z00zzbackend_backendz00,
		BgL_bgl_za762lambda1245za7621909z00,
		BGl_z62lambda1245z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1738z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1910za7,
		BGl_z62zc3z04anonymousza31271ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1739z00zzbackend_backendz00,
		BgL_bgl_za762lambda1270za7621911z00,
		BGl_z62lambda1270z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_backendzd2checkzd2inlineszd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2che1912z00,
		BGl_z62backendzd2checkzd2inlinesz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typezd2checkzd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ1913z00,
		BGl_z62backendzd2typezd2checkz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1740z00zzbackend_backendz00,
		BgL_bgl_za762lambda1269za7621914z00,
		BGl_z62lambda1269z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1741z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1915za7,
		BGl_z62zc3z04anonymousza31308ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1742z00zzbackend_backendz00,
		BgL_bgl_za762lambda1307za7621916z00,
		BGl_z62lambda1307z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1743z00zzbackend_backendz00,
		BgL_bgl_za762lambda1306za7621917z00,
		BGl_z62lambda1306z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1744z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1918za7,
		BGl_z62zc3z04anonymousza31317ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1745z00zzbackend_backendz00,
		BgL_bgl_za762lambda1316za7621919z00,
		BGl_z62lambda1316z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1746z00zzbackend_backendz00,
		BgL_bgl_za762lambda1315za7621920z00,
		BGl_z62lambda1315z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1747z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1921za7,
		BGl_z62zc3z04anonymousza31324ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1748z00zzbackend_backendz00,
		BgL_bgl_za762lambda1323za7621922z00,
		BGl_z62lambda1323z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1749z00zzbackend_backendz00,
		BgL_bgl_za762lambda1322za7621923z00,
		BGl_z62lambda1322z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2languagezd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2lan1924z00,
		BGl_z62backendzd2languagezd2setz12z70zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1750z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1925za7,
		BGl_z62zc3z04anonymousza31331ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1751z00zzbackend_backendz00,
		BgL_bgl_za762lambda1330za7621926z00,
		BGl_z62lambda1330z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1752z00zzbackend_backendz00,
		BgL_bgl_za762lambda1329za7621927z00,
		BGl_z62lambda1329z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1753z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1928za7,
		BGl_z62zc3z04anonymousza31342ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1754z00zzbackend_backendz00,
		BgL_bgl_za762lambda1341za7621929z00,
		BGl_z62lambda1341z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1755z00zzbackend_backendz00,
		BgL_bgl_za762lambda1340za7621930z00,
		BGl_z62lambda1340z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1756z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1931za7,
		BGl_z62zc3z04anonymousza31352ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2callcczd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2cal1932z00,
		BGl_z62backendzd2callcczd2setz12z70zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1757z00zzbackend_backendz00,
		BgL_bgl_za762lambda1351za7621933z00,
		BGl_z62lambda1351z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1758z00zzbackend_backendz00,
		BgL_bgl_za762lambda1350za7621934z00,
		BGl_z62lambda1350z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1759z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1935za7,
		BGl_z62zc3z04anonymousza31373ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2foreignzd2closurezd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2for1936z00,
		BGl_z62backendzd2foreignzd2closurezd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1760z00zzbackend_backendz00,
		BgL_bgl_za762lambda1372za7621937z00,
		BGl_z62lambda1372z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1761z00zzbackend_backendz00,
		BgL_bgl_za762lambda1371za7621938z00,
		BGl_z62lambda1371z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2externzd2typeszd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ext1939z00,
		BGl_z62backendzd2externzd2typesz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1762z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1940za7,
		BGl_z62zc3z04anonymousza31381ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1763z00zzbackend_backendz00,
		BgL_bgl_za762lambda1380za7621941z00,
		BGl_z62lambda1380z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1764z00zzbackend_backendz00,
		BgL_bgl_za762lambda1379za7621942z00,
		BGl_z62lambda1379z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1765z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1943za7,
		BGl_z62zc3z04anonymousza31425ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1766z00zzbackend_backendz00,
		BgL_bgl_za762lambda1424za7621944z00,
		BGl_z62lambda1424z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1767z00zzbackend_backendz00,
		BgL_bgl_za762lambda1423za7621945z00,
		BGl_z62lambda1423z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1768z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1946za7,
		BGl_z62zc3z04anonymousza31456ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1769z00zzbackend_backendz00,
		BgL_bgl_za762lambda1455za7621947z00,
		BGl_z62lambda1455z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1694z00zzbackend_backendz00,
		BgL_bgl_string1694za700za7za7b1948za7, "backend", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2strictzd2typezd2castzd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2str1949z00,
		BGl_z62backendzd2strictzd2typezd2castzb0zzbackend_backendz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1695z00zzbackend_backendz00,
		BgL_bgl_string1695za700za7za7b1950za7, "Unimplemented target language", 29);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_backendzd2linkzd2objectszd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2lin1951z00,
		BGl_z62backendzd2linkzd2objectsz62zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typezd2checkzd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ1952z00,
		BGl_z62backendzd2typezd2checkzd2setz12za2zzbackend_backendz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1770z00zzbackend_backendz00,
		BgL_bgl_za762lambda1454za7621953z00,
		BGl_z62lambda1454z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2registerszd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2reg1954z00,
		BGl_z62backendzd2registerszb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1771z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1955za7,
		BGl_z62zc3z04anonymousza31492ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1772z00zzbackend_backendz00,
		BgL_bgl_za762lambda1491za7621956z00,
		BGl_z62lambda1491z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1773z00zzbackend_backendz00,
		BgL_bgl_za762lambda1490za7621957z00,
		BGl_z62lambda1490z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1774z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1958za7,
		BGl_z62zc3z04anonymousza31517ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1775z00zzbackend_backendz00,
		BgL_bgl_za762lambda1516za7621959z00,
		BGl_z62lambda1516z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1776z00zzbackend_backendz00,
		BgL_bgl_za762lambda1515za7621960z00,
		BGl_z62lambda1515z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1777z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1961za7,
		BGl_z62zc3z04anonymousza31549ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1696z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1962za7,
		BGl_z62zc3z04anonymousza31106ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1778z00zzbackend_backendz00,
		BgL_bgl_za762lambda1548za7621963z00,
		BGl_z62lambda1548z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1697z00zzbackend_backendz00,
		BgL_bgl_za762lambda1105za7621964z00,
		BGl_z62lambda1105z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1779z00zzbackend_backendz00,
		BgL_bgl_za762lambda1547za7621965z00,
		BGl_z62lambda1547z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1698z00zzbackend_backendz00,
		BgL_bgl_za762lambda1104za7621966z00,
		BGl_z62lambda1104z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1699z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1967za7,
		BGl_z62zc3z04anonymousza31126ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2heapzd2compatiblezd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2hea1968z00,
		BGl_z62backendzd2heapzd2compatiblezd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ini1969z00,
		BGl_z62backendzd2initializa7ez12z05zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1780z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1970za7,
		BGl_z62zc3z04anonymousza31564ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1781z00zzbackend_backendz00,
		BgL_bgl_za762lambda1563za7621971z00,
		BGl_z62lambda1563z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2strictzd2typezd2castzd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2str1972z00,
		BGl_z62backendzd2strictzd2typezd2castzd2setz12z70zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1782z00zzbackend_backendz00,
		BgL_bgl_za762lambda1562za7621973z00,
		BGl_z62lambda1562z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1783z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1974za7,
		BGl_z62zc3z04anonymousza31578ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1784z00zzbackend_backendz00,
		BgL_bgl_za762lambda1577za7621975z00,
		BGl_z62lambda1577z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1785z00zzbackend_backendz00,
		BgL_bgl_za762lambda1576za7621976z00,
		BGl_z62lambda1576z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1786z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1977za7,
		BGl_z62zc3z04anonymousza31594ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1787z00zzbackend_backendz00,
		BgL_bgl_za762lambda1593za7621978z00,
		BGl_z62lambda1593z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1794z00zzbackend_backendz00,
		BgL_bgl_string1794za700za7za7b1979za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1788z00zzbackend_backendz00,
		BgL_bgl_za762lambda1592za7621980z00,
		BGl_z62lambda1592z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1795z00zzbackend_backendz00,
		BgL_bgl_string1795za700za7za7b1981za7, "heap", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1789z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1982za7,
		BGl_z62zc3z04anonymousza31609ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1796z00zzbackend_backendz00,
		BgL_bgl_string1796za700za7za7b1983za7, "dummy", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2srfi0zd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2srf1984z00,
		BGl_z62backendzd2srfi0zb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1798z00zzbackend_backendz00,
		BgL_bgl_string1798za700za7za7b1985za7, "backend-initialize!1057", 23);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_backendzd2compilezd2functionszd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2com1986z00,
		BGl_z62backendzd2compilezd2functionsz62zzbackend_backendz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2requirezd2tailczd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2req1987z00,
		BGl_z62backendzd2requirezd2tailczd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2tracezd2supportzd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2tra1988z00,
		BGl_z62backendzd2tracezd2supportzd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1790z00zzbackend_backendz00,
		BgL_bgl_za762lambda1608za7621989z00,
		BGl_z62lambda1608z62zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1791z00zzbackend_backendz00,
		BgL_bgl_za762lambda1607za7621990z00,
		BGl_z62lambda1607z62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2namezd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2nam1991z00,
		BGl_z62backendzd2namezd2setz12z70zzbackend_backendz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1792z00zzbackend_backendz00,
		BgL_bgl_za762za7c3za704anonymo1992za7,
		BGl_z62zc3z04anonymousza31091ze3ze5zzbackend_backendz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1793z00zzbackend_backendz00,
		BgL_bgl_za762lambda1088za7621993z00,
		BGl_z62lambda1088z62zzbackend_backendz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1797z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ini1994z00,
		BGl_z62backendzd2initializa7ez121057z05zzbackend_backendz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1799z00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2com1995z00,
		BGl_z62backendzd2compile1059zb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2tracezd2supportzd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2tra1996z00,
		BGl_z62backendzd2tracezd2supportz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_backendzd2cnstzd2tablezd2namezd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2cns1997z00,
		BGl_z62backendzd2cnstzd2tablezd2namezb0zzbackend_backendz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2pragmazd2supportzd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2pra1998z00,
		BGl_z62backendzd2pragmazd2supportzd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2tvectorzd2descrzd2supportzd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2tve1999z00,
		BGl_z62backendzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_backendz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2foreignzd2clausezd2supportzd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2for2000z00,
		BGl_z62backendzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_backendz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2boundzd2checkzd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2bou2001z00,
		BGl_z62backendzd2boundzd2checkz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2forcezd2registerzd2gczd2rootszd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2for2002z00,
		BGl_z62backendzd2forcezd2registerzd2gczd2rootszd2setz12za2zzbackend_backendz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2removezd2emptyzd2letzd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2rem2003z00,
		BGl_z62backendzd2removezd2emptyzd2letzb0zzbackend_backendz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typedzd2funcallzd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ2004z00,
		BGl_z62backendzd2typedzd2funcallzd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2externzd2variableszd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ext2005z00,
		BGl_z62backendzd2externzd2variablesz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2debugzd2supportzd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2deb2006z00,
		BGl_z62backendzd2debugzd2supportzd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2heapzd2suffixzd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2hea2007z00,
		BGl_z62backendzd2heapzd2suffixz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2languagezd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2lan2008z00,
		BGl_z62backendzd2languagezb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typedzd2eqzd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ2009z00,
		BGl_z62backendzd2typedzd2eqz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2heapzd2compatiblezd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2hea2010z00,
		BGl_z62backendzd2heapzd2compatiblez62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2stringzd2literalzd2supportzd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2str2011z00,
		BGl_z62backendzd2stringzd2literalzd2supportzb0zzbackend_backendz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2backendz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762registerza7d2ba2012z00,
		BGl_z62registerzd2backendz12za2zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2heapzd2suffixzd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2hea2013z00,
		BGl_z62backendzd2heapzd2suffixzd2setz12za2zzbackend_backendz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2qualifiedzd2typeszd2setz12zd2envz12zzbackend_backendz00,
		BgL_bgl_za762backendza7d2qua2014z00,
		BGl_z62backendzd2qualifiedzd2typeszd2setz12za2zzbackend_backendz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2effectzb2zd2setz12zd2envz72zzbackend_backendz00,
		BgL_bgl_za762backendza7d2eff2015z00,
		BGl_z62backendzd2effectzb2zd2setz12zc2zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2variableszd2setz12zd2envzc0zzbackend_backendz00,
		BgL_bgl_za762backendza7d2var2016z00,
		BGl_z62backendzd2variableszd2setz12z70zzbackend_backendz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2typedzd2funcallzd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2typ2017z00,
		BGl_z62backendzd2typedzd2funcallz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2variableszd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2var2018z00,
		BGl_z62backendzd2variableszb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2foreignzd2clausezd2supportzd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2for2019z00,
		BGl_z62backendzd2foreignzd2clausezd2supportzb0zzbackend_backendz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_backendzd2subtypezf3zd2envzf3zzbackend_backendz00,
		BgL_bgl_za762backendza7d2sub2020z00,
		BGl_z62backendzd2subtypezf3z43zzbackend_backendz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2requirezd2tailczd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2req2021z00,
		BGl_z62backendzd2requirezd2tailcz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2externzd2functionszd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2ext2022z00,
		BGl_z62backendzd2externzd2functionsz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2pregisterszd2envz00zzbackend_backendz00,
		BgL_bgl_za762backendza7d2pre2023z00,
		BGl_z62backendzd2pregisterszb0zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_backendzd2gczd2initzd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2gcza72024za7,
		BGl_z62backendzd2gczd2initz62zzbackend_backendz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_backendzd2debugzd2supportzd2envzd2zzbackend_backendz00,
		BgL_bgl_za762backendza7d2deb2025z00,
		BGl_z62backendzd2debugzd2supportz62zzbackend_backendz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzbackend_backendz00));
		     ADD_ROOT((void *) (&BGl_za2thezd2backendza2zd2zzbackend_backendz00));
		     ADD_ROOT((void *) (&BGl_za2backendsza2z00zzbackend_backendz00));
		     ADD_ROOT((void *) (&BGl_backendz00zzbackend_backendz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzbackend_backendz00(long
		BgL_checksumz00_1662, char *BgL_fromz00_1663)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_backendz00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_backendz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_backendz00();
					BGl_libraryzd2moduleszd2initz00zzbackend_backendz00();
					BGl_cnstzd2initzd2zzbackend_backendz00();
					BGl_importedzd2moduleszd2initz00zzbackend_backendz00();
					BGl_objectzd2initzd2zzbackend_backendz00();
					BGl_genericzd2initzd2zzbackend_backendz00();
					return BGl_toplevelzd2initzd2zzbackend_backendz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "backend_backend");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"backend_backend");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_backend");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"backend_backend");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "backend_backend");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"backend_backend");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "backend_backend");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"backend_backend");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"backend_backend");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"backend_backend");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_backend");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.scm 15 */
			{	/* BackEnd/backend.scm 15 */
				obj_t BgL_cportz00_1528;

				{	/* BackEnd/backend.scm 15 */
					obj_t BgL_stringz00_1535;

					BgL_stringz00_1535 = BGl_string1819z00zzbackend_backendz00;
					{	/* BackEnd/backend.scm 15 */
						obj_t BgL_startz00_1536;

						BgL_startz00_1536 = BINT(0L);
						{	/* BackEnd/backend.scm 15 */
							obj_t BgL_endz00_1537;

							BgL_endz00_1537 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1535)));
							{	/* BackEnd/backend.scm 15 */

								BgL_cportz00_1528 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1535, BgL_startz00_1536, BgL_endz00_1537);
				}}}}
				{
					long BgL_iz00_1529;

					BgL_iz00_1529 = 49L;
				BgL_loopz00_1530:
					if ((BgL_iz00_1529 == -1L))
						{	/* BackEnd/backend.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/backend.scm 15 */
							{	/* BackEnd/backend.scm 15 */
								obj_t BgL_arg1820z00_1531;

								{	/* BackEnd/backend.scm 15 */

									{	/* BackEnd/backend.scm 15 */
										obj_t BgL_locationz00_1533;

										BgL_locationz00_1533 = BBOOL(((bool_t) 0));
										{	/* BackEnd/backend.scm 15 */

											BgL_arg1820z00_1531 =
												BGl_readz00zz__readerz00(BgL_cportz00_1528,
												BgL_locationz00_1533);
										}
									}
								}
								{	/* BackEnd/backend.scm 15 */
									int BgL_tmpz00_1694;

									BgL_tmpz00_1694 = (int) (BgL_iz00_1529);
									CNST_TABLE_SET(BgL_tmpz00_1694, BgL_arg1820z00_1531);
							}}
							{	/* BackEnd/backend.scm 15 */
								int BgL_auxz00_1534;

								BgL_auxz00_1534 = (int) ((BgL_iz00_1529 - 1L));
								{
									long BgL_iz00_1699;

									BgL_iz00_1699 = (long) (BgL_auxz00_1534);
									BgL_iz00_1529 = BgL_iz00_1699;
									goto BgL_loopz00_1530;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.scm 15 */
			BGl_za2thezd2backendza2zd2zzbackend_backendz00 = BUNSPEC;
			BGl_za2backendsza2z00zzbackend_backendz00 = BNIL;
			return BUNSPEC;
		}

	}



/* backend? */
	BGL_EXPORTED_DEF bool_t BGl_backendzf3zf3zzbackend_backendz00(obj_t
		BgL_objz00_3)
	{
		{	/* BackEnd/backend.sch 84 */
			{	/* BackEnd/backend.sch 84 */
				obj_t BgL_classz00_1539;

				BgL_classz00_1539 = BGl_backendz00zzbackend_backendz00;
				if (BGL_OBJECTP(BgL_objz00_3))
					{	/* BackEnd/backend.sch 84 */
						BgL_objectz00_bglt BgL_arg1807z00_1540;

						BgL_arg1807z00_1540 = (BgL_objectz00_bglt) (BgL_objz00_3);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* BackEnd/backend.sch 84 */
								long BgL_idxz00_1541;

								BgL_idxz00_1541 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1540);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_1541 + 1L)) == BgL_classz00_1539);
							}
						else
							{	/* BackEnd/backend.sch 84 */
								bool_t BgL_res1643z00_1544;

								{	/* BackEnd/backend.sch 84 */
									obj_t BgL_oclassz00_1545;

									{	/* BackEnd/backend.sch 84 */
										obj_t BgL_arg1815z00_1546;
										long BgL_arg1816z00_1547;

										BgL_arg1815z00_1546 = (BGl_za2classesza2z00zz__objectz00);
										{	/* BackEnd/backend.sch 84 */
											long BgL_arg1817z00_1548;

											BgL_arg1817z00_1548 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1540);
											BgL_arg1816z00_1547 = (BgL_arg1817z00_1548 - OBJECT_TYPE);
										}
										BgL_oclassz00_1545 =
											VECTOR_REF(BgL_arg1815z00_1546, BgL_arg1816z00_1547);
									}
									{	/* BackEnd/backend.sch 84 */
										bool_t BgL__ortest_1115z00_1549;

										BgL__ortest_1115z00_1549 =
											(BgL_classz00_1539 == BgL_oclassz00_1545);
										if (BgL__ortest_1115z00_1549)
											{	/* BackEnd/backend.sch 84 */
												BgL_res1643z00_1544 = BgL__ortest_1115z00_1549;
											}
										else
											{	/* BackEnd/backend.sch 84 */
												long BgL_odepthz00_1550;

												{	/* BackEnd/backend.sch 84 */
													obj_t BgL_arg1804z00_1551;

													BgL_arg1804z00_1551 = (BgL_oclassz00_1545);
													BgL_odepthz00_1550 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_1551);
												}
												if ((1L < BgL_odepthz00_1550))
													{	/* BackEnd/backend.sch 84 */
														obj_t BgL_arg1802z00_1552;

														{	/* BackEnd/backend.sch 84 */
															obj_t BgL_arg1803z00_1553;

															BgL_arg1803z00_1553 = (BgL_oclassz00_1545);
															BgL_arg1802z00_1552 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1553,
																1L);
														}
														BgL_res1643z00_1544 =
															(BgL_arg1802z00_1552 == BgL_classz00_1539);
													}
												else
													{	/* BackEnd/backend.sch 84 */
														BgL_res1643z00_1544 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1643z00_1544;
							}
					}
				else
					{	/* BackEnd/backend.sch 84 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &backend? */
	obj_t BGl_z62backendzf3z91zzbackend_backendz00(obj_t BgL_envz00_1004,
		obj_t BgL_objz00_1005)
	{
		{	/* BackEnd/backend.sch 84 */
			return BBOOL(BGl_backendzf3zf3zzbackend_backendz00(BgL_objz00_1005));
		}

	}



/* backend-nil */
	BGL_EXPORTED_DEF BgL_backendz00_bglt
		BGl_backendzd2nilzd2zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.sch 85 */
			{	/* BackEnd/backend.sch 85 */
				obj_t BgL_classz00_655;

				BgL_classz00_655 = BGl_backendz00zzbackend_backendz00;
				{	/* BackEnd/backend.sch 85 */
					obj_t BgL__ortest_1117z00_656;

					BgL__ortest_1117z00_656 = BGL_CLASS_NIL(BgL_classz00_655);
					if (CBOOL(BgL__ortest_1117z00_656))
						{	/* BackEnd/backend.sch 85 */
							return ((BgL_backendz00_bglt) BgL__ortest_1117z00_656);
						}
					else
						{	/* BackEnd/backend.sch 85 */
							return
								((BgL_backendz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_655));
						}
				}
			}
		}

	}



/* &backend-nil */
	BgL_backendz00_bglt BGl_z62backendzd2nilzb0zzbackend_backendz00(obj_t
		BgL_envz00_1006)
	{
		{	/* BackEnd/backend.sch 85 */
			return BGl_backendzd2nilzd2zzbackend_backendz00();
		}

	}



/* backend-string-literal-support */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2stringzd2literalzd2supportzd2zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_4)
	{
		{	/* BackEnd/backend.sch 86 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_4))->
				BgL_stringzd2literalzd2supportz00);
		}

	}



/* &backend-string-literal-support */
	obj_t BGl_z62backendzd2stringzd2literalzd2supportzb0zzbackend_backendz00(obj_t
		BgL_envz00_1007, obj_t BgL_oz00_1008)
	{
		{	/* BackEnd/backend.sch 86 */
			return
				BBOOL(BGl_backendzd2stringzd2literalzd2supportzd2zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1008)));
		}

	}



/* backend-string-literal-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2stringzd2literalzd2supportzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_5, bool_t BgL_vz00_6)
	{
		{	/* BackEnd/backend.sch 87 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_5))->
					BgL_stringzd2literalzd2supportz00) = ((bool_t) BgL_vz00_6), BUNSPEC);
		}

	}



/* &backend-string-literal-support-set! */
	obj_t
		BGl_z62backendzd2stringzd2literalzd2supportzd2setz12z70zzbackend_backendz00
		(obj_t BgL_envz00_1009, obj_t BgL_oz00_1010, obj_t BgL_vz00_1011)
	{
		{	/* BackEnd/backend.sch 87 */
			return
				BGl_backendzd2stringzd2literalzd2supportzd2setz12z12zzbackend_backendz00
				(((BgL_backendz00_bglt) BgL_oz00_1010), CBOOL(BgL_vz00_1011));
		}

	}



/* backend-force-register-gc-roots */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2forcezd2registerzd2gczd2rootsz00zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_7)
	{
		{	/* BackEnd/backend.sch 88 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_7))->
				BgL_forcezd2registerzd2gczd2rootszd2);
		}

	}



/* &backend-force-register-gc-roots */
	obj_t
		BGl_z62backendzd2forcezd2registerzd2gczd2rootsz62zzbackend_backendz00(obj_t
		BgL_envz00_1012, obj_t BgL_oz00_1013)
	{
		{	/* BackEnd/backend.sch 88 */
			return
				BBOOL(BGl_backendzd2forcezd2registerzd2gczd2rootsz00zzbackend_backendz00
				(((BgL_backendz00_bglt) BgL_oz00_1013)));
		}

	}



/* backend-force-register-gc-roots-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2forcezd2registerzd2gczd2rootszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_8, bool_t BgL_vz00_9)
	{
		{	/* BackEnd/backend.sch 89 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_8))->
					BgL_forcezd2registerzd2gczd2rootszd2) =
				((bool_t) BgL_vz00_9), BUNSPEC);
		}

	}



/* &backend-force-register-gc-roots-set! */
	obj_t
		BGl_z62backendzd2forcezd2registerzd2gczd2rootszd2setz12za2zzbackend_backendz00
		(obj_t BgL_envz00_1014, obj_t BgL_oz00_1015, obj_t BgL_vz00_1016)
	{
		{	/* BackEnd/backend.sch 89 */
			return
				BGl_backendzd2forcezd2registerzd2gczd2rootszd2setz12zc0zzbackend_backendz00
				(((BgL_backendz00_bglt) BgL_oz00_1015), CBOOL(BgL_vz00_1016));
		}

	}



/* backend-strict-type-cast */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2strictzd2typezd2castzd2zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_10)
	{
		{	/* BackEnd/backend.sch 90 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_10))->
				BgL_strictzd2typezd2castz00);
		}

	}



/* &backend-strict-type-cast */
	obj_t BGl_z62backendzd2strictzd2typezd2castzb0zzbackend_backendz00(obj_t
		BgL_envz00_1017, obj_t BgL_oz00_1018)
	{
		{	/* BackEnd/backend.sch 90 */
			return
				BBOOL(BGl_backendzd2strictzd2typezd2castzd2zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1018)));
		}

	}



/* backend-strict-type-cast-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2strictzd2typezd2castzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_11, bool_t BgL_vz00_12)
	{
		{	/* BackEnd/backend.sch 91 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_11))->
					BgL_strictzd2typezd2castz00) = ((bool_t) BgL_vz00_12), BUNSPEC);
		}

	}



/* &backend-strict-type-cast-set! */
	obj_t
		BGl_z62backendzd2strictzd2typezd2castzd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1019, obj_t BgL_oz00_1020, obj_t BgL_vz00_1021)
	{
		{	/* BackEnd/backend.sch 91 */
			return
				BGl_backendzd2strictzd2typezd2castzd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1020), CBOOL(BgL_vz00_1021));
		}

	}



/* backend-typed-funcall */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2typedzd2funcallz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_13)
	{
		{	/* BackEnd/backend.sch 92 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_13))->BgL_typedzd2funcallzd2);
		}

	}



/* &backend-typed-funcall */
	obj_t BGl_z62backendzd2typedzd2funcallz62zzbackend_backendz00(obj_t
		BgL_envz00_1022, obj_t BgL_oz00_1023)
	{
		{	/* BackEnd/backend.sch 92 */
			return
				BBOOL(BGl_backendzd2typedzd2funcallz00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1023)));
		}

	}



/* backend-typed-funcall-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2typedzd2funcallzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_14, bool_t BgL_vz00_15)
	{
		{	/* BackEnd/backend.sch 93 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_14))->
					BgL_typedzd2funcallzd2) = ((bool_t) BgL_vz00_15), BUNSPEC);
		}

	}



/* &backend-typed-funcall-set! */
	obj_t BGl_z62backendzd2typedzd2funcallzd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1024, obj_t BgL_oz00_1025, obj_t BgL_vz00_1026)
	{
		{	/* BackEnd/backend.sch 93 */
			return
				BGl_backendzd2typedzd2funcallzd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1025), CBOOL(BgL_vz00_1026));
		}

	}



/* backend-type-check */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2typezd2checkz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_16)
	{
		{	/* BackEnd/backend.sch 94 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_16))->BgL_typezd2checkzd2);
		}

	}



/* &backend-type-check */
	obj_t BGl_z62backendzd2typezd2checkz62zzbackend_backendz00(obj_t
		BgL_envz00_1027, obj_t BgL_oz00_1028)
	{
		{	/* BackEnd/backend.sch 94 */
			return
				BBOOL(BGl_backendzd2typezd2checkz00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1028)));
		}

	}



/* backend-type-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2typezd2checkzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_17, bool_t BgL_vz00_18)
	{
		{	/* BackEnd/backend.sch 95 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_17))->BgL_typezd2checkzd2) =
				((bool_t) BgL_vz00_18), BUNSPEC);
		}

	}



/* &backend-type-check-set! */
	obj_t BGl_z62backendzd2typezd2checkzd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1029, obj_t BgL_oz00_1030, obj_t BgL_vz00_1031)
	{
		{	/* BackEnd/backend.sch 95 */
			return
				BGl_backendzd2typezd2checkzd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1030), CBOOL(BgL_vz00_1031));
		}

	}



/* backend-bound-check */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2boundzd2checkz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_19)
	{
		{	/* BackEnd/backend.sch 96 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_19))->BgL_boundzd2checkzd2);
		}

	}



/* &backend-bound-check */
	obj_t BGl_z62backendzd2boundzd2checkz62zzbackend_backendz00(obj_t
		BgL_envz00_1032, obj_t BgL_oz00_1033)
	{
		{	/* BackEnd/backend.sch 96 */
			return
				BBOOL(BGl_backendzd2boundzd2checkz00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1033)));
		}

	}



/* backend-bound-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2boundzd2checkzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_20, bool_t BgL_vz00_21)
	{
		{	/* BackEnd/backend.sch 97 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_20))->BgL_boundzd2checkzd2) =
				((bool_t) BgL_vz00_21), BUNSPEC);
		}

	}



/* &backend-bound-check-set! */
	obj_t BGl_z62backendzd2boundzd2checkzd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1034, obj_t BgL_oz00_1035, obj_t BgL_vz00_1036)
	{
		{	/* BackEnd/backend.sch 97 */
			return
				BGl_backendzd2boundzd2checkzd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1035), CBOOL(BgL_vz00_1036));
		}

	}



/* backend-pregisters */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2pregisterszd2zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_22)
	{
		{	/* BackEnd/backend.sch 98 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_22))->BgL_pregistersz00);
		}

	}



/* &backend-pregisters */
	obj_t BGl_z62backendzd2pregisterszb0zzbackend_backendz00(obj_t
		BgL_envz00_1037, obj_t BgL_oz00_1038)
	{
		{	/* BackEnd/backend.sch 98 */
			return
				BGl_backendzd2pregisterszd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1038));
		}

	}



/* backend-pregisters-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2pregisterszd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_23, obj_t BgL_vz00_24)
	{
		{	/* BackEnd/backend.sch 99 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_23))->BgL_pregistersz00) =
				((obj_t) BgL_vz00_24), BUNSPEC);
		}

	}



/* &backend-pregisters-set! */
	obj_t BGl_z62backendzd2pregisterszd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1039, obj_t BgL_oz00_1040, obj_t BgL_vz00_1041)
	{
		{	/* BackEnd/backend.sch 99 */
			return
				BGl_backendzd2pregisterszd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1040), BgL_vz00_1041);
		}

	}



/* backend-registers */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2registerszd2zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_25)
	{
		{	/* BackEnd/backend.sch 100 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_25))->BgL_registersz00);
		}

	}



/* &backend-registers */
	obj_t BGl_z62backendzd2registerszb0zzbackend_backendz00(obj_t BgL_envz00_1042,
		obj_t BgL_oz00_1043)
	{
		{	/* BackEnd/backend.sch 100 */
			return
				BGl_backendzd2registerszd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1043));
		}

	}



/* backend-registers-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2registerszd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* BackEnd/backend.sch 101 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_26))->BgL_registersz00) =
				((obj_t) BgL_vz00_27), BUNSPEC);
		}

	}



/* &backend-registers-set! */
	obj_t BGl_z62backendzd2registerszd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1044, obj_t BgL_oz00_1045, obj_t BgL_vz00_1046)
	{
		{	/* BackEnd/backend.sch 101 */
			return
				BGl_backendzd2registerszd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1045), BgL_vz00_1046);
		}

	}



/* backend-require-tailc */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2requirezd2tailcz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_28)
	{
		{	/* BackEnd/backend.sch 102 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_28))->BgL_requirezd2tailczd2);
		}

	}



/* &backend-require-tailc */
	obj_t BGl_z62backendzd2requirezd2tailcz62zzbackend_backendz00(obj_t
		BgL_envz00_1047, obj_t BgL_oz00_1048)
	{
		{	/* BackEnd/backend.sch 102 */
			return
				BBOOL(BGl_backendzd2requirezd2tailcz00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1048)));
		}

	}



/* backend-require-tailc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2requirezd2tailczd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_29, bool_t BgL_vz00_30)
	{
		{	/* BackEnd/backend.sch 103 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_29))->
					BgL_requirezd2tailczd2) = ((bool_t) BgL_vz00_30), BUNSPEC);
		}

	}



/* &backend-require-tailc-set! */
	obj_t BGl_z62backendzd2requirezd2tailczd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1049, obj_t BgL_oz00_1050, obj_t BgL_vz00_1051)
	{
		{	/* BackEnd/backend.sch 103 */
			return
				BGl_backendzd2requirezd2tailczd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1050), CBOOL(BgL_vz00_1051));
		}

	}



/* backend-tvector-descr-support */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2tvectorzd2descrzd2supportzd2zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_31)
	{
		{	/* BackEnd/backend.sch 104 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_31))->
				BgL_tvectorzd2descrzd2supportz00);
		}

	}



/* &backend-tvector-descr-support */
	obj_t BGl_z62backendzd2tvectorzd2descrzd2supportzb0zzbackend_backendz00(obj_t
		BgL_envz00_1052, obj_t BgL_oz00_1053)
	{
		{	/* BackEnd/backend.sch 104 */
			return
				BBOOL(BGl_backendzd2tvectorzd2descrzd2supportzd2zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1053)));
		}

	}



/* backend-tvector-descr-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_32, bool_t BgL_vz00_33)
	{
		{	/* BackEnd/backend.sch 105 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_32))->
					BgL_tvectorzd2descrzd2supportz00) = ((bool_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &backend-tvector-descr-support-set! */
	obj_t
		BGl_z62backendzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_backendz00
		(obj_t BgL_envz00_1054, obj_t BgL_oz00_1055, obj_t BgL_vz00_1056)
	{
		{	/* BackEnd/backend.sch 105 */
			return
				BGl_backendzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1055), CBOOL(BgL_vz00_1056));
		}

	}



/* backend-pragma-support */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2pragmazd2supportz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_34)
	{
		{	/* BackEnd/backend.sch 106 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_34))->BgL_pragmazd2supportzd2);
		}

	}



/* &backend-pragma-support */
	obj_t BGl_z62backendzd2pragmazd2supportz62zzbackend_backendz00(obj_t
		BgL_envz00_1057, obj_t BgL_oz00_1058)
	{
		{	/* BackEnd/backend.sch 106 */
			return
				BBOOL(BGl_backendzd2pragmazd2supportz00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1058)));
		}

	}



/* backend-pragma-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2pragmazd2supportzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_35, bool_t BgL_vz00_36)
	{
		{	/* BackEnd/backend.sch 107 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_35))->
					BgL_pragmazd2supportzd2) = ((bool_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &backend-pragma-support-set! */
	obj_t BGl_z62backendzd2pragmazd2supportzd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1059, obj_t BgL_oz00_1060, obj_t BgL_vz00_1061)
	{
		{	/* BackEnd/backend.sch 107 */
			return
				BGl_backendzd2pragmazd2supportzd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1060), CBOOL(BgL_vz00_1061));
		}

	}



/* backend-debug-support */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2debugzd2supportz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_37)
	{
		{	/* BackEnd/backend.sch 108 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_37))->BgL_debugzd2supportzd2);
		}

	}



/* &backend-debug-support */
	obj_t BGl_z62backendzd2debugzd2supportz62zzbackend_backendz00(obj_t
		BgL_envz00_1062, obj_t BgL_oz00_1063)
	{
		{	/* BackEnd/backend.sch 108 */
			return
				BGl_backendzd2debugzd2supportz00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1063));
		}

	}



/* backend-debug-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2debugzd2supportzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_38, obj_t BgL_vz00_39)
	{
		{	/* BackEnd/backend.sch 109 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_38))->
					BgL_debugzd2supportzd2) = ((obj_t) BgL_vz00_39), BUNSPEC);
		}

	}



/* &backend-debug-support-set! */
	obj_t BGl_z62backendzd2debugzd2supportzd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1064, obj_t BgL_oz00_1065, obj_t BgL_vz00_1066)
	{
		{	/* BackEnd/backend.sch 109 */
			return
				BGl_backendzd2debugzd2supportzd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1065), BgL_vz00_1066);
		}

	}



/* backend-foreign-clause-support */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2foreignzd2clausezd2supportzd2zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_40)
	{
		{	/* BackEnd/backend.sch 110 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_40))->
				BgL_foreignzd2clausezd2supportz00);
		}

	}



/* &backend-foreign-clause-support */
	obj_t BGl_z62backendzd2foreignzd2clausezd2supportzb0zzbackend_backendz00(obj_t
		BgL_envz00_1067, obj_t BgL_oz00_1068)
	{
		{	/* BackEnd/backend.sch 110 */
			return
				BGl_backendzd2foreignzd2clausezd2supportzd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1068));
		}

	}



/* backend-foreign-clause-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_41, obj_t BgL_vz00_42)
	{
		{	/* BackEnd/backend.sch 111 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_41))->
					BgL_foreignzd2clausezd2supportz00) = ((obj_t) BgL_vz00_42), BUNSPEC);
		}

	}



/* &backend-foreign-clause-support-set! */
	obj_t
		BGl_z62backendzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_backendz00
		(obj_t BgL_envz00_1069, obj_t BgL_oz00_1070, obj_t BgL_vz00_1071)
	{
		{	/* BackEnd/backend.sch 111 */
			return
				BGl_backendzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_backendz00
				(((BgL_backendz00_bglt) BgL_oz00_1070), BgL_vz00_1071);
		}

	}



/* backend-trace-support */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2tracezd2supportz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_43)
	{
		{	/* BackEnd/backend.sch 112 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_43))->BgL_tracezd2supportzd2);
		}

	}



/* &backend-trace-support */
	obj_t BGl_z62backendzd2tracezd2supportz62zzbackend_backendz00(obj_t
		BgL_envz00_1072, obj_t BgL_oz00_1073)
	{
		{	/* BackEnd/backend.sch 112 */
			return
				BBOOL(BGl_backendzd2tracezd2supportz00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1073)));
		}

	}



/* backend-trace-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2tracezd2supportzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_44, bool_t BgL_vz00_45)
	{
		{	/* BackEnd/backend.sch 113 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_44))->
					BgL_tracezd2supportzd2) = ((bool_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &backend-trace-support-set! */
	obj_t BGl_z62backendzd2tracezd2supportzd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1074, obj_t BgL_oz00_1075, obj_t BgL_vz00_1076)
	{
		{	/* BackEnd/backend.sch 113 */
			return
				BGl_backendzd2tracezd2supportzd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1075), CBOOL(BgL_vz00_1076));
		}

	}



/* backend-typed-eq */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2typedzd2eqz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_46)
	{
		{	/* BackEnd/backend.sch 114 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_46))->BgL_typedzd2eqzd2);
		}

	}



/* &backend-typed-eq */
	obj_t BGl_z62backendzd2typedzd2eqz62zzbackend_backendz00(obj_t
		BgL_envz00_1077, obj_t BgL_oz00_1078)
	{
		{	/* BackEnd/backend.sch 114 */
			return
				BBOOL(BGl_backendzd2typedzd2eqz00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1078)));
		}

	}



/* backend-typed-eq-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2typedzd2eqzd2setz12zc0zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_47, bool_t BgL_vz00_48)
	{
		{	/* BackEnd/backend.sch 115 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_47))->BgL_typedzd2eqzd2) =
				((bool_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &backend-typed-eq-set! */
	obj_t BGl_z62backendzd2typedzd2eqzd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1079, obj_t BgL_oz00_1080, obj_t BgL_vz00_1081)
	{
		{	/* BackEnd/backend.sch 115 */
			return
				BGl_backendzd2typedzd2eqzd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1080), CBOOL(BgL_vz00_1081));
		}

	}



/* backend-foreign-closure */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2foreignzd2closurez00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_49)
	{
		{	/* BackEnd/backend.sch 116 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_49))->
				BgL_foreignzd2closurezd2);
		}

	}



/* &backend-foreign-closure */
	obj_t BGl_z62backendzd2foreignzd2closurez62zzbackend_backendz00(obj_t
		BgL_envz00_1082, obj_t BgL_oz00_1083)
	{
		{	/* BackEnd/backend.sch 116 */
			return
				BBOOL(BGl_backendzd2foreignzd2closurez00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1083)));
		}

	}



/* backend-foreign-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2foreignzd2closurezd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_50, bool_t BgL_vz00_51)
	{
		{	/* BackEnd/backend.sch 117 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_50))->
					BgL_foreignzd2closurezd2) = ((bool_t) BgL_vz00_51), BUNSPEC);
		}

	}



/* &backend-foreign-closure-set! */
	obj_t BGl_z62backendzd2foreignzd2closurezd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1084, obj_t BgL_oz00_1085, obj_t BgL_vz00_1086)
	{
		{	/* BackEnd/backend.sch 117 */
			return
				BGl_backendzd2foreignzd2closurezd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1085), CBOOL(BgL_vz00_1086));
		}

	}



/* backend-remove-empty-let */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2removezd2emptyzd2letzd2zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_52)
	{
		{	/* BackEnd/backend.sch 118 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_52))->
				BgL_removezd2emptyzd2letz00);
		}

	}



/* &backend-remove-empty-let */
	obj_t BGl_z62backendzd2removezd2emptyzd2letzb0zzbackend_backendz00(obj_t
		BgL_envz00_1087, obj_t BgL_oz00_1088)
	{
		{	/* BackEnd/backend.sch 118 */
			return
				BBOOL(BGl_backendzd2removezd2emptyzd2letzd2zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1088)));
		}

	}



/* backend-remove-empty-let-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2removezd2emptyzd2letzd2setz12z12zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_53, bool_t BgL_vz00_54)
	{
		{	/* BackEnd/backend.sch 119 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_53))->
					BgL_removezd2emptyzd2letz00) = ((bool_t) BgL_vz00_54), BUNSPEC);
		}

	}



/* &backend-remove-empty-let-set! */
	obj_t
		BGl_z62backendzd2removezd2emptyzd2letzd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1089, obj_t BgL_oz00_1090, obj_t BgL_vz00_1091)
	{
		{	/* BackEnd/backend.sch 119 */
			return
				BGl_backendzd2removezd2emptyzd2letzd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1090), CBOOL(BgL_vz00_1091));
		}

	}



/* backend-effect+ */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2effectzb2z60zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_55)
	{
		{	/* BackEnd/backend.sch 120 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_55))->BgL_effectzb2zb2);
		}

	}



/* &backend-effect+ */
	obj_t BGl_z62backendzd2effectzb2z02zzbackend_backendz00(obj_t BgL_envz00_1092,
		obj_t BgL_oz00_1093)
	{
		{	/* BackEnd/backend.sch 120 */
			return
				BBOOL(BGl_backendzd2effectzb2z60zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1093)));
		}

	}



/* backend-effect+-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2effectzb2zd2setz12za0zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_56, bool_t BgL_vz00_57)
	{
		{	/* BackEnd/backend.sch 121 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_56))->BgL_effectzb2zb2) =
				((bool_t) BgL_vz00_57), BUNSPEC);
		}

	}



/* &backend-effect+-set! */
	obj_t BGl_z62backendzd2effectzb2zd2setz12zc2zzbackend_backendz00(obj_t
		BgL_envz00_1094, obj_t BgL_oz00_1095, obj_t BgL_vz00_1096)
	{
		{	/* BackEnd/backend.sch 121 */
			return
				BGl_backendzd2effectzb2zd2setz12za0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1095), CBOOL(BgL_vz00_1096));
		}

	}



/* backend-qualified-types */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2qualifiedzd2typesz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_58)
	{
		{	/* BackEnd/backend.sch 122 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_58))->
				BgL_qualifiedzd2typeszd2);
		}

	}



/* &backend-qualified-types */
	obj_t BGl_z62backendzd2qualifiedzd2typesz62zzbackend_backendz00(obj_t
		BgL_envz00_1097, obj_t BgL_oz00_1098)
	{
		{	/* BackEnd/backend.sch 122 */
			return
				BBOOL(BGl_backendzd2qualifiedzd2typesz00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1098)));
		}

	}



/* backend-qualified-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2qualifiedzd2typeszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_59, bool_t BgL_vz00_60)
	{
		{	/* BackEnd/backend.sch 123 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_59))->
					BgL_qualifiedzd2typeszd2) = ((bool_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &backend-qualified-types-set! */
	obj_t BGl_z62backendzd2qualifiedzd2typeszd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1099, obj_t BgL_oz00_1100, obj_t BgL_vz00_1101)
	{
		{	/* BackEnd/backend.sch 123 */
			return
				BGl_backendzd2qualifiedzd2typeszd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1100), CBOOL(BgL_vz00_1101));
		}

	}



/* backend-callcc */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2callcczd2zzbackend_backendz00(BgL_backendz00_bglt BgL_oz00_61)
	{
		{	/* BackEnd/backend.sch 124 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_61))->BgL_callccz00);
		}

	}



/* &backend-callcc */
	obj_t BGl_z62backendzd2callcczb0zzbackend_backendz00(obj_t BgL_envz00_1102,
		obj_t BgL_oz00_1103)
	{
		{	/* BackEnd/backend.sch 124 */
			return
				BBOOL(BGl_backendzd2callcczd2zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1103)));
		}

	}



/* backend-callcc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2callcczd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_62, bool_t BgL_vz00_63)
	{
		{	/* BackEnd/backend.sch 125 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_62))->BgL_callccz00) =
				((bool_t) BgL_vz00_63), BUNSPEC);
		}

	}



/* &backend-callcc-set! */
	obj_t BGl_z62backendzd2callcczd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1104, obj_t BgL_oz00_1105, obj_t BgL_vz00_1106)
	{
		{	/* BackEnd/backend.sch 125 */
			return
				BGl_backendzd2callcczd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1105), CBOOL(BgL_vz00_1106));
		}

	}



/* backend-heap-compatible */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2heapzd2compatiblez00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_64)
	{
		{	/* BackEnd/backend.sch 126 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_64))->
				BgL_heapzd2compatiblezd2);
		}

	}



/* &backend-heap-compatible */
	obj_t BGl_z62backendzd2heapzd2compatiblez62zzbackend_backendz00(obj_t
		BgL_envz00_1107, obj_t BgL_oz00_1108)
	{
		{	/* BackEnd/backend.sch 126 */
			return
				BGl_backendzd2heapzd2compatiblez00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1108));
		}

	}



/* backend-heap-compatible-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2heapzd2compatiblezd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_65, obj_t BgL_vz00_66)
	{
		{	/* BackEnd/backend.sch 127 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_65))->
					BgL_heapzd2compatiblezd2) = ((obj_t) BgL_vz00_66), BUNSPEC);
		}

	}



/* &backend-heap-compatible-set! */
	obj_t BGl_z62backendzd2heapzd2compatiblezd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1109, obj_t BgL_oz00_1110, obj_t BgL_vz00_1111)
	{
		{	/* BackEnd/backend.sch 127 */
			return
				BGl_backendzd2heapzd2compatiblezd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1110), BgL_vz00_1111);
		}

	}



/* backend-heap-suffix */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2heapzd2suffixz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_67)
	{
		{	/* BackEnd/backend.sch 128 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_67))->BgL_heapzd2suffixzd2);
		}

	}



/* &backend-heap-suffix */
	obj_t BGl_z62backendzd2heapzd2suffixz62zzbackend_backendz00(obj_t
		BgL_envz00_1112, obj_t BgL_oz00_1113)
	{
		{	/* BackEnd/backend.sch 128 */
			return
				BGl_backendzd2heapzd2suffixz00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1113));
		}

	}



/* backend-heap-suffix-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2heapzd2suffixzd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_68, obj_t BgL_vz00_69)
	{
		{	/* BackEnd/backend.sch 129 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_68))->BgL_heapzd2suffixzd2) =
				((obj_t) BgL_vz00_69), BUNSPEC);
		}

	}



/* &backend-heap-suffix-set! */
	obj_t BGl_z62backendzd2heapzd2suffixzd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1114, obj_t BgL_oz00_1115, obj_t BgL_vz00_1116)
	{
		{	/* BackEnd/backend.sch 129 */
			return
				BGl_backendzd2heapzd2suffixzd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1115), BgL_vz00_1116);
		}

	}



/* backend-typed */
	BGL_EXPORTED_DEF bool_t
		BGl_backendzd2typedzd2zzbackend_backendz00(BgL_backendz00_bglt BgL_oz00_70)
	{
		{	/* BackEnd/backend.sch 130 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_70))->BgL_typedz00);
		}

	}



/* &backend-typed */
	obj_t BGl_z62backendzd2typedzb0zzbackend_backendz00(obj_t BgL_envz00_1117,
		obj_t BgL_oz00_1118)
	{
		{	/* BackEnd/backend.sch 130 */
			return
				BBOOL(BGl_backendzd2typedzd2zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_oz00_1118)));
		}

	}



/* backend-typed-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2typedzd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_71, bool_t BgL_vz00_72)
	{
		{	/* BackEnd/backend.sch 131 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_71))->BgL_typedz00) =
				((bool_t) BgL_vz00_72), BUNSPEC);
		}

	}



/* &backend-typed-set! */
	obj_t BGl_z62backendzd2typedzd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1119, obj_t BgL_oz00_1120, obj_t BgL_vz00_1121)
	{
		{	/* BackEnd/backend.sch 131 */
			return
				BGl_backendzd2typedzd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1120), CBOOL(BgL_vz00_1121));
		}

	}



/* backend-types */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2typeszd2zzbackend_backendz00(BgL_backendz00_bglt BgL_oz00_73)
	{
		{	/* BackEnd/backend.sch 132 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_73))->BgL_typesz00);
		}

	}



/* &backend-types */
	obj_t BGl_z62backendzd2typeszb0zzbackend_backendz00(obj_t BgL_envz00_1122,
		obj_t BgL_oz00_1123)
	{
		{	/* BackEnd/backend.sch 132 */
			return
				BGl_backendzd2typeszd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1123));
		}

	}



/* backend-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2typeszd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_74, obj_t BgL_vz00_75)
	{
		{	/* BackEnd/backend.sch 133 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_74))->BgL_typesz00) =
				((obj_t) BgL_vz00_75), BUNSPEC);
		}

	}



/* &backend-types-set! */
	obj_t BGl_z62backendzd2typeszd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1124, obj_t BgL_oz00_1125, obj_t BgL_vz00_1126)
	{
		{	/* BackEnd/backend.sch 133 */
			return
				BGl_backendzd2typeszd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1125), BgL_vz00_1126);
		}

	}



/* backend-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2functionszd2zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_76)
	{
		{	/* BackEnd/backend.sch 134 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_76))->BgL_functionsz00);
		}

	}



/* &backend-functions */
	obj_t BGl_z62backendzd2functionszb0zzbackend_backendz00(obj_t BgL_envz00_1127,
		obj_t BgL_oz00_1128)
	{
		{	/* BackEnd/backend.sch 134 */
			return
				BGl_backendzd2functionszd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1128));
		}

	}



/* backend-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2functionszd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_77, obj_t BgL_vz00_78)
	{
		{	/* BackEnd/backend.sch 135 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_77))->BgL_functionsz00) =
				((obj_t) BgL_vz00_78), BUNSPEC);
		}

	}



/* &backend-functions-set! */
	obj_t BGl_z62backendzd2functionszd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1129, obj_t BgL_oz00_1130, obj_t BgL_vz00_1131)
	{
		{	/* BackEnd/backend.sch 135 */
			return
				BGl_backendzd2functionszd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1130), BgL_vz00_1131);
		}

	}



/* backend-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2variableszd2zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_79)
	{
		{	/* BackEnd/backend.sch 136 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_79))->BgL_variablesz00);
		}

	}



/* &backend-variables */
	obj_t BGl_z62backendzd2variableszb0zzbackend_backendz00(obj_t BgL_envz00_1132,
		obj_t BgL_oz00_1133)
	{
		{	/* BackEnd/backend.sch 136 */
			return
				BGl_backendzd2variableszd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1133));
		}

	}



/* backend-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2variableszd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_80, obj_t BgL_vz00_81)
	{
		{	/* BackEnd/backend.sch 137 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_80))->BgL_variablesz00) =
				((obj_t) BgL_vz00_81), BUNSPEC);
		}

	}



/* &backend-variables-set! */
	obj_t BGl_z62backendzd2variableszd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1134, obj_t BgL_oz00_1135, obj_t BgL_vz00_1136)
	{
		{	/* BackEnd/backend.sch 137 */
			return
				BGl_backendzd2variableszd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1135), BgL_vz00_1136);
		}

	}



/* backend-extern-types */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2externzd2typesz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_82)
	{
		{	/* BackEnd/backend.sch 138 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_82))->BgL_externzd2typeszd2);
		}

	}



/* &backend-extern-types */
	obj_t BGl_z62backendzd2externzd2typesz62zzbackend_backendz00(obj_t
		BgL_envz00_1137, obj_t BgL_oz00_1138)
	{
		{	/* BackEnd/backend.sch 138 */
			return
				BGl_backendzd2externzd2typesz00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1138));
		}

	}



/* backend-extern-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2externzd2typeszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_83, obj_t BgL_vz00_84)
	{
		{	/* BackEnd/backend.sch 139 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_83))->BgL_externzd2typeszd2) =
				((obj_t) BgL_vz00_84), BUNSPEC);
		}

	}



/* &backend-extern-types-set! */
	obj_t BGl_z62backendzd2externzd2typeszd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1139, obj_t BgL_oz00_1140, obj_t BgL_vz00_1141)
	{
		{	/* BackEnd/backend.sch 139 */
			return
				BGl_backendzd2externzd2typeszd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1140), BgL_vz00_1141);
		}

	}



/* backend-extern-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2externzd2functionsz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_85)
	{
		{	/* BackEnd/backend.sch 140 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_85))->
				BgL_externzd2functionszd2);
		}

	}



/* &backend-extern-functions */
	obj_t BGl_z62backendzd2externzd2functionsz62zzbackend_backendz00(obj_t
		BgL_envz00_1142, obj_t BgL_oz00_1143)
	{
		{	/* BackEnd/backend.sch 140 */
			return
				BGl_backendzd2externzd2functionsz00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1143));
		}

	}



/* backend-extern-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2externzd2functionszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_86, obj_t BgL_vz00_87)
	{
		{	/* BackEnd/backend.sch 141 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_86))->
					BgL_externzd2functionszd2) = ((obj_t) BgL_vz00_87), BUNSPEC);
		}

	}



/* &backend-extern-functions-set! */
	obj_t
		BGl_z62backendzd2externzd2functionszd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1144, obj_t BgL_oz00_1145, obj_t BgL_vz00_1146)
	{
		{	/* BackEnd/backend.sch 141 */
			return
				BGl_backendzd2externzd2functionszd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1145), BgL_vz00_1146);
		}

	}



/* backend-extern-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2externzd2variablesz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_88)
	{
		{	/* BackEnd/backend.sch 142 */
			return
				(((BgL_backendz00_bglt) COBJECT(BgL_oz00_88))->
				BgL_externzd2variableszd2);
		}

	}



/* &backend-extern-variables */
	obj_t BGl_z62backendzd2externzd2variablesz62zzbackend_backendz00(obj_t
		BgL_envz00_1147, obj_t BgL_oz00_1148)
	{
		{	/* BackEnd/backend.sch 142 */
			return
				BGl_backendzd2externzd2variablesz00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1148));
		}

	}



/* backend-extern-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2externzd2variableszd2setz12zc0zzbackend_backendz00
		(BgL_backendz00_bglt BgL_oz00_89, obj_t BgL_vz00_90)
	{
		{	/* BackEnd/backend.sch 143 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_89))->
					BgL_externzd2variableszd2) = ((obj_t) BgL_vz00_90), BUNSPEC);
		}

	}



/* &backend-extern-variables-set! */
	obj_t
		BGl_z62backendzd2externzd2variableszd2setz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1149, obj_t BgL_oz00_1150, obj_t BgL_vz00_1151)
	{
		{	/* BackEnd/backend.sch 143 */
			return
				BGl_backendzd2externzd2variableszd2setz12zc0zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1150), BgL_vz00_1151);
		}

	}



/* backend-name */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2namezd2zzbackend_backendz00(BgL_backendz00_bglt BgL_oz00_91)
	{
		{	/* BackEnd/backend.sch 144 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_91))->BgL_namez00);
		}

	}



/* &backend-name */
	obj_t BGl_z62backendzd2namezb0zzbackend_backendz00(obj_t BgL_envz00_1152,
		obj_t BgL_oz00_1153)
	{
		{	/* BackEnd/backend.sch 144 */
			return
				BGl_backendzd2namezd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1153));
		}

	}



/* backend-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2namezd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_92, obj_t BgL_vz00_93)
	{
		{	/* BackEnd/backend.sch 145 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_92))->BgL_namez00) =
				((obj_t) BgL_vz00_93), BUNSPEC);
		}

	}



/* &backend-name-set! */
	obj_t BGl_z62backendzd2namezd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1154, obj_t BgL_oz00_1155, obj_t BgL_vz00_1156)
	{
		{	/* BackEnd/backend.sch 145 */
			return
				BGl_backendzd2namezd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1155), BgL_vz00_1156);
		}

	}



/* backend-srfi0 */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2srfi0zd2zzbackend_backendz00(BgL_backendz00_bglt BgL_oz00_94)
	{
		{	/* BackEnd/backend.sch 146 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_94))->BgL_srfi0z00);
		}

	}



/* &backend-srfi0 */
	obj_t BGl_z62backendzd2srfi0zb0zzbackend_backendz00(obj_t BgL_envz00_1157,
		obj_t BgL_oz00_1158)
	{
		{	/* BackEnd/backend.sch 146 */
			return
				BGl_backendzd2srfi0zd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1158));
		}

	}



/* backend-srfi0-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2srfi0zd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_95, obj_t BgL_vz00_96)
	{
		{	/* BackEnd/backend.sch 147 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_95))->BgL_srfi0z00) =
				((obj_t) BgL_vz00_96), BUNSPEC);
		}

	}



/* &backend-srfi0-set! */
	obj_t BGl_z62backendzd2srfi0zd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1159, obj_t BgL_oz00_1160, obj_t BgL_vz00_1161)
	{
		{	/* BackEnd/backend.sch 147 */
			return
				BGl_backendzd2srfi0zd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1160), BgL_vz00_1161);
		}

	}



/* backend-language */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2languagezd2zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_97)
	{
		{	/* BackEnd/backend.sch 148 */
			return (((BgL_backendz00_bglt) COBJECT(BgL_oz00_97))->BgL_languagez00);
		}

	}



/* &backend-language */
	obj_t BGl_z62backendzd2languagezb0zzbackend_backendz00(obj_t BgL_envz00_1162,
		obj_t BgL_oz00_1163)
	{
		{	/* BackEnd/backend.sch 148 */
			return
				BGl_backendzd2languagezd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1163));
		}

	}



/* backend-language-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2languagezd2setz12z12zzbackend_backendz00(BgL_backendz00_bglt
		BgL_oz00_98, obj_t BgL_vz00_99)
	{
		{	/* BackEnd/backend.sch 149 */
			return
				((((BgL_backendz00_bglt) COBJECT(BgL_oz00_98))->BgL_languagez00) =
				((obj_t) BgL_vz00_99), BUNSPEC);
		}

	}



/* &backend-language-set! */
	obj_t BGl_z62backendzd2languagezd2setz12z70zzbackend_backendz00(obj_t
		BgL_envz00_1164, obj_t BgL_oz00_1165, obj_t BgL_vz00_1166)
	{
		{	/* BackEnd/backend.sch 149 */
			return
				BGl_backendzd2languagezd2setz12z12zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_oz00_1165), BgL_vz00_1166);
		}

	}



/* set-backend! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2backendz12zc0zzbackend_backendz00(obj_t
		BgL_languagez00_100)
	{
		{	/* BackEnd/backend.scm 79 */
			{	/* BackEnd/backend.scm 80 */
				obj_t BgL_cz00_127;

				BgL_cz00_127 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_languagez00_100,
					BGl_za2backendsza2z00zzbackend_backendz00);
				if (PAIRP(BgL_cz00_127))
					{	/* BackEnd/backend.scm 83 */
						obj_t BgL_fun1081z00_129;

						BgL_fun1081z00_129 = CDR(BgL_cz00_127);
						return (BGl_za2thezd2backendza2zd2zzbackend_backendz00 =
							BGL_PROCEDURE_CALL0(BgL_fun1081z00_129), BUNSPEC);
					}
				else
					{	/* BackEnd/backend.scm 81 */
						return
							BGl_errorz00zz__errorz00(BGl_string1694z00zzbackend_backendz00,
							BGl_string1695z00zzbackend_backendz00, BgL_languagez00_100);
					}
			}
		}

	}



/* &set-backend! */
	obj_t BGl_z62setzd2backendz12za2zzbackend_backendz00(obj_t BgL_envz00_1167,
		obj_t BgL_languagez00_1168)
	{
		{	/* BackEnd/backend.scm 79 */
			return BGl_setzd2backendz12zc0zzbackend_backendz00(BgL_languagez00_1168);
		}

	}



/* the-backend */
	BGL_EXPORTED_DEF obj_t BGl_thezd2backendzd2zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.scm 88 */
			return BGl_za2thezd2backendza2zd2zzbackend_backendz00;
		}

	}



/* &the-backend */
	obj_t BGl_z62thezd2backendzb0zzbackend_backendz00(obj_t BgL_envz00_1169)
	{
		{	/* BackEnd/backend.scm 88 */
			return BGl_thezd2backendzd2zzbackend_backendz00();
		}

	}



/* register-backend! */
	BGL_EXPORTED_DEF obj_t BGl_registerzd2backendz12zc0zzbackend_backendz00(obj_t
		BgL_idz00_101, obj_t BgL_builderz00_102)
	{
		{	/* BackEnd/backend.scm 99 */
			{	/* BackEnd/backend.scm 100 */
				obj_t BgL_arg1082z00_658;

				BgL_arg1082z00_658 = MAKE_YOUNG_PAIR(BgL_idz00_101, BgL_builderz00_102);
				return (BGl_za2backendsza2z00zzbackend_backendz00 =
					MAKE_YOUNG_PAIR(BgL_arg1082z00_658,
						BGl_za2backendsza2z00zzbackend_backendz00), BUNSPEC);
			}
		}

	}



/* &register-backend! */
	obj_t BGl_z62registerzd2backendz12za2zzbackend_backendz00(obj_t
		BgL_envz00_1170, obj_t BgL_idz00_1171, obj_t BgL_builderz00_1172)
	{
		{	/* BackEnd/backend.scm 99 */
			return
				BGl_registerzd2backendz12zc0zzbackend_backendz00(BgL_idz00_1171,
				BgL_builderz00_1172);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.scm 15 */
			{	/* BackEnd/backend.scm 21 */
				obj_t BgL_arg1085z00_133;
				obj_t BgL_arg1087z00_134;

				{	/* BackEnd/backend.scm 21 */
					obj_t BgL_v1055z00_140;

					BgL_v1055z00_140 = create_vector(32L);
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1092z00_141;

						BgL_arg1092z00_141 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc1698z00zzbackend_backendz00,
							BGl_proc1697z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1696z00zzbackend_backendz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1055z00_140, 0L, BgL_arg1092z00_141);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1114z00_154;

						BgL_arg1114z00_154 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc1701z00zzbackend_backendz00,
							BGl_proc1700z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1699z00zzbackend_backendz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1055z00_140, 1L, BgL_arg1114z00_154);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1127z00_167;

						BgL_arg1127z00_167 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(3),
							BGl_proc1704z00zzbackend_backendz00,
							BGl_proc1703z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1702z00zzbackend_backendz00, CNST_TABLE_REF(4));
						VECTOR_SET(BgL_v1055z00_140, 2L, BgL_arg1127z00_167);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1137z00_180;

						BgL_arg1137z00_180 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc1707z00zzbackend_backendz00,
							BGl_proc1706z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1705z00zzbackend_backendz00, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1055z00_140, 3L, BgL_arg1137z00_180);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1145z00_193;

						BgL_arg1145z00_193 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc1710z00zzbackend_backendz00,
							BGl_proc1709z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1708z00zzbackend_backendz00, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1055z00_140, 4L, BgL_arg1145z00_193);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1157z00_206;

						BgL_arg1157z00_206 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc1713z00zzbackend_backendz00,
							BGl_proc1712z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1711z00zzbackend_backendz00, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1055z00_140, 5L, BgL_arg1157z00_206);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1171z00_219;

						BgL_arg1171z00_219 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc1716z00zzbackend_backendz00,
							BGl_proc1715z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1714z00zzbackend_backendz00, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1055z00_140, 6L, BgL_arg1171z00_219);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1187z00_232;

						BgL_arg1187z00_232 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc1719z00zzbackend_backendz00,
							BGl_proc1718z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1717z00zzbackend_backendz00, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1055z00_140, 7L, BgL_arg1187z00_232);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1194z00_245;

						BgL_arg1194z00_245 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc1722z00zzbackend_backendz00,
							BGl_proc1721z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1720z00zzbackend_backendz00, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1055z00_140, 8L, BgL_arg1194z00_245);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1202z00_258;

						BgL_arg1202z00_258 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc1725z00zzbackend_backendz00,
							BGl_proc1724z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1723z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 9L, BgL_arg1202z00_258);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1212z00_271;

						BgL_arg1212z00_271 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc1728z00zzbackend_backendz00,
							BGl_proc1727z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1726z00zzbackend_backendz00, CNST_TABLE_REF(4));
						VECTOR_SET(BgL_v1055z00_140, 10L, BgL_arg1212z00_271);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1223z00_284;

						BgL_arg1223z00_284 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc1731z00zzbackend_backendz00,
							BGl_proc1730z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1729z00zzbackend_backendz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1055z00_140, 11L, BgL_arg1223z00_284);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1231z00_297;

						BgL_arg1231z00_297 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(16),
							BGl_proc1734z00zzbackend_backendz00,
							BGl_proc1733z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1732z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 12L, BgL_arg1231z00_297);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1238z00_310;

						BgL_arg1238z00_310 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(17),
							BGl_proc1737z00zzbackend_backendz00,
							BGl_proc1736z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1735z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 13L, BgL_arg1238z00_310);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1248z00_323;

						BgL_arg1248z00_323 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc1740z00zzbackend_backendz00,
							BGl_proc1739z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1738z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 14L, BgL_arg1248z00_323);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1272z00_336;

						BgL_arg1272z00_336 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(19),
							BGl_proc1743z00zzbackend_backendz00,
							BGl_proc1742z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1741z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 15L, BgL_arg1272z00_336);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1310z00_349;

						BgL_arg1310z00_349 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc1746z00zzbackend_backendz00,
							BGl_proc1745z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1744z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 16L, BgL_arg1310z00_349);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1318z00_362;

						BgL_arg1318z00_362 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc1749z00zzbackend_backendz00,
							BGl_proc1748z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1747z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 17L, BgL_arg1318z00_362);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1325z00_375;

						BgL_arg1325z00_375 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc1752z00zzbackend_backendz00,
							BGl_proc1751z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1750z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 18L, BgL_arg1325z00_375);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1332z00_388;

						BgL_arg1332z00_388 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(23),
							BGl_proc1755z00zzbackend_backendz00,
							BGl_proc1754z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1753z00zzbackend_backendz00, CNST_TABLE_REF(24));
						VECTOR_SET(BgL_v1055z00_140, 19L, BgL_arg1332z00_388);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1343z00_401;

						BgL_arg1343z00_401 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc1758z00zzbackend_backendz00,
							BGl_proc1757z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1756z00zzbackend_backendz00, CNST_TABLE_REF(24));
						VECTOR_SET(BgL_v1055z00_140, 20L, BgL_arg1343z00_401);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1361z00_414;

						BgL_arg1361z00_414 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc1761z00zzbackend_backendz00,
							BGl_proc1760z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1759z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 21L, BgL_arg1361z00_414);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1375z00_427;

						BgL_arg1375z00_427 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc1764z00zzbackend_backendz00,
							BGl_proc1763z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1762z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 22L, BgL_arg1375z00_427);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1408z00_440;

						BgL_arg1408z00_440 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc1767z00zzbackend_backendz00,
							BGl_proc1766z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1765z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 23L, BgL_arg1408z00_440);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1434z00_453;

						BgL_arg1434z00_453 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(29),
							BGl_proc1770z00zzbackend_backendz00,
							BGl_proc1769z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1768z00zzbackend_backendz00, CNST_TABLE_REF(24));
						VECTOR_SET(BgL_v1055z00_140, 24L, BgL_arg1434z00_453);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1472z00_466;

						BgL_arg1472z00_466 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(30),
							BGl_proc1773z00zzbackend_backendz00,
							BGl_proc1772z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1771z00zzbackend_backendz00, CNST_TABLE_REF(24));
						VECTOR_SET(BgL_v1055z00_140, 25L, BgL_arg1472z00_466);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1502z00_479;

						BgL_arg1502z00_479 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(31),
							BGl_proc1776z00zzbackend_backendz00,
							BGl_proc1775z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1774z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 26L, BgL_arg1502z00_479);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1535z00_492;

						BgL_arg1535z00_492 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(32),
							BGl_proc1779z00zzbackend_backendz00,
							BGl_proc1778z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1777z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 27L, BgL_arg1535z00_492);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1552z00_505;

						BgL_arg1552z00_505 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc1782z00zzbackend_backendz00,
							BGl_proc1781z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1780z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 28L, BgL_arg1552z00_505);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1565z00_518;

						BgL_arg1565z00_518 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(34),
							BGl_proc1785z00zzbackend_backendz00,
							BGl_proc1784z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1783z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 29L, BgL_arg1565z00_518);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1584z00_531;

						BgL_arg1584z00_531 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(35),
							BGl_proc1788z00zzbackend_backendz00,
							BGl_proc1787z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1786z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 30L, BgL_arg1584z00_531);
					}
					{	/* BackEnd/backend.scm 21 */
						obj_t BgL_arg1595z00_544;

						BgL_arg1595z00_544 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(36),
							BGl_proc1791z00zzbackend_backendz00,
							BGl_proc1790z00zzbackend_backendz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1789z00zzbackend_backendz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1055z00_140, 31L, BgL_arg1595z00_544);
					}
					BgL_arg1085z00_133 = BgL_v1055z00_140;
				}
				{	/* BackEnd/backend.scm 21 */
					obj_t BgL_v1056z00_557;

					BgL_v1056z00_557 = create_vector(0L);
					BgL_arg1087z00_134 = BgL_v1056z00_557;
				}
				return (BGl_backendz00zzbackend_backendz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(37),
						CNST_TABLE_REF(38), BGl_objectz00zz__objectz00, 52318L, BFALSE,
						BGl_proc1793z00zzbackend_backendz00,
						BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00,
						BGl_proc1792z00zzbackend_backendz00, BFALSE, BgL_arg1085z00_133,
						BgL_arg1087z00_134), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1091> */
	obj_t BGl_z62zc3z04anonymousza31091ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1271, obj_t BgL_new1049z00_1272)
	{
		{	/* BackEnd/backend.scm 21 */
			{
				BgL_backendz00_bglt BgL_auxz00_2105;

				((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_new1049z00_1272)))->
						BgL_languagez00) = ((obj_t) CNST_TABLE_REF(39)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(39)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_namez00) =
					((obj_t) BGl_string1794z00zzbackend_backendz00), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_externzd2variableszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_externzd2functionszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_externzd2typeszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_variablesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_functionsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_typesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_typedz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_heapzd2suffixzd2) =
					((obj_t) BGl_string1794z00zzbackend_backendz00), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_heapzd2compatiblezd2) =
					((obj_t) CNST_TABLE_REF(39)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_callccz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_qualifiedzd2typeszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_effectzb2zb2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_removezd2emptyzd2letz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_foreignzd2closurezd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_typedzd2eqzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_tracezd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_foreignzd2clausezd2supportz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_debugzd2supportzd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_pragmazd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_requirezd2tailczd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_registersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_pregistersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_boundzd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_typezd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_typedzd2funcallzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_strictzd2typezd2castz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1049z00_1272)))->BgL_stringzd2literalzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_2105 = ((BgL_backendz00_bglt) BgL_new1049z00_1272);
				return ((obj_t) BgL_auxz00_2105);
			}
		}

	}



/* &lambda1088 */
	BgL_backendz00_bglt BGl_z62lambda1088z62zzbackend_backendz00(obj_t
		BgL_envz00_1273)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				BgL_backendz00_bglt BgL_new1048z00_1555;

				BgL_new1048z00_1555 =
					((BgL_backendz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_backendz00_bgl))));
				{	/* BackEnd/backend.scm 21 */
					long BgL_arg1090z00_1556;

					BgL_arg1090z00_1556 =
						BGL_CLASS_NUM(BGl_backendz00zzbackend_backendz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1048z00_1555), BgL_arg1090z00_1556);
				}
				return BgL_new1048z00_1555;
			}
		}

	}



/* &<@anonymous:1609> */
	obj_t BGl_z62zc3z04anonymousza31609ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1274)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1608 */
	obj_t BGl_z62lambda1608z62zzbackend_backendz00(obj_t BgL_envz00_1275,
		obj_t BgL_oz00_1276, obj_t BgL_vz00_1277)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1558;

				BgL_vz00_1558 = CBOOL(BgL_vz00_1277);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1276)))->
						BgL_stringzd2literalzd2supportz00) =
					((bool_t) BgL_vz00_1558), BUNSPEC);
			}
		}

	}



/* &lambda1607 */
	obj_t BGl_z62lambda1607z62zzbackend_backendz00(obj_t BgL_envz00_1278,
		obj_t BgL_oz00_1279)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1279)))->
					BgL_stringzd2literalzd2supportz00));
		}

	}



/* &<@anonymous:1594> */
	obj_t BGl_z62zc3z04anonymousza31594ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1280)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1593 */
	obj_t BGl_z62lambda1593z62zzbackend_backendz00(obj_t BgL_envz00_1281,
		obj_t BgL_oz00_1282, obj_t BgL_vz00_1283)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1561;

				BgL_vz00_1561 = CBOOL(BgL_vz00_1283);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1282)))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) BgL_vz00_1561), BUNSPEC);
			}
		}

	}



/* &lambda1592 */
	obj_t BGl_z62lambda1592z62zzbackend_backendz00(obj_t BgL_envz00_1284,
		obj_t BgL_oz00_1285)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1285)))->
					BgL_forcezd2registerzd2gczd2rootszd2));
		}

	}



/* &<@anonymous:1578> */
	obj_t BGl_z62zc3z04anonymousza31578ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1286)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1577 */
	obj_t BGl_z62lambda1577z62zzbackend_backendz00(obj_t BgL_envz00_1287,
		obj_t BgL_oz00_1288, obj_t BgL_vz00_1289)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1564;

				BgL_vz00_1564 = CBOOL(BgL_vz00_1289);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1288)))->
						BgL_strictzd2typezd2castz00) = ((bool_t) BgL_vz00_1564), BUNSPEC);
			}
		}

	}



/* &lambda1576 */
	obj_t BGl_z62lambda1576z62zzbackend_backendz00(obj_t BgL_envz00_1290,
		obj_t BgL_oz00_1291)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1291)))->
					BgL_strictzd2typezd2castz00));
		}

	}



/* &<@anonymous:1564> */
	obj_t BGl_z62zc3z04anonymousza31564ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1292)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1563 */
	obj_t BGl_z62lambda1563z62zzbackend_backendz00(obj_t BgL_envz00_1293,
		obj_t BgL_oz00_1294, obj_t BgL_vz00_1295)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1567;

				BgL_vz00_1567 = CBOOL(BgL_vz00_1295);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1294)))->
						BgL_typedzd2funcallzd2) = ((bool_t) BgL_vz00_1567), BUNSPEC);
			}
		}

	}



/* &lambda1562 */
	obj_t BGl_z62lambda1562z62zzbackend_backendz00(obj_t BgL_envz00_1296,
		obj_t BgL_oz00_1297)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1297)))->BgL_typedzd2funcallzd2));
		}

	}



/* &<@anonymous:1549> */
	obj_t BGl_z62zc3z04anonymousza31549ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1298)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1548 */
	obj_t BGl_z62lambda1548z62zzbackend_backendz00(obj_t BgL_envz00_1299,
		obj_t BgL_oz00_1300, obj_t BgL_vz00_1301)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1570;

				BgL_vz00_1570 = CBOOL(BgL_vz00_1301);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1300)))->BgL_typezd2checkzd2) =
					((bool_t) BgL_vz00_1570), BUNSPEC);
			}
		}

	}



/* &lambda1547 */
	obj_t BGl_z62lambda1547z62zzbackend_backendz00(obj_t BgL_envz00_1302,
		obj_t BgL_oz00_1303)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1303)))->BgL_typezd2checkzd2));
		}

	}



/* &<@anonymous:1517> */
	obj_t BGl_z62zc3z04anonymousza31517ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1304)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1516 */
	obj_t BGl_z62lambda1516z62zzbackend_backendz00(obj_t BgL_envz00_1305,
		obj_t BgL_oz00_1306, obj_t BgL_vz00_1307)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1573;

				BgL_vz00_1573 = CBOOL(BgL_vz00_1307);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1306)))->BgL_boundzd2checkzd2) =
					((bool_t) BgL_vz00_1573), BUNSPEC);
			}
		}

	}



/* &lambda1515 */
	obj_t BGl_z62lambda1515z62zzbackend_backendz00(obj_t BgL_envz00_1308,
		obj_t BgL_oz00_1309)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1309)))->BgL_boundzd2checkzd2));
		}

	}



/* &<@anonymous:1492> */
	obj_t BGl_z62zc3z04anonymousza31492ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1310)
	{
		{	/* BackEnd/backend.scm 21 */
			return BNIL;
		}

	}



/* &lambda1491 */
	obj_t BGl_z62lambda1491z62zzbackend_backendz00(obj_t BgL_envz00_1311,
		obj_t BgL_oz00_1312, obj_t BgL_vz00_1313)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1312)))->BgL_pregistersz00) =
				((obj_t) ((obj_t) BgL_vz00_1313)), BUNSPEC);
		}

	}



/* &lambda1490 */
	obj_t BGl_z62lambda1490z62zzbackend_backendz00(obj_t BgL_envz00_1314,
		obj_t BgL_oz00_1315)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1315)))->BgL_pregistersz00);
		}

	}



/* &<@anonymous:1456> */
	obj_t BGl_z62zc3z04anonymousza31456ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1316)
	{
		{	/* BackEnd/backend.scm 21 */
			return BNIL;
		}

	}



/* &lambda1455 */
	obj_t BGl_z62lambda1455z62zzbackend_backendz00(obj_t BgL_envz00_1317,
		obj_t BgL_oz00_1318, obj_t BgL_vz00_1319)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1318)))->BgL_registersz00) =
				((obj_t) ((obj_t) BgL_vz00_1319)), BUNSPEC);
		}

	}



/* &lambda1454 */
	obj_t BGl_z62lambda1454z62zzbackend_backendz00(obj_t BgL_envz00_1320,
		obj_t BgL_oz00_1321)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1321)))->BgL_registersz00);
		}

	}



/* &<@anonymous:1425> */
	obj_t BGl_z62zc3z04anonymousza31425ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1322)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1424 */
	obj_t BGl_z62lambda1424z62zzbackend_backendz00(obj_t BgL_envz00_1323,
		obj_t BgL_oz00_1324, obj_t BgL_vz00_1325)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1582;

				BgL_vz00_1582 = CBOOL(BgL_vz00_1325);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1324)))->
						BgL_requirezd2tailczd2) = ((bool_t) BgL_vz00_1582), BUNSPEC);
			}
		}

	}



/* &lambda1423 */
	obj_t BGl_z62lambda1423z62zzbackend_backendz00(obj_t BgL_envz00_1326,
		obj_t BgL_oz00_1327)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1327)))->BgL_requirezd2tailczd2));
		}

	}



/* &<@anonymous:1381> */
	obj_t BGl_z62zc3z04anonymousza31381ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1328)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1380 */
	obj_t BGl_z62lambda1380z62zzbackend_backendz00(obj_t BgL_envz00_1329,
		obj_t BgL_oz00_1330, obj_t BgL_vz00_1331)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1585;

				BgL_vz00_1585 = CBOOL(BgL_vz00_1331);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1330)))->
						BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) BgL_vz00_1585), BUNSPEC);
			}
		}

	}



/* &lambda1379 */
	obj_t BGl_z62lambda1379z62zzbackend_backendz00(obj_t BgL_envz00_1332,
		obj_t BgL_oz00_1333)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1333)))->
					BgL_tvectorzd2descrzd2supportz00));
		}

	}



/* &<@anonymous:1373> */
	obj_t BGl_z62zc3z04anonymousza31373ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1334)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1372 */
	obj_t BGl_z62lambda1372z62zzbackend_backendz00(obj_t BgL_envz00_1335,
		obj_t BgL_oz00_1336, obj_t BgL_vz00_1337)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1588;

				BgL_vz00_1588 = CBOOL(BgL_vz00_1337);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1336)))->
						BgL_pragmazd2supportzd2) = ((bool_t) BgL_vz00_1588), BUNSPEC);
			}
		}

	}



/* &lambda1371 */
	obj_t BGl_z62lambda1371z62zzbackend_backendz00(obj_t BgL_envz00_1338,
		obj_t BgL_oz00_1339)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1339)))->
					BgL_pragmazd2supportzd2));
		}

	}



/* &<@anonymous:1352> */
	obj_t BGl_z62zc3z04anonymousza31352ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1340)
	{
		{	/* BackEnd/backend.scm 21 */
			return CNST_TABLE_REF(40);
		}

	}



/* &lambda1351 */
	obj_t BGl_z62lambda1351z62zzbackend_backendz00(obj_t BgL_envz00_1341,
		obj_t BgL_oz00_1342, obj_t BgL_vz00_1343)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1342)))->BgL_debugzd2supportzd2) =
				((obj_t) ((obj_t) BgL_vz00_1343)), BUNSPEC);
		}

	}



/* &lambda1350 */
	obj_t BGl_z62lambda1350z62zzbackend_backendz00(obj_t BgL_envz00_1344,
		obj_t BgL_oz00_1345)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1345)))->BgL_debugzd2supportzd2);
		}

	}



/* &<@anonymous:1342> */
	obj_t BGl_z62zc3z04anonymousza31342ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1346)
	{
		{	/* BackEnd/backend.scm 21 */
			return CNST_TABLE_REF(41);
		}

	}



/* &lambda1341 */
	obj_t BGl_z62lambda1341z62zzbackend_backendz00(obj_t BgL_envz00_1347,
		obj_t BgL_oz00_1348, obj_t BgL_vz00_1349)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1348)))->
					BgL_foreignzd2clausezd2supportz00) =
				((obj_t) ((obj_t) BgL_vz00_1349)), BUNSPEC);
		}

	}



/* &lambda1340 */
	obj_t BGl_z62lambda1340z62zzbackend_backendz00(obj_t BgL_envz00_1350,
		obj_t BgL_oz00_1351)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1351)))->
				BgL_foreignzd2clausezd2supportz00);
		}

	}



/* &<@anonymous:1331> */
	obj_t BGl_z62zc3z04anonymousza31331ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1352)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1330 */
	obj_t BGl_z62lambda1330z62zzbackend_backendz00(obj_t BgL_envz00_1353,
		obj_t BgL_oz00_1354, obj_t BgL_vz00_1355)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1597;

				BgL_vz00_1597 = CBOOL(BgL_vz00_1355);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1354)))->
						BgL_tracezd2supportzd2) = ((bool_t) BgL_vz00_1597), BUNSPEC);
			}
		}

	}



/* &lambda1329 */
	obj_t BGl_z62lambda1329z62zzbackend_backendz00(obj_t BgL_envz00_1356,
		obj_t BgL_oz00_1357)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1357)))->BgL_tracezd2supportzd2));
		}

	}



/* &<@anonymous:1324> */
	obj_t BGl_z62zc3z04anonymousza31324ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1358)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1323 */
	obj_t BGl_z62lambda1323z62zzbackend_backendz00(obj_t BgL_envz00_1359,
		obj_t BgL_oz00_1360, obj_t BgL_vz00_1361)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1600;

				BgL_vz00_1600 = CBOOL(BgL_vz00_1361);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1360)))->BgL_typedzd2eqzd2) =
					((bool_t) BgL_vz00_1600), BUNSPEC);
			}
		}

	}



/* &lambda1322 */
	obj_t BGl_z62lambda1322z62zzbackend_backendz00(obj_t BgL_envz00_1362,
		obj_t BgL_oz00_1363)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1363)))->BgL_typedzd2eqzd2));
		}

	}



/* &<@anonymous:1317> */
	obj_t BGl_z62zc3z04anonymousza31317ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1364)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1316 */
	obj_t BGl_z62lambda1316z62zzbackend_backendz00(obj_t BgL_envz00_1365,
		obj_t BgL_oz00_1366, obj_t BgL_vz00_1367)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1603;

				BgL_vz00_1603 = CBOOL(BgL_vz00_1367);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1366)))->
						BgL_foreignzd2closurezd2) = ((bool_t) BgL_vz00_1603), BUNSPEC);
			}
		}

	}



/* &lambda1315 */
	obj_t BGl_z62lambda1315z62zzbackend_backendz00(obj_t BgL_envz00_1368,
		obj_t BgL_oz00_1369)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1369)))->
					BgL_foreignzd2closurezd2));
		}

	}



/* &<@anonymous:1308> */
	obj_t BGl_z62zc3z04anonymousza31308ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1370)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1307 */
	obj_t BGl_z62lambda1307z62zzbackend_backendz00(obj_t BgL_envz00_1371,
		obj_t BgL_oz00_1372, obj_t BgL_vz00_1373)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1606;

				BgL_vz00_1606 = CBOOL(BgL_vz00_1373);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1372)))->
						BgL_removezd2emptyzd2letz00) = ((bool_t) BgL_vz00_1606), BUNSPEC);
			}
		}

	}



/* &lambda1306 */
	obj_t BGl_z62lambda1306z62zzbackend_backendz00(obj_t BgL_envz00_1374,
		obj_t BgL_oz00_1375)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1375)))->
					BgL_removezd2emptyzd2letz00));
		}

	}



/* &<@anonymous:1271> */
	obj_t BGl_z62zc3z04anonymousza31271ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1376)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1270 */
	obj_t BGl_z62lambda1270z62zzbackend_backendz00(obj_t BgL_envz00_1377,
		obj_t BgL_oz00_1378, obj_t BgL_vz00_1379)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1609;

				BgL_vz00_1609 = CBOOL(BgL_vz00_1379);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1378)))->BgL_effectzb2zb2) =
					((bool_t) BgL_vz00_1609), BUNSPEC);
			}
		}

	}



/* &lambda1269 */
	obj_t BGl_z62lambda1269z62zzbackend_backendz00(obj_t BgL_envz00_1380,
		obj_t BgL_oz00_1381)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1381)))->BgL_effectzb2zb2));
		}

	}



/* &<@anonymous:1247> */
	obj_t BGl_z62zc3z04anonymousza31247ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1382)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1246 */
	obj_t BGl_z62lambda1246z62zzbackend_backendz00(obj_t BgL_envz00_1383,
		obj_t BgL_oz00_1384, obj_t BgL_vz00_1385)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1612;

				BgL_vz00_1612 = CBOOL(BgL_vz00_1385);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1384)))->
						BgL_qualifiedzd2typeszd2) = ((bool_t) BgL_vz00_1612), BUNSPEC);
			}
		}

	}



/* &lambda1245 */
	obj_t BGl_z62lambda1245z62zzbackend_backendz00(obj_t BgL_envz00_1386,
		obj_t BgL_oz00_1387)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1387)))->
					BgL_qualifiedzd2typeszd2));
		}

	}



/* &<@anonymous:1237> */
	obj_t BGl_z62zc3z04anonymousza31237ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1388)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1236 */
	obj_t BGl_z62lambda1236z62zzbackend_backendz00(obj_t BgL_envz00_1389,
		obj_t BgL_oz00_1390, obj_t BgL_vz00_1391)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1615;

				BgL_vz00_1615 = CBOOL(BgL_vz00_1391);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1390)))->BgL_callccz00) =
					((bool_t) BgL_vz00_1615), BUNSPEC);
			}
		}

	}



/* &lambda1235 */
	obj_t BGl_z62lambda1235z62zzbackend_backendz00(obj_t BgL_envz00_1392,
		obj_t BgL_oz00_1393)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1393)))->BgL_callccz00));
		}

	}



/* &<@anonymous:1230> */
	obj_t BGl_z62zc3z04anonymousza31230ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1394)
	{
		{	/* BackEnd/backend.scm 21 */
			return CNST_TABLE_REF(42);
		}

	}



/* &lambda1229 */
	obj_t BGl_z62lambda1229z62zzbackend_backendz00(obj_t BgL_envz00_1395,
		obj_t BgL_oz00_1396, obj_t BgL_vz00_1397)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1396)))->
					BgL_heapzd2compatiblezd2) =
				((obj_t) ((obj_t) BgL_vz00_1397)), BUNSPEC);
		}

	}



/* &lambda1228 */
	obj_t BGl_z62lambda1228z62zzbackend_backendz00(obj_t BgL_envz00_1398,
		obj_t BgL_oz00_1399)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1399)))->BgL_heapzd2compatiblezd2);
		}

	}



/* &<@anonymous:1221> */
	obj_t BGl_z62zc3z04anonymousza31221ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1400)
	{
		{	/* BackEnd/backend.scm 21 */
			return BGl_string1795z00zzbackend_backendz00;
		}

	}



/* &lambda1220 */
	obj_t BGl_z62lambda1220z62zzbackend_backendz00(obj_t BgL_envz00_1401,
		obj_t BgL_oz00_1402, obj_t BgL_vz00_1403)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1402)))->BgL_heapzd2suffixzd2) =
				((obj_t) ((obj_t) BgL_vz00_1403)), BUNSPEC);
		}

	}



/* &lambda1219 */
	obj_t BGl_z62lambda1219z62zzbackend_backendz00(obj_t BgL_envz00_1404,
		obj_t BgL_oz00_1405)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1405)))->BgL_heapzd2suffixzd2);
		}

	}



/* &<@anonymous:1211> */
	obj_t BGl_z62zc3z04anonymousza31211ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1406)
	{
		{	/* BackEnd/backend.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1210 */
	obj_t BGl_z62lambda1210z62zzbackend_backendz00(obj_t BgL_envz00_1407,
		obj_t BgL_oz00_1408, obj_t BgL_vz00_1409)
	{
		{	/* BackEnd/backend.scm 21 */
			{	/* BackEnd/backend.scm 21 */
				bool_t BgL_vz00_1624;

				BgL_vz00_1624 = CBOOL(BgL_vz00_1409);
				return
					((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_oz00_1408)))->BgL_typedz00) =
					((bool_t) BgL_vz00_1624), BUNSPEC);
			}
		}

	}



/* &lambda1209 */
	obj_t BGl_z62lambda1209z62zzbackend_backendz00(obj_t BgL_envz00_1410,
		obj_t BgL_oz00_1411)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				BBOOL(
				(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1411)))->BgL_typedz00));
		}

	}



/* &<@anonymous:1201> */
	obj_t BGl_z62zc3z04anonymousza31201ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1412)
	{
		{	/* BackEnd/backend.scm 21 */
			return BNIL;
		}

	}



/* &lambda1200 */
	obj_t BGl_z62lambda1200z62zzbackend_backendz00(obj_t BgL_envz00_1413,
		obj_t BgL_oz00_1414, obj_t BgL_vz00_1415)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1414)))->BgL_typesz00) =
				((obj_t) BgL_vz00_1415), BUNSPEC);
		}

	}



/* &lambda1199 */
	obj_t BGl_z62lambda1199z62zzbackend_backendz00(obj_t BgL_envz00_1416,
		obj_t BgL_oz00_1417)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1417)))->BgL_typesz00);
		}

	}



/* &<@anonymous:1193> */
	obj_t BGl_z62zc3z04anonymousza31193ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1418)
	{
		{	/* BackEnd/backend.scm 21 */
			return BNIL;
		}

	}



/* &lambda1192 */
	obj_t BGl_z62lambda1192z62zzbackend_backendz00(obj_t BgL_envz00_1419,
		obj_t BgL_oz00_1420, obj_t BgL_vz00_1421)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1420)))->BgL_functionsz00) =
				((obj_t) BgL_vz00_1421), BUNSPEC);
		}

	}



/* &lambda1191 */
	obj_t BGl_z62lambda1191z62zzbackend_backendz00(obj_t BgL_envz00_1422,
		obj_t BgL_oz00_1423)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1423)))->BgL_functionsz00);
		}

	}



/* &<@anonymous:1186> */
	obj_t BGl_z62zc3z04anonymousza31186ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1424)
	{
		{	/* BackEnd/backend.scm 21 */
			return BNIL;
		}

	}



/* &lambda1185 */
	obj_t BGl_z62lambda1185z62zzbackend_backendz00(obj_t BgL_envz00_1425,
		obj_t BgL_oz00_1426, obj_t BgL_vz00_1427)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1426)))->BgL_variablesz00) =
				((obj_t) BgL_vz00_1427), BUNSPEC);
		}

	}



/* &lambda1184 */
	obj_t BGl_z62lambda1184z62zzbackend_backendz00(obj_t BgL_envz00_1428,
		obj_t BgL_oz00_1429)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1429)))->BgL_variablesz00);
		}

	}



/* &<@anonymous:1167> */
	obj_t BGl_z62zc3z04anonymousza31167ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1430)
	{
		{	/* BackEnd/backend.scm 21 */
			return BNIL;
		}

	}



/* &lambda1166 */
	obj_t BGl_z62lambda1166z62zzbackend_backendz00(obj_t BgL_envz00_1431,
		obj_t BgL_oz00_1432, obj_t BgL_vz00_1433)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1432)))->BgL_externzd2typeszd2) =
				((obj_t) BgL_vz00_1433), BUNSPEC);
		}

	}



/* &lambda1165 */
	obj_t BGl_z62lambda1165z62zzbackend_backendz00(obj_t BgL_envz00_1434,
		obj_t BgL_oz00_1435)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1435)))->BgL_externzd2typeszd2);
		}

	}



/* &<@anonymous:1155> */
	obj_t BGl_z62zc3z04anonymousza31155ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1436)
	{
		{	/* BackEnd/backend.scm 21 */
			return BNIL;
		}

	}



/* &lambda1154 */
	obj_t BGl_z62lambda1154z62zzbackend_backendz00(obj_t BgL_envz00_1437,
		obj_t BgL_oz00_1438, obj_t BgL_vz00_1439)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1438)))->
					BgL_externzd2functionszd2) = ((obj_t) BgL_vz00_1439), BUNSPEC);
		}

	}



/* &lambda1153 */
	obj_t BGl_z62lambda1153z62zzbackend_backendz00(obj_t BgL_envz00_1440,
		obj_t BgL_oz00_1441)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1441)))->BgL_externzd2functionszd2);
		}

	}



/* &<@anonymous:1144> */
	obj_t BGl_z62zc3z04anonymousza31144ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1442)
	{
		{	/* BackEnd/backend.scm 21 */
			return BNIL;
		}

	}



/* &lambda1143 */
	obj_t BGl_z62lambda1143z62zzbackend_backendz00(obj_t BgL_envz00_1443,
		obj_t BgL_oz00_1444, obj_t BgL_vz00_1445)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1444)))->
					BgL_externzd2variableszd2) = ((obj_t) BgL_vz00_1445), BUNSPEC);
		}

	}



/* &lambda1142 */
	obj_t BGl_z62lambda1142z62zzbackend_backendz00(obj_t BgL_envz00_1446,
		obj_t BgL_oz00_1447)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1447)))->BgL_externzd2variableszd2);
		}

	}



/* &<@anonymous:1135> */
	obj_t BGl_z62zc3z04anonymousza31135ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1448)
	{
		{	/* BackEnd/backend.scm 21 */
			return BGl_string1796z00zzbackend_backendz00;
		}

	}



/* &lambda1134 */
	obj_t BGl_z62lambda1134z62zzbackend_backendz00(obj_t BgL_envz00_1449,
		obj_t BgL_oz00_1450, obj_t BgL_vz00_1451)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1450)))->BgL_namez00) = ((obj_t)
					((obj_t) BgL_vz00_1451)), BUNSPEC);
		}

	}



/* &lambda1133 */
	obj_t BGl_z62lambda1133z62zzbackend_backendz00(obj_t BgL_envz00_1452,
		obj_t BgL_oz00_1453)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1453)))->BgL_namez00);
		}

	}



/* &<@anonymous:1126> */
	obj_t BGl_z62zc3z04anonymousza31126ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1454)
	{
		{	/* BackEnd/backend.scm 21 */
			return CNST_TABLE_REF(43);
		}

	}



/* &lambda1125 */
	obj_t BGl_z62lambda1125z62zzbackend_backendz00(obj_t BgL_envz00_1455,
		obj_t BgL_oz00_1456, obj_t BgL_vz00_1457)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1456)))->BgL_srfi0z00) = ((obj_t)
					((obj_t) BgL_vz00_1457)), BUNSPEC);
		}

	}



/* &lambda1124 */
	obj_t BGl_z62lambda1124z62zzbackend_backendz00(obj_t BgL_envz00_1458,
		obj_t BgL_oz00_1459)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1459)))->BgL_srfi0z00);
		}

	}



/* &<@anonymous:1106> */
	obj_t BGl_z62zc3z04anonymousza31106ze3ze5zzbackend_backendz00(obj_t
		BgL_envz00_1460)
	{
		{	/* BackEnd/backend.scm 21 */
			return CNST_TABLE_REF(43);
		}

	}



/* &lambda1105 */
	obj_t BGl_z62lambda1105z62zzbackend_backendz00(obj_t BgL_envz00_1461,
		obj_t BgL_oz00_1462, obj_t BgL_vz00_1463)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_1462)))->BgL_languagez00) =
				((obj_t) ((obj_t) BgL_vz00_1463)), BUNSPEC);
		}

	}



/* &lambda1104 */
	obj_t BGl_z62lambda1104z62zzbackend_backendz00(obj_t BgL_envz00_1464,
		obj_t BgL_oz00_1465)
	{
		{	/* BackEnd/backend.scm 21 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_1465)))->BgL_languagez00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00,
				BGl_proc1797z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1798z00zzbackend_backendz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2compilezd2envz00zzbackend_backendz00,
				BGl_proc1799z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1800z00zzbackend_backendz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2compilezd2functionszd2envzd2zzbackend_backendz00,
				BGl_proc1801z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1802z00zzbackend_backendz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2linkzd2envz00zzbackend_backendz00,
				BGl_proc1803z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1804z00zzbackend_backendz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2subtypezf3zd2envzf3zzbackend_backendz00,
				BGl_proc1805z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1806z00zzbackend_backendz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2cnstzd2tablezd2namezd2envz00zzbackend_backendz00,
				BGl_proc1807z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1808z00zzbackend_backendz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2linkzd2objectszd2envzd2zzbackend_backendz00,
				BGl_proc1809z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1810z00zzbackend_backendz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2instrzd2resetzd2registerszd2envz00zzbackend_backendz00,
				BGl_proc1811z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1812z00zzbackend_backendz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2checkzd2inlineszd2envzd2zzbackend_backendz00,
				BGl_proc1813z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1814z00zzbackend_backendz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_backendzd2gczd2initzd2envzd2zzbackend_backendz00,
				BGl_proc1815z00zzbackend_backendz00, BGl_backendz00zzbackend_backendz00,
				BGl_string1816z00zzbackend_backendz00);
		}

	}



/* &backend-gc-init1075 */
	obj_t BGl_z62backendzd2gczd2init1075z62zzbackend_backendz00(obj_t
		BgL_envz00_1478, obj_t BgL_bz00_1479)
	{
		{	/* BackEnd/backend.scm 154 */
			return BUNSPEC;
		}

	}



/* &backend-check-inline1073 */
	obj_t BGl_z62backendzd2checkzd2inline1073z62zzbackend_backendz00(obj_t
		BgL_envz00_1480, obj_t BgL_bz00_1481)
	{
		{	/* BackEnd/backend.scm 148 */
			return BUNSPEC;
		}

	}



/* &backend-instr-reset-1071 */
	obj_t BGl_z62backendzd2instrzd2resetzd21071zb0zzbackend_backendz00(obj_t
		BgL_envz00_1482, obj_t BgL_bz00_1483, obj_t BgL_iz00_1484)
	{
		{	/* BackEnd/backend.scm 141 */
			{	/* BackEnd/backend.scm 143 */
				obj_t BgL_val1_1052z00_1650;
				obj_t BgL_val2_1053z00_1651;
				obj_t BgL_val3_1054z00_1652;

				BgL_val1_1052z00_1650 =
					(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_bz00_1483)))->BgL_registersz00);
				BgL_val2_1053z00_1651 =
					(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_bz00_1483)))->BgL_registersz00);
				BgL_val3_1054z00_1652 =
					(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_bz00_1483)))->BgL_registersz00);
				{	/* BackEnd/backend.scm 143 */
					int BgL_tmpz00_2388;

					BgL_tmpz00_2388 = (int) (4L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2388);
				}
				{	/* BackEnd/backend.scm 143 */
					int BgL_tmpz00_2391;

					BgL_tmpz00_2391 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_2391, BgL_val1_1052z00_1650);
				}
				{	/* BackEnd/backend.scm 143 */
					int BgL_tmpz00_2394;

					BgL_tmpz00_2394 = (int) (2L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_2394, BgL_val2_1053z00_1651);
				}
				{	/* BackEnd/backend.scm 143 */
					int BgL_tmpz00_2397;

					BgL_tmpz00_2397 = (int) (3L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_2397, BgL_val3_1054z00_1652);
				}
				return BNIL;
			}
		}

	}



/* &backend-link-objects1069 */
	obj_t BGl_z62backendzd2linkzd2objects1069z62zzbackend_backendz00(obj_t
		BgL_envz00_1485, obj_t BgL_bz00_1486, obj_t BgL_lz00_1487)
	{
		{	/* BackEnd/backend.scm 136 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(44),
				BGl_string1817z00zzbackend_backendz00,
				((obj_t) ((BgL_backendz00_bglt) BgL_bz00_1486)));
		}

	}



/* &backend-cnst-table-n1067 */
	obj_t BGl_z62backendzd2cnstzd2tablezd2n1067zb0zzbackend_backendz00(obj_t
		BgL_envz00_1488, obj_t BgL_bz00_1489, obj_t BgL_oz00_1490)
	{
		{	/* BackEnd/backend.scm 131 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(45),
				BGl_string1817z00zzbackend_backendz00,
				((obj_t) ((BgL_backendz00_bglt) BgL_bz00_1489)));
		}

	}



/* &backend-subtype?1065 */
	obj_t BGl_z62backendzd2subtypezf31065z43zzbackend_backendz00(obj_t
		BgL_envz00_1491, obj_t BgL_bz00_1492, obj_t BgL_t1z00_1493,
		obj_t BgL_t2z00_1494)
	{
		{	/* BackEnd/backend.scm 126 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(46),
				BGl_string1817z00zzbackend_backendz00,
				((obj_t) ((BgL_backendz00_bglt) BgL_bz00_1492)));
		}

	}



/* &backend-link1063 */
	obj_t BGl_z62backendzd2link1063zb0zzbackend_backendz00(obj_t BgL_envz00_1495,
		obj_t BgL_bz00_1496, obj_t BgL_resultz00_1497)
	{
		{	/* BackEnd/backend.scm 121 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(47),
				BGl_string1817z00zzbackend_backendz00,
				((obj_t) ((BgL_backendz00_bglt) BgL_bz00_1496)));
		}

	}



/* &backend-compile-func1061 */
	obj_t BGl_z62backendzd2compilezd2func1061z62zzbackend_backendz00(obj_t
		BgL_envz00_1498, obj_t BgL_bz00_1499)
	{
		{	/* BackEnd/backend.scm 116 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(48),
				BGl_string1817z00zzbackend_backendz00,
				((obj_t) ((BgL_backendz00_bglt) BgL_bz00_1499)));
		}

	}



/* &backend-compile1059 */
	obj_t BGl_z62backendzd2compile1059zb0zzbackend_backendz00(obj_t
		BgL_envz00_1500, obj_t BgL_bz00_1501)
	{
		{	/* BackEnd/backend.scm 111 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(49),
				BGl_string1817z00zzbackend_backendz00,
				((obj_t) ((BgL_backendz00_bglt) BgL_bz00_1501)));
		}

	}



/* &backend-initialize!1057 */
	obj_t BGl_z62backendzd2initializa7ez121057z05zzbackend_backendz00(obj_t
		BgL_envz00_1502, obj_t BgL_bz00_1503)
	{
		{	/* BackEnd/backend.scm 105 */
			return BUNSPEC;
		}

	}



/* backend-initialize! */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2initializa7ez12z67zzbackend_backendz00(BgL_backendz00_bglt
		BgL_bz00_103)
	{
		{	/* BackEnd/backend.scm 105 */
			{	/* BackEnd/backend.scm 105 */
				obj_t BgL_method1058z00_611;

				{	/* BackEnd/backend.scm 105 */
					obj_t BgL_res1648z00_724;

					{	/* BackEnd/backend.scm 105 */
						long BgL_objzd2classzd2numz00_695;

						BgL_objzd2classzd2numz00_695 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_103));
						{	/* BackEnd/backend.scm 105 */
							obj_t BgL_arg1811z00_696;

							BgL_arg1811z00_696 =
								PROCEDURE_REF
								(BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 105 */
								int BgL_offsetz00_699;

								BgL_offsetz00_699 = (int) (BgL_objzd2classzd2numz00_695);
								{	/* BackEnd/backend.scm 105 */
									long BgL_offsetz00_700;

									BgL_offsetz00_700 =
										((long) (BgL_offsetz00_699) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 105 */
										long BgL_modz00_701;

										BgL_modz00_701 =
											(BgL_offsetz00_700 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 105 */
											long BgL_restz00_703;

											BgL_restz00_703 =
												(BgL_offsetz00_700 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 105 */

												{	/* BackEnd/backend.scm 105 */
													obj_t BgL_bucketz00_705;

													BgL_bucketz00_705 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_696), BgL_modz00_701);
													BgL_res1648z00_724 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_705), BgL_restz00_703);
					}}}}}}}}
					BgL_method1058z00_611 = BgL_res1648z00_724;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1058z00_611, ((obj_t) BgL_bz00_103));
			}
		}

	}



/* &backend-initialize! */
	obj_t BGl_z62backendzd2initializa7ez12z05zzbackend_backendz00(obj_t
		BgL_envz00_1466, obj_t BgL_bz00_1467)
	{
		{	/* BackEnd/backend.scm 105 */
			return
				BGl_backendzd2initializa7ez12z67zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1467));
		}

	}



/* backend-compile */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2compilezd2zzbackend_backendz00(BgL_backendz00_bglt
		BgL_bz00_104)
	{
		{	/* BackEnd/backend.scm 111 */
			{	/* BackEnd/backend.scm 111 */
				obj_t BgL_method1060z00_612;

				{	/* BackEnd/backend.scm 111 */
					obj_t BgL_res1653z00_755;

					{	/* BackEnd/backend.scm 111 */
						long BgL_objzd2classzd2numz00_726;

						BgL_objzd2classzd2numz00_726 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_104));
						{	/* BackEnd/backend.scm 111 */
							obj_t BgL_arg1811z00_727;

							BgL_arg1811z00_727 =
								PROCEDURE_REF
								(BGl_backendzd2compilezd2envz00zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 111 */
								int BgL_offsetz00_730;

								BgL_offsetz00_730 = (int) (BgL_objzd2classzd2numz00_726);
								{	/* BackEnd/backend.scm 111 */
									long BgL_offsetz00_731;

									BgL_offsetz00_731 =
										((long) (BgL_offsetz00_730) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 111 */
										long BgL_modz00_732;

										BgL_modz00_732 =
											(BgL_offsetz00_731 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 111 */
											long BgL_restz00_734;

											BgL_restz00_734 =
												(BgL_offsetz00_731 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 111 */

												{	/* BackEnd/backend.scm 111 */
													obj_t BgL_bucketz00_736;

													BgL_bucketz00_736 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_727), BgL_modz00_732);
													BgL_res1653z00_755 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_736), BgL_restz00_734);
					}}}}}}}}
					BgL_method1060z00_612 = BgL_res1653z00_755;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1060z00_612, ((obj_t) BgL_bz00_104));
			}
		}

	}



/* &backend-compile */
	obj_t BGl_z62backendzd2compilezb0zzbackend_backendz00(obj_t BgL_envz00_1504,
		obj_t BgL_bz00_1505)
	{
		{	/* BackEnd/backend.scm 111 */
			return
				BGl_backendzd2compilezd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1505));
		}

	}



/* backend-compile-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2compilezd2functionsz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_bz00_105)
	{
		{	/* BackEnd/backend.scm 116 */
			{	/* BackEnd/backend.scm 116 */
				obj_t BgL_method1062z00_613;

				{	/* BackEnd/backend.scm 116 */
					obj_t BgL_res1658z00_786;

					{	/* BackEnd/backend.scm 116 */
						long BgL_objzd2classzd2numz00_757;

						BgL_objzd2classzd2numz00_757 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_105));
						{	/* BackEnd/backend.scm 116 */
							obj_t BgL_arg1811z00_758;

							BgL_arg1811z00_758 =
								PROCEDURE_REF
								(BGl_backendzd2compilezd2functionszd2envzd2zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 116 */
								int BgL_offsetz00_761;

								BgL_offsetz00_761 = (int) (BgL_objzd2classzd2numz00_757);
								{	/* BackEnd/backend.scm 116 */
									long BgL_offsetz00_762;

									BgL_offsetz00_762 =
										((long) (BgL_offsetz00_761) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 116 */
										long BgL_modz00_763;

										BgL_modz00_763 =
											(BgL_offsetz00_762 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 116 */
											long BgL_restz00_765;

											BgL_restz00_765 =
												(BgL_offsetz00_762 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 116 */

												{	/* BackEnd/backend.scm 116 */
													obj_t BgL_bucketz00_767;

													BgL_bucketz00_767 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_758), BgL_modz00_763);
													BgL_res1658z00_786 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_767), BgL_restz00_765);
					}}}}}}}}
					BgL_method1062z00_613 = BgL_res1658z00_786;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1062z00_613, ((obj_t) BgL_bz00_105));
			}
		}

	}



/* &backend-compile-functions */
	obj_t BGl_z62backendzd2compilezd2functionsz62zzbackend_backendz00(obj_t
		BgL_envz00_1506, obj_t BgL_bz00_1507)
	{
		{	/* BackEnd/backend.scm 116 */
			return
				BGl_backendzd2compilezd2functionsz00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1507));
		}

	}



/* backend-link */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2linkzd2zzbackend_backendz00(BgL_backendz00_bglt BgL_bz00_106,
		obj_t BgL_resultz00_107)
	{
		{	/* BackEnd/backend.scm 121 */
			{	/* BackEnd/backend.scm 121 */
				obj_t BgL_method1064z00_614;

				{	/* BackEnd/backend.scm 121 */
					obj_t BgL_res1663z00_817;

					{	/* BackEnd/backend.scm 121 */
						long BgL_objzd2classzd2numz00_788;

						BgL_objzd2classzd2numz00_788 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_106));
						{	/* BackEnd/backend.scm 121 */
							obj_t BgL_arg1811z00_789;

							BgL_arg1811z00_789 =
								PROCEDURE_REF(BGl_backendzd2linkzd2envz00zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 121 */
								int BgL_offsetz00_792;

								BgL_offsetz00_792 = (int) (BgL_objzd2classzd2numz00_788);
								{	/* BackEnd/backend.scm 121 */
									long BgL_offsetz00_793;

									BgL_offsetz00_793 =
										((long) (BgL_offsetz00_792) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 121 */
										long BgL_modz00_794;

										BgL_modz00_794 =
											(BgL_offsetz00_793 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 121 */
											long BgL_restz00_796;

											BgL_restz00_796 =
												(BgL_offsetz00_793 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 121 */

												{	/* BackEnd/backend.scm 121 */
													obj_t BgL_bucketz00_798;

													BgL_bucketz00_798 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_789), BgL_modz00_794);
													BgL_res1663z00_817 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_798), BgL_restz00_796);
					}}}}}}}}
					BgL_method1064z00_614 = BgL_res1663z00_817;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1064z00_614,
					((obj_t) BgL_bz00_106), BgL_resultz00_107);
			}
		}

	}



/* &backend-link */
	obj_t BGl_z62backendzd2linkzb0zzbackend_backendz00(obj_t BgL_envz00_1508,
		obj_t BgL_bz00_1509, obj_t BgL_resultz00_1510)
	{
		{	/* BackEnd/backend.scm 121 */
			return
				BGl_backendzd2linkzd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1509), BgL_resultz00_1510);
		}

	}



/* backend-subtype? */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2subtypezf3z21zzbackend_backendz00(BgL_backendz00_bglt
		BgL_bz00_108, obj_t BgL_t1z00_109, obj_t BgL_t2z00_110)
	{
		{	/* BackEnd/backend.scm 126 */
			{	/* BackEnd/backend.scm 126 */
				obj_t BgL_method1066z00_615;

				{	/* BackEnd/backend.scm 126 */
					obj_t BgL_res1668z00_848;

					{	/* BackEnd/backend.scm 126 */
						long BgL_objzd2classzd2numz00_819;

						BgL_objzd2classzd2numz00_819 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_108));
						{	/* BackEnd/backend.scm 126 */
							obj_t BgL_arg1811z00_820;

							BgL_arg1811z00_820 =
								PROCEDURE_REF
								(BGl_backendzd2subtypezf3zd2envzf3zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 126 */
								int BgL_offsetz00_823;

								BgL_offsetz00_823 = (int) (BgL_objzd2classzd2numz00_819);
								{	/* BackEnd/backend.scm 126 */
									long BgL_offsetz00_824;

									BgL_offsetz00_824 =
										((long) (BgL_offsetz00_823) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 126 */
										long BgL_modz00_825;

										BgL_modz00_825 =
											(BgL_offsetz00_824 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 126 */
											long BgL_restz00_827;

											BgL_restz00_827 =
												(BgL_offsetz00_824 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 126 */

												{	/* BackEnd/backend.scm 126 */
													obj_t BgL_bucketz00_829;

													BgL_bucketz00_829 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_820), BgL_modz00_825);
													BgL_res1668z00_848 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_829), BgL_restz00_827);
					}}}}}}}}
					BgL_method1066z00_615 = BgL_res1668z00_848;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1066z00_615,
					((obj_t) BgL_bz00_108), BgL_t1z00_109, BgL_t2z00_110);
			}
		}

	}



/* &backend-subtype? */
	obj_t BGl_z62backendzd2subtypezf3z43zzbackend_backendz00(obj_t
		BgL_envz00_1511, obj_t BgL_bz00_1512, obj_t BgL_t1z00_1513,
		obj_t BgL_t2z00_1514)
	{
		{	/* BackEnd/backend.scm 126 */
			return
				BGl_backendzd2subtypezf3z21zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1512), BgL_t1z00_1513, BgL_t2z00_1514);
		}

	}



/* backend-cnst-table-name */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2cnstzd2tablezd2namezd2zzbackend_backendz00(BgL_backendz00_bglt
		BgL_bz00_111, int BgL_oz00_112)
	{
		{	/* BackEnd/backend.scm 131 */
			{	/* BackEnd/backend.scm 131 */
				obj_t BgL_method1068z00_616;

				{	/* BackEnd/backend.scm 131 */
					obj_t BgL_res1673z00_879;

					{	/* BackEnd/backend.scm 131 */
						long BgL_objzd2classzd2numz00_850;

						BgL_objzd2classzd2numz00_850 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_111));
						{	/* BackEnd/backend.scm 131 */
							obj_t BgL_arg1811z00_851;

							BgL_arg1811z00_851 =
								PROCEDURE_REF
								(BGl_backendzd2cnstzd2tablezd2namezd2envz00zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 131 */
								int BgL_offsetz00_854;

								BgL_offsetz00_854 = (int) (BgL_objzd2classzd2numz00_850);
								{	/* BackEnd/backend.scm 131 */
									long BgL_offsetz00_855;

									BgL_offsetz00_855 =
										((long) (BgL_offsetz00_854) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 131 */
										long BgL_modz00_856;

										BgL_modz00_856 =
											(BgL_offsetz00_855 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 131 */
											long BgL_restz00_858;

											BgL_restz00_858 =
												(BgL_offsetz00_855 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 131 */

												{	/* BackEnd/backend.scm 131 */
													obj_t BgL_bucketz00_860;

													BgL_bucketz00_860 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_851), BgL_modz00_856);
													BgL_res1673z00_879 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_860), BgL_restz00_858);
					}}}}}}}}
					BgL_method1068z00_616 = BgL_res1673z00_879;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1068z00_616,
					((obj_t) BgL_bz00_111), BINT(BgL_oz00_112));
			}
		}

	}



/* &backend-cnst-table-name */
	obj_t BGl_z62backendzd2cnstzd2tablezd2namezb0zzbackend_backendz00(obj_t
		BgL_envz00_1515, obj_t BgL_bz00_1516, obj_t BgL_oz00_1517)
	{
		{	/* BackEnd/backend.scm 131 */
			return
				BGl_backendzd2cnstzd2tablezd2namezd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1516), CINT(BgL_oz00_1517));
		}

	}



/* backend-link-objects */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2linkzd2objectsz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_bz00_113, obj_t BgL_lz00_114)
	{
		{	/* BackEnd/backend.scm 136 */
			{	/* BackEnd/backend.scm 136 */
				obj_t BgL_method1070z00_617;

				{	/* BackEnd/backend.scm 136 */
					obj_t BgL_res1678z00_910;

					{	/* BackEnd/backend.scm 136 */
						long BgL_objzd2classzd2numz00_881;

						BgL_objzd2classzd2numz00_881 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_113));
						{	/* BackEnd/backend.scm 136 */
							obj_t BgL_arg1811z00_882;

							BgL_arg1811z00_882 =
								PROCEDURE_REF
								(BGl_backendzd2linkzd2objectszd2envzd2zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 136 */
								int BgL_offsetz00_885;

								BgL_offsetz00_885 = (int) (BgL_objzd2classzd2numz00_881);
								{	/* BackEnd/backend.scm 136 */
									long BgL_offsetz00_886;

									BgL_offsetz00_886 =
										((long) (BgL_offsetz00_885) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 136 */
										long BgL_modz00_887;

										BgL_modz00_887 =
											(BgL_offsetz00_886 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 136 */
											long BgL_restz00_889;

											BgL_restz00_889 =
												(BgL_offsetz00_886 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 136 */

												{	/* BackEnd/backend.scm 136 */
													obj_t BgL_bucketz00_891;

													BgL_bucketz00_891 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_882), BgL_modz00_887);
													BgL_res1678z00_910 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_891), BgL_restz00_889);
					}}}}}}}}
					BgL_method1070z00_617 = BgL_res1678z00_910;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1070z00_617,
					((obj_t) BgL_bz00_113), BgL_lz00_114);
			}
		}

	}



/* &backend-link-objects */
	obj_t BGl_z62backendzd2linkzd2objectsz62zzbackend_backendz00(obj_t
		BgL_envz00_1518, obj_t BgL_bz00_1519, obj_t BgL_lz00_1520)
	{
		{	/* BackEnd/backend.scm 136 */
			return
				BGl_backendzd2linkzd2objectsz00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1519), BgL_lz00_1520);
		}

	}



/* backend-instr-reset-registers */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2instrzd2resetzd2registerszd2zzbackend_backendz00
		(BgL_backendz00_bglt BgL_bz00_115, obj_t BgL_iz00_116)
	{
		{	/* BackEnd/backend.scm 141 */
			{	/* BackEnd/backend.scm 141 */
				obj_t BgL_method1072z00_618;

				{	/* BackEnd/backend.scm 141 */
					obj_t BgL_res1683z00_941;

					{	/* BackEnd/backend.scm 141 */
						long BgL_objzd2classzd2numz00_912;

						BgL_objzd2classzd2numz00_912 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_115));
						{	/* BackEnd/backend.scm 141 */
							obj_t BgL_arg1811z00_913;

							BgL_arg1811z00_913 =
								PROCEDURE_REF
								(BGl_backendzd2instrzd2resetzd2registerszd2envz00zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 141 */
								int BgL_offsetz00_916;

								BgL_offsetz00_916 = (int) (BgL_objzd2classzd2numz00_912);
								{	/* BackEnd/backend.scm 141 */
									long BgL_offsetz00_917;

									BgL_offsetz00_917 =
										((long) (BgL_offsetz00_916) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 141 */
										long BgL_modz00_918;

										BgL_modz00_918 =
											(BgL_offsetz00_917 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 141 */
											long BgL_restz00_920;

											BgL_restz00_920 =
												(BgL_offsetz00_917 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 141 */

												{	/* BackEnd/backend.scm 141 */
													obj_t BgL_bucketz00_922;

													BgL_bucketz00_922 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_913), BgL_modz00_918);
													BgL_res1683z00_941 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_922), BgL_restz00_920);
					}}}}}}}}
					BgL_method1072z00_618 = BgL_res1683z00_941;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1072z00_618,
					((obj_t) BgL_bz00_115), BgL_iz00_116);
			}
		}

	}



/* &backend-instr-reset-registers */
	obj_t BGl_z62backendzd2instrzd2resetzd2registerszb0zzbackend_backendz00(obj_t
		BgL_envz00_1521, obj_t BgL_bz00_1522, obj_t BgL_iz00_1523)
	{
		{	/* BackEnd/backend.scm 141 */
			return
				BGl_backendzd2instrzd2resetzd2registerszd2zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1522), BgL_iz00_1523);
		}

	}



/* backend-check-inlines */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2checkzd2inlinesz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_bz00_117)
	{
		{	/* BackEnd/backend.scm 148 */
			{	/* BackEnd/backend.scm 148 */
				obj_t BgL_method1074z00_619;

				{	/* BackEnd/backend.scm 148 */
					obj_t BgL_res1688z00_972;

					{	/* BackEnd/backend.scm 148 */
						long BgL_objzd2classzd2numz00_943;

						BgL_objzd2classzd2numz00_943 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_117));
						{	/* BackEnd/backend.scm 148 */
							obj_t BgL_arg1811z00_944;

							BgL_arg1811z00_944 =
								PROCEDURE_REF
								(BGl_backendzd2checkzd2inlineszd2envzd2zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 148 */
								int BgL_offsetz00_947;

								BgL_offsetz00_947 = (int) (BgL_objzd2classzd2numz00_943);
								{	/* BackEnd/backend.scm 148 */
									long BgL_offsetz00_948;

									BgL_offsetz00_948 =
										((long) (BgL_offsetz00_947) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 148 */
										long BgL_modz00_949;

										BgL_modz00_949 =
											(BgL_offsetz00_948 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 148 */
											long BgL_restz00_951;

											BgL_restz00_951 =
												(BgL_offsetz00_948 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 148 */

												{	/* BackEnd/backend.scm 148 */
													obj_t BgL_bucketz00_953;

													BgL_bucketz00_953 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_944), BgL_modz00_949);
													BgL_res1688z00_972 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_953), BgL_restz00_951);
					}}}}}}}}
					BgL_method1074z00_619 = BgL_res1688z00_972;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1074z00_619, ((obj_t) BgL_bz00_117));
			}
		}

	}



/* &backend-check-inlines */
	obj_t BGl_z62backendzd2checkzd2inlinesz62zzbackend_backendz00(obj_t
		BgL_envz00_1524, obj_t BgL_bz00_1525)
	{
		{	/* BackEnd/backend.scm 148 */
			return
				BGl_backendzd2checkzd2inlinesz00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1525));
		}

	}



/* backend-gc-init */
	BGL_EXPORTED_DEF obj_t
		BGl_backendzd2gczd2initz00zzbackend_backendz00(BgL_backendz00_bglt
		BgL_bz00_118)
	{
		{	/* BackEnd/backend.scm 154 */
			{	/* BackEnd/backend.scm 154 */
				obj_t BgL_method1076z00_620;

				{	/* BackEnd/backend.scm 154 */
					obj_t BgL_res1693z00_1003;

					{	/* BackEnd/backend.scm 154 */
						long BgL_objzd2classzd2numz00_974;

						BgL_objzd2classzd2numz00_974 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_118));
						{	/* BackEnd/backend.scm 154 */
							obj_t BgL_arg1811z00_975;

							BgL_arg1811z00_975 =
								PROCEDURE_REF
								(BGl_backendzd2gczd2initzd2envzd2zzbackend_backendz00,
								(int) (1L));
							{	/* BackEnd/backend.scm 154 */
								int BgL_offsetz00_978;

								BgL_offsetz00_978 = (int) (BgL_objzd2classzd2numz00_974);
								{	/* BackEnd/backend.scm 154 */
									long BgL_offsetz00_979;

									BgL_offsetz00_979 =
										((long) (BgL_offsetz00_978) - OBJECT_TYPE);
									{	/* BackEnd/backend.scm 154 */
										long BgL_modz00_980;

										BgL_modz00_980 =
											(BgL_offsetz00_979 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/backend.scm 154 */
											long BgL_restz00_982;

											BgL_restz00_982 =
												(BgL_offsetz00_979 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/backend.scm 154 */

												{	/* BackEnd/backend.scm 154 */
													obj_t BgL_bucketz00_984;

													BgL_bucketz00_984 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_975), BgL_modz00_980);
													BgL_res1693z00_1003 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_984), BgL_restz00_982);
					}}}}}}}}
					BgL_method1076z00_620 = BgL_res1693z00_1003;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1076z00_620, ((obj_t) BgL_bz00_118));
			}
		}

	}



/* &backend-gc-init */
	obj_t BGl_z62backendzd2gczd2initz62zzbackend_backendz00(obj_t BgL_envz00_1526,
		obj_t BgL_bz00_1527)
	{
		{	/* BackEnd/backend.scm 154 */
			return
				BGl_backendzd2gczd2initz00zzbackend_backendz00(
				((BgL_backendz00_bglt) BgL_bz00_1527));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_backendz00(void)
	{
		{	/* BackEnd/backend.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1818z00zzbackend_backendz00));
		}

	}

#ifdef __cplusplus
}
#endif
