/*===========================================================================*/
/*   (BackEnd/bvm.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/bvm.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_BVM_TYPE_DEFINITIONS
#define BGL_BACKEND_BVM_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_bvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}             *BgL_bvmz00_bglt;


#endif													// BGL_BACKEND_BVM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62bvmzd2removezd2emptyzd2letzb0zzbackend_bvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2typedzd2eqzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2externzd2functionszd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62bvmzd2effectzb2z02zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2tracezd2supportz62zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbackend_bvmz00 = BUNSPEC;
	static obj_t BGl_z62bvmzd2externzd2variablesz62zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2debugzd2supportzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2debugzd2supportz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2typezd2checkz62zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2qualifiedzd2typeszd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bvmzd2effectzb2zd2setz12zc2zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2registerszd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2typedzd2funcallz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2removezd2emptyzd2letzd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2pragmazd2supportz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2externzd2variablesz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2heapzd2suffixz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2heapzd2suffixz62zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2heapzd2compatiblezd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bvmzd2heapzd2compatiblez62zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2variableszd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzbackend_bvmz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2requirezd2tailczd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2registerszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2typedzd2funcallzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		bool_t);
	static obj_t BGl_z62zc3z04anonymousza31132ze3ze5zzbackend_bvmz00(obj_t,
		obj_t);
	static obj_t BGl_z62bvmzf3z91zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2functionszb0zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzbackend_bvmz00(void);
	static obj_t BGl_objectzd2initzd2zzbackend_bvmz00(void);
	static obj_t BGl_z62bvmzd2typedzb0zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2pregisterszb0zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bvmzf3zf3zzbackend_bvmz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2requirezd2tailcz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2tracezd2supportzd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bvmzd2typeszb0zzbackend_bvmz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62bvmzd2pragmazd2supportzd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bvmzd2languagezd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2typezd2checkz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2foreignzd2closurezd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t BGl_bvmzd2namezd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2externzd2functionsz62zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2typedzd2eqz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t
		BGl_z62bvmzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62backendzd2initializa7ez12zd21115zd7zzbackend_bvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2variableszd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2externzd2functionsz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2callcczd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, bool_t);
	static obj_t BGl_z62bvmzd2callcczd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bvmzd2typedzd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	static BgL_bvmz00_bglt BGl_z62bvmzd2nilzb0zzbackend_bvmz00(obj_t);
	static obj_t BGl_z62bvmzd2languagezb0zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_bvmz00(void);
	BGL_EXPORTED_DECL obj_t BGl_bvmzd2typeszd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2functionszd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bvmzd2qualifiedzd2typesz62zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2heapzd2compatiblez00zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2typezd2checkzd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2qualifiedzd2typeszd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		bool_t);
	static obj_t BGl_z62bvmzd2debugzd2supportzd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bvmzd2heapzd2suffixzd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bvmzd2namezd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bvmzd2boundzd2checkzd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2pregisterszd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2typedzd2eqzd2setz12za2zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2heapzd2compatiblezd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		obj_t);
	static obj_t BGl_z62bvmzd2typedzd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bvmzd2debugzd2supportz62zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2heapzd2suffixzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2boundzd2checkzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		bool_t);
	static obj_t BGl_z62bvmzd2typedzd2funcallz62zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_bvmz00
		(BgL_bvmz00_bglt, bool_t);
	static obj_t BGl_z62bvmzd2requirezd2tailczd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bvmzd2typedzd2funcallzd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2foreignzd2clausezd2supportzd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_bvmz00
		(BgL_bvmz00_bglt, obj_t);
	static BgL_bvmz00_bglt BGl_z62lambda1130z62zzbackend_bvmz00(obj_t);
	static obj_t
		BGl_z62bvmzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2functionszd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2foreignzd2closurez62zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2foreignzd2clausezd2supportzb0zzbackend_bvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2variableszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzbackend_bvmz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2qualifiedzd2typesz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2requirezd2tailcz62zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2typedzd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2typezd2checkzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt, bool_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2boundzd2checkz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2pragmazd2supportz62zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2boundzd2checkz62zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2tvectorzd2descrzd2supportzb0zzbackend_bvmz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2tracezd2supportz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t
		BGl_z62bvmzd2removezd2emptyzd2letzd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bvmzd2callcczd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_cnstzd2initzd2zzbackend_bvmz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_bvmz00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_bvmz00(void);
	static obj_t
		BGl_z62bvmzd2externzd2variableszd2setz12za2zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzbackend_bvmz00(void);
	static obj_t BGl_z62bvmzd2registerszb0zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2typedzd2eqz62zzbackend_bvmz00(obj_t, obj_t);
	extern obj_t BGl_backendz00zzbackend_backendz00;
	static obj_t BGl_z62bvmzd2registerszd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2pragmazd2supportzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		bool_t);
	static obj_t BGl_z62bvmzd2callcczb0zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2pregisterszd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2externzd2typesz00zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2namezd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, obj_t);
	static obj_t BGl_z62bvmzd2srfi0zd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2tvectorzd2descrzd2supportzd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2functionszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2foreignzd2closurez00zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t BGl_z62bvmzd2typeszd2setz12z70zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2removezd2emptyzd2letzd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bvmzd2effectzb2z60zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2externzd2variableszd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_bvmz00_bglt BGl_bvmzd2nilzd2zzbackend_bvmz00(void);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2externzd2typeszd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_bvmz00zzbackend_bvmz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2languagezd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, obj_t);
	static obj_t BGl_z62bvmzd2srfi0zb0zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2effectzb2zd2setz12za0zzbackend_bvmz00(BgL_bvmz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2languagezd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2tracezd2supportzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt,
		bool_t);
	static obj_t
		BGl_z62bvmzd2externzd2functionszd2setz12za2zzbackend_bvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bvmzd2foreignzd2closurezd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bvmzd2externzd2typesz62zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2srfi0zd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, obj_t);
	static obj_t BGl_z62bvmzd2variableszb0zzbackend_bvmz00(obj_t, obj_t);
	static obj_t BGl_z62bvmzd2namezb0zzbackend_bvmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2typeszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, obj_t);
	static obj_t BGl_z62bvmzd2externzd2typeszd2setz12za2zzbackend_bvmz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bvmzd2pregisterszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bvmzd2srfi0zd2zzbackend_bvmz00(BgL_bvmz00_bglt);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1140z00zzbackend_bvmz00,
		BgL_bgl_za762za7c3za704anonymo1149za7,
		BGl_z62zc3z04anonymousza31132ze3ze5zzbackend_bvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1141z00zzbackend_bvmz00,
		BgL_bgl_za762lambda1130za7621150z00, BGl_z62lambda1130z62zzbackend_bvmz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1143z00zzbackend_bvmz00,
		BgL_bgl_za762backendza7d2ini1151z00,
		BGl_z62backendzd2initializa7ez12zd21115zd7zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2externzd2functionszd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2externza71152za7,
		BGl_z62bvmzd2externzd2functionszd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2effectzb2zd2envzb2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2effectza71153za7,
		BGl_z62bvmzd2effectzb2z02zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2tracezd2supportzd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2traceza7d1154za7,
		BGl_z62bvmzd2tracezd2supportz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2externzd2functionszd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2externza71155za7,
		BGl_z62bvmzd2externzd2functionsz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2callcczd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2callccza71156za7,
		BGl_z62bvmzd2callcczd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2typeszd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typesza7d1157za7,
		BGl_z62bvmzd2typeszd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2typezd2checkzd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typeza7d21158za7,
		BGl_z62bvmzd2typezd2checkz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2callcczd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2callccza71159za7,
		BGl_z62bvmzd2callcczb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2variableszd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2variabl1160z00,
		BGl_z62bvmzd2variableszd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2typeszd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typesza7b1161za7,
		BGl_z62bvmzd2typeszb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2foreignzd2closurezd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2foreign1162z00,
		BGl_z62bvmzd2foreignzd2closurezd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2boundzd2checkzd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2boundza7d1163za7,
		BGl_z62bvmzd2boundzd2checkz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2heapzd2compatiblezd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2heapza7d21164za7,
		BGl_z62bvmzd2heapzd2compatiblezd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2registerszd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2registe1165z00,
		BGl_z62bvmzd2registerszb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2typedzd2funcallzd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typedza7d1166za7,
		BGl_z62bvmzd2typedzd2funcallz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2boundzd2checkzd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2boundza7d1167za7,
		BGl_z62bvmzd2boundzd2checkzd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2typedzd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typedza7b1168za7,
		BGl_z62bvmzd2typedzb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2externzd2typeszd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2externza71169za7,
		BGl_z62bvmzd2externzd2typeszd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2requirezd2tailczd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2require1170z00,
		BGl_z62bvmzd2requirezd2tailcz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2heapzd2suffixzd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2heapza7d21171za7,
		BGl_z62bvmzd2heapzd2suffixz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2effectzb2zd2setz12zd2envz72zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2effectza71172za7,
		BGl_z62bvmzd2effectzb2zd2setz12zc2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2namezd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2nameza7d21173za7,
		BGl_z62bvmzd2namezd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2functionszd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2functio1174z00,
		BGl_z62bvmzd2functionszd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2debugzd2supportzd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2debugza7d1175za7,
		BGl_z62bvmzd2debugzd2supportz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2nilzd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2nilza7b0za71176z00,
		BGl_z62bvmzd2nilzb0zzbackend_bvmz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2pragmazd2supportzd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2pragmaza71177za7,
		BGl_z62bvmzd2pragmazd2supportzd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2foreignzd2clausezd2supportzd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2foreign1178z00,
		BGl_z62bvmzd2foreignzd2clausezd2supportzb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2pregisterszd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2pregist1179z00,
		BGl_z62bvmzd2pregisterszd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2requirezd2tailczd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2require1180z00,
		BGl_z62bvmzd2requirezd2tailczd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2tracezd2supportzd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2traceza7d1181za7,
		BGl_z62bvmzd2tracezd2supportzd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2foreignzd2closurezd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2foreign1182z00,
		BGl_z62bvmzd2foreignzd2closurez62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2pragmazd2supportzd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2pragmaza71183za7,
		BGl_z62bvmzd2pragmazd2supportz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2qualifiedzd2typeszd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2qualifi1184z00,
		BGl_z62bvmzd2qualifiedzd2typesz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2qualifiedzd2typeszd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2qualifi1185z00,
		BGl_z62bvmzd2qualifiedzd2typeszd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2namezd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2nameza7b01186za7, BGl_z62bvmzd2namezb0zzbackend_bvmz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2typedzd2eqzd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typedza7d1187za7,
		BGl_z62bvmzd2typedzd2eqzd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2languagezd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2languag1188z00,
		BGl_z62bvmzd2languagezb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2pregisterszd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2pregist1189z00,
		BGl_z62bvmzd2pregisterszb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2typedzd2eqzd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typedza7d1190za7,
		BGl_z62bvmzd2typedzd2eqz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2registerszd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2registe1191z00,
		BGl_z62bvmzd2registerszd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2typedzd2funcallzd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typedza7d1192za7,
		BGl_z62bvmzd2typedzd2funcallzd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2debugzd2supportzd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2debugza7d1193za7,
		BGl_z62bvmzd2debugzd2supportzd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2variableszd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2variabl1194z00,
		BGl_z62bvmzd2variableszb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzf3zd2envz21zzbackend_bvmz00,
		BgL_bgl_za762bvmza7f3za791za7za7ba1195za7, BGl_z62bvmzf3z91zzbackend_bvmz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2srfi0zd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2srfi0za7b1196za7,
		BGl_z62bvmzd2srfi0zb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2typezd2checkzd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typeza7d21197za7,
		BGl_z62bvmzd2typezd2checkzd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2foreignzd2clausezd2supportzd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2foreign1198z00,
		BGl_z62bvmzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_bvmz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2removezd2emptyzd2letzd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2removeza71199za7,
		BGl_z62bvmzd2removezd2emptyzd2letzd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2externzd2variableszd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2externza71200za7,
		BGl_z62bvmzd2externzd2variableszd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2removezd2emptyzd2letzd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2removeza71201za7,
		BGl_z62bvmzd2removezd2emptyzd2letzb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2tvectorzd2descrzd2supportzd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2tvector1202z00,
		BGl_z62bvmzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_bvmz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2externzd2variableszd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2externza71203za7,
		BGl_z62bvmzd2externzd2variablesz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2languagezd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2languag1204z00,
		BGl_z62bvmzd2languagezd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2typedzd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2typedza7d1205za7,
		BGl_z62bvmzd2typedzd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2tvectorzd2descrzd2supportzd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2tvector1206z00,
		BGl_z62bvmzd2tvectorzd2descrzd2supportzb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bvmzd2functionszd2envz00zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2functio1207z00,
		BGl_z62bvmzd2functionszb0zzbackend_bvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2heapzd2suffixzd2setz12zd2envz12zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2heapza7d21208za7,
		BGl_z62bvmzd2heapzd2suffixzd2setz12za2zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2srfi0zd2setz12zd2envzc0zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2srfi0za7d1209za7,
		BGl_z62bvmzd2srfi0zd2setz12z70zzbackend_bvmz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2heapzd2compatiblezd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2heapza7d21210za7,
		BGl_z62bvmzd2heapzd2compatiblez62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1142z00zzbackend_bvmz00,
		BgL_bgl_string1142za700za7za7b1211za7, "", 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bvmzd2externzd2typeszd2envzd2zzbackend_bvmz00,
		BgL_bgl_za762bvmza7d2externza71212za7,
		BGl_z62bvmzd2externzd2typesz62zzbackend_bvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1144z00zzbackend_bvmz00,
		BgL_bgl_string1144za700za7za7b1213za7, "backend-initialize!", 19);
	      DEFINE_STRING(BGl_string1145z00zzbackend_bvmz00,
		BgL_bgl_string1145za700za7za7b1214za7, "backend_bvm", 11);
	      DEFINE_STRING(BGl_string1146z00zzbackend_bvmz00,
		BgL_bgl_string1146za700za7za7b1215za7, "_ backend_bvm bvm ", 18);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzbackend_bvmz00));
		     ADD_ROOT((void *) (&BGl_bvmz00zzbackend_bvmz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzbackend_bvmz00(long
		BgL_checksumz00_652, char *BgL_fromz00_653)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_bvmz00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_bvmz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_bvmz00();
					BGl_libraryzd2moduleszd2initz00zzbackend_bvmz00();
					BGl_cnstzd2initzd2zzbackend_bvmz00();
					BGl_importedzd2moduleszd2initz00zzbackend_bvmz00();
					BGl_objectzd2initzd2zzbackend_bvmz00();
					BGl_methodzd2initzd2zzbackend_bvmz00();
					return BGl_toplevelzd2initzd2zzbackend_bvmz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_bvmz00(void)
	{
		{	/* BackEnd/bvm.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "backend_bvm");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_bvm");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "backend_bvm");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "backend_bvm");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"backend_bvm");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "backend_bvm");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_bvm");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_bvmz00(void)
	{
		{	/* BackEnd/bvm.scm 15 */
			{	/* BackEnd/bvm.scm 15 */
				obj_t BgL_cportz00_622;

				{	/* BackEnd/bvm.scm 15 */
					obj_t BgL_stringz00_629;

					BgL_stringz00_629 = BGl_string1146z00zzbackend_bvmz00;
					{	/* BackEnd/bvm.scm 15 */
						obj_t BgL_startz00_630;

						BgL_startz00_630 = BINT(0L);
						{	/* BackEnd/bvm.scm 15 */
							obj_t BgL_endz00_631;

							BgL_endz00_631 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_629)));
							{	/* BackEnd/bvm.scm 15 */

								BgL_cportz00_622 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_629, BgL_startz00_630, BgL_endz00_631);
				}}}}
				{
					long BgL_iz00_623;

					BgL_iz00_623 = 2L;
				BgL_loopz00_624:
					if ((BgL_iz00_623 == -1L))
						{	/* BackEnd/bvm.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/bvm.scm 15 */
							{	/* BackEnd/bvm.scm 15 */
								obj_t BgL_arg1148z00_625;

								{	/* BackEnd/bvm.scm 15 */

									{	/* BackEnd/bvm.scm 15 */
										obj_t BgL_locationz00_627;

										BgL_locationz00_627 = BBOOL(((bool_t) 0));
										{	/* BackEnd/bvm.scm 15 */

											BgL_arg1148z00_625 =
												BGl_readz00zz__readerz00(BgL_cportz00_622,
												BgL_locationz00_627);
										}
									}
								}
								{	/* BackEnd/bvm.scm 15 */
									int BgL_tmpz00_680;

									BgL_tmpz00_680 = (int) (BgL_iz00_623);
									CNST_TABLE_SET(BgL_tmpz00_680, BgL_arg1148z00_625);
							}}
							{	/* BackEnd/bvm.scm 15 */
								int BgL_auxz00_628;

								BgL_auxz00_628 = (int) ((BgL_iz00_623 - 1L));
								{
									long BgL_iz00_685;

									BgL_iz00_685 = (long) (BgL_auxz00_628);
									BgL_iz00_623 = BgL_iz00_685;
									goto BgL_loopz00_624;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_bvmz00(void)
	{
		{	/* BackEnd/bvm.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbackend_bvmz00(void)
	{
		{	/* BackEnd/bvm.scm 15 */
			return BUNSPEC;
		}

	}



/* bvm? */
	BGL_EXPORTED_DEF bool_t BGl_bvmzf3zf3zzbackend_bvmz00(obj_t BgL_objz00_3)
	{
		{	/* BackEnd/bvm.sch 78 */
			{	/* BackEnd/bvm.sch 78 */
				obj_t BgL_classz00_633;

				BgL_classz00_633 = BGl_bvmz00zzbackend_bvmz00;
				if (BGL_OBJECTP(BgL_objz00_3))
					{	/* BackEnd/bvm.sch 78 */
						BgL_objectz00_bglt BgL_arg1807z00_634;

						BgL_arg1807z00_634 = (BgL_objectz00_bglt) (BgL_objz00_3);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* BackEnd/bvm.sch 78 */
								long BgL_idxz00_635;

								BgL_idxz00_635 = BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_634);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_635 + 2L)) == BgL_classz00_633);
							}
						else
							{	/* BackEnd/bvm.sch 78 */
								bool_t BgL_res1138z00_638;

								{	/* BackEnd/bvm.sch 78 */
									obj_t BgL_oclassz00_639;

									{	/* BackEnd/bvm.sch 78 */
										obj_t BgL_arg1815z00_640;
										long BgL_arg1816z00_641;

										BgL_arg1815z00_640 = (BGl_za2classesza2z00zz__objectz00);
										{	/* BackEnd/bvm.sch 78 */
											long BgL_arg1817z00_642;

											BgL_arg1817z00_642 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_634);
											BgL_arg1816z00_641 = (BgL_arg1817z00_642 - OBJECT_TYPE);
										}
										BgL_oclassz00_639 =
											VECTOR_REF(BgL_arg1815z00_640, BgL_arg1816z00_641);
									}
									{	/* BackEnd/bvm.sch 78 */
										bool_t BgL__ortest_1115z00_643;

										BgL__ortest_1115z00_643 =
											(BgL_classz00_633 == BgL_oclassz00_639);
										if (BgL__ortest_1115z00_643)
											{	/* BackEnd/bvm.sch 78 */
												BgL_res1138z00_638 = BgL__ortest_1115z00_643;
											}
										else
											{	/* BackEnd/bvm.sch 78 */
												long BgL_odepthz00_644;

												{	/* BackEnd/bvm.sch 78 */
													obj_t BgL_arg1804z00_645;

													BgL_arg1804z00_645 = (BgL_oclassz00_639);
													BgL_odepthz00_644 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_645);
												}
												if ((2L < BgL_odepthz00_644))
													{	/* BackEnd/bvm.sch 78 */
														obj_t BgL_arg1802z00_646;

														{	/* BackEnd/bvm.sch 78 */
															obj_t BgL_arg1803z00_647;

															BgL_arg1803z00_647 = (BgL_oclassz00_639);
															BgL_arg1802z00_646 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_647, 2L);
														}
														BgL_res1138z00_638 =
															(BgL_arg1802z00_646 == BgL_classz00_633);
													}
												else
													{	/* BackEnd/bvm.sch 78 */
														BgL_res1138z00_638 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1138z00_638;
							}
					}
				else
					{	/* BackEnd/bvm.sch 78 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bvm? */
	obj_t BGl_z62bvmzf3z91zzbackend_bvmz00(obj_t BgL_envz00_464,
		obj_t BgL_objz00_465)
	{
		{	/* BackEnd/bvm.sch 78 */
			return BBOOL(BGl_bvmzf3zf3zzbackend_bvmz00(BgL_objz00_465));
		}

	}



/* bvm-nil */
	BGL_EXPORTED_DEF BgL_bvmz00_bglt BGl_bvmzd2nilzd2zzbackend_bvmz00(void)
	{
		{	/* BackEnd/bvm.sch 79 */
			{	/* BackEnd/bvm.sch 79 */
				obj_t BgL_classz00_368;

				BgL_classz00_368 = BGl_bvmz00zzbackend_bvmz00;
				{	/* BackEnd/bvm.sch 79 */
					obj_t BgL__ortest_1117z00_369;

					BgL__ortest_1117z00_369 = BGL_CLASS_NIL(BgL_classz00_368);
					if (CBOOL(BgL__ortest_1117z00_369))
						{	/* BackEnd/bvm.sch 79 */
							return ((BgL_bvmz00_bglt) BgL__ortest_1117z00_369);
						}
					else
						{	/* BackEnd/bvm.sch 79 */
							return
								((BgL_bvmz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_368));
						}
				}
			}
		}

	}



/* &bvm-nil */
	BgL_bvmz00_bglt BGl_z62bvmzd2nilzb0zzbackend_bvmz00(obj_t BgL_envz00_466)
	{
		{	/* BackEnd/bvm.sch 79 */
			return BGl_bvmzd2nilzd2zzbackend_bvmz00();
		}

	}



/* bvm-typed-funcall */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2typedzd2funcallz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_4)
	{
		{	/* BackEnd/bvm.sch 80 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_4)))->BgL_typedzd2funcallzd2);
		}

	}



/* &bvm-typed-funcall */
	obj_t BGl_z62bvmzd2typedzd2funcallz62zzbackend_bvmz00(obj_t BgL_envz00_467,
		obj_t BgL_oz00_468)
	{
		{	/* BackEnd/bvm.sch 80 */
			return
				BBOOL(BGl_bvmzd2typedzd2funcallz00zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_468)));
		}

	}



/* bvm-typed-funcall-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2typedzd2funcallzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_5, bool_t BgL_vz00_6)
	{
		{	/* BackEnd/bvm.sch 81 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_5)))->BgL_typedzd2funcallzd2) =
				((bool_t) BgL_vz00_6), BUNSPEC);
		}

	}



/* &bvm-typed-funcall-set! */
	obj_t BGl_z62bvmzd2typedzd2funcallzd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_469, obj_t BgL_oz00_470, obj_t BgL_vz00_471)
	{
		{	/* BackEnd/bvm.sch 81 */
			return
				BGl_bvmzd2typedzd2funcallzd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_470), CBOOL(BgL_vz00_471));
		}

	}



/* bvm-type-check */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2typezd2checkz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_7)
	{
		{	/* BackEnd/bvm.sch 82 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_7)))->BgL_typezd2checkzd2);
		}

	}



/* &bvm-type-check */
	obj_t BGl_z62bvmzd2typezd2checkz62zzbackend_bvmz00(obj_t BgL_envz00_472,
		obj_t BgL_oz00_473)
	{
		{	/* BackEnd/bvm.sch 82 */
			return
				BBOOL(BGl_bvmzd2typezd2checkz00zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_473)));
		}

	}



/* bvm-type-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2typezd2checkzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_8, bool_t BgL_vz00_9)
	{
		{	/* BackEnd/bvm.sch 83 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_8)))->BgL_typezd2checkzd2) =
				((bool_t) BgL_vz00_9), BUNSPEC);
		}

	}



/* &bvm-type-check-set! */
	obj_t BGl_z62bvmzd2typezd2checkzd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_474, obj_t BgL_oz00_475, obj_t BgL_vz00_476)
	{
		{	/* BackEnd/bvm.sch 83 */
			return
				BGl_bvmzd2typezd2checkzd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_475), CBOOL(BgL_vz00_476));
		}

	}



/* bvm-bound-check */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2boundzd2checkz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_10)
	{
		{	/* BackEnd/bvm.sch 84 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_10)))->BgL_boundzd2checkzd2);
		}

	}



/* &bvm-bound-check */
	obj_t BGl_z62bvmzd2boundzd2checkz62zzbackend_bvmz00(obj_t BgL_envz00_477,
		obj_t BgL_oz00_478)
	{
		{	/* BackEnd/bvm.sch 84 */
			return
				BBOOL(BGl_bvmzd2boundzd2checkz00zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_478)));
		}

	}



/* bvm-bound-check-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2boundzd2checkzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_11, bool_t BgL_vz00_12)
	{
		{	/* BackEnd/bvm.sch 85 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_11)))->BgL_boundzd2checkzd2) =
				((bool_t) BgL_vz00_12), BUNSPEC);
		}

	}



/* &bvm-bound-check-set! */
	obj_t BGl_z62bvmzd2boundzd2checkzd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_479, obj_t BgL_oz00_480, obj_t BgL_vz00_481)
	{
		{	/* BackEnd/bvm.sch 85 */
			return
				BGl_bvmzd2boundzd2checkzd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_480), CBOOL(BgL_vz00_481));
		}

	}



/* bvm-pregisters */
	BGL_EXPORTED_DEF obj_t BGl_bvmzd2pregisterszd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_13)
	{
		{	/* BackEnd/bvm.sch 86 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_13)))->BgL_pregistersz00);
		}

	}



/* &bvm-pregisters */
	obj_t BGl_z62bvmzd2pregisterszb0zzbackend_bvmz00(obj_t BgL_envz00_482,
		obj_t BgL_oz00_483)
	{
		{	/* BackEnd/bvm.sch 86 */
			return
				BGl_bvmzd2pregisterszd2zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_483));
		}

	}



/* bvm-pregisters-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2pregisterszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_14, obj_t BgL_vz00_15)
	{
		{	/* BackEnd/bvm.sch 87 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_14)))->BgL_pregistersz00) =
				((obj_t) BgL_vz00_15), BUNSPEC);
		}

	}



/* &bvm-pregisters-set! */
	obj_t BGl_z62bvmzd2pregisterszd2setz12z70zzbackend_bvmz00(obj_t
		BgL_envz00_484, obj_t BgL_oz00_485, obj_t BgL_vz00_486)
	{
		{	/* BackEnd/bvm.sch 87 */
			return
				BGl_bvmzd2pregisterszd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_485), BgL_vz00_486);
		}

	}



/* bvm-registers */
	BGL_EXPORTED_DEF obj_t BGl_bvmzd2registerszd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_16)
	{
		{	/* BackEnd/bvm.sch 88 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_16)))->BgL_registersz00);
		}

	}



/* &bvm-registers */
	obj_t BGl_z62bvmzd2registerszb0zzbackend_bvmz00(obj_t BgL_envz00_487,
		obj_t BgL_oz00_488)
	{
		{	/* BackEnd/bvm.sch 88 */
			return
				BGl_bvmzd2registerszd2zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_488));
		}

	}



/* bvm-registers-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2registerszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_17,
		obj_t BgL_vz00_18)
	{
		{	/* BackEnd/bvm.sch 89 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_17)))->BgL_registersz00) =
				((obj_t) BgL_vz00_18), BUNSPEC);
		}

	}



/* &bvm-registers-set! */
	obj_t BGl_z62bvmzd2registerszd2setz12z70zzbackend_bvmz00(obj_t BgL_envz00_489,
		obj_t BgL_oz00_490, obj_t BgL_vz00_491)
	{
		{	/* BackEnd/bvm.sch 89 */
			return
				BGl_bvmzd2registerszd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_490), BgL_vz00_491);
		}

	}



/* bvm-require-tailc */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2requirezd2tailcz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_19)
	{
		{	/* BackEnd/bvm.sch 90 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_19)))->BgL_requirezd2tailczd2);
		}

	}



/* &bvm-require-tailc */
	obj_t BGl_z62bvmzd2requirezd2tailcz62zzbackend_bvmz00(obj_t BgL_envz00_492,
		obj_t BgL_oz00_493)
	{
		{	/* BackEnd/bvm.sch 90 */
			return
				BBOOL(BGl_bvmzd2requirezd2tailcz00zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_493)));
		}

	}



/* bvm-require-tailc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2requirezd2tailczd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_20, bool_t BgL_vz00_21)
	{
		{	/* BackEnd/bvm.sch 91 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_20)))->BgL_requirezd2tailczd2) =
				((bool_t) BgL_vz00_21), BUNSPEC);
		}

	}



/* &bvm-require-tailc-set! */
	obj_t BGl_z62bvmzd2requirezd2tailczd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_494, obj_t BgL_oz00_495, obj_t BgL_vz00_496)
	{
		{	/* BackEnd/bvm.sch 91 */
			return
				BGl_bvmzd2requirezd2tailczd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_495), CBOOL(BgL_vz00_496));
		}

	}



/* bvm-tvector-descr-support */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2tvectorzd2descrzd2supportzd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_22)
	{
		{	/* BackEnd/bvm.sch 92 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_22)))->
				BgL_tvectorzd2descrzd2supportz00);
		}

	}



/* &bvm-tvector-descr-support */
	obj_t BGl_z62bvmzd2tvectorzd2descrzd2supportzb0zzbackend_bvmz00(obj_t
		BgL_envz00_497, obj_t BgL_oz00_498)
	{
		{	/* BackEnd/bvm.sch 92 */
			return
				BBOOL(BGl_bvmzd2tvectorzd2descrzd2supportzd2zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_498)));
		}

	}



/* bvm-tvector-descr-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_bvmz00
		(BgL_bvmz00_bglt BgL_oz00_23, bool_t BgL_vz00_24)
	{
		{	/* BackEnd/bvm.sch 93 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_23)))->
					BgL_tvectorzd2descrzd2supportz00) = ((bool_t) BgL_vz00_24), BUNSPEC);
		}

	}



/* &bvm-tvector-descr-support-set! */
	obj_t BGl_z62bvmzd2tvectorzd2descrzd2supportzd2setz12z70zzbackend_bvmz00(obj_t
		BgL_envz00_499, obj_t BgL_oz00_500, obj_t BgL_vz00_501)
	{
		{	/* BackEnd/bvm.sch 93 */
			return
				BGl_bvmzd2tvectorzd2descrzd2supportzd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_500), CBOOL(BgL_vz00_501));
		}

	}



/* bvm-pragma-support */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2pragmazd2supportz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_25)
	{
		{	/* BackEnd/bvm.sch 94 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_25)))->BgL_pragmazd2supportzd2);
		}

	}



/* &bvm-pragma-support */
	obj_t BGl_z62bvmzd2pragmazd2supportz62zzbackend_bvmz00(obj_t BgL_envz00_502,
		obj_t BgL_oz00_503)
	{
		{	/* BackEnd/bvm.sch 94 */
			return
				BBOOL(BGl_bvmzd2pragmazd2supportz00zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_503)));
		}

	}



/* bvm-pragma-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2pragmazd2supportzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_26, bool_t BgL_vz00_27)
	{
		{	/* BackEnd/bvm.sch 95 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_26)))->BgL_pragmazd2supportzd2) =
				((bool_t) BgL_vz00_27), BUNSPEC);
		}

	}



/* &bvm-pragma-support-set! */
	obj_t BGl_z62bvmzd2pragmazd2supportzd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_504, obj_t BgL_oz00_505, obj_t BgL_vz00_506)
	{
		{	/* BackEnd/bvm.sch 95 */
			return
				BGl_bvmzd2pragmazd2supportzd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_505), CBOOL(BgL_vz00_506));
		}

	}



/* bvm-debug-support */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2debugzd2supportz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_28)
	{
		{	/* BackEnd/bvm.sch 96 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_28)))->BgL_debugzd2supportzd2);
		}

	}



/* &bvm-debug-support */
	obj_t BGl_z62bvmzd2debugzd2supportz62zzbackend_bvmz00(obj_t BgL_envz00_507,
		obj_t BgL_oz00_508)
	{
		{	/* BackEnd/bvm.sch 96 */
			return
				BGl_bvmzd2debugzd2supportz00zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_508));
		}

	}



/* bvm-debug-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2debugzd2supportzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* BackEnd/bvm.sch 97 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_29)))->BgL_debugzd2supportzd2) =
				((obj_t) BgL_vz00_30), BUNSPEC);
		}

	}



/* &bvm-debug-support-set! */
	obj_t BGl_z62bvmzd2debugzd2supportzd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_509, obj_t BgL_oz00_510, obj_t BgL_vz00_511)
	{
		{	/* BackEnd/bvm.sch 97 */
			return
				BGl_bvmzd2debugzd2supportzd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_510), BgL_vz00_511);
		}

	}



/* bvm-foreign-clause-support */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2foreignzd2clausezd2supportzd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_31)
	{
		{	/* BackEnd/bvm.sch 98 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_31)))->
				BgL_foreignzd2clausezd2supportz00);
		}

	}



/* &bvm-foreign-clause-support */
	obj_t BGl_z62bvmzd2foreignzd2clausezd2supportzb0zzbackend_bvmz00(obj_t
		BgL_envz00_512, obj_t BgL_oz00_513)
	{
		{	/* BackEnd/bvm.sch 98 */
			return
				BGl_bvmzd2foreignzd2clausezd2supportzd2zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_513));
		}

	}



/* bvm-foreign-clause-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_bvmz00
		(BgL_bvmz00_bglt BgL_oz00_32, obj_t BgL_vz00_33)
	{
		{	/* BackEnd/bvm.sch 99 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_32)))->
					BgL_foreignzd2clausezd2supportz00) = ((obj_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &bvm-foreign-clause-support-set! */
	obj_t
		BGl_z62bvmzd2foreignzd2clausezd2supportzd2setz12z70zzbackend_bvmz00(obj_t
		BgL_envz00_514, obj_t BgL_oz00_515, obj_t BgL_vz00_516)
	{
		{	/* BackEnd/bvm.sch 99 */
			return
				BGl_bvmzd2foreignzd2clausezd2supportzd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_515), BgL_vz00_516);
		}

	}



/* bvm-trace-support */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2tracezd2supportz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_34)
	{
		{	/* BackEnd/bvm.sch 100 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_34)))->BgL_tracezd2supportzd2);
		}

	}



/* &bvm-trace-support */
	obj_t BGl_z62bvmzd2tracezd2supportz62zzbackend_bvmz00(obj_t BgL_envz00_517,
		obj_t BgL_oz00_518)
	{
		{	/* BackEnd/bvm.sch 100 */
			return
				BBOOL(BGl_bvmzd2tracezd2supportz00zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_518)));
		}

	}



/* bvm-trace-support-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2tracezd2supportzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_35, bool_t BgL_vz00_36)
	{
		{	/* BackEnd/bvm.sch 101 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_35)))->BgL_tracezd2supportzd2) =
				((bool_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &bvm-trace-support-set! */
	obj_t BGl_z62bvmzd2tracezd2supportzd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_519, obj_t BgL_oz00_520, obj_t BgL_vz00_521)
	{
		{	/* BackEnd/bvm.sch 101 */
			return
				BGl_bvmzd2tracezd2supportzd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_520), CBOOL(BgL_vz00_521));
		}

	}



/* bvm-typed-eq */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2typedzd2eqz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_37)
	{
		{	/* BackEnd/bvm.sch 102 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_37)))->BgL_typedzd2eqzd2);
		}

	}



/* &bvm-typed-eq */
	obj_t BGl_z62bvmzd2typedzd2eqz62zzbackend_bvmz00(obj_t BgL_envz00_522,
		obj_t BgL_oz00_523)
	{
		{	/* BackEnd/bvm.sch 102 */
			return
				BBOOL(BGl_bvmzd2typedzd2eqz00zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_523)));
		}

	}



/* bvm-typed-eq-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2typedzd2eqzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_38, bool_t BgL_vz00_39)
	{
		{	/* BackEnd/bvm.sch 103 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_38)))->BgL_typedzd2eqzd2) =
				((bool_t) BgL_vz00_39), BUNSPEC);
		}

	}



/* &bvm-typed-eq-set! */
	obj_t BGl_z62bvmzd2typedzd2eqzd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_524, obj_t BgL_oz00_525, obj_t BgL_vz00_526)
	{
		{	/* BackEnd/bvm.sch 103 */
			return
				BGl_bvmzd2typedzd2eqzd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_525), CBOOL(BgL_vz00_526));
		}

	}



/* bvm-foreign-closure */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2foreignzd2closurez00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_40)
	{
		{	/* BackEnd/bvm.sch 104 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_40)))->BgL_foreignzd2closurezd2);
		}

	}



/* &bvm-foreign-closure */
	obj_t BGl_z62bvmzd2foreignzd2closurez62zzbackend_bvmz00(obj_t BgL_envz00_527,
		obj_t BgL_oz00_528)
	{
		{	/* BackEnd/bvm.sch 104 */
			return
				BBOOL(BGl_bvmzd2foreignzd2closurez00zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_528)));
		}

	}



/* bvm-foreign-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2foreignzd2closurezd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_41, bool_t BgL_vz00_42)
	{
		{	/* BackEnd/bvm.sch 105 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_41)))->BgL_foreignzd2closurezd2) =
				((bool_t) BgL_vz00_42), BUNSPEC);
		}

	}



/* &bvm-foreign-closure-set! */
	obj_t BGl_z62bvmzd2foreignzd2closurezd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_529, obj_t BgL_oz00_530, obj_t BgL_vz00_531)
	{
		{	/* BackEnd/bvm.sch 105 */
			return
				BGl_bvmzd2foreignzd2closurezd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_530), CBOOL(BgL_vz00_531));
		}

	}



/* bvm-remove-empty-let */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2removezd2emptyzd2letzd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_43)
	{
		{	/* BackEnd/bvm.sch 106 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_43)))->BgL_removezd2emptyzd2letz00);
		}

	}



/* &bvm-remove-empty-let */
	obj_t BGl_z62bvmzd2removezd2emptyzd2letzb0zzbackend_bvmz00(obj_t
		BgL_envz00_532, obj_t BgL_oz00_533)
	{
		{	/* BackEnd/bvm.sch 106 */
			return
				BBOOL(BGl_bvmzd2removezd2emptyzd2letzd2zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_533)));
		}

	}



/* bvm-remove-empty-let-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2removezd2emptyzd2letzd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_44, bool_t BgL_vz00_45)
	{
		{	/* BackEnd/bvm.sch 107 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_44)))->
					BgL_removezd2emptyzd2letz00) = ((bool_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &bvm-remove-empty-let-set! */
	obj_t BGl_z62bvmzd2removezd2emptyzd2letzd2setz12z70zzbackend_bvmz00(obj_t
		BgL_envz00_534, obj_t BgL_oz00_535, obj_t BgL_vz00_536)
	{
		{	/* BackEnd/bvm.sch 107 */
			return
				BGl_bvmzd2removezd2emptyzd2letzd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_535), CBOOL(BgL_vz00_536));
		}

	}



/* bvm-effect+ */
	BGL_EXPORTED_DEF bool_t BGl_bvmzd2effectzb2z60zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_46)
	{
		{	/* BackEnd/bvm.sch 108 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_46)))->BgL_effectzb2zb2);
		}

	}



/* &bvm-effect+ */
	obj_t BGl_z62bvmzd2effectzb2z02zzbackend_bvmz00(obj_t BgL_envz00_537,
		obj_t BgL_oz00_538)
	{
		{	/* BackEnd/bvm.sch 108 */
			return
				BBOOL(BGl_bvmzd2effectzb2z60zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_538)));
		}

	}



/* bvm-effect+-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2effectzb2zd2setz12za0zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_47,
		bool_t BgL_vz00_48)
	{
		{	/* BackEnd/bvm.sch 109 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_47)))->BgL_effectzb2zb2) =
				((bool_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &bvm-effect+-set! */
	obj_t BGl_z62bvmzd2effectzb2zd2setz12zc2zzbackend_bvmz00(obj_t BgL_envz00_539,
		obj_t BgL_oz00_540, obj_t BgL_vz00_541)
	{
		{	/* BackEnd/bvm.sch 109 */
			return
				BGl_bvmzd2effectzb2zd2setz12za0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_540), CBOOL(BgL_vz00_541));
		}

	}



/* bvm-qualified-types */
	BGL_EXPORTED_DEF bool_t
		BGl_bvmzd2qualifiedzd2typesz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_49)
	{
		{	/* BackEnd/bvm.sch 110 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_49)))->BgL_qualifiedzd2typeszd2);
		}

	}



/* &bvm-qualified-types */
	obj_t BGl_z62bvmzd2qualifiedzd2typesz62zzbackend_bvmz00(obj_t BgL_envz00_542,
		obj_t BgL_oz00_543)
	{
		{	/* BackEnd/bvm.sch 110 */
			return
				BBOOL(BGl_bvmzd2qualifiedzd2typesz00zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_543)));
		}

	}



/* bvm-qualified-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2qualifiedzd2typeszd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_50, bool_t BgL_vz00_51)
	{
		{	/* BackEnd/bvm.sch 111 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_50)))->BgL_qualifiedzd2typeszd2) =
				((bool_t) BgL_vz00_51), BUNSPEC);
		}

	}



/* &bvm-qualified-types-set! */
	obj_t BGl_z62bvmzd2qualifiedzd2typeszd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_544, obj_t BgL_oz00_545, obj_t BgL_vz00_546)
	{
		{	/* BackEnd/bvm.sch 111 */
			return
				BGl_bvmzd2qualifiedzd2typeszd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_545), CBOOL(BgL_vz00_546));
		}

	}



/* bvm-callcc */
	BGL_EXPORTED_DEF bool_t BGl_bvmzd2callcczd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_52)
	{
		{	/* BackEnd/bvm.sch 112 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_52)))->BgL_callccz00);
		}

	}



/* &bvm-callcc */
	obj_t BGl_z62bvmzd2callcczb0zzbackend_bvmz00(obj_t BgL_envz00_547,
		obj_t BgL_oz00_548)
	{
		{	/* BackEnd/bvm.sch 112 */
			return
				BBOOL(BGl_bvmzd2callcczd2zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_548)));
		}

	}



/* bvm-callcc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2callcczd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_53,
		bool_t BgL_vz00_54)
	{
		{	/* BackEnd/bvm.sch 113 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_53)))->BgL_callccz00) =
				((bool_t) BgL_vz00_54), BUNSPEC);
		}

	}



/* &bvm-callcc-set! */
	obj_t BGl_z62bvmzd2callcczd2setz12z70zzbackend_bvmz00(obj_t BgL_envz00_549,
		obj_t BgL_oz00_550, obj_t BgL_vz00_551)
	{
		{	/* BackEnd/bvm.sch 113 */
			return
				BGl_bvmzd2callcczd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_550), CBOOL(BgL_vz00_551));
		}

	}



/* bvm-heap-compatible */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2heapzd2compatiblez00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_55)
	{
		{	/* BackEnd/bvm.sch 114 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_55)))->BgL_heapzd2compatiblezd2);
		}

	}



/* &bvm-heap-compatible */
	obj_t BGl_z62bvmzd2heapzd2compatiblez62zzbackend_bvmz00(obj_t BgL_envz00_552,
		obj_t BgL_oz00_553)
	{
		{	/* BackEnd/bvm.sch 114 */
			return
				BGl_bvmzd2heapzd2compatiblez00zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_553));
		}

	}



/* bvm-heap-compatible-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2heapzd2compatiblezd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_56, obj_t BgL_vz00_57)
	{
		{	/* BackEnd/bvm.sch 115 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_56)))->BgL_heapzd2compatiblezd2) =
				((obj_t) BgL_vz00_57), BUNSPEC);
		}

	}



/* &bvm-heap-compatible-set! */
	obj_t BGl_z62bvmzd2heapzd2compatiblezd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_554, obj_t BgL_oz00_555, obj_t BgL_vz00_556)
	{
		{	/* BackEnd/bvm.sch 115 */
			return
				BGl_bvmzd2heapzd2compatiblezd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_555), BgL_vz00_556);
		}

	}



/* bvm-heap-suffix */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2heapzd2suffixz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_58)
	{
		{	/* BackEnd/bvm.sch 116 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_58)))->BgL_heapzd2suffixzd2);
		}

	}



/* &bvm-heap-suffix */
	obj_t BGl_z62bvmzd2heapzd2suffixz62zzbackend_bvmz00(obj_t BgL_envz00_557,
		obj_t BgL_oz00_558)
	{
		{	/* BackEnd/bvm.sch 116 */
			return
				BGl_bvmzd2heapzd2suffixz00zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_558));
		}

	}



/* bvm-heap-suffix-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2heapzd2suffixzd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* BackEnd/bvm.sch 117 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_59)))->BgL_heapzd2suffixzd2) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &bvm-heap-suffix-set! */
	obj_t BGl_z62bvmzd2heapzd2suffixzd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_559, obj_t BgL_oz00_560, obj_t BgL_vz00_561)
	{
		{	/* BackEnd/bvm.sch 117 */
			return
				BGl_bvmzd2heapzd2suffixzd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_560), BgL_vz00_561);
		}

	}



/* bvm-typed */
	BGL_EXPORTED_DEF bool_t BGl_bvmzd2typedzd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_61)
	{
		{	/* BackEnd/bvm.sch 118 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_61)))->BgL_typedz00);
		}

	}



/* &bvm-typed */
	obj_t BGl_z62bvmzd2typedzb0zzbackend_bvmz00(obj_t BgL_envz00_562,
		obj_t BgL_oz00_563)
	{
		{	/* BackEnd/bvm.sch 118 */
			return
				BBOOL(BGl_bvmzd2typedzd2zzbackend_bvmz00(
					((BgL_bvmz00_bglt) BgL_oz00_563)));
		}

	}



/* bvm-typed-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2typedzd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_62,
		bool_t BgL_vz00_63)
	{
		{	/* BackEnd/bvm.sch 119 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_62)))->BgL_typedz00) =
				((bool_t) BgL_vz00_63), BUNSPEC);
		}

	}



/* &bvm-typed-set! */
	obj_t BGl_z62bvmzd2typedzd2setz12z70zzbackend_bvmz00(obj_t BgL_envz00_564,
		obj_t BgL_oz00_565, obj_t BgL_vz00_566)
	{
		{	/* BackEnd/bvm.sch 119 */
			return
				BGl_bvmzd2typedzd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_565), CBOOL(BgL_vz00_566));
		}

	}



/* bvm-types */
	BGL_EXPORTED_DEF obj_t BGl_bvmzd2typeszd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_64)
	{
		{	/* BackEnd/bvm.sch 120 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_64)))->BgL_typesz00);
		}

	}



/* &bvm-types */
	obj_t BGl_z62bvmzd2typeszb0zzbackend_bvmz00(obj_t BgL_envz00_567,
		obj_t BgL_oz00_568)
	{
		{	/* BackEnd/bvm.sch 120 */
			return
				BGl_bvmzd2typeszd2zzbackend_bvmz00(((BgL_bvmz00_bglt) BgL_oz00_568));
		}

	}



/* bvm-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2typeszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_65,
		obj_t BgL_vz00_66)
	{
		{	/* BackEnd/bvm.sch 121 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_65)))->BgL_typesz00) =
				((obj_t) BgL_vz00_66), BUNSPEC);
		}

	}



/* &bvm-types-set! */
	obj_t BGl_z62bvmzd2typeszd2setz12z70zzbackend_bvmz00(obj_t BgL_envz00_569,
		obj_t BgL_oz00_570, obj_t BgL_vz00_571)
	{
		{	/* BackEnd/bvm.sch 121 */
			return
				BGl_bvmzd2typeszd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_570), BgL_vz00_571);
		}

	}



/* bvm-functions */
	BGL_EXPORTED_DEF obj_t BGl_bvmzd2functionszd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_67)
	{
		{	/* BackEnd/bvm.sch 122 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_67)))->BgL_functionsz00);
		}

	}



/* &bvm-functions */
	obj_t BGl_z62bvmzd2functionszb0zzbackend_bvmz00(obj_t BgL_envz00_572,
		obj_t BgL_oz00_573)
	{
		{	/* BackEnd/bvm.sch 122 */
			return
				BGl_bvmzd2functionszd2zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_573));
		}

	}



/* bvm-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2functionszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_68,
		obj_t BgL_vz00_69)
	{
		{	/* BackEnd/bvm.sch 123 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_68)))->BgL_functionsz00) =
				((obj_t) BgL_vz00_69), BUNSPEC);
		}

	}



/* &bvm-functions-set! */
	obj_t BGl_z62bvmzd2functionszd2setz12z70zzbackend_bvmz00(obj_t BgL_envz00_574,
		obj_t BgL_oz00_575, obj_t BgL_vz00_576)
	{
		{	/* BackEnd/bvm.sch 123 */
			return
				BGl_bvmzd2functionszd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_575), BgL_vz00_576);
		}

	}



/* bvm-variables */
	BGL_EXPORTED_DEF obj_t BGl_bvmzd2variableszd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_70)
	{
		{	/* BackEnd/bvm.sch 124 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_70)))->BgL_variablesz00);
		}

	}



/* &bvm-variables */
	obj_t BGl_z62bvmzd2variableszb0zzbackend_bvmz00(obj_t BgL_envz00_577,
		obj_t BgL_oz00_578)
	{
		{	/* BackEnd/bvm.sch 124 */
			return
				BGl_bvmzd2variableszd2zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_578));
		}

	}



/* bvm-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2variableszd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_71,
		obj_t BgL_vz00_72)
	{
		{	/* BackEnd/bvm.sch 125 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_71)))->BgL_variablesz00) =
				((obj_t) BgL_vz00_72), BUNSPEC);
		}

	}



/* &bvm-variables-set! */
	obj_t BGl_z62bvmzd2variableszd2setz12z70zzbackend_bvmz00(obj_t BgL_envz00_579,
		obj_t BgL_oz00_580, obj_t BgL_vz00_581)
	{
		{	/* BackEnd/bvm.sch 125 */
			return
				BGl_bvmzd2variableszd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_580), BgL_vz00_581);
		}

	}



/* bvm-extern-types */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2externzd2typesz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_73)
	{
		{	/* BackEnd/bvm.sch 126 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_73)))->BgL_externzd2typeszd2);
		}

	}



/* &bvm-extern-types */
	obj_t BGl_z62bvmzd2externzd2typesz62zzbackend_bvmz00(obj_t BgL_envz00_582,
		obj_t BgL_oz00_583)
	{
		{	/* BackEnd/bvm.sch 126 */
			return
				BGl_bvmzd2externzd2typesz00zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_583));
		}

	}



/* bvm-extern-types-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2externzd2typeszd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_74, obj_t BgL_vz00_75)
	{
		{	/* BackEnd/bvm.sch 127 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_74)))->BgL_externzd2typeszd2) =
				((obj_t) BgL_vz00_75), BUNSPEC);
		}

	}



/* &bvm-extern-types-set! */
	obj_t BGl_z62bvmzd2externzd2typeszd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_584, obj_t BgL_oz00_585, obj_t BgL_vz00_586)
	{
		{	/* BackEnd/bvm.sch 127 */
			return
				BGl_bvmzd2externzd2typeszd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_585), BgL_vz00_586);
		}

	}



/* bvm-extern-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2externzd2functionsz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_76)
	{
		{	/* BackEnd/bvm.sch 128 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_76)))->BgL_externzd2functionszd2);
		}

	}



/* &bvm-extern-functions */
	obj_t BGl_z62bvmzd2externzd2functionsz62zzbackend_bvmz00(obj_t BgL_envz00_587,
		obj_t BgL_oz00_588)
	{
		{	/* BackEnd/bvm.sch 128 */
			return
				BGl_bvmzd2externzd2functionsz00zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_588));
		}

	}



/* bvm-extern-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2externzd2functionszd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_77, obj_t BgL_vz00_78)
	{
		{	/* BackEnd/bvm.sch 129 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_77)))->
					BgL_externzd2functionszd2) = ((obj_t) BgL_vz00_78), BUNSPEC);
		}

	}



/* &bvm-extern-functions-set! */
	obj_t BGl_z62bvmzd2externzd2functionszd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_589, obj_t BgL_oz00_590, obj_t BgL_vz00_591)
	{
		{	/* BackEnd/bvm.sch 129 */
			return
				BGl_bvmzd2externzd2functionszd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_590), BgL_vz00_591);
		}

	}



/* bvm-extern-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2externzd2variablesz00zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_79)
	{
		{	/* BackEnd/bvm.sch 130 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_79)))->BgL_externzd2variableszd2);
		}

	}



/* &bvm-extern-variables */
	obj_t BGl_z62bvmzd2externzd2variablesz62zzbackend_bvmz00(obj_t BgL_envz00_592,
		obj_t BgL_oz00_593)
	{
		{	/* BackEnd/bvm.sch 130 */
			return
				BGl_bvmzd2externzd2variablesz00zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_593));
		}

	}



/* bvm-extern-variables-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2externzd2variableszd2setz12zc0zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_80, obj_t BgL_vz00_81)
	{
		{	/* BackEnd/bvm.sch 131 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_80)))->
					BgL_externzd2variableszd2) = ((obj_t) BgL_vz00_81), BUNSPEC);
		}

	}



/* &bvm-extern-variables-set! */
	obj_t BGl_z62bvmzd2externzd2variableszd2setz12za2zzbackend_bvmz00(obj_t
		BgL_envz00_594, obj_t BgL_oz00_595, obj_t BgL_vz00_596)
	{
		{	/* BackEnd/bvm.sch 131 */
			return
				BGl_bvmzd2externzd2variableszd2setz12zc0zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_595), BgL_vz00_596);
		}

	}



/* bvm-name */
	BGL_EXPORTED_DEF obj_t BGl_bvmzd2namezd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_82)
	{
		{	/* BackEnd/bvm.sch 132 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_82)))->BgL_namez00);
		}

	}



/* &bvm-name */
	obj_t BGl_z62bvmzd2namezb0zzbackend_bvmz00(obj_t BgL_envz00_597,
		obj_t BgL_oz00_598)
	{
		{	/* BackEnd/bvm.sch 132 */
			return
				BGl_bvmzd2namezd2zzbackend_bvmz00(((BgL_bvmz00_bglt) BgL_oz00_598));
		}

	}



/* bvm-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2namezd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_83,
		obj_t BgL_vz00_84)
	{
		{	/* BackEnd/bvm.sch 133 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_83)))->BgL_namez00) =
				((obj_t) BgL_vz00_84), BUNSPEC);
		}

	}



/* &bvm-name-set! */
	obj_t BGl_z62bvmzd2namezd2setz12z70zzbackend_bvmz00(obj_t BgL_envz00_599,
		obj_t BgL_oz00_600, obj_t BgL_vz00_601)
	{
		{	/* BackEnd/bvm.sch 133 */
			return
				BGl_bvmzd2namezd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_600), BgL_vz00_601);
		}

	}



/* bvm-srfi0 */
	BGL_EXPORTED_DEF obj_t BGl_bvmzd2srfi0zd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_85)
	{
		{	/* BackEnd/bvm.sch 134 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_85)))->BgL_srfi0z00);
		}

	}



/* &bvm-srfi0 */
	obj_t BGl_z62bvmzd2srfi0zb0zzbackend_bvmz00(obj_t BgL_envz00_602,
		obj_t BgL_oz00_603)
	{
		{	/* BackEnd/bvm.sch 134 */
			return
				BGl_bvmzd2srfi0zd2zzbackend_bvmz00(((BgL_bvmz00_bglt) BgL_oz00_603));
		}

	}



/* bvm-srfi0-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2srfi0zd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_86,
		obj_t BgL_vz00_87)
	{
		{	/* BackEnd/bvm.sch 135 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_86)))->BgL_srfi0z00) =
				((obj_t) BgL_vz00_87), BUNSPEC);
		}

	}



/* &bvm-srfi0-set! */
	obj_t BGl_z62bvmzd2srfi0zd2setz12z70zzbackend_bvmz00(obj_t BgL_envz00_604,
		obj_t BgL_oz00_605, obj_t BgL_vz00_606)
	{
		{	/* BackEnd/bvm.sch 135 */
			return
				BGl_bvmzd2srfi0zd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_605), BgL_vz00_606);
		}

	}



/* bvm-language */
	BGL_EXPORTED_DEF obj_t BGl_bvmzd2languagezd2zzbackend_bvmz00(BgL_bvmz00_bglt
		BgL_oz00_88)
	{
		{	/* BackEnd/bvm.sch 136 */
			return
				(((BgL_backendz00_bglt) COBJECT(
						((BgL_backendz00_bglt) BgL_oz00_88)))->BgL_languagez00);
		}

	}



/* &bvm-language */
	obj_t BGl_z62bvmzd2languagezb0zzbackend_bvmz00(obj_t BgL_envz00_607,
		obj_t BgL_oz00_608)
	{
		{	/* BackEnd/bvm.sch 136 */
			return
				BGl_bvmzd2languagezd2zzbackend_bvmz00(((BgL_bvmz00_bglt) BgL_oz00_608));
		}

	}



/* bvm-language-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bvmzd2languagezd2setz12z12zzbackend_bvmz00(BgL_bvmz00_bglt BgL_oz00_89,
		obj_t BgL_vz00_90)
	{
		{	/* BackEnd/bvm.sch 137 */
			return
				((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_oz00_89)))->BgL_languagez00) =
				((obj_t) BgL_vz00_90), BUNSPEC);
		}

	}



/* &bvm-language-set! */
	obj_t BGl_z62bvmzd2languagezd2setz12z70zzbackend_bvmz00(obj_t BgL_envz00_609,
		obj_t BgL_oz00_610, obj_t BgL_vz00_611)
	{
		{	/* BackEnd/bvm.sch 137 */
			return
				BGl_bvmzd2languagezd2setz12z12zzbackend_bvmz00(
				((BgL_bvmz00_bglt) BgL_oz00_610), BgL_vz00_611);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_bvmz00(void)
	{
		{	/* BackEnd/bvm.scm 15 */
			{	/* BackEnd/bvm.scm 19 */
				obj_t BgL_arg1127z00_316;
				obj_t BgL_arg1129z00_317;

				{	/* BackEnd/bvm.scm 19 */
					obj_t BgL_v1112z00_323;

					BgL_v1112z00_323 = create_vector(0L);
					BgL_arg1127z00_316 = BgL_v1112z00_323;
				}
				{	/* BackEnd/bvm.scm 19 */
					obj_t BgL_v1113z00_324;

					BgL_v1113z00_324 = create_vector(0L);
					BgL_arg1129z00_317 = BgL_v1113z00_324;
				}
				return (BGl_bvmz00zzbackend_bvmz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(0),
						CNST_TABLE_REF(1), BGl_backendz00zzbackend_backendz00, 26995L,
						BFALSE, BGl_proc1141z00zzbackend_bvmz00,
						BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00,
						BGl_proc1140z00zzbackend_bvmz00, BFALSE, BgL_arg1127z00_316,
						BgL_arg1129z00_317), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1132> */
	obj_t BGl_z62zc3z04anonymousza31132ze3ze5zzbackend_bvmz00(obj_t
		BgL_envz00_614, obj_t BgL_new1051z00_615)
	{
		{	/* BackEnd/bvm.scm 19 */
			{
				BgL_bvmz00_bglt BgL_auxz00_984;

				((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt)
									((BgL_bvmz00_bglt) BgL_new1051z00_615))))->BgL_languagez00) =
					((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_namez00) =
					((obj_t) BGl_string1142z00zzbackend_bvmz00), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_externzd2variableszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_externzd2functionszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_externzd2typeszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_variablesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_functionsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_typesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_typedz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_heapzd2suffixzd2) =
					((obj_t) BGl_string1142z00zzbackend_bvmz00), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_heapzd2compatiblezd2) =
					((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_callccz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_qualifiedzd2typeszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_effectzb2zb2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_removezd2emptyzd2letz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_foreignzd2closurezd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_typedzd2eqzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_tracezd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_foreignzd2clausezd2supportz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_debugzd2supportzd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_pragmazd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_requirezd2tailczd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_registersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_pregistersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_boundzd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_typezd2checkzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_typedzd2funcallzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_strictzd2typezd2castz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt)
							COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
										BgL_new1051z00_615))))->BgL_stringzd2literalzd2supportz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_984 = ((BgL_bvmz00_bglt) BgL_new1051z00_615);
				return ((obj_t) BgL_auxz00_984);
			}
		}

	}



/* &lambda1130 */
	BgL_bvmz00_bglt BGl_z62lambda1130z62zzbackend_bvmz00(obj_t BgL_envz00_616)
	{
		{	/* BackEnd/bvm.scm 19 */
			{	/* BackEnd/bvm.scm 19 */
				BgL_bvmz00_bglt BgL_new1050z00_649;

				BgL_new1050z00_649 =
					((BgL_bvmz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct BgL_bvmz00_bgl))));
				{	/* BackEnd/bvm.scm 19 */
					long BgL_arg1131z00_650;

					BgL_arg1131z00_650 = BGL_CLASS_NUM(BGl_bvmz00zzbackend_bvmz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1050z00_649), BgL_arg1131z00_650);
				}
				return BgL_new1050z00_649;
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_bvmz00(void)
	{
		{	/* BackEnd/bvm.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_bvmz00(void)
	{
		{	/* BackEnd/bvm.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2initializa7ez12zd2envzb5zzbackend_backendz00,
				BGl_bvmz00zzbackend_bvmz00, BGl_proc1143z00zzbackend_bvmz00,
				BGl_string1144z00zzbackend_bvmz00);
		}

	}



/* &backend-initialize!-1115 */
	obj_t BGl_z62backendzd2initializa7ez12zd21115zd7zzbackend_bvmz00(obj_t
		BgL_envz00_620, obj_t BgL_bz00_621)
	{
		{	/* BackEnd/bvm.scm 24 */
			((((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt)
								((BgL_bvmz00_bglt) BgL_bz00_621))))->BgL_typedz00) =
				((bool_t) ((bool_t) 1)), BUNSPEC);
			((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
									BgL_bz00_621))))->BgL_callccz00) =
				((bool_t) ((bool_t) 0)), BUNSPEC);
			((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
									BgL_bz00_621))))->BgL_qualifiedzd2typeszd2) =
				((bool_t) ((bool_t) 1)), BUNSPEC);
			((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
									BgL_bz00_621))))->BgL_effectzb2zb2) =
				((bool_t) ((bool_t) 1)), BUNSPEC);
			((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
									BgL_bz00_621))))->BgL_removezd2emptyzd2letz00) =
				((bool_t) ((bool_t) 1)), BUNSPEC);
			((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
									BgL_bz00_621))))->BgL_foreignzd2closurezd2) =
				((bool_t) ((bool_t) 0)), BUNSPEC);
			((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
									BgL_bz00_621))))->BgL_typedzd2eqzd2) =
				((bool_t) ((bool_t) 1)), BUNSPEC);
			((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
									BgL_bz00_621))))->BgL_tracezd2supportzd2) =
				((bool_t) ((bool_t) 1)), BUNSPEC);
			((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
									BgL_bz00_621))))->BgL_pragmazd2supportzd2) =
				((bool_t) ((bool_t) 0)), BUNSPEC);
			((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt)
									BgL_bz00_621))))->BgL_tvectorzd2descrzd2supportz00) =
				((bool_t) ((bool_t) 0)), BUNSPEC);
			return ((((BgL_backendz00_bglt)
						COBJECT(((BgL_backendz00_bglt) ((BgL_bvmz00_bglt) BgL_bz00_621))))->
					BgL_requirezd2tailczd2) = ((bool_t) ((bool_t) 1)), BUNSPEC);
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_bvmz00(void)
	{
		{	/* BackEnd/bvm.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1145z00zzbackend_bvmz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1145z00zzbackend_bvmz00));
		}

	}

#ifdef __cplusplus
}
#endif
