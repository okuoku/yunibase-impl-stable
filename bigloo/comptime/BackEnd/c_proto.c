/*===========================================================================*/
/*   (BackEnd/c_proto.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/c_proto.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_C_PROTOTYPE_TYPE_DEFINITIONS
#define BGL_BACKEND_C_PROTOTYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_cvarz00_bgl
	{
		header_t header;
		obj_t widening;
		bool_t BgL_macrozf3zf3;
	}              *BgL_cvarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_tvecz00_bgl
	{
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}              *BgL_tvecz00_bglt;

	typedef struct BgL_copz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}             *BgL_copz00_bglt;

	typedef struct BgL_cpragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_formatz00;
		obj_t BgL_argsz00;
	}                 *BgL_cpragmaz00_bglt;


#endif													// BGL_BACKEND_C_PROTOTYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_emitzd2cnstzd2stvectorz00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	static obj_t BGl_emitzd2cnstzd2sfunzf2sgfunzf2zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt, obj_t);
	extern obj_t
		BGl_makezd2typedzd2declarationz00zztype_toolsz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbackend_c_prototypez00 =
		BUNSPEC;
	static obj_t
		BGl_emitzd2prototypezf2svarzf2scnstzd2zzbackend_c_prototypez00
		(BgL_valuez00_bglt, obj_t);
	static obj_t
		BGl_z62emitzd2prototypezd2sfun1418z62zzbackend_c_prototypez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2czd2portza2zd2zzbackend_c_emitz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	extern bool_t BGl_widezd2classzf3z21zzobject_classz00(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_typezd2nullzd2valuez00zzbackend_c_prototypez00(BgL_typez00_bglt);
	extern obj_t BGl_za2czd2splitzd2stringza2z00zzengine_paramz00;
	static obj_t
		BGl_getzd2czd2scopez00zzbackend_c_prototypez00(BgL_variablez00_bglt);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_emitzd2cnstzd2llongz00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzbackend_c_prototypez00(void);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_emitzd2cnstzd2selfunz00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzbackend_c_prototypez00(void);
	extern obj_t BGl_getzd2objectzd2typez00zztype_cachez00(void);
	static obj_t BGl_objectzd2initzd2zzbackend_c_prototypez00(void);
	extern obj_t BGl_llongzd2ze3czd2isoze3zzbackend_c_emitz00(BGL_LONGLONG_T);
	static obj_t BGl_z62getzd2czd2scope1424z62zzbackend_c_prototypez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_emitzd2prototypezd2formalsz00zzbackend_c_prototypez00(obj_t);
	static obj_t BGl_emitzd2cnstzd2elongz00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62requirezd2prototypezf3z43zzbackend_c_prototypez00(obj_t,
		obj_t);
	extern bool_t BGl_subzd2typezf3z21zztype_envz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_emitzd2cnstzd2int32z00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_emitzd2classzd2typesz00zzbackend_c_prototypez00(obj_t, obj_t);
	static obj_t BGl_z62emitzd2prototypezb0zzbackend_c_prototypez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_cpragmaz00zzcgen_copz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_emitzd2prototypeszd2zzbackend_c_prototypez00(void);
	static obj_t BGl_z62emitzd2prototypeszb0zzbackend_c_prototypez00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62getzd2czd2scopezd2local1430zb0zzbackend_c_prototypez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_c_prototypez00(void);
	static obj_t BGl_crosszd2namezd2zzbackend_c_prototypez00(obj_t);
	static obj_t BGl_z62getzd2czd2scopez62zzbackend_c_prototypez00(obj_t, obj_t);
	static obj_t BGl_emitzd2slotze70z35zzbackend_c_prototypez00(obj_t, obj_t);
	static obj_t BGl_emitzd2cnstzd2int64z00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_emitzd2tvectorzd2typesz00zztvector_tvectorz00(obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t
		BGl_z62emitzd2prototypezd2svar1414z62zzbackend_c_prototypez00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_funzd2vazd2stackablezf3zf3zzbackend_c_prototypez00
		(BgL_variablez00_bglt);
	extern bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_getzd2staticzd2pragmasz00zzast_pragmaz00(void);
	static obj_t
		BGl_emitzd2prototypezd2zzbackend_c_prototypez00(BgL_valuez00_bglt,
		BgL_variablez00_bglt);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_idzd2ze3namez31zzast_identz00(obj_t);
	static obj_t BGl_z62emitzd2classzd2typesz62zzbackend_c_prototypez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00(BgL_nodez00_bglt);
	extern obj_t BGl_svarz00zzast_varz00;
	static obj_t BGl_z62emitzd2prototype1411zb0zzbackend_c_prototypez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t
		BGl_emitzd2prototypezd2formalzd2typeszd2zzbackend_c_prototypez00(obj_t);
	BGL_IMPORT obj_t BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_untrigraphz00zzbackend_c_emitz00(obj_t);
	extern bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	static obj_t
		BGl_z62emitzd2prototypezd2cfun1420z62zzbackend_c_prototypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_emitzd2cnstzd2stringz00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	BGL_IMPORT obj_t string_for_read(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_emitzd2copzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_copz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_cnstz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_pragmaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern bool_t BGl_emitzd2copzd2zzcgen_emitzd2copzd2(BgL_copz00_bglt);
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t
		BGl_z62zc3z04anonymousza31542ze3ze5zzbackend_c_prototypez00(obj_t, obj_t);
	static obj_t BGl_emitzd2cnstzd2slfunz00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	static obj_t
		BGl_z62getzd2czd2scopezd2global1428zb0zzbackend_c_prototypez00(obj_t,
		obj_t);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzbackend_c_prototypez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_c_prototypez00(void);
	static obj_t
		BGl_z62emitzd2prototypezd2scnst1416z62zzbackend_c_prototypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(BgL_globalz00_bglt);
	BGL_IMPORT long bgl_list_length(obj_t);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_c_prototypez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzbackend_c_prototypez00(void);
	BGL_IMPORT obj_t bgl_display_fixnum(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_emitzd2cnstszd2zzbackend_c_prototypez00(void);
	extern obj_t BGl_typezd2classzd2namez00zzobject_classz00(BgL_typez00_bglt);
	extern obj_t BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00(BgL_typez00_bglt);
	static obj_t BGl_z62emitzd2cnstszb0zzbackend_c_prototypez00(obj_t);
	extern obj_t BGl_getzd2cnstzd2tablez00zzcnst_allocz00(void);
	extern obj_t BGl_valuez00zzast_varz00;
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_emitzd2cnstzd2zzbackend_c_prototypez00(BgL_scnstz00_bglt,
		BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_emitzd2cnstzd2realz00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	extern obj_t BGl_variablez00zzast_varz00;
	extern obj_t BGl_tvectorzd2ze3czd2vectorze3zztvector_cnstz00(obj_t);
	static obj_t BGl_emitzd2cnstzd2uint32z00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	static obj_t BGl_loopze70ze7zzbackend_c_prototypez00(long, obj_t);
	static obj_t BGl_loopze71ze7zzbackend_c_prototypez00(obj_t);
	static obj_t BGl_loopze72ze7zzbackend_c_prototypez00(obj_t);
	extern obj_t
		BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_variablez00_bglt);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t
		BGl_z62emitzd2prototypezd2cvar1423z62zzbackend_c_prototypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_emitzd2cnstzd2uint64z00zzbackend_c_prototypez00(obj_t,
		BgL_globalz00_bglt);
	extern obj_t BGl_cvarz00zzast_varz00;
	static obj_t
		BGl_z62zc3z04anonymousza31578ze3ze5zzbackend_c_prototypez00(obj_t, obj_t);
	static obj_t __cnst[23];


	   
		 
		DEFINE_STRING(BGl_string2300z00zzbackend_c_prototypez00,
		BgL_bgl_string2300za700za7za7b2331za7, "/* object type definitions */", 29);
	      DEFINE_STRING(BGl_string2301z00zzbackend_c_prototypez00,
		BgL_bgl_string2301za700za7za7b2332za7, "   ", 3);
	      DEFINE_STRING(BGl_string2302z00zzbackend_c_prototypez00,
		BgL_bgl_string2302za700za7za7b2333za7, " ", 1);
	      DEFINE_STRING(BGl_string2303z00zzbackend_c_prototypez00,
		BgL_bgl_string2303za700za7za7b2334za7, " *", 2);
	      DEFINE_STRING(BGl_string2305z00zzbackend_c_prototypez00,
		BgL_bgl_string2305za700za7za7b2335za7, "emit-prototype1411", 18);
	      DEFINE_STRING(BGl_string2307z00zzbackend_c_prototypez00,
		BgL_bgl_string2307za700za7za7b2336za7, "get-c-scope1424", 15);
	      DEFINE_STRING(BGl_string2308z00zzbackend_c_prototypez00,
		BgL_bgl_string2308za700za7za7b2337za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2310z00zzbackend_c_prototypez00,
		BgL_bgl_string2310za700za7za7b2338za7, "emit-prototype", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2304z00zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2protot2339z00,
		BGl_z62emitzd2prototype1411zb0zzbackend_c_prototypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2306z00zzbackend_c_prototypez00,
		BgL_bgl_za762getza7d2cza7d2sco2340za7,
		BGl_z62getzd2czd2scope1424z62zzbackend_c_prototypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2309z00zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2protot2341z00,
		BGl_z62emitzd2prototypezd2svar1414z62zzbackend_c_prototypez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2316z00zzbackend_c_prototypez00,
		BgL_bgl_string2316za700za7za7b2342za7, "get-c-scope", 11);
	      DEFINE_STRING(BGl_string2318z00zzbackend_c_prototypez00,
		BgL_bgl_string2318za700za7za7b2343za7, "BGL_IMPORT", 10);
	      DEFINE_STRING(BGl_string2319z00zzbackend_c_prototypez00,
		BgL_bgl_string2319za700za7za7b2344za7, "extern", 6);
	      DEFINE_STRING(BGl_string2239z00zzbackend_c_prototypez00,
		BgL_bgl_string2239za700za7za7b2345za7, "static BGL_THREAD_DECL", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2311z00zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2protot2346z00,
		BGl_z62emitzd2prototypezd2scnst1416z62zzbackend_c_prototypez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2312z00zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2protot2347z00,
		BGl_z62emitzd2prototypezd2sfun1418z62zzbackend_c_prototypez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2313z00zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2protot2348z00,
		BGl_z62emitzd2prototypezd2cfun1420z62zzbackend_c_prototypez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2320z00zzbackend_c_prototypez00,
		BgL_bgl_string2320za700za7za7b2349za7, "BGL_EXPORTED_DECL", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2314z00zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2protot2350z00,
		BGl_z62emitzd2prototypezd2cvar1423z62zzbackend_c_prototypez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2321z00zzbackend_c_prototypez00,
		BgL_bgl_string2321za700za7za7b2351za7, "Unknown importation", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2315z00zzbackend_c_prototypez00,
		BgL_bgl_za762getza7d2cza7d2sco2352za7,
		BGl_z62getzd2czd2scopezd2global1428zb0zzbackend_c_prototypez00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2240z00zzbackend_c_prototypez00,
		BgL_bgl_string2240za700za7za7b2353za7, "static", 6);
	      DEFINE_STRING(BGl_string2322z00zzbackend_c_prototypez00,
		BgL_bgl_string2322za700za7za7b2354za7, " BGL_THREAD_DECL", 16);
	      DEFINE_STRING(BGl_string2241z00zzbackend_c_prototypez00,
		BgL_bgl_string2241za700za7za7b2355za7, "BGL_THREAD_DECL ", 16);
	      DEFINE_STRING(BGl_string2323z00zzbackend_c_prototypez00,
		BgL_bgl_string2323za700za7za7b2356za7, "BGL_IMPORT ", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2317z00zzbackend_c_prototypez00,
		BgL_bgl_za762getza7d2cza7d2sco2357za7,
		BGl_z62getzd2czd2scopezd2local1430zb0zzbackend_c_prototypez00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2242z00zzbackend_c_prototypez00,
		BgL_bgl_string2242za700za7za7b2358za7, "BGL_EXPORTED_DEF ", 17);
	      DEFINE_STRING(BGl_string2324z00zzbackend_c_prototypez00,
		BgL_bgl_string2324za700za7za7b2359za7, "extern ", 7);
	      DEFINE_STRING(BGl_string2243z00zzbackend_c_prototypez00,
		BgL_bgl_string2243za700za7za7b2360za7, ";", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2237z00zzbackend_c_prototypez00,
		BgL_bgl_za762za7c3za704anonymo2361za7,
		BGl_z62zc3z04anonymousza31542ze3ze5zzbackend_c_prototypez00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2325z00zzbackend_c_prototypez00,
		BgL_bgl_string2325za700za7za7b2362za7, "void)", 5);
	      DEFINE_STRING(BGl_string2244z00zzbackend_c_prototypez00,
		BgL_bgl_string2244za700za7za7b2363za7, " = ", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2238z00zzbackend_c_prototypez00,
		BgL_bgl_za762za7c3za704anonymo2364za7,
		BGl_z62zc3z04anonymousza31578ze3ze5zzbackend_c_prototypez00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2326z00zzbackend_c_prototypez00,
		BgL_bgl_string2326za700za7za7b2365za7, ", ...)", 6);
	      DEFINE_STRING(BGl_string2245z00zzbackend_c_prototypez00,
		BgL_bgl_string2245za700za7za7b2366za7, " = BUNSPEC;", 11);
	      DEFINE_STRING(BGl_string2327z00zzbackend_c_prototypez00,
		BgL_bgl_string2327za700za7za7b2367za7, "...)", 4);
	      DEFINE_STRING(BGl_string2246z00zzbackend_c_prototypez00,
		BgL_bgl_string2246za700za7za7b2368za7, "(", 1);
	      DEFINE_STRING(BGl_string2328z00zzbackend_c_prototypez00,
		BgL_bgl_string2328za700za7za7b2369za7, "backend_c_prototype", 19);
	      DEFINE_STRING(BGl_string2247z00zzbackend_c_prototypez00,
		BgL_bgl_string2247za700za7za7b2370za7, ")", 1);
	      DEFINE_STRING(BGl_string2329z00zzbackend_c_prototypez00,
		BgL_bgl_string2329za700za7za7b2371za7,
		"emit-prototype1411 get-c-scope1424 * import stvector slfun selfun sgfun sfun suint64 sint64 suint32 sint32 sllong selong sreal sstring thread-local bigloo-initialized! __param never export static ",
		196);
	      DEFINE_STRING(BGl_string2248z00zzbackend_c_prototypez00,
		BgL_bgl_string2248za700za7za7b2372za7, ", ", 2);
	      DEFINE_STRING(BGl_string2249z00zzbackend_c_prototypez00,
		BgL_bgl_string2249za700za7za7b2373za7, "(void)", 6);
	      DEFINE_STRING(BGl_string2250z00zzbackend_c_prototypez00,
		BgL_bgl_string2250za700za7za7b2374za7, "PROCEDURE", 9);
	      DEFINE_STRING(BGl_string2251z00zzbackend_c_prototypez00,
		BgL_bgl_string2251za700za7za7b2375za7, "GENERIC", 7);
	      DEFINE_STRING(BGl_string2252z00zzbackend_c_prototypez00,
		BgL_bgl_string2252za700za7za7b2376za7, "Unknown cnst class \"~a\"", 23);
	      DEFINE_STRING(BGl_string2253z00zzbackend_c_prototypez00,
		BgL_bgl_string2253za700za7za7b2377za7, "backend:emit-cnst", 17);
	      DEFINE_STRING(BGl_string2254z00zzbackend_c_prototypez00,
		BgL_bgl_string2254za700za7za7b2378za7, "DEFINE_STRING_START( ", 21);
	      DEFINE_STRING(BGl_string2255z00zzbackend_c_prototypez00,
		BgL_bgl_string2255za700za7za7b2379za7, "), ", 3);
	      DEFINE_STRING(BGl_string2256z00zzbackend_c_prototypez00,
		BgL_bgl_string2256za700za7za7b2380za7, "{", 1);
	      DEFINE_STRING(BGl_string2257z00zzbackend_c_prototypez00,
		BgL_bgl_string2257za700za7za7b2381za7, "0 } \n", 5);
	      DEFINE_STRING(BGl_string2258z00zzbackend_c_prototypez00,
		BgL_bgl_string2258za700za7za7b2382za7, "DEFINE_STRING_STOP( ", 20);
	      DEFINE_STRING(BGl_string2259z00zzbackend_c_prototypez00,
		BgL_bgl_string2259za700za7za7b2383za7, ");", 2);
	      DEFINE_STRING(BGl_string2260z00zzbackend_c_prototypez00,
		BgL_bgl_string2260za700za7za7b2384za7, ",", 1);
	      DEFINE_STRING(BGl_string2261z00zzbackend_c_prototypez00,
		BgL_bgl_string2261za700za7za7b2385za7, "DEFINE_STRING( ", 15);
	      DEFINE_STRING(BGl_string2262z00zzbackend_c_prototypez00,
		BgL_bgl_string2262za700za7za7b2386za7, ", \"", 3);
	      DEFINE_STRING(BGl_string2263z00zzbackend_c_prototypez00,
		BgL_bgl_string2263za700za7za7b2387za7, "\", ", 3);
	      DEFINE_STRING(BGl_string2264z00zzbackend_c_prototypez00,
		BgL_bgl_string2264za700za7za7b2388za7, " );", 3);
	      DEFINE_STRING(BGl_string2265z00zzbackend_c_prototypez00,
		BgL_bgl_string2265za700za7za7b2389za7, "emit-cnst-string", 16);
	      DEFINE_STRING(BGl_string2266z00zzbackend_c_prototypez00,
		BgL_bgl_string2266za700za7za7b2390za7, "Can't emit string", 17);
	      DEFINE_STRING(BGl_string2267z00zzbackend_c_prototypez00,
		BgL_bgl_string2267za700za7za7b2391za7, "\"\n\"", 3);
	      DEFINE_STRING(BGl_string2268z00zzbackend_c_prototypez00,
		BgL_bgl_string2268za700za7za7b2392za7, "#define ", 8);
	      DEFINE_STRING(BGl_string2269z00zzbackend_c_prototypez00,
		BgL_bgl_string2269za700za7za7b2393za7, " bigloo_nan", 11);
	      DEFINE_STRING(BGl_string2270z00zzbackend_c_prototypez00,
		BgL_bgl_string2270za700za7za7b2394za7, " bigloo_infinity", 16);
	      DEFINE_STRING(BGl_string2271z00zzbackend_c_prototypez00,
		BgL_bgl_string2271za700za7za7b2395za7, " bigloo_minfinity", 17);
	      DEFINE_STRING(BGl_string2272z00zzbackend_c_prototypez00,
		BgL_bgl_string2272za700za7za7b2396za7, "DEFINE_REAL( ", 13);
	      DEFINE_STRING(BGl_string2273z00zzbackend_c_prototypez00,
		BgL_bgl_string2273za700za7za7b2397za7, "DEFINE_ELONG( ", 14);
	      DEFINE_STRING(BGl_string2274z00zzbackend_c_prototypez00,
		BgL_bgl_string2274za700za7za7b2398za7, "DEFINE_LLONG( ", 14);
	      DEFINE_STRING(BGl_string2275z00zzbackend_c_prototypez00,
		BgL_bgl_string2275za700za7za7b2399za7, "DEFINE_INT32( ", 14);
	      DEFINE_STRING(BGl_string2276z00zzbackend_c_prototypez00,
		BgL_bgl_string2276za700za7za7b2400za7, "DEFINE_UINT32( ", 15);
	      DEFINE_STRING(BGl_string2277z00zzbackend_c_prototypez00,
		BgL_bgl_string2277za700za7za7b2401za7, "DEFINE_INT64( ", 14);
	      DEFINE_STRING(BGl_string2278z00zzbackend_c_prototypez00,
		BgL_bgl_string2278za700za7za7b2402za7, "DEFINE_UINT64( ", 15);
	      DEFINE_STRING(BGl_string2279z00zzbackend_c_prototypez00,
		BgL_bgl_string2279za700za7za7b2403za7, "DEFINE_STATIC_BGL_", 18);
	      DEFINE_STRING(BGl_string2280z00zzbackend_c_prototypez00,
		BgL_bgl_string2280za700za7za7b2404za7, "( ", 2);
	      DEFINE_STRING(BGl_string2281z00zzbackend_c_prototypez00,
		BgL_bgl_string2281za700za7za7b2405za7, "DEFINE_EXPORT_BGL_", 18);
	      DEFINE_STRING(BGl_string2282z00zzbackend_c_prototypez00,
		BgL_bgl_string2282za700za7za7b2406za7, ", opt_generic_entry", 19);
	      DEFINE_STRING(BGl_string2283z00zzbackend_c_prototypez00,
		BgL_bgl_string2283za700za7za7b2407za7, ", BFALSE, ", 10);
	      DEFINE_STRING(BGl_string2284z00zzbackend_c_prototypez00,
		BgL_bgl_string2284za700za7za7b2408za7, ", 0L, BUNSPEC, ", 15);
	      DEFINE_STRING(BGl_string2285z00zzbackend_c_prototypez00,
		BgL_bgl_string2285za700za7za7b2409za7, ", bgl_va_stack_entry", 20);
	      DEFINE_STRING(BGl_string2286z00zzbackend_c_prototypez00,
		BgL_bgl_string2286za700za7za7b2410za7, ", va_generic_entry", 18);
	      DEFINE_STRING(BGl_string2287z00zzbackend_c_prototypez00,
		BgL_bgl_string2287za700za7za7b2411za7, ", BUNSPEC, ", 11);
	      DEFINE_STRING(BGl_string2288z00zzbackend_c_prototypez00,
		BgL_bgl_string2288za700za7za7b2412za7, "static obj_t ", 13);
	      DEFINE_STRING(BGl_string2289z00zzbackend_c_prototypez00,
		BgL_bgl_string2289za700za7za7b2413za7, "DEFINE_BGL_L_PROCEDURE(", 23);
	      DEFINE_STRING(BGl_string2290z00zzbackend_c_prototypez00,
		BgL_bgl_string2290za700za7za7b2414za7, "DEFINE_TVECTOR_START( ", 22);
	      DEFINE_STRING(BGl_string2291z00zzbackend_c_prototypez00,
		BgL_bgl_string2291za700za7za7b2415za7, " ) ", 3);
	      DEFINE_STRING(BGl_string2292z00zzbackend_c_prototypez00,
		BgL_bgl_string2292za700za7za7b2416za7, " DEFINE_TVECTOR_STOP( ", 22);
	      DEFINE_STRING(BGl_string2293z00zzbackend_c_prototypez00,
		BgL_bgl_string2293za700za7za7b2417za7, "typedef ", 8);
	      DEFINE_STRING(BGl_string2294z00zzbackend_c_prototypez00,
		BgL_bgl_string2294za700za7za7b2418za7, " {", 2);
	      DEFINE_STRING(BGl_string2295z00zzbackend_c_prototypez00,
		BgL_bgl_string2295za700za7za7b2419za7, "   char dummy;", 14);
	      DEFINE_STRING(BGl_string2296z00zzbackend_c_prototypez00,
		BgL_bgl_string2296za700za7za7b2420za7, "} *", 3);
	      DEFINE_STRING(BGl_string2297z00zzbackend_c_prototypez00,
		BgL_bgl_string2297za700za7za7b2421za7, ";\n", 2);
	      DEFINE_STRING(BGl_string2298z00zzbackend_c_prototypez00,
		BgL_bgl_string2298za700za7za7b2422za7, "   header_t header;", 19);
	      DEFINE_STRING(BGl_string2299z00zzbackend_c_prototypez00,
		BgL_bgl_string2299za700za7za7b2423za7, "   obj_t widening;", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_requirezd2prototypezf3zd2envzf3zzbackend_c_prototypez00,
		BgL_bgl_za762requireza7d2pro2424z00,
		BGl_z62requirezd2prototypezf3z43zzbackend_c_prototypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2cnstszd2envz00zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2cnstsza72425za7,
		BGl_z62emitzd2cnstszb0zzbackend_c_prototypez00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_emitzd2prototypezd2envz00zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2protot2426z00,
		BGl_z62emitzd2prototypezb0zzbackend_c_prototypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2classzd2typeszd2envzd2zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2classza72427za7,
		BGl_z62emitzd2classzd2typesz62zzbackend_c_prototypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2prototypeszd2envz00zzbackend_c_prototypez00,
		BgL_bgl_za762emitza7d2protot2428z00,
		BGl_z62emitzd2prototypeszb0zzbackend_c_prototypez00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_getzd2czd2scopezd2envzd2zzbackend_c_prototypez00,
		BgL_bgl_za762getza7d2cza7d2sco2429za7,
		BGl_z62getzd2czd2scopez62zzbackend_c_prototypez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzbackend_c_prototypez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long
		BgL_checksumz00_4210, char *BgL_fromz00_4211)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_c_prototypez00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_c_prototypez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_c_prototypez00();
					BGl_libraryzd2moduleszd2initz00zzbackend_c_prototypez00();
					BGl_cnstzd2initzd2zzbackend_c_prototypez00();
					BGl_importedzd2moduleszd2initz00zzbackend_c_prototypez00();
					BGl_genericzd2initzd2zzbackend_c_prototypez00();
					BGl_methodzd2initzd2zzbackend_c_prototypez00();
					return BGl_toplevelzd2initzd2zzbackend_c_prototypez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L,
				"backend_c_prototype");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "backend_c_prototype");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 15 */
			{	/* BackEnd/c_proto.scm 15 */
				obj_t BgL_cportz00_4113;

				{	/* BackEnd/c_proto.scm 15 */
					obj_t BgL_stringz00_4120;

					BgL_stringz00_4120 = BGl_string2329z00zzbackend_c_prototypez00;
					{	/* BackEnd/c_proto.scm 15 */
						obj_t BgL_startz00_4121;

						BgL_startz00_4121 = BINT(0L);
						{	/* BackEnd/c_proto.scm 15 */
							obj_t BgL_endz00_4122;

							BgL_endz00_4122 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4120)));
							{	/* BackEnd/c_proto.scm 15 */

								BgL_cportz00_4113 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4120, BgL_startz00_4121, BgL_endz00_4122);
				}}}}
				{
					long BgL_iz00_4114;

					BgL_iz00_4114 = 22L;
				BgL_loopz00_4115:
					if ((BgL_iz00_4114 == -1L))
						{	/* BackEnd/c_proto.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/c_proto.scm 15 */
							{	/* BackEnd/c_proto.scm 15 */
								obj_t BgL_arg2330z00_4116;

								{	/* BackEnd/c_proto.scm 15 */

									{	/* BackEnd/c_proto.scm 15 */
										obj_t BgL_locationz00_4118;

										BgL_locationz00_4118 = BBOOL(((bool_t) 0));
										{	/* BackEnd/c_proto.scm 15 */

											BgL_arg2330z00_4116 =
												BGl_readz00zz__readerz00(BgL_cportz00_4113,
												BgL_locationz00_4118);
										}
									}
								}
								{	/* BackEnd/c_proto.scm 15 */
									int BgL_tmpz00_4248;

									BgL_tmpz00_4248 = (int) (BgL_iz00_4114);
									CNST_TABLE_SET(BgL_tmpz00_4248, BgL_arg2330z00_4116);
							}}
							{	/* BackEnd/c_proto.scm 15 */
								int BgL_auxz00_4119;

								BgL_auxz00_4119 = (int) ((BgL_iz00_4114 - 1L));
								{
									long BgL_iz00_4253;

									BgL_iz00_4253 = (long) (BgL_auxz00_4119);
									BgL_iz00_4114 = BgL_iz00_4253;
									goto BgL_loopz00_4115;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 15 */
			return BUNSPEC;
		}

	}



/* require-prototype? */
	BGL_EXPORTED_DEF bool_t
		BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(BgL_globalz00_bglt
		BgL_globalz00_13)
	{
		{	/* BackEnd/c_proto.scm 49 */
			{	/* BackEnd/c_proto.scm 50 */
				bool_t BgL_test2432z00_4256;

				{	/* BackEnd/c_proto.scm 50 */
					bool_t BgL__ortest_1139z00_2137;

					{	/* BackEnd/c_proto.scm 50 */
						obj_t BgL_arg1535z00_2141;

						BgL_arg1535z00_2141 =
							(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_13))->BgL_modulez00);
						BgL__ortest_1139z00_2137 =
							(BgL_arg1535z00_2141 == BGl_za2moduleza2z00zzmodule_modulez00);
					}
					if (BgL__ortest_1139z00_2137)
						{	/* BackEnd/c_proto.scm 50 */
							BgL_test2432z00_4256 = BgL__ortest_1139z00_2137;
						}
					else
						{	/* BackEnd/c_proto.scm 50 */
							if (
								((((BgL_globalz00_bglt) COBJECT(BgL_globalz00_13))->
										BgL_importz00) == CNST_TABLE_REF(0)))
								{	/* BackEnd/c_proto.scm 51 */
									BgL_test2432z00_4256 = ((bool_t) 0);
								}
							else
								{	/* BackEnd/c_proto.scm 51 */
									BgL_test2432z00_4256 = ((bool_t) 1);
								}
						}
				}
				if (BgL_test2432z00_4256)
					{	/* BackEnd/c_proto.scm 52 */
						bool_t BgL__ortest_1140z00_2130;

						{	/* BackEnd/c_proto.scm 52 */
							bool_t BgL_test2435z00_4264;

							{	/* BackEnd/c_proto.scm 52 */
								obj_t BgL_arg1513z00_2136;

								BgL_arg1513z00_2136 =
									(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_13))->
									BgL_modulez00);
								BgL_test2435z00_4264 =
									(BgL_arg1513z00_2136 ==
									BGl_za2moduleza2z00zzmodule_modulez00);
							}
							if (BgL_test2435z00_4264)
								{	/* BackEnd/c_proto.scm 52 */
									BgL__ortest_1140z00_2130 =
										(
										(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_13))->
											BgL_importz00) == CNST_TABLE_REF(1));
								}
							else
								{	/* BackEnd/c_proto.scm 52 */
									BgL__ortest_1140z00_2130 = ((bool_t) 0);
								}
						}
						if (BgL__ortest_1140z00_2130)
							{	/* BackEnd/c_proto.scm 52 */
								return BgL__ortest_1140z00_2130;
							}
						else
							{	/* BackEnd/c_proto.scm 54 */
								bool_t BgL__ortest_1141z00_2131;

								BgL__ortest_1141z00_2131 =
									(
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_globalz00_13)))->
										BgL_occurrencez00) > 0L);
								if (BgL__ortest_1141z00_2131)
									{	/* BackEnd/c_proto.scm 54 */
										return BgL__ortest_1141z00_2131;
									}
								else
									{	/* BackEnd/c_proto.scm 54 */
										return
											(
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_globalz00_13)))->
												BgL_removablez00) == CNST_TABLE_REF(2));
									}
							}
					}
				else
					{	/* BackEnd/c_proto.scm 50 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &require-prototype? */
	obj_t BGl_z62requirezd2prototypezf3z43zzbackend_c_prototypez00(obj_t
		BgL_envz00_4059, obj_t BgL_globalz00_4060)
	{
		{	/* BackEnd/c_proto.scm 49 */
			return
				BBOOL(BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(
					((BgL_globalz00_bglt) BgL_globalz00_4060)));
		}

	}



/* emit-prototypes */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2prototypeszd2zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 60 */
			{	/* BackEnd/c_proto.scm 63 */
				obj_t BgL_initz00_2142;

				{	/* BackEnd/c_proto.scm 63 */
					obj_t BgL_list1536z00_2143;

					BgL_list1536z00_2143 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
					BgL_initz00_2142 =
						BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(4),
						BgL_list1536z00_2143);
				}
				if (CBOOL(BgL_initz00_2142))
					{	/* BackEnd/c_proto.scm 64 */
						BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
							((BgL_variablez00_bglt) BgL_initz00_2142));
					}
				else
					{	/* BackEnd/c_proto.scm 64 */
						BFALSE;
					}
			}
			BGl_forzd2eachzd2globalz12z12zzast_envz00
				(BGl_proc2237z00zzbackend_c_prototypez00, BNIL);
			{	/* BackEnd/c_proto.scm 74 */
				obj_t BgL_cnstzd2initzd2_2158;

				BgL_cnstzd2initzd2_2158 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
				{	/* BackEnd/c_proto.scm 75 */
					BgL_valuez00_bglt BgL_arg1564z00_2159;

					BgL_arg1564z00_2159 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_cnstzd2initzd2_2158))))->
						BgL_valuez00);
					BGl_emitzd2prototypezd2zzbackend_c_prototypez00(BgL_arg1564z00_2159,
						((BgL_variablez00_bglt) BgL_cnstzd2initzd2_2158));
				}
			}
			{	/* BackEnd/c_proto.scm 77 */
				obj_t BgL_pragmasz00_2160;

				BgL_pragmasz00_2160 = BGl_getzd2staticzd2pragmasz00zzast_pragmaz00();
				if (NULLP(BgL_pragmasz00_2160))
					{	/* BackEnd/c_proto.scm 78 */
						BFALSE;
					}
				else
					{	/* BackEnd/c_proto.scm 78 */
						{
							obj_t BgL_l1365z00_2163;

							BgL_l1365z00_2163 = BgL_pragmasz00_2160;
						BgL_zc3z04anonymousza31566ze3z87_2164:
							if (PAIRP(BgL_l1365z00_2163))
								{	/* BackEnd/c_proto.scm 79 */
									{	/* BackEnd/c_proto.scm 80 */
										BgL_pragmaz00_bglt BgL_pz00_2166;

										BgL_pz00_2166 =
											((BgL_pragmaz00_bglt) CAR(BgL_l1365z00_2163));
										{	/* BackEnd/c_proto.scm 80 */
											BgL_cpragmaz00_bglt BgL_arg1571z00_2167;

											{	/* BackEnd/c_proto.scm 80 */
												BgL_cpragmaz00_bglt BgL_new1144z00_2168;

												{	/* BackEnd/c_proto.scm 83 */
													BgL_cpragmaz00_bglt BgL_new1143z00_2169;

													BgL_new1143z00_2169 =
														((BgL_cpragmaz00_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_cpragmaz00_bgl))));
													{	/* BackEnd/c_proto.scm 83 */
														long BgL_arg1573z00_2170;

														BgL_arg1573z00_2170 =
															BGL_CLASS_NUM(BGl_cpragmaz00zzcgen_copz00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1143z00_2169),
															BgL_arg1573z00_2170);
													}
													BgL_new1144z00_2168 = BgL_new1143z00_2169;
												}
												((((BgL_copz00_bglt) COBJECT(
																((BgL_copz00_bglt) BgL_new1144z00_2168)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) BgL_pz00_2166)))->
															BgL_locz00)), BUNSPEC);
												((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																	BgL_new1144z00_2168)))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
												((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1144z00_2168))->
														BgL_formatz00) =
													((obj_t) (((BgL_pragmaz00_bglt)
																COBJECT(BgL_pz00_2166))->BgL_formatz00)),
													BUNSPEC);
												((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1144z00_2168))->
														BgL_argsz00) =
													((obj_t) (((BgL_externz00_bglt)
																COBJECT(((BgL_externz00_bglt) BgL_pz00_2166)))->
															BgL_exprza2za2)), BUNSPEC);
												BgL_arg1571z00_2167 = BgL_new1144z00_2168;
											}
											BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
												((BgL_copz00_bglt) BgL_arg1571z00_2167));
									}}
									{
										obj_t BgL_l1365z00_4322;

										BgL_l1365z00_4322 = CDR(BgL_l1365z00_2163);
										BgL_l1365z00_2163 = BgL_l1365z00_4322;
										goto BgL_zc3z04anonymousza31566ze3z87_2164;
									}
								}
							else
								{	/* BackEnd/c_proto.scm 79 */
									((bool_t) 1);
								}
						}
						bgl_display_char(((unsigned char) 10),
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			}}
			BGl_emitzd2tvectorzd2typesz00zztvector_tvectorz00
				(BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			return bgl_display_char(((unsigned char) 10),
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
		}

	}



/* &emit-prototypes */
	obj_t BGl_z62emitzd2prototypeszb0zzbackend_c_prototypez00(obj_t
		BgL_envz00_4062)
	{
		{	/* BackEnd/c_proto.scm 60 */
			return BGl_emitzd2prototypeszd2zzbackend_c_prototypez00();
		}

	}



/* &<@anonymous:1542> */
	obj_t BGl_z62zc3z04anonymousza31542ze3ze5zzbackend_c_prototypez00(obj_t
		BgL_envz00_4063, obj_t BgL_globalz00_4064)
	{
		{	/* BackEnd/c_proto.scm 67 */
			{	/* BackEnd/c_proto.scm 68 */
				bool_t BgL_test2441z00_4328;

				if (BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(
						((BgL_globalz00_bglt) BgL_globalz00_4064)))
					{	/* BackEnd/c_proto.scm 69 */
						bool_t BgL_test2443z00_4332;

						{	/* BackEnd/c_proto.scm 69 */
							BgL_valuez00_bglt BgL_arg1561z00_4124;

							BgL_arg1561z00_4124 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt) BgL_globalz00_4064))))->
								BgL_valuez00);
							{	/* BackEnd/c_proto.scm 69 */
								obj_t BgL_classz00_4125;

								BgL_classz00_4125 = BGl_scnstz00zzast_varz00;
								{	/* BackEnd/c_proto.scm 69 */
									BgL_objectz00_bglt BgL_arg1807z00_4126;

									{	/* BackEnd/c_proto.scm 69 */
										obj_t BgL_tmpz00_4336;

										BgL_tmpz00_4336 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1561z00_4124));
										BgL_arg1807z00_4126 =
											(BgL_objectz00_bglt) (BgL_tmpz00_4336);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* BackEnd/c_proto.scm 69 */
											long BgL_idxz00_4127;

											BgL_idxz00_4127 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4126);
											BgL_test2443z00_4332 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4127 + 2L)) == BgL_classz00_4125);
										}
									else
										{	/* BackEnd/c_proto.scm 69 */
											bool_t BgL_res2211z00_4130;

											{	/* BackEnd/c_proto.scm 69 */
												obj_t BgL_oclassz00_4131;

												{	/* BackEnd/c_proto.scm 69 */
													obj_t BgL_arg1815z00_4132;
													long BgL_arg1816z00_4133;

													BgL_arg1815z00_4132 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* BackEnd/c_proto.scm 69 */
														long BgL_arg1817z00_4134;

														BgL_arg1817z00_4134 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4126);
														BgL_arg1816z00_4133 =
															(BgL_arg1817z00_4134 - OBJECT_TYPE);
													}
													BgL_oclassz00_4131 =
														VECTOR_REF(BgL_arg1815z00_4132,
														BgL_arg1816z00_4133);
												}
												{	/* BackEnd/c_proto.scm 69 */
													bool_t BgL__ortest_1115z00_4135;

													BgL__ortest_1115z00_4135 =
														(BgL_classz00_4125 == BgL_oclassz00_4131);
													if (BgL__ortest_1115z00_4135)
														{	/* BackEnd/c_proto.scm 69 */
															BgL_res2211z00_4130 = BgL__ortest_1115z00_4135;
														}
													else
														{	/* BackEnd/c_proto.scm 69 */
															long BgL_odepthz00_4136;

															{	/* BackEnd/c_proto.scm 69 */
																obj_t BgL_arg1804z00_4137;

																BgL_arg1804z00_4137 = (BgL_oclassz00_4131);
																BgL_odepthz00_4136 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4137);
															}
															if ((2L < BgL_odepthz00_4136))
																{	/* BackEnd/c_proto.scm 69 */
																	obj_t BgL_arg1802z00_4138;

																	{	/* BackEnd/c_proto.scm 69 */
																		obj_t BgL_arg1803z00_4139;

																		BgL_arg1803z00_4139 = (BgL_oclassz00_4131);
																		BgL_arg1802z00_4138 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4139, 2L);
																	}
																	BgL_res2211z00_4130 =
																		(BgL_arg1802z00_4138 == BgL_classz00_4125);
																}
															else
																{	/* BackEnd/c_proto.scm 69 */
																	BgL_res2211z00_4130 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2443z00_4332 = BgL_res2211z00_4130;
										}
								}
							}
						}
						if (BgL_test2443z00_4332)
							{	/* BackEnd/c_proto.scm 69 */
								BgL_test2441z00_4328 = ((bool_t) 0);
							}
						else
							{	/* BackEnd/c_proto.scm 69 */
								BgL_test2441z00_4328 = ((bool_t) 1);
							}
					}
				else
					{	/* BackEnd/c_proto.scm 68 */
						BgL_test2441z00_4328 = ((bool_t) 0);
					}
				if (BgL_test2441z00_4328)
					{	/* BackEnd/c_proto.scm 70 */
						BgL_valuez00_bglt BgL_arg1559z00_4140;

						BgL_arg1559z00_4140 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_globalz00_4064))))->BgL_valuez00);
						return
							BGl_emitzd2prototypezd2zzbackend_c_prototypez00
							(BgL_arg1559z00_4140,
							((BgL_variablez00_bglt) BgL_globalz00_4064));
					}
				else
					{	/* BackEnd/c_proto.scm 68 */
						return BFALSE;
					}
			}
		}

	}



/* emit-cnsts */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2cnstszd2zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 95 */
			BGl_forzd2eachzd2globalz12z12zzast_envz00
				(BGl_proc2238z00zzbackend_c_prototypez00, BNIL);
			return bgl_display_char(((unsigned char) 10),
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
		}

	}



/* &emit-cnsts */
	obj_t BGl_z62emitzd2cnstszb0zzbackend_c_prototypez00(obj_t BgL_envz00_4066)
	{
		{	/* BackEnd/c_proto.scm 95 */
			return BGl_emitzd2cnstszd2zzbackend_c_prototypez00();
		}

	}



/* &<@anonymous:1578> */
	obj_t BGl_z62zc3z04anonymousza31578ze3ze5zzbackend_c_prototypez00(obj_t
		BgL_envz00_4067, obj_t BgL_globalz00_4068)
	{
		{	/* BackEnd/c_proto.scm 97 */
			{	/* BackEnd/c_proto.scm 98 */
				bool_t BgL_test2447z00_4367;

				if (BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(
						((BgL_globalz00_bglt) BgL_globalz00_4068)))
					{	/* BackEnd/c_proto.scm 99 */
						BgL_valuez00_bglt BgL_arg1591z00_4141;

						BgL_arg1591z00_4141 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_globalz00_4068))))->BgL_valuez00);
						{	/* BackEnd/c_proto.scm 99 */
							obj_t BgL_classz00_4142;

							BgL_classz00_4142 = BGl_scnstz00zzast_varz00;
							{	/* BackEnd/c_proto.scm 99 */
								BgL_objectz00_bglt BgL_arg1807z00_4143;

								{	/* BackEnd/c_proto.scm 99 */
									obj_t BgL_tmpz00_4374;

									BgL_tmpz00_4374 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1591z00_4141));
									BgL_arg1807z00_4143 = (BgL_objectz00_bglt) (BgL_tmpz00_4374);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* BackEnd/c_proto.scm 99 */
										long BgL_idxz00_4144;

										BgL_idxz00_4144 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4143);
										BgL_test2447z00_4367 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4144 + 2L)) == BgL_classz00_4142);
									}
								else
									{	/* BackEnd/c_proto.scm 99 */
										bool_t BgL_res2212z00_4147;

										{	/* BackEnd/c_proto.scm 99 */
											obj_t BgL_oclassz00_4148;

											{	/* BackEnd/c_proto.scm 99 */
												obj_t BgL_arg1815z00_4149;
												long BgL_arg1816z00_4150;

												BgL_arg1815z00_4149 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* BackEnd/c_proto.scm 99 */
													long BgL_arg1817z00_4151;

													BgL_arg1817z00_4151 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4143);
													BgL_arg1816z00_4150 =
														(BgL_arg1817z00_4151 - OBJECT_TYPE);
												}
												BgL_oclassz00_4148 =
													VECTOR_REF(BgL_arg1815z00_4149, BgL_arg1816z00_4150);
											}
											{	/* BackEnd/c_proto.scm 99 */
												bool_t BgL__ortest_1115z00_4152;

												BgL__ortest_1115z00_4152 =
													(BgL_classz00_4142 == BgL_oclassz00_4148);
												if (BgL__ortest_1115z00_4152)
													{	/* BackEnd/c_proto.scm 99 */
														BgL_res2212z00_4147 = BgL__ortest_1115z00_4152;
													}
												else
													{	/* BackEnd/c_proto.scm 99 */
														long BgL_odepthz00_4153;

														{	/* BackEnd/c_proto.scm 99 */
															obj_t BgL_arg1804z00_4154;

															BgL_arg1804z00_4154 = (BgL_oclassz00_4148);
															BgL_odepthz00_4153 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4154);
														}
														if ((2L < BgL_odepthz00_4153))
															{	/* BackEnd/c_proto.scm 99 */
																obj_t BgL_arg1802z00_4155;

																{	/* BackEnd/c_proto.scm 99 */
																	obj_t BgL_arg1803z00_4156;

																	BgL_arg1803z00_4156 = (BgL_oclassz00_4148);
																	BgL_arg1802z00_4155 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4156,
																		2L);
																}
																BgL_res2212z00_4147 =
																	(BgL_arg1802z00_4155 == BgL_classz00_4142);
															}
														else
															{	/* BackEnd/c_proto.scm 99 */
																BgL_res2212z00_4147 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2447z00_4367 = BgL_res2212z00_4147;
									}
							}
						}
					}
				else
					{	/* BackEnd/c_proto.scm 98 */
						BgL_test2447z00_4367 = ((bool_t) 0);
					}
				if (BgL_test2447z00_4367)
					{	/* BackEnd/c_proto.scm 100 */
						BgL_valuez00_bglt BgL_arg1589z00_4157;

						BgL_arg1589z00_4157 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_globalz00_4068))))->BgL_valuez00);
						return
							BGl_emitzd2cnstzd2zzbackend_c_prototypez00(
							((BgL_scnstz00_bglt) BgL_arg1589z00_4157),
							((BgL_globalz00_bglt) BgL_globalz00_4068));
					}
				else
					{	/* BackEnd/c_proto.scm 98 */
						return BFALSE;
					}
			}
		}

	}



/* emit-prototype/svar/scnst */
	obj_t
		BGl_emitzd2prototypezf2svarzf2scnstzd2zzbackend_c_prototypez00
		(BgL_valuez00_bglt BgL_valuez00_20, obj_t BgL_variablez00_21)
	{
		{	/* BackEnd/c_proto.scm 123 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_variablez00_21));
			if (
				((((BgL_globalz00_bglt) COBJECT(
								((BgL_globalz00_bglt) BgL_variablez00_21)))->BgL_importz00) ==
					CNST_TABLE_REF(0)))
				{	/* BackEnd/c_proto.scm 128 */
					obj_t BgL_port1367z00_2187;

					BgL_port1367z00_2187 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_proto.scm 129 */
						obj_t BgL_arg1595z00_2188;

						if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF
									(5),
									(((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
													BgL_variablez00_21)))->BgL_pragmaz00))))
							{	/* BackEnd/c_proto.scm 129 */
								BgL_arg1595z00_2188 = BGl_string2239z00zzbackend_c_prototypez00;
							}
						else
							{	/* BackEnd/c_proto.scm 129 */
								BgL_arg1595z00_2188 = BGl_string2240z00zzbackend_c_prototypez00;
							}
						bgl_display_obj(BgL_arg1595z00_2188, BgL_port1367z00_2187);
					}
					{	/* BackEnd/c_proto.scm 128 */
						obj_t BgL_tmpz00_4417;

						BgL_tmpz00_4417 = ((obj_t) BgL_port1367z00_2187);
						bgl_display_char(((unsigned char) ' '), BgL_tmpz00_4417);
					}
					{	/* BackEnd/c_proto.scm 133 */
						obj_t BgL_arg1606z00_2192;

						{	/* BackEnd/c_proto.scm 133 */
							BgL_typez00_bglt BgL_arg1609z00_2193;
							obj_t BgL_arg1611z00_2194;

							BgL_arg1609z00_2193 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_variablez00_21)))->BgL_typez00);
							BgL_arg1611z00_2194 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_variablez00_21)))->BgL_namez00);
							BgL_arg1606z00_2192 =
								BGl_makezd2typedzd2declarationz00zztype_toolsz00
								(BgL_arg1609z00_2193, BgL_arg1611z00_2194);
						}
						bgl_display_obj(BgL_arg1606z00_2192, BgL_port1367z00_2187);
					}
					{	/* BackEnd/c_proto.scm 134 */
						obj_t BgL_arg1613z00_2195;

						{	/* BackEnd/c_proto.scm 134 */
							BgL_typez00_bglt BgL_arg1615z00_2196;

							BgL_arg1615z00_2196 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_variablez00_21)))->BgL_typez00);
							BgL_arg1613z00_2195 =
								BGl_typezd2nullzd2valuez00zzbackend_c_prototypez00
								(BgL_arg1615z00_2196);
						}
						bgl_display_obj(BgL_arg1613z00_2195, BgL_port1367z00_2187);
					}
					{	/* BackEnd/c_proto.scm 128 */
						obj_t BgL_tmpz00_4430;

						BgL_tmpz00_4430 = ((obj_t) BgL_port1367z00_2187);
						return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4430);
				}}
			else
				{	/* BackEnd/c_proto.scm 127 */
					if (
						((((BgL_globalz00_bglt) COBJECT(
										((BgL_globalz00_bglt) BgL_variablez00_21)))->
								BgL_importz00) == CNST_TABLE_REF(1)))
						{	/* BackEnd/c_proto.scm 136 */
							obj_t BgL_port1368z00_2199;

							BgL_port1368z00_2199 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
							{	/* BackEnd/c_proto.scm 137 */
								obj_t BgL_arg1625z00_2200;

								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
										(CNST_TABLE_REF(5),
											(((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
															BgL_variablez00_21)))->BgL_pragmaz00))))
									{	/* BackEnd/c_proto.scm 137 */
										BgL_arg1625z00_2200 =
											BGl_string2241z00zzbackend_c_prototypez00;
									}
								else
									{	/* BackEnd/c_proto.scm 137 */
										BgL_arg1625z00_2200 =
											BGl_string2242z00zzbackend_c_prototypez00;
									}
								bgl_display_obj(BgL_arg1625z00_2200, BgL_port1368z00_2199);
							}
							{	/* BackEnd/c_proto.scm 140 */
								obj_t BgL_arg1630z00_2204;

								{	/* BackEnd/c_proto.scm 140 */
									BgL_typez00_bglt BgL_arg1642z00_2205;
									obj_t BgL_arg1646z00_2206;

									BgL_arg1642z00_2205 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_variablez00_21)))->
										BgL_typez00);
									BgL_arg1646z00_2206 =
										(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
													BgL_variablez00_21)))->BgL_namez00);
									BgL_arg1630z00_2204 =
										BGl_makezd2typedzd2declarationz00zztype_toolsz00
										(BgL_arg1642z00_2205, BgL_arg1646z00_2206);
								}
								bgl_display_obj(BgL_arg1630z00_2204, BgL_port1368z00_2199);
							}
							{	/* BackEnd/c_proto.scm 141 */
								obj_t BgL_arg1650z00_2207;

								{	/* BackEnd/c_proto.scm 141 */
									BgL_typez00_bglt BgL_arg1651z00_2208;

									BgL_arg1651z00_2208 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_variablez00_21)))->
										BgL_typez00);
									BgL_arg1650z00_2207 =
										BGl_typezd2nullzd2valuez00zzbackend_c_prototypez00
										(BgL_arg1651z00_2208);
								}
								bgl_display_obj(BgL_arg1650z00_2207, BgL_port1368z00_2199);
							}
							{	/* BackEnd/c_proto.scm 136 */
								obj_t BgL_tmpz00_4455;

								BgL_tmpz00_4455 = ((obj_t) BgL_port1368z00_2199);
								return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4455);
						}}
					else
						{	/* BackEnd/c_proto.scm 143 */
							obj_t BgL_port1369z00_2209;

							BgL_port1369z00_2209 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
							{	/* BackEnd/c_proto.scm 144 */
								obj_t BgL_arg1654z00_2210;

								BgL_arg1654z00_2210 =
									BGl_getzd2czd2scopez00zzbackend_c_prototypez00(
									((BgL_variablez00_bglt) BgL_variablez00_21));
								bgl_display_obj(BgL_arg1654z00_2210, BgL_port1369z00_2209);
							}
							{	/* BackEnd/c_proto.scm 143 */
								obj_t BgL_tmpz00_4461;

								BgL_tmpz00_4461 = ((obj_t) BgL_port1369z00_2209);
								bgl_display_char(((unsigned char) ' '), BgL_tmpz00_4461);
							}
							{	/* BackEnd/c_proto.scm 146 */
								obj_t BgL_arg1661z00_2211;

								{	/* BackEnd/c_proto.scm 146 */
									BgL_typez00_bglt BgL_arg1663z00_2212;
									obj_t BgL_arg1675z00_2213;

									BgL_arg1663z00_2212 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_variablez00_21)))->
										BgL_typez00);
									BgL_arg1675z00_2213 =
										(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
													BgL_variablez00_21)))->BgL_namez00);
									BgL_arg1661z00_2211 =
										BGl_makezd2typedzd2declarationz00zztype_toolsz00
										(BgL_arg1663z00_2212, BgL_arg1675z00_2213);
								}
								bgl_display_obj(BgL_arg1661z00_2211, BgL_port1369z00_2209);
							}
							{	/* BackEnd/c_proto.scm 143 */
								obj_t BgL_tmpz00_4470;

								BgL_tmpz00_4470 = ((obj_t) BgL_port1369z00_2209);
								bgl_display_string(BGl_string2243z00zzbackend_c_prototypez00,
									BgL_tmpz00_4470);
							}
							{	/* BackEnd/c_proto.scm 143 */
								obj_t BgL_tmpz00_4473;

								BgL_tmpz00_4473 = ((obj_t) BgL_port1369z00_2209);
								return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4473);
		}}}}

	}



/* type-null-value */
	obj_t BGl_typezd2nullzd2valuez00zzbackend_c_prototypez00(BgL_typez00_bglt
		BgL_tz00_22)
	{
		{	/* BackEnd/c_proto.scm 152 */
			{
				BgL_typez00_bglt BgL_tz00_2227;
				BgL_typez00_bglt BgL_tz00_2219;

				if (BGl_subzd2typezf3z21zztype_envz00(BgL_tz00_22,
						((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00)))
					{	/* BackEnd/c_proto.scm 166 */
						BgL_tz00_2219 = BgL_tz00_22;
						{	/* BackEnd/c_proto.scm 156 */
							bool_t BgL_test2457z00_4479;

							{	/* BackEnd/c_proto.scm 156 */
								obj_t BgL_arg1699z00_2226;

								BgL_arg1699z00_2226 =
									(((BgL_typez00_bglt) COBJECT(BgL_tz00_2219))->BgL_nullz00);
								{	/* BackEnd/c_proto.scm 156 */
									obj_t BgL_classz00_3104;

									BgL_classz00_3104 = BGl_globalz00zzast_varz00;
									if (BGL_OBJECTP(BgL_arg1699z00_2226))
										{	/* BackEnd/c_proto.scm 156 */
											BgL_objectz00_bglt BgL_arg1807z00_3106;

											BgL_arg1807z00_3106 =
												(BgL_objectz00_bglt) (BgL_arg1699z00_2226);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* BackEnd/c_proto.scm 156 */
													long BgL_idxz00_3112;

													BgL_idxz00_3112 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3106);
													BgL_test2457z00_4479 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3112 + 2L)) == BgL_classz00_3104);
												}
											else
												{	/* BackEnd/c_proto.scm 156 */
													bool_t BgL_res2213z00_3137;

													{	/* BackEnd/c_proto.scm 156 */
														obj_t BgL_oclassz00_3120;

														{	/* BackEnd/c_proto.scm 156 */
															obj_t BgL_arg1815z00_3128;
															long BgL_arg1816z00_3129;

															BgL_arg1815z00_3128 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* BackEnd/c_proto.scm 156 */
																long BgL_arg1817z00_3130;

																BgL_arg1817z00_3130 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3106);
																BgL_arg1816z00_3129 =
																	(BgL_arg1817z00_3130 - OBJECT_TYPE);
															}
															BgL_oclassz00_3120 =
																VECTOR_REF(BgL_arg1815z00_3128,
																BgL_arg1816z00_3129);
														}
														{	/* BackEnd/c_proto.scm 156 */
															bool_t BgL__ortest_1115z00_3121;

															BgL__ortest_1115z00_3121 =
																(BgL_classz00_3104 == BgL_oclassz00_3120);
															if (BgL__ortest_1115z00_3121)
																{	/* BackEnd/c_proto.scm 156 */
																	BgL_res2213z00_3137 =
																		BgL__ortest_1115z00_3121;
																}
															else
																{	/* BackEnd/c_proto.scm 156 */
																	long BgL_odepthz00_3122;

																	{	/* BackEnd/c_proto.scm 156 */
																		obj_t BgL_arg1804z00_3123;

																		BgL_arg1804z00_3123 = (BgL_oclassz00_3120);
																		BgL_odepthz00_3122 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3123);
																	}
																	if ((2L < BgL_odepthz00_3122))
																		{	/* BackEnd/c_proto.scm 156 */
																			obj_t BgL_arg1802z00_3125;

																			{	/* BackEnd/c_proto.scm 156 */
																				obj_t BgL_arg1803z00_3126;

																				BgL_arg1803z00_3126 =
																					(BgL_oclassz00_3120);
																				BgL_arg1802z00_3125 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3126, 2L);
																			}
																			BgL_res2213z00_3137 =
																				(BgL_arg1802z00_3125 ==
																				BgL_classz00_3104);
																		}
																	else
																		{	/* BackEnd/c_proto.scm 156 */
																			BgL_res2213z00_3137 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2457z00_4479 = BgL_res2213z00_3137;
												}
										}
									else
										{	/* BackEnd/c_proto.scm 156 */
											BgL_test2457z00_4479 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2457z00_4479)
								{	/* BackEnd/c_proto.scm 157 */
									obj_t BgL_arg1691z00_2224;

									BgL_arg1691z00_2224 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													(((BgL_typez00_bglt) COBJECT(BgL_tz00_2219))->
														BgL_nullz00))))->BgL_namez00);
									return
										string_append_3(BGl_string2244z00zzbackend_c_prototypez00,
										BgL_arg1691z00_2224,
										BGl_string2243z00zzbackend_c_prototypez00);
								}
							else
								{	/* BackEnd/c_proto.scm 156 */
									return BGl_string2245z00zzbackend_c_prototypez00;
								}
						}
					}
				else
					{	/* BackEnd/c_proto.scm 166 */
						BgL_tz00_2227 = BgL_tz00_22;
						{	/* BackEnd/c_proto.scm 162 */
							bool_t BgL_test2462z00_4507;

							{	/* BackEnd/c_proto.scm 162 */
								obj_t BgL_arg1708z00_2234;

								BgL_arg1708z00_2234 =
									(((BgL_typez00_bglt) COBJECT(BgL_tz00_2227))->BgL_nullz00);
								{	/* BackEnd/c_proto.scm 162 */
									obj_t BgL_classz00_3139;

									BgL_classz00_3139 = BGl_globalz00zzast_varz00;
									if (BGL_OBJECTP(BgL_arg1708z00_2234))
										{	/* BackEnd/c_proto.scm 162 */
											BgL_objectz00_bglt BgL_arg1807z00_3141;

											BgL_arg1807z00_3141 =
												(BgL_objectz00_bglt) (BgL_arg1708z00_2234);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* BackEnd/c_proto.scm 162 */
													long BgL_idxz00_3147;

													BgL_idxz00_3147 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3141);
													BgL_test2462z00_4507 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3147 + 2L)) == BgL_classz00_3139);
												}
											else
												{	/* BackEnd/c_proto.scm 162 */
													bool_t BgL_res2214z00_3172;

													{	/* BackEnd/c_proto.scm 162 */
														obj_t BgL_oclassz00_3155;

														{	/* BackEnd/c_proto.scm 162 */
															obj_t BgL_arg1815z00_3163;
															long BgL_arg1816z00_3164;

															BgL_arg1815z00_3163 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* BackEnd/c_proto.scm 162 */
																long BgL_arg1817z00_3165;

																BgL_arg1817z00_3165 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3141);
																BgL_arg1816z00_3164 =
																	(BgL_arg1817z00_3165 - OBJECT_TYPE);
															}
															BgL_oclassz00_3155 =
																VECTOR_REF(BgL_arg1815z00_3163,
																BgL_arg1816z00_3164);
														}
														{	/* BackEnd/c_proto.scm 162 */
															bool_t BgL__ortest_1115z00_3156;

															BgL__ortest_1115z00_3156 =
																(BgL_classz00_3139 == BgL_oclassz00_3155);
															if (BgL__ortest_1115z00_3156)
																{	/* BackEnd/c_proto.scm 162 */
																	BgL_res2214z00_3172 =
																		BgL__ortest_1115z00_3156;
																}
															else
																{	/* BackEnd/c_proto.scm 162 */
																	long BgL_odepthz00_3157;

																	{	/* BackEnd/c_proto.scm 162 */
																		obj_t BgL_arg1804z00_3158;

																		BgL_arg1804z00_3158 = (BgL_oclassz00_3155);
																		BgL_odepthz00_3157 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3158);
																	}
																	if ((2L < BgL_odepthz00_3157))
																		{	/* BackEnd/c_proto.scm 162 */
																			obj_t BgL_arg1802z00_3160;

																			{	/* BackEnd/c_proto.scm 162 */
																				obj_t BgL_arg1803z00_3161;

																				BgL_arg1803z00_3161 =
																					(BgL_oclassz00_3155);
																				BgL_arg1802z00_3160 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3161, 2L);
																			}
																			BgL_res2214z00_3172 =
																				(BgL_arg1802z00_3160 ==
																				BgL_classz00_3139);
																		}
																	else
																		{	/* BackEnd/c_proto.scm 162 */
																			BgL_res2214z00_3172 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2462z00_4507 = BgL_res2214z00_3172;
												}
										}
									else
										{	/* BackEnd/c_proto.scm 162 */
											BgL_test2462z00_4507 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2462z00_4507)
								{	/* BackEnd/c_proto.scm 163 */
									obj_t BgL_arg1703z00_2232;

									BgL_arg1703z00_2232 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													(((BgL_typez00_bglt) COBJECT(BgL_tz00_2227))->
														BgL_nullz00))))->BgL_namez00);
									return
										string_append_3(BGl_string2244z00zzbackend_c_prototypez00,
										BgL_arg1703z00_2232,
										BGl_string2243z00zzbackend_c_prototypez00);
								}
							else
								{	/* BackEnd/c_proto.scm 162 */
									return BGl_string2243z00zzbackend_c_prototypez00;
								}
						}
					}
			}
		}

	}



/* emit-prototype-formal-types */
	obj_t BGl_emitzd2prototypezd2formalzd2typeszd2zzbackend_c_prototypez00(obj_t
		BgL_typesz00_25)
	{
		{	/* BackEnd/c_proto.scm 192 */
			return
				string_append(BGl_string2246z00zzbackend_c_prototypez00,
				BGl_loopze72ze7zzbackend_c_prototypez00(BgL_typesz00_25));
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zzbackend_c_prototypez00(obj_t BgL_typesz00_2240)
	{
		{	/* BackEnd/c_proto.scm 197 */
			if (NULLP(CDR(((obj_t) BgL_typesz00_2240))))
				{	/* BackEnd/c_proto.scm 199 */
					obj_t BgL_arg1720z00_2244;

					{	/* BackEnd/c_proto.scm 199 */
						obj_t BgL_arg1722z00_2245;

						BgL_arg1722z00_2245 = CAR(((obj_t) BgL_typesz00_2240));
						BgL_arg1720z00_2244 =
							BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00(
							((BgL_typez00_bglt) BgL_arg1722z00_2245));
					}
					return
						string_append(BgL_arg1720z00_2244,
						BGl_string2247z00zzbackend_c_prototypez00);
				}
			else
				{	/* BackEnd/c_proto.scm 201 */
					obj_t BgL_arg1724z00_2246;
					obj_t BgL_arg1733z00_2247;

					{	/* BackEnd/c_proto.scm 201 */
						obj_t BgL_arg1734z00_2248;

						BgL_arg1734z00_2248 = CAR(((obj_t) BgL_typesz00_2240));
						BgL_arg1724z00_2246 =
							BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00(
							((BgL_typez00_bglt) BgL_arg1734z00_2248));
					}
					{	/* BackEnd/c_proto.scm 202 */
						obj_t BgL_arg1735z00_2249;

						BgL_arg1735z00_2249 = CDR(((obj_t) BgL_typesz00_2240));
						BgL_arg1733z00_2247 =
							BGl_loopze72ze7zzbackend_c_prototypez00(BgL_arg1735z00_2249);
					}
					return
						string_append_3(BgL_arg1724z00_2246,
						BGl_string2248z00zzbackend_c_prototypez00, BgL_arg1733z00_2247);
				}
		}

	}



/* emit-prototype-formals */
	obj_t BGl_emitzd2prototypezd2formalsz00zzbackend_c_prototypez00(obj_t
		BgL_argsz00_26)
	{
		{	/* BackEnd/c_proto.scm 207 */
			if (NULLP(BgL_argsz00_26))
				{	/* BackEnd/c_proto.scm 208 */
					return BGl_string2249z00zzbackend_c_prototypez00;
				}
			else
				{	/* BackEnd/c_proto.scm 208 */
					return
						string_append(BGl_string2246z00zzbackend_c_prototypez00,
						BGl_loopze71ze7zzbackend_c_prototypez00(BgL_argsz00_26));
				}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zzbackend_c_prototypez00(obj_t BgL_argsz00_2255)
	{
		{	/* BackEnd/c_proto.scm 212 */
			if (NULLP(CDR(((obj_t) BgL_argsz00_2255))))
				{	/* BackEnd/c_proto.scm 214 */
					BgL_localz00_bglt BgL_i1149z00_2259;

					BgL_i1149z00_2259 =
						((BgL_localz00_bglt) CAR(((obj_t) BgL_argsz00_2255)));
					{	/* BackEnd/c_proto.scm 215 */
						obj_t BgL_arg1746z00_2260;

						{	/* BackEnd/c_proto.scm 215 */
							BgL_typez00_bglt BgL_arg1747z00_2261;

							BgL_arg1747z00_2261 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_i1149z00_2259)))->BgL_typez00);
							BgL_arg1746z00_2260 =
								BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00
								(BgL_arg1747z00_2261);
						}
						return
							string_append(BgL_arg1746z00_2260,
							BGl_string2247z00zzbackend_c_prototypez00);
					}
				}
			else
				{	/* BackEnd/c_proto.scm 216 */
					BgL_localz00_bglt BgL_i1150z00_2262;

					BgL_i1150z00_2262 =
						((BgL_localz00_bglt) CAR(((obj_t) BgL_argsz00_2255)));
					{	/* BackEnd/c_proto.scm 217 */
						obj_t BgL_arg1748z00_2263;
						obj_t BgL_arg1749z00_2264;

						{	/* BackEnd/c_proto.scm 217 */
							BgL_typez00_bglt BgL_arg1750z00_2265;

							BgL_arg1750z00_2265 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_i1150z00_2262)))->BgL_typez00);
							BgL_arg1748z00_2263 =
								BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00
								(BgL_arg1750z00_2265);
						}
						{	/* BackEnd/c_proto.scm 218 */
							obj_t BgL_arg1751z00_2266;

							BgL_arg1751z00_2266 = CDR(((obj_t) BgL_argsz00_2255));
							BgL_arg1749z00_2264 =
								BGl_loopze71ze7zzbackend_c_prototypez00(BgL_arg1751z00_2266);
						}
						return
							string_append_3(BgL_arg1748z00_2263,
							BGl_string2248z00zzbackend_c_prototypez00, BgL_arg1749z00_2264);
					}
				}
		}

	}



/* emit-cnst */
	obj_t BGl_emitzd2cnstzd2zzbackend_c_prototypez00(BgL_scnstz00_bglt
		BgL_valuez00_31, BgL_globalz00_bglt BgL_variablez00_32)
	{
		{	/* BackEnd/c_proto.scm 266 */
			{	/* BackEnd/c_proto.scm 268 */
				obj_t BgL_casezd2valuezd2_2270;

				BgL_casezd2valuezd2_2270 =
					(((BgL_scnstz00_bglt) COBJECT(BgL_valuez00_31))->BgL_classz00);
				if ((BgL_casezd2valuezd2_2270 == CNST_TABLE_REF(6)))
					{	/* BackEnd/c_proto.scm 268 */
						return
							BGl_emitzd2cnstzd2stringz00zzbackend_c_prototypez00(
							(((BgL_scnstz00_bglt) COBJECT(BgL_valuez00_31))->BgL_nodez00),
							BgL_variablez00_32);
					}
				else
					{	/* BackEnd/c_proto.scm 268 */
						if ((BgL_casezd2valuezd2_2270 == CNST_TABLE_REF(7)))
							{	/* BackEnd/c_proto.scm 268 */
								return
									BGl_emitzd2cnstzd2realz00zzbackend_c_prototypez00(
									(((BgL_scnstz00_bglt) COBJECT(BgL_valuez00_31))->BgL_nodez00),
									BgL_variablez00_32);
							}
						else
							{	/* BackEnd/c_proto.scm 268 */
								if ((BgL_casezd2valuezd2_2270 == CNST_TABLE_REF(8)))
									{	/* BackEnd/c_proto.scm 268 */
										return
											BGl_emitzd2cnstzd2elongz00zzbackend_c_prototypez00(
											(((BgL_scnstz00_bglt) COBJECT(BgL_valuez00_31))->
												BgL_nodez00), BgL_variablez00_32);
									}
								else
									{	/* BackEnd/c_proto.scm 268 */
										if ((BgL_casezd2valuezd2_2270 == CNST_TABLE_REF(9)))
											{	/* BackEnd/c_proto.scm 268 */
												return
													BGl_emitzd2cnstzd2llongz00zzbackend_c_prototypez00(
													(((BgL_scnstz00_bglt) COBJECT(BgL_valuez00_31))->
														BgL_nodez00), BgL_variablez00_32);
											}
										else
											{	/* BackEnd/c_proto.scm 268 */
												if ((BgL_casezd2valuezd2_2270 == CNST_TABLE_REF(10)))
													{	/* BackEnd/c_proto.scm 268 */
														return
															BGl_emitzd2cnstzd2int32z00zzbackend_c_prototypez00
															((((BgL_scnstz00_bglt) COBJECT(BgL_valuez00_31))->
																BgL_nodez00), BgL_variablez00_32);
													}
												else
													{	/* BackEnd/c_proto.scm 268 */
														if (
															(BgL_casezd2valuezd2_2270 == CNST_TABLE_REF(11)))
															{	/* BackEnd/c_proto.scm 268 */
																return
																	BGl_emitzd2cnstzd2uint32z00zzbackend_c_prototypez00
																	((((BgL_scnstz00_bglt)
																			COBJECT(BgL_valuez00_31))->BgL_nodez00),
																	BgL_variablez00_32);
															}
														else
															{	/* BackEnd/c_proto.scm 268 */
																if (
																	(BgL_casezd2valuezd2_2270 ==
																		CNST_TABLE_REF(12)))
																	{	/* BackEnd/c_proto.scm 268 */
																		return
																			BGl_emitzd2cnstzd2int64z00zzbackend_c_prototypez00
																			((((BgL_scnstz00_bglt)
																					COBJECT(BgL_valuez00_31))->
																				BgL_nodez00), BgL_variablez00_32);
																	}
																else
																	{	/* BackEnd/c_proto.scm 268 */
																		if (
																			(BgL_casezd2valuezd2_2270 ==
																				CNST_TABLE_REF(13)))
																			{	/* BackEnd/c_proto.scm 268 */
																				return
																					BGl_emitzd2cnstzd2uint64z00zzbackend_c_prototypez00
																					((((BgL_scnstz00_bglt)
																							COBJECT(BgL_valuez00_31))->
																						BgL_nodez00), BgL_variablez00_32);
																			}
																		else
																			{	/* BackEnd/c_proto.scm 268 */
																				if (
																					(BgL_casezd2valuezd2_2270 ==
																						CNST_TABLE_REF(14)))
																					{	/* BackEnd/c_proto.scm 268 */
																						return
																							BGl_emitzd2cnstzd2sfunzf2sgfunzf2zzbackend_c_prototypez00
																							((((BgL_scnstz00_bglt)
																									COBJECT(BgL_valuez00_31))->
																								BgL_nodez00),
																							BgL_variablez00_32,
																							BGl_string2250z00zzbackend_c_prototypez00);
																					}
																				else
																					{	/* BackEnd/c_proto.scm 268 */
																						if (
																							(BgL_casezd2valuezd2_2270 ==
																								CNST_TABLE_REF(15)))
																							{	/* BackEnd/c_proto.scm 268 */
																								return
																									BGl_emitzd2cnstzd2sfunzf2sgfunzf2zzbackend_c_prototypez00
																									((((BgL_scnstz00_bglt)
																											COBJECT
																											(BgL_valuez00_31))->
																										BgL_nodez00),
																									BgL_variablez00_32,
																									BGl_string2251z00zzbackend_c_prototypez00);
																							}
																						else
																							{	/* BackEnd/c_proto.scm 268 */
																								if (
																									(BgL_casezd2valuezd2_2270 ==
																										CNST_TABLE_REF(16)))
																									{	/* BackEnd/c_proto.scm 268 */
																										return
																											BGl_emitzd2cnstzd2selfunz00zzbackend_c_prototypez00
																											((((BgL_scnstz00_bglt)
																													COBJECT
																													(BgL_valuez00_31))->
																												BgL_nodez00),
																											BgL_variablez00_32);
																									}
																								else
																									{	/* BackEnd/c_proto.scm 268 */
																										if (
																											(BgL_casezd2valuezd2_2270
																												== CNST_TABLE_REF(17)))
																											{	/* BackEnd/c_proto.scm 268 */
																												return
																													BGl_emitzd2cnstzd2slfunz00zzbackend_c_prototypez00
																													((((BgL_scnstz00_bglt)
																															COBJECT
																															(BgL_valuez00_31))->
																														BgL_nodez00),
																													BgL_variablez00_32);
																											}
																										else
																											{	/* BackEnd/c_proto.scm 268 */
																												if (
																													(BgL_casezd2valuezd2_2270
																														==
																														CNST_TABLE_REF(18)))
																													{	/* BackEnd/c_proto.scm 268 */
																														return
																															BGl_emitzd2cnstzd2stvectorz00zzbackend_c_prototypez00
																															((((BgL_scnstz00_bglt) COBJECT(BgL_valuez00_31))->BgL_nodez00), BgL_variablez00_32);
																													}
																												else
																													{	/* BackEnd/c_proto.scm 297 */
																														obj_t
																															BgL_arg1823z00_2297;
																														obj_t
																															BgL_arg1831z00_2298;
																														{	/* BackEnd/c_proto.scm 297 */
																															obj_t
																																BgL_arg1832z00_2299;
																															BgL_arg1832z00_2299
																																=
																																(((BgL_scnstz00_bglt) COBJECT(BgL_valuez00_31))->BgL_classz00);
																															{	/* BackEnd/c_proto.scm 297 */
																																obj_t
																																	BgL_list1833z00_2300;
																																BgL_list1833z00_2300
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1832z00_2299,
																																	BNIL);
																																BgL_arg1823z00_2297
																																	=
																																	BGl_formatz00zz__r4_output_6_10_3z00
																																	(BGl_string2252z00zzbackend_c_prototypez00,
																																	BgL_list1833z00_2300);
																															}
																														}
																														BgL_arg1831z00_2298
																															=
																															BGl_shapez00zztools_shapez00
																															((((BgL_scnstz00_bglt) COBJECT(BgL_valuez00_31))->BgL_nodez00));
																														return
																															BGl_internalzd2errorzd2zztools_errorz00
																															(BGl_string2253z00zzbackend_c_prototypez00,
																															BgL_arg1823z00_2297,
																															BgL_arg1831z00_2298);
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* emit-cnst-string */
	obj_t BGl_emitzd2cnstzd2stringz00zzbackend_c_prototypez00(obj_t
		BgL_ostrz00_33, BgL_globalz00_bglt BgL_globalz00_34)
	{
		{	/* BackEnd/c_proto.scm 303 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_globalz00_34));
			{	/* BackEnd/c_proto.scm 305 */
				bool_t BgL_test2483z00_4653;

				{	/* BackEnd/c_proto.scm 305 */
					bool_t BgL_test2484z00_4654;

					{	/* BackEnd/c_proto.scm 305 */
						long BgL_a1373z00_2398;

						BgL_a1373z00_2398 = STRING_LENGTH(((obj_t) BgL_ostrz00_33));
						if (INTEGERP
							(BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00))
							{	/* BackEnd/c_proto.scm 305 */
								BgL_test2484z00_4654 =
									(BgL_a1373z00_2398 >
									(long)
									CINT(BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00));
							}
						else
							{	/* BackEnd/c_proto.scm 305 */
								BgL_test2484z00_4654 =
									BGl_2ze3ze3zz__r4_numbers_6_5z00(BINT(BgL_a1373z00_2398),
									BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00);
							}
					}
					if (BgL_test2484z00_4654)
						{	/* BackEnd/c_proto.scm 305 */
							BgL_test2483z00_4653 =
								CBOOL(BGl_za2czd2splitzd2stringza2z00zzengine_paramz00);
						}
					else
						{	/* BackEnd/c_proto.scm 305 */
							BgL_test2483z00_4653 = ((bool_t) 0);
						}
				}
				if (BgL_test2483z00_4653)
					{	/* BackEnd/c_proto.scm 308 */
						obj_t BgL_auxz00_2308;

						{	/* BackEnd/c_proto.scm 308 */
							obj_t BgL_arg1846z00_2323;

							{	/* BackEnd/c_proto.scm 308 */
								obj_t BgL_arg1847z00_2324;

								BgL_arg1847z00_2324 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_34)))->BgL_namez00);
								BgL_arg1846z00_2323 =
									BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1847z00_2324);
							}
							BgL_auxz00_2308 =
								BGl_idzd2ze3namez31zzast_identz00(BgL_arg1846z00_2323);
						}
						{	/* BackEnd/c_proto.scm 309 */
							obj_t BgL_port1374z00_2309;

							BgL_port1374z00_2309 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
							{	/* BackEnd/c_proto.scm 309 */
								obj_t BgL_tmpz00_4668;

								BgL_tmpz00_4668 = ((obj_t) BgL_port1374z00_2309);
								bgl_display_string(BGl_string2254z00zzbackend_c_prototypez00,
									BgL_tmpz00_4668);
							}
							{	/* BackEnd/c_proto.scm 311 */
								obj_t BgL_arg1838z00_2310;

								BgL_arg1838z00_2310 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_34)))->BgL_namez00);
								bgl_display_obj(BgL_arg1838z00_2310, BgL_port1374z00_2309);
							}
							{	/* BackEnd/c_proto.scm 309 */
								obj_t BgL_tmpz00_4674;

								BgL_tmpz00_4674 = ((obj_t) BgL_port1374z00_2309);
								bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
									BgL_tmpz00_4674);
							}
							bgl_display_obj(BgL_auxz00_2308, BgL_port1374z00_2309);
							{	/* BackEnd/c_proto.scm 309 */
								obj_t BgL_tmpz00_4678;

								BgL_tmpz00_4678 = ((obj_t) BgL_port1374z00_2309);
								bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
									BgL_tmpz00_4678);
							}
							{	/* BackEnd/c_proto.scm 315 */
								long BgL_arg1839z00_2311;

								BgL_arg1839z00_2311 = STRING_LENGTH(((obj_t) BgL_ostrz00_33));
								bgl_display_obj(BINT(BgL_arg1839z00_2311),
									BgL_port1374z00_2309);
							}
							{	/* BackEnd/c_proto.scm 309 */
								obj_t BgL_tmpz00_4685;

								BgL_tmpz00_4685 = ((obj_t) BgL_port1374z00_2309);
								bgl_display_string(BGl_string2255z00zzbackend_c_prototypez00,
									BgL_tmpz00_4685);
							}
							{	/* BackEnd/c_proto.scm 309 */
								obj_t BgL_tmpz00_4688;

								BgL_tmpz00_4688 = ((obj_t) BgL_port1374z00_2309);
								bgl_display_char(((unsigned char) 10), BgL_tmpz00_4688);
						}}
						bgl_display_string(BGl_string2256z00zzbackend_c_prototypez00,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						{	/* BackEnd/c_proto.scm 318 */
							long BgL_rlenz00_2312;

							BgL_rlenz00_2312 = STRING_LENGTH(((obj_t) BgL_ostrz00_33));
							{
								long BgL_iz00_2314;

								BgL_iz00_2314 = 0L;
							BgL_zc3z04anonymousza31840ze3z87_2315:
								if ((BgL_iz00_2314 == BgL_rlenz00_2312))
									{	/* BackEnd/c_proto.scm 321 */
										obj_t BgL_port1375z00_2317;

										BgL_port1375z00_2317 =
											BGl_za2czd2portza2zd2zzbackend_c_emitz00;
										{	/* BackEnd/c_proto.scm 321 */
											obj_t BgL_tmpz00_4696;

											BgL_tmpz00_4696 = ((obj_t) BgL_port1375z00_2317);
											bgl_display_string
												(BGl_string2257z00zzbackend_c_prototypez00,
												BgL_tmpz00_4696);
										}
										{	/* BackEnd/c_proto.scm 321 */
											obj_t BgL_tmpz00_4699;

											BgL_tmpz00_4699 = ((obj_t) BgL_port1375z00_2317);
											bgl_display_string
												(BGl_string2258z00zzbackend_c_prototypez00,
												BgL_tmpz00_4699);
										}
										{	/* BackEnd/c_proto.scm 324 */
											obj_t BgL_arg1842z00_2318;

											BgL_arg1842z00_2318 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_globalz00_34)))->
												BgL_namez00);
											bgl_display_obj(BgL_arg1842z00_2318,
												BgL_port1375z00_2317);
										}
										{	/* BackEnd/c_proto.scm 321 */
											obj_t BgL_tmpz00_4705;

											BgL_tmpz00_4705 = ((obj_t) BgL_port1375z00_2317);
											bgl_display_string
												(BGl_string2248z00zzbackend_c_prototypez00,
												BgL_tmpz00_4705);
										}
										bgl_display_obj(BgL_auxz00_2308, BgL_port1375z00_2317);
										{	/* BackEnd/c_proto.scm 321 */
											obj_t BgL_tmpz00_4709;

											BgL_tmpz00_4709 = ((obj_t) BgL_port1375z00_2317);
											bgl_display_string
												(BGl_string2259z00zzbackend_c_prototypez00,
												BgL_tmpz00_4709);
										}
										{	/* BackEnd/c_proto.scm 321 */
											obj_t BgL_tmpz00_4712;

											BgL_tmpz00_4712 = ((obj_t) BgL_port1375z00_2317);
											return
												bgl_display_char(((unsigned char) 10), BgL_tmpz00_4712);
									}}
								else
									{	/* BackEnd/c_proto.scm 320 */
										{	/* BackEnd/c_proto.scm 329 */
											long BgL_arg1843z00_2319;

											BgL_arg1843z00_2319 =
												(STRING_REF(((obj_t) BgL_ostrz00_33), BgL_iz00_2314));
											bgl_display_obj(BINT(BgL_arg1843z00_2319),
												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										}
										bgl_display_string
											(BGl_string2260z00zzbackend_c_prototypez00,
											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										{
											long BgL_iz00_4721;

											BgL_iz00_4721 = (BgL_iz00_2314 + 1L);
											BgL_iz00_2314 = BgL_iz00_4721;
											goto BgL_zc3z04anonymousza31840ze3z87_2315;
										}
									}
							}
						}
					}
				else
					{	/* BackEnd/c_proto.scm 332 */
						obj_t BgL_strz00_2325;

						BgL_strz00_2325 = string_for_read(((obj_t) BgL_ostrz00_33));
						{	/* BackEnd/c_proto.scm 334 */
							obj_t BgL_port1154z00_2326;

							BgL_port1154z00_2326 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
							{	/* BackEnd/c_proto.scm 334 */
								obj_t BgL_tmpz00_4725;

								BgL_tmpz00_4725 = ((obj_t) BgL_port1154z00_2326);
								bgl_display_string(BGl_string2261z00zzbackend_c_prototypez00,
									BgL_tmpz00_4725);
							}
							{	/* BackEnd/c_proto.scm 336 */
								obj_t BgL_arg1848z00_2327;

								BgL_arg1848z00_2327 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_34)))->BgL_namez00);
								bgl_display_obj(BgL_arg1848z00_2327, BgL_port1154z00_2326);
							}
							{	/* BackEnd/c_proto.scm 334 */
								obj_t BgL_tmpz00_4731;

								BgL_tmpz00_4731 = ((obj_t) BgL_port1154z00_2326);
								bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
									BgL_tmpz00_4731);
							}
							{	/* BackEnd/c_proto.scm 338 */
								obj_t BgL_arg1849z00_2328;

								{	/* BackEnd/c_proto.scm 338 */
									obj_t BgL_arg1850z00_2329;

									{	/* BackEnd/c_proto.scm 338 */
										obj_t BgL_arg1851z00_2330;

										BgL_arg1851z00_2330 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_34)))->
											BgL_namez00);
										BgL_arg1850z00_2329 =
											BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1851z00_2330);
									}
									BgL_arg1849z00_2328 =
										BGl_idzd2ze3namez31zzast_identz00(BgL_arg1850z00_2329);
								}
								bgl_display_obj(BgL_arg1849z00_2328, BgL_port1154z00_2326);
							}
							{	/* BackEnd/c_proto.scm 334 */
								obj_t BgL_tmpz00_4739;

								BgL_tmpz00_4739 = ((obj_t) BgL_port1154z00_2326);
								bgl_display_string(BGl_string2262z00zzbackend_c_prototypez00,
									BgL_tmpz00_4739);
							}
						}
						{	/* BackEnd/c_proto.scm 340 */
							long BgL_g1155z00_2331;

							BgL_g1155z00_2331 = STRING_LENGTH(BgL_strz00_2325);
							{
								long BgL_readz00_2333;
								long BgL_rlenz00_2334;

								BgL_readz00_2333 = 0L;
								BgL_rlenz00_2334 = BgL_g1155z00_2331;
							BgL_zc3z04anonymousza31852ze3z87_2335:
								if (
									(BgL_rlenz00_2334 <=
										(long)
										CINT
										(BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00)))
									{	/* BackEnd/c_proto.scm 343 */
										{	/* BackEnd/c_proto.scm 344 */
											obj_t BgL_arg1854z00_2337;

											BgL_arg1854z00_2337 =
												BGl_untrigraphz00zzbackend_c_emitz00(c_substring
												(BgL_strz00_2325, BgL_readz00_2333,
													(BgL_readz00_2333 + BgL_rlenz00_2334)));
											bgl_display_obj(BgL_arg1854z00_2337,
												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										}
										{	/* BackEnd/c_proto.scm 346 */
											obj_t BgL_port1376z00_2340;

											BgL_port1376z00_2340 =
												BGl_za2czd2portza2zd2zzbackend_c_emitz00;
											{	/* BackEnd/c_proto.scm 346 */
												obj_t BgL_tmpz00_4750;

												BgL_tmpz00_4750 = ((obj_t) BgL_port1376z00_2340);
												bgl_display_string
													(BGl_string2263z00zzbackend_c_prototypez00,
													BgL_tmpz00_4750);
											}
											{	/* BackEnd/c_proto.scm 346 */
												long BgL_arg1858z00_2341;

												BgL_arg1858z00_2341 =
													STRING_LENGTH(((obj_t) BgL_ostrz00_33));
												bgl_display_obj(BINT(BgL_arg1858z00_2341),
													BgL_port1376z00_2340);
											}
											{	/* BackEnd/c_proto.scm 346 */
												obj_t BgL_tmpz00_4757;

												BgL_tmpz00_4757 = ((obj_t) BgL_port1376z00_2340);
												bgl_display_string
													(BGl_string2264z00zzbackend_c_prototypez00,
													BgL_tmpz00_4757);
											}
											{	/* BackEnd/c_proto.scm 346 */
												obj_t BgL_tmpz00_4760;

												BgL_tmpz00_4760 = ((obj_t) BgL_port1376z00_2340);
												return
													bgl_display_char(((unsigned char) 10),
													BgL_tmpz00_4760);
									}}}
								else
									{	/* BackEnd/c_proto.scm 348 */
										long BgL_g1159z00_2342;

										BgL_g1159z00_2342 =
											(BgL_readz00_2333 +
											(long)
											CINT
											(BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00));
										{
											long BgL_offsetz00_2344;

											BgL_offsetz00_2344 = BgL_g1159z00_2342;
										BgL_zc3z04anonymousza31859ze3z87_2345:
											if (((BgL_readz00_2333 + 3L) >= BgL_offsetz00_2344))
												{	/* BackEnd/c_proto.scm 350 */
													return
														BGl_internalzd2errorzd2zztools_errorz00
														(BGl_string2265z00zzbackend_c_prototypez00,
														BGl_string2266z00zzbackend_c_prototypez00,
														BgL_ostrz00_33);
												}
											else
												{	/* BackEnd/c_proto.scm 350 */
													if (
														(STRING_REF(BgL_strz00_2325,
																(BgL_offsetz00_2344 - 1L)) ==
															((unsigned char) '\\')))
														{
															long BgL_offsetz00_4773;

															BgL_offsetz00_4773 = (BgL_offsetz00_2344 - 1L);
															BgL_offsetz00_2344 = BgL_offsetz00_4773;
															goto BgL_zc3z04anonymousza31859ze3z87_2345;
														}
													else
														{	/* BackEnd/c_proto.scm 356 */
															bool_t BgL_test2490z00_4775;

															if (
																(STRING_REF(BgL_strz00_2325,
																		(BgL_offsetz00_2344 - 2L)) ==
																	((unsigned char) '\\')))
																{	/* BackEnd/c_proto.scm 357 */
																	unsigned char BgL_tmpz00_4780;

																	BgL_tmpz00_4780 =
																		STRING_REF(BgL_strz00_2325,
																		(BgL_offsetz00_2344 - 1L));
																	BgL_test2490z00_4775 =
																		isdigit(BgL_tmpz00_4780);
																}
															else
																{	/* BackEnd/c_proto.scm 356 */
																	BgL_test2490z00_4775 = ((bool_t) 0);
																}
															if (BgL_test2490z00_4775)
																{
																	long BgL_offsetz00_4784;

																	BgL_offsetz00_4784 =
																		(BgL_offsetz00_2344 - 2L);
																	BgL_offsetz00_2344 = BgL_offsetz00_4784;
																	goto BgL_zc3z04anonymousza31859ze3z87_2345;
																}
															else
																{	/* BackEnd/c_proto.scm 359 */
																	bool_t BgL_test2492z00_4786;

																	if (
																		(STRING_REF(BgL_strz00_2325,
																				(BgL_offsetz00_2344 - 3L)) ==
																			((unsigned char) '\\')))
																		{	/* BackEnd/c_proto.scm 360 */
																			bool_t BgL_test2494z00_4791;

																			{	/* BackEnd/c_proto.scm 360 */
																				unsigned char BgL_tmpz00_4792;

																				BgL_tmpz00_4792 =
																					STRING_REF(BgL_strz00_2325,
																					(BgL_offsetz00_2344 - 2L));
																				BgL_test2494z00_4791 =
																					isdigit(BgL_tmpz00_4792);
																			}
																			if (BgL_test2494z00_4791)
																				{	/* BackEnd/c_proto.scm 361 */
																					unsigned char BgL_tmpz00_4796;

																					BgL_tmpz00_4796 =
																						STRING_REF(BgL_strz00_2325,
																						(BgL_offsetz00_2344 - 1L));
																					BgL_test2492z00_4786 =
																						isdigit(BgL_tmpz00_4796);
																				}
																			else
																				{	/* BackEnd/c_proto.scm 360 */
																					BgL_test2492z00_4786 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* BackEnd/c_proto.scm 359 */
																			BgL_test2492z00_4786 = ((bool_t) 0);
																		}
																	if (BgL_test2492z00_4786)
																		{
																			long BgL_offsetz00_4800;

																			BgL_offsetz00_4800 =
																				(BgL_offsetz00_2344 - 3L);
																			BgL_offsetz00_2344 = BgL_offsetz00_4800;
																			goto
																				BgL_zc3z04anonymousza31859ze3z87_2345;
																		}
																	else
																		{	/* BackEnd/c_proto.scm 359 */
																			{	/* BackEnd/c_proto.scm 364 */
																				obj_t BgL_port1160z00_2375;

																				BgL_port1160z00_2375 =
																					BGl_za2czd2portza2zd2zzbackend_c_emitz00;
																				bgl_display_obj(c_substring
																					(BgL_strz00_2325, BgL_readz00_2333,
																						BgL_offsetz00_2344),
																					BgL_port1160z00_2375);
																				{	/* BackEnd/c_proto.scm 364 */
																					obj_t BgL_tmpz00_4804;

																					BgL_tmpz00_4804 =
																						((obj_t) BgL_port1160z00_2375);
																					bgl_display_string
																						(BGl_string2267z00zzbackend_c_prototypez00,
																						BgL_tmpz00_4804);
																				}
																			}
																			{
																				long BgL_rlenz00_4808;
																				long BgL_readz00_4807;

																				BgL_readz00_4807 = BgL_offsetz00_2344;
																				BgL_rlenz00_4808 =
																					(BgL_rlenz00_2334 -
																					(BgL_offsetz00_2344 -
																						BgL_readz00_2333));
																				BgL_rlenz00_2334 = BgL_rlenz00_4808;
																				BgL_readz00_2333 = BgL_readz00_4807;
																				goto
																					BgL_zc3z04anonymousza31852ze3z87_2335;
																			}
																		}
																}
														}
												}
										}
									}
							}
						}
					}
			}
		}

	}



/* emit-cnst-real */
	obj_t BGl_emitzd2cnstzd2realz00zzbackend_c_prototypez00(obj_t BgL_realz00_35,
		BgL_globalz00_bglt BgL_globalz00_36)
	{
		{	/* BackEnd/c_proto.scm 372 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_globalz00_36));
			if (BGL_IS_NAN(REAL_TO_DOUBLE(BgL_realz00_35)))
				{	/* BackEnd/c_proto.scm 376 */
					obj_t BgL_port1377z00_2401;

					BgL_port1377z00_2401 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_proto.scm 376 */
						obj_t BgL_tmpz00_4816;

						BgL_tmpz00_4816 = ((obj_t) BgL_port1377z00_2401);
						bgl_display_string(BGl_string2268z00zzbackend_c_prototypez00,
							BgL_tmpz00_4816);
					}
					{	/* BackEnd/c_proto.scm 376 */
						obj_t BgL_arg1918z00_2402;

						BgL_arg1918z00_2402 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_globalz00_36)))->BgL_namez00);
						bgl_display_obj(BgL_arg1918z00_2402, BgL_port1377z00_2401);
					}
					{	/* BackEnd/c_proto.scm 376 */
						obj_t BgL_tmpz00_4822;

						BgL_tmpz00_4822 = ((obj_t) BgL_port1377z00_2401);
						bgl_display_string(BGl_string2269z00zzbackend_c_prototypez00,
							BgL_tmpz00_4822);
					}
					{	/* BackEnd/c_proto.scm 376 */
						obj_t BgL_tmpz00_4825;

						BgL_tmpz00_4825 = ((obj_t) BgL_port1377z00_2401);
						return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4825);
				}}
			else
				{	/* BackEnd/c_proto.scm 377 */
					bool_t BgL_test2496z00_4828;

					if (BGL_IS_INF(REAL_TO_DOUBLE(BgL_realz00_35)))
						{	/* BackEnd/c_proto.scm 377 */
							BgL_test2496z00_4828 =
								(REAL_TO_DOUBLE(BgL_realz00_35) > ((double) 0.0));
						}
					else
						{	/* BackEnd/c_proto.scm 377 */
							BgL_test2496z00_4828 = ((bool_t) 0);
						}
					if (BgL_test2496z00_4828)
						{	/* BackEnd/c_proto.scm 378 */
							obj_t BgL_port1378z00_2405;

							BgL_port1378z00_2405 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
							{	/* BackEnd/c_proto.scm 378 */
								obj_t BgL_tmpz00_4834;

								BgL_tmpz00_4834 = ((obj_t) BgL_port1378z00_2405);
								bgl_display_string(BGl_string2268z00zzbackend_c_prototypez00,
									BgL_tmpz00_4834);
							}
							{	/* BackEnd/c_proto.scm 378 */
								obj_t BgL_arg1923z00_2406;

								BgL_arg1923z00_2406 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_36)))->BgL_namez00);
								bgl_display_obj(BgL_arg1923z00_2406, BgL_port1378z00_2405);
							}
							{	/* BackEnd/c_proto.scm 378 */
								obj_t BgL_tmpz00_4840;

								BgL_tmpz00_4840 = ((obj_t) BgL_port1378z00_2405);
								bgl_display_string(BGl_string2270z00zzbackend_c_prototypez00,
									BgL_tmpz00_4840);
							}
							{	/* BackEnd/c_proto.scm 378 */
								obj_t BgL_tmpz00_4843;

								BgL_tmpz00_4843 = ((obj_t) BgL_port1378z00_2405);
								return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4843);
						}}
					else
						{	/* BackEnd/c_proto.scm 377 */
							if (BGL_IS_INF(REAL_TO_DOUBLE(BgL_realz00_35)))
								{	/* BackEnd/c_proto.scm 380 */
									obj_t BgL_port1379z00_2408;

									BgL_port1379z00_2408 =
										BGl_za2czd2portza2zd2zzbackend_c_emitz00;
									{	/* BackEnd/c_proto.scm 380 */
										obj_t BgL_tmpz00_4849;

										BgL_tmpz00_4849 = ((obj_t) BgL_port1379z00_2408);
										bgl_display_string
											(BGl_string2268z00zzbackend_c_prototypez00,
											BgL_tmpz00_4849);
									}
									{	/* BackEnd/c_proto.scm 380 */
										obj_t BgL_arg1925z00_2409;

										BgL_arg1925z00_2409 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_36)))->
											BgL_namez00);
										bgl_display_obj(BgL_arg1925z00_2409, BgL_port1379z00_2408);
									}
									{	/* BackEnd/c_proto.scm 380 */
										obj_t BgL_tmpz00_4855;

										BgL_tmpz00_4855 = ((obj_t) BgL_port1379z00_2408);
										bgl_display_string
											(BGl_string2271z00zzbackend_c_prototypez00,
											BgL_tmpz00_4855);
									}
									{	/* BackEnd/c_proto.scm 380 */
										obj_t BgL_tmpz00_4858;

										BgL_tmpz00_4858 = ((obj_t) BgL_port1379z00_2408);
										return
											bgl_display_char(((unsigned char) 10), BgL_tmpz00_4858);
								}}
							else
								{	/* BackEnd/c_proto.scm 382 */
									obj_t BgL_port1380z00_2410;

									BgL_port1380z00_2410 =
										BGl_za2czd2portza2zd2zzbackend_c_emitz00;
									{	/* BackEnd/c_proto.scm 382 */
										obj_t BgL_tmpz00_4861;

										BgL_tmpz00_4861 = ((obj_t) BgL_port1380z00_2410);
										bgl_display_string
											(BGl_string2272z00zzbackend_c_prototypez00,
											BgL_tmpz00_4861);
									}
									{	/* BackEnd/c_proto.scm 383 */
										obj_t BgL_arg1926z00_2411;

										BgL_arg1926z00_2411 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_36)))->
											BgL_namez00);
										bgl_display_obj(BgL_arg1926z00_2411, BgL_port1380z00_2410);
									}
									{	/* BackEnd/c_proto.scm 382 */
										obj_t BgL_tmpz00_4867;

										BgL_tmpz00_4867 = ((obj_t) BgL_port1380z00_2410);
										bgl_display_string
											(BGl_string2248z00zzbackend_c_prototypez00,
											BgL_tmpz00_4867);
									}
									{	/* BackEnd/c_proto.scm 384 */
										obj_t BgL_arg1927z00_2412;

										{	/* BackEnd/c_proto.scm 384 */
											obj_t BgL_arg1928z00_2413;

											{	/* BackEnd/c_proto.scm 384 */
												obj_t BgL_arg1929z00_2414;

												BgL_arg1929z00_2414 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_globalz00_36)))->
													BgL_namez00);
												BgL_arg1928z00_2413 =
													BGl_gensymz00zz__r4_symbols_6_4z00
													(BgL_arg1929z00_2414);
											}
											BgL_arg1927z00_2412 =
												BGl_idzd2ze3namez31zzast_identz00(BgL_arg1928z00_2413);
										}
										bgl_display_obj(BgL_arg1927z00_2412, BgL_port1380z00_2410);
									}
									{	/* BackEnd/c_proto.scm 382 */
										obj_t BgL_tmpz00_4875;

										BgL_tmpz00_4875 = ((obj_t) BgL_port1380z00_2410);
										bgl_display_string
											(BGl_string2248z00zzbackend_c_prototypez00,
											BgL_tmpz00_4875);
									}
									bgl_display_obj(BgL_realz00_35, BgL_port1380z00_2410);
									{	/* BackEnd/c_proto.scm 382 */
										obj_t BgL_tmpz00_4879;

										BgL_tmpz00_4879 = ((obj_t) BgL_port1380z00_2410);
										bgl_display_string
											(BGl_string2264z00zzbackend_c_prototypez00,
											BgL_tmpz00_4879);
									}
									{	/* BackEnd/c_proto.scm 382 */
										obj_t BgL_tmpz00_4882;

										BgL_tmpz00_4882 = ((obj_t) BgL_port1380z00_2410);
										return
											bgl_display_char(((unsigned char) 10), BgL_tmpz00_4882);
		}}}}}

	}



/* emit-cnst-elong */
	obj_t BGl_emitzd2cnstzd2elongz00zzbackend_c_prototypez00(obj_t
		BgL_elongz00_37, BgL_globalz00_bglt BgL_globalz00_38)
	{
		{	/* BackEnd/c_proto.scm 390 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_globalz00_38));
			{	/* BackEnd/c_proto.scm 392 */
				obj_t BgL_port1381z00_2416;

				BgL_port1381z00_2416 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_proto.scm 392 */
					obj_t BgL_tmpz00_4887;

					BgL_tmpz00_4887 = ((obj_t) BgL_port1381z00_2416);
					bgl_display_string(BGl_string2273z00zzbackend_c_prototypez00,
						BgL_tmpz00_4887);
				}
				{	/* BackEnd/c_proto.scm 394 */
					obj_t BgL_arg1930z00_2417;

					BgL_arg1930z00_2417 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_38)))->BgL_namez00);
					bgl_display_obj(BgL_arg1930z00_2417, BgL_port1381z00_2416);
				}
				{	/* BackEnd/c_proto.scm 392 */
					obj_t BgL_tmpz00_4893;

					BgL_tmpz00_4893 = ((obj_t) BgL_port1381z00_2416);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_4893);
				}
				{	/* BackEnd/c_proto.scm 396 */
					obj_t BgL_arg1931z00_2418;

					{	/* BackEnd/c_proto.scm 396 */
						obj_t BgL_arg1932z00_2419;

						{	/* BackEnd/c_proto.scm 396 */
							obj_t BgL_arg1933z00_2420;

							BgL_arg1933z00_2420 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_38)))->BgL_namez00);
							BgL_arg1932z00_2419 =
								BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1933z00_2420);
						}
						BgL_arg1931z00_2418 =
							BGl_idzd2ze3namez31zzast_identz00(BgL_arg1932z00_2419);
					}
					bgl_display_obj(BgL_arg1931z00_2418, BgL_port1381z00_2416);
				}
				{	/* BackEnd/c_proto.scm 392 */
					obj_t BgL_tmpz00_4901;

					BgL_tmpz00_4901 = ((obj_t) BgL_port1381z00_2416);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_4901);
				}
				{	/* BackEnd/c_proto.scm 398 */
					obj_t BgL_arg1934z00_2421;

					BgL_arg1934z00_2421 =
						BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BELONG_TO_LONG
						(BgL_elongz00_37), BNIL);
					bgl_display_obj(BgL_arg1934z00_2421, BgL_port1381z00_2416);
				}
				{	/* BackEnd/c_proto.scm 392 */
					obj_t BgL_tmpz00_4907;

					BgL_tmpz00_4907 = ((obj_t) BgL_port1381z00_2416);
					bgl_display_string(BGl_string2264z00zzbackend_c_prototypez00,
						BgL_tmpz00_4907);
				}
				{	/* BackEnd/c_proto.scm 392 */
					obj_t BgL_tmpz00_4910;

					BgL_tmpz00_4910 = ((obj_t) BgL_port1381z00_2416);
					return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4910);
		}}}

	}



/* emit-cnst-llong */
	obj_t BGl_emitzd2cnstzd2llongz00zzbackend_c_prototypez00(obj_t
		BgL_llongz00_39, BgL_globalz00_bglt BgL_globalz00_40)
	{
		{	/* BackEnd/c_proto.scm 404 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_globalz00_40));
			{	/* BackEnd/c_proto.scm 406 */
				obj_t BgL_port1382z00_2423;

				BgL_port1382z00_2423 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_proto.scm 406 */
					obj_t BgL_tmpz00_4915;

					BgL_tmpz00_4915 = ((obj_t) BgL_port1382z00_2423);
					bgl_display_string(BGl_string2274z00zzbackend_c_prototypez00,
						BgL_tmpz00_4915);
				}
				{	/* BackEnd/c_proto.scm 408 */
					obj_t BgL_arg1936z00_2424;

					BgL_arg1936z00_2424 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_40)))->BgL_namez00);
					bgl_display_obj(BgL_arg1936z00_2424, BgL_port1382z00_2423);
				}
				{	/* BackEnd/c_proto.scm 406 */
					obj_t BgL_tmpz00_4921;

					BgL_tmpz00_4921 = ((obj_t) BgL_port1382z00_2423);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_4921);
				}
				{	/* BackEnd/c_proto.scm 410 */
					obj_t BgL_arg1937z00_2425;

					{	/* BackEnd/c_proto.scm 410 */
						obj_t BgL_arg1938z00_2426;

						{	/* BackEnd/c_proto.scm 410 */
							obj_t BgL_arg1939z00_2427;

							BgL_arg1939z00_2427 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_40)))->BgL_namez00);
							BgL_arg1938z00_2426 =
								BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1939z00_2427);
						}
						BgL_arg1937z00_2425 =
							BGl_idzd2ze3namez31zzast_identz00(BgL_arg1938z00_2426);
					}
					bgl_display_obj(BgL_arg1937z00_2425, BgL_port1382z00_2423);
				}
				{	/* BackEnd/c_proto.scm 406 */
					obj_t BgL_tmpz00_4929;

					BgL_tmpz00_4929 = ((obj_t) BgL_port1382z00_2423);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_4929);
				}
				bgl_display_obj(BGl_llongzd2ze3czd2isoze3zzbackend_c_emitz00
					(BLLONG_TO_LLONG(BgL_llongz00_39)), BgL_port1382z00_2423);
				{	/* BackEnd/c_proto.scm 406 */
					obj_t BgL_tmpz00_4935;

					BgL_tmpz00_4935 = ((obj_t) BgL_port1382z00_2423);
					bgl_display_string(BGl_string2264z00zzbackend_c_prototypez00,
						BgL_tmpz00_4935);
				}
				{	/* BackEnd/c_proto.scm 406 */
					obj_t BgL_tmpz00_4938;

					BgL_tmpz00_4938 = ((obj_t) BgL_port1382z00_2423);
					return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4938);
		}}}

	}



/* emit-cnst-int32 */
	obj_t BGl_emitzd2cnstzd2int32z00zzbackend_c_prototypez00(obj_t
		BgL_int32z00_41, BgL_globalz00_bglt BgL_globalz00_42)
	{
		{	/* BackEnd/c_proto.scm 418 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_globalz00_42));
			{	/* BackEnd/c_proto.scm 420 */
				obj_t BgL_port1383z00_2429;

				BgL_port1383z00_2429 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_proto.scm 420 */
					obj_t BgL_tmpz00_4943;

					BgL_tmpz00_4943 = ((obj_t) BgL_port1383z00_2429);
					bgl_display_string(BGl_string2275z00zzbackend_c_prototypez00,
						BgL_tmpz00_4943);
				}
				{	/* BackEnd/c_proto.scm 422 */
					obj_t BgL_arg1941z00_2430;

					BgL_arg1941z00_2430 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_42)))->BgL_namez00);
					bgl_display_obj(BgL_arg1941z00_2430, BgL_port1383z00_2429);
				}
				{	/* BackEnd/c_proto.scm 420 */
					obj_t BgL_tmpz00_4949;

					BgL_tmpz00_4949 = ((obj_t) BgL_port1383z00_2429);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_4949);
				}
				{	/* BackEnd/c_proto.scm 424 */
					obj_t BgL_arg1942z00_2431;

					{	/* BackEnd/c_proto.scm 424 */
						obj_t BgL_arg1943z00_2432;

						{	/* BackEnd/c_proto.scm 424 */
							obj_t BgL_arg1944z00_2433;

							BgL_arg1944z00_2433 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_42)))->BgL_namez00);
							BgL_arg1943z00_2432 =
								BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1944z00_2433);
						}
						BgL_arg1942z00_2431 =
							BGl_idzd2ze3namez31zzast_identz00(BgL_arg1943z00_2432);
					}
					bgl_display_obj(BgL_arg1942z00_2431, BgL_port1383z00_2429);
				}
				{	/* BackEnd/c_proto.scm 420 */
					obj_t BgL_tmpz00_4957;

					BgL_tmpz00_4957 = ((obj_t) BgL_port1383z00_2429);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_4957);
				}
				bgl_display_obj(BgL_int32z00_41, BgL_port1383z00_2429);
				{	/* BackEnd/c_proto.scm 420 */
					obj_t BgL_tmpz00_4961;

					BgL_tmpz00_4961 = ((obj_t) BgL_port1383z00_2429);
					bgl_display_string(BGl_string2264z00zzbackend_c_prototypez00,
						BgL_tmpz00_4961);
				}
				{	/* BackEnd/c_proto.scm 420 */
					obj_t BgL_tmpz00_4964;

					BgL_tmpz00_4964 = ((obj_t) BgL_port1383z00_2429);
					return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4964);
		}}}

	}



/* emit-cnst-uint32 */
	obj_t BGl_emitzd2cnstzd2uint32z00zzbackend_c_prototypez00(obj_t
		BgL_uint32z00_43, BgL_globalz00_bglt BgL_globalz00_44)
	{
		{	/* BackEnd/c_proto.scm 432 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_globalz00_44));
			{	/* BackEnd/c_proto.scm 434 */
				obj_t BgL_port1384z00_2434;

				BgL_port1384z00_2434 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_proto.scm 434 */
					obj_t BgL_tmpz00_4969;

					BgL_tmpz00_4969 = ((obj_t) BgL_port1384z00_2434);
					bgl_display_string(BGl_string2276z00zzbackend_c_prototypez00,
						BgL_tmpz00_4969);
				}
				{	/* BackEnd/c_proto.scm 436 */
					obj_t BgL_arg1945z00_2435;

					BgL_arg1945z00_2435 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_44)))->BgL_namez00);
					bgl_display_obj(BgL_arg1945z00_2435, BgL_port1384z00_2434);
				}
				{	/* BackEnd/c_proto.scm 434 */
					obj_t BgL_tmpz00_4975;

					BgL_tmpz00_4975 = ((obj_t) BgL_port1384z00_2434);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_4975);
				}
				{	/* BackEnd/c_proto.scm 438 */
					obj_t BgL_arg1946z00_2436;

					{	/* BackEnd/c_proto.scm 438 */
						obj_t BgL_arg1947z00_2437;

						{	/* BackEnd/c_proto.scm 438 */
							obj_t BgL_arg1948z00_2438;

							BgL_arg1948z00_2438 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_44)))->BgL_namez00);
							BgL_arg1947z00_2437 =
								BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1948z00_2438);
						}
						BgL_arg1946z00_2436 =
							BGl_idzd2ze3namez31zzast_identz00(BgL_arg1947z00_2437);
					}
					bgl_display_obj(BgL_arg1946z00_2436, BgL_port1384z00_2434);
				}
				{	/* BackEnd/c_proto.scm 434 */
					obj_t BgL_tmpz00_4983;

					BgL_tmpz00_4983 = ((obj_t) BgL_port1384z00_2434);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_4983);
				}
				bgl_display_obj(BgL_uint32z00_43, BgL_port1384z00_2434);
				{	/* BackEnd/c_proto.scm 434 */
					obj_t BgL_tmpz00_4987;

					BgL_tmpz00_4987 = ((obj_t) BgL_port1384z00_2434);
					bgl_display_string(BGl_string2264z00zzbackend_c_prototypez00,
						BgL_tmpz00_4987);
				}
				{	/* BackEnd/c_proto.scm 434 */
					obj_t BgL_tmpz00_4990;

					BgL_tmpz00_4990 = ((obj_t) BgL_port1384z00_2434);
					return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4990);
		}}}

	}



/* emit-cnst-int64 */
	obj_t BGl_emitzd2cnstzd2int64z00zzbackend_c_prototypez00(obj_t
		BgL_int64z00_45, BgL_globalz00_bglt BgL_globalz00_46)
	{
		{	/* BackEnd/c_proto.scm 446 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_globalz00_46));
			{	/* BackEnd/c_proto.scm 448 */
				obj_t BgL_port1385z00_2439;

				BgL_port1385z00_2439 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_proto.scm 448 */
					obj_t BgL_tmpz00_4995;

					BgL_tmpz00_4995 = ((obj_t) BgL_port1385z00_2439);
					bgl_display_string(BGl_string2277z00zzbackend_c_prototypez00,
						BgL_tmpz00_4995);
				}
				{	/* BackEnd/c_proto.scm 450 */
					obj_t BgL_arg1949z00_2440;

					BgL_arg1949z00_2440 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_46)))->BgL_namez00);
					bgl_display_obj(BgL_arg1949z00_2440, BgL_port1385z00_2439);
				}
				{	/* BackEnd/c_proto.scm 448 */
					obj_t BgL_tmpz00_5001;

					BgL_tmpz00_5001 = ((obj_t) BgL_port1385z00_2439);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_5001);
				}
				{	/* BackEnd/c_proto.scm 452 */
					obj_t BgL_arg1950z00_2441;

					{	/* BackEnd/c_proto.scm 452 */
						obj_t BgL_arg1951z00_2442;

						{	/* BackEnd/c_proto.scm 452 */
							obj_t BgL_arg1952z00_2443;

							BgL_arg1952z00_2443 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_46)))->BgL_namez00);
							BgL_arg1951z00_2442 =
								BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1952z00_2443);
						}
						BgL_arg1950z00_2441 =
							BGl_idzd2ze3namez31zzast_identz00(BgL_arg1951z00_2442);
					}
					bgl_display_obj(BgL_arg1950z00_2441, BgL_port1385z00_2439);
				}
				{	/* BackEnd/c_proto.scm 448 */
					obj_t BgL_tmpz00_5009;

					BgL_tmpz00_5009 = ((obj_t) BgL_port1385z00_2439);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_5009);
				}
				bgl_display_obj(BgL_int64z00_45, BgL_port1385z00_2439);
				{	/* BackEnd/c_proto.scm 448 */
					obj_t BgL_tmpz00_5013;

					BgL_tmpz00_5013 = ((obj_t) BgL_port1385z00_2439);
					bgl_display_string(BGl_string2264z00zzbackend_c_prototypez00,
						BgL_tmpz00_5013);
				}
				{	/* BackEnd/c_proto.scm 448 */
					obj_t BgL_tmpz00_5016;

					BgL_tmpz00_5016 = ((obj_t) BgL_port1385z00_2439);
					return bgl_display_char(((unsigned char) 10), BgL_tmpz00_5016);
		}}}

	}



/* emit-cnst-uint64 */
	obj_t BGl_emitzd2cnstzd2uint64z00zzbackend_c_prototypez00(obj_t
		BgL_uint64z00_47, BgL_globalz00_bglt BgL_globalz00_48)
	{
		{	/* BackEnd/c_proto.scm 460 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_globalz00_48));
			{	/* BackEnd/c_proto.scm 462 */
				obj_t BgL_port1386z00_2444;

				BgL_port1386z00_2444 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_proto.scm 462 */
					obj_t BgL_tmpz00_5021;

					BgL_tmpz00_5021 = ((obj_t) BgL_port1386z00_2444);
					bgl_display_string(BGl_string2278z00zzbackend_c_prototypez00,
						BgL_tmpz00_5021);
				}
				{	/* BackEnd/c_proto.scm 464 */
					obj_t BgL_arg1953z00_2445;

					BgL_arg1953z00_2445 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_48)))->BgL_namez00);
					bgl_display_obj(BgL_arg1953z00_2445, BgL_port1386z00_2444);
				}
				{	/* BackEnd/c_proto.scm 462 */
					obj_t BgL_tmpz00_5027;

					BgL_tmpz00_5027 = ((obj_t) BgL_port1386z00_2444);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_5027);
				}
				{	/* BackEnd/c_proto.scm 466 */
					obj_t BgL_arg1954z00_2446;

					{	/* BackEnd/c_proto.scm 466 */
						obj_t BgL_arg1955z00_2447;

						{	/* BackEnd/c_proto.scm 466 */
							obj_t BgL_arg1956z00_2448;

							BgL_arg1956z00_2448 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_48)))->BgL_namez00);
							BgL_arg1955z00_2447 =
								BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1956z00_2448);
						}
						BgL_arg1954z00_2446 =
							BGl_idzd2ze3namez31zzast_identz00(BgL_arg1955z00_2447);
					}
					bgl_display_obj(BgL_arg1954z00_2446, BgL_port1386z00_2444);
				}
				{	/* BackEnd/c_proto.scm 462 */
					obj_t BgL_tmpz00_5035;

					BgL_tmpz00_5035 = ((obj_t) BgL_port1386z00_2444);
					bgl_display_string(BGl_string2248z00zzbackend_c_prototypez00,
						BgL_tmpz00_5035);
				}
				bgl_display_obj(BgL_uint64z00_47, BgL_port1386z00_2444);
				{	/* BackEnd/c_proto.scm 462 */
					obj_t BgL_tmpz00_5039;

					BgL_tmpz00_5039 = ((obj_t) BgL_port1386z00_2444);
					bgl_display_string(BGl_string2264z00zzbackend_c_prototypez00,
						BgL_tmpz00_5039);
				}
				{	/* BackEnd/c_proto.scm 462 */
					obj_t BgL_tmpz00_5042;

					BgL_tmpz00_5042 = ((obj_t) BgL_port1386z00_2444);
					return bgl_display_char(((unsigned char) 10), BgL_tmpz00_5042);
		}}}

	}



/* emit-cnst-sfun/sgfun */
	obj_t BGl_emitzd2cnstzd2sfunzf2sgfunzf2zzbackend_c_prototypez00(obj_t
		BgL_funz00_53, BgL_globalz00_bglt BgL_globalz00_54, obj_t BgL_kindz00_55)
	{
		{	/* BackEnd/c_proto.scm 486 */
			if (
				((((BgL_globalz00_bglt) COBJECT(BgL_globalz00_54))->BgL_importz00) ==
					CNST_TABLE_REF(19)))
				{	/* BackEnd/c_proto.scm 488 */
					BgL_valuez00_bglt BgL_arg1959z00_2451;

					BgL_arg1959z00_2451 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_54)))->BgL_valuez00);
					return
						BGl_emitzd2prototypezd2zzbackend_c_prototypez00(BgL_arg1959z00_2451,
						((BgL_variablez00_bglt) BgL_globalz00_54));
				}
			else
				{	/* BackEnd/c_proto.scm 489 */
					obj_t BgL_actualsz00_2452;

					BgL_actualsz00_2452 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_funz00_53)))->BgL_argsz00);
					{	/* BackEnd/c_proto.scm 489 */
						obj_t BgL_entryz00_2453;

						BgL_entryz00_2453 = CAR(((obj_t) BgL_actualsz00_2452));
						{	/* BackEnd/c_proto.scm 490 */
							obj_t BgL_arityz00_2454;

							{	/* BackEnd/c_proto.scm 491 */
								obj_t BgL_arg2003z00_2508;

								{	/* BackEnd/c_proto.scm 491 */
									obj_t BgL_pairz00_3405;

									BgL_pairz00_3405 = CDR(((obj_t) BgL_actualsz00_2452));
									BgL_arg2003z00_2508 = CAR(BgL_pairz00_3405);
								}
								BgL_arityz00_2454 =
									BGl_getzd2nodezd2atomzd2valuezd2zzcnst_nodez00(
									((BgL_nodez00_bglt) BgL_arg2003z00_2508));
							}
							{	/* BackEnd/c_proto.scm 491 */
								obj_t BgL_vnamez00_2455;

								BgL_vnamez00_2455 =
									BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
									((BgL_variablez00_bglt) BgL_globalz00_54));
								{	/* BackEnd/c_proto.scm 492 */
									obj_t BgL_namez00_2456;

									{	/* BackEnd/c_proto.scm 493 */
										BgL_variablez00_bglt BgL_arg2002z00_2507;

										BgL_arg2002z00_2507 =
											(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_entryz00_2453)))->
											BgL_variablez00);
										BgL_namez00_2456 =
											BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
											(BgL_arg2002z00_2507);
									}
									{	/* BackEnd/c_proto.scm 493 */

										{	/* BackEnd/c_proto.scm 495 */
											bool_t BgL_test2500z00_5067;

											{	/* BackEnd/c_proto.scm 495 */
												bool_t BgL_test2501z00_5068;

												{	/* BackEnd/c_proto.scm 495 */
													obj_t BgL_classz00_3407;

													BgL_classz00_3407 = BGl_varz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_entryz00_2453))
														{	/* BackEnd/c_proto.scm 495 */
															BgL_objectz00_bglt BgL_arg1807z00_3409;

															BgL_arg1807z00_3409 =
																(BgL_objectz00_bglt) (BgL_entryz00_2453);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* BackEnd/c_proto.scm 495 */
																	long BgL_idxz00_3415;

																	BgL_idxz00_3415 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3409);
																	BgL_test2501z00_5068 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3415 + 2L)) ==
																		BgL_classz00_3407);
																}
															else
																{	/* BackEnd/c_proto.scm 495 */
																	bool_t BgL_res2215z00_3440;

																	{	/* BackEnd/c_proto.scm 495 */
																		obj_t BgL_oclassz00_3423;

																		{	/* BackEnd/c_proto.scm 495 */
																			obj_t BgL_arg1815z00_3431;
																			long BgL_arg1816z00_3432;

																			BgL_arg1815z00_3431 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* BackEnd/c_proto.scm 495 */
																				long BgL_arg1817z00_3433;

																				BgL_arg1817z00_3433 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3409);
																				BgL_arg1816z00_3432 =
																					(BgL_arg1817z00_3433 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3423 =
																				VECTOR_REF(BgL_arg1815z00_3431,
																				BgL_arg1816z00_3432);
																		}
																		{	/* BackEnd/c_proto.scm 495 */
																			bool_t BgL__ortest_1115z00_3424;

																			BgL__ortest_1115z00_3424 =
																				(BgL_classz00_3407 ==
																				BgL_oclassz00_3423);
																			if (BgL__ortest_1115z00_3424)
																				{	/* BackEnd/c_proto.scm 495 */
																					BgL_res2215z00_3440 =
																						BgL__ortest_1115z00_3424;
																				}
																			else
																				{	/* BackEnd/c_proto.scm 495 */
																					long BgL_odepthz00_3425;

																					{	/* BackEnd/c_proto.scm 495 */
																						obj_t BgL_arg1804z00_3426;

																						BgL_arg1804z00_3426 =
																							(BgL_oclassz00_3423);
																						BgL_odepthz00_3425 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3426);
																					}
																					if ((2L < BgL_odepthz00_3425))
																						{	/* BackEnd/c_proto.scm 495 */
																							obj_t BgL_arg1802z00_3428;

																							{	/* BackEnd/c_proto.scm 495 */
																								obj_t BgL_arg1803z00_3429;

																								BgL_arg1803z00_3429 =
																									(BgL_oclassz00_3423);
																								BgL_arg1802z00_3428 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3429, 2L);
																							}
																							BgL_res2215z00_3440 =
																								(BgL_arg1802z00_3428 ==
																								BgL_classz00_3407);
																						}
																					else
																						{	/* BackEnd/c_proto.scm 495 */
																							BgL_res2215z00_3440 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2501z00_5068 = BgL_res2215z00_3440;
																}
														}
													else
														{	/* BackEnd/c_proto.scm 495 */
															BgL_test2501z00_5068 = ((bool_t) 0);
														}
												}
												if (BgL_test2501z00_5068)
													{	/* BackEnd/c_proto.scm 496 */
														bool_t BgL_test2506z00_5091;

														{	/* BackEnd/c_proto.scm 496 */
															BgL_variablez00_bglt BgL_arg2001z00_2506;

															BgL_arg2001z00_2506 =
																(((BgL_varz00_bglt) COBJECT(
																		((BgL_varz00_bglt) BgL_entryz00_2453)))->
																BgL_variablez00);
															{	/* BackEnd/c_proto.scm 496 */
																obj_t BgL_classz00_3442;

																BgL_classz00_3442 = BGl_globalz00zzast_varz00;
																{	/* BackEnd/c_proto.scm 496 */
																	BgL_objectz00_bglt BgL_arg1807z00_3444;

																	{	/* BackEnd/c_proto.scm 496 */
																		obj_t BgL_tmpz00_5094;

																		BgL_tmpz00_5094 =
																			((obj_t)
																			((BgL_objectz00_bglt)
																				BgL_arg2001z00_2506));
																		BgL_arg1807z00_3444 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_5094);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* BackEnd/c_proto.scm 496 */
																			long BgL_idxz00_3450;

																			BgL_idxz00_3450 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3444);
																			BgL_test2506z00_5091 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3450 + 2L)) ==
																				BgL_classz00_3442);
																		}
																	else
																		{	/* BackEnd/c_proto.scm 496 */
																			bool_t BgL_res2216z00_3475;

																			{	/* BackEnd/c_proto.scm 496 */
																				obj_t BgL_oclassz00_3458;

																				{	/* BackEnd/c_proto.scm 496 */
																					obj_t BgL_arg1815z00_3466;
																					long BgL_arg1816z00_3467;

																					BgL_arg1815z00_3466 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* BackEnd/c_proto.scm 496 */
																						long BgL_arg1817z00_3468;

																						BgL_arg1817z00_3468 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3444);
																						BgL_arg1816z00_3467 =
																							(BgL_arg1817z00_3468 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3458 =
																						VECTOR_REF(BgL_arg1815z00_3466,
																						BgL_arg1816z00_3467);
																				}
																				{	/* BackEnd/c_proto.scm 496 */
																					bool_t BgL__ortest_1115z00_3459;

																					BgL__ortest_1115z00_3459 =
																						(BgL_classz00_3442 ==
																						BgL_oclassz00_3458);
																					if (BgL__ortest_1115z00_3459)
																						{	/* BackEnd/c_proto.scm 496 */
																							BgL_res2216z00_3475 =
																								BgL__ortest_1115z00_3459;
																						}
																					else
																						{	/* BackEnd/c_proto.scm 496 */
																							long BgL_odepthz00_3460;

																							{	/* BackEnd/c_proto.scm 496 */
																								obj_t BgL_arg1804z00_3461;

																								BgL_arg1804z00_3461 =
																									(BgL_oclassz00_3458);
																								BgL_odepthz00_3460 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3461);
																							}
																							if ((2L < BgL_odepthz00_3460))
																								{	/* BackEnd/c_proto.scm 496 */
																									obj_t BgL_arg1802z00_3463;

																									{	/* BackEnd/c_proto.scm 496 */
																										obj_t BgL_arg1803z00_3464;

																										BgL_arg1803z00_3464 =
																											(BgL_oclassz00_3458);
																										BgL_arg1802z00_3463 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3464, 2L);
																									}
																									BgL_res2216z00_3475 =
																										(BgL_arg1802z00_3463 ==
																										BgL_classz00_3442);
																								}
																							else
																								{	/* BackEnd/c_proto.scm 496 */
																									BgL_res2216z00_3475 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2506z00_5091 =
																				BgL_res2216z00_3475;
																		}
																}
															}
														}
														if (BgL_test2506z00_5091)
															{	/* BackEnd/c_proto.scm 498 */
																bool_t BgL__ortest_1161z00_2499;

																{	/* BackEnd/c_proto.scm 498 */
																	obj_t BgL_arg1998z00_2503;

																	BgL_arg1998z00_2503 =
																		(((BgL_sfunz00_bglt) COBJECT(
																				((BgL_sfunz00_bglt)
																					(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_globalz00_bglt)
																										(((BgL_varz00_bglt) COBJECT(
																													((BgL_varz00_bglt)
																														BgL_entryz00_2453)))->
																											BgL_variablez00)))))->
																						BgL_valuez00))))->
																		BgL_thezd2closurezd2globalz00);
																	BgL__ortest_1161z00_2499 =
																		BGl_globalzd2optionalzf3z21zzast_varz00
																		(BgL_arg1998z00_2503);
																}
																if (BgL__ortest_1161z00_2499)
																	{	/* BackEnd/c_proto.scm 498 */
																		BgL_test2500z00_5067 =
																			BgL__ortest_1161z00_2499;
																	}
																else
																	{	/* BackEnd/c_proto.scm 499 */
																		obj_t BgL_arg1995z00_2500;

																		BgL_arg1995z00_2500 =
																			(((BgL_sfunz00_bglt) COBJECT(
																					((BgL_sfunz00_bglt)
																						(((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										((BgL_globalz00_bglt)
																											(((BgL_varz00_bglt)
																													COBJECT((
																															(BgL_varz00_bglt)
																															BgL_entryz00_2453)))->
																												BgL_variablez00)))))->
																							BgL_valuez00))))->
																			BgL_thezd2closurezd2globalz00);
																		BgL_test2500z00_5067 =
																			BGl_globalzd2keyzf3z21zzast_varz00
																			(BgL_arg1995z00_2500);
																	}
															}
														else
															{	/* BackEnd/c_proto.scm 496 */
																BgL_test2500z00_5067 = ((bool_t) 0);
															}
													}
												else
													{	/* BackEnd/c_proto.scm 495 */
														BgL_test2500z00_5067 = ((bool_t) 0);
													}
											}
											if (BgL_test2500z00_5067)
												{	/* BackEnd/c_proto.scm 500 */
													obj_t BgL_port1387z00_2469;

													BgL_port1387z00_2469 =
														BGl_za2czd2portza2zd2zzbackend_c_emitz00;
													{	/* BackEnd/c_proto.scm 501 */
														obj_t BgL_arg1971z00_2470;

														if (
															((((BgL_globalz00_bglt)
																		COBJECT(BgL_globalz00_54))->
																	BgL_importz00) == CNST_TABLE_REF(0)))
															{	/* BackEnd/c_proto.scm 501 */
																BgL_arg1971z00_2470 =
																	string_append_3
																	(BGl_string2279z00zzbackend_c_prototypez00,
																	BgL_kindz00_55,
																	BGl_string2280z00zzbackend_c_prototypez00);
															}
														else
															{	/* BackEnd/c_proto.scm 501 */
																BgL_arg1971z00_2470 =
																	string_append_3
																	(BGl_string2281z00zzbackend_c_prototypez00,
																	BgL_kindz00_55,
																	BGl_string2280z00zzbackend_c_prototypez00);
															}
														bgl_display_obj(BgL_arg1971z00_2470,
															BgL_port1387z00_2469);
													}
													bgl_display_obj(BgL_vnamez00_2455,
														BgL_port1387z00_2469);
													{	/* BackEnd/c_proto.scm 500 */
														obj_t BgL_tmpz00_5142;

														BgL_tmpz00_5142 = ((obj_t) BgL_port1387z00_2469);
														bgl_display_string
															(BGl_string2248z00zzbackend_c_prototypez00,
															BgL_tmpz00_5142);
													}
													bgl_display_obj(BGl_idzd2ze3namez31zzast_identz00
														(BGl_gensymz00zz__r4_symbols_6_4z00
															(BgL_namez00_2456)), BgL_port1387z00_2469);
													{	/* BackEnd/c_proto.scm 500 */
														obj_t BgL_tmpz00_5148;

														BgL_tmpz00_5148 = ((obj_t) BgL_port1387z00_2469);
														bgl_display_string
															(BGl_string2282z00zzbackend_c_prototypez00,
															BgL_tmpz00_5148);
													}
													{	/* BackEnd/c_proto.scm 500 */
														obj_t BgL_tmpz00_5151;

														BgL_tmpz00_5151 = ((obj_t) BgL_port1387z00_2469);
														bgl_display_string
															(BGl_string2248z00zzbackend_c_prototypez00,
															BgL_tmpz00_5151);
													}
													bgl_display_obj(BgL_namez00_2456,
														BgL_port1387z00_2469);
													{	/* BackEnd/c_proto.scm 500 */
														obj_t BgL_tmpz00_5155;

														BgL_tmpz00_5155 = ((obj_t) BgL_port1387z00_2469);
														bgl_display_string
															(BGl_string2283z00zzbackend_c_prototypez00,
															BgL_tmpz00_5155);
													}
													bgl_display_fixnum(BINT(-1L),
														((obj_t) BgL_port1387z00_2469));
													{	/* BackEnd/c_proto.scm 500 */
														obj_t BgL_tmpz00_5161;

														BgL_tmpz00_5161 = ((obj_t) BgL_port1387z00_2469);
														bgl_display_string
															(BGl_string2264z00zzbackend_c_prototypez00,
															BgL_tmpz00_5161);
													}
													{	/* BackEnd/c_proto.scm 500 */
														obj_t BgL_tmpz00_5164;

														BgL_tmpz00_5164 = ((obj_t) BgL_port1387z00_2469);
														return
															bgl_display_char(((unsigned char) 10),
															BgL_tmpz00_5164);
												}}
											else
												{	/* BackEnd/c_proto.scm 495 */
													if (((long) CINT(BgL_arityz00_2454) >= 0L))
														{	/* BackEnd/c_proto.scm 515 */
															obj_t BgL_port1388z00_2477;

															BgL_port1388z00_2477 =
																BGl_za2czd2portza2zd2zzbackend_c_emitz00;
															{	/* BackEnd/c_proto.scm 516 */
																obj_t BgL_arg1978z00_2478;

																if (
																	((((BgL_globalz00_bglt)
																				COBJECT(BgL_globalz00_54))->
																			BgL_importz00) == CNST_TABLE_REF(0)))
																	{	/* BackEnd/c_proto.scm 516 */
																		BgL_arg1978z00_2478 =
																			string_append_3
																			(BGl_string2279z00zzbackend_c_prototypez00,
																			BgL_kindz00_55,
																			BGl_string2280z00zzbackend_c_prototypez00);
																	}
																else
																	{	/* BackEnd/c_proto.scm 516 */
																		BgL_arg1978z00_2478 =
																			string_append_3
																			(BGl_string2281z00zzbackend_c_prototypez00,
																			BgL_kindz00_55,
																			BGl_string2280z00zzbackend_c_prototypez00);
																	}
																bgl_display_obj(BgL_arg1978z00_2478,
																	BgL_port1388z00_2477);
															}
															bgl_display_obj(BgL_vnamez00_2455,
																BgL_port1388z00_2477);
															{	/* BackEnd/c_proto.scm 515 */
																obj_t BgL_tmpz00_5178;

																BgL_tmpz00_5178 =
																	((obj_t) BgL_port1388z00_2477);
																bgl_display_string
																	(BGl_string2248z00zzbackend_c_prototypez00,
																	BgL_tmpz00_5178);
															}
															bgl_display_obj(BGl_idzd2ze3namez31zzast_identz00
																(BGl_gensymz00zz__r4_symbols_6_4z00
																	(BgL_namez00_2456)), BgL_port1388z00_2477);
															{	/* BackEnd/c_proto.scm 515 */
																obj_t BgL_tmpz00_5184;

																BgL_tmpz00_5184 =
																	((obj_t) BgL_port1388z00_2477);
																bgl_display_string
																	(BGl_string2248z00zzbackend_c_prototypez00,
																	BgL_tmpz00_5184);
															}
															bgl_display_obj(BgL_namez00_2456,
																BgL_port1388z00_2477);
															{	/* BackEnd/c_proto.scm 515 */
																obj_t BgL_tmpz00_5188;

																BgL_tmpz00_5188 =
																	((obj_t) BgL_port1388z00_2477);
																bgl_display_string
																	(BGl_string2284z00zzbackend_c_prototypez00,
																	BgL_tmpz00_5188);
															}
															bgl_display_obj(BgL_arityz00_2454,
																BgL_port1388z00_2477);
															{	/* BackEnd/c_proto.scm 515 */
																obj_t BgL_tmpz00_5192;

																BgL_tmpz00_5192 =
																	((obj_t) BgL_port1388z00_2477);
																bgl_display_string
																	(BGl_string2264z00zzbackend_c_prototypez00,
																	BgL_tmpz00_5192);
															}
															{	/* BackEnd/c_proto.scm 515 */
																obj_t BgL_tmpz00_5195;

																BgL_tmpz00_5195 =
																	((obj_t) BgL_port1388z00_2477);
																return
																	bgl_display_char(((unsigned char) 10),
																	BgL_tmpz00_5195);
														}}
													else
														{	/* BackEnd/c_proto.scm 528 */
															obj_t BgL_port1389z00_2484;

															BgL_port1389z00_2484 =
																BGl_za2czd2portza2zd2zzbackend_c_emitz00;
															{	/* BackEnd/c_proto.scm 529 */
																obj_t BgL_arg1984z00_2485;

																if (
																	((((BgL_globalz00_bglt)
																				COBJECT(BgL_globalz00_54))->
																			BgL_importz00) == CNST_TABLE_REF(0)))
																	{	/* BackEnd/c_proto.scm 529 */
																		BgL_arg1984z00_2485 =
																			string_append_3
																			(BGl_string2279z00zzbackend_c_prototypez00,
																			BgL_kindz00_55,
																			BGl_string2280z00zzbackend_c_prototypez00);
																	}
																else
																	{	/* BackEnd/c_proto.scm 529 */
																		BgL_arg1984z00_2485 =
																			string_append_3
																			(BGl_string2281z00zzbackend_c_prototypez00,
																			BgL_kindz00_55,
																			BGl_string2280z00zzbackend_c_prototypez00);
																	}
																bgl_display_obj(BgL_arg1984z00_2485,
																	BgL_port1389z00_2484);
															}
															bgl_display_obj(BgL_vnamez00_2455,
																BgL_port1389z00_2484);
															{	/* BackEnd/c_proto.scm 528 */
																obj_t BgL_tmpz00_5206;

																BgL_tmpz00_5206 =
																	((obj_t) BgL_port1389z00_2484);
																bgl_display_string
																	(BGl_string2248z00zzbackend_c_prototypez00,
																	BgL_tmpz00_5206);
															}
															bgl_display_obj(BGl_idzd2ze3namez31zzast_identz00
																(BGl_gensymz00zz__r4_symbols_6_4z00
																	(BgL_namez00_2456)), BgL_port1389z00_2484);
															{	/* BackEnd/c_proto.scm 535 */
																obj_t BgL_arg1990z00_2491;

																{	/* BackEnd/c_proto.scm 535 */
																	bool_t BgL_test2515z00_5212;

																	{	/* BackEnd/c_proto.scm 535 */
																		bool_t BgL_test2516z00_5213;

																		{	/* BackEnd/c_proto.scm 535 */
																			obj_t BgL_classz00_3510;

																			BgL_classz00_3510 =
																				BGl_varz00zzast_nodez00;
																			if (BGL_OBJECTP(BgL_entryz00_2453))
																				{	/* BackEnd/c_proto.scm 535 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_3512;
																					BgL_arg1807z00_3512 =
																						(BgL_objectz00_bglt)
																						(BgL_entryz00_2453);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* BackEnd/c_proto.scm 535 */
																							long BgL_idxz00_3518;

																							BgL_idxz00_3518 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_3512);
																							BgL_test2516z00_5213 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_3518 + 2L)) ==
																								BgL_classz00_3510);
																						}
																					else
																						{	/* BackEnd/c_proto.scm 535 */
																							bool_t BgL_res2217z00_3543;

																							{	/* BackEnd/c_proto.scm 535 */
																								obj_t BgL_oclassz00_3526;

																								{	/* BackEnd/c_proto.scm 535 */
																									obj_t BgL_arg1815z00_3534;
																									long BgL_arg1816z00_3535;

																									BgL_arg1815z00_3534 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* BackEnd/c_proto.scm 535 */
																										long BgL_arg1817z00_3536;

																										BgL_arg1817z00_3536 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_3512);
																										BgL_arg1816z00_3535 =
																											(BgL_arg1817z00_3536 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_3526 =
																										VECTOR_REF
																										(BgL_arg1815z00_3534,
																										BgL_arg1816z00_3535);
																								}
																								{	/* BackEnd/c_proto.scm 535 */
																									bool_t
																										BgL__ortest_1115z00_3527;
																									BgL__ortest_1115z00_3527 =
																										(BgL_classz00_3510 ==
																										BgL_oclassz00_3526);
																									if (BgL__ortest_1115z00_3527)
																										{	/* BackEnd/c_proto.scm 535 */
																											BgL_res2217z00_3543 =
																												BgL__ortest_1115z00_3527;
																										}
																									else
																										{	/* BackEnd/c_proto.scm 535 */
																											long BgL_odepthz00_3528;

																											{	/* BackEnd/c_proto.scm 535 */
																												obj_t
																													BgL_arg1804z00_3529;
																												BgL_arg1804z00_3529 =
																													(BgL_oclassz00_3526);
																												BgL_odepthz00_3528 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_3529);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_3528))
																												{	/* BackEnd/c_proto.scm 535 */
																													obj_t
																														BgL_arg1802z00_3531;
																													{	/* BackEnd/c_proto.scm 535 */
																														obj_t
																															BgL_arg1803z00_3532;
																														BgL_arg1803z00_3532
																															=
																															(BgL_oclassz00_3526);
																														BgL_arg1802z00_3531
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_3532,
																															2L);
																													}
																													BgL_res2217z00_3543 =
																														(BgL_arg1802z00_3531
																														==
																														BgL_classz00_3510);
																												}
																											else
																												{	/* BackEnd/c_proto.scm 535 */
																													BgL_res2217z00_3543 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2516z00_5213 =
																								BgL_res2217z00_3543;
																						}
																				}
																			else
																				{	/* BackEnd/c_proto.scm 535 */
																					BgL_test2516z00_5213 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test2516z00_5213)
																			{	/* BackEnd/c_proto.scm 536 */
																				BgL_variablez00_bglt
																					BgL_arg1994z00_2496;
																				BgL_arg1994z00_2496 =
																					(((BgL_varz00_bglt)
																						COBJECT(((BgL_varz00_bglt)
																								BgL_entryz00_2453)))->
																					BgL_variablez00);
																				BgL_test2515z00_5212 =
																					CBOOL
																					(BGl_funzd2vazd2stackablezf3zf3zzbackend_c_prototypez00
																					(BgL_arg1994z00_2496));
																			}
																		else
																			{	/* BackEnd/c_proto.scm 535 */
																				BgL_test2515z00_5212 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test2515z00_5212)
																		{	/* BackEnd/c_proto.scm 535 */
																			BgL_arg1990z00_2491 =
																				BGl_string2285z00zzbackend_c_prototypez00;
																		}
																	else
																		{	/* BackEnd/c_proto.scm 535 */
																			BgL_arg1990z00_2491 =
																				BGl_string2286z00zzbackend_c_prototypez00;
																		}
																}
																bgl_display_obj(BgL_arg1990z00_2491,
																	BgL_port1389z00_2484);
															}
															{	/* BackEnd/c_proto.scm 528 */
																obj_t BgL_tmpz00_5241;

																BgL_tmpz00_5241 =
																	((obj_t) BgL_port1389z00_2484);
																bgl_display_string
																	(BGl_string2248z00zzbackend_c_prototypez00,
																	BgL_tmpz00_5241);
															}
															bgl_display_obj(BgL_namez00_2456,
																BgL_port1389z00_2484);
															{	/* BackEnd/c_proto.scm 528 */
																obj_t BgL_tmpz00_5245;

																BgL_tmpz00_5245 =
																	((obj_t) BgL_port1389z00_2484);
																bgl_display_string
																	(BGl_string2287z00zzbackend_c_prototypez00,
																	BgL_tmpz00_5245);
															}
															bgl_display_obj(BgL_arityz00_2454,
																BgL_port1389z00_2484);
															{	/* BackEnd/c_proto.scm 528 */
																obj_t BgL_tmpz00_5249;

																BgL_tmpz00_5249 =
																	((obj_t) BgL_port1389z00_2484);
																bgl_display_string
																	(BGl_string2264z00zzbackend_c_prototypez00,
																	BgL_tmpz00_5249);
															}
															{	/* BackEnd/c_proto.scm 528 */
																obj_t BgL_tmpz00_5252;

																BgL_tmpz00_5252 =
																	((obj_t) BgL_port1389z00_2484);
																return
																	bgl_display_char(((unsigned char) 10),
																	BgL_tmpz00_5252);
		}}}}}}}}}}}

	}



/* fun-va-stackable? */
	obj_t
		BGl_funzd2vazd2stackablezf3zf3zzbackend_c_prototypez00(BgL_variablez00_bglt
		BgL_varz00_56)
	{
		{	/* BackEnd/c_proto.scm 548 */
			{	/* BackEnd/c_proto.scm 549 */
				BgL_valuez00_bglt BgL_funz00_2510;

				BgL_funz00_2510 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_56))->BgL_valuez00);
				{	/* BackEnd/c_proto.scm 550 */
					bool_t BgL_test2521z00_5256;

					{	/* BackEnd/c_proto.scm 550 */
						obj_t BgL_classz00_3553;

						BgL_classz00_3553 = BGl_sfunz00zzast_varz00;
						{	/* BackEnd/c_proto.scm 550 */
							BgL_objectz00_bglt BgL_arg1807z00_3555;

							{	/* BackEnd/c_proto.scm 550 */
								obj_t BgL_tmpz00_5257;

								BgL_tmpz00_5257 =
									((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2510));
								BgL_arg1807z00_3555 = (BgL_objectz00_bglt) (BgL_tmpz00_5257);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* BackEnd/c_proto.scm 550 */
									long BgL_idxz00_3561;

									BgL_idxz00_3561 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3555);
									BgL_test2521z00_5256 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3561 + 3L)) == BgL_classz00_3553);
								}
							else
								{	/* BackEnd/c_proto.scm 550 */
									bool_t BgL_res2218z00_3586;

									{	/* BackEnd/c_proto.scm 550 */
										obj_t BgL_oclassz00_3569;

										{	/* BackEnd/c_proto.scm 550 */
											obj_t BgL_arg1815z00_3577;
											long BgL_arg1816z00_3578;

											BgL_arg1815z00_3577 = (BGl_za2classesza2z00zz__objectz00);
											{	/* BackEnd/c_proto.scm 550 */
												long BgL_arg1817z00_3579;

												BgL_arg1817z00_3579 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3555);
												BgL_arg1816z00_3578 =
													(BgL_arg1817z00_3579 - OBJECT_TYPE);
											}
											BgL_oclassz00_3569 =
												VECTOR_REF(BgL_arg1815z00_3577, BgL_arg1816z00_3578);
										}
										{	/* BackEnd/c_proto.scm 550 */
											bool_t BgL__ortest_1115z00_3570;

											BgL__ortest_1115z00_3570 =
												(BgL_classz00_3553 == BgL_oclassz00_3569);
											if (BgL__ortest_1115z00_3570)
												{	/* BackEnd/c_proto.scm 550 */
													BgL_res2218z00_3586 = BgL__ortest_1115z00_3570;
												}
											else
												{	/* BackEnd/c_proto.scm 550 */
													long BgL_odepthz00_3571;

													{	/* BackEnd/c_proto.scm 550 */
														obj_t BgL_arg1804z00_3572;

														BgL_arg1804z00_3572 = (BgL_oclassz00_3569);
														BgL_odepthz00_3571 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3572);
													}
													if ((3L < BgL_odepthz00_3571))
														{	/* BackEnd/c_proto.scm 550 */
															obj_t BgL_arg1802z00_3574;

															{	/* BackEnd/c_proto.scm 550 */
																obj_t BgL_arg1803z00_3575;

																BgL_arg1803z00_3575 = (BgL_oclassz00_3569);
																BgL_arg1802z00_3574 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3575,
																	3L);
															}
															BgL_res2218z00_3586 =
																(BgL_arg1802z00_3574 == BgL_classz00_3553);
														}
													else
														{	/* BackEnd/c_proto.scm 550 */
															BgL_res2218z00_3586 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2521z00_5256 = BgL_res2218z00_3586;
								}
						}
					}
					if (BgL_test2521z00_5256)
						{	/* BackEnd/c_proto.scm 552 */
							bool_t BgL__ortest_1163z00_2513;

							BgL__ortest_1163z00_2513 =
								(
								(((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_2510))))->
									BgL_argszd2noescapezd2) == CNST_TABLE_REF(20));
							if (BgL__ortest_1163z00_2513)
								{	/* BackEnd/c_proto.scm 552 */
									return BBOOL(BgL__ortest_1163z00_2513);
								}
							else
								{	/* BackEnd/c_proto.scm 553 */
									bool_t BgL_test2526z00_5287;

									{	/* BackEnd/c_proto.scm 553 */
										obj_t BgL_tmpz00_5288;

										BgL_tmpz00_5288 =
											(((BgL_funz00_bglt) COBJECT(
													((BgL_funz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2510))))->
											BgL_argszd2noescapezd2);
										BgL_test2526z00_5287 = PAIRP(BgL_tmpz00_5288);
									}
									if (BgL_test2526z00_5287)
										{	/* BackEnd/c_proto.scm 553 */
											return
												BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BINT(
													(bgl_list_length(
															(((BgL_sfunz00_bglt) COBJECT(
																		((BgL_sfunz00_bglt) BgL_funz00_2510)))->
																BgL_argszd2namezd2)) - 2L)),
												(((BgL_funz00_bglt)
														COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
																	BgL_funz00_2510))))->BgL_argszd2noescapezd2));
										}
									else
										{	/* BackEnd/c_proto.scm 553 */
											return BFALSE;
										}
								}
						}
					else
						{	/* BackEnd/c_proto.scm 550 */
							return BFALSE;
						}
				}
			}
		}

	}



/* emit-cnst-selfun */
	obj_t BGl_emitzd2cnstzd2selfunz00zzbackend_c_prototypez00(obj_t BgL_funz00_57,
		BgL_globalz00_bglt BgL_globalz00_58)
	{
		{	/* BackEnd/c_proto.scm 561 */
			{	/* BackEnd/c_proto.scm 562 */
				obj_t BgL_vnamez00_2521;

				BgL_vnamez00_2521 =
					BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
					((BgL_variablez00_bglt) BgL_globalz00_58));
				{	/* BackEnd/c_proto.scm 563 */
					obj_t BgL_port1390z00_2522;

					BgL_port1390z00_2522 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_proto.scm 563 */
						obj_t BgL_tmpz00_5304;

						BgL_tmpz00_5304 = ((obj_t) BgL_port1390z00_2522);
						bgl_display_string(BGl_string2288z00zzbackend_c_prototypez00,
							BgL_tmpz00_5304);
					}
					bgl_display_obj(BgL_vnamez00_2521, BgL_port1390z00_2522);
					{	/* BackEnd/c_proto.scm 563 */
						obj_t BgL_tmpz00_5308;

						BgL_tmpz00_5308 = ((obj_t) BgL_port1390z00_2522);
						bgl_display_string(BGl_string2243z00zzbackend_c_prototypez00,
							BgL_tmpz00_5308);
					}
					{	/* BackEnd/c_proto.scm 563 */
						obj_t BgL_tmpz00_5311;

						BgL_tmpz00_5311 = ((obj_t) BgL_port1390z00_2522);
						return bgl_display_char(((unsigned char) 10), BgL_tmpz00_5311);
		}}}}

	}



/* emit-cnst-slfun */
	obj_t BGl_emitzd2cnstzd2slfunz00zzbackend_c_prototypez00(obj_t BgL_funz00_59,
		BgL_globalz00_bglt BgL_globalz00_60)
	{
		{	/* BackEnd/c_proto.scm 568 */
			{	/* BackEnd/c_proto.scm 569 */
				obj_t BgL_actualsz00_2523;

				BgL_actualsz00_2523 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_funz00_59)))->BgL_argsz00);
				{	/* BackEnd/c_proto.scm 569 */
					obj_t BgL_entryz00_2524;

					BgL_entryz00_2524 = CAR(((obj_t) BgL_actualsz00_2523));
					{	/* BackEnd/c_proto.scm 570 */
						obj_t BgL_vnamez00_2525;

						BgL_vnamez00_2525 =
							BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
							((BgL_variablez00_bglt) BgL_globalz00_60));
						{	/* BackEnd/c_proto.scm 571 */
							obj_t BgL_namez00_2526;

							{	/* BackEnd/c_proto.scm 572 */
								BgL_variablez00_bglt BgL_arg2014z00_2530;

								BgL_arg2014z00_2530 =
									(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_entryz00_2524)))->BgL_variablez00);
								BgL_namez00_2526 =
									BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
									(BgL_arg2014z00_2530);
							}
							{	/* BackEnd/c_proto.scm 572 */

								{	/* BackEnd/c_proto.scm 573 */
									obj_t BgL_port1391z00_2527;

									BgL_port1391z00_2527 =
										BGl_za2czd2portza2zd2zzbackend_c_emitz00;
									{	/* BackEnd/c_proto.scm 573 */
										obj_t BgL_tmpz00_5323;

										BgL_tmpz00_5323 = ((obj_t) BgL_port1391z00_2527);
										bgl_display_string
											(BGl_string2289z00zzbackend_c_prototypez00,
											BgL_tmpz00_5323);
									}
									bgl_display_obj(BgL_vnamez00_2525, BgL_port1391z00_2527);
									{	/* BackEnd/c_proto.scm 573 */
										obj_t BgL_tmpz00_5327;

										BgL_tmpz00_5327 = ((obj_t) BgL_port1391z00_2527);
										bgl_display_string
											(BGl_string2248z00zzbackend_c_prototypez00,
											BgL_tmpz00_5327);
									}
									bgl_display_obj(BGl_idzd2ze3namez31zzast_identz00
										(BGl_gensymz00zz__r4_symbols_6_4z00(BgL_namez00_2526)),
										BgL_port1391z00_2527);
									{	/* BackEnd/c_proto.scm 573 */
										obj_t BgL_tmpz00_5333;

										BgL_tmpz00_5333 = ((obj_t) BgL_port1391z00_2527);
										bgl_display_string
											(BGl_string2248z00zzbackend_c_prototypez00,
											BgL_tmpz00_5333);
									}
									bgl_display_obj(BgL_namez00_2526, BgL_port1391z00_2527);
									{	/* BackEnd/c_proto.scm 573 */
										obj_t BgL_tmpz00_5337;

										BgL_tmpz00_5337 = ((obj_t) BgL_port1391z00_2527);
										bgl_display_string
											(BGl_string2264z00zzbackend_c_prototypez00,
											BgL_tmpz00_5337);
									}
									{	/* BackEnd/c_proto.scm 573 */
										obj_t BgL_tmpz00_5340;

										BgL_tmpz00_5340 = ((obj_t) BgL_port1391z00_2527);
										return
											bgl_display_char(((unsigned char) 10), BgL_tmpz00_5340);
		}}}}}}}}

	}



/* emit-cnst-stvector */
	obj_t BGl_emitzd2cnstzd2stvectorz00zzbackend_c_prototypez00(obj_t
		BgL_tvecz00_61, BgL_globalz00_bglt BgL_globalz00_62)
	{
		{	/* BackEnd/c_proto.scm 585 */
			{	/* BackEnd/c_proto.scm 586 */
				obj_t BgL_vecz00_2531;

				BgL_vecz00_2531 = STRUCT_REF(((obj_t) BgL_tvecz00_61), (int) (1L));
				{	/* BackEnd/c_proto.scm 586 */
					BgL_typez00_bglt BgL_itypez00_2532;

					{	/* BackEnd/c_proto.scm 587 */
						BgL_typez00_bglt BgL_oz00_3607;

						BgL_oz00_3607 =
							((BgL_typez00_bglt)
							STRUCT_REF(((obj_t) BgL_tvecz00_61), (int) (0L)));
						{
							BgL_tvecz00_bglt BgL_auxz00_5350;

							{
								obj_t BgL_auxz00_5351;

								{	/* BackEnd/c_proto.scm 587 */
									BgL_objectz00_bglt BgL_tmpz00_5352;

									BgL_tmpz00_5352 = ((BgL_objectz00_bglt) BgL_oz00_3607);
									BgL_auxz00_5351 = BGL_OBJECT_WIDENING(BgL_tmpz00_5352);
								}
								BgL_auxz00_5350 = ((BgL_tvecz00_bglt) BgL_auxz00_5351);
							}
							BgL_itypez00_2532 =
								(((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_5350))->
								BgL_itemzd2typezd2);
					}}
					{	/* BackEnd/c_proto.scm 587 */
						obj_t BgL_czd2veczd2_2533;

						BgL_czd2veczd2_2533 =
							BGl_tvectorzd2ze3czd2vectorze3zztvector_cnstz00(BgL_tvecz00_61);
						{	/* BackEnd/c_proto.scm 588 */

							BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
								((BgL_variablez00_bglt) BgL_globalz00_62));
							{	/* BackEnd/c_proto.scm 590 */
								obj_t BgL_auxz00_2534;

								{	/* BackEnd/c_proto.scm 590 */
									obj_t BgL_arg2019z00_2540;

									{	/* BackEnd/c_proto.scm 590 */
										obj_t BgL_arg2020z00_2541;

										BgL_arg2020z00_2541 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_62)))->
											BgL_namez00);
										BgL_arg2019z00_2540 =
											BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg2020z00_2541);
									}
									BgL_auxz00_2534 =
										BGl_idzd2ze3namez31zzast_identz00(BgL_arg2019z00_2540);
								}
								{	/* BackEnd/c_proto.scm 591 */
									obj_t BgL_port1392z00_2535;

									BgL_port1392z00_2535 =
										BGl_za2czd2portza2zd2zzbackend_c_emitz00;
									{	/* BackEnd/c_proto.scm 591 */
										obj_t BgL_tmpz00_5364;

										BgL_tmpz00_5364 = ((obj_t) BgL_port1392z00_2535);
										bgl_display_string
											(BGl_string2290z00zzbackend_c_prototypez00,
											BgL_tmpz00_5364);
									}
									bgl_display_obj(BgL_auxz00_2534, BgL_port1392z00_2535);
									{	/* BackEnd/c_proto.scm 591 */
										obj_t BgL_tmpz00_5368;

										BgL_tmpz00_5368 = ((obj_t) BgL_port1392z00_2535);
										bgl_display_string
											(BGl_string2248z00zzbackend_c_prototypez00,
											BgL_tmpz00_5368);
									}
									bgl_display_obj(BINT(VECTOR_LENGTH(
												((obj_t) BgL_vecz00_2531))), BgL_port1392z00_2535);
									{	/* BackEnd/c_proto.scm 591 */
										obj_t BgL_tmpz00_5375;

										BgL_tmpz00_5375 = ((obj_t) BgL_port1392z00_2535);
										bgl_display_string
											(BGl_string2248z00zzbackend_c_prototypez00,
											BgL_tmpz00_5375);
									}
									bgl_display_obj(BGl_stringzd2sanszd2z42z42zztype_toolsz00(
											(((BgL_typez00_bglt) COBJECT(BgL_itypez00_2532))->
												BgL_namez00)), BgL_port1392z00_2535);
									{	/* BackEnd/c_proto.scm 591 */
										obj_t BgL_tmpz00_5381;

										BgL_tmpz00_5381 = ((obj_t) BgL_port1392z00_2535);
										bgl_display_string
											(BGl_string2291z00zzbackend_c_prototypez00,
											BgL_tmpz00_5381);
									}
									bgl_display_obj(BgL_czd2veczd2_2533, BgL_port1392z00_2535);
									{	/* BackEnd/c_proto.scm 591 */
										obj_t BgL_tmpz00_5385;

										BgL_tmpz00_5385 = ((obj_t) BgL_port1392z00_2535);
										bgl_display_string
											(BGl_string2292z00zzbackend_c_prototypez00,
											BgL_tmpz00_5385);
									}
									{	/* BackEnd/c_proto.scm 601 */
										obj_t BgL_arg2018z00_2539;

										BgL_arg2018z00_2539 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_62)))->
											BgL_namez00);
										bgl_display_obj(BgL_arg2018z00_2539, BgL_port1392z00_2535);
									}
									{	/* BackEnd/c_proto.scm 591 */
										obj_t BgL_tmpz00_5391;

										BgL_tmpz00_5391 = ((obj_t) BgL_port1392z00_2535);
										bgl_display_string
											(BGl_string2248z00zzbackend_c_prototypez00,
											BgL_tmpz00_5391);
									}
									bgl_display_obj(BgL_auxz00_2534, BgL_port1392z00_2535);
									{	/* BackEnd/c_proto.scm 591 */
										obj_t BgL_tmpz00_5395;

										BgL_tmpz00_5395 = ((obj_t) BgL_port1392z00_2535);
										bgl_display_string
											(BGl_string2264z00zzbackend_c_prototypez00,
											BgL_tmpz00_5395);
									}
									{	/* BackEnd/c_proto.scm 591 */
										obj_t BgL_tmpz00_5398;

										BgL_tmpz00_5398 = ((obj_t) BgL_port1392z00_2535);
										return
											bgl_display_char(((unsigned char) 10), BgL_tmpz00_5398);
		}}}}}}}}

	}



/* emit-class-types */
	BGL_EXPORTED_DEF obj_t
		BGl_emitzd2classzd2typesz00zzbackend_c_prototypez00(obj_t
		BgL_classzd2listzd2_66, obj_t BgL_oportz00_67)
	{
		{	/* BackEnd/c_proto.scm 635 */
			{
				obj_t BgL_classz00_2570;
				obj_t BgL_classz00_2586;

				if (NULLP(BgL_classzd2listzd2_66))
					{	/* BackEnd/c_proto.scm 670 */
						return BFALSE;
					}
				else
					{	/* BackEnd/c_proto.scm 670 */
						bgl_display_char(((unsigned char) 10), BgL_oportz00_67);
						bgl_display_string(BGl_string2300z00zzbackend_c_prototypez00,
							BgL_oportz00_67);
						bgl_display_char(((unsigned char) 10), BgL_oportz00_67);
						{	/* BackEnd/c_proto.scm 672 */
							obj_t BgL_g1410z00_2548;

							BgL_g1410z00_2548 = bgl_reverse_bang(BgL_classzd2listzd2_66);
							{
								obj_t BgL_l1408z00_2550;

								BgL_l1408z00_2550 = BgL_g1410z00_2548;
							BgL_zc3z04anonymousza32023ze3z87_2551:
								if (PAIRP(BgL_l1408z00_2550))
									{	/* BackEnd/c_proto.scm 677 */
										{	/* BackEnd/c_proto.scm 673 */
											obj_t BgL_classz00_2553;

											BgL_classz00_2553 = CAR(BgL_l1408z00_2550);
											if (
												(BgL_classz00_2553 ==
													BGl_getzd2objectzd2typez00zztype_cachez00()))
												{	/* BackEnd/c_proto.scm 673 */
													BFALSE;
												}
											else
												{	/* BackEnd/c_proto.scm 673 */
													if (BGl_widezd2classzf3z21zzobject_classz00
														(BgL_classz00_2553))
														{	/* BackEnd/c_proto.scm 674 */
															BgL_classz00_2586 = BgL_classz00_2553;
															bgl_display_string
																(BGl_string2293z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															{	/* BackEnd/c_proto.scm 654 */
																obj_t BgL_arg2048z00_2589;

																{	/* BackEnd/c_proto.scm 654 */
																	obj_t BgL_arg2049z00_2590;

																	{
																		BgL_tclassz00_bglt BgL_auxz00_5416;

																		{
																			obj_t BgL_auxz00_5417;

																			{	/* BackEnd/c_proto.scm 654 */
																				BgL_objectz00_bglt BgL_tmpz00_5418;

																				BgL_tmpz00_5418 =
																					((BgL_objectz00_bglt)
																					((BgL_typez00_bglt)
																						BgL_classz00_2586));
																				BgL_auxz00_5417 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5418);
																			}
																			BgL_auxz00_5416 =
																				((BgL_tclassz00_bglt) BgL_auxz00_5417);
																		}
																		BgL_arg2049z00_2590 =
																			(((BgL_tclassz00_bglt)
																				COBJECT(BgL_auxz00_5416))->
																			BgL_widezd2typezd2);
																	}
																	BgL_arg2048z00_2589 =
																		(((BgL_typez00_bglt) COBJECT(
																				((BgL_typez00_bglt)
																					BgL_arg2049z00_2590)))->
																		BgL_siza7eza7);
																}
																bgl_display_obj(BgL_arg2048z00_2589,
																	BgL_oportz00_67);
															}
															bgl_display_string
																(BGl_string2294z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															bgl_display_char(((unsigned char) 10),
																BgL_oportz00_67);
															{	/* BackEnd/c_proto.scm 655 */
																bool_t BgL_test2531z00_5429;

																{	/* BackEnd/c_proto.scm 655 */
																	obj_t BgL_arg2052z00_2594;

																	{
																		BgL_tclassz00_bglt BgL_auxz00_5430;

																		{
																			obj_t BgL_auxz00_5431;

																			{	/* BackEnd/c_proto.scm 655 */
																				BgL_objectz00_bglt BgL_tmpz00_5432;

																				BgL_tmpz00_5432 =
																					((BgL_objectz00_bglt)
																					((BgL_typez00_bglt)
																						BgL_classz00_2586));
																				BgL_auxz00_5431 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5432);
																			}
																			BgL_auxz00_5430 =
																				((BgL_tclassz00_bglt) BgL_auxz00_5431);
																		}
																		BgL_arg2052z00_2594 =
																			(((BgL_tclassz00_bglt)
																				COBJECT(BgL_auxz00_5430))->
																			BgL_slotsz00);
																	}
																	BgL_test2531z00_5429 =
																		NULLP(BgL_arg2052z00_2594);
																}
																if (BgL_test2531z00_5429)
																	{	/* BackEnd/c_proto.scm 655 */
																		bgl_display_string
																			(BGl_string2295z00zzbackend_c_prototypez00,
																			BgL_oportz00_67);
																		bgl_display_char(((unsigned char) 10),
																			BgL_oportz00_67);
																	}
																else
																	{	/* BackEnd/c_proto.scm 655 */
																		BFALSE;
																	}
															}
															{	/* BackEnd/c_proto.scm 662 */
																obj_t BgL_g1405z00_2595;

																{
																	BgL_tclassz00_bglt BgL_auxz00_5441;

																	{
																		obj_t BgL_auxz00_5442;

																		{	/* BackEnd/c_proto.scm 665 */
																			BgL_objectz00_bglt BgL_tmpz00_5443;

																			BgL_tmpz00_5443 =
																				((BgL_objectz00_bglt)
																				((BgL_typez00_bglt) BgL_classz00_2586));
																			BgL_auxz00_5442 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_5443);
																		}
																		BgL_auxz00_5441 =
																			((BgL_tclassz00_bglt) BgL_auxz00_5442);
																	}
																	BgL_g1405z00_2595 =
																		(((BgL_tclassz00_bglt)
																			COBJECT(BgL_auxz00_5441))->BgL_slotsz00);
																}
																{
																	obj_t BgL_l1403z00_2597;

																	BgL_l1403z00_2597 = BgL_g1405z00_2595;
																BgL_zc3z04anonymousza32053ze3z87_2598:
																	if (PAIRP(BgL_l1403z00_2597))
																		{	/* BackEnd/c_proto.scm 665 */
																			{	/* BackEnd/c_proto.scm 663 */
																				obj_t BgL_sz00_2600;

																				BgL_sz00_2600 = CAR(BgL_l1403z00_2597);
																				if (
																					((((BgL_slotz00_bglt) COBJECT(
																									((BgL_slotz00_bglt)
																										BgL_sz00_2600)))->
																							BgL_classzd2ownerzd2) ==
																						BgL_classz00_2586))
																					{	/* BackEnd/c_proto.scm 663 */
																						BGl_emitzd2slotze70z35zzbackend_c_prototypez00
																							(BgL_oportz00_67, BgL_sz00_2600);
																					}
																				else
																					{	/* BackEnd/c_proto.scm 663 */
																						BFALSE;
																					}
																			}
																			{
																				obj_t BgL_l1403z00_5457;

																				BgL_l1403z00_5457 =
																					CDR(BgL_l1403z00_2597);
																				BgL_l1403z00_2597 = BgL_l1403z00_5457;
																				goto
																					BgL_zc3z04anonymousza32053ze3z87_2598;
																			}
																		}
																	else
																		{	/* BackEnd/c_proto.scm 665 */
																			((bool_t) 1);
																		}
																}
															}
															bgl_display_string
																(BGl_string2296z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															{	/* BackEnd/c_proto.scm 667 */
																obj_t BgL_arg2059z00_2607;

																{	/* BackEnd/c_proto.scm 667 */
																	obj_t BgL_arg2060z00_2608;

																	{
																		BgL_tclassz00_bglt BgL_auxz00_5460;

																		{
																			obj_t BgL_auxz00_5461;

																			{	/* BackEnd/c_proto.scm 667 */
																				BgL_objectz00_bglt BgL_tmpz00_5462;

																				BgL_tmpz00_5462 =
																					((BgL_objectz00_bglt)
																					((BgL_typez00_bglt)
																						BgL_classz00_2586));
																				BgL_auxz00_5461 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5462);
																			}
																			BgL_auxz00_5460 =
																				((BgL_tclassz00_bglt) BgL_auxz00_5461);
																		}
																		BgL_arg2060z00_2608 =
																			(((BgL_tclassz00_bglt)
																				COBJECT(BgL_auxz00_5460))->
																			BgL_widezd2typezd2);
																	}
																	BgL_arg2059z00_2607 =
																		(((BgL_typez00_bglt) COBJECT(
																				((BgL_typez00_bglt)
																					BgL_arg2060z00_2608)))->BgL_namez00);
																}
																bgl_display_obj(BgL_arg2059z00_2607,
																	BgL_oportz00_67);
															}
															bgl_display_string
																(BGl_string2297z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															bgl_display_char(((unsigned char) 10),
																BgL_oportz00_67);
														}
													else
														{	/* BackEnd/c_proto.scm 674 */
															BgL_classz00_2570 = BgL_classz00_2553;
															bgl_display_string
																(BGl_string2293z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															{	/* BackEnd/c_proto.scm 647 */
																obj_t BgL_arg2040z00_2573;

																BgL_arg2040z00_2573 =
																	(((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt) BgL_classz00_2570)))->
																	BgL_siza7eza7);
																bgl_display_obj(BgL_arg2040z00_2573,
																	BgL_oportz00_67);
															}
															bgl_display_string
																(BGl_string2294z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															bgl_display_char(((unsigned char) 10),
																BgL_oportz00_67);
															bgl_display_string
																(BGl_string2298z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															bgl_display_char(((unsigned char) 10),
																BgL_oportz00_67);
															bgl_display_string
																(BGl_string2299z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															bgl_display_char(((unsigned char) 10),
																BgL_oportz00_67);
															{	/* BackEnd/c_proto.scm 650 */
																obj_t BgL_g1399z00_2576;

																{
																	BgL_tclassz00_bglt BgL_auxz00_5483;

																	{
																		obj_t BgL_auxz00_5484;

																		{	/* BackEnd/c_proto.scm 650 */
																			BgL_objectz00_bglt BgL_tmpz00_5485;

																			BgL_tmpz00_5485 =
																				((BgL_objectz00_bglt)
																				((BgL_typez00_bglt) BgL_classz00_2570));
																			BgL_auxz00_5484 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_5485);
																		}
																		BgL_auxz00_5483 =
																			((BgL_tclassz00_bglt) BgL_auxz00_5484);
																	}
																	BgL_g1399z00_2576 =
																		(((BgL_tclassz00_bglt)
																			COBJECT(BgL_auxz00_5483))->BgL_slotsz00);
																}
																{
																	obj_t BgL_l1397z00_2578;

																	BgL_l1397z00_2578 = BgL_g1399z00_2576;
																BgL_zc3z04anonymousza32041ze3z87_2579:
																	if (PAIRP(BgL_l1397z00_2578))
																		{	/* BackEnd/c_proto.scm 650 */
																			BGl_emitzd2slotze70z35zzbackend_c_prototypez00
																				(BgL_oportz00_67,
																				CAR(BgL_l1397z00_2578));
																			{
																				obj_t BgL_l1397z00_5495;

																				BgL_l1397z00_5495 =
																					CDR(BgL_l1397z00_2578);
																				BgL_l1397z00_2578 = BgL_l1397z00_5495;
																				goto
																					BgL_zc3z04anonymousza32041ze3z87_2579;
																			}
																		}
																	else
																		{	/* BackEnd/c_proto.scm 650 */
																			((bool_t) 1);
																		}
																}
															}
															bgl_display_string
																(BGl_string2296z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															{	/* BackEnd/c_proto.scm 651 */
																obj_t BgL_arg2046z00_2585;

																BgL_arg2046z00_2585 =
																	BGl_typezd2classzd2namez00zzobject_classz00(
																	((BgL_typez00_bglt) BgL_classz00_2570));
																bgl_display_obj(BgL_arg2046z00_2585,
																	BgL_oportz00_67);
															}
															bgl_display_string
																(BGl_string2297z00zzbackend_c_prototypez00,
																BgL_oportz00_67);
															bgl_display_char(((unsigned char) 10),
																BgL_oportz00_67);
										}}}
										{
											obj_t BgL_l1408z00_5503;

											BgL_l1408z00_5503 = CDR(BgL_l1408z00_2550);
											BgL_l1408z00_2550 = BgL_l1408z00_5503;
											goto BgL_zc3z04anonymousza32023ze3z87_2551;
										}
									}
								else
									{	/* BackEnd/c_proto.scm 677 */
										((bool_t) 1);
									}
							}
						}
						return bgl_display_char(((unsigned char) 10), BgL_oportz00_67);
		}}}

	}



/* emit-slot~0 */
	obj_t BGl_emitzd2slotze70z35zzbackend_c_prototypez00(obj_t BgL_oportz00_4111,
		obj_t BgL_slotz00_2560)
	{
		{	/* BackEnd/c_proto.scm 644 */
			{	/* BackEnd/c_proto.scm 639 */
				obj_t BgL_cnamez00_2563;

				{	/* BackEnd/c_proto.scm 639 */
					obj_t BgL_arg2038z00_2569;

					BgL_arg2038z00_2569 =
						(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_2560)))->BgL_typez00);
					BgL_cnamez00_2563 =
						BGl_crosszd2namezd2zzbackend_c_prototypez00(BgL_arg2038z00_2569);
				}
				if (
					((long) CINT(
							(((BgL_slotz00_bglt) COBJECT(
										((BgL_slotz00_bglt) BgL_slotz00_2560)))->
								BgL_virtualzd2numzd2)) >= 0L))
					{	/* BackEnd/c_proto.scm 641 */
						return BUNSPEC;
					}
				else
					{	/* BackEnd/c_proto.scm 641 */
						bgl_display_string(BGl_string2301z00zzbackend_c_prototypez00,
							BgL_oportz00_4111);
						bgl_display_obj(BgL_cnamez00_2563, BgL_oportz00_4111);
						bgl_display_string(BGl_string2302z00zzbackend_c_prototypez00,
							BgL_oportz00_4111);
						{	/* BackEnd/c_proto.scm 644 */
							obj_t BgL_arg2036z00_2567;

							BgL_arg2036z00_2567 =
								(((BgL_slotz00_bglt) COBJECT(
										((BgL_slotz00_bglt) BgL_slotz00_2560)))->BgL_namez00);
							bgl_display_obj(BgL_arg2036z00_2567, BgL_oportz00_4111);
						}
						bgl_display_string(BGl_string2243z00zzbackend_c_prototypez00,
							BgL_oportz00_4111);
						return bgl_display_char(((unsigned char) 10), BgL_oportz00_4111);
		}}}

	}



/* &emit-class-types */
	obj_t BGl_z62emitzd2classzd2typesz62zzbackend_c_prototypez00(obj_t
		BgL_envz00_4069, obj_t BgL_classzd2listzd2_4070, obj_t BgL_oportz00_4071)
	{
		{	/* BackEnd/c_proto.scm 635 */
			return
				BGl_emitzd2classzd2typesz00zzbackend_c_prototypez00
				(BgL_classzd2listzd2_4070, BgL_oportz00_4071);
		}

	}



/* cross-name */
	obj_t BGl_crosszd2namezd2zzbackend_c_prototypez00(obj_t BgL_typez00_68)
	{
		{	/* BackEnd/c_proto.scm 687 */
			{	/* BackEnd/c_proto.scm 688 */
				bool_t BgL_test2536z00_5523;

				{	/* BackEnd/c_proto.scm 688 */
					obj_t BgL_classz00_3692;

					BgL_classz00_3692 = BGl_tclassz00zzobject_classz00;
					if (BGL_OBJECTP(BgL_typez00_68))
						{	/* BackEnd/c_proto.scm 688 */
							BgL_objectz00_bglt BgL_arg1807z00_3694;

							BgL_arg1807z00_3694 = (BgL_objectz00_bglt) (BgL_typez00_68);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* BackEnd/c_proto.scm 688 */
									long BgL_idxz00_3700;

									BgL_idxz00_3700 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3694);
									BgL_test2536z00_5523 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3700 + 2L)) == BgL_classz00_3692);
								}
							else
								{	/* BackEnd/c_proto.scm 688 */
									bool_t BgL_res2219z00_3725;

									{	/* BackEnd/c_proto.scm 688 */
										obj_t BgL_oclassz00_3708;

										{	/* BackEnd/c_proto.scm 688 */
											obj_t BgL_arg1815z00_3716;
											long BgL_arg1816z00_3717;

											BgL_arg1815z00_3716 = (BGl_za2classesza2z00zz__objectz00);
											{	/* BackEnd/c_proto.scm 688 */
												long BgL_arg1817z00_3718;

												BgL_arg1817z00_3718 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3694);
												BgL_arg1816z00_3717 =
													(BgL_arg1817z00_3718 - OBJECT_TYPE);
											}
											BgL_oclassz00_3708 =
												VECTOR_REF(BgL_arg1815z00_3716, BgL_arg1816z00_3717);
										}
										{	/* BackEnd/c_proto.scm 688 */
											bool_t BgL__ortest_1115z00_3709;

											BgL__ortest_1115z00_3709 =
												(BgL_classz00_3692 == BgL_oclassz00_3708);
											if (BgL__ortest_1115z00_3709)
												{	/* BackEnd/c_proto.scm 688 */
													BgL_res2219z00_3725 = BgL__ortest_1115z00_3709;
												}
											else
												{	/* BackEnd/c_proto.scm 688 */
													long BgL_odepthz00_3710;

													{	/* BackEnd/c_proto.scm 688 */
														obj_t BgL_arg1804z00_3711;

														BgL_arg1804z00_3711 = (BgL_oclassz00_3708);
														BgL_odepthz00_3710 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3711);
													}
													if ((2L < BgL_odepthz00_3710))
														{	/* BackEnd/c_proto.scm 688 */
															obj_t BgL_arg1802z00_3713;

															{	/* BackEnd/c_proto.scm 688 */
																obj_t BgL_arg1803z00_3714;

																BgL_arg1803z00_3714 = (BgL_oclassz00_3708);
																BgL_arg1802z00_3713 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3714,
																	2L);
															}
															BgL_res2219z00_3725 =
																(BgL_arg1802z00_3713 == BgL_classz00_3692);
														}
													else
														{	/* BackEnd/c_proto.scm 688 */
															BgL_res2219z00_3725 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2536z00_5523 = BgL_res2219z00_3725;
								}
						}
					else
						{	/* BackEnd/c_proto.scm 688 */
							BgL_test2536z00_5523 = ((bool_t) 0);
						}
				}
				if (BgL_test2536z00_5523)
					{	/* BackEnd/c_proto.scm 689 */
						obj_t BgL_arg2062z00_2613;

						BgL_arg2062z00_2613 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_typez00_68)))->BgL_siza7eza7);
						return
							string_append(BgL_arg2062z00_2613,
							BGl_string2303z00zzbackend_c_prototypez00);
					}
				else
					{	/* BackEnd/c_proto.scm 688 */
						return
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_typez00_68)))->BgL_namez00);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_emitzd2prototypezd2envz00zzbackend_c_prototypez00,
				BGl_proc2304z00zzbackend_c_prototypez00, BGl_valuez00zzast_varz00,
				BGl_string2305z00zzbackend_c_prototypez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_getzd2czd2scopezd2envzd2zzbackend_c_prototypez00,
				BGl_proc2306z00zzbackend_c_prototypez00, BGl_variablez00zzast_varz00,
				BGl_string2307z00zzbackend_c_prototypez00);
		}

	}



/* &get-c-scope1424 */
	obj_t BGl_z62getzd2czd2scope1424z62zzbackend_c_prototypez00(obj_t
		BgL_envz00_4074, obj_t BgL_variablez00_4075)
	{
		{	/* BackEnd/c_proto.scm 609 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(21),
				BGl_string2308z00zzbackend_c_prototypez00,
				((obj_t) ((BgL_variablez00_bglt) BgL_variablez00_4075)));
		}

	}



/* &emit-prototype1411 */
	obj_t BGl_z62emitzd2prototype1411zb0zzbackend_c_prototypez00(obj_t
		BgL_envz00_4076, obj_t BgL_valuez00_4077, obj_t BgL_variablez00_4078)
	{
		{	/* BackEnd/c_proto.scm 106 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(22),
				BGl_string2308z00zzbackend_c_prototypez00,
				((obj_t) ((BgL_valuez00_bglt) BgL_valuez00_4077)));
		}

	}



/* emit-prototype */
	obj_t BGl_emitzd2prototypezd2zzbackend_c_prototypez00(BgL_valuez00_bglt
		BgL_valuez00_14, BgL_variablez00_bglt BgL_variablez00_15)
	{
		{	/* BackEnd/c_proto.scm 106 */
			{	/* BackEnd/c_proto.scm 106 */
				obj_t BgL_method1412z00_2623;

				{	/* BackEnd/c_proto.scm 106 */
					obj_t BgL_res2224z00_3758;

					{	/* BackEnd/c_proto.scm 106 */
						long BgL_objzd2classzd2numz00_3729;

						BgL_objzd2classzd2numz00_3729 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_valuez00_14));
						{	/* BackEnd/c_proto.scm 106 */
							obj_t BgL_arg1811z00_3730;

							BgL_arg1811z00_3730 =
								PROCEDURE_REF
								(BGl_emitzd2prototypezd2envz00zzbackend_c_prototypez00,
								(int) (1L));
							{	/* BackEnd/c_proto.scm 106 */
								int BgL_offsetz00_3733;

								BgL_offsetz00_3733 = (int) (BgL_objzd2classzd2numz00_3729);
								{	/* BackEnd/c_proto.scm 106 */
									long BgL_offsetz00_3734;

									BgL_offsetz00_3734 =
										((long) (BgL_offsetz00_3733) - OBJECT_TYPE);
									{	/* BackEnd/c_proto.scm 106 */
										long BgL_modz00_3735;

										BgL_modz00_3735 =
											(BgL_offsetz00_3734 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/c_proto.scm 106 */
											long BgL_restz00_3737;

											BgL_restz00_3737 =
												(BgL_offsetz00_3734 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/c_proto.scm 106 */

												{	/* BackEnd/c_proto.scm 106 */
													obj_t BgL_bucketz00_3739;

													BgL_bucketz00_3739 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3730), BgL_modz00_3735);
													BgL_res2224z00_3758 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3739), BgL_restz00_3737);
					}}}}}}}}
					BgL_method1412z00_2623 = BgL_res2224z00_3758;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1412z00_2623,
					((obj_t) BgL_valuez00_14), ((obj_t) BgL_variablez00_15));
			}
		}

	}



/* &emit-prototype */
	obj_t BGl_z62emitzd2prototypezb0zzbackend_c_prototypez00(obj_t
		BgL_envz00_4079, obj_t BgL_valuez00_4080, obj_t BgL_variablez00_4081)
	{
		{	/* BackEnd/c_proto.scm 106 */
			return
				BGl_emitzd2prototypezd2zzbackend_c_prototypez00(
				((BgL_valuez00_bglt) BgL_valuez00_4080),
				((BgL_variablez00_bglt) BgL_variablez00_4081));
		}

	}



/* get-c-scope */
	obj_t BGl_getzd2czd2scopez00zzbackend_c_prototypez00(BgL_variablez00_bglt
		BgL_variablez00_63)
	{
		{	/* BackEnd/c_proto.scm 609 */
			{	/* BackEnd/c_proto.scm 609 */
				obj_t BgL_method1425z00_2624;

				{	/* BackEnd/c_proto.scm 609 */
					obj_t BgL_res2229z00_3789;

					{	/* BackEnd/c_proto.scm 609 */
						long BgL_objzd2classzd2numz00_3760;

						BgL_objzd2classzd2numz00_3760 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_variablez00_63));
						{	/* BackEnd/c_proto.scm 609 */
							obj_t BgL_arg1811z00_3761;

							BgL_arg1811z00_3761 =
								PROCEDURE_REF
								(BGl_getzd2czd2scopezd2envzd2zzbackend_c_prototypez00,
								(int) (1L));
							{	/* BackEnd/c_proto.scm 609 */
								int BgL_offsetz00_3764;

								BgL_offsetz00_3764 = (int) (BgL_objzd2classzd2numz00_3760);
								{	/* BackEnd/c_proto.scm 609 */
									long BgL_offsetz00_3765;

									BgL_offsetz00_3765 =
										((long) (BgL_offsetz00_3764) - OBJECT_TYPE);
									{	/* BackEnd/c_proto.scm 609 */
										long BgL_modz00_3766;

										BgL_modz00_3766 =
											(BgL_offsetz00_3765 >> (int) ((long) ((int) (4L))));
										{	/* BackEnd/c_proto.scm 609 */
											long BgL_restz00_3768;

											BgL_restz00_3768 =
												(BgL_offsetz00_3765 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* BackEnd/c_proto.scm 609 */

												{	/* BackEnd/c_proto.scm 609 */
													obj_t BgL_bucketz00_3770;

													BgL_bucketz00_3770 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3761), BgL_modz00_3766);
													BgL_res2229z00_3789 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3770), BgL_restz00_3768);
					}}}}}}}}
					BgL_method1425z00_2624 = BgL_res2229z00_3789;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1425z00_2624,
					((obj_t) BgL_variablez00_63));
			}
		}

	}



/* &get-c-scope */
	obj_t BGl_z62getzd2czd2scopez62zzbackend_c_prototypez00(obj_t BgL_envz00_4082,
		obj_t BgL_variablez00_4083)
	{
		{	/* BackEnd/c_proto.scm 609 */
			return
				BGl_getzd2czd2scopez00zzbackend_c_prototypez00(
				((BgL_variablez00_bglt) BgL_variablez00_4083));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2prototypezd2envz00zzbackend_c_prototypez00,
				BGl_svarz00zzast_varz00, BGl_proc2309z00zzbackend_c_prototypez00,
				BGl_string2310z00zzbackend_c_prototypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2prototypezd2envz00zzbackend_c_prototypez00,
				BGl_scnstz00zzast_varz00, BGl_proc2311z00zzbackend_c_prototypez00,
				BGl_string2310z00zzbackend_c_prototypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2prototypezd2envz00zzbackend_c_prototypez00,
				BGl_sfunz00zzast_varz00, BGl_proc2312z00zzbackend_c_prototypez00,
				BGl_string2310z00zzbackend_c_prototypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2prototypezd2envz00zzbackend_c_prototypez00,
				BGl_cfunz00zzast_varz00, BGl_proc2313z00zzbackend_c_prototypez00,
				BGl_string2310z00zzbackend_c_prototypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2prototypezd2envz00zzbackend_c_prototypez00,
				BGl_cvarz00zzast_varz00, BGl_proc2314z00zzbackend_c_prototypez00,
				BGl_string2310z00zzbackend_c_prototypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2czd2scopezd2envzd2zzbackend_c_prototypez00,
				BGl_globalz00zzast_varz00, BGl_proc2315z00zzbackend_c_prototypez00,
				BGl_string2316z00zzbackend_c_prototypez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2czd2scopezd2envzd2zzbackend_c_prototypez00,
				BGl_localz00zzast_varz00, BGl_proc2317z00zzbackend_c_prototypez00,
				BGl_string2316z00zzbackend_c_prototypez00);
		}

	}



/* &get-c-scope-local1430 */
	obj_t BGl_z62getzd2czd2scopezd2local1430zb0zzbackend_c_prototypez00(obj_t
		BgL_envz00_4091, obj_t BgL_variablez00_4092)
	{
		{	/* BackEnd/c_proto.scm 629 */
			return BGl_string2240z00zzbackend_c_prototypez00;
		}

	}



/* &get-c-scope-global1428 */
	obj_t BGl_z62getzd2czd2scopezd2global1428zb0zzbackend_c_prototypez00(obj_t
		BgL_envz00_4093, obj_t BgL_variablez00_4094)
	{
		{	/* BackEnd/c_proto.scm 614 */
			{	/* BackEnd/c_proto.scm 616 */
				obj_t BgL_scopez00_4163;

				{	/* BackEnd/c_proto.scm 616 */
					obj_t BgL_casezd2valuezd2_4164;

					BgL_casezd2valuezd2_4164 =
						(((BgL_globalz00_bglt) COBJECT(
								((BgL_globalz00_bglt) BgL_variablez00_4094)))->BgL_importz00);
					if ((BgL_casezd2valuezd2_4164 == CNST_TABLE_REF(0)))
						{	/* BackEnd/c_proto.scm 616 */
							BgL_scopez00_4163 = BGl_string2240z00zzbackend_c_prototypez00;
						}
					else
						{	/* BackEnd/c_proto.scm 616 */
							if ((BgL_casezd2valuezd2_4164 == CNST_TABLE_REF(19)))
								{	/* BackEnd/c_proto.scm 616 */
									if (CBOOL(
											(((BgL_globalz00_bglt) COBJECT(
														((BgL_globalz00_bglt) BgL_variablez00_4094)))->
												BgL_libraryz00)))
										{	/* BackEnd/c_proto.scm 618 */
											BgL_scopez00_4163 =
												BGl_string2318z00zzbackend_c_prototypez00;
										}
									else
										{	/* BackEnd/c_proto.scm 618 */
											BgL_scopez00_4163 =
												BGl_string2319z00zzbackend_c_prototypez00;
										}
								}
							else
								{	/* BackEnd/c_proto.scm 616 */
									if ((BgL_casezd2valuezd2_4164 == CNST_TABLE_REF(1)))
										{	/* BackEnd/c_proto.scm 616 */
											BgL_scopez00_4163 =
												BGl_string2320z00zzbackend_c_prototypez00;
										}
									else
										{	/* BackEnd/c_proto.scm 616 */
											BgL_scopez00_4163 =
												BGl_internalzd2errorzd2zztools_errorz00
												(BGl_string2316z00zzbackend_c_prototypez00,
												BGl_string2321z00zzbackend_c_prototypez00,
												(((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
																BgL_variablez00_4094)))->BgL_importz00));
										}
								}
						}
				}
				if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(5),
							(((BgL_globalz00_bglt) COBJECT(
										((BgL_globalz00_bglt) BgL_variablez00_4094)))->
								BgL_pragmaz00))))
					{	/* BackEnd/c_proto.scm 622 */
						return
							string_append(BgL_scopez00_4163,
							BGl_string2322z00zzbackend_c_prototypez00);
					}
				else
					{	/* BackEnd/c_proto.scm 622 */
						return BgL_scopez00_4163;
					}
			}
		}

	}



/* &emit-prototype-cvar1423 */
	obj_t BGl_z62emitzd2prototypezd2cvar1423z62zzbackend_c_prototypez00(obj_t
		BgL_envz00_4095, obj_t BgL_valuez00_4096, obj_t BgL_variablez00_4097)
	{
		{	/* BackEnd/c_proto.scm 256 */
			if (
				(((BgL_cvarz00_bglt) COBJECT(
							((BgL_cvarz00_bglt) BgL_valuez00_4096)))->BgL_macrozf3zf3))
				{	/* BackEnd/c_proto.scm 257 */
					return BFALSE;
				}
			else
				{	/* BackEnd/c_proto.scm 259 */
					obj_t BgL_port1372z00_4166;

					BgL_port1372z00_4166 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c_proto.scm 260 */
						obj_t BgL_arg2101z00_4167;

						if (CBOOL(
								(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_variablez00_4097)))->
									BgL_libraryz00)))
							{	/* BackEnd/c_proto.scm 260 */
								BgL_arg2101z00_4167 = BGl_string2323z00zzbackend_c_prototypez00;
							}
						else
							{	/* BackEnd/c_proto.scm 260 */
								BgL_arg2101z00_4167 = BGl_string2324z00zzbackend_c_prototypez00;
							}
						bgl_display_obj(BgL_arg2101z00_4167, BgL_port1372z00_4166);
					}
					{	/* BackEnd/c_proto.scm 261 */
						obj_t BgL_arg2103z00_4168;

						{	/* BackEnd/c_proto.scm 261 */
							BgL_typez00_bglt BgL_arg2104z00_4169;
							obj_t BgL_arg2105z00_4170;

							BgL_arg2104z00_4169 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt) BgL_variablez00_4097))))->
								BgL_typez00);
							BgL_arg2105z00_4170 =
								(((BgL_variablez00_bglt)
									COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
												BgL_variablez00_4097))))->BgL_namez00);
							BgL_arg2103z00_4168 =
								BGl_makezd2typedzd2declarationz00zztype_toolsz00
								(BgL_arg2104z00_4169, BgL_arg2105z00_4170);
						}
						bgl_display_obj(BgL_arg2103z00_4168, BgL_port1372z00_4166);
					}
					{	/* BackEnd/c_proto.scm 259 */
						obj_t BgL_tmpz00_5676;

						BgL_tmpz00_5676 = ((obj_t) BgL_port1372z00_4166);
						bgl_display_char(((unsigned char) ';'), BgL_tmpz00_5676);
					}
					{	/* BackEnd/c_proto.scm 259 */
						obj_t BgL_tmpz00_5679;

						BgL_tmpz00_5679 = ((obj_t) BgL_port1372z00_4166);
						return bgl_display_char(((unsigned char) 10), BgL_tmpz00_5679);
		}}}

	}



/* &emit-prototype-cfun1420 */
	obj_t BGl_z62emitzd2prototypezd2cfun1420z62zzbackend_c_prototypez00(obj_t
		BgL_envz00_4098, obj_t BgL_valuez00_4099, obj_t BgL_variablez00_4100)
	{
		{	/* BackEnd/c_proto.scm 223 */
			if (
				(((BgL_cfunz00_bglt) COBJECT(
							((BgL_cfunz00_bglt) BgL_valuez00_4099)))->BgL_macrozf3zf3))
				{	/* BackEnd/c_proto.scm 224 */
					return BFALSE;
				}
			else
				{	/* BackEnd/c_proto.scm 226 */
					long BgL_arityz00_4172;

					BgL_arityz00_4172 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_cfunz00_bglt) BgL_valuez00_4099))))->BgL_arityz00);
					{	/* BackEnd/c_proto.scm 226 */
						obj_t BgL_targsz00_4173;

						BgL_targsz00_4173 =
							(((BgL_cfunz00_bglt) COBJECT(
									((BgL_cfunz00_bglt) BgL_valuez00_4099)))->BgL_argszd2typezd2);
						{	/* BackEnd/c_proto.scm 227 */

							{	/* BackEnd/c_proto.scm 228 */
								obj_t BgL_port1371z00_4174;

								BgL_port1371z00_4174 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
								{	/* BackEnd/c_proto.scm 229 */
									obj_t BgL_arg2078z00_4175;

									if (CBOOL(
											(((BgL_globalz00_bglt) COBJECT(
														((BgL_globalz00_bglt) BgL_variablez00_4100)))->
												BgL_libraryz00)))
										{	/* BackEnd/c_proto.scm 229 */
											BgL_arg2078z00_4175 =
												BGl_string2323z00zzbackend_c_prototypez00;
										}
									else
										{	/* BackEnd/c_proto.scm 229 */
											BgL_arg2078z00_4175 =
												BGl_string2324z00zzbackend_c_prototypez00;
										}
									bgl_display_obj(BgL_arg2078z00_4175, BgL_port1371z00_4174);
								}
								{	/* BackEnd/c_proto.scm 230 */
									obj_t BgL_arg2080z00_4176;

									{	/* BackEnd/c_proto.scm 230 */
										BgL_typez00_bglt BgL_arg2081z00_4177;
										obj_t BgL_arg2082z00_4178;

										BgL_arg2081z00_4177 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_variablez00_4100))))->
											BgL_typez00);
										{	/* BackEnd/c_proto.scm 232 */
											obj_t BgL_arg2083z00_4179;
											obj_t BgL_arg2084z00_4180;

											BgL_arg2083z00_4179 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_variablez00_4100))))->
												BgL_namez00);
											if (NULLP(BgL_targsz00_4173))
												{	/* BackEnd/c_proto.scm 236 */
													BgL_arg2084z00_4180 =
														BGl_string2325z00zzbackend_c_prototypez00;
												}
											else
												{	/* BackEnd/c_proto.scm 236 */
													if ((BgL_arityz00_4172 <= -1L))
														{	/* BackEnd/c_proto.scm 238 */
															obj_t BgL_arg2087z00_4181;

															{	/* BackEnd/c_proto.scm 238 */
																obj_t BgL_arg2088z00_4182;

																BgL_arg2088z00_4182 =
																	CAR(((obj_t) BgL_targsz00_4173));
																BgL_arg2087z00_4181 =
																	BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00
																	(((BgL_typez00_bglt) BgL_arg2088z00_4182));
															}
															BgL_arg2084z00_4180 =
																string_append(BgL_arg2087z00_4181,
																BGl_string2326z00zzbackend_c_prototypez00);
														}
													else
														{	/* BackEnd/c_proto.scm 237 */
															BgL_arg2084z00_4180 =
																BGl_loopze70ze7zzbackend_c_prototypez00
																(BgL_arityz00_4172, BgL_targsz00_4173);
														}
												}
											BgL_arg2082z00_4178 =
												string_append_3(BgL_arg2083z00_4179,
												BGl_string2246z00zzbackend_c_prototypez00,
												BgL_arg2084z00_4180);
										}
										BgL_arg2080z00_4176 =
											BGl_makezd2typedzd2declarationz00zztype_toolsz00
											(BgL_arg2081z00_4177, BgL_arg2082z00_4178);
									}
									bgl_display_obj(BgL_arg2080z00_4176, BgL_port1371z00_4174);
								}
								{	/* BackEnd/c_proto.scm 228 */
									obj_t BgL_tmpz00_5714;

									BgL_tmpz00_5714 = ((obj_t) BgL_port1371z00_4174);
									bgl_display_string(BGl_string2243z00zzbackend_c_prototypez00,
										BgL_tmpz00_5714);
								}
								{	/* BackEnd/c_proto.scm 228 */
									obj_t BgL_tmpz00_5717;

									BgL_tmpz00_5717 = ((obj_t) BgL_port1371z00_4174);
									return
										bgl_display_char(((unsigned char) 10), BgL_tmpz00_5717);
		}}}}}}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzbackend_c_prototypez00(long BgL_arityz00_4110,
		obj_t BgL_targsz00_2675)
	{
		{	/* BackEnd/c_proto.scm 241 */
			if (NULLP(CDR(((obj_t) BgL_targsz00_2675))))
				{	/* BackEnd/c_proto.scm 242 */
					if ((BgL_arityz00_4110 < 0L))
						{	/* BackEnd/c_proto.scm 243 */
							return BGl_string2327z00zzbackend_c_prototypez00;
						}
					else
						{	/* BackEnd/c_proto.scm 246 */
							obj_t BgL_arg2093z00_2680;

							{	/* BackEnd/c_proto.scm 246 */
								obj_t BgL_arg2094z00_2681;

								BgL_arg2094z00_2681 = CAR(((obj_t) BgL_targsz00_2675));
								BgL_arg2093z00_2680 =
									BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00(
									((BgL_typez00_bglt) BgL_arg2094z00_2681));
							}
							return
								string_append(BgL_arg2093z00_2680,
								BGl_string2247z00zzbackend_c_prototypez00);
						}
				}
			else
				{	/* BackEnd/c_proto.scm 249 */
					obj_t BgL_arg2095z00_2682;
					obj_t BgL_arg2096z00_2683;

					{	/* BackEnd/c_proto.scm 249 */
						obj_t BgL_arg2097z00_2684;

						BgL_arg2097z00_2684 = CAR(((obj_t) BgL_targsz00_2675));
						BgL_arg2095z00_2682 =
							BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00(
							((BgL_typez00_bglt) BgL_arg2097z00_2684));
					}
					{	/* BackEnd/c_proto.scm 250 */
						obj_t BgL_arg2098z00_2685;

						BgL_arg2098z00_2685 = CDR(((obj_t) BgL_targsz00_2675));
						BgL_arg2096z00_2683 =
							BGl_loopze70ze7zzbackend_c_prototypez00(BgL_arityz00_4110,
							BgL_arg2098z00_2685);
					}
					return
						string_append_3(BgL_arg2095z00_2682,
						BGl_string2248z00zzbackend_c_prototypez00, BgL_arg2096z00_2683);
				}
		}

	}



/* &emit-prototype-sfun1418 */
	obj_t BGl_z62emitzd2prototypezd2sfun1418z62zzbackend_c_prototypez00(obj_t
		BgL_envz00_4101, obj_t BgL_valuez00_4102, obj_t BgL_variablez00_4103)
	{
		{	/* BackEnd/c_proto.scm 173 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_variablez00_4103));
			{	/* BackEnd/c_proto.scm 176 */
				obj_t BgL_port1370z00_4184;

				BgL_port1370z00_4184 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c_proto.scm 177 */
					obj_t BgL_arg2065z00_4185;

					BgL_arg2065z00_4185 =
						BGl_getzd2czd2scopez00zzbackend_c_prototypez00(
						((BgL_variablez00_bglt) BgL_variablez00_4103));
					bgl_display_obj(BgL_arg2065z00_4185, BgL_port1370z00_4184);
				}
				{	/* BackEnd/c_proto.scm 176 */
					obj_t BgL_tmpz00_5744;

					BgL_tmpz00_5744 = ((obj_t) BgL_port1370z00_4184);
					bgl_display_char(((unsigned char) ' '), BgL_tmpz00_5744);
				}
				{	/* BackEnd/c_proto.scm 179 */
					obj_t BgL_arg2067z00_4186;

					{	/* BackEnd/c_proto.scm 179 */
						BgL_typez00_bglt BgL_arg2068z00_4187;
						obj_t BgL_arg2069z00_4188;

						BgL_arg2068z00_4187 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_variablez00_4103)))->BgL_typez00);
						{	/* BackEnd/c_proto.scm 181 */
							obj_t BgL_arg2070z00_4189;
							obj_t BgL_arg2072z00_4190;

							BgL_arg2070z00_4189 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_variablez00_4103)))->
								BgL_namez00);
							{	/* BackEnd/c_proto.scm 183 */
								obj_t BgL_argsz00_4191;

								BgL_argsz00_4191 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_valuez00_4102)))->BgL_argsz00);
								{	/* BackEnd/c_proto.scm 184 */
									bool_t BgL_test2554z00_5753;

									if (PAIRP(BgL_argsz00_4191))
										{	/* BackEnd/c_proto.scm 184 */
											obj_t BgL_arg2076z00_4192;

											BgL_arg2076z00_4192 = CAR(BgL_argsz00_4191);
											{	/* BackEnd/c_proto.scm 184 */
												obj_t BgL_classz00_4193;

												BgL_classz00_4193 = BGl_typez00zztype_typez00;
												if (BGL_OBJECTP(BgL_arg2076z00_4192))
													{	/* BackEnd/c_proto.scm 184 */
														BgL_objectz00_bglt BgL_arg1807z00_4194;

														BgL_arg1807z00_4194 =
															(BgL_objectz00_bglt) (BgL_arg2076z00_4192);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* BackEnd/c_proto.scm 184 */
																long BgL_idxz00_4195;

																BgL_idxz00_4195 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4194);
																BgL_test2554z00_5753 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4195 + 1L)) ==
																	BgL_classz00_4193);
															}
														else
															{	/* BackEnd/c_proto.scm 184 */
																bool_t BgL_res2230z00_4198;

																{	/* BackEnd/c_proto.scm 184 */
																	obj_t BgL_oclassz00_4199;

																	{	/* BackEnd/c_proto.scm 184 */
																		obj_t BgL_arg1815z00_4200;
																		long BgL_arg1816z00_4201;

																		BgL_arg1815z00_4200 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* BackEnd/c_proto.scm 184 */
																			long BgL_arg1817z00_4202;

																			BgL_arg1817z00_4202 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4194);
																			BgL_arg1816z00_4201 =
																				(BgL_arg1817z00_4202 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4199 =
																			VECTOR_REF(BgL_arg1815z00_4200,
																			BgL_arg1816z00_4201);
																	}
																	{	/* BackEnd/c_proto.scm 184 */
																		bool_t BgL__ortest_1115z00_4203;

																		BgL__ortest_1115z00_4203 =
																			(BgL_classz00_4193 == BgL_oclassz00_4199);
																		if (BgL__ortest_1115z00_4203)
																			{	/* BackEnd/c_proto.scm 184 */
																				BgL_res2230z00_4198 =
																					BgL__ortest_1115z00_4203;
																			}
																		else
																			{	/* BackEnd/c_proto.scm 184 */
																				long BgL_odepthz00_4204;

																				{	/* BackEnd/c_proto.scm 184 */
																					obj_t BgL_arg1804z00_4205;

																					BgL_arg1804z00_4205 =
																						(BgL_oclassz00_4199);
																					BgL_odepthz00_4204 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4205);
																				}
																				if ((1L < BgL_odepthz00_4204))
																					{	/* BackEnd/c_proto.scm 184 */
																						obj_t BgL_arg1802z00_4206;

																						{	/* BackEnd/c_proto.scm 184 */
																							obj_t BgL_arg1803z00_4207;

																							BgL_arg1803z00_4207 =
																								(BgL_oclassz00_4199);
																							BgL_arg1802z00_4206 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4207, 1L);
																						}
																						BgL_res2230z00_4198 =
																							(BgL_arg1802z00_4206 ==
																							BgL_classz00_4193);
																					}
																				else
																					{	/* BackEnd/c_proto.scm 184 */
																						BgL_res2230z00_4198 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2554z00_5753 = BgL_res2230z00_4198;
															}
													}
												else
													{	/* BackEnd/c_proto.scm 184 */
														BgL_test2554z00_5753 = ((bool_t) 0);
													}
											}
										}
									else
										{	/* BackEnd/c_proto.scm 184 */
											BgL_test2554z00_5753 = ((bool_t) 0);
										}
									if (BgL_test2554z00_5753)
										{	/* BackEnd/c_proto.scm 184 */
											BgL_arg2072z00_4190 =
												BGl_emitzd2prototypezd2formalzd2typeszd2zzbackend_c_prototypez00
												(BgL_argsz00_4191);
										}
									else
										{	/* BackEnd/c_proto.scm 184 */
											BgL_arg2072z00_4190 =
												BGl_emitzd2prototypezd2formalsz00zzbackend_c_prototypez00
												(BgL_argsz00_4191);
										}
								}
							}
							BgL_arg2069z00_4188 =
								string_append(BgL_arg2070z00_4189, BgL_arg2072z00_4190);
						}
						BgL_arg2067z00_4186 =
							BGl_makezd2typedzd2declarationz00zztype_toolsz00
							(BgL_arg2068z00_4187, BgL_arg2069z00_4188);
					}
					bgl_display_obj(BgL_arg2067z00_4186, BgL_port1370z00_4184);
				}
				{	/* BackEnd/c_proto.scm 176 */
					obj_t BgL_tmpz00_5784;

					BgL_tmpz00_5784 = ((obj_t) BgL_port1370z00_4184);
					bgl_display_string(BGl_string2243z00zzbackend_c_prototypez00,
						BgL_tmpz00_5784);
				}
				{	/* BackEnd/c_proto.scm 176 */
					obj_t BgL_tmpz00_5787;

					BgL_tmpz00_5787 = ((obj_t) BgL_port1370z00_4184);
					return bgl_display_char(((unsigned char) 10), BgL_tmpz00_5787);
		}}}

	}



/* &emit-prototype-scnst1416 */
	obj_t BGl_z62emitzd2prototypezd2scnst1416z62zzbackend_c_prototypez00(obj_t
		BgL_envz00_4104, obj_t BgL_valuez00_4105, obj_t BgL_variablez00_4106)
	{
		{	/* BackEnd/c_proto.scm 117 */
			return
				BGl_emitzd2prototypezf2svarzf2scnstzd2zzbackend_c_prototypez00(
				((BgL_valuez00_bglt)
					((BgL_scnstz00_bglt) BgL_valuez00_4105)), BgL_variablez00_4106);
		}

	}



/* &emit-prototype-svar1414 */
	obj_t BGl_z62emitzd2prototypezd2svar1414z62zzbackend_c_prototypez00(obj_t
		BgL_envz00_4107, obj_t BgL_valuez00_4108, obj_t BgL_variablez00_4109)
	{
		{	/* BackEnd/c_proto.scm 111 */
			return
				BGl_emitzd2prototypezf2svarzf2scnstzd2zzbackend_c_prototypez00(
				((BgL_valuez00_bglt)
					((BgL_svarz00_bglt) BgL_valuez00_4108)), BgL_variablez00_4109);
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_c_prototypez00(void)
	{
		{	/* BackEnd/c_proto.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzast_pragmaz00(156774043L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zztvector_cnstz00(263487728L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzcnst_allocz00(192699986L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzcgen_copz00(529595144L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(474089076L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			BGl_modulezd2initializa7ationz75zzcgen_emitzd2copzd2(371489745L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
			return
				BGl_modulezd2initializa7ationz75zzcnst_nodez00(89013043L,
				BSTRING_TO_STRING(BGl_string2328z00zzbackend_c_prototypez00));
		}

	}

#ifdef __cplusplus
}
#endif
