/*===========================================================================*/
/*   (BackEnd/c.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/c.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_C_TYPE_DEFINITIONS
#define BGL_BACKEND_C_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_cvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}             *BgL_cvmz00_bglt;

	typedef struct BgL_sawcz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}              *BgL_sawcz00_bglt;

	typedef struct BgL_cgenz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}              *BgL_cgenz00_bglt;


#endif													// BGL_BACKEND_C_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31870ze3ze5zzbackend_cz00(obj_t);
	extern obj_t BGl_findzd2librarieszd2zzengine_linkz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbackend_cz00 = BUNSPEC;
	extern obj_t BGl_za2autozd2linkzd2mainza2z00zzengine_paramz00;
	extern obj_t
		BGl_backendzd2compilezd2functionsz00zzbackend_backendz00
		(BgL_backendz00_bglt);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_indentz00zzcc_indentz00(obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2czd2portza2zd2zzbackend_c_emitz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t BGl_addzd2evalzd2libraryz12z12zzmodule_evalz00(obj_t);
	extern obj_t BGl_setzd2backendz12zc0zzbackend_backendz00(obj_t);
	extern obj_t BGl_za2czd2userzd2footza2z00zzengine_paramz00;
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzbackend_cz00(void);
	extern obj_t BGl_za2bigloozd2tmpza2zd2zzengine_paramz00;
	extern obj_t BGl_ldz00zzcc_ldz00(obj_t, bool_t);
	extern obj_t BGl_emitzd2unsafezd2activationz00zzbackend_c_emitz00(void);
	extern obj_t BGl_cgenz00zzbackend_cvmz00;
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_getenvz00zz__osz00(obj_t);
	extern obj_t BGl_za2restzd2argsza2zd2zzengine_paramz00;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_stopzd2emissionz12zc0zzbackend_c_emitz00(void);
	static obj_t BGl_genericzd2initzd2zzbackend_cz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzbackend_cz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_gczd2rootszd2emitz00zzcc_rootsz00(obj_t);
	extern obj_t BGl_emitzd2includezd2zzbackend_c_emitz00(void);
	extern obj_t BGl_emitzd2classzd2typesz00zzbackend_c_prototypez00(obj_t,
		obj_t);
	static obj_t BGl_makezd2tmpzd2filezd2namezd2zzbackend_cz00(void);
	BGL_IMPORT obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_startzd2emissionz12zc0zzbackend_c_emitz00(obj_t);
	extern obj_t BGl_emitzd2prototypeszd2zzbackend_c_prototypez00(void);
	extern obj_t BGl_makezd2tmpzd2mainz00zzengine_linkz00(obj_t, bool_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cczd2compilerzd2zzbackend_cz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzbackend_cz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_cz00(void);
	extern obj_t
		BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(BgL_typez00_bglt);
	extern obj_t
		BGl_emitzd2garbagezd2collectorzd2selectionzd2zzbackend_c_emitz00(void);
	static obj_t BGl_z62backendzd2gczd2initzd2cvm1496zb0zzbackend_cz00(obj_t,
		obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_registerzd2backendz12zc0zzbackend_backendz00(obj_t, obj_t);
	extern obj_t BGl_emitzd2debugzd2activationz00zzbackend_c_emitz00(void);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	static obj_t BGl_z62backendzd2cnstzd2tablezd2n1490zb0zzbackend_cz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_findzd2mainzd2zzengine_linkz00(obj_t);
	static obj_t BGl_czd2walkzd2zzbackend_cz00(BgL_cvmz00_bglt);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62backendzd2compilezd2cvm1486z62zzbackend_cz00(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t);
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	static obj_t BGl_z62buildzd2sawczd2backendz62zzbackend_cz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31603ze3ze5zzbackend_cz00(obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static obj_t BGl_z62backendzd2subtypezf3zd2cvm1494z91zzbackend_cz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62buildzd2cgenzd2backendz62zzbackend_cz00(obj_t);
	BGL_IMPORT obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	static obj_t BGl_z62backendzd2linkzd2cvm1488z62zzbackend_cz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_emitzd2bdbzd2infoz00zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_z62cczd2compilerzb0zzbackend_cz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_compilerz00zzengine_compilerz00(void);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzbackend_cz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_c_compilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_compilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_typezd2occurzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_setrcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_rootsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_ldz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_ccz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_indentz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_mainz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_initz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzprof_emitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbdb_settingz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbdb_emitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_buildz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_evalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_linkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_compilerz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_configurez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t
		BGl_emitzd2dlopenzd2initz00zzbackend_c_emitz00(BgL_globalz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_expandz00zz__expandz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31607ze3ze5zzbackend_cz00(obj_t, obj_t);
	extern obj_t BGl_za2mcozd2suffixza2zd2zzengine_paramz00;
	BGL_IMPORT bool_t fexists(char *);
	extern obj_t BGl_za2profilezd2modeza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_emitzd2mainzd2zzbackend_c_emitz00(void);
	static obj_t BGl_z62backendzd2linkzd2objects1492z62zzbackend_cz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00;
	extern obj_t BGl_getzd2modulezd2initz00zzbackend_initz00(void);
	extern obj_t BGl_bdbzd2settingz12zc0zzbdb_settingz00(void);
	BGL_IMPORT obj_t BGl_suffixz00zz__osz00(obj_t);
	extern obj_t BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00(void);
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_za2unsafezd2libraryza2zd2zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zzbackend_cz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_cz00(void);
	extern bool_t
		BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(BgL_globalz00_bglt);
	extern obj_t BGl_compilerzd2readzd2zzread_readerz00(obj_t);
	extern obj_t BGl_emitzd2headerzd2zzbackend_c_emitz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_cz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	extern obj_t BGl_za2gczd2forcezd2registerzd2rootszf3za2z21zzengine_paramz00;
	static obj_t BGl_gczd2rootszd2initz00zzbackend_cz00(void);
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	extern obj_t BGl_za2srczd2suffixza2zd2zzengine_paramz00;
	extern obj_t BGl_za2ozd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_emitzd2cnstszd2zzbackend_c_prototypez00(void);
	BGL_IMPORT obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	extern obj_t BGl_emitzd2tracezd2activationz00zzbackend_c_emitz00(void);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t
		BGl_typezd2incrementzd2globalz12z12zzast_typezd2occurzd2
		(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_modulezd2ze3idz31zzbackend_cz00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ccz00zzcc_ccz00(obj_t, obj_t, bool_t);
	extern obj_t BGl_sawcz00zzbackend_cvmz00;
	static obj_t BGl_z62zc3z04anonymousza31736ze3ze5zzbackend_cz00(obj_t);
	extern obj_t BGl_forzd2eachzd2typez12z12zztype_envz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_buildzd2sawczd2backendz00zzbackend_cz00(void);
	extern obj_t BGl_cvmz00zzbackend_cvmz00;
	extern obj_t BGl_emitzd2profzd2infoz00zzprof_emitz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzbackend_cz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31754ze3ze5zzbackend_cz00(obj_t);
	extern obj_t BGl_stopzd2onzd2passz00zzengine_passz00(obj_t, obj_t);
	extern obj_t BGl_za2dlopenzd2initza2zd2zzengine_paramz00;
	extern obj_t BGl_za2czd2userzd2headerza2z00zzengine_paramz00;
	extern obj_t BGl_za2czd2debugza2zd2zzengine_paramz00;
	extern obj_t BGl_getzd2classzd2listz00zzobject_classz00(void);
	BGL_EXPORTED_DECL obj_t BGl_buildzd2cgenzd2backendz00zzbackend_cz00(void);
	extern obj_t BGl_usezd2libraryz12zc0zzmodule_alibraryz00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static bool_t BGl_z62zc3z04anonymousza31836ze3ze5zzbackend_cz00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2mainza2z00zzmodule_modulez00;
	static obj_t BGl_z62zc3z04anonymousza31756ze3ze5zzbackend_cz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31586ze3ze5zzbackend_cz00(obj_t);
	static obj_t __cnst[21];


	   
		 
		DEFINE_STRING(BGl_string2041z00zzbackend_cz00,
		BgL_bgl_string2041za700za7za7b2062za7, "backend-cnst-table-name", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2036z00zzbackend_cz00,
		BgL_bgl_za762backendza7d2com2063z00,
		BGl_z62backendzd2compilezd2cvm1486z62zzbackend_cz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2043z00zzbackend_cz00,
		BgL_bgl_string2043za700za7za7b2064za7, "backend-link-objects", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2038z00zzbackend_cz00,
		BgL_bgl_za762backendza7d2lin2065z00,
		BGl_z62backendzd2linkzd2cvm1488z62zzbackend_cz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2045z00zzbackend_cz00,
		BgL_bgl_string2045za700za7za7b2066za7, "backend-subtype?", 16);
	      DEFINE_STRING(BGl_string2047z00zzbackend_cz00,
		BgL_bgl_string2047za700za7za7b2067za7, "backend-gc-init", 15);
	      DEFINE_STRING(BGl_string2048z00zzbackend_cz00,
		BgL_bgl_string2048za700za7za7b2068za7, " -- ", 4);
	      DEFINE_STRING(BGl_string2049z00zzbackend_cz00,
		BgL_bgl_string2049za700za7za7b2069za7, "No source file found", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2040z00zzbackend_cz00,
		BgL_bgl_za762backendza7d2cns2070z00,
		BGl_z62backendzd2cnstzd2tablezd2n1490zb0zzbackend_cz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2042z00zzbackend_cz00,
		BgL_bgl_za762backendza7d2lin2071z00,
		BGl_z62backendzd2linkzd2objects1492z62zzbackend_cz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2050z00zzbackend_cz00,
		BgL_bgl_string2050za700za7za7b2072za7, "link", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2044z00zzbackend_cz00,
		BgL_bgl_za762backendza7d2sub2073z00,
		BGl_z62backendzd2subtypezf3zd2cvm1494z91zzbackend_cz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2051z00zzbackend_cz00,
		BgL_bgl_string2051za700za7za7b2074za7, "\"", 1);
	      DEFINE_STRING(BGl_string2052z00zzbackend_cz00,
		BgL_bgl_string2052za700za7za7b2075za7, "Illegal file", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2046z00zzbackend_cz00,
		BgL_bgl_za762backendza7d2gcza72076za7,
		BGl_z62backendzd2gczd2initzd2cvm1496zb0zzbackend_cz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2053z00zzbackend_cz00,
		BgL_bgl_string2053za700za7za7b2077za7, "*__cnst", 7);
	      DEFINE_STRING(BGl_string2054z00zzbackend_cz00,
		BgL_bgl_string2054za700za7za7b2078za7, "__cnst[ ", 8);
	      DEFINE_STRING(BGl_string2055z00zzbackend_cz00,
		BgL_bgl_string2055za700za7za7b2079za7, " ] ", 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cczd2compilerzd2envz00zzbackend_cz00,
		BgL_bgl_za762ccza7d2compiler2080z00, BGl_z62cczd2compilerzb0zzbackend_cz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2059z00zzbackend_cz00,
		BgL_bgl_string2059za700za7za7b2081za7, "backend_c", 9);
	      DEFINE_STRING(BGl_string2060z00zzbackend_cz00,
		BgL_bgl_string2060za700za7za7b2082za7,
		"cindent distrib cgen module eval library so (pragma \"BGL_GC_INIT()\") done cc have-dlopen imported (ld distrib) pass-started ((lambda () (start-emission! \".c\"))) native (c bdb module) (foreign extern) bigloo-c c-saw c ",
		217);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2056z00zzbackend_cz00,
		BgL_bgl_za762za7c3za704anonymo2083za7,
		BGl_z62zc3z04anonymousza31754ze3ze5zzbackend_cz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2057z00zzbackend_cz00,
		BgL_bgl_za762za7c3za704anonymo2084za7,
		BGl_z62zc3z04anonymousza31756ze3ze5zzbackend_cz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2058z00zzbackend_cz00,
		BgL_bgl_za762za7c3za704anonymo2085za7,
		BGl_z62zc3z04anonymousza31762ze3ze5zzbackend_cz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_backendzd2compilezd2envz00zzbackend_backendz00;
	extern obj_t BGl_backendzd2linkzd2envz00zzbackend_backendz00;
	extern obj_t BGl_backendzd2linkzd2objectszd2envzd2zzbackend_backendz00;
	extern obj_t BGl_backendzd2cnstzd2tablezd2namezd2envz00zzbackend_backendz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_buildzd2cgenzd2backendzd2envzd2zzbackend_cz00,
		BgL_bgl_za762buildza7d2cgenza72086za7,
		BGl_z62buildzd2cgenzd2backendz62zzbackend_cz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_backendzd2subtypezf3zd2envzf3zzbackend_backendz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_buildzd2sawczd2backendzd2envzd2zzbackend_cz00,
		BgL_bgl_za762buildza7d2sawcza72087za7,
		BGl_z62buildzd2sawczd2backendz62zzbackend_cz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2009z00zzbackend_cz00,
		BgL_bgl_string2009za700za7za7b2088za7, "dummy", 5);
	extern obj_t BGl_backendzd2gczd2initzd2envzd2zzbackend_backendz00;
	   
		 
		DEFINE_STRING(BGl_string2010z00zzbackend_cz00,
		BgL_bgl_string2010za700za7za7b2089za7, "heap", 4);
	      DEFINE_STRING(BGl_string2011z00zzbackend_cz00,
		BgL_bgl_string2011za700za7za7b2090za7, "C generation (saw)", 18);
	      DEFINE_STRING(BGl_string2012z00zzbackend_cz00,
		BgL_bgl_string2012za700za7za7b2091za7, "C generation (cgen)", 19);
	      DEFINE_STRING(BGl_string2013z00zzbackend_cz00,
		BgL_bgl_string2013za700za7za7b2092za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2015z00zzbackend_cz00,
		BgL_bgl_string2015za700za7za7b2093za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2016z00zzbackend_cz00,
		BgL_bgl_string2016za700za7za7b2094za7, "/* User header */", 17);
	      DEFINE_STRING(BGl_string2017z00zzbackend_cz00,
		BgL_bgl_string2017za700za7za7b2095za7, "#ifdef __cplusplus", 18);
	      DEFINE_STRING(BGl_string2018z00zzbackend_cz00,
		BgL_bgl_string2018za700za7za7b2096za7, "extern \"C\" {", 12);
	      DEFINE_STRING(BGl_string2019z00zzbackend_cz00,
		BgL_bgl_string2019za700za7za7b2097za7, "#endif", 6);
	      DEFINE_STRING(BGl_string2020z00zzbackend_cz00,
		BgL_bgl_string2020za700za7za7b2098za7,
		"#ifndef BGL_MODULE_TYPE_DEFINITIONS\n", 36);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2014z00zzbackend_cz00,
		BgL_bgl_za762za7c3za704anonymo2099za7,
		BGl_z62zc3z04anonymousza31586ze3ze5zzbackend_cz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2021z00zzbackend_cz00,
		BgL_bgl_string2021za700za7za7b2100za7,
		"#define BGL_MODULE_TYPE_DEFINITIONS\n", 36);
	      DEFINE_STRING(BGl_string2022z00zzbackend_cz00,
		BgL_bgl_string2022za700za7za7b2101za7, "#ifndef BGL_~a_TYPE_DEFINITIONS\n",
		32);
	      DEFINE_STRING(BGl_string2023z00zzbackend_cz00,
		BgL_bgl_string2023za700za7za7b2102za7, "#define BGL_~a_TYPE_DEFINITIONS\n",
		32);
	      DEFINE_STRING(BGl_string2026z00zzbackend_cz00,
		BgL_bgl_string2026za700za7za7b2103za7,
		"#endif // BGL_~a_TYPE_DEFINITIONS\n", 34);
	      DEFINE_STRING(BGl_string2027z00zzbackend_cz00,
		BgL_bgl_string2027za700za7za7b2104za7,
		"#endif // BGL_MODULE_TYPE_DEFINITIONS\n", 38);
	      DEFINE_STRING(BGl_string2028z00zzbackend_cz00,
		BgL_bgl_string2028za700za7za7b2105za7, "}", 1);
	      DEFINE_STRING(BGl_string2029z00zzbackend_cz00,
		BgL_bgl_string2029za700za7za7b2106za7, ".c", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2024z00zzbackend_cz00,
		BgL_bgl_za762za7c3za704anonymo2107za7,
		BGl_z62zc3z04anonymousza31603ze3ze5zzbackend_cz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2031z00zzbackend_cz00,
		BgL_bgl_string2031za700za7za7b2108za7, "USER", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2025z00zzbackend_cz00,
		BgL_bgl_za762za7c3za704anonymo2109za7,
		BGl_z62zc3z04anonymousza31607ze3ze5zzbackend_cz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2032z00zzbackend_cz00,
		BgL_bgl_string2032za700za7za7b2110za7, "", 0);
	      DEFINE_STRING(BGl_string2033z00zzbackend_cz00,
		BgL_bgl_string2033za700za7za7b2111za7, ".", 1);
	      DEFINE_STRING(BGl_string2034z00zzbackend_cz00,
		BgL_bgl_string2034za700za7za7b2112za7, "@", 1);
	      DEFINE_STRING(BGl_string2035z00zzbackend_cz00,
		BgL_bgl_string2035za700za7za7b2113za7, "main-tmp", 8);
	      DEFINE_STRING(BGl_string2037z00zzbackend_cz00,
		BgL_bgl_string2037za700za7za7b2114za7, "backend-compile", 15);
	      DEFINE_STRING(BGl_string2039z00zzbackend_cz00,
		BgL_bgl_string2039za700za7za7b2115za7, "backend-link", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2030z00zzbackend_cz00,
		BgL_bgl_za762za7c3za704anonymo2116za7,
		BGl_z62zc3z04anonymousza31736ze3ze5zzbackend_cz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzbackend_cz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzbackend_cz00(long
		BgL_checksumz00_3675, char *BgL_fromz00_3676)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_cz00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_cz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_cz00();
					BGl_libraryzd2moduleszd2initz00zzbackend_cz00();
					BGl_cnstzd2initzd2zzbackend_cz00();
					BGl_importedzd2moduleszd2initz00zzbackend_cz00();
					BGl_methodzd2initzd2zzbackend_cz00();
					return BGl_toplevelzd2initzd2zzbackend_cz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"backend_c");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__expandz00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__configurez00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"backend_c");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "backend_c");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_c");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 15 */
			{	/* BackEnd/c.scm 15 */
				obj_t BgL_cportz00_3497;

				{	/* BackEnd/c.scm 15 */
					obj_t BgL_stringz00_3504;

					BgL_stringz00_3504 = BGl_string2060z00zzbackend_cz00;
					{	/* BackEnd/c.scm 15 */
						obj_t BgL_startz00_3505;

						BgL_startz00_3505 = BINT(0L);
						{	/* BackEnd/c.scm 15 */
							obj_t BgL_endz00_3506;

							BgL_endz00_3506 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3504)));
							{	/* BackEnd/c.scm 15 */

								BgL_cportz00_3497 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3504, BgL_startz00_3505, BgL_endz00_3506);
				}}}}
				{
					long BgL_iz00_3498;

					BgL_iz00_3498 = 20L;
				BgL_loopz00_3499:
					if ((BgL_iz00_3498 == -1L))
						{	/* BackEnd/c.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/c.scm 15 */
							{	/* BackEnd/c.scm 15 */
								obj_t BgL_arg2061z00_3500;

								{	/* BackEnd/c.scm 15 */

									{	/* BackEnd/c.scm 15 */
										obj_t BgL_locationz00_3502;

										BgL_locationz00_3502 = BBOOL(((bool_t) 0));
										{	/* BackEnd/c.scm 15 */

											BgL_arg2061z00_3500 =
												BGl_readz00zz__readerz00(BgL_cportz00_3497,
												BgL_locationz00_3502);
										}
									}
								}
								{	/* BackEnd/c.scm 15 */
									int BgL_tmpz00_3712;

									BgL_tmpz00_3712 = (int) (BgL_iz00_3498);
									CNST_TABLE_SET(BgL_tmpz00_3712, BgL_arg2061z00_3500);
							}}
							{	/* BackEnd/c.scm 15 */
								int BgL_auxz00_3503;

								BgL_auxz00_3503 = (int) ((BgL_iz00_3498 - 1L));
								{
									long BgL_iz00_3717;

									BgL_iz00_3717 = (long) (BgL_auxz00_3503);
									BgL_iz00_3498 = BgL_iz00_3717;
									goto BgL_loopz00_3499;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 15 */
			BGl_registerzd2backendz12zc0zzbackend_backendz00(CNST_TABLE_REF(0),
				BGl_buildzd2cgenzd2backendzd2envzd2zzbackend_cz00);
			BGl_registerzd2backendz12zc0zzbackend_backendz00(CNST_TABLE_REF(1),
				BGl_buildzd2sawczd2backendzd2envzd2zzbackend_cz00);
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzbackend_cz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2067;

				BgL_headz00_2067 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2068;
					obj_t BgL_tailz00_2069;

					BgL_prevz00_2068 = BgL_headz00_2067;
					BgL_tailz00_2069 = BgL_l1z00_1;
				BgL_loopz00_2070:
					if (PAIRP(BgL_tailz00_2069))
						{
							obj_t BgL_newzd2prevzd2_2072;

							BgL_newzd2prevzd2_2072 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2069), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2068, BgL_newzd2prevzd2_2072);
							{
								obj_t BgL_tailz00_3731;
								obj_t BgL_prevz00_3730;

								BgL_prevz00_3730 = BgL_newzd2prevzd2_2072;
								BgL_tailz00_3731 = CDR(BgL_tailz00_2069);
								BgL_tailz00_2069 = BgL_tailz00_3731;
								BgL_prevz00_2068 = BgL_prevz00_3730;
								goto BgL_loopz00_2070;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2067);
				}
			}
		}

	}



/* build-sawc-backend */
	BGL_EXPORTED_DEF obj_t BGl_buildzd2sawczd2backendz00zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 75 */
			{	/* BackEnd/c.scm 76 */
				BgL_sawcz00_bglt BgL_new1120z00_2089;

				{	/* BackEnd/c.scm 77 */
					BgL_sawcz00_bglt BgL_new1119z00_2091;

					BgL_new1119z00_2091 =
						((BgL_sawcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sawcz00_bgl))));
					{	/* BackEnd/c.scm 77 */
						long BgL_arg1544z00_2092;

						BgL_arg1544z00_2092 = BGL_CLASS_NUM(BGl_sawcz00zzbackend_cvmz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1119z00_2091), BgL_arg1544z00_2092);
					}
					BgL_new1120z00_2089 = BgL_new1119z00_2091;
				}
				((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_new1120z00_2089)))->
						BgL_languagez00) = ((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_namez00) =
					((obj_t) BGl_string2009z00zzbackend_cz00), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_externzd2variableszd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_externzd2functionszd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_externzd2typeszd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_variablesz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_functionsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_typesz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_typedz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_heapzd2suffixzd2) =
					((obj_t) BGl_string2010z00zzbackend_cz00), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_heapzd2compatiblezd2) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_callccz00) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_qualifiedzd2typeszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_effectzb2zb2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_removezd2emptyzd2letz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_foreignzd2closurezd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_typedzd2eqzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_tracezd2supportzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_foreignzd2clausezd2supportz00) =
					((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_debugzd2supportzd2) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_pragmazd2supportzd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_requirezd2tailczd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_registersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_pregistersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_boundzd2checkzd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_typezd2checkzd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_typedzd2funcallzd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_strictzd2typezd2castz00) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1120z00_2089)))->BgL_stringzd2literalzd2supportz00) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				{	/* BackEnd/c.scm 76 */
					obj_t BgL_fun1541z00_2090;

					BgL_fun1541z00_2090 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_sawcz00zzbackend_cvmz00);
					BGL_PROCEDURE_CALL1(BgL_fun1541z00_2090,
						((obj_t) BgL_new1120z00_2089));
				}
				return ((obj_t) BgL_new1120z00_2089);
			}
		}

	}



/* &build-sawc-backend */
	obj_t BGl_z62buildzd2sawczd2backendz62zzbackend_cz00(obj_t BgL_envz00_3430)
	{
		{	/* BackEnd/c.scm 75 */
			return BGl_buildzd2sawczd2backendz00zzbackend_cz00();
		}

	}



/* build-cgen-backend */
	BGL_EXPORTED_DEF obj_t BGl_buildzd2cgenzd2backendz00zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 87 */
			{	/* BackEnd/c.scm 88 */
				BgL_cgenz00_bglt BgL_new1123z00_2093;

				{	/* BackEnd/c.scm 89 */
					BgL_cgenz00_bglt BgL_new1122z00_2095;

					BgL_new1122z00_2095 =
						((BgL_cgenz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cgenz00_bgl))));
					{	/* BackEnd/c.scm 89 */
						long BgL_arg1546z00_2096;

						BgL_arg1546z00_2096 = BGL_CLASS_NUM(BGl_cgenz00zzbackend_cvmz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1122z00_2095), BgL_arg1546z00_2096);
					}
					BgL_new1123z00_2093 = BgL_new1122z00_2095;
				}
				((((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_new1123z00_2093)))->
						BgL_languagez00) = ((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_namez00) =
					((obj_t) BGl_string2009z00zzbackend_cz00), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_externzd2variableszd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_externzd2functionszd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_externzd2typeszd2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_variablesz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_functionsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_typesz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_typedz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_heapzd2suffixzd2) =
					((obj_t) BGl_string2010z00zzbackend_cz00), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_heapzd2compatiblezd2) =
					((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_callccz00) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_qualifiedzd2typeszd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_effectzb2zb2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_removezd2emptyzd2letz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_foreignzd2closurezd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_typedzd2eqzd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_tracezd2supportzd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_foreignzd2clausezd2supportz00) =
					((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_debugzd2supportzd2) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_pragmazd2supportzd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_tvectorzd2descrzd2supportz00) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_requirezd2tailczd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_registersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_pregistersz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_boundzd2checkzd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_typezd2checkzd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_typedzd2funcallzd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_strictzd2typezd2castz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->
						BgL_forcezd2registerzd2gczd2rootszd2) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
									BgL_new1123z00_2093)))->BgL_stringzd2literalzd2supportz00) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				{	/* BackEnd/c.scm 88 */
					obj_t BgL_fun1545z00_2094;

					BgL_fun1545z00_2094 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_cgenz00zzbackend_cvmz00);
					BGL_PROCEDURE_CALL1(BgL_fun1545z00_2094,
						((obj_t) BgL_new1123z00_2093));
				}
				return ((obj_t) BgL_new1123z00_2093);
			}
		}

	}



/* &build-cgen-backend */
	obj_t BGl_z62buildzd2cgenzd2backendz62zzbackend_cz00(obj_t BgL_envz00_3429)
	{
		{	/* BackEnd/c.scm 87 */
			return BGl_buildzd2cgenzd2backendz00zzbackend_cz00();
		}

	}



/* module->id */
	obj_t BGl_modulezd2ze3idz31zzbackend_cz00(obj_t BgL_modz00_28)
	{
		{	/* BackEnd/c.scm 114 */
			{	/* BackEnd/c.scm 115 */
				obj_t BgL_namez00_2097;

				{	/* BackEnd/c.scm 115 */
					obj_t BgL_arg1552z00_2099;

					BgL_arg1552z00_2099 = SYMBOL_TO_STRING(((obj_t) BgL_modz00_28));
					BgL_namez00_2097 =
						BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(BgL_arg1552z00_2099);
				}
				if (BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(BgL_namez00_2097))
					{	/* BackEnd/c.scm 116 */
						return bigloo_mangle(BgL_namez00_2097);
					}
				else
					{	/* BackEnd/c.scm 116 */
						return BgL_namez00_2097;
					}
			}
		}

	}



/* c-walk */
	obj_t BGl_czd2walkzd2zzbackend_cz00(BgL_cvmz00_bglt BgL_mez00_29)
	{
		{	/* BackEnd/c.scm 123 */
			{	/* BackEnd/c.scm 124 */
				obj_t BgL_arg1553z00_2100;

				{	/* BackEnd/c.scm 124 */
					bool_t BgL_test2121z00_3902;

					{	/* BackEnd/c.scm 124 */
						obj_t BgL_classz00_2860;

						BgL_classz00_2860 = BGl_sawcz00zzbackend_cvmz00;
						{	/* BackEnd/c.scm 124 */
							BgL_objectz00_bglt BgL_arg1807z00_2862;

							{	/* BackEnd/c.scm 124 */
								obj_t BgL_tmpz00_3903;

								BgL_tmpz00_3903 = ((obj_t) BgL_mez00_29);
								BgL_arg1807z00_2862 = (BgL_objectz00_bglt) (BgL_tmpz00_3903);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* BackEnd/c.scm 124 */
									long BgL_idxz00_2868;

									BgL_idxz00_2868 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2862);
									BgL_test2121z00_3902 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2868 + 3L)) == BgL_classz00_2860);
								}
							else
								{	/* BackEnd/c.scm 124 */
									bool_t BgL_res1981z00_2893;

									{	/* BackEnd/c.scm 124 */
										obj_t BgL_oclassz00_2876;

										{	/* BackEnd/c.scm 124 */
											obj_t BgL_arg1815z00_2884;
											long BgL_arg1816z00_2885;

											BgL_arg1815z00_2884 = (BGl_za2classesza2z00zz__objectz00);
											{	/* BackEnd/c.scm 124 */
												long BgL_arg1817z00_2886;

												BgL_arg1817z00_2886 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2862);
												BgL_arg1816z00_2885 =
													(BgL_arg1817z00_2886 - OBJECT_TYPE);
											}
											BgL_oclassz00_2876 =
												VECTOR_REF(BgL_arg1815z00_2884, BgL_arg1816z00_2885);
										}
										{	/* BackEnd/c.scm 124 */
											bool_t BgL__ortest_1115z00_2877;

											BgL__ortest_1115z00_2877 =
												(BgL_classz00_2860 == BgL_oclassz00_2876);
											if (BgL__ortest_1115z00_2877)
												{	/* BackEnd/c.scm 124 */
													BgL_res1981z00_2893 = BgL__ortest_1115z00_2877;
												}
											else
												{	/* BackEnd/c.scm 124 */
													long BgL_odepthz00_2878;

													{	/* BackEnd/c.scm 124 */
														obj_t BgL_arg1804z00_2879;

														BgL_arg1804z00_2879 = (BgL_oclassz00_2876);
														BgL_odepthz00_2878 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2879);
													}
													if ((3L < BgL_odepthz00_2878))
														{	/* BackEnd/c.scm 124 */
															obj_t BgL_arg1802z00_2881;

															{	/* BackEnd/c.scm 124 */
																obj_t BgL_arg1803z00_2882;

																BgL_arg1803z00_2882 = (BgL_oclassz00_2876);
																BgL_arg1802z00_2881 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2882,
																	3L);
															}
															BgL_res1981z00_2893 =
																(BgL_arg1802z00_2881 == BgL_classz00_2860);
														}
													else
														{	/* BackEnd/c.scm 124 */
															BgL_res1981z00_2893 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2121z00_3902 = BgL_res1981z00_2893;
								}
						}
					}
					if (BgL_test2121z00_3902)
						{	/* BackEnd/c.scm 124 */
							BgL_arg1553z00_2100 = BGl_string2011z00zzbackend_cz00;
						}
					else
						{	/* BackEnd/c.scm 124 */
							BgL_arg1553z00_2100 = BGl_string2012z00zzbackend_cz00;
						}
				}
				{	/* BackEnd/c.scm 124 */
					obj_t BgL_list1554z00_2101;

					{	/* BackEnd/c.scm 124 */
						obj_t BgL_arg1559z00_2102;

						{	/* BackEnd/c.scm 124 */
							obj_t BgL_arg1561z00_2103;

							BgL_arg1561z00_2103 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1559z00_2102 =
								MAKE_YOUNG_PAIR(BgL_arg1553z00_2100, BgL_arg1561z00_2103);
						}
						BgL_list1554z00_2101 =
							MAKE_YOUNG_PAIR(BGl_string2013z00zzbackend_cz00,
							BgL_arg1559z00_2102);
					}
					BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1554z00_2101);
			}}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			{	/* BackEnd/c.scm 124 */
				bool_t BgL_test2125z00_3932;

				{	/* BackEnd/c.scm 124 */
					obj_t BgL_classz00_2894;

					BgL_classz00_2894 = BGl_sawcz00zzbackend_cvmz00;
					{	/* BackEnd/c.scm 124 */
						BgL_objectz00_bglt BgL_arg1807z00_2896;

						{	/* BackEnd/c.scm 124 */
							obj_t BgL_tmpz00_3933;

							BgL_tmpz00_3933 = ((obj_t) BgL_mez00_29);
							BgL_arg1807z00_2896 = (BgL_objectz00_bglt) (BgL_tmpz00_3933);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* BackEnd/c.scm 124 */
								long BgL_idxz00_2902;

								BgL_idxz00_2902 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2896);
								BgL_test2125z00_3932 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2902 + 3L)) == BgL_classz00_2894);
							}
						else
							{	/* BackEnd/c.scm 124 */
								bool_t BgL_res1982z00_2927;

								{	/* BackEnd/c.scm 124 */
									obj_t BgL_oclassz00_2910;

									{	/* BackEnd/c.scm 124 */
										obj_t BgL_arg1815z00_2918;
										long BgL_arg1816z00_2919;

										BgL_arg1815z00_2918 = (BGl_za2classesza2z00zz__objectz00);
										{	/* BackEnd/c.scm 124 */
											long BgL_arg1817z00_2920;

											BgL_arg1817z00_2920 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2896);
											BgL_arg1816z00_2919 = (BgL_arg1817z00_2920 - OBJECT_TYPE);
										}
										BgL_oclassz00_2910 =
											VECTOR_REF(BgL_arg1815z00_2918, BgL_arg1816z00_2919);
									}
									{	/* BackEnd/c.scm 124 */
										bool_t BgL__ortest_1115z00_2911;

										BgL__ortest_1115z00_2911 =
											(BgL_classz00_2894 == BgL_oclassz00_2910);
										if (BgL__ortest_1115z00_2911)
											{	/* BackEnd/c.scm 124 */
												BgL_res1982z00_2927 = BgL__ortest_1115z00_2911;
											}
										else
											{	/* BackEnd/c.scm 124 */
												long BgL_odepthz00_2912;

												{	/* BackEnd/c.scm 124 */
													obj_t BgL_arg1804z00_2913;

													BgL_arg1804z00_2913 = (BgL_oclassz00_2910);
													BgL_odepthz00_2912 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2913);
												}
												if ((3L < BgL_odepthz00_2912))
													{	/* BackEnd/c.scm 124 */
														obj_t BgL_arg1802z00_2915;

														{	/* BackEnd/c.scm 124 */
															obj_t BgL_arg1803z00_2916;

															BgL_arg1803z00_2916 = (BgL_oclassz00_2910);
															BgL_arg1802z00_2915 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2916,
																3L);
														}
														BgL_res1982z00_2927 =
															(BgL_arg1802z00_2915 == BgL_classz00_2894);
													}
												else
													{	/* BackEnd/c.scm 124 */
														BgL_res1982z00_2927 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2125z00_3932 = BgL_res1982z00_2927;
							}
					}
				}
				if (BgL_test2125z00_3932)
					{	/* BackEnd/c.scm 124 */
						BGl_za2currentzd2passza2zd2zzengine_passz00 =
							BGl_string2011z00zzbackend_cz00;
					}
				else
					{	/* BackEnd/c.scm 124 */
						BGl_za2currentzd2passza2zd2zzengine_passz00 =
							BGl_string2012z00zzbackend_cz00;
					}
			}
			{	/* BackEnd/c.scm 124 */
				obj_t BgL_g1124z00_2106;
				obj_t BgL_g1125z00_2107;

				{	/* BackEnd/c.scm 124 */
					obj_t BgL_list1585z00_2123;

					BgL_list1585z00_2123 =
						MAKE_YOUNG_PAIR(BGl_proc2014z00zzbackend_cz00, BNIL);
					BgL_g1124z00_2106 = BgL_list1585z00_2123;
				}
				BgL_g1125z00_2107 = CNST_TABLE_REF(6);
				{
					obj_t BgL_hooksz00_2109;
					obj_t BgL_hnamesz00_2110;

					BgL_hooksz00_2109 = BgL_g1124z00_2106;
					BgL_hnamesz00_2110 = BgL_g1125z00_2107;
				BgL_zc3z04anonymousza31563ze3z87_2111:
					if (NULLP(BgL_hooksz00_2109))
						{	/* BackEnd/c.scm 124 */
							CNST_TABLE_REF(7);
						}
					else
						{	/* BackEnd/c.scm 124 */
							bool_t BgL_test2130z00_3960;

							{	/* BackEnd/c.scm 124 */
								obj_t BgL_fun1577z00_2120;

								BgL_fun1577z00_2120 = CAR(((obj_t) BgL_hooksz00_2109));
								BgL_test2130z00_3960 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1577z00_2120));
							}
							if (BgL_test2130z00_3960)
								{	/* BackEnd/c.scm 124 */
									obj_t BgL_arg1571z00_2115;
									obj_t BgL_arg1573z00_2116;

									BgL_arg1571z00_2115 = CDR(((obj_t) BgL_hooksz00_2109));
									BgL_arg1573z00_2116 = CDR(((obj_t) BgL_hnamesz00_2110));
									{
										obj_t BgL_hnamesz00_3972;
										obj_t BgL_hooksz00_3971;

										BgL_hooksz00_3971 = BgL_arg1571z00_2115;
										BgL_hnamesz00_3972 = BgL_arg1573z00_2116;
										BgL_hnamesz00_2110 = BgL_hnamesz00_3972;
										BgL_hooksz00_2109 = BgL_hooksz00_3971;
										goto BgL_zc3z04anonymousza31563ze3z87_2111;
									}
								}
							else
								{	/* BackEnd/c.scm 124 */
									obj_t BgL_arg1575z00_2117;
									obj_t BgL_arg1576z00_2118;

									{	/* BackEnd/c.scm 124 */
										bool_t BgL_test2131z00_3973;

										{	/* BackEnd/c.scm 124 */
											obj_t BgL_classz00_2932;

											BgL_classz00_2932 = BGl_sawcz00zzbackend_cvmz00;
											{	/* BackEnd/c.scm 124 */
												BgL_objectz00_bglt BgL_arg1807z00_2934;

												{	/* BackEnd/c.scm 124 */
													obj_t BgL_tmpz00_3974;

													BgL_tmpz00_3974 = ((obj_t) BgL_mez00_29);
													BgL_arg1807z00_2934 =
														(BgL_objectz00_bglt) (BgL_tmpz00_3974);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* BackEnd/c.scm 124 */
														long BgL_idxz00_2940;

														BgL_idxz00_2940 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2934);
														BgL_test2131z00_3973 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2940 + 3L)) == BgL_classz00_2932);
													}
												else
													{	/* BackEnd/c.scm 124 */
														bool_t BgL_res1984z00_2965;

														{	/* BackEnd/c.scm 124 */
															obj_t BgL_oclassz00_2948;

															{	/* BackEnd/c.scm 124 */
																obj_t BgL_arg1815z00_2956;
																long BgL_arg1816z00_2957;

																BgL_arg1815z00_2956 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* BackEnd/c.scm 124 */
																	long BgL_arg1817z00_2958;

																	BgL_arg1817z00_2958 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2934);
																	BgL_arg1816z00_2957 =
																		(BgL_arg1817z00_2958 - OBJECT_TYPE);
																}
																BgL_oclassz00_2948 =
																	VECTOR_REF(BgL_arg1815z00_2956,
																	BgL_arg1816z00_2957);
															}
															{	/* BackEnd/c.scm 124 */
																bool_t BgL__ortest_1115z00_2949;

																BgL__ortest_1115z00_2949 =
																	(BgL_classz00_2932 == BgL_oclassz00_2948);
																if (BgL__ortest_1115z00_2949)
																	{	/* BackEnd/c.scm 124 */
																		BgL_res1984z00_2965 =
																			BgL__ortest_1115z00_2949;
																	}
																else
																	{	/* BackEnd/c.scm 124 */
																		long BgL_odepthz00_2950;

																		{	/* BackEnd/c.scm 124 */
																			obj_t BgL_arg1804z00_2951;

																			BgL_arg1804z00_2951 =
																				(BgL_oclassz00_2948);
																			BgL_odepthz00_2950 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2951);
																		}
																		if ((3L < BgL_odepthz00_2950))
																			{	/* BackEnd/c.scm 124 */
																				obj_t BgL_arg1802z00_2953;

																				{	/* BackEnd/c.scm 124 */
																					obj_t BgL_arg1803z00_2954;

																					BgL_arg1803z00_2954 =
																						(BgL_oclassz00_2948);
																					BgL_arg1802z00_2953 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2954, 3L);
																				}
																				BgL_res1984z00_2965 =
																					(BgL_arg1802z00_2953 ==
																					BgL_classz00_2932);
																			}
																		else
																			{	/* BackEnd/c.scm 124 */
																				BgL_res1984z00_2965 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2131z00_3973 = BgL_res1984z00_2965;
													}
											}
										}
										if (BgL_test2131z00_3973)
											{	/* BackEnd/c.scm 124 */
												BgL_arg1575z00_2117 = BGl_string2011z00zzbackend_cz00;
											}
										else
											{	/* BackEnd/c.scm 124 */
												BgL_arg1575z00_2117 = BGl_string2012z00zzbackend_cz00;
											}
									}
									BgL_arg1576z00_2118 = CAR(((obj_t) BgL_hnamesz00_2110));
									BGl_internalzd2errorzd2zztools_errorz00(BgL_arg1575z00_2117,
										BGl_string2015z00zzbackend_cz00, BgL_arg1576z00_2118);
								}
						}
				}
			}
			BGl_emitzd2headerzd2zzbackend_c_emitz00();
			BGl_emitzd2garbagezd2collectorzd2selectionzd2zzbackend_c_emitz00();
			{	/* BackEnd/c.scm 134 */
				bool_t BgL_test2135z00_4001;

				if (((long) CINT(BGl_za2compilerzd2debugza2zd2zzengine_paramz00) > 0L))
					{	/* BackEnd/c.scm 134 */
						BgL_test2135z00_4001 = ((bool_t) 1);
					}
				else
					{	/* BackEnd/c.scm 134 */
						BgL_test2135z00_4001 =
							CBOOL(BGl_za2czd2debugza2zd2zzengine_paramz00);
					}
				if (BgL_test2135z00_4001)
					{	/* BackEnd/c.scm 134 */
						BGl_emitzd2debugzd2activationz00zzbackend_c_emitz00();
					}
				else
					{	/* BackEnd/c.scm 134 */
						BFALSE;
					}
			}
			if (CBOOL(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
				{	/* BackEnd/c.scm 138 */
					BGl_emitzd2unsafezd2activationz00zzbackend_c_emitz00();
				}
			else
				{	/* BackEnd/c.scm 138 */
					BFALSE;
				}
			BGl_emitzd2tracezd2activationz00zzbackend_c_emitz00();
			BGl_emitzd2includezd2zzbackend_c_emitz00();
			if (PAIRP(BGl_za2czd2userzd2headerza2z00zzengine_paramz00))
				{	/* BackEnd/c.scm 148 */
					{	/* BackEnd/c.scm 149 */
						obj_t BgL_port1443z00_2130;

						BgL_port1443z00_2130 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
						{	/* BackEnd/c.scm 149 */
							obj_t BgL_tmpz00_4014;

							BgL_tmpz00_4014 = ((obj_t) BgL_port1443z00_2130);
							bgl_display_string(BGl_string2016z00zzbackend_cz00,
								BgL_tmpz00_4014);
						}
						{	/* BackEnd/c.scm 149 */
							obj_t BgL_tmpz00_4017;

							BgL_tmpz00_4017 = ((obj_t) BgL_port1443z00_2130);
							bgl_display_char(((unsigned char) 10), BgL_tmpz00_4017);
					}}
					{
						obj_t BgL_l1445z00_2132;

						BgL_l1445z00_2132 = BGl_za2czd2userzd2headerza2z00zzengine_paramz00;
					BgL_zc3z04anonymousza31590ze3z87_2133:
						if (PAIRP(BgL_l1445z00_2132))
							{	/* BackEnd/c.scm 150 */
								{	/* BackEnd/c.scm 150 */
									obj_t BgL_hz00_2135;

									BgL_hz00_2135 = CAR(BgL_l1445z00_2132);
									{	/* BackEnd/c.scm 150 */
										obj_t BgL_port1444z00_2136;

										BgL_port1444z00_2136 =
											BGl_za2czd2portza2zd2zzbackend_c_emitz00;
										bgl_display_obj(BgL_hz00_2135, BgL_port1444z00_2136);
										{	/* BackEnd/c.scm 150 */
											obj_t BgL_tmpz00_4024;

											BgL_tmpz00_4024 = ((obj_t) BgL_port1444z00_2136);
											bgl_display_char(((unsigned char) 10), BgL_tmpz00_4024);
								}}}
								{
									obj_t BgL_l1445z00_4027;

									BgL_l1445z00_4027 = CDR(BgL_l1445z00_2132);
									BgL_l1445z00_2132 = BgL_l1445z00_4027;
									goto BgL_zc3z04anonymousza31590ze3z87_2133;
								}
							}
						else
							{	/* BackEnd/c.scm 150 */
								((bool_t) 1);
							}
					}
				}
			else
				{	/* BackEnd/c.scm 148 */
					((bool_t) 0);
				}
			{	/* BackEnd/c.scm 153 */
				obj_t BgL_port1447z00_2139;

				BgL_port1447z00_2139 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c.scm 153 */
					obj_t BgL_tmpz00_4029;

					BgL_tmpz00_4029 = ((obj_t) BgL_port1447z00_2139);
					bgl_display_string(BGl_string2017z00zzbackend_cz00, BgL_tmpz00_4029);
				}
				{	/* BackEnd/c.scm 153 */
					obj_t BgL_tmpz00_4032;

					BgL_tmpz00_4032 = ((obj_t) BgL_port1447z00_2139);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_4032);
			}}
			{	/* BackEnd/c.scm 154 */
				obj_t BgL_port1448z00_2140;

				BgL_port1448z00_2140 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c.scm 154 */
					obj_t BgL_tmpz00_4035;

					BgL_tmpz00_4035 = ((obj_t) BgL_port1448z00_2140);
					bgl_display_string(BGl_string2018z00zzbackend_cz00, BgL_tmpz00_4035);
				}
				{	/* BackEnd/c.scm 154 */
					obj_t BgL_tmpz00_4038;

					BgL_tmpz00_4038 = ((obj_t) BgL_port1448z00_2140);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_4038);
			}}
			{	/* BackEnd/c.scm 155 */
				obj_t BgL_port1449z00_2141;

				BgL_port1449z00_2141 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* BackEnd/c.scm 155 */
					obj_t BgL_tmpz00_4041;

					BgL_tmpz00_4041 = ((obj_t) BgL_port1449z00_2141);
					bgl_display_string(BGl_string2019z00zzbackend_cz00, BgL_tmpz00_4041);
				}
				{	/* BackEnd/c.scm 155 */
					obj_t BgL_tmpz00_4044;

					BgL_tmpz00_4044 = ((obj_t) BgL_port1449z00_2141);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_4044);
			}}
			{	/* BackEnd/c.scm 158 */
				obj_t BgL_namez00_2142;

				BgL_namez00_2142 =
					BGl_modulezd2ze3idz31zzbackend_cz00
					(BGl_za2moduleza2z00zzmodule_modulez00);
				{	/* BackEnd/c.scm 159 */
					obj_t BgL_list1594z00_2143;

					BgL_list1594z00_2143 = MAKE_YOUNG_PAIR(BgL_namez00_2142, BNIL);
					BGl_fprintfz00zz__r4_output_6_10_3z00
						(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
						BGl_string2020z00zzbackend_cz00, BgL_list1594z00_2143);
				}
				{	/* BackEnd/c.scm 160 */
					obj_t BgL_list1595z00_2144;

					BgL_list1595z00_2144 = MAKE_YOUNG_PAIR(BgL_namez00_2142, BNIL);
					BGl_fprintfz00zz__r4_output_6_10_3z00
						(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
						BGl_string2021z00zzbackend_cz00, BgL_list1595z00_2144);
				}
				{	/* BackEnd/c.scm 161 */
					obj_t BgL_list1596z00_2145;

					BgL_list1596z00_2145 = MAKE_YOUNG_PAIR(BgL_namez00_2142, BNIL);
					BGl_fprintfz00zz__r4_output_6_10_3z00
						(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
						BGl_string2022z00zzbackend_cz00, BgL_list1596z00_2145);
				}
				{	/* BackEnd/c.scm 162 */
					obj_t BgL_list1597z00_2146;

					BgL_list1597z00_2146 = MAKE_YOUNG_PAIR(BgL_namez00_2142, BNIL);
					BGl_fprintfz00zz__r4_output_6_10_3z00
						(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
						BGl_string2023z00zzbackend_cz00, BgL_list1597z00_2146);
				}
				BGl_forzd2eachzd2typez12z12zztype_envz00(BGl_proc2024z00zzbackend_cz00);
				BGl_forzd2eachzd2globalz12z12zzast_envz00(BGl_proc2025z00zzbackend_cz00,
					BNIL);
				{	/* BackEnd/c.scm 182 */
					bool_t BgL_fixpointz00_2181;

					BgL_fixpointz00_2181 = ((bool_t) 0);
					{

						if (BgL_fixpointz00_2181)
							{	/* BackEnd/c.scm 184 */
								((bool_t) 0);
							}
						else
							{	/* BackEnd/c.scm 184 */
								BgL_fixpointz00_2181 = ((bool_t) 1);
								{	/* BackEnd/c.scm 186 */
									obj_t BgL_g1458z00_2184;

									BgL_g1458z00_2184 =
										BGl_getzd2classzd2listz00zzobject_classz00();
									{
										obj_t BgL_l1456z00_2186;

										BgL_l1456z00_2186 = BgL_g1458z00_2184;
									BgL_zc3z04anonymousza31663ze3z87_2187:
										if (PAIRP(BgL_l1456z00_2186))
											{	/* BackEnd/c.scm 195 */
												{	/* BackEnd/c.scm 187 */
													BgL_typez00_bglt BgL_tz00_2189;

													BgL_tz00_2189 =
														((BgL_typez00_bglt) CAR(BgL_l1456z00_2186));
													if (
														((long) (
																(((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt) BgL_tz00_2189)))->
																	BgL_occurrencez00)) > 0L))
														{	/* BackEnd/c.scm 189 */
															obj_t BgL_g1455z00_2193;

															{
																BgL_tclassz00_bglt BgL_auxz00_4069;

																{
																	obj_t BgL_auxz00_4070;

																	{	/* BackEnd/c.scm 189 */
																		BgL_objectz00_bglt BgL_tmpz00_4071;

																		BgL_tmpz00_4071 =
																			((BgL_objectz00_bglt) BgL_tz00_2189);
																		BgL_auxz00_4070 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4071);
																	}
																	BgL_auxz00_4069 =
																		((BgL_tclassz00_bglt) BgL_auxz00_4070);
																}
																BgL_g1455z00_2193 =
																	(((BgL_tclassz00_bglt)
																		COBJECT(BgL_auxz00_4069))->BgL_slotsz00);
															}
															{
																obj_t BgL_l1453z00_2195;

																BgL_l1453z00_2195 = BgL_g1455z00_2193;
															BgL_zc3z04anonymousza31677ze3z87_2196:
																if (PAIRP(BgL_l1453z00_2195))
																	{	/* BackEnd/c.scm 189 */
																		{	/* BackEnd/c.scm 190 */
																			BgL_slotz00_bglt BgL_tz00_2198;

																			BgL_tz00_2198 =
																				((BgL_slotz00_bglt)
																				CAR(BgL_l1453z00_2195));
																			if (
																				((long) (
																						(((BgL_typez00_bglt) COBJECT(
																									((BgL_typez00_bglt)
																										(((BgL_slotz00_bglt)
																												COBJECT
																												(BgL_tz00_2198))->
																											BgL_typez00))))->
																							BgL_occurrencez00)) == 0L))
																				{	/* BackEnd/c.scm 191 */
																					{	/* BackEnd/c.scm 192 */
																						obj_t BgL_arg1691z00_2203;

																						BgL_arg1691z00_2203 =
																							(((BgL_slotz00_bglt)
																								COBJECT(BgL_tz00_2198))->
																							BgL_typez00);
																						BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
																							(((BgL_typez00_bglt)
																								BgL_arg1691z00_2203));
																					}
																					BgL_fixpointz00_2181 = ((bool_t) 0);
																				}
																			else
																				{	/* BackEnd/c.scm 191 */
																					BFALSE;
																				}
																		}
																		{
																			obj_t BgL_l1453z00_4089;

																			BgL_l1453z00_4089 =
																				CDR(BgL_l1453z00_2195);
																			BgL_l1453z00_2195 = BgL_l1453z00_4089;
																			goto
																				BgL_zc3z04anonymousza31677ze3z87_2196;
																		}
																	}
																else
																	{	/* BackEnd/c.scm 189 */
																		((bool_t) 1);
																	}
															}
														}
													else
														{	/* BackEnd/c.scm 187 */
															((bool_t) 0);
														}
												}
												{
													obj_t BgL_l1456z00_4091;

													BgL_l1456z00_4091 = CDR(BgL_l1456z00_2186);
													BgL_l1456z00_2186 = BgL_l1456z00_4091;
													goto BgL_zc3z04anonymousza31663ze3z87_2187;
												}
											}
										else
											{	/* BackEnd/c.scm 195 */
												((bool_t) 1);
											}
									}
								}
							}
					}
				}
				{	/* BackEnd/c.scm 196 */
					obj_t BgL_classesz00_2212;

					{	/* BackEnd/c.scm 196 */
						obj_t BgL_hook1463z00_2213;

						BgL_hook1463z00_2213 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{	/* BackEnd/c.scm 198 */
							obj_t BgL_g1464z00_2214;

							BgL_g1464z00_2214 = BGl_getzd2classzd2listz00zzobject_classz00();
							{
								obj_t BgL_l1460z00_2216;
								obj_t BgL_h1461z00_2217;

								BgL_l1460z00_2216 = BgL_g1464z00_2214;
								BgL_h1461z00_2217 = BgL_hook1463z00_2213;
							BgL_zc3z04anonymousza31703ze3z87_2218:
								if (NULLP(BgL_l1460z00_2216))
									{	/* BackEnd/c.scm 198 */
										BgL_classesz00_2212 = CDR(BgL_hook1463z00_2213);
									}
								else
									{	/* BackEnd/c.scm 198 */
										if (
											((long) (
													(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt)
																	CAR(
																		((obj_t) BgL_l1460z00_2216)))))->
														BgL_occurrencez00)) > 0L))
											{	/* BackEnd/c.scm 198 */
												obj_t BgL_nh1462z00_2223;

												{	/* BackEnd/c.scm 198 */
													obj_t BgL_arg1709z00_2225;

													BgL_arg1709z00_2225 =
														CAR(((obj_t) BgL_l1460z00_2216));
													BgL_nh1462z00_2223 =
														MAKE_YOUNG_PAIR(BgL_arg1709z00_2225, BNIL);
												}
												SET_CDR(BgL_h1461z00_2217, BgL_nh1462z00_2223);
												{	/* BackEnd/c.scm 198 */
													obj_t BgL_arg1708z00_2224;

													BgL_arg1708z00_2224 =
														CDR(((obj_t) BgL_l1460z00_2216));
													{
														obj_t BgL_h1461z00_4112;
														obj_t BgL_l1460z00_4111;

														BgL_l1460z00_4111 = BgL_arg1708z00_2224;
														BgL_h1461z00_4112 = BgL_nh1462z00_2223;
														BgL_h1461z00_2217 = BgL_h1461z00_4112;
														BgL_l1460z00_2216 = BgL_l1460z00_4111;
														goto BgL_zc3z04anonymousza31703ze3z87_2218;
													}
												}
											}
										else
											{	/* BackEnd/c.scm 198 */
												obj_t BgL_arg1710z00_2226;

												BgL_arg1710z00_2226 = CDR(((obj_t) BgL_l1460z00_2216));
												{
													obj_t BgL_l1460z00_4115;

													BgL_l1460z00_4115 = BgL_arg1710z00_2226;
													BgL_l1460z00_2216 = BgL_l1460z00_4115;
													goto BgL_zc3z04anonymousza31703ze3z87_2218;
												}
											}
									}
							}
						}
					}
					BGl_emitzd2classzd2typesz00zzbackend_c_prototypez00
						(BgL_classesz00_2212, BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
				{	/* BackEnd/c.scm 201 */
					obj_t BgL_list1712z00_2230;

					BgL_list1712z00_2230 = MAKE_YOUNG_PAIR(BgL_namez00_2142, BNIL);
					BGl_fprintfz00zz__r4_output_6_10_3z00
						(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
						BGl_string2026z00zzbackend_cz00, BgL_list1712z00_2230);
				}
				{	/* BackEnd/c.scm 202 */
					obj_t BgL_list1713z00_2231;

					BgL_list1713z00_2231 = MAKE_YOUNG_PAIR(BgL_namez00_2142, BNIL);
					BGl_fprintfz00zz__r4_output_6_10_3z00
						(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
						BGl_string2027z00zzbackend_cz00, BgL_list1713z00_2231);
				}
				bgl_display_char(((unsigned char) 10),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			}
			BGl_emitzd2prototypeszd2zzbackend_c_prototypez00();
			BGl_emitzd2cnstszd2zzbackend_c_prototypez00();
			if (CBOOL(BGl_za2gczd2forcezd2registerzd2rootszf3za2z21zzengine_paramz00))
				{	/* BackEnd/c.scm 212 */
					BGl_gczd2rootszd2emitz00zzcc_rootsz00
						(BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
			else
				{	/* BackEnd/c.scm 212 */
					BFALSE;
				}
			{	/* BackEnd/c.scm 215 */
				obj_t BgL_globalsz00_2232;

				BgL_globalsz00_2232 =
					(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_mez00_29)))->BgL_functionsz00);
				if (((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) > 0L))
					{	/* BackEnd/c.scm 219 */
						BGl_emitzd2bdbzd2infoz00zzbdb_emitz00(BgL_globalsz00_2232,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
				else
					{	/* BackEnd/c.scm 219 */
						BFALSE;
					}
				if (((long) CINT(BGl_za2profilezd2modeza2zd2zzengine_paramz00) > 0L))
					{	/* BackEnd/c.scm 223 */
						BGl_emitzd2profzd2infoz00zzprof_emitz00(BgL_globalsz00_2232,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
				else
					{	/* BackEnd/c.scm 223 */
						BFALSE;
					}
				{	/* BackEnd/c.scm 227 */
					bool_t BgL_test2150z00_4137;

					{	/* BackEnd/c.scm 227 */
						bool_t BgL_test2151z00_4138;

						if (CBOOL(BGl_za2mainza2z00zzmodule_modulez00))
							{	/* BackEnd/c.scm 227 */
								BgL_test2151z00_4138 = ((bool_t) 1);
							}
						else
							{	/* BackEnd/c.scm 227 */
								BgL_test2151z00_4138 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
									(BGl_za2passza2z00zzengine_paramz00, CNST_TABLE_REF(8)));
							}
						if (BgL_test2151z00_4138)
							{	/* BackEnd/c.scm 227 */
								if ((BGl_za2mainza2z00zzmodule_modulez00 == CNST_TABLE_REF(9)))
									{	/* BackEnd/c.scm 228 */
										BgL_test2150z00_4137 = ((bool_t) 0);
									}
								else
									{	/* BackEnd/c.scm 228 */
										BgL_test2150z00_4137 = ((bool_t) 1);
									}
							}
						else
							{	/* BackEnd/c.scm 227 */
								BgL_test2150z00_4137 = ((bool_t) 0);
							}
					}
					if (BgL_test2150z00_4137)
						{	/* BackEnd/c.scm 227 */
							BGl_emitzd2mainzd2zzbackend_c_emitz00();
						}
					else
						{	/* BackEnd/c.scm 227 */
							BFALSE;
						}
				}
				{	/* BackEnd/c.scm 232 */
					obj_t BgL_modzd2initzd2_2238;

					BgL_modzd2initzd2_2238 =
						BGl_getzd2modulezd2initz00zzbackend_initz00();
					{	/* BackEnd/c.scm 233 */
						bool_t BgL_test2154z00_4149;

						if (CBOOL(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
									(10))))
							{	/* BackEnd/c.scm 234 */
								obj_t BgL_classz00_3119;

								BgL_classz00_3119 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_modzd2initzd2_2238))
									{	/* BackEnd/c.scm 234 */
										BgL_objectz00_bglt BgL_arg1807z00_3121;

										BgL_arg1807z00_3121 =
											(BgL_objectz00_bglt) (BgL_modzd2initzd2_2238);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* BackEnd/c.scm 234 */
												long BgL_idxz00_3127;

												BgL_idxz00_3127 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3121);
												BgL_test2154z00_4149 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3127 + 2L)) == BgL_classz00_3119);
											}
										else
											{	/* BackEnd/c.scm 234 */
												bool_t BgL_res1988z00_3152;

												{	/* BackEnd/c.scm 234 */
													obj_t BgL_oclassz00_3135;

													{	/* BackEnd/c.scm 234 */
														obj_t BgL_arg1815z00_3143;
														long BgL_arg1816z00_3144;

														BgL_arg1815z00_3143 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* BackEnd/c.scm 234 */
															long BgL_arg1817z00_3145;

															BgL_arg1817z00_3145 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3121);
															BgL_arg1816z00_3144 =
																(BgL_arg1817z00_3145 - OBJECT_TYPE);
														}
														BgL_oclassz00_3135 =
															VECTOR_REF(BgL_arg1815z00_3143,
															BgL_arg1816z00_3144);
													}
													{	/* BackEnd/c.scm 234 */
														bool_t BgL__ortest_1115z00_3136;

														BgL__ortest_1115z00_3136 =
															(BgL_classz00_3119 == BgL_oclassz00_3135);
														if (BgL__ortest_1115z00_3136)
															{	/* BackEnd/c.scm 234 */
																BgL_res1988z00_3152 = BgL__ortest_1115z00_3136;
															}
														else
															{	/* BackEnd/c.scm 234 */
																long BgL_odepthz00_3137;

																{	/* BackEnd/c.scm 234 */
																	obj_t BgL_arg1804z00_3138;

																	BgL_arg1804z00_3138 = (BgL_oclassz00_3135);
																	BgL_odepthz00_3137 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3138);
																}
																if ((2L < BgL_odepthz00_3137))
																	{	/* BackEnd/c.scm 234 */
																		obj_t BgL_arg1802z00_3140;

																		{	/* BackEnd/c.scm 234 */
																			obj_t BgL_arg1803z00_3141;

																			BgL_arg1803z00_3141 =
																				(BgL_oclassz00_3135);
																			BgL_arg1802z00_3140 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3141, 2L);
																		}
																		BgL_res1988z00_3152 =
																			(BgL_arg1802z00_3140 ==
																			BgL_classz00_3119);
																	}
																else
																	{	/* BackEnd/c.scm 234 */
																		BgL_res1988z00_3152 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2154z00_4149 = BgL_res1988z00_3152;
											}
									}
								else
									{	/* BackEnd/c.scm 234 */
										BgL_test2154z00_4149 = ((bool_t) 0);
									}
							}
						else
							{	/* BackEnd/c.scm 233 */
								BgL_test2154z00_4149 = ((bool_t) 0);
							}
						if (BgL_test2154z00_4149)
							{	/* BackEnd/c.scm 233 */
								if (SYMBOLP(BGl_za2dlopenzd2initza2zd2zzengine_paramz00))
									{	/* BackEnd/c.scm 237 */
										obj_t BgL_arg1722z00_2242;

										{	/* BackEnd/c.scm 237 */
											obj_t BgL_symbolz00_3154;

											BgL_symbolz00_3154 =
												BGl_za2dlopenzd2initza2zd2zzengine_paramz00;
											{	/* BackEnd/c.scm 237 */
												obj_t BgL_arg1455z00_3155;

												BgL_arg1455z00_3155 =
													SYMBOL_TO_STRING(BgL_symbolz00_3154);
												BgL_arg1722z00_2242 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_3155);
											}
										}
										BGl_emitzd2dlopenzd2initz00zzbackend_c_emitz00(
											((BgL_globalz00_bglt) BgL_modzd2initzd2_2238),
											BgL_arg1722z00_2242);
									}
								else
									{	/* BackEnd/c.scm 236 */
										if (STRINGP(BGl_za2dlopenzd2initza2zd2zzengine_paramz00))
											{	/* BackEnd/c.scm 238 */
												BGl_emitzd2dlopenzd2initz00zzbackend_c_emitz00(
													((BgL_globalz00_bglt) BgL_modzd2initzd2_2238),
													BGl_za2dlopenzd2initza2zd2zzengine_paramz00);
											}
										else
											{	/* BackEnd/c.scm 238 */
												if (CBOOL(BGl_za2dlopenzd2initza2zd2zzengine_paramz00))
													{	/* BackEnd/c.scm 240 */
														BGl_emitzd2dlopenzd2initz00zzbackend_c_emitz00(
															((BgL_globalz00_bglt) BgL_modzd2initzd2_2238),
															string_to_bstring(BGL_DYNAMIC_LOAD_INIT));
													}
												else
													{	/* BackEnd/c.scm 240 */
														BUNSPEC;
													}
											}
									}
							}
						else
							{	/* BackEnd/c.scm 233 */
								BFALSE;
							}
					}
				}
				BGl_backendzd2compilezd2functionsz00zzbackend_backendz00(
					((BgL_backendz00_bglt) BgL_mez00_29));
				{	/* BackEnd/c.scm 248 */
					obj_t BgL_port1465z00_2245;

					BgL_port1465z00_2245 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c.scm 248 */
						obj_t BgL_tmpz00_4193;

						BgL_tmpz00_4193 = ((obj_t) BgL_port1465z00_2245);
						bgl_display_string(BGl_string2017z00zzbackend_cz00,
							BgL_tmpz00_4193);
					}
					{	/* BackEnd/c.scm 248 */
						obj_t BgL_tmpz00_4196;

						BgL_tmpz00_4196 = ((obj_t) BgL_port1465z00_2245);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_4196);
				}}
				{	/* BackEnd/c.scm 249 */
					obj_t BgL_port1466z00_2246;

					BgL_port1466z00_2246 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c.scm 249 */
						obj_t BgL_tmpz00_4199;

						BgL_tmpz00_4199 = ((obj_t) BgL_port1466z00_2246);
						bgl_display_string(BGl_string2028z00zzbackend_cz00,
							BgL_tmpz00_4199);
					}
					{	/* BackEnd/c.scm 249 */
						obj_t BgL_tmpz00_4202;

						BgL_tmpz00_4202 = ((obj_t) BgL_port1466z00_2246);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_4202);
				}}
				{	/* BackEnd/c.scm 250 */
					obj_t BgL_port1467z00_2247;

					BgL_port1467z00_2247 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* BackEnd/c.scm 250 */
						obj_t BgL_tmpz00_4205;

						BgL_tmpz00_4205 = ((obj_t) BgL_port1467z00_2247);
						bgl_display_string(BGl_string2019z00zzbackend_cz00,
							BgL_tmpz00_4205);
					}
					{	/* BackEnd/c.scm 250 */
						obj_t BgL_tmpz00_4208;

						BgL_tmpz00_4208 = ((obj_t) BgL_port1467z00_2247);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_4208);
				}}
				{
					obj_t BgL_l1469z00_2249;

					BgL_l1469z00_2249 = BGl_za2czd2userzd2footza2z00zzengine_paramz00;
				BgL_zc3z04anonymousza31724ze3z87_2250:
					if (PAIRP(BgL_l1469z00_2249))
						{	/* BackEnd/c.scm 253 */
							{	/* BackEnd/c.scm 253 */
								obj_t BgL_hz00_2252;

								BgL_hz00_2252 = CAR(BgL_l1469z00_2249);
								{	/* BackEnd/c.scm 253 */
									obj_t BgL_port1468z00_2253;

									BgL_port1468z00_2253 =
										BGl_za2czd2portza2zd2zzbackend_c_emitz00;
									bgl_display_obj(BgL_hz00_2252, BgL_port1468z00_2253);
									{	/* BackEnd/c.scm 253 */
										obj_t BgL_tmpz00_4215;

										BgL_tmpz00_4215 = ((obj_t) BgL_port1468z00_2253);
										bgl_display_char(((unsigned char) 10), BgL_tmpz00_4215);
							}}}
							{
								obj_t BgL_l1469z00_4218;

								BgL_l1469z00_4218 = CDR(BgL_l1469z00_2249);
								BgL_l1469z00_2249 = BgL_l1469z00_4218;
								goto BgL_zc3z04anonymousza31724ze3z87_2250;
							}
						}
					else
						{	/* BackEnd/c.scm 253 */
							((bool_t) 1);
						}
				}
				return BGl_stopzd2emissionz12zc0zzbackend_c_emitz00();
			}
		}

	}



/* &<@anonymous:1607> */
	obj_t BGl_z62zc3z04anonymousza31607ze3ze5zzbackend_cz00(obj_t BgL_envz00_3434,
		obj_t BgL_globalz00_3435)
	{
		{	/* BackEnd/c.scm 166 */
			{	/* BackEnd/c.scm 168 */
				bool_t BgL_test2164z00_4221;

				{	/* BackEnd/c.scm 168 */
					bool_t BgL_test2165z00_4222;

					{	/* BackEnd/c.scm 168 */
						obj_t BgL_arg1661z00_3508;

						BgL_arg1661z00_3508 =
							(((BgL_globalz00_bglt) COBJECT(
									((BgL_globalz00_bglt) BgL_globalz00_3435)))->BgL_modulez00);
						BgL_test2165z00_4222 =
							(BgL_arg1661z00_3508 == BGl_za2moduleza2z00zzmodule_modulez00);
					}
					if (BgL_test2165z00_4222)
						{	/* BackEnd/c.scm 168 */
							BgL_test2164z00_4221 =
								(
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_3435))))->
									BgL_occurrencez00) > 0L);
						}
					else
						{	/* BackEnd/c.scm 168 */
							BgL_test2164z00_4221 = ((bool_t) 0);
						}
				}
				if (BgL_test2164z00_4221)
					{	/* BackEnd/c.scm 168 */
						return
							BGl_typezd2incrementzd2globalz12z12zzast_typezd2occurzd2(
							((BgL_globalz00_bglt) BgL_globalz00_3435));
					}
				else
					{	/* BackEnd/c.scm 168 */
						if (BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(
								((BgL_globalz00_bglt) BgL_globalz00_3435)))
							{	/* BackEnd/c.scm 171 */
								BGl_typezd2incrementzd2globalz12z12zzast_typezd2occurzd2(
									((BgL_globalz00_bglt) BgL_globalz00_3435));
								{	/* BackEnd/c.scm 173 */
									BgL_typez00_bglt BgL_arg1616z00_3509;

									BgL_arg1616z00_3509 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_globalz00_3435))))->
										BgL_typez00);
									BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
										(BgL_arg1616z00_3509);
								}
								{	/* BackEnd/c.scm 174 */
									bool_t BgL_test2167z00_4241;

									{	/* BackEnd/c.scm 174 */
										BgL_valuez00_bglt BgL_arg1651z00_3510;

										BgL_arg1651z00_3510 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_globalz00_3435))))->
											BgL_valuez00);
										{	/* BackEnd/c.scm 174 */
											obj_t BgL_classz00_3511;

											BgL_classz00_3511 = BGl_sfunz00zzast_varz00;
											{	/* BackEnd/c.scm 174 */
												BgL_objectz00_bglt BgL_arg1807z00_3512;

												{	/* BackEnd/c.scm 174 */
													obj_t BgL_tmpz00_4245;

													BgL_tmpz00_4245 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1651z00_3510));
													BgL_arg1807z00_3512 =
														(BgL_objectz00_bglt) (BgL_tmpz00_4245);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* BackEnd/c.scm 174 */
														long BgL_idxz00_3513;

														BgL_idxz00_3513 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3512);
														BgL_test2167z00_4241 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3513 + 3L)) == BgL_classz00_3511);
													}
												else
													{	/* BackEnd/c.scm 174 */
														bool_t BgL_res1985z00_3516;

														{	/* BackEnd/c.scm 174 */
															obj_t BgL_oclassz00_3517;

															{	/* BackEnd/c.scm 174 */
																obj_t BgL_arg1815z00_3518;
																long BgL_arg1816z00_3519;

																BgL_arg1815z00_3518 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* BackEnd/c.scm 174 */
																	long BgL_arg1817z00_3520;

																	BgL_arg1817z00_3520 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3512);
																	BgL_arg1816z00_3519 =
																		(BgL_arg1817z00_3520 - OBJECT_TYPE);
																}
																BgL_oclassz00_3517 =
																	VECTOR_REF(BgL_arg1815z00_3518,
																	BgL_arg1816z00_3519);
															}
															{	/* BackEnd/c.scm 174 */
																bool_t BgL__ortest_1115z00_3521;

																BgL__ortest_1115z00_3521 =
																	(BgL_classz00_3511 == BgL_oclassz00_3517);
																if (BgL__ortest_1115z00_3521)
																	{	/* BackEnd/c.scm 174 */
																		BgL_res1985z00_3516 =
																			BgL__ortest_1115z00_3521;
																	}
																else
																	{	/* BackEnd/c.scm 174 */
																		long BgL_odepthz00_3522;

																		{	/* BackEnd/c.scm 174 */
																			obj_t BgL_arg1804z00_3523;

																			BgL_arg1804z00_3523 =
																				(BgL_oclassz00_3517);
																			BgL_odepthz00_3522 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3523);
																		}
																		if ((3L < BgL_odepthz00_3522))
																			{	/* BackEnd/c.scm 174 */
																				obj_t BgL_arg1802z00_3524;

																				{	/* BackEnd/c.scm 174 */
																					obj_t BgL_arg1803z00_3525;

																					BgL_arg1803z00_3525 =
																						(BgL_oclassz00_3517);
																					BgL_arg1802z00_3524 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3525, 3L);
																				}
																				BgL_res1985z00_3516 =
																					(BgL_arg1802z00_3524 ==
																					BgL_classz00_3511);
																			}
																		else
																			{	/* BackEnd/c.scm 174 */
																				BgL_res1985z00_3516 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2167z00_4241 = BgL_res1985z00_3516;
													}
											}
										}
									}
									if (BgL_test2167z00_4241)
										{	/* BackEnd/c.scm 175 */
											obj_t BgL_g1452z00_3526;

											BgL_g1452z00_3526 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_globalz00_bglt)
																				BgL_globalz00_3435))))->
																BgL_valuez00))))->BgL_argsz00);
											{
												obj_t BgL_l1450z00_3528;

												{	/* BackEnd/c.scm 181 */
													bool_t BgL_tmpz00_4273;

													BgL_l1450z00_3528 = BgL_g1452z00_3526;
												BgL_zc3z04anonymousza31627ze3z87_3527:
													if (PAIRP(BgL_l1450z00_3528))
														{	/* BackEnd/c.scm 181 */
															{	/* BackEnd/c.scm 177 */
																obj_t BgL_az00_3529;

																BgL_az00_3529 = CAR(BgL_l1450z00_3528);
																{	/* BackEnd/c.scm 177 */
																	bool_t BgL_test2172z00_4277;

																	{	/* BackEnd/c.scm 177 */
																		obj_t BgL_classz00_3530;

																		BgL_classz00_3530 =
																			BGl_typez00zztype_typez00;
																		if (BGL_OBJECTP(BgL_az00_3529))
																			{	/* BackEnd/c.scm 177 */
																				BgL_objectz00_bglt BgL_arg1807z00_3531;

																				BgL_arg1807z00_3531 =
																					(BgL_objectz00_bglt) (BgL_az00_3529);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* BackEnd/c.scm 177 */
																						long BgL_idxz00_3532;

																						BgL_idxz00_3532 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_3531);
																						BgL_test2172z00_4277 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_3532 + 1L)) ==
																							BgL_classz00_3530);
																					}
																				else
																					{	/* BackEnd/c.scm 177 */
																						bool_t BgL_res1986z00_3535;

																						{	/* BackEnd/c.scm 177 */
																							obj_t BgL_oclassz00_3536;

																							{	/* BackEnd/c.scm 177 */
																								obj_t BgL_arg1815z00_3537;
																								long BgL_arg1816z00_3538;

																								BgL_arg1815z00_3537 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* BackEnd/c.scm 177 */
																									long BgL_arg1817z00_3539;

																									BgL_arg1817z00_3539 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_3531);
																									BgL_arg1816z00_3538 =
																										(BgL_arg1817z00_3539 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_3536 =
																									VECTOR_REF
																									(BgL_arg1815z00_3537,
																									BgL_arg1816z00_3538);
																							}
																							{	/* BackEnd/c.scm 177 */
																								bool_t BgL__ortest_1115z00_3540;

																								BgL__ortest_1115z00_3540 =
																									(BgL_classz00_3530 ==
																									BgL_oclassz00_3536);
																								if (BgL__ortest_1115z00_3540)
																									{	/* BackEnd/c.scm 177 */
																										BgL_res1986z00_3535 =
																											BgL__ortest_1115z00_3540;
																									}
																								else
																									{	/* BackEnd/c.scm 177 */
																										long BgL_odepthz00_3541;

																										{	/* BackEnd/c.scm 177 */
																											obj_t BgL_arg1804z00_3542;

																											BgL_arg1804z00_3542 =
																												(BgL_oclassz00_3536);
																											BgL_odepthz00_3541 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_3542);
																										}
																										if (
																											(1L < BgL_odepthz00_3541))
																											{	/* BackEnd/c.scm 177 */
																												obj_t
																													BgL_arg1802z00_3543;
																												{	/* BackEnd/c.scm 177 */
																													obj_t
																														BgL_arg1803z00_3544;
																													BgL_arg1803z00_3544 =
																														(BgL_oclassz00_3536);
																													BgL_arg1802z00_3543 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_3544,
																														1L);
																												}
																												BgL_res1986z00_3535 =
																													(BgL_arg1802z00_3543
																													== BgL_classz00_3530);
																											}
																										else
																											{	/* BackEnd/c.scm 177 */
																												BgL_res1986z00_3535 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2172z00_4277 =
																							BgL_res1986z00_3535;
																					}
																			}
																		else
																			{	/* BackEnd/c.scm 177 */
																				BgL_test2172z00_4277 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test2172z00_4277)
																		{	/* BackEnd/c.scm 177 */
																			BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
																				(((BgL_typez00_bglt) BgL_az00_3529));
																		}
																	else
																		{	/* BackEnd/c.scm 179 */
																			bool_t BgL_test2177z00_4302;

																			{	/* BackEnd/c.scm 179 */
																				obj_t BgL_classz00_3545;

																				BgL_classz00_3545 =
																					BGl_localz00zzast_varz00;
																				if (BGL_OBJECTP(BgL_az00_3529))
																					{	/* BackEnd/c.scm 179 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_3546;
																						BgL_arg1807z00_3546 =
																							(BgL_objectz00_bglt)
																							(BgL_az00_3529);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* BackEnd/c.scm 179 */
																								long BgL_idxz00_3547;

																								BgL_idxz00_3547 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_3546);
																								BgL_test2177z00_4302 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_3547 + 2L)) ==
																									BgL_classz00_3545);
																							}
																						else
																							{	/* BackEnd/c.scm 179 */
																								bool_t BgL_res1987z00_3550;

																								{	/* BackEnd/c.scm 179 */
																									obj_t BgL_oclassz00_3551;

																									{	/* BackEnd/c.scm 179 */
																										obj_t BgL_arg1815z00_3552;
																										long BgL_arg1816z00_3553;

																										BgL_arg1815z00_3552 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* BackEnd/c.scm 179 */
																											long BgL_arg1817z00_3554;

																											BgL_arg1817z00_3554 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_3546);
																											BgL_arg1816z00_3553 =
																												(BgL_arg1817z00_3554 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_3551 =
																											VECTOR_REF
																											(BgL_arg1815z00_3552,
																											BgL_arg1816z00_3553);
																									}
																									{	/* BackEnd/c.scm 179 */
																										bool_t
																											BgL__ortest_1115z00_3555;
																										BgL__ortest_1115z00_3555 =
																											(BgL_classz00_3545 ==
																											BgL_oclassz00_3551);
																										if (BgL__ortest_1115z00_3555)
																											{	/* BackEnd/c.scm 179 */
																												BgL_res1987z00_3550 =
																													BgL__ortest_1115z00_3555;
																											}
																										else
																											{	/* BackEnd/c.scm 179 */
																												long BgL_odepthz00_3556;

																												{	/* BackEnd/c.scm 179 */
																													obj_t
																														BgL_arg1804z00_3557;
																													BgL_arg1804z00_3557 =
																														(BgL_oclassz00_3551);
																													BgL_odepthz00_3556 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_3557);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_3556))
																													{	/* BackEnd/c.scm 179 */
																														obj_t
																															BgL_arg1802z00_3558;
																														{	/* BackEnd/c.scm 179 */
																															obj_t
																																BgL_arg1803z00_3559;
																															BgL_arg1803z00_3559
																																=
																																(BgL_oclassz00_3551);
																															BgL_arg1802z00_3558
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_3559,
																																2L);
																														}
																														BgL_res1987z00_3550
																															=
																															(BgL_arg1802z00_3558
																															==
																															BgL_classz00_3545);
																													}
																												else
																													{	/* BackEnd/c.scm 179 */
																														BgL_res1987z00_3550
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2177z00_4302 =
																									BgL_res1987z00_3550;
																							}
																					}
																				else
																					{	/* BackEnd/c.scm 179 */
																						BgL_test2177z00_4302 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test2177z00_4302)
																				{	/* BackEnd/c.scm 180 */
																					BgL_typez00_bglt BgL_arg1642z00_3560;

																					BgL_arg1642z00_3560 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_localz00_bglt)
																										BgL_az00_3529))))->
																						BgL_typez00);
																					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
																						(BgL_arg1642z00_3560);
																				}
																			else
																				{	/* BackEnd/c.scm 179 */
																					BFALSE;
																				}
																		}
																}
															}
															{
																obj_t BgL_l1450z00_4329;

																BgL_l1450z00_4329 = CDR(BgL_l1450z00_3528);
																BgL_l1450z00_3528 = BgL_l1450z00_4329;
																goto BgL_zc3z04anonymousza31627ze3z87_3527;
															}
														}
													else
														{	/* BackEnd/c.scm 181 */
															BgL_tmpz00_4273 = ((bool_t) 1);
														}
													return BBOOL(BgL_tmpz00_4273);
												}
											}
										}
									else
										{	/* BackEnd/c.scm 174 */
											return BFALSE;
										}
								}
							}
						else
							{	/* BackEnd/c.scm 171 */
								return BFALSE;
							}
					}
			}
		}

	}



/* &<@anonymous:1603> */
	obj_t BGl_z62zc3z04anonymousza31603ze3ze5zzbackend_cz00(obj_t BgL_envz00_3436,
		obj_t BgL_tz00_3437)
	{
		{	/* BackEnd/c.scm 164 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_tz00_3437)))->BgL_occurrencez00) = ((int)
					(int) (0L)), BUNSPEC);
		}

	}



/* &<@anonymous:1586> */
	obj_t BGl_z62zc3z04anonymousza31586ze3ze5zzbackend_cz00(obj_t BgL_envz00_3438)
	{
		{	/* BackEnd/c.scm 125 */
			return
				BGl_startzd2emissionz12zc0zzbackend_c_emitz00
				(BGl_string2029z00zzbackend_cz00);
		}

	}



/* cc-compiler */
	BGL_EXPORTED_DEF obj_t BGl_cczd2compilerzd2zzbackend_cz00(obj_t
		BgL_czd2prefixzd2_30, obj_t BgL_ozd2prefixzd2_31)
	{
		{	/* BackEnd/c.scm 260 */
			BGl_setzd2backendz12zc0zzbackend_backendz00(CNST_TABLE_REF(0));
			{	/* BackEnd/c.scm 264 */
				bool_t BgL_arg1734z00_2256;

				if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(11)))
					{	/* BackEnd/c.scm 264 */
						BgL_arg1734z00_2256 = ((bool_t) 0);
					}
				else
					{	/* BackEnd/c.scm 264 */
						BgL_arg1734z00_2256 = ((bool_t) 1);
					}
				BGl_ccz00zzcc_ccz00(BgL_czd2prefixzd2_30, BgL_ozd2prefixzd2_31,
					BgL_arg1734z00_2256);
			}
			BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(11),
				BGl_proc2030z00zzbackend_cz00);
			{	/* BackEnd/c.scm 267 */
				obj_t BgL_arg1737z00_2260;

				if (STRINGP(BgL_ozd2prefixzd2_31))
					{	/* BackEnd/c.scm 267 */
						BgL_arg1737z00_2260 = BgL_ozd2prefixzd2_31;
					}
				else
					{	/* BackEnd/c.scm 267 */
						BgL_arg1737z00_2260 = BgL_czd2prefixzd2_30;
					}
				return BGl_ldz00zzcc_ldz00(BgL_arg1737z00_2260, ((bool_t) 0));
			}
		}

	}



/* &cc-compiler */
	obj_t BGl_z62cczd2compilerzb0zzbackend_cz00(obj_t BgL_envz00_3440,
		obj_t BgL_czd2prefixzd2_3441, obj_t BgL_ozd2prefixzd2_3442)
	{
		{	/* BackEnd/c.scm 260 */
			return
				BGl_cczd2compilerzd2zzbackend_cz00(BgL_czd2prefixzd2_3441,
				BgL_ozd2prefixzd2_3442);
		}

	}



/* &<@anonymous:1736> */
	obj_t BGl_z62zc3z04anonymousza31736ze3ze5zzbackend_cz00(obj_t BgL_envz00_3443)
	{
		{	/* BackEnd/c.scm 265 */
			return CNST_TABLE_REF(12);
		}

	}



/* make-tmp-file-name */
	obj_t BGl_makezd2tmpzd2filezd2namezd2zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 280 */
			{	/* BackEnd/c.scm 284 */
				obj_t BgL_arg1739z00_2262;

				{	/* BackEnd/c.scm 284 */
					obj_t BgL_arg1740z00_2263;
					obj_t BgL_arg1746z00_2264;

					{	/* BackEnd/c.scm 284 */
						obj_t BgL_userz00_2270;

						BgL_userz00_2270 =
							BGl_getenvz00zz__osz00(BGl_string2031z00zzbackend_cz00);
						if (STRINGP(BgL_userz00_2270))
							{	/* BackEnd/c.scm 285 */
								BgL_arg1740z00_2263 = BgL_userz00_2270;
							}
						else
							{	/* BackEnd/c.scm 285 */
								BgL_arg1740z00_2263 = BGl_string2032z00zzbackend_cz00;
							}
					}
					BgL_arg1746z00_2264 = CAR(BGl_za2srczd2suffixza2zd2zzengine_paramz00);
					{	/* BackEnd/c.scm 282 */
						obj_t BgL_list1747z00_2265;

						{	/* BackEnd/c.scm 282 */
							obj_t BgL_arg1748z00_2266;

							{	/* BackEnd/c.scm 282 */
								obj_t BgL_arg1749z00_2267;

								{	/* BackEnd/c.scm 282 */
									obj_t BgL_arg1750z00_2268;

									{	/* BackEnd/c.scm 282 */
										obj_t BgL_arg1751z00_2269;

										BgL_arg1751z00_2269 =
											MAKE_YOUNG_PAIR(BgL_arg1746z00_2264, BNIL);
										BgL_arg1750z00_2268 =
											MAKE_YOUNG_PAIR(BGl_string2033z00zzbackend_cz00,
											BgL_arg1751z00_2269);
									}
									BgL_arg1749z00_2267 =
										MAKE_YOUNG_PAIR(BgL_arg1740z00_2263, BgL_arg1750z00_2268);
								}
								BgL_arg1748z00_2266 =
									MAKE_YOUNG_PAIR(BGl_string2034z00zzbackend_cz00,
									BgL_arg1749z00_2267);
							}
							BgL_list1747z00_2265 =
								MAKE_YOUNG_PAIR(BGl_string2035z00zzbackend_cz00,
								BgL_arg1748z00_2266);
						}
						BgL_arg1739z00_2262 =
							BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1747z00_2265);
					}
				}
				return
					BGl_makezd2filezd2namez00zz__osz00
					(BGl_za2bigloozd2tmpza2zd2zzengine_paramz00, BgL_arg1739z00_2262);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2compilezd2envz00zzbackend_backendz00,
				BGl_cvmz00zzbackend_cvmz00, BGl_proc2036z00zzbackend_cz00,
				BGl_string2037z00zzbackend_cz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2linkzd2envz00zzbackend_backendz00,
				BGl_cvmz00zzbackend_cvmz00, BGl_proc2038z00zzbackend_cz00,
				BGl_string2039z00zzbackend_cz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2cnstzd2tablezd2namezd2envz00zzbackend_backendz00,
				BGl_cvmz00zzbackend_cvmz00, BGl_proc2040z00zzbackend_cz00,
				BGl_string2041z00zzbackend_cz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2linkzd2objectszd2envzd2zzbackend_backendz00,
				BGl_cvmz00zzbackend_cvmz00, BGl_proc2042z00zzbackend_cz00,
				BGl_string2043z00zzbackend_cz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2subtypezf3zd2envzf3zzbackend_backendz00,
				BGl_cvmz00zzbackend_cvmz00, BGl_proc2044z00zzbackend_cz00,
				BGl_string2045z00zzbackend_cz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_backendzd2gczd2initzd2envzd2zzbackend_backendz00,
				BGl_cvmz00zzbackend_cvmz00, BGl_proc2046z00zzbackend_cz00,
				BGl_string2047z00zzbackend_cz00);
		}

	}



/* &backend-gc-init-cvm1496 */
	obj_t BGl_z62backendzd2gczd2initzd2cvm1496zb0zzbackend_cz00(obj_t
		BgL_envz00_3455, obj_t BgL_bz00_3456)
	{
		{	/* BackEnd/c.scm 398 */
			return CNST_TABLE_REF(13);
		}

	}



/* &backend-subtype?-cvm1494 */
	obj_t BGl_z62backendzd2subtypezf3zd2cvm1494z91zzbackend_cz00(obj_t
		BgL_envz00_3457, obj_t BgL_bz00_3458, obj_t BgL_t1z00_3459,
		obj_t BgL_t2z00_3460)
	{
		{	/* BackEnd/c.scm 388 */
			{	/* BackEnd/c.scm 390 */
				bool_t BgL__ortest_1137z00_3563;

				BgL__ortest_1137z00_3563 = (BgL_t1z00_3459 == BgL_t2z00_3460);
				if (BgL__ortest_1137z00_3563)
					{	/* BackEnd/c.scm 390 */
						return BBOOL(BgL__ortest_1137z00_3563);
					}
				else
					{	/* BackEnd/c.scm 391 */
						bool_t BgL__ortest_1138z00_3564;

						BgL__ortest_1138z00_3564 =
							(
							(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_t1z00_3459)))->BgL_idz00) ==
							(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_t2z00_3460)))->BgL_idz00));
						if (BgL__ortest_1138z00_3564)
							{	/* BackEnd/c.scm 391 */
								return BBOOL(BgL__ortest_1138z00_3564);
							}
						else
							{	/* BackEnd/c.scm 392 */
								bool_t BgL__ortest_1139z00_3565;

								BgL__ortest_1139z00_3565 =
									(BgL_t2z00_3460 == BGl_za2_za2z00zztype_cachez00);
								if (BgL__ortest_1139z00_3565)
									{	/* BackEnd/c.scm 392 */
										return BBOOL(BgL__ortest_1139z00_3565);
									}
								else
									{	/* BackEnd/c.scm 393 */
										obj_t BgL_arg1906z00_3566;
										obj_t BgL_arg1910z00_3567;

										BgL_arg1906z00_3566 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_t1z00_3459)))->BgL_namez00);
										BgL_arg1910z00_3567 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_t2z00_3460)))->BgL_namez00);
										{	/* BackEnd/c.scm 393 */
											long BgL_l1z00_3568;

											BgL_l1z00_3568 =
												STRING_LENGTH(((obj_t) BgL_arg1906z00_3566));
											if (
												(BgL_l1z00_3568 ==
													STRING_LENGTH(((obj_t) BgL_arg1910z00_3567))))
												{	/* BackEnd/c.scm 393 */
													int BgL_arg1282z00_3569;

													{	/* BackEnd/c.scm 393 */
														char *BgL_auxz00_4393;
														char *BgL_tmpz00_4390;

														BgL_auxz00_4393 =
															BSTRING_TO_STRING(((obj_t) BgL_arg1910z00_3567));
														BgL_tmpz00_4390 =
															BSTRING_TO_STRING(((obj_t) BgL_arg1906z00_3566));
														BgL_arg1282z00_3569 =
															memcmp(BgL_tmpz00_4390, BgL_auxz00_4393,
															BgL_l1z00_3568);
													}
													return BBOOL(((long) (BgL_arg1282z00_3569) == 0L));
												}
											else
												{	/* BackEnd/c.scm 393 */
													return BFALSE;
												}
										}
									}
							}
					}
			}
		}

	}



/* &backend-link-objects1492 */
	obj_t BGl_z62backendzd2linkzd2objects1492z62zzbackend_cz00(obj_t
		BgL_envz00_3461, obj_t BgL_mez00_3462, obj_t BgL_sourcesz00_3463)
	{
		{	/* BackEnd/c.scm 294 */
			if (((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) > 0L))
				{	/* BackEnd/c.scm 295 */
					BGl_bdbzd2settingz12zc0zzbdb_settingz00();
				}
			else
				{	/* BackEnd/c.scm 295 */
					BFALSE;
				}
			if (NULLP(BgL_sourcesz00_3463))
				{	/* BackEnd/c.scm 298 */
					obj_t BgL_firstz00_3571;

					BgL_firstz00_3571 =
						BGl_prefixz00zz__osz00(CAR
						(BGl_za2ozd2filesza2zd2zzengine_paramz00));
					{	/* BackEnd/c.scm 299 */
						obj_t BgL_list1768z00_3572;

						{	/* BackEnd/c.scm 299 */
							obj_t BgL_arg1770z00_3573;

							{	/* BackEnd/c.scm 299 */
								obj_t BgL_arg1771z00_3574;

								{	/* BackEnd/c.scm 299 */
									obj_t BgL_arg1773z00_3575;

									BgL_arg1773z00_3575 =
										MAKE_YOUNG_PAIR(BGl_za2ozd2filesza2zd2zzengine_paramz00,
										BNIL);
									BgL_arg1771z00_3574 =
										MAKE_YOUNG_PAIR(BGl_string2048z00zzbackend_cz00,
										BgL_arg1773z00_3575);
								}
								BgL_arg1770z00_3573 =
									MAKE_YOUNG_PAIR(BGl_string2049z00zzbackend_cz00,
									BgL_arg1771z00_3574);
							}
							BgL_list1768z00_3572 =
								MAKE_YOUNG_PAIR(BGl_string2050z00zzbackend_cz00,
								BgL_arg1770z00_3573);
						}
						BGl_warningz00zz__errorz00(BgL_list1768z00_3572);
					}
					BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00();
					BGl_za2ozd2filesza2zd2zzengine_paramz00 =
						CDR(BGl_za2ozd2filesza2zd2zzengine_paramz00);
					return BGl_ldz00zzcc_ldz00(BgL_firstz00_3571, ((bool_t) 0));
				}
			else
				{
					obj_t BgL_sourcesz00_3577;
					obj_t BgL_clausesz00_3578;
					obj_t BgL_mainz00_3579;
					obj_t BgL_fmainz00_3580;
					obj_t BgL_librariesz00_3581;

					BgL_sourcesz00_3577 = BgL_sourcesz00_3463;
					BgL_clausesz00_3578 = BNIL;
					BgL_mainz00_3579 = BFALSE;
					BgL_fmainz00_3580 = BGl_string2032z00zzbackend_cz00;
					BgL_librariesz00_3581 = BNIL;
				BgL_loopz00_3576:
					if (NULLP(BgL_sourcesz00_3577))
						{	/* BackEnd/c.scm 310 */
							bool_t BgL_test2192z00_4418;

							if (CBOOL(BgL_mainz00_3579))
								{	/* BackEnd/c.scm 310 */
									BgL_test2192z00_4418 = ((bool_t) 1);
								}
							else
								{	/* BackEnd/c.scm 310 */
									if (CBOOL(BGl_za2autozd2linkzd2mainza2z00zzengine_paramz00))
										{	/* BackEnd/c.scm 310 */
											BgL_test2192z00_4418 =
												(BGl_za2passza2z00zzengine_paramz00 ==
												CNST_TABLE_REF(14));
										}
									else
										{	/* BackEnd/c.scm 310 */
											BgL_test2192z00_4418 = ((bool_t) 1);
										}
								}
							if (BgL_test2192z00_4418)
								{	/* BackEnd/c.scm 311 */
									obj_t BgL_firstz00_3582;

									BgL_firstz00_3582 =
										BGl_prefixz00zz__osz00(CAR
										(BGl_za2ozd2filesza2zd2zzengine_paramz00));
									{
										obj_t BgL_l1480z00_3584;

										BgL_l1480z00_3584 = BgL_librariesz00_3581;
									BgL_zc3z04anonymousza31779ze3z87_3583:
										if (PAIRP(BgL_l1480z00_3584))
											{	/* BackEnd/c.scm 314 */
												{	/* BackEnd/c.scm 315 */
													obj_t BgL_libz00_3585;

													BgL_libz00_3585 = CAR(BgL_l1480z00_3584);
													{
														obj_t BgL_clausesz00_3591;
														obj_t BgL_libsz00_3588;

														if (PAIRP(BgL_libz00_3585))
															{	/* BackEnd/c.scm 315 */
																if (
																	(CAR(
																			((obj_t) BgL_libz00_3585)) ==
																		CNST_TABLE_REF(15)))
																	{	/* BackEnd/c.scm 315 */
																		obj_t BgL_arg1805z00_3597;

																		BgL_arg1805z00_3597 =
																			CDR(((obj_t) BgL_libz00_3585));
																		BgL_libsz00_3588 = BgL_arg1805z00_3597;
																		{
																			obj_t BgL_l1471z00_3590;

																			BgL_l1471z00_3590 = BgL_libsz00_3588;
																		BgL_zc3z04anonymousza31821ze3z87_3589:
																			if (PAIRP(BgL_l1471z00_3590))
																				{	/* BackEnd/c.scm 317 */
																					BGl_usezd2libraryz12zc0zzmodule_alibraryz00
																						(CAR(BgL_l1471z00_3590));
																					{
																						obj_t BgL_l1471z00_4443;

																						BgL_l1471z00_4443 =
																							CDR(BgL_l1471z00_3590);
																						BgL_l1471z00_3590 =
																							BgL_l1471z00_4443;
																						goto
																							BgL_zc3z04anonymousza31821ze3z87_3589;
																					}
																				}
																			else
																				{	/* BackEnd/c.scm 317 */
																					((bool_t) 1);
																				}
																		}
																	}
																else
																	{	/* BackEnd/c.scm 315 */
																		if (
																			(CAR(
																					((obj_t) BgL_libz00_3585)) ==
																				CNST_TABLE_REF(16)))
																			{	/* BackEnd/c.scm 315 */
																				obj_t BgL_arg1808z00_3598;

																				BgL_arg1808z00_3598 =
																					CDR(((obj_t) BgL_libz00_3585));
																				BgL_clausesz00_3591 =
																					BgL_arg1808z00_3598;
																				{	/* BackEnd/c.scm 319 */
																					obj_t BgL_fun1479z00_3592;

																					{	/* BackEnd/c.scm 319 */
																						obj_t
																							BgL_zc3z04anonymousza31836ze3z87_3593;
																						{	/* BackEnd/c.scm 315 */
																							int BgL_tmpz00_4452;

																							BgL_tmpz00_4452 = (int) (0L);
																							BgL_zc3z04anonymousza31836ze3z87_3593
																								=
																								MAKE_EL_PROCEDURE
																								(BgL_tmpz00_4452);
																						}
																						BgL_fun1479z00_3592 =
																							BgL_zc3z04anonymousza31836ze3z87_3593;
																					}
																					{
																						obj_t BgL_l1477z00_3595;

																						BgL_l1477z00_3595 =
																							BgL_clausesz00_3591;
																					BgL_zc3z04anonymousza31832ze3z87_3594:
																						if (PAIRP
																							(BgL_l1477z00_3595))
																							{	/* BackEnd/c.scm 319 */
																								{	/* BackEnd/c.scm 319 */
																									obj_t BgL_arg1834z00_3596;

																									BgL_arg1834z00_3596 =
																										CAR(BgL_l1477z00_3595);
																									BGl_z62zc3z04anonymousza31836ze3ze5zzbackend_cz00
																										(BgL_fun1479z00_3592,
																										BgL_arg1834z00_3596);
																								}
																								{
																									obj_t BgL_l1477z00_4462;

																									BgL_l1477z00_4462 =
																										CDR(BgL_l1477z00_3595);
																									BgL_l1477z00_3595 =
																										BgL_l1477z00_4462;
																									goto
																										BgL_zc3z04anonymousza31832ze3z87_3594;
																								}
																							}
																						else
																							{	/* BackEnd/c.scm 319 */
																								((bool_t) 1);
																							}
																					}
																				}
																			}
																		else
																			{	/* BackEnd/c.scm 315 */
																				((bool_t) 0);
																			}
																	}
															}
														else
															{	/* BackEnd/c.scm 315 */
																((bool_t) 0);
															}
													}
												}
												{
													obj_t BgL_l1480z00_4464;

													BgL_l1480z00_4464 = CDR(BgL_l1480z00_3584);
													BgL_l1480z00_3584 = BgL_l1480z00_4464;
													goto BgL_zc3z04anonymousza31779ze3z87_3583;
												}
											}
										else
											{	/* BackEnd/c.scm 314 */
												((bool_t) 1);
											}
									}
									BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00();
									{	/* BackEnd/c.scm 327 */
										obj_t BgL_list1852z00_3599;

										BgL_list1852z00_3599 =
											MAKE_YOUNG_PAIR(BgL_fmainz00_3580, BNIL);
										BGl_za2srczd2filesza2zd2zzengine_paramz00 =
											BgL_list1852z00_3599;
									}
									BGl_za2ozd2filesza2zd2zzengine_paramz00 =
										CDR(BGl_za2ozd2filesza2zd2zzengine_paramz00);
									return BGl_ldz00zzcc_ldz00(BgL_firstz00_3582, ((bool_t) 0));
								}
							else
								{	/* BackEnd/c.scm 331 */
									obj_t BgL_tmpz00_3600;

									BgL_tmpz00_3600 =
										BGl_makezd2tmpzd2filezd2namezd2zzbackend_cz00();
									BGl_makezd2tmpzd2mainz00zzengine_linkz00(BgL_tmpz00_3600,
										CBOOL(BgL_mainz00_3579),
										BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(17)),
										BgL_clausesz00_3578, BgL_librariesz00_3581);
									{	/* BackEnd/c.scm 333 */
										obj_t BgL_list1855z00_3601;

										BgL_list1855z00_3601 =
											MAKE_YOUNG_PAIR(BgL_tmpz00_3600, BNIL);
										BGl_za2srczd2filesza2zd2zzengine_paramz00 =
											BgL_list1855z00_3601;
									}
									{
										obj_t BgL_raz00_3603;
										obj_t BgL_resz00_3604;

										BgL_raz00_3603 = BGl_za2restzd2argsza2zd2zzengine_paramz00;
										BgL_resz00_3604 = BNIL;
									BgL_loopz00_3602:
										if (NULLP(BgL_raz00_3603))
											{	/* BackEnd/c.scm 339 */
												BGl_za2restzd2argsza2zd2zzengine_paramz00 =
													bgl_reverse_bang(BgL_resz00_3604);
											}
										else
											{	/* BackEnd/c.scm 341 */
												bool_t BgL_test2202z00_4479;

												{	/* BackEnd/c.scm 341 */
													obj_t BgL_arg1868z00_3605;

													{	/* BackEnd/c.scm 341 */
														obj_t BgL_arg1869z00_3606;

														BgL_arg1869z00_3606 = CAR(((obj_t) BgL_raz00_3603));
														BgL_arg1868z00_3605 =
															BGl_suffixz00zz__osz00(BgL_arg1869z00_3606);
													}
													BgL_test2202z00_4479 =
														CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1868z00_3605,
															BGl_za2mcozd2suffixza2zd2zzengine_paramz00));
												}
												if (BgL_test2202z00_4479)
													{	/* BackEnd/c.scm 342 */
														obj_t BgL_arg1862z00_3607;

														BgL_arg1862z00_3607 = CDR(((obj_t) BgL_raz00_3603));
														{
															obj_t BgL_raz00_4487;

															BgL_raz00_4487 = BgL_arg1862z00_3607;
															BgL_raz00_3603 = BgL_raz00_4487;
															goto BgL_loopz00_3602;
														}
													}
												else
													{	/* BackEnd/c.scm 344 */
														obj_t BgL_arg1863z00_3608;
														obj_t BgL_arg1864z00_3609;

														BgL_arg1863z00_3608 = CDR(((obj_t) BgL_raz00_3603));
														{	/* BackEnd/c.scm 344 */
															obj_t BgL_arg1866z00_3610;

															BgL_arg1866z00_3610 =
																CAR(((obj_t) BgL_raz00_3603));
															BgL_arg1864z00_3609 =
																MAKE_YOUNG_PAIR(BgL_arg1866z00_3610,
																BgL_resz00_3604);
														}
														{
															obj_t BgL_resz00_4494;
															obj_t BgL_raz00_4493;

															BgL_raz00_4493 = BgL_arg1863z00_3608;
															BgL_resz00_4494 = BgL_arg1864z00_3609;
															BgL_resz00_3604 = BgL_resz00_4494;
															BgL_raz00_3603 = BgL_raz00_4493;
															goto BgL_loopz00_3602;
														}
													}
											}
									}
									{	/* BackEnd/c.scm 345 */
										obj_t BgL_exitd1132z00_3611;

										BgL_exitd1132z00_3611 = BGL_EXITD_TOP_AS_OBJ();
										{	/* BackEnd/c.scm 349 */
											obj_t BgL_zc3z04anonymousza31870ze3z87_3612;

											BgL_zc3z04anonymousza31870ze3z87_3612 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31870ze3ze5zzbackend_cz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31870ze3z87_3612,
												(int) (0L), BgL_tmpz00_3600);
											{	/* BackEnd/c.scm 345 */
												obj_t BgL_arg1828z00_3613;

												{	/* BackEnd/c.scm 345 */
													obj_t BgL_arg1829z00_3614;

													BgL_arg1829z00_3614 =
														BGL_EXITD_PROTECT(BgL_exitd1132z00_3611);
													BgL_arg1828z00_3613 =
														MAKE_YOUNG_PAIR
														(BgL_zc3z04anonymousza31870ze3z87_3612,
														BgL_arg1829z00_3614);
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1132z00_3611,
													BgL_arg1828z00_3613);
												BUNSPEC;
											}
											{	/* BackEnd/c.scm 346 */
												obj_t BgL_tmp1134z00_3615;

												BgL_tmp1134z00_3615 =
													BGl_compilerz00zzengine_compilerz00();
												{	/* BackEnd/c.scm 345 */
													bool_t BgL_test2203z00_4505;

													{	/* BackEnd/c.scm 345 */
														obj_t BgL_arg1827z00_3616;

														BgL_arg1827z00_3616 =
															BGL_EXITD_PROTECT(BgL_exitd1132z00_3611);
														BgL_test2203z00_4505 = PAIRP(BgL_arg1827z00_3616);
													}
													if (BgL_test2203z00_4505)
														{	/* BackEnd/c.scm 345 */
															obj_t BgL_arg1825z00_3617;

															{	/* BackEnd/c.scm 345 */
																obj_t BgL_arg1826z00_3618;

																BgL_arg1826z00_3618 =
																	BGL_EXITD_PROTECT(BgL_exitd1132z00_3611);
																BgL_arg1825z00_3617 =
																	CDR(((obj_t) BgL_arg1826z00_3618));
															}
															BGL_EXITD_PROTECT_SET(BgL_exitd1132z00_3611,
																BgL_arg1825z00_3617);
															BUNSPEC;
														}
													else
														{	/* BackEnd/c.scm 345 */
															BFALSE;
														}
												}
												BGl_z62zc3z04anonymousza31870ze3ze5zzbackend_cz00
													(BgL_zc3z04anonymousza31870ze3z87_3612);
												BgL_tmp1134z00_3615;
											}
										}
									}
									return BINT(0L);
								}
						}
					else
						{	/* BackEnd/c.scm 361 */
							obj_t BgL_portz00_3619;

							{	/* BackEnd/c.scm 361 */
								obj_t BgL_arg1904z00_3620;

								{	/* BackEnd/c.scm 361 */
									obj_t BgL_pairz00_3621;

									BgL_pairz00_3621 = CAR(((obj_t) BgL_sourcesz00_3577));
									BgL_arg1904z00_3620 = CAR(BgL_pairz00_3621);
								}
								{	/* BackEnd/c.scm 361 */

									BgL_portz00_3619 =
										BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
										(BgL_arg1904z00_3620, BTRUE, BINT(5000000L));
								}
							}
							if (INPUT_PORTP(BgL_portz00_3619))
								{	/* BackEnd/c.scm 364 */
									obj_t BgL_expz00_3622;

									{	/* BackEnd/c.scm 364 */
										obj_t BgL_arg1901z00_3623;

										{	/* BackEnd/c.scm 364 */
											obj_t BgL_list1902z00_3624;

											BgL_list1902z00_3624 =
												MAKE_YOUNG_PAIR(BgL_portz00_3619, BNIL);
											BgL_arg1901z00_3623 =
												BGl_compilerzd2readzd2zzread_readerz00
												(BgL_list1902z00_3624);
										}
										BgL_expz00_3622 =
											BGl_expandz00zz__expandz00(BgL_arg1901z00_3623);
									}
									bgl_close_input_port(BgL_portz00_3619);
									{
										obj_t BgL_namez00_3627;

										if (PAIRP(BgL_expz00_3622))
											{	/* BackEnd/c.scm 366 */
												obj_t BgL_cdrzd2422zd2_3647;

												BgL_cdrzd2422zd2_3647 = CDR(((obj_t) BgL_expz00_3622));
												if (
													(CAR(
															((obj_t) BgL_expz00_3622)) == CNST_TABLE_REF(17)))
													{	/* BackEnd/c.scm 366 */
														if (PAIRP(BgL_cdrzd2422zd2_3647))
															{	/* BackEnd/c.scm 366 */
																BgL_namez00_3627 = CAR(BgL_cdrzd2422zd2_3647);
																{	/* BackEnd/c.scm 368 */
																	obj_t BgL_libsz00_3628;
																	obj_t BgL_nmainz00_3629;

																	{	/* BackEnd/c.scm 368 */
																		obj_t BgL_arg1897z00_3630;

																		{	/* BackEnd/c.scm 368 */
																			obj_t BgL_pairz00_3631;

																			BgL_pairz00_3631 =
																				CDR(((obj_t) BgL_expz00_3622));
																			BgL_arg1897z00_3630 =
																				CDR(BgL_pairz00_3631);
																		}
																		BgL_libsz00_3628 =
																			BGl_findzd2librarieszd2zzengine_linkz00
																			(BgL_arg1897z00_3630);
																	}
																	{	/* BackEnd/c.scm 369 */
																		obj_t BgL_arg1898z00_3632;

																		{	/* BackEnd/c.scm 369 */
																			obj_t BgL_pairz00_3633;

																			BgL_pairz00_3633 =
																				CDR(((obj_t) BgL_expz00_3622));
																			BgL_arg1898z00_3632 =
																				CDR(BgL_pairz00_3633);
																		}
																		BgL_nmainz00_3629 =
																			BGl_findzd2mainzd2zzengine_linkz00
																			(BgL_arg1898z00_3632);
																	}
																	{	/* BackEnd/c.scm 370 */
																		obj_t BgL_arg1885z00_3634;
																		obj_t BgL_arg1887z00_3635;
																		obj_t BgL_arg1888z00_3636;
																		obj_t BgL_arg1889z00_3637;
																		obj_t BgL_arg1890z00_3638;

																		BgL_arg1885z00_3634 =
																			CDR(((obj_t) BgL_sourcesz00_3577));
																		{	/* BackEnd/c.scm 373 */
																			obj_t BgL_arg1891z00_3639;

																			{	/* BackEnd/c.scm 373 */
																				obj_t BgL_arg1892z00_3640;

																				{	/* BackEnd/c.scm 373 */
																					obj_t BgL_arg1896z00_3641;

																					{	/* BackEnd/c.scm 373 */
																						obj_t BgL_pairz00_3642;

																						BgL_pairz00_3642 =
																							CAR(
																							((obj_t) BgL_sourcesz00_3577));
																						BgL_arg1896z00_3641 =
																							CAR(BgL_pairz00_3642);
																					}
																					BgL_arg1892z00_3640 =
																						string_append_3
																						(BGl_string2051z00zzbackend_cz00,
																						BgL_arg1896z00_3641,
																						BGl_string2051z00zzbackend_cz00);
																				}
																				{	/* BackEnd/c.scm 371 */
																					obj_t BgL_list1893z00_3643;

																					{	/* BackEnd/c.scm 371 */
																						obj_t BgL_arg1894z00_3644;

																						BgL_arg1894z00_3644 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1892z00_3640, BNIL);
																						BgL_list1893z00_3643 =
																							MAKE_YOUNG_PAIR(BgL_namez00_3627,
																							BgL_arg1894z00_3644);
																					}
																					BgL_arg1891z00_3639 =
																						BgL_list1893z00_3643;
																				}
																			}
																			BgL_arg1887z00_3635 =
																				MAKE_YOUNG_PAIR(BgL_arg1891z00_3639,
																				BgL_clausesz00_3578);
																		}
																		if (CBOOL(BgL_nmainz00_3629))
																			{	/* BackEnd/c.scm 375 */
																				BgL_arg1888z00_3636 = BgL_nmainz00_3629;
																			}
																		else
																			{	/* BackEnd/c.scm 375 */
																				BgL_arg1888z00_3636 = BgL_mainz00_3579;
																			}
																		if (CBOOL(BgL_nmainz00_3629))
																			{	/* BackEnd/c.scm 376 */
																				obj_t BgL_pairz00_3645;

																				BgL_pairz00_3645 =
																					CAR(((obj_t) BgL_sourcesz00_3577));
																				BgL_arg1889z00_3637 =
																					CAR(BgL_pairz00_3645);
																			}
																		else
																			{	/* BackEnd/c.scm 376 */
																				BgL_arg1889z00_3637 = BgL_fmainz00_3580;
																			}
																		BgL_arg1890z00_3638 =
																			BGl_appendzd221011zd2zzbackend_cz00
																			(BgL_libsz00_3628, BgL_librariesz00_3581);
																		{
																			obj_t BgL_librariesz00_4565;
																			obj_t BgL_fmainz00_4564;
																			obj_t BgL_mainz00_4563;
																			obj_t BgL_clausesz00_4562;
																			obj_t BgL_sourcesz00_4561;

																			BgL_sourcesz00_4561 = BgL_arg1885z00_3634;
																			BgL_clausesz00_4562 = BgL_arg1887z00_3635;
																			BgL_mainz00_4563 = BgL_arg1888z00_3636;
																			BgL_fmainz00_4564 = BgL_arg1889z00_3637;
																			BgL_librariesz00_4565 =
																				BgL_arg1890z00_3638;
																			BgL_librariesz00_3581 =
																				BgL_librariesz00_4565;
																			BgL_fmainz00_3580 = BgL_fmainz00_4564;
																			BgL_mainz00_3579 = BgL_mainz00_4563;
																			BgL_clausesz00_3578 = BgL_clausesz00_4562;
																			BgL_sourcesz00_3577 = BgL_sourcesz00_4561;
																			goto BgL_loopz00_3576;
																		}
																	}
																}
															}
														else
															{	/* BackEnd/c.scm 366 */
															BgL_tagzd2417zd2_3626:
																{	/* BackEnd/c.scm 379 */
																	obj_t BgL_arg1899z00_3646;

																	BgL_arg1899z00_3646 =
																		CDR(((obj_t) BgL_sourcesz00_3577));
																	{
																		obj_t BgL_sourcesz00_4569;

																		BgL_sourcesz00_4569 = BgL_arg1899z00_3646;
																		BgL_sourcesz00_3577 = BgL_sourcesz00_4569;
																		goto BgL_loopz00_3576;
																	}
																}
															}
													}
												else
													{	/* BackEnd/c.scm 366 */
														goto BgL_tagzd2417zd2_3626;
													}
											}
										else
											{	/* BackEnd/c.scm 366 */
												goto BgL_tagzd2417zd2_3626;
											}
									}
								}
							else
								{	/* BackEnd/c.scm 363 */
									obj_t BgL_arg1903z00_3648;

									{	/* BackEnd/c.scm 363 */
										obj_t BgL_pairz00_3649;

										BgL_pairz00_3649 = CAR(((obj_t) BgL_sourcesz00_3577));
										BgL_arg1903z00_3648 = CAR(BgL_pairz00_3649);
									}
									return
										BGl_errorz00zz__errorz00(BGl_string2032z00zzbackend_cz00,
										BGl_string2052z00zzbackend_cz00, BgL_arg1903z00_3648);
								}
						}
				}
		}

	}



/* &<@anonymous:1836> */
	bool_t BGl_z62zc3z04anonymousza31836ze3ze5zzbackend_cz00(obj_t
		BgL_envz00_3464, obj_t BgL_ezd2410zd2_3465)
	{
		{	/* BackEnd/c.scm 319 */
			{
				obj_t BgL_libsz00_3651;

				if (PAIRP(BgL_ezd2410zd2_3465))
					{	/* BackEnd/c.scm 319 */
						if ((CAR(BgL_ezd2410zd2_3465) == CNST_TABLE_REF(15)))
							{	/* BackEnd/c.scm 319 */
								BgL_libsz00_3651 = CDR(BgL_ezd2410zd2_3465);
								{
									obj_t BgL_l1473z00_3653;

									BgL_l1473z00_3653 = BgL_libsz00_3651;
								BgL_zc3z04anonymousza31843ze3z87_3652:
									if (PAIRP(BgL_l1473z00_3653))
										{	/* BackEnd/c.scm 321 */
											BGl_usezd2libraryz12zc0zzmodule_alibraryz00(CAR
												(BgL_l1473z00_3653));
											{
												obj_t BgL_l1473z00_4584;

												BgL_l1473z00_4584 = CDR(BgL_l1473z00_3653);
												BgL_l1473z00_3653 = BgL_l1473z00_4584;
												goto BgL_zc3z04anonymousza31843ze3z87_3652;
											}
										}
									else
										{	/* BackEnd/c.scm 321 */
											((bool_t) 1);
										}
								}
								{
									obj_t BgL_l1475z00_3655;

									BgL_l1475z00_3655 = BgL_libsz00_3651;
								BgL_zc3z04anonymousza31847ze3z87_3654:
									if (PAIRP(BgL_l1475z00_3655))
										{	/* BackEnd/c.scm 322 */
											BGl_addzd2evalzd2libraryz12z12zzmodule_evalz00(CAR
												(BgL_l1475z00_3655));
											{
												obj_t BgL_l1475z00_4590;

												BgL_l1475z00_4590 = CDR(BgL_l1475z00_3655);
												BgL_l1475z00_3655 = BgL_l1475z00_4590;
												goto BgL_zc3z04anonymousza31847ze3z87_3654;
											}
										}
									else
										{	/* BackEnd/c.scm 322 */
											return ((bool_t) 1);
										}
								}
							}
						else
							{	/* BackEnd/c.scm 319 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* BackEnd/c.scm 319 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &<@anonymous:1870> */
	obj_t BGl_z62zc3z04anonymousza31870ze3ze5zzbackend_cz00(obj_t BgL_envz00_3466)
	{
		{	/* BackEnd/c.scm 345 */
			{	/* BackEnd/c.scm 349 */
				obj_t BgL_tmpz00_3467;

				BgL_tmpz00_3467 = ((obj_t) PROCEDURE_REF(BgL_envz00_3466, (int) (0L)));
				{	/* BackEnd/c.scm 349 */
					bool_t BgL_tmpz00_4596;

					BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00();
					{	/* BackEnd/c.scm 350 */
						obj_t BgL_prez00_3656;

						BgL_prez00_3656 = BGl_prefixz00zz__osz00(BgL_tmpz00_3467);
						{	/* BackEnd/c.scm 350 */
							obj_t BgL_czd2filezd2_3657;

							BgL_czd2filezd2_3657 =
								string_append(BgL_prez00_3656, BGl_string2029z00zzbackend_cz00);
							{	/* BackEnd/c.scm 351 */
								obj_t BgL_ozd2filezd2_3658;

								BgL_ozd2filezd2_3658 =
									string_append_3(BgL_prez00_3656,
									BGl_string2033z00zzbackend_cz00,
									BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00);
								{	/* BackEnd/c.scm 352 */

									{	/* BackEnd/c.scm 356 */
										obj_t BgL_g1484z00_3659;

										{	/* BackEnd/c.scm 359 */
											obj_t BgL_list1875z00_3660;

											{	/* BackEnd/c.scm 359 */
												obj_t BgL_arg1876z00_3661;

												{	/* BackEnd/c.scm 359 */
													obj_t BgL_arg1877z00_3662;

													BgL_arg1877z00_3662 =
														MAKE_YOUNG_PAIR(BgL_ozd2filezd2_3658, BNIL);
													BgL_arg1876z00_3661 =
														MAKE_YOUNG_PAIR(BgL_czd2filezd2_3657,
														BgL_arg1877z00_3662);
												}
												BgL_list1875z00_3660 =
													MAKE_YOUNG_PAIR(BgL_tmpz00_3467, BgL_arg1876z00_3661);
											}
											BgL_g1484z00_3659 = BgL_list1875z00_3660;
										}
										{
											obj_t BgL_l1482z00_3664;

											BgL_l1482z00_3664 = BgL_g1484z00_3659;
										BgL_zc3z04anonymousza31871ze3z87_3663:
											if (PAIRP(BgL_l1482z00_3664))
												{	/* BackEnd/c.scm 359 */
													{	/* BackEnd/c.scm 357 */
														obj_t BgL_fz00_3665;

														BgL_fz00_3665 = CAR(BgL_l1482z00_3664);
														if (fexists(BSTRING_TO_STRING(BgL_fz00_3665)))
															{	/* BackEnd/c.scm 358 */
																char *BgL_stringz00_3666;

																BgL_stringz00_3666 =
																	BSTRING_TO_STRING(BgL_fz00_3665);
																if (unlink(BgL_stringz00_3666))
																	{	/* BackEnd/c.scm 358 */
																		((bool_t) 0);
																	}
																else
																	{	/* BackEnd/c.scm 358 */
																		((bool_t) 1);
																	}
															}
														else
															{	/* BackEnd/c.scm 357 */
																((bool_t) 0);
															}
													}
													{
														obj_t BgL_l1482z00_4613;

														BgL_l1482z00_4613 = CDR(BgL_l1482z00_3664);
														BgL_l1482z00_3664 = BgL_l1482z00_4613;
														goto BgL_zc3z04anonymousza31871ze3z87_3663;
													}
												}
											else
												{	/* BackEnd/c.scm 359 */
													BgL_tmpz00_4596 = ((bool_t) 1);
												}
										}
									}
								}
							}
						}
					}
					return BBOOL(BgL_tmpz00_4596);
				}
			}
		}

	}



/* &backend-cnst-table-n1490 */
	obj_t BGl_z62backendzd2cnstzd2tablezd2n1490zb0zzbackend_cz00(obj_t
		BgL_envz00_3468, obj_t BgL_mez00_3469, obj_t BgL_offsetz00_3470)
	{
		{	/* BackEnd/c.scm 272 */
			if (((long) CINT(BgL_offsetz00_3470) == 0L))
				{	/* BackEnd/c.scm 273 */
					return BGl_string2053z00zzbackend_cz00;
				}
			else
				{	/* BackEnd/c.scm 275 */
					obj_t BgL_arg1765z00_3668;

					{	/* BackEnd/c.scm 275 */

						BgL_arg1765z00_3668 =
							BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_offsetz00_3470,
							BINT(10L));
					}
					return
						string_append_3(BGl_string2054z00zzbackend_cz00,
						BgL_arg1765z00_3668, BGl_string2055z00zzbackend_cz00);
				}
		}

	}



/* &backend-link-cvm1488 */
	obj_t BGl_z62backendzd2linkzd2cvm1488z62zzbackend_cz00(obj_t BgL_envz00_3471,
		obj_t BgL_mez00_3472, obj_t BgL_resultz00_3473)
	{
		{	/* BackEnd/c.scm 107 */
			if (STRINGP(BgL_resultz00_3473))
				{	/* BackEnd/c.scm 108 */
					return BGl_cczd2compilerzd2zzbackend_cz00(BgL_resultz00_3473, BFALSE);
				}
			else
				{	/* BackEnd/c.scm 108 */
					return BFALSE;
				}
		}

	}



/* &backend-compile-cvm1486 */
	obj_t BGl_z62backendzd2compilezd2cvm1486z62zzbackend_cz00(obj_t
		BgL_envz00_3474, obj_t BgL_mez00_3475)
	{
		{	/* BackEnd/c.scm 95 */
			{	/* BackEnd/c.scm 96 */
				obj_t BgL_czd2prefixzd2_3671;

				BgL_czd2prefixzd2_3671 =
					BGl_czd2walkzd2zzbackend_cz00(((BgL_cvmz00_bglt) BgL_mez00_3475));
				BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(18),
					BGl_proc2056z00zzbackend_cz00);
				BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(19),
					BGl_proc2057z00zzbackend_cz00);
				if (STRINGP(BgL_czd2prefixzd2_3671))
					{	/* BackEnd/c.scm 99 */
						{	/* BackEnd/c.scm 100 */
							bool_t BgL_test2220z00_4633;

							if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(20)))
								{	/* BackEnd/c.scm 100 */
									BgL_test2220z00_4633 = ((bool_t) 1);
								}
							else
								{	/* BackEnd/c.scm 100 */
									BgL_test2220z00_4633 =
										CBOOL(BGl_za2czd2debugza2zd2zzengine_paramz00);
								}
							if (BgL_test2220z00_4633)
								{	/* BackEnd/c.scm 100 */
									BGl_indentz00zzcc_indentz00(BgL_czd2prefixzd2_3671);
								}
							else
								{	/* BackEnd/c.scm 100 */
									BFALSE;
								}
						}
						BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(20),
							BGl_proc2058z00zzbackend_cz00);
					}
				else
					{	/* BackEnd/c.scm 99 */
						BFALSE;
					}
				return BgL_czd2prefixzd2_3671;
			}
		}

	}



/* &<@anonymous:1762> */
	obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzbackend_cz00(obj_t BgL_envz00_3476)
	{
		{	/* BackEnd/c.scm 101 */
			return CNST_TABLE_REF(12);
		}

	}



/* &<@anonymous:1756> */
	obj_t BGl_z62zc3z04anonymousza31756ze3ze5zzbackend_cz00(obj_t BgL_envz00_3477)
	{
		{	/* BackEnd/c.scm 98 */
			return CNST_TABLE_REF(12);
		}

	}



/* &<@anonymous:1754> */
	obj_t BGl_z62zc3z04anonymousza31754ze3ze5zzbackend_cz00(obj_t BgL_envz00_3478)
	{
		{	/* BackEnd/c.scm 97 */
			return CNST_TABLE_REF(12);
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_cz00(void)
	{
		{	/* BackEnd/c.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzengine_configurez00(272817175L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzengine_compilerz00(412406770L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzengine_linkz00(117219619L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzmodule_libraryz00(292140514L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(316727058L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzmodule_evalz00(428236825L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzast_buildz00(428035925L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzbdb_emitz00(182920176L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzbdb_settingz00(444161932L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzprof_emitz00(522750846L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzbackend_initz00(517592975L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzbackend_cvmz00(18449009L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(474089076L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(364917963L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_mainz00(65542503L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzcc_indentz00(287974509L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzcc_ccz00(527776065L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzcc_ldz00(335315213L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzcc_rootsz00(74776091L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzinit_setrcz00(32737986L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzread_readerz00(95801752L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzast_typezd2occurzd2(176519367L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			BGl_modulezd2initializa7ationz75zzcgen_compilez00(281305322L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_c_compilez00(361755791L,
				BSTRING_TO_STRING(BGl_string2059z00zzbackend_cz00));
		}

	}

#ifdef __cplusplus
}
#endif
