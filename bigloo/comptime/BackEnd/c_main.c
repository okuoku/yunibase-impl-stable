/*===========================================================================*/
/*   (BackEnd/c_main.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent BackEnd/c_main.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BACKEND_C_MAIN_TYPE_DEFINITIONS
#define BGL_BACKEND_C_MAIN_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;


#endif													// BGL_BACKEND_C_MAIN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzbackend_c_mainz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_bglt);
	static obj_t BGl_genericzd2initzd2zzbackend_c_mainz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzbackend_c_mainz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbackend_c_mainz00(void);
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t BGl_makezd2bigloozd2mainz00zzbackend_c_mainz00(void);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzbackend_c_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzbackend_c_mainz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbackend_c_mainz00(void);
	static obj_t BGl_z62makezd2bigloozd2mainz62zzbackend_c_mainz00(obj_t);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzbackend_c_mainz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzbackend_c_mainz00(void);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_za2mainza2z00zzmodule_modulez00;
	static obj_t __cnst[17];


	   
		 
		DEFINE_STRING(BGl_string1646z00zzbackend_c_mainz00,
		BgL_bgl_string1646za700za7za7b1651za7, "bigloo_main", 11);
	      DEFINE_STRING(BGl_string1647z00zzbackend_c_mainz00,
		BgL_bgl_string1647za700za7za7b1652za7, "backend_c_main", 14);
	      DEFINE_STRING(BGl_string1648z00zzbackend_c_mainz00,
		BgL_bgl_string1648za700za7za7b1653za7,
		"export now bigloo-main-procedure sfun (argv::obj) bigloo_main::obj value let z z::bint $long->bint begin %exit @ bigloo-initialized! __param argv ",
		146);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bigloozd2mainzd2envzd2zzbackend_c_mainz00,
		BgL_bgl_za762makeza7d2bigloo1654z00,
		BGl_z62makezd2bigloozd2mainz62zzbackend_c_mainz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzbackend_c_mainz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzbackend_c_mainz00(long
		BgL_checksumz00_1945, char *BgL_fromz00_1946)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbackend_c_mainz00))
				{
					BGl_requirezd2initializa7ationz75zzbackend_c_mainz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbackend_c_mainz00();
					BGl_libraryzd2moduleszd2initz00zzbackend_c_mainz00();
					BGl_cnstzd2initzd2zzbackend_c_mainz00();
					BGl_importedzd2moduleszd2initz00zzbackend_c_mainz00();
					return BGl_methodzd2initzd2zzbackend_c_mainz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbackend_c_mainz00(void)
	{
		{	/* BackEnd/c_main.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "backend_c_main");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"backend_c_main");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "backend_c_main");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"backend_c_main");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"backend_c_main");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"backend_c_main");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"backend_c_main");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"backend_c_main");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"backend_c_main");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbackend_c_mainz00(void)
	{
		{	/* BackEnd/c_main.scm 15 */
			{	/* BackEnd/c_main.scm 15 */
				obj_t BgL_cportz00_1934;

				{	/* BackEnd/c_main.scm 15 */
					obj_t BgL_stringz00_1941;

					BgL_stringz00_1941 = BGl_string1648z00zzbackend_c_mainz00;
					{	/* BackEnd/c_main.scm 15 */
						obj_t BgL_startz00_1942;

						BgL_startz00_1942 = BINT(0L);
						{	/* BackEnd/c_main.scm 15 */
							obj_t BgL_endz00_1943;

							BgL_endz00_1943 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1941)));
							{	/* BackEnd/c_main.scm 15 */

								BgL_cportz00_1934 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1941, BgL_startz00_1942, BgL_endz00_1943);
				}}}}
				{
					long BgL_iz00_1935;

					BgL_iz00_1935 = 16L;
				BgL_loopz00_1936:
					if ((BgL_iz00_1935 == -1L))
						{	/* BackEnd/c_main.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* BackEnd/c_main.scm 15 */
							{	/* BackEnd/c_main.scm 15 */
								obj_t BgL_arg1650z00_1937;

								{	/* BackEnd/c_main.scm 15 */

									{	/* BackEnd/c_main.scm 15 */
										obj_t BgL_locationz00_1939;

										BgL_locationz00_1939 = BBOOL(((bool_t) 0));
										{	/* BackEnd/c_main.scm 15 */

											BgL_arg1650z00_1937 =
												BGl_readz00zz__readerz00(BgL_cportz00_1934,
												BgL_locationz00_1939);
										}
									}
								}
								{	/* BackEnd/c_main.scm 15 */
									int BgL_tmpz00_1973;

									BgL_tmpz00_1973 = (int) (BgL_iz00_1935);
									CNST_TABLE_SET(BgL_tmpz00_1973, BgL_arg1650z00_1937);
							}}
							{	/* BackEnd/c_main.scm 15 */
								int BgL_auxz00_1940;

								BgL_auxz00_1940 = (int) ((BgL_iz00_1935 - 1L));
								{
									long BgL_iz00_1978;

									BgL_iz00_1978 = (long) (BgL_auxz00_1940);
									BgL_iz00_1935 = BgL_iz00_1978;
									goto BgL_loopz00_1936;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbackend_c_mainz00(void)
	{
		{	/* BackEnd/c_main.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* make-bigloo-main */
	BGL_EXPORTED_DEF obj_t BGl_makezd2bigloozd2mainz00zzbackend_c_mainz00(void)
	{
		{	/* BackEnd/c_main.scm 37 */
			{	/* BackEnd/c_main.scm 38 */
				obj_t BgL_argsz00_1619;

				{	/* BackEnd/c_main.scm 38 */
					BgL_localz00_bglt BgL_arg1367z00_1669;

					BgL_arg1367z00_1669 =
						BGl_makezd2localzd2svarz00zzast_localz00(CNST_TABLE_REF(0),
						((BgL_typez00_bglt) BGl_za2pairza2z00zztype_cachez00));
					{	/* BackEnd/c_main.scm 38 */
						obj_t BgL_list1368z00_1670;

						BgL_list1368z00_1670 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_arg1367z00_1669), BNIL);
						BgL_argsz00_1619 = BgL_list1368z00_1670;
				}}
				{	/* BackEnd/c_main.scm 38 */
					obj_t BgL_mainzd2bodyzd2_1620;

					{	/* BackEnd/c_main.scm 39 */
						bool_t BgL_test1657z00_1986;

						{	/* BackEnd/c_main.scm 39 */
							obj_t BgL_objz00_1855;

							BgL_objz00_1855 = BGl_za2mainza2z00zzmodule_modulez00;
							{	/* BackEnd/c_main.scm 39 */
								obj_t BgL_classz00_1856;

								BgL_classz00_1856 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_objz00_1855))
									{	/* BackEnd/c_main.scm 39 */
										BgL_objectz00_bglt BgL_arg1807z00_1858;

										BgL_arg1807z00_1858 =
											(BgL_objectz00_bglt) (BgL_objz00_1855);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* BackEnd/c_main.scm 39 */
												long BgL_idxz00_1864;

												BgL_idxz00_1864 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1858);
												BgL_test1657z00_1986 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_1864 + 2L)) == BgL_classz00_1856);
											}
										else
											{	/* BackEnd/c_main.scm 39 */
												bool_t BgL_res1644z00_1889;

												{	/* BackEnd/c_main.scm 39 */
													obj_t BgL_oclassz00_1872;

													{	/* BackEnd/c_main.scm 39 */
														obj_t BgL_arg1815z00_1880;
														long BgL_arg1816z00_1881;

														BgL_arg1815z00_1880 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* BackEnd/c_main.scm 39 */
															long BgL_arg1817z00_1882;

															BgL_arg1817z00_1882 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1858);
															BgL_arg1816z00_1881 =
																(BgL_arg1817z00_1882 - OBJECT_TYPE);
														}
														BgL_oclassz00_1872 =
															VECTOR_REF(BgL_arg1815z00_1880,
															BgL_arg1816z00_1881);
													}
													{	/* BackEnd/c_main.scm 39 */
														bool_t BgL__ortest_1115z00_1873;

														BgL__ortest_1115z00_1873 =
															(BgL_classz00_1856 == BgL_oclassz00_1872);
														if (BgL__ortest_1115z00_1873)
															{	/* BackEnd/c_main.scm 39 */
																BgL_res1644z00_1889 = BgL__ortest_1115z00_1873;
															}
														else
															{	/* BackEnd/c_main.scm 39 */
																long BgL_odepthz00_1874;

																{	/* BackEnd/c_main.scm 39 */
																	obj_t BgL_arg1804z00_1875;

																	BgL_arg1804z00_1875 = (BgL_oclassz00_1872);
																	BgL_odepthz00_1874 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_1875);
																}
																if ((2L < BgL_odepthz00_1874))
																	{	/* BackEnd/c_main.scm 39 */
																		obj_t BgL_arg1802z00_1877;

																		{	/* BackEnd/c_main.scm 39 */
																			obj_t BgL_arg1803z00_1878;

																			BgL_arg1803z00_1878 =
																				(BgL_oclassz00_1872);
																			BgL_arg1802z00_1877 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_1878, 2L);
																		}
																		BgL_res1644z00_1889 =
																			(BgL_arg1802z00_1877 ==
																			BgL_classz00_1856);
																	}
																else
																	{	/* BackEnd/c_main.scm 39 */
																		BgL_res1644z00_1889 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1657z00_1986 = BgL_res1644z00_1889;
											}
									}
								else
									{	/* BackEnd/c_main.scm 39 */
										BgL_test1657z00_1986 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test1657z00_1986)
							{	/* BackEnd/c_main.scm 41 */
								obj_t BgL_arg1248z00_1625;

								{	/* BackEnd/c_main.scm 41 */
									obj_t BgL_arg1249z00_1626;
									obj_t BgL_arg1252z00_1627;

									{	/* BackEnd/c_main.scm 41 */
										obj_t BgL_arg1268z00_1628;
										obj_t BgL_arg1272z00_1629;

										BgL_arg1268z00_1628 =
											BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00
											(BGl_za2moduleza2z00zzmodule_modulez00);
										{	/* BackEnd/c_main.scm 43 */
											obj_t BgL_arg1284z00_1630;

											{	/* BackEnd/c_main.scm 43 */
												obj_t BgL_arg1304z00_1631;

												{	/* BackEnd/c_main.scm 43 */
													obj_t BgL_symbolz00_1890;

													BgL_symbolz00_1890 =
														BGl_za2moduleza2z00zzmodule_modulez00;
													{	/* BackEnd/c_main.scm 43 */
														obj_t BgL_arg1455z00_1891;

														BgL_arg1455z00_1891 =
															SYMBOL_TO_STRING(BgL_symbolz00_1890);
														BgL_arg1304z00_1631 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1891);
													}
												}
												BgL_arg1284z00_1630 =
													MAKE_YOUNG_PAIR(BgL_arg1304z00_1631, BNIL);
											}
											BgL_arg1272z00_1629 =
												MAKE_YOUNG_PAIR(BINT(0L), BgL_arg1284z00_1630);
										}
										BgL_arg1249z00_1626 =
											MAKE_YOUNG_PAIR(BgL_arg1268z00_1628, BgL_arg1272z00_1629);
									}
									{	/* BackEnd/c_main.scm 44 */
										obj_t BgL_arg1305z00_1632;
										obj_t BgL_arg1306z00_1633;

										{	/* BackEnd/c_main.scm 44 */
											obj_t BgL_arg1307z00_1634;

											{	/* BackEnd/c_main.scm 44 */
												obj_t BgL_arg1308z00_1635;

												{	/* BackEnd/c_main.scm 44 */
													obj_t BgL_arg1310z00_1636;

													BgL_arg1310z00_1636 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BNIL);
													BgL_arg1308z00_1635 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
														BgL_arg1310z00_1636);
												}
												BgL_arg1307z00_1634 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
													BgL_arg1308z00_1635);
											}
											BgL_arg1305z00_1632 =
												MAKE_YOUNG_PAIR(BgL_arg1307z00_1634, BNIL);
										}
										{	/* BackEnd/c_main.scm 45 */
											obj_t BgL_arg1311z00_1637;

											{	/* BackEnd/c_main.scm 45 */
												obj_t BgL_arg1312z00_1638;

												{	/* BackEnd/c_main.scm 45 */
													obj_t BgL_arg1314z00_1639;

													{	/* BackEnd/c_main.scm 45 */
														obj_t BgL_arg1315z00_1640;
														obj_t BgL_arg1316z00_1641;

														{	/* BackEnd/c_main.scm 45 */
															obj_t BgL_arg1317z00_1642;

															{	/* BackEnd/c_main.scm 45 */
																obj_t BgL_arg1318z00_1643;
																obj_t BgL_arg1319z00_1644;

																{	/* BackEnd/c_main.scm 45 */
																	BgL_globalz00_bglt BgL_oz00_1892;

																	BgL_oz00_1892 =
																		((BgL_globalz00_bglt)
																		BGl_za2mainza2z00zzmodule_modulez00);
																	BgL_arg1318z00_1643 =
																		(((BgL_variablez00_bglt)
																			COBJECT(((BgL_variablez00_bglt)
																					BgL_oz00_1892)))->BgL_idz00);
																}
																{	/* BackEnd/c_main.scm 46 */
																	obj_t BgL_arg1320z00_1645;

																	{	/* BackEnd/c_main.scm 46 */
																		BgL_globalz00_bglt BgL_oz00_1893;

																		BgL_oz00_1893 =
																			((BgL_globalz00_bglt)
																			BGl_za2mainza2z00zzmodule_modulez00);
																		BgL_arg1320z00_1645 =
																			(((BgL_globalz00_bglt)
																				COBJECT(BgL_oz00_1893))->BgL_modulez00);
																	}
																	BgL_arg1319z00_1644 =
																		MAKE_YOUNG_PAIR(BgL_arg1320z00_1645, BNIL);
																}
																BgL_arg1317z00_1642 =
																	MAKE_YOUNG_PAIR(BgL_arg1318z00_1643,
																	BgL_arg1319z00_1644);
															}
															BgL_arg1315z00_1640 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																BgL_arg1317z00_1642);
														}
														BgL_arg1316z00_1641 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
														BgL_arg1314z00_1639 =
															MAKE_YOUNG_PAIR(BgL_arg1315z00_1640,
															BgL_arg1316z00_1641);
													}
													BgL_arg1312z00_1638 =
														MAKE_YOUNG_PAIR(BgL_arg1314z00_1639, BNIL);
												}
												BgL_arg1311z00_1637 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
													BgL_arg1312z00_1638);
											}
											BgL_arg1306z00_1633 =
												MAKE_YOUNG_PAIR(BgL_arg1311z00_1637, BNIL);
										}
										BgL_arg1252z00_1627 =
											MAKE_YOUNG_PAIR(BgL_arg1305z00_1632, BgL_arg1306z00_1633);
									}
									BgL_arg1248z00_1625 =
										MAKE_YOUNG_PAIR(BgL_arg1249z00_1626, BgL_arg1252z00_1627);
								}
								BgL_mainzd2bodyzd2_1620 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1248z00_1625);
							}
						else
							{	/* BackEnd/c_main.scm 49 */
								obj_t BgL_arg1321z00_1646;

								{	/* BackEnd/c_main.scm 49 */
									obj_t BgL_arg1322z00_1647;
									obj_t BgL_arg1323z00_1648;

									{	/* BackEnd/c_main.scm 49 */
										obj_t BgL_arg1325z00_1649;
										obj_t BgL_arg1326z00_1650;

										BgL_arg1325z00_1649 =
											BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00
											(BGl_za2moduleza2z00zzmodule_modulez00);
										{	/* BackEnd/c_main.scm 51 */
											obj_t BgL_arg1327z00_1651;

											{	/* BackEnd/c_main.scm 51 */
												obj_t BgL_arg1328z00_1652;

												{	/* BackEnd/c_main.scm 51 */
													obj_t BgL_symbolz00_1894;

													BgL_symbolz00_1894 =
														BGl_za2moduleza2z00zzmodule_modulez00;
													{	/* BackEnd/c_main.scm 51 */
														obj_t BgL_arg1455z00_1895;

														BgL_arg1455z00_1895 =
															SYMBOL_TO_STRING(BgL_symbolz00_1894);
														BgL_arg1328z00_1652 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1895);
													}
												}
												BgL_arg1327z00_1651 =
													MAKE_YOUNG_PAIR(BgL_arg1328z00_1652, BNIL);
											}
											BgL_arg1326z00_1650 =
												MAKE_YOUNG_PAIR(BINT(0L), BgL_arg1327z00_1651);
										}
										BgL_arg1322z00_1647 =
											MAKE_YOUNG_PAIR(BgL_arg1325z00_1649, BgL_arg1326z00_1650);
									}
									{	/* BackEnd/c_main.scm 52 */
										obj_t BgL_arg1329z00_1653;
										obj_t BgL_arg1331z00_1654;

										{	/* BackEnd/c_main.scm 52 */
											obj_t BgL_arg1332z00_1655;

											{	/* BackEnd/c_main.scm 52 */
												obj_t BgL_arg1333z00_1656;

												{	/* BackEnd/c_main.scm 52 */
													obj_t BgL_arg1335z00_1657;

													BgL_arg1335z00_1657 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BNIL);
													BgL_arg1333z00_1656 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
														BgL_arg1335z00_1657);
												}
												BgL_arg1332z00_1655 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
													BgL_arg1333z00_1656);
											}
											BgL_arg1329z00_1653 =
												MAKE_YOUNG_PAIR(BgL_arg1332z00_1655, BNIL);
										}
										{	/* BackEnd/c_main.scm 53 */
											obj_t BgL_arg1339z00_1658;
											obj_t BgL_arg1340z00_1659;

											{	/* BackEnd/c_main.scm 53 */
												obj_t BgL_arg1342z00_1660;

												{	/* BackEnd/c_main.scm 53 */
													obj_t BgL_arg1343z00_1661;
													obj_t BgL_arg1346z00_1662;

													{	/* BackEnd/c_main.scm 53 */
														obj_t BgL_arg1348z00_1663;

														{	/* BackEnd/c_main.scm 53 */
															obj_t BgL_arg1349z00_1664;

															{	/* BackEnd/c_main.scm 53 */
																obj_t BgL_arg1351z00_1665;

																{	/* BackEnd/c_main.scm 53 */
																	obj_t BgL_arg1352z00_1666;

																	BgL_arg1352z00_1666 =
																		MAKE_YOUNG_PAIR(BINT(0L), BNIL);
																	BgL_arg1351z00_1665 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																		BgL_arg1352z00_1666);
																}
																BgL_arg1349z00_1664 =
																	MAKE_YOUNG_PAIR(BgL_arg1351z00_1665, BNIL);
															}
															BgL_arg1348z00_1663 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																BgL_arg1349z00_1664);
														}
														BgL_arg1343z00_1661 =
															MAKE_YOUNG_PAIR(BgL_arg1348z00_1663, BNIL);
													}
													{	/* BackEnd/c_main.scm 54 */
														obj_t BgL_arg1361z00_1667;

														{	/* BackEnd/c_main.scm 54 */
															obj_t BgL_arg1364z00_1668;

															BgL_arg1364z00_1668 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
															BgL_arg1361z00_1667 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																BgL_arg1364z00_1668);
														}
														BgL_arg1346z00_1662 =
															MAKE_YOUNG_PAIR(BgL_arg1361z00_1667, BNIL);
													}
													BgL_arg1342z00_1660 =
														MAKE_YOUNG_PAIR(BgL_arg1343z00_1661,
														BgL_arg1346z00_1662);
												}
												BgL_arg1339z00_1658 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
													BgL_arg1342z00_1660);
											}
											BgL_arg1340z00_1659 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
											BgL_arg1331z00_1654 =
												MAKE_YOUNG_PAIR(BgL_arg1339z00_1658,
												BgL_arg1340z00_1659);
										}
										BgL_arg1323z00_1648 =
											MAKE_YOUNG_PAIR(BgL_arg1329z00_1653, BgL_arg1331z00_1654);
									}
									BgL_arg1321z00_1646 =
										MAKE_YOUNG_PAIR(BgL_arg1322z00_1647, BgL_arg1323z00_1648);
								}
								BgL_mainzd2bodyzd2_1620 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1321z00_1646);
							}
					}
					{	/* BackEnd/c_main.scm 39 */
						BgL_nodez00_bglt BgL_nodez00_1621;

						{	/* BackEnd/c_main.scm 56 */
							BgL_nodez00_bglt BgL_nodez00_1623;

							BgL_nodez00_1623 =
								BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_mainzd2bodyzd2_1620,
								BgL_argsz00_1619, BFALSE, CNST_TABLE_REF(10));
							BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_1623);
							BgL_nodez00_1621 = BgL_nodez00_1623;
						}
						{	/* BackEnd/c_main.scm 56 */
							BgL_globalz00_bglt BgL_bigloozd2mainzd2_1622;

							BgL_bigloozd2mainzd2_1622 =
								BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2(CNST_TABLE_REF
								(11), CNST_TABLE_REF(12), BgL_argsz00_1619,
								BGl_za2moduleza2z00zzmodule_modulez00, CNST_TABLE_REF(13),
								CNST_TABLE_REF(14), CNST_TABLE_REF(15),
								((obj_t) BgL_nodez00_1621));
							{	/* BackEnd/c_main.scm 59 */

								{	/* BackEnd/c_main.scm 67 */
									obj_t BgL_vz00_1897;

									BgL_vz00_1897 = CNST_TABLE_REF(16);
									((((BgL_globalz00_bglt) COBJECT(BgL_bigloozd2mainzd2_1622))->
											BgL_importz00) = ((obj_t) BgL_vz00_1897), BUNSPEC);
								}
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_bigloozd2mainzd2_1622)))->
										BgL_namez00) =
									((obj_t) BGl_string1646z00zzbackend_c_mainz00), BUNSPEC);
								return ((obj_t) BgL_bigloozd2mainzd2_1622);
							}
						}
					}
				}
			}
		}

	}



/* &make-bigloo-main */
	obj_t BGl_z62makezd2bigloozd2mainz62zzbackend_c_mainz00(obj_t BgL_envz00_1933)
	{
		{	/* BackEnd/c_main.scm 37 */
			return BGl_makezd2bigloozd2mainz00zzbackend_c_mainz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbackend_c_mainz00(void)
	{
		{	/* BackEnd/c_main.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbackend_c_mainz00(void)
	{
		{	/* BackEnd/c_main.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbackend_c_mainz00(void)
	{
		{	/* BackEnd/c_main.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbackend_c_mainz00(void)
	{
		{	/* BackEnd/c_main.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(364917963L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string1647z00zzbackend_c_mainz00));
		}

	}

#ifdef __cplusplus
}
#endif
