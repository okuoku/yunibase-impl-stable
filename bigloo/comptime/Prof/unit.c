/*===========================================================================*/
/*   (Prof/unit.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Prof/unit.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_PROF_WALK_TYPE_DEFINITIONS
#define BGL_PROF_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_PROF_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzprof_walkz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzprof_walkz00(void);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_getzd2toplevelzd2unitzd2weightzd2zzmodule_includez00(void);
	static obj_t BGl_genericzd2initzd2zzprof_walkz00(void);
	static obj_t BGl_objectzd2initzd2zzprof_walkz00(void);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzprof_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzprof_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zzprof_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzprof_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzprof_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzprof_walkz00(void);
	static obj_t BGl_z62makezd2profzd2unitz62zzprof_walkz00(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	BGL_EXPORTED_DECL obj_t BGl_makezd2profzd2unitz00zzprof_walkz00(void);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[5];


	   
		 
		DEFINE_STRING(BGl_string1492z00zzprof_walkz00,
		BgL_bgl_string1492za700za7za7p1503za7, "Prof", 4);
	      DEFINE_STRING(BGl_string1493z00zzprof_walkz00,
		BgL_bgl_string1493za700za7za7p1504za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1494z00zzprof_walkz00,
		BgL_bgl_string1494za700za7za7p1505za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1495z00zzprof_walkz00,
		BgL_bgl_string1495za700za7za7p1506za7, "prof_walk", 9);
	      DEFINE_STRING(BGl_string1496z00zzprof_walkz00,
		BgL_bgl_string1496za700za7za7p1507za7,
		"unit prof begin (begin (pragma \"write_bprof_table()\")) pass-started ",
		68);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2profzd2unitzd2envzd2zzprof_walkz00,
		BgL_bgl_za762makeza7d2profza7d1508za7,
		BGl_z62makezd2profzd2unitz62zzprof_walkz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzprof_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzprof_walkz00(long
		BgL_checksumz00_1719, char *BgL_fromz00_1720)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzprof_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzprof_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzprof_walkz00();
					BGl_libraryzd2moduleszd2initz00zzprof_walkz00();
					BGl_cnstzd2initzd2zzprof_walkz00();
					BGl_importedzd2moduleszd2initz00zzprof_walkz00();
					return BGl_toplevelzd2initzd2zzprof_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzprof_walkz00(void)
	{
		{	/* Prof/unit.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "prof_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "prof_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "prof_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "prof_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"prof_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"prof_walk");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "prof_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "prof_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzprof_walkz00(void)
	{
		{	/* Prof/unit.scm 15 */
			{	/* Prof/unit.scm 15 */
				obj_t BgL_cportz00_1708;

				{	/* Prof/unit.scm 15 */
					obj_t BgL_stringz00_1715;

					BgL_stringz00_1715 = BGl_string1496z00zzprof_walkz00;
					{	/* Prof/unit.scm 15 */
						obj_t BgL_startz00_1716;

						BgL_startz00_1716 = BINT(0L);
						{	/* Prof/unit.scm 15 */
							obj_t BgL_endz00_1717;

							BgL_endz00_1717 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1715)));
							{	/* Prof/unit.scm 15 */

								BgL_cportz00_1708 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1715, BgL_startz00_1716, BgL_endz00_1717);
				}}}}
				{
					long BgL_iz00_1709;

					BgL_iz00_1709 = 4L;
				BgL_loopz00_1710:
					if ((BgL_iz00_1709 == -1L))
						{	/* Prof/unit.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Prof/unit.scm 15 */
							{	/* Prof/unit.scm 15 */
								obj_t BgL_arg1502z00_1711;

								{	/* Prof/unit.scm 15 */

									{	/* Prof/unit.scm 15 */
										obj_t BgL_locationz00_1713;

										BgL_locationz00_1713 = BBOOL(((bool_t) 0));
										{	/* Prof/unit.scm 15 */

											BgL_arg1502z00_1711 =
												BGl_readz00zz__readerz00(BgL_cportz00_1708,
												BgL_locationz00_1713);
										}
									}
								}
								{	/* Prof/unit.scm 15 */
									int BgL_tmpz00_1746;

									BgL_tmpz00_1746 = (int) (BgL_iz00_1709);
									CNST_TABLE_SET(BgL_tmpz00_1746, BgL_arg1502z00_1711);
							}}
							{	/* Prof/unit.scm 15 */
								int BgL_auxz00_1714;

								BgL_auxz00_1714 = (int) ((BgL_iz00_1709 - 1L));
								{
									long BgL_iz00_1751;

									BgL_iz00_1751 = (long) (BgL_auxz00_1714);
									BgL_iz00_1709 = BgL_iz00_1751;
									goto BgL_loopz00_1710;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzprof_walkz00(void)
	{
		{	/* Prof/unit.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzprof_walkz00(void)
	{
		{	/* Prof/unit.scm 15 */
			return BUNSPEC;
		}

	}



/* make-prof-unit */
	BGL_EXPORTED_DEF obj_t BGl_makezd2profzd2unitz00zzprof_walkz00(void)
	{
		{	/* Prof/unit.scm 31 */
			{	/* Prof/unit.scm 32 */
				obj_t BgL_list1245z00_1485;

				{	/* Prof/unit.scm 32 */
					obj_t BgL_arg1248z00_1486;

					{	/* Prof/unit.scm 32 */
						obj_t BgL_arg1249z00_1487;

						BgL_arg1249z00_1487 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1248z00_1486 =
							MAKE_YOUNG_PAIR(BGl_string1492z00zzprof_walkz00,
							BgL_arg1249z00_1487);
					}
					BgL_list1245z00_1485 =
						MAKE_YOUNG_PAIR(BGl_string1493z00zzprof_walkz00,
						BgL_arg1248z00_1486);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1245z00_1485);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1492z00zzprof_walkz00;
			{	/* Prof/unit.scm 32 */
				obj_t BgL_g1107z00_1488;

				BgL_g1107z00_1488 = BNIL;
				{
					obj_t BgL_hooksz00_1491;
					obj_t BgL_hnamesz00_1492;

					BgL_hooksz00_1491 = BgL_g1107z00_1488;
					BgL_hnamesz00_1492 = BNIL;
				BgL_zc3z04anonymousza31250ze3z87_1493:
					if (NULLP(BgL_hooksz00_1491))
						{	/* Prof/unit.scm 32 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Prof/unit.scm 32 */
							bool_t BgL_test1512z00_1764;

							{	/* Prof/unit.scm 32 */
								obj_t BgL_fun1285z00_1500;

								BgL_fun1285z00_1500 = CAR(((obj_t) BgL_hooksz00_1491));
								BgL_test1512z00_1764 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1285z00_1500));
							}
							if (BgL_test1512z00_1764)
								{	/* Prof/unit.scm 32 */
									obj_t BgL_arg1268z00_1497;
									obj_t BgL_arg1272z00_1498;

									BgL_arg1268z00_1497 = CDR(((obj_t) BgL_hooksz00_1491));
									BgL_arg1272z00_1498 = CDR(((obj_t) BgL_hnamesz00_1492));
									{
										obj_t BgL_hnamesz00_1776;
										obj_t BgL_hooksz00_1775;

										BgL_hooksz00_1775 = BgL_arg1268z00_1497;
										BgL_hnamesz00_1776 = BgL_arg1272z00_1498;
										BgL_hnamesz00_1492 = BgL_hnamesz00_1776;
										BgL_hooksz00_1491 = BgL_hooksz00_1775;
										goto BgL_zc3z04anonymousza31250ze3z87_1493;
									}
								}
							else
								{	/* Prof/unit.scm 32 */
									obj_t BgL_arg1284z00_1499;

									BgL_arg1284z00_1499 = CAR(((obj_t) BgL_hnamesz00_1492));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1492z00zzprof_walkz00,
										BGl_string1494z00zzprof_walkz00, BgL_arg1284z00_1499);
								}
						}
				}
			}
			{	/* Prof/unit.scm 34 */
				obj_t BgL_arg1304z00_1503;
				obj_t BgL_arg1305z00_1504;

				{	/* Prof/unit.scm 34 */
					obj_t BgL_b1227z00_1506;

					BgL_b1227z00_1506 =
						BGl_getzd2toplevelzd2unitzd2weightzd2zzmodule_includez00();
					{	/* Prof/unit.scm 34 */

						if (INTEGERP(BgL_b1227z00_1506))
							{	/* Prof/unit.scm 34 */
								obj_t BgL_za71za7_1688;

								BgL_za71za7_1688 = BINT(100L);
								{	/* Prof/unit.scm 34 */
									obj_t BgL_tmpz00_1690;

									BgL_tmpz00_1690 = BINT(0L);
									if (BGL_ADDFX_OV(BgL_za71za7_1688, BgL_b1227z00_1506,
											BgL_tmpz00_1690))
										{	/* Prof/unit.scm 34 */
											BgL_arg1304z00_1503 =
												bgl_bignum_add(bgl_long_to_bignum(
													(long) CINT(BgL_za71za7_1688)),
												bgl_long_to_bignum((long) CINT(BgL_b1227z00_1506)));
										}
									else
										{	/* Prof/unit.scm 34 */
											BgL_arg1304z00_1503 = BgL_tmpz00_1690;
										}
								}
							}
						else
							{	/* Prof/unit.scm 34 */
								BgL_arg1304z00_1503 =
									BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(100L),
									BgL_b1227z00_1506);
							}
					}
				}
				{	/* Prof/unit.scm 35 */
					bool_t BgL_test1515z00_1794;

					{	/* Prof/unit.scm 35 */
						obj_t BgL_arg1311z00_1511;

						BgL_arg1311z00_1511 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_test1515z00_1794 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1311z00_1511)))->
							BgL_pragmazd2supportzd2);
					}
					if (BgL_test1515z00_1794)
						{	/* Prof/unit.scm 35 */
							BgL_arg1305z00_1504 = CNST_TABLE_REF(1);
						}
					else
						{	/* Prof/unit.scm 37 */
							obj_t BgL_arg1310z00_1510;

							BgL_arg1310z00_1510 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
							BgL_arg1305z00_1504 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1310z00_1510);
						}
				}
				{	/* Prof/unit.scm 33 */
					obj_t BgL_idz00_1699;

					BgL_idz00_1699 = CNST_TABLE_REF(3);
					{	/* Prof/unit.scm 33 */
						obj_t BgL_newz00_1700;

						BgL_newz00_1700 = create_struct(CNST_TABLE_REF(4), (int) (5L));
						{	/* Prof/unit.scm 33 */
							int BgL_tmpz00_1806;

							BgL_tmpz00_1806 = (int) (4L);
							STRUCT_SET(BgL_newz00_1700, BgL_tmpz00_1806, BFALSE);
						}
						{	/* Prof/unit.scm 33 */
							int BgL_tmpz00_1809;

							BgL_tmpz00_1809 = (int) (3L);
							STRUCT_SET(BgL_newz00_1700, BgL_tmpz00_1809, BTRUE);
						}
						{	/* Prof/unit.scm 33 */
							int BgL_tmpz00_1812;

							BgL_tmpz00_1812 = (int) (2L);
							STRUCT_SET(BgL_newz00_1700, BgL_tmpz00_1812, BgL_arg1305z00_1504);
						}
						{	/* Prof/unit.scm 33 */
							int BgL_tmpz00_1815;

							BgL_tmpz00_1815 = (int) (1L);
							STRUCT_SET(BgL_newz00_1700, BgL_tmpz00_1815, BgL_arg1304z00_1503);
						}
						{	/* Prof/unit.scm 33 */
							int BgL_tmpz00_1818;

							BgL_tmpz00_1818 = (int) (0L);
							STRUCT_SET(BgL_newz00_1700, BgL_tmpz00_1818, BgL_idz00_1699);
						}
						return BgL_newz00_1700;
					}
				}
			}
		}

	}



/* &make-prof-unit */
	obj_t BGl_z62makezd2profzd2unitz62zzprof_walkz00(obj_t BgL_envz00_1706)
	{
		{	/* Prof/unit.scm 31 */
			return BGl_makezd2profzd2unitz00zzprof_walkz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzprof_walkz00(void)
	{
		{	/* Prof/unit.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzprof_walkz00(void)
	{
		{	/* Prof/unit.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzprof_walkz00(void)
	{
		{	/* Prof/unit.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzprof_walkz00(void)
	{
		{	/* Prof/unit.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1495z00zzprof_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
