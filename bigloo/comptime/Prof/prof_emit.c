/*===========================================================================*/
/*   (Prof/prof_emit.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Prof/prof_emit.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_PROF_EMIT_TYPE_DEFINITIONS
#define BGL_PROF_EMIT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;


#endif													// BGL_PROF_EMIT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2profzd2tablezd2nameza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzprof_emitz00 = BUNSPEC;
	extern obj_t BGl_za2builtinzd2allocatorsza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzprof_emitz00(void);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzprof_emitz00(void);
	static obj_t BGl_objectzd2initzd2zzprof_emitz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzprof_emitz00(void);
	static obj_t BGl_z62zc3z04anonymousza31328ze3ze5zzprof_emitz00(obj_t, obj_t);
	static obj_t BGl_z62emitzd2globalz12za2zzprof_emitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzprof_emitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_unitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzprof_emitz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzprof_emitz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzprof_emitz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzprof_emitz00(void);
	static obj_t BGl_z62emitzd2profzd2infoz62zzprof_emitz00(obj_t, obj_t, obj_t);
	extern obj_t
		BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_emitzd2profzd2infoz00zzprof_emitz00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string1658z00zzprof_emitz00,
		BgL_bgl_string1658za700za7za7p1679za7, "/* prof association table */", 28);
	      DEFINE_STRING(BGl_string1659z00zzprof_emitz00,
		BgL_bgl_string1659za700za7za7p1680za7, "static obj_t write_bprof_table() {",
		34);
	      DEFINE_STRING(BGl_string1660z00zzprof_emitz00,
		BgL_bgl_string1660za700za7za7p1681za7, "   extern obj_t bprof_port;", 27);
	      DEFINE_STRING(BGl_string1661z00zzprof_emitz00,
		BgL_bgl_string1661za700za7za7p1682za7,
		"   if( bprof_port == BUNSPEC ) bprof_port = (obj_t)fopen( \"", 59);
	      DEFINE_STRING(BGl_string1662z00zzprof_emitz00,
		BgL_bgl_string1662za700za7za7p1683za7, "\", \"w\" );", 9);
	      DEFINE_STRING(BGl_string1663z00zzprof_emitz00,
		BgL_bgl_string1663za700za7za7p1684za7, "   if( bprof_port ) {", 21);
	      DEFINE_STRING(BGl_string1664z00zzprof_emitz00,
		BgL_bgl_string1664za700za7za7p1685za7, "      fputs( \"(\\\"", 17);
	      DEFINE_STRING(BGl_string1665z00zzprof_emitz00,
		BgL_bgl_string1665za700za7za7p1686za7, "\\\" \\\"", 5);
	      DEFINE_STRING(BGl_string1666z00zzprof_emitz00,
		BgL_bgl_string1666za700za7za7p1687za7, "\\\"", 2);
	      DEFINE_STRING(BGl_string1667z00zzprof_emitz00,
		BgL_bgl_string1667za700za7za7p1688za7, ")\\n\", (FILE *)bprof_port );", 27);
	      DEFINE_STRING(BGl_string1668z00zzprof_emitz00,
		BgL_bgl_string1668za700za7za7p1689za7, "   }", 4);
	      DEFINE_STRING(BGl_string1669z00zzprof_emitz00,
		BgL_bgl_string1669za700za7za7p1690za7, "   return BUNSPEC;", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_emitzd2profzd2infozd2envzd2zzprof_emitz00,
		BgL_bgl_za762emitza7d2profza7d1691za7,
		BGl_z62emitzd2profzd2infoz62zzprof_emitz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1670z00zzprof_emitz00,
		BgL_bgl_string1670za700za7za7p1692za7, "}\n", 2);
	      DEFINE_STRING(BGl_string1671z00zzprof_emitz00,
		BgL_bgl_string1671za700za7za7p1693za7, "      fputs( \"((\\\"", 18);
	      DEFINE_STRING(BGl_string1672z00zzprof_emitz00,
		BgL_bgl_string1672za700za7za7p1694za7, "\\\" ", 3);
	      DEFINE_STRING(BGl_string1673z00zzprof_emitz00,
		BgL_bgl_string1673za700za7za7p1695za7, ") \\\"", 4);
	      DEFINE_STRING(BGl_string1674z00zzprof_emitz00,
		BgL_bgl_string1674za700za7za7p1696za7, " allocator", 10);
	      DEFINE_STRING(BGl_string1675z00zzprof_emitz00,
		BgL_bgl_string1675za700za7za7p1697za7, "", 0);
	      DEFINE_STRING(BGl_string1676z00zzprof_emitz00,
		BgL_bgl_string1676za700za7za7p1698za7, "prof_emit", 9);
	      DEFINE_STRING(BGl_string1677z00zzprof_emitz00,
		BgL_bgl_string1677za700za7za7p1699za7, "foreign location allocator ", 27);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzprof_emitz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzprof_emitz00(long
		BgL_checksumz00_2147, char *BgL_fromz00_2148)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzprof_emitz00))
				{
					BGl_requirezd2initializa7ationz75zzprof_emitz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzprof_emitz00();
					BGl_libraryzd2moduleszd2initz00zzprof_emitz00();
					BGl_cnstzd2initzd2zzprof_emitz00();
					BGl_importedzd2moduleszd2initz00zzprof_emitz00();
					return BGl_toplevelzd2initzd2zzprof_emitz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzprof_emitz00(void)
	{
		{	/* Prof/prof_emit.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "prof_emit");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "prof_emit");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"prof_emit");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "prof_emit");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "prof_emit");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "prof_emit");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"prof_emit");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "prof_emit");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"prof_emit");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "prof_emit");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "prof_emit");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzprof_emitz00(void)
	{
		{	/* Prof/prof_emit.scm 15 */
			{	/* Prof/prof_emit.scm 15 */
				obj_t BgL_cportz00_2136;

				{	/* Prof/prof_emit.scm 15 */
					obj_t BgL_stringz00_2143;

					BgL_stringz00_2143 = BGl_string1677z00zzprof_emitz00;
					{	/* Prof/prof_emit.scm 15 */
						obj_t BgL_startz00_2144;

						BgL_startz00_2144 = BINT(0L);
						{	/* Prof/prof_emit.scm 15 */
							obj_t BgL_endz00_2145;

							BgL_endz00_2145 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2143)));
							{	/* Prof/prof_emit.scm 15 */

								BgL_cportz00_2136 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2143, BgL_startz00_2144, BgL_endz00_2145);
				}}}}
				{
					long BgL_iz00_2137;

					BgL_iz00_2137 = 2L;
				BgL_loopz00_2138:
					if ((BgL_iz00_2137 == -1L))
						{	/* Prof/prof_emit.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Prof/prof_emit.scm 15 */
							{	/* Prof/prof_emit.scm 15 */
								obj_t BgL_arg1678z00_2139;

								{	/* Prof/prof_emit.scm 15 */

									{	/* Prof/prof_emit.scm 15 */
										obj_t BgL_locationz00_2141;

										BgL_locationz00_2141 = BBOOL(((bool_t) 0));
										{	/* Prof/prof_emit.scm 15 */

											BgL_arg1678z00_2139 =
												BGl_readz00zz__readerz00(BgL_cportz00_2136,
												BgL_locationz00_2141);
										}
									}
								}
								{	/* Prof/prof_emit.scm 15 */
									int BgL_tmpz00_2177;

									BgL_tmpz00_2177 = (int) (BgL_iz00_2137);
									CNST_TABLE_SET(BgL_tmpz00_2177, BgL_arg1678z00_2139);
							}}
							{	/* Prof/prof_emit.scm 15 */
								int BgL_auxz00_2142;

								BgL_auxz00_2142 = (int) ((BgL_iz00_2137 - 1L));
								{
									long BgL_iz00_2182;

									BgL_iz00_2182 = (long) (BgL_auxz00_2142);
									BgL_iz00_2137 = BgL_iz00_2182;
									goto BgL_loopz00_2138;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzprof_emitz00(void)
	{
		{	/* Prof/prof_emit.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzprof_emitz00(void)
	{
		{	/* Prof/prof_emit.scm 15 */
			return BUNSPEC;
		}

	}



/* emit-prof-info */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2profzd2infoz00zzprof_emitz00(obj_t
		BgL_globalsz00_39, obj_t BgL_portz00_40)
	{
		{	/* Prof/prof_emit.scm 40 */
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1658z00zzprof_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1659z00zzprof_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1660z00zzprof_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1661z00zzprof_emitz00, BgL_portz00_40);
			bgl_display_obj(BGl_za2profzd2tablezd2nameza2z00zzengine_paramz00,
				BgL_portz00_40);
			bgl_display_string(BGl_string1662z00zzprof_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1663z00zzprof_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			{	/* Prof/prof_emit.scm 80 */
				obj_t BgL_zc3z04anonymousza31328ze3z87_2126;

				BgL_zc3z04anonymousza31328ze3z87_2126 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31328ze3ze5zzprof_emitz00,
					(int) (1L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_2126,
					(int) (0L), BgL_portz00_40);
				BGl_forzd2eachzd2globalz12z12zzast_envz00
					(BgL_zc3z04anonymousza31328ze3z87_2126, BNIL);
			}
			{
				obj_t BgL_l1250z00_1710;

				BgL_l1250z00_1710 = BgL_globalsz00_39;
			BgL_zc3z04anonymousza31344ze3z87_1711:
				if (PAIRP(BgL_l1250z00_1710))
					{	/* Prof/prof_emit.scm 86 */
						{	/* Prof/prof_emit.scm 87 */
							obj_t BgL_globalz00_1713;

							BgL_globalz00_1713 = CAR(BgL_l1250z00_1710);
							if (
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1713))))->
									BgL_userzf3zf3))
								{	/* Prof/prof_emit.scm 87 */
									BGl_z62emitzd2globalz12za2zzprof_emitz00(BgL_portz00_40,
										BgL_globalz00_1713);
								}
							else
								{	/* Prof/prof_emit.scm 87 */
									BFALSE;
								}
						}
						{
							obj_t BgL_l1250z00_2213;

							BgL_l1250z00_2213 = CDR(BgL_l1250z00_1710);
							BgL_l1250z00_1710 = BgL_l1250z00_2213;
							goto BgL_zc3z04anonymousza31344ze3z87_1711;
						}
					}
				else
					{	/* Prof/prof_emit.scm 86 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1253z00_1718;

				BgL_l1253z00_1718 = BGl_za2builtinzd2allocatorsza2zd2zzengine_paramz00;
			BgL_zc3z04anonymousza31349ze3z87_1719:
				if (PAIRP(BgL_l1253z00_1718))
					{	/* Prof/prof_emit.scm 92 */
						{	/* Prof/prof_emit.scm 93 */
							obj_t BgL_scmzd2czd2_1721;

							BgL_scmzd2czd2_1721 = CAR(BgL_l1253z00_1718);
							{	/* Prof/prof_emit.scm 93 */
								obj_t BgL_scmz00_1722;
								obj_t BgL_cz00_1723;

								BgL_scmz00_1722 = CAR(((obj_t) BgL_scmzd2czd2_1721));
								BgL_cz00_1723 = CDR(((obj_t) BgL_scmzd2czd2_1721));
								bgl_display_string(BGl_string1664z00zzprof_emitz00,
									BgL_portz00_40);
								bgl_display_obj(BgL_scmz00_1722, BgL_portz00_40);
								bgl_display_string(BGl_string1665z00zzprof_emitz00,
									BgL_portz00_40);
								bgl_display_obj(BgL_cz00_1723, BgL_portz00_40);
								bgl_display_string(BGl_string1666z00zzprof_emitz00,
									BgL_portz00_40);
								bgl_display_string(BGl_string1667z00zzprof_emitz00,
									BgL_portz00_40);
								bgl_display_char(((unsigned char) 10), BgL_portz00_40);
						}}
						{
							obj_t BgL_l1253z00_2229;

							BgL_l1253z00_2229 = CDR(BgL_l1253z00_1718);
							BgL_l1253z00_1718 = BgL_l1253z00_2229;
							goto BgL_zc3z04anonymousza31349ze3z87_1719;
						}
					}
				else
					{	/* Prof/prof_emit.scm 92 */
						((bool_t) 1);
					}
			}
			bgl_display_string(BGl_string1668z00zzprof_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1669z00zzprof_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1670z00zzprof_emitz00, BgL_portz00_40);
			return bgl_display_char(((unsigned char) 10), BgL_portz00_40);
		}

	}



/* &emit-prof-info */
	obj_t BGl_z62emitzd2profzd2infoz62zzprof_emitz00(obj_t BgL_envz00_2127,
		obj_t BgL_globalsz00_2128, obj_t BgL_portz00_2129)
	{
		{	/* Prof/prof_emit.scm 40 */
			return
				BGl_emitzd2profzd2infoz00zzprof_emitz00(BgL_globalsz00_2128,
				BgL_portz00_2129);
		}

	}



/* &emit-global! */
	obj_t BGl_z62emitzd2globalz12za2zzprof_emitz00(obj_t BgL_portz00_2130,
		obj_t BgL_globalz00_1730)
	{
		{	/* Prof/prof_emit.scm 66 */
			{	/* Prof/prof_emit.scm 43 */
				BgL_valuez00_bglt BgL_sfunz00_1732;

				BgL_sfunz00_1732 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_1730))))->BgL_valuez00);
				BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
					((BgL_variablez00_bglt) BgL_globalz00_1730));
				{	/* Prof/prof_emit.scm 45 */
					obj_t BgL_idz00_1733;
					obj_t BgL_czd2namezd2_1734;
					obj_t BgL_iszd2alloczd2_1735;
					obj_t BgL_locz00_1736;

					BgL_idz00_1733 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_globalz00_1730))))->BgL_idz00);
					BgL_czd2namezd2_1734 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_globalz00_1730))))->BgL_namez00);
					{	/* Prof/prof_emit.scm 47 */
						bool_t BgL_test1705z00_2249;

						{	/* Prof/prof_emit.scm 47 */
							obj_t BgL_classz00_1942;

							BgL_classz00_1942 = BGl_sfunz00zzast_varz00;
							{	/* Prof/prof_emit.scm 47 */
								BgL_objectz00_bglt BgL_arg1807z00_1944;

								{	/* Prof/prof_emit.scm 47 */
									obj_t BgL_tmpz00_2250;

									BgL_tmpz00_2250 =
										((obj_t) ((BgL_objectz00_bglt) BgL_sfunz00_1732));
									BgL_arg1807z00_1944 = (BgL_objectz00_bglt) (BgL_tmpz00_2250);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Prof/prof_emit.scm 47 */
										long BgL_idxz00_1950;

										BgL_idxz00_1950 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1944);
										BgL_test1705z00_2249 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1950 + 3L)) == BgL_classz00_1942);
									}
								else
									{	/* Prof/prof_emit.scm 47 */
										bool_t BgL_res1654z00_1975;

										{	/* Prof/prof_emit.scm 47 */
											obj_t BgL_oclassz00_1958;

											{	/* Prof/prof_emit.scm 47 */
												obj_t BgL_arg1815z00_1966;
												long BgL_arg1816z00_1967;

												BgL_arg1815z00_1966 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Prof/prof_emit.scm 47 */
													long BgL_arg1817z00_1968;

													BgL_arg1817z00_1968 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1944);
													BgL_arg1816z00_1967 =
														(BgL_arg1817z00_1968 - OBJECT_TYPE);
												}
												BgL_oclassz00_1958 =
													VECTOR_REF(BgL_arg1815z00_1966, BgL_arg1816z00_1967);
											}
											{	/* Prof/prof_emit.scm 47 */
												bool_t BgL__ortest_1115z00_1959;

												BgL__ortest_1115z00_1959 =
													(BgL_classz00_1942 == BgL_oclassz00_1958);
												if (BgL__ortest_1115z00_1959)
													{	/* Prof/prof_emit.scm 47 */
														BgL_res1654z00_1975 = BgL__ortest_1115z00_1959;
													}
												else
													{	/* Prof/prof_emit.scm 47 */
														long BgL_odepthz00_1960;

														{	/* Prof/prof_emit.scm 47 */
															obj_t BgL_arg1804z00_1961;

															BgL_arg1804z00_1961 = (BgL_oclassz00_1958);
															BgL_odepthz00_1960 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1961);
														}
														if ((3L < BgL_odepthz00_1960))
															{	/* Prof/prof_emit.scm 47 */
																obj_t BgL_arg1802z00_1963;

																{	/* Prof/prof_emit.scm 47 */
																	obj_t BgL_arg1803z00_1964;

																	BgL_arg1803z00_1964 = (BgL_oclassz00_1958);
																	BgL_arg1802z00_1963 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1964,
																		3L);
																}
																BgL_res1654z00_1975 =
																	(BgL_arg1802z00_1963 == BgL_classz00_1942);
															}
														else
															{	/* Prof/prof_emit.scm 47 */
																BgL_res1654z00_1975 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1705z00_2249 = BgL_res1654z00_1975;
									}
							}
						}
						if (BgL_test1705z00_2249)
							{	/* Prof/prof_emit.scm 47 */
								BgL_iszd2alloczd2_1735 =
									BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(0),
									(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_sfunz00_1732)))->
										BgL_propertyz00));
							}
						else
							{	/* Prof/prof_emit.scm 47 */
								BgL_iszd2alloczd2_1735 = BFALSE;
							}
					}
					{	/* Prof/prof_emit.scm 48 */
						bool_t BgL_test1712z00_2277;

						{	/* Prof/prof_emit.scm 48 */
							obj_t BgL_classz00_1977;

							BgL_classz00_1977 = BGl_sfunz00zzast_varz00;
							{	/* Prof/prof_emit.scm 48 */
								BgL_objectz00_bglt BgL_arg1807z00_1979;

								{	/* Prof/prof_emit.scm 48 */
									obj_t BgL_tmpz00_2278;

									BgL_tmpz00_2278 =
										((obj_t) ((BgL_objectz00_bglt) BgL_sfunz00_1732));
									BgL_arg1807z00_1979 = (BgL_objectz00_bglt) (BgL_tmpz00_2278);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Prof/prof_emit.scm 48 */
										long BgL_idxz00_1985;

										BgL_idxz00_1985 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1979);
										BgL_test1712z00_2277 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1985 + 3L)) == BgL_classz00_1977);
									}
								else
									{	/* Prof/prof_emit.scm 48 */
										bool_t BgL_res1655z00_2010;

										{	/* Prof/prof_emit.scm 48 */
											obj_t BgL_oclassz00_1993;

											{	/* Prof/prof_emit.scm 48 */
												obj_t BgL_arg1815z00_2001;
												long BgL_arg1816z00_2002;

												BgL_arg1815z00_2001 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Prof/prof_emit.scm 48 */
													long BgL_arg1817z00_2003;

													BgL_arg1817z00_2003 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1979);
													BgL_arg1816z00_2002 =
														(BgL_arg1817z00_2003 - OBJECT_TYPE);
												}
												BgL_oclassz00_1993 =
													VECTOR_REF(BgL_arg1815z00_2001, BgL_arg1816z00_2002);
											}
											{	/* Prof/prof_emit.scm 48 */
												bool_t BgL__ortest_1115z00_1994;

												BgL__ortest_1115z00_1994 =
													(BgL_classz00_1977 == BgL_oclassz00_1993);
												if (BgL__ortest_1115z00_1994)
													{	/* Prof/prof_emit.scm 48 */
														BgL_res1655z00_2010 = BgL__ortest_1115z00_1994;
													}
												else
													{	/* Prof/prof_emit.scm 48 */
														long BgL_odepthz00_1995;

														{	/* Prof/prof_emit.scm 48 */
															obj_t BgL_arg1804z00_1996;

															BgL_arg1804z00_1996 = (BgL_oclassz00_1993);
															BgL_odepthz00_1995 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1996);
														}
														if ((3L < BgL_odepthz00_1995))
															{	/* Prof/prof_emit.scm 48 */
																obj_t BgL_arg1802z00_1998;

																{	/* Prof/prof_emit.scm 48 */
																	obj_t BgL_arg1803z00_1999;

																	BgL_arg1803z00_1999 = (BgL_oclassz00_1993);
																	BgL_arg1802z00_1998 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1999,
																		3L);
																}
																BgL_res1655z00_2010 =
																	(BgL_arg1802z00_1998 == BgL_classz00_1977);
															}
														else
															{	/* Prof/prof_emit.scm 48 */
																BgL_res1655z00_2010 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1712z00_2277 = BgL_res1655z00_2010;
									}
							}
						}
						if (BgL_test1712z00_2277)
							{	/* Prof/prof_emit.scm 48 */
								BgL_locz00_1736 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_sfunz00_1732)))->BgL_locz00);
							}
						else
							{	/* Prof/prof_emit.scm 48 */
								BgL_locz00_1736 = BFALSE;
							}
					}
					{	/* Prof/prof_emit.scm 49 */
						bool_t BgL_test1718z00_2303;

						if (STRUCTP(BgL_locz00_1736))
							{	/* Prof/prof_emit.scm 49 */
								BgL_test1718z00_2303 =
									(STRUCT_KEY(BgL_locz00_1736) == CNST_TABLE_REF(1));
							}
						else
							{	/* Prof/prof_emit.scm 49 */
								BgL_test1718z00_2303 = ((bool_t) 0);
							}
						if (BgL_test1718z00_2303)
							{	/* Prof/prof_emit.scm 49 */
								bgl_display_string(BGl_string1671z00zzprof_emitz00,
									BgL_portz00_2130);
								bgl_display_obj(BgL_idz00_1733, BgL_portz00_2130);
								bgl_display_string(BGl_string1672z00zzprof_emitz00,
									BgL_portz00_2130);
								bgl_display_string(BGl_string1666z00zzprof_emitz00,
									BgL_portz00_2130);
								bgl_display_obj(STRUCT_REF(BgL_locz00_1736, (int) (0L)),
									BgL_portz00_2130);
								bgl_display_string(BGl_string1672z00zzprof_emitz00,
									BgL_portz00_2130);
								bgl_display_obj(STRUCT_REF(BgL_locz00_1736, (int) (1L)),
									BgL_portz00_2130);
								bgl_display_string(BGl_string1673z00zzprof_emitz00,
									BgL_portz00_2130);
								bgl_display_obj(BgL_czd2namezd2_1734, BgL_portz00_2130);
								bgl_display_string(BGl_string1666z00zzprof_emitz00,
									BgL_portz00_2130);
								{	/* Prof/prof_emit.scm 58 */
									obj_t BgL_arg1367z00_1741;

									if (CBOOL(BgL_iszd2alloczd2_1735))
										{	/* Prof/prof_emit.scm 58 */
											BgL_arg1367z00_1741 = BGl_string1674z00zzprof_emitz00;
										}
									else
										{	/* Prof/prof_emit.scm 58 */
											BgL_arg1367z00_1741 = BGl_string1675z00zzprof_emitz00;
										}
									bgl_display_obj(BgL_arg1367z00_1741, BgL_portz00_2130);
								}
								bgl_display_string(BGl_string1667z00zzprof_emitz00,
									BgL_portz00_2130);
								return bgl_display_char(((unsigned char) 10), BgL_portz00_2130);
							}
						else
							{	/* Prof/prof_emit.scm 49 */
								bgl_display_string(BGl_string1664z00zzprof_emitz00,
									BgL_portz00_2130);
								bgl_display_obj(BgL_idz00_1733, BgL_portz00_2130);
								bgl_display_string(BGl_string1665z00zzprof_emitz00,
									BgL_portz00_2130);
								bgl_display_obj(BgL_czd2namezd2_1734, BgL_portz00_2130);
								bgl_display_string(BGl_string1666z00zzprof_emitz00,
									BgL_portz00_2130);
								{	/* Prof/prof_emit.scm 65 */
									obj_t BgL_arg1370z00_1743;

									if (CBOOL(BgL_iszd2alloczd2_1735))
										{	/* Prof/prof_emit.scm 65 */
											BgL_arg1370z00_1743 = BGl_string1674z00zzprof_emitz00;
										}
									else
										{	/* Prof/prof_emit.scm 65 */
											BgL_arg1370z00_1743 = BGl_string1675z00zzprof_emitz00;
										}
									bgl_display_obj(BgL_arg1370z00_1743, BgL_portz00_2130);
								}
								bgl_display_string(BGl_string1667z00zzprof_emitz00,
									BgL_portz00_2130);
								return bgl_display_char(((unsigned char) 10), BgL_portz00_2130);
		}}}}}

	}



/* &<@anonymous:1328> */
	obj_t BGl_z62zc3z04anonymousza31328ze3ze5zzprof_emitz00(obj_t BgL_envz00_2131,
		obj_t BgL_gz00_2133)
	{
		{	/* Prof/prof_emit.scm 79 */
			{	/* Prof/prof_emit.scm 80 */
				obj_t BgL_portz00_2132;

				BgL_portz00_2132 = ((obj_t) PROCEDURE_REF(BgL_envz00_2131, (int) (0L)));
				{	/* Prof/prof_emit.scm 80 */
					bool_t BgL_test1723z00_2341;

					{	/* Prof/prof_emit.scm 80 */
						bool_t BgL_test1724z00_2342;

						if (
							((((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_gz00_2133))))->
									BgL_occurrencez00) > 0L))
							{	/* Prof/prof_emit.scm 80 */
								BgL_test1724z00_2342 = ((bool_t) 1);
							}
						else
							{	/* Prof/prof_emit.scm 80 */
								BgL_test1724z00_2342 =
									(
									(((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt) BgL_gz00_2133)))->
										BgL_modulez00) == CNST_TABLE_REF(2));
							}
						if (BgL_test1724z00_2342)
							{	/* Prof/prof_emit.scm 80 */
								if (CBOOL(
										(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_gz00_2133)))->
											BgL_libraryz00)))
									{	/* Prof/prof_emit.scm 82 */
										BgL_test1723z00_2341 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_gz00_2133))))->
											BgL_userzf3zf3);
									}
								else
									{	/* Prof/prof_emit.scm 82 */
										BgL_test1723z00_2341 = ((bool_t) 0);
									}
							}
						else
							{	/* Prof/prof_emit.scm 80 */
								BgL_test1723z00_2341 = ((bool_t) 0);
							}
					}
					if (BgL_test1723z00_2341)
						{	/* Prof/prof_emit.scm 80 */
							return
								BGl_z62emitzd2globalz12za2zzprof_emitz00(BgL_portz00_2132,
								BgL_gz00_2133);
						}
					else
						{	/* Prof/prof_emit.scm 80 */
							return BFALSE;
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzprof_emitz00(void)
	{
		{	/* Prof/prof_emit.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzprof_emitz00(void)
	{
		{	/* Prof/prof_emit.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzprof_emitz00(void)
	{
		{	/* Prof/prof_emit.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzprof_emitz00(void)
	{
		{	/* Prof/prof_emit.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzast_unitz00(234044111L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(364917963L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string1676z00zzprof_emitz00));
		}

	}

#ifdef __cplusplus
}
#endif
