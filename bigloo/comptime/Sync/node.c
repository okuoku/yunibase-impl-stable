/*===========================================================================*/
/*   (Sync/node.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Sync/node.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SYNC_NODE_TYPE_DEFINITIONS
#define BGL_SYNC_NODE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_SYNC_NODE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_mpopz00zzsync_nodez00 = BUNSPEC;
	extern obj_t BGl_failsafezd2synczf3z21zzsync_failsafez00(BgL_syncz00_bglt);
	extern obj_t BGl_za2synczd2profilingza2zd2zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzsync_nodez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern BgL_nodez00_bglt BGl_applicationzd2ze3nodez31zzast_appz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzsync_nodez00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_failsafezd2mutexzd2profilez00zzsync_nodez00 = BUNSPEC;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzsync_nodez00(void);
	static obj_t BGl_objectzd2initzd2zzsync_nodez00(void);
	static obj_t BGl_initzd2syncz12zc0zzsync_nodez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	static obj_t BGl_getexitdtopz00zzsync_nodez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_mlockz00zzsync_nodez00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zzsync_nodez00(void);
	static obj_t BGl_exitdzd2mutexzd2profilez00zzsync_nodez00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62synczd2ze3sequencez53zzsync_nodez00(obj_t,
		obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_mlockprelockz00zzsync_nodez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsync_nodez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsync_failsafez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_mpushz00zzsync_nodez00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_synczd2ze3sequencez31zzsync_nodez00(BgL_syncz00_bglt);
	static obj_t BGl_mulockz00zzsync_nodez00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzsync_nodez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsync_nodez00(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzsync_nodez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsync_nodez00(void);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_occurzd2nodez12zc0zzast_occurz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t
		BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_variablez00_bglt);
	static obj_t __cnst[16];


	   
		 
		DEFINE_STRING(BGl_string1836z00zzsync_nodez00,
		BgL_bgl_string1836za700za7za7s1839za7, "sync_node", 9);
	      DEFINE_STRING(BGl_string1837z00zzsync_nodez00,
		BgL_bgl_string1837za700za7za7s1840za7,
		"top tmp value (@ exitd-pop-protect! __bexit) (@ exitd-push-protect! __bexit) $exitd-pop-protect! $exitd-push-protect! $failsafe-mutex-profile $exitd-mutex-profile c-saw c $mutex-unlock $mutex-lock-prelock $mutex-lock app $get-exitd-top ",
		236);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_synczd2ze3sequencezd2envze3zzsync_nodez00,
		BgL_bgl_za762syncza7d2za7e3seq1841za7,
		BGl_z62synczd2ze3sequencez53zzsync_nodez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_mpopz00zzsync_nodez00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsync_nodez00));
		     ADD_ROOT((void *) (&BGl_failsafezd2mutexzd2profilez00zzsync_nodez00));
		     ADD_ROOT((void *) (&BGl_getexitdtopz00zzsync_nodez00));
		     ADD_ROOT((void *) (&BGl_mlockz00zzsync_nodez00));
		     ADD_ROOT((void *) (&BGl_exitdzd2mutexzd2profilez00zzsync_nodez00));
		     ADD_ROOT((void *) (&BGl_mlockprelockz00zzsync_nodez00));
		     ADD_ROOT((void *) (&BGl_mpushz00zzsync_nodez00));
		     ADD_ROOT((void *) (&BGl_mulockz00zzsync_nodez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsync_nodez00(long
		BgL_checksumz00_2258, char *BgL_fromz00_2259)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsync_nodez00))
				{
					BGl_requirezd2initializa7ationz75zzsync_nodez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsync_nodez00();
					BGl_libraryzd2moduleszd2initz00zzsync_nodez00();
					BGl_cnstzd2initzd2zzsync_nodez00();
					BGl_importedzd2moduleszd2initz00zzsync_nodez00();
					return BGl_toplevelzd2initzd2zzsync_nodez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsync_nodez00(void)
	{
		{	/* Sync/node.scm 18 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "sync_node");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "sync_node");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "sync_node");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "sync_node");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "sync_node");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"sync_node");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "sync_node");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"sync_node");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"sync_node");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "sync_node");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsync_nodez00(void)
	{
		{	/* Sync/node.scm 18 */
			{	/* Sync/node.scm 18 */
				obj_t BgL_cportz00_2247;

				{	/* Sync/node.scm 18 */
					obj_t BgL_stringz00_2254;

					BgL_stringz00_2254 = BGl_string1837z00zzsync_nodez00;
					{	/* Sync/node.scm 18 */
						obj_t BgL_startz00_2255;

						BgL_startz00_2255 = BINT(0L);
						{	/* Sync/node.scm 18 */
							obj_t BgL_endz00_2256;

							BgL_endz00_2256 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2254)));
							{	/* Sync/node.scm 18 */

								BgL_cportz00_2247 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2254, BgL_startz00_2255, BgL_endz00_2256);
				}}}}
				{
					long BgL_iz00_2248;

					BgL_iz00_2248 = 15L;
				BgL_loopz00_2249:
					if ((BgL_iz00_2248 == -1L))
						{	/* Sync/node.scm 18 */
							return BUNSPEC;
						}
					else
						{	/* Sync/node.scm 18 */
							{	/* Sync/node.scm 18 */
								obj_t BgL_arg1838z00_2250;

								{	/* Sync/node.scm 18 */

									{	/* Sync/node.scm 18 */
										obj_t BgL_locationz00_2252;

										BgL_locationz00_2252 = BBOOL(((bool_t) 0));
										{	/* Sync/node.scm 18 */

											BgL_arg1838z00_2250 =
												BGl_readz00zz__readerz00(BgL_cportz00_2247,
												BgL_locationz00_2252);
										}
									}
								}
								{	/* Sync/node.scm 18 */
									int BgL_tmpz00_2287;

									BgL_tmpz00_2287 = (int) (BgL_iz00_2248);
									CNST_TABLE_SET(BgL_tmpz00_2287, BgL_arg1838z00_2250);
							}}
							{	/* Sync/node.scm 18 */
								int BgL_auxz00_2253;

								BgL_auxz00_2253 = (int) ((BgL_iz00_2248 - 1L));
								{
									long BgL_iz00_2292;

									BgL_iz00_2292 = (long) (BgL_auxz00_2253);
									BgL_iz00_2248 = BgL_iz00_2292;
									goto BgL_loopz00_2249;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsync_nodez00(void)
	{
		{	/* Sync/node.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsync_nodez00(void)
	{
		{	/* Sync/node.scm 18 */
			BGl_mlockz00zzsync_nodez00 = BFALSE;
			BGl_mlockprelockz00zzsync_nodez00 = BFALSE;
			BGl_mulockz00zzsync_nodez00 = BFALSE;
			BGl_mpushz00zzsync_nodez00 = BFALSE;
			BGl_mpopz00zzsync_nodez00 = BFALSE;
			BGl_getexitdtopz00zzsync_nodez00 = BFALSE;
			BGl_exitdzd2mutexzd2profilez00zzsync_nodez00 = BFALSE;
			return (BGl_failsafezd2mutexzd2profilez00zzsync_nodez00 =
				BFALSE, BUNSPEC);
		}

	}



/* init-sync! */
	obj_t BGl_initzd2syncz12zc0zzsync_nodez00(obj_t BgL_locz00_17)
	{
		{	/* Sync/node.scm 62 */
			if (CBOOL(BGl_mlockz00zzsync_nodez00))
				{	/* Sync/node.scm 63 */
					return BFALSE;
				}
			else
				{	/* Sync/node.scm 63 */
					BGl_getexitdtopz00zzsync_nodez00 =
						((obj_t)
						BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(0), BNIL,
							BgL_locz00_17, CNST_TABLE_REF(1)));
					BGl_mlockz00zzsync_nodez00 =
						((obj_t) BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(2), BNIL,
							BgL_locz00_17, CNST_TABLE_REF(1)));
					BGl_mlockprelockz00zzsync_nodez00 =
						((obj_t) BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(3), BNIL,
							BgL_locz00_17, CNST_TABLE_REF(1)));
					BGl_mulockz00zzsync_nodez00 =
						((obj_t) BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(4), BNIL,
							BgL_locz00_17, CNST_TABLE_REF(1)));
					{	/* Sync/node.scm 68 */
						obj_t BgL_casezd2valuezd2_1735;

						{	/* Sync/node.scm 68 */
							obj_t BgL_arg1318z00_1741;

							BgL_arg1318z00_1741 = BGl_thezd2backendzd2zzbackend_backendz00();
							BgL_casezd2valuezd2_1735 =
								(((BgL_backendz00_bglt) COBJECT(
										((BgL_backendz00_bglt) BgL_arg1318z00_1741)))->
								BgL_languagez00);
						}
						{	/* Sync/node.scm 68 */
							bool_t BgL_test1845z00_2316;

							{	/* Sync/node.scm 68 */
								bool_t BgL__ortest_1111z00_1740;

								BgL__ortest_1111z00_1740 =
									(BgL_casezd2valuezd2_1735 == CNST_TABLE_REF(5));
								if (BgL__ortest_1111z00_1740)
									{	/* Sync/node.scm 68 */
										BgL_test1845z00_2316 = BgL__ortest_1111z00_1740;
									}
								else
									{	/* Sync/node.scm 68 */
										BgL_test1845z00_2316 =
											(BgL_casezd2valuezd2_1735 == CNST_TABLE_REF(6));
									}
							}
							if (BgL_test1845z00_2316)
								{	/* Sync/node.scm 68 */
									if (CBOOL(BGl_za2synczd2profilingza2zd2zzengine_paramz00))
										{	/* Sync/node.scm 70 */
											BGl_exitdzd2mutexzd2profilez00zzsync_nodez00 =
												((obj_t)
												BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(7),
													BNIL, BgL_locz00_17, CNST_TABLE_REF(1)));
											BGl_failsafezd2mutexzd2profilez00zzsync_nodez00 =
												((obj_t)
												BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(8),
													BNIL, BgL_locz00_17, CNST_TABLE_REF(1)));
											{	/* Sync/node.scm 75 */
												BgL_variablez00_bglt BgL_arg1316z00_1738;

												{	/* Sync/node.scm 75 */
													BgL_varz00_bglt BgL_oz00_2083;

													BgL_oz00_2083 =
														((BgL_varz00_bglt)
														BGl_exitdzd2mutexzd2profilez00zzsync_nodez00);
													BgL_arg1316z00_1738 =
														(((BgL_varz00_bglt) COBJECT(BgL_oz00_2083))->
														BgL_variablez00);
												}
												BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
													(BgL_arg1316z00_1738);
											}
											{	/* Sync/node.scm 76 */
												BgL_variablez00_bglt BgL_arg1317z00_1739;

												{	/* Sync/node.scm 76 */
													BgL_varz00_bglt BgL_oz00_2084;

													BgL_oz00_2084 =
														((BgL_varz00_bglt)
														BGl_failsafezd2mutexzd2profilez00zzsync_nodez00);
													BgL_arg1317z00_1739 =
														(((BgL_varz00_bglt) COBJECT(BgL_oz00_2084))->
														BgL_variablez00);
												}
												BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
													(BgL_arg1317z00_1739);
											}
										}
									else
										{	/* Sync/node.scm 70 */
											BFALSE;
										}
									BGl_mpushz00zzsync_nodez00 =
										((obj_t)
										BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(9), BNIL,
											BgL_locz00_17, CNST_TABLE_REF(1)));
									BGl_mpopz00zzsync_nodez00 =
										((obj_t)
										BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(10), BNIL,
											BgL_locz00_17, CNST_TABLE_REF(1)));
								}
							else
								{	/* Sync/node.scm 68 */
									BGl_mpushz00zzsync_nodez00 =
										((obj_t)
										BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(11), BNIL,
											BgL_locz00_17, CNST_TABLE_REF(1)));
									BGl_mpopz00zzsync_nodez00 =
										((obj_t)
										BGl_sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(12), BNIL,
											BgL_locz00_17, CNST_TABLE_REF(1)));
								}
						}
					}
					{	/* Sync/node.scm 82 */
						BgL_variablez00_bglt BgL_arg1319z00_1742;

						{	/* Sync/node.scm 82 */
							BgL_varz00_bglt BgL_oz00_2085;

							BgL_oz00_2085 =
								((BgL_varz00_bglt) BGl_getexitdtopz00zzsync_nodez00);
							BgL_arg1319z00_1742 =
								(((BgL_varz00_bglt) COBJECT(BgL_oz00_2085))->BgL_variablez00);
						}
						BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
							(BgL_arg1319z00_1742);
					}
					{	/* Sync/node.scm 83 */
						BgL_variablez00_bglt BgL_arg1320z00_1743;

						{	/* Sync/node.scm 83 */
							BgL_varz00_bglt BgL_oz00_2086;

							BgL_oz00_2086 = ((BgL_varz00_bglt) BGl_mlockz00zzsync_nodez00);
							BgL_arg1320z00_1743 =
								(((BgL_varz00_bglt) COBJECT(BgL_oz00_2086))->BgL_variablez00);
						}
						BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
							(BgL_arg1320z00_1743);
					}
					{	/* Sync/node.scm 84 */
						BgL_variablez00_bglt BgL_arg1321z00_1744;

						{	/* Sync/node.scm 84 */
							BgL_varz00_bglt BgL_oz00_2087;

							BgL_oz00_2087 =
								((BgL_varz00_bglt) BGl_mlockprelockz00zzsync_nodez00);
							BgL_arg1321z00_1744 =
								(((BgL_varz00_bglt) COBJECT(BgL_oz00_2087))->BgL_variablez00);
						}
						BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
							(BgL_arg1321z00_1744);
					}
					{	/* Sync/node.scm 85 */
						BgL_variablez00_bglt BgL_arg1322z00_1745;

						{	/* Sync/node.scm 85 */
							BgL_varz00_bglt BgL_oz00_2088;

							BgL_oz00_2088 = ((BgL_varz00_bglt) BGl_mulockz00zzsync_nodez00);
							BgL_arg1322z00_1745 =
								(((BgL_varz00_bglt) COBJECT(BgL_oz00_2088))->BgL_variablez00);
						}
						BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
							(BgL_arg1322z00_1745);
					}
					{	/* Sync/node.scm 86 */
						BgL_variablez00_bglt BgL_arg1323z00_1746;

						{	/* Sync/node.scm 86 */
							BgL_varz00_bglt BgL_oz00_2089;

							BgL_oz00_2089 = ((BgL_varz00_bglt) BGl_mpushz00zzsync_nodez00);
							BgL_arg1323z00_1746 =
								(((BgL_varz00_bglt) COBJECT(BgL_oz00_2089))->BgL_variablez00);
						}
						BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
							(BgL_arg1323z00_1746);
					}
					{	/* Sync/node.scm 87 */
						BgL_variablez00_bglt BgL_arg1325z00_1747;

						{	/* Sync/node.scm 87 */
							BgL_varz00_bglt BgL_oz00_2090;

							BgL_oz00_2090 = ((BgL_varz00_bglt) BGl_mpopz00zzsync_nodez00);
							BgL_arg1325z00_1747 =
								(((BgL_varz00_bglt) COBJECT(BgL_oz00_2090))->BgL_variablez00);
						}
						return
							BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
							(BgL_arg1325z00_1747);
					}
				}
		}

	}



/* sync->sequence */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_synczd2ze3sequencez31zzsync_nodez00(BgL_syncz00_bglt BgL_nodez00_18)
	{
		{	/* Sync/node.scm 112 */
			{
				BgL_syncz00_bglt BgL_nodez00_1824;
				BgL_syncz00_bglt BgL_nodez00_1778;
				BgL_letzd2varzd2_bglt BgL_nodez00_1769;
				obj_t BgL_locz00_1770;
				BgL_letzd2varzd2_bglt BgL_nodez00_1760;
				obj_t BgL_locz00_1761;

				{	/* Sync/node.scm 192 */
					obj_t BgL_arg1326z00_1754;

					BgL_arg1326z00_1754 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_nodez00_18)))->BgL_locz00);
					BGl_initzd2syncz12zc0zzsync_nodez00(BgL_arg1326z00_1754);
				}
				{	/* Sync/node.scm 194 */
					BgL_nodezf2effectzf2_bglt BgL_enodez00_1755;

					if (CBOOL(BGl_failsafezd2synczf3z21zzsync_failsafez00
							(BgL_nodez00_18)))
						{
							BgL_sequencez00_bglt BgL_auxz00_2378;

							BgL_nodez00_1778 = BgL_nodez00_18;
							{	/* Sync/node.scm 131 */
								BgL_localz00_bglt BgL_tmpz00_1781;

								{	/* Sync/node.scm 131 */
									obj_t BgL_arg1485z00_1822;
									BgL_typez00_bglt BgL_arg1489z00_1823;

									BgL_arg1485z00_1822 =
										BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(14));
									BgL_arg1489z00_1823 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_nodez00_1778)))->BgL_typez00);
									BgL_tmpz00_1781 =
										BGl_makezd2localzd2svarz00zzast_localz00
										(BgL_arg1485z00_1822, BgL_arg1489z00_1823);
								}
								{	/* Sync/node.scm 131 */
									BgL_nodez00_bglt BgL_lockz00_1782;

									{	/* Sync/node.scm 132 */
										bool_t BgL_test1849z00_2384;

										{	/* Sync/node.scm 132 */
											BgL_nodez00_bglt BgL_arg1473z00_1821;

											BgL_arg1473z00_1821 =
												(((BgL_syncz00_bglt) COBJECT(BgL_nodez00_1778))->
												BgL_prelockz00);
											{	/* Sync/node.scm 132 */
												obj_t BgL_classz00_2095;

												BgL_classz00_2095 = BGl_atomz00zzast_nodez00;
												{	/* Sync/node.scm 132 */
													BgL_objectz00_bglt BgL_arg1807z00_2097;

													{	/* Sync/node.scm 132 */
														obj_t BgL_tmpz00_2386;

														BgL_tmpz00_2386 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_arg1473z00_1821));
														BgL_arg1807z00_2097 =
															(BgL_objectz00_bglt) (BgL_tmpz00_2386);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Sync/node.scm 132 */
															long BgL_idxz00_2103;

															BgL_idxz00_2103 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2097);
															BgL_test1849z00_2384 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2103 + 2L)) == BgL_classz00_2095);
														}
													else
														{	/* Sync/node.scm 132 */
															bool_t BgL_res1828z00_2128;

															{	/* Sync/node.scm 132 */
																obj_t BgL_oclassz00_2111;

																{	/* Sync/node.scm 132 */
																	obj_t BgL_arg1815z00_2119;
																	long BgL_arg1816z00_2120;

																	BgL_arg1815z00_2119 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Sync/node.scm 132 */
																		long BgL_arg1817z00_2121;

																		BgL_arg1817z00_2121 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2097);
																		BgL_arg1816z00_2120 =
																			(BgL_arg1817z00_2121 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2111 =
																		VECTOR_REF(BgL_arg1815z00_2119,
																		BgL_arg1816z00_2120);
																}
																{	/* Sync/node.scm 132 */
																	bool_t BgL__ortest_1115z00_2112;

																	BgL__ortest_1115z00_2112 =
																		(BgL_classz00_2095 == BgL_oclassz00_2111);
																	if (BgL__ortest_1115z00_2112)
																		{	/* Sync/node.scm 132 */
																			BgL_res1828z00_2128 =
																				BgL__ortest_1115z00_2112;
																		}
																	else
																		{	/* Sync/node.scm 132 */
																			long BgL_odepthz00_2113;

																			{	/* Sync/node.scm 132 */
																				obj_t BgL_arg1804z00_2114;

																				BgL_arg1804z00_2114 =
																					(BgL_oclassz00_2111);
																				BgL_odepthz00_2113 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2114);
																			}
																			if ((2L < BgL_odepthz00_2113))
																				{	/* Sync/node.scm 132 */
																					obj_t BgL_arg1802z00_2116;

																					{	/* Sync/node.scm 132 */
																						obj_t BgL_arg1803z00_2117;

																						BgL_arg1803z00_2117 =
																							(BgL_oclassz00_2111);
																						BgL_arg1802z00_2116 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2117, 2L);
																					}
																					BgL_res1828z00_2128 =
																						(BgL_arg1802z00_2116 ==
																						BgL_classz00_2095);
																				}
																			else
																				{	/* Sync/node.scm 132 */
																					BgL_res1828z00_2128 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1849z00_2384 = BgL_res1828z00_2128;
														}
												}
											}
										}
										if (BgL_test1849z00_2384)
											{	/* Sync/node.scm 133 */
												obj_t BgL_arg1408z00_1811;
												obj_t BgL_arg1410z00_1812;

												{	/* Sync/node.scm 133 */
													obj_t BgL_arg1421z00_1813;

													{	/* Sync/node.scm 133 */
														BgL_nodez00_bglt BgL_arg1422z00_1814;

														BgL_arg1422z00_1814 =
															(((BgL_syncz00_bglt) COBJECT(BgL_nodez00_1778))->
															BgL_mutexz00);
														BgL_arg1421z00_1813 =
															MAKE_YOUNG_PAIR(((obj_t) BgL_arg1422z00_1814),
															BNIL);
													}
													BgL_arg1408z00_1811 =
														MAKE_YOUNG_PAIR(BGl_mlockz00zzsync_nodez00,
														BgL_arg1421z00_1813);
												}
												BgL_arg1410z00_1812 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_nodez00_1778)))->
													BgL_locz00);
												BgL_lockz00_1782 =
													BGl_applicationzd2ze3nodez31zzast_appz00
													(BgL_arg1408z00_1811, BNIL, BgL_arg1410z00_1812,
													CNST_TABLE_REF(13));
											}
										else
											{	/* Sync/node.scm 134 */
												obj_t BgL_arg1434z00_1815;
												obj_t BgL_arg1437z00_1816;

												{	/* Sync/node.scm 134 */
													obj_t BgL_arg1448z00_1817;

													{	/* Sync/node.scm 134 */
														BgL_nodez00_bglt BgL_arg1453z00_1818;
														obj_t BgL_arg1454z00_1819;

														BgL_arg1453z00_1818 =
															(((BgL_syncz00_bglt) COBJECT(BgL_nodez00_1778))->
															BgL_mutexz00);
														{	/* Sync/node.scm 134 */
															BgL_nodez00_bglt BgL_arg1472z00_1820;

															BgL_arg1472z00_1820 =
																(((BgL_syncz00_bglt)
																	COBJECT(BgL_nodez00_1778))->BgL_prelockz00);
															BgL_arg1454z00_1819 =
																MAKE_YOUNG_PAIR(((obj_t) BgL_arg1472z00_1820),
																BNIL);
														}
														BgL_arg1448z00_1817 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1453z00_1818),
															BgL_arg1454z00_1819);
													}
													BgL_arg1434z00_1815 =
														MAKE_YOUNG_PAIR(BGl_mlockprelockz00zzsync_nodez00,
														BgL_arg1448z00_1817);
												}
												BgL_arg1437z00_1816 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_nodez00_1778)))->
													BgL_locz00);
												BgL_lockz00_1782 =
													BGl_applicationzd2ze3nodez31zzast_appz00
													(BgL_arg1434z00_1815, BNIL, BgL_arg1437z00_1816,
													CNST_TABLE_REF(13));
											}
									}
									{	/* Sync/node.scm 132 */
										BgL_nodez00_bglt BgL_unlockz00_1783;

										{	/* Sync/node.scm 135 */
											obj_t BgL_arg1375z00_1805;
											obj_t BgL_arg1376z00_1806;

											{	/* Sync/node.scm 135 */
												obj_t BgL_arg1377z00_1807;

												{	/* Sync/node.scm 135 */
													BgL_nodez00_bglt BgL_arg1378z00_1808;

													BgL_arg1378z00_1808 =
														(((BgL_syncz00_bglt) COBJECT(BgL_nodez00_1778))->
														BgL_mutexz00);
													BgL_arg1377z00_1807 =
														MAKE_YOUNG_PAIR(((obj_t) BgL_arg1378z00_1808),
														BNIL);
												}
												BgL_arg1375z00_1805 =
													MAKE_YOUNG_PAIR(BGl_mulockz00zzsync_nodez00,
													BgL_arg1377z00_1807);
											}
											BgL_arg1376z00_1806 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_nodez00_1778)))->
												BgL_locz00);
											BgL_unlockz00_1783 =
												BGl_applicationzd2ze3nodez31zzast_appz00
												(BgL_arg1375z00_1805, BNIL, BgL_arg1376z00_1806,
												CNST_TABLE_REF(13));
										}
										{	/* Sync/node.scm 135 */
											BgL_refz00_bglt BgL_vrefz00_1784;

											{	/* Sync/node.scm 136 */
												BgL_refz00_bglt BgL_new1114z00_1802;

												{	/* Sync/node.scm 137 */
													BgL_refz00_bglt BgL_new1113z00_1803;

													BgL_new1113z00_1803 =
														((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_refz00_bgl))));
													{	/* Sync/node.scm 137 */
														long BgL_arg1371z00_1804;

														BgL_arg1371z00_1804 =
															BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1113z00_1803),
															BgL_arg1371z00_1804);
													}
													{	/* Sync/node.scm 137 */
														BgL_objectz00_bglt BgL_tmpz00_2440;

														BgL_tmpz00_2440 =
															((BgL_objectz00_bglt) BgL_new1113z00_1803);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2440, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1113z00_1803);
													BgL_new1114z00_1802 = BgL_new1113z00_1803;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1114z00_1802)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt)
																		BgL_nodez00_1778)))->BgL_locz00)), BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1114z00_1802)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt)
																		BgL_nodez00_1778)))->BgL_typez00)),
													BUNSPEC);
												((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																	BgL_new1114z00_1802)))->BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															BgL_tmpz00_1781)), BUNSPEC);
												BgL_vrefz00_1784 = BgL_new1114z00_1802;
											}
											{	/* Sync/node.scm 136 */
												BgL_letzd2varzd2_bglt BgL_lbodyz00_1785;

												{	/* Sync/node.scm 140 */
													BgL_letzd2varzd2_bglt BgL_new1116z00_1791;

													{	/* Sync/node.scm 141 */
														BgL_letzd2varzd2_bglt BgL_new1115z00_1800;

														BgL_new1115z00_1800 =
															((BgL_letzd2varzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_letzd2varzd2_bgl))));
														{	/* Sync/node.scm 141 */
															long BgL_arg1370z00_1801;

															BgL_arg1370z00_1801 =
																BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1115z00_1800),
																BgL_arg1370z00_1801);
														}
														{	/* Sync/node.scm 141 */
															BgL_objectz00_bglt BgL_tmpz00_2459;

															BgL_tmpz00_2459 =
																((BgL_objectz00_bglt) BgL_new1115z00_1800);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2459, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1115z00_1800);
														BgL_new1116z00_1791 = BgL_new1115z00_1800;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1116z00_1791)))->
															BgL_locz00) =
														((obj_t) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt)
																			BgL_nodez00_1778)))->BgL_locz00)),
														BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1116z00_1791)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt)
																			BgL_nodez00_1778)))->BgL_typez00)),
														BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1116z00_1791)))->
															BgL_sidezd2effectzd2) =
														((obj_t) BUNSPEC), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1116z00_1791)))->BgL_keyz00) =
														((obj_t) BINT(-1L)), BUNSPEC);
													{
														obj_t BgL_auxz00_2476;

														{	/* Sync/node.scm 143 */
															obj_t BgL_arg1351z00_1792;

															{	/* Sync/node.scm 143 */
																BgL_nodez00_bglt BgL_arg1361z00_1794;

																BgL_arg1361z00_1794 =
																	(((BgL_syncz00_bglt)
																		COBJECT(BgL_nodez00_1778))->BgL_bodyz00);
																BgL_arg1351z00_1792 =
																	MAKE_YOUNG_PAIR(((obj_t) BgL_tmpz00_1781),
																	((obj_t) BgL_arg1361z00_1794));
															}
															{	/* Sync/node.scm 143 */
																obj_t BgL_list1352z00_1793;

																BgL_list1352z00_1793 =
																	MAKE_YOUNG_PAIR(BgL_arg1351z00_1792, BNIL);
																BgL_auxz00_2476 = BgL_list1352z00_1793;
														}}
														((((BgL_letzd2varzd2_bglt)
																	COBJECT(BgL_new1116z00_1791))->
																BgL_bindingsz00) =
															((obj_t) BgL_auxz00_2476), BUNSPEC);
													}
													{
														BgL_nodez00_bglt BgL_auxz00_2483;

														{	/* Sync/node.scm 144 */
															BgL_sequencez00_bglt BgL_new1119z00_1795;

															{	/* Sync/node.scm 145 */
																BgL_sequencez00_bglt BgL_new1118z00_1798;

																BgL_new1118z00_1798 =
																	((BgL_sequencez00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_sequencez00_bgl))));
																{	/* Sync/node.scm 145 */
																	long BgL_arg1367z00_1799;

																	{	/* Sync/node.scm 145 */
																		obj_t BgL_classz00_2138;

																		BgL_classz00_2138 =
																			BGl_sequencez00zzast_nodez00;
																		BgL_arg1367z00_1799 =
																			BGL_CLASS_NUM(BgL_classz00_2138);
																	}
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1118z00_1798),
																		BgL_arg1367z00_1799);
																}
																{	/* Sync/node.scm 145 */
																	BgL_objectz00_bglt BgL_tmpz00_2488;

																	BgL_tmpz00_2488 =
																		((BgL_objectz00_bglt) BgL_new1118z00_1798);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2488,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1118z00_1798);
																BgL_new1119z00_1795 = BgL_new1118z00_1798;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1119z00_1795)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_nodez00_1778)))->BgL_locz00)),
																BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1119z00_1795)))->BgL_typez00) =
																((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_nodez00_1778)))->BgL_typez00)),
																BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1119z00_1795)))->
																	BgL_sidezd2effectzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1119z00_1795)))->BgL_keyz00) =
																((obj_t) BINT(-1L)), BUNSPEC);
															{
																obj_t BgL_auxz00_2505;

																{	/* Sync/node.scm 147 */
																	obj_t BgL_list1362z00_1796;

																	{	/* Sync/node.scm 147 */
																		obj_t BgL_arg1364z00_1797;

																		BgL_arg1364z00_1797 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_vrefz00_1784), BNIL);
																		BgL_list1362z00_1796 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_unlockz00_1783),
																			BgL_arg1364z00_1797);
																	}
																	BgL_auxz00_2505 = BgL_list1362z00_1796;
																}
																((((BgL_sequencez00_bglt)
																			COBJECT(BgL_new1119z00_1795))->
																		BgL_nodesz00) =
																	((obj_t) BgL_auxz00_2505), BUNSPEC);
															}
															((((BgL_sequencez00_bglt)
																		COBJECT(BgL_new1119z00_1795))->
																	BgL_unsafez00) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
															((((BgL_sequencez00_bglt)
																		COBJECT(BgL_new1119z00_1795))->
																	BgL_metaz00) = ((obj_t) BNIL), BUNSPEC);
															BgL_auxz00_2483 =
																((BgL_nodez00_bglt) BgL_new1119z00_1795);
														}
														((((BgL_letzd2varzd2_bglt)
																	COBJECT(BgL_new1116z00_1791))->BgL_bodyz00) =
															((BgL_nodez00_bglt) BgL_auxz00_2483), BUNSPEC);
													}
													((((BgL_letzd2varzd2_bglt)
																COBJECT(BgL_new1116z00_1791))->
															BgL_removablezf3zf3) =
														((bool_t) ((bool_t) 1)), BUNSPEC);
													BgL_lbodyz00_1785 = BgL_new1116z00_1791;
												}
												{	/* Sync/node.scm 140 */

													{	/* Sync/node.scm 148 */
														BgL_sequencez00_bglt BgL_new1122z00_1786;

														{	/* Sync/node.scm 149 */
															BgL_sequencez00_bglt BgL_new1120z00_1789;

															BgL_new1120z00_1789 =
																((BgL_sequencez00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_sequencez00_bgl))));
															{	/* Sync/node.scm 149 */
																long BgL_arg1349z00_1790;

																BgL_arg1349z00_1790 =
																	BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1120z00_1789),
																	BgL_arg1349z00_1790);
															}
															{	/* Sync/node.scm 149 */
																BgL_objectz00_bglt BgL_tmpz00_2520;

																BgL_tmpz00_2520 =
																	((BgL_objectz00_bglt) BgL_new1120z00_1789);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2520,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1120z00_1789);
															BgL_new1122z00_1786 = BgL_new1120z00_1789;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1122z00_1786)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_nodez00_1778)))->BgL_locz00)),
															BUNSPEC);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_new1122z00_1786)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_nodez00_1778)))->BgL_typez00)),
															BUNSPEC);
														((((BgL_nodezf2effectzf2_bglt)
																	COBJECT(((BgL_nodezf2effectzf2_bglt)
																			BgL_new1122z00_1786)))->
																BgL_sidezd2effectzd2) =
															((obj_t) BUNSPEC), BUNSPEC);
														((((BgL_nodezf2effectzf2_bglt)
																	COBJECT(((BgL_nodezf2effectzf2_bglt)
																			BgL_new1122z00_1786)))->BgL_keyz00) =
															((obj_t) BINT(-1L)), BUNSPEC);
														{
															obj_t BgL_auxz00_2537;

															{	/* Sync/node.scm 151 */
																obj_t BgL_arg1346z00_1787;

																{	/* Sync/node.scm 151 */
																	obj_t BgL_arg1348z00_1788;

																	BgL_arg1348z00_1788 =
																		(((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_nodez00_1778)))->BgL_locz00);
																	BgL_nodez00_1769 = BgL_lbodyz00_1785;
																	BgL_locz00_1770 = BgL_arg1348z00_1788;
																	if (CBOOL
																		(BGl_failsafezd2mutexzd2profilez00zzsync_nodez00))
																		{	/* Sync/node.scm 124 */
																			BgL_nodez00_bglt BgL_arg1339z00_1773;

																			{	/* Sync/node.scm 124 */
																				obj_t BgL_arg1343z00_1776;

																				BgL_arg1343z00_1776 =
																					MAKE_YOUNG_PAIR
																					(BGl_failsafezd2mutexzd2profilez00zzsync_nodez00,
																					BNIL);
																				BgL_arg1339z00_1773 =
																					BGl_applicationzd2ze3nodez31zzast_appz00
																					(BgL_arg1343z00_1776, BNIL,
																					BgL_locz00_1770, CNST_TABLE_REF(13));
																			}
																			{	/* Sync/node.scm 124 */
																				obj_t BgL_list1340z00_1774;

																				{	/* Sync/node.scm 124 */
																					obj_t BgL_arg1342z00_1775;

																					BgL_arg1342z00_1775 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_nodez00_1769), BNIL);
																					BgL_list1340z00_1774 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_arg1339z00_1773),
																						BgL_arg1342z00_1775);
																				}
																				BgL_arg1346z00_1787 =
																					BgL_list1340z00_1774;
																			}
																		}
																	else
																		{	/* Sync/node.scm 125 */
																			obj_t BgL_list1344z00_1777;

																			BgL_list1344z00_1777 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_nodez00_1769), BNIL);
																			BgL_arg1346z00_1787 =
																				BgL_list1344z00_1777;
																		}
																}
																BgL_auxz00_2537 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_lockz00_1782),
																	BgL_arg1346z00_1787);
															}
															((((BgL_sequencez00_bglt)
																		COBJECT(BgL_new1122z00_1786))->
																	BgL_nodesz00) =
																((obj_t) BgL_auxz00_2537), BUNSPEC);
														}
														((((BgL_sequencez00_bglt)
																	COBJECT(BgL_new1122z00_1786))->
																BgL_unsafez00) =
															((bool_t) ((bool_t) 0)), BUNSPEC);
														((((BgL_sequencez00_bglt)
																	COBJECT(BgL_new1122z00_1786))->BgL_metaz00) =
															((obj_t) BNIL), BUNSPEC);
														BgL_auxz00_2378 = BgL_new1122z00_1786;
													}
												}
											}
										}
									}
								}
							}
							BgL_enodez00_1755 = ((BgL_nodezf2effectzf2_bglt) BgL_auxz00_2378);
						}
					else
						{
							BgL_letzd2varzd2_bglt BgL_auxz00_2557;

							BgL_nodez00_1824 = BgL_nodez00_18;
							{	/* Sync/node.scm 157 */
								BgL_localz00_bglt BgL_tmpz00_1827;

								{	/* Sync/node.scm 157 */
									obj_t BgL_arg1650z00_1894;
									BgL_typez00_bglt BgL_arg1651z00_1895;

									BgL_arg1650z00_1894 =
										BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(14));
									BgL_arg1651z00_1895 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_nodez00_1824)))->BgL_typez00);
									BgL_tmpz00_1827 =
										BGl_makezd2localzd2svarz00zzast_localz00
										(BgL_arg1650z00_1894, BgL_arg1651z00_1895);
								}
								{	/* Sync/node.scm 157 */
									BgL_localz00_bglt BgL_topz00_1828;

									{	/* Sync/node.scm 158 */
										obj_t BgL_arg1646z00_1893;

										BgL_arg1646z00_1893 =
											BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(15));
										BgL_topz00_1828 =
											BGl_makezd2localzd2svarz00zzast_localz00
											(BgL_arg1646z00_1893,
											((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
									}
									{	/* Sync/node.scm 158 */
										BgL_refz00_bglt BgL_toprefz00_1829;

										{	/* Sync/node.scm 159 */
											BgL_refz00_bglt BgL_new1125z00_1890;

											{	/* Sync/node.scm 160 */
												BgL_refz00_bglt BgL_new1124z00_1891;

												BgL_new1124z00_1891 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Sync/node.scm 160 */
													long BgL_arg1642z00_1892;

													BgL_arg1642z00_1892 =
														BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1124z00_1891),
														BgL_arg1642z00_1892);
												}
												{	/* Sync/node.scm 160 */
													BgL_objectz00_bglt BgL_tmpz00_2571;

													BgL_tmpz00_2571 =
														((BgL_objectz00_bglt) BgL_new1124z00_1891);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2571, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1124z00_1891);
												BgL_new1125z00_1890 = BgL_new1124z00_1891;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1125z00_1890)))->
													BgL_locz00) =
												((obj_t) (((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) BgL_nodez00_1824)))->
														BgL_locz00)), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1125z00_1890)))->BgL_typez00) =
												((BgL_typez00_bglt) ((BgL_typez00_bglt)
														BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
											((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_new1125z00_1890)))->BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_topz00_1828)), BUNSPEC);
											BgL_toprefz00_1829 = BgL_new1125z00_1890;
										}
										{	/* Sync/node.scm 159 */
											BgL_nodez00_bglt BgL_gettopz00_1830;

											{	/* Sync/node.scm 163 */
												obj_t BgL_arg1629z00_1888;
												obj_t BgL_arg1630z00_1889;

												BgL_arg1629z00_1888 =
													MAKE_YOUNG_PAIR(BGl_getexitdtopz00zzsync_nodez00,
													BNIL);
												BgL_arg1630z00_1889 =
													(((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_nodez00_1824)))->BgL_locz00);
												BgL_gettopz00_1830 =
													BGl_applicationzd2ze3nodez31zzast_appz00
													(BgL_arg1629z00_1888, BNIL, BgL_arg1630z00_1889,
													CNST_TABLE_REF(13));
											}
											{	/* Sync/node.scm 163 */
												BgL_nodez00_bglt BgL_lockz00_1831;

												{	/* Sync/node.scm 164 */
													bool_t BgL_test1854z00_2590;

													{	/* Sync/node.scm 164 */
														BgL_nodez00_bglt BgL_arg1627z00_1887;

														BgL_arg1627z00_1887 =
															(((BgL_syncz00_bglt) COBJECT(BgL_nodez00_1824))->
															BgL_prelockz00);
														{	/* Sync/node.scm 164 */
															obj_t BgL_classz00_2151;

															BgL_classz00_2151 = BGl_atomz00zzast_nodez00;
															{	/* Sync/node.scm 164 */
																BgL_objectz00_bglt BgL_arg1807z00_2153;

																{	/* Sync/node.scm 164 */
																	obj_t BgL_tmpz00_2592;

																	BgL_tmpz00_2592 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1627z00_1887));
																	BgL_arg1807z00_2153 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_2592);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Sync/node.scm 164 */
																		long BgL_idxz00_2159;

																		BgL_idxz00_2159 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2153);
																		BgL_test1854z00_2590 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2159 + 2L)) ==
																			BgL_classz00_2151);
																	}
																else
																	{	/* Sync/node.scm 164 */
																		bool_t BgL_res1831z00_2184;

																		{	/* Sync/node.scm 164 */
																			obj_t BgL_oclassz00_2167;

																			{	/* Sync/node.scm 164 */
																				obj_t BgL_arg1815z00_2175;
																				long BgL_arg1816z00_2176;

																				BgL_arg1815z00_2175 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Sync/node.scm 164 */
																					long BgL_arg1817z00_2177;

																					BgL_arg1817z00_2177 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2153);
																					BgL_arg1816z00_2176 =
																						(BgL_arg1817z00_2177 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2167 =
																					VECTOR_REF(BgL_arg1815z00_2175,
																					BgL_arg1816z00_2176);
																			}
																			{	/* Sync/node.scm 164 */
																				bool_t BgL__ortest_1115z00_2168;

																				BgL__ortest_1115z00_2168 =
																					(BgL_classz00_2151 ==
																					BgL_oclassz00_2167);
																				if (BgL__ortest_1115z00_2168)
																					{	/* Sync/node.scm 164 */
																						BgL_res1831z00_2184 =
																							BgL__ortest_1115z00_2168;
																					}
																				else
																					{	/* Sync/node.scm 164 */
																						long BgL_odepthz00_2169;

																						{	/* Sync/node.scm 164 */
																							obj_t BgL_arg1804z00_2170;

																							BgL_arg1804z00_2170 =
																								(BgL_oclassz00_2167);
																							BgL_odepthz00_2169 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2170);
																						}
																						if ((2L < BgL_odepthz00_2169))
																							{	/* Sync/node.scm 164 */
																								obj_t BgL_arg1802z00_2172;

																								{	/* Sync/node.scm 164 */
																									obj_t BgL_arg1803z00_2173;

																									BgL_arg1803z00_2173 =
																										(BgL_oclassz00_2167);
																									BgL_arg1802z00_2172 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2173, 2L);
																								}
																								BgL_res1831z00_2184 =
																									(BgL_arg1802z00_2172 ==
																									BgL_classz00_2151);
																							}
																						else
																							{	/* Sync/node.scm 164 */
																								BgL_res1831z00_2184 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1854z00_2590 = BgL_res1831z00_2184;
																	}
															}
														}
													}
													if (BgL_test1854z00_2590)
														{	/* Sync/node.scm 165 */
															obj_t BgL_arg1602z00_1877;
															obj_t BgL_arg1605z00_1878;

															{	/* Sync/node.scm 165 */
																obj_t BgL_arg1606z00_1879;

																{	/* Sync/node.scm 165 */
																	BgL_nodez00_bglt BgL_arg1609z00_1880;

																	BgL_arg1609z00_1880 =
																		(((BgL_syncz00_bglt)
																			COBJECT(BgL_nodez00_1824))->BgL_mutexz00);
																	BgL_arg1606z00_1879 =
																		MAKE_YOUNG_PAIR(((obj_t)
																			BgL_arg1609z00_1880), BNIL);
																}
																BgL_arg1602z00_1877 =
																	MAKE_YOUNG_PAIR(BGl_mlockz00zzsync_nodez00,
																	BgL_arg1606z00_1879);
															}
															BgL_arg1605z00_1878 =
																(((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_nodez00_1824)))->
																BgL_locz00);
															BgL_lockz00_1831 =
																BGl_applicationzd2ze3nodez31zzast_appz00
																(BgL_arg1602z00_1877, BNIL, BgL_arg1605z00_1878,
																CNST_TABLE_REF(13));
														}
													else
														{	/* Sync/node.scm 166 */
															obj_t BgL_arg1611z00_1881;
															obj_t BgL_arg1613z00_1882;

															{	/* Sync/node.scm 166 */
																obj_t BgL_arg1615z00_1883;

																{	/* Sync/node.scm 166 */
																	BgL_nodez00_bglt BgL_arg1616z00_1884;
																	obj_t BgL_arg1625z00_1885;

																	BgL_arg1616z00_1884 =
																		(((BgL_syncz00_bglt)
																			COBJECT(BgL_nodez00_1824))->BgL_mutexz00);
																	{	/* Sync/node.scm 166 */
																		BgL_nodez00_bglt BgL_arg1626z00_1886;

																		BgL_arg1626z00_1886 =
																			(((BgL_syncz00_bglt)
																				COBJECT(BgL_nodez00_1824))->
																			BgL_prelockz00);
																		BgL_arg1625z00_1885 =
																			MAKE_YOUNG_PAIR(((obj_t)
																				BgL_arg1626z00_1886), BNIL);
																	}
																	BgL_arg1615z00_1883 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1616z00_1884),
																		BgL_arg1625z00_1885);
																}
																BgL_arg1611z00_1881 =
																	MAKE_YOUNG_PAIR
																	(BGl_mlockprelockz00zzsync_nodez00,
																	BgL_arg1615z00_1883);
															}
															BgL_arg1613z00_1882 =
																(((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_nodez00_1824)))->
																BgL_locz00);
															BgL_lockz00_1831 =
																BGl_applicationzd2ze3nodez31zzast_appz00
																(BgL_arg1611z00_1881, BNIL, BgL_arg1613z00_1882,
																CNST_TABLE_REF(13));
														}
												}
												{	/* Sync/node.scm 164 */
													BgL_nodez00_bglt BgL_pushz00_1832;

													{	/* Sync/node.scm 167 */
														obj_t BgL_arg1585z00_1870;
														obj_t BgL_arg1589z00_1871;

														{	/* Sync/node.scm 167 */
															obj_t BgL_arg1591z00_1872;

															{	/* Sync/node.scm 167 */
																obj_t BgL_arg1593z00_1873;

																{	/* Sync/node.scm 167 */
																	BgL_nodez00_bglt BgL_arg1594z00_1874;

																	BgL_arg1594z00_1874 =
																		(((BgL_syncz00_bglt)
																			COBJECT(BgL_nodez00_1824))->BgL_mutexz00);
																	BgL_arg1593z00_1873 =
																		MAKE_YOUNG_PAIR(((obj_t)
																			BgL_arg1594z00_1874), BNIL);
																}
																BgL_arg1591z00_1872 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_toprefz00_1829),
																	BgL_arg1593z00_1873);
															}
															BgL_arg1585z00_1870 =
																MAKE_YOUNG_PAIR(BGl_mpushz00zzsync_nodez00,
																BgL_arg1591z00_1872);
														}
														BgL_arg1589z00_1871 =
															(((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_nodez00_1824)))->
															BgL_locz00);
														BgL_pushz00_1832 =
															BGl_applicationzd2ze3nodez31zzast_appz00
															(BgL_arg1585z00_1870, BNIL, BgL_arg1589z00_1871,
															CNST_TABLE_REF(13));
													}
													{	/* Sync/node.scm 167 */
														BgL_nodez00_bglt BgL_popz00_1833;

														{	/* Sync/node.scm 168 */
															obj_t BgL_arg1575z00_1867;
															obj_t BgL_arg1576z00_1868;

															{	/* Sync/node.scm 168 */
																obj_t BgL_arg1584z00_1869;

																BgL_arg1584z00_1869 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_toprefz00_1829), BNIL);
																BgL_arg1575z00_1867 =
																	MAKE_YOUNG_PAIR(BGl_mpopz00zzsync_nodez00,
																	BgL_arg1584z00_1869);
															}
															BgL_arg1576z00_1868 =
																(((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_nodez00_1824)))->
																BgL_locz00);
															BgL_popz00_1833 =
																BGl_applicationzd2ze3nodez31zzast_appz00
																(BgL_arg1575z00_1867, BNIL, BgL_arg1576z00_1868,
																CNST_TABLE_REF(13));
														}
														{	/* Sync/node.scm 168 */
															BgL_nodez00_bglt BgL_unlockz00_1834;

															{	/* Sync/node.scm 169 */
																obj_t BgL_arg1564z00_1863;
																obj_t BgL_arg1565z00_1864;

																{	/* Sync/node.scm 169 */
																	obj_t BgL_arg1571z00_1865;

																	{	/* Sync/node.scm 169 */
																		BgL_nodez00_bglt BgL_arg1573z00_1866;

																		BgL_arg1573z00_1866 =
																			(((BgL_syncz00_bglt)
																				COBJECT(BgL_nodez00_1824))->
																			BgL_mutexz00);
																		BgL_arg1571z00_1865 =
																			MAKE_YOUNG_PAIR(((obj_t)
																				BgL_arg1573z00_1866), BNIL);
																	}
																	BgL_arg1564z00_1863 =
																		MAKE_YOUNG_PAIR(BGl_mulockz00zzsync_nodez00,
																		BgL_arg1571z00_1865);
																}
																BgL_arg1565z00_1864 =
																	(((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt) BgL_nodez00_1824)))->
																	BgL_locz00);
																BgL_unlockz00_1834 =
																	BGl_applicationzd2ze3nodez31zzast_appz00
																	(BgL_arg1564z00_1863, BNIL,
																	BgL_arg1565z00_1864, CNST_TABLE_REF(13));
															}
															{	/* Sync/node.scm 169 */
																BgL_refz00_bglt BgL_vrefz00_1835;

																{	/* Sync/node.scm 170 */
																	BgL_refz00_bglt BgL_new1129z00_1860;

																	{	/* Sync/node.scm 171 */
																		BgL_refz00_bglt BgL_new1127z00_1861;

																		BgL_new1127z00_1861 =
																			((BgL_refz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_refz00_bgl))));
																		{	/* Sync/node.scm 171 */
																			long BgL_arg1561z00_1862;

																			BgL_arg1561z00_1862 =
																				BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1127z00_1861),
																				BgL_arg1561z00_1862);
																		}
																		{	/* Sync/node.scm 171 */
																			BgL_objectz00_bglt BgL_tmpz00_2663;

																			BgL_tmpz00_2663 =
																				((BgL_objectz00_bglt)
																				BgL_new1127z00_1861);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2663,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1127z00_1861);
																		BgL_new1129z00_1860 = BgL_new1127z00_1861;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1129z00_1860)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_nodez00_1824)))->BgL_locz00)),
																		BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1129z00_1860)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_nodez00_1824)))->
																				BgL_typez00)), BUNSPEC);
																	((((BgL_varz00_bglt)
																				COBJECT(((BgL_varz00_bglt)
																						BgL_new1129z00_1860)))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_tmpz00_1827)), BUNSPEC);
																	BgL_vrefz00_1835 = BgL_new1129z00_1860;
																}
																{	/* Sync/node.scm 170 */
																	BgL_letzd2varzd2_bglt BgL_lbodyz00_1836;

																	{	/* Sync/node.scm 174 */
																		BgL_letzd2varzd2_bglt BgL_new1131z00_1848;

																		{	/* Sync/node.scm 175 */
																			BgL_letzd2varzd2_bglt BgL_new1130z00_1858;

																			BgL_new1130z00_1858 =
																				((BgL_letzd2varzd2_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_letzd2varzd2_bgl))));
																			{	/* Sync/node.scm 175 */
																				long BgL_arg1559z00_1859;

																				BgL_arg1559z00_1859 =
																					BGL_CLASS_NUM
																					(BGl_letzd2varzd2zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1130z00_1858),
																					BgL_arg1559z00_1859);
																			}
																			{	/* Sync/node.scm 175 */
																				BgL_objectz00_bglt BgL_tmpz00_2682;

																				BgL_tmpz00_2682 =
																					((BgL_objectz00_bglt)
																					BgL_new1130z00_1858);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2682,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1130z00_1858);
																			BgL_new1131z00_1848 = BgL_new1130z00_1858;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1131z00_1848)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_nodez00_1824)))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1131z00_1848)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_nodez00_1824)))->
																					BgL_typez00)), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1131z00_1848)))->
																				BgL_sidezd2effectzd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1131z00_1848)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		{
																			obj_t BgL_auxz00_2699;

																			{	/* Sync/node.scm 177 */
																				obj_t BgL_arg1540z00_1849;

																				{	/* Sync/node.scm 177 */
																					BgL_nodez00_bglt BgL_arg1544z00_1851;

																					BgL_arg1544z00_1851 =
																						(((BgL_syncz00_bglt)
																							COBJECT(BgL_nodez00_1824))->
																						BgL_bodyz00);
																					BgL_arg1540z00_1849 =
																						MAKE_YOUNG_PAIR(((obj_t)
																							BgL_tmpz00_1827),
																						((obj_t) BgL_arg1544z00_1851));
																				}
																				{	/* Sync/node.scm 177 */
																					obj_t BgL_list1541z00_1850;

																					BgL_list1541z00_1850 =
																						MAKE_YOUNG_PAIR(BgL_arg1540z00_1849,
																						BNIL);
																					BgL_auxz00_2699 =
																						BgL_list1541z00_1850;
																			}}
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1131z00_1848))->
																					BgL_bindingsz00) =
																				((obj_t) BgL_auxz00_2699), BUNSPEC);
																		}
																		{
																			BgL_nodez00_bglt BgL_auxz00_2706;

																			{	/* Sync/node.scm 178 */
																				BgL_sequencez00_bglt
																					BgL_new1134z00_1852;
																				{	/* Sync/node.scm 179 */
																					BgL_sequencez00_bglt
																						BgL_new1133z00_1856;
																					BgL_new1133z00_1856 =
																						((BgL_sequencez00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_sequencez00_bgl))));
																					{	/* Sync/node.scm 179 */
																						long BgL_arg1553z00_1857;

																						{	/* Sync/node.scm 179 */
																							obj_t BgL_classz00_2194;

																							BgL_classz00_2194 =
																								BGl_sequencez00zzast_nodez00;
																							BgL_arg1553z00_1857 =
																								BGL_CLASS_NUM
																								(BgL_classz00_2194);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1133z00_1856),
																							BgL_arg1553z00_1857);
																					}
																					{	/* Sync/node.scm 179 */
																						BgL_objectz00_bglt BgL_tmpz00_2711;

																						BgL_tmpz00_2711 =
																							((BgL_objectz00_bglt)
																							BgL_new1133z00_1856);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_2711, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1133z00_1856);
																					BgL_new1134z00_1852 =
																						BgL_new1133z00_1856;
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1134z00_1852)))->
																						BgL_locz00) =
																					((obj_t) (((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_nodez00_1824)))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_new1134z00_1852)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) ((
																								(BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_nodez00_1824)))->
																							BgL_typez00)), BUNSPEC);
																				((((BgL_nodezf2effectzf2_bglt)
																							COBJECT((
																									(BgL_nodezf2effectzf2_bglt)
																									BgL_new1134z00_1852)))->
																						BgL_sidezd2effectzd2) =
																					((obj_t) BUNSPEC), BUNSPEC);
																				((((BgL_nodezf2effectzf2_bglt)
																							COBJECT((
																									(BgL_nodezf2effectzf2_bglt)
																									BgL_new1134z00_1852)))->
																						BgL_keyz00) =
																					((obj_t) BINT(-1L)), BUNSPEC);
																				{
																					obj_t BgL_auxz00_2728;

																					{	/* Sync/node.scm 181 */
																						obj_t BgL_list1545z00_1853;

																						{	/* Sync/node.scm 181 */
																							obj_t BgL_arg1546z00_1854;

																							{	/* Sync/node.scm 181 */
																								obj_t BgL_arg1552z00_1855;

																								BgL_arg1552z00_1855 =
																									MAKE_YOUNG_PAIR(
																									((obj_t) BgL_vrefz00_1835),
																									BNIL);
																								BgL_arg1546z00_1854 =
																									MAKE_YOUNG_PAIR(((obj_t)
																										BgL_unlockz00_1834),
																									BgL_arg1552z00_1855);
																							}
																							BgL_list1545z00_1853 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_popz00_1833),
																								BgL_arg1546z00_1854);
																						}
																						BgL_auxz00_2728 =
																							BgL_list1545z00_1853;
																					}
																					((((BgL_sequencez00_bglt)
																								COBJECT(BgL_new1134z00_1852))->
																							BgL_nodesz00) =
																						((obj_t) BgL_auxz00_2728), BUNSPEC);
																				}
																				((((BgL_sequencez00_bglt)
																							COBJECT(BgL_new1134z00_1852))->
																						BgL_unsafez00) =
																					((bool_t) ((bool_t) 0)), BUNSPEC);
																				((((BgL_sequencez00_bglt)
																							COBJECT(BgL_new1134z00_1852))->
																						BgL_metaz00) =
																					((obj_t) BNIL), BUNSPEC);
																				BgL_auxz00_2706 =
																					((BgL_nodez00_bglt)
																					BgL_new1134z00_1852);
																			}
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1131z00_1848))->
																					BgL_bodyz00) =
																				((BgL_nodez00_bglt) BgL_auxz00_2706),
																				BUNSPEC);
																		}
																		((((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_new1131z00_1848))->
																				BgL_removablezf3zf3) =
																			((bool_t) ((bool_t) 1)), BUNSPEC);
																		BgL_lbodyz00_1836 = BgL_new1131z00_1848;
																	}
																	{	/* Sync/node.scm 174 */

																		{	/* Sync/node.scm 182 */
																			BgL_letzd2varzd2_bglt BgL_new1136z00_1837;

																			{	/* Sync/node.scm 183 */
																				BgL_letzd2varzd2_bglt
																					BgL_new1135z00_1846;
																				BgL_new1135z00_1846 =
																					((BgL_letzd2varzd2_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_letzd2varzd2_bgl))));
																				{	/* Sync/node.scm 183 */
																					long BgL_arg1535z00_1847;

																					BgL_arg1535z00_1847 =
																						BGL_CLASS_NUM
																						(BGl_letzd2varzd2zzast_nodez00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1135z00_1846),
																						BgL_arg1535z00_1847);
																				}
																				{	/* Sync/node.scm 183 */
																					BgL_objectz00_bglt BgL_tmpz00_2745;

																					BgL_tmpz00_2745 =
																						((BgL_objectz00_bglt)
																						BgL_new1135z00_1846);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_2745, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1135z00_1846);
																				BgL_new1136z00_1837 =
																					BgL_new1135z00_1846;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1136z00_1837)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_nodez00_1824)))->
																						BgL_locz00)), BUNSPEC);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_new1136z00_1837)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_nodez00_1824)))->
																						BgL_typez00)), BUNSPEC);
																			((((BgL_nodezf2effectzf2_bglt)
																						COBJECT(((BgL_nodezf2effectzf2_bglt)
																								BgL_new1136z00_1837)))->
																					BgL_sidezd2effectzd2) =
																				((obj_t) BUNSPEC), BUNSPEC);
																			((((BgL_nodezf2effectzf2_bglt)
																						COBJECT(((BgL_nodezf2effectzf2_bglt)
																								BgL_new1136z00_1837)))->
																					BgL_keyz00) =
																				((obj_t) BINT(-1L)), BUNSPEC);
																			{
																				obj_t BgL_auxz00_2762;

																				{	/* Sync/node.scm 185 */
																					obj_t BgL_arg1502z00_1838;

																					BgL_arg1502z00_1838 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_topz00_1828),
																						((obj_t) BgL_gettopz00_1830));
																					{	/* Sync/node.scm 185 */
																						obj_t BgL_list1503z00_1839;

																						BgL_list1503z00_1839 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1502z00_1838, BNIL);
																						BgL_auxz00_2762 =
																							BgL_list1503z00_1839;
																				}}
																				((((BgL_letzd2varzd2_bglt)
																							COBJECT(BgL_new1136z00_1837))->
																						BgL_bindingsz00) =
																					((obj_t) BgL_auxz00_2762), BUNSPEC);
																			}
																			{
																				BgL_nodez00_bglt BgL_auxz00_2768;

																				{	/* Sync/node.scm 186 */
																					BgL_sequencez00_bglt
																						BgL_new1138z00_1840;
																					{	/* Sync/node.scm 187 */
																						BgL_sequencez00_bglt
																							BgL_new1137z00_1844;
																						BgL_new1137z00_1844 =
																							((BgL_sequencez00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_sequencez00_bgl))));
																						{	/* Sync/node.scm 187 */
																							long BgL_arg1516z00_1845;

																							{	/* Sync/node.scm 187 */
																								obj_t BgL_classz00_2204;

																								BgL_classz00_2204 =
																									BGl_sequencez00zzast_nodez00;
																								BgL_arg1516z00_1845 =
																									BGL_CLASS_NUM
																									(BgL_classz00_2204);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1137z00_1844),
																								BgL_arg1516z00_1845);
																						}
																						{	/* Sync/node.scm 187 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_2773;
																							BgL_tmpz00_2773 =
																								((BgL_objectz00_bglt)
																								BgL_new1137z00_1844);
																							BGL_OBJECT_WIDENING_SET
																								(BgL_tmpz00_2773, BFALSE);
																						}
																						((BgL_objectz00_bglt)
																							BgL_new1137z00_1844);
																						BgL_new1138z00_1840 =
																							BgL_new1137z00_1844;
																					}
																					((((BgL_nodez00_bglt) COBJECT(
																									((BgL_nodez00_bglt)
																										BgL_new1138z00_1840)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt)
																											BgL_nodez00_1824)))->
																								BgL_locz00)), BUNSPEC);
																					((((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_new1138z00_1840)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) ((
																									(BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt)
																											BgL_nodez00_1824)))->
																								BgL_typez00)), BUNSPEC);
																					((((BgL_nodezf2effectzf2_bglt)
																								COBJECT((
																										(BgL_nodezf2effectzf2_bglt)
																										BgL_new1138z00_1840)))->
																							BgL_sidezd2effectzd2) =
																						((obj_t) BUNSPEC), BUNSPEC);
																					((((BgL_nodezf2effectzf2_bglt)
																								COBJECT((
																										(BgL_nodezf2effectzf2_bglt)
																										BgL_new1138z00_1840)))->
																							BgL_keyz00) =
																						((obj_t) BINT(-1L)), BUNSPEC);
																					{
																						obj_t BgL_auxz00_2790;

																						{	/* Sync/node.scm 189 */
																							obj_t BgL_arg1509z00_1841;

																							{	/* Sync/node.scm 189 */
																								obj_t BgL_arg1513z00_1842;

																								{	/* Sync/node.scm 189 */
																									obj_t BgL_arg1514z00_1843;

																									BgL_arg1514z00_1843 =
																										(((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_nodez00_1824)))->
																										BgL_locz00);
																									BgL_nodez00_1760 =
																										BgL_lbodyz00_1836;
																									BgL_locz00_1761 =
																										BgL_arg1514z00_1843;
																									if (CBOOL
																										(BGl_exitdzd2mutexzd2profilez00zzsync_nodez00))
																										{	/* Sync/node.scm 119 */
																											BgL_nodez00_bglt
																												BgL_arg1331z00_1764;
																											{	/* Sync/node.scm 119 */
																												obj_t
																													BgL_arg1335z00_1767;
																												BgL_arg1335z00_1767 =
																													MAKE_YOUNG_PAIR
																													(BGl_exitdzd2mutexzd2profilez00zzsync_nodez00,
																													BNIL);
																												BgL_arg1331z00_1764 =
																													BGl_applicationzd2ze3nodez31zzast_appz00
																													(BgL_arg1335z00_1767,
																													BNIL, BgL_locz00_1761,
																													CNST_TABLE_REF(13));
																											}
																											{	/* Sync/node.scm 119 */
																												obj_t
																													BgL_list1332z00_1765;
																												{	/* Sync/node.scm 119 */
																													obj_t
																														BgL_arg1333z00_1766;
																													BgL_arg1333z00_1766 =
																														MAKE_YOUNG_PAIR((
																															(obj_t)
																															BgL_nodez00_1760),
																														BNIL);
																													BgL_list1332z00_1765 =
																														MAKE_YOUNG_PAIR((
																															(obj_t)
																															BgL_arg1331z00_1764),
																														BgL_arg1333z00_1766);
																												}
																												BgL_arg1513z00_1842 =
																													BgL_list1332z00_1765;
																											}
																										}
																									else
																										{	/* Sync/node.scm 120 */
																											obj_t
																												BgL_list1336z00_1768;
																											BgL_list1336z00_1768 =
																												MAKE_YOUNG_PAIR(((obj_t)
																													BgL_nodez00_1760),
																												BNIL);
																											BgL_arg1513z00_1842 =
																												BgL_list1336z00_1768;
																										}
																								}
																								BgL_arg1509z00_1841 =
																									MAKE_YOUNG_PAIR(
																									((obj_t) BgL_pushz00_1832),
																									BgL_arg1513z00_1842);
																							}
																							BgL_auxz00_2790 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_lockz00_1831),
																								BgL_arg1509z00_1841);
																						}
																						((((BgL_sequencez00_bglt)
																									COBJECT
																									(BgL_new1138z00_1840))->
																								BgL_nodesz00) =
																							((obj_t) BgL_auxz00_2790),
																							BUNSPEC);
																					}
																					((((BgL_sequencez00_bglt)
																								COBJECT(BgL_new1138z00_1840))->
																							BgL_unsafez00) =
																						((bool_t) ((bool_t) 0)), BUNSPEC);
																					((((BgL_sequencez00_bglt)
																								COBJECT(BgL_new1138z00_1840))->
																							BgL_metaz00) =
																						((obj_t) BNIL), BUNSPEC);
																					BgL_auxz00_2768 =
																						((BgL_nodez00_bglt)
																						BgL_new1138z00_1840);
																				}
																				((((BgL_letzd2varzd2_bglt)
																							COBJECT(BgL_new1136z00_1837))->
																						BgL_bodyz00) =
																					((BgL_nodez00_bglt) BgL_auxz00_2768),
																					BUNSPEC);
																			}
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1136z00_1837))->
																					BgL_removablezf3zf3) =
																				((bool_t) ((bool_t) 1)), BUNSPEC);
																			BgL_auxz00_2557 = BgL_new1136z00_1837;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
							BgL_enodez00_1755 = ((BgL_nodezf2effectzf2_bglt) BgL_auxz00_2557);
						}
					BGl_occurzd2nodez12zc0zzast_occurz00(
						((BgL_nodez00_bglt) BgL_enodez00_1755));
					return ((BgL_nodez00_bglt) BgL_enodez00_1755);
				}
			}
		}

	}



/* &sync->sequence */
	BgL_nodez00_bglt BGl_z62synczd2ze3sequencez53zzsync_nodez00(obj_t
		BgL_envz00_2242, obj_t BgL_nodez00_2243)
	{
		{	/* Sync/node.scm 112 */
			return
				BGl_synczd2ze3sequencez31zzsync_nodez00(
				((BgL_syncz00_bglt) BgL_nodez00_2243));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsync_nodez00(void)
	{
		{	/* Sync/node.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsync_nodez00(void)
	{
		{	/* Sync/node.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsync_nodez00(void)
	{
		{	/* Sync/node.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsync_nodez00(void)
	{
		{	/* Sync/node.scm 18 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzast_appz00(449859288L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzinline_appz00(148760855L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
			return
				BGl_modulezd2initializa7ationz75zzsync_failsafez00(377892095L,
				BSTRING_TO_STRING(BGl_string1836z00zzsync_nodez00));
		}

	}

#ifdef __cplusplus
}
#endif
