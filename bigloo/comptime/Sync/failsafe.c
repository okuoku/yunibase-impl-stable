/*===========================================================================*/
/*   (Sync/failsafe.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Sync/failsafe.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SYNC_FAILSAFE_TYPE_DEFINITIONS
#define BGL_SYNC_FAILSAFE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_SYNC_FAILSAFE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_failsafezd2synczf3z21zzsync_failsafez00(BgL_syncz00_bglt);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzsync_failsafez00 = BUNSPEC;
	extern obj_t BGl_funz00zzast_varz00;
	extern obj_t BGl_syncz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_failsafezd2funzf3z21zzsync_failsafez00(BgL_funz00_bglt,
		BgL_variablez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62failsafezf3zd2sequence1292z43zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzsync_failsafez00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzsync_failsafez00(void);
	static obj_t BGl_objectzd2initzd2zzsync_failsafez00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62failsafezf3zd2pragma1306z43zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62failsafezd2funzf3zd2cfun1302z91zzsync_failsafez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsync_failsafez00(void);
	static obj_t BGl_z62failsafezf3zd2var1288z43zzsync_failsafez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62failsafezf3zd2atom1286z43zzsync_failsafez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62failsafezf3zd2letzd2fun1316z91zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_pragmaz00zzast_nodez00;
	static obj_t BGl_z62failsafezf3zd2app1296z43zzsync_failsafez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62failsafezf3zd2kwote1290z43zzsync_failsafez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsync_failsafez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62failsafezf3zd2cast1308z43zzsync_failsafez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_z62failsafezf3zd2boxzd2setz121324z83zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62failsafezf3zd2makezd2box1320z91zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_za2optimzd2synczd2failsafezf3za2zf3zzengine_paramz00;
	static obj_t BGl_z62failsafezf3zd2setq1310z43zzsync_failsafez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzsync_failsafez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsync_failsafez00(void);
	static obj_t BGl_z62failsafezf3z91zzsync_failsafez00(obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsync_failsafez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsync_failsafez00(void);
	static obj_t BGl_z62failsafezd2funzf3z43zzsync_failsafez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62failsafezf3zd2letzd2var1318z91zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_failsafezf3zf3zzsync_failsafez00(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62failsafezd2funzf31297z43zzsync_failsafez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62failsafezd2synczf3z43zzsync_failsafez00(obj_t, obj_t);
	static obj_t BGl_z62failsafezf3zd2conditiona1312z43zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62failsafezd2funzf3zd2sfun1300z91zzsync_failsafez00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62failsafezf3zd2boxzd2ref1322z91zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62failsafezf3zd2extern1304z43zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62failsafezf3zd2sync1294z43zzsync_failsafez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62failsafezf3zd2switch1314z43zzsync_failsafez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62failsafezf31283z91zzsync_failsafez00(obj_t, obj_t, obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_failsafezf3zd2envz21zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za791799za7,
		BGl_z62failsafezf3z91zzsync_failsafez00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_failsafezd2funzf3zd2envzf3zzsync_failsafez00,
		BgL_bgl_za762failsafeza7d2fu1800z00,
		BGl_z62failsafezd2funzf3z43zzsync_failsafez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1765z00zzsync_failsafez00,
		BgL_bgl_string1765za700za7za7s1801za7, "failsafe?1283", 13);
	      DEFINE_STRING(BGl_string1767z00zzsync_failsafez00,
		BgL_bgl_string1767za700za7za7s1802za7, "failsafe-fun?1297", 17);
	      DEFINE_STRING(BGl_string1769z00zzsync_failsafez00,
		BgL_bgl_string1769za700za7za7s1803za7, "failsafe?", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1764z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3121804z00,
		BGl_z62failsafezf31283z91zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1766z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7d2fu1805z00,
		BGl_z62failsafezd2funzf31297z43zzsync_failsafez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1768z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1806za7,
		BGl_z62failsafezf3zd2atom1286z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1776z00zzsync_failsafez00,
		BgL_bgl_string1776za700za7za7s1807za7, "failsafe-fun?", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_failsafezd2synczf3zd2envzf3zzsync_failsafez00,
		BgL_bgl_za762failsafeza7d2sy1808z00,
		BGl_z62failsafezd2synczf3z43zzsync_failsafez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1770z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1809za7,
		BGl_z62failsafezf3zd2var1288z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1771z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1810za7,
		BGl_z62failsafezf3zd2kwote1290z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1772z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1811za7,
		BGl_z62failsafezf3zd2sequence1292z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1773z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1812za7,
		BGl_z62failsafezf3zd2sync1294z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1774z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1813za7,
		BGl_z62failsafezf3zd2app1296z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1775z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7d2fu1814z00,
		BGl_z62failsafezd2funzf3zd2sfun1300z91zzsync_failsafez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1777z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7d2fu1815z00,
		BGl_z62failsafezd2funzf3zd2cfun1302z91zzsync_failsafez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1778z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1816za7,
		BGl_z62failsafezf3zd2extern1304z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1779z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1817za7,
		BGl_z62failsafezf3zd2pragma1306z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1789z00zzsync_failsafez00,
		BgL_bgl_string1789za700za7za7s1818za7, "sync_failsafe", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1780z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1819za7,
		BGl_z62failsafezf3zd2cast1308z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1781z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1820za7,
		BGl_z62failsafezf3zd2setq1310z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1782z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1821za7,
		BGl_z62failsafezf3zd2conditiona1312z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1783z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1822za7,
		BGl_z62failsafezf3zd2switch1314z43zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1790z00zzsync_failsafez00,
		BgL_bgl_string1790za700za7za7s1823za7, "fail-safe ", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1784z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1824za7,
		BGl_z62failsafezf3zd2letzd2fun1316z91zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1785z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1825za7,
		BGl_z62failsafezf3zd2letzd2var1318z91zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1786z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1826za7,
		BGl_z62failsafezf3zd2makezd2box1320z91zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1787z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1827za7,
		BGl_z62failsafezf3zd2boxzd2ref1322z91zzsync_failsafez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1788z00zzsync_failsafez00,
		BgL_bgl_za762failsafeza7f3za7d1828za7,
		BGl_z62failsafezf3zd2boxzd2setz121324z83zzsync_failsafez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsync_failsafez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsync_failsafez00(long
		BgL_checksumz00_2503, char *BgL_fromz00_2504)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsync_failsafez00))
				{
					BGl_requirezd2initializa7ationz75zzsync_failsafez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsync_failsafez00();
					BGl_libraryzd2moduleszd2initz00zzsync_failsafez00();
					BGl_cnstzd2initzd2zzsync_failsafez00();
					BGl_importedzd2moduleszd2initz00zzsync_failsafez00();
					BGl_genericzd2initzd2zzsync_failsafez00();
					BGl_methodzd2initzd2zzsync_failsafez00();
					return BGl_toplevelzd2initzd2zzsync_failsafez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsync_failsafez00(void)
	{
		{	/* Sync/failsafe.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L,
				"sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"sync_failsafe");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "sync_failsafe");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsync_failsafez00(void)
	{
		{	/* Sync/failsafe.scm 16 */
			{	/* Sync/failsafe.scm 16 */
				obj_t BgL_cportz00_2393;

				{	/* Sync/failsafe.scm 16 */
					obj_t BgL_stringz00_2400;

					BgL_stringz00_2400 = BGl_string1790z00zzsync_failsafez00;
					{	/* Sync/failsafe.scm 16 */
						obj_t BgL_startz00_2401;

						BgL_startz00_2401 = BINT(0L);
						{	/* Sync/failsafe.scm 16 */
							obj_t BgL_endz00_2402;

							BgL_endz00_2402 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2400)));
							{	/* Sync/failsafe.scm 16 */

								BgL_cportz00_2393 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2400, BgL_startz00_2401, BgL_endz00_2402);
				}}}}
				{
					long BgL_iz00_2394;

					BgL_iz00_2394 = 0L;
				BgL_loopz00_2395:
					if ((BgL_iz00_2394 == -1L))
						{	/* Sync/failsafe.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Sync/failsafe.scm 16 */
							{	/* Sync/failsafe.scm 16 */
								obj_t BgL_arg1798z00_2396;

								{	/* Sync/failsafe.scm 16 */

									{	/* Sync/failsafe.scm 16 */
										obj_t BgL_locationz00_2398;

										BgL_locationz00_2398 = BBOOL(((bool_t) 0));
										{	/* Sync/failsafe.scm 16 */

											BgL_arg1798z00_2396 =
												BGl_readz00zz__readerz00(BgL_cportz00_2393,
												BgL_locationz00_2398);
										}
									}
								}
								{	/* Sync/failsafe.scm 16 */
									int BgL_tmpz00_2535;

									BgL_tmpz00_2535 = (int) (BgL_iz00_2394);
									CNST_TABLE_SET(BgL_tmpz00_2535, BgL_arg1798z00_2396);
							}}
							{	/* Sync/failsafe.scm 16 */
								int BgL_auxz00_2399;

								BgL_auxz00_2399 = (int) ((BgL_iz00_2394 - 1L));
								{
									long BgL_iz00_2540;

									BgL_iz00_2540 = (long) (BgL_auxz00_2399);
									BgL_iz00_2394 = BgL_iz00_2540;
									goto BgL_loopz00_2395;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsync_failsafez00(void)
	{
		{	/* Sync/failsafe.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsync_failsafez00(void)
	{
		{	/* Sync/failsafe.scm 16 */
			return BUNSPEC;
		}

	}



/* failsafe-sync? */
	BGL_EXPORTED_DEF obj_t
		BGl_failsafezd2synczf3z21zzsync_failsafez00(BgL_syncz00_bglt BgL_nz00_17)
	{
		{	/* Sync/failsafe.scm 45 */
			if (CBOOL(BGl_za2optimzd2synczd2failsafezf3za2zf3zzengine_paramz00))
				{	/* Sync/failsafe.scm 46 */
					return
						BGl_failsafezf3zf3zzsync_failsafez00(
						(((BgL_syncz00_bglt) COBJECT(BgL_nz00_17))->BgL_bodyz00), BNIL);
				}
			else
				{	/* Sync/failsafe.scm 46 */
					return BFALSE;
				}
		}

	}



/* &failsafe-sync? */
	obj_t BGl_z62failsafezd2synczf3z43zzsync_failsafez00(obj_t BgL_envz00_2296,
		obj_t BgL_nz00_2297)
	{
		{	/* Sync/failsafe.scm 45 */
			return
				BGl_failsafezd2synczf3z21zzsync_failsafez00(
				((BgL_syncz00_bglt) BgL_nz00_2297));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsync_failsafez00(void)
	{
		{	/* Sync/failsafe.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsync_failsafez00(void)
	{
		{	/* Sync/failsafe.scm 16 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00,
				BGl_proc1764z00zzsync_failsafez00, BGl_nodez00zzast_nodez00,
				BGl_string1765z00zzsync_failsafez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_failsafezd2funzf3zd2envzf3zzsync_failsafez00,
				BGl_proc1766z00zzsync_failsafez00, BGl_funz00zzast_varz00,
				BGl_string1767z00zzsync_failsafez00);
		}

	}



/* &failsafe-fun?1297 */
	obj_t BGl_z62failsafezd2funzf31297z43zzsync_failsafez00(obj_t BgL_envz00_2300,
		obj_t BgL_funz00_2301, obj_t BgL_varz00_2302, obj_t BgL_stkz00_2303)
	{
		{	/* Sync/failsafe.scm 102 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &failsafe?1283 */
	obj_t BGl_z62failsafezf31283z91zzsync_failsafez00(obj_t BgL_envz00_2304,
		obj_t BgL_nz00_2305, obj_t BgL_stkz00_2306)
	{
		{	/* Sync/failsafe.scm 53 */
			return BBOOL(((bool_t) 0));
		}

	}



/* failsafe? */
	obj_t BGl_failsafezf3zf3zzsync_failsafez00(BgL_nodez00_bglt BgL_nz00_18,
		obj_t BgL_stkz00_19)
	{
		{	/* Sync/failsafe.scm 53 */
			{	/* Sync/failsafe.scm 53 */
				obj_t BgL_method1284z00_1704;

				{	/* Sync/failsafe.scm 53 */
					obj_t BgL_res1755z00_2136;

					{	/* Sync/failsafe.scm 53 */
						long BgL_objzd2classzd2numz00_2107;

						BgL_objzd2classzd2numz00_2107 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_18));
						{	/* Sync/failsafe.scm 53 */
							obj_t BgL_arg1811z00_2108;

							BgL_arg1811z00_2108 =
								PROCEDURE_REF(BGl_failsafezf3zd2envz21zzsync_failsafez00,
								(int) (1L));
							{	/* Sync/failsafe.scm 53 */
								int BgL_offsetz00_2111;

								BgL_offsetz00_2111 = (int) (BgL_objzd2classzd2numz00_2107);
								{	/* Sync/failsafe.scm 53 */
									long BgL_offsetz00_2112;

									BgL_offsetz00_2112 =
										((long) (BgL_offsetz00_2111) - OBJECT_TYPE);
									{	/* Sync/failsafe.scm 53 */
										long BgL_modz00_2113;

										BgL_modz00_2113 =
											(BgL_offsetz00_2112 >> (int) ((long) ((int) (4L))));
										{	/* Sync/failsafe.scm 53 */
											long BgL_restz00_2115;

											BgL_restz00_2115 =
												(BgL_offsetz00_2112 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Sync/failsafe.scm 53 */

												{	/* Sync/failsafe.scm 53 */
													obj_t BgL_bucketz00_2117;

													BgL_bucketz00_2117 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2108), BgL_modz00_2113);
													BgL_res1755z00_2136 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2117), BgL_restz00_2115);
					}}}}}}}}
					BgL_method1284z00_1704 = BgL_res1755z00_2136;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1284z00_1704,
					((obj_t) BgL_nz00_18), BgL_stkz00_19);
			}
		}

	}



/* &failsafe? */
	obj_t BGl_z62failsafezf3z91zzsync_failsafez00(obj_t BgL_envz00_2307,
		obj_t BgL_nz00_2308, obj_t BgL_stkz00_2309)
	{
		{	/* Sync/failsafe.scm 53 */
			return
				BGl_failsafezf3zf3zzsync_failsafez00(
				((BgL_nodez00_bglt) BgL_nz00_2308), BgL_stkz00_2309);
		}

	}



/* failsafe-fun? */
	obj_t BGl_failsafezd2funzf3z21zzsync_failsafez00(BgL_funz00_bglt
		BgL_funz00_32, BgL_variablez00_bglt BgL_varz00_33, obj_t BgL_stkz00_34)
	{
		{	/* Sync/failsafe.scm 102 */
			{	/* Sync/failsafe.scm 102 */
				obj_t BgL_method1298z00_1705;

				{	/* Sync/failsafe.scm 102 */
					obj_t BgL_res1760z00_2167;

					{	/* Sync/failsafe.scm 102 */
						long BgL_objzd2classzd2numz00_2138;

						BgL_objzd2classzd2numz00_2138 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_32));
						{	/* Sync/failsafe.scm 102 */
							obj_t BgL_arg1811z00_2139;

							BgL_arg1811z00_2139 =
								PROCEDURE_REF(BGl_failsafezd2funzf3zd2envzf3zzsync_failsafez00,
								(int) (1L));
							{	/* Sync/failsafe.scm 102 */
								int BgL_offsetz00_2142;

								BgL_offsetz00_2142 = (int) (BgL_objzd2classzd2numz00_2138);
								{	/* Sync/failsafe.scm 102 */
									long BgL_offsetz00_2143;

									BgL_offsetz00_2143 =
										((long) (BgL_offsetz00_2142) - OBJECT_TYPE);
									{	/* Sync/failsafe.scm 102 */
										long BgL_modz00_2144;

										BgL_modz00_2144 =
											(BgL_offsetz00_2143 >> (int) ((long) ((int) (4L))));
										{	/* Sync/failsafe.scm 102 */
											long BgL_restz00_2146;

											BgL_restz00_2146 =
												(BgL_offsetz00_2143 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Sync/failsafe.scm 102 */

												{	/* Sync/failsafe.scm 102 */
													obj_t BgL_bucketz00_2148;

													BgL_bucketz00_2148 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2139), BgL_modz00_2144);
													BgL_res1760z00_2167 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2148), BgL_restz00_2146);
					}}}}}}}}
					BgL_method1298z00_1705 = BgL_res1760z00_2167;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1298z00_1705,
					((obj_t) BgL_funz00_32), ((obj_t) BgL_varz00_33), BgL_stkz00_34);
			}
		}

	}



/* &failsafe-fun? */
	obj_t BGl_z62failsafezd2funzf3z43zzsync_failsafez00(obj_t BgL_envz00_2310,
		obj_t BgL_funz00_2311, obj_t BgL_varz00_2312, obj_t BgL_stkz00_2313)
	{
		{	/* Sync/failsafe.scm 102 */
			return
				BGl_failsafezd2funzf3z21zzsync_failsafez00(
				((BgL_funz00_bglt) BgL_funz00_2311),
				((BgL_variablez00_bglt) BgL_varz00_2312), BgL_stkz00_2313);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsync_failsafez00(void)
	{
		{	/* Sync/failsafe.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_atomz00zzast_nodez00,
				BGl_proc1768z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_varz00zzast_nodez00,
				BGl_proc1770z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_kwotez00zzast_nodez00,
				BGl_proc1771z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00,
				BGl_sequencez00zzast_nodez00, BGl_proc1772z00zzsync_failsafez00,
				BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_syncz00zzast_nodez00,
				BGl_proc1773z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_appz00zzast_nodez00,
				BGl_proc1774z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezd2funzf3zd2envzf3zzsync_failsafez00,
				BGl_sfunz00zzast_varz00, BGl_proc1775z00zzsync_failsafez00,
				BGl_string1776z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezd2funzf3zd2envzf3zzsync_failsafez00,
				BGl_cfunz00zzast_varz00, BGl_proc1777z00zzsync_failsafez00,
				BGl_string1776z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_externz00zzast_nodez00,
				BGl_proc1778z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_pragmaz00zzast_nodez00,
				BGl_proc1779z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_castz00zzast_nodez00,
				BGl_proc1780z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_setqz00zzast_nodez00,
				BGl_proc1781z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1782z00zzsync_failsafez00,
				BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00, BGl_switchz00zzast_nodez00,
				BGl_proc1783z00zzsync_failsafez00, BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1784z00zzsync_failsafez00,
				BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1785z00zzsync_failsafez00,
				BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1786z00zzsync_failsafez00,
				BGl_string1769z00zzsync_failsafez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1787z00zzsync_failsafez00,
				BGl_string1769z00zzsync_failsafez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_failsafezf3zd2envz21zzsync_failsafez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1788z00zzsync_failsafez00,
				BGl_string1769z00zzsync_failsafez00);
		}

	}



/* &failsafe?-box-set!1324 */
	obj_t BGl_z62failsafezf3zd2boxzd2setz121324z83zzsync_failsafez00(obj_t
		BgL_envz00_2333, obj_t BgL_nz00_2334, obj_t BgL_stkz00_2335)
	{
		{	/* Sync/failsafe.scm 216 */
			return
				BGl_failsafezf3zf3zzsync_failsafez00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nz00_2334)))->BgL_valuez00),
				BgL_stkz00_2335);
		}

	}



/* &failsafe?-box-ref1322 */
	obj_t BGl_z62failsafezf3zd2boxzd2ref1322z91zzsync_failsafez00(obj_t
		BgL_envz00_2336, obj_t BgL_nz00_2337, obj_t BgL_stkz00_2338)
	{
		{	/* Sync/failsafe.scm 210 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &failsafe?-make-box1320 */
	obj_t BGl_z62failsafezf3zd2makezd2box1320z91zzsync_failsafez00(obj_t
		BgL_envz00_2339, obj_t BgL_nz00_2340, obj_t BgL_stkz00_2341)
	{
		{	/* Sync/failsafe.scm 203 */
			return
				BGl_failsafezf3zf3zzsync_failsafez00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nz00_2340)))->BgL_valuez00),
				BgL_stkz00_2341);
		}

	}



/* &failsafe?-let-var1318 */
	obj_t BGl_z62failsafezf3zd2letzd2var1318z91zzsync_failsafez00(obj_t
		BgL_envz00_2342, obj_t BgL_nz00_2343, obj_t BgL_stkz00_2344)
	{
		{	/* Sync/failsafe.scm 195 */
			{	/* Sync/failsafe.scm 196 */
				bool_t BgL_tmpz00_2648;

				{	/* Sync/failsafe.scm 197 */
					bool_t BgL_test1833z00_2649;

					{	/* Sync/failsafe.scm 197 */
						BgL_nodez00_bglt BgL_arg1575z00_2412;

						BgL_arg1575z00_2412 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nz00_2343)))->BgL_bodyz00);
						BgL_test1833z00_2649 =
							CBOOL(BGl_failsafezf3zf3zzsync_failsafez00(BgL_arg1575z00_2412,
								BgL_stkz00_2344));
					}
					if (BgL_test1833z00_2649)
						{	/* Sync/failsafe.scm 198 */
							obj_t BgL_g1281z00_2413;

							BgL_g1281z00_2413 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nz00_2343)))->BgL_bindingsz00);
							{
								obj_t BgL_l1279z00_2415;

								BgL_l1279z00_2415 = BgL_g1281z00_2413;
							BgL_zc3z04anonymousza31566ze3z87_2414:
								if (NULLP(BgL_l1279z00_2415))
									{	/* Sync/failsafe.scm 198 */
										BgL_tmpz00_2648 = ((bool_t) 1);
									}
								else
									{	/* Sync/failsafe.scm 198 */
										obj_t BgL_nvz00_2416;

										{	/* Sync/failsafe.scm 198 */
											obj_t BgL_bz00_2417;

											BgL_bz00_2417 = CAR(((obj_t) BgL_l1279z00_2415));
											{	/* Sync/failsafe.scm 198 */
												obj_t BgL_arg1573z00_2418;

												BgL_arg1573z00_2418 = CDR(((obj_t) BgL_bz00_2417));
												BgL_nvz00_2416 =
													BGl_failsafezf3zf3zzsync_failsafez00(
													((BgL_nodez00_bglt) BgL_arg1573z00_2418),
													BgL_stkz00_2344);
											}
										}
										if (CBOOL(BgL_nvz00_2416))
											{	/* Sync/failsafe.scm 198 */
												obj_t BgL_arg1571z00_2419;

												BgL_arg1571z00_2419 = CDR(((obj_t) BgL_l1279z00_2415));
												{
													obj_t BgL_l1279z00_2668;

													BgL_l1279z00_2668 = BgL_arg1571z00_2419;
													BgL_l1279z00_2415 = BgL_l1279z00_2668;
													goto BgL_zc3z04anonymousza31566ze3z87_2414;
												}
											}
										else
											{	/* Sync/failsafe.scm 198 */
												BgL_tmpz00_2648 = ((bool_t) 0);
											}
									}
							}
						}
					else
						{	/* Sync/failsafe.scm 197 */
							BgL_tmpz00_2648 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_2648);
			}
		}

	}



/* &failsafe?-let-fun1316 */
	obj_t BGl_z62failsafezf3zd2letzd2fun1316z91zzsync_failsafez00(obj_t
		BgL_envz00_2345, obj_t BgL_nz00_2346, obj_t BgL_stkz00_2347)
	{
		{	/* Sync/failsafe.scm 187 */
			return
				BGl_failsafezf3zf3zzsync_failsafez00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_2346)))->BgL_bodyz00),
				BgL_stkz00_2347);
		}

	}



/* &failsafe?-switch1314 */
	obj_t BGl_z62failsafezf3zd2switch1314z43zzsync_failsafez00(obj_t
		BgL_envz00_2348, obj_t BgL_nz00_2349, obj_t BgL_stkz00_2350)
	{
		{	/* Sync/failsafe.scm 179 */
			{	/* Sync/failsafe.scm 180 */
				bool_t BgL_tmpz00_2673;

				{	/* Sync/failsafe.scm 181 */
					bool_t BgL_test1836z00_2674;

					{	/* Sync/failsafe.scm 181 */
						BgL_nodez00_bglt BgL_arg1559z00_2422;

						BgL_arg1559z00_2422 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nz00_2349)))->BgL_testz00);
						BgL_test1836z00_2674 =
							CBOOL(BGl_failsafezf3zf3zzsync_failsafez00(BgL_arg1559z00_2422,
								BgL_stkz00_2350));
					}
					if (BgL_test1836z00_2674)
						{	/* Sync/failsafe.scm 182 */
							obj_t BgL_g1277z00_2423;

							BgL_g1277z00_2423 =
								(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nz00_2349)))->BgL_clausesz00);
							{
								obj_t BgL_l1275z00_2425;

								BgL_l1275z00_2425 = BgL_g1277z00_2423;
							BgL_zc3z04anonymousza31548ze3z87_2424:
								if (NULLP(BgL_l1275z00_2425))
									{	/* Sync/failsafe.scm 182 */
										BgL_tmpz00_2673 = ((bool_t) 1);
									}
								else
									{	/* Sync/failsafe.scm 182 */
										obj_t BgL_nvz00_2426;

										{	/* Sync/failsafe.scm 182 */
											obj_t BgL_cz00_2427;

											BgL_cz00_2427 = CAR(((obj_t) BgL_l1275z00_2425));
											{	/* Sync/failsafe.scm 182 */
												obj_t BgL_arg1553z00_2428;

												BgL_arg1553z00_2428 = CDR(((obj_t) BgL_cz00_2427));
												BgL_nvz00_2426 =
													BGl_failsafezf3zf3zzsync_failsafez00(
													((BgL_nodez00_bglt) BgL_arg1553z00_2428),
													BgL_stkz00_2350);
											}
										}
										if (CBOOL(BgL_nvz00_2426))
											{	/* Sync/failsafe.scm 182 */
												obj_t BgL_arg1552z00_2429;

												BgL_arg1552z00_2429 = CDR(((obj_t) BgL_l1275z00_2425));
												{
													obj_t BgL_l1275z00_2693;

													BgL_l1275z00_2693 = BgL_arg1552z00_2429;
													BgL_l1275z00_2425 = BgL_l1275z00_2693;
													goto BgL_zc3z04anonymousza31548ze3z87_2424;
												}
											}
										else
											{	/* Sync/failsafe.scm 182 */
												BgL_tmpz00_2673 = ((bool_t) 0);
											}
									}
							}
						}
					else
						{	/* Sync/failsafe.scm 181 */
							BgL_tmpz00_2673 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_2673);
			}
		}

	}



/* &failsafe?-conditiona1312 */
	obj_t BGl_z62failsafezf3zd2conditiona1312z43zzsync_failsafez00(obj_t
		BgL_envz00_2351, obj_t BgL_nz00_2352, obj_t BgL_stkz00_2353)
	{
		{	/* Sync/failsafe.scm 172 */
			{	/* Sync/failsafe.scm 174 */
				obj_t BgL__andtest_1126z00_2431;

				BgL__andtest_1126z00_2431 =
					BGl_failsafezf3zf3zzsync_failsafez00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_2352)))->BgL_testz00),
					BgL_stkz00_2353);
				if (CBOOL(BgL__andtest_1126z00_2431))
					{	/* Sync/failsafe.scm 174 */
						obj_t BgL__andtest_1127z00_2432;

						BgL__andtest_1127z00_2432 =
							BGl_failsafezf3zf3zzsync_failsafez00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nz00_2352)))->BgL_truez00),
							BgL_stkz00_2353);
						if (CBOOL(BgL__andtest_1127z00_2432))
							{	/* Sync/failsafe.scm 174 */
								return
									BGl_failsafezf3zf3zzsync_failsafez00(
									(((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nz00_2352)))->
										BgL_falsez00), BgL_stkz00_2353);
							}
						else
							{	/* Sync/failsafe.scm 174 */
								return BFALSE;
							}
					}
				else
					{	/* Sync/failsafe.scm 174 */
						return BFALSE;
					}
			}
		}

	}



/* &failsafe?-setq1310 */
	obj_t BGl_z62failsafezf3zd2setq1310z43zzsync_failsafez00(obj_t
		BgL_envz00_2354, obj_t BgL_nz00_2355, obj_t BgL_stkz00_2356)
	{
		{	/* Sync/failsafe.scm 165 */
			return
				BGl_failsafezf3zf3zzsync_failsafez00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nz00_2355)))->BgL_valuez00),
				BgL_stkz00_2356);
		}

	}



/* &failsafe?-cast1308 */
	obj_t BGl_z62failsafezf3zd2cast1308z43zzsync_failsafez00(obj_t
		BgL_envz00_2357, obj_t BgL_nz00_2358, obj_t BgL_stkz00_2359)
	{
		{	/* Sync/failsafe.scm 158 */
			return
				BGl_failsafezf3zf3zzsync_failsafez00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nz00_2358)))->BgL_argz00),
				BgL_stkz00_2359);
		}

	}



/* &failsafe?-pragma1306 */
	obj_t BGl_z62failsafezf3zd2pragma1306z43zzsync_failsafez00(obj_t
		BgL_envz00_2360, obj_t BgL_nz00_2361, obj_t BgL_stkz00_2362)
	{
		{	/* Sync/failsafe.scm 152 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &failsafe?-extern1304 */
	obj_t BGl_z62failsafezf3zd2extern1304z43zzsync_failsafez00(obj_t
		BgL_envz00_2363, obj_t BgL_nz00_2364, obj_t BgL_stkz00_2365)
	{
		{	/* Sync/failsafe.scm 145 */
			{	/* Sync/failsafe.scm 146 */
				bool_t BgL_tmpz00_2715;

				{	/* Sync/failsafe.scm 147 */
					obj_t BgL_g1273z00_2437;

					BgL_g1273z00_2437 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nz00_2364)))->BgL_exprza2za2);
					{
						obj_t BgL_l1271z00_2439;

						BgL_l1271z00_2439 = BgL_g1273z00_2437;
					BgL_zc3z04anonymousza31510ze3z87_2438:
						if (NULLP(BgL_l1271z00_2439))
							{	/* Sync/failsafe.scm 147 */
								BgL_tmpz00_2715 = ((bool_t) 1);
							}
						else
							{	/* Sync/failsafe.scm 147 */
								obj_t BgL_nvz00_2440;

								{	/* Sync/failsafe.scm 147 */
									obj_t BgL_nz00_2441;

									BgL_nz00_2441 = CAR(((obj_t) BgL_l1271z00_2439));
									BgL_nvz00_2440 =
										BGl_failsafezf3zf3zzsync_failsafez00(
										((BgL_nodez00_bglt) BgL_nz00_2441), BgL_stkz00_2365);
								}
								if (CBOOL(BgL_nvz00_2440))
									{	/* Sync/failsafe.scm 147 */
										obj_t BgL_arg1513z00_2442;

										BgL_arg1513z00_2442 = CDR(((obj_t) BgL_l1271z00_2439));
										{
											obj_t BgL_l1271z00_2728;

											BgL_l1271z00_2728 = BgL_arg1513z00_2442;
											BgL_l1271z00_2439 = BgL_l1271z00_2728;
											goto BgL_zc3z04anonymousza31510ze3z87_2438;
										}
									}
								else
									{	/* Sync/failsafe.scm 147 */
										BgL_tmpz00_2715 = ((bool_t) 0);
									}
							}
					}
				}
				return BBOOL(BgL_tmpz00_2715);
			}
		}

	}



/* &failsafe-fun?-cfun1302 */
	obj_t BGl_z62failsafezd2funzf3zd2cfun1302z91zzsync_failsafez00(obj_t
		BgL_envz00_2366, obj_t BgL_funz00_2367, obj_t BgL_varz00_2368,
		obj_t BgL_stkz00_2369)
	{
		{	/* Sync/failsafe.scm 130 */
			{	/* Sync/failsafe.scm 133 */
				bool_t BgL_test1843z00_2730;

				{	/* Sync/failsafe.scm 133 */
					obj_t BgL_tmpz00_2731;

					BgL_tmpz00_2731 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_cfunz00_bglt) BgL_funz00_2367))))->BgL_failsafez00);
					BgL_test1843z00_2730 = BOOLEANP(BgL_tmpz00_2731);
				}
				if (BgL_test1843z00_2730)
					{	/* Sync/failsafe.scm 133 */
						return
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt)
										((BgL_cfunz00_bglt) BgL_funz00_2367))))->BgL_failsafez00);
					}
				else
					{	/* Sync/failsafe.scm 135 */
						bool_t BgL_test1844z00_2739;

						{	/* Sync/failsafe.scm 135 */
							obj_t BgL_classz00_2445;

							BgL_classz00_2445 = BGl_globalz00zzast_varz00;
							{	/* Sync/failsafe.scm 135 */
								BgL_objectz00_bglt BgL_arg1807z00_2446;

								{	/* Sync/failsafe.scm 135 */
									obj_t BgL_tmpz00_2740;

									BgL_tmpz00_2740 =
										((obj_t) ((BgL_variablez00_bglt) BgL_varz00_2368));
									BgL_arg1807z00_2446 = (BgL_objectz00_bglt) (BgL_tmpz00_2740);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Sync/failsafe.scm 135 */
										long BgL_idxz00_2447;

										BgL_idxz00_2447 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2446);
										BgL_test1844z00_2739 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2447 + 2L)) == BgL_classz00_2445);
									}
								else
									{	/* Sync/failsafe.scm 135 */
										bool_t BgL_res1762z00_2450;

										{	/* Sync/failsafe.scm 135 */
											obj_t BgL_oclassz00_2451;

											{	/* Sync/failsafe.scm 135 */
												obj_t BgL_arg1815z00_2452;
												long BgL_arg1816z00_2453;

												BgL_arg1815z00_2452 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Sync/failsafe.scm 135 */
													long BgL_arg1817z00_2454;

													BgL_arg1817z00_2454 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2446);
													BgL_arg1816z00_2453 =
														(BgL_arg1817z00_2454 - OBJECT_TYPE);
												}
												BgL_oclassz00_2451 =
													VECTOR_REF(BgL_arg1815z00_2452, BgL_arg1816z00_2453);
											}
											{	/* Sync/failsafe.scm 135 */
												bool_t BgL__ortest_1115z00_2455;

												BgL__ortest_1115z00_2455 =
													(BgL_classz00_2445 == BgL_oclassz00_2451);
												if (BgL__ortest_1115z00_2455)
													{	/* Sync/failsafe.scm 135 */
														BgL_res1762z00_2450 = BgL__ortest_1115z00_2455;
													}
												else
													{	/* Sync/failsafe.scm 135 */
														long BgL_odepthz00_2456;

														{	/* Sync/failsafe.scm 135 */
															obj_t BgL_arg1804z00_2457;

															BgL_arg1804z00_2457 = (BgL_oclassz00_2451);
															BgL_odepthz00_2456 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2457);
														}
														if ((2L < BgL_odepthz00_2456))
															{	/* Sync/failsafe.scm 135 */
																obj_t BgL_arg1802z00_2458;

																{	/* Sync/failsafe.scm 135 */
																	obj_t BgL_arg1803z00_2459;

																	BgL_arg1803z00_2459 = (BgL_oclassz00_2451);
																	BgL_arg1802z00_2458 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2459,
																		2L);
																}
																BgL_res1762z00_2450 =
																	(BgL_arg1802z00_2458 == BgL_classz00_2445);
															}
														else
															{	/* Sync/failsafe.scm 135 */
																BgL_res1762z00_2450 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1844z00_2739 = BgL_res1762z00_2450;
									}
							}
						}
						if (BgL_test1844z00_2739)
							{	/* Sync/failsafe.scm 135 */
								{
									obj_t BgL_auxz00_2763;

									{	/* Sync/failsafe.scm 137 */
										bool_t BgL_tmpz00_2766;

										{	/* Sync/failsafe.scm 137 */
											obj_t BgL_tmpz00_2767;

											BgL_tmpz00_2767 =
												BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF
												(0),
												(((BgL_globalz00_bglt)
														COBJECT(((BgL_globalz00_bglt) (
																	(BgL_variablez00_bglt) BgL_varz00_2368))))->
													BgL_pragmaz00));
											BgL_tmpz00_2766 = PAIRP(BgL_tmpz00_2767);
										}
										BgL_auxz00_2763 = BBOOL(BgL_tmpz00_2766);
									}
									((((BgL_funz00_bglt) COBJECT(
													((BgL_funz00_bglt)
														((BgL_cfunz00_bglt) BgL_funz00_2367))))->
											BgL_failsafez00) = ((obj_t) BgL_auxz00_2763), BUNSPEC);
								}
								return
									(((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt)
												((BgL_cfunz00_bglt) BgL_funz00_2367))))->
									BgL_failsafez00);
							}
						else
							{	/* Sync/failsafe.scm 135 */
								return BFALSE;
							}
					}
			}
		}

	}



/* &failsafe-fun?-sfun1300 */
	obj_t BGl_z62failsafezd2funzf3zd2sfun1300z91zzsync_failsafez00(obj_t
		BgL_envz00_2370, obj_t BgL_funz00_2371, obj_t BgL_varz00_2372,
		obj_t BgL_stkz00_2373)
	{
		{	/* Sync/failsafe.scm 108 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
						((obj_t)
							((BgL_variablez00_bglt) BgL_varz00_2372)), BgL_stkz00_2373)))
				{	/* Sync/failsafe.scm 111 */
					return BTRUE;
				}
			else
				{	/* Sync/failsafe.scm 113 */
					bool_t BgL_test1849z00_2784;

					{	/* Sync/failsafe.scm 113 */
						obj_t BgL_tmpz00_2785;

						BgL_tmpz00_2785 =
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt)
										((BgL_sfunz00_bglt) BgL_funz00_2371))))->BgL_failsafez00);
						BgL_test1849z00_2784 = BOOLEANP(BgL_tmpz00_2785);
					}
					if (BgL_test1849z00_2784)
						{	/* Sync/failsafe.scm 113 */
							return
								(((BgL_funz00_bglt) COBJECT(
										((BgL_funz00_bglt)
											((BgL_sfunz00_bglt) BgL_funz00_2371))))->BgL_failsafez00);
						}
					else
						{	/* Sync/failsafe.scm 115 */
							bool_t BgL_test1850z00_2793;

							{	/* Sync/failsafe.scm 115 */
								bool_t BgL_test1851z00_2794;

								{	/* Sync/failsafe.scm 115 */
									obj_t BgL_classz00_2462;

									BgL_classz00_2462 = BGl_globalz00zzast_varz00;
									{	/* Sync/failsafe.scm 115 */
										BgL_objectz00_bglt BgL_arg1807z00_2463;

										{	/* Sync/failsafe.scm 115 */
											obj_t BgL_tmpz00_2795;

											BgL_tmpz00_2795 =
												((obj_t) ((BgL_variablez00_bglt) BgL_varz00_2372));
											BgL_arg1807z00_2463 =
												(BgL_objectz00_bglt) (BgL_tmpz00_2795);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Sync/failsafe.scm 115 */
												long BgL_idxz00_2464;

												BgL_idxz00_2464 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2463);
												BgL_test1851z00_2794 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2464 + 2L)) == BgL_classz00_2462);
											}
										else
											{	/* Sync/failsafe.scm 115 */
												bool_t BgL_res1761z00_2467;

												{	/* Sync/failsafe.scm 115 */
													obj_t BgL_oclassz00_2468;

													{	/* Sync/failsafe.scm 115 */
														obj_t BgL_arg1815z00_2469;
														long BgL_arg1816z00_2470;

														BgL_arg1815z00_2469 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Sync/failsafe.scm 115 */
															long BgL_arg1817z00_2471;

															BgL_arg1817z00_2471 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2463);
															BgL_arg1816z00_2470 =
																(BgL_arg1817z00_2471 - OBJECT_TYPE);
														}
														BgL_oclassz00_2468 =
															VECTOR_REF(BgL_arg1815z00_2469,
															BgL_arg1816z00_2470);
													}
													{	/* Sync/failsafe.scm 115 */
														bool_t BgL__ortest_1115z00_2472;

														BgL__ortest_1115z00_2472 =
															(BgL_classz00_2462 == BgL_oclassz00_2468);
														if (BgL__ortest_1115z00_2472)
															{	/* Sync/failsafe.scm 115 */
																BgL_res1761z00_2467 = BgL__ortest_1115z00_2472;
															}
														else
															{	/* Sync/failsafe.scm 115 */
																long BgL_odepthz00_2473;

																{	/* Sync/failsafe.scm 115 */
																	obj_t BgL_arg1804z00_2474;

																	BgL_arg1804z00_2474 = (BgL_oclassz00_2468);
																	BgL_odepthz00_2473 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2474);
																}
																if ((2L < BgL_odepthz00_2473))
																	{	/* Sync/failsafe.scm 115 */
																		obj_t BgL_arg1802z00_2475;

																		{	/* Sync/failsafe.scm 115 */
																			obj_t BgL_arg1803z00_2476;

																			BgL_arg1803z00_2476 =
																				(BgL_oclassz00_2468);
																			BgL_arg1802z00_2475 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2476, 2L);
																		}
																		BgL_res1761z00_2467 =
																			(BgL_arg1802z00_2475 ==
																			BgL_classz00_2462);
																	}
																else
																	{	/* Sync/failsafe.scm 115 */
																		BgL_res1761z00_2467 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1851z00_2794 = BgL_res1761z00_2467;
											}
									}
								}
								if (BgL_test1851z00_2794)
									{	/* Sync/failsafe.scm 115 */
										bool_t BgL_test1855z00_2818;

										{	/* Sync/failsafe.scm 115 */
											obj_t BgL_arg1472z00_2477;

											BgL_arg1472z00_2477 =
												(((BgL_globalz00_bglt) COBJECT(
														((BgL_globalz00_bglt)
															((BgL_variablez00_bglt) BgL_varz00_2372))))->
												BgL_modulez00);
											BgL_test1855z00_2818 =
												(BgL_arg1472z00_2477 ==
												BGl_za2moduleza2z00zzmodule_modulez00);
										}
										if (BgL_test1855z00_2818)
											{	/* Sync/failsafe.scm 115 */
												BgL_test1850z00_2793 = ((bool_t) 0);
											}
										else
											{	/* Sync/failsafe.scm 115 */
												BgL_test1850z00_2793 = ((bool_t) 1);
											}
									}
								else
									{	/* Sync/failsafe.scm 115 */
										BgL_test1850z00_2793 = ((bool_t) 0);
									}
							}
							if (BgL_test1850z00_2793)
								{	/* Sync/failsafe.scm 115 */
									{
										obj_t BgL_auxz00_2823;

										{	/* Sync/failsafe.scm 118 */
											bool_t BgL_tmpz00_2826;

											{	/* Sync/failsafe.scm 118 */
												obj_t BgL_tmpz00_2827;

												BgL_tmpz00_2827 =
													BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(CNST_TABLE_REF(0),
													(((BgL_globalz00_bglt)
															COBJECT(((BgL_globalz00_bglt) (
																		(BgL_variablez00_bglt) BgL_varz00_2372))))->
														BgL_pragmaz00));
												BgL_tmpz00_2826 = PAIRP(BgL_tmpz00_2827);
											}
											BgL_auxz00_2823 = BBOOL(BgL_tmpz00_2826);
										}
										((((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt)
															((BgL_sfunz00_bglt) BgL_funz00_2371))))->
												BgL_failsafez00) = ((obj_t) BgL_auxz00_2823), BUNSPEC);
									}
									return
										(((BgL_funz00_bglt) COBJECT(
												((BgL_funz00_bglt)
													((BgL_sfunz00_bglt) BgL_funz00_2371))))->
										BgL_failsafez00);
								}
							else
								{	/* Sync/failsafe.scm 122 */
									obj_t BgL_fsafez00_2478;

									{	/* Sync/failsafe.scm 122 */
										obj_t BgL_arg1453z00_2479;
										obj_t BgL_arg1454z00_2480;

										BgL_arg1453z00_2479 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_2371)))->BgL_bodyz00);
										BgL_arg1454z00_2480 =
											MAKE_YOUNG_PAIR(
											((obj_t)
												((BgL_variablez00_bglt) BgL_varz00_2372)),
											BgL_stkz00_2373);
										BgL_fsafez00_2478 =
											BGl_failsafezf3zf3zzsync_failsafez00(((BgL_nodez00_bglt)
												BgL_arg1453z00_2479), BgL_arg1454z00_2480);
									}
									if (NULLP(BgL_stkz00_2373))
										{	/* Sync/failsafe.scm 124 */
											((((BgL_funz00_bglt) COBJECT(
															((BgL_funz00_bglt)
																((BgL_sfunz00_bglt) BgL_funz00_2371))))->
													BgL_failsafez00) =
												((obj_t) BgL_fsafez00_2478), BUNSPEC);
										}
									else
										{	/* Sync/failsafe.scm 124 */
											BFALSE;
										}
									return BgL_fsafez00_2478;
								}
						}
				}
		}

	}



/* &failsafe?-app1296 */
	obj_t BGl_z62failsafezf3zd2app1296z43zzsync_failsafez00(obj_t BgL_envz00_2374,
		obj_t BgL_nz00_2375, obj_t BgL_stkz00_2376)
	{
		{	/* Sync/failsafe.scm 93 */
			{	/* Sync/failsafe.scm 94 */
				bool_t BgL_tmpz00_2851;

				{	/* Sync/failsafe.scm 95 */
					BgL_variablez00_bglt BgL_vz00_2482;

					BgL_vz00_2482 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nz00_2375)))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Sync/failsafe.scm 96 */
						bool_t BgL_test1857z00_2855;

						{	/* Sync/failsafe.scm 96 */
							BgL_valuez00_bglt BgL_arg1376z00_2483;

							BgL_arg1376z00_2483 =
								(((BgL_variablez00_bglt) COBJECT(BgL_vz00_2482))->BgL_valuez00);
							BgL_test1857z00_2855 =
								CBOOL(BGl_failsafezd2funzf3z21zzsync_failsafez00(
									((BgL_funz00_bglt) BgL_arg1376z00_2483), BgL_vz00_2482,
									BgL_stkz00_2376));
						}
						if (BgL_test1857z00_2855)
							{	/* Sync/failsafe.scm 97 */
								obj_t BgL_g1269z00_2484;

								BgL_g1269z00_2484 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nz00_2375)))->BgL_argsz00);
								{
									obj_t BgL_l1267z00_2486;

									BgL_l1267z00_2486 = BgL_g1269z00_2484;
								BgL_zc3z04anonymousza31369ze3z87_2485:
									if (NULLP(BgL_l1267z00_2486))
										{	/* Sync/failsafe.scm 97 */
											BgL_tmpz00_2851 = ((bool_t) 1);
										}
									else
										{	/* Sync/failsafe.scm 97 */
											obj_t BgL_nvz00_2487;

											{	/* Sync/failsafe.scm 97 */
												obj_t BgL_nz00_2488;

												BgL_nz00_2488 = CAR(((obj_t) BgL_l1267z00_2486));
												BgL_nvz00_2487 =
													BGl_failsafezf3zf3zzsync_failsafez00(
													((BgL_nodez00_bglt) BgL_nz00_2488), BgL_stkz00_2376);
											}
											if (CBOOL(BgL_nvz00_2487))
												{	/* Sync/failsafe.scm 97 */
													obj_t BgL_arg1375z00_2489;

													BgL_arg1375z00_2489 =
														CDR(((obj_t) BgL_l1267z00_2486));
													{
														obj_t BgL_l1267z00_2872;

														BgL_l1267z00_2872 = BgL_arg1375z00_2489;
														BgL_l1267z00_2486 = BgL_l1267z00_2872;
														goto BgL_zc3z04anonymousza31369ze3z87_2485;
													}
												}
											else
												{	/* Sync/failsafe.scm 97 */
													BgL_tmpz00_2851 = ((bool_t) 0);
												}
										}
								}
							}
						else
							{	/* Sync/failsafe.scm 96 */
								BgL_tmpz00_2851 = ((bool_t) 0);
							}
					}
				}
				return BBOOL(BgL_tmpz00_2851);
			}
		}

	}



/* &failsafe?-sync1294 */
	obj_t BGl_z62failsafezf3zd2sync1294z43zzsync_failsafez00(obj_t
		BgL_envz00_2377, obj_t BgL_nz00_2378, obj_t BgL_stkz00_2379)
	{
		{	/* Sync/failsafe.scm 84 */
			{	/* Sync/failsafe.scm 86 */
				obj_t BgL__andtest_1114z00_2491;

				BgL__andtest_1114z00_2491 =
					BGl_failsafezf3zf3zzsync_failsafez00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_2378)))->BgL_mutexz00),
					BgL_stkz00_2379);
				if (CBOOL(BgL__andtest_1114z00_2491))
					{	/* Sync/failsafe.scm 87 */
						obj_t BgL__andtest_1115z00_2492;

						BgL__andtest_1115z00_2492 =
							BGl_failsafezf3zf3zzsync_failsafez00(
							(((BgL_syncz00_bglt) COBJECT(
										((BgL_syncz00_bglt) BgL_nz00_2378)))->BgL_prelockz00),
							BgL_stkz00_2379);
						if (CBOOL(BgL__andtest_1115z00_2492))
							{	/* Sync/failsafe.scm 87 */
								return
									BGl_failsafezf3zf3zzsync_failsafez00(
									(((BgL_syncz00_bglt) COBJECT(
												((BgL_syncz00_bglt) BgL_nz00_2378)))->BgL_bodyz00),
									BgL_stkz00_2379);
							}
						else
							{	/* Sync/failsafe.scm 87 */
								return BFALSE;
							}
					}
				else
					{	/* Sync/failsafe.scm 86 */
						return BFALSE;
					}
			}
		}

	}



/* &failsafe?-sequence1292 */
	obj_t BGl_z62failsafezf3zd2sequence1292z43zzsync_failsafez00(obj_t
		BgL_envz00_2380, obj_t BgL_nz00_2381, obj_t BgL_stkz00_2382)
	{
		{	/* Sync/failsafe.scm 77 */
			{	/* Sync/failsafe.scm 78 */
				bool_t BgL_tmpz00_2887;

				{	/* Sync/failsafe.scm 79 */
					obj_t BgL_g1265z00_2494;

					BgL_g1265z00_2494 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nz00_2381)))->BgL_nodesz00);
					{
						obj_t BgL_l1263z00_2496;

						BgL_l1263z00_2496 = BgL_g1265z00_2494;
					BgL_zc3z04anonymousza31349ze3z87_2495:
						if (NULLP(BgL_l1263z00_2496))
							{	/* Sync/failsafe.scm 79 */
								BgL_tmpz00_2887 = ((bool_t) 1);
							}
						else
							{	/* Sync/failsafe.scm 79 */
								obj_t BgL_nvz00_2497;

								{	/* Sync/failsafe.scm 79 */
									obj_t BgL_nz00_2498;

									BgL_nz00_2498 = CAR(((obj_t) BgL_l1263z00_2496));
									BgL_nvz00_2497 =
										BGl_failsafezf3zf3zzsync_failsafez00(
										((BgL_nodez00_bglt) BgL_nz00_2498), BgL_stkz00_2382);
								}
								if (CBOOL(BgL_nvz00_2497))
									{	/* Sync/failsafe.scm 79 */
										obj_t BgL_arg1351z00_2499;

										BgL_arg1351z00_2499 = CDR(((obj_t) BgL_l1263z00_2496));
										{
											obj_t BgL_l1263z00_2900;

											BgL_l1263z00_2900 = BgL_arg1351z00_2499;
											BgL_l1263z00_2496 = BgL_l1263z00_2900;
											goto BgL_zc3z04anonymousza31349ze3z87_2495;
										}
									}
								else
									{	/* Sync/failsafe.scm 79 */
										BgL_tmpz00_2887 = ((bool_t) 0);
									}
							}
					}
				}
				return BBOOL(BgL_tmpz00_2887);
			}
		}

	}



/* &failsafe?-kwote1290 */
	obj_t BGl_z62failsafezf3zd2kwote1290z43zzsync_failsafez00(obj_t
		BgL_envz00_2383, obj_t BgL_nz00_2384, obj_t BgL_stkz00_2385)
	{
		{	/* Sync/failsafe.scm 71 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &failsafe?-var1288 */
	obj_t BGl_z62failsafezf3zd2var1288z43zzsync_failsafez00(obj_t BgL_envz00_2386,
		obj_t BgL_nz00_2387, obj_t BgL_stkz00_2388)
	{
		{	/* Sync/failsafe.scm 65 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &failsafe?-atom1286 */
	obj_t BGl_z62failsafezf3zd2atom1286z43zzsync_failsafez00(obj_t
		BgL_envz00_2389, obj_t BgL_nz00_2390, obj_t BgL_stkz00_2391)
	{
		{	/* Sync/failsafe.scm 59 */
			return BBOOL(((bool_t) 1));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsync_failsafez00(void)
	{
		{	/* Sync/failsafe.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzast_appz00(449859288L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string1789z00zzsync_failsafez00));
		}

	}

#ifdef __cplusplus
}
#endif
