/*===========================================================================*/
/*   (Type/typeof.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Type/typeof.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TYPE_TYPEOF_TYPE_DEFINITIONS
#define BGL_TYPE_TYPEOF_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;


#endif													// BGL_TYPE_TYPEOF_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_typez00_bglt BGl_z62getzd2typezd2var1268z62zztype_typeofz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62getzd2type1265zb0zztype_typeofz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2symbolza2z00zztype_cachez00;
	extern obj_t BGl_za2uint32za2z00zztype_cachez00;
	extern obj_t BGl_za2uint16za2z00zztype_cachez00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static BgL_typez00_bglt
		BGl_z62getzd2typezd2sequence1270z62zztype_typeofz00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztype_typeofz00 = BUNSPEC;
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	extern obj_t BGl_za2bignumza2z00zztype_cachez00;
	static bool_t BGl_pairzd2nilzf3z21zztype_typeofz00(obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62getzd2typezd2atomz62zztype_typeofz00(obj_t,
		obj_t);
	static obj_t BGl_z62iszd2subtypezf3z43zztype_typeofz00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zztype_typeofz00(void);
	static BgL_typez00_bglt BGl_z62getzd2typezb0zztype_typeofz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zztype_typeofz00(void);
	static BgL_typez00_bglt BGl_z62getzd2typezd2app1283z62zztype_typeofz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zztype_typeofz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	static BgL_typez00_bglt BGl_z62getzd2typezd2sync1272z62zztype_typeofz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern obj_t BGl_za2magicza2z00zztype_cachez00;
	extern bool_t BGl_czd2subtypezf3z21zztype_miscz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2uint64za2z00zztype_cachez00;
	static BgL_typez00_bglt
		BGl_z62getzd2typezd2letzd2var1281zb0zztype_typeofz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bstringza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztype_typeofz00(void);
	static BgL_typez00_bglt
		BGl_z62getzd2typezd2conditional1274z62zztype_typeofz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	extern obj_t BGl_varz00zzast_nodez00;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt, bool_t);
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_za2int32za2z00zztype_cachez00;
	extern obj_t BGl_za2int16za2z00zztype_cachez00;
	extern obj_t BGl_za2uint8za2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_za2elongza2z00zztype_cachez00;
	extern bool_t BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2int8za2z00zztype_cachez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zztype_typeofz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztype_typeofz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztype_typeofz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztype_typeofz00(void);
	extern obj_t BGl_za2llongza2z00zztype_cachez00;
	static bool_t
		BGl_iszd2strictzd2subtypezf3zf3zztype_typeofz00(BgL_typez00_bglt,
		BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62getzd2typezd2kwotez62zztype_typeofz00(obj_t,
		obj_t);
	extern obj_t BGl_za2bnilza2z00zztype_cachez00;
	static BgL_typez00_bglt
		BGl_z62getzd2typezd2switch1276z62zztype_typeofz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2int64za2z00zztype_cachez00;
	extern obj_t BGl_za2keywordza2z00zztype_cachez00;
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern obj_t BGl_za2epairza2z00zztype_cachez00;
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_iszd2subtypezf3z21zztype_typeofz00(obj_t, obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static BgL_typez00_bglt
		BGl_z62getzd2typezd2letzd2fun1279zb0zztype_typeofz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2typezd2kwotez00zztype_typeofz00(obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT obj_t BGl_za2nilza2z00zz__evalz00;
	extern obj_t BGl_za2charza2z00zztype_cachez00;
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2typezd2atomzd2envzd2zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21832za7,
		BGl_z62getzd2typezd2atomz62zztype_typeofz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_getzd2typezd2envz00zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7b01833za7, BGl_z62getzd2typezb0zztype_typeofz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1819z00zztype_typeofz00,
		BgL_bgl_string1819za700za7za7t1834za7, "get-type1265", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_iszd2subtypezf3zd2envzf3zztype_typeofz00,
		BgL_bgl_za762isza7d2subtypeza71835za7,
		BGl_z62iszd2subtypezf3z43zztype_typeofz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1821z00zztype_typeofz00,
		BgL_bgl_string1821za700za7za7t1836za7, "get-type", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1818z00zztype_typeofz00,
		BgL_bgl_za762getza7d2type1261837z00,
		BGl_z62getzd2type1265zb0zztype_typeofz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1829z00zztype_typeofz00,
		BgL_bgl_string1829za700za7za7t1838za7, "type_typeof", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1820z00zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21839za7,
		BGl_z62getzd2typezd2var1268z62zztype_typeofz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1822z00zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21840za7,
		BGl_z62getzd2typezd2sequence1270z62zztype_typeofz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1823z00zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21841za7,
		BGl_z62getzd2typezd2sync1272z62zztype_typeofz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1830z00zztype_typeofz00,
		BgL_bgl_string1830za700za7za7t1842za7, "a-tvector ", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1824z00zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21843za7,
		BGl_z62getzd2typezd2conditional1274z62zztype_typeofz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1825z00zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21844za7,
		BGl_z62getzd2typezd2switch1276z62zztype_typeofz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1826z00zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21845za7,
		BGl_z62getzd2typezd2letzd2fun1279zb0zztype_typeofz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1827z00zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21846za7,
		BGl_z62getzd2typezd2letzd2var1281zb0zztype_typeofz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1828z00zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21847za7,
		BGl_z62getzd2typezd2app1283z62zztype_typeofz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2typezd2kwotezd2envzd2zztype_typeofz00,
		BgL_bgl_za762getza7d2typeza7d21848za7,
		BGl_z62getzd2typezd2kwotez62zztype_typeofz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztype_typeofz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long
		BgL_checksumz00_2308, char *BgL_fromz00_2309)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztype_typeofz00))
				{
					BGl_requirezd2initializa7ationz75zztype_typeofz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztype_typeofz00();
					BGl_libraryzd2moduleszd2initz00zztype_typeofz00();
					BGl_cnstzd2initzd2zztype_typeofz00();
					BGl_importedzd2moduleszd2initz00zztype_typeofz00();
					BGl_genericzd2initzd2zztype_typeofz00();
					BGl_methodzd2initzd2zztype_typeofz00();
					return BGl_toplevelzd2initzd2zztype_typeofz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztype_typeofz00(void)
	{
		{	/* Type/typeof.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"type_typeof");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"type_typeof");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"type_typeof");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "type_typeof");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "type_typeof");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztype_typeofz00(void)
	{
		{	/* Type/typeof.scm 15 */
			{	/* Type/typeof.scm 15 */
				obj_t BgL_cportz00_2244;

				{	/* Type/typeof.scm 15 */
					obj_t BgL_stringz00_2251;

					BgL_stringz00_2251 = BGl_string1830z00zztype_typeofz00;
					{	/* Type/typeof.scm 15 */
						obj_t BgL_startz00_2252;

						BgL_startz00_2252 = BINT(0L);
						{	/* Type/typeof.scm 15 */
							obj_t BgL_endz00_2253;

							BgL_endz00_2253 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2251)));
							{	/* Type/typeof.scm 15 */

								BgL_cportz00_2244 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2251, BgL_startz00_2252, BgL_endz00_2253);
				}}}}
				{
					long BgL_iz00_2245;

					BgL_iz00_2245 = 0L;
				BgL_loopz00_2246:
					if ((BgL_iz00_2245 == -1L))
						{	/* Type/typeof.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Type/typeof.scm 15 */
							{	/* Type/typeof.scm 15 */
								obj_t BgL_arg1831z00_2247;

								{	/* Type/typeof.scm 15 */

									{	/* Type/typeof.scm 15 */
										obj_t BgL_locationz00_2249;

										BgL_locationz00_2249 = BBOOL(((bool_t) 0));
										{	/* Type/typeof.scm 15 */

											BgL_arg1831z00_2247 =
												BGl_readz00zz__readerz00(BgL_cportz00_2244,
												BgL_locationz00_2249);
										}
									}
								}
								{	/* Type/typeof.scm 15 */
									int BgL_tmpz00_2345;

									BgL_tmpz00_2345 = (int) (BgL_iz00_2245);
									CNST_TABLE_SET(BgL_tmpz00_2345, BgL_arg1831z00_2247);
							}}
							{	/* Type/typeof.scm 15 */
								int BgL_auxz00_2250;

								BgL_auxz00_2250 = (int) ((BgL_iz00_2245 - 1L));
								{
									long BgL_iz00_2350;

									BgL_iz00_2350 = (long) (BgL_auxz00_2250);
									BgL_iz00_2245 = BgL_iz00_2350;
									goto BgL_loopz00_2246;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztype_typeofz00(void)
	{
		{	/* Type/typeof.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztype_typeofz00(void)
	{
		{	/* Type/typeof.scm 15 */
			return BUNSPEC;
		}

	}



/* get-type-atom */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t BgL_atomz00_13)
	{
		{	/* Type/typeof.scm 35 */
			if (NULLP(BgL_atomz00_13))
				{	/* Type/typeof.scm 37 */
					return ((BgL_typez00_bglt) BGl_za2bnilza2z00zztype_cachez00);
				}
			else
				{	/* Type/typeof.scm 37 */
					if (INTEGERP(BgL_atomz00_13))
						{	/* Type/typeof.scm 38 */
							return ((BgL_typez00_bglt) BGl_za2longza2z00zztype_cachez00);
						}
					else
						{	/* Type/typeof.scm 38 */
							if (BIGNUMP(BgL_atomz00_13))
								{	/* Type/typeof.scm 39 */
									return
										((BgL_typez00_bglt) BGl_za2bignumza2z00zztype_cachez00);
								}
							else
								{	/* Type/typeof.scm 40 */
									bool_t BgL_test1854z00_2362;

									if (INTEGERP(BgL_atomz00_13))
										{	/* Type/typeof.scm 40 */
											BgL_test1854z00_2362 = ((bool_t) 1);
										}
									else
										{	/* Type/typeof.scm 40 */
											BgL_test1854z00_2362 = REALP(BgL_atomz00_13);
										}
									if (BgL_test1854z00_2362)
										{	/* Type/typeof.scm 40 */
											return
												((BgL_typez00_bglt) BGl_za2realza2z00zztype_cachez00);
										}
									else
										{	/* Type/typeof.scm 40 */
											if (BOOLEANP(BgL_atomz00_13))
												{	/* Type/typeof.scm 41 */
													return
														((BgL_typez00_bglt)
														BGl_za2boolza2z00zztype_cachez00);
												}
											else
												{	/* Type/typeof.scm 41 */
													if (CHARP(BgL_atomz00_13))
														{	/* Type/typeof.scm 42 */
															return
																((BgL_typez00_bglt)
																BGl_za2charza2z00zztype_cachez00);
														}
													else
														{	/* Type/typeof.scm 42 */
															if (STRINGP(BgL_atomz00_13))
																{	/* Type/typeof.scm 43 */
																	return
																		((BgL_typez00_bglt)
																		BGl_za2bstringza2z00zztype_cachez00);
																}
															else
																{	/* Type/typeof.scm 43 */
																	if ((BgL_atomz00_13 == BUNSPEC))
																		{	/* Type/typeof.scm 44 */
																			return
																				((BgL_typez00_bglt)
																				BGl_za2unspecza2z00zztype_cachez00);
																		}
																	else
																		{	/* Type/typeof.scm 44 */
																			if (ELONGP(BgL_atomz00_13))
																				{	/* Type/typeof.scm 45 */
																					return
																						((BgL_typez00_bglt)
																						BGl_za2elongza2z00zztype_cachez00);
																				}
																			else
																				{	/* Type/typeof.scm 45 */
																					if (LLONGP(BgL_atomz00_13))
																						{	/* Type/typeof.scm 46 */
																							return
																								((BgL_typez00_bglt)
																								BGl_za2llongza2z00zztype_cachez00);
																						}
																					else
																						{	/* Type/typeof.scm 46 */
																							if (BGL_INT8P(BgL_atomz00_13))
																								{	/* Type/typeof.scm 47 */
																									return
																										((BgL_typez00_bglt)
																										BGl_za2int8za2z00zztype_cachez00);
																								}
																							else
																								{	/* Type/typeof.scm 47 */
																									if (BGL_UINT8P
																										(BgL_atomz00_13))
																										{	/* Type/typeof.scm 48 */
																											return
																												((BgL_typez00_bglt)
																												BGl_za2uint8za2z00zztype_cachez00);
																										}
																									else
																										{	/* Type/typeof.scm 48 */
																											if (BGL_INT16P
																												(BgL_atomz00_13))
																												{	/* Type/typeof.scm 49 */
																													return
																														((BgL_typez00_bglt)
																														BGl_za2int16za2z00zztype_cachez00);
																												}
																											else
																												{	/* Type/typeof.scm 49 */
																													if (BGL_UINT16P
																														(BgL_atomz00_13))
																														{	/* Type/typeof.scm 50 */
																															return
																																(
																																(BgL_typez00_bglt)
																																BGl_za2uint16za2z00zztype_cachez00);
																														}
																													else
																														{	/* Type/typeof.scm 50 */
																															if (BGL_INT32P
																																(BgL_atomz00_13))
																																{	/* Type/typeof.scm 51 */
																																	return
																																		(
																																		(BgL_typez00_bglt)
																																		BGl_za2int32za2z00zztype_cachez00);
																																}
																															else
																																{	/* Type/typeof.scm 51 */
																																	if (BGL_UINT32P(BgL_atomz00_13))
																																		{	/* Type/typeof.scm 52 */
																																			return
																																				(
																																				(BgL_typez00_bglt)
																																				BGl_za2uint32za2z00zztype_cachez00);
																																		}
																																	else
																																		{	/* Type/typeof.scm 52 */
																																			if (BGL_INT64P(BgL_atomz00_13))
																																				{	/* Type/typeof.scm 53 */
																																					return
																																						(
																																						(BgL_typez00_bglt)
																																						BGl_za2int64za2z00zztype_cachez00);
																																				}
																																			else
																																				{	/* Type/typeof.scm 53 */
																																					if (BGL_UINT64P(BgL_atomz00_13))
																																						{	/* Type/typeof.scm 54 */
																																							return
																																								(
																																								(BgL_typez00_bglt)
																																								BGl_za2uint64za2z00zztype_cachez00);
																																						}
																																					else
																																						{	/* Type/typeof.scm 54 */
																																							if (KEYWORDP(BgL_atomz00_13))
																																								{	/* Type/typeof.scm 55 */
																																									return
																																										(
																																										(BgL_typez00_bglt)
																																										BGl_za2keywordza2z00zztype_cachez00);
																																								}
																																							else
																																								{	/* Type/typeof.scm 55 */
																																									return
																																										(
																																										(BgL_typez00_bglt)
																																										BGl_za2objza2z00zztype_cachez00);
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &get-type-atom */
	BgL_typez00_bglt BGl_z62getzd2typezd2atomz62zztype_typeofz00(obj_t
		BgL_envz00_2197, obj_t BgL_atomz00_2198)
	{
		{	/* Type/typeof.scm 35 */
			return BGl_getzd2typezd2atomz00zztype_typeofz00(BgL_atomz00_2198);
		}

	}



/* get-type-kwote */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2typezd2kwotez00zztype_typeofz00(obj_t BgL_kwotez00_14)
	{
		{	/* Type/typeof.scm 61 */
			if (SYMBOLP(BgL_kwotez00_14))
				{	/* Type/typeof.scm 63 */
					return ((BgL_typez00_bglt) BGl_za2symbolza2z00zztype_cachez00);
				}
			else
				{	/* Type/typeof.scm 63 */
					if (KEYWORDP(BgL_kwotez00_14))
						{	/* Type/typeof.scm 64 */
							return ((BgL_typez00_bglt) BGl_za2keywordza2z00zztype_cachez00);
						}
					else
						{	/* Type/typeof.scm 64 */
							if (PAIRP(BgL_kwotez00_14))
								{	/* Type/typeof.scm 65 */
									return ((BgL_typez00_bglt) BGl_za2pairza2z00zztype_cachez00);
								}
							else
								{	/* Type/typeof.scm 65 */
									if (NULLP(BgL_kwotez00_14))
										{	/* Type/typeof.scm 66 */
											return
												((BgL_typez00_bglt) BGl_za2bnilza2z00zztype_cachez00);
										}
									else
										{	/* Type/typeof.scm 66 */
											if (VECTORP(BgL_kwotez00_14))
												{	/* Type/typeof.scm 67 */
													return
														((BgL_typez00_bglt)
														BGl_za2vectorza2z00zztype_cachez00);
												}
											else
												{	/* Type/typeof.scm 68 */
													bool_t BgL_test1876z00_2429;

													if (STRUCTP(BgL_kwotez00_14))
														{	/* Type/typeof.scm 68 */
															BgL_test1876z00_2429 =
																(STRUCT_KEY(BgL_kwotez00_14) ==
																CNST_TABLE_REF(0));
														}
													else
														{	/* Type/typeof.scm 68 */
															BgL_test1876z00_2429 = ((bool_t) 0);
														}
													if (BgL_test1876z00_2429)
														{	/* Type/typeof.scm 68 */
															return
																((BgL_typez00_bglt)
																STRUCT_REF(BgL_kwotez00_14, (int) (0L)));
														}
													else
														{	/* Type/typeof.scm 68 */
															return
																((BgL_typez00_bglt)
																BGl_za2objza2z00zztype_cachez00);
														}
												}
										}
								}
						}
				}
		}

	}



/* &get-type-kwote */
	BgL_typez00_bglt BGl_z62getzd2typezd2kwotez62zztype_typeofz00(obj_t
		BgL_envz00_2199, obj_t BgL_kwotez00_2200)
	{
		{	/* Type/typeof.scm 61 */
			return BGl_getzd2typezd2kwotez00zztype_typeofz00(BgL_kwotez00_2200);
		}

	}



/* pair-nil? */
	bool_t BGl_pairzd2nilzf3z21zztype_typeofz00(obj_t BgL_tz00_15)
	{
		{	/* Type/typeof.scm 74 */
			{	/* Type/typeof.scm 75 */
				bool_t BgL__ortest_1105z00_1648;

				BgL__ortest_1105z00_1648 =
					(BgL_tz00_15 == BGl_za2pairza2z00zztype_cachez00);
				if (BgL__ortest_1105z00_1648)
					{	/* Type/typeof.scm 75 */
						return BgL__ortest_1105z00_1648;
					}
				else
					{	/* Type/typeof.scm 75 */
						bool_t BgL__ortest_1106z00_1649;

						BgL__ortest_1106z00_1649 =
							(BgL_tz00_15 == BGl_za2epairza2z00zztype_cachez00);
						if (BgL__ortest_1106z00_1649)
							{	/* Type/typeof.scm 75 */
								return BgL__ortest_1106z00_1649;
							}
						else
							{	/* Type/typeof.scm 75 */
								bool_t BgL__ortest_1108z00_1650;

								BgL__ortest_1108z00_1650 =
									(BgL_tz00_15 == BGl_za2bnilza2z00zztype_cachez00);
								if (BgL__ortest_1108z00_1650)
									{	/* Type/typeof.scm 75 */
										return BgL__ortest_1108z00_1650;
									}
								else
									{	/* Type/typeof.scm 75 */
										return
											(BgL_tz00_15 == BGl_za2pairzd2nilza2zd2zztype_cachez00);
									}
							}
					}
			}
		}

	}



/* is-strict-subtype? */
	bool_t BGl_iszd2strictzd2subtypezf3zf3zztype_typeofz00(BgL_typez00_bglt
		BgL_t1z00_26, BgL_typez00_bglt BgL_t2z00_27, obj_t BgL_strictz00_28)
	{
		{	/* Type/typeof.scm 170 */
			if (CBOOL(BGl_iszd2subtypezf3z21zztype_typeofz00(
						((obj_t) BgL_t1z00_26), ((obj_t) BgL_t2z00_27))))
				{	/* Type/typeof.scm 172 */
					bool_t BgL__ortest_1127z00_1652;

					if (CBOOL(BgL_strictz00_28))
						{	/* Type/typeof.scm 172 */
							BgL__ortest_1127z00_1652 = ((bool_t) 0);
						}
					else
						{	/* Type/typeof.scm 172 */
							BgL__ortest_1127z00_1652 = ((bool_t) 1);
						}
					if (BgL__ortest_1127z00_1652)
						{	/* Type/typeof.scm 172 */
							return BgL__ortest_1127z00_1652;
						}
					else
						{	/* Type/typeof.scm 172 */
							if ((((obj_t) BgL_t1z00_26) == BGl_za2_za2z00zztype_cachez00))
								{	/* Type/typeof.scm 172 */
									return ((bool_t) 0);
								}
							else
								{	/* Type/typeof.scm 172 */
									if ((((obj_t) BgL_t2z00_27) == BGl_za2_za2z00zztype_cachez00))
										{	/* Type/typeof.scm 172 */
											return ((bool_t) 0);
										}
									else
										{	/* Type/typeof.scm 172 */
											return ((bool_t) 1);
										}
								}
						}
				}
			else
				{	/* Type/typeof.scm 171 */
					return ((bool_t) 0);
				}
		}

	}



/* is-subtype? */
	BGL_EXPORTED_DEF obj_t BGl_iszd2subtypezf3z21zztype_typeofz00(obj_t
		BgL_t1z00_29, obj_t BgL_t2z00_30)
	{
		{	/* Type/typeof.scm 179 */
			if ((BgL_t1z00_29 == BgL_t2z00_30))
				{	/* Type/typeof.scm 181 */
					return BTRUE;
				}
			else
				{	/* Type/typeof.scm 183 */
					bool_t BgL_test1887z00_2463;

					if ((BgL_t1z00_29 == BGl_za2magicza2z00zztype_cachez00))
						{	/* Type/typeof.scm 183 */
							BgL_test1887z00_2463 = ((bool_t) 1);
						}
					else
						{	/* Type/typeof.scm 183 */
							BgL_test1887z00_2463 =
								(BgL_t2z00_30 == BGl_za2magicza2z00zztype_cachez00);
						}
					if (BgL_test1887z00_2463)
						{	/* Type/typeof.scm 183 */
							return BTRUE;
						}
					else
						{	/* Type/typeof.scm 185 */
							bool_t BgL_test1889z00_2467;

							if ((BgL_t2z00_30 == BGl_za2_za2z00zztype_cachez00))
								{	/* Type/typeof.scm 185 */
									BgL_test1889z00_2467 = ((bool_t) 1);
								}
							else
								{	/* Type/typeof.scm 185 */
									BgL_test1889z00_2467 =
										(BgL_t1z00_29 == BGl_za2_za2z00zztype_cachez00);
								}
							if (BgL_test1889z00_2467)
								{	/* Type/typeof.scm 185 */
									return BTRUE;
								}
							else
								{	/* Type/typeof.scm 185 */
									if ((BgL_t2z00_30 == BGl_za2pairzd2nilza2zd2zztype_cachez00))
										{	/* Type/typeof.scm 188 */
											bool_t BgL__ortest_1129z00_1656;

											BgL__ortest_1129z00_1656 =
												(BgL_t1z00_29 == BGl_za2pairza2z00zztype_cachez00);
											if (BgL__ortest_1129z00_1656)
												{	/* Type/typeof.scm 188 */
													return BBOOL(BgL__ortest_1129z00_1656);
												}
											else
												{	/* Type/typeof.scm 188 */
													bool_t BgL__ortest_1130z00_1657;

													BgL__ortest_1130z00_1657 =
														(BgL_t1z00_29 == BGl_za2epairza2z00zztype_cachez00);
													if (BgL__ortest_1130z00_1657)
														{	/* Type/typeof.scm 188 */
															return BBOOL(BgL__ortest_1130z00_1657);
														}
													else
														{	/* Type/typeof.scm 188 */
															return
																BBOOL(
																(BgL_t1z00_29 == BGl_za2nilza2z00zz__evalz00));
														}
												}
										}
									else
										{	/* Type/typeof.scm 187 */
											if ((BgL_t2z00_30 == BGl_za2pairza2z00zztype_cachez00))
												{	/* Type/typeof.scm 189 */
													return
														BBOOL(
														(BgL_t1z00_29 ==
															BGl_za2epairza2z00zztype_cachez00));
												}
											else
												{	/* Type/typeof.scm 191 */
													bool_t BgL_test1895z00_2485;

													{	/* Type/typeof.scm 191 */
														bool_t BgL_test1896z00_2486;

														{	/* Type/typeof.scm 191 */
															obj_t BgL_classz00_2017;

															BgL_classz00_2017 =
																BGl_tclassz00zzobject_classz00;
															if (BGL_OBJECTP(BgL_t1z00_29))
																{	/* Type/typeof.scm 191 */
																	BgL_objectz00_bglt BgL_arg1807z00_2019;

																	BgL_arg1807z00_2019 =
																		(BgL_objectz00_bglt) (BgL_t1z00_29);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Type/typeof.scm 191 */
																			long BgL_idxz00_2025;

																			BgL_idxz00_2025 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2019);
																			BgL_test1896z00_2486 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2025 + 2L)) ==
																				BgL_classz00_2017);
																		}
																	else
																		{	/* Type/typeof.scm 191 */
																			bool_t BgL_res1809z00_2050;

																			{	/* Type/typeof.scm 191 */
																				obj_t BgL_oclassz00_2033;

																				{	/* Type/typeof.scm 191 */
																					obj_t BgL_arg1815z00_2041;
																					long BgL_arg1816z00_2042;

																					BgL_arg1815z00_2041 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Type/typeof.scm 191 */
																						long BgL_arg1817z00_2043;

																						BgL_arg1817z00_2043 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2019);
																						BgL_arg1816z00_2042 =
																							(BgL_arg1817z00_2043 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2033 =
																						VECTOR_REF(BgL_arg1815z00_2041,
																						BgL_arg1816z00_2042);
																				}
																				{	/* Type/typeof.scm 191 */
																					bool_t BgL__ortest_1115z00_2034;

																					BgL__ortest_1115z00_2034 =
																						(BgL_classz00_2017 ==
																						BgL_oclassz00_2033);
																					if (BgL__ortest_1115z00_2034)
																						{	/* Type/typeof.scm 191 */
																							BgL_res1809z00_2050 =
																								BgL__ortest_1115z00_2034;
																						}
																					else
																						{	/* Type/typeof.scm 191 */
																							long BgL_odepthz00_2035;

																							{	/* Type/typeof.scm 191 */
																								obj_t BgL_arg1804z00_2036;

																								BgL_arg1804z00_2036 =
																									(BgL_oclassz00_2033);
																								BgL_odepthz00_2035 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2036);
																							}
																							if ((2L < BgL_odepthz00_2035))
																								{	/* Type/typeof.scm 191 */
																									obj_t BgL_arg1802z00_2038;

																									{	/* Type/typeof.scm 191 */
																										obj_t BgL_arg1803z00_2039;

																										BgL_arg1803z00_2039 =
																											(BgL_oclassz00_2033);
																										BgL_arg1802z00_2038 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2039, 2L);
																									}
																									BgL_res1809z00_2050 =
																										(BgL_arg1802z00_2038 ==
																										BgL_classz00_2017);
																								}
																							else
																								{	/* Type/typeof.scm 191 */
																									BgL_res1809z00_2050 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1896z00_2486 =
																				BgL_res1809z00_2050;
																		}
																}
															else
																{	/* Type/typeof.scm 191 */
																	BgL_test1896z00_2486 = ((bool_t) 0);
																}
														}
														if (BgL_test1896z00_2486)
															{	/* Type/typeof.scm 191 */
																BgL_test1895z00_2485 = ((bool_t) 1);
															}
														else
															{	/* Type/typeof.scm 191 */
																obj_t BgL_classz00_2051;

																BgL_classz00_2051 =
																	BGl_tclassz00zzobject_classz00;
																if (BGL_OBJECTP(BgL_t2z00_30))
																	{	/* Type/typeof.scm 191 */
																		BgL_objectz00_bglt BgL_arg1807z00_2053;

																		BgL_arg1807z00_2053 =
																			(BgL_objectz00_bglt) (BgL_t2z00_30);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Type/typeof.scm 191 */
																				long BgL_idxz00_2059;

																				BgL_idxz00_2059 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2053);
																				BgL_test1895z00_2485 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2059 + 2L)) ==
																					BgL_classz00_2051);
																			}
																		else
																			{	/* Type/typeof.scm 191 */
																				bool_t BgL_res1810z00_2084;

																				{	/* Type/typeof.scm 191 */
																					obj_t BgL_oclassz00_2067;

																					{	/* Type/typeof.scm 191 */
																						obj_t BgL_arg1815z00_2075;
																						long BgL_arg1816z00_2076;

																						BgL_arg1815z00_2075 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Type/typeof.scm 191 */
																							long BgL_arg1817z00_2077;

																							BgL_arg1817z00_2077 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2053);
																							BgL_arg1816z00_2076 =
																								(BgL_arg1817z00_2077 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2067 =
																							VECTOR_REF(BgL_arg1815z00_2075,
																							BgL_arg1816z00_2076);
																					}
																					{	/* Type/typeof.scm 191 */
																						bool_t BgL__ortest_1115z00_2068;

																						BgL__ortest_1115z00_2068 =
																							(BgL_classz00_2051 ==
																							BgL_oclassz00_2067);
																						if (BgL__ortest_1115z00_2068)
																							{	/* Type/typeof.scm 191 */
																								BgL_res1810z00_2084 =
																									BgL__ortest_1115z00_2068;
																							}
																						else
																							{	/* Type/typeof.scm 191 */
																								long BgL_odepthz00_2069;

																								{	/* Type/typeof.scm 191 */
																									obj_t BgL_arg1804z00_2070;

																									BgL_arg1804z00_2070 =
																										(BgL_oclassz00_2067);
																									BgL_odepthz00_2069 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2070);
																								}
																								if ((2L < BgL_odepthz00_2069))
																									{	/* Type/typeof.scm 191 */
																										obj_t BgL_arg1802z00_2072;

																										{	/* Type/typeof.scm 191 */
																											obj_t BgL_arg1803z00_2073;

																											BgL_arg1803z00_2073 =
																												(BgL_oclassz00_2067);
																											BgL_arg1802z00_2072 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2073,
																												2L);
																										}
																										BgL_res1810z00_2084 =
																											(BgL_arg1802z00_2072 ==
																											BgL_classz00_2051);
																									}
																								else
																									{	/* Type/typeof.scm 191 */
																										BgL_res1810z00_2084 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test1895z00_2485 =
																					BgL_res1810z00_2084;
																			}
																	}
																else
																	{	/* Type/typeof.scm 191 */
																		BgL_test1895z00_2485 = ((bool_t) 0);
																	}
															}
													}
													if (BgL_test1895z00_2485)
														{	/* Type/typeof.scm 191 */
															return
																BBOOL(BGl_typezd2subclasszf3z21zzobject_classz00
																(((BgL_typez00_bglt) BgL_t1z00_29),
																	((BgL_typez00_bglt) BgL_t2z00_30)));
														}
													else
														{	/* Type/typeof.scm 193 */
															bool_t BgL_test1905z00_2535;

															if (BGl_bigloozd2typezf3z21zztype_typez00(
																	((BgL_typez00_bglt) BgL_t1z00_29)))
																{	/* Type/typeof.scm 193 */
																	BgL_test1905z00_2535 = ((bool_t) 0);
																}
															else
																{	/* Type/typeof.scm 193 */
																	if (BGl_bigloozd2typezf3z21zztype_typez00(
																			((BgL_typez00_bglt) BgL_t2z00_30)))
																		{	/* Type/typeof.scm 193 */
																			BgL_test1905z00_2535 = ((bool_t) 0);
																		}
																	else
																		{	/* Type/typeof.scm 193 */
																			BgL_test1905z00_2535 = ((bool_t) 1);
																		}
																}
															if (BgL_test1905z00_2535)
																{	/* Type/typeof.scm 193 */
																	return
																		BBOOL(BGl_czd2subtypezf3z21zztype_miscz00(
																			((BgL_typez00_bglt) BgL_t1z00_29),
																			((BgL_typez00_bglt) BgL_t2z00_30)));
																}
															else
																{	/* Type/typeof.scm 195 */
																	bool_t BgL_test1908z00_2546;

																	if (
																		(BgL_t2z00_30 ==
																			BGl_za2bintza2z00zztype_cachez00))
																		{	/* Type/typeof.scm 195 */
																			bool_t BgL__ortest_1131z00_1665;

																			BgL__ortest_1131z00_1665 =
																				(BGl_typez00zztype_typez00 ==
																				BGl_za2intza2z00zztype_cachez00);
																			if (BgL__ortest_1131z00_1665)
																				{	/* Type/typeof.scm 195 */
																					BgL_test1908z00_2546 =
																						BgL__ortest_1131z00_1665;
																				}
																			else
																				{	/* Type/typeof.scm 195 */
																					BgL_test1908z00_2546 =
																						(BGl_typez00zztype_typez00 ==
																						BGl_za2longza2z00zztype_cachez00);
																				}
																		}
																	else
																		{	/* Type/typeof.scm 195 */
																			BgL_test1908z00_2546 = ((bool_t) 0);
																		}
																	if (BgL_test1908z00_2546)
																		{	/* Type/typeof.scm 195 */
																			return BTRUE;
																		}
																	else
																		{	/* Type/typeof.scm 195 */
																			return
																				BBOOL(
																				(BgL_t2z00_30 ==
																					BGl_za2objza2z00zztype_cachez00));
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &is-subtype? */
	obj_t BGl_z62iszd2subtypezf3z43zztype_typeofz00(obj_t BgL_envz00_2201,
		obj_t BgL_t1z00_2202, obj_t BgL_t2z00_2203)
	{
		{	/* Type/typeof.scm 179 */
			return
				BGl_iszd2subtypezf3z21zztype_typeofz00(BgL_t1z00_2202, BgL_t2z00_2203);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztype_typeofz00(void)
	{
		{	/* Type/typeof.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztype_typeofz00(void)
	{
		{	/* Type/typeof.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_getzd2typezd2envz00zztype_typeofz00,
				BGl_proc1818z00zztype_typeofz00, BGl_nodez00zzast_nodez00,
				BGl_string1819z00zztype_typeofz00);
		}

	}



/* &get-type1265 */
	obj_t BGl_z62getzd2type1265zb0zztype_typeofz00(obj_t BgL_envz00_2205,
		obj_t BgL_nodez00_2206, obj_t BgL_strictz00_2207)
	{
		{	/* Type/typeof.scm 86 */
			return
				((obj_t)
				(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_nodez00_2206)))->BgL_typez00));
		}

	}



/* get-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt BgL_nodez00_16,
		bool_t BgL_strictz00_17)
	{
		{	/* Type/typeof.scm 86 */
			{	/* Type/typeof.scm 86 */
				obj_t BgL_method1266z00_1674;

				{	/* Type/typeof.scm 86 */
					obj_t BgL_res1815z00_2116;

					{	/* Type/typeof.scm 86 */
						long BgL_objzd2classzd2numz00_2087;

						BgL_objzd2classzd2numz00_2087 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_16));
						{	/* Type/typeof.scm 86 */
							obj_t BgL_arg1811z00_2088;

							BgL_arg1811z00_2088 =
								PROCEDURE_REF(BGl_getzd2typezd2envz00zztype_typeofz00,
								(int) (1L));
							{	/* Type/typeof.scm 86 */
								int BgL_offsetz00_2091;

								BgL_offsetz00_2091 = (int) (BgL_objzd2classzd2numz00_2087);
								{	/* Type/typeof.scm 86 */
									long BgL_offsetz00_2092;

									BgL_offsetz00_2092 =
										((long) (BgL_offsetz00_2091) - OBJECT_TYPE);
									{	/* Type/typeof.scm 86 */
										long BgL_modz00_2093;

										BgL_modz00_2093 =
											(BgL_offsetz00_2092 >> (int) ((long) ((int) (4L))));
										{	/* Type/typeof.scm 86 */
											long BgL_restz00_2095;

											BgL_restz00_2095 =
												(BgL_offsetz00_2092 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Type/typeof.scm 86 */

												{	/* Type/typeof.scm 86 */
													obj_t BgL_bucketz00_2097;

													BgL_bucketz00_2097 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2088), BgL_modz00_2093);
													BgL_res1815z00_2116 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2097), BgL_restz00_2095);
					}}}}}}}}
					BgL_method1266z00_1674 = BgL_res1815z00_2116;
				}
				return
					((BgL_typez00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1266z00_1674,
						((obj_t) BgL_nodez00_16), BBOOL(BgL_strictz00_17)));
			}
		}

	}



/* &get-type */
	BgL_typez00_bglt BGl_z62getzd2typezb0zztype_typeofz00(obj_t BgL_envz00_2208,
		obj_t BgL_nodez00_2209, obj_t BgL_strictz00_2210)
	{
		{	/* Type/typeof.scm 86 */
			return
				BGl_getzd2typezd2zztype_typeofz00(
				((BgL_nodez00_bglt) BgL_nodez00_2209), CBOOL(BgL_strictz00_2210));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztype_typeofz00(void)
	{
		{	/* Type/typeof.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2typezd2envz00zztype_typeofz00, BGl_varz00zzast_nodez00,
				BGl_proc1820z00zztype_typeofz00, BGl_string1821z00zztype_typeofz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2typezd2envz00zztype_typeofz00, BGl_sequencez00zzast_nodez00,
				BGl_proc1822z00zztype_typeofz00, BGl_string1821z00zztype_typeofz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2typezd2envz00zztype_typeofz00, BGl_syncz00zzast_nodez00,
				BGl_proc1823z00zztype_typeofz00, BGl_string1821z00zztype_typeofz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2typezd2envz00zztype_typeofz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1824z00zztype_typeofz00,
				BGl_string1821z00zztype_typeofz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2typezd2envz00zztype_typeofz00, BGl_switchz00zzast_nodez00,
				BGl_proc1825z00zztype_typeofz00, BGl_string1821z00zztype_typeofz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2typezd2envz00zztype_typeofz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc1826z00zztype_typeofz00, BGl_string1821z00zztype_typeofz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2typezd2envz00zztype_typeofz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1827z00zztype_typeofz00, BGl_string1821z00zztype_typeofz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2typezd2envz00zztype_typeofz00, BGl_appz00zzast_nodez00,
				BGl_proc1828z00zztype_typeofz00, BGl_string1821z00zztype_typeofz00);
		}

	}



/* &get-type-app1283 */
	BgL_typez00_bglt BGl_z62getzd2typezd2app1283z62zztype_typeofz00(obj_t
		BgL_envz00_2219, obj_t BgL_nodez00_2220, obj_t BgL_strictz00_2221)
	{
		{	/* Type/typeof.scm 244 */
			{	/* Type/typeof.scm 246 */
				bool_t BgL_test1911z00_2603;

				{	/* Type/typeof.scm 246 */
					BgL_typez00_bglt BgL_arg1630z00_2258;

					BgL_arg1630z00_2258 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_2220))))->BgL_typez00);
					BgL_test1911z00_2603 =
						(((obj_t) BgL_arg1630z00_2258) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1911z00_2603)
					{	/* Type/typeof.scm 246 */
						return
							(((BgL_variablez00_bglt) COBJECT(
									(((BgL_varz00_bglt) COBJECT(
												(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_2220)))->
													BgL_funz00)))->BgL_variablez00)))->BgL_typez00);
					}
				else
					{	/* Type/typeof.scm 246 */
						return
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_2220))))->BgL_typez00);
					}
			}
		}

	}



/* &get-type-let-var1281 */
	BgL_typez00_bglt BGl_z62getzd2typezd2letzd2var1281zb0zztype_typeofz00(obj_t
		BgL_envz00_2222, obj_t BgL_nodez00_2223, obj_t BgL_strictz00_2224)
	{
		{	/* Type/typeof.scm 235 */
			{	/* Type/typeof.scm 237 */
				bool_t BgL_test1912z00_2616;

				{	/* Type/typeof.scm 237 */
					BgL_typez00_bglt BgL_arg1616z00_2260;

					BgL_arg1616z00_2260 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2223))))->BgL_typez00);
					BgL_test1912z00_2616 =
						(((obj_t) BgL_arg1616z00_2260) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1912z00_2616)
					{	/* Type/typeof.scm 237 */
						return
							BGl_getzd2typezd2zztype_typeofz00(
							(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_2223)))->BgL_bodyz00),
							CBOOL(BgL_strictz00_2224));
					}
				else
					{	/* Type/typeof.scm 237 */
						return
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_nodez00_2223))))->BgL_typez00);
					}
			}
		}

	}



/* &get-type-let-fun1279 */
	BgL_typez00_bglt BGl_z62getzd2typezd2letzd2fun1279zb0zztype_typeofz00(obj_t
		BgL_envz00_2225, obj_t BgL_nodez00_2226, obj_t BgL_strictz00_2227)
	{
		{	/* Type/typeof.scm 226 */
			{	/* Type/typeof.scm 228 */
				bool_t BgL_test1913z00_2629;

				{	/* Type/typeof.scm 228 */
					BgL_typez00_bglt BgL_arg1611z00_2262;

					BgL_arg1611z00_2262 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2funzd2_bglt) BgL_nodez00_2226))))->BgL_typez00);
					BgL_test1913z00_2629 =
						(((obj_t) BgL_arg1611z00_2262) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1913z00_2629)
					{	/* Type/typeof.scm 228 */
						return
							BGl_getzd2typezd2zztype_typeofz00(
							(((BgL_letzd2funzd2_bglt) COBJECT(
										((BgL_letzd2funzd2_bglt) BgL_nodez00_2226)))->BgL_bodyz00),
							CBOOL(BgL_strictz00_2227));
					}
				else
					{	/* Type/typeof.scm 228 */
						return
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2funzd2_bglt) BgL_nodez00_2226))))->BgL_typez00);
					}
			}
		}

	}



/* &get-type-switch1276 */
	BgL_typez00_bglt BGl_z62getzd2typezd2switch1276z62zztype_typeofz00(obj_t
		BgL_envz00_2228, obj_t BgL_nodez00_2229, obj_t BgL_strictz00_2230)
	{
		{	/* Type/typeof.scm 203 */
			{	/* Type/typeof.scm 205 */
				bool_t BgL_test1914z00_2642;

				{	/* Type/typeof.scm 205 */
					BgL_typez00_bglt BgL_arg1602z00_2264;

					BgL_arg1602z00_2264 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_switchz00_bglt) BgL_nodez00_2229))))->BgL_typez00);
					BgL_test1914z00_2642 =
						(((obj_t) BgL_arg1602z00_2264) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1914z00_2642)
					{	/* Type/typeof.scm 206 */
						obj_t BgL_g1133z00_2265;
						BgL_typez00_bglt BgL_g1134z00_2266;

						BgL_g1133z00_2265 =
							CDR(
							(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_2229)))->BgL_clausesz00));
						{	/* Type/typeof.scm 207 */
							obj_t BgL_arg1593z00_2267;

							BgL_arg1593z00_2267 =
								CDR(CAR(
									(((BgL_switchz00_bglt) COBJECT(
												((BgL_switchz00_bglt) BgL_nodez00_2229)))->
										BgL_clausesz00)));
							BgL_g1134z00_2266 =
								BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt)
									BgL_arg1593z00_2267), CBOOL(BgL_strictz00_2230));
						}
						{
							obj_t BgL_clausesz00_2269;
							obj_t BgL_typez00_2270;

							{
								obj_t BgL_auxz00_2658;

								BgL_clausesz00_2269 = BgL_g1133z00_2265;
								BgL_typez00_2270 = ((obj_t) BgL_g1134z00_2266);
							BgL_loopz00_2268:
								if (NULLP(BgL_clausesz00_2269))
									{	/* Type/typeof.scm 208 */
										BgL_auxz00_2658 = BgL_typez00_2270;
									}
								else
									{	/* Type/typeof.scm 210 */
										BgL_typez00_bglt BgL_ntypez00_2271;

										{	/* Type/typeof.scm 210 */
											obj_t BgL_arg1585z00_2272;

											{	/* Type/typeof.scm 210 */
												obj_t BgL_pairz00_2273;

												BgL_pairz00_2273 = CAR(((obj_t) BgL_clausesz00_2269));
												BgL_arg1585z00_2272 = CDR(BgL_pairz00_2273);
											}
											BgL_ntypez00_2271 =
												BGl_getzd2typezd2zztype_typeofz00(
												((BgL_nodez00_bglt) BgL_arg1585z00_2272),
												CBOOL(BgL_strictz00_2230));
										}
										if ((((obj_t) BgL_ntypez00_2271) == BgL_typez00_2270))
											{	/* Type/typeof.scm 213 */
												obj_t BgL_arg1565z00_2274;

												BgL_arg1565z00_2274 =
													CDR(((obj_t) BgL_clausesz00_2269));
												{
													obj_t BgL_clausesz00_2672;

													BgL_clausesz00_2672 = BgL_arg1565z00_2274;
													BgL_clausesz00_2269 = BgL_clausesz00_2672;
													goto BgL_loopz00_2268;
												}
											}
										else
											{	/* Type/typeof.scm 212 */
												if (
													(BgL_typez00_2270 ==
														BGl_za2magicza2z00zztype_cachez00))
													{	/* Type/typeof.scm 215 */
														obj_t BgL_arg1571z00_2275;

														BgL_arg1571z00_2275 =
															CDR(((obj_t) BgL_clausesz00_2269));
														{
															obj_t BgL_typez00_2678;
															obj_t BgL_clausesz00_2677;

															BgL_clausesz00_2677 = BgL_arg1571z00_2275;
															BgL_typez00_2678 = ((obj_t) BgL_ntypez00_2271);
															BgL_typez00_2270 = BgL_typez00_2678;
															BgL_clausesz00_2269 = BgL_clausesz00_2677;
															goto BgL_loopz00_2268;
														}
													}
												else
													{	/* Type/typeof.scm 216 */
														bool_t BgL_test1918z00_2680;

														if (
															(((obj_t) BgL_ntypez00_2271) == BgL_typez00_2270))
															{	/* Type/typeof.scm 216 */
																BgL_test1918z00_2680 = ((bool_t) 1);
															}
														else
															{	/* Type/typeof.scm 216 */
																BgL_test1918z00_2680 =
																	(
																	((obj_t) BgL_ntypez00_2271) ==
																	BGl_za2magicza2z00zztype_cachez00);
															}
														if (BgL_test1918z00_2680)
															{	/* Type/typeof.scm 217 */
																obj_t BgL_arg1575z00_2276;

																BgL_arg1575z00_2276 =
																	CDR(((obj_t) BgL_clausesz00_2269));
																{
																	obj_t BgL_clausesz00_2688;

																	BgL_clausesz00_2688 = BgL_arg1575z00_2276;
																	BgL_clausesz00_2269 = BgL_clausesz00_2688;
																	goto BgL_loopz00_2268;
																}
															}
														else
															{	/* Type/typeof.scm 218 */
																bool_t BgL_test1920z00_2689;

																if (BGl_pairzd2nilzf3z21zztype_typeofz00
																	(BgL_typez00_2270))
																	{	/* Type/typeof.scm 218 */
																		BgL_test1920z00_2689 =
																			BGl_pairzd2nilzf3z21zztype_typeofz00(
																			((obj_t) BgL_ntypez00_2271));
																	}
																else
																	{	/* Type/typeof.scm 218 */
																		BgL_test1920z00_2689 = ((bool_t) 0);
																	}
																if (BgL_test1920z00_2689)
																	{	/* Type/typeof.scm 219 */
																		obj_t BgL_arg1584z00_2277;

																		BgL_arg1584z00_2277 =
																			CDR(((obj_t) BgL_clausesz00_2269));
																		{
																			obj_t BgL_typez00_2697;
																			obj_t BgL_clausesz00_2696;

																			BgL_clausesz00_2696 = BgL_arg1584z00_2277;
																			BgL_typez00_2697 =
																				BGl_za2pairzd2nilza2zd2zztype_cachez00;
																			BgL_typez00_2270 = BgL_typez00_2697;
																			BgL_clausesz00_2269 = BgL_clausesz00_2696;
																			goto BgL_loopz00_2268;
																		}
																	}
																else
																	{	/* Type/typeof.scm 218 */
																		BgL_auxz00_2658 =
																			BGl_za2objza2z00zztype_cachez00;
																	}
															}
													}
											}
									}
								return ((BgL_typez00_bglt) BgL_auxz00_2658);
							}
						}
					}
				else
					{	/* Type/typeof.scm 205 */
						return
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_switchz00_bglt) BgL_nodez00_2229))))->BgL_typez00);
					}
			}
		}

	}



/* &get-type-conditional1274 */
	BgL_typez00_bglt BGl_z62getzd2typezd2conditional1274z62zztype_typeofz00(obj_t
		BgL_envz00_2231, obj_t BgL_nodez00_2232, obj_t BgL_strictz00_2233)
	{
		{	/* Type/typeof.scm 152 */
			{	/* Type/typeof.scm 154 */
				bool_t BgL_test1922z00_2703;

				{	/* Type/typeof.scm 154 */
					BgL_typez00_bglt BgL_arg1559z00_2279;

					BgL_arg1559z00_2279 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_conditionalz00_bglt) BgL_nodez00_2232))))->BgL_typez00);
					BgL_test1922z00_2703 =
						(((obj_t) BgL_arg1559z00_2279) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1922z00_2703)
					{	/* Type/typeof.scm 156 */
						BgL_typez00_bglt BgL_ttruez00_2280;
						BgL_typez00_bglt BgL_tfalsez00_2281;

						BgL_ttruez00_2280 =
							BGl_getzd2typezd2zztype_typeofz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_2232)))->
								BgL_truez00), CBOOL(BgL_strictz00_2233));
						BgL_tfalsez00_2281 =
							BGl_getzd2typezd2zztype_typeofz00((((BgL_conditionalz00_bglt)
									COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_2232)))->
								BgL_falsez00), CBOOL(BgL_strictz00_2233));
						{	/* Type/typeof.scm 159 */
							bool_t BgL_test1923z00_2717;

							if ((((obj_t) BgL_ttruez00_2280) == ((obj_t) BgL_tfalsez00_2281)))
								{	/* Type/typeof.scm 159 */
									BgL_test1923z00_2717 = ((bool_t) 1);
								}
							else
								{	/* Type/typeof.scm 159 */
									BgL_test1923z00_2717 =
										(
										((obj_t) BgL_tfalsez00_2281) ==
										BGl_za2magicza2z00zztype_cachez00);
								}
							if (BgL_test1923z00_2717)
								{	/* Type/typeof.scm 159 */
									return BgL_ttruez00_2280;
								}
							else
								{	/* Type/typeof.scm 159 */
									if (
										(((obj_t) BgL_ttruez00_2280) ==
											BGl_za2magicza2z00zztype_cachez00))
										{	/* Type/typeof.scm 160 */
											return BgL_tfalsez00_2281;
										}
									else
										{	/* Type/typeof.scm 161 */
											bool_t BgL_test1926z00_2727;

											if (BGl_pairzd2nilzf3z21zztype_typeofz00(
													((obj_t) BgL_ttruez00_2280)))
												{	/* Type/typeof.scm 161 */
													BgL_test1926z00_2727 =
														BGl_pairzd2nilzf3z21zztype_typeofz00(
														((obj_t) BgL_tfalsez00_2281));
												}
											else
												{	/* Type/typeof.scm 161 */
													BgL_test1926z00_2727 = ((bool_t) 0);
												}
											if (BgL_test1926z00_2727)
												{	/* Type/typeof.scm 161 */
													return
														((BgL_typez00_bglt)
														BGl_za2pairzd2nilza2zd2zztype_cachez00);
												}
											else
												{	/* Type/typeof.scm 161 */
													if (BGl_iszd2strictzd2subtypezf3zf3zztype_typeofz00
														(BgL_ttruez00_2280, BgL_tfalsez00_2281,
															BgL_strictz00_2233))
														{	/* Type/typeof.scm 162 */
															return BgL_tfalsez00_2281;
														}
													else
														{	/* Type/typeof.scm 162 */
															if (BGl_iszd2strictzd2subtypezf3zf3zztype_typeofz00(BgL_tfalsez00_2281, BgL_ttruez00_2280, BgL_strictz00_2233))
																{	/* Type/typeof.scm 163 */
																	return BgL_ttruez00_2280;
																}
															else
																{	/* Type/typeof.scm 163 */
																	return
																		((BgL_typez00_bglt)
																		BGl_za2objza2z00zztype_cachez00);
																}
														}
												}
										}
								}
						}
					}
				else
					{	/* Type/typeof.scm 154 */
						return
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_conditionalz00_bglt) BgL_nodez00_2232))))->
							BgL_typez00);
					}
			}
		}

	}



/* &get-type-sync1272 */
	BgL_typez00_bglt BGl_z62getzd2typezd2sync1272z62zztype_typeofz00(obj_t
		BgL_envz00_2234, obj_t BgL_nodez00_2235, obj_t BgL_strictz00_2236)
	{
		{	/* Type/typeof.scm 142 */
			{	/* Type/typeof.scm 144 */
				bool_t BgL_test1930z00_2742;

				{	/* Type/typeof.scm 144 */
					BgL_typez00_bglt BgL_arg1540z00_2283;

					BgL_arg1540z00_2283 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_nodez00_2235))))->BgL_typez00);
					BgL_test1930z00_2742 =
						(((obj_t) BgL_arg1540z00_2283) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1930z00_2742)
					{	/* Type/typeof.scm 144 */
						return
							BGl_getzd2typezd2zztype_typeofz00(
							(((BgL_syncz00_bglt) COBJECT(
										((BgL_syncz00_bglt) BgL_nodez00_2235)))->BgL_bodyz00),
							CBOOL(BgL_strictz00_2236));
					}
				else
					{	/* Type/typeof.scm 144 */
						return
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_syncz00_bglt) BgL_nodez00_2235))))->BgL_typez00);
					}
			}
		}

	}



/* &get-type-sequence1270 */
	BgL_typez00_bglt BGl_z62getzd2typezd2sequence1270z62zztype_typeofz00(obj_t
		BgL_envz00_2237, obj_t BgL_nodez00_2238, obj_t BgL_strictz00_2239)
	{
		{	/* Type/typeof.scm 132 */
			{	/* Type/typeof.scm 134 */
				bool_t BgL_test1931z00_2755;

				{	/* Type/typeof.scm 134 */
					BgL_typez00_bglt BgL_arg1514z00_2285;

					BgL_arg1514z00_2285 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_sequencez00_bglt) BgL_nodez00_2238))))->BgL_typez00);
					BgL_test1931z00_2755 =
						(((obj_t) BgL_arg1514z00_2285) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1931z00_2755)
					{	/* Type/typeof.scm 136 */
						obj_t BgL_arg1502z00_2286;

						BgL_arg1502z00_2286 =
							CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
								(((BgL_sequencez00_bglt) COBJECT(
											((BgL_sequencez00_bglt) BgL_nodez00_2238)))->
									BgL_nodesz00)));
						return BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt)
								BgL_arg1502z00_2286), CBOOL(BgL_strictz00_2239));
					}
				else
					{	/* Type/typeof.scm 134 */
						return
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_sequencez00_bglt) BgL_nodez00_2238))))->BgL_typez00);
					}
			}
		}

	}



/* &get-type-var1268 */
	BgL_typez00_bglt BGl_z62getzd2typezd2var1268z62zztype_typeofz00(obj_t
		BgL_envz00_2240, obj_t BgL_nodez00_2241, obj_t BgL_strictz00_2242)
	{
		{	/* Type/typeof.scm 108 */
			{	/* Type/typeof.scm 123 */
				BgL_variablez00_bglt BgL_i1120z00_2288;

				BgL_i1120z00_2288 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2241)))->BgL_variablez00);
				{	/* Type/typeof.scm 125 */
					bool_t BgL_test1932z00_2773;

					{	/* Type/typeof.scm 125 */
						BgL_typez00_bglt BgL_arg1422z00_2289;

						BgL_arg1422z00_2289 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_varz00_bglt) BgL_nodez00_2241))))->BgL_typez00);
						BgL_test1932z00_2773 =
							(((obj_t) BgL_arg1422z00_2289) == BGl_za2_za2z00zztype_cachez00);
					}
					if (BgL_test1932z00_2773)
						{	/* Type/typeof.scm 125 */
							return
								(((BgL_variablez00_bglt) COBJECT(BgL_i1120z00_2288))->
								BgL_typez00);
						}
					else
						{	/* Type/typeof.scm 126 */
							bool_t BgL_test1933z00_2780;

							{	/* Type/typeof.scm 126 */
								bool_t BgL_test1934z00_2781;

								{	/* Type/typeof.scm 126 */
									BgL_typez00_bglt BgL_arg1421z00_2290;

									BgL_arg1421z00_2290 =
										(((BgL_variablez00_bglt) COBJECT(BgL_i1120z00_2288))->
										BgL_typez00);
									{	/* Type/typeof.scm 126 */
										obj_t BgL_classz00_2291;

										BgL_classz00_2291 = BGl_tclassz00zzobject_classz00;
										{	/* Type/typeof.scm 126 */
											BgL_objectz00_bglt BgL_arg1807z00_2292;

											{	/* Type/typeof.scm 126 */
												obj_t BgL_tmpz00_2783;

												BgL_tmpz00_2783 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1421z00_2290));
												BgL_arg1807z00_2292 =
													(BgL_objectz00_bglt) (BgL_tmpz00_2783);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Type/typeof.scm 126 */
													long BgL_idxz00_2293;

													BgL_idxz00_2293 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2292);
													BgL_test1934z00_2781 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2293 + 2L)) == BgL_classz00_2291);
												}
											else
												{	/* Type/typeof.scm 126 */
													bool_t BgL_res1816z00_2296;

													{	/* Type/typeof.scm 126 */
														obj_t BgL_oclassz00_2297;

														{	/* Type/typeof.scm 126 */
															obj_t BgL_arg1815z00_2298;
															long BgL_arg1816z00_2299;

															BgL_arg1815z00_2298 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Type/typeof.scm 126 */
																long BgL_arg1817z00_2300;

																BgL_arg1817z00_2300 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2292);
																BgL_arg1816z00_2299 =
																	(BgL_arg1817z00_2300 - OBJECT_TYPE);
															}
															BgL_oclassz00_2297 =
																VECTOR_REF(BgL_arg1815z00_2298,
																BgL_arg1816z00_2299);
														}
														{	/* Type/typeof.scm 126 */
															bool_t BgL__ortest_1115z00_2301;

															BgL__ortest_1115z00_2301 =
																(BgL_classz00_2291 == BgL_oclassz00_2297);
															if (BgL__ortest_1115z00_2301)
																{	/* Type/typeof.scm 126 */
																	BgL_res1816z00_2296 =
																		BgL__ortest_1115z00_2301;
																}
															else
																{	/* Type/typeof.scm 126 */
																	long BgL_odepthz00_2302;

																	{	/* Type/typeof.scm 126 */
																		obj_t BgL_arg1804z00_2303;

																		BgL_arg1804z00_2303 = (BgL_oclassz00_2297);
																		BgL_odepthz00_2302 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2303);
																	}
																	if ((2L < BgL_odepthz00_2302))
																		{	/* Type/typeof.scm 126 */
																			obj_t BgL_arg1802z00_2304;

																			{	/* Type/typeof.scm 126 */
																				obj_t BgL_arg1803z00_2305;

																				BgL_arg1803z00_2305 =
																					(BgL_oclassz00_2297);
																				BgL_arg1802z00_2304 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2305, 2L);
																			}
																			BgL_res1816z00_2296 =
																				(BgL_arg1802z00_2304 ==
																				BgL_classz00_2291);
																		}
																	else
																		{	/* Type/typeof.scm 126 */
																			BgL_res1816z00_2296 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1934z00_2781 = BgL_res1816z00_2296;
												}
										}
									}
								}
								if (BgL_test1934z00_2781)
									{	/* Type/typeof.scm 126 */
										BgL_typez00_bglt BgL_arg1408z00_2306;
										BgL_typez00_bglt BgL_arg1410z00_2307;

										BgL_arg1408z00_2306 =
											(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_varz00_bglt) BgL_nodez00_2241))))->
											BgL_typez00);
										BgL_arg1410z00_2307 =
											(((BgL_variablez00_bglt) COBJECT(BgL_i1120z00_2288))->
											BgL_typez00);
										BgL_test1933z00_2780 =
											BGl_typezd2subclasszf3z21zzobject_classz00
											(BgL_arg1408z00_2306, BgL_arg1410z00_2307);
									}
								else
									{	/* Type/typeof.scm 126 */
										BgL_test1933z00_2780 = ((bool_t) 0);
									}
							}
							if (BgL_test1933z00_2780)
								{	/* Type/typeof.scm 126 */
									return
										(((BgL_variablez00_bglt) COBJECT(BgL_i1120z00_2288))->
										BgL_typez00);
								}
							else
								{	/* Type/typeof.scm 126 */
									return
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_varz00_bglt) BgL_nodez00_2241))))->BgL_typez00);
								}
						}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztype_typeofz00(void)
	{
		{	/* Type/typeof.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
			BGl_modulezd2initializa7ationz75zztype_coercionz00(116865673L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
			return
				BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1829z00zztype_typeofz00));
		}

	}

#ifdef __cplusplus
}
#endif
