/*===========================================================================*/
/*   (Type/misc.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Type/misc.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TYPE_MISC_TYPE_DEFINITIONS
#define BGL_TYPE_MISC_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;


#endif													// BGL_TYPE_MISC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static long BGl_czd2weightze70z35zztype_miscz00(BgL_typez00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zztype_miscz00 = BUNSPEC;
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zztype_miscz00(void);
	static obj_t BGl_z62typezd2disjointzf3z43zztype_miscz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zztype_miscz00(void);
	static obj_t BGl_objectzd2initzd2zztype_miscz00(void);
	static obj_t BGl_z62isazd2ofzb0zztype_miscz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62typezd2lesszd2specificzf3z91zztype_miscz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_czd2subtypezf3z21zztype_miscz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62czd2subtypezf3z43zztype_miscz00(obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztype_miscz00(void);
	BGL_EXPORTED_DECL bool_t
		BGl_typezd2disjointzf3z21zztype_miscz00(BgL_typez00_bglt, BgL_typez00_bglt);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	BGL_EXPORTED_DECL bool_t
		BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_findzd2coercerzd2zztype_coercionz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_appzd2predicatezd2ofz00zztype_miscz00(BgL_appz00_bglt);
	extern bool_t BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_cnstzd2initzd2zztype_miscz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztype_miscz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztype_miscz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztype_miscz00(void);
	static obj_t BGl_za2isaza2z00zztype_miscz00 = BUNSPEC;
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_za2bnilza2z00zztype_cachez00;
	static obj_t BGl_z62appzd2predicatezd2ofz62zztype_miscz00(obj_t, obj_t);
	extern obj_t BGl_za2epairza2z00zztype_cachez00;
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_isazd2ofzd2zztype_miscz00(BgL_nodez00_bglt);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[18];


	   
		 
		DEFINE_STRING(BGl_string1634z00zztype_miscz00,
		BgL_bgl_string1634za700za7za7t1643za7, "type_misc", 9);
	      DEFINE_STRING(BGl_string1635z00zztype_miscz00,
		BgL_bgl_string1635za700za7za7t1644za7,
		"__object isa? real double uint64 int64 uint32 int32 uint16 int16 uint8 int8 llong elong long int short char ",
		108);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2lesszd2specificzf3zd2envz21zztype_miscz00,
		BgL_bgl_za762typeza7d2lessza7d1645za7,
		BGl_z62typezd2lesszd2specificzf3z91zztype_miscz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2disjointzf3zd2envzf3zztype_miscz00,
		BgL_bgl_za762typeza7d2disjoi1646z00,
		BGl_z62typezd2disjointzf3z43zztype_miscz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_czd2subtypezf3zd2envzf3zztype_miscz00,
		BgL_bgl_za762cza7d2subtypeza7f1647za7,
		BGl_z62czd2subtypezf3z43zztype_miscz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_appzd2predicatezd2ofzd2envzd2zztype_miscz00,
		BgL_bgl_za762appza7d2predica1648z00,
		BGl_z62appzd2predicatezd2ofz62zztype_miscz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_isazd2ofzd2envz00zztype_miscz00,
		BgL_bgl_za762isaza7d2ofza7b0za7za71649za7, BGl_z62isazd2ofzb0zztype_miscz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztype_miscz00));
		     ADD_ROOT((void *) (&BGl_za2isaza2z00zztype_miscz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long
		BgL_checksumz00_2171, char *BgL_fromz00_2172)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztype_miscz00))
				{
					BGl_requirezd2initializa7ationz75zztype_miscz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztype_miscz00();
					BGl_libraryzd2moduleszd2initz00zztype_miscz00();
					BGl_cnstzd2initzd2zztype_miscz00();
					BGl_importedzd2moduleszd2initz00zztype_miscz00();
					return BGl_toplevelzd2initzd2zztype_miscz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztype_miscz00(void)
	{
		{	/* Type/misc.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "type_misc");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "type_misc");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "type_misc");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "type_misc");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"type_misc");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "type_misc");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"type_misc");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"type_misc");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztype_miscz00(void)
	{
		{	/* Type/misc.scm 15 */
			{	/* Type/misc.scm 15 */
				obj_t BgL_cportz00_2160;

				{	/* Type/misc.scm 15 */
					obj_t BgL_stringz00_2167;

					BgL_stringz00_2167 = BGl_string1635z00zztype_miscz00;
					{	/* Type/misc.scm 15 */
						obj_t BgL_startz00_2168;

						BgL_startz00_2168 = BINT(0L);
						{	/* Type/misc.scm 15 */
							obj_t BgL_endz00_2169;

							BgL_endz00_2169 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2167)));
							{	/* Type/misc.scm 15 */

								BgL_cportz00_2160 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2167, BgL_startz00_2168, BgL_endz00_2169);
				}}}}
				{
					long BgL_iz00_2161;

					BgL_iz00_2161 = 17L;
				BgL_loopz00_2162:
					if ((BgL_iz00_2161 == -1L))
						{	/* Type/misc.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Type/misc.scm 15 */
							{	/* Type/misc.scm 15 */
								obj_t BgL_arg1642z00_2163;

								{	/* Type/misc.scm 15 */

									{	/* Type/misc.scm 15 */
										obj_t BgL_locationz00_2165;

										BgL_locationz00_2165 = BBOOL(((bool_t) 0));
										{	/* Type/misc.scm 15 */

											BgL_arg1642z00_2163 =
												BGl_readz00zz__readerz00(BgL_cportz00_2160,
												BgL_locationz00_2165);
										}
									}
								}
								{	/* Type/misc.scm 15 */
									int BgL_tmpz00_2198;

									BgL_tmpz00_2198 = (int) (BgL_iz00_2161);
									CNST_TABLE_SET(BgL_tmpz00_2198, BgL_arg1642z00_2163);
							}}
							{	/* Type/misc.scm 15 */
								int BgL_auxz00_2166;

								BgL_auxz00_2166 = (int) ((BgL_iz00_2161 - 1L));
								{
									long BgL_iz00_2203;

									BgL_iz00_2203 = (long) (BgL_auxz00_2166);
									BgL_iz00_2161 = BgL_iz00_2203;
									goto BgL_loopz00_2162;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztype_miscz00(void)
	{
		{	/* Type/misc.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztype_miscz00(void)
	{
		{	/* Type/misc.scm 15 */
			return (BGl_za2isaza2z00zztype_miscz00 = BFALSE, BUNSPEC);
		}

	}



/* type-less-specific? */
	BGL_EXPORTED_DEF bool_t
		BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(BgL_typez00_bglt BgL_t1z00_3,
		BgL_typez00_bglt BgL_t2z00_4)
	{
		{	/* Type/misc.scm 38 */
			if ((((obj_t) BgL_t1z00_3) == ((obj_t) BgL_t2z00_4)))
				{	/* Type/misc.scm 40 */
					return ((bool_t) 1);
				}
			else
				{	/* Type/misc.scm 40 */
					if ((((obj_t) BgL_t2z00_4) == BGl_za2_za2z00zztype_cachez00))
						{	/* Type/misc.scm 42 */
							return ((bool_t) 0);
						}
					else
						{	/* Type/misc.scm 44 */
							bool_t BgL_test1655z00_2213;

							if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t1z00_3))
								{	/* Type/misc.scm 44 */
									if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t2z00_4))
										{	/* Type/misc.scm 44 */
											BgL_test1655z00_2213 = ((bool_t) 0);
										}
									else
										{	/* Type/misc.scm 44 */
											BgL_test1655z00_2213 = ((bool_t) 1);
										}
								}
							else
								{	/* Type/misc.scm 44 */
									BgL_test1655z00_2213 = ((bool_t) 0);
								}
							if (BgL_test1655z00_2213)
								{	/* Type/misc.scm 45 */
									BgL_typez00_bglt BgL_arg1252z00_1588;

									BgL_arg1252z00_1588 =
										BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_t2z00_4);
									return
										(((obj_t) BgL_t1z00_3) == ((obj_t) BgL_arg1252z00_1588));
								}
							else
								{	/* Type/misc.scm 46 */
									bool_t BgL_test1658z00_2222;

									if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t1z00_3))
										{	/* Type/misc.scm 46 */
											if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t2z00_4))
												{	/* Type/misc.scm 46 */
													BgL_test1658z00_2222 = ((bool_t) 0);
												}
											else
												{	/* Type/misc.scm 46 */
													BgL_test1658z00_2222 = ((bool_t) 1);
												}
										}
									else
										{	/* Type/misc.scm 46 */
											BgL_test1658z00_2222 = ((bool_t) 1);
										}
									if (BgL_test1658z00_2222)
										{	/* Type/misc.scm 46 */
											return ((bool_t) 0);
										}
									else
										{	/* Type/misc.scm 46 */
											if (BGl_typezd2subclasszf3z21zzobject_classz00
												(BgL_t2z00_4, BgL_t1z00_3))
												{	/* Type/misc.scm 48 */
													return ((bool_t) 1);
												}
											else
												{	/* Type/misc.scm 48 */
													if (
														(((obj_t) BgL_t1z00_3) ==
															BGl_za2objza2z00zztype_cachez00))
														{	/* Type/misc.scm 50 */
															return ((bool_t) 1);
														}
													else
														{	/* Type/misc.scm 50 */
															if (
																(((obj_t) BgL_t1z00_3) ==
																	BGl_za2pairzd2nilza2zd2zztype_cachez00))
																{	/* Type/misc.scm 53 */
																	bool_t BgL__ortest_1105z00_1596;

																	BgL__ortest_1105z00_1596 =
																		(
																		((obj_t) BgL_t2z00_4) ==
																		BGl_za2pairza2z00zztype_cachez00);
																	if (BgL__ortest_1105z00_1596)
																		{	/* Type/misc.scm 53 */
																			return BgL__ortest_1105z00_1596;
																		}
																	else
																		{	/* Type/misc.scm 53 */
																			bool_t BgL__ortest_1106z00_1597;

																			BgL__ortest_1106z00_1597 =
																				(
																				((obj_t) BgL_t2z00_4) ==
																				BGl_za2epairza2z00zztype_cachez00);
																			if (BgL__ortest_1106z00_1597)
																				{	/* Type/misc.scm 53 */
																					return BgL__ortest_1106z00_1597;
																				}
																			else
																				{	/* Type/misc.scm 53 */
																					return
																						(
																						((obj_t) BgL_t2z00_4) ==
																						BGl_za2bnilza2z00zztype_cachez00);
																				}
																		}
																}
															else
																{	/* Type/misc.scm 52 */
																	return ((bool_t) 0);
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &type-less-specific? */
	obj_t BGl_z62typezd2lesszd2specificzf3z91zztype_miscz00(obj_t BgL_envz00_2147,
		obj_t BgL_t1z00_2148, obj_t BgL_t2z00_2149)
	{
		{	/* Type/misc.scm 38 */
			return
				BBOOL(BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(
					((BgL_typez00_bglt) BgL_t1z00_2148),
					((BgL_typez00_bglt) BgL_t2z00_2149)));
		}

	}



/* type-disjoint? */
	BGL_EXPORTED_DEF bool_t
		BGl_typezd2disjointzf3z21zztype_miscz00(BgL_typez00_bglt BgL_t1z00_5,
		BgL_typez00_bglt BgL_t2z00_6)
	{
		{	/* Type/misc.scm 63 */
			if ((((obj_t) BgL_t1z00_5) == ((obj_t) BgL_t2z00_6)))
				{	/* Type/misc.scm 65 */
					return ((bool_t) 0);
				}
			else
				{	/* Type/misc.scm 65 */
					if ((((obj_t) BgL_t2z00_6) == BGl_za2_za2z00zztype_cachez00))
						{	/* Type/misc.scm 67 */
							return ((bool_t) 0);
						}
					else
						{	/* Type/misc.scm 69 */
							bool_t BgL_test1669z00_2254;

							{	/* Type/misc.scm 69 */
								bool_t BgL_test1670z00_2255;

								if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t1z00_5))
									{	/* Type/misc.scm 69 */
										if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t2z00_6))
											{	/* Type/misc.scm 69 */
												BgL_test1670z00_2255 = ((bool_t) 0);
											}
										else
											{	/* Type/misc.scm 69 */
												BgL_test1670z00_2255 = ((bool_t) 1);
											}
									}
								else
									{	/* Type/misc.scm 69 */
										BgL_test1670z00_2255 = ((bool_t) 0);
									}
								if (BgL_test1670z00_2255)
									{	/* Type/misc.scm 69 */
										BgL_test1669z00_2254 = ((bool_t) 1);
									}
								else
									{	/* Type/misc.scm 69 */
										if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t1z00_5))
											{	/* Type/misc.scm 70 */
												BgL_test1669z00_2254 = ((bool_t) 0);
											}
										else
											{	/* Type/misc.scm 70 */
												BgL_test1669z00_2254 =
													BGl_bigloozd2typezf3z21zztype_typez00(BgL_t2z00_6);
											}
									}
							}
							if (BgL_test1669z00_2254)
								{	/* Type/misc.scm 73 */
									bool_t BgL_test1674z00_2263;

									if (CBOOL(BGl_findzd2coercerzd2zztype_coercionz00(BgL_t1z00_5,
												BgL_t2z00_6)))
										{	/* Type/misc.scm 73 */
											BgL_test1674z00_2263 = ((bool_t) 1);
										}
									else
										{	/* Type/misc.scm 73 */
											BgL_test1674z00_2263 =
												CBOOL(BGl_findzd2coercerzd2zztype_coercionz00
												(BgL_t2z00_6, BgL_t1z00_5));
										}
									if (BgL_test1674z00_2263)
										{	/* Type/misc.scm 73 */
											return ((bool_t) 0);
										}
									else
										{	/* Type/misc.scm 73 */
											return ((bool_t) 1);
										}
								}
							else
								{	/* Type/misc.scm 74 */
									bool_t BgL_test1676z00_2269;

									if (BGl_typezd2lesszd2specificzf3zf3zztype_miscz00
										(BgL_t1z00_5, BgL_t2z00_6))
										{	/* Type/misc.scm 74 */
											BgL_test1676z00_2269 = ((bool_t) 1);
										}
									else
										{	/* Type/misc.scm 74 */
											BgL_test1676z00_2269 =
												BGl_typezd2lesszd2specificzf3zf3zztype_miscz00
												(BgL_t2z00_6, BgL_t1z00_5);
										}
									if (BgL_test1676z00_2269)
										{	/* Type/misc.scm 74 */
											return ((bool_t) 0);
										}
									else
										{	/* Type/misc.scm 77 */
											bool_t BgL_test1679z00_2273;

											if (CBOOL(BGl_findzd2coercerzd2zztype_coercionz00
													(BgL_t1z00_5, BgL_t2z00_6)))
												{	/* Type/misc.scm 77 */
													BgL_test1679z00_2273 = ((bool_t) 1);
												}
											else
												{	/* Type/misc.scm 77 */
													BgL_test1679z00_2273 =
														CBOOL(BGl_findzd2coercerzd2zztype_coercionz00
														(BgL_t2z00_6, BgL_t1z00_5));
												}
											if (BgL_test1679z00_2273)
												{	/* Type/misc.scm 77 */
													return ((bool_t) 0);
												}
											else
												{	/* Type/misc.scm 77 */
													return ((bool_t) 1);
												}
										}
								}
						}
				}
		}

	}



/* &type-disjoint? */
	obj_t BGl_z62typezd2disjointzf3z43zztype_miscz00(obj_t BgL_envz00_2150,
		obj_t BgL_t1z00_2151, obj_t BgL_t2z00_2152)
	{
		{	/* Type/misc.scm 63 */
			return
				BBOOL(BGl_typezd2disjointzf3z21zztype_miscz00(
					((BgL_typez00_bglt) BgL_t1z00_2151),
					((BgL_typez00_bglt) BgL_t2z00_2152)));
		}

	}



/* c-subtype? */
	BGL_EXPORTED_DEF bool_t BGl_czd2subtypezf3z21zztype_miscz00(BgL_typez00_bglt
		BgL_t1z00_7, BgL_typez00_bglt BgL_t2z00_8)
	{
		{	/* Type/misc.scm 87 */
			{	/* Type/misc.scm 109 */
				bool_t BgL_test1682z00_2283;

				if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t1z00_7))
					{	/* Type/misc.scm 109 */
						BgL_test1682z00_2283 = ((bool_t) 0);
					}
				else
					{	/* Type/misc.scm 109 */
						if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t2z00_8))
							{	/* Type/misc.scm 109 */
								BgL_test1682z00_2283 = ((bool_t) 0);
							}
						else
							{	/* Type/misc.scm 109 */
								BgL_test1682z00_2283 = ((bool_t) 1);
							}
					}
				if (BgL_test1682z00_2283)
					{	/* Type/misc.scm 110 */
						long BgL_w1z00_1626;
						long BgL_w2z00_1627;

						BgL_w1z00_1626 = BGl_czd2weightze70z35zztype_miscz00(BgL_t1z00_7);
						BgL_w2z00_1627 = BGl_czd2weightze70z35zztype_miscz00(BgL_t2z00_8);
						if (((BgL_w1z00_1626 * BgL_w2z00_1627) > 0L))
							{	/* Type/misc.scm 112 */
								return (BgL_w1z00_1626 < BgL_w2z00_1627);
							}
						else
							{	/* Type/misc.scm 112 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Type/misc.scm 109 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* c-weight~0 */
	long BGl_czd2weightze70z35zztype_miscz00(BgL_typez00_bglt BgL_tz00_1632)
	{
		{	/* Type/misc.scm 107 */
			{	/* Type/misc.scm 90 */
				obj_t BgL_casezd2valuezd2_1634;

				BgL_casezd2valuezd2_1634 =
					(((BgL_typez00_bglt) COBJECT(BgL_tz00_1632))->BgL_idz00);
				if ((BgL_casezd2valuezd2_1634 == CNST_TABLE_REF(0)))
					{	/* Type/misc.scm 90 */
						return 1L;
					}
				else
					{	/* Type/misc.scm 90 */
						if ((BgL_casezd2valuezd2_1634 == CNST_TABLE_REF(1)))
							{	/* Type/misc.scm 90 */
								return 2L;
							}
						else
							{	/* Type/misc.scm 90 */
								if ((BgL_casezd2valuezd2_1634 == CNST_TABLE_REF(2)))
									{	/* Type/misc.scm 90 */
										return 3L;
									}
								else
									{	/* Type/misc.scm 90 */
										if ((BgL_casezd2valuezd2_1634 == CNST_TABLE_REF(3)))
											{	/* Type/misc.scm 90 */
												return 4L;
											}
										else
											{	/* Type/misc.scm 90 */
												if ((BgL_casezd2valuezd2_1634 == CNST_TABLE_REF(4)))
													{	/* Type/misc.scm 90 */
														return 4L;
													}
												else
													{	/* Type/misc.scm 90 */
														if ((BgL_casezd2valuezd2_1634 == CNST_TABLE_REF(5)))
															{	/* Type/misc.scm 90 */
																return 5L;
															}
														else
															{	/* Type/misc.scm 90 */
																if (
																	(BgL_casezd2valuezd2_1634 ==
																		CNST_TABLE_REF(6)))
																	{	/* Type/misc.scm 90 */
																		return 6L;
																	}
																else
																	{	/* Type/misc.scm 90 */
																		if (
																			(BgL_casezd2valuezd2_1634 ==
																				CNST_TABLE_REF(7)))
																			{	/* Type/misc.scm 90 */
																				return 7L;
																			}
																		else
																			{	/* Type/misc.scm 90 */
																				if (
																					(BgL_casezd2valuezd2_1634 ==
																						CNST_TABLE_REF(8)))
																					{	/* Type/misc.scm 90 */
																						return 8L;
																					}
																				else
																					{	/* Type/misc.scm 90 */
																						if (
																							(BgL_casezd2valuezd2_1634 ==
																								CNST_TABLE_REF(9)))
																							{	/* Type/misc.scm 90 */
																								return 9L;
																							}
																						else
																							{	/* Type/misc.scm 90 */
																								if (
																									(BgL_casezd2valuezd2_1634 ==
																										CNST_TABLE_REF(10)))
																									{	/* Type/misc.scm 90 */
																										return 10L;
																									}
																								else
																									{	/* Type/misc.scm 90 */
																										if (
																											(BgL_casezd2valuezd2_1634
																												== CNST_TABLE_REF(11)))
																											{	/* Type/misc.scm 90 */
																												return 11L;
																											}
																										else
																											{	/* Type/misc.scm 90 */
																												if (
																													(BgL_casezd2valuezd2_1634
																														==
																														CNST_TABLE_REF(12)))
																													{	/* Type/misc.scm 90 */
																														return 12L;
																													}
																												else
																													{	/* Type/misc.scm 90 */
																														if (
																															(BgL_casezd2valuezd2_1634
																																==
																																CNST_TABLE_REF
																																(13)))
																															{	/* Type/misc.scm 90 */
																																return 13L;
																															}
																														else
																															{	/* Type/misc.scm 90 */
																																if (
																																	(BgL_casezd2valuezd2_1634
																																		==
																																		CNST_TABLE_REF
																																		(14)))
																																	{	/* Type/misc.scm 90 */
																																		return -1L;
																																	}
																																else
																																	{	/* Type/misc.scm 90 */
																																		if (
																																			(BgL_casezd2valuezd2_1634
																																				==
																																				CNST_TABLE_REF
																																				(15)))
																																			{	/* Type/misc.scm 90 */
																																				return
																																					-2L;
																																			}
																																		else
																																			{	/* Type/misc.scm 90 */
																																				return
																																					-1L;
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &c-subtype? */
	obj_t BGl_z62czd2subtypezf3z43zztype_miscz00(obj_t BgL_envz00_2153,
		obj_t BgL_t1z00_2154, obj_t BgL_t2z00_2155)
	{
		{	/* Type/misc.scm 87 */
			return
				BBOOL(BGl_czd2subtypezf3z21zztype_miscz00(
					((BgL_typez00_bglt) BgL_t1z00_2154),
					((BgL_typez00_bglt) BgL_t2z00_2155)));
		}

	}



/* isa-of */
	BGL_EXPORTED_DEF obj_t BGl_isazd2ofzd2zztype_miscz00(BgL_nodez00_bglt
		BgL_nodez00_9)
	{
		{	/* Type/misc.scm 122 */
			{	/* Type/misc.scm 123 */
				bool_t BgL_test1703z00_2347;

				{	/* Type/misc.scm 123 */
					obj_t BgL_classz00_1889;

					BgL_classz00_1889 = BGl_appz00zzast_nodez00;
					{	/* Type/misc.scm 123 */
						BgL_objectz00_bglt BgL_arg1807z00_1891;

						{	/* Type/misc.scm 123 */
							obj_t BgL_tmpz00_2348;

							BgL_tmpz00_2348 = ((obj_t) BgL_nodez00_9);
							BgL_arg1807z00_1891 = (BgL_objectz00_bglt) (BgL_tmpz00_2348);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Type/misc.scm 123 */
								long BgL_idxz00_1897;

								BgL_idxz00_1897 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1891);
								BgL_test1703z00_2347 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_1897 + 3L)) == BgL_classz00_1889);
							}
						else
							{	/* Type/misc.scm 123 */
								bool_t BgL_res1627z00_1922;

								{	/* Type/misc.scm 123 */
									obj_t BgL_oclassz00_1905;

									{	/* Type/misc.scm 123 */
										obj_t BgL_arg1815z00_1913;
										long BgL_arg1816z00_1914;

										BgL_arg1815z00_1913 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Type/misc.scm 123 */
											long BgL_arg1817z00_1915;

											BgL_arg1817z00_1915 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1891);
											BgL_arg1816z00_1914 = (BgL_arg1817z00_1915 - OBJECT_TYPE);
										}
										BgL_oclassz00_1905 =
											VECTOR_REF(BgL_arg1815z00_1913, BgL_arg1816z00_1914);
									}
									{	/* Type/misc.scm 123 */
										bool_t BgL__ortest_1115z00_1906;

										BgL__ortest_1115z00_1906 =
											(BgL_classz00_1889 == BgL_oclassz00_1905);
										if (BgL__ortest_1115z00_1906)
											{	/* Type/misc.scm 123 */
												BgL_res1627z00_1922 = BgL__ortest_1115z00_1906;
											}
										else
											{	/* Type/misc.scm 123 */
												long BgL_odepthz00_1907;

												{	/* Type/misc.scm 123 */
													obj_t BgL_arg1804z00_1908;

													BgL_arg1804z00_1908 = (BgL_oclassz00_1905);
													BgL_odepthz00_1907 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_1908);
												}
												if ((3L < BgL_odepthz00_1907))
													{	/* Type/misc.scm 123 */
														obj_t BgL_arg1802z00_1910;

														{	/* Type/misc.scm 123 */
															obj_t BgL_arg1803z00_1911;

															BgL_arg1803z00_1911 = (BgL_oclassz00_1905);
															BgL_arg1802z00_1910 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1911,
																3L);
														}
														BgL_res1627z00_1922 =
															(BgL_arg1802z00_1910 == BgL_classz00_1889);
													}
												else
													{	/* Type/misc.scm 123 */
														BgL_res1627z00_1922 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test1703z00_2347 = BgL_res1627z00_1922;
							}
					}
				}
				if (BgL_test1703z00_2347)
					{	/* Type/misc.scm 123 */
						{	/* Type/misc.scm 124 */
							bool_t BgL_test1707z00_2370;

							{	/* Type/misc.scm 124 */
								obj_t BgL_objz00_1923;

								BgL_objz00_1923 = BGl_za2isaza2z00zztype_miscz00;
								{	/* Type/misc.scm 124 */
									obj_t BgL_classz00_1924;

									BgL_classz00_1924 = BGl_globalz00zzast_varz00;
									if (BGL_OBJECTP(BgL_objz00_1923))
										{	/* Type/misc.scm 124 */
											BgL_objectz00_bglt BgL_arg1807z00_1926;

											BgL_arg1807z00_1926 =
												(BgL_objectz00_bglt) (BgL_objz00_1923);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Type/misc.scm 124 */
													long BgL_idxz00_1932;

													BgL_idxz00_1932 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1926);
													BgL_test1707z00_2370 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1932 + 2L)) == BgL_classz00_1924);
												}
											else
												{	/* Type/misc.scm 124 */
													bool_t BgL_res1628z00_1957;

													{	/* Type/misc.scm 124 */
														obj_t BgL_oclassz00_1940;

														{	/* Type/misc.scm 124 */
															obj_t BgL_arg1815z00_1948;
															long BgL_arg1816z00_1949;

															BgL_arg1815z00_1948 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Type/misc.scm 124 */
																long BgL_arg1817z00_1950;

																BgL_arg1817z00_1950 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1926);
																BgL_arg1816z00_1949 =
																	(BgL_arg1817z00_1950 - OBJECT_TYPE);
															}
															BgL_oclassz00_1940 =
																VECTOR_REF(BgL_arg1815z00_1948,
																BgL_arg1816z00_1949);
														}
														{	/* Type/misc.scm 124 */
															bool_t BgL__ortest_1115z00_1941;

															BgL__ortest_1115z00_1941 =
																(BgL_classz00_1924 == BgL_oclassz00_1940);
															if (BgL__ortest_1115z00_1941)
																{	/* Type/misc.scm 124 */
																	BgL_res1628z00_1957 =
																		BgL__ortest_1115z00_1941;
																}
															else
																{	/* Type/misc.scm 124 */
																	long BgL_odepthz00_1942;

																	{	/* Type/misc.scm 124 */
																		obj_t BgL_arg1804z00_1943;

																		BgL_arg1804z00_1943 = (BgL_oclassz00_1940);
																		BgL_odepthz00_1942 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1943);
																	}
																	if ((2L < BgL_odepthz00_1942))
																		{	/* Type/misc.scm 124 */
																			obj_t BgL_arg1802z00_1945;

																			{	/* Type/misc.scm 124 */
																				obj_t BgL_arg1803z00_1946;

																				BgL_arg1803z00_1946 =
																					(BgL_oclassz00_1940);
																				BgL_arg1802z00_1945 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1946, 2L);
																			}
																			BgL_res1628z00_1957 =
																				(BgL_arg1802z00_1945 ==
																				BgL_classz00_1924);
																		}
																	else
																		{	/* Type/misc.scm 124 */
																			BgL_res1628z00_1957 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1707z00_2370 = BgL_res1628z00_1957;
												}
										}
									else
										{	/* Type/misc.scm 124 */
											BgL_test1707z00_2370 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test1707z00_2370)
								{	/* Type/misc.scm 124 */
									BFALSE;
								}
							else
								{	/* Type/misc.scm 124 */
									BGl_za2isaza2z00zztype_miscz00 =
										BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF
										(16), CNST_TABLE_REF(17));
								}
						}
						{	/* Type/misc.scm 127 */
							bool_t BgL_test1716z00_2396;

							{	/* Type/misc.scm 127 */
								obj_t BgL_tmpz00_2397;

								BgL_tmpz00_2397 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_9)))->BgL_argsz00);
								BgL_test1716z00_2396 = PAIRP(BgL_tmpz00_2397);
							}
							if (BgL_test1716z00_2396)
								{	/* Type/misc.scm 128 */
									bool_t BgL_test1718z00_2401;

									{	/* Type/misc.scm 128 */
										bool_t BgL_test1719z00_2402;

										{	/* Type/misc.scm 128 */
											BgL_variablez00_bglt BgL_arg1348z00_1682;

											BgL_arg1348z00_1682 =
												(((BgL_varz00_bglt) COBJECT(
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_9)))->
															BgL_funz00)))->BgL_variablez00);
											BgL_test1719z00_2402 =
												(((obj_t) BgL_arg1348z00_1682) ==
												BGl_za2isaza2z00zztype_miscz00);
										}
										if (BgL_test1719z00_2402)
											{	/* Type/misc.scm 129 */
												bool_t BgL_test1720z00_2408;

												{	/* Type/misc.scm 129 */
													obj_t BgL_arg1343z00_1680;

													{	/* Type/misc.scm 129 */
														obj_t BgL_pairz00_1959;

														BgL_pairz00_1959 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_9)))->
															BgL_argsz00);
														BgL_arg1343z00_1680 = CAR(CDR(BgL_pairz00_1959));
													}
													{	/* Type/misc.scm 129 */
														obj_t BgL_classz00_1963;

														BgL_classz00_1963 = BGl_varz00zzast_nodez00;
														if (BGL_OBJECTP(BgL_arg1343z00_1680))
															{	/* Type/misc.scm 129 */
																BgL_objectz00_bglt BgL_arg1807z00_1965;

																BgL_arg1807z00_1965 =
																	(BgL_objectz00_bglt) (BgL_arg1343z00_1680);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Type/misc.scm 129 */
																		long BgL_idxz00_1971;

																		BgL_idxz00_1971 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_1965);
																		BgL_test1720z00_2408 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_1971 + 2L)) ==
																			BgL_classz00_1963);
																	}
																else
																	{	/* Type/misc.scm 129 */
																		bool_t BgL_res1629z00_1996;

																		{	/* Type/misc.scm 129 */
																			obj_t BgL_oclassz00_1979;

																			{	/* Type/misc.scm 129 */
																				obj_t BgL_arg1815z00_1987;
																				long BgL_arg1816z00_1988;

																				BgL_arg1815z00_1987 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Type/misc.scm 129 */
																					long BgL_arg1817z00_1989;

																					BgL_arg1817z00_1989 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_1965);
																					BgL_arg1816z00_1988 =
																						(BgL_arg1817z00_1989 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_1979 =
																					VECTOR_REF(BgL_arg1815z00_1987,
																					BgL_arg1816z00_1988);
																			}
																			{	/* Type/misc.scm 129 */
																				bool_t BgL__ortest_1115z00_1980;

																				BgL__ortest_1115z00_1980 =
																					(BgL_classz00_1963 ==
																					BgL_oclassz00_1979);
																				if (BgL__ortest_1115z00_1980)
																					{	/* Type/misc.scm 129 */
																						BgL_res1629z00_1996 =
																							BgL__ortest_1115z00_1980;
																					}
																				else
																					{	/* Type/misc.scm 129 */
																						long BgL_odepthz00_1981;

																						{	/* Type/misc.scm 129 */
																							obj_t BgL_arg1804z00_1982;

																							BgL_arg1804z00_1982 =
																								(BgL_oclassz00_1979);
																							BgL_odepthz00_1981 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_1982);
																						}
																						if ((2L < BgL_odepthz00_1981))
																							{	/* Type/misc.scm 129 */
																								obj_t BgL_arg1802z00_1984;

																								{	/* Type/misc.scm 129 */
																									obj_t BgL_arg1803z00_1985;

																									BgL_arg1803z00_1985 =
																										(BgL_oclassz00_1979);
																									BgL_arg1802z00_1984 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_1985, 2L);
																								}
																								BgL_res1629z00_1996 =
																									(BgL_arg1802z00_1984 ==
																									BgL_classz00_1963);
																							}
																						else
																							{	/* Type/misc.scm 129 */
																								BgL_res1629z00_1996 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1720z00_2408 = BgL_res1629z00_1996;
																	}
															}
														else
															{	/* Type/misc.scm 129 */
																BgL_test1720z00_2408 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test1720z00_2408)
													{	/* Type/misc.scm 130 */
														BgL_variablez00_bglt BgL_arg1339z00_1677;

														{
															BgL_varz00_bglt BgL_auxz00_2435;

															{
																obj_t BgL_auxz00_2436;

																{	/* Type/misc.scm 130 */
																	obj_t BgL_pairz00_1997;

																	BgL_pairz00_1997 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_nodez00_9)))->
																		BgL_argsz00);
																	{	/* Type/misc.scm 130 */
																		obj_t BgL_pairz00_2000;

																		BgL_pairz00_2000 = CDR(BgL_pairz00_1997);
																		BgL_auxz00_2436 = CAR(BgL_pairz00_2000);
																	}
																}
																BgL_auxz00_2435 =
																	((BgL_varz00_bglt) BgL_auxz00_2436);
															}
															BgL_arg1339z00_1677 =
																(((BgL_varz00_bglt) COBJECT(BgL_auxz00_2435))->
																BgL_variablez00);
														}
														{	/* Type/misc.scm 130 */
															obj_t BgL_classz00_2002;

															BgL_classz00_2002 = BGl_globalz00zzast_varz00;
															{	/* Type/misc.scm 130 */
																BgL_objectz00_bglt BgL_arg1807z00_2004;

																{	/* Type/misc.scm 130 */
																	obj_t BgL_tmpz00_2443;

																	BgL_tmpz00_2443 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1339z00_1677));
																	BgL_arg1807z00_2004 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_2443);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Type/misc.scm 130 */
																		long BgL_idxz00_2010;

																		BgL_idxz00_2010 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2004);
																		BgL_test1718z00_2401 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2010 + 2L)) ==
																			BgL_classz00_2002);
																	}
																else
																	{	/* Type/misc.scm 130 */
																		bool_t BgL_res1630z00_2035;

																		{	/* Type/misc.scm 130 */
																			obj_t BgL_oclassz00_2018;

																			{	/* Type/misc.scm 130 */
																				obj_t BgL_arg1815z00_2026;
																				long BgL_arg1816z00_2027;

																				BgL_arg1815z00_2026 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Type/misc.scm 130 */
																					long BgL_arg1817z00_2028;

																					BgL_arg1817z00_2028 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2004);
																					BgL_arg1816z00_2027 =
																						(BgL_arg1817z00_2028 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2018 =
																					VECTOR_REF(BgL_arg1815z00_2026,
																					BgL_arg1816z00_2027);
																			}
																			{	/* Type/misc.scm 130 */
																				bool_t BgL__ortest_1115z00_2019;

																				BgL__ortest_1115z00_2019 =
																					(BgL_classz00_2002 ==
																					BgL_oclassz00_2018);
																				if (BgL__ortest_1115z00_2019)
																					{	/* Type/misc.scm 130 */
																						BgL_res1630z00_2035 =
																							BgL__ortest_1115z00_2019;
																					}
																				else
																					{	/* Type/misc.scm 130 */
																						long BgL_odepthz00_2020;

																						{	/* Type/misc.scm 130 */
																							obj_t BgL_arg1804z00_2021;

																							BgL_arg1804z00_2021 =
																								(BgL_oclassz00_2018);
																							BgL_odepthz00_2020 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2021);
																						}
																						if ((2L < BgL_odepthz00_2020))
																							{	/* Type/misc.scm 130 */
																								obj_t BgL_arg1802z00_2023;

																								{	/* Type/misc.scm 130 */
																									obj_t BgL_arg1803z00_2024;

																									BgL_arg1803z00_2024 =
																										(BgL_oclassz00_2018);
																									BgL_arg1802z00_2023 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2024, 2L);
																								}
																								BgL_res1630z00_2035 =
																									(BgL_arg1802z00_2023 ==
																									BgL_classz00_2002);
																							}
																						else
																							{	/* Type/misc.scm 130 */
																								BgL_res1630z00_2035 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1718z00_2401 = BgL_res1630z00_2035;
																	}
															}
														}
													}
												else
													{	/* Type/misc.scm 129 */
														BgL_test1718z00_2401 = ((bool_t) 0);
													}
											}
										else
											{	/* Type/misc.scm 128 */
												BgL_test1718z00_2401 = ((bool_t) 0);
											}
									}
									if (BgL_test1718z00_2401)
										{	/* Type/misc.scm 131 */
											obj_t BgL_arg1331z00_1671;

											{
												BgL_variablez00_bglt BgL_auxz00_2466;

												{
													BgL_globalz00_bglt BgL_auxz00_2467;

													{
														BgL_variablez00_bglt BgL_auxz00_2468;

														{
															BgL_varz00_bglt BgL_auxz00_2469;

															{
																obj_t BgL_auxz00_2470;

																{	/* Type/misc.scm 131 */
																	obj_t BgL_pairz00_2036;

																	BgL_pairz00_2036 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_nodez00_9)))->
																		BgL_argsz00);
																	{	/* Type/misc.scm 131 */
																		obj_t BgL_pairz00_2039;

																		BgL_pairz00_2039 = CDR(BgL_pairz00_2036);
																		BgL_auxz00_2470 = CAR(BgL_pairz00_2039);
																	}
																}
																BgL_auxz00_2469 =
																	((BgL_varz00_bglt) BgL_auxz00_2470);
															}
															BgL_auxz00_2468 =
																(((BgL_varz00_bglt) COBJECT(BgL_auxz00_2469))->
																BgL_variablez00);
														}
														BgL_auxz00_2467 =
															((BgL_globalz00_bglt) BgL_auxz00_2468);
													}
													BgL_auxz00_2466 =
														((BgL_variablez00_bglt) BgL_auxz00_2467);
												}
												BgL_arg1331z00_1671 =
													(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_2466))->
													BgL_idz00);
											}
											return
												((obj_t)
												BGl_findzd2typezd2zztype_envz00(BgL_arg1331z00_1671));
										}
									else
										{	/* Type/misc.scm 128 */
											return BFALSE;
										}
								}
							else
								{	/* Type/misc.scm 127 */
									return BFALSE;
								}
						}
					}
				else
					{	/* Type/misc.scm 123 */
						return BFALSE;
					}
			}
		}

	}



/* &isa-of */
	obj_t BGl_z62isazd2ofzb0zztype_miscz00(obj_t BgL_envz00_2156,
		obj_t BgL_nodez00_2157)
	{
		{	/* Type/misc.scm 122 */
			return
				BGl_isazd2ofzd2zztype_miscz00(((BgL_nodez00_bglt) BgL_nodez00_2157));
		}

	}



/* app-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_appzd2predicatezd2ofz00zztype_miscz00(BgL_appz00_bglt BgL_nodez00_10)
	{
		{	/* Type/misc.scm 136 */
			{	/* Type/misc.scm 138 */
				BgL_valuez00_bglt BgL_valz00_1686;

				BgL_valz00_1686 =
					(((BgL_variablez00_bglt) COBJECT(
							(((BgL_varz00_bglt) COBJECT(
										(((BgL_appz00_bglt) COBJECT(BgL_nodez00_10))->
											BgL_funz00)))->BgL_variablez00)))->BgL_valuez00);
				{	/* Type/misc.scm 139 */
					obj_t BgL__ortest_1111z00_1687;

					BgL__ortest_1111z00_1687 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_valz00_1686)))->BgL_predicatezd2ofzd2);
					if (CBOOL(BgL__ortest_1111z00_1687))
						{	/* Type/misc.scm 139 */
							return BgL__ortest_1111z00_1687;
						}
					else
						{	/* Type/misc.scm 139 */
							return
								BGl_isazd2ofzd2zztype_miscz00(
								((BgL_nodez00_bglt) BgL_nodez00_10));
						}
				}
			}
		}

	}



/* &app-predicate-of */
	obj_t BGl_z62appzd2predicatezd2ofz62zztype_miscz00(obj_t BgL_envz00_2158,
		obj_t BgL_nodez00_2159)
	{
		{	/* Type/misc.scm 136 */
			return
				BGl_appzd2predicatezd2ofz00zztype_miscz00(
				((BgL_appz00_bglt) BgL_nodez00_2159));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztype_miscz00(void)
	{
		{	/* Type/misc.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztype_miscz00(void)
	{
		{	/* Type/misc.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztype_miscz00(void)
	{
		{	/* Type/misc.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztype_miscz00(void)
	{
		{	/* Type/misc.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1634z00zztype_miscz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1634z00zztype_miscz00));
			BGl_modulezd2initializa7ationz75zztype_coercionz00(116865673L,
				BSTRING_TO_STRING(BGl_string1634z00zztype_miscz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1634z00zztype_miscz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1634z00zztype_miscz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1634z00zztype_miscz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1634z00zztype_miscz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1634z00zztype_miscz00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1634z00zztype_miscz00));
		}

	}

#ifdef __cplusplus
}
#endif
