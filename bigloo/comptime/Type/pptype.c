/*===========================================================================*/
/*   (Type/pptype.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Type/pptype.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TYPE_PPTYPE_TYPE_DEFINITIONS
#define BGL_TYPE_PPTYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;


#endif													// BGL_TYPE_PPTYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zztype_pptypez00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zztype_pptypez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zztype_pptypez00(void);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztype_pptypez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2typezd2ze3stringze3zztype_pptypez00(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztype_pptypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	static obj_t BGl_z62variablezd2typezd2ze3stringz81zztype_pptypez00(obj_t,
		obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zztype_pptypez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztype_pptypez00(void);
	static obj_t BGl_gczd2rootszd2initz00zztype_pptypez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_functionzd2typezd2ze3stringze3zztype_pptypez00(BgL_variablez00_bglt);
	static obj_t BGl_z62functionzd2typezd2ze3stringz81zztype_pptypez00(obj_t,
		obj_t);
	static obj_t BGl_loopze70ze7zztype_pptypez00(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1173z00zztype_pptypez00,
		BgL_bgl_string1173za700za7za7t1177za7, " -> ", 4);
	      DEFINE_STRING(BGl_string1174z00zztype_pptypez00,
		BgL_bgl_string1174za700za7za7t1178za7, "", 0);
	      DEFINE_STRING(BGl_string1175z00zztype_pptypez00,
		BgL_bgl_string1175za700za7za7t1179za7, " x ", 3);
	      DEFINE_STRING(BGl_string1176z00zztype_pptypez00,
		BgL_bgl_string1176za700za7za7t1180za7, "type_pptype", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2typezd2ze3stringzd2envz31zztype_pptypez00,
		BgL_bgl_za762variableza7d2ty1181z00,
		BGl_z62variablezd2typezd2ze3stringz81zztype_pptypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_functionzd2typezd2ze3stringzd2envz31zztype_pptypez00,
		BgL_bgl_za762functionza7d2ty1182z00,
		BGl_z62functionzd2typezd2ze3stringz81zztype_pptypez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztype_pptypez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztype_pptypez00(long
		BgL_checksumz00_534, char *BgL_fromz00_535)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztype_pptypez00))
				{
					BGl_requirezd2initializa7ationz75zztype_pptypez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztype_pptypez00();
					BGl_libraryzd2moduleszd2initz00zztype_pptypez00();
					BGl_importedzd2moduleszd2initz00zztype_pptypez00();
					return BGl_methodzd2initzd2zztype_pptypez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztype_pptypez00(void)
	{
		{	/* Type/pptype.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "type_pptype");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "type_pptype");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"type_pptype");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "type_pptype");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztype_pptypez00(void)
	{
		{	/* Type/pptype.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* function-type->string */
	BGL_EXPORTED_DEF obj_t
		BGl_functionzd2typezd2ze3stringze3zztype_pptypez00(BgL_variablez00_bglt
		BgL_variablez00_4)
	{
		{	/* Type/pptype.scm 32 */
			{	/* Type/pptype.scm 33 */
				BgL_valuez00_bglt BgL_sfunz00_448;

				BgL_sfunz00_448 =
					(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_4))->BgL_valuez00);
				{	/* Type/pptype.scm 34 */
					obj_t BgL_arg1102z00_449;
					obj_t BgL_arg1103z00_450;

					{	/* Type/pptype.scm 34 */
						obj_t BgL_g1060z00_451;

						BgL_g1060z00_451 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_sfunz00_448)))->BgL_argsz00);
						BgL_arg1102z00_449 =
							BGl_loopze70ze7zztype_pptypez00(BgL_g1060z00_451);
					}
					{	/* Type/pptype.scm 47 */
						obj_t BgL_arg1137z00_468;

						{	/* Type/pptype.scm 47 */
							obj_t BgL_arg1138z00_469;

							BgL_arg1138z00_469 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_4))->
											BgL_typez00)))->BgL_idz00);
							{	/* Type/pptype.scm 47 */
								obj_t BgL_arg1455z00_525;

								BgL_arg1455z00_525 = SYMBOL_TO_STRING(BgL_arg1138z00_469);
								BgL_arg1137z00_468 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_525);
							}
						}
						BgL_arg1103z00_450 =
							BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(BgL_arg1137z00_468);
					}
					return
						string_append_3(BgL_arg1102z00_449,
						BGl_string1173z00zztype_pptypez00, BgL_arg1103z00_450);
				}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zztype_pptypez00(obj_t BgL_argsz00_453)
	{
		{	/* Type/pptype.scm 34 */
			if (NULLP(BgL_argsz00_453))
				{	/* Type/pptype.scm 35 */
					return BGl_string1174z00zztype_pptypez00;
				}
			else
				{	/* Type/pptype.scm 40 */
					obj_t BgL_arg1114z00_456;
					obj_t BgL_arg1115z00_457;
					obj_t BgL_arg1122z00_458;

					{	/* Type/pptype.scm 40 */
						obj_t BgL_arg1123z00_459;

						{	/* Type/pptype.scm 40 */
							obj_t BgL_arg1125z00_460;

							BgL_arg1125z00_460 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt)
															CAR(
																((obj_t) BgL_argsz00_453))))))->BgL_typez00)))->
								BgL_idz00);
							{	/* Type/pptype.scm 39 */
								obj_t BgL_arg1455z00_519;

								BgL_arg1455z00_519 = SYMBOL_TO_STRING(BgL_arg1125z00_460);
								BgL_arg1123z00_459 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_519);
							}
						}
						BgL_arg1114z00_456 =
							BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(BgL_arg1123z00_459);
					}
					if (NULLP(CDR(((obj_t) BgL_argsz00_453))))
						{	/* Type/pptype.scm 41 */
							BgL_arg1115z00_457 = BGl_string1174z00zztype_pptypez00;
						}
					else
						{	/* Type/pptype.scm 41 */
							BgL_arg1115z00_457 = BGl_string1175z00zztype_pptypez00;
						}
					{	/* Type/pptype.scm 44 */
						obj_t BgL_arg1132z00_466;

						BgL_arg1132z00_466 = CDR(((obj_t) BgL_argsz00_453));
						BgL_arg1122z00_458 =
							BGl_loopze70ze7zztype_pptypez00(BgL_arg1132z00_466);
					}
					return
						string_append_3(BgL_arg1114z00_456, BgL_arg1115z00_457,
						BgL_arg1122z00_458);
				}
		}

	}



/* &function-type->string */
	obj_t BGl_z62functionzd2typezd2ze3stringz81zztype_pptypez00(obj_t
		BgL_envz00_530, obj_t BgL_variablez00_531)
	{
		{	/* Type/pptype.scm 32 */
			return
				BGl_functionzd2typezd2ze3stringze3zztype_pptypez00(
				((BgL_variablez00_bglt) BgL_variablez00_531));
		}

	}



/* variable-type->string */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2typezd2ze3stringze3zztype_pptypez00(BgL_variablez00_bglt
		BgL_variablez00_5)
	{
		{	/* Type/pptype.scm 52 */
			{	/* Type/pptype.scm 53 */
				obj_t BgL_arg1141z00_471;

				{	/* Type/pptype.scm 53 */
					obj_t BgL_arg1142z00_472;

					BgL_arg1142z00_472 =
						(((BgL_typez00_bglt) COBJECT(
								(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_5))->
									BgL_typez00)))->BgL_idz00);
					{	/* Type/pptype.scm 53 */
						obj_t BgL_arg1455z00_529;

						BgL_arg1455z00_529 = SYMBOL_TO_STRING(BgL_arg1142z00_472);
						BgL_arg1141z00_471 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_529);
					}
				}
				return
					BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(BgL_arg1141z00_471);
			}
		}

	}



/* &variable-type->string */
	obj_t BGl_z62variablezd2typezd2ze3stringz81zztype_pptypez00(obj_t
		BgL_envz00_532, obj_t BgL_variablez00_533)
	{
		{	/* Type/pptype.scm 52 */
			return
				BGl_variablezd2typezd2ze3stringze3zztype_pptypez00(
				((BgL_variablez00_bglt) BgL_variablez00_533));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztype_pptypez00(void)
	{
		{	/* Type/pptype.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztype_pptypez00(void)
	{
		{	/* Type/pptype.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztype_pptypez00(void)
	{
		{	/* Type/pptype.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztype_pptypez00(void)
	{
		{	/* Type/pptype.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1176z00zztype_pptypez00));
			return
				BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1176z00zztype_pptypez00));
		}

	}

#ifdef __cplusplus
}
#endif
