/*===========================================================================*/
/*   (Type/type.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Type/type.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TYPE_TYPE_TYPE_DEFINITIONS
#define BGL_TYPE_TYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_TYPE_TYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_typezd2parentszd2zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2pointedzd2tozd2byzb0zztype_typez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62typezd2namezd2setz12z70zztype_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztype_typez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2pointedzd2tozd2byzd2setz12z12zztype_typez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62foreignzd2typezf3z43zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2aliaszb0zztype_typez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2locationzd2setz12z12zztype_typez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2coercezd2toz00zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2namezb0zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2occurrencezd2setz12z70zztype_typez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2coercezd2tozd2setz12zc0zztype_typez00(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62getzd2aliasedzd2typez62zztype_typez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31123ze3ze5zztype_typez00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zztype_typez00(void);
	static obj_t BGl_z62typezd2importzd2locationzd2setz12za2zztype_typez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2occurrencezd2setz12z12zztype_typez00(BgL_typez00_bglt, int);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2magiczf3zd2setz12ze1zztype_typez00(BgL_typez00_bglt, bool_t);
	static obj_t BGl_z62zc3z04anonymousza31132ze3ze5zztype_typez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_typezd2z42z90zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2importzd2locationz62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zztype_typez00(void);
	static obj_t BGl_z62zc3z04anonymousza31206ze3ze5zztype_typez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_typezd2namezd2zztype_typez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zztype_typez00(void);
	static obj_t BGl_z62zc3z04anonymousza31231ze3ze5zztype_typez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2namezd2setz12z12zztype_typez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31223ze3ze5zztype_typez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31215ze3ze5zztype_typez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2importzd2locationz00zztype_typez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_typezd2idzd2zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2occurrencezd2incr1073z62zztype_typez00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_typez00zztype_typez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2importzd2locationzd2setz12zc0zztype_typez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2locationzd2zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31322ze3ze5zztype_typez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31144ze3ze5zztype_typez00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_typezd2magiczf3z21zztype_typez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62makezd2typezb0zztype_typez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_typezd2classzd2zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31315ze3ze5zztype_typez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31307ze3ze5zztype_typez00(obj_t);
	static obj_t BGl_z62bigloozd2typezf3z43zztype_typez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztype_typez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2magiczf3zd2setz12z83zztype_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62typezd2classzb0zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31252ze3ze5zztype_typez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31155ze3ze5zztype_typez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2parentszd2setz12z12zztype_typez00(BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1104z62zztype_typez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_makezd2typezd2zztype_typez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, int);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2tvectorzd2setz12z12zztype_typez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62typezd2occurrencezd2incrementz12z70zztype_typez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2aliaszd2setz12z12zztype_typez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_foreignzd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2aliaszd2setz12z70zztype_typez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1115z62zztype_typez00(obj_t);
	static obj_t BGl_z62typezd2parentszb0zztype_typez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_typezd2tvectorzd2zztype_typez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2pointedzd2tozd2byzd2zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62lambda1204z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1205z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezd2siza7ez17zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31239ze3ze5zztype_typez00(obj_t);
	static obj_t BGl_z62typezd2z42zf2zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2siza7ezd2setz12zd7zztype_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1130z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1131z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1213z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1214z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31167ze3ze5zztype_typez00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_typezf3zf3zztype_typez00(obj_t);
	static obj_t BGl_z62lambda1221z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1222z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1142z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1143z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1305z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1306z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1229z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2magiczf3z43zztype_typez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62typezd2nilzb0zztype_typez00(obj_t);
	static obj_t BGl_z62typezd2pointedzd2tozd2byzd2setz12z70zztype_typez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1230z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1313z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1314z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1153z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31193ze3ze5zztype_typez00(obj_t);
	static obj_t BGl_z62lambda1154z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1237z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1238z62zztype_typez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2initzf3zd2setz12ze1zztype_typez00(BgL_typez00_bglt, bool_t);
	static obj_t BGl_z62typezd2initzf3zd2setz12z83zztype_typez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_typezd2siza7ez75zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62lambda1320z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1321z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31186ze3ze5zztype_typez00(obj_t);
	static obj_t BGl_z62lambda1165z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1166z62zztype_typez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2classzd2setz12z12zztype_typez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62typezd2classzd2setz12z70zztype_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62typezd2locationzd2setz12z70zztype_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1250z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1251z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezf3z91zztype_typez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2z42zd2setz12z50zztype_typez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62lambda1184z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1185z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zztype_typez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztype_typez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2siza7ezd2setz12zb5zztype_typez00(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zztype_typez00(void);
	static obj_t BGl_gczd2rootszd2initz00zztype_typez00(void);
	static obj_t BGl_z62lambda1191z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1192z62zztype_typez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1198z62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1199z62zztype_typez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	static obj_t BGl_z62typezd2idzb0zztype_typez00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_typezd2initzf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2parentszd2setz12z70zztype_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62typezd2tvectorzd2setz12z70zztype_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62typezd2initzf3z43zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2coercezd2tozd2setz12za2zztype_typez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_typezd2nilzd2zztype_typez00(void);
	BGL_EXPORTED_DECL int
		BGl_typezd2occurrencezd2zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2occurrencezb0zztype_typez00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2coercezd2toz62zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2tvectorzb0zztype_typez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2locationzb0zztype_typez00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62typezd2z42zd2setz12z32zztype_typez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_typezd2aliaszd2zztype_typez00(BgL_typez00_bglt);
	static obj_t __cnst[25];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2typezd2envz00zztype_typez00,
		BgL_bgl_za762makeza7d2typeza7b1409za7, BGl_z62makezd2typezb0zztype_typez00,
		0L, BUNSPEC, 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2coercezd2tozd2envzd2zztype_typez00,
		BgL_bgl_za762typeza7d2coerce1410z00,
		BGl_z62typezd2coercezd2toz62zztype_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2initzf3zd2setz12zd2envz33zztype_typez00,
		BgL_bgl_za762typeza7d2initza7f1411za7,
		BGl_z62typezd2initzf3zd2setz12z83zztype_typez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2nilzd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2nilza7b01412za7, BGl_z62typezd2nilzb0zztype_typez00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2aliaszd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2aliasza71413za7, BGl_z62typezd2aliaszb0zztype_typez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1333z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1414za7,
		BGl_z62zc3z04anonymousza31132ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1334z00zztype_typez00,
		BgL_bgl_za762lambda1131za7621415z00, BGl_z62lambda1131z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1335z00zztype_typez00,
		BgL_bgl_za762lambda1130za7621416z00, BGl_z62lambda1130z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2typezf3zd2envzf3zztype_typez00,
		BgL_bgl_za762biglooza7d2type1417z00,
		BGl_z62bigloozd2typezf3z43zztype_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1336z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1418za7,
		BGl_z62zc3z04anonymousza31144ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1337z00zztype_typez00,
		BgL_bgl_za762lambda1143za7621419z00, BGl_z62lambda1143z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1338z00zztype_typez00,
		BgL_bgl_za762lambda1142za7621420z00, BGl_z62lambda1142z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1339z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1421za7,
		BGl_z62zc3z04anonymousza31155ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1340z00zztype_typez00,
		BgL_bgl_za762lambda1154za7621422z00, BGl_z62lambda1154z62zztype_typez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2parentszd2setz12zd2envzc0zztype_typez00,
		BgL_bgl_za762typeza7d2parent1423z00,
		BGl_z62typezd2parentszd2setz12z70zztype_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1341z00zztype_typez00,
		BgL_bgl_za762lambda1153za7621424z00, BGl_z62lambda1153z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1342z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1425za7,
		BGl_z62zc3z04anonymousza31167ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1343z00zztype_typez00,
		BgL_bgl_za762lambda1166za7621426z00, BGl_z62lambda1166z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1344z00zztype_typez00,
		BgL_bgl_za762lambda1165za7621427z00, BGl_z62lambda1165z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1345z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1428za7,
		BGl_z62zc3z04anonymousza31186ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1346z00zztype_typez00,
		BgL_bgl_za762lambda1185za7621429z00, BGl_z62lambda1185z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1347z00zztype_typez00,
		BgL_bgl_za762lambda1184za7621430z00, BGl_z62lambda1184z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1348z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1431za7,
		BGl_z62zc3z04anonymousza31193ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1349z00zztype_typez00,
		BgL_bgl_za762lambda1192za7621432z00, BGl_z62lambda1192z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2magiczf3zd2envzf3zztype_typez00,
		BgL_bgl_za762typeza7d2magicza71433za7,
		BGl_z62typezd2magiczf3z43zztype_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2siza7ezd2setz12zd2envz67zztype_typez00,
		BgL_bgl_za762typeza7d2siza7a7e1434za7,
		BGl_z62typezd2siza7ezd2setz12zd7zztype_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1350z00zztype_typez00,
		BgL_bgl_za762lambda1191za7621435z00, BGl_z62lambda1191z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1351z00zztype_typez00,
		BgL_bgl_za762lambda1199za7621436z00, BGl_z62lambda1199z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1352z00zztype_typez00,
		BgL_bgl_za762lambda1198za7621437z00, BGl_z62lambda1198z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1353z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1438za7,
		BGl_z62zc3z04anonymousza31206ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1354z00zztype_typez00,
		BgL_bgl_za762lambda1205za7621439z00, BGl_z62lambda1205z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1355z00zztype_typez00,
		BgL_bgl_za762lambda1204za7621440z00, BGl_z62lambda1204z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1356z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1441za7,
		BGl_z62zc3z04anonymousza31215ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1357z00zztype_typez00,
		BgL_bgl_za762lambda1214za7621442z00, BGl_z62lambda1214z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1358z00zztype_typez00,
		BgL_bgl_za762lambda1213za7621443z00, BGl_z62lambda1213z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1359z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1444za7,
		BGl_z62zc3z04anonymousza31223ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2namezd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2nameza7b1445za7, BGl_z62typezd2namezb0zztype_typez00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2tvectorzd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2tvecto1446z00, BGl_z62typezd2tvectorzb0zztype_typez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1360z00zztype_typez00,
		BgL_bgl_za762lambda1222za7621447z00, BGl_z62lambda1222z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1361z00zztype_typez00,
		BgL_bgl_za762lambda1221za7621448z00, BGl_z62lambda1221z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1362z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1449za7,
		BGl_z62zc3z04anonymousza31231ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1363z00zztype_typez00,
		BgL_bgl_za762lambda1230za7621450z00, BGl_z62lambda1230z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1364z00zztype_typez00,
		BgL_bgl_za762lambda1229za7621451z00, BGl_z62lambda1229z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1365z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1452za7,
		BGl_z62zc3z04anonymousza31239ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1366z00zztype_typez00,
		BgL_bgl_za762lambda1238za7621453z00, BGl_z62lambda1238z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1367z00zztype_typez00,
		BgL_bgl_za762lambda1237za7621454z00, BGl_z62lambda1237z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1368z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1455za7,
		BGl_z62zc3z04anonymousza31252ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2importzd2locationzd2envzd2zztype_typez00,
		BgL_bgl_za762typeza7d2import1456z00,
		BGl_z62typezd2importzd2locationz62zztype_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1369z00zztype_typez00,
		BgL_bgl_za762lambda1251za7621457z00, BGl_z62lambda1251z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1370z00zztype_typez00,
		BgL_bgl_za762lambda1250za7621458z00, BGl_z62lambda1250z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1371z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1459za7,
		BGl_z62zc3z04anonymousza31307ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1372z00zztype_typez00,
		BgL_bgl_za762lambda1306za7621460z00, BGl_z62lambda1306z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1373z00zztype_typez00,
		BgL_bgl_za762lambda1305za7621461z00, BGl_z62lambda1305z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1374z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1462za7,
		BGl_z62zc3z04anonymousza31315ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1375z00zztype_typez00,
		BgL_bgl_za762lambda1314za7621463z00, BGl_z62lambda1314z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1376z00zztype_typez00,
		BgL_bgl_za762lambda1313za7621464z00, BGl_z62lambda1313z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1377z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1465za7,
		BGl_z62zc3z04anonymousza31322ze3ze5zztype_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1384z00zztype_typez00,
		BgL_bgl_string1384za700za7za7t1466za7, "type-occurrence-incr1073", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1378z00zztype_typez00,
		BgL_bgl_za762lambda1321za7621467z00, BGl_z62lambda1321z62zztype_typez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1385z00zztype_typez00,
		BgL_bgl_string1385za700za7za7t1468za7, "type_type", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1379z00zztype_typez00,
		BgL_bgl_za762lambda1320za7621469z00, BGl_z62lambda1320z62zztype_typez00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1386z00zztype_typez00,
		BgL_bgl_string1386za700za7za7t1470za7,
		"_ type_type type int occurrence import-location location tvector pointed-to-by alias $ null magic? bool init? parents coerce-to class size obj name symbol id C bigloo ",
		167);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2locationzd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2locati1471z00,
		BGl_z62typezd2locationzb0zztype_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2magiczf3zd2setz12zd2envz33zztype_typez00,
		BgL_bgl_za762typeza7d2magicza71472za7,
		BGl_z62typezd2magiczf3zd2setz12z83zztype_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1380z00zztype_typez00,
		BgL_bgl_za762za7c3za704anonymo1473za7,
		BGl_z62zc3z04anonymousza31123ze3ze5zztype_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1381z00zztype_typez00,
		BgL_bgl_za762lambda1115za7621474z00, BGl_z62lambda1115z62zztype_typez00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1382z00zztype_typez00,
		BgL_bgl_za762lambda1104za7621475z00, BGl_z62lambda1104z62zztype_typez00, 0L,
		BUNSPEC, 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1383z00zztype_typez00,
		BgL_bgl_za762typeza7d2occurr1476z00,
		BGl_z62typezd2occurrencezd2incr1073z62zztype_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2z42zd2setz12zd2envz82zztype_typez00,
		BgL_bgl_za762typeza7d2za742za7d21477z00,
		BGl_z62typezd2z42zd2setz12z32zztype_typez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2locationzd2setz12zd2envzc0zztype_typez00,
		BgL_bgl_za762typeza7d2locati1478z00,
		BGl_z62typezd2locationzd2setz12z70zztype_typez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2coercezd2tozd2setz12zd2envz12zztype_typez00,
		BgL_bgl_za762typeza7d2coerce1479z00,
		BGl_z62typezd2coercezd2tozd2setz12za2zztype_typez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2occurrencezd2setz12zd2envzc0zztype_typez00,
		BgL_bgl_za762typeza7d2occurr1480z00,
		BGl_z62typezd2occurrencezd2setz12z70zztype_typez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2tvectorzd2setz12zd2envzc0zztype_typez00,
		BgL_bgl_za762typeza7d2tvecto1481z00,
		BGl_z62typezd2tvectorzd2setz12z70zztype_typez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_foreignzd2typezf3zd2envzf3zztype_typez00,
		BgL_bgl_za762foreignza7d2typ1482z00,
		BGl_z62foreignzd2typezf3z43zztype_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2namezd2setz12zd2envzc0zztype_typez00,
		BgL_bgl_za762typeza7d2nameza7d1483za7,
		BGl_z62typezd2namezd2setz12z70zztype_typez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2classzd2setz12zd2envzc0zztype_typez00,
		BgL_bgl_za762typeza7d2classza71484za7,
		BGl_z62typezd2classzd2setz12z70zztype_typez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2importzd2locationzd2setz12zd2envz12zztype_typez00,
		BgL_bgl_za762typeza7d2import1485z00,
		BGl_z62typezd2importzd2locationzd2setz12za2zztype_typez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2parentszd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2parent1486z00, BGl_z62typezd2parentszb0zztype_typez00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2z42zd2envz42zztype_typez00,
		BgL_bgl_za762typeza7d2za742za7f21487z00, BGl_z62typezd2z42zf2zztype_typez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2pointedzd2tozd2byzd2setz12zd2envzc0zztype_typez00,
		BgL_bgl_za762typeza7d2pointe1488z00,
		BGl_z62typezd2pointedzd2tozd2byzd2setz12z70zztype_typez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezf3zd2envz21zztype_typez00,
		BgL_bgl_za762typeza7f3za791za7za7t1489za7, BGl_z62typezf3z91zztype_typez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2pointedzd2tozd2byzd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2pointe1490z00,
		BGl_z62typezd2pointedzd2tozd2byzb0zztype_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_typezd2occurrencezd2incrementz12zd2envzc0zztype_typez00,
		BgL_bgl_za762typeza7d2occurr1491z00,
		BGl_z62typezd2occurrencezd2incrementz12z70zztype_typez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2idzd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2idza7b0za71492z00, BGl_z62typezd2idzb0zztype_typez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2aliasedzd2typezd2envzd2zztype_typez00,
		BgL_bgl_za762getza7d2aliased1493z00,
		BGl_z62getzd2aliasedzd2typez62zztype_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2aliaszd2setz12zd2envzc0zztype_typez00,
		BgL_bgl_za762typeza7d2aliasza71494za7,
		BGl_z62typezd2aliaszd2setz12z70zztype_typez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2siza7ezd2envza7zztype_typez00,
		BgL_bgl_za762typeza7d2siza7a7e1495za7,
		BGl_z62typezd2siza7ez17zztype_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2occurrencezd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2occurr1496z00,
		BGl_z62typezd2occurrencezb0zztype_typez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2classzd2envz00zztype_typez00,
		BgL_bgl_za762typeza7d2classza71497za7, BGl_z62typezd2classzb0zztype_typez00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2initzf3zd2envzf3zztype_typez00,
		BgL_bgl_za762typeza7d2initza7f1498za7,
		BGl_z62typezd2initzf3z43zztype_typez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztype_typez00));
		     ADD_ROOT((void *) (&BGl_typez00zztype_typez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long
		BgL_checksumz00_821, char *BgL_fromz00_822)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztype_typez00))
				{
					BGl_requirezd2initializa7ationz75zztype_typez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztype_typez00();
					BGl_libraryzd2moduleszd2initz00zztype_typez00();
					BGl_cnstzd2initzd2zztype_typez00();
					BGl_importedzd2moduleszd2initz00zztype_typez00();
					BGl_objectzd2initzd2zztype_typez00();
					BGl_genericzd2initzd2zztype_typez00();
					return BGl_toplevelzd2initzd2zztype_typez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztype_typez00(void)
	{
		{	/* Type/type.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "type_type");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "type_type");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "type_type");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "type_type");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "type_type");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"type_type");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "type_type");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"type_type");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztype_typez00(void)
	{
		{	/* Type/type.scm 15 */
			{	/* Type/type.scm 15 */
				obj_t BgL_cportz00_745;

				{	/* Type/type.scm 15 */
					obj_t BgL_stringz00_752;

					BgL_stringz00_752 = BGl_string1386z00zztype_typez00;
					{	/* Type/type.scm 15 */
						obj_t BgL_startz00_753;

						BgL_startz00_753 = BINT(0L);
						{	/* Type/type.scm 15 */
							obj_t BgL_endz00_754;

							BgL_endz00_754 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_752)));
							{	/* Type/type.scm 15 */

								BgL_cportz00_745 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_752, BgL_startz00_753, BgL_endz00_754);
				}}}}
				{
					long BgL_iz00_746;

					BgL_iz00_746 = 24L;
				BgL_loopz00_747:
					if ((BgL_iz00_746 == -1L))
						{	/* Type/type.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Type/type.scm 15 */
							{	/* Type/type.scm 15 */
								obj_t BgL_arg1408z00_748;

								{	/* Type/type.scm 15 */

									{	/* Type/type.scm 15 */
										obj_t BgL_locationz00_750;

										BgL_locationz00_750 = BBOOL(((bool_t) 0));
										{	/* Type/type.scm 15 */

											BgL_arg1408z00_748 =
												BGl_readz00zz__readerz00(BgL_cportz00_745,
												BgL_locationz00_750);
										}
									}
								}
								{	/* Type/type.scm 15 */
									int BgL_tmpz00_850;

									BgL_tmpz00_850 = (int) (BgL_iz00_746);
									CNST_TABLE_SET(BgL_tmpz00_850, BgL_arg1408z00_748);
							}}
							{	/* Type/type.scm 15 */
								int BgL_auxz00_751;

								BgL_auxz00_751 = (int) ((BgL_iz00_746 - 1L));
								{
									long BgL_iz00_855;

									BgL_iz00_855 = (long) (BgL_auxz00_751);
									BgL_iz00_746 = BgL_iz00_855;
									goto BgL_loopz00_747;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztype_typez00(void)
	{
		{	/* Type/type.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztype_typez00(void)
	{
		{	/* Type/type.scm 15 */
			return BUNSPEC;
		}

	}



/* make-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_makezd2typezd2zztype_typez00(obj_t
		BgL_id1030z00_3, obj_t BgL_name1031z00_4, obj_t BgL_siza7e1032za7_5,
		obj_t BgL_class1033z00_6, obj_t BgL_coercezd2to1034zd2_7,
		obj_t BgL_parents1035z00_8, bool_t BgL_initzf31036zf3_9,
		bool_t BgL_magiczf31037zf3_10, obj_t BgL_z421038z42_11,
		obj_t BgL_alias1039z00_12, obj_t BgL_pointedzd2tozd2by1040z00_13,
		obj_t BgL_tvector1041z00_14, obj_t BgL_location1042z00_15,
		obj_t BgL_importzd2location1043zd2_16, int BgL_occurrence1044z00_17)
	{
		{	/* Type/type.sch 50 */
			{	/* Type/type.sch 50 */
				BgL_typez00_bglt BgL_new1067z00_756;

				{	/* Type/type.sch 50 */
					BgL_typez00_bglt BgL_new1066z00_757;

					BgL_new1066z00_757 =
						((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_typez00_bgl))));
					{	/* Type/type.sch 50 */
						long BgL_arg1079z00_758;

						BgL_arg1079z00_758 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1066z00_757), BgL_arg1079z00_758);
					}
					{	/* Type/type.sch 50 */
						BgL_objectz00_bglt BgL_tmpz00_862;

						BgL_tmpz00_862 = ((BgL_objectz00_bglt) BgL_new1066z00_757);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_862, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1066z00_757);
					BgL_new1067z00_756 = BgL_new1066z00_757;
				}
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_idz00) =
					((obj_t) BgL_id1030z00_3), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_namez00) =
					((obj_t) BgL_name1031z00_4), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1032za7_5), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_classz00) =
					((obj_t) BgL_class1033z00_6), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->
						BgL_coercezd2tozd2) = ((obj_t) BgL_coercezd2to1034zd2_7), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_parentsz00) =
					((obj_t) BgL_parents1035z00_8), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31036zf3_9), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31037zf3_10), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_z42z42) =
					((obj_t) BgL_z421038z42_11), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_aliasz00) =
					((obj_t) BgL_alias1039z00_12), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->
						BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1040z00_13), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1041z00_14), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_locationz00) =
					((obj_t) BgL_location1042z00_15), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->
						BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1043zd2_16), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_new1067z00_756))->BgL_occurrencez00) =
					((int) BgL_occurrence1044z00_17), BUNSPEC);
				return BgL_new1067z00_756;
			}
		}

	}



/* &make-type */
	BgL_typez00_bglt BGl_z62makezd2typezb0zztype_typez00(obj_t BgL_envz00_478,
		obj_t BgL_id1030z00_479, obj_t BgL_name1031z00_480,
		obj_t BgL_siza7e1032za7_481, obj_t BgL_class1033z00_482,
		obj_t BgL_coercezd2to1034zd2_483, obj_t BgL_parents1035z00_484,
		obj_t BgL_initzf31036zf3_485, obj_t BgL_magiczf31037zf3_486,
		obj_t BgL_z421038z42_487, obj_t BgL_alias1039z00_488,
		obj_t BgL_pointedzd2tozd2by1040z00_489, obj_t BgL_tvector1041z00_490,
		obj_t BgL_location1042z00_491, obj_t BgL_importzd2location1043zd2_492,
		obj_t BgL_occurrence1044z00_493)
	{
		{	/* Type/type.sch 50 */
			return
				BGl_makezd2typezd2zztype_typez00(BgL_id1030z00_479, BgL_name1031z00_480,
				BgL_siza7e1032za7_481, BgL_class1033z00_482, BgL_coercezd2to1034zd2_483,
				BgL_parents1035z00_484, CBOOL(BgL_initzf31036zf3_485),
				CBOOL(BgL_magiczf31037zf3_486), BgL_z421038z42_487,
				BgL_alias1039z00_488, BgL_pointedzd2tozd2by1040z00_489,
				BgL_tvector1041z00_490, BgL_location1042z00_491,
				BgL_importzd2location1043zd2_492, CINT(BgL_occurrence1044z00_493));
		}

	}



/* type? */
	BGL_EXPORTED_DEF bool_t BGl_typezf3zf3zztype_typez00(obj_t BgL_objz00_18)
	{
		{	/* Type/type.sch 51 */
			{	/* Type/type.sch 51 */
				obj_t BgL_classz00_759;

				BgL_classz00_759 = BGl_typez00zztype_typez00;
				if (BGL_OBJECTP(BgL_objz00_18))
					{	/* Type/type.sch 51 */
						BgL_objectz00_bglt BgL_arg1807z00_760;

						BgL_arg1807z00_760 = (BgL_objectz00_bglt) (BgL_objz00_18);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Type/type.sch 51 */
								long BgL_idxz00_761;

								BgL_idxz00_761 = BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_760);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_761 + 1L)) == BgL_classz00_759);
							}
						else
							{	/* Type/type.sch 51 */
								bool_t BgL_res1326z00_764;

								{	/* Type/type.sch 51 */
									obj_t BgL_oclassz00_765;

									{	/* Type/type.sch 51 */
										obj_t BgL_arg1815z00_766;
										long BgL_arg1816z00_767;

										BgL_arg1815z00_766 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Type/type.sch 51 */
											long BgL_arg1817z00_768;

											BgL_arg1817z00_768 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_760);
											BgL_arg1816z00_767 = (BgL_arg1817z00_768 - OBJECT_TYPE);
										}
										BgL_oclassz00_765 =
											VECTOR_REF(BgL_arg1815z00_766, BgL_arg1816z00_767);
									}
									{	/* Type/type.sch 51 */
										bool_t BgL__ortest_1115z00_769;

										BgL__ortest_1115z00_769 =
											(BgL_classz00_759 == BgL_oclassz00_765);
										if (BgL__ortest_1115z00_769)
											{	/* Type/type.sch 51 */
												BgL_res1326z00_764 = BgL__ortest_1115z00_769;
											}
										else
											{	/* Type/type.sch 51 */
												long BgL_odepthz00_770;

												{	/* Type/type.sch 51 */
													obj_t BgL_arg1804z00_771;

													BgL_arg1804z00_771 = (BgL_oclassz00_765);
													BgL_odepthz00_770 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_771);
												}
												if ((1L < BgL_odepthz00_770))
													{	/* Type/type.sch 51 */
														obj_t BgL_arg1802z00_772;

														{	/* Type/type.sch 51 */
															obj_t BgL_arg1803z00_773;

															BgL_arg1803z00_773 = (BgL_oclassz00_765);
															BgL_arg1802z00_772 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_773, 1L);
														}
														BgL_res1326z00_764 =
															(BgL_arg1802z00_772 == BgL_classz00_759);
													}
												else
													{	/* Type/type.sch 51 */
														BgL_res1326z00_764 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1326z00_764;
							}
					}
				else
					{	/* Type/type.sch 51 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &type? */
	obj_t BGl_z62typezf3z91zztype_typez00(obj_t BgL_envz00_494,
		obj_t BgL_objz00_495)
	{
		{	/* Type/type.sch 51 */
			return BBOOL(BGl_typezf3zf3zztype_typez00(BgL_objz00_495));
		}

	}



/* type-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_typezd2nilzd2zztype_typez00(void)
	{
		{	/* Type/type.sch 52 */
			{	/* Type/type.sch 52 */
				obj_t BgL_classz00_377;

				BgL_classz00_377 = BGl_typez00zztype_typez00;
				{	/* Type/type.sch 52 */
					obj_t BgL__ortest_1117z00_378;

					BgL__ortest_1117z00_378 = BGL_CLASS_NIL(BgL_classz00_377);
					if (CBOOL(BgL__ortest_1117z00_378))
						{	/* Type/type.sch 52 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_378);
						}
					else
						{	/* Type/type.sch 52 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_377));
						}
				}
			}
		}

	}



/* &type-nil */
	BgL_typez00_bglt BGl_z62typezd2nilzb0zztype_typez00(obj_t BgL_envz00_496)
	{
		{	/* Type/type.sch 52 */
			return BGl_typezd2nilzd2zztype_typez00();
		}

	}



/* type-occurrence */
	BGL_EXPORTED_DEF int BGl_typezd2occurrencezd2zztype_typez00(BgL_typez00_bglt
		BgL_oz00_19)
	{
		{	/* Type/type.sch 53 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_19))->BgL_occurrencez00);
		}

	}



/* &type-occurrence */
	obj_t BGl_z62typezd2occurrencezb0zztype_typez00(obj_t BgL_envz00_497,
		obj_t BgL_oz00_498)
	{
		{	/* Type/type.sch 53 */
			return
				BINT(BGl_typezd2occurrencezd2zztype_typez00(
					((BgL_typez00_bglt) BgL_oz00_498)));
		}

	}



/* type-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2occurrencezd2setz12z12zztype_typez00(BgL_typez00_bglt
		BgL_oz00_20, int BgL_vz00_21)
	{
		{	/* Type/type.sch 54 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_20))->BgL_occurrencez00) =
				((int) BgL_vz00_21), BUNSPEC);
		}

	}



/* &type-occurrence-set! */
	obj_t BGl_z62typezd2occurrencezd2setz12z70zztype_typez00(obj_t BgL_envz00_499,
		obj_t BgL_oz00_500, obj_t BgL_vz00_501)
	{
		{	/* Type/type.sch 54 */
			return
				BGl_typezd2occurrencezd2setz12z12zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_500), CINT(BgL_vz00_501));
		}

	}



/* type-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2importzd2locationz00zztype_typez00(BgL_typez00_bglt BgL_oz00_22)
	{
		{	/* Type/type.sch 55 */
			return
				(((BgL_typez00_bglt) COBJECT(BgL_oz00_22))->BgL_importzd2locationzd2);
		}

	}



/* &type-import-location */
	obj_t BGl_z62typezd2importzd2locationz62zztype_typez00(obj_t BgL_envz00_502,
		obj_t BgL_oz00_503)
	{
		{	/* Type/type.sch 55 */
			return
				BGl_typezd2importzd2locationz00zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_503));
		}

	}



/* type-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2importzd2locationzd2setz12zc0zztype_typez00(BgL_typez00_bglt
		BgL_oz00_23, obj_t BgL_vz00_24)
	{
		{	/* Type/type.sch 56 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_23))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_24), BUNSPEC);
		}

	}



/* &type-import-location-set! */
	obj_t BGl_z62typezd2importzd2locationzd2setz12za2zztype_typez00(obj_t
		BgL_envz00_504, obj_t BgL_oz00_505, obj_t BgL_vz00_506)
	{
		{	/* Type/type.sch 56 */
			return
				BGl_typezd2importzd2locationzd2setz12zc0zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_505), BgL_vz00_506);
		}

	}



/* type-location */
	BGL_EXPORTED_DEF obj_t BGl_typezd2locationzd2zztype_typez00(BgL_typez00_bglt
		BgL_oz00_25)
	{
		{	/* Type/type.sch 57 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_25))->BgL_locationz00);
		}

	}



/* &type-location */
	obj_t BGl_z62typezd2locationzb0zztype_typez00(obj_t BgL_envz00_507,
		obj_t BgL_oz00_508)
	{
		{	/* Type/type.sch 57 */
			return
				BGl_typezd2locationzd2zztype_typez00(((BgL_typez00_bglt) BgL_oz00_508));
		}

	}



/* type-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2locationzd2setz12z12zztype_typez00(BgL_typez00_bglt BgL_oz00_26,
		obj_t BgL_vz00_27)
	{
		{	/* Type/type.sch 58 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_26))->BgL_locationz00) =
				((obj_t) BgL_vz00_27), BUNSPEC);
		}

	}



/* &type-location-set! */
	obj_t BGl_z62typezd2locationzd2setz12z70zztype_typez00(obj_t BgL_envz00_509,
		obj_t BgL_oz00_510, obj_t BgL_vz00_511)
	{
		{	/* Type/type.sch 58 */
			return
				BGl_typezd2locationzd2setz12z12zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_510), BgL_vz00_511);
		}

	}



/* type-tvector */
	BGL_EXPORTED_DEF obj_t BGl_typezd2tvectorzd2zztype_typez00(BgL_typez00_bglt
		BgL_oz00_28)
	{
		{	/* Type/type.sch 59 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_28))->BgL_tvectorz00);
		}

	}



/* &type-tvector */
	obj_t BGl_z62typezd2tvectorzb0zztype_typez00(obj_t BgL_envz00_512,
		obj_t BgL_oz00_513)
	{
		{	/* Type/type.sch 59 */
			return
				BGl_typezd2tvectorzd2zztype_typez00(((BgL_typez00_bglt) BgL_oz00_513));
		}

	}



/* type-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2tvectorzd2setz12z12zztype_typez00(BgL_typez00_bglt BgL_oz00_29,
		obj_t BgL_vz00_30)
	{
		{	/* Type/type.sch 60 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_29))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_30), BUNSPEC);
		}

	}



/* &type-tvector-set! */
	obj_t BGl_z62typezd2tvectorzd2setz12z70zztype_typez00(obj_t BgL_envz00_514,
		obj_t BgL_oz00_515, obj_t BgL_vz00_516)
	{
		{	/* Type/type.sch 60 */
			return
				BGl_typezd2tvectorzd2setz12z12zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_515), BgL_vz00_516);
		}

	}



/* type-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2pointedzd2tozd2byzd2zztype_typez00(BgL_typez00_bglt BgL_oz00_31)
	{
		{	/* Type/type.sch 61 */
			return
				(((BgL_typez00_bglt) COBJECT(BgL_oz00_31))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &type-pointed-to-by */
	obj_t BGl_z62typezd2pointedzd2tozd2byzb0zztype_typez00(obj_t BgL_envz00_517,
		obj_t BgL_oz00_518)
	{
		{	/* Type/type.sch 61 */
			return
				BGl_typezd2pointedzd2tozd2byzd2zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_518));
		}

	}



/* type-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2pointedzd2tozd2byzd2setz12z12zztype_typez00(BgL_typez00_bglt
		BgL_oz00_32, obj_t BgL_vz00_33)
	{
		{	/* Type/type.sch 62 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_32))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &type-pointed-to-by-set! */
	obj_t BGl_z62typezd2pointedzd2tozd2byzd2setz12z70zztype_typez00(obj_t
		BgL_envz00_519, obj_t BgL_oz00_520, obj_t BgL_vz00_521)
	{
		{	/* Type/type.sch 62 */
			return
				BGl_typezd2pointedzd2tozd2byzd2setz12z12zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_520), BgL_vz00_521);
		}

	}



/* type-alias */
	BGL_EXPORTED_DEF obj_t BGl_typezd2aliaszd2zztype_typez00(BgL_typez00_bglt
		BgL_oz00_34)
	{
		{	/* Type/type.sch 63 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_34))->BgL_aliasz00);
		}

	}



/* &type-alias */
	obj_t BGl_z62typezd2aliaszb0zztype_typez00(obj_t BgL_envz00_522,
		obj_t BgL_oz00_523)
	{
		{	/* Type/type.sch 63 */
			return
				BGl_typezd2aliaszd2zztype_typez00(((BgL_typez00_bglt) BgL_oz00_523));
		}

	}



/* type-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2aliaszd2setz12z12zztype_typez00(BgL_typez00_bglt BgL_oz00_35,
		obj_t BgL_vz00_36)
	{
		{	/* Type/type.sch 64 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_35))->BgL_aliasz00) =
				((obj_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &type-alias-set! */
	obj_t BGl_z62typezd2aliaszd2setz12z70zztype_typez00(obj_t BgL_envz00_524,
		obj_t BgL_oz00_525, obj_t BgL_vz00_526)
	{
		{	/* Type/type.sch 64 */
			return
				BGl_typezd2aliaszd2setz12z12zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_525), BgL_vz00_526);
		}

	}



/* type-$ */
	BGL_EXPORTED_DEF obj_t BGl_typezd2z42z90zztype_typez00(BgL_typez00_bglt
		BgL_oz00_37)
	{
		{	/* Type/type.sch 65 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_37))->BgL_z42z42);
		}

	}



/* &type-$ */
	obj_t BGl_z62typezd2z42zf2zztype_typez00(obj_t BgL_envz00_527,
		obj_t BgL_oz00_528)
	{
		{	/* Type/type.sch 65 */
			return BGl_typezd2z42z90zztype_typez00(((BgL_typez00_bglt) BgL_oz00_528));
		}

	}



/* type-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2z42zd2setz12z50zztype_typez00(BgL_typez00_bglt BgL_oz00_38,
		obj_t BgL_vz00_39)
	{
		{	/* Type/type.sch 66 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_38))->BgL_z42z42) =
				((obj_t) BgL_vz00_39), BUNSPEC);
		}

	}



/* &type-$-set! */
	obj_t BGl_z62typezd2z42zd2setz12z32zztype_typez00(obj_t BgL_envz00_529,
		obj_t BgL_oz00_530, obj_t BgL_vz00_531)
	{
		{	/* Type/type.sch 66 */
			return
				BGl_typezd2z42zd2setz12z50zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_530), BgL_vz00_531);
		}

	}



/* type-magic? */
	BGL_EXPORTED_DEF bool_t BGl_typezd2magiczf3z21zztype_typez00(BgL_typez00_bglt
		BgL_oz00_40)
	{
		{	/* Type/type.sch 67 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_40))->BgL_magiczf3zf3);
		}

	}



/* &type-magic? */
	obj_t BGl_z62typezd2magiczf3z43zztype_typez00(obj_t BgL_envz00_532,
		obj_t BgL_oz00_533)
	{
		{	/* Type/type.sch 67 */
			return
				BBOOL(BGl_typezd2magiczf3z21zztype_typez00(
					((BgL_typez00_bglt) BgL_oz00_533)));
		}

	}



/* type-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2magiczf3zd2setz12ze1zztype_typez00(BgL_typez00_bglt BgL_oz00_41,
		bool_t BgL_vz00_42)
	{
		{	/* Type/type.sch 68 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_41))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_42), BUNSPEC);
		}

	}



/* &type-magic?-set! */
	obj_t BGl_z62typezd2magiczf3zd2setz12z83zztype_typez00(obj_t BgL_envz00_534,
		obj_t BgL_oz00_535, obj_t BgL_vz00_536)
	{
		{	/* Type/type.sch 68 */
			return
				BGl_typezd2magiczf3zd2setz12ze1zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_535), CBOOL(BgL_vz00_536));
		}

	}



/* type-init? */
	BGL_EXPORTED_DEF bool_t BGl_typezd2initzf3z21zztype_typez00(BgL_typez00_bglt
		BgL_oz00_43)
	{
		{	/* Type/type.sch 69 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_43))->BgL_initzf3zf3);
		}

	}



/* &type-init? */
	obj_t BGl_z62typezd2initzf3z43zztype_typez00(obj_t BgL_envz00_537,
		obj_t BgL_oz00_538)
	{
		{	/* Type/type.sch 69 */
			return
				BBOOL(BGl_typezd2initzf3z21zztype_typez00(
					((BgL_typez00_bglt) BgL_oz00_538)));
		}

	}



/* type-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2initzf3zd2setz12ze1zztype_typez00(BgL_typez00_bglt BgL_oz00_44,
		bool_t BgL_vz00_45)
	{
		{	/* Type/type.sch 70 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_44))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &type-init?-set! */
	obj_t BGl_z62typezd2initzf3zd2setz12z83zztype_typez00(obj_t BgL_envz00_539,
		obj_t BgL_oz00_540, obj_t BgL_vz00_541)
	{
		{	/* Type/type.sch 70 */
			return
				BGl_typezd2initzf3zd2setz12ze1zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_540), CBOOL(BgL_vz00_541));
		}

	}



/* type-parents */
	BGL_EXPORTED_DEF obj_t BGl_typezd2parentszd2zztype_typez00(BgL_typez00_bglt
		BgL_oz00_46)
	{
		{	/* Type/type.sch 71 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_46))->BgL_parentsz00);
		}

	}



/* &type-parents */
	obj_t BGl_z62typezd2parentszb0zztype_typez00(obj_t BgL_envz00_542,
		obj_t BgL_oz00_543)
	{
		{	/* Type/type.sch 71 */
			return
				BGl_typezd2parentszd2zztype_typez00(((BgL_typez00_bglt) BgL_oz00_543));
		}

	}



/* type-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2parentszd2setz12z12zztype_typez00(BgL_typez00_bglt BgL_oz00_47,
		obj_t BgL_vz00_48)
	{
		{	/* Type/type.sch 72 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_47))->BgL_parentsz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &type-parents-set! */
	obj_t BGl_z62typezd2parentszd2setz12z70zztype_typez00(obj_t BgL_envz00_544,
		obj_t BgL_oz00_545, obj_t BgL_vz00_546)
	{
		{	/* Type/type.sch 72 */
			return
				BGl_typezd2parentszd2setz12z12zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_545), BgL_vz00_546);
		}

	}



/* type-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2coercezd2toz00zztype_typez00(BgL_typez00_bglt BgL_oz00_49)
	{
		{	/* Type/type.sch 73 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_49))->BgL_coercezd2tozd2);
		}

	}



/* &type-coerce-to */
	obj_t BGl_z62typezd2coercezd2toz62zztype_typez00(obj_t BgL_envz00_547,
		obj_t BgL_oz00_548)
	{
		{	/* Type/type.sch 73 */
			return
				BGl_typezd2coercezd2toz00zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_548));
		}

	}



/* type-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2coercezd2tozd2setz12zc0zztype_typez00(BgL_typez00_bglt
		BgL_oz00_50, obj_t BgL_vz00_51)
	{
		{	/* Type/type.sch 74 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_50))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_51), BUNSPEC);
		}

	}



/* &type-coerce-to-set! */
	obj_t BGl_z62typezd2coercezd2tozd2setz12za2zztype_typez00(obj_t
		BgL_envz00_549, obj_t BgL_oz00_550, obj_t BgL_vz00_551)
	{
		{	/* Type/type.sch 74 */
			return
				BGl_typezd2coercezd2tozd2setz12zc0zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_550), BgL_vz00_551);
		}

	}



/* type-class */
	BGL_EXPORTED_DEF obj_t BGl_typezd2classzd2zztype_typez00(BgL_typez00_bglt
		BgL_oz00_52)
	{
		{	/* Type/type.sch 75 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_52))->BgL_classz00);
		}

	}



/* &type-class */
	obj_t BGl_z62typezd2classzb0zztype_typez00(obj_t BgL_envz00_552,
		obj_t BgL_oz00_553)
	{
		{	/* Type/type.sch 75 */
			return
				BGl_typezd2classzd2zztype_typez00(((BgL_typez00_bglt) BgL_oz00_553));
		}

	}



/* type-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2classzd2setz12z12zztype_typez00(BgL_typez00_bglt BgL_oz00_53,
		obj_t BgL_vz00_54)
	{
		{	/* Type/type.sch 76 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_53))->BgL_classz00) =
				((obj_t) BgL_vz00_54), BUNSPEC);
		}

	}



/* &type-class-set! */
	obj_t BGl_z62typezd2classzd2setz12z70zztype_typez00(obj_t BgL_envz00_554,
		obj_t BgL_oz00_555, obj_t BgL_vz00_556)
	{
		{	/* Type/type.sch 76 */
			return
				BGl_typezd2classzd2setz12z12zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_555), BgL_vz00_556);
		}

	}



/* type-size */
	BGL_EXPORTED_DEF obj_t BGl_typezd2siza7ez75zztype_typez00(BgL_typez00_bglt
		BgL_oz00_55)
	{
		{	/* Type/type.sch 77 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_55))->BgL_siza7eza7);
		}

	}



/* &type-size */
	obj_t BGl_z62typezd2siza7ez17zztype_typez00(obj_t BgL_envz00_557,
		obj_t BgL_oz00_558)
	{
		{	/* Type/type.sch 77 */
			return
				BGl_typezd2siza7ez75zztype_typez00(((BgL_typez00_bglt) BgL_oz00_558));
		}

	}



/* type-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2siza7ezd2setz12zb5zztype_typez00(BgL_typez00_bglt BgL_oz00_56,
		obj_t BgL_vz00_57)
	{
		{	/* Type/type.sch 78 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_56))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_57), BUNSPEC);
		}

	}



/* &type-size-set! */
	obj_t BGl_z62typezd2siza7ezd2setz12zd7zztype_typez00(obj_t BgL_envz00_559,
		obj_t BgL_oz00_560, obj_t BgL_vz00_561)
	{
		{	/* Type/type.sch 78 */
			return
				BGl_typezd2siza7ezd2setz12zb5zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_560), BgL_vz00_561);
		}

	}



/* type-name */
	BGL_EXPORTED_DEF obj_t BGl_typezd2namezd2zztype_typez00(BgL_typez00_bglt
		BgL_oz00_58)
	{
		{	/* Type/type.sch 79 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_58))->BgL_namez00);
		}

	}



/* &type-name */
	obj_t BGl_z62typezd2namezb0zztype_typez00(obj_t BgL_envz00_562,
		obj_t BgL_oz00_563)
	{
		{	/* Type/type.sch 79 */
			return
				BGl_typezd2namezd2zztype_typez00(((BgL_typez00_bglt) BgL_oz00_563));
		}

	}



/* type-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2namezd2setz12z12zztype_typez00(BgL_typez00_bglt BgL_oz00_59,
		obj_t BgL_vz00_60)
	{
		{	/* Type/type.sch 80 */
			return
				((((BgL_typez00_bglt) COBJECT(BgL_oz00_59))->BgL_namez00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &type-name-set! */
	obj_t BGl_z62typezd2namezd2setz12z70zztype_typez00(obj_t BgL_envz00_564,
		obj_t BgL_oz00_565, obj_t BgL_vz00_566)
	{
		{	/* Type/type.sch 80 */
			return
				BGl_typezd2namezd2setz12z12zztype_typez00(
				((BgL_typez00_bglt) BgL_oz00_565), BgL_vz00_566);
		}

	}



/* type-id */
	BGL_EXPORTED_DEF obj_t BGl_typezd2idzd2zztype_typez00(BgL_typez00_bglt
		BgL_oz00_61)
	{
		{	/* Type/type.sch 81 */
			return (((BgL_typez00_bglt) COBJECT(BgL_oz00_61))->BgL_idz00);
		}

	}



/* &type-id */
	obj_t BGl_z62typezd2idzb0zztype_typez00(obj_t BgL_envz00_567,
		obj_t BgL_oz00_568)
	{
		{	/* Type/type.sch 81 */
			return BGl_typezd2idzd2zztype_typez00(((BgL_typez00_bglt) BgL_oz00_568));
		}

	}



/* get-aliased-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_typez00_bglt BgL_typez00_64)
	{
		{	/* Type/type.scm 64 */
			{
				obj_t BgL_typez00_80;

				{
					obj_t BgL_auxz00_1010;

					BgL_typez00_80 = ((obj_t) BgL_typez00_64);
				BgL_zc3z04anonymousza31080ze3z87_81:
					{	/* Type/type.scm 66 */
						bool_t BgL_test1506z00_1011;

						{	/* Type/type.scm 66 */
							obj_t BgL_arg1085z00_85;

							BgL_arg1085z00_85 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_typez00_80)))->BgL_aliasz00);
							{	/* Type/type.scm 66 */
								obj_t BgL_classz00_380;

								BgL_classz00_380 = BGl_typez00zztype_typez00;
								if (BGL_OBJECTP(BgL_arg1085z00_85))
									{	/* Type/type.scm 66 */
										BgL_objectz00_bglt BgL_arg1807z00_382;

										BgL_arg1807z00_382 =
											(BgL_objectz00_bglt) (BgL_arg1085z00_85);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Type/type.scm 66 */
												long BgL_idxz00_388;

												BgL_idxz00_388 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_382);
												BgL_test1506z00_1011 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_388 + 1L)) == BgL_classz00_380);
											}
										else
											{	/* Type/type.scm 66 */
												bool_t BgL_res1327z00_413;

												{	/* Type/type.scm 66 */
													obj_t BgL_oclassz00_396;

													{	/* Type/type.scm 66 */
														obj_t BgL_arg1815z00_404;
														long BgL_arg1816z00_405;

														BgL_arg1815z00_404 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Type/type.scm 66 */
															long BgL_arg1817z00_406;

															BgL_arg1817z00_406 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_382);
															BgL_arg1816z00_405 =
																(BgL_arg1817z00_406 - OBJECT_TYPE);
														}
														BgL_oclassz00_396 =
															VECTOR_REF(BgL_arg1815z00_404,
															BgL_arg1816z00_405);
													}
													{	/* Type/type.scm 66 */
														bool_t BgL__ortest_1115z00_397;

														BgL__ortest_1115z00_397 =
															(BgL_classz00_380 == BgL_oclassz00_396);
														if (BgL__ortest_1115z00_397)
															{	/* Type/type.scm 66 */
																BgL_res1327z00_413 = BgL__ortest_1115z00_397;
															}
														else
															{	/* Type/type.scm 66 */
																long BgL_odepthz00_398;

																{	/* Type/type.scm 66 */
																	obj_t BgL_arg1804z00_399;

																	BgL_arg1804z00_399 = (BgL_oclassz00_396);
																	BgL_odepthz00_398 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_399);
																}
																if ((1L < BgL_odepthz00_398))
																	{	/* Type/type.scm 66 */
																		obj_t BgL_arg1802z00_401;

																		{	/* Type/type.scm 66 */
																			obj_t BgL_arg1803z00_402;

																			BgL_arg1803z00_402 = (BgL_oclassz00_396);
																			BgL_arg1802z00_401 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_402, 1L);
																		}
																		BgL_res1327z00_413 =
																			(BgL_arg1802z00_401 == BgL_classz00_380);
																	}
																else
																	{	/* Type/type.scm 66 */
																		BgL_res1327z00_413 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1506z00_1011 = BgL_res1327z00_413;
											}
									}
								else
									{	/* Type/type.scm 66 */
										BgL_test1506z00_1011 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test1506z00_1011)
							{
								obj_t BgL_typez00_1036;

								BgL_typez00_1036 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_typez00_80)))->BgL_aliasz00);
								BgL_typez00_80 = BgL_typez00_1036;
								goto BgL_zc3z04anonymousza31080ze3z87_81;
							}
						else
							{	/* Type/type.scm 66 */
								BgL_auxz00_1010 = BgL_typez00_80;
							}
					}
					return ((BgL_typez00_bglt) BgL_auxz00_1010);
				}
			}
		}

	}



/* &get-aliased-type */
	BgL_typez00_bglt BGl_z62getzd2aliasedzd2typez62zztype_typez00(obj_t
		BgL_envz00_569, obj_t BgL_typez00_570)
	{
		{	/* Type/type.scm 64 */
			return
				BGl_getzd2aliasedzd2typez00zztype_typez00(
				((BgL_typez00_bglt) BgL_typez00_570));
		}

	}



/* bigloo-type? */
	BGL_EXPORTED_DEF bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt
		BgL_typez00_65)
	{
		{	/* Type/type.scm 73 */
			return
				(
				(((BgL_typez00_bglt) COBJECT(BgL_typez00_65))->BgL_classz00) ==
				CNST_TABLE_REF(0));
		}

	}



/* &bigloo-type? */
	obj_t BGl_z62bigloozd2typezf3z43zztype_typez00(obj_t BgL_envz00_571,
		obj_t BgL_typez00_572)
	{
		{	/* Type/type.scm 73 */
			return
				BBOOL(BGl_bigloozd2typezf3z21zztype_typez00(
					((BgL_typez00_bglt) BgL_typez00_572)));
		}

	}



/* foreign-type? */
	BGL_EXPORTED_DEF bool_t
		BGl_foreignzd2typezf3z21zztype_typez00(BgL_typez00_bglt BgL_typez00_66)
	{
		{	/* Type/type.scm 79 */
			return
				(
				(((BgL_typez00_bglt) COBJECT(BgL_typez00_66))->BgL_classz00) ==
				CNST_TABLE_REF(1));
		}

	}



/* &foreign-type? */
	obj_t BGl_z62foreignzd2typezf3z43zztype_typez00(obj_t BgL_envz00_573,
		obj_t BgL_typez00_574)
	{
		{	/* Type/type.scm 79 */
			return
				BBOOL(BGl_foreignzd2typezf3z21zztype_typez00(
					((BgL_typez00_bglt) BgL_typez00_574)));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztype_typez00(void)
	{
		{	/* Type/type.scm 15 */
			{	/* Type/type.scm 21 */
				obj_t BgL_arg1102z00_92;
				obj_t BgL_arg1103z00_93;

				{	/* Type/type.scm 21 */
					obj_t BgL_v1071z00_119;

					BgL_v1071z00_119 = create_vector(16L);
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1125z00_120;

						BgL_arg1125z00_120 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc1335z00zztype_typez00, BGl_proc1334z00zztype_typez00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BGl_proc1333z00zztype_typez00,
							CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1071z00_119, 0L, BgL_arg1125z00_120);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1137z00_133;

						BgL_arg1137z00_133 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc1338z00zztype_typez00, BGl_proc1337z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1336z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 1L, BgL_arg1137z00_133);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1145z00_146;

						BgL_arg1145z00_146 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc1341z00zztype_typez00, BGl_proc1340z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1339z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 2L, BgL_arg1145z00_146);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1157z00_159;

						BgL_arg1157z00_159 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc1344z00zztype_typez00, BGl_proc1343z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1342z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 3L, BgL_arg1157z00_159);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1171z00_172;

						BgL_arg1171z00_172 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc1347z00zztype_typez00, BGl_proc1346z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1345z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 4L, BgL_arg1171z00_172);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1187z00_185;

						BgL_arg1187z00_185 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc1350z00zztype_typez00, BGl_proc1349z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1348z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 5L, BgL_arg1187z00_185);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1194z00_198;

						BgL_arg1194z00_198 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc1352z00zztype_typez00, BGl_proc1351z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(11));
						VECTOR_SET(BgL_v1071z00_119, 6L, BgL_arg1194z00_198);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1200z00_208;

						BgL_arg1200z00_208 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc1355z00zztype_typez00, BGl_proc1354z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1353z00zztype_typez00,
							CNST_TABLE_REF(11));
						VECTOR_SET(BgL_v1071z00_119, 7L, BgL_arg1200z00_208);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1208z00_221;

						BgL_arg1208z00_221 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(13),
							BGl_proc1358z00zztype_typez00, BGl_proc1357z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1356z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 8L, BgL_arg1208z00_221);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1216z00_234;

						BgL_arg1216z00_234 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc1361z00zztype_typez00, BGl_proc1360z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1359z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 9L, BgL_arg1216z00_234);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1225z00_247;

						BgL_arg1225z00_247 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc1364z00zztype_typez00, BGl_proc1363z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1362z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 10L, BgL_arg1225z00_247);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1232z00_260;

						BgL_arg1232z00_260 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(16),
							BGl_proc1367z00zztype_typez00, BGl_proc1366z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1365z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 11L, BgL_arg1232z00_260);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1242z00_273;

						BgL_arg1242z00_273 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(17),
							BGl_proc1370z00zztype_typez00, BGl_proc1369z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1368z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 12L, BgL_arg1242z00_273);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1268z00_286;

						BgL_arg1268z00_286 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc1373z00zztype_typez00, BGl_proc1372z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1371z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 13L, BgL_arg1268z00_286);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1308z00_299;

						BgL_arg1308z00_299 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(19),
							BGl_proc1376z00zztype_typez00, BGl_proc1375z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1374z00zztype_typez00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1071z00_119, 14L, BgL_arg1308z00_299);
					}
					{	/* Type/type.scm 21 */
						obj_t BgL_arg1316z00_312;

						BgL_arg1316z00_312 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc1379z00zztype_typez00, BGl_proc1378z00zztype_typez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc1377z00zztype_typez00,
							CNST_TABLE_REF(21));
						VECTOR_SET(BgL_v1071z00_119, 15L, BgL_arg1316z00_312);
					}
					BgL_arg1102z00_92 = BgL_v1071z00_119;
				}
				{	/* Type/type.scm 21 */
					obj_t BgL_v1072z00_325;

					BgL_v1072z00_325 = create_vector(0L);
					BgL_arg1103z00_93 = BgL_v1072z00_325;
				}
				return (BGl_typez00zztype_typez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(22),
						CNST_TABLE_REF(23), BGl_objectz00zz__objectz00, 30566L,
						BGl_proc1382z00zztype_typez00, BGl_proc1381z00zztype_typez00,
						BFALSE, BGl_proc1380z00zztype_typez00, BFALSE, BgL_arg1102z00_92,
						BgL_arg1103z00_93), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1123> */
	obj_t BGl_z62zc3z04anonymousza31123ze3ze5zztype_typez00(obj_t BgL_envz00_625,
		obj_t BgL_new1065z00_626)
	{
		{	/* Type/type.scm 21 */
			{
				BgL_typez00_bglt BgL_auxz00_1124;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1065z00_626)))->BgL_idz00) = ((obj_t)
						CNST_TABLE_REF(24)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1065z00_626)))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1065z00_626)))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				BgL_auxz00_1124 = ((BgL_typez00_bglt) BgL_new1065z00_626);
				return ((obj_t) BgL_auxz00_1124);
			}
		}

	}



/* &lambda1115 */
	BgL_typez00_bglt BGl_z62lambda1115z62zztype_typez00(obj_t BgL_envz00_627)
	{
		{	/* Type/type.scm 21 */
			{	/* Type/type.scm 21 */
				BgL_typez00_bglt BgL_new1064z00_775;

				BgL_new1064z00_775 =
					((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_typez00_bgl))));
				{	/* Type/type.scm 21 */
					long BgL_arg1122z00_776;

					BgL_arg1122z00_776 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1064z00_775), BgL_arg1122z00_776);
				}
				{	/* Type/type.scm 21 */
					BgL_objectz00_bglt BgL_tmpz00_1165;

					BgL_tmpz00_1165 = ((BgL_objectz00_bglt) BgL_new1064z00_775);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1165, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1064z00_775);
				return BgL_new1064z00_775;
			}
		}

	}



/* &lambda1104 */
	BgL_typez00_bglt BGl_z62lambda1104z62zztype_typez00(obj_t BgL_envz00_628,
		obj_t BgL_id1048z00_629, obj_t BgL_name1049z00_630,
		obj_t BgL_siza7e1050za7_631, obj_t BgL_class1051z00_632,
		obj_t BgL_coercezd2to1052zd2_633, obj_t BgL_parents1053z00_634,
		obj_t BgL_initzf31054zf3_635, obj_t BgL_magiczf31055zf3_636,
		obj_t BgL_null1056z00_637, obj_t BgL_z421057z42_638,
		obj_t BgL_alias1058z00_639, obj_t BgL_pointedzd2tozd2by1059z00_640,
		obj_t BgL_tvector1060z00_641, obj_t BgL_location1061z00_642,
		obj_t BgL_importzd2location1062zd2_643, obj_t BgL_occurrence1063z00_644)
	{
		{	/* Type/type.scm 21 */
			{	/* Type/type.scm 21 */
				bool_t BgL_initzf31054zf3_778;
				bool_t BgL_magiczf31055zf3_779;
				int BgL_occurrence1063z00_780;

				BgL_initzf31054zf3_778 = CBOOL(BgL_initzf31054zf3_635);
				BgL_magiczf31055zf3_779 = CBOOL(BgL_magiczf31055zf3_636);
				BgL_occurrence1063z00_780 = CINT(BgL_occurrence1063z00_644);
				{	/* Type/type.scm 21 */
					BgL_typez00_bglt BgL_new1070z00_781;

					{	/* Type/type.scm 21 */
						BgL_typez00_bglt BgL_new1069z00_782;

						BgL_new1069z00_782 =
							((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_typez00_bgl))));
						{	/* Type/type.scm 21 */
							long BgL_arg1114z00_783;

							BgL_arg1114z00_783 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1069z00_782), BgL_arg1114z00_783);
						}
						{	/* Type/type.scm 21 */
							BgL_objectz00_bglt BgL_tmpz00_1176;

							BgL_tmpz00_1176 = ((BgL_objectz00_bglt) BgL_new1069z00_782);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1176, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1069z00_782);
						BgL_new1070z00_781 = BgL_new1069z00_782;
					}
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1048z00_629)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_namez00) =
						((obj_t) BgL_name1049z00_630), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1050za7_631), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_classz00) =
						((obj_t) BgL_class1051z00_632), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->
							BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1052zd2_633), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_parentsz00) =
						((obj_t) BgL_parents1053z00_634), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31054zf3_778), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31055zf3_779), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_nullz00) =
						((obj_t) BgL_null1056z00_637), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_z42z42) =
						((obj_t) BgL_z421057z42_638), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_aliasz00) =
						((obj_t) BgL_alias1058z00_639), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->
							BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1059z00_640), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1060z00_641), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->BgL_locationz00) =
						((obj_t) BgL_location1061z00_642), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->
							BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1062zd2_643), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_new1070z00_781))->
							BgL_occurrencez00) = ((int) BgL_occurrence1063z00_780), BUNSPEC);
					return BgL_new1070z00_781;
				}
			}
		}

	}



/* &<@anonymous:1322> */
	obj_t BGl_z62zc3z04anonymousza31322ze3ze5zztype_typez00(obj_t BgL_envz00_645)
	{
		{	/* Type/type.scm 21 */
			return BINT(0L);
		}

	}



/* &lambda1321 */
	obj_t BGl_z62lambda1321z62zztype_typez00(obj_t BgL_envz00_646,
		obj_t BgL_oz00_647, obj_t BgL_vz00_648)
	{
		{	/* Type/type.scm 21 */
			{	/* Type/type.scm 21 */
				int BgL_vz00_785;

				BgL_vz00_785 = CINT(BgL_vz00_648);
				return
					((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_oz00_647)))->BgL_occurrencez00) =
					((int) BgL_vz00_785), BUNSPEC);
		}}

	}



/* &lambda1320 */
	obj_t BGl_z62lambda1320z62zztype_typez00(obj_t BgL_envz00_649,
		obj_t BgL_oz00_650)
	{
		{	/* Type/type.scm 21 */
			return
				BINT(
				(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_650)))->BgL_occurrencez00));
		}

	}



/* &<@anonymous:1315> */
	obj_t BGl_z62zc3z04anonymousza31315ze3ze5zztype_typez00(obj_t BgL_envz00_651)
	{
		{	/* Type/type.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1314 */
	obj_t BGl_z62lambda1314z62zztype_typez00(obj_t BgL_envz00_652,
		obj_t BgL_oz00_653, obj_t BgL_vz00_654)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_653)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_654), BUNSPEC);
		}

	}



/* &lambda1313 */
	obj_t BGl_z62lambda1313z62zztype_typez00(obj_t BgL_envz00_655,
		obj_t BgL_oz00_656)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_656)))->BgL_importzd2locationzd2);
		}

	}



/* &<@anonymous:1307> */
	obj_t BGl_z62zc3z04anonymousza31307ze3ze5zztype_typez00(obj_t BgL_envz00_657)
	{
		{	/* Type/type.scm 21 */
			return BUNSPEC;
		}

	}



/* &lambda1306 */
	obj_t BGl_z62lambda1306z62zztype_typez00(obj_t BgL_envz00_658,
		obj_t BgL_oz00_659, obj_t BgL_vz00_660)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_659)))->BgL_locationz00) =
				((obj_t) BgL_vz00_660), BUNSPEC);
		}

	}



/* &lambda1305 */
	obj_t BGl_z62lambda1305z62zztype_typez00(obj_t BgL_envz00_661,
		obj_t BgL_oz00_662)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_662)))->BgL_locationz00);
		}

	}



/* &<@anonymous:1252> */
	obj_t BGl_z62zc3z04anonymousza31252ze3ze5zztype_typez00(obj_t BgL_envz00_663)
	{
		{	/* Type/type.scm 21 */
			return BUNSPEC;
		}

	}



/* &lambda1251 */
	obj_t BGl_z62lambda1251z62zztype_typez00(obj_t BgL_envz00_664,
		obj_t BgL_oz00_665, obj_t BgL_vz00_666)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_665)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_666), BUNSPEC);
		}

	}



/* &lambda1250 */
	obj_t BGl_z62lambda1250z62zztype_typez00(obj_t BgL_envz00_667,
		obj_t BgL_oz00_668)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_668)))->BgL_tvectorz00);
		}

	}



/* &<@anonymous:1239> */
	obj_t BGl_z62zc3z04anonymousza31239ze3ze5zztype_typez00(obj_t BgL_envz00_669)
	{
		{	/* Type/type.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1238 */
	obj_t BGl_z62lambda1238z62zztype_typez00(obj_t BgL_envz00_670,
		obj_t BgL_oz00_671, obj_t BgL_vz00_672)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_671)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_672), BUNSPEC);
		}

	}



/* &lambda1237 */
	obj_t BGl_z62lambda1237z62zztype_typez00(obj_t BgL_envz00_673,
		obj_t BgL_oz00_674)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_674)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &<@anonymous:1231> */
	obj_t BGl_z62zc3z04anonymousza31231ze3ze5zztype_typez00(obj_t BgL_envz00_675)
	{
		{	/* Type/type.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1230 */
	obj_t BGl_z62lambda1230z62zztype_typez00(obj_t BgL_envz00_676,
		obj_t BgL_oz00_677, obj_t BgL_vz00_678)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_677)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_678), BUNSPEC);
		}

	}



/* &lambda1229 */
	obj_t BGl_z62lambda1229z62zztype_typez00(obj_t BgL_envz00_679,
		obj_t BgL_oz00_680)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_680)))->BgL_aliasz00);
		}

	}



/* &<@anonymous:1223> */
	obj_t BGl_z62zc3z04anonymousza31223ze3ze5zztype_typez00(obj_t BgL_envz00_681)
	{
		{	/* Type/type.scm 21 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1222 */
	obj_t BGl_z62lambda1222z62zztype_typez00(obj_t BgL_envz00_682,
		obj_t BgL_oz00_683, obj_t BgL_vz00_684)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_683)))->BgL_z42z42) =
				((obj_t) BgL_vz00_684), BUNSPEC);
		}

	}



/* &lambda1221 */
	obj_t BGl_z62lambda1221z62zztype_typez00(obj_t BgL_envz00_685,
		obj_t BgL_oz00_686)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_686)))->BgL_z42z42);
		}

	}



/* &<@anonymous:1215> */
	obj_t BGl_z62zc3z04anonymousza31215ze3ze5zztype_typez00(obj_t BgL_envz00_687)
	{
		{	/* Type/type.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1214 */
	obj_t BGl_z62lambda1214z62zztype_typez00(obj_t BgL_envz00_688,
		obj_t BgL_oz00_689, obj_t BgL_vz00_690)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_689)))->BgL_nullz00) =
				((obj_t) BgL_vz00_690), BUNSPEC);
		}

	}



/* &lambda1213 */
	obj_t BGl_z62lambda1213z62zztype_typez00(obj_t BgL_envz00_691,
		obj_t BgL_oz00_692)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_692)))->BgL_nullz00);
		}

	}



/* &<@anonymous:1206> */
	obj_t BGl_z62zc3z04anonymousza31206ze3ze5zztype_typez00(obj_t BgL_envz00_693)
	{
		{	/* Type/type.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1205 */
	obj_t BGl_z62lambda1205z62zztype_typez00(obj_t BgL_envz00_694,
		obj_t BgL_oz00_695, obj_t BgL_vz00_696)
	{
		{	/* Type/type.scm 21 */
			{	/* Type/type.scm 21 */
				bool_t BgL_vz00_802;

				BgL_vz00_802 = CBOOL(BgL_vz00_696);
				return
					((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_oz00_695)))->BgL_magiczf3zf3) =
					((bool_t) BgL_vz00_802), BUNSPEC);
			}
		}

	}



/* &lambda1204 */
	obj_t BGl_z62lambda1204z62zztype_typez00(obj_t BgL_envz00_697,
		obj_t BgL_oz00_698)
	{
		{	/* Type/type.scm 21 */
			return
				BBOOL(
				(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_698)))->BgL_magiczf3zf3));
		}

	}



/* &lambda1199 */
	obj_t BGl_z62lambda1199z62zztype_typez00(obj_t BgL_envz00_699,
		obj_t BgL_oz00_700, obj_t BgL_vz00_701)
	{
		{	/* Type/type.scm 21 */
			{	/* Type/type.scm 21 */
				bool_t BgL_vz00_805;

				BgL_vz00_805 = CBOOL(BgL_vz00_701);
				return
					((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_oz00_700)))->BgL_initzf3zf3) =
					((bool_t) BgL_vz00_805), BUNSPEC);
			}
		}

	}



/* &lambda1198 */
	obj_t BGl_z62lambda1198z62zztype_typez00(obj_t BgL_envz00_702,
		obj_t BgL_oz00_703)
	{
		{	/* Type/type.scm 21 */
			return
				BBOOL(
				(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_703)))->BgL_initzf3zf3));
		}

	}



/* &<@anonymous:1193> */
	obj_t BGl_z62zc3z04anonymousza31193ze3ze5zztype_typez00(obj_t BgL_envz00_704)
	{
		{	/* Type/type.scm 21 */
			return BNIL;
		}

	}



/* &lambda1192 */
	obj_t BGl_z62lambda1192z62zztype_typez00(obj_t BgL_envz00_705,
		obj_t BgL_oz00_706, obj_t BgL_vz00_707)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_706)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_707), BUNSPEC);
		}

	}



/* &lambda1191 */
	obj_t BGl_z62lambda1191z62zztype_typez00(obj_t BgL_envz00_708,
		obj_t BgL_oz00_709)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_709)))->BgL_parentsz00);
		}

	}



/* &<@anonymous:1186> */
	obj_t BGl_z62zc3z04anonymousza31186ze3ze5zztype_typez00(obj_t BgL_envz00_710)
	{
		{	/* Type/type.scm 21 */
			return BNIL;
		}

	}



/* &lambda1185 */
	obj_t BGl_z62lambda1185z62zztype_typez00(obj_t BgL_envz00_711,
		obj_t BgL_oz00_712, obj_t BgL_vz00_713)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_712)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_713), BUNSPEC);
		}

	}



/* &lambda1184 */
	obj_t BGl_z62lambda1184z62zztype_typez00(obj_t BgL_envz00_714,
		obj_t BgL_oz00_715)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_715)))->BgL_coercezd2tozd2);
		}

	}



/* &<@anonymous:1167> */
	obj_t BGl_z62zc3z04anonymousza31167ze3ze5zztype_typez00(obj_t BgL_envz00_716)
	{
		{	/* Type/type.scm 21 */
			return CNST_TABLE_REF(0);
		}

	}



/* &lambda1166 */
	obj_t BGl_z62lambda1166z62zztype_typez00(obj_t BgL_envz00_717,
		obj_t BgL_oz00_718, obj_t BgL_vz00_719)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_718)))->BgL_classz00) =
				((obj_t) BgL_vz00_719), BUNSPEC);
		}

	}



/* &lambda1165 */
	obj_t BGl_z62lambda1165z62zztype_typez00(obj_t BgL_envz00_720,
		obj_t BgL_oz00_721)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_721)))->BgL_classz00);
		}

	}



/* &<@anonymous:1155> */
	obj_t BGl_z62zc3z04anonymousza31155ze3ze5zztype_typez00(obj_t BgL_envz00_722)
	{
		{	/* Type/type.scm 21 */
			return BUNSPEC;
		}

	}



/* &lambda1154 */
	obj_t BGl_z62lambda1154z62zztype_typez00(obj_t BgL_envz00_723,
		obj_t BgL_oz00_724, obj_t BgL_vz00_725)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_724)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_725), BUNSPEC);
		}

	}



/* &lambda1153 */
	obj_t BGl_z62lambda1153z62zztype_typez00(obj_t BgL_envz00_726,
		obj_t BgL_oz00_727)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_727)))->BgL_siza7eza7);
		}

	}



/* &<@anonymous:1144> */
	obj_t BGl_z62zc3z04anonymousza31144ze3ze5zztype_typez00(obj_t BgL_envz00_728)
	{
		{	/* Type/type.scm 21 */
			return BUNSPEC;
		}

	}



/* &lambda1143 */
	obj_t BGl_z62lambda1143z62zztype_typez00(obj_t BgL_envz00_729,
		obj_t BgL_oz00_730, obj_t BgL_vz00_731)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_730)))->BgL_namez00) =
				((obj_t) BgL_vz00_731), BUNSPEC);
		}

	}



/* &lambda1142 */
	obj_t BGl_z62lambda1142z62zztype_typez00(obj_t BgL_envz00_732,
		obj_t BgL_oz00_733)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_733)))->BgL_namez00);
		}

	}



/* &<@anonymous:1132> */
	obj_t BGl_z62zc3z04anonymousza31132ze3ze5zztype_typez00(obj_t BgL_envz00_734)
	{
		{	/* Type/type.scm 21 */
			return CNST_TABLE_REF(24);
		}

	}



/* &lambda1131 */
	obj_t BGl_z62lambda1131z62zztype_typez00(obj_t BgL_envz00_735,
		obj_t BgL_oz00_736, obj_t BgL_vz00_737)
	{
		{	/* Type/type.scm 21 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_736)))->BgL_idz00) = ((obj_t)
					((obj_t) BgL_vz00_737)), BUNSPEC);
		}

	}



/* &lambda1130 */
	obj_t BGl_z62lambda1130z62zztype_typez00(obj_t BgL_envz00_738,
		obj_t BgL_oz00_739)
	{
		{	/* Type/type.scm 21 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_739)))->BgL_idz00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztype_typez00(void)
	{
		{	/* Type/type.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_typezd2occurrencezd2incrementz12zd2envzc0zztype_typez00,
				BGl_proc1383z00zztype_typez00, BGl_typez00zztype_typez00,
				BGl_string1384z00zztype_typez00);
		}

	}



/* &type-occurrence-incr1073 */
	obj_t BGl_z62typezd2occurrencezd2incr1073z62zztype_typez00(obj_t
		BgL_envz00_741, obj_t BgL_tz00_742)
	{
		{	/* Type/type.scm 85 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_tz00_742)))->BgL_occurrencez00) = ((int)
					(int) (
						(1L +
							(long) (
								(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_tz00_742)))->
									BgL_occurrencez00))))), BUNSPEC);
		}

	}



/* type-occurrence-increment! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(BgL_typez00_bglt
		BgL_tz00_67)
	{
		{	/* Type/type.scm 85 */
			{	/* Type/type.scm 85 */
				obj_t BgL_method1074z00_335;

				{	/* Type/type.scm 85 */
					obj_t BgL_res1332z00_477;

					{	/* Type/type.scm 85 */
						long BgL_objzd2classzd2numz00_448;

						BgL_objzd2classzd2numz00_448 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_tz00_67));
						{	/* Type/type.scm 85 */
							obj_t BgL_arg1811z00_449;

							BgL_arg1811z00_449 =
								PROCEDURE_REF
								(BGl_typezd2occurrencezd2incrementz12zd2envzc0zztype_typez00,
								(int) (1L));
							{	/* Type/type.scm 85 */
								int BgL_offsetz00_452;

								BgL_offsetz00_452 = (int) (BgL_objzd2classzd2numz00_448);
								{	/* Type/type.scm 85 */
									long BgL_offsetz00_453;

									BgL_offsetz00_453 =
										((long) (BgL_offsetz00_452) - OBJECT_TYPE);
									{	/* Type/type.scm 85 */
										long BgL_modz00_454;

										BgL_modz00_454 =
											(BgL_offsetz00_453 >> (int) ((long) ((int) (4L))));
										{	/* Type/type.scm 85 */
											long BgL_restz00_456;

											BgL_restz00_456 =
												(BgL_offsetz00_453 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Type/type.scm 85 */

												{	/* Type/type.scm 85 */
													obj_t BgL_bucketz00_458;

													BgL_bucketz00_458 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_449), BgL_modz00_454);
													BgL_res1332z00_477 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_458), BgL_restz00_456);
					}}}}}}}}
					BgL_method1074z00_335 = BgL_res1332z00_477;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1074z00_335, ((obj_t) BgL_tz00_67));
			}
		}

	}



/* &type-occurrence-increment! */
	obj_t BGl_z62typezd2occurrencezd2incrementz12z70zztype_typez00(obj_t
		BgL_envz00_743, obj_t BgL_tz00_744)
	{
		{	/* Type/type.scm 85 */
			return
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
				((BgL_typez00_bglt) BgL_tz00_744));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztype_typez00(void)
	{
		{	/* Type/type.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztype_typez00(void)
	{
		{	/* Type/type.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1385z00zztype_typez00));
		}

	}

#ifdef __cplusplus
}
#endif
