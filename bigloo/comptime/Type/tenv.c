/*===========================================================================*/
/*   (Type/tenv.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Type/tenv.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TYPE_ENV_TYPE_DEFINITIONS
#define BGL_TYPE_ENV_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;


#endif													// BGL_TYPE_ENV_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_initializa7ezd2Tenvz12z67zztype_envz00(void);
	extern obj_t BGl_heapzd2addzd2jclassz12z12zzmodule_javaz00(obj_t);
	extern obj_t BGl_delayzd2tvectorz12zc0zzmodule_typez00(obj_t, obj_t, bool_t);
	BGL_EXPORTED_DECL bool_t BGl_typezd2existszf3z21zztype_envz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztype_envz00 = BUNSPEC;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_declarezd2subtypez12zc0zztype_envz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2tenvzd2zztype_envz00(void);
	static obj_t BGl_toplevelzd2initzd2zztype_envz00(void);
	static obj_t BGl_z62subzd2typezf3z43zztype_envz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zztype_envz00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_declarezd2typez12zc0zztype_envz00(obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	static obj_t BGl_z62initializa7ezd2Tenvz12z05zztype_envz00(obj_t);
	static BgL_typez00_bglt
		BGl_z62usezd2foreignzd2typezf2importzd2locz12z50zztype_envz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zztype_envz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_checkzd2typeszd2zztype_envz00(void);
	extern obj_t BGl_typez00zztype_typez00;
	extern bool_t BGl_ctypezf3zf3zzforeign_ctypez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31224ze3ze5zztype_envz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31216ze3ze5zztype_envz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_usezd2foreignzd2typez12z12zztype_envz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_subzd2typezf3z21zztype_envz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00;
	extern obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_hashtablezf3zf3zz__hashz00(obj_t);
	static BgL_typez00_bglt BGl_z62declarezd2subtypez12za2zztype_envz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_findzd2typezf2locationz20zztype_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31250ze3ze5zztype_envz00(obj_t, obj_t,
		obj_t);
	static bool_t BGl_za2typeszd2alreadyzd2checkedzf3za2zf3zztype_envz00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztype_envz00(void);
	BGL_IMPORT obj_t BGl_makezd2promisezd2zz__r4_control_features_6_9z00(obj_t);
	extern obj_t
		BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_bindzd2typez12zc0zztype_envz00(obj_t, bool_t,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62usezd2typezf2importzd2locz12z82zztype_envz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_declarezd2aliastypez12zc0zztype_envz00(obj_t, obj_t, obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31245ze3ze5zztype_envz00(obj_t);
	extern obj_t BGl_z42zd2inzd2namezf3zb1zztype_toolsz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_usezd2typezf2importzd2locz12ze0zztype_envz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezd2existszf3z43zztype_envz00(obj_t, obj_t);
	static obj_t BGl_z62checkzd2typeszb0zztype_envz00(obj_t);
	static obj_t BGl_z62setzd2tenvz12za2zztype_envz00(obj_t, obj_t);
	extern obj_t BGl_jclassz00zzobject_classz00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	static obj_t BGl_z62zc3z04anonymousza31614ze3ze5zztype_envz00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_javaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_foreignz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_creatorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_predicatez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_setzd2tenvz12zc0zztype_envz00(obj_t);
	static BgL_typez00_bglt BGl_z62usezd2foreignzd2typez12z70zztype_envz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31560ze3ze5zztype_envz00(obj_t, obj_t);
	extern obj_t BGl_za2allowzd2typezd2redefinitionza2z00zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zztype_envz00(void);
	static obj_t BGl_z62forzd2eachzd2typez12z70zztype_envz00(obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zztype_envz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zztype_envz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztype_envz00(void);
	extern obj_t BGl_tvecz00zztvector_tvectorz00;
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62findzd2typezb0zztype_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31554ze3ze5zztype_envz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62usezd2typez12za2zztype_envz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_usezd2foreignzd2typezf2importzd2locz12z32zztype_envz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_za2Tenvza2z00zztype_envz00 = BUNSPEC;
	extern obj_t BGl_foreignzd2accesseszd2addz12z12zzmodule_foreignz00(obj_t);
	static obj_t BGl_z62addzd2tenvz12za2zztype_envz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62declarezd2typez12za2zztype_envz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t
		BGl_makezd2ctypezd2accessesz12z12zzforeign_accessz00(BgL_typez00_bglt,
		BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62getzd2tenvzb0zztype_envz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_heapzd2addzd2classz12z12zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_forzd2eachzd2typez12z12zztype_envz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62findzd2typezf2locationz42zztype_envz00(obj_t,
		obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_delayzd2classzd2accessorsz12z12zzmodule_classz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_addzd2tenvz12zc0zztype_envz00(obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31576ze3ze5zztype_envz00(obj_t);
	static obj_t BGl_uninitializa7edzd2typesz75zztype_envz00(void);
	extern obj_t BGl_compilerzd2exitzd2zzinit_mainz00(obj_t);
	static BgL_typez00_bglt BGl_z62declarezd2aliastypez12za2zztype_envz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t __cnst[8];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_declarezd2typez12zd2envz12zztype_envz00,
		BgL_bgl_za762declareza7d2typ1799z00,
		BGl_z62declarezd2typez12za2zztype_envz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_checkzd2typeszd2envz00zztype_envz00,
		BgL_bgl_za762checkza7d2types1800z00, BGl_z62checkzd2typeszb0zztype_envz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_subzd2typezf3zd2envzf3zztype_envz00,
		BgL_bgl_za762subza7d2typeza7f31801za7, BGl_z62subzd2typezf3z43zztype_envz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2typezf2locationzd2envzf2zztype_envz00,
		BgL_bgl_za762findza7d2typeza7f1802za7,
		BGl_z62findzd2typezf2locationz42zztype_envz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_declarezd2subtypez12zd2envz12zztype_envz00,
		BgL_bgl_za762declareza7d2sub1803z00,
		BGl_z62declarezd2subtypez12za2zztype_envz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2tenvzd2envz00zztype_envz00,
		BgL_bgl_za762getza7d2tenvza7b01804za7, BGl_z62getzd2tenvzb0zztype_envz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_addzd2tenvz12zd2envz12zztype_envz00,
		BgL_bgl_za762addza7d2tenvza7121805za7, BGl_z62addzd2tenvz12za2zztype_envz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1763z00zztype_envz00,
		BgL_bgl_string1763za700za7za7t1806za7, "Can't find super class of", 25);
	      DEFINE_STRING(BGl_string1766z00zztype_envz00,
		BgL_bgl_string1766za700za7za7t1807za7, "add-Tenv!", 9);
	      DEFINE_STRING(BGl_string1767z00zztype_envz00,
		BgL_bgl_string1767za700za7za7t1808za7, "Illegal type heap redefinition",
		30);
	      DEFINE_STRING(BGl_string1768z00zztype_envz00,
		BgL_bgl_string1768za700za7za7t1809za7, "Can't find type", 15);
	      DEFINE_STRING(BGl_string1769z00zztype_envz00,
		BgL_bgl_string1769za700za7za7t1810za7, "Type redefinition", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1762z00zztype_envz00,
		BgL_bgl_za762za7c3za704anonymo1811za7,
		BGl_z62zc3z04anonymousza31224ze3ze5zztype_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1770z00zztype_envz00,
		BgL_bgl_string1770za700za7za7t1812za7, "declare-type!", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1764z00zztype_envz00,
		BgL_bgl_za762za7c3za704anonymo1813za7,
		BGl_z62zc3z04anonymousza31245ze3ze5zztype_envz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1771z00zztype_envz00,
		BgL_bgl_string1771za700za7za7t1814za7, "Illegal type class", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1765z00zztype_envz00,
		BgL_bgl_za762za7c3za704anonymo1815za7,
		BGl_z62zc3z04anonymousza31250ze3ze5zztype_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1772z00zztype_envz00,
		BgL_bgl_string1772za700za7za7t1816za7, " type(s) used but not defined.",
		30);
	      DEFINE_STRING(BGl_string1773z00zztype_envz00,
		BgL_bgl_string1773za700za7za7t1817za7, "Stopping compilation...", 23);
	      DEFINE_STRING(BGl_string1774z00zztype_envz00,
		BgL_bgl_string1774za700za7za7t1818za7,
		"Undefined type used in export clause", 36);
	      DEFINE_STRING(BGl_string1775z00zztype_envz00,
		BgL_bgl_string1775za700za7za7t1819za7, "Undefined used type", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_forzd2eachzd2typez12zd2envzc0zztype_envz00,
		BgL_bgl_za762forza7d2eachza7d21820za7,
		BGl_z62forzd2eachzd2typez12z70zztype_envz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1776z00zztype_envz00,
		BgL_bgl_string1776za700za7za7t1821za7, "Undefined type", 14);
	      DEFINE_STRING(BGl_string1777z00zztype_envz00,
		BgL_bgl_string1777za700za7za7t1822za7, "type_env", 8);
	      DEFINE_STRING(BGl_string1778z00zztype_envz00,
		BgL_bgl_string1778za700za7za7t1823za7,
		"(bigloo C _ java) use-type! bigloo bind-type! find-type heap add-Tenv the-global-environment ",
		93);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_usezd2typez12zd2envz12zztype_envz00,
		BgL_bgl_za762useza7d2typeza7121824za7, BGl_z62usezd2typez12za2zztype_envz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2tenvz12zd2envz12zztype_envz00,
		BgL_bgl_za762setza7d2tenvza7121825za7, BGl_z62setzd2tenvz12za2zztype_envz00,
		0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_errorzd2notifyzd2envz00zz__errorz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initializa7ezd2Tenvz12zd2envzb5zztype_envz00,
		BgL_bgl_za762initializa7a7eza71826za7,
		BGl_z62initializa7ezd2Tenvz12z05zztype_envz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2aliastypez12zd2envz12zztype_envz00,
		BgL_bgl_za762declareza7d2ali1827z00,
		BGl_z62declarezd2aliastypez12za2zztype_envz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_usezd2typezf2importzd2locz12zd2envz32zztype_envz00,
		BgL_bgl_za762useza7d2typeza7f21828za7,
		BGl_z62usezd2typezf2importzd2locz12z82zztype_envz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_usezd2foreignzd2typez12zd2envzc0zztype_envz00,
		BgL_bgl_za762useza7d2foreign1829z00,
		BGl_z62usezd2foreignzd2typez12z70zztype_envz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2existszf3zd2envzf3zztype_envz00,
		BgL_bgl_za762typeza7d2exists1830z00,
		BGl_z62typezd2existszf3z43zztype_envz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2typezd2envz00zztype_envz00,
		BgL_bgl_za762findza7d2typeza7b1831za7, BGl_z62findzd2typezb0zztype_envz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_usezd2foreignzd2typezf2importzd2locz12zd2envze0zztype_envz00,
		BgL_bgl_za762useza7d2foreign1832z00,
		BGl_z62usezd2foreignzd2typezf2importzd2locz12z50zztype_envz00, 0L, BUNSPEC,
		3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztype_envz00));
		     ADD_ROOT((void *) (&BGl_za2Tenvza2z00zztype_envz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long
		BgL_checksumz00_2773, char *BgL_fromz00_2774)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztype_envz00))
				{
					BGl_requirezd2initializa7ationz75zztype_envz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztype_envz00();
					BGl_libraryzd2moduleszd2initz00zztype_envz00();
					BGl_cnstzd2initzd2zztype_envz00();
					BGl_importedzd2moduleszd2initz00zztype_envz00();
					return BGl_toplevelzd2initzd2zztype_envz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztype_envz00(void)
	{
		{	/* Type/tenv.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"type_env");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"type_env");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"type_env");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "type_env");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "type_env");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztype_envz00(void)
	{
		{	/* Type/tenv.scm 15 */
			{	/* Type/tenv.scm 15 */
				obj_t BgL_cportz00_2613;

				{	/* Type/tenv.scm 15 */
					obj_t BgL_stringz00_2620;

					BgL_stringz00_2620 = BGl_string1778z00zztype_envz00;
					{	/* Type/tenv.scm 15 */
						obj_t BgL_startz00_2621;

						BgL_startz00_2621 = BINT(0L);
						{	/* Type/tenv.scm 15 */
							obj_t BgL_endz00_2622;

							BgL_endz00_2622 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2620)));
							{	/* Type/tenv.scm 15 */

								BgL_cportz00_2613 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2620, BgL_startz00_2621, BgL_endz00_2622);
				}}}}
				{
					long BgL_iz00_2614;

					BgL_iz00_2614 = 7L;
				BgL_loopz00_2615:
					if ((BgL_iz00_2614 == -1L))
						{	/* Type/tenv.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Type/tenv.scm 15 */
							{	/* Type/tenv.scm 15 */
								obj_t BgL_arg1798z00_2616;

								{	/* Type/tenv.scm 15 */

									{	/* Type/tenv.scm 15 */
										obj_t BgL_locationz00_2618;

										BgL_locationz00_2618 = BBOOL(((bool_t) 0));
										{	/* Type/tenv.scm 15 */

											BgL_arg1798z00_2616 =
												BGl_readz00zz__readerz00(BgL_cportz00_2613,
												BgL_locationz00_2618);
										}
									}
								}
								{	/* Type/tenv.scm 15 */
									int BgL_tmpz00_2806;

									BgL_tmpz00_2806 = (int) (BgL_iz00_2614);
									CNST_TABLE_SET(BgL_tmpz00_2806, BgL_arg1798z00_2616);
							}}
							{	/* Type/tenv.scm 15 */
								int BgL_auxz00_2619;

								BgL_auxz00_2619 = (int) ((BgL_iz00_2614 - 1L));
								{
									long BgL_iz00_2811;

									BgL_iz00_2811 = (long) (BgL_auxz00_2619);
									BgL_iz00_2614 = BgL_iz00_2811;
									goto BgL_loopz00_2615;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztype_envz00(void)
	{
		{	/* Type/tenv.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztype_envz00(void)
	{
		{	/* Type/tenv.scm 15 */
			BGl_za2Tenvza2z00zztype_envz00 = CNST_TABLE_REF(0);
			return (BGl_za2typeszd2alreadyzd2checkedzf3za2zf3zztype_envz00 =
				((bool_t) 0), BUNSPEC);
		}

	}



/* initialize-Tenv! */
	BGL_EXPORTED_DEF obj_t BGl_initializa7ezd2Tenvz12z67zztype_envz00(void)
	{
		{	/* Type/tenv.scm 75 */
			if (BGl_hashtablezf3zf3zz__hashz00(BGl_za2Tenvza2z00zztype_envz00))
				{	/* Type/tenv.scm 77 */
					return BFALSE;
				}
			else
				{	/* Type/tenv.scm 77 */
					return (BGl_za2Tenvza2z00zztype_envz00 =
						BGl_makezd2hashtablezd2zz__hashz00(BNIL), BUNSPEC);
				}
		}

	}



/* &initialize-Tenv! */
	obj_t BGl_z62initializa7ezd2Tenvz12z05zztype_envz00(obj_t BgL_envz00_2523)
	{
		{	/* Type/tenv.scm 75 */
			return BGl_initializa7ezd2Tenvz12z67zztype_envz00();
		}

	}



/* get-tenv */
	BGL_EXPORTED_DEF obj_t BGl_getzd2tenvzd2zztype_envz00(void)
	{
		{	/* Type/tenv.scm 83 */
			return BGl_za2Tenvza2z00zztype_envz00;
		}

	}



/* &get-tenv */
	obj_t BGl_z62getzd2tenvzb0zztype_envz00(obj_t BgL_envz00_2524)
	{
		{	/* Type/tenv.scm 83 */
			return BGl_getzd2tenvzd2zztype_envz00();
		}

	}



/* set-tenv! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2tenvz12zc0zztype_envz00(obj_t BgL_tenvz00_21)
	{
		{	/* Type/tenv.scm 89 */
			BGl_initializa7ezd2Tenvz12z67zztype_envz00();
			BGL_TAIL return BGl_addzd2tenvz12zc0zztype_envz00(BgL_tenvz00_21);
		}

	}



/* &set-tenv! */
	obj_t BGl_z62setzd2tenvz12za2zztype_envz00(obj_t BgL_envz00_2525,
		obj_t BgL_tenvz00_2526)
	{
		{	/* Type/tenv.scm 89 */
			return BGl_setzd2tenvz12zc0zztype_envz00(BgL_tenvz00_2526);
		}

	}



/* add-tenv! */
	BGL_EXPORTED_DEF obj_t BGl_addzd2tenvz12zc0zztype_envz00(obj_t BgL_tenvz00_22)
	{
		{	/* Type/tenv.scm 101 */
			{	/* Type/tenv.scm 155 */
				obj_t BgL_rememberzd2listzd2_2547;
				obj_t BgL_tvectorzd2listzd2_2548;

				BgL_rememberzd2listzd2_2547 = MAKE_CELL(BNIL);
				BgL_tvectorzd2listzd2_2548 = MAKE_CELL(BNIL);
				{	/* Type/tenv.scm 167 */
					obj_t BgL_zc3z04anonymousza31216ze3z87_2530;

					BgL_zc3z04anonymousza31216ze3z87_2530 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31216ze3ze5zztype_envz00,
						(int) (2L), (int) (2L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31216ze3z87_2530,
						(int) (0L), ((obj_t) BgL_rememberzd2listzd2_2547));
					PROCEDURE_SET(BgL_zc3z04anonymousza31216ze3z87_2530,
						(int) (1L), ((obj_t) BgL_tvectorzd2listzd2_2548));
					BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_tenvz00_22,
						BgL_zc3z04anonymousza31216ze3z87_2530);
				}
				BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_tenvz00_22,
					BGl_proc1762z00zztype_envz00);
				{
					obj_t BgL_l1181z00_1314;

					BgL_l1181z00_1314 = CELL_REF(BgL_rememberzd2listzd2_2547);
				BgL_zc3z04anonymousza31227ze3z87_1315:
					if (PAIRP(BgL_l1181z00_1314))
						{	/* Type/tenv.scm 200 */
							{	/* Type/tenv.scm 201 */
								obj_t BgL_newz00_1317;

								BgL_newz00_1317 = CAR(BgL_l1181z00_1314);
								{	/* Type/tenv.scm 201 */
									bool_t BgL_test1837z00_2837;

									{	/* Type/tenv.scm 201 */
										obj_t BgL_classz00_1913;

										BgL_classz00_1913 = BGl_tclassz00zzobject_classz00;
										if (BGL_OBJECTP(BgL_newz00_1317))
											{	/* Type/tenv.scm 201 */
												BgL_objectz00_bglt BgL_arg1807z00_1915;

												BgL_arg1807z00_1915 =
													(BgL_objectz00_bglt) (BgL_newz00_1317);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Type/tenv.scm 201 */
														long BgL_idxz00_1921;

														BgL_idxz00_1921 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1915);
														BgL_test1837z00_2837 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_1921 + 2L)) == BgL_classz00_1913);
													}
												else
													{	/* Type/tenv.scm 201 */
														bool_t BgL_res1745z00_1946;

														{	/* Type/tenv.scm 201 */
															obj_t BgL_oclassz00_1929;

															{	/* Type/tenv.scm 201 */
																obj_t BgL_arg1815z00_1937;
																long BgL_arg1816z00_1938;

																BgL_arg1815z00_1937 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Type/tenv.scm 201 */
																	long BgL_arg1817z00_1939;

																	BgL_arg1817z00_1939 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1915);
																	BgL_arg1816z00_1938 =
																		(BgL_arg1817z00_1939 - OBJECT_TYPE);
																}
																BgL_oclassz00_1929 =
																	VECTOR_REF(BgL_arg1815z00_1937,
																	BgL_arg1816z00_1938);
															}
															{	/* Type/tenv.scm 201 */
																bool_t BgL__ortest_1115z00_1930;

																BgL__ortest_1115z00_1930 =
																	(BgL_classz00_1913 == BgL_oclassz00_1929);
																if (BgL__ortest_1115z00_1930)
																	{	/* Type/tenv.scm 201 */
																		BgL_res1745z00_1946 =
																			BgL__ortest_1115z00_1930;
																	}
																else
																	{	/* Type/tenv.scm 201 */
																		long BgL_odepthz00_1931;

																		{	/* Type/tenv.scm 201 */
																			obj_t BgL_arg1804z00_1932;

																			BgL_arg1804z00_1932 =
																				(BgL_oclassz00_1929);
																			BgL_odepthz00_1931 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_1932);
																		}
																		if ((2L < BgL_odepthz00_1931))
																			{	/* Type/tenv.scm 201 */
																				obj_t BgL_arg1802z00_1934;

																				{	/* Type/tenv.scm 201 */
																					obj_t BgL_arg1803z00_1935;

																					BgL_arg1803z00_1935 =
																						(BgL_oclassz00_1929);
																					BgL_arg1802z00_1934 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_1935, 2L);
																				}
																				BgL_res1745z00_1946 =
																					(BgL_arg1802z00_1934 ==
																					BgL_classz00_1913);
																			}
																		else
																			{	/* Type/tenv.scm 201 */
																				BgL_res1745z00_1946 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1837z00_2837 = BgL_res1745z00_1946;
													}
											}
										else
											{	/* Type/tenv.scm 201 */
												BgL_test1837z00_2837 = ((bool_t) 0);
											}
									}
									if (BgL_test1837z00_2837)
										{	/* Type/tenv.scm 203 */
											obj_t BgL_superz00_1319;

											{
												BgL_tclassz00_bglt BgL_auxz00_2860;

												{
													obj_t BgL_auxz00_2861;

													{	/* Type/tenv.scm 203 */
														BgL_objectz00_bglt BgL_tmpz00_2862;

														BgL_tmpz00_2862 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_newz00_1317));
														BgL_auxz00_2861 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2862);
													}
													BgL_auxz00_2860 =
														((BgL_tclassz00_bglt) BgL_auxz00_2861);
												}
												BgL_superz00_1319 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2860))->
													BgL_itszd2superzd2);
											}
											{	/* Type/tenv.scm 204 */
												bool_t BgL_test1842z00_2868;

												{	/* Type/tenv.scm 204 */
													obj_t BgL_classz00_1949;

													BgL_classz00_1949 = BGl_tclassz00zzobject_classz00;
													if (BGL_OBJECTP(BgL_superz00_1319))
														{	/* Type/tenv.scm 204 */
															BgL_objectz00_bglt BgL_arg1807z00_1951;

															BgL_arg1807z00_1951 =
																(BgL_objectz00_bglt) (BgL_superz00_1319);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Type/tenv.scm 204 */
																	long BgL_idxz00_1957;

																	BgL_idxz00_1957 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_1951);
																	BgL_test1842z00_2868 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_1957 + 2L)) ==
																		BgL_classz00_1949);
																}
															else
																{	/* Type/tenv.scm 204 */
																	bool_t BgL_res1746z00_1982;

																	{	/* Type/tenv.scm 204 */
																		obj_t BgL_oclassz00_1965;

																		{	/* Type/tenv.scm 204 */
																			obj_t BgL_arg1815z00_1973;
																			long BgL_arg1816z00_1974;

																			BgL_arg1815z00_1973 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Type/tenv.scm 204 */
																				long BgL_arg1817z00_1975;

																				BgL_arg1817z00_1975 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_1951);
																				BgL_arg1816z00_1974 =
																					(BgL_arg1817z00_1975 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_1965 =
																				VECTOR_REF(BgL_arg1815z00_1973,
																				BgL_arg1816z00_1974);
																		}
																		{	/* Type/tenv.scm 204 */
																			bool_t BgL__ortest_1115z00_1966;

																			BgL__ortest_1115z00_1966 =
																				(BgL_classz00_1949 ==
																				BgL_oclassz00_1965);
																			if (BgL__ortest_1115z00_1966)
																				{	/* Type/tenv.scm 204 */
																					BgL_res1746z00_1982 =
																						BgL__ortest_1115z00_1966;
																				}
																			else
																				{	/* Type/tenv.scm 204 */
																					long BgL_odepthz00_1967;

																					{	/* Type/tenv.scm 204 */
																						obj_t BgL_arg1804z00_1968;

																						BgL_arg1804z00_1968 =
																							(BgL_oclassz00_1965);
																						BgL_odepthz00_1967 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_1968);
																					}
																					if ((2L < BgL_odepthz00_1967))
																						{	/* Type/tenv.scm 204 */
																							obj_t BgL_arg1802z00_1970;

																							{	/* Type/tenv.scm 204 */
																								obj_t BgL_arg1803z00_1971;

																								BgL_arg1803z00_1971 =
																									(BgL_oclassz00_1965);
																								BgL_arg1802z00_1970 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_1971, 2L);
																							}
																							BgL_res1746z00_1982 =
																								(BgL_arg1802z00_1970 ==
																								BgL_classz00_1949);
																						}
																					else
																						{	/* Type/tenv.scm 204 */
																							BgL_res1746z00_1982 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1842z00_2868 = BgL_res1746z00_1982;
																}
														}
													else
														{	/* Type/tenv.scm 204 */
															BgL_test1842z00_2868 = ((bool_t) 0);
														}
												}
												if (BgL_test1842z00_2868)
													{	/* Type/tenv.scm 205 */
														obj_t BgL_superzd2idzd2_1321;

														BgL_superzd2idzd2_1321 =
															(((BgL_typez00_bglt) COBJECT(
																	((BgL_typez00_bglt)
																		((BgL_typez00_bglt) BgL_superz00_1319))))->
															BgL_idz00);
														{	/* Type/tenv.scm 205 */
															BgL_typez00_bglt BgL_oldzd2szd2_1322;

															BgL_oldzd2szd2_1322 =
																BGl_findzd2typezd2zztype_envz00
																(BgL_superzd2idzd2_1321);
															{	/* Type/tenv.scm 206 */

																{	/* Type/tenv.scm 207 */
																	bool_t BgL_test1847z00_2895;

																	{	/* Type/tenv.scm 207 */
																		obj_t BgL_classz00_1984;

																		BgL_classz00_1984 =
																			BGl_tclassz00zzobject_classz00;
																		{	/* Type/tenv.scm 207 */
																			BgL_objectz00_bglt BgL_arg1807z00_1986;

																			{	/* Type/tenv.scm 207 */
																				obj_t BgL_tmpz00_2896;

																				BgL_tmpz00_2896 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_oldzd2szd2_1322));
																				BgL_arg1807z00_1986 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_2896);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Type/tenv.scm 207 */
																					long BgL_idxz00_1992;

																					BgL_idxz00_1992 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_1986);
																					BgL_test1847z00_2895 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_1992 + 2L)) ==
																						BgL_classz00_1984);
																				}
																			else
																				{	/* Type/tenv.scm 207 */
																					bool_t BgL_res1747z00_2017;

																					{	/* Type/tenv.scm 207 */
																						obj_t BgL_oclassz00_2000;

																						{	/* Type/tenv.scm 207 */
																							obj_t BgL_arg1815z00_2008;
																							long BgL_arg1816z00_2009;

																							BgL_arg1815z00_2008 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Type/tenv.scm 207 */
																								long BgL_arg1817z00_2010;

																								BgL_arg1817z00_2010 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_1986);
																								BgL_arg1816z00_2009 =
																									(BgL_arg1817z00_2010 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2000 =
																								VECTOR_REF(BgL_arg1815z00_2008,
																								BgL_arg1816z00_2009);
																						}
																						{	/* Type/tenv.scm 207 */
																							bool_t BgL__ortest_1115z00_2001;

																							BgL__ortest_1115z00_2001 =
																								(BgL_classz00_1984 ==
																								BgL_oclassz00_2000);
																							if (BgL__ortest_1115z00_2001)
																								{	/* Type/tenv.scm 207 */
																									BgL_res1747z00_2017 =
																										BgL__ortest_1115z00_2001;
																								}
																							else
																								{	/* Type/tenv.scm 207 */
																									long BgL_odepthz00_2002;

																									{	/* Type/tenv.scm 207 */
																										obj_t BgL_arg1804z00_2003;

																										BgL_arg1804z00_2003 =
																											(BgL_oclassz00_2000);
																										BgL_odepthz00_2002 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2003);
																									}
																									if ((2L < BgL_odepthz00_2002))
																										{	/* Type/tenv.scm 207 */
																											obj_t BgL_arg1802z00_2005;

																											{	/* Type/tenv.scm 207 */
																												obj_t
																													BgL_arg1803z00_2006;
																												BgL_arg1803z00_2006 =
																													(BgL_oclassz00_2000);
																												BgL_arg1802z00_2005 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2006,
																													2L);
																											}
																											BgL_res1747z00_2017 =
																												(BgL_arg1802z00_2005 ==
																												BgL_classz00_1984);
																										}
																									else
																										{	/* Type/tenv.scm 207 */
																											BgL_res1747z00_2017 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1847z00_2895 =
																						BgL_res1747z00_2017;
																				}
																		}
																	}
																	if (BgL_test1847z00_2895)
																		{
																			BgL_tclassz00_bglt BgL_auxz00_2919;

																			{
																				obj_t BgL_auxz00_2920;

																				{	/* Type/tenv.scm 211 */
																					BgL_objectz00_bglt BgL_tmpz00_2921;

																					BgL_tmpz00_2921 =
																						((BgL_objectz00_bglt)
																						((BgL_typez00_bglt)
																							BgL_newz00_1317));
																					BgL_auxz00_2920 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_2921);
																				}
																				BgL_auxz00_2919 =
																					((BgL_tclassz00_bglt)
																					BgL_auxz00_2920);
																			}
																			((((BgL_tclassz00_bglt)
																						COBJECT(BgL_auxz00_2919))->
																					BgL_itszd2superzd2) =
																				((obj_t) ((obj_t) BgL_oldzd2szd2_1322)),
																				BUNSPEC);
																		}
																	else
																		{	/* Type/tenv.scm 210 */
																			obj_t BgL_arg1232z00_1324;

																			BgL_arg1232z00_1324 =
																				(((BgL_typez00_bglt) COBJECT(
																						((BgL_typez00_bglt)
																							((BgL_typez00_bglt)
																								BgL_newz00_1317))))->
																				BgL_namez00);
																			BGl_errorz00zz__errorz00(CNST_TABLE_REF
																				(1), BGl_string1763z00zztype_envz00,
																				BgL_arg1232z00_1324);
																		}
																}
															}
														}
													}
												else
													{	/* Type/tenv.scm 204 */
														BFALSE;
													}
											}
										}
									else
										{	/* Type/tenv.scm 201 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1181z00_2933;

								BgL_l1181z00_2933 = CDR(BgL_l1181z00_1314);
								BgL_l1181z00_1314 = BgL_l1181z00_2933;
								goto BgL_zc3z04anonymousza31227ze3z87_1315;
							}
						}
					else
						{	/* Type/tenv.scm 200 */
							((bool_t) 1);
						}
				}
				{
					obj_t BgL_l1183z00_1328;

					BgL_l1183z00_1328 = CELL_REF(BgL_tvectorzd2listzd2_2548);
				BgL_zc3z04anonymousza31234ze3z87_1329:
					if (PAIRP(BgL_l1183z00_1328))
						{	/* Type/tenv.scm 214 */
							{	/* Type/tenv.scm 215 */
								obj_t BgL_newz00_1331;

								BgL_newz00_1331 = CAR(BgL_l1183z00_1328);
								BGl_delayzd2tvectorz12zc0zzmodule_typez00(BgL_newz00_1331,
									CNST_TABLE_REF(2), ((bool_t) 0));
							}
							{
								obj_t BgL_l1183z00_2940;

								BgL_l1183z00_2940 = CDR(BgL_l1183z00_1328);
								BgL_l1183z00_1328 = BgL_l1183z00_2940;
								goto BgL_zc3z04anonymousza31234ze3z87_1329;
							}
						}
					else
						{	/* Type/tenv.scm 214 */
							((bool_t) 1);
						}
				}
				{
					obj_t BgL_l1185z00_1335;

					BgL_l1185z00_1335 = CELL_REF(BgL_rememberzd2listzd2_2547);
				BgL_zc3z04anonymousza31237ze3z87_1336:
					if (PAIRP(BgL_l1185z00_1335))
						{	/* Type/tenv.scm 222 */
							{	/* Type/tenv.scm 223 */
								obj_t BgL_nz00_1338;

								BgL_nz00_1338 = CAR(BgL_l1185z00_1335);
								{	/* Type/tenv.scm 223 */
									bool_t BgL_test1853z00_2945;

									{	/* Type/tenv.scm 223 */
										obj_t BgL_classz00_2025;

										BgL_classz00_2025 = BGl_tclassz00zzobject_classz00;
										if (BGL_OBJECTP(BgL_nz00_1338))
											{	/* Type/tenv.scm 223 */
												BgL_objectz00_bglt BgL_arg1807z00_2027;

												BgL_arg1807z00_2027 =
													(BgL_objectz00_bglt) (BgL_nz00_1338);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Type/tenv.scm 223 */
														long BgL_idxz00_2033;

														BgL_idxz00_2033 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2027);
														BgL_test1853z00_2945 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2033 + 2L)) == BgL_classz00_2025);
													}
												else
													{	/* Type/tenv.scm 223 */
														bool_t BgL_res1748z00_2058;

														{	/* Type/tenv.scm 223 */
															obj_t BgL_oclassz00_2041;

															{	/* Type/tenv.scm 223 */
																obj_t BgL_arg1815z00_2049;
																long BgL_arg1816z00_2050;

																BgL_arg1815z00_2049 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Type/tenv.scm 223 */
																	long BgL_arg1817z00_2051;

																	BgL_arg1817z00_2051 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2027);
																	BgL_arg1816z00_2050 =
																		(BgL_arg1817z00_2051 - OBJECT_TYPE);
																}
																BgL_oclassz00_2041 =
																	VECTOR_REF(BgL_arg1815z00_2049,
																	BgL_arg1816z00_2050);
															}
															{	/* Type/tenv.scm 223 */
																bool_t BgL__ortest_1115z00_2042;

																BgL__ortest_1115z00_2042 =
																	(BgL_classz00_2025 == BgL_oclassz00_2041);
																if (BgL__ortest_1115z00_2042)
																	{	/* Type/tenv.scm 223 */
																		BgL_res1748z00_2058 =
																			BgL__ortest_1115z00_2042;
																	}
																else
																	{	/* Type/tenv.scm 223 */
																		long BgL_odepthz00_2043;

																		{	/* Type/tenv.scm 223 */
																			obj_t BgL_arg1804z00_2044;

																			BgL_arg1804z00_2044 =
																				(BgL_oclassz00_2041);
																			BgL_odepthz00_2043 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2044);
																		}
																		if ((2L < BgL_odepthz00_2043))
																			{	/* Type/tenv.scm 223 */
																				obj_t BgL_arg1802z00_2046;

																				{	/* Type/tenv.scm 223 */
																					obj_t BgL_arg1803z00_2047;

																					BgL_arg1803z00_2047 =
																						(BgL_oclassz00_2041);
																					BgL_arg1802z00_2046 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2047, 2L);
																				}
																				BgL_res1748z00_2058 =
																					(BgL_arg1802z00_2046 ==
																					BgL_classz00_2025);
																			}
																		else
																			{	/* Type/tenv.scm 223 */
																				BgL_res1748z00_2058 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1853z00_2945 = BgL_res1748z00_2058;
													}
											}
										else
											{	/* Type/tenv.scm 223 */
												BgL_test1853z00_2945 = ((bool_t) 0);
											}
									}
									if (BgL_test1853z00_2945)
										{	/* Type/tenv.scm 230 */
											obj_t BgL_arg1242z00_1340;

											BgL_arg1242z00_1340 =
												BGl_makezd2promisezd2zz__r4_control_features_6_9z00
												(BGl_proc1764z00zztype_envz00);
											((obj_t)
												BGl_delayzd2classzd2accessorsz12z12zzmodule_classz00((
														(BgL_typez00_bglt) BgL_nz00_1338),
													BgL_arg1242z00_1340));
										}
									else
										{	/* Type/tenv.scm 223 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1185z00_2972;

								BgL_l1185z00_2972 = CDR(BgL_l1185z00_1335);
								BgL_l1185z00_1335 = BgL_l1185z00_2972;
								goto BgL_zc3z04anonymousza31237ze3z87_1336;
							}
						}
					else
						{	/* Type/tenv.scm 222 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_hashtablezd2forzd2eachz00zz__hashz00(BGl_za2Tenvza2z00zztype_envz00,
				BGl_proc1765z00zztype_envz00);
		}

	}



/* &add-tenv! */
	obj_t BGl_z62addzd2tenvz12za2zztype_envz00(obj_t BgL_envz00_2531,
		obj_t BgL_tenvz00_2532)
	{
		{	/* Type/tenv.scm 101 */
			return BGl_addzd2tenvz12zc0zztype_envz00(BgL_tenvz00_2532);
		}

	}



/* &<@anonymous:1250> */
	obj_t BGl_z62zc3z04anonymousza31250ze3ze5zztype_envz00(obj_t BgL_envz00_2533,
		obj_t BgL_kz00_2534, obj_t BgL_newz00_2535)
	{
		{	/* Type/tenv.scm 239 */
			{	/* Type/tenv.scm 240 */
				bool_t BgL_tmpz00_2976;

				{
					obj_t BgL_typez00_2625;

					BgL_typez00_2625 = BgL_newz00_2535;
					{	/* Type/tenv.scm 105 */
						bool_t BgL_test1858z00_2977;

						{	/* Type/tenv.scm 105 */
							obj_t BgL_arg1304z00_2626;

							BgL_arg1304z00_2626 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_typez00_2625)))->BgL_aliasz00);
							{	/* Type/tenv.scm 105 */
								obj_t BgL_classz00_2627;

								BgL_classz00_2627 = BGl_typez00zztype_typez00;
								if (BGL_OBJECTP(BgL_arg1304z00_2626))
									{	/* Type/tenv.scm 105 */
										BgL_objectz00_bglt BgL_arg1807z00_2628;

										BgL_arg1807z00_2628 =
											(BgL_objectz00_bglt) (BgL_arg1304z00_2626);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Type/tenv.scm 105 */
												long BgL_idxz00_2629;

												BgL_idxz00_2629 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2628);
												BgL_test1858z00_2977 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2629 + 1L)) == BgL_classz00_2627);
											}
										else
											{	/* Type/tenv.scm 105 */
												bool_t BgL_res1740z00_2632;

												{	/* Type/tenv.scm 105 */
													obj_t BgL_oclassz00_2633;

													{	/* Type/tenv.scm 105 */
														obj_t BgL_arg1815z00_2634;
														long BgL_arg1816z00_2635;

														BgL_arg1815z00_2634 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Type/tenv.scm 105 */
															long BgL_arg1817z00_2636;

															BgL_arg1817z00_2636 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2628);
															BgL_arg1816z00_2635 =
																(BgL_arg1817z00_2636 - OBJECT_TYPE);
														}
														BgL_oclassz00_2633 =
															VECTOR_REF(BgL_arg1815z00_2634,
															BgL_arg1816z00_2635);
													}
													{	/* Type/tenv.scm 105 */
														bool_t BgL__ortest_1115z00_2637;

														BgL__ortest_1115z00_2637 =
															(BgL_classz00_2627 == BgL_oclassz00_2633);
														if (BgL__ortest_1115z00_2637)
															{	/* Type/tenv.scm 105 */
																BgL_res1740z00_2632 = BgL__ortest_1115z00_2637;
															}
														else
															{	/* Type/tenv.scm 105 */
																long BgL_odepthz00_2638;

																{	/* Type/tenv.scm 105 */
																	obj_t BgL_arg1804z00_2639;

																	BgL_arg1804z00_2639 = (BgL_oclassz00_2633);
																	BgL_odepthz00_2638 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2639);
																}
																if ((1L < BgL_odepthz00_2638))
																	{	/* Type/tenv.scm 105 */
																		obj_t BgL_arg1802z00_2640;

																		{	/* Type/tenv.scm 105 */
																			obj_t BgL_arg1803z00_2641;

																			BgL_arg1803z00_2641 =
																				(BgL_oclassz00_2633);
																			BgL_arg1802z00_2640 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2641, 1L);
																		}
																		BgL_res1740z00_2632 =
																			(BgL_arg1802z00_2640 ==
																			BgL_classz00_2627);
																	}
																else
																	{	/* Type/tenv.scm 105 */
																		BgL_res1740z00_2632 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1858z00_2977 = BgL_res1740z00_2632;
											}
									}
								else
									{	/* Type/tenv.scm 105 */
										BgL_test1858z00_2977 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test1858z00_2977)
							{	/* Type/tenv.scm 106 */
								BgL_typez00_bglt BgL_arg1268z00_2642;

								{	/* Type/tenv.scm 106 */
									obj_t BgL_arg1272z00_2643;

									BgL_arg1272z00_2643 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt)
													(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_typez00_2625)))->
														BgL_aliasz00))))->BgL_idz00);
									BgL_arg1268z00_2642 =
										BGl_findzd2typezd2zztype_envz00(BgL_arg1272z00_2643);
								}
								((((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_typez00_2625)))->BgL_aliasz00) =
									((obj_t) ((obj_t) BgL_arg1268z00_2642)), BUNSPEC);
							}
						else
							{	/* Type/tenv.scm 105 */
								BFALSE;
							}
					}
					{	/* Type/tenv.scm 109 */
						obj_t BgL_arg1305z00_2644;

						{	/* Type/tenv.scm 109 */
							obj_t BgL_l1163z00_2645;

							BgL_l1163z00_2645 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_typez00_2625)))->BgL_parentsz00);
							if (NULLP(BgL_l1163z00_2645))
								{	/* Type/tenv.scm 109 */
									BgL_arg1305z00_2644 = BNIL;
								}
							else
								{	/* Type/tenv.scm 109 */
									obj_t BgL_head1165z00_2646;

									BgL_head1165z00_2646 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1163z00_2648;
										obj_t BgL_tail1166z00_2649;

										BgL_l1163z00_2648 = BgL_l1163z00_2645;
										BgL_tail1166z00_2649 = BgL_head1165z00_2646;
									BgL_zc3z04anonymousza31308ze3z87_2647:
										if (NULLP(BgL_l1163z00_2648))
											{	/* Type/tenv.scm 109 */
												BgL_arg1305z00_2644 = CDR(BgL_head1165z00_2646);
											}
										else
											{	/* Type/tenv.scm 109 */
												obj_t BgL_newtail1167z00_2650;

												{	/* Type/tenv.scm 109 */
													BgL_typez00_bglt BgL_arg1311z00_2651;

													{	/* Type/tenv.scm 109 */
														obj_t BgL_parentz00_2652;

														BgL_parentz00_2652 =
															CAR(((obj_t) BgL_l1163z00_2648));
														{	/* Type/tenv.scm 110 */
															obj_t BgL_arg1312z00_2653;

															BgL_arg1312z00_2653 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt) BgL_parentz00_2652)))->
																BgL_idz00);
															BgL_arg1311z00_2651 =
																BGl_findzd2typezd2zztype_envz00
																(BgL_arg1312z00_2653);
														}
													}
													BgL_newtail1167z00_2650 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg1311z00_2651), BNIL);
												}
												SET_CDR(BgL_tail1166z00_2649, BgL_newtail1167z00_2650);
												{	/* Type/tenv.scm 109 */
													obj_t BgL_arg1310z00_2654;

													BgL_arg1310z00_2654 =
														CDR(((obj_t) BgL_l1163z00_2648));
													{
														obj_t BgL_tail1166z00_3029;
														obj_t BgL_l1163z00_3028;

														BgL_l1163z00_3028 = BgL_arg1310z00_2654;
														BgL_tail1166z00_3029 = BgL_newtail1167z00_2650;
														BgL_tail1166z00_2649 = BgL_tail1166z00_3029;
														BgL_l1163z00_2648 = BgL_l1163z00_3028;
														goto BgL_zc3z04anonymousza31308ze3z87_2647;
													}
												}
											}
									}
								}
						}
						((((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_typez00_2625)))->BgL_parentsz00) =
							((obj_t) BgL_arg1305z00_2644), BUNSPEC);
					}
					{	/* Type/tenv.scm 113 */
						obj_t BgL_g1177z00_2655;

						BgL_g1177z00_2655 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_typez00_2625)))->BgL_coercezd2tozd2);
						{
							obj_t BgL_l1175z00_2657;

							BgL_l1175z00_2657 = BgL_g1177z00_2655;
						BgL_zc3z04anonymousza31313ze3z87_2656:
							if (PAIRP(BgL_l1175z00_2657))
								{	/* Type/tenv.scm 124 */
									{	/* Type/tenv.scm 114 */
										obj_t BgL_coercerz00_2658;

										BgL_coercerz00_2658 = CAR(BgL_l1175z00_2657);
										{	/* Type/tenv.scm 114 */
											obj_t BgL_fromz00_2659;
											obj_t BgL_toz00_2660;

											BgL_fromz00_2659 =
												STRUCT_REF(((obj_t) BgL_coercerz00_2658), (int) (0L));
											BgL_toz00_2660 =
												STRUCT_REF(((obj_t) BgL_coercerz00_2658), (int) (1L));
											{	/* Type/tenv.scm 116 */
												BgL_typez00_bglt BgL_arg1315z00_2661;

												{	/* Type/tenv.scm 116 */
													obj_t BgL_arg1316z00_2662;

													BgL_arg1316z00_2662 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_fromz00_2659)))->
														BgL_idz00);
													BgL_arg1315z00_2661 =
														BGl_findzd2typezd2zztype_envz00
														(BgL_arg1316z00_2662);
												}
												{	/* Type/tenv.scm 116 */
													obj_t BgL_auxz00_3050;
													int BgL_auxz00_3048;
													obj_t BgL_tmpz00_3046;

													BgL_auxz00_3050 = ((obj_t) BgL_arg1315z00_2661);
													BgL_auxz00_3048 = (int) (0L);
													BgL_tmpz00_3046 = ((obj_t) BgL_coercerz00_2658);
													STRUCT_SET(BgL_tmpz00_3046, BgL_auxz00_3048,
														BgL_auxz00_3050);
											}}
											{	/* Type/tenv.scm 117 */
												BgL_typez00_bglt BgL_arg1317z00_2663;

												{	/* Type/tenv.scm 117 */
													obj_t BgL_arg1318z00_2664;

													BgL_arg1318z00_2664 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_toz00_2660)))->
														BgL_idz00);
													BgL_arg1317z00_2663 =
														BGl_findzd2typezd2zztype_envz00
														(BgL_arg1318z00_2664);
												}
												{	/* Type/tenv.scm 117 */
													obj_t BgL_auxz00_3060;
													int BgL_auxz00_3058;
													obj_t BgL_tmpz00_3056;

													BgL_auxz00_3060 = ((obj_t) BgL_arg1317z00_2663);
													BgL_auxz00_3058 = (int) (1L);
													BgL_tmpz00_3056 = ((obj_t) BgL_coercerz00_2658);
													STRUCT_SET(BgL_tmpz00_3056, BgL_auxz00_3058,
														BgL_auxz00_3060);
											}}
											{	/* Type/tenv.scm 118 */
												obj_t BgL_g1171z00_2665;

												BgL_g1171z00_2665 =
													STRUCT_REF(((obj_t) BgL_coercerz00_2658), (int) (2L));
												{
													obj_t BgL_l1168z00_2667;

													BgL_l1168z00_2667 = BgL_g1171z00_2665;
												BgL_zc3z04anonymousza31319ze3z87_2666:
													if (PAIRP(BgL_l1168z00_2667))
														{	/* Type/tenv.scm 120 */
															{	/* Type/tenv.scm 119 */
																obj_t BgL_coz00_2668;

																BgL_coz00_2668 = CAR(BgL_l1168z00_2667);
																{	/* Type/tenv.scm 119 */
																	BgL_typez00_bglt BgL_arg1321z00_2669;

																	{	/* Type/tenv.scm 119 */
																		obj_t BgL_arg1322z00_2670;

																		BgL_arg1322z00_2670 =
																			(((BgL_typez00_bglt) COBJECT(
																					((BgL_typez00_bglt)
																						CDR(
																							((obj_t) BgL_coz00_2668)))))->
																			BgL_idz00);
																		BgL_arg1321z00_2669 =
																			BGl_findzd2typezd2zztype_envz00
																			(BgL_arg1322z00_2670);
																	}
																	{	/* Type/tenv.scm 119 */
																		obj_t BgL_auxz00_3076;
																		obj_t BgL_tmpz00_3074;

																		BgL_auxz00_3076 =
																			((obj_t) BgL_arg1321z00_2669);
																		BgL_tmpz00_3074 = ((obj_t) BgL_coz00_2668);
																		SET_CDR(BgL_tmpz00_3074, BgL_auxz00_3076);
																	}
																}
															}
															{
																obj_t BgL_l1168z00_3079;

																BgL_l1168z00_3079 = CDR(BgL_l1168z00_2667);
																BgL_l1168z00_2667 = BgL_l1168z00_3079;
																goto BgL_zc3z04anonymousza31319ze3z87_2666;
															}
														}
													else
														{	/* Type/tenv.scm 120 */
															((bool_t) 1);
														}
												}
											}
											{	/* Type/tenv.scm 121 */
												obj_t BgL_g1174z00_2671;

												BgL_g1174z00_2671 =
													STRUCT_REF(((obj_t) BgL_coercerz00_2658), (int) (3L));
												{
													obj_t BgL_l1172z00_2673;

													BgL_l1172z00_2673 = BgL_g1174z00_2671;
												BgL_zc3z04anonymousza31326ze3z87_2672:
													if (PAIRP(BgL_l1172z00_2673))
														{	/* Type/tenv.scm 123 */
															{	/* Type/tenv.scm 122 */
																obj_t BgL_coz00_2674;

																BgL_coz00_2674 = CAR(BgL_l1172z00_2673);
																{	/* Type/tenv.scm 122 */
																	BgL_typez00_bglt BgL_arg1328z00_2675;

																	{	/* Type/tenv.scm 122 */
																		obj_t BgL_arg1329z00_2676;

																		BgL_arg1329z00_2676 =
																			(((BgL_typez00_bglt) COBJECT(
																					((BgL_typez00_bglt)
																						CDR(
																							((obj_t) BgL_coz00_2674)))))->
																			BgL_idz00);
																		BgL_arg1328z00_2675 =
																			BGl_findzd2typezd2zztype_envz00
																			(BgL_arg1329z00_2676);
																	}
																	{	/* Type/tenv.scm 122 */
																		obj_t BgL_auxz00_3094;
																		obj_t BgL_tmpz00_3092;

																		BgL_auxz00_3094 =
																			((obj_t) BgL_arg1328z00_2675);
																		BgL_tmpz00_3092 = ((obj_t) BgL_coz00_2674);
																		SET_CDR(BgL_tmpz00_3092, BgL_auxz00_3094);
																	}
																}
															}
															{
																obj_t BgL_l1172z00_3097;

																BgL_l1172z00_3097 = CDR(BgL_l1172z00_2673);
																BgL_l1172z00_2673 = BgL_l1172z00_3097;
																goto BgL_zc3z04anonymousza31326ze3z87_2672;
															}
														}
													else
														{	/* Type/tenv.scm 123 */
															((bool_t) 1);
														}
												}
											}
										}
									}
									{
										obj_t BgL_l1175z00_3099;

										BgL_l1175z00_3099 = CDR(BgL_l1175z00_2657);
										BgL_l1175z00_2657 = BgL_l1175z00_3099;
										goto BgL_zc3z04anonymousza31313ze3z87_2656;
									}
								}
							else
								{	/* Type/tenv.scm 124 */
									BgL_tmpz00_2976 = ((bool_t) 1);
								}
						}
					}
				}
				return BBOOL(BgL_tmpz00_2976);
			}
		}

	}



/* &<@anonymous:1245> */
	obj_t BGl_z62zc3z04anonymousza31245ze3ze5zztype_envz00(obj_t BgL_envz00_2536)
	{
		{	/* Type/tenv.scm 226 */
			return BNIL;
		}

	}



/* &<@anonymous:1224> */
	obj_t BGl_z62zc3z04anonymousza31224ze3ze5zztype_envz00(obj_t BgL_envz00_2537,
		obj_t BgL_kz00_2538, obj_t BgL_newz00_2539)
	{
		{	/* Type/tenv.scm 191 */
			{	/* Type/tenv.scm 192 */
				obj_t BgL_idz00_2677;

				BgL_idz00_2677 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_newz00_2539)))->BgL_idz00);
				{	/* Type/tenv.scm 192 */
					obj_t BgL_oldz00_2678;

					BgL_oldz00_2678 =
						BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Tenvza2z00zztype_envz00,
						BgL_idz00_2677);
					{	/* Type/tenv.scm 193 */

						if (BGl_ctypezf3zf3zzforeign_ctypez00(BgL_oldz00_2678))
							{	/* Type/tenv.scm 195 */
								obj_t BgL_lz00_2679;

								BgL_lz00_2679 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_oldz00_2678)))->BgL_locationz00);
								{	/* Type/tenv.scm 197 */
									obj_t BgL_arg1226z00_2680;

									BgL_arg1226z00_2680 =
										BGl_makezd2ctypezd2accessesz12z12zzforeign_accessz00(
										((BgL_typez00_bglt) BgL_oldz00_2678),
										((BgL_typez00_bglt) BgL_oldz00_2678), BgL_lz00_2679);
									return
										BGl_foreignzd2accesseszd2addz12z12zzmodule_foreignz00
										(BgL_arg1226z00_2680);
								}
							}
						else
							{	/* Type/tenv.scm 194 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1216> */
	obj_t BGl_z62zc3z04anonymousza31216ze3ze5zztype_envz00(obj_t BgL_envz00_2540,
		obj_t BgL_kz00_2543, obj_t BgL_newz00_2544)
	{
		{	/* Type/tenv.scm 166 */
			{	/* Type/tenv.scm 167 */
				obj_t BgL_rememberzd2listzd2_2541;
				obj_t BgL_tvectorzd2listzd2_2542;

				BgL_rememberzd2listzd2_2541 =
					PROCEDURE_REF(BgL_envz00_2540, (int) (0L));
				BgL_tvectorzd2listzd2_2542 = PROCEDURE_REF(BgL_envz00_2540, (int) (1L));
				{
					obj_t BgL_oldz00_2688;
					obj_t BgL_newz00_2689;
					obj_t BgL_fromz00_2683;
					BgL_typez00_bglt BgL_toz00_2684;

					{	/* Type/tenv.scm 167 */
						obj_t BgL_idz00_2700;

						BgL_idz00_2700 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_newz00_2544)))->BgL_idz00);
						{	/* Type/tenv.scm 167 */
							obj_t BgL_oldz00_2701;

							BgL_oldz00_2701 =
								BGl_hashtablezd2getzd2zz__hashz00
								(BGl_za2Tenvza2z00zztype_envz00, BgL_idz00_2700);
							{	/* Type/tenv.scm 168 */

								{	/* Type/tenv.scm 170 */
									bool_t BgL_test1869z00_3120;

									{	/* Type/tenv.scm 170 */
										obj_t BgL_classz00_2702;

										BgL_classz00_2702 = BGl_typez00zztype_typez00;
										if (BGL_OBJECTP(BgL_oldz00_2701))
											{	/* Type/tenv.scm 170 */
												BgL_objectz00_bglt BgL_arg1807z00_2703;

												BgL_arg1807z00_2703 =
													(BgL_objectz00_bglt) (BgL_oldz00_2701);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Type/tenv.scm 170 */
														long BgL_idxz00_2704;

														BgL_idxz00_2704 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2703);
														BgL_test1869z00_3120 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2704 + 1L)) == BgL_classz00_2702);
													}
												else
													{	/* Type/tenv.scm 170 */
														bool_t BgL_res1741z00_2707;

														{	/* Type/tenv.scm 170 */
															obj_t BgL_oclassz00_2708;

															{	/* Type/tenv.scm 170 */
																obj_t BgL_arg1815z00_2709;
																long BgL_arg1816z00_2710;

																BgL_arg1815z00_2709 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Type/tenv.scm 170 */
																	long BgL_arg1817z00_2711;

																	BgL_arg1817z00_2711 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2703);
																	BgL_arg1816z00_2710 =
																		(BgL_arg1817z00_2711 - OBJECT_TYPE);
																}
																BgL_oclassz00_2708 =
																	VECTOR_REF(BgL_arg1815z00_2709,
																	BgL_arg1816z00_2710);
															}
															{	/* Type/tenv.scm 170 */
																bool_t BgL__ortest_1115z00_2712;

																BgL__ortest_1115z00_2712 =
																	(BgL_classz00_2702 == BgL_oclassz00_2708);
																if (BgL__ortest_1115z00_2712)
																	{	/* Type/tenv.scm 170 */
																		BgL_res1741z00_2707 =
																			BgL__ortest_1115z00_2712;
																	}
																else
																	{	/* Type/tenv.scm 170 */
																		long BgL_odepthz00_2713;

																		{	/* Type/tenv.scm 170 */
																			obj_t BgL_arg1804z00_2714;

																			BgL_arg1804z00_2714 =
																				(BgL_oclassz00_2708);
																			BgL_odepthz00_2713 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2714);
																		}
																		if ((1L < BgL_odepthz00_2713))
																			{	/* Type/tenv.scm 170 */
																				obj_t BgL_arg1802z00_2715;

																				{	/* Type/tenv.scm 170 */
																					obj_t BgL_arg1803z00_2716;

																					BgL_arg1803z00_2716 =
																						(BgL_oclassz00_2708);
																					BgL_arg1802z00_2715 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2716, 1L);
																				}
																				BgL_res1741z00_2707 =
																					(BgL_arg1802z00_2715 ==
																					BgL_classz00_2702);
																			}
																		else
																			{	/* Type/tenv.scm 170 */
																				BgL_res1741z00_2707 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1869z00_3120 = BgL_res1741z00_2707;
													}
											}
										else
											{	/* Type/tenv.scm 170 */
												BgL_test1869z00_3120 = ((bool_t) 0);
											}
									}
									if (BgL_test1869z00_3120)
										{	/* Type/tenv.scm 170 */
											if (
												(((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt) BgL_oldz00_2701)))->
													BgL_initzf3zf3))
												{	/* Type/tenv.scm 189 */
													bool_t BgL_tmpz00_3146;

													BgL_oldz00_2688 = BgL_oldz00_2701;
													BgL_newz00_2689 = BgL_newz00_2544;
													{	/* Type/tenv.scm 134 */
														obj_t BgL_g1180z00_2690;

														BgL_g1180z00_2690 =
															(((BgL_typez00_bglt) COBJECT(
																	((BgL_typez00_bglt) BgL_newz00_2689)))->
															BgL_coercezd2tozd2);
														{
															obj_t BgL_l1178z00_2692;

															BgL_l1178z00_2692 = BgL_g1180z00_2690;
														BgL_zc3z04anonymousza31348ze3z87_2691:
															if (PAIRP(BgL_l1178z00_2692))
																{	/* Type/tenv.scm 144 */
																	{	/* Type/tenv.scm 135 */
																		obj_t BgL_coercerz00_2693;

																		BgL_coercerz00_2693 =
																			CAR(BgL_l1178z00_2692);
																		{	/* Type/tenv.scm 135 */
																			obj_t BgL_toz00_2694;

																			BgL_toz00_2694 =
																				STRUCT_REF(
																				((obj_t) BgL_coercerz00_2693),
																				(int) (1L));
																			{	/* Type/tenv.scm 136 */
																				obj_t BgL_tidz00_2695;

																				BgL_tidz00_2695 =
																					(((BgL_typez00_bglt) COBJECT(
																							((BgL_typez00_bglt)
																								BgL_toz00_2694)))->BgL_idz00);
																				{	/* Type/tenv.scm 137 */
																					bool_t BgL_tidzd2existszf3z21_2696;

																					BgL_tidzd2existszf3z21_2696 =
																						BGl_typezd2existszf3z21zztype_envz00
																						(BgL_tidz00_2695);
																					{	/* Type/tenv.scm 138 */

																						{	/* Type/tenv.scm 139 */
																							bool_t BgL_test1876z00_3158;

																							if (BgL_tidzd2existszf3z21_2696)
																								{	/* Type/tenv.scm 140 */
																									bool_t BgL_test1878z00_3160;

																									{	/* Type/tenv.scm 140 */
																										BgL_typez00_bglt
																											BgL_arg1370z00_2697;
																										BgL_arg1370z00_2697 =
																											BGl_findzd2typezd2zztype_envz00
																											(BgL_tidz00_2695);
																										{	/* Type/tenv.scm 140 */
																											obj_t BgL_tmpz00_3162;

																											BgL_fromz00_2683 =
																												BgL_oldz00_2688;
																											BgL_toz00_2684 =
																												BgL_arg1370z00_2697;
																											{
																												obj_t
																													BgL_coercerz00_2686;
																												BgL_coercerz00_2686 =
																													(((BgL_typez00_bglt)
																														COBJECT((
																																(BgL_typez00_bglt)
																																BgL_fromz00_2683)))->
																													BgL_coercezd2tozd2);
																											BgL_loopz00_2685:
																												if (NULLP
																													(BgL_coercerz00_2686))
																													{	/* Type/tenv.scm 129 */
																														BgL_tmpz00_3162 =
																															BFALSE;
																													}
																												else
																													{	/* Type/tenv.scm 130 */
																														bool_t
																															BgL_test1880z00_3165;
																														{	/* Type/tenv.scm 130 */
																															obj_t
																																BgL_tmpz00_3166;
																															{	/* Type/tenv.scm 130 */
																																obj_t
																																	BgL_sz00_2687;
																																BgL_sz00_2687 =
																																	CAR(((obj_t)
																																		BgL_coercerz00_2686));
																																BgL_tmpz00_3166
																																	=
																																	STRUCT_REF
																																	(BgL_sz00_2687,
																																	(int) (1L));
																															}
																															BgL_test1880z00_3165
																																=
																																(BgL_tmpz00_3166
																																==
																																((obj_t)
																																	BgL_toz00_2684));
																														}
																														if (BgL_test1880z00_3165)
																															{	/* Type/tenv.scm 130 */
																																BgL_tmpz00_3162
																																	=
																																	CAR(((obj_t)
																																		BgL_coercerz00_2686));
																															}
																														else
																															{
																																obj_t
																																	BgL_coercerz00_3175;
																																BgL_coercerz00_3175
																																	=
																																	CDR(((obj_t)
																																		BgL_coercerz00_2686));
																																BgL_coercerz00_2686
																																	=
																																	BgL_coercerz00_3175;
																																goto
																																	BgL_loopz00_2685;
																															}
																													}
																											}
																											BgL_test1878z00_3160 =
																												CBOOL(BgL_tmpz00_3162);
																										}
																									}
																									if (BgL_test1878z00_3160)
																										{	/* Type/tenv.scm 140 */
																											BgL_test1876z00_3158 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Type/tenv.scm 140 */
																											BgL_test1876z00_3158 =
																												((bool_t) 1);
																										}
																								}
																							else
																								{	/* Type/tenv.scm 139 */
																									BgL_test1876z00_3158 =
																										((bool_t) 1);
																								}
																							if (BgL_test1876z00_3158)
																								{	/* Type/tenv.scm 143 */
																									obj_t BgL_arg1364z00_2698;

																									{	/* Type/tenv.scm 143 */
																										obj_t BgL_arg1367z00_2699;

																										BgL_arg1367z00_2699 =
																											(((BgL_typez00_bglt)
																												COBJECT((
																														(BgL_typez00_bglt)
																														BgL_oldz00_2688)))->
																											BgL_coercezd2tozd2);
																										BgL_arg1364z00_2698 =
																											MAKE_YOUNG_PAIR
																											(BgL_coercerz00_2693,
																											BgL_arg1367z00_2699);
																									}
																									((((BgL_typez00_bglt) COBJECT(
																													((BgL_typez00_bglt)
																														BgL_oldz00_2688)))->
																											BgL_coercezd2tozd2) =
																										((obj_t)
																											BgL_arg1364z00_2698),
																										BUNSPEC);
																								}
																							else
																								{	/* Type/tenv.scm 139 */
																									BFALSE;
																								}
																						}
																					}
																				}
																			}
																		}
																	}
																	{
																		obj_t BgL_l1178z00_3186;

																		BgL_l1178z00_3186 = CDR(BgL_l1178z00_2692);
																		BgL_l1178z00_2692 = BgL_l1178z00_3186;
																		goto BgL_zc3z04anonymousza31348ze3z87_2691;
																	}
																}
															else
																{	/* Type/tenv.scm 144 */
																	BgL_tmpz00_3146 = ((bool_t) 1);
																}
														}
													}
													return BBOOL(BgL_tmpz00_3146);
												}
											else
												{	/* Type/tenv.scm 181 */
													BGl_errorz00zz__errorz00
														(BGl_string1766z00zztype_envz00,
														BGl_string1767z00zztype_envz00, BgL_idz00_2700);
													return
														BGl_compilerzd2exitzd2zzinit_mainz00(BINT(55L));
												}
										}
									else
										{	/* Type/tenv.scm 170 */
											BGl_hashtablezd2putz12zc0zz__hashz00
												(BGl_za2Tenvza2z00zztype_envz00, BgL_idz00_2700,
												BgL_newz00_2544);
											{	/* Type/tenv.scm 172 */
												bool_t BgL_test1881z00_3193;

												{	/* Type/tenv.scm 172 */
													obj_t BgL_classz00_2717;

													BgL_classz00_2717 = BGl_tclassz00zzobject_classz00;
													if (BGL_OBJECTP(BgL_newz00_2544))
														{	/* Type/tenv.scm 172 */
															BgL_objectz00_bglt BgL_arg1807z00_2718;

															BgL_arg1807z00_2718 =
																(BgL_objectz00_bglt) (BgL_newz00_2544);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Type/tenv.scm 172 */
																	long BgL_idxz00_2719;

																	BgL_idxz00_2719 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2718);
																	BgL_test1881z00_3193 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2719 + 2L)) ==
																		BgL_classz00_2717);
																}
															else
																{	/* Type/tenv.scm 172 */
																	bool_t BgL_res1742z00_2722;

																	{	/* Type/tenv.scm 172 */
																		obj_t BgL_oclassz00_2723;

																		{	/* Type/tenv.scm 172 */
																			obj_t BgL_arg1815z00_2724;
																			long BgL_arg1816z00_2725;

																			BgL_arg1815z00_2724 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Type/tenv.scm 172 */
																				long BgL_arg1817z00_2726;

																				BgL_arg1817z00_2726 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2718);
																				BgL_arg1816z00_2725 =
																					(BgL_arg1817z00_2726 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2723 =
																				VECTOR_REF(BgL_arg1815z00_2724,
																				BgL_arg1816z00_2725);
																		}
																		{	/* Type/tenv.scm 172 */
																			bool_t BgL__ortest_1115z00_2727;

																			BgL__ortest_1115z00_2727 =
																				(BgL_classz00_2717 ==
																				BgL_oclassz00_2723);
																			if (BgL__ortest_1115z00_2727)
																				{	/* Type/tenv.scm 172 */
																					BgL_res1742z00_2722 =
																						BgL__ortest_1115z00_2727;
																				}
																			else
																				{	/* Type/tenv.scm 172 */
																					long BgL_odepthz00_2728;

																					{	/* Type/tenv.scm 172 */
																						obj_t BgL_arg1804z00_2729;

																						BgL_arg1804z00_2729 =
																							(BgL_oclassz00_2723);
																						BgL_odepthz00_2728 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2729);
																					}
																					if ((2L < BgL_odepthz00_2728))
																						{	/* Type/tenv.scm 172 */
																							obj_t BgL_arg1802z00_2730;

																							{	/* Type/tenv.scm 172 */
																								obj_t BgL_arg1803z00_2731;

																								BgL_arg1803z00_2731 =
																									(BgL_oclassz00_2723);
																								BgL_arg1802z00_2730 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2731, 2L);
																							}
																							BgL_res1742z00_2722 =
																								(BgL_arg1802z00_2730 ==
																								BgL_classz00_2717);
																						}
																					else
																						{	/* Type/tenv.scm 172 */
																							BgL_res1742z00_2722 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1881z00_3193 = BgL_res1742z00_2722;
																}
														}
													else
														{	/* Type/tenv.scm 172 */
															BgL_test1881z00_3193 = ((bool_t) 0);
														}
												}
												if (BgL_test1881z00_3193)
													{	/* Type/tenv.scm 172 */
														BGl_heapzd2addzd2classz12z12zzobject_classz00(
															((BgL_typez00_bglt) BgL_newz00_2544));
														{	/* Type/tenv.scm 174 */
															obj_t BgL_auxz00_2732;

															BgL_auxz00_2732 =
																MAKE_YOUNG_PAIR(BgL_newz00_2544,
																CELL_REF(BgL_rememberzd2listzd2_2541));
															CELL_SET(BgL_rememberzd2listzd2_2541,
																BgL_auxz00_2732);
														}
													}
												else
													{	/* Type/tenv.scm 172 */
														BFALSE;
													}
											}
											{	/* Type/tenv.scm 176 */
												bool_t BgL_test1886z00_3219;

												{	/* Type/tenv.scm 176 */
													obj_t BgL_classz00_2733;

													BgL_classz00_2733 = BGl_jclassz00zzobject_classz00;
													if (BGL_OBJECTP(BgL_newz00_2544))
														{	/* Type/tenv.scm 176 */
															BgL_objectz00_bglt BgL_arg1807z00_2734;

															BgL_arg1807z00_2734 =
																(BgL_objectz00_bglt) (BgL_newz00_2544);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Type/tenv.scm 176 */
																	long BgL_idxz00_2735;

																	BgL_idxz00_2735 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2734);
																	BgL_test1886z00_3219 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2735 + 2L)) ==
																		BgL_classz00_2733);
																}
															else
																{	/* Type/tenv.scm 176 */
																	bool_t BgL_res1743z00_2738;

																	{	/* Type/tenv.scm 176 */
																		obj_t BgL_oclassz00_2739;

																		{	/* Type/tenv.scm 176 */
																			obj_t BgL_arg1815z00_2740;
																			long BgL_arg1816z00_2741;

																			BgL_arg1815z00_2740 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Type/tenv.scm 176 */
																				long BgL_arg1817z00_2742;

																				BgL_arg1817z00_2742 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2734);
																				BgL_arg1816z00_2741 =
																					(BgL_arg1817z00_2742 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2739 =
																				VECTOR_REF(BgL_arg1815z00_2740,
																				BgL_arg1816z00_2741);
																		}
																		{	/* Type/tenv.scm 176 */
																			bool_t BgL__ortest_1115z00_2743;

																			BgL__ortest_1115z00_2743 =
																				(BgL_classz00_2733 ==
																				BgL_oclassz00_2739);
																			if (BgL__ortest_1115z00_2743)
																				{	/* Type/tenv.scm 176 */
																					BgL_res1743z00_2738 =
																						BgL__ortest_1115z00_2743;
																				}
																			else
																				{	/* Type/tenv.scm 176 */
																					long BgL_odepthz00_2744;

																					{	/* Type/tenv.scm 176 */
																						obj_t BgL_arg1804z00_2745;

																						BgL_arg1804z00_2745 =
																							(BgL_oclassz00_2739);
																						BgL_odepthz00_2744 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2745);
																					}
																					if ((2L < BgL_odepthz00_2744))
																						{	/* Type/tenv.scm 176 */
																							obj_t BgL_arg1802z00_2746;

																							{	/* Type/tenv.scm 176 */
																								obj_t BgL_arg1803z00_2747;

																								BgL_arg1803z00_2747 =
																									(BgL_oclassz00_2739);
																								BgL_arg1802z00_2746 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2747, 2L);
																							}
																							BgL_res1743z00_2738 =
																								(BgL_arg1802z00_2746 ==
																								BgL_classz00_2733);
																						}
																					else
																						{	/* Type/tenv.scm 176 */
																							BgL_res1743z00_2738 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1886z00_3219 = BgL_res1743z00_2738;
																}
														}
													else
														{	/* Type/tenv.scm 176 */
															BgL_test1886z00_3219 = ((bool_t) 0);
														}
												}
												if (BgL_test1886z00_3219)
													{	/* Type/tenv.scm 176 */
														BGl_heapzd2addzd2jclassz12z12zzmodule_javaz00
															(BgL_newz00_2544);
													}
												else
													{	/* Type/tenv.scm 176 */
														BFALSE;
													}
											}
											{	/* Type/tenv.scm 178 */
												bool_t BgL_test1891z00_3243;

												{	/* Type/tenv.scm 178 */
													obj_t BgL_classz00_2748;

													BgL_classz00_2748 = BGl_tvecz00zztvector_tvectorz00;
													if (BGL_OBJECTP(BgL_newz00_2544))
														{	/* Type/tenv.scm 178 */
															BgL_objectz00_bglt BgL_arg1807z00_2749;

															BgL_arg1807z00_2749 =
																(BgL_objectz00_bglt) (BgL_newz00_2544);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Type/tenv.scm 178 */
																	long BgL_idxz00_2750;

																	BgL_idxz00_2750 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2749);
																	BgL_test1891z00_3243 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2750 + 2L)) ==
																		BgL_classz00_2748);
																}
															else
																{	/* Type/tenv.scm 178 */
																	bool_t BgL_res1744z00_2753;

																	{	/* Type/tenv.scm 178 */
																		obj_t BgL_oclassz00_2754;

																		{	/* Type/tenv.scm 178 */
																			obj_t BgL_arg1815z00_2755;
																			long BgL_arg1816z00_2756;

																			BgL_arg1815z00_2755 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Type/tenv.scm 178 */
																				long BgL_arg1817z00_2757;

																				BgL_arg1817z00_2757 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2749);
																				BgL_arg1816z00_2756 =
																					(BgL_arg1817z00_2757 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2754 =
																				VECTOR_REF(BgL_arg1815z00_2755,
																				BgL_arg1816z00_2756);
																		}
																		{	/* Type/tenv.scm 178 */
																			bool_t BgL__ortest_1115z00_2758;

																			BgL__ortest_1115z00_2758 =
																				(BgL_classz00_2748 ==
																				BgL_oclassz00_2754);
																			if (BgL__ortest_1115z00_2758)
																				{	/* Type/tenv.scm 178 */
																					BgL_res1744z00_2753 =
																						BgL__ortest_1115z00_2758;
																				}
																			else
																				{	/* Type/tenv.scm 178 */
																					long BgL_odepthz00_2759;

																					{	/* Type/tenv.scm 178 */
																						obj_t BgL_arg1804z00_2760;

																						BgL_arg1804z00_2760 =
																							(BgL_oclassz00_2754);
																						BgL_odepthz00_2759 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2760);
																					}
																					if ((2L < BgL_odepthz00_2759))
																						{	/* Type/tenv.scm 178 */
																							obj_t BgL_arg1802z00_2761;

																							{	/* Type/tenv.scm 178 */
																								obj_t BgL_arg1803z00_2762;

																								BgL_arg1803z00_2762 =
																									(BgL_oclassz00_2754);
																								BgL_arg1802z00_2761 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2762, 2L);
																							}
																							BgL_res1744z00_2753 =
																								(BgL_arg1802z00_2761 ==
																								BgL_classz00_2748);
																						}
																					else
																						{	/* Type/tenv.scm 178 */
																							BgL_res1744z00_2753 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1891z00_3243 = BgL_res1744z00_2753;
																}
														}
													else
														{	/* Type/tenv.scm 178 */
															BgL_test1891z00_3243 = ((bool_t) 0);
														}
												}
												if (BgL_test1891z00_3243)
													{	/* Type/tenv.scm 179 */
														obj_t BgL_auxz00_2763;

														BgL_auxz00_2763 =
															MAKE_YOUNG_PAIR(BgL_newz00_2544,
															CELL_REF(BgL_tvectorzd2listzd2_2542));
														return
															CELL_SET(BgL_tvectorzd2listzd2_2542,
															BgL_auxz00_2763);
													}
												else
													{	/* Type/tenv.scm 178 */
														return BFALSE;
													}
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* find-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t
		BgL_idz00_23)
	{
		{	/* Type/tenv.scm 245 */
			{	/* Type/tenv.scm 246 */
				obj_t BgL_typez00_1459;

				BgL_typez00_1459 =
					BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Tenvza2z00zztype_envz00,
					BgL_idz00_23);
				{	/* Type/tenv.scm 247 */
					bool_t BgL_test1896z00_3268;

					{	/* Type/tenv.scm 247 */
						obj_t BgL_classz00_2060;

						BgL_classz00_2060 = BGl_typez00zztype_typez00;
						if (BGL_OBJECTP(BgL_typez00_1459))
							{	/* Type/tenv.scm 247 */
								BgL_objectz00_bglt BgL_arg1807z00_2062;

								BgL_arg1807z00_2062 = (BgL_objectz00_bglt) (BgL_typez00_1459);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Type/tenv.scm 247 */
										long BgL_idxz00_2068;

										BgL_idxz00_2068 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2062);
										BgL_test1896z00_3268 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2068 + 1L)) == BgL_classz00_2060);
									}
								else
									{	/* Type/tenv.scm 247 */
										bool_t BgL_res1749z00_2093;

										{	/* Type/tenv.scm 247 */
											obj_t BgL_oclassz00_2076;

											{	/* Type/tenv.scm 247 */
												obj_t BgL_arg1815z00_2084;
												long BgL_arg1816z00_2085;

												BgL_arg1815z00_2084 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Type/tenv.scm 247 */
													long BgL_arg1817z00_2086;

													BgL_arg1817z00_2086 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2062);
													BgL_arg1816z00_2085 =
														(BgL_arg1817z00_2086 - OBJECT_TYPE);
												}
												BgL_oclassz00_2076 =
													VECTOR_REF(BgL_arg1815z00_2084, BgL_arg1816z00_2085);
											}
											{	/* Type/tenv.scm 247 */
												bool_t BgL__ortest_1115z00_2077;

												BgL__ortest_1115z00_2077 =
													(BgL_classz00_2060 == BgL_oclassz00_2076);
												if (BgL__ortest_1115z00_2077)
													{	/* Type/tenv.scm 247 */
														BgL_res1749z00_2093 = BgL__ortest_1115z00_2077;
													}
												else
													{	/* Type/tenv.scm 247 */
														long BgL_odepthz00_2078;

														{	/* Type/tenv.scm 247 */
															obj_t BgL_arg1804z00_2079;

															BgL_arg1804z00_2079 = (BgL_oclassz00_2076);
															BgL_odepthz00_2078 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2079);
														}
														if ((1L < BgL_odepthz00_2078))
															{	/* Type/tenv.scm 247 */
																obj_t BgL_arg1802z00_2081;

																{	/* Type/tenv.scm 247 */
																	obj_t BgL_arg1803z00_2082;

																	BgL_arg1803z00_2082 = (BgL_oclassz00_2076);
																	BgL_arg1802z00_2081 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2082,
																		1L);
																}
																BgL_res1749z00_2093 =
																	(BgL_arg1802z00_2081 == BgL_classz00_2060);
															}
														else
															{	/* Type/tenv.scm 247 */
																BgL_res1749z00_2093 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1896z00_3268 = BgL_res1749z00_2093;
									}
							}
						else
							{	/* Type/tenv.scm 247 */
								BgL_test1896z00_3268 = ((bool_t) 0);
							}
					}
					if (BgL_test1896z00_3268)
						{	/* Type/tenv.scm 247 */
							return ((BgL_typez00_bglt) BgL_typez00_1459);
						}
					else
						{	/* Type/tenv.scm 247 */
							return
								((BgL_typez00_bglt)
								BGl_errorz00zz__errorz00(CNST_TABLE_REF(3),
									BGl_string1768z00zztype_envz00, BgL_idz00_23));
						}
				}
			}
		}

	}



/* &find-type */
	BgL_typez00_bglt BGl_z62findzd2typezb0zztype_envz00(obj_t BgL_envz00_2551,
		obj_t BgL_idz00_2552)
	{
		{	/* Type/tenv.scm 245 */
			return BGl_findzd2typezd2zztype_envz00(BgL_idz00_2552);
		}

	}



/* find-type/location */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_findzd2typezf2locationz20zztype_envz00(obj_t BgL_idz00_24,
		obj_t BgL_locz00_25)
	{
		{	/* Type/tenv.scm 254 */
			{	/* Type/tenv.scm 255 */
				obj_t BgL_typez00_1461;

				BgL_typez00_1461 =
					BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Tenvza2z00zztype_envz00,
					BgL_idz00_24);
				{	/* Type/tenv.scm 256 */
					bool_t BgL_test1901z00_3297;

					{	/* Type/tenv.scm 256 */
						obj_t BgL_classz00_2094;

						BgL_classz00_2094 = BGl_typez00zztype_typez00;
						if (BGL_OBJECTP(BgL_typez00_1461))
							{	/* Type/tenv.scm 256 */
								BgL_objectz00_bglt BgL_arg1807z00_2096;

								BgL_arg1807z00_2096 = (BgL_objectz00_bglt) (BgL_typez00_1461);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Type/tenv.scm 256 */
										long BgL_idxz00_2102;

										BgL_idxz00_2102 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2096);
										BgL_test1901z00_3297 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2102 + 1L)) == BgL_classz00_2094);
									}
								else
									{	/* Type/tenv.scm 256 */
										bool_t BgL_res1750z00_2127;

										{	/* Type/tenv.scm 256 */
											obj_t BgL_oclassz00_2110;

											{	/* Type/tenv.scm 256 */
												obj_t BgL_arg1815z00_2118;
												long BgL_arg1816z00_2119;

												BgL_arg1815z00_2118 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Type/tenv.scm 256 */
													long BgL_arg1817z00_2120;

													BgL_arg1817z00_2120 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2096);
													BgL_arg1816z00_2119 =
														(BgL_arg1817z00_2120 - OBJECT_TYPE);
												}
												BgL_oclassz00_2110 =
													VECTOR_REF(BgL_arg1815z00_2118, BgL_arg1816z00_2119);
											}
											{	/* Type/tenv.scm 256 */
												bool_t BgL__ortest_1115z00_2111;

												BgL__ortest_1115z00_2111 =
													(BgL_classz00_2094 == BgL_oclassz00_2110);
												if (BgL__ortest_1115z00_2111)
													{	/* Type/tenv.scm 256 */
														BgL_res1750z00_2127 = BgL__ortest_1115z00_2111;
													}
												else
													{	/* Type/tenv.scm 256 */
														long BgL_odepthz00_2112;

														{	/* Type/tenv.scm 256 */
															obj_t BgL_arg1804z00_2113;

															BgL_arg1804z00_2113 = (BgL_oclassz00_2110);
															BgL_odepthz00_2112 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2113);
														}
														if ((1L < BgL_odepthz00_2112))
															{	/* Type/tenv.scm 256 */
																obj_t BgL_arg1802z00_2115;

																{	/* Type/tenv.scm 256 */
																	obj_t BgL_arg1803z00_2116;

																	BgL_arg1803z00_2116 = (BgL_oclassz00_2110);
																	BgL_arg1802z00_2115 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2116,
																		1L);
																}
																BgL_res1750z00_2127 =
																	(BgL_arg1802z00_2115 == BgL_classz00_2094);
															}
														else
															{	/* Type/tenv.scm 256 */
																BgL_res1750z00_2127 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1901z00_3297 = BgL_res1750z00_2127;
									}
							}
						else
							{	/* Type/tenv.scm 256 */
								BgL_test1901z00_3297 = ((bool_t) 0);
							}
					}
					if (BgL_test1901z00_3297)
						{	/* Type/tenv.scm 256 */
							return ((BgL_typez00_bglt) BgL_typez00_1461);
						}
					else
						{	/* Type/tenv.scm 256 */
							return
								((BgL_typez00_bglt)
								BGl_userzd2errorzf2locationz20zztools_errorz00(BgL_locz00_25,
									CNST_TABLE_REF(3), BGl_string1768z00zztype_envz00,
									BgL_idz00_24, BNIL));
						}
				}
			}
		}

	}



/* &find-type/location */
	BgL_typez00_bglt BGl_z62findzd2typezf2locationz42zztype_envz00(obj_t
		BgL_envz00_2553, obj_t BgL_idz00_2554, obj_t BgL_locz00_2555)
	{
		{	/* Type/tenv.scm 254 */
			return
				BGl_findzd2typezf2locationz20zztype_envz00(BgL_idz00_2554,
				BgL_locz00_2555);
		}

	}



/* type-exists? */
	BGL_EXPORTED_DEF bool_t BGl_typezd2existszf3z21zztype_envz00(obj_t
		BgL_idz00_26)
	{
		{	/* Type/tenv.scm 265 */
			{	/* Type/tenv.scm 266 */
				obj_t BgL_typez00_1464;

				BgL_typez00_1464 =
					BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Tenvza2z00zztype_envz00,
					BgL_idz00_26);
				{	/* Type/tenv.scm 267 */
					bool_t BgL_test1906z00_3326;

					{	/* Type/tenv.scm 267 */
						obj_t BgL_classz00_2128;

						BgL_classz00_2128 = BGl_typez00zztype_typez00;
						if (BGL_OBJECTP(BgL_typez00_1464))
							{	/* Type/tenv.scm 267 */
								BgL_objectz00_bglt BgL_arg1807z00_2130;

								BgL_arg1807z00_2130 = (BgL_objectz00_bglt) (BgL_typez00_1464);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Type/tenv.scm 267 */
										long BgL_idxz00_2136;

										BgL_idxz00_2136 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2130);
										BgL_test1906z00_3326 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2136 + 1L)) == BgL_classz00_2128);
									}
								else
									{	/* Type/tenv.scm 267 */
										bool_t BgL_res1751z00_2161;

										{	/* Type/tenv.scm 267 */
											obj_t BgL_oclassz00_2144;

											{	/* Type/tenv.scm 267 */
												obj_t BgL_arg1815z00_2152;
												long BgL_arg1816z00_2153;

												BgL_arg1815z00_2152 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Type/tenv.scm 267 */
													long BgL_arg1817z00_2154;

													BgL_arg1817z00_2154 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2130);
													BgL_arg1816z00_2153 =
														(BgL_arg1817z00_2154 - OBJECT_TYPE);
												}
												BgL_oclassz00_2144 =
													VECTOR_REF(BgL_arg1815z00_2152, BgL_arg1816z00_2153);
											}
											{	/* Type/tenv.scm 267 */
												bool_t BgL__ortest_1115z00_2145;

												BgL__ortest_1115z00_2145 =
													(BgL_classz00_2128 == BgL_oclassz00_2144);
												if (BgL__ortest_1115z00_2145)
													{	/* Type/tenv.scm 267 */
														BgL_res1751z00_2161 = BgL__ortest_1115z00_2145;
													}
												else
													{	/* Type/tenv.scm 267 */
														long BgL_odepthz00_2146;

														{	/* Type/tenv.scm 267 */
															obj_t BgL_arg1804z00_2147;

															BgL_arg1804z00_2147 = (BgL_oclassz00_2144);
															BgL_odepthz00_2146 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2147);
														}
														if ((1L < BgL_odepthz00_2146))
															{	/* Type/tenv.scm 267 */
																obj_t BgL_arg1802z00_2149;

																{	/* Type/tenv.scm 267 */
																	obj_t BgL_arg1803z00_2150;

																	BgL_arg1803z00_2150 = (BgL_oclassz00_2144);
																	BgL_arg1802z00_2149 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2150,
																		1L);
																}
																BgL_res1751z00_2161 =
																	(BgL_arg1802z00_2149 == BgL_classz00_2128);
															}
														else
															{	/* Type/tenv.scm 267 */
																BgL_res1751z00_2161 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1906z00_3326 = BgL_res1751z00_2161;
									}
							}
						else
							{	/* Type/tenv.scm 267 */
								BgL_test1906z00_3326 = ((bool_t) 0);
							}
					}
					if (BgL_test1906z00_3326)
						{	/* Type/tenv.scm 267 */
							return
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_typez00_1464)))->BgL_initzf3zf3);
						}
					else
						{	/* Type/tenv.scm 267 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* &type-exists? */
	obj_t BGl_z62typezd2existszf3z43zztype_envz00(obj_t BgL_envz00_2556,
		obj_t BgL_idz00_2557)
	{
		{	/* Type/tenv.scm 265 */
			return BBOOL(BGl_typezd2existszf3z21zztype_envz00(BgL_idz00_2557));
		}

	}



/* bind-type! */
	BgL_typez00_bglt BGl_bindzd2typez12zc0zztype_envz00(obj_t BgL_idz00_27,
		bool_t BgL_initzf3zf3_28, obj_t BgL_locz00_29)
	{
		{	/* Type/tenv.scm 274 */
			{	/* Type/tenv.scm 275 */
				obj_t BgL_typez00_1466;

				BgL_typez00_1466 =
					BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Tenvza2z00zztype_envz00,
					BgL_idz00_27);
				{	/* Type/tenv.scm 276 */
					bool_t BgL_test1911z00_3354;

					{	/* Type/tenv.scm 276 */
						obj_t BgL_classz00_2163;

						BgL_classz00_2163 = BGl_typez00zztype_typez00;
						if (BGL_OBJECTP(BgL_typez00_1466))
							{	/* Type/tenv.scm 276 */
								BgL_objectz00_bglt BgL_arg1807z00_2165;

								BgL_arg1807z00_2165 = (BgL_objectz00_bglt) (BgL_typez00_1466);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Type/tenv.scm 276 */
										long BgL_idxz00_2171;

										BgL_idxz00_2171 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2165);
										BgL_test1911z00_3354 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2171 + 1L)) == BgL_classz00_2163);
									}
								else
									{	/* Type/tenv.scm 276 */
										bool_t BgL_res1752z00_2196;

										{	/* Type/tenv.scm 276 */
											obj_t BgL_oclassz00_2179;

											{	/* Type/tenv.scm 276 */
												obj_t BgL_arg1815z00_2187;
												long BgL_arg1816z00_2188;

												BgL_arg1815z00_2187 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Type/tenv.scm 276 */
													long BgL_arg1817z00_2189;

													BgL_arg1817z00_2189 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2165);
													BgL_arg1816z00_2188 =
														(BgL_arg1817z00_2189 - OBJECT_TYPE);
												}
												BgL_oclassz00_2179 =
													VECTOR_REF(BgL_arg1815z00_2187, BgL_arg1816z00_2188);
											}
											{	/* Type/tenv.scm 276 */
												bool_t BgL__ortest_1115z00_2180;

												BgL__ortest_1115z00_2180 =
													(BgL_classz00_2163 == BgL_oclassz00_2179);
												if (BgL__ortest_1115z00_2180)
													{	/* Type/tenv.scm 276 */
														BgL_res1752z00_2196 = BgL__ortest_1115z00_2180;
													}
												else
													{	/* Type/tenv.scm 276 */
														long BgL_odepthz00_2181;

														{	/* Type/tenv.scm 276 */
															obj_t BgL_arg1804z00_2182;

															BgL_arg1804z00_2182 = (BgL_oclassz00_2179);
															BgL_odepthz00_2181 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2182);
														}
														if ((1L < BgL_odepthz00_2181))
															{	/* Type/tenv.scm 276 */
																obj_t BgL_arg1802z00_2184;

																{	/* Type/tenv.scm 276 */
																	obj_t BgL_arg1803z00_2185;

																	BgL_arg1803z00_2185 = (BgL_oclassz00_2179);
																	BgL_arg1802z00_2184 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2185,
																		1L);
																}
																BgL_res1752z00_2196 =
																	(BgL_arg1802z00_2184 == BgL_classz00_2163);
															}
														else
															{	/* Type/tenv.scm 276 */
																BgL_res1752z00_2196 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1911z00_3354 = BgL_res1752z00_2196;
									}
							}
						else
							{	/* Type/tenv.scm 276 */
								BgL_test1911z00_3354 = ((bool_t) 0);
							}
					}
					if (BgL_test1911z00_3354)
						{	/* Type/tenv.scm 277 */
							bool_t BgL_test1916z00_3377;

							if (CBOOL
								(BGl_za2allowzd2typezd2redefinitionza2z00zzengine_paramz00))
								{	/* Type/tenv.scm 277 */
									BgL_test1916z00_3377 = ((bool_t) 0);
								}
							else
								{	/* Type/tenv.scm 277 */
									if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
										{	/* Type/tenv.scm 278 */
											BgL_test1916z00_3377 = ((bool_t) 0);
										}
									else
										{	/* Type/tenv.scm 278 */
											BgL_test1916z00_3377 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_typez00_1466)))->
												BgL_initzf3zf3);
										}
								}
							if (BgL_test1916z00_3377)
								{	/* Type/tenv.scm 280 */
									obj_t BgL_arg1408z00_1469;

									BgL_arg1408z00_1469 =
										BGl_shapez00zztools_shapez00(BgL_typez00_1466);
									return
										((BgL_typez00_bglt)
										BGl_userzd2errorzd2zztools_errorz00(CNST_TABLE_REF(4),
											BGl_string1769z00zztype_envz00, BgL_arg1408z00_1469,
											BNIL));
								}
							else
								{	/* Type/tenv.scm 277 */
									{	/* Type/tenv.scm 282 */
										bool_t BgL_test1919z00_3388;

										{	/* Type/tenv.scm 282 */
											bool_t BgL_test1920z00_3389;

											{	/* Type/tenv.scm 282 */
												obj_t BgL_classz00_2198;

												BgL_classz00_2198 = BGl_typez00zztype_typez00;
												if (BGL_OBJECTP(BgL_typez00_1466))
													{	/* Type/tenv.scm 282 */
														BgL_objectz00_bglt BgL_arg1807z00_2200;

														BgL_arg1807z00_2200 =
															(BgL_objectz00_bglt) (BgL_typez00_1466);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Type/tenv.scm 282 */
																long BgL_idxz00_2206;

																BgL_idxz00_2206 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2200);
																BgL_test1920z00_3389 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2206 + 1L)) ==
																	BgL_classz00_2198);
															}
														else
															{	/* Type/tenv.scm 282 */
																bool_t BgL_res1753z00_2231;

																{	/* Type/tenv.scm 282 */
																	obj_t BgL_oclassz00_2214;

																	{	/* Type/tenv.scm 282 */
																		obj_t BgL_arg1815z00_2222;
																		long BgL_arg1816z00_2223;

																		BgL_arg1815z00_2222 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Type/tenv.scm 282 */
																			long BgL_arg1817z00_2224;

																			BgL_arg1817z00_2224 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2200);
																			BgL_arg1816z00_2223 =
																				(BgL_arg1817z00_2224 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2214 =
																			VECTOR_REF(BgL_arg1815z00_2222,
																			BgL_arg1816z00_2223);
																	}
																	{	/* Type/tenv.scm 282 */
																		bool_t BgL__ortest_1115z00_2215;

																		BgL__ortest_1115z00_2215 =
																			(BgL_classz00_2198 == BgL_oclassz00_2214);
																		if (BgL__ortest_1115z00_2215)
																			{	/* Type/tenv.scm 282 */
																				BgL_res1753z00_2231 =
																					BgL__ortest_1115z00_2215;
																			}
																		else
																			{	/* Type/tenv.scm 282 */
																				long BgL_odepthz00_2216;

																				{	/* Type/tenv.scm 282 */
																					obj_t BgL_arg1804z00_2217;

																					BgL_arg1804z00_2217 =
																						(BgL_oclassz00_2214);
																					BgL_odepthz00_2216 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2217);
																				}
																				if ((1L < BgL_odepthz00_2216))
																					{	/* Type/tenv.scm 282 */
																						obj_t BgL_arg1802z00_2219;

																						{	/* Type/tenv.scm 282 */
																							obj_t BgL_arg1803z00_2220;

																							BgL_arg1803z00_2220 =
																								(BgL_oclassz00_2214);
																							BgL_arg1802z00_2219 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2220, 1L);
																						}
																						BgL_res1753z00_2231 =
																							(BgL_arg1802z00_2219 ==
																							BgL_classz00_2198);
																					}
																				else
																					{	/* Type/tenv.scm 282 */
																						BgL_res1753z00_2231 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1920z00_3389 = BgL_res1753z00_2231;
															}
													}
												else
													{	/* Type/tenv.scm 282 */
														BgL_test1920z00_3389 = ((bool_t) 0);
													}
											}
											if (BgL_test1920z00_3389)
												{	/* Type/tenv.scm 282 */
													if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
														{	/* Type/tenv.scm 283 */
															BgL_test1919z00_3388 = ((bool_t) 0);
														}
													else
														{	/* Type/tenv.scm 283 */
															BgL_test1919z00_3388 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt) BgL_typez00_1466)))->
																BgL_initzf3zf3);
														}
												}
											else
												{	/* Type/tenv.scm 282 */
													BgL_test1919z00_3388 = ((bool_t) 0);
												}
										}
										if (BgL_test1919z00_3388)
											{	/* Type/tenv.scm 282 */
												if (CBOOL
													(BGl_za2allowzd2typezd2redefinitionza2z00zzengine_paramz00))
													{	/* Type/tenv.scm 285 */
														BFALSE;
													}
												else
													{	/* Type/tenv.scm 288 */
														obj_t BgL_arg1421z00_1473;

														BgL_arg1421z00_1473 =
															BGl_shapez00zztools_shapez00(BgL_typez00_1466);
														BGl_userzd2warningzd2zztools_errorz00(CNST_TABLE_REF
															(4), BGl_string1769z00zztype_envz00,
															BgL_arg1421z00_1473);
													}
											}
										else
											{	/* Type/tenv.scm 282 */
												BFALSE;
											}
									}
									if (BgL_initzf3zf3_28)
										{	/* Type/tenv.scm 291 */
											((((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt) BgL_typez00_1466)))->
													BgL_initzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
										}
									else
										{	/* Type/tenv.scm 291 */
											BFALSE;
										}
									return ((BgL_typez00_bglt) BgL_typez00_1466);
								}
						}
					else
						{	/* Type/tenv.scm 294 */
							BgL_typez00_bglt BgL_newz00_1475;

							{	/* Type/tenv.scm 294 */
								BgL_typez00_bglt BgL_new1082z00_1476;

								{	/* Type/tenv.scm 294 */
									BgL_typez00_bglt BgL_new1081z00_1477;

									BgL_new1081z00_1477 =
										((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_typez00_bgl))));
									{	/* Type/tenv.scm 294 */
										long BgL_arg1422z00_1478;

										BgL_arg1422z00_1478 =
											BGL_CLASS_NUM(BGl_typez00zztype_typez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1081z00_1477),
											BgL_arg1422z00_1478);
									}
									{	/* Type/tenv.scm 294 */
										BgL_objectz00_bglt BgL_tmpz00_3429;

										BgL_tmpz00_3429 =
											((BgL_objectz00_bglt) BgL_new1081z00_1477);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3429, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1081z00_1477);
									BgL_new1082z00_1476 = BgL_new1081z00_1477;
								}
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_idz00) = ((obj_t) BgL_idz00_27), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_namez00) = ((obj_t) BUNSPEC), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_siza7eza7) = ((obj_t) BUNSPEC), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_classz00) = ((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_coercezd2tozd2) = ((obj_t) BNIL), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_parentsz00) = ((obj_t) BNIL), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_initzf3zf3) = ((bool_t) BgL_initzf3zf3_28), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_magiczf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_nullz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_z42z42) = ((obj_t) BTRUE), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_aliasz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_pointedzd2tozd2byz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_tvectorz00) = ((obj_t) BUNSPEC), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_locationz00) = ((obj_t) BgL_locz00_29), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_importzd2locationzd2) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_typez00_bglt) COBJECT(BgL_new1082z00_1476))->
										BgL_occurrencez00) = ((int) (int) (0L)), BUNSPEC);
								BgL_newz00_1475 = BgL_new1082z00_1476;
							}
							BGl_hashtablezd2putz12zc0zz__hashz00
								(BGl_za2Tenvza2z00zztype_envz00, BgL_idz00_27,
								((obj_t) BgL_newz00_1475));
							return BgL_newz00_1475;
						}
				}
			}
		}

	}



/* use-type! */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t
		BgL_idz00_30, obj_t BgL_locz00_31)
	{
		{	/* Type/tenv.scm 301 */
			{	/* Type/tenv.scm 303 */
				obj_t BgL_typez00_1479;

				BgL_typez00_1479 =
					BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Tenvza2z00zztype_envz00,
					BgL_idz00_30);
				{	/* Type/tenv.scm 305 */
					bool_t BgL_test1928z00_3454;

					{	/* Type/tenv.scm 305 */
						obj_t BgL_classz00_2239;

						BgL_classz00_2239 = BGl_typez00zztype_typez00;
						if (BGL_OBJECTP(BgL_typez00_1479))
							{	/* Type/tenv.scm 305 */
								BgL_objectz00_bglt BgL_arg1807z00_2241;

								BgL_arg1807z00_2241 = (BgL_objectz00_bglt) (BgL_typez00_1479);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Type/tenv.scm 305 */
										long BgL_idxz00_2247;

										BgL_idxz00_2247 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2241);
										BgL_test1928z00_3454 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2247 + 1L)) == BgL_classz00_2239);
									}
								else
									{	/* Type/tenv.scm 305 */
										bool_t BgL_res1754z00_2272;

										{	/* Type/tenv.scm 305 */
											obj_t BgL_oclassz00_2255;

											{	/* Type/tenv.scm 305 */
												obj_t BgL_arg1815z00_2263;
												long BgL_arg1816z00_2264;

												BgL_arg1815z00_2263 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Type/tenv.scm 305 */
													long BgL_arg1817z00_2265;

													BgL_arg1817z00_2265 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2241);
													BgL_arg1816z00_2264 =
														(BgL_arg1817z00_2265 - OBJECT_TYPE);
												}
												BgL_oclassz00_2255 =
													VECTOR_REF(BgL_arg1815z00_2263, BgL_arg1816z00_2264);
											}
											{	/* Type/tenv.scm 305 */
												bool_t BgL__ortest_1115z00_2256;

												BgL__ortest_1115z00_2256 =
													(BgL_classz00_2239 == BgL_oclassz00_2255);
												if (BgL__ortest_1115z00_2256)
													{	/* Type/tenv.scm 305 */
														BgL_res1754z00_2272 = BgL__ortest_1115z00_2256;
													}
												else
													{	/* Type/tenv.scm 305 */
														long BgL_odepthz00_2257;

														{	/* Type/tenv.scm 305 */
															obj_t BgL_arg1804z00_2258;

															BgL_arg1804z00_2258 = (BgL_oclassz00_2255);
															BgL_odepthz00_2257 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2258);
														}
														if ((1L < BgL_odepthz00_2257))
															{	/* Type/tenv.scm 305 */
																obj_t BgL_arg1802z00_2260;

																{	/* Type/tenv.scm 305 */
																	obj_t BgL_arg1803z00_2261;

																	BgL_arg1803z00_2261 = (BgL_oclassz00_2255);
																	BgL_arg1802z00_2260 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2261,
																		1L);
																}
																BgL_res1754z00_2272 =
																	(BgL_arg1802z00_2260 == BgL_classz00_2239);
															}
														else
															{	/* Type/tenv.scm 305 */
																BgL_res1754z00_2272 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1928z00_3454 = BgL_res1754z00_2272;
									}
							}
						else
							{	/* Type/tenv.scm 305 */
								BgL_test1928z00_3454 = ((bool_t) 0);
							}
					}
					if (BgL_test1928z00_3454)
						{	/* Type/tenv.scm 305 */
							BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
								((BgL_typez00_bglt) BgL_typez00_1479));
							return ((BgL_typez00_bglt) BgL_typez00_1479);
						}
					else
						{	/* Type/tenv.scm 305 */
							if (BGl_za2typeszd2alreadyzd2checkedzf3za2zf3zztype_envz00)
								{	/* Type/tenv.scm 308 */
									return
										((BgL_typez00_bglt)
										BGl_userzd2errorzf2locationz20zztools_errorz00
										(BgL_locz00_31, CNST_TABLE_REF(6),
											BGl_string1768z00zztype_envz00, BgL_idz00_30, BNIL));
								}
							else
								{	/* Type/tenv.scm 308 */
									{	/* Type/tenv.scm 312 */
										BgL_typez00_bglt BgL_typez00_1482;

										BgL_typez00_1482 =
											BGl_bindzd2typez12zc0zztype_envz00(BgL_idz00_30,
											((bool_t) 0), BgL_locz00_31);
										BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
											(BgL_typez00_1482);
										return BgL_typez00_1482;
									}
								}
						}
				}
			}
		}

	}



/* &use-type! */
	BgL_typez00_bglt BGl_z62usezd2typez12za2zztype_envz00(obj_t BgL_envz00_2558,
		obj_t BgL_idz00_2559, obj_t BgL_locz00_2560)
	{
		{	/* Type/tenv.scm 301 */
			return BGl_usezd2typez12zc0zztype_envz00(BgL_idz00_2559, BgL_locz00_2560);
		}

	}



/* use-type/import-loc! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_usezd2typezf2importzd2locz12ze0zztype_envz00(obj_t BgL_idz00_32,
		obj_t BgL_locz00_33, obj_t BgL_lociz00_34)
	{
		{	/* Type/tenv.scm 319 */
			{	/* Type/tenv.scm 321 */
				obj_t BgL_typez00_1483;

				BgL_typez00_1483 =
					BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Tenvza2z00zztype_envz00,
					BgL_idz00_32);
				{	/* Type/tenv.scm 323 */
					bool_t BgL_test1934z00_3488;

					{	/* Type/tenv.scm 323 */
						obj_t BgL_classz00_2273;

						BgL_classz00_2273 = BGl_typez00zztype_typez00;
						if (BGL_OBJECTP(BgL_typez00_1483))
							{	/* Type/tenv.scm 323 */
								BgL_objectz00_bglt BgL_arg1807z00_2275;

								BgL_arg1807z00_2275 = (BgL_objectz00_bglt) (BgL_typez00_1483);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Type/tenv.scm 323 */
										long BgL_idxz00_2281;

										BgL_idxz00_2281 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2275);
										BgL_test1934z00_3488 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2281 + 1L)) == BgL_classz00_2273);
									}
								else
									{	/* Type/tenv.scm 323 */
										bool_t BgL_res1755z00_2306;

										{	/* Type/tenv.scm 323 */
											obj_t BgL_oclassz00_2289;

											{	/* Type/tenv.scm 323 */
												obj_t BgL_arg1815z00_2297;
												long BgL_arg1816z00_2298;

												BgL_arg1815z00_2297 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Type/tenv.scm 323 */
													long BgL_arg1817z00_2299;

													BgL_arg1817z00_2299 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2275);
													BgL_arg1816z00_2298 =
														(BgL_arg1817z00_2299 - OBJECT_TYPE);
												}
												BgL_oclassz00_2289 =
													VECTOR_REF(BgL_arg1815z00_2297, BgL_arg1816z00_2298);
											}
											{	/* Type/tenv.scm 323 */
												bool_t BgL__ortest_1115z00_2290;

												BgL__ortest_1115z00_2290 =
													(BgL_classz00_2273 == BgL_oclassz00_2289);
												if (BgL__ortest_1115z00_2290)
													{	/* Type/tenv.scm 323 */
														BgL_res1755z00_2306 = BgL__ortest_1115z00_2290;
													}
												else
													{	/* Type/tenv.scm 323 */
														long BgL_odepthz00_2291;

														{	/* Type/tenv.scm 323 */
															obj_t BgL_arg1804z00_2292;

															BgL_arg1804z00_2292 = (BgL_oclassz00_2289);
															BgL_odepthz00_2291 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2292);
														}
														if ((1L < BgL_odepthz00_2291))
															{	/* Type/tenv.scm 323 */
																obj_t BgL_arg1802z00_2294;

																{	/* Type/tenv.scm 323 */
																	obj_t BgL_arg1803z00_2295;

																	BgL_arg1803z00_2295 = (BgL_oclassz00_2289);
																	BgL_arg1802z00_2294 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2295,
																		1L);
																}
																BgL_res1755z00_2306 =
																	(BgL_arg1802z00_2294 == BgL_classz00_2273);
															}
														else
															{	/* Type/tenv.scm 323 */
																BgL_res1755z00_2306 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1934z00_3488 = BgL_res1755z00_2306;
									}
							}
						else
							{	/* Type/tenv.scm 323 */
								BgL_test1934z00_3488 = ((bool_t) 0);
							}
					}
					if (BgL_test1934z00_3488)
						{	/* Type/tenv.scm 323 */
							return ((BgL_typez00_bglt) BgL_typez00_1483);
						}
					else
						{	/* Type/tenv.scm 323 */
							if (BGl_za2typeszd2alreadyzd2checkedzf3za2zf3zztype_envz00)
								{	/* Type/tenv.scm 325 */
									return
										((BgL_typez00_bglt)
										BGl_userzd2errorzf2locationz20zztools_errorz00
										(BgL_locz00_33, CNST_TABLE_REF(6),
											BGl_string1768z00zztype_envz00, BgL_idz00_32, BNIL));
								}
							else
								{	/* Type/tenv.scm 325 */
									{	/* Type/tenv.scm 329 */
										BgL_typez00_bglt BgL_typez00_1486;

										BgL_typez00_1486 =
											BGl_bindzd2typez12zc0zztype_envz00(BgL_idz00_32,
											((bool_t) 0), BgL_locz00_33);
										((((BgL_typez00_bglt) COBJECT(BgL_typez00_1486))->
												BgL_importzd2locationzd2) =
											((obj_t) BgL_lociz00_34), BUNSPEC);
										return BgL_typez00_1486;
									}
								}
						}
				}
			}
		}

	}



/* &use-type/import-loc! */
	BgL_typez00_bglt BGl_z62usezd2typezf2importzd2locz12z82zztype_envz00(obj_t
		BgL_envz00_2561, obj_t BgL_idz00_2562, obj_t BgL_locz00_2563,
		obj_t BgL_lociz00_2564)
	{
		{	/* Type/tenv.scm 319 */
			return
				BGl_usezd2typezf2importzd2locz12ze0zztype_envz00(BgL_idz00_2562,
				BgL_locz00_2563, BgL_lociz00_2564);
		}

	}



/* use-foreign-type! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_usezd2foreignzd2typez12z12zztype_envz00(obj_t BgL_idz00_35,
		obj_t BgL_locz00_36)
	{
		{	/* Type/tenv.scm 342 */
			{	/* Type/tenv.scm 344 */
				obj_t BgL_tidz00_1487;

				BgL_tidz00_1487 =
					BGl_parsezd2idzd2zzast_identz00(BgL_idz00_35, BgL_locz00_36);
				{	/* Type/tenv.scm 349 */
					bool_t BgL_test1940z00_3520;

					{	/* Type/tenv.scm 349 */
						obj_t BgL_arg1453z00_1492;
						BgL_typez00_bglt BgL_arg1454z00_1493;

						BgL_arg1453z00_1492 = CDR(BgL_tidz00_1487);
						BgL_arg1454z00_1493 = BGl_getzd2defaultzd2typez00zztype_cachez00();
						BgL_test1940z00_3520 =
							(BgL_arg1453z00_1492 == ((obj_t) BgL_arg1454z00_1493));
					}
					if (BgL_test1940z00_3520)
						{	/* Type/tenv.scm 349 */
							BGL_TAIL return
								BGl_usezd2typez12zc0zztype_envz00(CAR(BgL_tidz00_1487),
								BgL_locz00_36);
						}
					else
						{	/* Type/tenv.scm 349 */
							return ((BgL_typez00_bglt) CDR(BgL_tidz00_1487));
						}
				}
			}
		}

	}



/* &use-foreign-type! */
	BgL_typez00_bglt BGl_z62usezd2foreignzd2typez12z70zztype_envz00(obj_t
		BgL_envz00_2565, obj_t BgL_idz00_2566, obj_t BgL_locz00_2567)
	{
		{	/* Type/tenv.scm 342 */
			return
				BGl_usezd2foreignzd2typez12z12zztype_envz00(BgL_idz00_2566,
				BgL_locz00_2567);
		}

	}



/* use-foreign-type/import-loc! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_usezd2foreignzd2typezf2importzd2locz12z32zztype_envz00(obj_t
		BgL_idz00_37, obj_t BgL_locz00_38, obj_t BgL_lociz00_39)
	{
		{	/* Type/tenv.scm 364 */
			{	/* Type/tenv.scm 366 */
				obj_t BgL_tidz00_1494;

				BgL_tidz00_1494 =
					BGl_parsezd2idzd2zzast_identz00(BgL_idz00_37, BgL_locz00_38);
				{	/* Type/tenv.scm 371 */
					bool_t BgL_test1941z00_3531;

					{	/* Type/tenv.scm 371 */
						obj_t BgL_arg1489z00_1499;
						BgL_typez00_bglt BgL_arg1502z00_1500;

						BgL_arg1489z00_1499 = CDR(BgL_tidz00_1494);
						BgL_arg1502z00_1500 = BGl_getzd2defaultzd2typez00zztype_cachez00();
						BgL_test1941z00_3531 =
							(BgL_arg1489z00_1499 == ((obj_t) BgL_arg1502z00_1500));
					}
					if (BgL_test1941z00_3531)
						{	/* Type/tenv.scm 371 */
							BGL_TAIL return
								BGl_usezd2typezf2importzd2locz12ze0zztype_envz00(CAR
								(BgL_tidz00_1494), BgL_locz00_38, BgL_lociz00_39);
						}
					else
						{	/* Type/tenv.scm 371 */
							return ((BgL_typez00_bglt) CDR(BgL_tidz00_1494));
						}
				}
			}
		}

	}



/* &use-foreign-type/import-loc! */
	BgL_typez00_bglt
		BGl_z62usezd2foreignzd2typezf2importzd2locz12z50zztype_envz00(obj_t
		BgL_envz00_2568, obj_t BgL_idz00_2569, obj_t BgL_locz00_2570,
		obj_t BgL_lociz00_2571)
	{
		{	/* Type/tenv.scm 364 */
			return
				BGl_usezd2foreignzd2typezf2importzd2locz12z32zztype_envz00
				(BgL_idz00_2569, BgL_locz00_2570, BgL_lociz00_2571);
		}

	}



/* declare-type! */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_declarezd2typez12zc0zztype_envz00(obj_t
		BgL_idz00_40, obj_t BgL_namez00_41, obj_t BgL_classz00_42)
	{
		{	/* Type/tenv.scm 380 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_classz00_42,
						CNST_TABLE_REF(7))))
				{	/* Type/tenv.scm 386 */
					BgL_typez00_bglt BgL_typez00_1502;

					BgL_typez00_1502 =
						BGl_bindzd2typez12zc0zztype_envz00(BgL_idz00_40, ((bool_t) 1),
						BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_typez00_1502))->BgL_namez00) =
						((obj_t) BgL_namez00_41), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_typez00_1502))->BgL_z42z42) =
						((obj_t) BGl_z42zd2inzd2namezf3zb1zztype_toolsz00(BgL_namez00_41)),
						BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(BgL_typez00_1502))->BgL_classz00) =
						((obj_t) BgL_classz00_42), BUNSPEC);
					return BgL_typez00_1502;
				}
			else
				{	/* Type/tenv.scm 382 */
					return
						((BgL_typez00_bglt)
						BGl_userzd2errorzd2zztools_errorz00(BGl_string1770z00zztype_envz00,
							BGl_string1771z00zztype_envz00, BgL_classz00_42, BNIL));
				}
		}

	}



/* &declare-type! */
	BgL_typez00_bglt BGl_z62declarezd2typez12za2zztype_envz00(obj_t
		BgL_envz00_2572, obj_t BgL_idz00_2573, obj_t BgL_namez00_2574,
		obj_t BgL_classz00_2575)
	{
		{	/* Type/tenv.scm 380 */
			return
				BGl_declarezd2typez12zc0zztype_envz00(BgL_idz00_2573, BgL_namez00_2574,
				BgL_classz00_2575);
		}

	}



/* declare-subtype! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_declarezd2subtypez12zc0zztype_envz00(obj_t BgL_idz00_43,
		obj_t BgL_namez00_44, obj_t BgL_parentsz00_45, obj_t BgL_classz00_46)
	{
		{	/* Type/tenv.scm 397 */
			{	/* Type/tenv.scm 400 */
				BgL_typez00_bglt BgL_typez00_1505;
				obj_t BgL_parentsz00_1506;

				BgL_typez00_1505 =
					BGl_bindzd2typez12zc0zztype_envz00(BgL_idz00_43, ((bool_t) 1),
					BUNSPEC);
				if (NULLP(BgL_parentsz00_45))
					{	/* Type/tenv.scm 401 */
						BgL_parentsz00_1506 = BNIL;
					}
				else
					{	/* Type/tenv.scm 401 */
						obj_t BgL_head1189z00_1510;

						{	/* Type/tenv.scm 401 */
							BgL_typez00_bglt BgL_arg1546z00_1522;

							{	/* Type/tenv.scm 401 */
								obj_t BgL_arg1552z00_1523;

								BgL_arg1552z00_1523 = CAR(((obj_t) BgL_parentsz00_45));
								BgL_arg1546z00_1522 =
									BGl_findzd2typezd2zztype_envz00(BgL_arg1552z00_1523);
							}
							BgL_head1189z00_1510 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg1546z00_1522), BNIL);
						}
						{	/* Type/tenv.scm 401 */
							obj_t BgL_g1192z00_1511;

							BgL_g1192z00_1511 = CDR(((obj_t) BgL_parentsz00_45));
							{
								obj_t BgL_l1187z00_1513;
								obj_t BgL_tail1190z00_1514;

								BgL_l1187z00_1513 = BgL_g1192z00_1511;
								BgL_tail1190z00_1514 = BgL_head1189z00_1510;
							BgL_zc3z04anonymousza31515ze3z87_1515:
								if (NULLP(BgL_l1187z00_1513))
									{	/* Type/tenv.scm 401 */
										BgL_parentsz00_1506 = BgL_head1189z00_1510;
									}
								else
									{	/* Type/tenv.scm 401 */
										obj_t BgL_newtail1191z00_1517;

										{	/* Type/tenv.scm 401 */
											BgL_typez00_bglt BgL_arg1540z00_1519;

											{	/* Type/tenv.scm 401 */
												obj_t BgL_arg1544z00_1520;

												BgL_arg1544z00_1520 = CAR(((obj_t) BgL_l1187z00_1513));
												BgL_arg1540z00_1519 =
													BGl_findzd2typezd2zztype_envz00(BgL_arg1544z00_1520);
											}
											BgL_newtail1191z00_1517 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1540z00_1519), BNIL);
										}
										SET_CDR(BgL_tail1190z00_1514, BgL_newtail1191z00_1517);
										{	/* Type/tenv.scm 401 */
											obj_t BgL_arg1535z00_1518;

											BgL_arg1535z00_1518 = CDR(((obj_t) BgL_l1187z00_1513));
											{
												obj_t BgL_tail1190z00_3574;
												obj_t BgL_l1187z00_3573;

												BgL_l1187z00_3573 = BgL_arg1535z00_1518;
												BgL_tail1190z00_3574 = BgL_newtail1191z00_1517;
												BgL_tail1190z00_1514 = BgL_tail1190z00_3574;
												BgL_l1187z00_1513 = BgL_l1187z00_3573;
												goto BgL_zc3z04anonymousza31515ze3z87_1515;
											}
										}
									}
							}
						}
					}
				((((BgL_typez00_bglt) COBJECT(BgL_typez00_1505))->BgL_namez00) =
					((obj_t) BgL_namez00_44), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_typez00_1505))->BgL_z42z42) =
					((obj_t) BGl_z42zd2inzd2namezf3zb1zztype_toolsz00(BgL_namez00_44)),
					BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_typez00_1505))->BgL_classz00) =
					((obj_t) BgL_classz00_46), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(BgL_typez00_1505))->BgL_parentsz00) =
					((obj_t) BgL_parentsz00_1506), BUNSPEC);
				return BgL_typez00_1505;
			}
		}

	}



/* &declare-subtype! */
	BgL_typez00_bglt BGl_z62declarezd2subtypez12za2zztype_envz00(obj_t
		BgL_envz00_2576, obj_t BgL_idz00_2577, obj_t BgL_namez00_2578,
		obj_t BgL_parentsz00_2579, obj_t BgL_classz00_2580)
	{
		{	/* Type/tenv.scm 397 */
			return
				BGl_declarezd2subtypez12zc0zztype_envz00(BgL_idz00_2577,
				BgL_namez00_2578, BgL_parentsz00_2579, BgL_classz00_2580);
		}

	}



/* declare-aliastype! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_declarezd2aliastypez12zc0zztype_envz00(obj_t BgL_idz00_47,
		obj_t BgL_namez00_48, obj_t BgL_classz00_49,
		BgL_typez00_bglt BgL_aliasz00_50)
	{
		{	/* Type/tenv.scm 411 */
			{	/* Type/tenv.scm 413 */
				BgL_typez00_bglt BgL_typez00_2326;

				BgL_typez00_2326 =
					BGl_declarezd2typez12zc0zztype_envz00(BgL_idz00_47, BgL_namez00_48,
					BgL_classz00_49);
				((((BgL_typez00_bglt) COBJECT(BgL_typez00_2326))->BgL_aliasz00) =
					((obj_t) ((obj_t) BgL_aliasz00_50)), BUNSPEC);
				return BgL_typez00_2326;
			}
		}

	}



/* &declare-aliastype! */
	BgL_typez00_bglt BGl_z62declarezd2aliastypez12za2zztype_envz00(obj_t
		BgL_envz00_2581, obj_t BgL_idz00_2582, obj_t BgL_namez00_2583,
		obj_t BgL_classz00_2584, obj_t BgL_aliasz00_2585)
	{
		{	/* Type/tenv.scm 411 */
			return
				BGl_declarezd2aliastypez12zc0zztype_envz00(BgL_idz00_2582,
				BgL_namez00_2583, BgL_classz00_2584,
				((BgL_typez00_bglt) BgL_aliasz00_2585));
		}

	}



/* for-each-type! */
	BGL_EXPORTED_DEF obj_t BGl_forzd2eachzd2typez12z12zztype_envz00(obj_t
		BgL_procz00_51)
	{
		{	/* Type/tenv.scm 420 */
			{	/* Type/tenv.scm 421 */
				obj_t BgL_zc3z04anonymousza31554ze3z87_2586;

				BgL_zc3z04anonymousza31554ze3z87_2586 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31554ze3ze5zztype_envz00,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31554ze3z87_2586,
					(int) (0L), ((obj_t) BgL_procz00_51));
				return
					BGl_hashtablezd2forzd2eachz00zz__hashz00
					(BGl_za2Tenvza2z00zztype_envz00,
					BgL_zc3z04anonymousza31554ze3z87_2586);
			}
		}

	}



/* &for-each-type! */
	obj_t BGl_z62forzd2eachzd2typez12z70zztype_envz00(obj_t BgL_envz00_2587,
		obj_t BgL_procz00_2588)
	{
		{	/* Type/tenv.scm 420 */
			return BGl_forzd2eachzd2typez12z12zztype_envz00(BgL_procz00_2588);
		}

	}



/* &<@anonymous:1554> */
	obj_t BGl_z62zc3z04anonymousza31554ze3ze5zztype_envz00(obj_t BgL_envz00_2589,
		obj_t BgL_kz00_2591, obj_t BgL_xz00_2592)
	{
		{	/* Type/tenv.scm 421 */
			{	/* Type/tenv.scm 421 */
				obj_t BgL_procz00_2590;

				BgL_procz00_2590 = PROCEDURE_REF(BgL_envz00_2589, (int) (0L));
				return BGL_PROCEDURE_CALL1(BgL_procz00_2590, BgL_xz00_2592);
			}
		}

	}



/* uninitialized-types */
	obj_t BGl_uninitializa7edzd2typesz75zztype_envz00(void)
	{
		{	/* Type/tenv.scm 428 */
			{	/* Type/tenv.scm 429 */
				obj_t BgL_uninitz00_2598;

				BgL_uninitz00_2598 = MAKE_CELL(BNIL);
				{	/* Type/tenv.scm 431 */
					obj_t BgL_zc3z04anonymousza31560ze3z87_2593;

					BgL_zc3z04anonymousza31560ze3z87_2593 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31560ze3ze5zztype_envz00,
						(int) (1L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31560ze3z87_2593,
						(int) (0L), ((obj_t) BgL_uninitz00_2598));
					BGl_forzd2eachzd2typez12z12zztype_envz00
						(BgL_zc3z04anonymousza31560ze3z87_2593);
				}
				return CELL_REF(BgL_uninitz00_2598);
			}
		}

	}



/* &<@anonymous:1560> */
	obj_t BGl_z62zc3z04anonymousza31560ze3ze5zztype_envz00(obj_t BgL_envz00_2594,
		obj_t BgL_tz00_2596)
	{
		{	/* Type/tenv.scm 430 */
			{	/* Type/tenv.scm 431 */
				obj_t BgL_uninitz00_2595;

				BgL_uninitz00_2595 = PROCEDURE_REF(BgL_envz00_2594, (int) (0L));
				if (
					(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_tz00_2596)))->BgL_initzf3zf3))
					{	/* Type/tenv.scm 431 */
						return BFALSE;
					}
				else
					{	/* Type/tenv.scm 432 */
						obj_t BgL_auxz00_2764;

						BgL_auxz00_2764 =
							MAKE_YOUNG_PAIR(BgL_tz00_2596, CELL_REF(BgL_uninitz00_2595));
						return CELL_SET(BgL_uninitz00_2595, BgL_auxz00_2764);
					}
			}
		}

	}



/* check-types */
	BGL_EXPORTED_DEF obj_t BGl_checkzd2typeszd2zztype_envz00(void)
	{
		{	/* Type/tenv.scm 449 */
			{	/* Type/tenv.scm 450 */
				obj_t BgL_utz00_1536;

				BgL_utz00_1536 = BGl_uninitializa7edzd2typesz75zztype_envz00();
				if (PAIRP(BgL_utz00_1536))
					{	/* Type/tenv.scm 451 */
						{	/* Type/tenv.scm 452 */
							obj_t BgL_arg1564z00_1538;

							{	/* Type/tenv.scm 452 */
								obj_t BgL_tmpz00_3616;

								BgL_tmpz00_3616 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg1564z00_1538 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3616);
							}
							bgl_display_char(((unsigned char) 10), BgL_arg1564z00_1538);
						}
						{	/* Type/tenv.scm 453 */
							obj_t BgL_port1193z00_1539;

							{	/* Type/tenv.scm 453 */
								obj_t BgL_tmpz00_3620;

								BgL_tmpz00_3620 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_port1193z00_1539 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3620);
							}
							{	/* Type/tenv.scm 454 */
								long BgL_arg1565z00_1540;

								BgL_arg1565z00_1540 = bgl_list_length(BgL_utz00_1536);
								bgl_display_obj(BINT(BgL_arg1565z00_1540),
									BgL_port1193z00_1539);
							}
							bgl_display_string(BGl_string1772z00zztype_envz00,
								BgL_port1193z00_1539);
							bgl_display_char(((unsigned char) 10), BgL_port1193z00_1539);
						}
						{
							obj_t BgL_utz00_1542;

							BgL_utz00_1542 = BgL_utz00_1536;
						BgL_zc3z04anonymousza31566ze3z87_1543:
							if (NULLP(BgL_utz00_1542))
								{	/* Type/tenv.scm 457 */
									{	/* Type/tenv.scm 458 */
										obj_t BgL_port1194z00_1545;

										{	/* Type/tenv.scm 458 */
											obj_t BgL_tmpz00_3630;

											BgL_tmpz00_3630 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_port1194z00_1545 =
												BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3630);
										}
										bgl_display_string(BGl_string1773z00zztype_envz00,
											BgL_port1194z00_1545);
										bgl_display_char(((unsigned char) 10),
											BgL_port1194z00_1545);
									}
									BGl_compilerzd2exitzd2zzinit_mainz00(BINT(56L));
								}
							else
								{	/* Type/tenv.scm 460 */
									bool_t BgL_test1948z00_3637;

									{	/* Type/tenv.scm 460 */
										obj_t BgL_arg1625z00_1570;

										BgL_arg1625z00_1570 = CAR(((obj_t) BgL_utz00_1542));
										{	/* Type/tenv.scm 460 */
											obj_t BgL_classz00_2340;

											BgL_classz00_2340 = BGl_typez00zztype_typez00;
											if (BGL_OBJECTP(BgL_arg1625z00_1570))
												{	/* Type/tenv.scm 460 */
													BgL_objectz00_bglt BgL_arg1807z00_2342;

													BgL_arg1807z00_2342 =
														(BgL_objectz00_bglt) (BgL_arg1625z00_1570);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Type/tenv.scm 460 */
															long BgL_idxz00_2348;

															BgL_idxz00_2348 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2342);
															BgL_test1948z00_3637 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2348 + 1L)) == BgL_classz00_2340);
														}
													else
														{	/* Type/tenv.scm 460 */
															bool_t BgL_res1756z00_2373;

															{	/* Type/tenv.scm 460 */
																obj_t BgL_oclassz00_2356;

																{	/* Type/tenv.scm 460 */
																	obj_t BgL_arg1815z00_2364;
																	long BgL_arg1816z00_2365;

																	BgL_arg1815z00_2364 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Type/tenv.scm 460 */
																		long BgL_arg1817z00_2366;

																		BgL_arg1817z00_2366 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2342);
																		BgL_arg1816z00_2365 =
																			(BgL_arg1817z00_2366 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2356 =
																		VECTOR_REF(BgL_arg1815z00_2364,
																		BgL_arg1816z00_2365);
																}
																{	/* Type/tenv.scm 460 */
																	bool_t BgL__ortest_1115z00_2357;

																	BgL__ortest_1115z00_2357 =
																		(BgL_classz00_2340 == BgL_oclassz00_2356);
																	if (BgL__ortest_1115z00_2357)
																		{	/* Type/tenv.scm 460 */
																			BgL_res1756z00_2373 =
																				BgL__ortest_1115z00_2357;
																		}
																	else
																		{	/* Type/tenv.scm 460 */
																			long BgL_odepthz00_2358;

																			{	/* Type/tenv.scm 460 */
																				obj_t BgL_arg1804z00_2359;

																				BgL_arg1804z00_2359 =
																					(BgL_oclassz00_2356);
																				BgL_odepthz00_2358 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2359);
																			}
																			if ((1L < BgL_odepthz00_2358))
																				{	/* Type/tenv.scm 460 */
																					obj_t BgL_arg1802z00_2361;

																					{	/* Type/tenv.scm 460 */
																						obj_t BgL_arg1803z00_2362;

																						BgL_arg1803z00_2362 =
																							(BgL_oclassz00_2356);
																						BgL_arg1802z00_2361 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2362, 1L);
																					}
																					BgL_res1756z00_2373 =
																						(BgL_arg1802z00_2361 ==
																						BgL_classz00_2340);
																				}
																			else
																				{	/* Type/tenv.scm 460 */
																					BgL_res1756z00_2373 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1948z00_3637 = BgL_res1756z00_2373;
														}
												}
											else
												{	/* Type/tenv.scm 460 */
													BgL_test1948z00_3637 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test1948z00_3637)
										{	/* Type/tenv.scm 460 */
											{	/* Type/tenv.scm 464 */
												obj_t BgL_zc3z04anonymousza31576ze3z87_2600;

												BgL_zc3z04anonymousza31576ze3z87_2600 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31576ze3ze5zztype_envz00,
													(int) (0L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31576ze3z87_2600,
													(int) (0L), BgL_utz00_1542);
												BGl_withzd2exceptionzd2handlerz00zz__errorz00
													(BGl_errorzd2notifyzd2envz00zz__errorz00,
													BgL_zc3z04anonymousza31576ze3z87_2600);
											}
											{	/* Type/tenv.scm 473 */
												obj_t BgL_arg1611z00_1564;

												BgL_arg1611z00_1564 = CDR(((obj_t) BgL_utz00_1542));
												{
													obj_t BgL_utz00_3670;

													BgL_utz00_3670 = BgL_arg1611z00_1564;
													BgL_utz00_1542 = BgL_utz00_3670;
													goto BgL_zc3z04anonymousza31566ze3z87_1543;
												}
											}
										}
									else
										{	/* Type/tenv.scm 478 */
											obj_t BgL_zc3z04anonymousza31614ze3z87_2601;

											BgL_zc3z04anonymousza31614ze3z87_2601 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31614ze3ze5zztype_envz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31614ze3z87_2601,
												(int) (0L), BgL_utz00_1542);
											BGl_withzd2exceptionzd2handlerz00zz__errorz00
												(BGl_errorzd2notifyzd2envz00zz__errorz00,
												BgL_zc3z04anonymousza31614ze3z87_2601);
					}}}}
				else
					{	/* Type/tenv.scm 451 */
						BFALSE;
					}
				return (BGl_za2typeszd2alreadyzd2checkedzf3za2zf3zztype_envz00 =
					((bool_t) 1), BUNSPEC);
			}
		}

	}



/* &check-types */
	obj_t BGl_z62checkzd2typeszb0zztype_envz00(obj_t BgL_envz00_2602)
	{
		{	/* Type/tenv.scm 449 */
			return BGl_checkzd2typeszd2zztype_envz00();
		}

	}



/* &<@anonymous:1576> */
	obj_t BGl_z62zc3z04anonymousza31576ze3ze5zztype_envz00(obj_t BgL_envz00_2603)
	{
		{	/* Type/tenv.scm 463 */
			{	/* Type/tenv.scm 464 */
				obj_t BgL_utz00_2604;

				BgL_utz00_2604 = PROCEDURE_REF(BgL_envz00_2603, (int) (0L));
				if (CBOOL(
						(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt)
										CAR(
											((obj_t) BgL_utz00_2604)))))->BgL_importzd2locationzd2)))
					{	/* Type/tenv.scm 465 */
						obj_t BgL_arg1589z00_2765;
						obj_t BgL_arg1591z00_2766;

						BgL_arg1589z00_2765 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt)
										CAR(((obj_t) BgL_utz00_2604)))))->BgL_importzd2locationzd2);
						{	/* Type/tenv.scm 468 */
							obj_t BgL_arg1594z00_2767;

							BgL_arg1594z00_2767 = CAR(((obj_t) BgL_utz00_2604));
							BgL_arg1591z00_2766 =
								BGl_shapez00zztools_shapez00(BgL_arg1594z00_2767);
						}
						return
							BGl_userzd2errorzf2locationz20zztools_errorz00
							(BgL_arg1589z00_2765, BGl_za2moduleza2z00zzmodule_modulez00,
							BGl_string1774z00zztype_envz00, BgL_arg1591z00_2766, BNIL);
					}
				else
					{	/* Type/tenv.scm 469 */
						obj_t BgL_arg1595z00_2768;
						obj_t BgL_arg1602z00_2769;

						BgL_arg1595z00_2768 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt)
										CAR(((obj_t) BgL_utz00_2604)))))->BgL_locationz00);
						{	/* Type/tenv.scm 472 */
							obj_t BgL_arg1606z00_2770;

							BgL_arg1606z00_2770 = CAR(((obj_t) BgL_utz00_2604));
							BgL_arg1602z00_2769 =
								BGl_shapez00zztools_shapez00(BgL_arg1606z00_2770);
						}
						return
							BGl_userzd2errorzf2locationz20zztools_errorz00
							(BgL_arg1595z00_2768, BGl_za2moduleza2z00zzmodule_modulez00,
							BGl_string1775z00zztype_envz00, BgL_arg1602z00_2769, BNIL);
					}
			}
		}

	}



/* &<@anonymous:1614> */
	obj_t BGl_z62zc3z04anonymousza31614ze3ze5zztype_envz00(obj_t BgL_envz00_2605)
	{
		{	/* Type/tenv.scm 477 */
			{	/* Type/tenv.scm 478 */
				obj_t BgL_utz00_2606;

				BgL_utz00_2606 = PROCEDURE_REF(BgL_envz00_2605, (int) (0L));
				{	/* Type/tenv.scm 478 */
					obj_t BgL_arg1615z00_2771;

					{	/* Type/tenv.scm 478 */
						obj_t BgL_arg1616z00_2772;

						BgL_arg1616z00_2772 = CAR(((obj_t) BgL_utz00_2606));
						BgL_arg1615z00_2771 =
							BGl_shapez00zztools_shapez00(BgL_arg1616z00_2772);
					}
					return
						BGl_errorz00zz__errorz00(BGl_za2moduleza2z00zzmodule_modulez00,
						BGl_string1776z00zztype_envz00, BgL_arg1615z00_2771);
				}
			}
		}

	}



/* sub-type? */
	BGL_EXPORTED_DEF bool_t BGl_subzd2typezf3z21zztype_envz00(BgL_typez00_bglt
		BgL_minorz00_52, BgL_typez00_bglt BgL_majorz00_53)
	{
		{	/* Type/tenv.scm 486 */
			if ((((obj_t) BgL_minorz00_52) == ((obj_t) BgL_majorz00_53)))
				{	/* Type/tenv.scm 488 */
					return ((bool_t) 1);
				}
			else
				{	/* Type/tenv.scm 488 */
					return
						CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
							((obj_t) BgL_majorz00_53),
							(((BgL_typez00_bglt) COBJECT(BgL_minorz00_52))->BgL_parentsz00)));
				}
		}

	}



/* &sub-type? */
	obj_t BGl_z62subzd2typezf3z43zztype_envz00(obj_t BgL_envz00_2609,
		obj_t BgL_minorz00_2610, obj_t BgL_majorz00_2611)
	{
		{	/* Type/tenv.scm 486 */
			return
				BBOOL(BGl_subzd2typezf3z21zztype_envz00(
					((BgL_typez00_bglt) BgL_minorz00_2610),
					((BgL_typez00_bglt) BgL_majorz00_2611)));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztype_envz00(void)
	{
		{	/* Type/tenv.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztype_envz00(void)
	{
		{	/* Type/tenv.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztype_envz00(void)
	{
		{	/* Type/tenv.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztype_envz00(void)
	{
		{	/* Type/tenv.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzinit_mainz00(288050968L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzmodule_classz00(153808441L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzforeign_ctypez00(396356879L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzobject_predicatez00(458696231L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzobject_creatorz00(508385191L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzforeign_accessz00(471073666L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzmodule_foreignz00(42743976L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzmodule_javaz00(93941037L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			BGl_modulezd2initializa7ationz75zzmodule_typez00(410405073L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
			return
				BGl_modulezd2initializa7ationz75zztvector_accessz00(189470597L,
				BSTRING_TO_STRING(BGl_string1777z00zztype_envz00));
		}

	}

#ifdef __cplusplus
}
#endif
