/*===========================================================================*/
/*   (Type/coercion.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Type/coercion.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TYPE_COERCION_TYPE_DEFINITIONS
#define BGL_TYPE_COERCION_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_TYPE_COERCION_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zztype_coercionz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zztype_coercionz00(void);
	static obj_t BGl_z62addzd2coercionz12za2zztype_coercionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zztype_coercionz00(void);
	static obj_t BGl_objectzd2initzd2zztype_coercionz00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	extern obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zztype_coercionz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztype_coercionz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_addzd2coercionz12zc0zztype_coercionz00(BgL_typez00_bglt,
		BgL_typez00_bglt, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_findzd2coercerzd2zztype_coercionz00(BgL_typez00_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	BGL_EXPORTED_DECL bool_t
		BGl_coercerzd2existszf3z21zztype_coercionz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62coercerzd2existszf3z43zztype_coercionz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zztype_coercionz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztype_coercionz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztype_coercionz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztype_coercionz00(void);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62findzd2coercerzb0zztype_coercionz00(obj_t, obj_t, obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1420z00zztype_coercionz00,
		BgL_bgl_string1420za700za7za7t1435za7, "Type coercion redefinition -- ",
		30);
	      DEFINE_STRING(BGl_string1421z00zztype_coercionz00,
		BgL_bgl_string1421za700za7za7t1436za7, "add-coercion!", 13);
	      DEFINE_STRING(BGl_string1422z00zztype_coercionz00,
		BgL_bgl_string1422za700za7za7t1437za7, "~a -> ~a", 8);
	      DEFINE_STRING(BGl_string1423z00zztype_coercionz00,
		BgL_bgl_string1423za700za7za7t1438za7, "while adding: ~a -> ~a", 22);
	      DEFINE_STRING(BGl_string1424z00zztype_coercionz00,
		BgL_bgl_string1424za700za7za7t1439za7, "Can't find coercion", 19);
	      DEFINE_STRING(BGl_string1425z00zztype_coercionz00,
		BgL_bgl_string1425za700za7za7t1440za7, "type_coercion", 13);
	      DEFINE_STRING(BGl_string1426z00zztype_coercionz00,
		BgL_bgl_string1426za700za7za7t1441za7, "coercer ", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_coercerzd2existszf3zd2envzf3zztype_coercionz00,
		BgL_bgl_za762coercerza7d2exi1442z00,
		BGl_z62coercerzd2existszf3z43zztype_coercionz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2coercionz12zd2envz12zztype_coercionz00,
		BgL_bgl_za762addza7d2coercio1443z00,
		BGl_z62addzd2coercionz12za2zztype_coercionz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2coercerzd2envz00zztype_coercionz00,
		BgL_bgl_za762findza7d2coerce1444z00,
		BGl_z62findzd2coercerzb0zztype_coercionz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zztype_coercionz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long
		BgL_checksumz00_486, char *BgL_fromz00_487)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztype_coercionz00))
				{
					BGl_requirezd2initializa7ationz75zztype_coercionz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztype_coercionz00();
					BGl_libraryzd2moduleszd2initz00zztype_coercionz00();
					BGl_cnstzd2initzd2zztype_coercionz00();
					BGl_importedzd2moduleszd2initz00zztype_coercionz00();
					return BGl_toplevelzd2initzd2zztype_coercionz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztype_coercionz00(void)
	{
		{	/* Type/coercion.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "type_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"type_coercion");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "type_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"type_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"type_coercion");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "type_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"type_coercion");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "type_coercion");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztype_coercionz00(void)
	{
		{	/* Type/coercion.scm 15 */
			{	/* Type/coercion.scm 15 */
				obj_t BgL_cportz00_475;

				{	/* Type/coercion.scm 15 */
					obj_t BgL_stringz00_482;

					BgL_stringz00_482 = BGl_string1426z00zztype_coercionz00;
					{	/* Type/coercion.scm 15 */
						obj_t BgL_startz00_483;

						BgL_startz00_483 = BINT(0L);
						{	/* Type/coercion.scm 15 */
							obj_t BgL_endz00_484;

							BgL_endz00_484 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_482)));
							{	/* Type/coercion.scm 15 */

								BgL_cportz00_475 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_482, BgL_startz00_483, BgL_endz00_484);
				}}}}
				{
					long BgL_iz00_476;

					BgL_iz00_476 = 0L;
				BgL_loopz00_477:
					if ((BgL_iz00_476 == -1L))
						{	/* Type/coercion.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Type/coercion.scm 15 */
							{	/* Type/coercion.scm 15 */
								obj_t BgL_arg1434z00_478;

								{	/* Type/coercion.scm 15 */

									{	/* Type/coercion.scm 15 */
										obj_t BgL_locationz00_480;

										BgL_locationz00_480 = BBOOL(((bool_t) 0));
										{	/* Type/coercion.scm 15 */

											BgL_arg1434z00_478 =
												BGl_readz00zz__readerz00(BgL_cportz00_475,
												BgL_locationz00_480);
										}
									}
								}
								{	/* Type/coercion.scm 15 */
									int BgL_tmpz00_513;

									BgL_tmpz00_513 = (int) (BgL_iz00_476);
									CNST_TABLE_SET(BgL_tmpz00_513, BgL_arg1434z00_478);
							}}
							{	/* Type/coercion.scm 15 */
								int BgL_auxz00_481;

								BgL_auxz00_481 = (int) ((BgL_iz00_476 - 1L));
								{
									long BgL_iz00_518;

									BgL_iz00_518 = (long) (BgL_auxz00_481);
									BgL_iz00_476 = BgL_iz00_518;
									goto BgL_loopz00_477;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztype_coercionz00(void)
	{
		{	/* Type/coercion.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztype_coercionz00(void)
	{
		{	/* Type/coercion.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zztype_coercionz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_90;

				BgL_headz00_90 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_91;
					obj_t BgL_tailz00_92;

					BgL_prevz00_91 = BgL_headz00_90;
					BgL_tailz00_92 = BgL_l1z00_1;
				BgL_loopz00_93:
					if (PAIRP(BgL_tailz00_92))
						{
							obj_t BgL_newzd2prevzd2_95;

							BgL_newzd2prevzd2_95 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_92), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_91, BgL_newzd2prevzd2_95);
							{
								obj_t BgL_tailz00_528;
								obj_t BgL_prevz00_527;

								BgL_prevz00_527 = BgL_newzd2prevzd2_95;
								BgL_tailz00_528 = CDR(BgL_tailz00_92);
								BgL_tailz00_92 = BgL_tailz00_528;
								BgL_prevz00_91 = BgL_prevz00_527;
								goto BgL_loopz00_93;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_90);
				}
			}
		}

	}



/* find-coercer */
	BGL_EXPORTED_DEF obj_t
		BGl_findzd2coercerzd2zztype_coercionz00(BgL_typez00_bglt BgL_fromz00_21,
		BgL_typez00_bglt BgL_toz00_22)
	{
		{	/* Type/coercion.scm 31 */
			{	/* Type/coercion.scm 32 */
				BgL_typez00_bglt BgL_fromz00_111;
				BgL_typez00_bglt BgL_toz00_112;

				BgL_fromz00_111 =
					BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_fromz00_21);
				BgL_toz00_112 = BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_toz00_22);
				{
					obj_t BgL_coercerz00_115;

					BgL_coercerz00_115 =
						(((BgL_typez00_bglt) COBJECT(BgL_fromz00_111))->BgL_coercezd2tozd2);
				BgL_zc3z04anonymousza31077ze3z87_116:
					if (NULLP(BgL_coercerz00_115))
						{	/* Type/coercion.scm 36 */
							return BFALSE;
						}
					else
						{	/* Type/coercion.scm 38 */
							bool_t BgL_test1449z00_535;

							{	/* Type/coercion.scm 38 */
								obj_t BgL_tmpz00_536;

								{	/* Type/coercion.scm 38 */
									obj_t BgL_sz00_417;

									BgL_sz00_417 = CAR(((obj_t) BgL_coercerz00_115));
									BgL_tmpz00_536 = STRUCT_REF(BgL_sz00_417, (int) (1L));
								}
								BgL_test1449z00_535 =
									(BgL_tmpz00_536 == ((obj_t) BgL_toz00_112));
							}
							if (BgL_test1449z00_535)
								{	/* Type/coercion.scm 38 */
									return CAR(((obj_t) BgL_coercerz00_115));
								}
							else
								{
									obj_t BgL_coercerz00_545;

									BgL_coercerz00_545 = CDR(((obj_t) BgL_coercerz00_115));
									BgL_coercerz00_115 = BgL_coercerz00_545;
									goto BgL_zc3z04anonymousza31077ze3z87_116;
								}
						}
				}
			}
		}

	}



/* &find-coercer */
	obj_t BGl_z62findzd2coercerzb0zztype_coercionz00(obj_t BgL_envz00_463,
		obj_t BgL_fromz00_464, obj_t BgL_toz00_465)
	{
		{	/* Type/coercion.scm 31 */
			return
				BGl_findzd2coercerzd2zztype_coercionz00(
				((BgL_typez00_bglt) BgL_fromz00_464),
				((BgL_typez00_bglt) BgL_toz00_465));
		}

	}



/* add-coercion! */
	BGL_EXPORTED_DEF obj_t
		BGl_addzd2coercionz12zc0zztype_coercionz00(BgL_typez00_bglt BgL_fromz00_23,
		BgL_typez00_bglt BgL_toz00_24, obj_t BgL_checkz00_25,
		obj_t BgL_coercez00_26)
	{
		{	/* Type/coercion.scm 77 */
			{	/* Type/coercion.scm 83 */
				BgL_typez00_bglt BgL_fromz00_125;
				BgL_typez00_bglt BgL_toz00_126;

				BgL_fromz00_125 =
					BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_fromz00_23);
				BgL_toz00_126 = BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_toz00_24);
				{	/* Type/coercion.scm 85 */
					bool_t BgL_test1450z00_554;

					{	/* Type/coercion.scm 85 */
						obj_t BgL_arg1199z00_203;

						BgL_arg1199z00_203 =
							BGl_findzd2coercerzd2zztype_coercionz00(BgL_fromz00_125,
							BgL_toz00_126);
						if (STRUCTP(BgL_arg1199z00_203))
							{	/* Type/coercion.scm 85 */
								BgL_test1450z00_554 =
									(STRUCT_KEY(BgL_arg1199z00_203) == CNST_TABLE_REF(0));
							}
						else
							{	/* Type/coercion.scm 85 */
								BgL_test1450z00_554 = ((bool_t) 0);
							}
					}
					if (BgL_test1450z00_554)
						{	/* Type/coercion.scm 85 */
							if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
								{	/* Type/coercion.scm 86 */
									return BFALSE;
								}
							else
								{	/* Type/coercion.scm 89 */
									obj_t BgL_arg1087z00_129;

									{	/* Type/coercion.scm 89 */
										obj_t BgL_arg1097z00_133;

										{	/* Type/coercion.scm 89 */
											obj_t BgL_list1098z00_134;

											{	/* Type/coercion.scm 89 */
												obj_t BgL_arg1102z00_135;

												{	/* Type/coercion.scm 89 */
													obj_t BgL_arg1103z00_136;

													{	/* Type/coercion.scm 89 */
														obj_t BgL_arg1104z00_137;

														BgL_arg1104z00_137 =
															MAKE_YOUNG_PAIR(BgL_coercez00_26, BNIL);
														BgL_arg1103z00_136 =
															MAKE_YOUNG_PAIR(BgL_checkz00_25,
															BgL_arg1104z00_137);
													}
													BgL_arg1102z00_135 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_toz00_126), BgL_arg1103z00_136);
												}
												BgL_list1098z00_134 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_fromz00_125), BgL_arg1102z00_135);
											}
											BgL_arg1097z00_133 = BgL_list1098z00_134;
										}
										BgL_arg1087z00_129 =
											BGl_shapez00zztools_shapez00(BgL_arg1097z00_133);
									}
									{	/* Type/coercion.scm 87 */
										obj_t BgL_list1088z00_130;

										{	/* Type/coercion.scm 87 */
											obj_t BgL_arg1090z00_131;

											{	/* Type/coercion.scm 87 */
												obj_t BgL_arg1092z00_132;

												BgL_arg1092z00_132 =
													MAKE_YOUNG_PAIR(BgL_arg1087z00_129, BNIL);
												BgL_arg1090z00_131 =
													MAKE_YOUNG_PAIR(BGl_string1420z00zztype_coercionz00,
													BgL_arg1092z00_132);
											}
											BgL_list1088z00_130 =
												MAKE_YOUNG_PAIR(BGl_string1421z00zztype_coercionz00,
												BgL_arg1090z00_131);
										}
										return BGl_warningz00zz__errorz00(BgL_list1088z00_130);
									}
								}
						}
					else
						{	/* Type/coercion.scm 85 */
							{	/* Type/coercion.scm 92 */
								obj_t BgL_newz00_138;

								{	/* Type/coercion.scm 92 */
									obj_t BgL_newz00_425;

									BgL_newz00_425 = create_struct(CNST_TABLE_REF(0), (int) (4L));
									{	/* Type/coercion.scm 92 */
										int BgL_tmpz00_577;

										BgL_tmpz00_577 = (int) (3L);
										STRUCT_SET(BgL_newz00_425, BgL_tmpz00_577,
											BgL_coercez00_26);
									}
									{	/* Type/coercion.scm 92 */
										int BgL_tmpz00_580;

										BgL_tmpz00_580 = (int) (2L);
										STRUCT_SET(BgL_newz00_425, BgL_tmpz00_580, BgL_checkz00_25);
									}
									{	/* Type/coercion.scm 92 */
										obj_t BgL_auxz00_585;
										int BgL_tmpz00_583;

										BgL_auxz00_585 = ((obj_t) BgL_toz00_126);
										BgL_tmpz00_583 = (int) (1L);
										STRUCT_SET(BgL_newz00_425, BgL_tmpz00_583, BgL_auxz00_585);
									}
									{	/* Type/coercion.scm 92 */
										obj_t BgL_auxz00_590;
										int BgL_tmpz00_588;

										BgL_auxz00_590 = ((obj_t) BgL_fromz00_125);
										BgL_tmpz00_588 = (int) (0L);
										STRUCT_SET(BgL_newz00_425, BgL_tmpz00_588, BgL_auxz00_590);
									}
									BgL_newz00_138 = BgL_newz00_425;
								}
								{	/* Type/coercion.scm 93 */
									obj_t BgL_arg1114z00_139;

									BgL_arg1114z00_139 =
										MAKE_YOUNG_PAIR(BgL_newz00_138,
										(((BgL_typez00_bglt) COBJECT(BgL_fromz00_125))->
											BgL_coercezd2tozd2));
									((((BgL_typez00_bglt) COBJECT(BgL_fromz00_125))->
											BgL_coercezd2tozd2) =
										((obj_t) BgL_arg1114z00_139), BUNSPEC);
							}}
							{	/* Type/coercion.scm 95 */
								obj_t BgL_g1057z00_141;

								BgL_g1057z00_141 =
									(((BgL_typez00_bglt) COBJECT(BgL_toz00_126))->BgL_parentsz00);
								{
									obj_t BgL_l1055z00_143;

									BgL_l1055z00_143 = BgL_g1057z00_141;
								BgL_zc3z04anonymousza31116ze3z87_144:
									if (PAIRP(BgL_l1055z00_143))
										{	/* Type/coercion.scm 114 */
											{	/* Type/coercion.scm 97 */
												obj_t BgL_parentz00_146;

												BgL_parentz00_146 = CAR(BgL_l1055z00_143);
												{	/* Type/coercion.scm 97 */
													bool_t BgL_test1456z00_600;

													if ((((obj_t) BgL_fromz00_125) == BgL_parentz00_146))
														{	/* Type/coercion.scm 97 */
															BgL_test1456z00_600 = ((bool_t) 0);
														}
													else
														{	/* Type/coercion.scm 97 */
															if (
																(((obj_t) BgL_toz00_126) == BgL_parentz00_146))
																{	/* Type/coercion.scm 98 */
																	BgL_test1456z00_600 = ((bool_t) 0);
																}
															else
																{	/* Type/coercion.scm 99 */
																	bool_t BgL_test1459z00_607;

																	{	/* Type/coercion.scm 99 */
																		obj_t BgL_arg1153z00_169;

																		BgL_arg1153z00_169 =
																			BGl_findzd2coercerzd2zztype_coercionz00
																			(BgL_fromz00_125,
																			((BgL_typez00_bglt) BgL_parentz00_146));
																		if (STRUCTP(BgL_arg1153z00_169))
																			{	/* Type/coercion.scm 99 */
																				BgL_test1459z00_607 =
																					(STRUCT_KEY(BgL_arg1153z00_169) ==
																					CNST_TABLE_REF(0));
																			}
																		else
																			{	/* Type/coercion.scm 99 */
																				BgL_test1459z00_607 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test1459z00_607)
																		{	/* Type/coercion.scm 99 */
																			BgL_test1456z00_600 = ((bool_t) 0);
																		}
																	else
																		{	/* Type/coercion.scm 99 */
																			BgL_test1456z00_600 = ((bool_t) 1);
																		}
																}
														}
													if (BgL_test1456z00_600)
														{	/* Type/coercion.scm 100 */
															obj_t BgL_coercerzd2pzd2_151;

															BgL_coercerzd2pzd2_151 =
																BGl_findzd2coercerzd2zztype_coercionz00
																(BgL_toz00_126,
																((BgL_typez00_bglt) BgL_parentz00_146));
															{	/* Type/coercion.scm 101 */
																bool_t BgL_test1461z00_617;

																if (STRUCTP(BgL_coercerzd2pzd2_151))
																	{	/* Type/coercion.scm 101 */
																		BgL_test1461z00_617 =
																			(STRUCT_KEY(BgL_coercerzd2pzd2_151) ==
																			CNST_TABLE_REF(0));
																	}
																else
																	{	/* Type/coercion.scm 101 */
																		BgL_test1461z00_617 = ((bool_t) 0);
																	}
																if (BgL_test1461z00_617)
																	{	/* Type/coercion.scm 108 */
																		obj_t BgL_checkzd2pzd2_153;
																		obj_t BgL_coercezd2pzd2_154;

																		BgL_checkzd2pzd2_153 =
																			STRUCT_REF(BgL_coercerzd2pzd2_151,
																			(int) (2L));
																		BgL_coercezd2pzd2_154 =
																			STRUCT_REF(BgL_coercerzd2pzd2_151,
																			(int) (3L));
																		{	/* Type/coercion.scm 112 */
																			obj_t BgL_arg1129z00_155;
																			obj_t BgL_arg1131z00_156;

																			BgL_arg1129z00_155 =
																				BGl_appendzd221011zd2zztype_coercionz00
																				(BgL_checkz00_25, BgL_checkzd2pzd2_153);
																			BgL_arg1131z00_156 =
																				BGl_appendzd221011zd2zztype_coercionz00
																				(BgL_coercez00_26,
																				BgL_coercezd2pzd2_154);
																			BGl_addzd2coercionz12zc0zztype_coercionz00
																				(BgL_fromz00_125,
																				((BgL_typez00_bglt) BgL_parentz00_146),
																				BgL_arg1129z00_155, BgL_arg1131z00_156);
																	}}
																else
																	{	/* Type/coercion.scm 104 */
																		obj_t BgL_arg1132z00_157;
																		obj_t BgL_arg1137z00_158;

																		{	/* Type/coercion.scm 104 */
																			obj_t BgL_arg1140z00_160;
																			obj_t BgL_arg1141z00_161;

																			BgL_arg1140z00_160 =
																				BGl_shapez00zztools_shapez00(
																				((obj_t) BgL_toz00_126));
																			BgL_arg1141z00_161 =
																				BGl_shapez00zztools_shapez00
																				(BgL_parentz00_146);
																			{	/* Type/coercion.scm 103 */
																				obj_t BgL_list1142z00_162;

																				{	/* Type/coercion.scm 103 */
																					obj_t BgL_arg1143z00_163;

																					BgL_arg1143z00_163 =
																						MAKE_YOUNG_PAIR(BgL_arg1141z00_161,
																						BNIL);
																					BgL_list1142z00_162 =
																						MAKE_YOUNG_PAIR(BgL_arg1140z00_160,
																						BgL_arg1143z00_163);
																				}
																				BgL_arg1132z00_157 =
																					BGl_formatz00zz__r4_output_6_10_3z00
																					(BGl_string1422z00zztype_coercionz00,
																					BgL_list1142z00_162);
																			}
																		}
																		{	/* Type/coercion.scm 106 */
																			obj_t BgL_arg1145z00_164;
																			obj_t BgL_arg1148z00_165;

																			BgL_arg1145z00_164 =
																				BGl_shapez00zztools_shapez00(
																				((obj_t) BgL_fromz00_125));
																			BgL_arg1148z00_165 =
																				BGl_shapez00zztools_shapez00(
																				((obj_t) BgL_toz00_126));
																			{	/* Type/coercion.scm 105 */
																				obj_t BgL_list1149z00_166;

																				{	/* Type/coercion.scm 105 */
																					obj_t BgL_arg1152z00_167;

																					BgL_arg1152z00_167 =
																						MAKE_YOUNG_PAIR(BgL_arg1148z00_165,
																						BNIL);
																					BgL_list1149z00_166 =
																						MAKE_YOUNG_PAIR(BgL_arg1145z00_164,
																						BgL_arg1152z00_167);
																				}
																				BgL_arg1137z00_158 =
																					BGl_formatz00zz__r4_output_6_10_3z00
																					(BGl_string1423z00zztype_coercionz00,
																					BgL_list1149z00_166);
																			}
																		}
																		BGl_userzd2errorzd2zztools_errorz00
																			(BGl_string1424z00zztype_coercionz00,
																			BgL_arg1132z00_157, BgL_arg1137z00_158,
																			BNIL);
																	}
															}
														}
													else
														{	/* Type/coercion.scm 97 */
															BFALSE;
														}
												}
											}
											{
												obj_t BgL_l1055z00_645;

												BgL_l1055z00_645 = CDR(BgL_l1055z00_143);
												BgL_l1055z00_143 = BgL_l1055z00_645;
												goto BgL_zc3z04anonymousza31116ze3z87_144;
											}
										}
									else
										{	/* Type/coercion.scm 114 */
											((bool_t) 1);
										}
								}
							}
							{	/* Type/coercion.scm 116 */
								obj_t BgL_g1060z00_172;

								BgL_g1060z00_172 =
									(((BgL_typez00_bglt) COBJECT(BgL_fromz00_125))->
									BgL_parentsz00);
								{
									obj_t BgL_l1058z00_174;

									{	/* Type/coercion.scm 134 */
										bool_t BgL_tmpz00_648;

										BgL_l1058z00_174 = BgL_g1060z00_172;
									BgL_zc3z04anonymousza31155ze3z87_175:
										if (PAIRP(BgL_l1058z00_174))
											{	/* Type/coercion.scm 134 */
												{	/* Type/coercion.scm 118 */
													obj_t BgL_parentz00_177;

													BgL_parentz00_177 = CAR(BgL_l1058z00_174);
													{	/* Type/coercion.scm 118 */
														bool_t BgL_test1464z00_652;

														if (
															(((obj_t) BgL_fromz00_125) == BgL_parentz00_177))
															{	/* Type/coercion.scm 118 */
																BgL_test1464z00_652 = ((bool_t) 0);
															}
														else
															{	/* Type/coercion.scm 118 */
																if (
																	(((obj_t) BgL_toz00_126) ==
																		BgL_parentz00_177))
																	{	/* Type/coercion.scm 119 */
																		BgL_test1464z00_652 = ((bool_t) 0);
																	}
																else
																	{	/* Type/coercion.scm 120 */
																		bool_t BgL_test1467z00_659;

																		{	/* Type/coercion.scm 120 */
																			obj_t BgL_arg1197z00_200;

																			BgL_arg1197z00_200 =
																				BGl_findzd2coercerzd2zztype_coercionz00(
																				((BgL_typez00_bglt) BgL_parentz00_177),
																				BgL_toz00_126);
																			if (STRUCTP(BgL_arg1197z00_200))
																				{	/* Type/coercion.scm 120 */
																					BgL_test1467z00_659 =
																						(STRUCT_KEY(BgL_arg1197z00_200) ==
																						CNST_TABLE_REF(0));
																				}
																			else
																				{	/* Type/coercion.scm 120 */
																					BgL_test1467z00_659 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test1467z00_659)
																			{	/* Type/coercion.scm 120 */
																				BgL_test1464z00_652 = ((bool_t) 0);
																			}
																		else
																			{	/* Type/coercion.scm 120 */
																				BgL_test1464z00_652 = ((bool_t) 1);
																			}
																	}
															}
														if (BgL_test1464z00_652)
															{	/* Type/coercion.scm 121 */
																obj_t BgL_coercerzd2pzd2_182;

																BgL_coercerzd2pzd2_182 =
																	BGl_findzd2coercerzd2zztype_coercionz00(
																	((BgL_typez00_bglt) BgL_parentz00_177),
																	BgL_fromz00_125);
																{	/* Type/coercion.scm 122 */
																	bool_t BgL_test1469z00_669;

																	if (STRUCTP(BgL_coercerzd2pzd2_182))
																		{	/* Type/coercion.scm 122 */
																			BgL_test1469z00_669 =
																				(STRUCT_KEY(BgL_coercerzd2pzd2_182) ==
																				CNST_TABLE_REF(0));
																		}
																	else
																		{	/* Type/coercion.scm 122 */
																			BgL_test1469z00_669 = ((bool_t) 0);
																		}
																	if (BgL_test1469z00_669)
																		{	/* Type/coercion.scm 128 */
																			obj_t BgL_checkzd2pzd2_184;
																			obj_t BgL_coercezd2pzd2_185;

																			BgL_checkzd2pzd2_184 =
																				STRUCT_REF(BgL_coercerzd2pzd2_182,
																				(int) (2L));
																			BgL_coercezd2pzd2_185 =
																				STRUCT_REF(BgL_coercerzd2pzd2_182,
																				(int) (3L));
																			{	/* Type/coercion.scm 132 */
																				obj_t BgL_arg1166z00_186;
																				obj_t BgL_arg1171z00_187;

																				BgL_arg1166z00_186 =
																					BGl_appendzd221011zd2zztype_coercionz00
																					(BgL_checkzd2pzd2_184,
																					BgL_checkz00_25);
																				BgL_arg1171z00_187 =
																					BGl_appendzd221011zd2zztype_coercionz00
																					(BgL_coercezd2pzd2_185,
																					BgL_coercez00_26);
																				BGl_addzd2coercionz12zc0zztype_coercionz00
																					(((BgL_typez00_bglt)
																						BgL_parentz00_177), BgL_toz00_126,
																					BgL_arg1166z00_186,
																					BgL_arg1171z00_187);
																		}}
																	else
																		{	/* Type/coercion.scm 125 */
																			obj_t BgL_arg1172z00_188;
																			obj_t BgL_arg1182z00_189;

																			{	/* Type/coercion.scm 125 */
																				obj_t BgL_arg1187z00_191;
																				obj_t BgL_arg1188z00_192;

																				BgL_arg1187z00_191 =
																					BGl_shapez00zztools_shapez00
																					(BgL_parentz00_177);
																				BgL_arg1188z00_192 =
																					BGl_shapez00zztools_shapez00(((obj_t)
																						BgL_fromz00_125));
																				{	/* Type/coercion.scm 124 */
																					obj_t BgL_list1189z00_193;

																					{	/* Type/coercion.scm 124 */
																						obj_t BgL_arg1190z00_194;

																						BgL_arg1190z00_194 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1188z00_192, BNIL);
																						BgL_list1189z00_193 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1187z00_191,
																							BgL_arg1190z00_194);
																					}
																					BgL_arg1172z00_188 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string1422z00zztype_coercionz00,
																						BgL_list1189z00_193);
																				}
																			}
																			{	/* Type/coercion.scm 127 */
																				obj_t BgL_arg1191z00_195;
																				obj_t BgL_arg1193z00_196;

																				BgL_arg1191z00_195 =
																					BGl_shapez00zztools_shapez00(
																					((obj_t) BgL_fromz00_125));
																				BgL_arg1193z00_196 =
																					BGl_shapez00zztools_shapez00(
																					((obj_t) BgL_toz00_126));
																				{	/* Type/coercion.scm 126 */
																					obj_t BgL_list1194z00_197;

																					{	/* Type/coercion.scm 126 */
																						obj_t BgL_arg1196z00_198;

																						BgL_arg1196z00_198 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1193z00_196, BNIL);
																						BgL_list1194z00_197 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1191z00_195,
																							BgL_arg1196z00_198);
																					}
																					BgL_arg1182z00_189 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string1423z00zztype_coercionz00,
																						BgL_list1194z00_197);
																				}
																			}
																			BGl_userzd2errorzd2zztools_errorz00
																				(BGl_string1424z00zztype_coercionz00,
																				BgL_arg1172z00_188, BgL_arg1182z00_189,
																				BNIL);
																		}
																}
															}
														else
															{	/* Type/coercion.scm 118 */
																BFALSE;
															}
													}
												}
												{
													obj_t BgL_l1058z00_697;

													BgL_l1058z00_697 = CDR(BgL_l1058z00_174);
													BgL_l1058z00_174 = BgL_l1058z00_697;
													goto BgL_zc3z04anonymousza31155ze3z87_175;
												}
											}
										else
											{	/* Type/coercion.scm 134 */
												BgL_tmpz00_648 = ((bool_t) 1);
											}
										return BBOOL(BgL_tmpz00_648);
									}
								}
							}
						}
				}
			}
		}

	}



/* &add-coercion! */
	obj_t BGl_z62addzd2coercionz12za2zztype_coercionz00(obj_t BgL_envz00_466,
		obj_t BgL_fromz00_467, obj_t BgL_toz00_468, obj_t BgL_checkz00_469,
		obj_t BgL_coercez00_470)
	{
		{	/* Type/coercion.scm 77 */
			return
				BGl_addzd2coercionz12zc0zztype_coercionz00(
				((BgL_typez00_bglt) BgL_fromz00_467),
				((BgL_typez00_bglt) BgL_toz00_468), BgL_checkz00_469,
				BgL_coercez00_470);
		}

	}



/* coercer-exists? */
	BGL_EXPORTED_DEF bool_t
		BGl_coercerzd2existszf3z21zztype_coercionz00(BgL_typez00_bglt BgL_toz00_29,
		BgL_typez00_bglt BgL_fromz00_30)
	{
		{	/* Type/coercion.scm 169 */
			{	/* Type/coercion.scm 170 */
				BgL_typez00_bglt BgL_toz00_387;
				BgL_typez00_bglt BgL_fromz00_388;

				BgL_toz00_387 = BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_toz00_29);
				BgL_fromz00_388 =
					BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_fromz00_30);
				{	/* Type/coercion.scm 172 */
					bool_t BgL_test1471z00_705;

					if ((((obj_t) BgL_fromz00_388) == ((obj_t) BgL_toz00_387)))
						{	/* Type/coercion.scm 172 */
							BgL_test1471z00_705 = ((bool_t) 1);
						}
					else
						{	/* Type/coercion.scm 172 */
							BgL_test1471z00_705 =
								(((BgL_typez00_bglt) COBJECT(BgL_fromz00_388))->
								BgL_magiczf3zf3);
						}
					if (BgL_test1471z00_705)
						{	/* Type/coercion.scm 172 */
							return ((bool_t) 1);
						}
					else
						{	/* Type/coercion.scm 174 */
							obj_t BgL_arg1408z00_390;

							BgL_arg1408z00_390 =
								BGl_findzd2coercerzd2zztype_coercionz00(BgL_fromz00_388,
								BgL_toz00_387);
							if (STRUCTP(BgL_arg1408z00_390))
								{	/* Type/coercion.scm 174 */
									return (STRUCT_KEY(BgL_arg1408z00_390) == CNST_TABLE_REF(0));
								}
							else
								{	/* Type/coercion.scm 174 */
									return ((bool_t) 0);
								}
						}
				}
			}
		}

	}



/* &coercer-exists? */
	obj_t BGl_z62coercerzd2existszf3z43zztype_coercionz00(obj_t BgL_envz00_471,
		obj_t BgL_toz00_472, obj_t BgL_fromz00_473)
	{
		{	/* Type/coercion.scm 169 */
			return
				BBOOL(BGl_coercerzd2existszf3z21zztype_coercionz00(
					((BgL_typez00_bglt) BgL_toz00_472),
					((BgL_typez00_bglt) BgL_fromz00_473)));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztype_coercionz00(void)
	{
		{	/* Type/coercion.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztype_coercionz00(void)
	{
		{	/* Type/coercion.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztype_coercionz00(void)
	{
		{	/* Type/coercion.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztype_coercionz00(void)
	{
		{	/* Type/coercion.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1425z00zztype_coercionz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1425z00zztype_coercionz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1425z00zztype_coercionz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1425z00zztype_coercionz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1425z00zztype_coercionz00));
		}

	}

#ifdef __cplusplus
}
#endif
