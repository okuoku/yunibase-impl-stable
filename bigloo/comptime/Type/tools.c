/*===========================================================================*/
/*   (Type/tools.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Type/tools.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TYPE_TOOLS_TYPE_DEFINITIONS
#define BGL_TYPE_TOOLS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_TYPE_TOOLS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_makezd2typedzd2declarationz00zztype_toolsz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_replacezd2z42z90zztype_toolsz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztype_toolsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	static obj_t BGl_z62za2zd2namezf3ze1zztype_toolsz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zztype_toolsz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zztype_toolsz00(void);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztype_toolsz00(void);
	static obj_t BGl_z62makezd2typedzd2declarationz62zztype_toolsz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t make_string(long, unsigned char);
	BGL_EXPORTED_DECL obj_t BGl_z42zd2inzd2namezf3zb1zztype_toolsz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62z42zd2inzd2namezf3zd3zztype_toolsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_za2zd2namezf3z83zztype_toolsz00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zztype_toolsz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztype_toolsz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztype_toolsz00(void);
	static obj_t BGl_z62makezd2pointerzd2tozd2namezb0zztype_toolsz00(obj_t,
		obj_t);
	static obj_t BGl_z62stringzd2sanszd2z42z20zztype_toolsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2pointerzd2tozd2namezd2zztype_toolsz00(BgL_typez00_bglt);
	static obj_t BGl_z62replacezd2z42zf2zztype_toolsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezd2namezd2sanszd2z42zf2zztype_toolsz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2pointerzd2tozd2namezd2envz00zztype_toolsz00,
		BgL_bgl_za762makeza7d2pointe1130z00,
		BGl_z62makezd2pointerzd2tozd2namezb0zztype_toolsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42zd2inzd2namezf3zd2envz63zztype_toolsz00,
		BgL_bgl_za762za742za7d2inza7d2na1131z00,
		BGl_z62z42zd2inzd2namezf3zd3zztype_toolsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2namezd2sanszd2z42zd2envz42zztype_toolsz00,
		BgL_bgl_za762typeza7d2nameza7d1132za7,
		BGl_z62typezd2namezd2sanszd2z42zf2zztype_toolsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2sanszd2z42zd2envz90zztype_toolsz00,
		BgL_bgl_za762stringza7d2sans1133z00,
		BGl_z62stringzd2sanszd2z42z20zztype_toolsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_za2zd2namezf3zd2envz51zztype_toolsz00,
		BgL_bgl_za762za7a2za7d2nameza7f31134z00,
		BGl_z62za2zd2namezf3ze1zztype_toolsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2typedzd2declarationzd2envzd2zztype_toolsz00,
		BgL_bgl_za762makeza7d2typedza71135za7,
		BGl_z62makezd2typedzd2declarationz62zztype_toolsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_replacezd2z42zd2envz42zztype_toolsz00,
		BgL_bgl_za762replaceza7d2za7421136za7,
		BGl_z62replacezd2z42zf2zztype_toolsz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1124z00zztype_toolsz00,
		BgL_bgl_string1124za700za7za7t1137za7, " ", 1);
	      DEFINE_STRING(BGl_string1125z00zztype_toolsz00,
		BgL_bgl_string1125za700za7za7t1138za7, "(*)", 3);
	      DEFINE_STRING(BGl_string1126z00zztype_toolsz00,
		BgL_bgl_string1126za700za7za7t1139za7, " *", 2);
	      DEFINE_STRING(BGl_string1127z00zztype_toolsz00,
		BgL_bgl_string1127za700za7za7t1140za7, "make-pointer-to-name", 20);
	      DEFINE_STRING(BGl_string1128z00zztype_toolsz00,
		BgL_bgl_string1128za700za7za7t1141za7, "Unbound foreign type", 20);
	      DEFINE_STRING(BGl_string1129z00zztype_toolsz00,
		BgL_bgl_string1129za700za7za7t1142za7, "type_tools", 10);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztype_toolsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long
		BgL_checksumz00_224, char *BgL_fromz00_225)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztype_toolsz00))
				{
					BGl_requirezd2initializa7ationz75zztype_toolsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztype_toolsz00();
					BGl_libraryzd2moduleszd2initz00zztype_toolsz00();
					BGl_importedzd2moduleszd2initz00zztype_toolsz00();
					return BGl_methodzd2initzd2zztype_toolsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztype_toolsz00(void)
	{
		{	/* Type/tools.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "type_tools");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "type_tools");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"type_tools");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"type_tools");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "type_tools");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztype_toolsz00(void)
	{
		{	/* Type/tools.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* string-sans-$ */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t
		BgL_stringz00_3)
	{
		{	/* Type/tools.scm 33 */
			{	/* Type/tools.scm 34 */
				obj_t BgL_newz00_79;

				BgL_newz00_79 =
					BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_stringz00_3);
				{	/* Type/tools.scm 35 */
					long BgL_g1048z00_80;

					BgL_g1048z00_80 = (STRING_LENGTH(BgL_newz00_79) - 1L);
					{
						long BgL_iz00_82;

						BgL_iz00_82 = BgL_g1048z00_80;
					BgL_zc3z04anonymousza31055ze3z87_83:
						if ((BgL_iz00_82 == -1L))
							{	/* Type/tools.scm 37 */
								return BgL_newz00_79;
							}
						else
							{	/* Type/tools.scm 37 */
								if (
									(STRING_REF(BgL_newz00_79,
											BgL_iz00_82) == ((unsigned char) '$')))
									{	/* Type/tools.scm 39 */
										STRING_SET(BgL_newz00_79, BgL_iz00_82,
											((unsigned char) ' '));
										{
											long BgL_iz00_248;

											BgL_iz00_248 = (BgL_iz00_82 - 1L);
											BgL_iz00_82 = BgL_iz00_248;
											goto BgL_zc3z04anonymousza31055ze3z87_83;
										}
									}
								else
									{
										long BgL_iz00_250;

										BgL_iz00_250 = (BgL_iz00_82 - 1L);
										BgL_iz00_82 = BgL_iz00_250;
										goto BgL_zc3z04anonymousza31055ze3z87_83;
									}
							}
					}
				}
			}
		}

	}



/* &string-sans-$ */
	obj_t BGl_z62stringzd2sanszd2z42z20zztype_toolsz00(obj_t BgL_envz00_208,
		obj_t BgL_stringz00_209)
	{
		{	/* Type/tools.scm 33 */
			return BGl_stringzd2sanszd2z42z42zztype_toolsz00(BgL_stringz00_209);
		}

	}



/* type-name-sans-$ */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00(BgL_typez00_bglt
		BgL_typez00_4)
	{
		{	/* Type/tools.scm 48 */
			{	/* Type/tools.scm 49 */
				obj_t BgL_tnamez00_92;

				BgL_tnamez00_92 =
					(((BgL_typez00_bglt) COBJECT(BgL_typez00_4))->BgL_namez00);
				if (CBOOL((((BgL_typez00_bglt) COBJECT(BgL_typez00_4))->BgL_z42z42)))
					{	/* Type/tools.scm 50 */
						return BGl_stringzd2sanszd2z42z42zztype_toolsz00(BgL_tnamez00_92);
					}
				else
					{	/* Type/tools.scm 50 */
						return BgL_tnamez00_92;
					}
			}
		}

	}



/* &type-name-sans-$ */
	obj_t BGl_z62typezd2namezd2sanszd2z42zf2zztype_toolsz00(obj_t BgL_envz00_210,
		obj_t BgL_typez00_211)
	{
		{	/* Type/tools.scm 48 */
			return
				BGl_typezd2namezd2sanszd2z42z90zztype_toolsz00(
				((BgL_typez00_bglt) BgL_typez00_211));
		}

	}



/* $-in-name? */
	BGL_EXPORTED_DEF obj_t BGl_z42zd2inzd2namezf3zb1zztype_toolsz00(obj_t
		BgL_namez00_5)
	{
		{	/* Type/tools.scm 59 */
			{
				long BgL_iz00_96;

				{	/* Type/tools.scm 60 */
					bool_t BgL_tmpz00_260;

					BgL_iz00_96 = (STRING_LENGTH(BgL_namez00_5) - 1L);
				BgL_zc3z04anonymousza31065ze3z87_97:
					if ((BgL_iz00_96 == -1L))
						{	/* Type/tools.scm 62 */
							BgL_tmpz00_260 = ((bool_t) 0);
						}
					else
						{	/* Type/tools.scm 62 */
							if (
								(STRING_REF(BgL_namez00_5,
										BgL_iz00_96) == ((unsigned char) '$')))
								{	/* Type/tools.scm 64 */
									BgL_tmpz00_260 = ((bool_t) 1);
								}
							else
								{
									long BgL_iz00_266;

									BgL_iz00_266 = (BgL_iz00_96 - 1L);
									BgL_iz00_96 = BgL_iz00_266;
									goto BgL_zc3z04anonymousza31065ze3z87_97;
								}
						}
					return BBOOL(BgL_tmpz00_260);
				}
			}
		}

	}



/* &$-in-name? */
	obj_t BGl_z62z42zd2inzd2namezf3zd3zztype_toolsz00(obj_t BgL_envz00_212,
		obj_t BgL_namez00_213)
	{
		{	/* Type/tools.scm 59 */
			return BGl_z42zd2inzd2namezf3zb1zztype_toolsz00(BgL_namez00_213);
		}

	}



/* *-name? */
	BGL_EXPORTED_DEF obj_t BGl_za2zd2namezf3z83zztype_toolsz00(obj_t
		BgL_namez00_6)
	{
		{	/* Type/tools.scm 74 */
			return
				BBOOL(
				(STRING_REF(BgL_namez00_6,
						(STRING_LENGTH(BgL_namez00_6) - 1L)) == ((unsigned char) '*')));
		}

	}



/* &*-name? */
	obj_t BGl_z62za2zd2namezf3ze1zztype_toolsz00(obj_t BgL_envz00_214,
		obj_t BgL_namez00_215)
	{
		{	/* Type/tools.scm 74 */
			return BGl_za2zd2namezf3z83zztype_toolsz00(BgL_namez00_215);
		}

	}



/* replace-$ */
	BGL_EXPORTED_DEF obj_t BGl_replacezd2z42z90zztype_toolsz00(obj_t
		BgL_stringz00_7, obj_t BgL_rplacz00_8)
	{
		{	/* Type/tools.scm 80 */
			{	/* Type/tools.scm 81 */
				long BgL_lenzd2stringzd2_108;

				BgL_lenzd2stringzd2_108 = STRING_LENGTH(BgL_stringz00_7);
				{	/* Type/tools.scm 81 */
					long BgL_lenzd2rplaczd2_109;

					BgL_lenzd2rplaczd2_109 = STRING_LENGTH(BgL_rplacz00_8);
					{	/* Type/tools.scm 82 */
						long BgL_lenz00_110;

						BgL_lenz00_110 =
							((BgL_lenzd2stringzd2_108 + BgL_lenzd2rplaczd2_109) - 1L);
						{	/* Type/tools.scm 83 */
							obj_t BgL_newz00_111;

							{	/* Type/tools.scm 84 */

								BgL_newz00_111 =
									make_string(BgL_lenz00_110, ((unsigned char) ' '));
							}
							{	/* Type/tools.scm 84 */

								{
									long BgL_rz00_113;
									long BgL_wz00_114;

									BgL_rz00_113 = 0L;
									BgL_wz00_114 = 0L;
								BgL_zc3z04anonymousza31081ze3z87_115:
									if ((BgL_rz00_113 == BgL_lenzd2stringzd2_108))
										{	/* Type/tools.scm 88 */
											return BgL_newz00_111;
										}
									else
										{	/* Type/tools.scm 88 */
											if (
												(STRING_REF(BgL_stringz00_7,
														BgL_rz00_113) == ((unsigned char) '$')))
												{
													long BgL_wz00_120;
													long BgL_rrz00_121;

													BgL_wz00_120 = BgL_wz00_114;
													BgL_rrz00_121 = 0L;
												BgL_zc3z04anonymousza31085ze3z87_122:
													if ((BgL_rrz00_121 == BgL_lenzd2rplaczd2_109))
														{
															long BgL_wz00_292;
															long BgL_rz00_290;

															BgL_rz00_290 = (BgL_rz00_113 + 1L);
															BgL_wz00_292 = BgL_wz00_120;
															BgL_wz00_114 = BgL_wz00_292;
															BgL_rz00_113 = BgL_rz00_290;
															goto BgL_zc3z04anonymousza31081ze3z87_115;
														}
													else
														{	/* Type/tools.scm 94 */
															{	/* Type/tools.scm 97 */
																unsigned char BgL_tmpz00_293;

																BgL_tmpz00_293 =
																	STRING_REF(BgL_rplacz00_8, BgL_rrz00_121);
																STRING_SET(BgL_newz00_111, BgL_wz00_120,
																	BgL_tmpz00_293);
															}
															{
																long BgL_rrz00_298;
																long BgL_wz00_296;

																BgL_wz00_296 = (BgL_wz00_120 + 1L);
																BgL_rrz00_298 = (BgL_rrz00_121 + 1L);
																BgL_rrz00_121 = BgL_rrz00_298;
																BgL_wz00_120 = BgL_wz00_296;
																goto BgL_zc3z04anonymousza31085ze3z87_122;
															}
														}
												}
											else
												{	/* Type/tools.scm 90 */
													{	/* Type/tools.scm 100 */
														unsigned char BgL_tmpz00_300;

														BgL_tmpz00_300 =
															STRING_REF(BgL_stringz00_7, BgL_rz00_113);
														STRING_SET(BgL_newz00_111, BgL_wz00_114,
															BgL_tmpz00_300);
													}
													{
														long BgL_wz00_305;
														long BgL_rz00_303;

														BgL_rz00_303 = (BgL_rz00_113 + 1L);
														BgL_wz00_305 = (BgL_wz00_114 + 1L);
														BgL_wz00_114 = BgL_wz00_305;
														BgL_rz00_113 = BgL_rz00_303;
														goto BgL_zc3z04anonymousza31081ze3z87_115;
													}
												}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &replace-$ */
	obj_t BGl_z62replacezd2z42zf2zztype_toolsz00(obj_t BgL_envz00_216,
		obj_t BgL_stringz00_217, obj_t BgL_rplacz00_218)
	{
		{	/* Type/tools.scm 80 */
			return
				BGl_replacezd2z42z90zztype_toolsz00(BgL_stringz00_217,
				BgL_rplacz00_218);
		}

	}



/* make-typed-declaration */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2typedzd2declarationz00zztype_toolsz00(BgL_typez00_bglt
		BgL_typez00_9, obj_t BgL_idz00_10)
	{
		{	/* Type/tools.scm 106 */
			{	/* Type/tools.scm 107 */
				obj_t BgL_tnamez00_137;

				BgL_tnamez00_137 =
					(((BgL_typez00_bglt) COBJECT(BgL_typez00_9))->BgL_namez00);
				if (CBOOL((((BgL_typez00_bglt) COBJECT(BgL_typez00_9))->BgL_z42z42)))
					{	/* Type/tools.scm 109 */
						return
							BGl_replacezd2z42z90zztype_toolsz00(BgL_tnamez00_137,
							BgL_idz00_10);
					}
				else
					{	/* Type/tools.scm 109 */
						return
							string_append_3(BgL_tnamez00_137,
							BGl_string1124z00zztype_toolsz00, BgL_idz00_10);
					}
			}
		}

	}



/* &make-typed-declaration */
	obj_t BGl_z62makezd2typedzd2declarationz62zztype_toolsz00(obj_t
		BgL_envz00_219, obj_t BgL_typez00_220, obj_t BgL_idz00_221)
	{
		{	/* Type/tools.scm 106 */
			return
				BGl_makezd2typedzd2declarationz00zztype_toolsz00(
				((BgL_typez00_bglt) BgL_typez00_220), BgL_idz00_221);
		}

	}



/* make-pointer-to-name */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2pointerzd2tozd2namezd2zztype_toolsz00(BgL_typez00_bglt
		BgL_typez00_11)
	{
		{	/* Type/tools.scm 117 */
			{	/* Type/tools.scm 118 */
				obj_t BgL_tnamez00_139;

				BgL_tnamez00_139 =
					(((BgL_typez00_bglt) COBJECT(BgL_typez00_11))->BgL_namez00);
				if (STRINGP(BgL_tnamez00_139))
					{	/* Type/tools.scm 119 */
						if (CBOOL(
								(((BgL_typez00_bglt) COBJECT(BgL_typez00_11))->BgL_z42z42)))
							{	/* Type/tools.scm 122 */
								return
									BGl_replacezd2z42z90zztype_toolsz00(BgL_tnamez00_139,
									BGl_string1125z00zztype_toolsz00);
							}
						else
							{	/* Type/tools.scm 122 */
								return
									string_append(BgL_tnamez00_139,
									BGl_string1126z00zztype_toolsz00);
							}
					}
				else
					{	/* Type/tools.scm 119 */
						return
							BGl_errorz00zz__errorz00(BGl_string1127z00zztype_toolsz00,
							BGl_string1128z00zztype_toolsz00,
							(((BgL_typez00_bglt) COBJECT(BgL_typez00_11))->BgL_idz00));
					}
			}
		}

	}



/* &make-pointer-to-name */
	obj_t BGl_z62makezd2pointerzd2tozd2namezb0zztype_toolsz00(obj_t
		BgL_envz00_222, obj_t BgL_typez00_223)
	{
		{	/* Type/tools.scm 117 */
			return
				BGl_makezd2pointerzd2tozd2namezd2zztype_toolsz00(
				((BgL_typez00_bglt) BgL_typez00_223));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztype_toolsz00(void)
	{
		{	/* Type/tools.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztype_toolsz00(void)
	{
		{	/* Type/tools.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztype_toolsz00(void)
	{
		{	/* Type/tools.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztype_toolsz00(void)
	{
		{	/* Type/tools.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1129z00zztype_toolsz00));
		}

	}

#ifdef __cplusplus
}
#endif
