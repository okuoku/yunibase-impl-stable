/*===========================================================================*/
/*   (Type/cache.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Type/cache.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TYPE_CACHE_TYPE_DEFINITIONS
#define BGL_TYPE_CACHE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_TYPE_CACHE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_installzd2typezd2cachez12z12zztype_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2symbolza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2uint32za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2buint32za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2uint16za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2buint16za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2intza2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_za2objectza2z00zztype_cachez00 = BUNSPEC;
	extern bool_t BGl_typezd2existszf3z21zztype_envz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2vectorza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bignumza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bboolza2z00zztype_cachez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DEF obj_t BGl_za2voidza2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zztype_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2outputzd2portza2zd2zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2structza2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zztype_cachez00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	BGL_EXPORTED_DECL obj_t BGl_getzd2objectzd2typez00zztype_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2cellza2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zztype_cachez00(void);
	static obj_t BGl_z62getzd2objectzd2typez62zztype_cachez00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2boolza2z00zztype_cachez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static BgL_typez00_bglt
		BGl_z62getzd2defaultzd2czd2typezb0zztype_cachez00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2foreignza2z00zztype_cachez00 = BUNSPEC;
	extern obj_t BGl_typez00zztype_typez00;
	BGL_EXPORTED_DEF obj_t BGl_za2longza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2magicza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2brealza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2uint64za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2buint64za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2stringza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bstringza2z00zztype_cachez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2exitza2z00zztype_cachez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztype_cachez00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62getzd2bigloozd2typez62zztype_cachez00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62getzd2defaultzd2typez62zztype_cachez00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2realza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2hvectorsza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2defaultzd2typez12z12zztype_cachez00(BgL_typez00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_za2listza2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_z62getzd2classzd2typez62zztype_cachez00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2bint32za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2mutexza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bint16za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2buint8za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2cobjza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2pairza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2_za2z00zztype_cachez00 = BUNSPEC;
	static BgL_typez00_bglt
		BGl_z62getzd2bigloozd2definedzd2typezb0zztype_cachez00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2procedureza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2int32za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2int16za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2uint8za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2objza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bint8za2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2scharza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2belongza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DEF obj_t BGl_za2elongza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_getzd2classzd2typez00zztype_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2ucs2stringza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bllongza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2int8za2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_za2classza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2defaultzd2czd2typezd2zztype_cachez00(void);
	static obj_t BGl_cnstzd2initzd2zztype_cachez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztype_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2bint64za2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zztype_cachez00(void);
	static obj_t BGl_gczd2rootszd2initz00zztype_cachez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2llongza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bnilza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2int64za2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_z62installzd2typezd2cachez12z70zztype_cachez00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2keywordza2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_z62setzd2defaultzd2typez12z70zztype_cachez00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2unspecza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2epairza2z00zztype_cachez00 = BUNSPEC;
	static obj_t BGl_za2defaultzd2typeza2zd2zztype_cachez00 = BUNSPEC;
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2bintza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bcharza2z00zztype_cachez00 = BUNSPEC;
	extern BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t, obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_za2procedurezd2elza2zd2zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2inputzd2portza2zd2zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2charza2z00zztype_cachez00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00(BgL_typez00_bglt);
	static obj_t __cnst[70];


	   
		 
		DEFINE_STRING(BGl_string1092z00zztype_cachez00,
		BgL_bgl_string1092za700za7za7t1098za7, "type_cache", 10);
	      DEFINE_STRING(BGl_string1093z00zztype_cachez00,
		BgL_bgl_string1093za700za7za7t1099za7,
		"_ input-port output-port mutex foreign class object exit unspecified procedure-el procedure struct f64vector f32vector u64vector s64vector u32vector s32vector u16vector s16vector u8vector s8vector vector keyword symbol ucs2string bstring string nil list pair-nil epair pair bchar char uchar bignum real double bbool bool buint64 uint64 bint64 int64 buint32 uint32 bint32 int32 buint16 uint16 bint16 int16 buint8 uint8 bint8 int8 bllong llong belong elong long int bint void magic cell cobj obj no-type-yet ",
		506);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2defaultzd2typez12zd2envzc0zztype_cachez00,
		BgL_bgl_za762setza7d2default1100z00,
		BGl_z62setzd2defaultzd2typez12z70zztype_cachez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2defaultzd2typezd2envzd2zztype_cachez00,
		BgL_bgl_za762getza7d2default1101z00,
		BGl_z62getzd2defaultzd2typez62zztype_cachez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2bigloozd2typezd2envzd2zztype_cachez00,
		BgL_bgl_za762getza7d2biglooza71102za7,
		BGl_z62getzd2bigloozd2typez62zztype_cachez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2bigloozd2definedzd2typezd2envz00zztype_cachez00,
		BgL_bgl_za762getza7d2biglooza71103za7,
		BGl_z62getzd2bigloozd2definedzd2typezb0zztype_cachez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2objectzd2typezd2envzd2zztype_cachez00,
		BgL_bgl_za762getza7d2objectza71104za7,
		BGl_z62getzd2objectzd2typez62zztype_cachez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2defaultzd2czd2typezd2envz00zztype_cachez00,
		BgL_bgl_za762getza7d2default1105z00,
		BGl_z62getzd2defaultzd2czd2typezb0zztype_cachez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2classzd2typezd2envzd2zztype_cachez00,
		BgL_bgl_za762getza7d2classza7d1106za7,
		BGl_z62getzd2classzd2typez62zztype_cachez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_installzd2typezd2cachez12zd2envzc0zztype_cachez00,
		BgL_bgl_za762installza7d2typ1107z00,
		BGl_z62installzd2typezd2cachez12z70zztype_cachez00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2symbolza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2uint32za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2buint32za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2uint16za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2buint16za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2intza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2objectza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2vectorza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2pairzd2nilza2zd2zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bignumza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bboolza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2voidza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2outputzd2portza2zd2zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2structza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2cellza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2boolza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2foreignza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2longza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2magicza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2brealza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2uint64za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2buint64za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2stringza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bstringza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2exitza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2realza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2hvectorsza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2listza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bint32za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2mutexza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bint16za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2buint8za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2cobjza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2pairza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2_za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2procedureza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2int32za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2int16za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2uint8za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2objza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bint8za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2scharza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2belongza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2elongza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2ucs2stringza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bllongza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2int8za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2classza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bint64za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2llongza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bnilza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2int64za2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2keywordza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2unspecza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2epairza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2defaultzd2typeza2zd2zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bintza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2bcharza2z00zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2procedurezd2elza2zd2zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2inputzd2portza2zd2zztype_cachez00));
		     ADD_ROOT((void *) (&BGl_za2charza2z00zztype_cachez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long
		BgL_checksumz00_229, char *BgL_fromz00_230)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztype_cachez00))
				{
					BGl_requirezd2initializa7ationz75zztype_cachez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztype_cachez00();
					BGl_libraryzd2moduleszd2initz00zztype_cachez00();
					BGl_cnstzd2initzd2zztype_cachez00();
					BGl_importedzd2moduleszd2initz00zztype_cachez00();
					return BGl_toplevelzd2initzd2zztype_cachez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztype_cachez00(void)
	{
		{	/* Type/cache.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "type_cache");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "type_cache");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "type_cache");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "type_cache");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"type_cache");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "type_cache");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"type_cache");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"type_cache");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztype_cachez00(void)
	{
		{	/* Type/cache.scm 15 */
			{	/* Type/cache.scm 15 */
				obj_t BgL_cportz00_218;

				{	/* Type/cache.scm 15 */
					obj_t BgL_stringz00_225;

					BgL_stringz00_225 = BGl_string1093z00zztype_cachez00;
					{	/* Type/cache.scm 15 */
						obj_t BgL_startz00_226;

						BgL_startz00_226 = BINT(0L);
						{	/* Type/cache.scm 15 */
							obj_t BgL_endz00_227;

							BgL_endz00_227 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_225)));
							{	/* Type/cache.scm 15 */

								BgL_cportz00_218 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_225, BgL_startz00_226, BgL_endz00_227);
				}}}}
				{
					long BgL_iz00_219;

					BgL_iz00_219 = 69L;
				BgL_loopz00_220:
					if ((BgL_iz00_219 == -1L))
						{	/* Type/cache.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Type/cache.scm 15 */
							{	/* Type/cache.scm 15 */
								obj_t BgL_arg1097z00_221;

								{	/* Type/cache.scm 15 */

									{	/* Type/cache.scm 15 */
										obj_t BgL_locationz00_223;

										BgL_locationz00_223 = BBOOL(((bool_t) 0));
										{	/* Type/cache.scm 15 */

											BgL_arg1097z00_221 =
												BGl_readz00zz__readerz00(BgL_cportz00_218,
												BgL_locationz00_223);
										}
									}
								}
								{	/* Type/cache.scm 15 */
									int BgL_tmpz00_256;

									BgL_tmpz00_256 = (int) (BgL_iz00_219);
									CNST_TABLE_SET(BgL_tmpz00_256, BgL_arg1097z00_221);
							}}
							{	/* Type/cache.scm 15 */
								int BgL_auxz00_224;

								BgL_auxz00_224 = (int) ((BgL_iz00_219 - 1L));
								{
									long BgL_iz00_261;

									BgL_iz00_261 = (long) (BgL_auxz00_224);
									BgL_iz00_219 = BgL_iz00_261;
									goto BgL_loopz00_220;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztype_cachez00(void)
	{
		{	/* Type/cache.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztype_cachez00(void)
	{
		{	/* Type/cache.scm 15 */
			BGl_za2objza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2cobjza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2cellza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2magicza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2voidza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bintza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2intza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2longza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2llongza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bllongza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2int8za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bint8za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2uint8za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2buint8za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2int16za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bint16za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2uint16za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2buint16za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2int32za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bint32za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2uint32za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2buint32za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2int64za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bint64za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2uint64za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2buint64za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2elongza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2belongza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2boolza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bboolza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2realza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2brealza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bignumza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2charza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2scharza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bcharza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2stringza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bstringza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2ucs2stringza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2symbolza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2keywordza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2pairza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2epairza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2pairzd2nilza2zd2zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2listza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2bnilza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2vectorza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2hvectorsza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2structza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2procedureza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2procedurezd2elza2zd2zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2unspecza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2exitza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2objectza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2classza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2foreignza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2mutexza2z00zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2outputzd2portza2zd2zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2inputzd2portza2zd2zztype_cachez00 = CNST_TABLE_REF(0);
			BGl_za2_za2z00zztype_cachez00 = CNST_TABLE_REF(0);
			return (BGl_za2defaultzd2typeza2zd2zztype_cachez00 =
				CNST_TABLE_REF(0), BUNSPEC);
		}

	}



/* install-type-cache! */
	BGL_EXPORTED_DEF obj_t BGl_installzd2typezd2cachez12z12zztype_cachez00(void)
	{
		{	/* Type/cache.scm 88 */
			BGl_za2objza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(1), BFALSE));
			BGl_za2cobjza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(2), BFALSE));
			BGl_za2cellza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(3), BFALSE));
			BGl_za2magicza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(4), BFALSE));
			BGl_za2voidza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(5), BFALSE));
			BGl_za2bintza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(6), BFALSE));
			BGl_za2intza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(7), BFALSE));
			BGl_za2longza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(8), BFALSE));
			BGl_za2elongza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(9), BFALSE));
			BGl_za2belongza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(10), BFALSE));
			BGl_za2llongza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(11), BFALSE));
			BGl_za2bllongza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(12), BFALSE));
			BGl_za2int8za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(13), BFALSE));
			BGl_za2bint8za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(14), BFALSE));
			BGl_za2uint8za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(15), BFALSE));
			BGl_za2buint8za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(16), BFALSE));
			BGl_za2int16za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(17), BFALSE));
			BGl_za2bint16za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(18), BFALSE));
			BGl_za2uint16za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(19), BFALSE));
			BGl_za2buint16za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(20), BFALSE));
			BGl_za2int32za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(21), BFALSE));
			BGl_za2bint32za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(22), BFALSE));
			BGl_za2uint32za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(23), BFALSE));
			BGl_za2buint32za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(24), BFALSE));
			BGl_za2int64za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(25), BFALSE));
			BGl_za2bint64za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(26), BFALSE));
			BGl_za2uint64za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(27), BFALSE));
			BGl_za2buint64za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(28), BFALSE));
			BGl_za2boolza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(29), BFALSE));
			BGl_za2bboolza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(30), BFALSE));
			BGl_za2realza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(31), BFALSE));
			BGl_za2brealza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(32), BFALSE));
			BGl_za2bignumza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(33), BFALSE));
			BGl_za2charza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(34), BFALSE));
			BGl_za2scharza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(35), BFALSE));
			BGl_za2bcharza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(36), BFALSE));
			BGl_za2pairza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(37), BFALSE));
			BGl_za2epairza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(38), BFALSE));
			BGl_za2pairzd2nilza2zd2zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(39), BFALSE));
			BGl_za2listza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(40), BFALSE));
			BGl_za2bnilza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(41), BFALSE));
			BGl_za2stringza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(42), BFALSE));
			BGl_za2bstringza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(43), BFALSE));
			BGl_za2ucs2stringza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(44), BFALSE));
			BGl_za2symbolza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(45), BFALSE));
			BGl_za2keywordza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(46), BFALSE));
			BGl_za2vectorza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(47), BFALSE));
			{	/* Type/cache.scm 137 */
				BgL_typez00_bglt BgL_arg1053z00_73;
				BgL_typez00_bglt BgL_arg1054z00_74;
				BgL_typez00_bglt BgL_arg1055z00_75;
				BgL_typez00_bglt BgL_arg1056z00_76;
				BgL_typez00_bglt BgL_arg1057z00_77;
				BgL_typez00_bglt BgL_arg1058z00_78;
				BgL_typez00_bglt BgL_arg1059z00_79;
				BgL_typez00_bglt BgL_arg1060z00_80;
				BgL_typez00_bglt BgL_arg1062z00_81;
				BgL_typez00_bglt BgL_arg1063z00_82;

				BgL_arg1053z00_73 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(48), BFALSE);
				BgL_arg1054z00_74 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(49), BFALSE);
				BgL_arg1055z00_75 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(50), BFALSE);
				BgL_arg1056z00_76 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(51), BFALSE);
				BgL_arg1057z00_77 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(52), BFALSE);
				BgL_arg1058z00_78 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(53), BFALSE);
				BgL_arg1059z00_79 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(54), BFALSE);
				BgL_arg1060z00_80 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(55), BFALSE);
				BgL_arg1062z00_81 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(56), BFALSE);
				BgL_arg1063z00_82 =
					BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(57), BFALSE);
				{	/* Type/cache.scm 136 */
					obj_t BgL_list1064z00_83;

					{	/* Type/cache.scm 136 */
						obj_t BgL_arg1065z00_84;

						{	/* Type/cache.scm 136 */
							obj_t BgL_arg1066z00_85;

							{	/* Type/cache.scm 136 */
								obj_t BgL_arg1068z00_86;

								{	/* Type/cache.scm 136 */
									obj_t BgL_arg1074z00_87;

									{	/* Type/cache.scm 136 */
										obj_t BgL_arg1075z00_88;

										{	/* Type/cache.scm 136 */
											obj_t BgL_arg1076z00_89;

											{	/* Type/cache.scm 136 */
												obj_t BgL_arg1078z00_90;

												{	/* Type/cache.scm 136 */
													obj_t BgL_arg1079z00_91;

													{	/* Type/cache.scm 136 */
														obj_t BgL_arg1080z00_92;

														BgL_arg1080z00_92 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1063z00_82), BNIL);
														BgL_arg1079z00_91 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1062z00_81), BgL_arg1080z00_92);
													}
													BgL_arg1078z00_90 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg1060z00_80), BgL_arg1079z00_91);
												}
												BgL_arg1076z00_89 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_arg1059z00_79), BgL_arg1078z00_90);
											}
											BgL_arg1075z00_88 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg1058z00_78), BgL_arg1076z00_89);
										}
										BgL_arg1074z00_87 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_arg1057z00_77), BgL_arg1075z00_88);
									}
									BgL_arg1068z00_86 =
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_arg1056z00_76), BgL_arg1074z00_87);
								}
								BgL_arg1066z00_85 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg1055z00_75), BgL_arg1068z00_86);
							}
							BgL_arg1065z00_84 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg1054z00_74), BgL_arg1066z00_85);
						}
						BgL_list1064z00_83 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_arg1053z00_73), BgL_arg1065z00_84);
					}
					BGl_za2hvectorsza2z00zztype_cachez00 = BgL_list1064z00_83;
			}}
			BGl_za2structza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(58), BFALSE));
			BGl_za2procedureza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(59), BFALSE));
			BGl_za2procedurezd2elza2zd2zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(60), BFALSE));
			BGl_za2unspecza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(61), BFALSE));
			BGl_za2exitza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(62), BFALSE));
			if (BGl_typezd2existszf3z21zztype_envz00(CNST_TABLE_REF(63)))
				{	/* Type/cache.scm 152 */
					BGl_za2objectza2z00zztype_cachez00 =
						((obj_t) BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(63)));
				}
			else
				{	/* Type/cache.scm 152 */
					BGl_za2objectza2z00zztype_cachez00 = BFALSE;
				}
			if (BGl_typezd2existszf3z21zztype_envz00(CNST_TABLE_REF(64)))
				{	/* Type/cache.scm 155 */
					BGl_za2classza2z00zztype_cachez00 =
						((obj_t) BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(64)));
				}
			else
				{	/* Type/cache.scm 155 */
					BGl_za2classza2z00zztype_cachez00 = BFALSE;
				}
			BGl_za2foreignza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(65), BFALSE));
			BGl_za2mutexza2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(66), BFALSE));
			BGl_za2outputzd2portza2zd2zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(67), BFALSE));
			BGl_za2inputzd2portza2zd2zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(68), BFALSE));
			BGl_za2_za2z00zztype_cachez00 =
				((obj_t) BGl_usezd2typez12zc0zztype_envz00(CNST_TABLE_REF(69), BFALSE));
			return (BGl_za2defaultzd2typeza2zd2zztype_cachez00 =
				BGl_za2_za2z00zztype_cachez00, BUNSPEC);
		}

	}



/* &install-type-cache! */
	obj_t BGl_z62installzd2typezd2cachez12z70zztype_cachez00(obj_t BgL_envz00_207)
	{
		{	/* Type/cache.scm 88 */
			return BGl_installzd2typezd2cachez12z12zztype_cachez00();
		}

	}



/* get-default-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2defaultzd2typez00zztype_cachez00(void)
	{
		{	/* Type/cache.scm 237 */
			return ((BgL_typez00_bglt) BGl_za2defaultzd2typeza2zd2zztype_cachez00);
		}

	}



/* &get-default-type */
	BgL_typez00_bglt BGl_z62getzd2defaultzd2typez62zztype_cachez00(obj_t
		BgL_envz00_208)
	{
		{	/* Type/cache.scm 237 */
			return BGl_getzd2defaultzd2typez00zztype_cachez00();
		}

	}



/* set-default-type! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2defaultzd2typez12z12zztype_cachez00(BgL_typez00_bglt
		BgL_typez00_3)
	{
		{	/* Type/cache.scm 243 */
			return (BGl_za2defaultzd2typeza2zd2zztype_cachez00 =
				((obj_t) BgL_typez00_3), BUNSPEC);
		}

	}



/* &set-default-type! */
	obj_t BGl_z62setzd2defaultzd2typez12z70zztype_cachez00(obj_t BgL_envz00_209,
		obj_t BgL_typez00_210)
	{
		{	/* Type/cache.scm 243 */
			return
				BGl_setzd2defaultzd2typez12z12zztype_cachez00(
				((BgL_typez00_bglt) BgL_typez00_210));
		}

	}



/* get-object-type */
	BGL_EXPORTED_DEF obj_t BGl_getzd2objectzd2typez00zztype_cachez00(void)
	{
		{	/* Type/cache.scm 249 */
			{	/* Type/cache.scm 251 */
				bool_t BgL_test1112z00_554;

				{	/* Type/cache.scm 251 */
					obj_t BgL_objz00_103;

					BgL_objz00_103 = BGl_za2objectza2z00zztype_cachez00;
					{	/* Type/cache.scm 251 */
						obj_t BgL_classz00_104;

						BgL_classz00_104 = BGl_typez00zztype_typez00;
						if (BGL_OBJECTP(BgL_objz00_103))
							{	/* Type/cache.scm 251 */
								BgL_objectz00_bglt BgL_arg1807z00_106;

								BgL_arg1807z00_106 = (BgL_objectz00_bglt) (BgL_objz00_103);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Type/cache.scm 251 */
										long BgL_idxz00_112;

										BgL_idxz00_112 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_106);
										BgL_test1112z00_554 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_112 + 1L)) == BgL_classz00_104);
									}
								else
									{	/* Type/cache.scm 251 */
										bool_t BgL_res1089z00_137;

										{	/* Type/cache.scm 251 */
											obj_t BgL_oclassz00_120;

											{	/* Type/cache.scm 251 */
												obj_t BgL_arg1815z00_128;
												long BgL_arg1816z00_129;

												BgL_arg1815z00_128 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Type/cache.scm 251 */
													long BgL_arg1817z00_130;

													BgL_arg1817z00_130 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_106);
													BgL_arg1816z00_129 =
														(BgL_arg1817z00_130 - OBJECT_TYPE);
												}
												BgL_oclassz00_120 =
													VECTOR_REF(BgL_arg1815z00_128, BgL_arg1816z00_129);
											}
											{	/* Type/cache.scm 251 */
												bool_t BgL__ortest_1115z00_121;

												BgL__ortest_1115z00_121 =
													(BgL_classz00_104 == BgL_oclassz00_120);
												if (BgL__ortest_1115z00_121)
													{	/* Type/cache.scm 251 */
														BgL_res1089z00_137 = BgL__ortest_1115z00_121;
													}
												else
													{	/* Type/cache.scm 251 */
														long BgL_odepthz00_122;

														{	/* Type/cache.scm 251 */
															obj_t BgL_arg1804z00_123;

															BgL_arg1804z00_123 = (BgL_oclassz00_120);
															BgL_odepthz00_122 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_123);
														}
														if ((1L < BgL_odepthz00_122))
															{	/* Type/cache.scm 251 */
																obj_t BgL_arg1802z00_125;

																{	/* Type/cache.scm 251 */
																	obj_t BgL_arg1803z00_126;

																	BgL_arg1803z00_126 = (BgL_oclassz00_120);
																	BgL_arg1802z00_125 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_126,
																		1L);
																}
																BgL_res1089z00_137 =
																	(BgL_arg1802z00_125 == BgL_classz00_104);
															}
														else
															{	/* Type/cache.scm 251 */
																BgL_res1089z00_137 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1112z00_554 = BgL_res1089z00_137;
									}
							}
						else
							{	/* Type/cache.scm 251 */
								BgL_test1112z00_554 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test1112z00_554)
					{	/* Type/cache.scm 251 */
						return BGl_za2objectza2z00zztype_cachez00;
					}
				else
					{	/* Type/cache.scm 251 */
						BGl_za2objectza2z00zztype_cachez00 =
							((obj_t) BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(63)));
						return BGl_za2objectza2z00zztype_cachez00;
					}
			}
		}

	}



/* &get-object-type */
	obj_t BGl_z62getzd2objectzd2typez62zztype_cachez00(obj_t BgL_envz00_211)
	{
		{	/* Type/cache.scm 249 */
			return BGl_getzd2objectzd2typez00zztype_cachez00();
		}

	}



/* get-class-type */
	BGL_EXPORTED_DEF obj_t BGl_getzd2classzd2typez00zztype_cachez00(void)
	{
		{	/* Type/cache.scm 260 */
			{	/* Type/cache.scm 262 */
				bool_t BgL_test1117z00_581;

				{	/* Type/cache.scm 262 */
					obj_t BgL_objz00_138;

					BgL_objz00_138 = BGl_za2classza2z00zztype_cachez00;
					{	/* Type/cache.scm 262 */
						obj_t BgL_classz00_139;

						BgL_classz00_139 = BGl_typez00zztype_typez00;
						if (BGL_OBJECTP(BgL_objz00_138))
							{	/* Type/cache.scm 262 */
								BgL_objectz00_bglt BgL_arg1807z00_141;

								BgL_arg1807z00_141 = (BgL_objectz00_bglt) (BgL_objz00_138);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Type/cache.scm 262 */
										long BgL_idxz00_147;

										BgL_idxz00_147 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_141);
										BgL_test1117z00_581 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_147 + 1L)) == BgL_classz00_139);
									}
								else
									{	/* Type/cache.scm 262 */
										bool_t BgL_res1090z00_172;

										{	/* Type/cache.scm 262 */
											obj_t BgL_oclassz00_155;

											{	/* Type/cache.scm 262 */
												obj_t BgL_arg1815z00_163;
												long BgL_arg1816z00_164;

												BgL_arg1815z00_163 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Type/cache.scm 262 */
													long BgL_arg1817z00_165;

													BgL_arg1817z00_165 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_141);
													BgL_arg1816z00_164 =
														(BgL_arg1817z00_165 - OBJECT_TYPE);
												}
												BgL_oclassz00_155 =
													VECTOR_REF(BgL_arg1815z00_163, BgL_arg1816z00_164);
											}
											{	/* Type/cache.scm 262 */
												bool_t BgL__ortest_1115z00_156;

												BgL__ortest_1115z00_156 =
													(BgL_classz00_139 == BgL_oclassz00_155);
												if (BgL__ortest_1115z00_156)
													{	/* Type/cache.scm 262 */
														BgL_res1090z00_172 = BgL__ortest_1115z00_156;
													}
												else
													{	/* Type/cache.scm 262 */
														long BgL_odepthz00_157;

														{	/* Type/cache.scm 262 */
															obj_t BgL_arg1804z00_158;

															BgL_arg1804z00_158 = (BgL_oclassz00_155);
															BgL_odepthz00_157 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_158);
														}
														if ((1L < BgL_odepthz00_157))
															{	/* Type/cache.scm 262 */
																obj_t BgL_arg1802z00_160;

																{	/* Type/cache.scm 262 */
																	obj_t BgL_arg1803z00_161;

																	BgL_arg1803z00_161 = (BgL_oclassz00_155);
																	BgL_arg1802z00_160 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_161,
																		1L);
																}
																BgL_res1090z00_172 =
																	(BgL_arg1802z00_160 == BgL_classz00_139);
															}
														else
															{	/* Type/cache.scm 262 */
																BgL_res1090z00_172 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1117z00_581 = BgL_res1090z00_172;
									}
							}
						else
							{	/* Type/cache.scm 262 */
								BgL_test1117z00_581 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test1117z00_581)
					{	/* Type/cache.scm 262 */
						return BGl_za2classza2z00zztype_cachez00;
					}
				else
					{	/* Type/cache.scm 262 */
						BGl_za2classza2z00zztype_cachez00 =
							((obj_t) BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(64)));
						return BGl_za2classza2z00zztype_cachez00;
					}
			}
		}

	}



/* &get-class-type */
	obj_t BGl_z62getzd2classzd2typez62zztype_cachez00(obj_t BgL_envz00_212)
	{
		{	/* Type/cache.scm 260 */
			return BGl_getzd2classzd2typez00zztype_cachez00();
		}

	}



/* get-default-c-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2defaultzd2czd2typezd2zztype_cachez00(void)
	{
		{	/* Type/cache.scm 273 */
			return ((BgL_typez00_bglt) BGl_za2intza2z00zztype_cachez00);
		}

	}



/* &get-default-c-type */
	BgL_typez00_bglt BGl_z62getzd2defaultzd2czd2typezb0zztype_cachez00(obj_t
		BgL_envz00_213)
	{
		{	/* Type/cache.scm 273 */
			return BGl_getzd2defaultzd2czd2typezd2zztype_cachez00();
		}

	}



/* get-bigloo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt BgL_typez00_4)
	{
		{	/* Type/cache.scm 281 */
			if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_4))
				{	/* Type/cache.scm 283 */
					return BgL_typez00_4;
				}
			else
				{	/* Type/cache.scm 284 */
					bool_t BgL_test1123z00_612;

					if ((((obj_t) BgL_typez00_4) == BGl_za2intza2z00zztype_cachez00))
						{	/* Type/cache.scm 284 */
							BgL_test1123z00_612 = ((bool_t) 1);
						}
					else
						{	/* Type/cache.scm 284 */
							BgL_test1123z00_612 =
								(((obj_t) BgL_typez00_4) == BGl_za2longza2z00zztype_cachez00);
						}
					if (BgL_test1123z00_612)
						{	/* Type/cache.scm 284 */
							return ((BgL_typez00_bglt) BGl_za2bintza2z00zztype_cachez00);
						}
					else
						{	/* Type/cache.scm 284 */
							if (
								(((obj_t) BgL_typez00_4) == BGl_za2elongza2z00zztype_cachez00))
								{	/* Type/cache.scm 285 */
									return
										((BgL_typez00_bglt) BGl_za2belongza2z00zztype_cachez00);
								}
							else
								{	/* Type/cache.scm 285 */
									if (
										(((obj_t) BgL_typez00_4) ==
											BGl_za2llongza2z00zztype_cachez00))
										{	/* Type/cache.scm 286 */
											return
												((BgL_typez00_bglt) BGl_za2bllongza2z00zztype_cachez00);
										}
									else
										{	/* Type/cache.scm 286 */
											if (
												(((obj_t) BgL_typez00_4) ==
													BGl_za2int8za2z00zztype_cachez00))
												{	/* Type/cache.scm 287 */
													return
														((BgL_typez00_bglt)
														BGl_za2bint8za2z00zztype_cachez00);
												}
											else
												{	/* Type/cache.scm 287 */
													if (
														(((obj_t) BgL_typez00_4) ==
															BGl_za2uint8za2z00zztype_cachez00))
														{	/* Type/cache.scm 288 */
															return
																((BgL_typez00_bglt)
																BGl_za2buint8za2z00zztype_cachez00);
														}
													else
														{	/* Type/cache.scm 288 */
															if (
																(((obj_t) BgL_typez00_4) ==
																	BGl_za2int16za2z00zztype_cachez00))
																{	/* Type/cache.scm 289 */
																	return
																		((BgL_typez00_bglt)
																		BGl_za2bint16za2z00zztype_cachez00);
																}
															else
																{	/* Type/cache.scm 289 */
																	if (
																		(((obj_t) BgL_typez00_4) ==
																			BGl_za2uint16za2z00zztype_cachez00))
																		{	/* Type/cache.scm 290 */
																			return
																				((BgL_typez00_bglt)
																				BGl_za2buint16za2z00zztype_cachez00);
																		}
																	else
																		{	/* Type/cache.scm 290 */
																			if (
																				(((obj_t) BgL_typez00_4) ==
																					BGl_za2int32za2z00zztype_cachez00))
																				{	/* Type/cache.scm 291 */
																					return
																						((BgL_typez00_bglt)
																						BGl_za2bint32za2z00zztype_cachez00);
																				}
																			else
																				{	/* Type/cache.scm 291 */
																					if (
																						(((obj_t) BgL_typez00_4) ==
																							BGl_za2uint32za2z00zztype_cachez00))
																						{	/* Type/cache.scm 292 */
																							return
																								((BgL_typez00_bglt)
																								BGl_za2buint32za2z00zztype_cachez00);
																						}
																					else
																						{	/* Type/cache.scm 292 */
																							if (
																								(((obj_t) BgL_typez00_4) ==
																									BGl_za2int64za2z00zztype_cachez00))
																								{	/* Type/cache.scm 293 */
																									return
																										((BgL_typez00_bglt)
																										BGl_za2bint64za2z00zztype_cachez00);
																								}
																							else
																								{	/* Type/cache.scm 293 */
																									if (
																										(((obj_t) BgL_typez00_4) ==
																											BGl_za2uint64za2z00zztype_cachez00))
																										{	/* Type/cache.scm 294 */
																											return
																												((BgL_typez00_bglt)
																												BGl_za2buint64za2z00zztype_cachez00);
																										}
																									else
																										{	/* Type/cache.scm 294 */
																											if (
																												(((obj_t) BgL_typez00_4)
																													==
																													BGl_za2boolza2z00zztype_cachez00))
																												{	/* Type/cache.scm 295 */
																													return
																														((BgL_typez00_bglt)
																														BGl_za2bboolza2z00zztype_cachez00);
																												}
																											else
																												{	/* Type/cache.scm 295 */
																													if (
																														(((obj_t)
																																BgL_typez00_4)
																															==
																															BGl_za2realza2z00zztype_cachez00))
																														{	/* Type/cache.scm 296 */
																															return
																																(
																																(BgL_typez00_bglt)
																																BGl_za2brealza2z00zztype_cachez00);
																														}
																													else
																														{	/* Type/cache.scm 296 */
																															if (
																																(((obj_t)
																																		BgL_typez00_4)
																																	==
																																	BGl_za2charza2z00zztype_cachez00))
																																{	/* Type/cache.scm 297 */
																																	return
																																		(
																																		(BgL_typez00_bglt)
																																		BGl_za2bcharza2z00zztype_cachez00);
																																}
																															else
																																{	/* Type/cache.scm 297 */
																																	if (
																																		(((obj_t)
																																				BgL_typez00_4)
																																			==
																																			BGl_za2stringza2z00zztype_cachez00))
																																		{	/* Type/cache.scm 298 */
																																			return
																																				(
																																				(BgL_typez00_bglt)
																																				BGl_za2bstringza2z00zztype_cachez00);
																																		}
																																	else
																																		{	/* Type/cache.scm 298 */
																																			return
																																				(
																																				(BgL_typez00_bglt)
																																				BGl_za2objza2z00zztype_cachez00);
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &get-bigloo-type */
	BgL_typez00_bglt BGl_z62getzd2bigloozd2typez62zztype_cachez00(obj_t
		BgL_envz00_214, obj_t BgL_typez00_215)
	{
		{	/* Type/cache.scm 281 */
			return
				BGl_getzd2bigloozd2typez00zztype_cachez00(
				((BgL_typez00_bglt) BgL_typez00_215));
		}

	}



/* get-bigloo-defined-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00(BgL_typez00_bglt
		BgL_tz00_5)
	{
		{	/* Type/cache.scm 304 */
			if ((((obj_t) BgL_tz00_5) == BGl_za2_za2z00zztype_cachez00))
				{	/* Type/cache.scm 305 */
					return ((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
				}
			else
				{	/* Type/cache.scm 305 */
					BGL_TAIL return BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_tz00_5);
				}
		}

	}



/* &get-bigloo-defined-type */
	BgL_typez00_bglt BGl_z62getzd2bigloozd2definedzd2typezb0zztype_cachez00(obj_t
		BgL_envz00_216, obj_t BgL_tz00_217)
	{
		{	/* Type/cache.scm 304 */
			return
				BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00(
				((BgL_typez00_bglt) BgL_tz00_217));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztype_cachez00(void)
	{
		{	/* Type/cache.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztype_cachez00(void)
	{
		{	/* Type/cache.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztype_cachez00(void)
	{
		{	/* Type/cache.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztype_cachez00(void)
	{
		{	/* Type/cache.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1092z00zztype_cachez00));
			return
				BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1092z00zztype_cachez00));
		}

	}

#ifdef __cplusplus
}
#endif
