/*===========================================================================*/
/*   (Engine/param.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Engine/param.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_ENGINE_PARAM_TYPE_DEFINITIONS
#define BGL_ENGINE_PARAM_TYPE_DEFINITIONS
#endif													// BGL_ENGINE_PARAM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DEF obj_t BGl_za2heapzd2jvmzd2nameza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2jvmzd2inliningza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2additionalzd2tracesza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2synczd2profilingza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2cfazd2pairzd2quotezd2maxzd2lengthza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2libzd2srczd2dirza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2cfazd2pairzf3za2zf3zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2unrollzd2loopzf3za2zf3zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2unsafezd2arityza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2javaza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2heapzd2dumpzd2namesza2z00zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_za2oldzd2loadzd2pathza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2bigloozd2classpathza2z00zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2destza2z00zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31201ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31031ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2helloza2z00zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31104ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2integratezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2profzd2tablezd2nameza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2specializa7ezd2flonumzf3za2z54zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2cczd2styleza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2autozd2linkzd2mainza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2accesszd2shapezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2compilerzd2stackzd2debugzf3za2zf3zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2emailza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2stripza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2sawzd2registerzd2allocationzd2functionsza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2builtinzd2allocatorsza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2sawzd2spillza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2cczd2optionsza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2czd2filesza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2doublezd2ldzd2libszf3za2zf3zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31130ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2warningzd2overridenzd2variablesza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2targetzd2languageza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ldzd2relativeza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2stackablezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2czd2splitzd2stringza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2taggedzd2fxopzf3za2zf3zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2gczd2customzf3za2z21zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2czd2userzd2footza2z00zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31050ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2passza2z00zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzengine_paramz00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2tmpza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2csharpzd2suffixza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2tmpzd2destza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2isazf3za2z21zzengine_paramz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_evalz00zz__evalz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2additionalzd2bigloozd2za7ipsza2za7zzengine_paramz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_getenvz00zz__osz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31124ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2uncellzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2restzd2argsza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2cfazd2fixnumzd2arithmeticzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2typezd2shapezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2cfazf3za2z21zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2jvmzd2peepholeza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2maxzd2czd2foreignzd2arityza2zd2zzengine_paramz00
		= BUNSPEC;
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2ldzd2optionsza2zd2zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zzengine_paramz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62addzd2doczd2variablez12z70zzengine_paramz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2returnzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31060ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2locationzd2shapezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31052ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2jarpathza2zd2zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zzengine_paramz00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2shellza2zd2zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31150ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31142ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31126ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31029ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_za2ldzd2libraryzd2dirza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2jvmzd2branchza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2keyzd2shapezf3za2z21zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2sawza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2autozd2modeza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2bigloozd2librarieszd2czd2setupza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2cfazd2flonumzd2arithmeticzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31313ze3ze5zzengine_paramz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2czd2debugzd2lineszd2infoza2zd2zzengine_paramz00
		= BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31054ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31216ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2debugzd2moduleza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2initzd2modeza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_evalzd2initzd2zzengine_paramz00(void);
	static obj_t BGl_z62zc3z04anonymousza31039ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2startupzd2fileza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2userzd2inliningzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2cmdzd2nameza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2warningzd2overridenzd2slotsza2z00zzengine_paramz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2warningzd2typesza2zd2zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31153ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31056ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31048ze3ze5zzengine_paramz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2jvmzd2foreignzd2classzd2idza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2modulezd2shapezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2Ozd2macrozf3za2zf3zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_appendzd221011zd2zzengine_paramz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2modulezd2checksumzd2objectzf3za2zf3zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zzengine_paramz00(void);
	static obj_t BGl_z62zc3z04anonymousza31081ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31227ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ccza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2variableszd2usagez00zzengine_paramz00(bool_t);
	BGL_EXPORTED_DEF obj_t BGl_za2shellza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2jvmzd2constructorzd2inliningza2zd2zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31066ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31058ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2classpathza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2errorzd2localiza7ationza2z75zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2pathzd2separatorza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2cczd2ozd2optionza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2inliningzd2kfactorza2zd2zzengine_paramz00 =
		BUNSPEC;
	extern obj_t BGl_bigloozd2datezd2zztools_datez00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00
		= BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31091ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31172ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31083ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31075ze3ze5zzengine_paramz00(obj_t);
	static obj_t
		BGl_z62zc3z04za2prezd2processorza2za31229ze3z37zzengine_paramz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2urlza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2heapzd2libraryza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2cinitzd2moduleza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2licensingzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_definezd2primopzd2refz12z12zz__evenvz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2compilerzd2sharingzd2debugzf3za2zf3zzengine_paramz00 = BUNSPEC;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2inliningzf3za2zf3zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31165ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2ldzd2debugzd2optionza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2gczd2libza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2heapzd2wasmzd2nameza2z00zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31190ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31085ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31077ze3ze5zzengine_paramz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31158ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2ldzd2optimzd2flagsza2z00zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_za2paramzd2updatersza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2alloczd2shapezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2staticzd2bigloozf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2cflagsza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2tracezd2levelza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2unsafezd2structza2zd2zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31183ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2ldzd2postzd2optionsza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2cflagszd2optimza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ldzd2styleza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2versionza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2sawzd2bbvzf3za2z21zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2accesszd2filezd2defaultza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2mainclassza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2verboseza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2sawzd2bbvzd2functionsza2z00zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31192ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2unsafezd2rangeza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2catchza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2includezd2multipleza2zd2zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31079ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2initflowzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2czd2suffixza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_IMPORT bool_t bigloo_string_lt(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2multizd2threadedzd2gczf3za2zf3zzengine_paramz00
		= BUNSPEC;
	BGL_IMPORT obj_t BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2jvmzd2fasteqza2z00zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31088ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2evalzd2optionsza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2sawzd2registerzd2reallocationzf3za2zf3zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_configurez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_datez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long,
		char *);
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2nameza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2heapzd2nameza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2profilezd2libraryza2zd2zzengine_paramz00 =
		BUNSPEC;
	static obj_t
		BGl_z62zc3z04za2inliningzd2kfactorza21202ze3z94zzengine_paramz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2dlopenzd2initzd2gcza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2tracezd2nameza2zd2zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31194ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2mcozd2suffixza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2debugza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2profilezd2modeza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00
		= BUNSPEC;
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2sawzd2registerzd2allocationzf3za2zf3zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2tracezd2writezd2lengthza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2argsza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2withzd2filesza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2warningzd2defaultzd2slotzd2valueza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2dataflowzd2forzd2errorszf3za2z21zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2userzd2heapzd2siza7eza2za7zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2cflagszd2profza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2inliningzd2reducezd2kfactorza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2jarzf3za2z21zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2synczd2failsafezf3za2zf3zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2allowzd2typezd2redefinitionza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2libza2zd2zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31188ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2unsafezd2libraryza2zd2zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzengine_paramz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzengine_paramz00(void);
	static obj_t
		BGl_z62reinitializa7ezd2bigloozd2variablesz12zd7zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2userzd2libza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2unsafezd2heapza2zd2zzengine_paramz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04za2inliningzd2reducezd2k1203ze3ze4zzengine_paramz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2prezd2processorza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2loopzd2inliningzf3za2zf3zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2variableszd2usagez62zzengine_paramz00(obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzengine_paramz00(void);
	BGL_EXPORTED_DEF obj_t
		BGl_za2gczd2forcezd2registerzd2rootszf3za2z21zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zzengine_paramz00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2objzd2suffixza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2sawzd2nozd2registerzd2allocationzd2functionsza2z00zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31197ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2extendzd2entryza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2dataflowzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2srczd2suffixza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2cczd2moveza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ozd2filesza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bmemzd2profilingza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2cflagszd2rpathza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2authorza2zd2zzengine_paramz00 =
		BUNSPEC;
	static obj_t BGl_z62addzd2updaterz12za2zzengine_paramz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2namezd2shapezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2abortzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2typenamezd2shapezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2indentza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2arithmeticzd2newzd2overflowza2z00zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_za2optimzd2jvmza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2directoryza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2interpreterza2z00zzengine_paramz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2loadzd2pathza2zd2zz__evalz00;
	BGL_EXPORTED_DEF obj_t BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_definezd2primopz12zc0zz__evenvz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31199ze3ze5zzengine_paramz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2bigloozd2dateza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2dataflowzd2typeszf3za2zf3zzengine_paramz00 = BUNSPEC;
	static obj_t BGl_z62appendzd221011zb0zzengine_paramz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2earlyzd2withzd2modulesza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2globalzd2tailzd2callzf3za2zf3zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2cfazd2applyzd2trackingzf3za2z21zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2unsafezd2evalza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2userzd2shapezf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2compilerzd2typezd2debugzf3za2zf3zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2qualifiedzd2typezd2filezd2defaultza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2arithmeticzd2overflowza2zd2zzengine_paramz00 =
		BUNSPEC;
	extern obj_t
		BGl_buildzd2pathzd2fromzd2shellzd2variablez00zztools_miscz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2callzf2cczf3za2z01zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2qualifiedzd2typezd2fileza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ldzd2ozd2optionza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2czd2debugzd2optionza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2sawzd2registerzd2allocationzd2onexpressionzf3za2z21zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2sawzd2registerzd2allocationzd2maxzd2siza7eza2za7zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2stdcza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2jvmzd2foreignzd2classzd2nameza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_reinitializa7ezd2bigloozd2variablesz12zb5zzengine_paramz00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2optionsza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2atomzd2inliningzf3za2zf3zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2additionalzd2heapzd2nameza2z00zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2defaultzd2libzd2dirza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2readerza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2symbolzd2caseza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2czd2tailzd2callza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2jvmzd2envza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2dlopenzd2initza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2czd2userzd2headerza2z00zzengine_paramz00 =
		BUNSPEC;
	extern obj_t BGl_stringzd2splitzd2charz00zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2mcozd2includezd2pathza2z00zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2accesszd2filesza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2optimzd2cfazd2forcezd2loosezd2localzd2functionzf3za2z21zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2czd2debugza2zd2zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2includezd2foreignza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2garbagezd2collectorza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2purifyza2z00zzengine_paramz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2localzd2exitzf3za2z21zzengine_paramz00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2unsafezd2versionza2zd2zzengine_paramz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2optimzd2reducezd2betazf3za2zf3zzengine_paramz00
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2warningzd2typezd2errorza2z00zzengine_paramz00 =
		BUNSPEC;
	static obj_t __cnst[289];


	   
		 
		DEFINE_STRING(BGl_string1400z00zzengine_paramz00,
		BgL_bgl_string1400za700za7za7e1626za7, ".", 1);
	      DEFINE_STRING(BGl_string1401z00zzengine_paramz00,
		BgL_bgl_string1401za700za7za7e1627za7, "The lib dir path", 16);
	      DEFINE_STRING(BGl_string1403z00zzengine_paramz00,
		BgL_bgl_string1403za700za7za7e1628za7, "Depreacted, don't use", 21);
	      DEFINE_STRING(BGl_string1406z00zzengine_paramz00,
		BgL_bgl_string1406za700za7za7e1629za7, "runtime", 7);
	      DEFINE_STRING(BGl_string1408z00zzengine_paramz00,
		BgL_bgl_string1408za700za7za7e1630za7, "The Bigloo library", 18);
	      DEFINE_STRING(BGl_string1409z00zzengine_paramz00,
		BgL_bgl_string1409za700za7za7e1631za7, "The Gc library", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1402z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1632za7,
		BGl_z62zc3z04anonymousza31104ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1404z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1633za7,
		BGl_z62zc3z04anonymousza31124ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1411z00zzengine_paramz00,
		BgL_bgl_string1411za700za7za7e1634za7, "Are we using a custom GC library?",
		33);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1405z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1635za7,
		BGl_z62zc3z04anonymousza31126ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1413z00zzengine_paramz00,
		BgL_bgl_string1413za700za7za7e1636za7, "Are we using a multi-threaded GC?",
		33);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1407z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1637za7,
		BGl_z62zc3z04anonymousza31130ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1414z00zzengine_paramz00,
		BgL_bgl_string1414za700za7za7e1638za7,
		"Force GC roots registration for global variables (for experts only)", 67);
	      DEFINE_STRING(BGl_string1415z00zzengine_paramz00,
		BgL_bgl_string1415za700za7za7e1639za7,
		"Do we have the bigloo-abort function in executables?", 52);
	      DEFINE_STRING(BGl_string1417z00zzengine_paramz00,
		BgL_bgl_string1417za700za7za7e1640za7,
		"Do we use the static Bigloo library", 35);
	      DEFINE_STRING(BGl_string1418z00zzengine_paramz00,
		BgL_bgl_string1418za700za7za7e1641za7,
		"Do we use the static version of all Bigloo libraries?", 53);
	      DEFINE_STRING(BGl_string1419z00zzengine_paramz00,
		BgL_bgl_string1419za700za7za7e1642za7,
		"Do we include the additional user libraries twice?", 50);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1410z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1643za7,
		BGl_z62zc3z04anonymousza31142ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1412z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1644za7,
		BGl_z62zc3z04anonymousza31150ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1500z00zzengine_paramz00,
		BgL_bgl_string1500za700za7za7e1645za7, "The garbage collector", 21);
	      DEFINE_STRING(BGl_string1501z00zzengine_paramz00,
		BgL_bgl_string1501za700za7za7e1646za7, "Runtime type safety", 19);
	      DEFINE_STRING(BGl_string1420z00zzengine_paramz00,
		BgL_bgl_string1420za700za7za7e1647za7, "The user extra C libraries", 26);
	      DEFINE_STRING(BGl_string1502z00zzengine_paramz00,
		BgL_bgl_string1502za700za7za7e1648za7, "Runtime type arity safety", 25);
	      DEFINE_STRING(BGl_string1503z00zzengine_paramz00,
		BgL_bgl_string1503za700za7za7e1649za7, "Runtime range safety", 20);
	      DEFINE_STRING(BGl_string1422z00zzengine_paramz00,
		BgL_bgl_string1422za700za7za7e1650za7, "The user extra Bigloo libraries",
		31);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1416z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1651za7,
		BGl_z62zc3z04anonymousza31153ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1504z00zzengine_paramz00,
		BgL_bgl_string1504za700za7za7e1652za7, "Runtime struct range safety", 27);
	      DEFINE_STRING(BGl_string1423z00zzengine_paramz00,
		BgL_bgl_string1423za700za7za7e1653za7,
		"A list of C functions to be called when starting the application", 64);
	      DEFINE_STRING(BGl_string1505z00zzengine_paramz00,
		BgL_bgl_string1505za700za7za7e1654za7, "Module version safety", 21);
	      DEFINE_STRING(BGl_string1424z00zzengine_paramz00,
		BgL_bgl_string1424za700za7za7e1655za7, "The user extra Bigloo Zip files",
		31);
	      DEFINE_STRING(BGl_string1343z00zzengine_paramz00,
		BgL_bgl_string1343za700za7za7e1656za7, "The Bigloo major release number",
		31);
	      DEFINE_STRING(BGl_string1506z00zzengine_paramz00,
		BgL_bgl_string1506za700za7za7e1657za7, "Use the unsafe library version",
		30);
	      DEFINE_STRING(BGl_string1507z00zzengine_paramz00,
		BgL_bgl_string1507za700za7za7e1658za7,
		"Disable type checking for eval functions", 40);
	      DEFINE_STRING(BGl_string1426z00zzengine_paramz00,
		BgL_bgl_string1426za700za7za7e1659za7,
		"Enable/disable multiple inclusion of same file", 46);
	      DEFINE_STRING(BGl_string1345z00zzengine_paramz00,
		BgL_bgl_string1345za700za7za7e1660za7, "The Bigloo specific version", 27);
	      DEFINE_STRING(BGl_string1508z00zzengine_paramz00,
		BgL_bgl_string1508za700za7za7e1661za7, "Disable heap version checking", 29);
	      DEFINE_STRING(BGl_string1427z00zzengine_paramz00,
		BgL_bgl_string1427za700za7za7e1662za7, "bigloo.h", 8);
	      DEFINE_STRING(BGl_string1509z00zzengine_paramz00,
		BgL_bgl_string1509za700za7za7e1663za7,
		"Set to #t to warn about virtual slot overriding", 47);
	      DEFINE_STRING(BGl_string1428z00zzengine_paramz00,
		BgL_bgl_string1428za700za7za7e1664za7, "The C included files", 20);
	      DEFINE_STRING(BGl_string1347z00zzengine_paramz00,
		BgL_bgl_string1347za700za7za7e1665za7, ")", 1);
	      DEFINE_STRING(BGl_string1348z00zzengine_paramz00,
		BgL_bgl_string1348za700za7za7e1666za7, "(", 1);
	      DEFINE_STRING(BGl_string1349z00zzengine_paramz00,
		BgL_bgl_string1349za700za7za7e1667za7, "Bigloo ", 7);
	extern obj_t BGl_uncygdrivezd2envzd2zztools_miscz00;
	extern obj_t BGl_stringza2zd2ze3stringzd2envz41zztools_miscz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1421z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1668za7,
		BGl_z62zc3z04anonymousza31158ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1510z00zzengine_paramz00,
		BgL_bgl_string1510za700za7za7e1669za7,
		"Set to #t to warn about variable overriding", 43);
	      DEFINE_STRING(BGl_string1511z00zzengine_paramz00,
		BgL_bgl_string1511za700za7za7e1670za7,
		"Set to #t to warn about type checks", 35);
	      DEFINE_STRING(BGl_string1430z00zzengine_paramz00,
		BgL_bgl_string1430za700za7za7e1671za7, "The additional C included files",
		31);
	      DEFINE_STRING(BGl_string1512z00zzengine_paramz00,
		BgL_bgl_string1512za700za7za7e1672za7,
		"Set to #t to treat type warnigns as error", 41);
	      DEFINE_STRING(BGl_string1431z00zzengine_paramz00,
		BgL_bgl_string1431za700za7za7e1673za7, "bigloo", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1425z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1674za7,
		BGl_z62zc3z04anonymousza31165ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1350z00zzengine_paramz00,
		BgL_bgl_string1350za700za7za7e1675za7, "The Bigloo name", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1344z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1676za7,
		BGl_z62zc3z04anonymousza31029ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1513z00zzengine_paramz00,
		BgL_bgl_string1513za700za7za7e1677za7,
		"Set to #t to warn about non-inlinable default slot values", 57);
	      DEFINE_STRING(BGl_string1432z00zzengine_paramz00,
		BgL_bgl_string1432za700za7za7e1678za7, "The Bigloo heap base name", 25);
	      DEFINE_STRING(BGl_string1514z00zzengine_paramz00,
		BgL_bgl_string1514za700za7za7e1679za7, "Use the profiled library version",
		32);
	      DEFINE_STRING(BGl_string1433z00zzengine_paramz00,
		BgL_bgl_string1433za700za7za7e1680za7, ".heap", 5);
	      DEFINE_STRING(BGl_string1352z00zzengine_paramz00,
		BgL_bgl_string1352za700za7za7e1681za7, "Inria -- Sophia Antipolis", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1346z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1682za7,
		BGl_z62zc3z04anonymousza31031ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1515z00zzengine_paramz00,
		BgL_bgl_string1515za700za7za7e1683za7, "Shared constant compilation?", 28);
	      DEFINE_STRING(BGl_string1434z00zzengine_paramz00,
		BgL_bgl_string1434za700za7za7e1684za7, "The Bigloo heap file name", 25);
	      DEFINE_STRING(BGl_string1353z00zzengine_paramz00,
		BgL_bgl_string1353za700za7za7e1685za7, "bigloo@lists-sop.inria.fr", 25);
	      DEFINE_STRING(BGl_string1516z00zzengine_paramz00,
		BgL_bgl_string1516za700za7za7e1686za7, "Lib-mode compilation?", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1429z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1687za7,
		BGl_z62zc3z04anonymousza31172ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1354z00zzengine_paramz00,
		BgL_bgl_string1354za700za7za7e1688za7,
		"http://www-sop.inria.fr/indes/fp/Bigloo", 39);
	      DEFINE_STRING(BGl_string1517z00zzengine_paramz00,
		BgL_bgl_string1517za700za7za7e1689za7, "Module initialization mode", 26);
	      DEFINE_STRING(BGl_string1436z00zzengine_paramz00,
		BgL_bgl_string1436za700za7za7e1690za7, "The library the heap belongs to",
		31);
	      DEFINE_STRING(BGl_string1355z00zzengine_paramz00,
		BgL_bgl_string1355za700za7za7e1691za7, "TMPDIR", 6);
	      DEFINE_STRING(BGl_string1518z00zzengine_paramz00,
		BgL_bgl_string1518za700za7za7e1692za7, "Object initialization mode", 26);
	      DEFINE_STRING(BGl_string1437z00zzengine_paramz00,
		BgL_bgl_string1437za700za7za7e1693za7, ".jheap", 6);
	      DEFINE_STRING(BGl_string1356z00zzengine_paramz00,
		BgL_bgl_string1356za700za7za7e1694za7, "The tmp directory name", 22);
	      DEFINE_STRING(BGl_string1519z00zzengine_paramz00,
		BgL_bgl_string1519za700za7za7e1695za7,
		"Emit a standard Bigloo dynamic loading init entry point", 55);
	      DEFINE_STRING(BGl_string1438z00zzengine_paramz00,
		BgL_bgl_string1438za700za7za7e1696za7,
		"The Bigloo heap file name for the JVM backend", 45);
	      DEFINE_STRING(BGl_string1358z00zzengine_paramz00,
		BgL_bgl_string1358za700za7za7e1697za7, "Add the Bigloo license ?", 24);
	      DEFINE_STRING(BGl_string1359z00zzengine_paramz00,
		BgL_bgl_string1359za700za7za7e1698za7, "The verbosity level", 19);
	extern obj_t BGl_replacez12zd2envzc0zztools_miscz00;
	extern obj_t
		BGl_buildzd2pathzd2fromzd2shellzd2variablezd2envzd2zztools_miscz00;
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1351z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1699za7,
		BGl_z62zc3z04anonymousza31039ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1601z00zzengine_paramz00,
		BgL_bgl_string1601za700za7za7e1700za7,
		"An optional function that pre-processes the source file", 55);
	      DEFINE_STRING(BGl_string1520z00zzengine_paramz00,
		BgL_bgl_string1520za700za7za7e1701za7,
		"Emit a standard GC init call when initialization the module", 59);
	      DEFINE_STRING(BGl_string1602z00zzengine_paramz00,
		BgL_bgl_string1602za700za7za7e1702za7, "The load path", 13);
	      DEFINE_STRING(BGl_string1521z00zzengine_paramz00,
		BgL_bgl_string1521za700za7za7e1703za7, "Max C token length", 18);
	      DEFINE_STRING(BGl_string1440z00zzengine_paramz00,
		BgL_bgl_string1440za700za7za7e1704za7, ".wheap", 6);
	      DEFINE_STRING(BGl_string1603z00zzengine_paramz00,
		BgL_bgl_string1603za700za7za7e1705za7, "The user specific compilation pass",
		34);
	      DEFINE_STRING(BGl_string1522z00zzengine_paramz00,
		BgL_bgl_string1522za700za7za7e1706za7, "C split long strings", 20);
	      DEFINE_STRING(BGl_string1441z00zzengine_paramz00,
		BgL_bgl_string1441za700za7za7e1707za7,
		"The Bigloo heap file name for the WASM backend", 46);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1435z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1708za7,
		BGl_z62zc3z04anonymousza31183ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1360z00zzengine_paramz00,
		BgL_bgl_string1360za700za7za7e1709za7, "Say hello (when verbose)", 24);
	      DEFINE_STRING(BGl_string1604z00zzengine_paramz00,
		BgL_bgl_string1604za700za7za7e1710za7,
		"   All the Bigloo control variables can be changed from the", 59);
	      DEFINE_STRING(BGl_string1361z00zzengine_paramz00,
		BgL_bgl_string1361za700za7za7e1711za7, "The sources files", 17);
	      DEFINE_STRING(BGl_string1605z00zzengine_paramz00,
		BgL_bgl_string1605za700za7za7e1712za7,
		"   interpreter, by the means of the `-eval' option, or using", 60);
	      DEFINE_STRING(BGl_string1524z00zzengine_paramz00,
		BgL_bgl_string1524za700za7za7e1713za7, "Max C function arity", 20);
	      DEFINE_STRING(BGl_string1443z00zzengine_paramz00,
		BgL_bgl_string1443za700za7za7e1714za7, "The name of the heap to be dumped",
		33);
	      DEFINE_STRING(BGl_string1362z00zzengine_paramz00,
		BgL_bgl_string1362za700za7za7e1715za7, "The target name", 15);
	      DEFINE_STRING(BGl_string1606z00zzengine_paramz00,
		BgL_bgl_string1606za700za7za7e1716za7,
		"   the module clause `option'. For instance the option", 54);
	      DEFINE_STRING(BGl_string1525z00zzengine_paramz00,
		BgL_bgl_string1525za700za7za7e1717za7, "trace", 5);
	      DEFINE_STRING(BGl_string1444z00zzengine_paramz00,
		BgL_bgl_string1444za700za7za7e1718za7,
		"The identifier of the Jlib foreign class", 40);
	      DEFINE_STRING(BGl_string1363z00zzengine_paramz00,
		BgL_bgl_string1363za700za7za7e1719za7, "The shell to exec C compilations",
		32);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1357z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1720za7,
		BGl_z62zc3z04anonymousza31048ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1607z00zzengine_paramz00,
		BgL_bgl_string1607za700za7za7e1721za7,
		"   \"-eval '(set! *strip* #t)'\" will set the variable", 52);
	      DEFINE_STRING(BGl_string1526z00zzengine_paramz00,
		BgL_bgl_string1526za700za7za7e1722za7, "Trace file name", 15);
	      DEFINE_STRING(BGl_string1445z00zzengine_paramz00,
		BgL_bgl_string1445za700za7za7e1723za7, "bigloo.foreign", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1439z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1724za7,
		BGl_z62zc3z04anonymousza31188ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1608z00zzengine_paramz00,
		BgL_bgl_string1608za700za7za7e1725za7, "   `*strip*' to the value `#t'.",
		31);
	      DEFINE_STRING(BGl_string1527z00zzengine_paramz00,
		BgL_bgl_string1527za700za7za7e1726za7, "Trace level", 11);
	      DEFINE_STRING(BGl_string1446z00zzengine_paramz00,
		BgL_bgl_string1446za700za7za7e1727za7, "The name of the Jlib foreign class",
		34);
	      DEFINE_STRING(BGl_string1365z00zzengine_paramz00,
		BgL_bgl_string1365za700za7za7e1728za7, "The C compiler style", 20);
	      DEFINE_STRING(BGl_string1609z00zzengine_paramz00,
		BgL_bgl_string1609za700za7za7e1729za7, "   These variables are:", 23);
	      DEFINE_STRING(BGl_string1528z00zzengine_paramz00,
		BgL_bgl_string1528za700za7za7e1730za7, "Trace dumping max level", 23);
	      DEFINE_STRING(BGl_string1447z00zzengine_paramz00,
		BgL_bgl_string1447za700za7za7e1731za7,
		"A name of an additional heap file name to be build", 50);
	      DEFINE_STRING(BGl_string1529z00zzengine_paramz00,
		BgL_bgl_string1529za700za7za7e1732za7, "Optimization level", 18);
	      DEFINE_STRING(BGl_string1448z00zzengine_paramz00,
		BgL_bgl_string1448za700za7za7e1733za7,
		"A list of Bigloo additional heap file name", 42);
	      DEFINE_STRING(BGl_string1367z00zzengine_paramz00,
		BgL_bgl_string1367za700za7za7e1734za7, "The C compiler", 14);
	      DEFINE_STRING(BGl_string1449z00zzengine_paramz00,
		BgL_bgl_string1449za700za7za7e1735za7, "The name of the C beautifier", 28);
	      DEFINE_STRING(BGl_string1369z00zzengine_paramz00,
		BgL_bgl_string1369za700za7za7e1736za7, "The C compiler option", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1600z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704za7a2preza71737za7,
		BGl_z62zc3z04za2prezd2processorza2za31229ze3z37zzengine_paramz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1523z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1738za7,
		BGl_z62zc3z04anonymousza31201ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1442z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1739za7,
		BGl_z62zc3z04anonymousza31190ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1611z00zzengine_paramz00,
		BgL_bgl_string1611za700za7za7e1740za7, "   - ", 5);
	      DEFINE_STRING(BGl_string1530z00zzengine_paramz00,
		BgL_bgl_string1530za700za7za7e1741za7, "Loop unrolling optimization", 27);
	      DEFINE_STRING(BGl_string1612z00zzengine_paramz00,
		BgL_bgl_string1612za700za7za7e1742za7, " : ", 3);
	      DEFINE_STRING(BGl_string1531z00zzengine_paramz00,
		BgL_bgl_string1531za700za7za7e1743za7, "Loop inlining optimization", 26);
	      DEFINE_STRING(BGl_string1613z00zzengine_paramz00,
		BgL_bgl_string1613za700za7za7e1744za7, "     ", 5);
	      DEFINE_STRING(BGl_string1532z00zzengine_paramz00,
		BgL_bgl_string1532za700za7za7e1745za7,
		"Skip atom in inlining parameter counting", 40);
	      DEFINE_STRING(BGl_string1451z00zzengine_paramz00,
		BgL_bgl_string1451za700za7za7e1746za7, "Debugging level", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1364z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1747za7,
		BGl_z62zc3z04anonymousza31050ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1614z00zzengine_paramz00,
		BgL_bgl_string1614za700za7za7e1748za7, "     default: ", 14);
	      DEFINE_STRING(BGl_string1533z00zzengine_paramz00,
		BgL_bgl_string1533za700za7za7e1749za7,
		"Enable optimization by macro-expansion", 38);
	      DEFINE_STRING(BGl_string1452z00zzengine_paramz00,
		BgL_bgl_string1452za700za7za7e1750za7, "Debugging trace level", 21);
	      DEFINE_STRING(BGl_string1371z00zzengine_paramz00,
		BgL_bgl_string1371za700za7za7e1751za7, "The C compiler optimization option",
		34);
	      DEFINE_STRING(BGl_string1615z00zzengine_paramz00,
		BgL_bgl_string1615za700za7za7e1752za7, " [", 2);
	      DEFINE_STRING(BGl_string1534z00zzengine_paramz00,
		BgL_bgl_string1534za700za7za7e1753za7, "Enable JVM inlining", 19);
	      DEFINE_STRING(BGl_string1453z00zzengine_paramz00,
		BgL_bgl_string1453za700za7za7e1754za7,
		"Localize error calls in the source code", 39);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1366z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1755za7,
		BGl_z62zc3z04anonymousza31052ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1616z00zzengine_paramz00,
		BgL_bgl_string1616za700za7za7e1756za7, "]", 1);
	      DEFINE_STRING(BGl_string1535z00zzengine_paramz00,
		BgL_bgl_string1535za700za7za7e1757za7,
		"Enable JVM inlining for constructors", 36);
	      DEFINE_STRING(BGl_string1454z00zzengine_paramz00,
		BgL_bgl_string1454za700za7za7e1758za7, "Compiler self sharing debug", 27);
	      DEFINE_STRING(BGl_string1373z00zzengine_paramz00,
		BgL_bgl_string1373za700za7za7e1759za7, "The C compiler profiling option",
		31);
	      DEFINE_STRING(BGl_string1617z00zzengine_paramz00,
		BgL_bgl_string1617za700za7za7e1760za7, "engine_param", 12);
	      DEFINE_STRING(BGl_string1536z00zzengine_paramz00,
		BgL_bgl_string1536za700za7za7e1761za7, "Enable JVM peephole optimization",
		32);
	      DEFINE_STRING(BGl_string1455z00zzengine_paramz00,
		BgL_bgl_string1455za700za7za7e1762za7, "Compiler self type debug", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1368z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1763za7,
		BGl_z62zc3z04anonymousza31054ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1618z00zzengine_paramz00,
		BgL_bgl_string1618za700za7za7e1764za7, "Engine/param.scm", 16);
	      DEFINE_STRING(BGl_string1537z00zzengine_paramz00,
		BgL_bgl_string1537za700za7za7e1765za7, "Enable JVM branch tensioning", 28);
	      DEFINE_STRING(BGl_string1456z00zzengine_paramz00,
		BgL_bgl_string1456za700za7za7e1766za7, "Compiler self stack trace debug",
		31);
	      DEFINE_STRING(BGl_string1375z00zzengine_paramz00,
		BgL_bgl_string1375za700za7za7e1767za7, "The C compiler rpath option", 27);
	      DEFINE_STRING(BGl_string1619z00zzengine_paramz00,
		BgL_bgl_string1619za700za7za7e1768za7,
		"*additional-traces* *old-load-path* *access-shape?* *bigloo-email* *builtin-allocators* epairify* epairify uncygdrive *tmp-dest* *rest-args* *type-shape?* *location-shape?* *key-shape?* add-updater! *bigloo-cmd-name* *module-shape?* append-21011 bigloo-variables-usage bigloo-date *bigloo-url* epairify-propagate-loc *param-updaters* *alloc-shape?* *bigloo-variables* replace! *typenode-shape?* *bigloo-args* *bigloo-author* *name-shape?* *typename-shape?* *arithmetic-new-overflow* *arithmetic-genericity* epairify-rec *bigloo-date* *early-with-modules* string*->string *user-shape?* *arithmetic-overflow* add-doc-variable! build-path-from-shell-variable reinitialize-bigloo-variables! epairify-propagate *bdb-debug-no-line-directives?* string-split-char *user-pass* *load-path* *pre-processor* *local-exit?* *allow-type-redefinition* *eval-options* ((\"CONS\" . \"make_pair\") (\"%STRING->SYMBOL\" . \"make_symbol\") (\"%MAKE-STRING\" . \"string_to_bstring_len\") (\"%MAKE-OUTPUT-PORT\" . \"bgl_make_output_port\") (\"%MAK"
		"E-INPUT-PORT\" . \"bgl_make_input_port\") (\"%MAKE-ERROR-PORT\" . \"make_error_port\")) *global-tail-call?* *saw-bbv-functions* *saw-bbv?* *saw-spill* *saw-no-register-allocation-functions* *saw-register-allocation-functions* *saw-register-allocation-max-size* *saw-register-allocation-onexpression?* *saw-register-allocation?* *saw-register-reallocation?* *saw* *target-language* default-back-end *reader* plain *user-heap-size* *ast-case-sensitive* *auto-mode* ((\"ml\" . \"caml\") (\"mli\" . \"caml\") (\"oon\" . \"meroon\") (\"snow\" . \"snow\") (\"spi\" . \"pkgcomp\")) *mco-include-path* (\".\") *mco-suffix* (\"mco\") *obj-suffix* *csharp-suffix* (\"cs\") *c-suffix* (\"c\") *src-suffix* (\"scm\" \"bgl\") *extend-entry* *optim-uncell?* *optim-stackable?* *optim-specialize-flonum?* *optim-tagged-fxop?* *optim-return-goto?* *optim-return?* *inlining-reduce-kfactor* *inlining-kfactor* *user-inlining?* *inlining?* *optim-reduce-beta?* *optim-sync-failsafe?* *optim-initflow?* *optim-dataflow-types?* *optim-dataflow-"
		"for-errors?* *optim-dataflow?* *optim-integrate?* *optim-cfa-force-loose-local-function?* *optim-cfa-unbox-closure-args* *optim-cfa-pair-quote-max-length* *optim-cfa-pair?* *optim-cfa-apply-tracking?* *optim-cfa-free-var-tracking?* *optim-cfa-flonum-arithmetic?* *optim-cfa-fixnum-arithmetic?* *optim-cfa?* *optim-isa?* *optim-jvm* *jvm-env* *purify* *optim-symbol-case* *optim-jvm-fasteq* *optim-jvm-branch* *optim-jvm-peephole* *optim-jvm-constructor-inlining* *optim-jvm-inlining* *optim-O-macro?* *optim-atom-inlining?* *optim-loop-inlining?* *optim-unroll-loop?* *optim* *trace-write-length* *trace-level* *trace-name* *max-c-foreign-arity* *c-split-string* c-string-split *max-c-token-length* *dlopen-init-gc* *dlopen-init* *object-init-mode* stagged *init-mode* read *lib-mode* *shared-cnst?* *profile-library* *warning-default-slot-value* *warning-type-error* *warning-types* *warning-overriden-variables* *warning-overriden-slots* *unsafe-heap* *unsafe-eval* *unsafe-library* *unsafe-version* *unsafe-struct* *unsaf"
		"e-range* *unsafe-arity* *unsafe-type* *garbage-collector* boehm *module-checksum-object?* *jvm-cinit-module* *jvm-catch* *jvm-directory* *jvm-jarpath* *jvm-path-separator* *jvm-mainclass* *jvm-classpath* *jvm-bigloo-classpath* *jvm-options* *jvm-java* java *jvm-shell* java-shell *jvm-jar?* *pass* ld *auto-link-main* *call/cc?* *startup-file* *interpreter* *with-files* *c-files* *o-files* *qualified-type-file-default* *qualified-type-file* *access-file-default* *access-files* *prof-table-name* *profile-mode* *bdb-debug* *jvm-debug* *c-tail-call* *c-user-foot* *c-user-header* *c-debug-option* c-compiler-debug-option *c-debug-lines-info* *c-debug* *debug-module* *sync-profiling* *bmem-profiling* *compiler-stack-debug?* *compiler-type-debug?* *compiler-sharing-debug?* *error-localization* *compiler-debug-trace* *compiler-debug* *indent* c-beautifier *additional-heap-names* *additional-heap-name* *jvm-foreign-class-name* *jvm-foreign-class-id* foreign *heap-dump-names* *heap-wasm-name* *heap-jvm-name* *heap-librar"
		"y* *heap-name* *heap-base-name* *additional-include-foreign* *include-foreign* *include-multiple* *additional-bigloo-zips* *bigloo-libraries-c-setup* *additional-bigloo-libraries* *bigloo-user-lib* user-libraries *double-ld-libs?* *static-all-bigloo?* *static-bigloo?* *bigloo-abort?* have-bigloo-abort *gc-force-register-roots?* *multi-threaded-gc?* *gc-custom?* *gc-lib* gc-lib gc-custom *bigloo-lib* bigloo *lib-src-dir* *ld-library-dir* *default-lib-dir* *lib-dir* *strip* *ld-relative* *cc-move* *ld-post-options* *ld-optim-flags* c-linker-optim-flags *ld-debug-option* c-linker-debug-option *ld-o-option* c-linker-o-option *ld-options* c-linker-flags *ld-style* c-linker-style *rm-tmp-files* *cc-options* (\"\") *stdc* strict-stdc *c-object-file-extension* c-object-file-extension *cc-o-option* c-compiler-o-option *cflags-rpath* library-directory *cflags-prof* c-prof-flag *cflags-optim* c-compiler-optim-flag *cflags* c-flags *cc* c-compiler *cc-style* c-compiler-style *shell* shell *dest* *src-files* *hello* *verb"
		"ose* *bigloo-licensing?* *bigloo-tmp* nothing-yet *bigloo-name* *bigloo-specific-version* specific-version *bigloo-version* release-number ",
		5201);
	      DEFINE_STRING(BGl_string1538z00zzengine_paramz00,
		BgL_bgl_string1538za700za7za7e1769za7,
		"EQ? no longer works on integers (use =FX instead)", 49);
	      DEFINE_STRING(BGl_string1457z00zzengine_paramz00,
		BgL_bgl_string1457za700za7za7e1770za7, "Instrument code for bmem profiling",
		34);
	      DEFINE_STRING(BGl_string1539z00zzengine_paramz00,
		BgL_bgl_string1539za700za7za7e1771za7,
		"Optimize case forms descrimining on symbols only", 48);
	      DEFINE_STRING(BGl_string1458z00zzengine_paramz00,
		BgL_bgl_string1458za700za7za7e1772za7,
		"Instrument code for synchronize profiling", 41);
	      DEFINE_STRING(BGl_string1377z00zzengine_paramz00,
		BgL_bgl_string1377za700za7za7e1773za7, "The C compiler -o option", 24);
	      DEFINE_STRING(BGl_string1459z00zzengine_paramz00,
		BgL_bgl_string1459za700za7za7e1774za7, "Module initilazation debugging",
		30);
	      DEFINE_STRING(BGl_string1379z00zzengine_paramz00,
		BgL_bgl_string1379za700za7za7e1775za7, "The C object file extension", 27);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1610z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1776za7,
		BGl_z62zc3z04anonymousza31313ze3ze5zzengine_paramz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1450z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1777za7,
		BGl_z62zc3z04anonymousza31192ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1370z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1778za7,
		BGl_z62zc3z04anonymousza31056ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1540z00zzengine_paramz00,
		BgL_bgl_string1540za700za7za7e1779za7,
		"Produce byte code verifier compliant JVM code", 45);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1372z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1780za7,
		BGl_z62zc3z04anonymousza31058ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1541z00zzengine_paramz00,
		BgL_bgl_string1541za700za7za7e1781za7,
		"List of environment variables to be available in the compiled code", 66);
	      DEFINE_STRING(BGl_string1460z00zzengine_paramz00,
		BgL_bgl_string1460za700za7za7e1782za7, "C debugging mode?", 17);
	      DEFINE_STRING(BGl_string1542z00zzengine_paramz00,
		BgL_bgl_string1542za700za7za7e1783za7,
		"Enable optimization by inlining jvm code", 40);
	      DEFINE_STRING(BGl_string1461z00zzengine_paramz00,
		BgL_bgl_string1461za700za7za7e1784za7, "Emit # line directives", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1374z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1785za7,
		BGl_z62zc3z04anonymousza31060ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1543z00zzengine_paramz00,
		BgL_bgl_string1543za700za7za7e1786za7,
		"Enable isa type tests optimization (inlining)", 45);
	      DEFINE_STRING(BGl_string1462z00zzengine_paramz00,
		BgL_bgl_string1462za700za7za7e1787za7, "cc debugging option", 19);
	      DEFINE_STRING(BGl_string1381z00zzengine_paramz00,
		BgL_bgl_string1381za700za7za7e1788za7, "Shall we produce ISO C?", 23);
	      DEFINE_STRING(BGl_string1544z00zzengine_paramz00,
		BgL_bgl_string1544za700za7za7e1789za7, "Enable CFA", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1376z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1790za7,
		BGl_z62zc3z04anonymousza31066ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1545z00zzengine_paramz00,
		BgL_bgl_string1545za700za7za7e1791za7,
		"Enable refined fixnum arithmetic specialization", 47);
	      DEFINE_STRING(BGl_string1464z00zzengine_paramz00,
		BgL_bgl_string1464za700za7za7e1792za7, "C header", 8);
	      DEFINE_STRING(BGl_string1383z00zzengine_paramz00,
		BgL_bgl_string1383za700za7za7e1793za7, "cc options", 10);
	      DEFINE_STRING(BGl_string1546z00zzengine_paramz00,
		BgL_bgl_string1546za700za7za7e1794za7,
		"Enable refined flonum arithmetic specialization", 47);
	      DEFINE_STRING(BGl_string1465z00zzengine_paramz00,
		BgL_bgl_string1465za700za7za7e1795za7, "C foot", 6);
	      DEFINE_STRING(BGl_string1384z00zzengine_paramz00,
		BgL_bgl_string1384za700za7za7e1796za7,
		"Shall the .c and .il produced files be removed?", 47);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1378z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1797za7,
		BGl_z62zc3z04anonymousza31075ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1547z00zzengine_paramz00,
		BgL_bgl_string1547za700za7za7e1798za7,
		"Enable closure free-variables specialization", 44);
	      DEFINE_STRING(BGl_string1466z00zzengine_paramz00,
		BgL_bgl_string1466za700za7za7e1799za7, "C tail call?", 12);
	      DEFINE_STRING(BGl_string1385z00zzengine_paramz00,
		BgL_bgl_string1385za700za7za7e1800za7, "ld style", 8);
	      DEFINE_STRING(BGl_string1548z00zzengine_paramz00,
		BgL_bgl_string1548za700za7za7e1801za7, "Track values across apply", 25);
	      DEFINE_STRING(BGl_string1467z00zzengine_paramz00,
		BgL_bgl_string1467za700za7za7e1802za7, "JVM debugging mode?", 19);
	      DEFINE_STRING(BGl_string1549z00zzengine_paramz00,
		BgL_bgl_string1549za700za7za7e1803za7, "Track values across pairs", 25);
	      DEFINE_STRING(BGl_string1468z00zzengine_paramz00,
		BgL_bgl_string1468za700za7za7e1804za7, "Bdb debugging mode", 18);
	      DEFINE_STRING(BGl_string1387z00zzengine_paramz00,
		BgL_bgl_string1387za700za7za7e1805za7, "ld options", 10);
	      DEFINE_STRING(BGl_string1469z00zzengine_paramz00,
		BgL_bgl_string1469za700za7za7e1806za7, "Bigloo profile mode", 19);
	      DEFINE_STRING(BGl_string1389z00zzengine_paramz00,
		BgL_bgl_string1389za700za7za7e1807za7, "The C linker -o option", 22);
	extern obj_t BGl_epairifyzd2envzd2zztools_miscz00;
	extern obj_t BGl_bigloozd2datezd2envz00zztools_datez00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1380z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1808za7,
		BGl_z62zc3z04anonymousza31077ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1550z00zzengine_paramz00,
		BgL_bgl_string1550za700za7za7e1809za7,
		"Maximum length for pair literal tracking", 40);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1463z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1810za7,
		BGl_z62zc3z04anonymousza31194ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1382z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1811za7,
		BGl_z62zc3z04anonymousza31079ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1551z00zzengine_paramz00,
		BgL_bgl_string1551za700za7za7e1812za7, "Unbox closure arguments", 23);
	      DEFINE_STRING(BGl_string1470z00zzengine_paramz00,
		BgL_bgl_string1470za700za7za7e1813za7, "bmon.out", 8);
	      DEFINE_STRING(BGl_string1552z00zzengine_paramz00,
		BgL_bgl_string1552za700za7za7e1814za7,
		"Force loosing local function approximations (for debugging)", 59);
	      DEFINE_STRING(BGl_string1471z00zzengine_paramz00,
		BgL_bgl_string1471za700za7za7e1815za7, "Bprof translation table file name",
		33);
	      DEFINE_STRING(BGl_string1553z00zzengine_paramz00,
		BgL_bgl_string1553za700za7za7e1816za7,
		"Enable function integration (closure analysis)", 46);
	      DEFINE_STRING(BGl_string1472z00zzengine_paramz00,
		BgL_bgl_string1472za700za7za7e1817za7, "The access file names", 21);
	      DEFINE_STRING(BGl_string1391z00zzengine_paramz00,
		BgL_bgl_string1391za700za7za7e1818za7, "The C linker debugging option", 29);
	      DEFINE_STRING(BGl_string1554z00zzengine_paramz00,
		BgL_bgl_string1554za700za7za7e1819za7,
		"Enable simple dataflow optimization", 35);
	      DEFINE_STRING(BGl_string1473z00zzengine_paramz00,
		BgL_bgl_string1473za700za7za7e1820za7, ".afile", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1386z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1821za7,
		BGl_z62zc3z04anonymousza31081ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1555z00zzengine_paramz00,
		BgL_bgl_string1555za700za7za7e1822za7,
		"Enable simple dataflow optimization for eliminating bad error messages",
		70);
	      DEFINE_STRING(BGl_string1474z00zzengine_paramz00,
		BgL_bgl_string1474za700za7za7e1823za7, "The default access file name", 28);
	      DEFINE_STRING(BGl_string1393z00zzengine_paramz00,
		BgL_bgl_string1393za700za7za7e1824za7, "The C linker optimization flags",
		31);
	      DEFINE_STRING(BGl_string1556z00zzengine_paramz00,
		BgL_bgl_string1556za700za7za7e1825za7,
		"Enable dataflow optimization for types", 38);
	      DEFINE_STRING(BGl_string1475z00zzengine_paramz00,
		BgL_bgl_string1475za700za7za7e1826za7,
		"The qualifed-type association file name", 39);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1388z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1827za7,
		BGl_z62zc3z04anonymousza31083ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1557z00zzengine_paramz00,
		BgL_bgl_string1557za700za7za7e1828za7,
		"Enable initflow optimization for global variables", 49);
	      DEFINE_STRING(BGl_string1476z00zzengine_paramz00,
		BgL_bgl_string1476za700za7za7e1829za7, ".jfile", 6);
	      DEFINE_STRING(BGl_string1395z00zzengine_paramz00,
		BgL_bgl_string1395za700za7za7e1830za7, "ld post options", 15);
	      DEFINE_STRING(BGl_string1558z00zzengine_paramz00,
		BgL_bgl_string1558za700za7za7e1831za7,
		"Enable failsafe synchronize optimization", 40);
	      DEFINE_STRING(BGl_string1477z00zzengine_paramz00,
		BgL_bgl_string1477za700za7za7e1832za7, "The additional obect files", 26);
	      DEFINE_STRING(BGl_string1396z00zzengine_paramz00,
		BgL_bgl_string1396za700za7za7e1833za7,
		"Use mv instead of -o when C compiling", 37);
	      DEFINE_STRING(BGl_string1559z00zzengine_paramz00,
		BgL_bgl_string1559za700za7za7e1834za7, "Enable simple beta reduction", 28);
	      DEFINE_STRING(BGl_string1478z00zzengine_paramz00,
		BgL_bgl_string1478za700za7za7e1835za7, "The C source files", 18);
	      DEFINE_STRING(BGl_string1397z00zzengine_paramz00,
		BgL_bgl_string1397za700za7za7e1836za7,
		"Relative or absolute path names for libraries", 45);
	      DEFINE_STRING(BGl_string1479z00zzengine_paramz00,
		BgL_bgl_string1479za700za7za7e1837za7, "The additional modules", 22);
	      DEFINE_STRING(BGl_string1398z00zzengine_paramz00,
		BgL_bgl_string1398za700za7za7e1838za7, "Shall we strip the executable?",
		30);
	      DEFINE_STRING(BGl_string1399z00zzengine_paramz00,
		BgL_bgl_string1399za700za7za7e1839za7, "BIGLOOLIB", 9);
	extern obj_t BGl_stringzd2splitzd2charzd2envzd2zztools_miscz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1390z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1840za7,
		BGl_z62zc3z04anonymousza31085ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1560z00zzengine_paramz00,
		BgL_bgl_string1560za700za7za7e1841za7, "Inlining optimization", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1392z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1842za7,
		BGl_z62zc3z04anonymousza31088ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1561z00zzengine_paramz00,
		BgL_bgl_string1561za700za7za7e1843za7, "User inlining optimization", 26);
	      DEFINE_STRING(BGl_string1480z00zzengine_paramz00,
		BgL_bgl_string1480za700za7za7e1844za7,
		"Shall we interprete the source file?", 36);
	      DEFINE_STRING(BGl_string1481z00zzengine_paramz00,
		BgL_bgl_string1481za700za7za7e1845za7, "A startup file for the interpreter",
		34);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1394z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1846za7,
		BGl_z62zc3z04anonymousza31091ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1563z00zzengine_paramz00,
		BgL_bgl_string1563za700za7za7e1847za7, "Inlining growth factor", 22);
	      DEFINE_STRING(BGl_string1482z00zzengine_paramz00,
		BgL_bgl_string1482za700za7za7e1848za7, "Shall we enable call/cc?", 24);
	      DEFINE_STRING(BGl_string1483z00zzengine_paramz00,
		BgL_bgl_string1483za700za7za7e1849za7,
		"Enable automatically a main generation when linking", 51);
	      DEFINE_STRING(BGl_string1565z00zzengine_paramz00,
		BgL_bgl_string1565za700za7za7e1850za7, "Inlinine growth factor reductor",
		31);
	      DEFINE_STRING(BGl_string1484z00zzengine_paramz00,
		BgL_bgl_string1484za700za7za7e1851za7, "Stop after the pass", 19);
	      DEFINE_STRING(BGl_string1566z00zzengine_paramz00,
		BgL_bgl_string1566za700za7za7e1852za7, "Optimize set-exit used as return",
		32);
	      DEFINE_STRING(BGl_string1485z00zzengine_paramz00,
		BgL_bgl_string1485za700za7za7e1853za7,
		"Enable/disable a JAR file production for the JVM back-end", 57);
	      DEFINE_STRING(BGl_string1567z00zzengine_paramz00,
		BgL_bgl_string1567za700za7za7e1854za7,
		"Optimize set-exit by enabling local return", 42);
	      DEFINE_STRING(BGl_string1486z00zzengine_paramz00,
		BgL_bgl_string1486za700za7za7e1855za7,
		"Shell to be used when producing JVM run scripts", 47);
	      DEFINE_STRING(BGl_string1568z00zzengine_paramz00,
		BgL_bgl_string1568za700za7za7e1856za7, "Optimize tagged fixnum operations",
		33);
	      DEFINE_STRING(BGl_string1569z00zzengine_paramz00,
		BgL_bgl_string1569za700za7za7e1857za7,
		"Optimize specialize flonum operations", 37);
	      DEFINE_STRING(BGl_string1488z00zzengine_paramz00,
		BgL_bgl_string1488za700za7za7e1858za7,
		"JVM to be used to run Java programs", 35);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1562z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704za7a2inli1859z00,
		BGl_z62zc3z04za2inliningzd2kfactorza21202ze3z94zzengine_paramz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1570z00zzengine_paramz00,
		BgL_bgl_string1570za700za7za7e1860za7, "Optimize stackable allocation", 29);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1564z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704za7a2inli1861z00,
		BGl_z62zc3z04za2inliningzd2reducezd2k1203ze3ze4zzengine_paramz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1571z00zzengine_paramz00,
		BgL_bgl_string1571za700za7za7e1862za7, "Remove useless cells", 20);
	      DEFINE_STRING(BGl_string1490z00zzengine_paramz00,
		BgL_bgl_string1490za700za7za7e1863za7, "", 0);
	      DEFINE_STRING(BGl_string1572z00zzengine_paramz00,
		BgL_bgl_string1572za700za7za7e1864za7, "Extend entry", 12);
	      DEFINE_STRING(BGl_string1491z00zzengine_paramz00,
		BgL_bgl_string1491za700za7za7e1865za7, "JVM options", 11);
	      DEFINE_STRING(BGl_string1573z00zzengine_paramz00,
		BgL_bgl_string1573za700za7za7e1866za7, "Scheme legal suffixes", 21);
	      DEFINE_STRING(BGl_string1492z00zzengine_paramz00,
		BgL_bgl_string1492za700za7za7e1867za7, "JVM Bigloo classpath", 20);
	      DEFINE_STRING(BGl_string1574z00zzengine_paramz00,
		BgL_bgl_string1574za700za7za7e1868za7, "C legal suffixes", 16);
	      DEFINE_STRING(BGl_string1493z00zzengine_paramz00,
		BgL_bgl_string1493za700za7za7e1869za7, "JVM classpath", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1487z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1870za7,
		BGl_z62zc3z04anonymousza31197ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1575z00zzengine_paramz00,
		BgL_bgl_string1575za700za7za7e1871za7, "C# legal suffixes", 17);
	      DEFINE_STRING(BGl_string1494z00zzengine_paramz00,
		BgL_bgl_string1494za700za7za7e1872za7, "JVM main class", 14);
	      DEFINE_STRING(BGl_string1576z00zzengine_paramz00,
		BgL_bgl_string1576za700za7za7e1873za7, "Object legal suffixes", 21);
	      DEFINE_STRING(BGl_string1495z00zzengine_paramz00,
		BgL_bgl_string1495za700za7za7e1874za7, "JVM jarpath", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1489z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1875za7,
		BGl_z62zc3z04anonymousza31199ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1496z00zzengine_paramz00,
		BgL_bgl_string1496za700za7za7e1876za7, "JVM object directory", 20);
	      DEFINE_STRING(BGl_string1578z00zzengine_paramz00,
		BgL_bgl_string1578za700za7za7e1877za7,
		"Module checksum object legal suffixes", 37);
	      DEFINE_STRING(BGl_string1497z00zzengine_paramz00,
		BgL_bgl_string1497za700za7za7e1878za7, "Catch internal errors", 21);
	      DEFINE_STRING(BGl_string1579z00zzengine_paramz00,
		BgL_bgl_string1579za700za7za7e1879za7, "Module checksum C include path",
		30);
	      DEFINE_STRING(BGl_string1498z00zzengine_paramz00,
		BgL_bgl_string1498za700za7za7e1880za7,
		"Enable JVM class constructors to initiliaze bigloo modules", 58);
	      DEFINE_STRING(BGl_string1499z00zzengine_paramz00,
		BgL_bgl_string1499za700za7za7e1881za7,
		"Produce a module checksum object (.mco)", 39);
	      DEFINE_STRING(BGl_string1580z00zzengine_paramz00,
		BgL_bgl_string1580za700za7za7e1882za7, "auto-mode (extend mode) list", 28);
	      DEFINE_STRING(BGl_string1581z00zzengine_paramz00,
		BgL_bgl_string1581za700za7za7e1883za7, "Case sensitivity", 16);
	      DEFINE_STRING(BGl_string1582z00zzengine_paramz00,
		BgL_bgl_string1582za700za7za7e1884za7,
		"Heap size (in MegaByte) or #f for default value", 47);
	      DEFINE_STRING(BGl_string1583z00zzengine_paramz00,
		BgL_bgl_string1583za700za7za7e1885za7,
		"The way the reader reads input file ('plain or 'intern)", 55);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1577z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1886za7,
		BGl_z62zc3z04anonymousza31216ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1584z00zzengine_paramz00,
		BgL_bgl_string1584za700za7za7e1887za7,
		"The target language (either c, c-saw, jvm, or .net)", 51);
	      DEFINE_STRING(BGl_string1586z00zzengine_paramz00,
		BgL_bgl_string1586za700za7za7e1888za7, "Do we go to the saw-mill?", 25);
	      DEFINE_STRING(BGl_string1587z00zzengine_paramz00,
		BgL_bgl_string1587za700za7za7e1889za7,
		"Enable/disable saw register re-allocation", 41);
	      DEFINE_STRING(BGl_string1588z00zzengine_paramz00,
		BgL_bgl_string1588za700za7za7e1890za7,
		"Enable/disable saw register allocation", 38);
	      DEFINE_STRING(BGl_string1589z00zzengine_paramz00,
		BgL_bgl_string1589za700za7za7e1891za7,
		"Enable/disable saw register allocation on expression", 52);
	      DEFINE_STRING(BGl_string1590z00zzengine_paramz00,
		BgL_bgl_string1590za700za7za7e1892za7,
		"Max function size for optimizing the register allocation", 56);
	      DEFINE_STRING(BGl_string1591z00zzengine_paramz00,
		BgL_bgl_string1591za700za7za7e1893za7,
		"The list of functions allowing register allocation", 50);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1585z00zzengine_paramz00,
		BgL_bgl_za762za7c3za704anonymo1894za7,
		BGl_z62zc3z04anonymousza31227ze3ze5zzengine_paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1592z00zzengine_paramz00,
		BgL_bgl_string1592za700za7za7e1895za7,
		"The list of functions disabling register allocation", 51);
	      DEFINE_STRING(BGl_string1593z00zzengine_paramz00,
		BgL_bgl_string1593za700za7za7e1896za7, "Enable saw spill optimization", 29);
	      DEFINE_STRING(BGl_string1594z00zzengine_paramz00,
		BgL_bgl_string1594za700za7za7e1897za7,
		"Enable/disable saw basic-blocks versionning", 43);
	      DEFINE_STRING(BGl_string1595z00zzengine_paramz00,
		BgL_bgl_string1595za700za7za7e1898za7,
		"Optional white list of bbv functions", 36);
	      DEFINE_STRING(BGl_string1596z00zzengine_paramz00,
		BgL_bgl_string1596za700za7za7e1899za7,
		"Do we apply the self-global-tail-call stage?", 44);
	      DEFINE_STRING(BGl_string1597z00zzengine_paramz00,
		BgL_bgl_string1597za700za7za7e1900za7,
		"A user variable to store dynamic command line options", 53);
	      DEFINE_STRING(BGl_string1598z00zzengine_paramz00,
		BgL_bgl_string1598za700za7za7e1901za7, "If true, allow type redefinitions",
		33);
	      DEFINE_STRING(BGl_string1599z00zzengine_paramz00,
		BgL_bgl_string1599za700za7za7e1902za7,
		"If false, set-ex-it nodes force the creation of a globalized function",
		69);
	extern obj_t BGl_epairifyza2zd2envz70zztools_miscz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_appendzd221011zd2envz00zzengine_paramz00,
		BgL_bgl_za762appendza7d221011903z00,
		BGl_z62appendzd221011zb0zzengine_paramz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_epairifyzd2reczd2envz00zztools_miscz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_addzd2updaterz12zd2envz12zzengine_paramz00,
		BgL_bgl_za762addza7d2updater1904z00,
		BGl_z62addzd2updaterz12za2zzengine_paramz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_addzd2doczd2variablez12zd2envzc0zzengine_paramz00,
		BgL_bgl_za762addza7d2docza7d2v1905za7,
		BGl_z62addzd2doczd2variablez12z70zzengine_paramz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2variableszd2usagezd2envzd2zzengine_paramz00,
		BgL_bgl_za762biglooza7d2vari1906z00,
		BGl_z62bigloozd2variableszd2usagez62zzengine_paramz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_epairifyzd2propagatezd2loczd2envzd2zztools_miscz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reinitializa7ezd2bigloozd2variablesz12zd2envz67zzengine_paramz00,
		BgL_bgl_za762reinitializa7a71907z00,
		BGl_z62reinitializa7ezd2bigloozd2variablesz12zd7zzengine_paramz00, 0L,
		BUNSPEC, 0);
	extern obj_t BGl_epairifyzd2propagatezd2envz00zztools_miscz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2heapzd2jvmzd2nameza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2jvmzd2inliningza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2additionalzd2tracesza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2synczd2profilingza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2optimzd2cfazd2pairzd2quotezd2maxzd2lengthza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2libzd2srczd2dirza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2cfazd2pairzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2optimzd2unrollzd2loopzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2unsafezd2arityza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2javaza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2heapzd2dumpzd2namesza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2oldzd2loadzd2pathza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2jvmzd2bigloozd2classpathza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2optimza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2destza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2helloza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2optimzd2integratezf3za2z21zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2profzd2tablezd2nameza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2optimzd2specializa7ezd2flonumzf3za2z54zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2cczd2styleza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2autozd2linkzd2mainza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2accesszd2shapezf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2compilerzd2stackzd2debugzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2emailza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2stripza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2sawzd2registerzd2allocationzd2functionsza2zd2zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2builtinzd2allocatorsza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2sawzd2spillza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2cczd2optionsza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2czd2filesza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2srczd2filesza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2doublezd2ldzd2libszf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2warningzd2overridenzd2variablesza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2targetzd2languageza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2ldzd2relativeza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2optimzd2stackablezf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2czd2splitzd2stringza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2taggedzd2fxopzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2gczd2customzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2czd2userzd2footza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2passza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2tmpza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2csharpzd2suffixza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2tmpzd2destza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2optimzd2isazf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2additionalzd2bigloozd2za7ipsza2za7zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2optimzd2uncellzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2restzd2argsza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2optimzd2cfazd2fixnumzd2arithmeticzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2typezd2shapezf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2optimzd2cfazf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2jvmzd2peepholeza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2maxzd2czd2foreignzd2arityza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2ldzd2optionsza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2optimzd2returnzf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2locationzd2shapezf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2jarpathza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2shellza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2ldzd2libraryzd2dirza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2optimzd2jvmzd2branchza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2keyzd2shapezf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2sawza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2autozd2modeza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2bigloozd2librarieszd2czd2setupza2zd2zzengine_paramz00));
		     ADD_ROOT((void
				*)
			(&BGl_za2optimzd2cfazd2flonumzd2arithmeticzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2czd2debugzd2lineszd2infoza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2debugzd2moduleza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2initzd2modeza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2libzd2modeza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2startupzd2fileza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2userzd2inliningzf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2bigloozd2cmdzd2nameza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2warningzd2overridenzd2slotsza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2warningzd2typesza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2jvmzd2foreignzd2classzd2idza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2modulezd2shapezf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2optimzd2Ozd2macrozf3za2zf3zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2modulezd2checksumzd2objectzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2ccza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2shellza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2optimzd2jvmzd2constructorzd2inliningza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2classpathza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2errorzd2localiza7ationza2z75zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2jvmzd2pathzd2separatorza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2cczd2ozd2optionza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2inliningzd2kfactorza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bdbzd2debugza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2urlza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2heapzd2libraryza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2jvmzd2cinitzd2moduleza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2bigloozd2licensingzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2compilerzd2sharingzd2debugzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2inliningzf3za2zf3zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2ldzd2debugzd2optionza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2gczd2libza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2compilerzd2debugza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2heapzd2wasmzd2nameza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2ldzd2optimzd2flagsza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2paramzd2updatersza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2alloczd2shapezf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2staticzd2bigloozf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2cflagsza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2tracezd2levelza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2unsafezd2structza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2ldzd2postzd2optionsza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2cflagszd2optimza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2ldzd2styleza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2versionza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2sawzd2bbvzf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2accesszd2filezd2defaultza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2mainclassza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2verboseza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2sawzd2bbvzd2functionsza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2unsafezd2rangeza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2catchza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2includezd2multipleza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2optimzd2initflowzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2variablesza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2czd2suffixza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2multizd2threadedzd2gczf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2optimzd2jvmzd2fasteqza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2evalzd2optionsza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2sawzd2registerzd2reallocationzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2nameza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2heapzd2nameza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2profilezd2libraryza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2dlopenzd2initzd2gcza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2tracezd2nameza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2mcozd2suffixza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2debugza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2profilezd2modeza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2sawzd2registerzd2allocationzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2tracezd2writezd2lengthza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2argsza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2withzd2filesza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2warningzd2defaultzd2slotzd2valueza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2libzd2dirza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2dataflowzd2forzd2errorszf3za2z21zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2userzd2heapzd2siza7eza2za7zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2cflagszd2profza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2inliningzd2reducezd2kfactorza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2jarzf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2synczd2failsafezf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2allowzd2typezd2redefinitionza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2libza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2unsafezd2libraryza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2bigloozd2userzd2libza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2unsafezd2heapza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2unsafezd2typeza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2prezd2processorza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2loopzd2inliningzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2gczd2forcezd2registerzd2rootszf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2objzd2suffixza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2sawzd2nozd2registerzd2allocationzd2functionsza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2extendzd2entryza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2optimzd2dataflowzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2srczd2suffixza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2cczd2moveza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2ozd2filesza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bmemzd2profilingza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2cflagszd2rpathza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2authorza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2namezd2shapezf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2abortzf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2typenamezd2shapezf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2indentza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2arithmeticzd2newzd2overflowza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2optimzd2jvmza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2directoryza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2interpreterza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2dateza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2dataflowzd2typeszf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2earlyzd2withzd2modulesza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2globalzd2tailzd2callzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2optimzd2cfazd2applyzd2trackingzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2unsafezd2evalza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2userzd2shapezf3za2z21zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2compilerzd2typezd2debugzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2qualifiedzd2typezd2filezd2defaultza2zd2zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2arithmeticzd2overflowza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2callzf2cczf3za2z01zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2qualifiedzd2typezd2fileza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2ldzd2ozd2optionza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2czd2debugzd2optionza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2sawzd2registerzd2allocationzd2onexpressionzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void
				*)
			(&BGl_za2sawzd2registerzd2allocationzd2maxzd2siza7eza2za7zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2stdcza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2jvmzd2foreignzd2classzd2nameza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2optionsza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2atomzd2inliningzf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2additionalzd2heapzd2nameza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2defaultzd2libzd2dirza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*)
			(&BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2readerza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2symbolzd2caseza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2czd2tailzd2callza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2envza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2dlopenzd2initza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2czd2userzd2headerza2z00zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2mcozd2includezd2pathza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2accesszd2filesza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2optimzd2cfazd2forcezd2loosezd2localzd2functionzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2czd2debugza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2includezd2foreignza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2garbagezd2collectorza2zd2zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2purifyza2z00zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2localzd2exitzf3za2z21zzengine_paramz00));
		     ADD_ROOT((void *) (&BGl_za2unsafezd2versionza2zd2zzengine_paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2optimzd2reducezd2betazf3za2zf3zzengine_paramz00));
		     ADD_ROOT((void
				*) (&BGl_za2warningzd2typezd2errorza2z00zzengine_paramz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long
		BgL_checksumz00_911, char *BgL_fromz00_912)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzengine_paramz00))
				{
					BGl_requirezd2initializa7ationz75zzengine_paramz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzengine_paramz00();
					BGl_libraryzd2moduleszd2initz00zzengine_paramz00();
					BGl_cnstzd2initzd2zzengine_paramz00();
					BGl_importedzd2moduleszd2initz00zzengine_paramz00();
					BGl_evalzd2initzd2zzengine_paramz00();
					return BGl_toplevelzd2initzd2zzengine_paramz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"engine_param");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "engine_param");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "engine_param");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"engine_param");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "engine_param");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "engine_param");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"engine_param");
			BGl_modulezd2initializa7ationz75zz__evenvz00(0L, "engine_param");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "engine_param");
			BGl_modulezd2initializa7ationz75zz__configurez00(0L, "engine_param");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"engine_param");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "engine_param");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 15 */
			{	/* Engine/param.scm 15 */
				obj_t BgL_cportz00_874;

				{	/* Engine/param.scm 15 */
					obj_t BgL_stringz00_881;

					BgL_stringz00_881 = BGl_string1619z00zzengine_paramz00;
					{	/* Engine/param.scm 15 */
						obj_t BgL_startz00_882;

						BgL_startz00_882 = BINT(0L);
						{	/* Engine/param.scm 15 */
							obj_t BgL_endz00_883;

							BgL_endz00_883 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_881)));
							{	/* Engine/param.scm 15 */

								BgL_cportz00_874 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_881, BgL_startz00_882, BgL_endz00_883);
				}}}}
				{
					long BgL_iz00_875;

					BgL_iz00_875 = 288L;
				BgL_loopz00_876:
					if ((BgL_iz00_875 == -1L))
						{	/* Engine/param.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Engine/param.scm 15 */
							{	/* Engine/param.scm 15 */
								obj_t BgL_arg1625z00_877;

								{	/* Engine/param.scm 15 */

									{	/* Engine/param.scm 15 */
										obj_t BgL_locationz00_879;

										BgL_locationz00_879 = BBOOL(((bool_t) 0));
										{	/* Engine/param.scm 15 */

											BgL_arg1625z00_877 =
												BGl_readz00zz__readerz00(BgL_cportz00_874,
												BgL_locationz00_879);
										}
									}
								}
								{	/* Engine/param.scm 15 */
									int BgL_tmpz00_943;

									BgL_tmpz00_943 = (int) (BgL_iz00_875);
									CNST_TABLE_SET(BgL_tmpz00_943, BgL_arg1625z00_877);
							}}
							{	/* Engine/param.scm 15 */
								int BgL_auxz00_880;

								BgL_auxz00_880 = (int) ((BgL_iz00_875 - 1L));
								{
									long BgL_iz00_948;

									BgL_iz00_948 = (long) (BgL_auxz00_880);
									BgL_iz00_875 = BgL_iz00_948;
									goto BgL_loopz00_876;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 15 */
			BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 = BNIL;
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 = BNIL;
			BGl_za2bigloozd2versionza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
			{	/* Engine/param.scm 323 */
				obj_t BgL_idz00_250;

				BgL_idz00_250 = CNST_TABLE_REF(1);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_251;

					BgL_arg1233z00_251 =
						MAKE_YOUNG_PAIR(BgL_idz00_250, BGl_string1343z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_251,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1344z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(2));
			{	/* Engine/param.scm 327 */
				obj_t BgL_idz00_252;

				BgL_idz00_252 = CNST_TABLE_REF(3);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_253;

					BgL_arg1233z00_253 =
						MAKE_YOUNG_PAIR(BgL_idz00_252, BGl_string1345z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_253,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1346z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			{	/* Engine/param.scm 333 */
				obj_t BgL_list1032z00_13;

				{	/* Engine/param.scm 333 */
					obj_t BgL_arg1033z00_14;

					{	/* Engine/param.scm 333 */
						obj_t BgL_arg1035z00_15;

						{	/* Engine/param.scm 333 */
							obj_t BgL_arg1036z00_16;

							{	/* Engine/param.scm 333 */
								obj_t BgL_arg1037z00_17;

								BgL_arg1037z00_17 =
									MAKE_YOUNG_PAIR(BGl_string1347z00zzengine_paramz00, BNIL);
								BgL_arg1036z00_16 =
									MAKE_YOUNG_PAIR
									(BGl_za2bigloozd2versionza2zd2zzengine_paramz00,
									BgL_arg1037z00_17);
							}
							BgL_arg1035z00_15 =
								MAKE_YOUNG_PAIR(BGl_string1348z00zzengine_paramz00,
								BgL_arg1036z00_16);
						}
						BgL_arg1033z00_14 =
							MAKE_YOUNG_PAIR
							(BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00,
							BgL_arg1035z00_15);
					}
					BgL_list1032z00_13 =
						MAKE_YOUNG_PAIR(BGl_string1349z00zzengine_paramz00,
						BgL_arg1033z00_14);
				}
				BGl_za2bigloozd2nameza2zd2zzengine_paramz00 =
					BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1032z00_13);
			}
			{	/* Engine/param.scm 331 */
				obj_t BgL_idz00_254;

				BgL_idz00_254 = CNST_TABLE_REF(4);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_255;

					BgL_arg1233z00_255 =
						MAKE_YOUNG_PAIR(BgL_idz00_254, BGl_string1350z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_255,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1351z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2bigloozd2cmdzd2nameza2z00zzengine_paramz00 = CNST_TABLE_REF(5);
			BGl_za2bigloozd2argsza2zd2zzengine_paramz00 = CNST_TABLE_REF(5);
			BGl_za2restzd2argsza2zd2zzengine_paramz00 = BNIL;
			BGl_za2bigloozd2authorza2zd2zzengine_paramz00 =
				BGl_string1352z00zzengine_paramz00;
			BGl_za2bigloozd2emailza2zd2zzengine_paramz00 =
				BGl_string1353z00zzengine_paramz00;
			BGl_za2bigloozd2urlza2zd2zzengine_paramz00 =
				BGl_string1354z00zzengine_paramz00;
			BGl_za2bigloozd2dateza2zd2zzengine_paramz00 =
				BGl_bigloozd2datezd2zztools_datez00();
			{	/* Engine/param.scm 348 */
				obj_t BgL_tmpz00_26;

				BgL_tmpz00_26 =
					BGl_getenvz00zz__osz00(BGl_string1355z00zzengine_paramz00);
				if (STRINGP(BgL_tmpz00_26))
					{	/* Engine/param.scm 349 */
						BGl_za2bigloozd2tmpza2zd2zzengine_paramz00 = BgL_tmpz00_26;
					}
				else
					{	/* Engine/param.scm 349 */
						BGl_za2bigloozd2tmpza2zd2zzengine_paramz00 =
							string_to_bstring(OS_TMP);
					}
			}
			{	/* Engine/param.scm 346 */
				obj_t BgL_idz00_256;

				BgL_idz00_256 = CNST_TABLE_REF(6);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_257;

					BgL_arg1233z00_257 =
						MAKE_YOUNG_PAIR(BgL_idz00_256, BGl_string1356z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_257,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1357z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2bigloozd2licensingzf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 353 */
				obj_t BgL_idz00_258;

				BgL_idz00_258 = CNST_TABLE_REF(7);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_259;

					BgL_arg1233z00_259 =
						MAKE_YOUNG_PAIR(BgL_idz00_258, BGl_string1358z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_259,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2verboseza2z00zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 360 */
				obj_t BgL_idz00_260;

				BgL_idz00_260 = CNST_TABLE_REF(8);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_261;

					BgL_arg1233z00_261 =
						MAKE_YOUNG_PAIR(BgL_idz00_260, BGl_string1359z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_261,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2helloza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 364 */
				obj_t BgL_idz00_262;

				BgL_idz00_262 = CNST_TABLE_REF(9);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_263;

					BgL_arg1233z00_263 =
						MAKE_YOUNG_PAIR(BgL_idz00_262, BGl_string1360z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_263,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2srczd2filesza2zd2zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 372 */
				obj_t BgL_idz00_264;

				BgL_idz00_264 = CNST_TABLE_REF(10);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_265;

					BgL_arg1233z00_265 =
						MAKE_YOUNG_PAIR(BgL_idz00_264, BGl_string1361z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_265,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2tmpzd2destza2zd2zzengine_paramz00 = BFALSE;
			BGl_za2destza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 377 */
				obj_t BgL_idz00_266;

				BgL_idz00_266 = CNST_TABLE_REF(11);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_267;

					BgL_arg1233z00_267 =
						MAKE_YOUNG_PAIR(BgL_idz00_266, BGl_string1362z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_267,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2shellza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(12));
			{	/* Engine/param.scm 385 */
				obj_t BgL_idz00_268;

				BgL_idz00_268 = CNST_TABLE_REF(13);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_269;

					BgL_arg1233z00_269 =
						MAKE_YOUNG_PAIR(BgL_idz00_268, BGl_string1363z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_269,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1364z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2cczd2styleza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(14));
			{	/* Engine/param.scm 389 */
				obj_t BgL_idz00_270;

				BgL_idz00_270 = CNST_TABLE_REF(15);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_271;

					BgL_arg1233z00_271 =
						MAKE_YOUNG_PAIR(BgL_idz00_270, BGl_string1365z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_271,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1366z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2ccza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(16));
			{	/* Engine/param.scm 393 */
				obj_t BgL_idz00_272;

				BgL_idz00_272 = CNST_TABLE_REF(17);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_273;

					BgL_arg1233z00_273 =
						MAKE_YOUNG_PAIR(BgL_idz00_272, BGl_string1367z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_273,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1368z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2cflagsza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(18));
			{	/* Engine/param.scm 397 */
				obj_t BgL_idz00_274;

				BgL_idz00_274 = CNST_TABLE_REF(19);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_275;

					BgL_arg1233z00_275 =
						MAKE_YOUNG_PAIR(BgL_idz00_274, BGl_string1369z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_275,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1370z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2cflagszd2optimza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(20));
			{	/* Engine/param.scm 401 */
				obj_t BgL_idz00_276;

				BgL_idz00_276 = CNST_TABLE_REF(21);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_277;

					BgL_arg1233z00_277 =
						MAKE_YOUNG_PAIR(BgL_idz00_276, BGl_string1371z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_277,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1372z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2cflagszd2profza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(22));
			{	/* Engine/param.scm 405 */
				obj_t BgL_idz00_278;

				BgL_idz00_278 = CNST_TABLE_REF(23);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_279;

					BgL_arg1233z00_279 =
						MAKE_YOUNG_PAIR(BgL_idz00_278, BGl_string1373z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_279,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1374z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			{	/* Engine/param.scm 411 */
				obj_t BgL_arg1062z00_51;

				BgL_arg1062z00_51 =
					BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(24));
				{	/* Engine/param.scm 411 */
					obj_t BgL_list1063z00_52;

					BgL_list1063z00_52 = MAKE_YOUNG_PAIR(BgL_arg1062z00_51, BNIL);
					BGl_za2cflagszd2rpathza2zd2zzengine_paramz00 = BgL_list1063z00_52;
				}
			}
			{	/* Engine/param.scm 409 */
				obj_t BgL_idz00_281;

				BgL_idz00_281 = CNST_TABLE_REF(25);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_282;

					BgL_arg1233z00_282 =
						MAKE_YOUNG_PAIR(BgL_idz00_281, BGl_string1375z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_282,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1376z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2cczd2ozd2optionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(26));
			{	/* Engine/param.scm 413 */
				obj_t BgL_idz00_284;

				BgL_idz00_284 = CNST_TABLE_REF(27);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_285;

					BgL_arg1233z00_285 =
						MAKE_YOUNG_PAIR(BgL_idz00_284, BGl_string1377z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_285,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1378z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(28));
			{	/* Engine/param.scm 417 */
				obj_t BgL_idz00_286;

				BgL_idz00_286 = CNST_TABLE_REF(29);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_287;

					BgL_arg1233z00_287 =
						MAKE_YOUNG_PAIR(BgL_idz00_286, BGl_string1379z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_287,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1380z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2stdcza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(30));
			{	/* Engine/param.scm 421 */
				obj_t BgL_idz00_288;

				BgL_idz00_288 = CNST_TABLE_REF(31);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_289;

					BgL_arg1233z00_289 =
						MAKE_YOUNG_PAIR(BgL_idz00_288, BGl_string1381z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_289,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1382z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2cczd2optionsza2zd2zzengine_paramz00 = CNST_TABLE_REF(32);
			{	/* Engine/param.scm 425 */
				obj_t BgL_idz00_290;

				BgL_idz00_290 = CNST_TABLE_REF(33);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_291;

					BgL_arg1233z00_291 =
						MAKE_YOUNG_PAIR(BgL_idz00_290, BGl_string1383z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_291,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 429 */
				obj_t BgL_idz00_292;

				BgL_idz00_292 = CNST_TABLE_REF(34);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_293;

					BgL_arg1233z00_293 =
						MAKE_YOUNG_PAIR(BgL_idz00_292, BGl_string1384z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_293,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2ldzd2styleza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(35));
			{	/* Engine/param.scm 433 */
				obj_t BgL_idz00_294;

				BgL_idz00_294 = CNST_TABLE_REF(36);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_295;

					BgL_arg1233z00_295 =
						MAKE_YOUNG_PAIR(BgL_idz00_294, BGl_string1385z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_295,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1386z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2ldzd2optionsza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(37));
			{	/* Engine/param.scm 437 */
				obj_t BgL_idz00_296;

				BgL_idz00_296 = CNST_TABLE_REF(38);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_297;

					BgL_arg1233z00_297 =
						MAKE_YOUNG_PAIR(BgL_idz00_296, BGl_string1387z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_297,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1388z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2ldzd2ozd2optionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(39));
			{	/* Engine/param.scm 441 */
				obj_t BgL_idz00_298;

				BgL_idz00_298 = CNST_TABLE_REF(40);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_299;

					BgL_arg1233z00_299 =
						MAKE_YOUNG_PAIR(BgL_idz00_298, BGl_string1389z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_299,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1390z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2ldzd2debugzd2optionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(41));
			{	/* Engine/param.scm 445 */
				obj_t BgL_idz00_300;

				BgL_idz00_300 = CNST_TABLE_REF(42);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_301;

					BgL_arg1233z00_301 =
						MAKE_YOUNG_PAIR(BgL_idz00_300, BGl_string1391z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_301,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1392z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2ldzd2optimzd2flagsza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(43));
			{	/* Engine/param.scm 449 */
				obj_t BgL_idz00_302;

				BgL_idz00_302 = CNST_TABLE_REF(44);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_303;

					BgL_arg1233z00_303 =
						MAKE_YOUNG_PAIR(BgL_idz00_302, BGl_string1393z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_303,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1394z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2ldzd2postzd2optionsza2z00zzengine_paramz00 = CNST_TABLE_REF(32);
			{	/* Engine/param.scm 453 */
				obj_t BgL_idz00_304;

				BgL_idz00_304 = CNST_TABLE_REF(45);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_305;

					BgL_arg1233z00_305 =
						MAKE_YOUNG_PAIR(BgL_idz00_304, BGl_string1395z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_305,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2cczd2moveza2zd2zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 457 */
				obj_t BgL_idz00_306;

				BgL_idz00_306 = CNST_TABLE_REF(46);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_307;

					BgL_arg1233z00_307 =
						MAKE_YOUNG_PAIR(BgL_idz00_306, BGl_string1396z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_307,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2ldzd2relativeza2zd2zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 461 */
				obj_t BgL_idz00_308;

				BgL_idz00_308 = CNST_TABLE_REF(47);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_309;

					BgL_arg1233z00_309 =
						MAKE_YOUNG_PAIR(BgL_idz00_308, BGl_string1397z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_309,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2stripza2z00zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 465 */
				obj_t BgL_idz00_310;

				BgL_idz00_310 = CNST_TABLE_REF(48);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_311;

					BgL_arg1233z00_311 =
						MAKE_YOUNG_PAIR(BgL_idz00_310, BGl_string1398z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_311,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			{	/* Engine/param.scm 471 */
				obj_t BgL_libzd2envzd2_82;

				BgL_libzd2envzd2_82 =
					BGl_buildzd2pathzd2fromzd2shellzd2variablez00zztools_miscz00
					(BGl_string1399z00zzengine_paramz00);
				if (NULLP(BgL_libzd2envzd2_82))
					{	/* Engine/param.scm 473 */
						obj_t BgL_arg1097z00_84;

						BgL_arg1097z00_84 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(24));
						{	/* Engine/param.scm 473 */
							obj_t BgL_list1098z00_85;

							{	/* Engine/param.scm 473 */
								obj_t BgL_arg1102z00_86;

								BgL_arg1102z00_86 = MAKE_YOUNG_PAIR(BgL_arg1097z00_84, BNIL);
								BgL_list1098z00_85 =
									MAKE_YOUNG_PAIR(BGl_string1400z00zzengine_paramz00,
									BgL_arg1102z00_86);
							}
							BGl_za2libzd2dirza2zd2zzengine_paramz00 = BgL_list1098z00_85;
						}
					}
				else
					{	/* Engine/param.scm 472 */
						BGl_za2libzd2dirza2zd2zzengine_paramz00 =
							MAKE_YOUNG_PAIR(BGl_string1400z00zzengine_paramz00,
							BgL_libzd2envzd2_82);
					}
			}
			{	/* Engine/param.scm 469 */
				obj_t BgL_idz00_313;

				BgL_idz00_313 = CNST_TABLE_REF(49);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_314;

					BgL_arg1233z00_314 =
						MAKE_YOUNG_PAIR(BgL_idz00_313, BGl_string1401z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_314,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1402z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2defaultzd2libzd2dirza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(24));
			{	/* Engine/param.scm 475 */
				obj_t BgL_idz00_316;

				BgL_idz00_316 = CNST_TABLE_REF(50);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_317;

					BgL_arg1233z00_317 =
						MAKE_YOUNG_PAIR(BgL_idz00_316, BGl_string1403z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_317,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1404z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2ldzd2libraryzd2dirza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(24));
			{	/* Engine/param.scm 478 */
				obj_t BgL_idz00_318;

				BgL_idz00_318 = CNST_TABLE_REF(51);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_319;

					BgL_arg1233z00_319 =
						MAKE_YOUNG_PAIR(BgL_idz00_318, BGl_string1403z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_319,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1405z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2libzd2srczd2dirza2z00zzengine_paramz00 =
				BGl_makezd2filezd2namez00zz__osz00(CAR
				(BGl_za2libzd2dirza2zd2zzengine_paramz00),
				BGl_string1406z00zzengine_paramz00);
			{	/* Engine/param.scm 482 */
				obj_t BgL_idz00_321;

				BgL_idz00_321 = CNST_TABLE_REF(52);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_322;

					BgL_arg1233z00_322 =
						MAKE_YOUNG_PAIR(BgL_idz00_321, BGl_string1401z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_322,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1407z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2bigloozd2libza2zd2zzengine_paramz00 = CNST_TABLE_REF(53);
			{	/* Engine/param.scm 486 */
				obj_t BgL_idz00_324;

				BgL_idz00_324 = CNST_TABLE_REF(54);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_325;

					BgL_arg1233z00_325 =
						MAKE_YOUNG_PAIR(BgL_idz00_324, BGl_string1408z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_325,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			{	/* Engine/param.scm 492 */
				bool_t BgL_test1912z00_1145;

				{	/* Engine/param.scm 492 */
					obj_t BgL_tmpz00_1146;

					BgL_tmpz00_1146 =
						BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(55));
					BgL_test1912z00_1145 = STRINGP(BgL_tmpz00_1146);
				}
				if (BgL_test1912z00_1145)
					{	/* Engine/param.scm 493 */
						obj_t BgL_arg1137z00_108;

						BgL_arg1137z00_108 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(55));
						BGl_za2gczd2libza2zd2zzengine_paramz00 =
							bstring_to_symbol(((obj_t) BgL_arg1137z00_108));
					}
				else
					{	/* Engine/param.scm 494 */
						obj_t BgL_arg1138z00_109;

						BgL_arg1138z00_109 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(56));
						BGl_za2gczd2libza2zd2zzengine_paramz00 =
							bstring_to_symbol(((obj_t) BgL_arg1138z00_109));
					}
			}
			{	/* Engine/param.scm 490 */
				obj_t BgL_idz00_328;

				BgL_idz00_328 = CNST_TABLE_REF(57);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_329;

					BgL_arg1233z00_329 =
						MAKE_YOUNG_PAIR(BgL_idz00_328, BGl_string1409z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_329,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1410z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2gczd2customzf3za2z21zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(55));
			{	/* Engine/param.scm 496 */
				obj_t BgL_idz00_332;

				BgL_idz00_332 = CNST_TABLE_REF(58);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_333;

					BgL_arg1233z00_333 =
						MAKE_YOUNG_PAIR(BgL_idz00_332, BGl_string1411z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_333,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1412z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2multizd2threadedzd2gczf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 500 */
				obj_t BgL_idz00_334;

				BgL_idz00_334 = CNST_TABLE_REF(59);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_335;

					BgL_arg1233z00_335 =
						MAKE_YOUNG_PAIR(BgL_idz00_334, BGl_string1413z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_335,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2gczd2forcezd2registerzd2rootszf3za2z21zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 504 */
				obj_t BgL_idz00_336;

				BgL_idz00_336 = CNST_TABLE_REF(60);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_337;

					BgL_arg1233z00_337 =
						MAKE_YOUNG_PAIR(BgL_idz00_336, BGl_string1414z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_337,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2bigloozd2abortzf3za2z21zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(61));
			{	/* Engine/param.scm 508 */
				obj_t BgL_idz00_338;

				BgL_idz00_338 = CNST_TABLE_REF(62);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_339;

					BgL_arg1233z00_339 =
						MAKE_YOUNG_PAIR(BgL_idz00_338, BGl_string1415z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_339,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1416z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2staticzd2bigloozf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 512 */
				obj_t BgL_idz00_340;

				BgL_idz00_340 = CNST_TABLE_REF(63);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_341;

					BgL_arg1233z00_341 =
						MAKE_YOUNG_PAIR(BgL_idz00_340, BGl_string1417z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_341,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 516 */
				obj_t BgL_idz00_342;

				BgL_idz00_342 = CNST_TABLE_REF(64);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_343;

					BgL_arg1233z00_343 =
						MAKE_YOUNG_PAIR(BgL_idz00_342, BGl_string1418z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_343,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2doublezd2ldzd2libszf3za2zf3zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 520 */
				obj_t BgL_idz00_344;

				BgL_idz00_344 = CNST_TABLE_REF(65);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_345;

					BgL_arg1233z00_345 =
						MAKE_YOUNG_PAIR(BgL_idz00_344, BGl_string1419z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_345,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
				}
			}
			BGl_za2bigloozd2userzd2libza2z00zzengine_paramz00 =
				BGl_stringzd2splitzd2charz00zztools_miscz00
				(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(66)),
				BCHAR(((unsigned char) ' ')));
			{	/* Engine/param.scm 524 */
				obj_t BgL_idz00_346;

				BgL_idz00_346 = CNST_TABLE_REF(67);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_347;

					BgL_arg1233z00_347 =
						MAKE_YOUNG_PAIR(BgL_idz00_346, BGl_string1420z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_347,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1421z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 528 */
				obj_t BgL_idz00_348;

				BgL_idz00_348 = CNST_TABLE_REF(68);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_349;

					BgL_arg1233z00_349 =
						MAKE_YOUNG_PAIR(BgL_idz00_348, BGl_string1422z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_349,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2bigloozd2librarieszd2czd2setupza2zd2zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 532 */
				obj_t BgL_idz00_350;

				BgL_idz00_350 = CNST_TABLE_REF(69);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_351;

					BgL_arg1233z00_351 =
						MAKE_YOUNG_PAIR(BgL_idz00_350, BGl_string1423z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_351,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2additionalzd2bigloozd2za7ipsza2za7zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 536 */
				obj_t BgL_idz00_352;

				BgL_idz00_352 = CNST_TABLE_REF(70);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_353;

					BgL_arg1233z00_353 =
						MAKE_YOUNG_PAIR(BgL_idz00_352, BGl_string1424z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_353,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2oldzd2loadzd2pathza2z00zzengine_paramz00 =
				BGl_za2loadzd2pathza2zd2zz__evalz00;
			BGl_za2loadzd2pathza2zd2zz__evalz00 =
				BGl_appendzd221011zd2zzengine_paramz00
				(BGl_za2loadzd2pathza2zd2zz__evalz00,
				BGl_za2libzd2dirza2zd2zzengine_paramz00);
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1425z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2includezd2multipleza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 546 */
				obj_t BgL_idz00_354;

				BgL_idz00_354 = CNST_TABLE_REF(71);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_355;

					BgL_arg1233z00_355 =
						MAKE_YOUNG_PAIR(BgL_idz00_354, BGl_string1426z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_355,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			{	/* Engine/param.scm 552 */
				obj_t BgL_list1166z00_132;

				BgL_list1166z00_132 =
					MAKE_YOUNG_PAIR(BGl_string1427z00zzengine_paramz00, BNIL);
				BGl_za2includezd2foreignza2zd2zzengine_paramz00 = BgL_list1166z00_132;
			}
			{	/* Engine/param.scm 550 */
				obj_t BgL_idz00_357;

				BgL_idz00_357 = CNST_TABLE_REF(72);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_358;

					BgL_arg1233z00_358 =
						MAKE_YOUNG_PAIR(BgL_idz00_357, BGl_string1428z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_358,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1429z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 554 */
				obj_t BgL_idz00_360;

				BgL_idz00_360 = CNST_TABLE_REF(73);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_361;

					BgL_arg1233z00_361 =
						MAKE_YOUNG_PAIR(BgL_idz00_360, BGl_string1430z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_361,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00 =
				BGl_string1431z00zzengine_paramz00;
			{	/* Engine/param.scm 558 */
				obj_t BgL_idz00_362;

				BgL_idz00_362 = CNST_TABLE_REF(74);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_363;

					BgL_arg1233z00_363 =
						MAKE_YOUNG_PAIR(BgL_idz00_362, BGl_string1432z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_363,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2heapzd2nameza2zd2zzengine_paramz00 =
				string_append(BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00,
				BGl_string1433z00zzengine_paramz00);
			{	/* Engine/param.scm 562 */
				obj_t BgL_idz00_364;

				BgL_idz00_364 = CNST_TABLE_REF(75);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_365;

					BgL_arg1233z00_365 =
						MAKE_YOUNG_PAIR(BgL_idz00_364, BGl_string1434z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_365,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1435z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2heapzd2libraryza2zd2zzengine_paramz00 = CNST_TABLE_REF(53);
			{	/* Engine/param.scm 566 */
				obj_t BgL_idz00_366;

				BgL_idz00_366 = CNST_TABLE_REF(76);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_367;

					BgL_arg1233z00_367 =
						MAKE_YOUNG_PAIR(BgL_idz00_366, BGl_string1436z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_367,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2heapzd2jvmzd2nameza2z00zzengine_paramz00 =
				string_append(BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00,
				BGl_string1437z00zzengine_paramz00);
			{	/* Engine/param.scm 570 */
				obj_t BgL_idz00_368;

				BgL_idz00_368 = CNST_TABLE_REF(77);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_369;

					BgL_arg1233z00_369 =
						MAKE_YOUNG_PAIR(BgL_idz00_368, BGl_string1438z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_369,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1439z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2heapzd2wasmzd2nameza2z00zzengine_paramz00 =
				string_append(BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00,
				BGl_string1440z00zzengine_paramz00);
			{	/* Engine/param.scm 574 */
				obj_t BgL_idz00_370;

				BgL_idz00_370 = CNST_TABLE_REF(78);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_371;

					BgL_arg1233z00_371 =
						MAKE_YOUNG_PAIR(BgL_idz00_370, BGl_string1441z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_371,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1442z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2heapzd2dumpzd2namesza2z00zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 578 */
				obj_t BgL_idz00_372;

				BgL_idz00_372 = CNST_TABLE_REF(79);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_373;

					BgL_arg1233z00_373 =
						MAKE_YOUNG_PAIR(BgL_idz00_372, BGl_string1443z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_373,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2foreignzd2classzd2idza2zd2zzengine_paramz00 =
				CNST_TABLE_REF(80);
			{	/* Engine/param.scm 582 */
				obj_t BgL_idz00_374;

				BgL_idz00_374 = CNST_TABLE_REF(81);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_375;

					BgL_arg1233z00_375 =
						MAKE_YOUNG_PAIR(BgL_idz00_374, BGl_string1444z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_375,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2foreignzd2classzd2nameza2zd2zzengine_paramz00 =
				BGl_string1445z00zzengine_paramz00;
			{	/* Engine/param.scm 586 */
				obj_t BgL_idz00_376;

				BgL_idz00_376 = CNST_TABLE_REF(82);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_377;

					BgL_arg1233z00_377 =
						MAKE_YOUNG_PAIR(BgL_idz00_376, BGl_string1446z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_377,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2additionalzd2heapzd2nameza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 590 */
				obj_t BgL_idz00_378;

				BgL_idz00_378 = CNST_TABLE_REF(83);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_379;

					BgL_arg1233z00_379 =
						MAKE_YOUNG_PAIR(BgL_idz00_378, BGl_string1447z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_379,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 594 */
				obj_t BgL_idz00_380;

				BgL_idz00_380 = CNST_TABLE_REF(84);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_381;

					BgL_arg1233z00_381 =
						MAKE_YOUNG_PAIR(BgL_idz00_380, BGl_string1448z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_381,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2indentza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(85));
			{	/* Engine/param.scm 598 */
				obj_t BgL_idz00_382;

				BgL_idz00_382 = CNST_TABLE_REF(86);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_383;

					BgL_arg1233z00_383 =
						MAKE_YOUNG_PAIR(BgL_idz00_382, BGl_string1449z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_383,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1450z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2compilerzd2debugza2zd2zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 602 */
				obj_t BgL_idz00_384;

				BgL_idz00_384 = CNST_TABLE_REF(87);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_385;

					BgL_arg1233z00_385 =
						MAKE_YOUNG_PAIR(BgL_idz00_384, BGl_string1451z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_385,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 606 */
				obj_t BgL_idz00_386;

				BgL_idz00_386 = CNST_TABLE_REF(88);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_387;

					BgL_arg1233z00_387 =
						MAKE_YOUNG_PAIR(BgL_idz00_386, BGl_string1452z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_387,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2errorzd2localiza7ationza2z75zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 610 */
				obj_t BgL_idz00_388;

				BgL_idz00_388 = CNST_TABLE_REF(89);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_389;

					BgL_arg1233z00_389 =
						MAKE_YOUNG_PAIR(BgL_idz00_388, BGl_string1453z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_389,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2compilerzd2sharingzd2debugzf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 613 */
				obj_t BgL_idz00_390;

				BgL_idz00_390 = CNST_TABLE_REF(90);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_391;

					BgL_arg1233z00_391 =
						MAKE_YOUNG_PAIR(BgL_idz00_390, BGl_string1454z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_391,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2compilerzd2typezd2debugzf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 616 */
				obj_t BgL_idz00_392;

				BgL_idz00_392 = CNST_TABLE_REF(91);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_393;

					BgL_arg1233z00_393 =
						MAKE_YOUNG_PAIR(BgL_idz00_392, BGl_string1455z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_393,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2compilerzd2stackzd2debugzf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 619 */
				obj_t BgL_idz00_394;

				BgL_idz00_394 = CNST_TABLE_REF(92);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_395;

					BgL_arg1233z00_395 =
						MAKE_YOUNG_PAIR(BgL_idz00_394, BGl_string1456z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_395,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2bmemzd2profilingza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 623 */
				obj_t BgL_idz00_396;

				BgL_idz00_396 = CNST_TABLE_REF(93);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_397;

					BgL_arg1233z00_397 =
						MAKE_YOUNG_PAIR(BgL_idz00_396, BGl_string1457z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_397,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2synczd2profilingza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 627 */
				obj_t BgL_idz00_398;

				BgL_idz00_398 = CNST_TABLE_REF(94);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_399;

					BgL_arg1233z00_399 =
						MAKE_YOUNG_PAIR(BgL_idz00_398, BGl_string1458z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_399,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2debugzd2moduleza2zd2zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 631 */
				obj_t BgL_idz00_400;

				BgL_idz00_400 = CNST_TABLE_REF(95);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_401;

					BgL_arg1233z00_401 =
						MAKE_YOUNG_PAIR(BgL_idz00_400, BGl_string1459z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_401,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2czd2debugza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 635 */
				obj_t BgL_idz00_402;

				BgL_idz00_402 = CNST_TABLE_REF(96);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_403;

					BgL_arg1233z00_403 =
						MAKE_YOUNG_PAIR(BgL_idz00_402, BGl_string1460z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_403,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2czd2debugzd2lineszd2infoza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 639 */
				obj_t BgL_idz00_404;

				BgL_idz00_404 = CNST_TABLE_REF(97);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_405;

					BgL_arg1233z00_405 =
						MAKE_YOUNG_PAIR(BgL_idz00_404, BGl_string1461z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_405,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2czd2debugzd2optionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(98));
			{	/* Engine/param.scm 643 */
				obj_t BgL_idz00_406;

				BgL_idz00_406 = CNST_TABLE_REF(99);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_407;

					BgL_arg1233z00_407 =
						MAKE_YOUNG_PAIR(BgL_idz00_406, BGl_string1462z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_407,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1463z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2czd2userzd2headerza2z00zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 647 */
				obj_t BgL_idz00_408;

				BgL_idz00_408 = CNST_TABLE_REF(100);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_409;

					BgL_arg1233z00_409 =
						MAKE_YOUNG_PAIR(BgL_idz00_408, BGl_string1464z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_409,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2czd2userzd2footza2z00zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 651 */
				obj_t BgL_idz00_410;

				BgL_idz00_410 = CNST_TABLE_REF(101);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_411;

					BgL_arg1233z00_411 =
						MAKE_YOUNG_PAIR(BgL_idz00_410, BGl_string1465z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_411,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2czd2tailzd2callza2z00zzengine_paramz00 = BUNSPEC;
			{	/* Engine/param.scm 655 */
				obj_t BgL_idz00_412;

				BgL_idz00_412 = CNST_TABLE_REF(102);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_413;

					BgL_arg1233z00_413 =
						MAKE_YOUNG_PAIR(BgL_idz00_412, BGl_string1466z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_413,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2debugza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 659 */
				obj_t BgL_idz00_414;

				BgL_idz00_414 = CNST_TABLE_REF(103);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_415;

					BgL_arg1233z00_415 =
						MAKE_YOUNG_PAIR(BgL_idz00_414, BGl_string1467z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_415,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2bdbzd2debugza2zd2zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 663 */
				obj_t BgL_idz00_416;

				BgL_idz00_416 = CNST_TABLE_REF(104);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_417;

					BgL_arg1233z00_417 =
						MAKE_YOUNG_PAIR(BgL_idz00_416, BGl_string1468z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_417,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00 =
				BFALSE;
			BGl_za2profilezd2modeza2zd2zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 668 */
				obj_t BgL_idz00_418;

				BgL_idz00_418 = CNST_TABLE_REF(105);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_419;

					BgL_arg1233z00_419 =
						MAKE_YOUNG_PAIR(BgL_idz00_418, BGl_string1469z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_419,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2profzd2tablezd2nameza2z00zzengine_paramz00 =
				BGl_string1470z00zzengine_paramz00;
			{	/* Engine/param.scm 672 */
				obj_t BgL_idz00_420;

				BgL_idz00_420 = CNST_TABLE_REF(106);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_421;

					BgL_arg1233z00_421 =
						MAKE_YOUNG_PAIR(BgL_idz00_420, BGl_string1471z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_421,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2accesszd2filesza2zd2zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 679 */
				obj_t BgL_idz00_422;

				BgL_idz00_422 = CNST_TABLE_REF(107);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_423;

					BgL_arg1233z00_423 =
						MAKE_YOUNG_PAIR(BgL_idz00_422, BGl_string1472z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_423,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2accesszd2filezd2defaultza2z00zzengine_paramz00 =
				BGl_string1473z00zzengine_paramz00;
			{	/* Engine/param.scm 682 */
				obj_t BgL_idz00_424;

				BgL_idz00_424 = CNST_TABLE_REF(108);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_425;

					BgL_arg1233z00_425 =
						MAKE_YOUNG_PAIR(BgL_idz00_424, BGl_string1474z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_425,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2qualifiedzd2typezd2fileza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 686 */
				obj_t BgL_idz00_426;

				BgL_idz00_426 = CNST_TABLE_REF(109);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_427;

					BgL_arg1233z00_427 =
						MAKE_YOUNG_PAIR(BgL_idz00_426, BGl_string1475z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_427,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2qualifiedzd2typezd2filezd2defaultza2zd2zzengine_paramz00 =
				BGl_string1476z00zzengine_paramz00;
			{	/* Engine/param.scm 689 */
				obj_t BgL_idz00_428;

				BgL_idz00_428 = CNST_TABLE_REF(110);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_429;

					BgL_arg1233z00_429 =
						MAKE_YOUNG_PAIR(BgL_idz00_428, BGl_string1475z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_429,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2ozd2filesza2zd2zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 696 */
				obj_t BgL_idz00_430;

				BgL_idz00_430 = CNST_TABLE_REF(111);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_431;

					BgL_arg1233z00_431 =
						MAKE_YOUNG_PAIR(BgL_idz00_430, BGl_string1477z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_431,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2czd2filesza2zd2zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 699 */
				obj_t BgL_idz00_432;

				BgL_idz00_432 = CNST_TABLE_REF(112);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_433;

					BgL_arg1233z00_433 =
						MAKE_YOUNG_PAIR(BgL_idz00_432, BGl_string1478z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_433,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2withzd2filesza2zd2zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 702 */
				obj_t BgL_idz00_434;

				BgL_idz00_434 = CNST_TABLE_REF(113);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_435;

					BgL_arg1233z00_435 =
						MAKE_YOUNG_PAIR(BgL_idz00_434, BGl_string1479z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_435,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2earlyzd2withzd2modulesza2z00zzengine_paramz00 = BNIL;
			BGl_za2interpreterza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 710 */
				obj_t BgL_idz00_436;

				BgL_idz00_436 = CNST_TABLE_REF(114);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_437;

					BgL_arg1233z00_437 =
						MAKE_YOUNG_PAIR(BgL_idz00_436, BGl_string1480z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_437,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2startupzd2fileza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 713 */
				obj_t BgL_idz00_438;

				BgL_idz00_438 = CNST_TABLE_REF(115);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_439;

					BgL_arg1233z00_439 =
						MAKE_YOUNG_PAIR(BgL_idz00_438, BGl_string1481z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_439,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2callzf2cczf3za2z01zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 716 */
				obj_t BgL_idz00_440;

				BgL_idz00_440 = CNST_TABLE_REF(116);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_441;

					BgL_arg1233z00_441 =
						MAKE_YOUNG_PAIR(BgL_idz00_440, BGl_string1482z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_441,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2autozd2linkzd2mainza2z00zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 719 */
				obj_t BgL_idz00_442;

				BgL_idz00_442 = CNST_TABLE_REF(117);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_443;

					BgL_arg1233z00_443 =
						MAKE_YOUNG_PAIR(BgL_idz00_442, BGl_string1483z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_443,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2passza2z00zzengine_paramz00 = CNST_TABLE_REF(118);
			{	/* Engine/param.scm 723 */
				obj_t BgL_idz00_444;

				BgL_idz00_444 = CNST_TABLE_REF(119);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_445;

					BgL_arg1233z00_445 =
						MAKE_YOUNG_PAIR(BgL_idz00_444, BGl_string1484z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_445,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2jarzf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 726 */
				obj_t BgL_idz00_446;

				BgL_idz00_446 = CNST_TABLE_REF(120);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_447;

					BgL_arg1233z00_447 =
						MAKE_YOUNG_PAIR(BgL_idz00_446, BGl_string1485z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_447,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2shellza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(121));
			{	/* Engine/param.scm 729 */
				obj_t BgL_idz00_448;

				BgL_idz00_448 = CNST_TABLE_REF(122);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_449;

					BgL_arg1233z00_449 =
						MAKE_YOUNG_PAIR(BgL_idz00_448, BGl_string1486z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_449,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1487z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2jvmzd2javaza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(123));
			{	/* Engine/param.scm 732 */
				obj_t BgL_idz00_450;

				BgL_idz00_450 = CNST_TABLE_REF(124);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_451;

					BgL_arg1233z00_451 =
						MAKE_YOUNG_PAIR(BgL_idz00_450, BGl_string1488z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_451,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1489z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2jvmzd2optionsza2zd2zzengine_paramz00 =
				BGl_string1490z00zzengine_paramz00;
			{	/* Engine/param.scm 735 */
				obj_t BgL_idz00_452;

				BgL_idz00_452 = CNST_TABLE_REF(125);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_453;

					BgL_arg1233z00_453 =
						MAKE_YOUNG_PAIR(BgL_idz00_452, BGl_string1491z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_453,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2bigloozd2classpathza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 738 */
				obj_t BgL_idz00_454;

				BgL_idz00_454 = CNST_TABLE_REF(126);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_455;

					BgL_arg1233z00_455 =
						MAKE_YOUNG_PAIR(BgL_idz00_454, BGl_string1492z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_455,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2classpathza2zd2zzengine_paramz00 =
				BGl_string1400z00zzengine_paramz00;
			{	/* Engine/param.scm 741 */
				obj_t BgL_idz00_456;

				BgL_idz00_456 = CNST_TABLE_REF(127);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_457;

					BgL_arg1233z00_457 =
						MAKE_YOUNG_PAIR(BgL_idz00_456, BGl_string1493z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_457,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2mainclassza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 744 */
				obj_t BgL_idz00_458;

				BgL_idz00_458 = CNST_TABLE_REF(128);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_459;

					BgL_arg1233z00_459 =
						MAKE_YOUNG_PAIR(BgL_idz00_458, BGl_string1494z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_459,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2pathzd2separatorza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 747 */
				obj_t BgL_idz00_460;

				BgL_idz00_460 = CNST_TABLE_REF(129);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_461;

					BgL_arg1233z00_461 =
						MAKE_YOUNG_PAIR(BgL_idz00_460, BGl_string1493z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_461,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2jarpathza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 750 */
				obj_t BgL_idz00_462;

				BgL_idz00_462 = CNST_TABLE_REF(130);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_463;

					BgL_arg1233z00_463 =
						MAKE_YOUNG_PAIR(BgL_idz00_462, BGl_string1495z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_463,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2directoryza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 753 */
				obj_t BgL_idz00_464;

				BgL_idz00_464 = CNST_TABLE_REF(131);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_465;

					BgL_arg1233z00_465 =
						MAKE_YOUNG_PAIR(BgL_idz00_464, BGl_string1496z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_465,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2catchza2zd2zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 756 */
				obj_t BgL_idz00_466;

				BgL_idz00_466 = CNST_TABLE_REF(132);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_467;

					BgL_arg1233z00_467 =
						MAKE_YOUNG_PAIR(BgL_idz00_466, BGl_string1497z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_467,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2cinitzd2moduleza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 759 */
				obj_t BgL_idz00_468;

				BgL_idz00_468 = CNST_TABLE_REF(133);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_469;

					BgL_arg1233z00_469 =
						MAKE_YOUNG_PAIR(BgL_idz00_468, BGl_string1498z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_469,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2modulezd2checksumzd2objectzf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 762 */
				obj_t BgL_idz00_470;

				BgL_idz00_470 = CNST_TABLE_REF(134);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_471;

					BgL_arg1233z00_471 =
						MAKE_YOUNG_PAIR(BgL_idz00_470, BGl_string1499z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_471,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2garbagezd2collectorza2zd2zzengine_paramz00 = CNST_TABLE_REF(135);
			{	/* Engine/param.scm 765 */
				obj_t BgL_idz00_472;

				BgL_idz00_472 = CNST_TABLE_REF(136);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_473;

					BgL_arg1233z00_473 =
						MAKE_YOUNG_PAIR(BgL_idz00_472, BGl_string1500z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_473,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2unsafezd2typeza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 772 */
				obj_t BgL_idz00_474;

				BgL_idz00_474 = CNST_TABLE_REF(137);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_475;

					BgL_arg1233z00_475 =
						MAKE_YOUNG_PAIR(BgL_idz00_474, BGl_string1501z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_475,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2unsafezd2arityza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 775 */
				obj_t BgL_idz00_476;

				BgL_idz00_476 = CNST_TABLE_REF(138);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_477;

					BgL_arg1233z00_477 =
						MAKE_YOUNG_PAIR(BgL_idz00_476, BGl_string1502z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_477,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2unsafezd2rangeza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 778 */
				obj_t BgL_idz00_478;

				BgL_idz00_478 = CNST_TABLE_REF(139);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_479;

					BgL_arg1233z00_479 =
						MAKE_YOUNG_PAIR(BgL_idz00_478, BGl_string1503z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_479,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2unsafezd2structza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 781 */
				obj_t BgL_idz00_480;

				BgL_idz00_480 = CNST_TABLE_REF(140);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_481;

					BgL_arg1233z00_481 =
						MAKE_YOUNG_PAIR(BgL_idz00_480, BGl_string1504z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_481,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2unsafezd2versionza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 784 */
				obj_t BgL_idz00_482;

				BgL_idz00_482 = CNST_TABLE_REF(141);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_483;

					BgL_arg1233z00_483 =
						MAKE_YOUNG_PAIR(BgL_idz00_482, BGl_string1505z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_483,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2unsafezd2libraryza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 787 */
				obj_t BgL_idz00_484;

				BgL_idz00_484 = CNST_TABLE_REF(142);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_485;

					BgL_arg1233z00_485 =
						MAKE_YOUNG_PAIR(BgL_idz00_484, BGl_string1506z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_485,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2unsafezd2evalza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 790 */
				obj_t BgL_idz00_486;

				BgL_idz00_486 = CNST_TABLE_REF(143);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_487;

					BgL_arg1233z00_487 =
						MAKE_YOUNG_PAIR(BgL_idz00_486, BGl_string1507z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_487,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2unsafezd2heapza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 793 */
				obj_t BgL_idz00_488;

				BgL_idz00_488 = CNST_TABLE_REF(144);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_489;

					BgL_arg1233z00_489 =
						MAKE_YOUNG_PAIR(BgL_idz00_488, BGl_string1508z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_489,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2warningzd2overridenzd2slotsza2z00zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 796 */
				obj_t BgL_idz00_490;

				BgL_idz00_490 = CNST_TABLE_REF(145);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_491;

					BgL_arg1233z00_491 =
						MAKE_YOUNG_PAIR(BgL_idz00_490, BGl_string1509z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_491,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2warningzd2overridenzd2variablesza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 799 */
				obj_t BgL_idz00_492;

				BgL_idz00_492 = CNST_TABLE_REF(146);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_493;

					BgL_arg1233z00_493 =
						MAKE_YOUNG_PAIR(BgL_idz00_492, BGl_string1510z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_493,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2warningzd2typesza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 802 */
				obj_t BgL_idz00_494;

				BgL_idz00_494 = CNST_TABLE_REF(147);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_495;

					BgL_arg1233z00_495 =
						MAKE_YOUNG_PAIR(BgL_idz00_494, BGl_string1511z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_495,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2warningzd2typezd2errorza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 805 */
				obj_t BgL_idz00_496;

				BgL_idz00_496 = CNST_TABLE_REF(148);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_497;

					BgL_arg1233z00_497 =
						MAKE_YOUNG_PAIR(BgL_idz00_496, BGl_string1512z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_497,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2warningzd2defaultzd2slotzd2valueza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 808 */
				obj_t BgL_idz00_498;

				BgL_idz00_498 = CNST_TABLE_REF(149);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_499;

					BgL_arg1233z00_499 =
						MAKE_YOUNG_PAIR(BgL_idz00_498, BGl_string1513z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_499,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2profilezd2libraryza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 811 */
				obj_t BgL_idz00_500;

				BgL_idz00_500 = CNST_TABLE_REF(150);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_501;

					BgL_arg1233z00_501 =
						MAKE_YOUNG_PAIR(BgL_idz00_500, BGl_string1514z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_501,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2modulezd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2keyzd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2typezd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2typenamezd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2accesszd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2locationzd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2userzd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2namezd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2alloczd2shapezf3za2z21zzengine_paramz00 = BFALSE;
			BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00 = BTRUE;
			BGl_za2arithmeticzd2overflowza2zd2zzengine_paramz00 = BTRUE;
			BGl_za2arithmeticzd2newzd2overflowza2z00zzengine_paramz00 = BTRUE;
			BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 827 */
				obj_t BgL_idz00_502;

				BgL_idz00_502 = CNST_TABLE_REF(151);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_503;

					BgL_arg1233z00_503 =
						MAKE_YOUNG_PAIR(BgL_idz00_502, BGl_string1515z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_503,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2libzd2modeza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 830 */
				obj_t BgL_idz00_504;

				BgL_idz00_504 = CNST_TABLE_REF(152);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_505;

					BgL_arg1233z00_505 =
						MAKE_YOUNG_PAIR(BgL_idz00_504, BGl_string1516z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_505,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2initzd2modeza2zd2zzengine_paramz00 = CNST_TABLE_REF(153);
			{	/* Engine/param.scm 833 */
				obj_t BgL_idz00_506;

				BgL_idz00_506 = CNST_TABLE_REF(154);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_507;

					BgL_arg1233z00_507 =
						MAKE_YOUNG_PAIR(BgL_idz00_506, BGl_string1517z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_507,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 = CNST_TABLE_REF(155);
			{	/* Engine/param.scm 836 */
				obj_t BgL_idz00_508;

				BgL_idz00_508 = CNST_TABLE_REF(156);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_509;

					BgL_arg1233z00_509 =
						MAKE_YOUNG_PAIR(BgL_idz00_508, BGl_string1518z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_509,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2dlopenzd2initza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 839 */
				obj_t BgL_idz00_510;

				BgL_idz00_510 = CNST_TABLE_REF(157);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_511;

					BgL_arg1233z00_511 =
						MAKE_YOUNG_PAIR(BgL_idz00_510, BGl_string1519z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_511,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2dlopenzd2initzd2gcza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 842 */
				obj_t BgL_idz00_512;

				BgL_idz00_512 = CNST_TABLE_REF(158);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_513;

					BgL_arg1233z00_513 =
						MAKE_YOUNG_PAIR(BgL_idz00_512, BGl_string1520z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_513,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00 = BINT(1024L);
			{	/* Engine/param.scm 845 */
				obj_t BgL_idz00_514;

				BgL_idz00_514 = CNST_TABLE_REF(159);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_515;

					BgL_arg1233z00_515 =
						MAKE_YOUNG_PAIR(BgL_idz00_514, BGl_string1521z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_515,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2czd2splitzd2stringza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(160));
			{	/* Engine/param.scm 848 */
				obj_t BgL_idz00_516;

				BgL_idz00_516 = CNST_TABLE_REF(161);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_517;

					BgL_arg1233z00_517 =
						MAKE_YOUNG_PAIR(BgL_idz00_516, BGl_string1522z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_517,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1523z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2maxzd2czd2foreignzd2arityza2zd2zzengine_paramz00 = BINT(16L);
			{	/* Engine/param.scm 851 */
				obj_t BgL_idz00_518;

				BgL_idz00_518 = CNST_TABLE_REF(162);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_519;

					BgL_arg1233z00_519 =
						MAKE_YOUNG_PAIR(BgL_idz00_518, BGl_string1524z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_519,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2tracezd2nameza2zd2zzengine_paramz00 =
				BGl_string1525z00zzengine_paramz00;
			{	/* Engine/param.scm 854 */
				obj_t BgL_idz00_520;

				BgL_idz00_520 = CNST_TABLE_REF(163);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_521;

					BgL_arg1233z00_521 =
						MAKE_YOUNG_PAIR(BgL_idz00_520, BGl_string1526z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_521,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2tracezd2levelza2zd2zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 857 */
				obj_t BgL_idz00_522;

				BgL_idz00_522 = CNST_TABLE_REF(164);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_523;

					BgL_arg1233z00_523 =
						MAKE_YOUNG_PAIR(BgL_idz00_522, BGl_string1527z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_523,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2tracezd2writezd2lengthza2z00zzengine_paramz00 = BINT(80L);
			{	/* Engine/param.scm 860 */
				obj_t BgL_idz00_524;

				BgL_idz00_524 = CNST_TABLE_REF(165);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_525;

					BgL_arg1233z00_525 =
						MAKE_YOUNG_PAIR(BgL_idz00_524, BGl_string1528z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_525,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2additionalzd2tracesza2zd2zzengine_paramz00 = BNIL;
			BGl_za2optimza2z00zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 868 */
				obj_t BgL_idz00_526;

				BgL_idz00_526 = CNST_TABLE_REF(166);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_527;

					BgL_arg1233z00_527 =
						MAKE_YOUNG_PAIR(BgL_idz00_526, BGl_string1529z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_527,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2unrollzd2loopzf3za2zf3zzengine_paramz00 = BUNSPEC;
			{	/* Engine/param.scm 871 */
				obj_t BgL_idz00_528;

				BgL_idz00_528 = CNST_TABLE_REF(167);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_529;

					BgL_arg1233z00_529 =
						MAKE_YOUNG_PAIR(BgL_idz00_528, BGl_string1530z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_529,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2loopzd2inliningzf3za2zf3zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 874 */
				obj_t BgL_idz00_530;

				BgL_idz00_530 = CNST_TABLE_REF(168);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_531;

					BgL_arg1233z00_531 =
						MAKE_YOUNG_PAIR(BgL_idz00_530, BGl_string1531z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_531,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2atomzd2inliningzf3za2zf3zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 877 */
				obj_t BgL_idz00_532;

				BgL_idz00_532 = CNST_TABLE_REF(169);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_533;

					BgL_arg1233z00_533 =
						MAKE_YOUNG_PAIR(BgL_idz00_532, BGl_string1532z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_533,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2Ozd2macrozf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 880 */
				obj_t BgL_idz00_534;

				BgL_idz00_534 = CNST_TABLE_REF(170);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_535;

					BgL_arg1233z00_535 =
						MAKE_YOUNG_PAIR(BgL_idz00_534, BGl_string1533z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_535,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2jvmzd2inliningza2z00zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 883 */
				obj_t BgL_idz00_536;

				BgL_idz00_536 = CNST_TABLE_REF(171);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_537;

					BgL_arg1233z00_537 =
						MAKE_YOUNG_PAIR(BgL_idz00_536, BGl_string1534z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_537,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2jvmzd2constructorzd2inliningza2zd2zzengine_paramz00 =
				BINT(0L);
			{	/* Engine/param.scm 886 */
				obj_t BgL_idz00_538;

				BgL_idz00_538 = CNST_TABLE_REF(172);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_539;

					BgL_arg1233z00_539 =
						MAKE_YOUNG_PAIR(BgL_idz00_538, BGl_string1535z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_539,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2jvmzd2peepholeza2z00zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 889 */
				obj_t BgL_idz00_540;

				BgL_idz00_540 = CNST_TABLE_REF(173);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_541;

					BgL_arg1233z00_541 =
						MAKE_YOUNG_PAIR(BgL_idz00_540, BGl_string1536z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_541,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2jvmzd2branchza2z00zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 892 */
				obj_t BgL_idz00_542;

				BgL_idz00_542 = CNST_TABLE_REF(174);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_543;

					BgL_arg1233z00_543 =
						MAKE_YOUNG_PAIR(BgL_idz00_542, BGl_string1537z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_543,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2jvmzd2fasteqza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 895 */
				obj_t BgL_idz00_544;

				BgL_idz00_544 = CNST_TABLE_REF(175);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_545;

					BgL_arg1233z00_545 =
						MAKE_YOUNG_PAIR(BgL_idz00_544, BGl_string1538z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_545,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2symbolzd2caseza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 898 */
				obj_t BgL_idz00_546;

				BgL_idz00_546 = CNST_TABLE_REF(176);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_547;

					BgL_arg1233z00_547 =
						MAKE_YOUNG_PAIR(BgL_idz00_546, BGl_string1539z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_547,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2purifyza2z00zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 901 */
				obj_t BgL_idz00_548;

				BgL_idz00_548 = CNST_TABLE_REF(177);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_549;

					BgL_arg1233z00_549 =
						MAKE_YOUNG_PAIR(BgL_idz00_548, BGl_string1540z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_549,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2jvmzd2envza2zd2zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 904 */
				obj_t BgL_idz00_550;

				BgL_idz00_550 = CNST_TABLE_REF(178);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_551;

					BgL_arg1233z00_551 =
						MAKE_YOUNG_PAIR(BgL_idz00_550, BGl_string1541z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_551,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2jvmza2zd2zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 907 */
				obj_t BgL_idz00_552;

				BgL_idz00_552 = CNST_TABLE_REF(179);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_553;

					BgL_arg1233z00_553 =
						MAKE_YOUNG_PAIR(BgL_idz00_552, BGl_string1542z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_553,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2isazf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 910 */
				obj_t BgL_idz00_554;

				BgL_idz00_554 = CNST_TABLE_REF(180);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_555;

					BgL_arg1233z00_555 =
						MAKE_YOUNG_PAIR(BgL_idz00_554, BGl_string1543z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_555,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2cfazf3za2z21zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 913 */
				obj_t BgL_idz00_556;

				BgL_idz00_556 = CNST_TABLE_REF(181);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_557;

					BgL_arg1233z00_557 =
						MAKE_YOUNG_PAIR(BgL_idz00_556, BGl_string1544z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_557,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2cfazd2fixnumzd2arithmeticzf3za2z21zzengine_paramz00 =
				BFALSE;
			{	/* Engine/param.scm 916 */
				obj_t BgL_idz00_558;

				BgL_idz00_558 = CNST_TABLE_REF(182);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_559;

					BgL_arg1233z00_559 =
						MAKE_YOUNG_PAIR(BgL_idz00_558, BGl_string1545z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_559,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2cfazd2flonumzd2arithmeticzf3za2z21zzengine_paramz00 =
				BFALSE;
			{	/* Engine/param.scm 919 */
				obj_t BgL_idz00_560;

				BgL_idz00_560 = CNST_TABLE_REF(183);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_561;

					BgL_arg1233z00_561 =
						MAKE_YOUNG_PAIR(BgL_idz00_560, BGl_string1546z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_561,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00 =
				BFALSE;
			{	/* Engine/param.scm 922 */
				obj_t BgL_idz00_562;

				BgL_idz00_562 = CNST_TABLE_REF(184);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_563;

					BgL_arg1233z00_563 =
						MAKE_YOUNG_PAIR(BgL_idz00_562, BGl_string1547z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_563,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2cfazd2applyzd2trackingzf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 925 */
				obj_t BgL_idz00_564;

				BgL_idz00_564 = CNST_TABLE_REF(185);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_565;

					BgL_arg1233z00_565 =
						MAKE_YOUNG_PAIR(BgL_idz00_564, BGl_string1548z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_565,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2cfazd2pairzf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 928 */
				obj_t BgL_idz00_566;

				BgL_idz00_566 = CNST_TABLE_REF(186);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_567;

					BgL_arg1233z00_567 =
						MAKE_YOUNG_PAIR(BgL_idz00_566, BGl_string1549z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_567,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2cfazd2pairzd2quotezd2maxzd2lengthza2zd2zzengine_paramz00 =
				BINT(4L);
			{	/* Engine/param.scm 931 */
				obj_t BgL_idz00_568;

				BgL_idz00_568 = CNST_TABLE_REF(187);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_569;

					BgL_arg1233z00_569 =
						MAKE_YOUNG_PAIR(BgL_idz00_568, BGl_string1550z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_569,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00 =
				BFALSE;
			{	/* Engine/param.scm 934 */
				obj_t BgL_idz00_570;

				BgL_idz00_570 = CNST_TABLE_REF(188);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_571;

					BgL_arg1233z00_571 =
						MAKE_YOUNG_PAIR(BgL_idz00_570, BGl_string1551z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_571,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2cfazd2forcezd2loosezd2localzd2functionzf3za2z21zzengine_paramz00
				= BFALSE;
			{	/* Engine/param.scm 937 */
				obj_t BgL_idz00_572;

				BgL_idz00_572 = CNST_TABLE_REF(189);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_573;

					BgL_arg1233z00_573 =
						MAKE_YOUNG_PAIR(BgL_idz00_572, BGl_string1552z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_573,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2integratezf3za2z21zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 940 */
				obj_t BgL_idz00_574;

				BgL_idz00_574 = CNST_TABLE_REF(190);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_575;

					BgL_arg1233z00_575 =
						MAKE_YOUNG_PAIR(BgL_idz00_574, BGl_string1553z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_575,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2dataflowzf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 943 */
				obj_t BgL_idz00_576;

				BgL_idz00_576 = CNST_TABLE_REF(191);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_577;

					BgL_arg1233z00_577 =
						MAKE_YOUNG_PAIR(BgL_idz00_576, BGl_string1554z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_577,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2dataflowzd2forzd2errorszf3za2z21zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 946 */
				obj_t BgL_idz00_578;

				BgL_idz00_578 = CNST_TABLE_REF(192);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_579;

					BgL_arg1233z00_579 =
						MAKE_YOUNG_PAIR(BgL_idz00_578, BGl_string1555z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_579,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2dataflowzd2typeszf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 949 */
				obj_t BgL_idz00_580;

				BgL_idz00_580 = CNST_TABLE_REF(193);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_581;

					BgL_arg1233z00_581 =
						MAKE_YOUNG_PAIR(BgL_idz00_580, BGl_string1556z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_581,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2initflowzf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 952 */
				obj_t BgL_idz00_582;

				BgL_idz00_582 = CNST_TABLE_REF(194);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_583;

					BgL_arg1233z00_583 =
						MAKE_YOUNG_PAIR(BgL_idz00_582, BGl_string1557z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_583,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2synczd2failsafezf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 955 */
				obj_t BgL_idz00_584;

				BgL_idz00_584 = CNST_TABLE_REF(195);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_585;

					BgL_arg1233z00_585 =
						MAKE_YOUNG_PAIR(BgL_idz00_584, BGl_string1558z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_585,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2reducezd2betazf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 958 */
				obj_t BgL_idz00_586;

				BgL_idz00_586 = CNST_TABLE_REF(196);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_587;

					BgL_arg1233z00_587 =
						MAKE_YOUNG_PAIR(BgL_idz00_586, BGl_string1559z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_587,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2inliningzf3za2zf3zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 961 */
				obj_t BgL_idz00_588;

				BgL_idz00_588 = CNST_TABLE_REF(197);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_589;

					BgL_arg1233z00_589 =
						MAKE_YOUNG_PAIR(BgL_idz00_588, BGl_string1560z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_589,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2userzd2inliningzf3za2z21zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 964 */
				obj_t BgL_idz00_590;

				BgL_idz00_590 = CNST_TABLE_REF(198);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_591;

					BgL_arg1233z00_591 =
						MAKE_YOUNG_PAIR(BgL_idz00_590, BGl_string1561z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_591,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2inliningzd2kfactorza2zd2zzengine_paramz00 =
				BGl_proc1562z00zzengine_paramz00;
			{	/* Engine/param.scm 967 */
				obj_t BgL_idz00_593;

				BgL_idz00_593 = CNST_TABLE_REF(199);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_594;

					BgL_arg1233z00_594 =
						MAKE_YOUNG_PAIR(BgL_idz00_593, BGl_string1563z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_594,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2inliningzd2reducezd2kfactorza2z00zzengine_paramz00 =
				BGl_proc1564z00zzengine_paramz00;
			{	/* Engine/param.scm 970 */
				obj_t BgL_idz00_596;

				BgL_idz00_596 = CNST_TABLE_REF(200);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_597;

					BgL_arg1233z00_597 =
						MAKE_YOUNG_PAIR(BgL_idz00_596, BGl_string1565z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_597,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2returnzf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 973 */
				obj_t BgL_idz00_598;

				BgL_idz00_598 = CNST_TABLE_REF(201);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_599;

					BgL_arg1233z00_599 =
						MAKE_YOUNG_PAIR(BgL_idz00_598, BGl_string1566z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_599,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 976 */
				obj_t BgL_idz00_600;

				BgL_idz00_600 = CNST_TABLE_REF(202);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_601;

					BgL_arg1233z00_601 =
						MAKE_YOUNG_PAIR(BgL_idz00_600, BGl_string1567z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_601,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2taggedzd2fxopzf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 979 */
				obj_t BgL_idz00_602;

				BgL_idz00_602 = CNST_TABLE_REF(203);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_603;

					BgL_arg1233z00_603 =
						MAKE_YOUNG_PAIR(BgL_idz00_602, BGl_string1568z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_603,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2specializa7ezd2flonumzf3za2z54zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 982 */
				obj_t BgL_idz00_604;

				BgL_idz00_604 = CNST_TABLE_REF(204);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_605;

					BgL_arg1233z00_605 =
						MAKE_YOUNG_PAIR(BgL_idz00_604, BGl_string1569z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_605,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2stackablezf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 985 */
				obj_t BgL_idz00_606;

				BgL_idz00_606 = CNST_TABLE_REF(205);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_607;

					BgL_arg1233z00_607 =
						MAKE_YOUNG_PAIR(BgL_idz00_606, BGl_string1570z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_607,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2optimzd2uncellzf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 988 */
				obj_t BgL_idz00_608;

				BgL_idz00_608 = CNST_TABLE_REF(206);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_609;

					BgL_arg1233z00_609 =
						MAKE_YOUNG_PAIR(BgL_idz00_608, BGl_string1571z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_609,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2extendzd2entryza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 995 */
				obj_t BgL_idz00_610;

				BgL_idz00_610 = CNST_TABLE_REF(207);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_611;

					BgL_arg1233z00_611 =
						MAKE_YOUNG_PAIR(BgL_idz00_610, BGl_string1572z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_611,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2srczd2suffixza2zd2zzengine_paramz00 = CNST_TABLE_REF(208);
			{	/* Engine/param.scm 1004 */
				obj_t BgL_idz00_612;

				BgL_idz00_612 = CNST_TABLE_REF(209);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_613;

					BgL_arg1233z00_613 =
						MAKE_YOUNG_PAIR(BgL_idz00_612, BGl_string1573z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_613,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2czd2suffixza2zd2zzengine_paramz00 = CNST_TABLE_REF(210);
			{	/* Engine/param.scm 1013 */
				obj_t BgL_idz00_614;

				BgL_idz00_614 = CNST_TABLE_REF(211);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_615;

					BgL_arg1233z00_615 =
						MAKE_YOUNG_PAIR(BgL_idz00_614, BGl_string1574z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_615,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2csharpzd2suffixza2zd2zzengine_paramz00 = CNST_TABLE_REF(212);
			{	/* Engine/param.scm 1023 */
				obj_t BgL_idz00_616;

				BgL_idz00_616 = CNST_TABLE_REF(213);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_617;

					BgL_arg1233z00_617 =
						MAKE_YOUNG_PAIR(BgL_idz00_616, BGl_string1575z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_617,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			{	/* Engine/param.scm 1034 */
				obj_t BgL_list1209z00_167;

				{	/* Engine/param.scm 1034 */
					obj_t BgL_arg1210z00_168;

					{	/* Engine/param.scm 1034 */
						obj_t BgL_arg1212z00_169;

						BgL_arg1212z00_169 =
							MAKE_YOUNG_PAIR(string_to_bstring(SHARED_LIB_SUFFIX), BNIL);
						BgL_arg1210z00_168 =
							MAKE_YOUNG_PAIR(string_to_bstring(STATIC_LIB_SUFFIX),
							BgL_arg1212z00_169);
					}
					BgL_list1209z00_167 =
						MAKE_YOUNG_PAIR
						(BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00,
						BgL_arg1210z00_168);
				}
				BGl_za2objzd2suffixza2zd2zzengine_paramz00 = BgL_list1209z00_167;
			}
			{	/* Engine/param.scm 1032 */
				obj_t BgL_idz00_619;

				BgL_idz00_619 = CNST_TABLE_REF(214);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_620;

					BgL_arg1233z00_620 =
						MAKE_YOUNG_PAIR(BgL_idz00_619, BGl_string1576z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_620,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1577z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2mcozd2suffixza2zd2zzengine_paramz00 = CNST_TABLE_REF(215);
			{	/* Engine/param.scm 1043 */
				obj_t BgL_idz00_622;

				BgL_idz00_622 = CNST_TABLE_REF(216);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_623;

					BgL_arg1233z00_623 =
						MAKE_YOUNG_PAIR(BgL_idz00_622, BGl_string1578z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_623,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2mcozd2includezd2pathza2z00zzengine_paramz00 = CNST_TABLE_REF(217);
			{	/* Engine/param.scm 1050 */
				obj_t BgL_idz00_624;

				BgL_idz00_624 = CNST_TABLE_REF(218);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_625;

					BgL_arg1233z00_625 =
						MAKE_YOUNG_PAIR(BgL_idz00_624, BGl_string1579z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_625,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2autozd2modeza2zd2zzengine_paramz00 = CNST_TABLE_REF(219);
			{	/* Engine/param.scm 1057 */
				obj_t BgL_idz00_626;

				BgL_idz00_626 = CNST_TABLE_REF(220);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_627;

					BgL_arg1233z00_627 =
						MAKE_YOUNG_PAIR(BgL_idz00_626, BGl_string1580z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_627,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00 = BTRUE;
			{	/* Engine/param.scm 1068 */
				obj_t BgL_idz00_628;

				BgL_idz00_628 = CNST_TABLE_REF(221);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_629;

					BgL_arg1233z00_629 =
						MAKE_YOUNG_PAIR(BgL_idz00_628, BGl_string1581z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_629,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2userzd2heapzd2siza7eza2za7zzengine_paramz00 = BINT(0L);
			{	/* Engine/param.scm 1075 */
				obj_t BgL_idz00_630;

				BgL_idz00_630 = CNST_TABLE_REF(222);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_631;

					BgL_arg1233z00_631 =
						MAKE_YOUNG_PAIR(BgL_idz00_630, BGl_string1582z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_631,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2readerza2z00zzengine_paramz00 = CNST_TABLE_REF(223);
			{	/* Engine/param.scm 1082 */
				obj_t BgL_idz00_632;

				BgL_idz00_632 = CNST_TABLE_REF(224);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_633;

					BgL_arg1233z00_633 =
						MAKE_YOUNG_PAIR(BgL_idz00_632, BGl_string1583z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_633,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			{	/* Engine/param.scm 1091 */
				obj_t BgL_arg1225z00_178;

				BgL_arg1225z00_178 =
					BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(225));
				BGl_za2targetzd2languageza2zd2zzengine_paramz00 =
					bstring_to_symbol(((obj_t) BgL_arg1225z00_178));
			}
			{	/* Engine/param.scm 1089 */
				obj_t BgL_idz00_635;

				BgL_idz00_635 = CNST_TABLE_REF(226);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_636;

					BgL_arg1233z00_636 =
						MAKE_YOUNG_PAIR(BgL_idz00_635, BGl_string1584z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_636,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BGl_proc1585z00zzengine_paramz00,
				BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
			BGl_za2sawza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 1096 */
				obj_t BgL_idz00_638;

				BgL_idz00_638 = CNST_TABLE_REF(227);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_639;

					BgL_arg1233z00_639 =
						MAKE_YOUNG_PAIR(BgL_idz00_638, BGl_string1586z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_639,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2sawzd2registerzd2reallocationzf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 1103 */
				obj_t BgL_idz00_640;

				BgL_idz00_640 = CNST_TABLE_REF(228);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_641;

					BgL_arg1233z00_641 =
						MAKE_YOUNG_PAIR(BgL_idz00_640, BGl_string1587z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_641,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2sawzd2registerzd2allocationzf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 1110 */
				obj_t BgL_idz00_642;

				BgL_idz00_642 = CNST_TABLE_REF(229);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_643;

					BgL_arg1233z00_643 =
						MAKE_YOUNG_PAIR(BgL_idz00_642, BGl_string1588z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_643,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2sawzd2registerzd2allocationzd2onexpressionzf3za2z21zzengine_paramz00
				= BFALSE;
			{	/* Engine/param.scm 1117 */
				obj_t BgL_idz00_644;

				BgL_idz00_644 = CNST_TABLE_REF(230);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_645;

					BgL_arg1233z00_645 =
						MAKE_YOUNG_PAIR(BgL_idz00_644, BGl_string1589z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_645,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2sawzd2registerzd2allocationzd2maxzd2siza7eza2za7zzengine_paramz00 =
				BINT(4000L);
			{	/* Engine/param.scm 1124 */
				obj_t BgL_idz00_646;

				BgL_idz00_646 = CNST_TABLE_REF(231);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_647;

					BgL_arg1233z00_647 =
						MAKE_YOUNG_PAIR(BgL_idz00_646, BGl_string1590z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_647,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2sawzd2registerzd2allocationzd2functionsza2zd2zzengine_paramz00 =
				BNIL;
			{	/* Engine/param.scm 1131 */
				obj_t BgL_idz00_648;

				BgL_idz00_648 = CNST_TABLE_REF(232);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_649;

					BgL_arg1233z00_649 =
						MAKE_YOUNG_PAIR(BgL_idz00_648, BGl_string1591z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_649,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2sawzd2nozd2registerzd2allocationzd2functionsza2z00zzengine_paramz00
				= BNIL;
			{	/* Engine/param.scm 1138 */
				obj_t BgL_idz00_650;

				BgL_idz00_650 = CNST_TABLE_REF(233);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_651;

					BgL_arg1233z00_651 =
						MAKE_YOUNG_PAIR(BgL_idz00_650, BGl_string1592z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_651,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2sawzd2spillza2zd2zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 1145 */
				obj_t BgL_idz00_652;

				BgL_idz00_652 = CNST_TABLE_REF(234);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_653;

					BgL_arg1233z00_653 =
						MAKE_YOUNG_PAIR(BgL_idz00_652, BGl_string1593z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_653,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2sawzd2bbvzf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 1152 */
				obj_t BgL_idz00_654;

				BgL_idz00_654 = CNST_TABLE_REF(235);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_655;

					BgL_arg1233z00_655 =
						MAKE_YOUNG_PAIR(BgL_idz00_654, BGl_string1594z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_655,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2sawzd2bbvzd2functionsza2z00zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 1159 */
				obj_t BgL_idz00_656;

				BgL_idz00_656 = CNST_TABLE_REF(236);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_657;

					BgL_arg1233z00_657 =
						MAKE_YOUNG_PAIR(BgL_idz00_656, BGl_string1595z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_657,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2globalzd2tailzd2callzf3za2zf3zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 1166 */
				obj_t BgL_idz00_658;

				BgL_idz00_658 = CNST_TABLE_REF(237);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_659;

					BgL_arg1233z00_659 =
						MAKE_YOUNG_PAIR(BgL_idz00_658, BGl_string1596z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_659,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2builtinzd2allocatorsza2zd2zzengine_paramz00 = CNST_TABLE_REF(238);
			BGl_za2evalzd2optionsza2zd2zzengine_paramz00 = BNIL;
			{	/* Engine/param.scm 1186 */
				obj_t BgL_idz00_660;

				BgL_idz00_660 = CNST_TABLE_REF(239);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_661;

					BgL_arg1233z00_661 =
						MAKE_YOUNG_PAIR(BgL_idz00_660, BGl_string1597z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_661,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2allowzd2typezd2redefinitionza2z00zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 1193 */
				obj_t BgL_idz00_662;

				BgL_idz00_662 = CNST_TABLE_REF(240);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_663;

					BgL_arg1233z00_663 =
						MAKE_YOUNG_PAIR(BgL_idz00_662, BGl_string1598z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_663,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2localzd2exitzf3za2z21zzengine_paramz00 = BFALSE;
			{	/* Engine/param.scm 1200 */
				obj_t BgL_idz00_664;

				BgL_idz00_664 = CNST_TABLE_REF(241);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_665;

					BgL_arg1233z00_665 =
						MAKE_YOUNG_PAIR(BgL_idz00_664, BGl_string1599z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_665,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			BGl_za2prezd2processorza2zd2zzengine_paramz00 =
				BGl_proc1600z00zzengine_paramz00;
			{	/* Engine/param.scm 1207 */
				obj_t BgL_idz00_666;

				BgL_idz00_666 = CNST_TABLE_REF(242);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_667;

					BgL_arg1233z00_667 =
						MAKE_YOUNG_PAIR(BgL_idz00_666, BGl_string1601z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_667,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			{	/* Engine/param.scm 1214 */
				obj_t BgL_idz00_668;

				BgL_idz00_668 = CNST_TABLE_REF(243);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_669;

					BgL_arg1233z00_669 =
						MAKE_YOUNG_PAIR(BgL_idz00_668, BGl_string1602z00zzengine_paramz00);
					BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_669,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00);
			}}
			{	/* Engine/param.scm 1215 */
				obj_t BgL_idz00_670;

				BgL_idz00_670 = CNST_TABLE_REF(244);
				{	/* Engine/param.scm 265 */
					obj_t BgL_arg1233z00_671;

					BgL_arg1233z00_671 =
						MAKE_YOUNG_PAIR(BgL_idz00_670, BGl_string1603z00zzengine_paramz00);
					return (BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_arg1233z00_671,
							BGl_za2bigloozd2variablesza2zd2zzengine_paramz00), BUNSPEC);
				}
			}
		}

	}



/* &<@*pre-processor*:1229> */
	obj_t BGl_z62zc3z04za2prezd2processorza2za31229ze3z37zzengine_paramz00(obj_t
		BgL_envz00_786, obj_t BgL_xz00_787)
	{
		{	/* Engine/param.scm 1207 */
			return BgL_xz00_787;
		}

	}



/* &<@anonymous:1227> */
	obj_t BGl_z62zc3z04anonymousza31227ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_788)
	{
		{	/* Engine/param.scm 1089 */
			{	/* Engine/param.scm 1091 */
				obj_t BgL_arg1228z00_885;

				BgL_arg1228z00_885 =
					BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(225));
				return (BGl_za2targetzd2languageza2zd2zzengine_paramz00 =
					bstring_to_symbol(((obj_t) BgL_arg1228z00_885)), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1216> */
	obj_t BGl_z62zc3z04anonymousza31216ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_789)
	{
		{	/* Engine/param.scm 1032 */
			{	/* Engine/param.scm 1034 */
				obj_t BgL_list1220z00_886;

				{	/* Engine/param.scm 1034 */
					obj_t BgL_arg1221z00_887;

					{	/* Engine/param.scm 1034 */
						obj_t BgL_arg1223z00_888;

						BgL_arg1223z00_888 =
							MAKE_YOUNG_PAIR(string_to_bstring(SHARED_LIB_SUFFIX), BNIL);
						BgL_arg1221z00_887 =
							MAKE_YOUNG_PAIR(string_to_bstring(STATIC_LIB_SUFFIX),
							BgL_arg1223z00_888);
					}
					BgL_list1220z00_886 =
						MAKE_YOUNG_PAIR
						(BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00,
						BgL_arg1221z00_887);
				}
				return (BGl_za2objzd2suffixza2zd2zzengine_paramz00 =
					BgL_list1220z00_886, BUNSPEC);
			}
		}

	}



/* &<@*inlining-reduce-k1203> */
	obj_t BGl_z62zc3z04za2inliningzd2reducezd2k1203ze3ze4zzengine_paramz00(obj_t
		BgL_envz00_790, obj_t BgL_kfactorz00_791)
	{
		{	/* Engine/param.scm 970 */
			return BINT(((long) CINT(BgL_kfactorz00_791) / 2L));
		}

	}



/* &<@*inlining-kfactor*1202> */
	obj_t BGl_z62zc3z04za2inliningzd2kfactorza21202ze3z94zzengine_paramz00(obj_t
		BgL_envz00_792, obj_t BgL_olevelz00_793)
	{
		{	/* Engine/param.scm 967 */
			return BINT((2L * (long) CINT(BgL_olevelz00_793)));
		}

	}



/* &<@anonymous:1201> */
	obj_t BGl_z62zc3z04anonymousza31201ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_794)
	{
		{	/* Engine/param.scm 848 */
			return (BGl_za2czd2splitzd2stringza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(160)), BUNSPEC);
		}

	}



/* &<@anonymous:1199> */
	obj_t BGl_z62zc3z04anonymousza31199ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_795)
	{
		{	/* Engine/param.scm 732 */
			return (BGl_za2jvmzd2javaza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(123)), BUNSPEC);
		}

	}



/* &<@anonymous:1197> */
	obj_t BGl_z62zc3z04anonymousza31197ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_796)
	{
		{	/* Engine/param.scm 729 */
			return (BGl_za2jvmzd2shellza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(121)), BUNSPEC);
		}

	}



/* &<@anonymous:1194> */
	obj_t BGl_z62zc3z04anonymousza31194ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_797)
	{
		{	/* Engine/param.scm 643 */
			return (BGl_za2czd2debugzd2optionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(98)), BUNSPEC);
		}

	}



/* &<@anonymous:1192> */
	obj_t BGl_z62zc3z04anonymousza31192ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_798)
	{
		{	/* Engine/param.scm 598 */
			return (BGl_za2indentza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(85)), BUNSPEC);
		}

	}



/* &<@anonymous:1190> */
	obj_t BGl_z62zc3z04anonymousza31190ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_799)
	{
		{	/* Engine/param.scm 574 */
			return (BGl_za2heapzd2wasmzd2nameza2z00zzengine_paramz00 =
				string_append(BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00,
					BGl_string1440z00zzengine_paramz00), BUNSPEC);
		}

	}



/* &<@anonymous:1188> */
	obj_t BGl_z62zc3z04anonymousza31188ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_800)
	{
		{	/* Engine/param.scm 570 */
			return (BGl_za2heapzd2jvmzd2nameza2z00zzengine_paramz00 =
				string_append(BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00,
					BGl_string1437z00zzengine_paramz00), BUNSPEC);
		}

	}



/* &<@anonymous:1183> */
	obj_t BGl_z62zc3z04anonymousza31183ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_801)
	{
		{	/* Engine/param.scm 562 */
			return (BGl_za2heapzd2nameza2zd2zzengine_paramz00 =
				string_append(BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00,
					BGl_string1433z00zzengine_paramz00), BUNSPEC);
		}

	}



/* &<@anonymous:1172> */
	obj_t BGl_z62zc3z04anonymousza31172ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_802)
	{
		{	/* Engine/param.scm 550 */
			{	/* Engine/param.scm 552 */
				obj_t BgL_list1173z00_889;

				BgL_list1173z00_889 =
					MAKE_YOUNG_PAIR(BGl_string1427z00zzengine_paramz00, BNIL);
				return (BGl_za2includezd2foreignza2zd2zzengine_paramz00 =
					BgL_list1173z00_889, BUNSPEC);
			}
		}

	}



/* &<@anonymous:1165> */
	obj_t BGl_z62zc3z04anonymousza31165ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_803)
	{
		{	/* Engine/param.scm 542 */
			return (BGl_za2loadzd2pathza2zd2zz__evalz00 =
				BGl_appendzd221011zd2zzengine_paramz00
				(BGl_za2oldzd2loadzd2pathza2z00zzengine_paramz00,
					BGl_za2libzd2dirza2zd2zzengine_paramz00), BUNSPEC);
		}

	}



/* &<@anonymous:1158> */
	obj_t BGl_z62zc3z04anonymousza31158ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_804)
	{
		{	/* Engine/param.scm 524 */
			return (BGl_za2bigloozd2userzd2libza2z00zzengine_paramz00 =
				BGl_stringzd2splitzd2charz00zztools_miscz00
				(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(66)),
					BCHAR(((unsigned char) ' '))), BUNSPEC);
		}

	}



/* &<@anonymous:1153> */
	obj_t BGl_z62zc3z04anonymousza31153ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_805)
	{
		{	/* Engine/param.scm 508 */
			return (BGl_za2bigloozd2abortzf3za2z21zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(61)), BUNSPEC);
		}

	}



/* &<@anonymous:1150> */
	obj_t BGl_z62zc3z04anonymousza31150ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_806)
	{
		{	/* Engine/param.scm 496 */
			return (BGl_za2gczd2customzf3za2z21zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(55)), BUNSPEC);
		}

	}



/* &<@anonymous:1142> */
	obj_t BGl_z62zc3z04anonymousza31142ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_807)
	{
		{	/* Engine/param.scm 490 */
			{	/* Engine/param.scm 492 */
				bool_t BgL_test1913z00_1777;

				{	/* Engine/param.scm 492 */
					obj_t BgL_tmpz00_1778;

					BgL_tmpz00_1778 =
						BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(55));
					BgL_test1913z00_1777 = STRINGP(BgL_tmpz00_1778);
				}
				if (BgL_test1913z00_1777)
					{	/* Engine/param.scm 493 */
						obj_t BgL_arg1143z00_890;

						BgL_arg1143z00_890 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(55));
						return (BGl_za2gczd2libza2zd2zzengine_paramz00 =
							bstring_to_symbol(((obj_t) BgL_arg1143z00_890)), BUNSPEC);
					}
				else
					{	/* Engine/param.scm 494 */
						obj_t BgL_arg1145z00_891;

						BgL_arg1145z00_891 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(56));
						return (BGl_za2gczd2libza2zd2zzengine_paramz00 =
							bstring_to_symbol(((obj_t) BgL_arg1145z00_891)), BUNSPEC);
					}
			}
		}

	}



/* &<@anonymous:1130> */
	obj_t BGl_z62zc3z04anonymousza31130ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_808)
	{
		{	/* Engine/param.scm 482 */
			return (BGl_za2libzd2srczd2dirza2z00zzengine_paramz00 =
				BGl_makezd2filezd2namez00zz__osz00(CAR
					(BGl_za2libzd2dirza2zd2zzengine_paramz00),
					BGl_string1406z00zzengine_paramz00), BUNSPEC);
		}

	}



/* &<@anonymous:1126> */
	obj_t BGl_z62zc3z04anonymousza31126ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_809)
	{
		{	/* Engine/param.scm 478 */
			return (BGl_za2ldzd2libraryzd2dirza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(24)), BUNSPEC);
		}

	}



/* &<@anonymous:1124> */
	obj_t BGl_z62zc3z04anonymousza31124ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_810)
	{
		{	/* Engine/param.scm 475 */
			return (BGl_za2defaultzd2libzd2dirza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(24)), BUNSPEC);
		}

	}



/* &<@anonymous:1104> */
	obj_t BGl_z62zc3z04anonymousza31104ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_811)
	{
		{	/* Engine/param.scm 469 */
			{	/* Engine/param.scm 471 */
				obj_t BgL_libzd2envzd2_892;

				BgL_libzd2envzd2_892 =
					BGl_buildzd2pathzd2fromzd2shellzd2variablez00zztools_miscz00
					(BGl_string1399z00zzengine_paramz00);
				if (NULLP(BgL_libzd2envzd2_892))
					{	/* Engine/param.scm 473 */
						obj_t BgL_arg1114z00_893;

						BgL_arg1114z00_893 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(24));
						{	/* Engine/param.scm 473 */
							obj_t BgL_list1115z00_894;

							{	/* Engine/param.scm 473 */
								obj_t BgL_arg1122z00_895;

								BgL_arg1122z00_895 = MAKE_YOUNG_PAIR(BgL_arg1114z00_893, BNIL);
								BgL_list1115z00_894 =
									MAKE_YOUNG_PAIR(BGl_string1400z00zzengine_paramz00,
									BgL_arg1122z00_895);
							}
							return (BGl_za2libzd2dirza2zd2zzengine_paramz00 =
								BgL_list1115z00_894, BUNSPEC);
						}
					}
				else
					{	/* Engine/param.scm 472 */
						return (BGl_za2libzd2dirza2zd2zzengine_paramz00 =
							MAKE_YOUNG_PAIR(BGl_string1400z00zzengine_paramz00,
								BgL_libzd2envzd2_892), BUNSPEC);
					}
			}
		}

	}



/* &<@anonymous:1091> */
	obj_t BGl_z62zc3z04anonymousza31091ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_812)
	{
		{	/* Engine/param.scm 449 */
			return (BGl_za2ldzd2optimzd2flagsza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(43)), BUNSPEC);
		}

	}



/* &<@anonymous:1088> */
	obj_t BGl_z62zc3z04anonymousza31088ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_813)
	{
		{	/* Engine/param.scm 445 */
			return (BGl_za2ldzd2debugzd2optionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(41)), BUNSPEC);
		}

	}



/* &<@anonymous:1085> */
	obj_t BGl_z62zc3z04anonymousza31085ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_814)
	{
		{	/* Engine/param.scm 441 */
			return (BGl_za2ldzd2ozd2optionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(39)), BUNSPEC);
		}

	}



/* &<@anonymous:1083> */
	obj_t BGl_z62zc3z04anonymousza31083ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_815)
	{
		{	/* Engine/param.scm 437 */
			return (BGl_za2ldzd2optionsza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(37)), BUNSPEC);
		}

	}



/* &<@anonymous:1081> */
	obj_t BGl_z62zc3z04anonymousza31081ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_816)
	{
		{	/* Engine/param.scm 433 */
			return (BGl_za2ldzd2styleza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(35)), BUNSPEC);
		}

	}



/* &<@anonymous:1079> */
	obj_t BGl_z62zc3z04anonymousza31079ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_817)
	{
		{	/* Engine/param.scm 421 */
			return (BGl_za2stdcza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(30)), BUNSPEC);
		}

	}



/* &<@anonymous:1077> */
	obj_t BGl_z62zc3z04anonymousza31077ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_818)
	{
		{	/* Engine/param.scm 417 */
			return (BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(28)), BUNSPEC);
		}

	}



/* &<@anonymous:1075> */
	obj_t BGl_z62zc3z04anonymousza31075ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_819)
	{
		{	/* Engine/param.scm 413 */
			return (BGl_za2cczd2ozd2optionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(26)), BUNSPEC);
		}

	}



/* &<@anonymous:1066> */
	obj_t BGl_z62zc3z04anonymousza31066ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_820)
	{
		{	/* Engine/param.scm 409 */
			{	/* Engine/param.scm 411 */
				obj_t BgL_arg1068z00_896;

				BgL_arg1068z00_896 =
					BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(24));
				{	/* Engine/param.scm 411 */
					obj_t BgL_list1069z00_897;

					BgL_list1069z00_897 = MAKE_YOUNG_PAIR(BgL_arg1068z00_896, BNIL);
					return (BGl_za2cflagszd2rpathza2zd2zzengine_paramz00 =
						BgL_list1069z00_897, BUNSPEC);
				}
			}
		}

	}



/* &<@anonymous:1060> */
	obj_t BGl_z62zc3z04anonymousza31060ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_821)
	{
		{	/* Engine/param.scm 405 */
			return (BGl_za2cflagszd2profza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(22)), BUNSPEC);
		}

	}



/* &<@anonymous:1058> */
	obj_t BGl_z62zc3z04anonymousza31058ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_822)
	{
		{	/* Engine/param.scm 401 */
			return (BGl_za2cflagszd2optimza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(20)), BUNSPEC);
		}

	}



/* &<@anonymous:1056> */
	obj_t BGl_z62zc3z04anonymousza31056ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_823)
	{
		{	/* Engine/param.scm 397 */
			return (BGl_za2cflagsza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(18)), BUNSPEC);
		}

	}



/* &<@anonymous:1054> */
	obj_t BGl_z62zc3z04anonymousza31054ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_824)
	{
		{	/* Engine/param.scm 393 */
			return (BGl_za2ccza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(16)), BUNSPEC);
		}

	}



/* &<@anonymous:1052> */
	obj_t BGl_z62zc3z04anonymousza31052ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_825)
	{
		{	/* Engine/param.scm 389 */
			return (BGl_za2cczd2styleza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(14)), BUNSPEC);
		}

	}



/* &<@anonymous:1050> */
	obj_t BGl_z62zc3z04anonymousza31050ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_826)
	{
		{	/* Engine/param.scm 385 */
			return (BGl_za2shellza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(12)), BUNSPEC);
		}

	}



/* &<@anonymous:1048> */
	obj_t BGl_z62zc3z04anonymousza31048ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_827)
	{
		{	/* Engine/param.scm 346 */
			{	/* Engine/param.scm 348 */
				obj_t BgL_tmpz00_898;

				BgL_tmpz00_898 =
					BGl_getenvz00zz__osz00(BGl_string1355z00zzengine_paramz00);
				if (STRINGP(BgL_tmpz00_898))
					{	/* Engine/param.scm 349 */
						return (BGl_za2bigloozd2tmpza2zd2zzengine_paramz00 =
							BgL_tmpz00_898, BUNSPEC);
					}
				else
					{	/* Engine/param.scm 349 */
						return (BGl_za2bigloozd2tmpza2zd2zzengine_paramz00 =
							string_to_bstring(OS_TMP), BUNSPEC);
					}
			}
		}

	}



/* &<@anonymous:1039> */
	obj_t BGl_z62zc3z04anonymousza31039ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_828)
	{
		{	/* Engine/param.scm 331 */
			{	/* Engine/param.scm 333 */
				obj_t BgL_list1040z00_899;

				{	/* Engine/param.scm 333 */
					obj_t BgL_arg1041z00_900;

					{	/* Engine/param.scm 333 */
						obj_t BgL_arg1042z00_901;

						{	/* Engine/param.scm 333 */
							obj_t BgL_arg1044z00_902;

							{	/* Engine/param.scm 333 */
								obj_t BgL_arg1045z00_903;

								BgL_arg1045z00_903 =
									MAKE_YOUNG_PAIR(BGl_string1347z00zzengine_paramz00, BNIL);
								BgL_arg1044z00_902 =
									MAKE_YOUNG_PAIR
									(BGl_za2bigloozd2versionza2zd2zzengine_paramz00,
									BgL_arg1045z00_903);
							}
							BgL_arg1042z00_901 =
								MAKE_YOUNG_PAIR(BGl_string1348z00zzengine_paramz00,
								BgL_arg1044z00_902);
						}
						BgL_arg1041z00_900 =
							MAKE_YOUNG_PAIR
							(BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00,
							BgL_arg1042z00_901);
					}
					BgL_list1040z00_899 =
						MAKE_YOUNG_PAIR(BGl_string1349z00zzengine_paramz00,
						BgL_arg1041z00_900);
				}
				return (BGl_za2bigloozd2nameza2zd2zzengine_paramz00 =
					BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1040z00_899),
					BUNSPEC);
			}
		}

	}



/* &<@anonymous:1031> */
	obj_t BGl_z62zc3z04anonymousza31031ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_829)
	{
		{	/* Engine/param.scm 327 */
			return (BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(2)), BUNSPEC);
		}

	}



/* &<@anonymous:1029> */
	obj_t BGl_z62zc3z04anonymousza31029ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_830)
	{
		{	/* Engine/param.scm 323 */
			return (BGl_za2bigloozd2versionza2zd2zzengine_paramz00 =
				BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0)), BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzengine_paramz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_185;

				BgL_headz00_185 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_186;
					obj_t BgL_tailz00_187;

					BgL_prevz00_186 = BgL_headz00_185;
					BgL_tailz00_187 = BgL_l1z00_1;
				BgL_loopz00_188:
					if (PAIRP(BgL_tailz00_187))
						{
							obj_t BgL_newzd2prevzd2_190;

							BgL_newzd2prevzd2_190 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_187), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_186, BgL_newzd2prevzd2_190);
							{
								obj_t BgL_tailz00_1856;
								obj_t BgL_prevz00_1855;

								BgL_prevz00_1855 = BgL_newzd2prevzd2_190;
								BgL_tailz00_1856 = CDR(BgL_tailz00_187);
								BgL_tailz00_187 = BgL_tailz00_1856;
								BgL_prevz00_186 = BgL_prevz00_1855;
								goto BgL_loopz00_188;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_185);
				}
			}
		}

	}



/* &append-21011 */
	obj_t BGl_z62appendzd221011zb0zzengine_paramz00(obj_t BgL_envz00_831,
		obj_t BgL_l1z00_832, obj_t BgL_l2z00_833)
	{
		{
			return
				BGl_appendzd221011zd2zzengine_paramz00(BgL_l1z00_832, BgL_l2z00_833);
		}

	}



/* &add-doc-variable! */
	obj_t BGl_z62addzd2doczd2variablez12z70zzengine_paramz00(obj_t BgL_envz00_834,
		obj_t BgL_idz00_835, obj_t BgL_docz00_836)
	{
		{	/* Engine/param.scm 264 */
			{	/* Engine/param.scm 265 */
				obj_t BgL_arg1233z00_904;

				BgL_arg1233z00_904 = MAKE_YOUNG_PAIR(BgL_idz00_835, BgL_docz00_836);
				return (BGl_za2bigloozd2variablesza2zd2zzengine_paramz00 =
					MAKE_YOUNG_PAIR(BgL_arg1233z00_904,
						BGl_za2bigloozd2variablesza2zd2zzengine_paramz00), BUNSPEC);
			}
		}

	}



/* &add-updater! */
	obj_t BGl_z62addzd2updaterz12za2zzengine_paramz00(obj_t BgL_envz00_837,
		obj_t BgL_procz00_838)
	{
		{	/* Engine/param.scm 271 */
			return (BGl_za2paramzd2updatersza2zd2zzengine_paramz00 =
				MAKE_YOUNG_PAIR(BgL_procz00_838,
					BGl_za2paramzd2updatersza2zd2zzengine_paramz00), BUNSPEC);
		}

	}



/* reinitialize-bigloo-variables! */
	BGL_EXPORTED_DEF obj_t
		BGl_reinitializa7ezd2bigloozd2variablesz12zb5zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 274 */
			{	/* Engine/param.scm 275 */
				obj_t BgL_g1018z00_194;

				BgL_g1018z00_194 =
					bgl_reverse(BGl_za2paramzd2updatersza2zd2zzengine_paramz00);
				{
					obj_t BgL_l1016z00_196;

					{	/* Engine/param.scm 275 */
						bool_t BgL_tmpz00_1864;

						BgL_l1016z00_196 = BgL_g1018z00_194;
					BgL_zc3z04anonymousza31234ze3z87_197:
						if (PAIRP(BgL_l1016z00_196))
							{	/* Engine/param.scm 275 */
								{	/* Engine/param.scm 275 */
									obj_t BgL_procz00_199;

									BgL_procz00_199 = CAR(BgL_l1016z00_196);
									BGL_PROCEDURE_CALL0(BgL_procz00_199);
								}
								{
									obj_t BgL_l1016z00_1871;

									BgL_l1016z00_1871 = CDR(BgL_l1016z00_196);
									BgL_l1016z00_196 = BgL_l1016z00_1871;
									goto BgL_zc3z04anonymousza31234ze3z87_197;
								}
							}
						else
							{	/* Engine/param.scm 275 */
								BgL_tmpz00_1864 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_1864);
					}
				}
			}
		}

	}



/* &reinitialize-bigloo-variables! */
	obj_t BGl_z62reinitializa7ezd2bigloozd2variablesz12zd7zzengine_paramz00(obj_t
		BgL_envz00_839)
	{
		{	/* Engine/param.scm 274 */
			return BGl_reinitializa7ezd2bigloozd2variablesz12zb5zzengine_paramz00();
		}

	}



/* bigloo-variables-usage */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2variableszd2usagez00zzengine_paramz00(bool_t
		BgL_manualzf3zf3_6)
	{
		{	/* Engine/param.scm 292 */
			{	/* Engine/param.scm 293 */
				obj_t BgL_port1019z00_202;

				{	/* Engine/param.scm 293 */
					obj_t BgL_tmpz00_1875;

					BgL_tmpz00_1875 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1019z00_202 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1875);
				}
				bgl_display_string(BGl_string1604z00zzengine_paramz00,
					BgL_port1019z00_202);
				bgl_display_char(((unsigned char) 10), BgL_port1019z00_202);
			}
			{	/* Engine/param.scm 294 */
				obj_t BgL_port1020z00_203;

				{	/* Engine/param.scm 294 */
					obj_t BgL_tmpz00_1880;

					BgL_tmpz00_1880 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1020z00_203 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1880);
				}
				bgl_display_string(BGl_string1605z00zzengine_paramz00,
					BgL_port1020z00_203);
				bgl_display_char(((unsigned char) 10), BgL_port1020z00_203);
			}
			{	/* Engine/param.scm 295 */
				obj_t BgL_port1021z00_204;

				{	/* Engine/param.scm 295 */
					obj_t BgL_tmpz00_1885;

					BgL_tmpz00_1885 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1021z00_204 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1885);
				}
				bgl_display_string(BGl_string1606z00zzengine_paramz00,
					BgL_port1021z00_204);
				bgl_display_char(((unsigned char) 10), BgL_port1021z00_204);
			}
			{	/* Engine/param.scm 296 */
				obj_t BgL_port1022z00_205;

				{	/* Engine/param.scm 296 */
					obj_t BgL_tmpz00_1890;

					BgL_tmpz00_1890 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1022z00_205 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1890);
				}
				bgl_display_string(BGl_string1607z00zzengine_paramz00,
					BgL_port1022z00_205);
				bgl_display_char(((unsigned char) 10), BgL_port1022z00_205);
			}
			{	/* Engine/param.scm 297 */
				obj_t BgL_port1023z00_206;

				{	/* Engine/param.scm 297 */
					obj_t BgL_tmpz00_1895;

					BgL_tmpz00_1895 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1023z00_206 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1895);
				}
				bgl_display_string(BGl_string1608z00zzengine_paramz00,
					BgL_port1023z00_206);
				bgl_display_char(((unsigned char) 10), BgL_port1023z00_206);
			}
			{	/* Engine/param.scm 298 */
				obj_t BgL_port1024z00_207;

				{	/* Engine/param.scm 298 */
					obj_t BgL_tmpz00_1900;

					BgL_tmpz00_1900 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1024z00_207 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1900);
				}
				bgl_display_string(BGl_string1609z00zzengine_paramz00,
					BgL_port1024z00_207);
				bgl_display_char(((unsigned char) 10), BgL_port1024z00_207);
			}
			{	/* Engine/param.scm 299 */
				obj_t BgL_arg1238z00_208;

				{	/* Engine/param.scm 299 */
					obj_t BgL_tmpz00_1905;

					BgL_tmpz00_1905 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1238z00_208 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1905);
				}
				bgl_display_char(((unsigned char) 10), BgL_arg1238z00_208);
			}
			{	/* Engine/param.scm 300 */
				obj_t BgL_g1015z00_209;

				BgL_g1015z00_209 =
					BGl_sortz00zz__r4_vectors_6_8z00
					(BGl_za2bigloozd2variablesza2zd2zzengine_paramz00,
					BGl_proc1610z00zzengine_paramz00);
				{
					obj_t BgL_lz00_211;

					{	/* Engine/param.scm 300 */
						bool_t BgL_tmpz00_1910;

						BgL_lz00_211 = BgL_g1015z00_209;
					BgL_zc3z04anonymousza31239ze3z87_212:
						if (PAIRP(BgL_lz00_211))
							{	/* Engine/param.scm 305 */
								obj_t BgL_varz00_214;

								BgL_varz00_214 = CAR(BgL_lz00_211);
								if (BgL_manualzf3zf3_6)
									{	/* Engine/param.scm 306 */
										{	/* Engine/param.scm 308 */
											obj_t BgL_port1025z00_215;

											{	/* Engine/param.scm 308 */
												obj_t BgL_tmpz00_1915;

												BgL_tmpz00_1915 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_port1025z00_215 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1915);
											}
											bgl_display_string(BGl_string1611z00zzengine_paramz00,
												BgL_port1025z00_215);
											{	/* Engine/param.scm 308 */
												obj_t BgL_arg1242z00_216;

												BgL_arg1242z00_216 = CAR(((obj_t) BgL_varz00_214));
												bgl_display_obj(BgL_arg1242z00_216,
													BgL_port1025z00_215);
											}
											bgl_display_string(BGl_string1612z00zzengine_paramz00,
												BgL_port1025z00_215);
											bgl_display_char(((unsigned char) 10),
												BgL_port1025z00_215);
										}
										{	/* Engine/param.scm 309 */
											obj_t BgL_port1026z00_217;

											{	/* Engine/param.scm 309 */
												obj_t BgL_tmpz00_1924;

												BgL_tmpz00_1924 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_port1026z00_217 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1924);
											}
											bgl_display_string(BGl_string1613z00zzengine_paramz00,
												BgL_port1026z00_217);
											{	/* Engine/param.scm 309 */
												obj_t BgL_arg1244z00_218;

												BgL_arg1244z00_218 = CDR(((obj_t) BgL_varz00_214));
												bgl_display_obj(BgL_arg1244z00_218,
													BgL_port1026z00_217);
											}
											bgl_display_char(((unsigned char) 10),
												BgL_port1026z00_217);
										}
										{	/* Engine/param.scm 310 */
											obj_t BgL_arg1248z00_219;

											{	/* Engine/param.scm 310 */
												obj_t BgL_tmpz00_1932;

												BgL_tmpz00_1932 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg1248z00_219 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1932);
											}
											bgl_display_string(BGl_string1614z00zzengine_paramz00,
												BgL_arg1248z00_219);
										}
										{	/* Engine/param.scm 311 */
											obj_t BgL_arg1249z00_220;

											{	/* Engine/param.scm 311 */
												obj_t BgL_arg1252z00_222;

												BgL_arg1252z00_222 = CAR(((obj_t) BgL_varz00_214));
												{	/* Engine/param.scm 311 */
													obj_t BgL_envz00_224;

													BgL_envz00_224 =
														BGl_defaultzd2environmentzd2zz__evalz00();
													{	/* Engine/param.scm 311 */

														BgL_arg1249z00_220 =
															BGl_evalz00zz__evalz00(BgL_arg1252z00_222,
															BgL_envz00_224);
											}}}
											BGl_writez00zz__r4_output_6_10_3z00(BgL_arg1249z00_220,
												BNIL);
										}
										{	/* Engine/param.scm 312 */
											obj_t BgL_arg1268z00_225;

											{	/* Engine/param.scm 312 */
												obj_t BgL_tmpz00_1941;

												BgL_tmpz00_1941 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg1268z00_225 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1941);
											}
											bgl_display_char(((unsigned char) 10),
												BgL_arg1268z00_225);
									}}
								else
									{	/* Engine/param.scm 306 */
										{	/* Engine/param.scm 314 */
											obj_t BgL_arg1272z00_226;
											obj_t BgL_arg1284z00_227;

											BgL_arg1272z00_226 = CAR(((obj_t) BgL_varz00_214));
											BgL_arg1284z00_227 = CDR(((obj_t) BgL_varz00_214));
											{	/* Engine/param.scm 314 */
												obj_t BgL_list1285z00_228;

												{	/* Engine/param.scm 314 */
													obj_t BgL_arg1304z00_229;

													{	/* Engine/param.scm 314 */
														obj_t BgL_arg1305z00_230;

														{	/* Engine/param.scm 314 */
															obj_t BgL_arg1306z00_231;

															{	/* Engine/param.scm 314 */
																obj_t BgL_arg1307z00_232;

																BgL_arg1307z00_232 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1615z00zzengine_paramz00, BNIL);
																BgL_arg1306z00_231 =
																	MAKE_YOUNG_PAIR(BgL_arg1284z00_227,
																	BgL_arg1307z00_232);
															}
															BgL_arg1305z00_230 =
																MAKE_YOUNG_PAIR
																(BGl_string1612z00zzengine_paramz00,
																BgL_arg1306z00_231);
														}
														BgL_arg1304z00_229 =
															MAKE_YOUNG_PAIR(BgL_arg1272z00_226,
															BgL_arg1305z00_230);
													}
													BgL_list1285z00_228 =
														MAKE_YOUNG_PAIR(BGl_string1611z00zzengine_paramz00,
														BgL_arg1304z00_229);
												}
												BGl_displayza2za2zz__r4_output_6_10_3z00
													(BgL_list1285z00_228);
											}
										}
										{	/* Engine/param.scm 315 */
											obj_t BgL_arg1308z00_233;

											{	/* Engine/param.scm 315 */
												obj_t BgL_arg1310z00_235;

												BgL_arg1310z00_235 = CAR(((obj_t) BgL_varz00_214));
												{	/* Engine/param.scm 315 */
													obj_t BgL_envz00_237;

													BgL_envz00_237 =
														BGl_defaultzd2environmentzd2zz__evalz00();
													{	/* Engine/param.scm 315 */

														BgL_arg1308z00_233 =
															BGl_evalz00zz__evalz00(BgL_arg1310z00_235,
															BgL_envz00_237);
													}
												}
											}
											BGl_writez00zz__r4_output_6_10_3z00(BgL_arg1308z00_233,
												BNIL);
										}
										{	/* Engine/param.scm 316 */
											obj_t BgL_port1027z00_238;

											{	/* Engine/param.scm 316 */
												obj_t BgL_tmpz00_1960;

												BgL_tmpz00_1960 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_port1027z00_238 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1960);
											}
											bgl_display_string(BGl_string1616z00zzengine_paramz00,
												BgL_port1027z00_238);
											bgl_display_char(((unsigned char) 10),
												BgL_port1027z00_238);
									}}
								{
									obj_t BgL_lz00_1965;

									BgL_lz00_1965 = CDR(BgL_lz00_211);
									BgL_lz00_211 = BgL_lz00_1965;
									goto BgL_zc3z04anonymousza31239ze3z87_212;
								}
							}
						else
							{	/* Engine/param.scm 304 */
								BgL_tmpz00_1910 = ((bool_t) 0);
							}
						return BBOOL(BgL_tmpz00_1910);
					}
				}
			}
		}

	}



/* &bigloo-variables-usage */
	obj_t BGl_z62bigloozd2variableszd2usagez62zzengine_paramz00(obj_t
		BgL_envz00_841, obj_t BgL_manualzf3zf3_842)
	{
		{	/* Engine/param.scm 292 */
			return
				BGl_bigloozd2variableszd2usagez00zzengine_paramz00(CBOOL
				(BgL_manualzf3zf3_842));
		}

	}



/* &<@anonymous:1313> */
	obj_t BGl_z62zc3z04anonymousza31313ze3ze5zzengine_paramz00(obj_t
		BgL_envz00_843, obj_t BgL_xz00_844, obj_t BgL_yz00_845)
	{
		{	/* Engine/param.scm 301 */
			{	/* Engine/param.scm 302 */
				bool_t BgL_tmpz00_1970;

				{	/* Engine/param.scm 302 */
					obj_t BgL_arg1314z00_905;
					obj_t BgL_arg1315z00_906;

					{	/* Engine/param.scm 302 */
						obj_t BgL_arg1316z00_907;

						BgL_arg1316z00_907 = CAR(((obj_t) BgL_xz00_844));
						{	/* Engine/param.scm 302 */
							obj_t BgL_arg1455z00_908;

							BgL_arg1455z00_908 =
								SYMBOL_TO_STRING(((obj_t) BgL_arg1316z00_907));
							BgL_arg1314z00_905 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_908);
						}
					}
					{	/* Engine/param.scm 303 */
						obj_t BgL_arg1317z00_909;

						BgL_arg1317z00_909 = CAR(((obj_t) BgL_yz00_845));
						{	/* Engine/param.scm 303 */
							obj_t BgL_arg1455z00_910;

							BgL_arg1455z00_910 =
								SYMBOL_TO_STRING(((obj_t) BgL_arg1317z00_909));
							BgL_arg1315z00_906 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_910);
						}
					}
					BgL_tmpz00_1970 =
						bigloo_string_lt(BgL_arg1314z00_905, BgL_arg1315z00_906);
				}
				return BBOOL(BgL_tmpz00_1970);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_datez00(408802545L,
				BSTRING_TO_STRING(BGl_string1617z00zzengine_paramz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1617z00zzengine_paramz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_configurez00(272817175L,
				BSTRING_TO_STRING(BGl_string1617z00zzengine_paramz00));
		}

	}



/* eval-init */
	obj_t BGl_evalzd2initzd2zzengine_paramz00(void)
	{
		{	/* Engine/param.scm 15 */
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(148),
				__EVMEANING_ADDRESS(
					(BGl_za2warningzd2typezd2errorza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(196),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2reducezd2betazf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(141),
				__EVMEANING_ADDRESS((BGl_za2unsafezd2versionza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(241),
				__EVMEANING_ADDRESS((BGl_za2localzd2exitzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(177),
				__EVMEANING_ADDRESS((BGl_za2purifyza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(136),
				__EVMEANING_ADDRESS(
					(BGl_za2garbagezd2collectorza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(72),
				__EVMEANING_ADDRESS((BGl_za2includezd2foreignza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(96),
				__EVMEANING_ADDRESS((BGl_za2czd2debugza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(189),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2cfazd2forcezd2loosezd2localzd2functionzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(107),
				__EVMEANING_ADDRESS((BGl_za2accesszd2filesza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(218),
				__EVMEANING_ADDRESS(
					(BGl_za2mcozd2includezd2pathza2z00zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(245),
				BGl_stringzd2splitzd2charzd2envzd2zztools_miscz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(100),
				__EVMEANING_ADDRESS((BGl_za2czd2userzd2headerza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(157),
				__EVMEANING_ADDRESS((BGl_za2dlopenzd2initza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(178),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2envza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(102),
				__EVMEANING_ADDRESS((BGl_za2czd2tailzd2callza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(34),
				__EVMEANING_ADDRESS((BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(176),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2symbolzd2caseza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(224),
				__EVMEANING_ADDRESS((BGl_za2readerza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(88),
				__EVMEANING_ADDRESS(
					(BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(3),
				__EVMEANING_ADDRESS(
					(BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(246),
				__EVMEANING_ADDRESS(
					(BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(50),
				__EVMEANING_ADDRESS(
					(BGl_za2defaultzd2libzd2dirza2z00zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(247),
				BGl_epairifyzd2propagatezd2envz00zztools_miscz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(83),
				__EVMEANING_ADDRESS(
					(BGl_za2additionalzd2heapzd2nameza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(169),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2atomzd2inliningzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(125),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2optionsza2zd2zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(248),
				BGl_reinitializa7ezd2bigloozd2variablesz12zd2envz67zzengine_paramz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(82),
				__EVMEANING_ADDRESS(
					(BGl_za2jvmzd2foreignzd2classzd2nameza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(31),
				__EVMEANING_ADDRESS((BGl_za2stdcza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(231),
				__EVMEANING_ADDRESS(
					(BGl_za2sawzd2registerzd2allocationzd2maxzd2siza7eza2za7zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(230),
				__EVMEANING_ADDRESS(
					(BGl_za2sawzd2registerzd2allocationzd2onexpressionzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(99),
				__EVMEANING_ADDRESS(
					(BGl_za2czd2debugzd2optionza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(40),
				__EVMEANING_ADDRESS((BGl_za2ldzd2ozd2optionza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(109),
				__EVMEANING_ADDRESS(
					(BGl_za2qualifiedzd2typezd2fileza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(116),
				__EVMEANING_ADDRESS((BGl_za2callzf2cczf3za2z01zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(249),
				BGl_buildzd2pathzd2fromzd2shellzd2variablezd2envzd2zztools_miscz00);
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(250),
				BGl_addzd2doczd2variablez12zd2envzc0zzengine_paramz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(251),
				__EVMEANING_ADDRESS(
					(BGl_za2arithmeticzd2overflowza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(110),
				__EVMEANING_ADDRESS(
					(BGl_za2qualifiedzd2typezd2filezd2defaultza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(91),
				__EVMEANING_ADDRESS(
					(BGl_za2compilerzd2typezd2debugzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(252),
				__EVMEANING_ADDRESS((BGl_za2userzd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(143),
				__EVMEANING_ADDRESS((BGl_za2unsafezd2evalza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(185),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2cfazd2applyzd2trackingzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(237),
				__EVMEANING_ADDRESS(
					(BGl_za2globalzd2tailzd2callzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(253),
				BGl_stringza2zd2ze3stringzd2envz41zztools_miscz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(254),
				__EVMEANING_ADDRESS(
					(BGl_za2earlyzd2withzd2modulesza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(193),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2dataflowzd2typeszf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(255),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2dateza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(151),
				__EVMEANING_ADDRESS((BGl_za2sharedzd2cnstzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(114),
				__EVMEANING_ADDRESS((BGl_za2interpreterza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(131),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2directoryza2zd2zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(256),
				BGl_epairifyzd2reczd2envz00zztools_miscz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(257),
				__EVMEANING_ADDRESS(
					(BGl_za2arithmeticzd2genericityza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(CNST_TABLE_REF(179),
				__EVMEANING_ADDRESS((BGl_za2optimzd2jvmza2zd2zzengine_paramz00)),
				BGl_string1618z00zzengine_paramz00, BINT(26816L));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(258),
				__EVMEANING_ADDRESS(
					(BGl_za2arithmeticzd2newzd2overflowza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(86),
				__EVMEANING_ADDRESS((BGl_za2indentza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(259),
				__EVMEANING_ADDRESS(
					(BGl_za2typenamezd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(62),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2abortzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(260),
				__EVMEANING_ADDRESS((BGl_za2namezd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(261),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2authorza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(25),
				__EVMEANING_ADDRESS((BGl_za2cflagszd2rpathza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(93),
				__EVMEANING_ADDRESS((BGl_za2bmemzd2profilingza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(111),
				__EVMEANING_ADDRESS((BGl_za2ozd2filesza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(46),
				__EVMEANING_ADDRESS((BGl_za2cczd2moveza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(209),
				__EVMEANING_ADDRESS((BGl_za2srczd2suffixza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(191),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2dataflowzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(207),
				__EVMEANING_ADDRESS((BGl_za2extendzd2entryza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(233),
				__EVMEANING_ADDRESS(
					(BGl_za2sawzd2nozd2registerzd2allocationzd2functionsza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(214),
				__EVMEANING_ADDRESS((BGl_za2objzd2suffixza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(60),
				__EVMEANING_ADDRESS(
					(BGl_za2gczd2forcezd2registerzd2rootszf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(168),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2loopzd2inliningzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(242),
				__EVMEANING_ADDRESS((BGl_za2prezd2processorza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(137),
				__EVMEANING_ADDRESS((BGl_za2unsafezd2typeza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(144),
				__EVMEANING_ADDRESS((BGl_za2unsafezd2heapza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(67),
				__EVMEANING_ADDRESS(
					(BGl_za2bigloozd2userzd2libza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(142),
				__EVMEANING_ADDRESS((BGl_za2unsafezd2libraryza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(54),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2libza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(202),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(240),
				__EVMEANING_ADDRESS(
					(BGl_za2allowzd2typezd2redefinitionza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(195),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2synczd2failsafezf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(120),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2jarzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(200),
				__EVMEANING_ADDRESS(
					(BGl_za2inliningzd2reducezd2kfactorza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(23),
				__EVMEANING_ADDRESS((BGl_za2cflagszd2profza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(222),
				__EVMEANING_ADDRESS(
					(BGl_za2userzd2heapzd2siza7eza2za7zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(192),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2dataflowzd2forzd2errorszf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(49),
				__EVMEANING_ADDRESS((BGl_za2libzd2dirza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(149),
				__EVMEANING_ADDRESS(
					(BGl_za2warningzd2defaultzd2slotzd2valueza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(29),
				__EVMEANING_ADDRESS(
					(BGl_za2czd2objectzd2filezd2extensionza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(113),
				__EVMEANING_ADDRESS((BGl_za2withzd2filesza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(262),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2argsza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(165),
				__EVMEANING_ADDRESS(
					(BGl_za2tracezd2writezd2lengthza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(229),
				__EVMEANING_ADDRESS(
					(BGl_za2sawzd2registerzd2allocationzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(84),
				__EVMEANING_ADDRESS(
					(BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(105),
				__EVMEANING_ADDRESS((BGl_za2profilezd2modeza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(103),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2debugza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(216),
				__EVMEANING_ADDRESS((BGl_za2mcozd2suffixza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(163),
				__EVMEANING_ADDRESS((BGl_za2tracezd2nameza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(158),
				__EVMEANING_ADDRESS(
					(BGl_za2dlopenzd2initzd2gcza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(150),
				__EVMEANING_ADDRESS((BGl_za2profilezd2libraryza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(75),
				__EVMEANING_ADDRESS((BGl_za2heapzd2nameza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(4),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2nameza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(228),
				__EVMEANING_ADDRESS(
					(BGl_za2sawzd2registerzd2reallocationzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(263),
				__EVMEANING_ADDRESS(
					(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(239),
				__EVMEANING_ADDRESS((BGl_za2evalzd2optionsza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(175),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2jvmzd2fasteqza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(59),
				__EVMEANING_ADDRESS(
					(BGl_za2multizd2threadedzd2gczf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(264),
				BGl_replacez12zd2envzc0zztools_miscz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(211),
				__EVMEANING_ADDRESS((BGl_za2czd2suffixza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(184),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(CNST_TABLE_REF(265),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2variablesza2zd2zzengine_paramz00)),
				BGl_string1618z00zzengine_paramz00, BINT(7289L));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(194),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2initflowzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(71),
				__EVMEANING_ADDRESS(
					(BGl_za2includezd2multipleza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(132),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2catchza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(139),
				__EVMEANING_ADDRESS((BGl_za2unsafezd2rangeza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(236),
				__EVMEANING_ADDRESS(
					(BGl_za2sawzd2bbvzd2functionsza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(8),
				__EVMEANING_ADDRESS((BGl_za2verboseza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(128),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2mainclassza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(108),
				__EVMEANING_ADDRESS(
					(BGl_za2accesszd2filezd2defaultza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(235),
				__EVMEANING_ADDRESS((BGl_za2sawzd2bbvzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(1),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2versionza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(36),
				__EVMEANING_ADDRESS((BGl_za2ldzd2styleza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(21),
				__EVMEANING_ADDRESS((BGl_za2cflagszd2optimza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(45),
				__EVMEANING_ADDRESS(
					(BGl_za2ldzd2postzd2optionsza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(140),
				__EVMEANING_ADDRESS((BGl_za2unsafezd2structza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(164),
				__EVMEANING_ADDRESS((BGl_za2tracezd2levelza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(19),
				__EVMEANING_ADDRESS((BGl_za2cflagsza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(63),
				__EVMEANING_ADDRESS(
					(BGl_za2staticzd2bigloozf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(266),
				__EVMEANING_ADDRESS((BGl_za2alloczd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(CNST_TABLE_REF(267),
				__EVMEANING_ADDRESS((BGl_za2paramzd2updatersza2zd2zzengine_paramz00)),
				BGl_string1618z00zzengine_paramz00, BINT(7740L));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(44),
				__EVMEANING_ADDRESS(
					(BGl_za2ldzd2optimzd2flagsza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(188),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(78),
				__EVMEANING_ADDRESS(
					(BGl_za2heapzd2wasmzd2nameza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(87),
				__EVMEANING_ADDRESS((BGl_za2compilerzd2debugza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(57),
				__EVMEANING_ADDRESS((BGl_za2gczd2libza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(42),
				__EVMEANING_ADDRESS(
					(BGl_za2ldzd2debugzd2optionza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(197),
				__EVMEANING_ADDRESS((BGl_za2inliningzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(90),
				__EVMEANING_ADDRESS(
					(BGl_za2compilerzd2sharingzd2debugzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(7),
				__EVMEANING_ADDRESS(
					(BGl_za2bigloozd2licensingzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(133),
				__EVMEANING_ADDRESS(
					(BGl_za2jvmzd2cinitzd2moduleza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(76),
				__EVMEANING_ADDRESS((BGl_za2heapzd2libraryza2zd2zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(268),
				BGl_epairifyzd2propagatezd2loczd2envzd2zztools_miscz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(269),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2urlza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(104),
				__EVMEANING_ADDRESS((BGl_za2bdbzd2debugza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(159),
				__EVMEANING_ADDRESS(
					(BGl_za2maxzd2czd2tokenzd2lengthza2zd2zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(270),
				BGl_bigloozd2datezd2envz00zztools_datez00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(199),
				__EVMEANING_ADDRESS(
					(BGl_za2inliningzd2kfactorza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(27),
				__EVMEANING_ADDRESS((BGl_za2cczd2ozd2optionza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(129),
				__EVMEANING_ADDRESS(
					(BGl_za2jvmzd2pathzd2separatorza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(89),
				__EVMEANING_ADDRESS(
					(BGl_za2errorzd2localiza7ationza2z75zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(127),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2classpathza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(172),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2jvmzd2constructorzd2inliningza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(13),
				__EVMEANING_ADDRESS((BGl_za2shellza2z00zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(271),
				BGl_bigloozd2variableszd2usagezd2envzd2zzengine_paramz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(17),
				__EVMEANING_ADDRESS((BGl_za2ccza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(73),
				__EVMEANING_ADDRESS(
					(BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(134),
				__EVMEANING_ADDRESS(
					(BGl_za2modulezd2checksumzd2objectzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(272),
				BGl_appendzd221011zd2envz00zzengine_paramz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(170),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2Ozd2macrozf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(273),
				__EVMEANING_ADDRESS((BGl_za2modulezd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(81),
				__EVMEANING_ADDRESS(
					(BGl_za2jvmzd2foreignzd2classzd2idza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(147),
				__EVMEANING_ADDRESS((BGl_za2warningzd2typesza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(145),
				__EVMEANING_ADDRESS(
					(BGl_za2warningzd2overridenzd2slotsza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(274),
				__EVMEANING_ADDRESS(
					(BGl_za2bigloozd2cmdzd2nameza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(198),
				__EVMEANING_ADDRESS(
					(BGl_za2userzd2inliningzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(115),
				__EVMEANING_ADDRESS((BGl_za2startupzd2fileza2zd2zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(275),
				BGl_addzd2updaterz12zd2envz12zzengine_paramz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(152),
				__EVMEANING_ADDRESS((BGl_za2libzd2modeza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(154),
				__EVMEANING_ADDRESS((BGl_za2initzd2modeza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(95),
				__EVMEANING_ADDRESS((BGl_za2debugzd2moduleza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(97),
				__EVMEANING_ADDRESS(
					(BGl_za2czd2debugzd2lineszd2infoza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(183),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2cfazd2flonumzd2arithmeticzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(69),
				__EVMEANING_ADDRESS(
					(BGl_za2bigloozd2librarieszd2czd2setupza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(68),
				__EVMEANING_ADDRESS(
					(BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(220),
				__EVMEANING_ADDRESS((BGl_za2autozd2modeza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(227),
				__EVMEANING_ADDRESS((BGl_za2sawza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(276),
				__EVMEANING_ADDRESS((BGl_za2keyzd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(174),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2jvmzd2branchza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(CNST_TABLE_REF(51),
				__EVMEANING_ADDRESS((BGl_za2ldzd2libraryzd2dirza2z00zzengine_paramz00)),
				BGl_string1618z00zzengine_paramz00, BINT(14499L));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(122),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2shellza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(130),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2jarpathza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(277),
				__EVMEANING_ADDRESS(
					(BGl_za2locationzd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(201),
				__EVMEANING_ADDRESS((BGl_za2optimzd2returnzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(38),
				__EVMEANING_ADDRESS((BGl_za2ldzd2optionsza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(162),
				__EVMEANING_ADDRESS(
					(BGl_za2maxzd2czd2foreignzd2arityza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(173),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2jvmzd2peepholeza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(181),
				__EVMEANING_ADDRESS((BGl_za2optimzd2cfazf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(278),
				__EVMEANING_ADDRESS((BGl_za2typezd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(182),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2cfazd2fixnumzd2arithmeticzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(279),
				__EVMEANING_ADDRESS((BGl_za2restzd2argsza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(206),
				__EVMEANING_ADDRESS((BGl_za2optimzd2uncellzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(70),
				__EVMEANING_ADDRESS(
					(BGl_za2additionalzd2bigloozd2za7ipsza2za7zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(180),
				__EVMEANING_ADDRESS((BGl_za2optimzd2isazf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(280),
				__EVMEANING_ADDRESS((BGl_za2tmpzd2destza2zd2zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(281),
				BGl_uncygdrivezd2envzd2zztools_miscz00);
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(282),
				BGl_epairifyzd2envzd2zztools_miscz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(213),
				__EVMEANING_ADDRESS((BGl_za2csharpzd2suffixza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(6),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2tmpza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(119),
				__EVMEANING_ADDRESS((BGl_za2passza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(101),
				__EVMEANING_ADDRESS((BGl_za2czd2userzd2footza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(58),
				__EVMEANING_ADDRESS((BGl_za2gczd2customzf3za2z21zzengine_paramz00)));
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(283),
				BGl_epairifyza2zd2envz70zztools_miscz00);
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(203),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2taggedzd2fxopzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(156),
				__EVMEANING_ADDRESS(
					(BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(161),
				__EVMEANING_ADDRESS(
					(BGl_za2czd2splitzd2stringza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(205),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2stackablezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(47),
				__EVMEANING_ADDRESS((BGl_za2ldzd2relativeza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(226),
				__EVMEANING_ADDRESS((BGl_za2targetzd2languageza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(146),
				__EVMEANING_ADDRESS(
					(BGl_za2warningzd2overridenzd2variablesza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(65),
				__EVMEANING_ADDRESS(
					(BGl_za2doublezd2ldzd2libszf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(10),
				__EVMEANING_ADDRESS((BGl_za2srczd2filesza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(112),
				__EVMEANING_ADDRESS((BGl_za2czd2filesza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(33),
				__EVMEANING_ADDRESS((BGl_za2cczd2optionsza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(234),
				__EVMEANING_ADDRESS((BGl_za2sawzd2spillza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(284),
				__EVMEANING_ADDRESS(
					(BGl_za2builtinzd2allocatorsza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(64),
				__EVMEANING_ADDRESS(
					(BGl_za2staticzd2allzd2bigloozf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(232),
				__EVMEANING_ADDRESS(
					(BGl_za2sawzd2registerzd2allocationzd2functionsza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(48),
				__EVMEANING_ADDRESS((BGl_za2stripza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(285),
				__EVMEANING_ADDRESS((BGl_za2bigloozd2emailza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(92),
				__EVMEANING_ADDRESS(
					(BGl_za2compilerzd2stackzd2debugzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(286),
				__EVMEANING_ADDRESS((BGl_za2accesszd2shapezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(117),
				__EVMEANING_ADDRESS(
					(BGl_za2autozd2linkzd2mainza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(15),
				__EVMEANING_ADDRESS((BGl_za2cczd2styleza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(204),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2specializa7ezd2flonumzf3za2z54zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(106),
				__EVMEANING_ADDRESS(
					(BGl_za2profzd2tablezd2nameza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(190),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2integratezf3za2z21zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(221),
				__EVMEANING_ADDRESS(
					(BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(9),
				__EVMEANING_ADDRESS((BGl_za2helloza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(74),
				__EVMEANING_ADDRESS(
					(BGl_za2heapzd2basezd2nameza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(11),
				__EVMEANING_ADDRESS((BGl_za2destza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(166),
				__EVMEANING_ADDRESS((BGl_za2optimza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(126),
				__EVMEANING_ADDRESS(
					(BGl_za2jvmzd2bigloozd2classpathza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(CNST_TABLE_REF(287),
				__EVMEANING_ADDRESS((BGl_za2oldzd2loadzd2pathza2z00zzengine_paramz00)),
				BGl_string1618z00zzengine_paramz00, BINT(16625L));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(79),
				__EVMEANING_ADDRESS(
					(BGl_za2heapzd2dumpzd2namesza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(124),
				__EVMEANING_ADDRESS((BGl_za2jvmzd2javaza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(138),
				__EVMEANING_ADDRESS((BGl_za2unsafezd2arityza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(167),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2unrollzd2loopzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(186),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2cfazd2pairzf3za2zf3zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(52),
				__EVMEANING_ADDRESS((BGl_za2libzd2srczd2dirza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(187),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2cfazd2pairzd2quotezd2maxzd2lengthza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(94),
				__EVMEANING_ADDRESS((BGl_za2synczd2profilingza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(288),
				__EVMEANING_ADDRESS(
					(BGl_za2additionalzd2tracesza2zd2zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(171),
				__EVMEANING_ADDRESS(
					(BGl_za2optimzd2jvmzd2inliningza2z00zzengine_paramz00)));
			BGl_definezd2primopzd2refz12z12zz__evenvz00(CNST_TABLE_REF(77),
				__EVMEANING_ADDRESS((BGl_za2heapzd2jvmzd2nameza2z00zzengine_paramz00)));
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
