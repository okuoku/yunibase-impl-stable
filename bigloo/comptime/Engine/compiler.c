/*===========================================================================*/
/*   (Engine/compiler.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Engine/compiler.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_ENGINE_COMPILER_TYPE_DEFINITIONS
#define BGL_ENGINE_COMPILER_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_ENGINE_COMPILER_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_initializa7ezd2Tenvz12z67zztype_envz00(void);
	extern obj_t BGl_producezd2modulez12zc0zzmodule_modulez00(obj_t);
	extern obj_t BGl_installzd2typezd2cachez12z12zztype_cachez00(void);
	extern obj_t BGl_za2unsafezd2arityza2zd2zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza31692ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31676ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	extern obj_t BGl_tailczd2walkz12zc0zztailc_walkz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzengine_compilerz00 = BUNSPEC;
	extern obj_t BGl_reducezd2walkz12zc0zzreduce_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31596ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_comptimezd2expandzf2errorz20zzexpand_epsz00(obj_t);
	extern obj_t
		BGl_additionalzd2heapzd2restorezd2globalsz12zc0zzast_envz00(void);
	extern obj_t BGl_restorezd2additionalzd2heapsz00zzheap_restorez00(void);
	extern obj_t BGl_appendzd2astzd2zzast_buildz00(obj_t, obj_t);
	extern obj_t BGl_z52appendzd22zd2definez52zzexpand_installz00(void);
	extern obj_t BGl_cnstzd2walkz12zc0zzcnst_walkz00(obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_makezd2addzd2heapz00zzheap_makez00(void);
	extern obj_t BGl_makezd2gczd2rootszd2unitzd2zzcc_rootsz00(void);
	extern obj_t BGl_za2targetzd2languageza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31679ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_za2optimzd2taggedzd2fxopzf3za2zf3zzengine_paramz00;
	extern obj_t BGl_setzd2backendz12zc0zzbackend_backendz00(obj_t);
	extern obj_t BGl_patchzd2pairzd2setz12z12zzcfa_pairz00(void);
	extern obj_t BGl_getzd2toplevelzd2unitz00zzmodule_includez00(void);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzengine_compilerz00(void);
	extern obj_t
		BGl_backendzd2checkzd2inlinesz00zzbackend_backendz00(BgL_backendz00_bglt);
	extern obj_t BGl_installzd2initialzd2expanderz00zzexpand_installz00(void);
	extern obj_t BGl_isazd2walkz12zc0zzisa_walkz00(obj_t);
	extern obj_t BGl_za2optimzd2uncellzf3za2z21zzengine_paramz00;
	extern obj_t BGl_za2restzd2argsza2zd2zzengine_paramz00;
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_betazd2walkz12zc0zzbeta_walkz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzengine_compilerz00(void);
	extern obj_t BGl_za2optimzd2returnzf3za2z21zzengine_paramz00;
	static obj_t BGl_objectzd2initzd2zzengine_compilerz00(void);
	extern obj_t BGl_installzd2compilerzd2signalsz12z12zzengine_signalsz00(void);
	extern obj_t BGl_hellozd2worldzd2zzengine_enginez00(void);
	extern obj_t BGl_tracezd2walkz12zc0zztrace_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_callcczd2walkz12zc0zzcallcc_walkz00(obj_t);
	extern obj_t BGl_checkzd2typeszd2zztype_envz00(void);
	extern obj_t BGl_effectzd2walkz12zc0zzeffect_walkz00(obj_t, bool_t);
	extern obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00;
	extern obj_t BGl_expandzd2unitszd2zzexpand_epsz00(obj_t);
	static obj_t BGl_z62compilerz62zzengine_compilerz00(obj_t);
	extern obj_t BGl_bdbzd2walkz12zc0zzbdb_walkz00(obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_dataflowzd2walkz12zc0zzdataflow_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_getzd2alibraryzd2initsz00zzmodule_alibraryz00(void);
	static obj_t BGl_appendzd221011zd2zzengine_compilerz00(obj_t, obj_t);
	extern obj_t BGl_za2modulezd2checksumzd2objectzf3za2zf3zzengine_paramz00;
	static obj_t BGl_methodzd2initzd2zzengine_compilerz00(void);
	extern obj_t BGl_initializa7ezd2Genvz12z67zzast_envz00(void);
	extern obj_t BGl_dumpzd2modulezd2zzmodule_modulez00(obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_za2errorzd2localiza7ationza2z75zzengine_paramz00;
	extern obj_t BGl_writezd2effectzd2zzeffect_walkz00(obj_t);
	extern obj_t BGl_initflowzd2walkz12zc0zzinitflow_walkz00(obj_t);
	extern obj_t BGl_setzd2defaultzd2typez12z12zztype_cachez00(BgL_typez00_bglt);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	extern obj_t BGl_readzd2accesszd2filesz00zzread_accessz00(void);
	extern obj_t BGl_inlinezd2walkz12zc0zzinline_walkz00(obj_t, obj_t);
	extern obj_t BGl_backendzd2walkzd2zzbackend_walkz00(obj_t);
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31603ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31352ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31514ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_makezd2heapzd2zzheap_makez00(void);
	static obj_t BGl_z62zc3z04anonymousza31710ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31702ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_unitzd2sexpza2zd2addzd2headz12z62zzast_unitz00(obj_t, obj_t);
	extern obj_t BGl_coercezd2walkz12zc0zzcoerce_walkz00(obj_t);
	extern obj_t BGl_za2unsafezd2rangeza2zd2zzengine_paramz00;
	extern obj_t BGl_za2optimzd2initflowzf3za2z21zzengine_paramz00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_returnzd2walkz12zc0zzreturn_walkz00(obj_t);
	extern obj_t BGl_writezd2astzd2zzwrite_astz00(obj_t);
	extern obj_t BGl_buildzd2astzd2zzast_buildz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31630ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_checkzd2typezd2zzast_checkzd2typezd2(obj_t, obj_t, bool_t,
		bool_t);
	static obj_t BGl_z62zc3z04anonymousza31541ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_restorezd2heapzd2zzheap_restorez00(void);
	static obj_t BGl_z62zc3z04anonymousza31606ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31517ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31347ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_aboundzd2walkz12zc0zzabound_walkz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_compilerz00zzengine_compilerz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzengine_compilerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_rootsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_ldz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_ccz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzisa_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzuncell_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreturn_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzprof_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbdb_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbdb_spreadzd2objzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbdb_settingz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzhgen_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classgenz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzflop_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzfxop_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztailc_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_pairz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_walkza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinitflow_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzdataflow_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzabound_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzfail_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcallcc_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzstackable_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbeta_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztrace_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_setrcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_installz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzuser_userz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_initz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzast_checkzd2globalzd2initz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_unitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_buildz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_checkzd2typezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_checkzd2sharingzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzheap_makez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzheap_restorez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_accessz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzwrite_astz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzwrite_expandedz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_srcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_enginez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_signalsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_unitzd2sexpza2zd2addz12zb0zzast_unitz00(obj_t, obj_t);
	extern obj_t BGl_za2profilezd2modeza2zd2zzengine_paramz00;
	extern obj_t BGl_modulezd2checksumzd2objectz00zzmodule_modulez00(void);
	extern obj_t BGl_astzd2initializa7ersz75zzast_initz00(void);
	extern obj_t BGl_prognzd2tailzd2expressionsz00zztools_prognz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_bdbzd2settingz12zc0zzbdb_settingz00(void);
	extern obj_t BGl_za2optimzd2dataflowzd2forzd2errorszf3za2z21zzengine_paramz00;
	extern obj_t BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00(void);
	static obj_t BGl_z62zc3z04anonymousza31560ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza31706ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31455ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31536ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_za2unsafezd2libraryza2zd2zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zzengine_compilerz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzengine_compilerz00(void);
	extern obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
	extern obj_t BGl_za2prezd2processorza2zd2zzengine_paramz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzengine_compilerz00(void);
	extern obj_t BGl_za2gczd2forcezd2registerzd2rootszf3za2z21zzengine_paramz00;
	static obj_t BGl_gczd2rootszd2initz00zzengine_compilerz00(void);
	static obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_stackablezd2walkz12zc0zzstackable_walkz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31643ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31473ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_lvtypezd2astz12zc0zzast_lvtypez00(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(obj_t);
	extern obj_t BGl_za2interpreterza2z00zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza31741ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_readzd2srczd2zzread_srcz00(void);
	static obj_t BGl_z62zc3z04anonymousza31725ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31628ze3ze5zzengine_compilerz00(obj_t);
	BGL_IMPORT obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_za2optimzd2dataflowzd2typeszf3za2zf3zzengine_paramz00;
	extern obj_t BGl_integratezd2walkz12zc0zzintegrate_walkz00(obj_t);
	extern obj_t BGl_za2globalzd2tailzd2callzf3za2zf3zzengine_paramz00;
	extern obj_t BGl_cfazd2walkz12zc0zzcfa_walkz00(obj_t);
	extern obj_t BGl_patchzd2vectorzd2setz12z12zzcfa_tvectorz00(void);
	extern obj_t BGl_za2unsafezd2evalza2zd2zzengine_paramz00;
	extern obj_t BGl_flopzd2walkz12zc0zzflop_walkz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31734ze3ze5zzengine_compilerz00(obj_t);
	BGL_IMPORT int BGl_bigloozd2compilerzd2debugz00zz__paramz00(void);
	extern obj_t BGl_fxopzd2walkz12zc0zzfxop_walkz00(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_za2callzf2cczf3za2z01zzengine_paramz00;
	extern obj_t BGl_readzd2jfilezd2zzread_jvmz00(void);
	extern obj_t BGl_prognzd2firstzd2expressionz00zztools_prognz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31590ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31752ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31647ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_writezd2expandedzd2zzwrite_expandedz00(obj_t);
	extern obj_t BGl_makezd2profzd2unitz00zzprof_walkz00(void);
	static obj_t BGl_z62zc3z04anonymousza31664ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00;
	extern obj_t BGl_uncellzd2walkz12zc0zzuncell_walkz00(obj_t);
	extern obj_t BGl_za2readerza2z00zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31592ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_stopzd2onzd2passz00zzengine_passz00(obj_t, obj_t);
	extern obj_t BGl_globaliza7ezd2walkz12z67zzglobaliza7e_walkza7(obj_t, obj_t);
	extern obj_t BGl_userzd2walkzd2zzuser_userz00(obj_t);
	extern obj_t BGl_compilerzd2exitzd2zzinit_mainz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31682ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31585ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31747ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31577ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31739ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_failzd2walkz12zc0zzfail_walkz00(obj_t);
	extern obj_t BGl_za2mainza2z00zzmodule_modulez00;
	extern obj_t BGl_bdbzd2spreadzd2objz12z12zzbdb_spreadzd2objzd2(obj_t);
	extern obj_t BGl_za2unsafezd2versionza2zd2zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza31594ze3ze5zzengine_compilerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31756ze3ze5zzengine_compilerz00(obj_t);
	extern obj_t BGl_enginez00zzengine_enginez00(void);
	static obj_t __cnst[60];


	extern obj_t BGl_hgenzd2walkzd2envz00zzhgen_walkz00;
	   
		 
		DEFINE_STRING(BGl_string1900z00zzengine_compilerz00,
		BgL_bgl_string1900za700za7za7e1943za7, "", 0);
	      DEFINE_STRING(BGl_string1901z00zzengine_compilerz00,
		BgL_bgl_string1901za700za7za7e1944za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1902z00zzengine_compilerz00,
		BgL_bgl_string1902za700za7za7e1945za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1903z00zzengine_compilerz00,
		BgL_bgl_string1903za700za7za7e1946za7, "ast", 3);
	      DEFINE_STRING(BGl_string1905z00zzengine_compilerz00,
		BgL_bgl_string1905za700za7za7e1947za7, "initflow", 8);
	      DEFINE_STRING(BGl_string1907z00zzengine_compilerz00,
		BgL_bgl_string1907za700za7za7e1948za7, "trace", 5);
	      DEFINE_STRING(BGl_string1908z00zzengine_compilerz00,
		BgL_bgl_string1908za700za7za7e1949za7, "callcc", 6);
	      DEFINE_STRING(BGl_string1909z00zzengine_compilerz00,
		BgL_bgl_string1909za700za7za7e1950za7, "effect", 6);
	      DEFINE_STRING(BGl_string1910z00zzengine_compilerz00,
		BgL_bgl_string1910za700za7za7e1951za7, "stackable", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1904z00zzengine_compilerz00,
		BgL_bgl_za762za7c3za704anonymo1952za7,
		BGl_z62zc3z04anonymousza31517ze3ze5zzengine_compilerz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1911z00zzengine_compilerz00,
		BgL_bgl_string1911za700za7za7e1953za7, "isa", 3);
	      DEFINE_STRING(BGl_string1912z00zzengine_compilerz00,
		BgL_bgl_string1912za700za7za7e1954za7, "flop", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1906z00zzengine_compilerz00,
		BgL_bgl_za762za7c3za704anonymo1955za7,
		BGl_z62zc3z04anonymousza31541ze3ze5zzengine_compilerz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1913z00zzengine_compilerz00,
		BgL_bgl_string1913za700za7za7e1956za7, "inline", 6);
	      DEFINE_STRING(BGl_string1915z00zzengine_compilerz00,
		BgL_bgl_string1915za700za7za7e1957za7, "beta", 4);
	      DEFINE_STRING(BGl_string1916z00zzengine_compilerz00,
		BgL_bgl_string1916za700za7za7e1958za7, "fail", 4);
	      DEFINE_STRING(BGl_string1917z00zzengine_compilerz00,
		BgL_bgl_string1917za700za7za7e1959za7, "Reduce0", 7);
	      DEFINE_STRING(BGl_string1918z00zzengine_compilerz00,
		BgL_bgl_string1918za700za7za7e1960za7, "Dataflow", 8);
	      DEFINE_STRING(BGl_string1919z00zzengine_compilerz00,
		BgL_bgl_string1919za700za7za7e1961za7, "dataflow", 8);
	      DEFINE_STRING(BGl_string1920z00zzengine_compilerz00,
		BgL_bgl_string1920za700za7za7e1962za7, "closure", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1914z00zzengine_compilerz00,
		BgL_bgl_za762za7c3za704anonymo1963za7,
		BGl_z62zc3z04anonymousza31603ze3ze5zzengine_compilerz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1921z00zzengine_compilerz00,
		BgL_bgl_string1921za700za7za7e1964za7, "cfa", 3);
	      DEFINE_STRING(BGl_string1922z00zzengine_compilerz00,
		BgL_bgl_string1922za700za7za7e1965za7, "Dataflow+", 9);
	      DEFINE_STRING(BGl_string1923z00zzengine_compilerz00,
		BgL_bgl_string1923za700za7za7e1966za7, "integrate", 9);
	      DEFINE_STRING(BGl_string1924z00zzengine_compilerz00,
		BgL_bgl_string1924za700za7za7e1967za7, "tailc", 5);
	      DEFINE_STRING(BGl_string1925z00zzengine_compilerz00,
		BgL_bgl_string1925za700za7za7e1968za7, "Reduce-", 7);
	      DEFINE_STRING(BGl_string1926z00zzengine_compilerz00,
		BgL_bgl_string1926za700za7za7e1969za7, "reduce-", 7);
	      DEFINE_STRING(BGl_string1927z00zzengine_compilerz00,
		BgL_bgl_string1927za700za7za7e1970za7, "abound", 6);
	      DEFINE_STRING(BGl_string1928z00zzengine_compilerz00,
		BgL_bgl_string1928za700za7za7e1971za7, "coerce", 6);
	      DEFINE_STRING(BGl_string1929z00zzengine_compilerz00,
		BgL_bgl_string1929za700za7za7e1972za7, "fxop", 4);
	      DEFINE_STRING(BGl_string1930z00zzengine_compilerz00,
		BgL_bgl_string1930za700za7za7e1973za7, "Dataflow++", 10);
	      DEFINE_STRING(BGl_string1931z00zzengine_compilerz00,
		BgL_bgl_string1931za700za7za7e1974za7, "Reduce", 6);
	      DEFINE_STRING(BGl_string1932z00zzengine_compilerz00,
		BgL_bgl_string1932za700za7za7e1975za7, "reduce", 6);
	      DEFINE_STRING(BGl_string1933z00zzengine_compilerz00,
		BgL_bgl_string1933za700za7za7e1976za7, "cnst", 4);
	      DEFINE_STRING(BGl_string1934z00zzengine_compilerz00,
		BgL_bgl_string1934za700za7za7e1977za7, "return", 6);
	      DEFINE_STRING(BGl_string1935z00zzengine_compilerz00,
		BgL_bgl_string1935za700za7za7e1978za7, "inline+", 7);
	      DEFINE_STRING(BGl_string1936z00zzengine_compilerz00,
		BgL_bgl_string1936za700za7za7e1979za7, "init", 4);
	      DEFINE_STRING(BGl_string1937z00zzengine_compilerz00,
		BgL_bgl_string1937za700za7za7e1980za7, "Reduce+", 7);
	      DEFINE_STRING(BGl_string1938z00zzengine_compilerz00,
		BgL_bgl_string1938za700za7za7e1981za7, "reduce+", 7);
	      DEFINE_STRING(BGl_string1939z00zzengine_compilerz00,
		BgL_bgl_string1939za700za7za7e1982za7, "uncell", 6);
	      DEFINE_STRING(BGl_string1940z00zzengine_compilerz00,
		BgL_bgl_string1940za700za7za7e1983za7, "engine_compiler", 15);
	      DEFINE_STRING(BGl_string1941z00zzengine_compilerz00,
		BgL_bgl_string1941za700za7za7e1984za7,
		"now uncell reduce+ effect+ init inline+ reducer return cnst reduce egen dataflow++ fxop (c c-saw) coerce abound reduce- tailc integrate dataflow+ cfa closure dataflow reduce0 fail beta make-add-heap inline all flop isa stackable effect callcc bdb-spread-obj make-heap initflow syntax-check ast expand user hgen bigloo-debug-set! (#unspecified) mco dump-module bigloo-class-sans bigloo-unsafe bigloo-unsafe-version bigloo-unsafe-eval bigloo-unsafe-library bigloo-unsafe-arity bigloo-unsafe-range bigloo-unsafe-type bigloo-debug bigloo-class-generate classgen bigloo-compile bdb intern-src ",
		588);
	extern obj_t BGl_classgenzd2walkzd2envz00zzobject_classgenz00;
	   
		 
		DEFINE_STRING(BGl_string1894z00zzengine_compilerz00,
		BgL_bgl_string1894za700za7za7e1985za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1895z00zzengine_compilerz00,
		BgL_bgl_string1895za700za7za7e1986za7, "Illegal source file", 19);
	      DEFINE_STRING(BGl_string1896z00zzengine_compilerz00,
		BgL_bgl_string1896za700za7za7e1987za7,
		"Don't know what to do with arguments: ", 38);
	      DEFINE_STRING(BGl_string1898z00zzengine_compilerz00,
		BgL_bgl_string1898za700za7za7e1988za7, " error", 6);
	      DEFINE_STRING(BGl_string1899z00zzengine_compilerz00,
		BgL_bgl_string1899za700za7za7e1989za7, "s", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1897z00zzengine_compilerz00,
		BgL_bgl_za762za7c3za704anonymo1990za7,
		BGl_z62zc3z04anonymousza31352ze3ze5zzengine_compilerz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_compilerzd2envzd2zzengine_compilerz00,
		BgL_bgl_za762compilerza762za7za71991z00,
		BGl_z62compilerz62zzengine_compilerz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzengine_compilerz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzengine_compilerz00(long
		BgL_checksumz00_2432, char *BgL_fromz00_2433)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzengine_compilerz00))
				{
					BGl_requirezd2initializa7ationz75zzengine_compilerz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzengine_compilerz00();
					BGl_libraryzd2moduleszd2initz00zzengine_compilerz00();
					BGl_cnstzd2initzd2zzengine_compilerz00();
					BGl_importedzd2moduleszd2initz00zzengine_compilerz00();
					return BGl_toplevelzd2initzd2zzengine_compilerz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzengine_compilerz00(void)
	{
		{	/* Engine/compiler.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"engine_compiler");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "engine_compiler");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"engine_compiler");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"engine_compiler");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"engine_compiler");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "engine_compiler");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"engine_compiler");
			BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(0L,
				"engine_compiler");
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "engine_compiler");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"engine_compiler");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "engine_compiler");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzengine_compilerz00(void)
	{
		{	/* Engine/compiler.scm 15 */
			{	/* Engine/compiler.scm 15 */
				obj_t BgL_cportz00_2421;

				{	/* Engine/compiler.scm 15 */
					obj_t BgL_stringz00_2428;

					BgL_stringz00_2428 = BGl_string1941z00zzengine_compilerz00;
					{	/* Engine/compiler.scm 15 */
						obj_t BgL_startz00_2429;

						BgL_startz00_2429 = BINT(0L);
						{	/* Engine/compiler.scm 15 */
							obj_t BgL_endz00_2430;

							BgL_endz00_2430 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2428)));
							{	/* Engine/compiler.scm 15 */

								BgL_cportz00_2421 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2428, BgL_startz00_2429, BgL_endz00_2430);
				}}}}
				{
					long BgL_iz00_2422;

					BgL_iz00_2422 = 59L;
				BgL_loopz00_2423:
					if ((BgL_iz00_2422 == -1L))
						{	/* Engine/compiler.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Engine/compiler.scm 15 */
							{	/* Engine/compiler.scm 15 */
								obj_t BgL_arg1942z00_2424;

								{	/* Engine/compiler.scm 15 */

									{	/* Engine/compiler.scm 15 */
										obj_t BgL_locationz00_2426;

										BgL_locationz00_2426 = BBOOL(((bool_t) 0));
										{	/* Engine/compiler.scm 15 */

											BgL_arg1942z00_2424 =
												BGl_readz00zz__readerz00(BgL_cportz00_2421,
												BgL_locationz00_2426);
										}
									}
								}
								{	/* Engine/compiler.scm 15 */
									int BgL_tmpz00_2462;

									BgL_tmpz00_2462 = (int) (BgL_iz00_2422);
									CNST_TABLE_SET(BgL_tmpz00_2462, BgL_arg1942z00_2424);
							}}
							{	/* Engine/compiler.scm 15 */
								int BgL_auxz00_2427;

								BgL_auxz00_2427 = (int) ((BgL_iz00_2422 - 1L));
								{
									long BgL_iz00_2467;

									BgL_iz00_2467 = (long) (BgL_auxz00_2427);
									BgL_iz00_2422 = BgL_iz00_2467;
									goto BgL_loopz00_2423;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzengine_compilerz00(void)
	{
		{	/* Engine/compiler.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzengine_compilerz00(void)
	{
		{	/* Engine/compiler.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzengine_compilerz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1730;

				BgL_headz00_1730 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1731;
					obj_t BgL_tailz00_1732;

					BgL_prevz00_1731 = BgL_headz00_1730;
					BgL_tailz00_1732 = BgL_l1z00_1;
				BgL_loopz00_1733:
					if (PAIRP(BgL_tailz00_1732))
						{
							obj_t BgL_newzd2prevzd2_1735;

							BgL_newzd2prevzd2_1735 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1732), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1731, BgL_newzd2prevzd2_1735);
							{
								obj_t BgL_tailz00_2477;
								obj_t BgL_prevz00_2476;

								BgL_prevz00_2476 = BgL_newzd2prevzd2_1735;
								BgL_tailz00_2477 = CDR(BgL_tailz00_1732);
								BgL_tailz00_1732 = BgL_tailz00_2477;
								BgL_prevz00_1731 = BgL_prevz00_2476;
								goto BgL_loopz00_1733;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1730);
				}
			}
		}

	}



/* compiler */
	BGL_EXPORTED_DEF obj_t BGl_compilerz00zzengine_compilerz00(void)
	{
		{	/* Engine/compiler.scm 101 */
			BGl_installzd2compilerzd2signalsz12z12zzengine_signalsz00();
			BGl_setzd2backendz12zc0zzbackend_backendz00
				(BGl_za2targetzd2languageza2zd2zzengine_paramz00);
			{	/* Engine/compiler.scm 110 */
				bool_t BgL_test1995z00_2482;

				{	/* Engine/compiler.scm 110 */
					int BgL_arg1315z00_1760;

					BgL_arg1315z00_1760 = BGl_bigloozd2compilerzd2debugz00zz__paramz00();
					BgL_test1995z00_2482 = ((long) (BgL_arg1315z00_1760) > 0L);
				}
				if (BgL_test1995z00_2482)
					{	/* Engine/compiler.scm 110 */
						BFALSE;
					}
				else
					{	/* Engine/compiler.scm 110 */
						{	/* Engine/compiler.scm 111 */
							bool_t BgL_test1996z00_2486;

							{	/* Engine/compiler.scm 111 */
								obj_t BgL_arg1310z00_1756;

								BgL_arg1310z00_1756 =
									BGl_thezd2backendzd2zzbackend_backendz00();
								BgL_test1996z00_2486 =
									(((BgL_backendz00_bglt) COBJECT(
											((BgL_backendz00_bglt) BgL_arg1310z00_1756)))->
									BgL_boundzd2checkzd2);
							}
							if (BgL_test1996z00_2486)
								{	/* Engine/compiler.scm 111 */
									BFALSE;
								}
							else
								{	/* Engine/compiler.scm 111 */
									BGl_za2unsafezd2rangeza2zd2zzengine_paramz00 = BTRUE;
								}
						}
						{	/* Engine/compiler.scm 113 */
							bool_t BgL_test1997z00_2490;

							{	/* Engine/compiler.scm 113 */
								obj_t BgL_arg1314z00_1759;

								BgL_arg1314z00_1759 =
									BGl_thezd2backendzd2zzbackend_backendz00();
								BgL_test1997z00_2490 =
									(((BgL_backendz00_bglt) COBJECT(
											((BgL_backendz00_bglt) BgL_arg1314z00_1759)))->
									BgL_typezd2checkzd2);
							}
							if (BgL_test1997z00_2490)
								{	/* Engine/compiler.scm 113 */
									BFALSE;
								}
							else
								{	/* Engine/compiler.scm 113 */
									BGl_za2unsafezd2typeza2zd2zzengine_paramz00 = BTRUE;
								}
						}
					}
			}
			{	/* Engine/compiler.scm 115 */
				bool_t BgL_test1998z00_2494;

				{	/* Engine/compiler.scm 115 */
					obj_t BgL_arg1318z00_1763;

					BgL_arg1318z00_1763 = BGl_thezd2backendzd2zzbackend_backendz00();
					BgL_test1998z00_2494 =
						(((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_arg1318z00_1763)))->
						BgL_typedzd2funcallzd2);
				}
				if (BgL_test1998z00_2494)
					{	/* Engine/compiler.scm 115 */
						BFALSE;
					}
				else
					{	/* Engine/compiler.scm 115 */
						BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00 =
							BFALSE;
					}
			}
			{	/* Engine/compiler.scm 117 */
				bool_t BgL_test1999z00_2498;

				if (
					((long) CINT(BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00)
						>= 1L))
					{	/* Engine/compiler.scm 117 */
						BgL_test1999z00_2498 = ((bool_t) 1);
					}
				else
					{	/* Engine/compiler.scm 117 */
						BgL_test1999z00_2498 =
							(
							(long) CINT(BGl_za2compilerzd2debugza2zd2zzengine_paramz00) >=
							1L);
					}
				if (BgL_test1999z00_2498)
					{	/* Engine/compiler.scm 117 */
						BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00 = BFALSE;
					}
				else
					{	/* Engine/compiler.scm 117 */
						BFALSE;
					}
			}
			{	/* Engine/compiler.scm 124 */
				obj_t BgL_srcz00_1767;

				{	/* Engine/compiler.scm 124 */
					obj_t BgL_arg1775z00_2019;

					BgL_arg1775z00_2019 = BGl_readzd2srczd2zzread_srcz00();
					BgL_srcz00_1767 =
						BGL_PROCEDURE_CALL1(BGl_za2prezd2processorza2zd2zzengine_paramz00,
						BgL_arg1775z00_2019);
				}
				if (CBOOL(BgL_srcz00_1767))
					{	/* Engine/compiler.scm 129 */
						if (PAIRP(BgL_srcz00_1767))
							{	/* Engine/compiler.scm 132 */
								BFALSE;
							}
						else
							{	/* Engine/compiler.scm 132 */
								if ((BGl_za2readerza2z00zzengine_paramz00 == CNST_TABLE_REF(0)))
									{	/* Engine/compiler.scm 134 */
										obj_t BgL_list1322z00_1769;

										BgL_list1322z00_1769 = MAKE_YOUNG_PAIR(BINT(1L), BNIL);
										BGl_exitz00zz__errorz00(BgL_list1322z00_1769);
									}
								else
									{	/* Engine/compiler.scm 133 */
										BGl_userzd2errorzd2zztools_errorz00
											(BGl_string1894z00zzengine_compilerz00,
											BGl_string1895z00zzengine_compilerz00, BgL_srcz00_1767,
											BNIL);
									}
							}
					}
				else
					{	/* Engine/compiler.scm 129 */
						BGl_za2interpreterza2z00zzengine_paramz00 = BTRUE;
						BGl_compilerzd2exitzd2zzinit_mainz00(BGl_enginez00zzengine_enginez00
							());
					}
				BGl_hellozd2worldzd2zzengine_enginez00();
				{	/* Engine/compiler.scm 141 */
					bool_t BgL_test2004z00_2523;

					if (((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) > 0L))
						{	/* Engine/compiler.scm 142 */
							obj_t BgL_arg1331z00_1777;

							{	/* Engine/compiler.scm 142 */
								obj_t BgL_arg1332z00_1778;

								BgL_arg1332z00_1778 =
									BGl_thezd2backendzd2zzbackend_backendz00();
								BgL_arg1331z00_1777 =
									(((BgL_backendz00_bglt) COBJECT(
											((BgL_backendz00_bglt) BgL_arg1332z00_1778)))->
									BgL_debugzd2supportzd2);
							}
							BgL_test2004z00_2523 =
								CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF
									(1), BgL_arg1331z00_1777));
						}
					else
						{	/* Engine/compiler.scm 141 */
							BgL_test2004z00_2523 = ((bool_t) 0);
						}
					if (BgL_test2004z00_2523)
						{	/* Engine/compiler.scm 141 */
							BGl_bdbzd2settingz12zc0zzbdb_settingz00();
						}
					else
						{	/* Engine/compiler.scm 141 */
							BFALSE;
						}
				}
				if (NULLP(BGl_za2restzd2argsza2zd2zzengine_paramz00))
					{	/* Engine/compiler.scm 146 */
						BFALSE;
					}
				else
					{	/* Engine/compiler.scm 147 */
						obj_t BgL_list1335z00_1780;

						{	/* Engine/compiler.scm 147 */
							obj_t BgL_arg1339z00_1781;

							BgL_arg1339z00_1781 =
								MAKE_YOUNG_PAIR(BGl_za2restzd2argsza2zd2zzengine_paramz00,
								BNIL);
							BgL_list1335z00_1780 =
								MAKE_YOUNG_PAIR(BGl_string1896z00zzengine_compilerz00,
								BgL_arg1339z00_1781);
						}
						BGl_warningz00zz__errorz00(BgL_list1335z00_1780);
					}
				BGl_readzd2accesszd2filesz00zzread_accessz00();
				BGl_readzd2jfilezd2zzread_jvmz00();
				if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
					{	/* Engine/compiler.scm 154 */
						BGl_initializa7ezd2Genvz12z67zzast_envz00();
						BGl_initializa7ezd2Tenvz12z67zztype_envz00();
					}
				else
					{	/* Engine/compiler.scm 154 */
						BGl_restorezd2heapzd2zzheap_restorez00();
					}
				BGl_installzd2typezd2cachez12z12zztype_cachez00();
				BGl_patchzd2vectorzd2setz12z12zzcfa_tvectorz00();
				BGl_patchzd2pairzd2setz12z12zzcfa_pairz00();
				BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF(2));
				if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(3)))
					{	/* Engine/compiler.scm 174 */
						BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF(4));
					}
				else
					{	/* Engine/compiler.scm 174 */
						BFALSE;
					}
				{	/* Engine/compiler.scm 175 */
					bool_t BgL_test2009z00_2556;

					{	/* Engine/compiler.scm 175 */
						int BgL_arg1342z00_1784;

						BgL_arg1342z00_1784 =
							BGl_bigloozd2compilerzd2debugz00zz__paramz00();
						BgL_test2009z00_2556 = ((long) (BgL_arg1342z00_1784) >= 1L);
					}
					if (BgL_test2009z00_2556)
						{	/* Engine/compiler.scm 175 */
							BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF(5));
						}
					else
						{	/* Engine/compiler.scm 175 */
							BFALSE;
						}
				}
				{	/* Engine/compiler.scm 176 */
					long BgL_unsafez00_1785;

					BgL_unsafez00_1785 = 0L;
					if (CBOOL(BGl_za2unsafezd2typeza2zd2zzengine_paramz00))
						{	/* Engine/compiler.scm 177 */
							BgL_unsafez00_1785 = (1L + BgL_unsafez00_1785);
							BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF(6));
						}
					else
						{	/* Engine/compiler.scm 177 */
							BFALSE;
						}
					if (CBOOL(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
						{	/* Engine/compiler.scm 180 */
							BgL_unsafez00_1785 = (1L + BgL_unsafez00_1785);
							BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF(7));
						}
					else
						{	/* Engine/compiler.scm 180 */
							BFALSE;
						}
					if (CBOOL(BGl_za2unsafezd2arityza2zd2zzengine_paramz00))
						{	/* Engine/compiler.scm 183 */
							BgL_unsafez00_1785 = (1L + BgL_unsafez00_1785);
							BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF(8));
						}
					else
						{	/* Engine/compiler.scm 183 */
							BFALSE;
						}
					if (CBOOL(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
						{	/* Engine/compiler.scm 186 */
							BgL_unsafez00_1785 = (1L + BgL_unsafez00_1785);
							BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF(9));
						}
					else
						{	/* Engine/compiler.scm 186 */
							BFALSE;
						}
					if (CBOOL(BGl_za2unsafezd2evalza2zd2zzengine_paramz00))
						{	/* Engine/compiler.scm 189 */
							BgL_unsafez00_1785 = (1L + BgL_unsafez00_1785);
							BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF
								(10));
						}
					else
						{	/* Engine/compiler.scm 189 */
							BFALSE;
						}
					if (CBOOL(BGl_za2unsafezd2versionza2zd2zzengine_paramz00))
						{	/* Engine/compiler.scm 192 */
							BgL_unsafez00_1785 = (1L + BgL_unsafez00_1785);
							BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF
								(11));
						}
					else
						{	/* Engine/compiler.scm 192 */
							BFALSE;
						}
					if ((BgL_unsafez00_1785 == 6L))
						{	/* Engine/compiler.scm 195 */
							BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF
								(12));
						}
					else
						{	/* Engine/compiler.scm 195 */
							BFALSE;
						}
				}
				BGl_installzd2initialzd2expanderz00zzexpand_installz00();
				BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(CNST_TABLE_REF(13));
				{	/* Engine/compiler.scm 206 */
					obj_t BgL_exp0z00_1787;

					{	/* Engine/compiler.scm 206 */
						obj_t BgL_arg1773z00_2018;

						BgL_arg1773z00_2018 = CAR(((obj_t) BgL_srcz00_1767));
						BgL_exp0z00_1787 =
							BGl_comptimezd2expandzf2errorz20zzexpand_epsz00
							(BgL_arg1773z00_2018);
					}
					{	/* Engine/compiler.scm 206 */
						obj_t BgL_modulez00_1788;

						BgL_modulez00_1788 =
							BGl_prognzd2firstzd2expressionz00zztools_prognz00
							(BgL_exp0z00_1787);
						{	/* Engine/compiler.scm 207 */
							obj_t BgL_srczd2codezd2_1789;

							{	/* Engine/compiler.scm 208 */
								obj_t BgL_arg1770z00_2016;
								obj_t BgL_arg1771z00_2017;

								BgL_arg1770z00_2016 =
									BGl_prognzd2tailzd2expressionsz00zztools_prognz00
									(BgL_exp0z00_1787);
								BgL_arg1771z00_2017 = CDR(((obj_t) BgL_srcz00_1767));
								BgL_srczd2codezd2_1789 =
									BGl_appendzd221011zd2zzengine_compilerz00(BgL_arg1770z00_2016,
									BgL_arg1771z00_2017);
							}
							{	/* Engine/compiler.scm 208 */
								obj_t BgL_unitsz00_2377;

								{	/* Engine/compiler.scm 209 */
									obj_t BgL_cellvalz00_2607;

									BgL_cellvalz00_2607 =
										BGl_producezd2modulez12zc0zzmodule_modulez00
										(BgL_modulez00_1788);
									BgL_unitsz00_2377 = MAKE_CELL(BgL_cellvalz00_2607);
								}
								{	/* Engine/compiler.scm 209 */

									{	/* Engine/compiler.scm 211 */
										obj_t BgL_zc3z04anonymousza31347ze3z87_2299;

										BgL_zc3z04anonymousza31347ze3z87_2299 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31347ze3ze5zzengine_compilerz00,
											(int) (0L), (int) (1L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31347ze3z87_2299,
											(int) (0L), BgL_modulez00_1788);
										BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(14),
											BgL_zc3z04anonymousza31347ze3z87_2299);
									}
									if (
										((long) CINT(BGl_za2profilezd2modeza2zd2zzengine_paramz00)
											>= 1L))
										{	/* Engine/compiler.scm 215 */
											obj_t BgL_auxz00_2378;

											{	/* Engine/compiler.scm 215 */
												obj_t BgL_arg1349z00_1795;

												BgL_arg1349z00_1795 =
													BGl_makezd2profzd2unitz00zzprof_walkz00();
												BgL_auxz00_2378 =
													MAKE_YOUNG_PAIR(BgL_arg1349z00_1795,
													CELL_REF(BgL_unitsz00_2377));
											}
											CELL_SET(BgL_unitsz00_2377, BgL_auxz00_2378);
										}
									else
										{	/* Engine/compiler.scm 214 */
											BFALSE;
										}
									if (CBOOL
										(BGl_za2modulezd2checksumzd2objectzf3za2zf3zzengine_paramz00))
										{	/* Engine/compiler.scm 218 */
											BGl_modulezd2checksumzd2objectz00zzmodule_modulez00();
										}
									else
										{	/* Engine/compiler.scm 218 */
											BFALSE;
										}
									BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(15),
										BGl_proc1897z00zzengine_compilerz00);
									{	/* Engine/compiler.scm 223 */
										obj_t BgL_c0z00_1799;

										if (NULLP(BgL_srczd2codezd2_1789))
											{	/* Engine/compiler.scm 223 */
												BgL_c0z00_1799 = CNST_TABLE_REF(16);
											}
										else
											{	/* Engine/compiler.scm 223 */
												BgL_c0z00_1799 = BgL_srczd2codezd2_1789;
											}
										{	/* Engine/compiler.scm 223 */
											obj_t BgL_c1z00_1800;

											{	/* Engine/compiler.scm 224 */
												bool_t BgL_test2020z00_2629;

												if (
													((long)
														CINT
														(BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00)
														>= 1L))
													{	/* Engine/compiler.scm 224 */
														BgL_test2020z00_2629 =
															CBOOL(BGl_za2mainza2z00zzmodule_modulez00);
													}
												else
													{	/* Engine/compiler.scm 224 */
														BgL_test2020z00_2629 = ((bool_t) 0);
													}
												if (BgL_test2020z00_2629)
													{	/* Engine/compiler.scm 226 */
														obj_t BgL_arg1375z00_1809;

														{	/* Engine/compiler.scm 226 */
															obj_t BgL_arg1376z00_1810;

															{	/* Engine/compiler.scm 226 */
																obj_t BgL_arg1377z00_1811;

																{	/* Engine/compiler.scm 226 */
																	int BgL_xz00_1812;
																	obj_t BgL_yz00_1813;

																	BgL_xz00_1812 =
																		BGl_bigloozd2compilerzd2debugz00zz__paramz00
																		();
																	BgL_yz00_1813 =
																		BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00;
																	BgL_arg1377z00_1811 =
																		BGl_2maxz00zz__r4_numbers_6_5z00(BINT
																		(BgL_xz00_1812), BgL_yz00_1813);
																}
																BgL_arg1376z00_1810 =
																	MAKE_YOUNG_PAIR(BgL_arg1377z00_1811, BNIL);
															}
															BgL_arg1375z00_1809 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(17),
																BgL_arg1376z00_1810);
														}
														BgL_c1z00_1800 =
															MAKE_YOUNG_PAIR(BgL_arg1375z00_1809,
															BgL_c0z00_1799);
													}
												else
													{	/* Engine/compiler.scm 224 */
														BgL_c1z00_1800 = BgL_c0z00_1799;
													}
											}
											{	/* Engine/compiler.scm 224 */

												BGl_unitzd2sexpza2zd2addz12zb0zzast_unitz00
													(BGl_getzd2toplevelzd2unitz00zzmodule_includez00(),
													BgL_c1z00_1800);
												if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >=
														2L))
													{	/* Engine/compiler.scm 232 */
														obj_t BgL_arg1364z00_1803;
														obj_t BgL_arg1367z00_1804;

														BgL_arg1364z00_1803 =
															BGl_getzd2toplevelzd2unitz00zzmodule_includez00();
														{	/* Engine/compiler.scm 233 */
															obj_t BgL_arg1370z00_1805;

															BgL_arg1370z00_1805 =
																BGl_z52appendzd22zd2definez52zzexpand_installz00
																();
															{	/* Engine/compiler.scm 233 */
																obj_t BgL_list1371z00_1806;

																BgL_list1371z00_1806 =
																	MAKE_YOUNG_PAIR(BgL_arg1370z00_1805, BNIL);
																BgL_arg1367z00_1804 = BgL_list1371z00_1806;
														}}
														BGl_unitzd2sexpza2zd2addzd2headz12z62zzast_unitz00
															(BgL_arg1364z00_1803, BgL_arg1367z00_1804);
													}
												else
													{	/* Engine/compiler.scm 231 */
														BFALSE;
													}
											}
										}
									}
									if (
										((long)
											CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
											> 0L))
										{	/* Engine/compiler.scm 236 */
											{	/* Engine/compiler.scm 236 */
												obj_t BgL_port1245z00_1818;

												{	/* Engine/compiler.scm 236 */
													obj_t BgL_tmpz00_2653;

													BgL_tmpz00_2653 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_port1245z00_1818 =
														BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2653);
												}
												bgl_display_obj
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BgL_port1245z00_1818);
												bgl_display_string
													(BGl_string1898z00zzengine_compilerz00,
													BgL_port1245z00_1818);
												{	/* Engine/compiler.scm 236 */
													obj_t BgL_arg1408z00_1819;

													{	/* Engine/compiler.scm 236 */
														bool_t BgL_test2024z00_2658;

														if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
															{	/* Engine/compiler.scm 236 */
																if (INTEGERP
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
																	{	/* Engine/compiler.scm 236 */
																		BgL_test2024z00_2658 =
																			(
																			(long)
																			CINT
																			(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																			> 1L);
																	}
																else
																	{	/* Engine/compiler.scm 236 */
																		BgL_test2024z00_2658 =
																			BGl_2ze3ze3zz__r4_numbers_6_5z00
																			(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																			BINT(1L));
																	}
															}
														else
															{	/* Engine/compiler.scm 236 */
																BgL_test2024z00_2658 = ((bool_t) 0);
															}
														if (BgL_test2024z00_2658)
															{	/* Engine/compiler.scm 236 */
																BgL_arg1408z00_1819 =
																	BGl_string1899z00zzengine_compilerz00;
															}
														else
															{	/* Engine/compiler.scm 236 */
																BgL_arg1408z00_1819 =
																	BGl_string1900z00zzengine_compilerz00;
															}
													}
													bgl_display_obj(BgL_arg1408z00_1819,
														BgL_port1245z00_1818);
												}
												bgl_display_string
													(BGl_string1901z00zzengine_compilerz00,
													BgL_port1245z00_1818);
												bgl_display_char(((unsigned char) 10),
													BgL_port1245z00_1818);
											}
											{	/* Engine/compiler.scm 236 */
												obj_t BgL_list1412z00_1823;

												BgL_list1412z00_1823 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
												BGl_exitz00zz__errorz00(BgL_list1412z00_1823);
										}}
									else
										{	/* Engine/compiler.scm 236 */
											obj_t BgL_g1112z00_1824;

											BgL_g1112z00_1824 = BNIL;
											{
												obj_t BgL_hooksz00_1827;
												obj_t BgL_hnamesz00_1828;

												BgL_hooksz00_1827 = BgL_g1112z00_1824;
												BgL_hnamesz00_1828 = BNIL;
											BgL_zc3z04anonymousza31413ze3z87_1829:
												if (NULLP(BgL_hooksz00_1827))
													{	/* Engine/compiler.scm 236 */
														BUNSPEC;
													}
												else
													{	/* Engine/compiler.scm 236 */
														bool_t BgL_test2028z00_2675;

														{	/* Engine/compiler.scm 236 */
															obj_t BgL_fun1436z00_1836;

															BgL_fun1436z00_1836 =
																CAR(((obj_t) BgL_hooksz00_1827));
															BgL_test2028z00_2675 =
																CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1436z00_1836));
														}
														if (BgL_test2028z00_2675)
															{	/* Engine/compiler.scm 236 */
																obj_t BgL_arg1421z00_1833;
																obj_t BgL_arg1422z00_1834;

																BgL_arg1421z00_1833 =
																	CDR(((obj_t) BgL_hooksz00_1827));
																BgL_arg1422z00_1834 =
																	CDR(((obj_t) BgL_hnamesz00_1828));
																{
																	obj_t BgL_hnamesz00_2687;
																	obj_t BgL_hooksz00_2686;

																	BgL_hooksz00_2686 = BgL_arg1421z00_1833;
																	BgL_hnamesz00_2687 = BgL_arg1422z00_1834;
																	BgL_hnamesz00_1828 = BgL_hnamesz00_2687;
																	BgL_hooksz00_1827 = BgL_hooksz00_2686;
																	goto BgL_zc3z04anonymousza31413ze3z87_1829;
																}
															}
														else
															{	/* Engine/compiler.scm 236 */
																obj_t BgL_arg1434z00_1835;

																BgL_arg1434z00_1835 =
																	CAR(((obj_t) BgL_hnamesz00_1828));
																BGl_internalzd2errorzd2zztools_errorz00
																	(BGl_za2currentzd2passza2zd2zzengine_passz00,
																	BGl_string1902z00zzengine_compilerz00,
																	BgL_arg1434z00_1835);
															}
													}
											}
										}
									BGl_checkzd2typeszd2zztype_envz00();
									BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(3),
										BGl_classgenzd2walkzd2envz00zzobject_classgenz00);
									BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(18),
										BGl_hgenzd2walkzd2envz00zzhgen_walkz00);
									BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00();
									BGl_restorezd2additionalzd2heapsz00zzheap_restorez00();
									BGl_additionalzd2heapzd2restorezd2globalsz12zc0zzast_envz00();
									BGl_unitzd2sexpza2zd2addzd2headz12z62zzast_unitz00
										(BGl_getzd2toplevelzd2unitz00zzmodule_includez00(),
										BGl_getzd2alibraryzd2initsz00zzmodule_alibraryz00());
									BGl_userzd2walkzd2zzuser_userz00(CELL_REF(BgL_unitsz00_2377));
									{	/* Engine/compiler.scm 259 */
										obj_t BgL_zc3z04anonymousza31455ze3z87_2297;

										BgL_zc3z04anonymousza31455ze3z87_2297 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31455ze3ze5zzengine_compilerz00,
											(int) (0L), (int) (1L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31455ze3z87_2297,
											(int) (0L), ((obj_t) BgL_unitsz00_2377));
										BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(19),
											BgL_zc3z04anonymousza31455ze3z87_2297);
									}
									BGl_expandzd2unitszd2zzexpand_epsz00(CELL_REF
										(BgL_unitsz00_2377));
									{	/* Engine/compiler.scm 263 */
										obj_t BgL_zc3z04anonymousza31473ze3z87_2296;

										BgL_zc3z04anonymousza31473ze3z87_2296 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31473ze3ze5zzengine_compilerz00,
											(int) (0L), (int) (1L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31473ze3z87_2296,
											(int) (0L), ((obj_t) BgL_unitsz00_2377));
										BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(20),
											BgL_zc3z04anonymousza31473ze3z87_2296);
									}
									{	/* Engine/compiler.scm 266 */
										obj_t BgL_arg1485z00_1847;

										BgL_arg1485z00_1847 =
											BGl_thezd2backendzd2zzbackend_backendz00();
										BGl_backendzd2checkzd2inlinesz00zzbackend_backendz00(
											((BgL_backendz00_bglt) BgL_arg1485z00_1847));
									}
									{	/* Engine/compiler.scm 269 */
										bool_t BgL_test2029z00_2723;

										if (CBOOL
											(BGl_za2gczd2forcezd2registerzd2rootszf3za2z21zzengine_paramz00))
											{	/* Engine/compiler.scm 270 */
												obj_t BgL_arg1509z00_1851;

												BgL_arg1509z00_1851 =
													BGl_thezd2backendzd2zzbackend_backendz00();
												BgL_test2029z00_2723 =
													(((BgL_backendz00_bglt) COBJECT(
															((BgL_backendz00_bglt) BgL_arg1509z00_1851)))->
													BgL_forcezd2registerzd2gczd2rootszd2);
											}
										else
											{	/* Engine/compiler.scm 269 */
												BgL_test2029z00_2723 = ((bool_t) 0);
											}
										if (BgL_test2029z00_2723)
											{	/* Engine/compiler.scm 271 */
												obj_t BgL_auxz00_2381;

												{	/* Engine/compiler.scm 271 */
													obj_t BgL_arg1502z00_1850;

													BgL_arg1502z00_1850 =
														BGl_makezd2gczd2rootszd2unitzd2zzcc_rootsz00();
													BgL_auxz00_2381 =
														MAKE_YOUNG_PAIR(BgL_arg1502z00_1850,
														CELL_REF(BgL_unitsz00_2377));
												}
												CELL_SET(BgL_unitsz00_2377, BgL_auxz00_2381);
											}
										else
											{	/* Engine/compiler.scm 269 */
												BFALSE;
											}
									}
									{	/* Engine/compiler.scm 274 */
										obj_t BgL_astz00_2382;

										{	/* Engine/compiler.scm 274 */
											obj_t BgL_cellvalz00_2731;

											BgL_cellvalz00_2731 =
												BGl_buildzd2astzd2zzast_buildz00(CELL_REF
												(BgL_unitsz00_2377));
											BgL_astz00_2382 = MAKE_CELL(BgL_cellvalz00_2731);
										}
										{	/* Engine/compiler.scm 275 */
											obj_t BgL_zc3z04anonymousza31514ze3z87_2295;

											BgL_zc3z04anonymousza31514ze3z87_2295 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31514ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31514ze3z87_2295,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(21), BgL_zc3z04anonymousza31514ze3z87_2295);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1903z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1903z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(22),
											BGl_proc1904z00zzengine_compilerz00);
										if (CBOOL
											(BGl_za2optimzd2initflowzf3za2z21zzengine_paramz00))
											{	/* Engine/compiler.scm 286 */
												obj_t BgL_auxz00_2383;

												BgL_auxz00_2383 =
													BGl_initflowzd2walkz12zc0zzinitflow_walkz00(CELL_REF
													(BgL_astz00_2382));
												CELL_SET(BgL_astz00_2382, BgL_auxz00_2383);
											}
										else
											{	/* Engine/compiler.scm 285 */
												BFALSE;
											}
										{	/* Engine/compiler.scm 287 */
											obj_t BgL_zc3z04anonymousza31536ze3z87_2293;

											BgL_zc3z04anonymousza31536ze3z87_2293 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31536ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31536ze3z87_2293,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(23), BgL_zc3z04anonymousza31536ze3z87_2293);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1905z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1905z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(24),
											BGl_proc1906z00zzengine_compilerz00);
										{	/* Engine/compiler.scm 295 */
											bool_t BgL_test2032z00_2760;

											if (
												((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
													> 0L))
												{	/* Engine/compiler.scm 296 */
													obj_t BgL_arg1552z00_1870;

													{	/* Engine/compiler.scm 296 */
														obj_t BgL_arg1553z00_1871;

														BgL_arg1553z00_1871 =
															BGl_thezd2backendzd2zzbackend_backendz00();
														BgL_arg1552z00_1870 =
															(((BgL_backendz00_bglt) COBJECT(
																	((BgL_backendz00_bglt)
																		BgL_arg1553z00_1871)))->
															BgL_debugzd2supportzd2);
													}
													BgL_test2032z00_2760 =
														CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(CNST_TABLE_REF(1), BgL_arg1552z00_1870));
												}
											else
												{	/* Engine/compiler.scm 295 */
													BgL_test2032z00_2760 = ((bool_t) 0);
												}
											if (BgL_test2032z00_2760)
												{	/* Engine/compiler.scm 295 */
													BGl_bdbzd2spreadzd2objz12z12zzbdb_spreadzd2objzd2
														(CELL_REF(BgL_astz00_2382));
												}
											else
												{	/* Engine/compiler.scm 295 */
													BFALSE;
												}
										}
										{	/* Engine/compiler.scm 298 */
											obj_t BgL_zc3z04anonymousza31560ze3z87_2291;

											BgL_zc3z04anonymousza31560ze3z87_2291 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31560ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31560ze3z87_2291,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(25), BgL_zc3z04anonymousza31560ze3z87_2291);
										}
										{	/* Engine/compiler.scm 301 */
											bool_t BgL_test2034z00_2779;

											if (
												((long)
													CINT
													(BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00)
													>= 1L))
												{	/* Engine/compiler.scm 303 */
													obj_t BgL_arg1571z00_1879;

													BgL_arg1571z00_1879 =
														BGl_thezd2backendzd2zzbackend_backendz00();
													BgL_test2034z00_2779 =
														(((BgL_backendz00_bglt) COBJECT(
																((BgL_backendz00_bglt) BgL_arg1571z00_1879)))->
														BgL_tracezd2supportzd2);
												}
											else
												{	/* Engine/compiler.scm 301 */
													BgL_test2034z00_2779 = ((bool_t) 0);
												}
											if (BgL_test2034z00_2779)
												{	/* Engine/compiler.scm 304 */
													obj_t BgL_auxz00_2384;

													BgL_auxz00_2384 =
														BGl_tracezd2walkz12zc0zztrace_walkz00(CELL_REF
														(BgL_astz00_2382));
													CELL_SET(BgL_astz00_2382, BgL_auxz00_2384);
												}
											else
												{	/* Engine/compiler.scm 301 */
													BFALSE;
												}
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1907z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1907z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										{	/* Engine/compiler.scm 310 */
											bool_t BgL_test2036z00_2789;

											if (CBOOL(BGl_za2callzf2cczf3za2z01zzengine_paramz00))
												{	/* Engine/compiler.scm 310 */
													obj_t BgL_arg1575z00_1882;

													BgL_arg1575z00_1882 =
														BGl_thezd2backendzd2zzbackend_backendz00();
													BgL_test2036z00_2789 =
														(((BgL_backendz00_bglt) COBJECT(
																((BgL_backendz00_bglt) BgL_arg1575z00_1882)))->
														BgL_callccz00);
												}
											else
												{	/* Engine/compiler.scm 310 */
													BgL_test2036z00_2789 = ((bool_t) 0);
												}
											if (BgL_test2036z00_2789)
												{	/* Engine/compiler.scm 311 */
													obj_t BgL_auxz00_2385;

													BgL_auxz00_2385 =
														BGl_callcczd2walkz12zc0zzcallcc_walkz00(CELL_REF
														(BgL_astz00_2382));
													CELL_SET(BgL_astz00_2382, BgL_auxz00_2385);
												}
											else
												{	/* Engine/compiler.scm 310 */
													BFALSE;
												}
										}
										{	/* Engine/compiler.scm 312 */
											obj_t BgL_zc3z04anonymousza31577ze3z87_2290;

											BgL_zc3z04anonymousza31577ze3z87_2290 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31577ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31577ze3z87_2290,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(26), BgL_zc3z04anonymousza31577ze3z87_2290);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1908z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1908z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										{	/* Engine/compiler.scm 317 */
											obj_t BgL_auxz00_2386;

											BgL_auxz00_2386 =
												BGl_effectzd2walkz12zc0zzeffect_walkz00(CELL_REF
												(BgL_astz00_2382), ((bool_t) 0));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2386);
										}
										{	/* Engine/compiler.scm 318 */
											obj_t BgL_zc3z04anonymousza31585ze3z87_2289;

											BgL_zc3z04anonymousza31585ze3z87_2289 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31585ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31585ze3z87_2289,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(27), BgL_zc3z04anonymousza31585ze3z87_2289);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1909z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1909z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										{	/* Engine/compiler.scm 323 */
											obj_t BgL_auxz00_2387;

											BgL_auxz00_2387 =
												BGl_stackablezd2walkz12zc0zzstackable_walkz00(CELL_REF
												(BgL_astz00_2382));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2387);
										}
										{	/* Engine/compiler.scm 324 */
											obj_t BgL_zc3z04anonymousza31590ze3z87_2288;

											BgL_zc3z04anonymousza31590ze3z87_2288 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31590ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31590ze3z87_2288,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(28), BgL_zc3z04anonymousza31590ze3z87_2288);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1910z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1910z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										{	/* Engine/compiler.scm 329 */
											obj_t BgL_auxz00_2388;

											BgL_auxz00_2388 =
												BGl_isazd2walkz12zc0zzisa_walkz00(CELL_REF
												(BgL_astz00_2382));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2388);
										}
										{	/* Engine/compiler.scm 330 */
											obj_t BgL_zc3z04anonymousza31592ze3z87_2287;

											BgL_zc3z04anonymousza31592ze3z87_2287 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31592ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31592ze3z87_2287,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(29), BgL_zc3z04anonymousza31592ze3z87_2287);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1911z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1911z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										{	/* Engine/compiler.scm 335 */
											obj_t BgL_auxz00_2389;

											BgL_auxz00_2389 =
												BGl_flopzd2walkz12zc0zzflop_walkz00(CELL_REF
												(BgL_astz00_2382));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2389);
										}
										{	/* Engine/compiler.scm 336 */
											obj_t BgL_zc3z04anonymousza31594ze3z87_2286;

											BgL_zc3z04anonymousza31594ze3z87_2286 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31594ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31594ze3z87_2286,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(30), BgL_zc3z04anonymousza31594ze3z87_2286);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1912z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1912z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										{	/* Engine/compiler.scm 341 */
											obj_t BgL_auxz00_2390;

											BgL_auxz00_2390 =
												BGl_inlinezd2walkz12zc0zzinline_walkz00(CELL_REF
												(BgL_astz00_2382), CNST_TABLE_REF(31));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2390);
										}
										{	/* Engine/compiler.scm 342 */
											obj_t BgL_zc3z04anonymousza31596ze3z87_2285;

											BgL_zc3z04anonymousza31596ze3z87_2285 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31596ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31596ze3z87_2285,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(32), BgL_zc3z04anonymousza31596ze3z87_2285);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1913z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1913z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(33),
											BGl_proc1914z00zzengine_compilerz00);
										{	/* Engine/compiler.scm 356 */
											obj_t BgL_auxz00_2391;

											BgL_auxz00_2391 =
												BGl_betazd2walkz12zc0zzbeta_walkz00(CELL_REF
												(BgL_astz00_2382));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2391);
										}
										{	/* Engine/compiler.scm 357 */
											obj_t BgL_zc3z04anonymousza31606ze3z87_2283;

											BgL_zc3z04anonymousza31606ze3z87_2283 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31606ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31606ze3z87_2283,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(34), BgL_zc3z04anonymousza31606ze3z87_2283);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1915z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1915z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										{	/* Engine/compiler.scm 363 */
											bool_t BgL_test2038z00_2875;

											{	/* Engine/compiler.scm 363 */
												bool_t BgL_test2039z00_2876;

												{	/* Engine/compiler.scm 363 */
													int BgL_arg1613z00_1912;

													BgL_arg1613z00_1912 =
														BGl_bigloozd2compilerzd2debugz00zz__paramz00();
													BgL_test2039z00_2876 =
														((long) (BgL_arg1613z00_1912) >= 1L);
												}
												if (BgL_test2039z00_2876)
													{	/* Engine/compiler.scm 363 */
														BgL_test2038z00_2875 =
															CBOOL
															(BGl_za2errorzd2localiza7ationza2z75zzengine_paramz00);
													}
												else
													{	/* Engine/compiler.scm 363 */
														BgL_test2038z00_2875 = ((bool_t) 0);
													}
											}
											if (BgL_test2038z00_2875)
												{	/* Engine/compiler.scm 364 */
													obj_t BgL_auxz00_2392;

													BgL_auxz00_2392 =
														BGl_failzd2walkz12zc0zzfail_walkz00(CELL_REF
														(BgL_astz00_2382));
													CELL_SET(BgL_astz00_2382, BgL_auxz00_2392);
												}
											else
												{	/* Engine/compiler.scm 363 */
													BFALSE;
												}
										}
										{	/* Engine/compiler.scm 365 */
											obj_t BgL_zc3z04anonymousza31616ze3z87_2282;

											BgL_zc3z04anonymousza31616ze3z87_2282 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31616ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31616ze3z87_2282,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(35), BgL_zc3z04anonymousza31616ze3z87_2282);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1916z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1916z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										if (CBOOL
											(BGl_za2optimzd2dataflowzd2typeszf3za2zf3zzengine_paramz00))
											{	/* Engine/compiler.scm 371 */
												if (CBOOL(BGl_za2callzf2cczf3za2z01zzengine_paramz00))
													{	/* Engine/compiler.scm 372 */
														BGl_za2optimzd2dataflowzd2typeszf3za2zf3zzengine_paramz00
															= BFALSE;
													}
												else
													{	/* Engine/compiler.scm 372 */
														{	/* Engine/compiler.scm 375 */
															obj_t BgL_auxz00_2393;

															{	/* Engine/compiler.scm 375 */
																obj_t BgL_list1617z00_1916;

																BgL_list1617z00_1916 =
																	MAKE_YOUNG_PAIR(BTRUE, BNIL);
																BgL_auxz00_2393 =
																	BGl_reducezd2walkz12zc0zzreduce_walkz00
																	(CELL_REF(BgL_astz00_2382),
																	BGl_string1917z00zzengine_compilerz00,
																	BgL_list1617z00_1916);
															}
															CELL_SET(BgL_astz00_2382, BgL_auxz00_2393);
														}
														{	/* Engine/compiler.scm 376 */
															obj_t BgL_zc3z04anonymousza31626ze3z87_2281;

															BgL_zc3z04anonymousza31626ze3z87_2281 =
																MAKE_FX_PROCEDURE
																(BGl_z62zc3z04anonymousza31626ze3ze5zzengine_compilerz00,
																(int) (0L), (int) (1L));
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31626ze3z87_2281,
																(int) (0L), ((obj_t) BgL_astz00_2382));
															BGl_stopzd2onzd2passz00zzengine_passz00
																(CNST_TABLE_REF(36),
																BgL_zc3z04anonymousza31626ze3z87_2281);
														}
														{	/* Engine/compiler.scm 377 */
															obj_t BgL_auxz00_2394;

															BgL_auxz00_2394 =
																BGl_dataflowzd2walkz12zc0zzdataflow_walkz00
																(CELL_REF(BgL_astz00_2382),
																BGl_string1918z00zzengine_compilerz00);
															CELL_SET(BgL_astz00_2382, BgL_auxz00_2394);
											}}}
										else
											{	/* Engine/compiler.scm 371 */
												BFALSE;
											}
										{	/* Engine/compiler.scm 378 */
											obj_t BgL_zc3z04anonymousza31628ze3z87_2280;

											BgL_zc3z04anonymousza31628ze3z87_2280 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31628ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31628ze3z87_2280,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(37), BgL_zc3z04anonymousza31628ze3z87_2280);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1919z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1919z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										{	/* Engine/compiler.scm 383 */
											obj_t BgL_auxz00_2395;

											BgL_auxz00_2395 =
												BGl_globaliza7ezd2walkz12z67zzglobaliza7e_walkza7
												(CELL_REF(BgL_astz00_2382), CNST_TABLE_REF(38));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2395);
										}
										{	/* Engine/compiler.scm 384 */
											obj_t BgL_zc3z04anonymousza31630ze3z87_2279;

											BgL_zc3z04anonymousza31630ze3z87_2279 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31630ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31630ze3z87_2279,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(38), BgL_zc3z04anonymousza31630ze3z87_2279);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1920z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1920z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 0), ((bool_t) 0));
										{	/* Engine/compiler.scm 389 */
											obj_t BgL_auxz00_2396;

											BgL_auxz00_2396 =
												BGl_cfazd2walkz12zc0zzcfa_walkz00(CELL_REF
												(BgL_astz00_2382));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2396);
										}
										{	/* Engine/compiler.scm 390 */
											obj_t BgL_zc3z04anonymousza31643ze3z87_2278;

											BgL_zc3z04anonymousza31643ze3z87_2278 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31643ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31643ze3z87_2278,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(39), BgL_zc3z04anonymousza31643ze3z87_2278);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1921z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1921z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 0));
										BGl_setzd2defaultzd2typez12z12zztype_cachez00((
												(BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
										if (CBOOL
											(BGl_za2optimzd2dataflowzd2typeszf3za2zf3zzengine_paramz00))
											{	/* Engine/compiler.scm 401 */
												obj_t BgL_auxz00_2397;

												BgL_auxz00_2397 =
													BGl_dataflowzd2walkz12zc0zzdataflow_walkz00(CELL_REF
													(BgL_astz00_2382),
													BGl_string1922z00zzengine_compilerz00);
												CELL_SET(BgL_astz00_2382, BgL_auxz00_2397);
											}
										else
											{	/* Engine/compiler.scm 400 */
												BFALSE;
											}
										{	/* Engine/compiler.scm 402 */
											obj_t BgL_zc3z04anonymousza31647ze3z87_2277;

											BgL_zc3z04anonymousza31647ze3z87_2277 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31647ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31647ze3z87_2277,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(40), BgL_zc3z04anonymousza31647ze3z87_2277);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1919z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1919z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 0));
										{	/* Engine/compiler.scm 407 */
											obj_t BgL_auxz00_2398;

											BgL_auxz00_2398 =
												BGl_integratezd2walkz12zc0zzintegrate_walkz00(CELL_REF
												(BgL_astz00_2382));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2398);
										}
										{	/* Engine/compiler.scm 408 */
											obj_t BgL_zc3z04anonymousza31651ze3z87_2276;

											BgL_zc3z04anonymousza31651ze3z87_2276 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31651ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31651ze3z87_2276,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(41), BgL_zc3z04anonymousza31651ze3z87_2276);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1923z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1923z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 0));
										{	/* Engine/compiler.scm 413 */
											bool_t BgL_test2043z00_2966;

											if (CBOOL
												(BGl_za2globalzd2tailzd2callzf3za2zf3zzengine_paramz00))
												{	/* Engine/compiler.scm 413 */
													BgL_test2043z00_2966 = ((bool_t) 1);
												}
											else
												{	/* Engine/compiler.scm 414 */
													obj_t BgL_arg1661z00_1937;

													BgL_arg1661z00_1937 =
														BGl_thezd2backendzd2zzbackend_backendz00();
													BgL_test2043z00_2966 =
														(((BgL_backendz00_bglt) COBJECT(
																((BgL_backendz00_bglt) BgL_arg1661z00_1937)))->
														BgL_requirezd2tailczd2);
												}
											if (BgL_test2043z00_2966)
												{	/* Engine/compiler.scm 413 */
													{	/* Engine/compiler.scm 415 */
														obj_t BgL_auxz00_2399;

														BgL_auxz00_2399 =
															BGl_tailczd2walkz12zc0zztailc_walkz00(CELL_REF
															(BgL_astz00_2382));
														CELL_SET(BgL_astz00_2382, BgL_auxz00_2399);
													}
													BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
														(BGl_string1924z00zzengine_compilerz00,
														CELL_REF(BgL_astz00_2382));
													BGl_checkzd2typezd2zzast_checkzd2typezd2
														(BGl_string1924z00zzengine_compilerz00,
														CELL_REF(BgL_astz00_2382), ((bool_t) 1),
														((bool_t) 0));
												}
											else
												{	/* Engine/compiler.scm 413 */
													BFALSE;
												}
										}
										{	/* Engine/compiler.scm 418 */
											obj_t BgL_zc3z04anonymousza31664ze3z87_2275;

											BgL_zc3z04anonymousza31664ze3z87_2275 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31664ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31664ze3z87_2275,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(42), BgL_zc3z04anonymousza31664ze3z87_2275);
										}
										if (CBOOL
											(BGl_za2optimzd2dataflowzd2forzd2errorszf3za2z21zzengine_paramz00))
											{	/* Engine/compiler.scm 421 */
												{	/* Engine/compiler.scm 422 */
													obj_t BgL_auxz00_2400;

													{	/* Engine/compiler.scm 422 */
														obj_t BgL_list1665z00_1941;

														BgL_list1665z00_1941 = MAKE_YOUNG_PAIR(BTRUE, BNIL);
														BgL_auxz00_2400 =
															BGl_reducezd2walkz12zc0zzreduce_walkz00(CELL_REF
															(BgL_astz00_2382),
															BGl_string1925z00zzengine_compilerz00,
															BgL_list1665z00_1941);
													}
													CELL_SET(BgL_astz00_2382, BgL_auxz00_2400);
												}
												BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
													(BGl_string1926z00zzengine_compilerz00,
													CELL_REF(BgL_astz00_2382));
												BGl_checkzd2typezd2zzast_checkzd2typezd2
													(BGl_string1926z00zzengine_compilerz00,
													CELL_REF(BgL_astz00_2382), ((bool_t) 1),
													((bool_t) 0));
											}
										else
											{	/* Engine/compiler.scm 421 */
												BFALSE;
											}
										{	/* Engine/compiler.scm 425 */
											obj_t BgL_zc3z04anonymousza31676ze3z87_2274;

											BgL_zc3z04anonymousza31676ze3z87_2274 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31676ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31676ze3z87_2274,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(43), BgL_zc3z04anonymousza31676ze3z87_2274);
										}
										if (CBOOL(BGl_za2unsafezd2rangeza2zd2zzengine_paramz00))
											{	/* Engine/compiler.scm 432 */
												BFALSE;
											}
										else
											{	/* Engine/compiler.scm 433 */
												obj_t BgL_auxz00_2401;

												BgL_auxz00_2401 =
													BGl_aboundzd2walkz12zc0zzabound_walkz00(CELL_REF
													(BgL_astz00_2382));
												CELL_SET(BgL_astz00_2382, BgL_auxz00_2401);
											}
										{	/* Engine/compiler.scm 434 */
											obj_t BgL_zc3z04anonymousza31679ze3z87_2273;

											BgL_zc3z04anonymousza31679ze3z87_2273 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31679ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31679ze3z87_2273,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(44), BgL_zc3z04anonymousza31679ze3z87_2273);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1927z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1927z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 0));
										{	/* Engine/compiler.scm 439 */
											obj_t BgL_auxz00_2402;

											BgL_auxz00_2402 =
												BGl_coercezd2walkz12zc0zzcoerce_walkz00(CELL_REF
												(BgL_astz00_2382));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2402);
										}
										{	/* Engine/compiler.scm 440 */
											obj_t BgL_zc3z04anonymousza31682ze3z87_2272;

											BgL_zc3z04anonymousza31682ze3z87_2272 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31682ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31682ze3z87_2272,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(45), BgL_zc3z04anonymousza31682ze3z87_2272);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1928z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1928z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 1));
										{	/* Engine/compiler.scm 445 */
											bool_t BgL_test2047z00_3021;

											if (CBOOL
												(BGl_za2optimzd2taggedzd2fxopzf3za2zf3zzengine_paramz00))
												{	/* Engine/compiler.scm 446 */
													obj_t BgL_arg1699z00_1957;

													{	/* Engine/compiler.scm 446 */
														obj_t BgL_arg1700z00_1958;

														BgL_arg1700z00_1958 =
															BGl_thezd2backendzd2zzbackend_backendz00();
														BgL_arg1699z00_1957 =
															(((BgL_backendz00_bglt) COBJECT(
																	((BgL_backendz00_bglt)
																		BgL_arg1700z00_1958)))->BgL_languagez00);
													}
													BgL_test2047z00_3021 =
														CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1699z00_1957, CNST_TABLE_REF(46)));
												}
											else
												{	/* Engine/compiler.scm 445 */
													BgL_test2047z00_3021 = ((bool_t) 0);
												}
											if (BgL_test2047z00_3021)
												{	/* Engine/compiler.scm 445 */
													{	/* Engine/compiler.scm 447 */
														obj_t BgL_auxz00_2403;

														BgL_auxz00_2403 =
															BGl_fxopzd2walkz12zc0zzfxop_walkz00(CELL_REF
															(BgL_astz00_2382));
														CELL_SET(BgL_astz00_2382, BgL_auxz00_2403);
													}
													{	/* Engine/compiler.scm 448 */
														obj_t BgL_zc3z04anonymousza31692ze3z87_2271;

														BgL_zc3z04anonymousza31692ze3z87_2271 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31692ze3ze5zzengine_compilerz00,
															(int) (0L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31692ze3z87_2271,
															(int) (0L), ((obj_t) BgL_astz00_2382));
														BGl_stopzd2onzd2passz00zzengine_passz00
															(CNST_TABLE_REF(47),
															BgL_zc3z04anonymousza31692ze3z87_2271);
													}
													BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
														(BGl_string1929z00zzengine_compilerz00,
														CELL_REF(BgL_astz00_2382));
													BGl_checkzd2typezd2zzast_checkzd2typezd2
														(BGl_string1929z00zzengine_compilerz00,
														CELL_REF(BgL_astz00_2382), ((bool_t) 1),
														((bool_t) 0));
												}
											else
												{	/* Engine/compiler.scm 445 */
													BFALSE;
												}
										}
										if (CBOOL
											(BGl_za2optimzd2dataflowzd2typeszf3za2zf3zzengine_paramz00))
											{	/* Engine/compiler.scm 455 */
												obj_t BgL_auxz00_2404;

												BgL_auxz00_2404 =
													BGl_dataflowzd2walkz12zc0zzdataflow_walkz00(CELL_REF
													(BgL_astz00_2382),
													BGl_string1930z00zzengine_compilerz00);
												CELL_SET(BgL_astz00_2382, BgL_auxz00_2404);
											}
										else
											{	/* Engine/compiler.scm 454 */
												BFALSE;
											}
										{	/* Engine/compiler.scm 456 */
											obj_t BgL_zc3z04anonymousza31702ze3z87_2270;

											BgL_zc3z04anonymousza31702ze3z87_2270 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31702ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31702ze3z87_2270,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(48), BgL_zc3z04anonymousza31702ze3z87_2270);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1919z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1919z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 1));
										{	/* Engine/compiler.scm 462 */
											bool_t BgL_test2050z00_3054;

											if (
												((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >=
													1L))
												{	/* Engine/compiler.scm 462 */
													BgL_test2050z00_3054 = ((bool_t) 1);
												}
											else
												{	/* Engine/compiler.scm 462 */
													BgL_test2050z00_3054 =
														(BGl_za2passza2z00zzengine_paramz00 ==
														CNST_TABLE_REF(49));
												}
											if (BgL_test2050z00_3054)
												{	/* Engine/compiler.scm 463 */
													obj_t BgL_auxz00_2405;

													BgL_auxz00_2405 =
														BGl_effectzd2walkz12zc0zzeffect_walkz00(CELL_REF
														(BgL_astz00_2382), ((bool_t) 1));
													CELL_SET(BgL_astz00_2382, BgL_auxz00_2405);
												}
											else
												{	/* Engine/compiler.scm 462 */
													BFALSE;
												}
										}
										{	/* Engine/compiler.scm 464 */
											obj_t BgL_zc3z04anonymousza31706ze3z87_2269;

											BgL_zc3z04anonymousza31706ze3z87_2269 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31706ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31706ze3z87_2269,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(49), BgL_zc3z04anonymousza31706ze3z87_2269);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1909z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1909z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 1));
										if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >=
												1L))
											{	/* Engine/compiler.scm 470 */
												obj_t BgL_auxz00_2406;

												BgL_auxz00_2406 =
													BGl_reducezd2walkz12zc0zzreduce_walkz00(CELL_REF
													(BgL_astz00_2382),
													BGl_string1931z00zzengine_compilerz00, BNIL);
												CELL_SET(BgL_astz00_2382, BgL_auxz00_2406);
											}
										else
											{	/* Engine/compiler.scm 469 */
												BFALSE;
											}
										{	/* Engine/compiler.scm 471 */
											obj_t BgL_zc3z04anonymousza31710ze3z87_2268;

											BgL_zc3z04anonymousza31710ze3z87_2268 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31710ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31710ze3z87_2268,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(50), BgL_zc3z04anonymousza31710ze3z87_2268);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1932z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1932z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 1));
										{	/* Engine/compiler.scm 476 */
											bool_t BgL_test2053z00_3085;

											if (
												((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
													> 0L))
												{	/* Engine/compiler.scm 477 */
													obj_t BgL_arg1720z00_1978;

													{	/* Engine/compiler.scm 477 */
														obj_t BgL_arg1722z00_1979;

														BgL_arg1722z00_1979 =
															BGl_thezd2backendzd2zzbackend_backendz00();
														BgL_arg1720z00_1978 =
															(((BgL_backendz00_bglt) COBJECT(
																	((BgL_backendz00_bglt)
																		BgL_arg1722z00_1979)))->
															BgL_debugzd2supportzd2);
													}
													BgL_test2053z00_3085 =
														CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(CNST_TABLE_REF(1), BgL_arg1720z00_1978));
												}
											else
												{	/* Engine/compiler.scm 476 */
													BgL_test2053z00_3085 = ((bool_t) 0);
												}
											if (BgL_test2053z00_3085)
												{	/* Engine/compiler.scm 478 */
													obj_t BgL_auxz00_2407;

													BgL_auxz00_2407 =
														BGl_bdbzd2walkz12zc0zzbdb_walkz00(CELL_REF
														(BgL_astz00_2382));
													CELL_SET(BgL_astz00_2382, BgL_auxz00_2407);
												}
											else
												{	/* Engine/compiler.scm 476 */
													BFALSE;
												}
										}
										{	/* Engine/compiler.scm 479 */
											obj_t BgL_zc3z04anonymousza31725ze3z87_2267;

											BgL_zc3z04anonymousza31725ze3z87_2267 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31725ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31725ze3z87_2267,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF(1),
												BgL_zc3z04anonymousza31725ze3z87_2267);
										}
										{	/* Engine/compiler.scm 482 */
											obj_t BgL_auxz00_2408;

											BgL_auxz00_2408 =
												BGl_cnstzd2walkz12zc0zzcnst_walkz00(CELL_REF
												(BgL_astz00_2382));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2408);
										}
										{	/* Engine/compiler.scm 483 */
											obj_t BgL_zc3z04anonymousza31734ze3z87_2266;

											BgL_zc3z04anonymousza31734ze3z87_2266 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31734ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31734ze3z87_2266,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(51), BgL_zc3z04anonymousza31734ze3z87_2266);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1933z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1933z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 1));
										{	/* Engine/compiler.scm 488 */
											bool_t BgL_test2055z00_3115;

											if (CBOOL
												(BGl_za2optimzd2returnzf3za2z21zzengine_paramz00))
												{	/* Engine/compiler.scm 489 */
													obj_t BgL_arg1737z00_1988;

													BgL_arg1737z00_1988 =
														BGl_thezd2backendzd2zzbackend_backendz00();
													BgL_test2055z00_3115 =
														(((BgL_backendz00_bglt) COBJECT(
																((BgL_backendz00_bglt) BgL_arg1737z00_1988)))->
														BgL_pragmazd2supportzd2);
												}
											else
												{	/* Engine/compiler.scm 488 */
													BgL_test2055z00_3115 = ((bool_t) 0);
												}
											if (BgL_test2055z00_3115)
												{	/* Engine/compiler.scm 488 */
													{	/* Engine/compiler.scm 490 */
														obj_t BgL_auxz00_2409;

														BgL_auxz00_2409 =
															BGl_returnzd2walkz12zc0zzreturn_walkz00(CELL_REF
															(BgL_astz00_2382));
														CELL_SET(BgL_astz00_2382, BgL_auxz00_2409);
													}
													{	/* Engine/compiler.scm 491 */
														obj_t BgL_auxz00_2410;

														BgL_auxz00_2410 =
															BGl_lvtypezd2astz12zc0zzast_lvtypez00(CELL_REF
															(BgL_astz00_2382));
														CELL_SET(BgL_astz00_2382, BgL_auxz00_2410);
													}
												}
											else
												{	/* Engine/compiler.scm 488 */
													BFALSE;
												}
										}
										{	/* Engine/compiler.scm 492 */
											obj_t BgL_zc3z04anonymousza31739ze3z87_2265;

											BgL_zc3z04anonymousza31739ze3z87_2265 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31739ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31739ze3z87_2265,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(52), BgL_zc3z04anonymousza31739ze3z87_2265);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1934z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1934z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 0));
										{	/* Engine/compiler.scm 498 */
											obj_t BgL_auxz00_2411;

											BgL_auxz00_2411 =
												BGl_inlinezd2walkz12zc0zzinline_walkz00(CELL_REF
												(BgL_astz00_2382), CNST_TABLE_REF(53));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2411);
										}
										{	/* Engine/compiler.scm 499 */
											obj_t BgL_auxz00_2412;

											BgL_auxz00_2412 =
												BGl_lvtypezd2astz12zc0zzast_lvtypez00(CELL_REF
												(BgL_astz00_2382));
											CELL_SET(BgL_astz00_2382, BgL_auxz00_2412);
										}
										BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
											(BGl_string1935z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382));
										BGl_checkzd2typezd2zzast_checkzd2typezd2
											(BGl_string1935z00zzengine_compilerz00,
											CELL_REF(BgL_astz00_2382), ((bool_t) 1), ((bool_t) 1));
										{	/* Engine/compiler.scm 502 */
											obj_t BgL_zc3z04anonymousza31741ze3z87_2264;

											BgL_zc3z04anonymousza31741ze3z87_2264 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31741ze3ze5zzengine_compilerz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31741ze3z87_2264,
												(int) (0L), ((obj_t) BgL_astz00_2382));
											BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
												(54), BgL_zc3z04anonymousza31741ze3z87_2264);
										}
										{	/* Engine/compiler.scm 505 */
											obj_t BgL_ast2z00_2413;

											{	/* Engine/compiler.scm 505 */
												obj_t BgL_cellvalz00_3146;

												{	/* Engine/compiler.scm 505 */
													obj_t BgL_arg1767z00_2015;

													BgL_arg1767z00_2015 =
														BGl_astzd2initializa7ersz75zzast_initz00();
													BgL_cellvalz00_3146 =
														BGl_appendzd2astzd2zzast_buildz00
														(BgL_arg1767z00_2015, CELL_REF(BgL_astz00_2382));
												}
												BgL_ast2z00_2413 = MAKE_CELL(BgL_cellvalz00_3146);
											}
											{	/* Engine/compiler.scm 506 */
												obj_t BgL_zc3z04anonymousza31747ze3z87_2263;

												BgL_zc3z04anonymousza31747ze3z87_2263 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31747ze3ze5zzengine_compilerz00,
													(int) (0L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31747ze3z87_2263,
													(int) (0L), ((obj_t) BgL_ast2z00_2413));
												BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
													(55), BgL_zc3z04anonymousza31747ze3z87_2263);
											}
											BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
												(BGl_string1936z00zzengine_compilerz00,
												CELL_REF(BgL_ast2z00_2413));
											BGl_checkzd2typezd2zzast_checkzd2typezd2
												(BGl_string1936z00zzengine_compilerz00,
												CELL_REF(BgL_ast2z00_2413), ((bool_t) 1), ((bool_t) 1));
											{	/* Engine/compiler.scm 511 */
												bool_t BgL_test2057z00_3159;

												if (
													((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >=
														2L))
													{	/* Engine/compiler.scm 511 */
														BgL_test2057z00_3159 = ((bool_t) 1);
													}
												else
													{	/* Engine/compiler.scm 511 */
														obj_t BgL_arg1754z00_2007;

														BgL_arg1754z00_2007 =
															BGl_thezd2backendzd2zzbackend_backendz00();
														BgL_test2057z00_3159 =
															(((BgL_backendz00_bglt) COBJECT(
																	((BgL_backendz00_bglt)
																		BgL_arg1754z00_2007)))->BgL_effectzb2zb2);
													}
												if (BgL_test2057z00_3159)
													{	/* Engine/compiler.scm 511 */
														{	/* Engine/compiler.scm 512 */
															obj_t BgL_auxz00_2414;

															BgL_auxz00_2414 =
																BGl_effectzd2walkz12zc0zzeffect_walkz00(CELL_REF
																(BgL_ast2z00_2413), ((bool_t) 1));
															CELL_SET(BgL_ast2z00_2413, BgL_auxz00_2414);
														}
														{	/* Engine/compiler.scm 513 */
															obj_t BgL_zc3z04anonymousza31752ze3z87_2262;

															BgL_zc3z04anonymousza31752ze3z87_2262 =
																MAKE_FX_PROCEDURE
																(BGl_z62zc3z04anonymousza31752ze3ze5zzengine_compilerz00,
																(int) (0L), (int) (1L));
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31752ze3z87_2262,
																(int) (0L), ((obj_t) BgL_ast2z00_2413));
															BGl_stopzd2onzd2passz00zzengine_passz00
																(CNST_TABLE_REF(56),
																BgL_zc3z04anonymousza31752ze3z87_2262);
														}
														{	/* Engine/compiler.scm 514 */
															obj_t BgL_auxz00_2415;

															BgL_auxz00_2415 =
																BGl_reducezd2walkz12zc0zzreduce_walkz00(CELL_REF
																(BgL_ast2z00_2413),
																BGl_string1937z00zzengine_compilerz00, BNIL);
															CELL_SET(BgL_ast2z00_2413, BgL_auxz00_2415);
													}}
												else
													{	/* Engine/compiler.scm 511 */
														BFALSE;
													}
											}
											{	/* Engine/compiler.scm 515 */
												obj_t BgL_zc3z04anonymousza31756ze3z87_2261;

												BgL_zc3z04anonymousza31756ze3z87_2261 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31756ze3ze5zzengine_compilerz00,
													(int) (0L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31756ze3z87_2261,
													(int) (0L), ((obj_t) BgL_ast2z00_2413));
												BGl_stopzd2onzd2passz00zzengine_passz00(CNST_TABLE_REF
													(57), BgL_zc3z04anonymousza31756ze3z87_2261);
											}
											BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
												(BGl_string1938z00zzengine_compilerz00,
												CELL_REF(BgL_ast2z00_2413));
											BGl_checkzd2typezd2zzast_checkzd2typezd2
												(BGl_string1938z00zzengine_compilerz00,
												CELL_REF(BgL_ast2z00_2413), ((bool_t) 1), ((bool_t) 1));
											if (CBOOL
												(BGl_za2optimzd2uncellzf3za2z21zzengine_paramz00))
												{	/* Engine/compiler.scm 520 */
													{	/* Engine/compiler.scm 521 */
														obj_t BgL_auxz00_2416;

														BgL_auxz00_2416 =
															BGl_uncellzd2walkz12zc0zzuncell_walkz00(CELL_REF
															(BgL_astz00_2382));
														CELL_SET(BgL_astz00_2382, BgL_auxz00_2416);
													}
													{	/* Engine/compiler.scm 522 */
														obj_t BgL_zc3z04anonymousza31762ze3z87_2260;

														BgL_zc3z04anonymousza31762ze3z87_2260 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31762ze3ze5zzengine_compilerz00,
															(int) (0L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31762ze3z87_2260,
															(int) (0L), ((obj_t) BgL_astz00_2382));
														BGl_stopzd2onzd2passz00zzengine_passz00
															(CNST_TABLE_REF(58),
															BgL_zc3z04anonymousza31762ze3z87_2260);
													}
													BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2
														(BGl_string1939z00zzengine_compilerz00,
														CELL_REF(BgL_astz00_2382));
													BGl_checkzd2typezd2zzast_checkzd2typezd2
														(BGl_string1939z00zzengine_compilerz00,
														CELL_REF(BgL_astz00_2382), ((bool_t) 1),
														((bool_t) 0));
												}
											else
												{	/* Engine/compiler.scm 520 */
													BFALSE;
												}
											{	/* Engine/compiler.scm 526 */
												obj_t BgL_arg1765z00_2014;

												BgL_arg1765z00_2014 =
													BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(59),
													CELL_REF(BgL_ast2z00_2413));
												BGl_backendzd2walkzd2zzbackend_walkz00
													(BgL_arg1765z00_2014);
											}
										}
										return BINT(0L);
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &compiler */
	obj_t BGl_z62compilerz62zzengine_compilerz00(obj_t BgL_envz00_2300)
	{
		{	/* Engine/compiler.scm 101 */
			return BGl_compilerz00zzengine_compilerz00();
		}

	}



/* &<@anonymous:1762> */
	obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2301)
	{
		{	/* Engine/compiler.scm 522 */
			{	/* Engine/compiler.scm 522 */
				obj_t BgL_astz00_2302;

				BgL_astz00_2302 = PROCEDURE_REF(BgL_envz00_2301, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2302));
			}
		}

	}



/* &<@anonymous:1756> */
	obj_t BGl_z62zc3z04anonymousza31756ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2303)
	{
		{	/* Engine/compiler.scm 515 */
			{	/* Engine/compiler.scm 515 */
				obj_t BgL_ast2z00_2304;

				BgL_ast2z00_2304 = PROCEDURE_REF(BgL_envz00_2303, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_ast2z00_2304));
			}
		}

	}



/* &<@anonymous:1752> */
	obj_t BGl_z62zc3z04anonymousza31752ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2305)
	{
		{	/* Engine/compiler.scm 513 */
			{	/* Engine/compiler.scm 513 */
				obj_t BgL_ast2z00_2306;

				BgL_ast2z00_2306 = PROCEDURE_REF(BgL_envz00_2305, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_ast2z00_2306));
			}
		}

	}



/* &<@anonymous:1747> */
	obj_t BGl_z62zc3z04anonymousza31747ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2307)
	{
		{	/* Engine/compiler.scm 506 */
			{	/* Engine/compiler.scm 506 */
				obj_t BgL_ast2z00_2308;

				BgL_ast2z00_2308 = PROCEDURE_REF(BgL_envz00_2307, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_ast2z00_2308));
			}
		}

	}



/* &<@anonymous:1741> */
	obj_t BGl_z62zc3z04anonymousza31741ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2309)
	{
		{	/* Engine/compiler.scm 502 */
			{	/* Engine/compiler.scm 502 */
				obj_t BgL_astz00_2310;

				BgL_astz00_2310 = PROCEDURE_REF(BgL_envz00_2309, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2310));
			}
		}

	}



/* &<@anonymous:1739> */
	obj_t BGl_z62zc3z04anonymousza31739ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2311)
	{
		{	/* Engine/compiler.scm 492 */
			{	/* Engine/compiler.scm 492 */
				obj_t BgL_astz00_2312;

				BgL_astz00_2312 = PROCEDURE_REF(BgL_envz00_2311, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2312));
			}
		}

	}



/* &<@anonymous:1734> */
	obj_t BGl_z62zc3z04anonymousza31734ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2313)
	{
		{	/* Engine/compiler.scm 483 */
			{	/* Engine/compiler.scm 483 */
				obj_t BgL_astz00_2314;

				BgL_astz00_2314 = PROCEDURE_REF(BgL_envz00_2313, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2314));
			}
		}

	}



/* &<@anonymous:1725> */
	obj_t BGl_z62zc3z04anonymousza31725ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2315)
	{
		{	/* Engine/compiler.scm 479 */
			{	/* Engine/compiler.scm 479 */
				obj_t BgL_astz00_2316;

				BgL_astz00_2316 = PROCEDURE_REF(BgL_envz00_2315, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2316));
			}
		}

	}



/* &<@anonymous:1710> */
	obj_t BGl_z62zc3z04anonymousza31710ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2317)
	{
		{	/* Engine/compiler.scm 471 */
			{	/* Engine/compiler.scm 471 */
				obj_t BgL_astz00_2318;

				BgL_astz00_2318 = PROCEDURE_REF(BgL_envz00_2317, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2318));
			}
		}

	}



/* &<@anonymous:1706> */
	obj_t BGl_z62zc3z04anonymousza31706ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2319)
	{
		{	/* Engine/compiler.scm 464 */
			{	/* Engine/compiler.scm 464 */
				obj_t BgL_astz00_2320;

				BgL_astz00_2320 = PROCEDURE_REF(BgL_envz00_2319, (int) (0L));
				return BGl_writezd2effectzd2zzeffect_walkz00(CELL_REF(BgL_astz00_2320));
			}
		}

	}



/* &<@anonymous:1702> */
	obj_t BGl_z62zc3z04anonymousza31702ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2321)
	{
		{	/* Engine/compiler.scm 456 */
			{	/* Engine/compiler.scm 456 */
				obj_t BgL_astz00_2322;

				BgL_astz00_2322 = PROCEDURE_REF(BgL_envz00_2321, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2322));
			}
		}

	}



/* &<@anonymous:1692> */
	obj_t BGl_z62zc3z04anonymousza31692ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2323)
	{
		{	/* Engine/compiler.scm 448 */
			{	/* Engine/compiler.scm 448 */
				obj_t BgL_astz00_2324;

				BgL_astz00_2324 = PROCEDURE_REF(BgL_envz00_2323, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2324));
			}
		}

	}



/* &<@anonymous:1682> */
	obj_t BGl_z62zc3z04anonymousza31682ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2325)
	{
		{	/* Engine/compiler.scm 440 */
			{	/* Engine/compiler.scm 440 */
				obj_t BgL_astz00_2326;

				BgL_astz00_2326 = PROCEDURE_REF(BgL_envz00_2325, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2326));
			}
		}

	}



/* &<@anonymous:1679> */
	obj_t BGl_z62zc3z04anonymousza31679ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2327)
	{
		{	/* Engine/compiler.scm 434 */
			{	/* Engine/compiler.scm 434 */
				obj_t BgL_astz00_2328;

				BgL_astz00_2328 = PROCEDURE_REF(BgL_envz00_2327, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2328));
			}
		}

	}



/* &<@anonymous:1676> */
	obj_t BGl_z62zc3z04anonymousza31676ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2329)
	{
		{	/* Engine/compiler.scm 425 */
			{	/* Engine/compiler.scm 425 */
				obj_t BgL_astz00_2330;

				BgL_astz00_2330 = PROCEDURE_REF(BgL_envz00_2329, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2330));
			}
		}

	}



/* &<@anonymous:1664> */
	obj_t BGl_z62zc3z04anonymousza31664ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2331)
	{
		{	/* Engine/compiler.scm 418 */
			{	/* Engine/compiler.scm 418 */
				obj_t BgL_astz00_2332;

				BgL_astz00_2332 = PROCEDURE_REF(BgL_envz00_2331, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2332));
			}
		}

	}



/* &<@anonymous:1651> */
	obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2333)
	{
		{	/* Engine/compiler.scm 408 */
			{	/* Engine/compiler.scm 408 */
				obj_t BgL_astz00_2334;

				BgL_astz00_2334 = PROCEDURE_REF(BgL_envz00_2333, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2334));
			}
		}

	}



/* &<@anonymous:1647> */
	obj_t BGl_z62zc3z04anonymousza31647ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2335)
	{
		{	/* Engine/compiler.scm 402 */
			{	/* Engine/compiler.scm 402 */
				obj_t BgL_astz00_2336;

				BgL_astz00_2336 = PROCEDURE_REF(BgL_envz00_2335, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2336));
			}
		}

	}



/* &<@anonymous:1643> */
	obj_t BGl_z62zc3z04anonymousza31643ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2337)
	{
		{	/* Engine/compiler.scm 390 */
			{	/* Engine/compiler.scm 390 */
				obj_t BgL_astz00_2338;

				BgL_astz00_2338 = PROCEDURE_REF(BgL_envz00_2337, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2338));
			}
		}

	}



/* &<@anonymous:1630> */
	obj_t BGl_z62zc3z04anonymousza31630ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2339)
	{
		{	/* Engine/compiler.scm 384 */
			{	/* Engine/compiler.scm 384 */
				obj_t BgL_astz00_2340;

				BgL_astz00_2340 = PROCEDURE_REF(BgL_envz00_2339, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2340));
			}
		}

	}



/* &<@anonymous:1628> */
	obj_t BGl_z62zc3z04anonymousza31628ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2341)
	{
		{	/* Engine/compiler.scm 378 */
			{	/* Engine/compiler.scm 378 */
				obj_t BgL_astz00_2342;

				BgL_astz00_2342 = PROCEDURE_REF(BgL_envz00_2341, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2342));
			}
		}

	}



/* &<@anonymous:1626> */
	obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2343)
	{
		{	/* Engine/compiler.scm 376 */
			{	/* Engine/compiler.scm 376 */
				obj_t BgL_astz00_2344;

				BgL_astz00_2344 = PROCEDURE_REF(BgL_envz00_2343, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2344));
			}
		}

	}



/* &<@anonymous:1616> */
	obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2345)
	{
		{	/* Engine/compiler.scm 365 */
			{	/* Engine/compiler.scm 365 */
				obj_t BgL_astz00_2346;

				BgL_astz00_2346 = PROCEDURE_REF(BgL_envz00_2345, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2346));
			}
		}

	}



/* &<@anonymous:1606> */
	obj_t BGl_z62zc3z04anonymousza31606ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2347)
	{
		{	/* Engine/compiler.scm 357 */
			{	/* Engine/compiler.scm 357 */
				obj_t BgL_astz00_2348;

				BgL_astz00_2348 = PROCEDURE_REF(BgL_envz00_2347, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2348));
			}
		}

	}



/* &<@anonymous:1603> */
	obj_t BGl_z62zc3z04anonymousza31603ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2349)
	{
		{	/* Engine/compiler.scm 353 */
			return BGl_makezd2addzd2heapz00zzheap_makez00();
		}

	}



/* &<@anonymous:1596> */
	obj_t BGl_z62zc3z04anonymousza31596ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2350)
	{
		{	/* Engine/compiler.scm 342 */
			{	/* Engine/compiler.scm 342 */
				obj_t BgL_astz00_2351;

				BgL_astz00_2351 = PROCEDURE_REF(BgL_envz00_2350, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2351));
			}
		}

	}



/* &<@anonymous:1594> */
	obj_t BGl_z62zc3z04anonymousza31594ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2352)
	{
		{	/* Engine/compiler.scm 336 */
			{	/* Engine/compiler.scm 336 */
				obj_t BgL_astz00_2353;

				BgL_astz00_2353 = PROCEDURE_REF(BgL_envz00_2352, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2353));
			}
		}

	}



/* &<@anonymous:1592> */
	obj_t BGl_z62zc3z04anonymousza31592ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2354)
	{
		{	/* Engine/compiler.scm 330 */
			{	/* Engine/compiler.scm 330 */
				obj_t BgL_astz00_2355;

				BgL_astz00_2355 = PROCEDURE_REF(BgL_envz00_2354, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2355));
			}
		}

	}



/* &<@anonymous:1590> */
	obj_t BGl_z62zc3z04anonymousza31590ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2356)
	{
		{	/* Engine/compiler.scm 324 */
			{	/* Engine/compiler.scm 324 */
				obj_t BgL_astz00_2357;

				BgL_astz00_2357 = PROCEDURE_REF(BgL_envz00_2356, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2357));
			}
		}

	}



/* &<@anonymous:1585> */
	obj_t BGl_z62zc3z04anonymousza31585ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2358)
	{
		{	/* Engine/compiler.scm 318 */
			{	/* Engine/compiler.scm 318 */
				obj_t BgL_astz00_2359;

				BgL_astz00_2359 = PROCEDURE_REF(BgL_envz00_2358, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2359));
			}
		}

	}



/* &<@anonymous:1577> */
	obj_t BGl_z62zc3z04anonymousza31577ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2360)
	{
		{	/* Engine/compiler.scm 312 */
			{	/* Engine/compiler.scm 312 */
				obj_t BgL_astz00_2361;

				BgL_astz00_2361 = PROCEDURE_REF(BgL_envz00_2360, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2361));
			}
		}

	}



/* &<@anonymous:1560> */
	obj_t BGl_z62zc3z04anonymousza31560ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2362)
	{
		{	/* Engine/compiler.scm 298 */
			{	/* Engine/compiler.scm 298 */
				obj_t BgL_astz00_2363;

				BgL_astz00_2363 = PROCEDURE_REF(BgL_envz00_2362, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2363));
			}
		}

	}



/* &<@anonymous:1541> */
	obj_t BGl_z62zc3z04anonymousza31541ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2364)
	{
		{	/* Engine/compiler.scm 292 */
			return BGl_makezd2heapzd2zzheap_makez00();
		}

	}



/* &<@anonymous:1536> */
	obj_t BGl_z62zc3z04anonymousza31536ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2365)
	{
		{	/* Engine/compiler.scm 287 */
			{	/* Engine/compiler.scm 287 */
				obj_t BgL_astz00_2366;

				BgL_astz00_2366 = PROCEDURE_REF(BgL_envz00_2365, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2366));
			}
		}

	}



/* &<@anonymous:1517> */
	obj_t BGl_z62zc3z04anonymousza31517ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2367)
	{
		{	/* Engine/compiler.scm 282 */
			return BUNSPEC;
		}

	}



/* &<@anonymous:1514> */
	obj_t BGl_z62zc3z04anonymousza31514ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2368)
	{
		{	/* Engine/compiler.scm 275 */
			{	/* Engine/compiler.scm 275 */
				obj_t BgL_astz00_2369;

				BgL_astz00_2369 = PROCEDURE_REF(BgL_envz00_2368, (int) (0L));
				return BGl_writezd2astzd2zzwrite_astz00(CELL_REF(BgL_astz00_2369));
			}
		}

	}



/* &<@anonymous:1473> */
	obj_t BGl_z62zc3z04anonymousza31473ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2370)
	{
		{	/* Engine/compiler.scm 263 */
			{	/* Engine/compiler.scm 263 */
				obj_t BgL_unitsz00_2371;

				BgL_unitsz00_2371 = PROCEDURE_REF(BgL_envz00_2370, (int) (0L));
				return
					BGl_writezd2expandedzd2zzwrite_expandedz00(CELL_REF
					(BgL_unitsz00_2371));
			}
		}

	}



/* &<@anonymous:1455> */
	obj_t BGl_z62zc3z04anonymousza31455ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2372)
	{
		{	/* Engine/compiler.scm 259 */
			{	/* Engine/compiler.scm 259 */
				obj_t BgL_unitsz00_2373;

				BgL_unitsz00_2373 = PROCEDURE_REF(BgL_envz00_2372, (int) (0L));
				return
					BGl_writezd2expandedzd2zzwrite_expandedz00(CELL_REF
					(BgL_unitsz00_2373));
			}
		}

	}



/* &<@anonymous:1352> */
	obj_t BGl_z62zc3z04anonymousza31352ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2374)
	{
		{	/* Engine/compiler.scm 220 */
			return BINT(0L);
		}

	}



/* &<@anonymous:1347> */
	obj_t BGl_z62zc3z04anonymousza31347ze3ze5zzengine_compilerz00(obj_t
		BgL_envz00_2375)
	{
		{	/* Engine/compiler.scm 211 */
			return
				BGl_dumpzd2modulezd2zzmodule_modulez00(PROCEDURE_REF(BgL_envz00_2375,
					(int) (0L)));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzengine_compilerz00(void)
	{
		{	/* Engine/compiler.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzengine_compilerz00(void)
	{
		{	/* Engine/compiler.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzengine_compilerz00(void)
	{
		{	/* Engine/compiler.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzengine_compilerz00(void)
	{
		{	/* Engine/compiler.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzengine_signalsz00(437317298L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzengine_enginez00(373986149L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzread_srcz00(6602148L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzwrite_expandedz00(320989184L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzwrite_astz00(250486252L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzread_accessz00(11403588L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzheap_restorez00(147989063L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzheap_makez00(525175851L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_checkzd2sharingzd2(88839753L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_checkzd2typezd2(290987094L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_buildz00(428035925L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_unitz00(234044111L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_checkzd2globalzd2initz00(513785737L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_initz00(409655650L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzuser_userz00(352878539L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(316727058L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzexpand_installz00(291484121L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzinit_mainz00(288050968L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzinit_setrcz00(32737986L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zztrace_walkz00(146692749L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzbeta_walkz00(254688521L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzinline_walkz00(385476819L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzeffect_walkz00(42129111L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzstackable_walkz00(440685959L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzcallcc_walkz00(105123174L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzfail_walkz00(51531863L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzabound_walkz00(224477217L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzdataflow_walkz00(18612756L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzinitflow_walkz00(146152102L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_walkza7(69163963L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzcfa_walkz00(285783706L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(324810626L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzcfa_pairz00(37668556L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzintegrate_walkz00(67376259L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zztailc_walkz00(347113675L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzfxop_walkz00(501948674L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzflop_walkz00(79978526L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzcoerce_walkz00(224340927L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzreduce_walkz00(459999967L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzcnst_walkz00(424305745L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzobject_classgenz00(502726840L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzhgen_walkz00(362377213L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzbdb_settingz00(444161932L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzbdb_spreadzd2objzd2(53868587L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzbdb_walkz00(260685909L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzprof_walkz00(181156913L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzreturn_walkz00(216820585L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzuncell_walkz00(492672753L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzisa_walkz00(303087161L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzcc_ccz00(527776065L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzcc_ldz00(335315213L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzcc_rootsz00(74776091L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzbackend_walkz00(141649863L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			BGl_modulezd2initializa7ationz75zzbackend_cz00(190235040L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_jvmz00(170301672L,
				BSTRING_TO_STRING(BGl_string1940z00zzengine_compilerz00));
		}

	}

#ifdef __cplusplus
}
#endif
