/*===========================================================================*/
/*   (Engine/heap.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Engine/heap.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_ENGINE_HEAP_TYPE_DEFINITIONS
#define BGL_ENGINE_HEAP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;


#endif													// BGL_ENGINE_HEAP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_initializa7ezd2Tenvz12z67zztype_envz00(void);
	extern obj_t BGl_setzd2genvz12zc0zzast_envz00(obj_t);
	extern obj_t BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzengine_heapz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31202ze3ze5zzengine_heapz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_dumpzd2Genvzd2zzengine_heapz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dumpzd2heapszd2zzengine_heapz00(obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31211ze3ze5zzengine_heapz00(obj_t);
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern obj_t BGl_za2targetzd2languageza2zd2zzengine_paramz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t BGl_setzd2backendz12zc0zzbackend_backendz00(obj_t);
	static obj_t BGl_z62dumpzd2heapszb0zzengine_heapz00(obj_t, obj_t);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t input_obj(obj_t);
	static obj_t BGl_genericzd2initzd2zzengine_heapz00(void);
	static obj_t BGl_objectzd2initzd2zzengine_heapz00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_addzd2genvz12zc0zzast_envz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31232ze3ze5zzengine_heapz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_dumpzd2Tenvzd2zzengine_heapz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31226ze3ze5zzengine_heapz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzengine_heapz00(void);
	extern obj_t BGl_initializa7ezd2Genvz12z67zzast_envz00(void);
	BGL_IMPORT obj_t close_binary_port(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzengine_heapz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_pragmaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzheap_restorez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	BGL_IMPORT bool_t fexists(char *);
	static obj_t BGl_z62zc3z04anonymousza31195ze3ze5zzengine_heapz00(obj_t,
		obj_t);
	extern obj_t BGl_setzd2tenvz12zc0zztype_envz00(obj_t);
	extern obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zzengine_heapz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzengine_heapz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzengine_heapz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzengine_heapz00(void);
	BGL_IMPORT obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
	static obj_t BGl_dumpzd2heapzd2zzengine_heapz00(obj_t);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	extern obj_t BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31664ze3ze5zzengine_heapz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_heapzd2modulezd2listz00zzheap_restorez00(obj_t);
	static obj_t __cnst[14];


	   
		 
		DEFINE_STRING(BGl_string1768z00zzengine_heapz00,
		BgL_bgl_string1768za700za7za7e1799za7, "dump-heap", 9);
	      DEFINE_STRING(BGl_string1769z00zzengine_heapz00,
		BgL_bgl_string1769za700za7za7e1800za7, "Corrupted heap", 14);
	      DEFINE_STRING(BGl_string1772z00zzengine_heapz00,
		BgL_bgl_string1772za700za7za7e1801za7, "Cannot open heap file ~s", 24);
	      DEFINE_STRING(BGl_string1773z00zzengine_heapz00,
		BgL_bgl_string1773za700za7za7e1802za7, "(heap \"", 7);
	      DEFINE_STRING(BGl_string1774z00zzengine_heapz00,
		BgL_bgl_string1774za700za7za7e1803za7, "\"", 1);
	      DEFINE_STRING(BGl_string1775z00zzengine_heapz00,
		BgL_bgl_string1775za700za7za7e1804za7, " (variables", 11);
	      DEFINE_STRING(BGl_string1776z00zzengine_heapz00,
		BgL_bgl_string1776za700za7za7e1805za7, " )\n", 3);
	      DEFINE_STRING(BGl_string1777z00zzengine_heapz00,
		BgL_bgl_string1777za700za7za7e1806za7, " (types", 7);
	      DEFINE_STRING(BGl_string1778z00zzengine_heapz00,
		BgL_bgl_string1778za700za7za7e1807za7, " )", 2);
	      DEFINE_STRING(BGl_string1779z00zzengine_heapz00,
		BgL_bgl_string1779za700za7za7e1808za7, " (includes", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1770z00zzengine_heapz00,
		BgL_bgl_za762za7c3za704anonymo1809za7,
		BGl_z62zc3z04anonymousza31195ze3ze5zzengine_heapz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1771z00zzengine_heapz00,
		BgL_bgl_za762za7c3za704anonymo1810za7,
		BGl_z62zc3z04anonymousza31202ze3ze5zzengine_heapz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1780z00zzengine_heapz00,
		BgL_bgl_string1780za700za7za7e1811za7, ")\n", 2);
	      DEFINE_STRING(BGl_string1782z00zzengine_heapz00,
		BgL_bgl_string1782za700za7za7e1812za7, "   ", 3);
	      DEFINE_STRING(BGl_string1783z00zzengine_heapz00,
		BgL_bgl_string1783za700za7za7e1813za7, "~s", 2);
	      DEFINE_STRING(BGl_string1784z00zzengine_heapz00,
		BgL_bgl_string1784za700za7za7e1814za7, "", 0);
	      DEFINE_STRING(BGl_string1785z00zzengine_heapz00,
		BgL_bgl_string1785za700za7za7e1815za7, "\n   ", 4);
	      DEFINE_STRING(BGl_string1786z00zzengine_heapz00,
		BgL_bgl_string1786za700za7za7e1816za7, "\n    ", 5);
	      DEFINE_STRING(BGl_string1788z00zzengine_heapz00,
		BgL_bgl_string1788za700za7za7e1817za7, "engine_heap", 11);
	      DEFINE_STRING(BGl_string1789z00zzengine_heapz00,
		BgL_bgl_string1789za700za7za7e1818za7,
		"type super variable class native function args jvm-type-name inline sifun qualified-type name module id ",
		104);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1781z00zzengine_heapz00,
		BgL_bgl_za762za7c3za704anonymo1819za7,
		BGl_z62zc3z04anonymousza31232ze3ze5zzengine_heapz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1787z00zzengine_heapz00,
		BgL_bgl_za762za7c3za704anonymo1820za7,
		BGl_z62zc3z04anonymousza31664ze3ze5zzengine_heapz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dumpzd2heapszd2envz00zzengine_heapz00,
		BgL_bgl_za762dumpza7d2heapsza71821za7,
		BGl_z62dumpzd2heapszb0zzengine_heapz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzengine_heapz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzengine_heapz00(long
		BgL_checksumz00_1721, char *BgL_fromz00_1722)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzengine_heapz00))
				{
					BGl_requirezd2initializa7ationz75zzengine_heapz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzengine_heapz00();
					BGl_libraryzd2moduleszd2initz00zzengine_heapz00();
					BGl_cnstzd2initzd2zzengine_heapz00();
					BGl_importedzd2moduleszd2initz00zzengine_heapz00();
					return BGl_methodzd2initzd2zzengine_heapz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzengine_heapz00(void)
	{
		{	/* Engine/heap.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"engine_heap");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"engine_heap");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "engine_heap");
			BGl_modulezd2initializa7ationz75zz__binaryz00(0L, "engine_heap");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "engine_heap");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "engine_heap");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "engine_heap");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "engine_heap");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "engine_heap");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"engine_heap");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"engine_heap");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "engine_heap");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "engine_heap");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "engine_heap");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzengine_heapz00(void)
	{
		{	/* Engine/heap.scm 15 */
			{	/* Engine/heap.scm 15 */
				obj_t BgL_cportz00_1485;

				{	/* Engine/heap.scm 15 */
					obj_t BgL_stringz00_1492;

					BgL_stringz00_1492 = BGl_string1789z00zzengine_heapz00;
					{	/* Engine/heap.scm 15 */
						obj_t BgL_startz00_1493;

						BgL_startz00_1493 = BINT(0L);
						{	/* Engine/heap.scm 15 */
							obj_t BgL_endz00_1494;

							BgL_endz00_1494 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1492)));
							{	/* Engine/heap.scm 15 */

								BgL_cportz00_1485 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1492, BgL_startz00_1493, BgL_endz00_1494);
				}}}}
				{
					long BgL_iz00_1486;

					BgL_iz00_1486 = 13L;
				BgL_loopz00_1487:
					if ((BgL_iz00_1486 == -1L))
						{	/* Engine/heap.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Engine/heap.scm 15 */
							{	/* Engine/heap.scm 15 */
								obj_t BgL_arg1798z00_1488;

								{	/* Engine/heap.scm 15 */

									{	/* Engine/heap.scm 15 */
										obj_t BgL_locationz00_1490;

										BgL_locationz00_1490 = BBOOL(((bool_t) 0));
										{	/* Engine/heap.scm 15 */

											BgL_arg1798z00_1488 =
												BGl_readz00zz__readerz00(BgL_cportz00_1485,
												BgL_locationz00_1490);
										}
									}
								}
								{	/* Engine/heap.scm 15 */
									int BgL_tmpz00_1754;

									BgL_tmpz00_1754 = (int) (BgL_iz00_1486);
									CNST_TABLE_SET(BgL_tmpz00_1754, BgL_arg1798z00_1488);
							}}
							{	/* Engine/heap.scm 15 */
								int BgL_auxz00_1491;

								BgL_auxz00_1491 = (int) ((BgL_iz00_1486 - 1L));
								{
									long BgL_iz00_1759;

									BgL_iz00_1759 = (long) (BgL_auxz00_1491);
									BgL_iz00_1486 = BgL_iz00_1759;
									goto BgL_loopz00_1487;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzengine_heapz00(void)
	{
		{	/* Engine/heap.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* dump-heaps */
	BGL_EXPORTED_DEF obj_t BGl_dumpzd2heapszd2zzengine_heapz00(obj_t
		BgL_namesz00_3)
	{
		{	/* Engine/heap.scm 33 */
			BGl_setzd2backendz12zc0zzbackend_backendz00
				(BGl_za2targetzd2languageza2zd2zzengine_paramz00);
			BGl_initializa7ezd2Genvz12z67zzast_envz00();
			BGl_initializa7ezd2Tenvz12z67zztype_envz00();
			{
				obj_t BgL_l1125z00_778;

				{	/* Engine/heap.scm 40 */
					bool_t BgL_tmpz00_1765;

					BgL_l1125z00_778 = BgL_namesz00_3;
				BgL_zc3z04anonymousza31173ze3z87_779:
					if (PAIRP(BgL_l1125z00_778))
						{	/* Engine/heap.scm 40 */
							BGl_dumpzd2heapzd2zzengine_heapz00(CAR(BgL_l1125z00_778));
							{
								obj_t BgL_l1125z00_1770;

								BgL_l1125z00_1770 = CDR(BgL_l1125z00_778);
								BgL_l1125z00_778 = BgL_l1125z00_1770;
								goto BgL_zc3z04anonymousza31173ze3z87_779;
							}
						}
					else
						{	/* Engine/heap.scm 40 */
							BgL_tmpz00_1765 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_1765);
				}
			}
		}

	}



/* &dump-heaps */
	obj_t BGl_z62dumpzd2heapszb0zzengine_heapz00(obj_t BgL_envz00_1458,
		obj_t BgL_namesz00_1459)
	{
		{	/* Engine/heap.scm 33 */
			return BGl_dumpzd2heapszd2zzengine_heapz00(BgL_namesz00_1459);
		}

	}



/* dump-heap */
	obj_t BGl_dumpzd2heapzd2zzengine_heapz00(obj_t BgL_heapz00_4)
	{
		{	/* Engine/heap.scm 45 */
			{	/* Engine/heap.scm 46 */
				obj_t BgL_fnamez00_784;

				if (fexists(BSTRING_TO_STRING(BgL_heapz00_4)))
					{	/* Engine/heap.scm 46 */
						BgL_fnamez00_784 = BgL_heapz00_4;
					}
				else
					{	/* Engine/heap.scm 46 */
						BgL_fnamez00_784 =
							BGl_findzd2filezf2pathz20zz__osz00(BgL_heapz00_4,
							BGl_za2libzd2dirza2zd2zzengine_paramz00);
					}
				if (STRINGP(BgL_fnamez00_784))
					{	/* Engine/heap.scm 50 */
						obj_t BgL_portz00_786;

						BgL_portz00_786 =
							BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00
							(BgL_fnamez00_784);
						if (BINARY_PORTP(BgL_portz00_786))
							{	/* Engine/heap.scm 55 */
								obj_t BgL_exitd1065z00_788;

								BgL_exitd1065z00_788 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Engine/heap.scm 115 */
									obj_t BgL_zc3z04anonymousza31226ze3z87_1463;

									BgL_zc3z04anonymousza31226ze3z87_1463 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31226ze3ze5zzengine_heapz00,
										(int) (0L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31226ze3z87_1463,
										(int) (0L), BgL_portz00_786);
									{	/* Engine/heap.scm 55 */
										obj_t BgL_arg1828z00_1131;

										{	/* Engine/heap.scm 55 */
											obj_t BgL_arg1829z00_1132;

											BgL_arg1829z00_1132 =
												BGL_EXITD_PROTECT(BgL_exitd1065z00_788);
											BgL_arg1828z00_1131 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31226ze3z87_1463,
												BgL_arg1829z00_1132);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1065z00_788,
											BgL_arg1828z00_1131);
										BUNSPEC;
									}
									{	/* Engine/heap.scm 56 */
										obj_t BgL_tmp1067z00_790;

										{	/* Engine/heap.scm 56 */
											obj_t BgL_envsz00_791;

											BgL_envsz00_791 = input_obj(BgL_portz00_786);
											{	/* Engine/heap.scm 56 */
												obj_t BgL__z00_792;

												{	/* Engine/heap.scm 57 */
													bool_t BgL_test1829z00_1793;

													{	/* Engine/heap.scm 57 */
														obj_t BgL_v1127z00_857;

														BgL_v1127z00_857 = create_vector(1L);
														VECTOR_SET(BgL_v1127z00_857, 0L, BgL_envsz00_791);
														((bool_t) 1);
													}
													{	/* Engine/heap.scm 58 */
														bool_t BgL__ortest_1079z00_854;

														BgL__ortest_1079z00_854 =
															(VECTOR_LENGTH(((obj_t) BgL_envsz00_791)) == 5L);
														if (BgL__ortest_1079z00_854)
															{	/* Engine/heap.scm 58 */
																BgL_test1829z00_1793 = BgL__ortest_1079z00_854;
															}
														else
															{	/* Engine/heap.scm 58 */
																BgL_test1829z00_1793 =
																	(VECTOR_LENGTH(
																		((obj_t) BgL_envsz00_791)) == 7L);
															}
													}
													if (BgL_test1829z00_1793)
														{	/* Engine/heap.scm 57 */
															BgL__z00_792 = BFALSE;
														}
													else
														{	/* Engine/heap.scm 57 */
															BgL__z00_792 =
																BGl_errorz00zz__errorz00
																(BGl_string1768z00zzengine_heapz00,
																BGl_string1769z00zzengine_heapz00,
																BgL_heapz00_4);
														}
												}
												{	/* Engine/heap.scm 57 */
													obj_t BgL_targetz00_793;

													BgL_targetz00_793 =
														VECTOR_REF(((obj_t) BgL_envsz00_791), 0L);
													{	/* Engine/heap.scm 61 */
														obj_t BgL_versionz00_794;

														BgL_versionz00_794 =
															VECTOR_REF(((obj_t) BgL_envsz00_791), 1L);
														{	/* Engine/heap.scm 62 */
															obj_t BgL_specificz00_795;

															BgL_specificz00_795 =
																VECTOR_REF(((obj_t) BgL_envsz00_791), 2L);
															{	/* Engine/heap.scm 63 */
																obj_t BgL_genvz00_796;

																BgL_genvz00_796 =
																	VECTOR_REF(((obj_t) BgL_envsz00_791), 3L);
																{	/* Engine/heap.scm 64 */
																	obj_t BgL_tenvz00_797;

																	BgL_tenvz00_797 =
																		VECTOR_REF(((obj_t) BgL_envsz00_791), 4L);
																	{	/* Engine/heap.scm 65 */
																		obj_t BgL_includesz00_798;

																		if (
																			(VECTOR_LENGTH(
																					((obj_t) BgL_envsz00_791)) == 6L))
																			{	/* Engine/heap.scm 66 */
																				BgL_includesz00_798 =
																					VECTOR_REF(
																					((obj_t) BgL_envsz00_791), 5L);
																			}
																		else
																			{	/* Engine/heap.scm 66 */
																				BgL_includesz00_798 = BNIL;
																			}
																		{	/* Engine/heap.scm 66 */

																			if (
																				(VECTOR_LENGTH(
																						((obj_t) BgL_envsz00_791)) == 6L))
																				{	/* Engine/heap.scm 78 */
																					BGl_addzd2genvz12zc0zzast_envz00
																						(BgL_genvz00_796);
																				}
																			else
																				{	/* Engine/heap.scm 78 */
																					BGl_setzd2tenvz12zc0zztype_envz00
																						(BgL_tenvz00_797);
																					BGl_setzd2genvz12zc0zzast_envz00
																						(BgL_genvz00_796);
																				}
																			{	/* Engine/heap.scm 87 */
																				bool_t BgL_test1834z00_1827;

																				{	/* Engine/heap.scm 87 */
																					obj_t BgL_arg1200z00_813;

																					BgL_arg1200z00_813 =
																						BGl_thezd2backendzd2zzbackend_backendz00
																						();
																					BgL_test1834z00_1827 =
																						(((BgL_backendz00_bglt)
																							COBJECT(((BgL_backendz00_bglt)
																									BgL_arg1200z00_813)))->
																						BgL_qualifiedzd2typeszd2);
																				}
																				if (BgL_test1834z00_1827)
																					{	/* Engine/heap.scm 87 */
																						BGl_forzd2eachzd2globalz12z12zzast_envz00
																							(BGl_proc1770z00zzengine_heapz00,
																							BNIL);
																					}
																				else
																					{	/* Engine/heap.scm 87 */
																						BFALSE;
																					}
																			}
																			BGl_hashtablezd2forzd2eachz00zz__hashz00
																				(BgL_genvz00_796,
																				BGl_proc1771z00zzengine_heapz00);
																			{	/* Engine/heap.scm 101 */
																				obj_t BgL_arg1209z00_829;

																				{	/* Engine/heap.scm 101 */
																					obj_t BgL_tmpz00_1833;

																					BgL_tmpz00_1833 =
																						BGL_CURRENT_DYNAMIC_ENV();
																					BgL_arg1209z00_829 =
																						BGL_ENV_CURRENT_ERROR_PORT
																						(BgL_tmpz00_1833);
																				}
																				{	/* Engine/heap.scm 103 */
																					obj_t
																						BgL_zc3z04anonymousza31211ze3z87_1460;
																					BgL_zc3z04anonymousza31211ze3z87_1460
																						=
																						MAKE_FX_PROCEDURE
																						(BGl_z62zc3z04anonymousza31211ze3ze5zzengine_heapz00,
																						(int) (0L), (int) (4L));
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31211ze3z87_1460,
																						(int) (0L), BgL_heapz00_4);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31211ze3z87_1460,
																						(int) (1L), BgL_genvz00_796);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31211ze3z87_1460,
																						(int) (2L), BgL_tenvz00_797);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31211ze3z87_1460,
																						(int) (3L), BgL_includesz00_798);
																					BgL_tmp1067z00_790 =
																						BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
																						(BgL_arg1209z00_829,
																						BgL_zc3z04anonymousza31211ze3z87_1460);
										}}}}}}}}}}}
										{	/* Engine/heap.scm 55 */
											bool_t BgL_test1835z00_1848;

											{	/* Engine/heap.scm 55 */
												obj_t BgL_arg1827z00_1194;

												BgL_arg1827z00_1194 =
													BGL_EXITD_PROTECT(BgL_exitd1065z00_788);
												BgL_test1835z00_1848 = PAIRP(BgL_arg1827z00_1194);
											}
											if (BgL_test1835z00_1848)
												{	/* Engine/heap.scm 55 */
													obj_t BgL_arg1825z00_1195;

													{	/* Engine/heap.scm 55 */
														obj_t BgL_arg1826z00_1196;

														BgL_arg1826z00_1196 =
															BGL_EXITD_PROTECT(BgL_exitd1065z00_788);
														BgL_arg1825z00_1195 =
															CDR(((obj_t) BgL_arg1826z00_1196));
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1065z00_788,
														BgL_arg1825z00_1195);
													BUNSPEC;
												}
											else
												{	/* Engine/heap.scm 55 */
													BFALSE;
												}
										}
										close_binary_port(BgL_portz00_786);
										return BgL_tmp1067z00_790;
									}
								}
							}
						else
							{	/* Engine/heap.scm 53 */
								obj_t BgL_arg1227z00_860;

								{	/* Engine/heap.scm 53 */
									obj_t BgL_list1228z00_861;

									BgL_list1228z00_861 = MAKE_YOUNG_PAIR(BgL_fnamez00_784, BNIL);
									BgL_arg1227z00_860 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string1772z00zzengine_heapz00, BgL_list1228z00_861);
								}
								return
									BGl_errorz00zz__errorz00(BGl_string1768z00zzengine_heapz00,
									BgL_arg1227z00_860, BGl_za2libzd2dirza2zd2zzengine_paramz00);
							}
					}
				else
					{	/* Engine/heap.scm 116 */
						obj_t BgL_mz00_862;

						{	/* Engine/heap.scm 116 */
							obj_t BgL_list1229z00_863;

							BgL_list1229z00_863 = MAKE_YOUNG_PAIR(BgL_heapz00_4, BNIL);
							BgL_mz00_862 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string1772z00zzengine_heapz00, BgL_list1229z00_863);
						}
						BGl_errorz00zz__errorz00(BGl_string1768z00zzengine_heapz00,
							BgL_mz00_862, BGl_za2libzd2dirza2zd2zzengine_paramz00);
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1211> */
	obj_t BGl_z62zc3z04anonymousza31211ze3ze5zzengine_heapz00(obj_t
		BgL_envz00_1464)
	{
		{	/* Engine/heap.scm 102 */
			{	/* Engine/heap.scm 103 */
				obj_t BgL_heapz00_1465;
				obj_t BgL_genvz00_1466;
				obj_t BgL_tenvz00_1467;
				obj_t BgL_includesz00_1468;

				BgL_heapz00_1465 = PROCEDURE_REF(BgL_envz00_1464, (int) (0L));
				BgL_genvz00_1466 = PROCEDURE_REF(BgL_envz00_1464, (int) (1L));
				BgL_tenvz00_1467 = PROCEDURE_REF(BgL_envz00_1464, (int) (2L));
				BgL_includesz00_1468 = PROCEDURE_REF(BgL_envz00_1464, (int) (3L));
				{	/* Engine/heap.scm 103 */
					obj_t BgL_port1131z00_1496;

					{	/* Engine/heap.scm 103 */
						obj_t BgL_tmpz00_1870;

						BgL_tmpz00_1870 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1131z00_1496 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1870);
					}
					bgl_display_string(BGl_string1773z00zzengine_heapz00,
						BgL_port1131z00_1496);
					bgl_display_obj(BgL_heapz00_1465, BgL_port1131z00_1496);
					bgl_display_string(BGl_string1774z00zzengine_heapz00,
						BgL_port1131z00_1496);
					bgl_display_char(((unsigned char) 10), BgL_port1131z00_1496);
				}
				{	/* Engine/heap.scm 104 */
					obj_t BgL_port1132z00_1497;

					{	/* Engine/heap.scm 104 */
						obj_t BgL_tmpz00_1877;

						BgL_tmpz00_1877 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1132z00_1497 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1877);
					}
					bgl_display_string(BGl_string1775z00zzengine_heapz00,
						BgL_port1132z00_1497);
					bgl_display_char(((unsigned char) 10), BgL_port1132z00_1497);
				}
				BGl_dumpzd2Genvzd2zzengine_heapz00(BgL_genvz00_1466);
				{	/* Engine/heap.scm 106 */
					obj_t BgL_port1133z00_1498;

					{	/* Engine/heap.scm 106 */
						obj_t BgL_tmpz00_1883;

						BgL_tmpz00_1883 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1133z00_1498 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1883);
					}
					bgl_display_string(BGl_string1776z00zzengine_heapz00,
						BgL_port1133z00_1498);
					bgl_display_char(((unsigned char) 10), BgL_port1133z00_1498);
				}
				{	/* Engine/heap.scm 107 */
					obj_t BgL_port1134z00_1499;

					{	/* Engine/heap.scm 107 */
						obj_t BgL_tmpz00_1888;

						BgL_tmpz00_1888 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1134z00_1499 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1888);
					}
					bgl_display_string(BGl_string1777z00zzengine_heapz00,
						BgL_port1134z00_1499);
					bgl_display_char(((unsigned char) 10), BgL_port1134z00_1499);
				}
				BGl_dumpzd2Tenvzd2zzengine_heapz00(BgL_tenvz00_1467);
				{	/* Engine/heap.scm 109 */
					obj_t BgL_port1135z00_1500;

					{	/* Engine/heap.scm 109 */
						obj_t BgL_tmpz00_1894;

						BgL_tmpz00_1894 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1135z00_1500 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1894);
					}
					bgl_display_string(BGl_string1778z00zzengine_heapz00,
						BgL_port1135z00_1500);
					bgl_display_char(((unsigned char) 10), BgL_port1135z00_1500);
				}
				if (PAIRP(BgL_includesz00_1468))
					{	/* Engine/heap.scm 110 */
						{	/* Engine/heap.scm 111 */
							obj_t BgL_port1136z00_1501;

							{	/* Engine/heap.scm 111 */
								obj_t BgL_tmpz00_1901;

								BgL_tmpz00_1901 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_port1136z00_1501 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1901);
							}
							bgl_display_string(BGl_string1779z00zzengine_heapz00,
								BgL_port1136z00_1501);
							bgl_display_char(((unsigned char) 10), BgL_port1136z00_1501);
						}
						{	/* Engine/heap.scm 112 */
							obj_t BgL_port1137z00_1502;

							{	/* Engine/heap.scm 112 */
								obj_t BgL_tmpz00_1906;

								BgL_tmpz00_1906 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_port1137z00_1502 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1906);
							}
							bgl_display_obj(BgL_includesz00_1468, BgL_port1137z00_1502);
							bgl_display_char(((unsigned char) 10), BgL_port1137z00_1502);
						}
						{	/* Engine/heap.scm 113 */
							obj_t BgL_port1138z00_1503;

							{	/* Engine/heap.scm 113 */
								obj_t BgL_tmpz00_1911;

								BgL_tmpz00_1911 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_port1138z00_1503 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1911);
							}
							bgl_display_string(BGl_string1778z00zzengine_heapz00,
								BgL_port1138z00_1503);
							bgl_display_char(((unsigned char) 10), BgL_port1138z00_1503);
					}}
				else
					{	/* Engine/heap.scm 110 */
						BFALSE;
					}
				{	/* Engine/heap.scm 114 */
					obj_t BgL_port1139z00_1504;

					{	/* Engine/heap.scm 114 */
						obj_t BgL_tmpz00_1916;

						BgL_tmpz00_1916 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1139z00_1504 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1916);
					}
					bgl_display_string(BGl_string1780z00zzengine_heapz00,
						BgL_port1139z00_1504);
					return bgl_display_char(((unsigned char) 10), BgL_port1139z00_1504);
		}}}

	}



/* &<@anonymous:1202> */
	obj_t BGl_z62zc3z04anonymousza31202ze3ze5zzengine_heapz00(obj_t
		BgL_envz00_1469, obj_t BgL_kz00_1470, obj_t BgL_bucketz00_1471)
	{
		{	/* Engine/heap.scm 97 */
			{	/* Engine/heap.scm 98 */
				bool_t BgL_tmpz00_1921;

				{	/* Engine/heap.scm 98 */
					obj_t BgL_g1130z00_1505;

					BgL_g1130z00_1505 = CDR(((obj_t) BgL_bucketz00_1471));
					{
						obj_t BgL_l1128z00_1507;

						BgL_l1128z00_1507 = BgL_g1130z00_1505;
					BgL_zc3z04anonymousza31203ze3z87_1506:
						if (PAIRP(BgL_l1128z00_1507))
							{	/* Engine/heap.scm 100 */
								{	/* Engine/heap.scm 99 */
									obj_t BgL_newz00_1508;

									BgL_newz00_1508 = CAR(BgL_l1128z00_1507);
									{	/* Engine/heap.scm 99 */
										obj_t BgL_arg1206z00_1509;

										BgL_arg1206z00_1509 =
											(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_newz00_1508)))->
											BgL_modulez00);
										{	/* Engine/heap.scm 99 */
											obj_t BgL_list1207z00_1510;

											BgL_list1207z00_1510 =
												MAKE_YOUNG_PAIR(BgL_arg1206z00_1509, BNIL);
											BGl_heapzd2modulezd2listz00zzheap_restorez00
												(BgL_list1207z00_1510);
										}
									}
								}
								{
									obj_t BgL_l1128z00_1931;

									BgL_l1128z00_1931 = CDR(BgL_l1128z00_1507);
									BgL_l1128z00_1507 = BgL_l1128z00_1931;
									goto BgL_zc3z04anonymousza31203ze3z87_1506;
								}
							}
						else
							{	/* Engine/heap.scm 100 */
								BgL_tmpz00_1921 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_1921);
			}
		}

	}



/* &<@anonymous:1195> */
	obj_t BGl_z62zc3z04anonymousza31195ze3ze5zzengine_heapz00(obj_t
		BgL_envz00_1472, obj_t BgL_newz00_1473)
	{
		{	/* Engine/heap.scm 89 */
			{	/* Engine/heap.scm 91 */
				obj_t BgL_arg1196z00_1511;
				obj_t BgL_arg1197z00_1512;
				obj_t BgL_arg1198z00_1513;

				BgL_arg1196z00_1511 =
					(((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_newz00_1473)))->BgL_modulez00);
				BgL_arg1197z00_1512 =
					(((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_newz00_1473)))->
					BgL_jvmzd2typezd2namez00);
				BgL_arg1198z00_1513 = BGl_shapez00zztools_shapez00(BgL_newz00_1473);
				{	/* Engine/heap.scm 90 */
					obj_t BgL_list1199z00_1514;

					BgL_list1199z00_1514 = MAKE_YOUNG_PAIR(BgL_arg1198z00_1513, BNIL);
					return
						BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(BgL_arg1196z00_1511,
						BgL_arg1197z00_1512, BgL_list1199z00_1514);
				}
			}
		}

	}



/* &<@anonymous:1226> */
	obj_t BGl_z62zc3z04anonymousza31226ze3ze5zzengine_heapz00(obj_t
		BgL_envz00_1474)
	{
		{	/* Engine/heap.scm 55 */
			{	/* Engine/heap.scm 115 */
				obj_t BgL_portz00_1475;

				BgL_portz00_1475 = PROCEDURE_REF(BgL_envz00_1474, (int) (0L));
				return close_binary_port(((obj_t) BgL_portz00_1475));
			}
		}

	}



/* dump-Genv */
	obj_t BGl_dumpzd2Genvzd2zzengine_heapz00(obj_t BgL_genvz00_5)
	{
		{	/* Engine/heap.scm 123 */
			return
				BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_genvz00_5,
				BGl_proc1781z00zzengine_heapz00);
		}

	}



/* &<@anonymous:1232> */
	obj_t BGl_z62zc3z04anonymousza31232ze3ze5zzengine_heapz00(obj_t
		BgL_envz00_1477, obj_t BgL_kz00_1478, obj_t BgL_bucketz00_1479)
	{
		{	/* Engine/heap.scm 178 */
			{	/* Engine/heap.scm 178 */
				bool_t BgL_tmpz00_1946;

				{
					obj_t BgL_newz00_1516;

					{	/* Engine/heap.scm 178 */
						obj_t BgL_g1159z00_1672;

						BgL_g1159z00_1672 = CDR(((obj_t) BgL_bucketz00_1479));
						{
							obj_t BgL_l1155z00_1674;

							BgL_l1155z00_1674 = BgL_g1159z00_1672;
						BgL_zc3z04anonymousza31233ze3z87_1673:
							if (PAIRP(BgL_l1155z00_1674))
								{	/* Engine/heap.scm 178 */
									BgL_newz00_1516 = CAR(BgL_l1155z00_1674);
									{	/* Engine/heap.scm 125 */
										obj_t BgL_modulez00_1517;

										BgL_modulez00_1517 =
											(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_newz00_1516)))->
											BgL_modulez00);
										{	/* Engine/heap.scm 125 */
											obj_t BgL_idz00_1518;

											BgL_idz00_1518 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_newz00_1516))))->
												BgL_idz00);
											{	/* Engine/heap.scm 126 */
												obj_t BgL_qtz00_1519;

												BgL_qtz00_1519 =
													BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00
													(BgL_modulez00_1517);
												{	/* Engine/heap.scm 127 */
													obj_t BgL_jtz00_1520;

													BgL_jtz00_1520 =
														(((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_newz00_1516)))->
														BgL_jvmzd2typezd2namez00);
													{	/* Engine/heap.scm 128 */
														BgL_valuez00_bglt BgL_valz00_1521;

														BgL_valz00_1521 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_newz00_1516))))->
															BgL_valuez00);
														{	/* Engine/heap.scm 129 */

															{	/* Engine/heap.scm 131 */
																bool_t BgL_test1839z00_1962;

																{	/* Engine/heap.scm 131 */
																	obj_t BgL_classz00_1522;

																	BgL_classz00_1522 = BGl_sfunz00zzast_varz00;
																	{	/* Engine/heap.scm 131 */
																		BgL_objectz00_bglt BgL_arg1807z00_1523;

																		{	/* Engine/heap.scm 131 */
																			obj_t BgL_tmpz00_1963;

																			BgL_tmpz00_1963 =
																				((obj_t)
																				((BgL_objectz00_bglt) BgL_valz00_1521));
																			BgL_arg1807z00_1523 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_1963);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Engine/heap.scm 131 */
																				long BgL_idxz00_1524;

																				BgL_idxz00_1524 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_1523);
																				BgL_test1839z00_1962 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_1524 + 3L)) ==
																					BgL_classz00_1522);
																			}
																		else
																			{	/* Engine/heap.scm 131 */
																				bool_t BgL_res1762z00_1527;

																				{	/* Engine/heap.scm 131 */
																					obj_t BgL_oclassz00_1528;

																					{	/* Engine/heap.scm 131 */
																						obj_t BgL_arg1815z00_1529;
																						long BgL_arg1816z00_1530;

																						BgL_arg1815z00_1529 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Engine/heap.scm 131 */
																							long BgL_arg1817z00_1531;

																							BgL_arg1817z00_1531 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_1523);
																							BgL_arg1816z00_1530 =
																								(BgL_arg1817z00_1531 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_1528 =
																							VECTOR_REF(BgL_arg1815z00_1529,
																							BgL_arg1816z00_1530);
																					}
																					{	/* Engine/heap.scm 131 */
																						bool_t BgL__ortest_1115z00_1532;

																						BgL__ortest_1115z00_1532 =
																							(BgL_classz00_1522 ==
																							BgL_oclassz00_1528);
																						if (BgL__ortest_1115z00_1532)
																							{	/* Engine/heap.scm 131 */
																								BgL_res1762z00_1527 =
																									BgL__ortest_1115z00_1532;
																							}
																						else
																							{	/* Engine/heap.scm 131 */
																								long BgL_odepthz00_1533;

																								{	/* Engine/heap.scm 131 */
																									obj_t BgL_arg1804z00_1534;

																									BgL_arg1804z00_1534 =
																										(BgL_oclassz00_1528);
																									BgL_odepthz00_1533 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_1534);
																								}
																								if ((3L < BgL_odepthz00_1533))
																									{	/* Engine/heap.scm 131 */
																										obj_t BgL_arg1802z00_1535;

																										{	/* Engine/heap.scm 131 */
																											obj_t BgL_arg1803z00_1536;

																											BgL_arg1803z00_1536 =
																												(BgL_oclassz00_1528);
																											BgL_arg1802z00_1535 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_1536,
																												3L);
																										}
																										BgL_res1762z00_1527 =
																											(BgL_arg1802z00_1535 ==
																											BgL_classz00_1522);
																									}
																								else
																									{	/* Engine/heap.scm 131 */
																										BgL_res1762z00_1527 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test1839z00_1962 =
																					BgL_res1762z00_1527;
																			}
																	}
																}
																if (BgL_test1839z00_1962)
																	{	/* Engine/heap.scm 132 */
																		obj_t BgL_port1146z00_1537;

																		{	/* Engine/heap.scm 132 */
																			obj_t BgL_tmpz00_1986;

																			BgL_tmpz00_1986 =
																				BGL_CURRENT_DYNAMIC_ENV();
																			BgL_port1146z00_1537 =
																				BGL_ENV_CURRENT_OUTPUT_PORT
																				(BgL_tmpz00_1986);
																		}
																		bgl_display_string
																			(BGl_string1782z00zzengine_heapz00,
																			BgL_port1146z00_1537);
																		{	/* Engine/heap.scm 133 */
																			obj_t BgL_arg1242z00_1538;

																			{	/* Engine/heap.scm 133 */
																				obj_t BgL_arg1244z00_1539;

																				{	/* Engine/heap.scm 133 */
																					obj_t BgL_arg1248z00_1540;
																					obj_t BgL_arg1249z00_1541;

																					BgL_arg1248z00_1540 =
																						BGl_shapez00zztools_shapez00
																						(BgL_newz00_1516);
																					{	/* Engine/heap.scm 135 */
																						obj_t BgL_arg1252z00_1542;

																						{	/* Engine/heap.scm 135 */
																							obj_t BgL_arg1268z00_1543;
																							obj_t BgL_arg1272z00_1544;

																							{	/* Engine/heap.scm 135 */
																								obj_t BgL_arg1284z00_1545;

																								BgL_arg1284z00_1545 =
																									MAKE_YOUNG_PAIR
																									(BgL_idz00_1518, BNIL);
																								BgL_arg1268z00_1543 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(0), BgL_arg1284z00_1545);
																							}
																							{	/* Engine/heap.scm 137 */
																								obj_t BgL_arg1304z00_1546;

																								{	/* Engine/heap.scm 137 */
																									obj_t BgL_arg1305z00_1547;
																									obj_t BgL_arg1306z00_1548;

																									{	/* Engine/heap.scm 137 */
																										obj_t BgL_arg1307z00_1549;

																										BgL_arg1307z00_1549 =
																											MAKE_YOUNG_PAIR
																											(BgL_modulez00_1517,
																											BNIL);
																										BgL_arg1305z00_1547 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(1),
																											BgL_arg1307z00_1549);
																									}
																									{	/* Engine/heap.scm 139 */
																										obj_t BgL_arg1308z00_1550;

																										{	/* Engine/heap.scm 139 */
																											obj_t BgL_arg1310z00_1551;
																											obj_t BgL_arg1311z00_1552;

																											{	/* Engine/heap.scm 139 */
																												obj_t
																													BgL_arg1312z00_1553;
																												{	/* Engine/heap.scm 139 */
																													obj_t
																														BgL_arg1314z00_1554;
																													{	/* Engine/heap.scm 139 */
																														obj_t
																															BgL_arg1315z00_1555;
																														BgL_arg1315z00_1555
																															=
																															(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_newz00_1516))))->BgL_namez00);
																														{	/* Engine/heap.scm 139 */
																															obj_t
																																BgL_list1316z00_1556;
																															BgL_list1316z00_1556
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1315z00_1555,
																																BNIL);
																															BgL_arg1314z00_1554
																																=
																																BGl_formatz00zz__r4_output_6_10_3z00
																																(BGl_string1783z00zzengine_heapz00,
																																BgL_list1316z00_1556);
																														}
																													}
																													BgL_arg1312z00_1553 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1314z00_1554,
																														BNIL);
																												}
																												BgL_arg1310z00_1551 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(2),
																													BgL_arg1312z00_1553);
																											}
																											{	/* Engine/heap.scm 141 */
																												obj_t
																													BgL_arg1317z00_1557;
																												{	/* Engine/heap.scm 141 */
																													obj_t
																														BgL_arg1318z00_1558;
																													obj_t
																														BgL_arg1319z00_1559;
																													{	/* Engine/heap.scm 141 */
																														obj_t
																															BgL_arg1320z00_1560;
																														BgL_arg1320z00_1560
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_qtz00_1519,
																															BNIL);
																														BgL_arg1318z00_1558
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(3),
																															BgL_arg1320z00_1560);
																													}
																													{	/* Engine/heap.scm 142 */
																														obj_t
																															BgL_arg1321z00_1561;
																														obj_t
																															BgL_arg1322z00_1562;
																														if (((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) BgL_valz00_1521)))->BgL_classz00) == CNST_TABLE_REF(4)))
																															{	/* Engine/heap.scm 143 */
																																obj_t
																																	BgL_arg1325z00_1563;
																																{	/* Engine/heap.scm 143 */
																																	obj_t
																																		BgL_arg1326z00_1564;
																																	{	/* Engine/heap.scm 143 */
																																		obj_t
																																			BgL_arg1327z00_1565;
																																		BgL_arg1327z00_1565
																																			=
																																			(((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) BgL_valz00_1521)))->BgL_bodyz00);
																																		BgL_arg1326z00_1564
																																			=
																																			BGl_shapez00zztools_shapez00
																																			(BgL_arg1327z00_1565);
																																	}
																																	BgL_arg1325z00_1563
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1326z00_1564,
																																		BNIL);
																																}
																																BgL_arg1321z00_1561
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(5),
																																	BgL_arg1325z00_1563);
																															}
																														else
																															{	/* Engine/heap.scm 142 */
																																BgL_arg1321z00_1561
																																	=
																																	BGl_string1784z00zzengine_heapz00;
																															}
																														{	/* Engine/heap.scm 146 */
																															obj_t
																																BgL_arg1329z00_1566;
																															{	/* Engine/heap.scm 146 */
																																obj_t
																																	BgL_arg1331z00_1567;
																																obj_t
																																	BgL_arg1332z00_1568;
																																{	/* Engine/heap.scm 146 */
																																	obj_t
																																		BgL_arg1333z00_1569;
																																	BgL_arg1333z00_1569
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_jtz00_1520,
																																		BNIL);
																																	BgL_arg1331z00_1567
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(6),
																																		BgL_arg1333z00_1569);
																																}
																																{	/* Engine/heap.scm 147 */
																																	obj_t
																																		BgL_arg1335z00_1570;
																																	{	/* Engine/heap.scm 147 */
																																		obj_t
																																			BgL_arg1339z00_1571;
																																		{	/* Engine/heap.scm 147 */
																																			obj_t
																																				BgL_arg1340z00_1572;
																																			{	/* Engine/heap.scm 147 */
																																				obj_t
																																					BgL_arg1342z00_1573;
																																				{	/* Engine/heap.scm 147 */
																																					obj_t
																																						BgL_l1140z00_1574;
																																					BgL_l1140z00_1574
																																						=
																																						(((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) BgL_valz00_1521)))->BgL_argsz00);
																																					if (NULLP(BgL_l1140z00_1574))
																																						{	/* Engine/heap.scm 147 */
																																							BgL_arg1342z00_1573
																																								=
																																								BNIL;
																																						}
																																					else
																																						{	/* Engine/heap.scm 147 */
																																							obj_t
																																								BgL_head1142z00_1575;
																																							{	/* Engine/heap.scm 147 */
																																								obj_t
																																									BgL_arg1352z00_1576;
																																								{	/* Engine/heap.scm 147 */
																																									obj_t
																																										BgL_arg1361z00_1577;
																																									BgL_arg1361z00_1577
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_l1140z00_1574));
																																									BgL_arg1352z00_1576
																																										=
																																										BGl_shapez00zztools_shapez00
																																										(BgL_arg1361z00_1577);
																																								}
																																								BgL_head1142z00_1575
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1352z00_1576,
																																									BNIL);
																																							}
																																							{	/* Engine/heap.scm 147 */
																																								obj_t
																																									BgL_g1145z00_1578;
																																								BgL_g1145z00_1578
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_l1140z00_1574));
																																								{
																																									obj_t
																																										BgL_l1140z00_1580;
																																									obj_t
																																										BgL_tail1143z00_1581;
																																									BgL_l1140z00_1580
																																										=
																																										BgL_g1145z00_1578;
																																									BgL_tail1143z00_1581
																																										=
																																										BgL_head1142z00_1575;
																																								BgL_zc3z04anonymousza31345ze3z87_1579:
																																									if (NULLP(BgL_l1140z00_1580))
																																										{	/* Engine/heap.scm 147 */
																																											BgL_arg1342z00_1573
																																												=
																																												BgL_head1142z00_1575;
																																										}
																																									else
																																										{	/* Engine/heap.scm 147 */
																																											obj_t
																																												BgL_newtail1144z00_1582;
																																											{	/* Engine/heap.scm 147 */
																																												obj_t
																																													BgL_arg1349z00_1583;
																																												{	/* Engine/heap.scm 147 */
																																													obj_t
																																														BgL_arg1351z00_1584;
																																													BgL_arg1351z00_1584
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_l1140z00_1580));
																																													BgL_arg1349z00_1583
																																														=
																																														BGl_shapez00zztools_shapez00
																																														(BgL_arg1351z00_1584);
																																												}
																																												BgL_newtail1144z00_1582
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1349z00_1583,
																																													BNIL);
																																											}
																																											SET_CDR
																																												(BgL_tail1143z00_1581,
																																												BgL_newtail1144z00_1582);
																																											{	/* Engine/heap.scm 147 */
																																												obj_t
																																													BgL_arg1348z00_1585;
																																												BgL_arg1348z00_1585
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_l1140z00_1580));
																																												{
																																													obj_t
																																														BgL_tail1143z00_2042;
																																													obj_t
																																														BgL_l1140z00_2041;
																																													BgL_l1140z00_2041
																																														=
																																														BgL_arg1348z00_1585;
																																													BgL_tail1143z00_2042
																																														=
																																														BgL_newtail1144z00_1582;
																																													BgL_tail1143z00_1581
																																														=
																																														BgL_tail1143z00_2042;
																																													BgL_l1140z00_1580
																																														=
																																														BgL_l1140z00_2041;
																																													goto
																																														BgL_zc3z04anonymousza31345ze3z87_1579;
																																												}
																																											}
																																										}
																																								}
																																							}
																																						}
																																				}
																																				BgL_arg1340z00_1572
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1342z00_1573,
																																					BNIL);
																																			}
																																			BgL_arg1339z00_1571
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(7),
																																				BgL_arg1340z00_1572);
																																		}
																																		BgL_arg1335z00_1570
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1339z00_1571,
																																			BNIL);
																																	}
																																	BgL_arg1332z00_1568
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_string1785z00zzengine_heapz00,
																																		BgL_arg1335z00_1570);
																																}
																																BgL_arg1329z00_1566
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1331z00_1567,
																																	BgL_arg1332z00_1568);
																															}
																															BgL_arg1322z00_1562
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_string1786z00zzengine_heapz00,
																																BgL_arg1329z00_1566);
																														}
																														BgL_arg1319z00_1559
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1321z00_1561,
																															BgL_arg1322z00_1562);
																													}
																													BgL_arg1317z00_1557 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1318z00_1558,
																														BgL_arg1319z00_1559);
																												}
																												BgL_arg1311z00_1552 =
																													MAKE_YOUNG_PAIR
																													(BGl_string1786z00zzengine_heapz00,
																													BgL_arg1317z00_1557);
																											}
																											BgL_arg1308z00_1550 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1310z00_1551,
																												BgL_arg1311z00_1552);
																										}
																										BgL_arg1306z00_1548 =
																											MAKE_YOUNG_PAIR
																											(BGl_string1786z00zzengine_heapz00,
																											BgL_arg1308z00_1550);
																									}
																									BgL_arg1304z00_1546 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1305z00_1547,
																										BgL_arg1306z00_1548);
																								}
																								BgL_arg1272z00_1544 =
																									MAKE_YOUNG_PAIR
																									(BGl_string1786z00zzengine_heapz00,
																									BgL_arg1304z00_1546);
																							}
																							BgL_arg1252z00_1542 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1268z00_1543,
																								BgL_arg1272z00_1544);
																						}
																						BgL_arg1249z00_1541 =
																							MAKE_YOUNG_PAIR
																							(BGl_string1786z00zzengine_heapz00,
																							BgL_arg1252z00_1542);
																					}
																					BgL_arg1244z00_1539 =
																						MAKE_YOUNG_PAIR(BgL_arg1248z00_1540,
																						BgL_arg1249z00_1541);
																				}
																				BgL_arg1242z00_1538 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																					BgL_arg1244z00_1539);
																			}
																			bgl_display_obj(BgL_arg1242z00_1538,
																				BgL_port1146z00_1537);
																		}
																		bgl_display_char(((unsigned char) 10),
																			BgL_port1146z00_1537);
																	}
																else
																	{	/* Engine/heap.scm 148 */
																		bool_t BgL_test1846z00_2064;

																		{	/* Engine/heap.scm 148 */
																			obj_t BgL_classz00_1586;

																			BgL_classz00_1586 =
																				BGl_cfunz00zzast_varz00;
																			{	/* Engine/heap.scm 148 */
																				BgL_objectz00_bglt BgL_arg1807z00_1587;

																				{	/* Engine/heap.scm 148 */
																					obj_t BgL_tmpz00_2065;

																					BgL_tmpz00_2065 =
																						((obj_t)
																						((BgL_objectz00_bglt)
																							BgL_valz00_1521));
																					BgL_arg1807z00_1587 =
																						(BgL_objectz00_bglt)
																						(BgL_tmpz00_2065);
																				}
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Engine/heap.scm 148 */
																						long BgL_idxz00_1588;

																						BgL_idxz00_1588 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_1587);
																						BgL_test1846z00_2064 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_1588 + 3L)) ==
																							BgL_classz00_1586);
																					}
																				else
																					{	/* Engine/heap.scm 148 */
																						bool_t BgL_res1763z00_1591;

																						{	/* Engine/heap.scm 148 */
																							obj_t BgL_oclassz00_1592;

																							{	/* Engine/heap.scm 148 */
																								obj_t BgL_arg1815z00_1593;
																								long BgL_arg1816z00_1594;

																								BgL_arg1815z00_1593 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Engine/heap.scm 148 */
																									long BgL_arg1817z00_1595;

																									BgL_arg1817z00_1595 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_1587);
																									BgL_arg1816z00_1594 =
																										(BgL_arg1817z00_1595 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_1592 =
																									VECTOR_REF
																									(BgL_arg1815z00_1593,
																									BgL_arg1816z00_1594);
																							}
																							{	/* Engine/heap.scm 148 */
																								bool_t BgL__ortest_1115z00_1596;

																								BgL__ortest_1115z00_1596 =
																									(BgL_classz00_1586 ==
																									BgL_oclassz00_1592);
																								if (BgL__ortest_1115z00_1596)
																									{	/* Engine/heap.scm 148 */
																										BgL_res1763z00_1591 =
																											BgL__ortest_1115z00_1596;
																									}
																								else
																									{	/* Engine/heap.scm 148 */
																										long BgL_odepthz00_1597;

																										{	/* Engine/heap.scm 148 */
																											obj_t BgL_arg1804z00_1598;

																											BgL_arg1804z00_1598 =
																												(BgL_oclassz00_1592);
																											BgL_odepthz00_1597 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_1598);
																										}
																										if (
																											(3L < BgL_odepthz00_1597))
																											{	/* Engine/heap.scm 148 */
																												obj_t
																													BgL_arg1802z00_1599;
																												{	/* Engine/heap.scm 148 */
																													obj_t
																														BgL_arg1803z00_1600;
																													BgL_arg1803z00_1600 =
																														(BgL_oclassz00_1592);
																													BgL_arg1802z00_1599 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_1600,
																														3L);
																												}
																												BgL_res1763z00_1591 =
																													(BgL_arg1802z00_1599
																													== BgL_classz00_1586);
																											}
																										else
																											{	/* Engine/heap.scm 148 */
																												BgL_res1763z00_1591 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test1846z00_2064 =
																							BgL_res1763z00_1591;
																					}
																			}
																		}
																		if (BgL_test1846z00_2064)
																			{	/* Engine/heap.scm 149 */
																				obj_t BgL_port1153z00_1601;

																				{	/* Engine/heap.scm 149 */
																					obj_t BgL_tmpz00_2088;

																					BgL_tmpz00_2088 =
																						BGL_CURRENT_DYNAMIC_ENV();
																					BgL_port1153z00_1601 =
																						BGL_ENV_CURRENT_OUTPUT_PORT
																						(BgL_tmpz00_2088);
																				}
																				bgl_display_string
																					(BGl_string1782z00zzengine_heapz00,
																					BgL_port1153z00_1601);
																				{	/* Engine/heap.scm 150 */
																					obj_t BgL_arg1364z00_1602;

																					{	/* Engine/heap.scm 150 */
																						obj_t BgL_arg1367z00_1603;

																						{	/* Engine/heap.scm 150 */
																							obj_t BgL_arg1370z00_1604;
																							obj_t BgL_arg1371z00_1605;

																							BgL_arg1370z00_1604 =
																								BGl_shapez00zztools_shapez00
																								(BgL_newz00_1516);
																							{	/* Engine/heap.scm 152 */
																								obj_t BgL_arg1375z00_1606;

																								{	/* Engine/heap.scm 152 */
																									obj_t BgL_arg1376z00_1607;
																									obj_t BgL_arg1377z00_1608;

																									{	/* Engine/heap.scm 152 */
																										obj_t BgL_arg1378z00_1609;

																										BgL_arg1378z00_1609 =
																											MAKE_YOUNG_PAIR
																											(BgL_idz00_1518, BNIL);
																										BgL_arg1376z00_1607 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(0),
																											BgL_arg1378z00_1609);
																									}
																									{	/* Engine/heap.scm 154 */
																										obj_t BgL_arg1379z00_1610;

																										{	/* Engine/heap.scm 154 */
																											obj_t BgL_arg1380z00_1611;
																											obj_t BgL_arg1408z00_1612;

																											{	/* Engine/heap.scm 154 */
																												obj_t
																													BgL_arg1410z00_1613;
																												BgL_arg1410z00_1613 =
																													MAKE_YOUNG_PAIR
																													(BgL_modulez00_1517,
																													BNIL);
																												BgL_arg1380z00_1611 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(1),
																													BgL_arg1410z00_1613);
																											}
																											{	/* Engine/heap.scm 156 */
																												obj_t
																													BgL_arg1421z00_1614;
																												{	/* Engine/heap.scm 156 */
																													obj_t
																														BgL_arg1422z00_1615;
																													obj_t
																														BgL_arg1434z00_1616;
																													{	/* Engine/heap.scm 156 */
																														obj_t
																															BgL_arg1437z00_1617;
																														{	/* Engine/heap.scm 156 */
																															obj_t
																																BgL_arg1448z00_1618;
																															{	/* Engine/heap.scm 156 */
																																obj_t
																																	BgL_arg1453z00_1619;
																																BgL_arg1453z00_1619
																																	=
																																	(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_newz00_1516))))->BgL_namez00);
																																{	/* Engine/heap.scm 156 */
																																	obj_t
																																		BgL_list1454z00_1620;
																																	BgL_list1454z00_1620
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1453z00_1619,
																																		BNIL);
																																	BgL_arg1448z00_1618
																																		=
																																		BGl_formatz00zz__r4_output_6_10_3z00
																																		(BGl_string1783z00zzengine_heapz00,
																																		BgL_list1454z00_1620);
																																}
																															}
																															BgL_arg1437z00_1617
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1448z00_1618,
																																BNIL);
																														}
																														BgL_arg1422z00_1615
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(2),
																															BgL_arg1437z00_1617);
																													}
																													{	/* Engine/heap.scm 158 */
																														obj_t
																															BgL_arg1472z00_1621;
																														{	/* Engine/heap.scm 158 */
																															obj_t
																																BgL_arg1473z00_1622;
																															obj_t
																																BgL_arg1485z00_1623;
																															{	/* Engine/heap.scm 158 */
																																obj_t
																																	BgL_arg1489z00_1624;
																																BgL_arg1489z00_1624
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_qtz00_1519,
																																	BNIL);
																																BgL_arg1473z00_1622
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(3),
																																	BgL_arg1489z00_1624);
																															}
																															{	/* Engine/heap.scm 160 */
																																obj_t
																																	BgL_arg1502z00_1625;
																																{	/* Engine/heap.scm 160 */
																																	obj_t
																																		BgL_arg1509z00_1626;
																																	obj_t
																																		BgL_arg1513z00_1627;
																																	{	/* Engine/heap.scm 160 */
																																		obj_t
																																			BgL_arg1514z00_1628;
																																		BgL_arg1514z00_1628
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_jtz00_1520,
																																			BNIL);
																																		BgL_arg1509z00_1626
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(6),
																																			BgL_arg1514z00_1628);
																																	}
																																	{	/* Engine/heap.scm 161 */
																																		obj_t
																																			BgL_arg1516z00_1629;
																																		{	/* Engine/heap.scm 161 */
																																			obj_t
																																				BgL_arg1535z00_1630;
																																			{	/* Engine/heap.scm 161 */
																																				obj_t
																																					BgL_arg1540z00_1631;
																																				{	/* Engine/heap.scm 161 */
																																					obj_t
																																						BgL_arg1544z00_1632;
																																					{	/* Engine/heap.scm 161 */
																																						obj_t
																																							BgL_l1147z00_1633;
																																						BgL_l1147z00_1633
																																							=
																																							(((BgL_cfunz00_bglt) COBJECT(((BgL_cfunz00_bglt) BgL_valz00_1521)))->BgL_argszd2typezd2);
																																						if (NULLP(BgL_l1147z00_1633))
																																							{	/* Engine/heap.scm 161 */
																																								BgL_arg1544z00_1632
																																									=
																																									BNIL;
																																							}
																																						else
																																							{	/* Engine/heap.scm 161 */
																																								obj_t
																																									BgL_head1149z00_1634;
																																								{	/* Engine/heap.scm 161 */
																																									obj_t
																																										BgL_arg1561z00_1635;
																																									{	/* Engine/heap.scm 161 */
																																										obj_t
																																											BgL_arg1564z00_1636;
																																										BgL_arg1564z00_1636
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_l1147z00_1633));
																																										BgL_arg1561z00_1635
																																											=
																																											BGl_shapez00zztools_shapez00
																																											(BgL_arg1564z00_1636);
																																									}
																																									BgL_head1149z00_1634
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1561z00_1635,
																																										BNIL);
																																								}
																																								{	/* Engine/heap.scm 161 */
																																									obj_t
																																										BgL_g1152z00_1637;
																																									BgL_g1152z00_1637
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_l1147z00_1633));
																																									{
																																										obj_t
																																											BgL_l1147z00_1639;
																																										obj_t
																																											BgL_tail1150z00_1640;
																																										BgL_l1147z00_1639
																																											=
																																											BgL_g1152z00_1637;
																																										BgL_tail1150z00_1640
																																											=
																																											BgL_head1149z00_1634;
																																									BgL_zc3z04anonymousza31547ze3z87_1638:
																																										if (NULLP(BgL_l1147z00_1639))
																																											{	/* Engine/heap.scm 161 */
																																												BgL_arg1544z00_1632
																																													=
																																													BgL_head1149z00_1634;
																																											}
																																										else
																																											{	/* Engine/heap.scm 161 */
																																												obj_t
																																													BgL_newtail1151z00_1641;
																																												{	/* Engine/heap.scm 161 */
																																													obj_t
																																														BgL_arg1553z00_1642;
																																													{	/* Engine/heap.scm 161 */
																																														obj_t
																																															BgL_arg1559z00_1643;
																																														BgL_arg1559z00_1643
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_l1147z00_1639));
																																														BgL_arg1553z00_1642
																																															=
																																															BGl_shapez00zztools_shapez00
																																															(BgL_arg1559z00_1643);
																																													}
																																													BgL_newtail1151z00_1641
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1553z00_1642,
																																														BNIL);
																																												}
																																												SET_CDR
																																													(BgL_tail1150z00_1640,
																																													BgL_newtail1151z00_1641);
																																												{	/* Engine/heap.scm 161 */
																																													obj_t
																																														BgL_arg1552z00_1644;
																																													BgL_arg1552z00_1644
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_l1147z00_1639));
																																													{
																																														obj_t
																																															BgL_tail1150z00_2133;
																																														obj_t
																																															BgL_l1147z00_2132;
																																														BgL_l1147z00_2132
																																															=
																																															BgL_arg1552z00_1644;
																																														BgL_tail1150z00_2133
																																															=
																																															BgL_newtail1151z00_1641;
																																														BgL_tail1150z00_1640
																																															=
																																															BgL_tail1150z00_2133;
																																														BgL_l1147z00_1639
																																															=
																																															BgL_l1147z00_2132;
																																														goto
																																															BgL_zc3z04anonymousza31547ze3z87_1638;
																																													}
																																												}
																																											}
																																									}
																																								}
																																							}
																																					}
																																					BgL_arg1540z00_1631
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1544z00_1632,
																																						BNIL);
																																				}
																																				BgL_arg1535z00_1630
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(7),
																																					BgL_arg1540z00_1631);
																																			}
																																			BgL_arg1516z00_1629
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1535z00_1630,
																																				BNIL);
																																		}
																																		BgL_arg1513z00_1627
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_string1785z00zzengine_heapz00,
																																			BgL_arg1516z00_1629);
																																	}
																																	BgL_arg1502z00_1625
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1509z00_1626,
																																		BgL_arg1513z00_1627);
																																}
																																BgL_arg1485z00_1623
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string1786z00zzengine_heapz00,
																																	BgL_arg1502z00_1625);
																															}
																															BgL_arg1472z00_1621
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1473z00_1622,
																																BgL_arg1485z00_1623);
																														}
																														BgL_arg1434z00_1616
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string1786z00zzengine_heapz00,
																															BgL_arg1472z00_1621);
																													}
																													BgL_arg1421z00_1614 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1422z00_1615,
																														BgL_arg1434z00_1616);
																												}
																												BgL_arg1408z00_1612 =
																													MAKE_YOUNG_PAIR
																													(BGl_string1786z00zzengine_heapz00,
																													BgL_arg1421z00_1614);
																											}
																											BgL_arg1379z00_1610 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1380z00_1611,
																												BgL_arg1408z00_1612);
																										}
																										BgL_arg1377z00_1608 =
																											MAKE_YOUNG_PAIR
																											(BGl_string1786z00zzengine_heapz00,
																											BgL_arg1379z00_1610);
																									}
																									BgL_arg1375z00_1606 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1376z00_1607,
																										BgL_arg1377z00_1608);
																								}
																								BgL_arg1371z00_1605 =
																									MAKE_YOUNG_PAIR
																									(BGl_string1786z00zzengine_heapz00,
																									BgL_arg1375z00_1606);
																							}
																							BgL_arg1367z00_1603 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1370z00_1604,
																								BgL_arg1371z00_1605);
																						}
																						BgL_arg1364z00_1602 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																							BgL_arg1367z00_1603);
																					}
																					bgl_display_obj(BgL_arg1364z00_1602,
																						BgL_port1153z00_1601);
																				}
																				bgl_display_char(((unsigned char) 10),
																					BgL_port1153z00_1601);
																			}
																		else
																			{	/* Engine/heap.scm 148 */
																				if (
																					((((BgL_typez00_bglt) COBJECT(
																									(((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													((BgL_globalz00_bglt)
																														BgL_newz00_1516))))->
																										BgL_typez00)))->
																							BgL_idz00) == CNST_TABLE_REF(10)))
																					{	/* Engine/heap.scm 163 */
																						BFALSE;
																					}
																				else
																					{	/* Engine/heap.scm 164 */
																						obj_t BgL_port1154z00_1645;

																						{	/* Engine/heap.scm 164 */
																							obj_t BgL_tmpz00_2161;

																							BgL_tmpz00_2161 =
																								BGL_CURRENT_DYNAMIC_ENV();
																							BgL_port1154z00_1645 =
																								BGL_ENV_CURRENT_OUTPUT_PORT
																								(BgL_tmpz00_2161);
																						}
																						bgl_display_string
																							(BGl_string1782z00zzengine_heapz00,
																							BgL_port1154z00_1645);
																						{	/* Engine/heap.scm 165 */
																							obj_t BgL_arg1575z00_1646;

																							{	/* Engine/heap.scm 165 */
																								obj_t BgL_arg1576z00_1647;

																								{	/* Engine/heap.scm 165 */
																									obj_t BgL_arg1584z00_1648;
																									obj_t BgL_arg1585z00_1649;

																									BgL_arg1584z00_1648 =
																										BGl_shapez00zztools_shapez00
																										(BgL_newz00_1516);
																									{	/* Engine/heap.scm 167 */
																										obj_t BgL_arg1589z00_1650;

																										{	/* Engine/heap.scm 167 */
																											obj_t BgL_arg1591z00_1651;
																											obj_t BgL_arg1593z00_1652;

																											{	/* Engine/heap.scm 167 */
																												obj_t
																													BgL_arg1594z00_1653;
																												BgL_arg1594z00_1653 =
																													MAKE_YOUNG_PAIR
																													(BgL_idz00_1518,
																													BNIL);
																												BgL_arg1591z00_1651 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(0),
																													BgL_arg1594z00_1653);
																											}
																											{	/* Engine/heap.scm 169 */
																												obj_t
																													BgL_arg1595z00_1654;
																												{	/* Engine/heap.scm 169 */
																													obj_t
																														BgL_arg1602z00_1655;
																													obj_t
																														BgL_arg1605z00_1656;
																													{	/* Engine/heap.scm 169 */
																														obj_t
																															BgL_arg1606z00_1657;
																														BgL_arg1606z00_1657
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_modulez00_1517,
																															BNIL);
																														BgL_arg1602z00_1655
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(1),
																															BgL_arg1606z00_1657);
																													}
																													{	/* Engine/heap.scm 171 */
																														obj_t
																															BgL_arg1609z00_1658;
																														{	/* Engine/heap.scm 171 */
																															obj_t
																																BgL_arg1611z00_1659;
																															obj_t
																																BgL_arg1613z00_1660;
																															{	/* Engine/heap.scm 171 */
																																obj_t
																																	BgL_arg1615z00_1661;
																																{	/* Engine/heap.scm 171 */
																																	obj_t
																																		BgL_arg1616z00_1662;
																																	{	/* Engine/heap.scm 171 */
																																		obj_t
																																			BgL_arg1625z00_1663;
																																		BgL_arg1625z00_1663
																																			=
																																			(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_newz00_1516))))->BgL_namez00);
																																		{	/* Engine/heap.scm 171 */
																																			obj_t
																																				BgL_list1626z00_1664;
																																			BgL_list1626z00_1664
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1625z00_1663,
																																				BNIL);
																																			BgL_arg1616z00_1662
																																				=
																																				BGl_formatz00zz__r4_output_6_10_3z00
																																				(BGl_string1783z00zzengine_heapz00,
																																				BgL_list1626z00_1664);
																																		}
																																	}
																																	BgL_arg1615z00_1661
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1616z00_1662,
																																		BNIL);
																																}
																																BgL_arg1611z00_1659
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(2),
																																	BgL_arg1615z00_1661);
																															}
																															{	/* Engine/heap.scm 173 */
																																obj_t
																																	BgL_arg1627z00_1665;
																																{	/* Engine/heap.scm 173 */
																																	obj_t
																																		BgL_arg1629z00_1666;
																																	obj_t
																																		BgL_arg1630z00_1667;
																																	{	/* Engine/heap.scm 173 */
																																		obj_t
																																			BgL_arg1642z00_1668;
																																		BgL_arg1642z00_1668
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_qtz00_1519,
																																			BNIL);
																																		BgL_arg1629z00_1666
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(3),
																																			BgL_arg1642z00_1668);
																																	}
																																	{	/* Engine/heap.scm 175 */
																																		obj_t
																																			BgL_arg1646z00_1669;
																																		{	/* Engine/heap.scm 175 */
																																			obj_t
																																				BgL_arg1650z00_1670;
																																			{	/* Engine/heap.scm 175 */
																																				obj_t
																																					BgL_arg1651z00_1671;
																																				BgL_arg1651z00_1671
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_jtz00_1520,
																																					BNIL);
																																				BgL_arg1650z00_1670
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(6),
																																					BgL_arg1651z00_1671);
																																			}
																																			BgL_arg1646z00_1669
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1650z00_1670,
																																				BNIL);
																																		}
																																		BgL_arg1630z00_1667
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_string1786z00zzengine_heapz00,
																																			BgL_arg1646z00_1669);
																																	}
																																	BgL_arg1627z00_1665
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1629z00_1666,
																																		BgL_arg1630z00_1667);
																																}
																																BgL_arg1613z00_1660
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string1786z00zzengine_heapz00,
																																	BgL_arg1627z00_1665);
																															}
																															BgL_arg1609z00_1658
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1611z00_1659,
																																BgL_arg1613z00_1660);
																														}
																														BgL_arg1605z00_1656
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string1786z00zzengine_heapz00,
																															BgL_arg1609z00_1658);
																													}
																													BgL_arg1595z00_1654 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1602z00_1655,
																														BgL_arg1605z00_1656);
																												}
																												BgL_arg1593z00_1652 =
																													MAKE_YOUNG_PAIR
																													(BGl_string1786z00zzengine_heapz00,
																													BgL_arg1595z00_1654);
																											}
																											BgL_arg1589z00_1650 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1591z00_1651,
																												BgL_arg1593z00_1652);
																										}
																										BgL_arg1585z00_1649 =
																											MAKE_YOUNG_PAIR
																											(BGl_string1786z00zzengine_heapz00,
																											BgL_arg1589z00_1650);
																									}
																									BgL_arg1576z00_1647 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1584z00_1648,
																										BgL_arg1585z00_1649);
																								}
																								BgL_arg1575z00_1646 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(11), BgL_arg1576z00_1647);
																							}
																							bgl_display_obj
																								(BgL_arg1575z00_1646,
																								BgL_port1154z00_1645);
																						}
																						bgl_display_char(((unsigned char)
																								10), BgL_port1154z00_1645);
									}}}}}}}}}}
									{
										obj_t BgL_l1155z00_2202;

										BgL_l1155z00_2202 = CDR(BgL_l1155z00_1674);
										BgL_l1155z00_1674 = BgL_l1155z00_2202;
										goto BgL_zc3z04anonymousza31233ze3z87_1673;
									}
								}
							else
								{	/* Engine/heap.scm 178 */
									BgL_tmpz00_1946 = ((bool_t) 1);
								}
						}
					}
				}
				return BBOOL(BgL_tmpz00_1946);
			}
		}

	}



/* dump-Tenv */
	obj_t BGl_dumpzd2Tenvzd2zzengine_heapz00(obj_t BgL_tenvz00_6)
	{
		{	/* Engine/heap.scm 183 */
			return
				BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_tenvz00_6,
				BGl_proc1787z00zzengine_heapz00);
		}

	}



/* &<@anonymous:1664> */
	obj_t BGl_z62zc3z04anonymousza31664ze3ze5zzengine_heapz00(obj_t
		BgL_envz00_1481, obj_t BgL_kz00_1482, obj_t BgL_newz00_1483)
	{
		{	/* Engine/heap.scm 185 */
			{	/* Engine/heap.scm 186 */
				obj_t BgL_idz00_1675;

				BgL_idz00_1675 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_newz00_1483)))->BgL_idz00);
				{	/* Engine/heap.scm 186 */
					obj_t BgL_namez00_1676;

					BgL_namez00_1676 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_newz00_1483)))->BgL_namez00);
					{	/* Engine/heap.scm 187 */

						{	/* Engine/heap.scm 188 */
							bool_t BgL_test1853z00_2210;

							{	/* Engine/heap.scm 188 */
								obj_t BgL_classz00_1677;

								BgL_classz00_1677 = BGl_tclassz00zzobject_classz00;
								if (BGL_OBJECTP(BgL_newz00_1483))
									{	/* Engine/heap.scm 188 */
										BgL_objectz00_bglt BgL_arg1807z00_1678;

										BgL_arg1807z00_1678 =
											(BgL_objectz00_bglt) (BgL_newz00_1483);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Engine/heap.scm 188 */
												long BgL_idxz00_1679;

												BgL_idxz00_1679 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1678);
												BgL_test1853z00_2210 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_1679 + 2L)) == BgL_classz00_1677);
											}
										else
											{	/* Engine/heap.scm 188 */
												bool_t BgL_res1764z00_1682;

												{	/* Engine/heap.scm 188 */
													obj_t BgL_oclassz00_1683;

													{	/* Engine/heap.scm 188 */
														obj_t BgL_arg1815z00_1684;
														long BgL_arg1816z00_1685;

														BgL_arg1815z00_1684 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Engine/heap.scm 188 */
															long BgL_arg1817z00_1686;

															BgL_arg1817z00_1686 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1678);
															BgL_arg1816z00_1685 =
																(BgL_arg1817z00_1686 - OBJECT_TYPE);
														}
														BgL_oclassz00_1683 =
															VECTOR_REF(BgL_arg1815z00_1684,
															BgL_arg1816z00_1685);
													}
													{	/* Engine/heap.scm 188 */
														bool_t BgL__ortest_1115z00_1687;

														BgL__ortest_1115z00_1687 =
															(BgL_classz00_1677 == BgL_oclassz00_1683);
														if (BgL__ortest_1115z00_1687)
															{	/* Engine/heap.scm 188 */
																BgL_res1764z00_1682 = BgL__ortest_1115z00_1687;
															}
														else
															{	/* Engine/heap.scm 188 */
																long BgL_odepthz00_1688;

																{	/* Engine/heap.scm 188 */
																	obj_t BgL_arg1804z00_1689;

																	BgL_arg1804z00_1689 = (BgL_oclassz00_1683);
																	BgL_odepthz00_1688 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_1689);
																}
																if ((2L < BgL_odepthz00_1688))
																	{	/* Engine/heap.scm 188 */
																		obj_t BgL_arg1802z00_1690;

																		{	/* Engine/heap.scm 188 */
																			obj_t BgL_arg1803z00_1691;

																			BgL_arg1803z00_1691 =
																				(BgL_oclassz00_1683);
																			BgL_arg1802z00_1690 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_1691, 2L);
																		}
																		BgL_res1764z00_1682 =
																			(BgL_arg1802z00_1690 ==
																			BgL_classz00_1677);
																	}
																else
																	{	/* Engine/heap.scm 188 */
																		BgL_res1764z00_1682 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1853z00_2210 = BgL_res1764z00_1682;
											}
									}
								else
									{	/* Engine/heap.scm 188 */
										BgL_test1853z00_2210 = ((bool_t) 0);
									}
							}
							if (BgL_test1853z00_2210)
								{	/* Engine/heap.scm 190 */
									obj_t BgL_port1167z00_1692;

									{	/* Engine/heap.scm 190 */
										obj_t BgL_tmpz00_2233;

										BgL_tmpz00_2233 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_port1167z00_1692 =
											BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2233);
									}
									bgl_display_string(BGl_string1782z00zzengine_heapz00,
										BgL_port1167z00_1692);
									{	/* Engine/heap.scm 191 */
										obj_t BgL_arg1675z00_1693;

										{	/* Engine/heap.scm 191 */
											obj_t BgL_arg1678z00_1694;

											{	/* Engine/heap.scm 191 */
												obj_t BgL_arg1681z00_1695;
												obj_t BgL_arg1688z00_1696;

												BgL_arg1681z00_1695 =
													BGl_shapez00zztools_shapez00(BgL_newz00_1483);
												{	/* Engine/heap.scm 192 */
													obj_t BgL_arg1689z00_1697;
													obj_t BgL_arg1691z00_1698;

													{	/* Engine/heap.scm 192 */
														obj_t BgL_arg1692z00_1699;

														{	/* Engine/heap.scm 192 */
															obj_t BgL_arg1699z00_1700;

															{	/* Engine/heap.scm 192 */
																obj_t BgL_arg1700z00_1701;

																{
																	BgL_tclassz00_bglt BgL_auxz00_2238;

																	{
																		obj_t BgL_auxz00_2239;

																		{	/* Engine/heap.scm 192 */
																			BgL_objectz00_bglt BgL_tmpz00_2240;

																			BgL_tmpz00_2240 =
																				((BgL_objectz00_bglt)
																				((BgL_typez00_bglt) BgL_newz00_1483));
																			BgL_auxz00_2239 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2240);
																		}
																		BgL_auxz00_2238 =
																			((BgL_tclassz00_bglt) BgL_auxz00_2239);
																	}
																	BgL_arg1700z00_1701 =
																		(((BgL_tclassz00_bglt)
																			COBJECT(BgL_auxz00_2238))->
																		BgL_itszd2superzd2);
																}
																BgL_arg1699z00_1700 =
																	BGl_shapez00zztools_shapez00
																	(BgL_arg1700z00_1701);
															}
															BgL_arg1692z00_1699 =
																MAKE_YOUNG_PAIR(BgL_arg1699z00_1700, BNIL);
														}
														BgL_arg1689z00_1697 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
															BgL_arg1692z00_1699);
													}
													{	/* Engine/heap.scm 193 */
														obj_t BgL_arg1701z00_1702;

														{	/* Engine/heap.scm 193 */
															obj_t BgL_l1160z00_1703;

															{
																BgL_tclassz00_bglt BgL_auxz00_2250;

																{
																	obj_t BgL_auxz00_2251;

																	{	/* Engine/heap.scm 193 */
																		BgL_objectz00_bglt BgL_tmpz00_2252;

																		BgL_tmpz00_2252 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_newz00_1483));
																		BgL_auxz00_2251 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2252);
																	}
																	BgL_auxz00_2250 =
																		((BgL_tclassz00_bglt) BgL_auxz00_2251);
																}
																BgL_l1160z00_1703 =
																	(((BgL_tclassz00_bglt)
																		COBJECT(BgL_auxz00_2250))->BgL_slotsz00);
															}
															if (NULLP(BgL_l1160z00_1703))
																{	/* Engine/heap.scm 193 */
																	BgL_arg1701z00_1702 = BNIL;
																}
															else
																{	/* Engine/heap.scm 193 */
																	obj_t BgL_head1162z00_1704;

																	{	/* Engine/heap.scm 193 */
																		obj_t BgL_arg1710z00_1705;

																		{	/* Engine/heap.scm 193 */
																			obj_t BgL_arg1711z00_1706;

																			BgL_arg1711z00_1706 =
																				CAR(((obj_t) BgL_l1160z00_1703));
																			BgL_arg1710z00_1705 =
																				BGl_shapez00zztools_shapez00
																				(BgL_arg1711z00_1706);
																		}
																		BgL_head1162z00_1704 =
																			MAKE_YOUNG_PAIR(BgL_arg1710z00_1705,
																			BNIL);
																	}
																	{	/* Engine/heap.scm 193 */
																		obj_t BgL_g1166z00_1707;

																		BgL_g1166z00_1707 =
																			CDR(((obj_t) BgL_l1160z00_1703));
																		{
																			obj_t BgL_l1160z00_1709;
																			obj_t BgL_tail1163z00_1710;

																			BgL_l1160z00_1709 = BgL_g1166z00_1707;
																			BgL_tail1163z00_1710 =
																				BgL_head1162z00_1704;
																		BgL_zc3z04anonymousza31703ze3z87_1708:
																			if (NULLP(BgL_l1160z00_1709))
																				{	/* Engine/heap.scm 193 */
																					BgL_arg1701z00_1702 =
																						BgL_head1162z00_1704;
																				}
																			else
																				{	/* Engine/heap.scm 193 */
																					obj_t BgL_newtail1164z00_1711;

																					{	/* Engine/heap.scm 193 */
																						obj_t BgL_arg1708z00_1712;

																						{	/* Engine/heap.scm 193 */
																							obj_t BgL_arg1709z00_1713;

																							BgL_arg1709z00_1713 =
																								CAR(
																								((obj_t) BgL_l1160z00_1709));
																							BgL_arg1708z00_1712 =
																								BGl_shapez00zztools_shapez00
																								(BgL_arg1709z00_1713);
																						}
																						BgL_newtail1164z00_1711 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1708z00_1712, BNIL);
																					}
																					SET_CDR(BgL_tail1163z00_1710,
																						BgL_newtail1164z00_1711);
																					{	/* Engine/heap.scm 193 */
																						obj_t BgL_arg1705z00_1714;

																						BgL_arg1705z00_1714 =
																							CDR(((obj_t) BgL_l1160z00_1709));
																						{
																							obj_t BgL_tail1163z00_2276;
																							obj_t BgL_l1160z00_2275;

																							BgL_l1160z00_2275 =
																								BgL_arg1705z00_1714;
																							BgL_tail1163z00_2276 =
																								BgL_newtail1164z00_1711;
																							BgL_tail1163z00_1710 =
																								BgL_tail1163z00_2276;
																							BgL_l1160z00_1709 =
																								BgL_l1160z00_2275;
																							goto
																								BgL_zc3z04anonymousza31703ze3z87_1708;
																						}
																					}
																				}
																		}
																	}
																}
														}
														BgL_arg1691z00_1698 =
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_arg1701z00_1702, BNIL);
													}
													BgL_arg1688z00_1696 =
														MAKE_YOUNG_PAIR(BgL_arg1689z00_1697,
														BgL_arg1691z00_1698);
												}
												BgL_arg1678z00_1694 =
													MAKE_YOUNG_PAIR(BgL_arg1681z00_1695,
													BgL_arg1688z00_1696);
											}
											BgL_arg1675z00_1693 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
												BgL_arg1678z00_1694);
										}
										bgl_display_obj(BgL_arg1675z00_1693, BgL_port1167z00_1692);
									}
									return
										bgl_display_char(((unsigned char) 10),
										BgL_port1167z00_1692);
								}
							else
								{	/* Engine/heap.scm 194 */
									obj_t BgL_port1168z00_1715;

									{	/* Engine/heap.scm 194 */
										obj_t BgL_tmpz00_2284;

										BgL_tmpz00_2284 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_port1168z00_1715 =
											BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2284);
									}
									bgl_display_string(BGl_string1782z00zzengine_heapz00,
										BgL_port1168z00_1715);
									{	/* Engine/heap.scm 194 */
										obj_t BgL_arg1714z00_1716;

										{	/* Engine/heap.scm 194 */
											obj_t BgL_arg1717z00_1717;

											{	/* Engine/heap.scm 194 */
												obj_t BgL_arg1718z00_1718;

												{	/* Engine/heap.scm 194 */
													obj_t BgL_arg1720z00_1719;

													{	/* Engine/heap.scm 194 */
														obj_t BgL_arg1722z00_1720;

														BgL_arg1722z00_1720 =
															MAKE_YOUNG_PAIR(BgL_namez00_1676, BNIL);
														BgL_arg1720z00_1719 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
															BgL_arg1722z00_1720);
													}
													BgL_arg1718z00_1718 =
														MAKE_YOUNG_PAIR(BgL_arg1720z00_1719, BNIL);
												}
												BgL_arg1717z00_1717 =
													MAKE_YOUNG_PAIR(BgL_idz00_1675, BgL_arg1718z00_1718);
											}
											BgL_arg1714z00_1716 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
												BgL_arg1717z00_1717);
										}
										bgl_display_obj(BgL_arg1714z00_1716, BgL_port1168z00_1715);
									}
									return
										bgl_display_char(((unsigned char) 10),
										BgL_port1168z00_1715);
		}}}}}}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzengine_heapz00(void)
	{
		{	/* Engine/heap.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzengine_heapz00(void)
	{
		{	/* Engine/heap.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzengine_heapz00(void)
	{
		{	/* Engine/heap.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzengine_heapz00(void)
	{
		{	/* Engine/heap.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zzheap_restorez00(147989063L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
			return
				BGl_modulezd2initializa7ationz75zzmodule_pragmaz00(433305536L,
				BSTRING_TO_STRING(BGl_string1788z00zzengine_heapz00));
		}

	}

#ifdef __cplusplus
}
#endif
