/*===========================================================================*/
/*   (Engine/link.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Engine/link.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_ENGINE_LINK_TYPE_DEFINITIONS
#define BGL_ENGINE_LINK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_ENGINE_LINK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_IMPORT int BGl_bigloozd2warningzd2zz__paramz00(void);
	static obj_t BGl_z62findzd2filezd2forzd2linkzb0zzengine_linkz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_findzd2librarieszd2zzengine_linkz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzengine_linkz00 = BUNSPEC;
	extern obj_t BGl_comptimezd2expandzf2errorz20zzexpand_epsz00(obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_za2targetzd2languageza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	extern obj_t BGl_setzd2backendz12zc0zzbackend_backendz00(obj_t);
	extern obj_t BGl_installzd2initialzd2expanderz00zzexpand_installz00(void);
	BGL_EXPORTED_DECL obj_t BGl_unprofzd2srczd2namez00zzengine_linkz00(obj_t);
	static obj_t BGl_z62unprofzd2srczd2namez62zzengine_linkz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzengine_linkz00(void);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzengine_linkz00(void);
	BGL_IMPORT obj_t bgl_register_eval_srfi(obj_t);
	extern obj_t
		BGl_backendzd2linkzd2objectsz00zzbackend_backendz00(BgL_backendz00_bglt,
		obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	static obj_t BGl_z62findzd2mainzb0zzengine_linkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_basenamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2tmpzd2mainz00zzengine_linkz00(obj_t,
		bool_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzengine_linkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzengine_linkz00(void);
	static obj_t BGl_z62findzd2srczd2filez62zzengine_linkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62linkz62zzengine_linkz00(obj_t);
	extern obj_t BGl_readzd2directiveszd2zzread_includez00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_linkz00zzengine_linkz00(void);
	BGL_EXPORTED_DECL obj_t BGl_findzd2mainzd2zzengine_linkz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_findzd2filezd2forzd2linkzd2zzengine_linkz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31274ze3ze5zzengine_linkz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzengine_linkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_includez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_installz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_setrcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcc_ldz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_za2bigloozd2nameza2zd2zzengine_paramz00;
	BGL_IMPORT bool_t fexists(char *);
	extern obj_t BGl_za2profilezd2modeza2zd2zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zzengine_linkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzengine_linkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzengine_linkz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzengine_linkz00(void);
	extern obj_t BGl_za2srczd2suffixza2zd2zzengine_paramz00;
	extern obj_t BGl_za2ozd2filesza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_findzd2srczd2filez00zzengine_linkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2loadzd2pathza2zd2zz__evalz00;
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	static obj_t BGl_z62findzd2librarieszb0zzengine_linkz00(obj_t, obj_t);
	static obj_t BGl_loopze70ze7zzengine_linkz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2tmpzd2mainz62zzengine_linkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza31308ze3ze70z60zzengine_linkz00(obj_t);
	static obj_t __cnst[8];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2tmpzd2mainzd2envzd2zzengine_linkz00,
		BgL_bgl_za762makeza7d2tmpza7d21473za7,
		BGl_z62makezd2tmpzd2mainz62zzengine_linkz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2filezd2forzd2linkzd2envz00zzengine_linkz00,
		BgL_bgl_za762findza7d2fileza7d1474za7,
		BGl_z62findzd2filezd2forzd2linkzb0zzengine_linkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2srczd2filezd2envzd2zzengine_linkz00,
		BgL_bgl_za762findza7d2srcza7d21475za7,
		BGl_z62findzd2srczd2filez62zzengine_linkz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_linkzd2envzd2zzengine_linkz00,
		BgL_bgl_za762linkza762za7za7engi1476z00, BGl_z62linkz62zzengine_linkz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1451z00zzengine_linkz00,
		BgL_bgl_string1451za700za7za7e1477za7, "No Bigloo module found for -- ",
		30);
	      DEFINE_STRING(BGl_string1452z00zzengine_linkz00,
		BgL_bgl_string1452za700za7za7e1478za7, "link", 4);
	      DEFINE_STRING(BGl_string1453z00zzengine_linkz00,
		BgL_bgl_string1453za700za7za7e1479za7, "'. Using file -- ", 17);
	      DEFINE_STRING(BGl_string1454z00zzengine_linkz00,
		BgL_bgl_string1454za700za7za7e1480za7,
		"Several source files found for object `", 39);
	      DEFINE_STRING(BGl_string1455z00zzengine_linkz00,
		BgL_bgl_string1455za700za7za7e1481za7, ".", 1);
	      DEFINE_STRING(BGl_string1457z00zzengine_linkz00,
		BgL_bgl_string1457za700za7za7e1482za7, ";; ", 3);
	      DEFINE_STRING(BGl_string1458z00zzengine_linkz00,
		BgL_bgl_string1458za700za7za7e1483za7,
		";; !!! generated file, don't edit !!!", 37);
	      DEFINE_STRING(BGl_string1459z00zzengine_linkz00,
		BgL_bgl_string1459za700za7za7e1484za7,
		";; ==================================", 37);
	      DEFINE_STRING(BGl_string1460z00zzengine_linkz00,
		BgL_bgl_string1460za700za7za7e1485za7, "(main *the-command-line*)", 25);
	      DEFINE_STRING(BGl_string1461z00zzengine_linkz00,
		BgL_bgl_string1461za700za7za7e1486za7, "", 0);
	      DEFINE_STRING(BGl_string1462z00zzengine_linkz00,
		BgL_bgl_string1462za700za7za7e1487za7, "Can't open output file", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1456z00zzengine_linkz00,
		BgL_bgl_za762za7c3za704anonymo1488za7,
		BGl_z62zc3z04anonymousza31274ze3ze5zzengine_linkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1463z00zzengine_linkz00,
		BgL_bgl_string1463za700za7za7e1489za7, "engine_link", 11);
	      DEFINE_STRING(BGl_string1464z00zzengine_linkz00,
		BgL_bgl_string1464za700za7za7e1490za7,
		"module import main cond-expand include eval library bigloo-compile ", 67);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2librarieszd2envz00zzengine_linkz00,
		BgL_bgl_za762findza7d2librar1491z00,
		BGl_z62findzd2librarieszb0zzengine_linkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unprofzd2srczd2namezd2envzd2zzengine_linkz00,
		BgL_bgl_za762unprofza7d2srcza71492za7,
		BGl_z62unprofzd2srczd2namez62zzengine_linkz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2mainzd2envz00zzengine_linkz00,
		BgL_bgl_za762findza7d2mainza7b1493za7,
		BGl_z62findzd2mainzb0zzengine_linkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzengine_linkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzengine_linkz00(long
		BgL_checksumz00_548, char *BgL_fromz00_549)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzengine_linkz00))
				{
					BGl_requirezd2initializa7ationz75zzengine_linkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzengine_linkz00();
					BGl_libraryzd2moduleszd2initz00zzengine_linkz00();
					BGl_cnstzd2initzd2zzengine_linkz00();
					BGl_importedzd2moduleszd2initz00zzengine_linkz00();
					return BGl_methodzd2initzd2zzengine_linkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzengine_linkz00(void)
	{
		{	/* Engine/link.scm 20 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"engine_link");
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "engine_link");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"engine_link");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "engine_link");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"engine_link");
			BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(0L, "engine_link");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "engine_link");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "engine_link");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "engine_link");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "engine_link");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"engine_link");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"engine_link");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "engine_link");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzengine_linkz00(void)
	{
		{	/* Engine/link.scm 20 */
			{	/* Engine/link.scm 20 */
				obj_t BgL_cportz00_536;

				{	/* Engine/link.scm 20 */
					obj_t BgL_stringz00_543;

					BgL_stringz00_543 = BGl_string1464z00zzengine_linkz00;
					{	/* Engine/link.scm 20 */
						obj_t BgL_startz00_544;

						BgL_startz00_544 = BINT(0L);
						{	/* Engine/link.scm 20 */
							obj_t BgL_endz00_545;

							BgL_endz00_545 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_543)));
							{	/* Engine/link.scm 20 */

								BgL_cportz00_536 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_543, BgL_startz00_544, BgL_endz00_545);
				}}}}
				{
					long BgL_iz00_537;

					BgL_iz00_537 = 7L;
				BgL_loopz00_538:
					if ((BgL_iz00_537 == -1L))
						{	/* Engine/link.scm 20 */
							return BUNSPEC;
						}
					else
						{	/* Engine/link.scm 20 */
							{	/* Engine/link.scm 20 */
								obj_t BgL_arg1472z00_539;

								{	/* Engine/link.scm 20 */

									{	/* Engine/link.scm 20 */
										obj_t BgL_locationz00_541;

										BgL_locationz00_541 = BBOOL(((bool_t) 0));
										{	/* Engine/link.scm 20 */

											BgL_arg1472z00_539 =
												BGl_readz00zz__readerz00(BgL_cportz00_536,
												BgL_locationz00_541);
										}
									}
								}
								{	/* Engine/link.scm 20 */
									int BgL_tmpz00_580;

									BgL_tmpz00_580 = (int) (BgL_iz00_537);
									CNST_TABLE_SET(BgL_tmpz00_580, BgL_arg1472z00_539);
							}}
							{	/* Engine/link.scm 20 */
								int BgL_auxz00_542;

								BgL_auxz00_542 = (int) ((BgL_iz00_537 - 1L));
								{
									long BgL_iz00_585;

									BgL_iz00_585 = (long) (BgL_auxz00_542);
									BgL_iz00_537 = BgL_iz00_585;
									goto BgL_loopz00_538;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzengine_linkz00(void)
	{
		{	/* Engine/link.scm 20 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzengine_linkz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_120;

				BgL_headz00_120 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_121;
					obj_t BgL_tailz00_122;

					BgL_prevz00_121 = BgL_headz00_120;
					BgL_tailz00_122 = BgL_l1z00_1;
				BgL_loopz00_123:
					if (PAIRP(BgL_tailz00_122))
						{
							obj_t BgL_newzd2prevzd2_125;

							BgL_newzd2prevzd2_125 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_122), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_121, BgL_newzd2prevzd2_125);
							{
								obj_t BgL_tailz00_595;
								obj_t BgL_prevz00_594;

								BgL_prevz00_594 = BgL_newzd2prevzd2_125;
								BgL_tailz00_595 = CDR(BgL_tailz00_122);
								BgL_tailz00_122 = BgL_tailz00_595;
								BgL_prevz00_121 = BgL_prevz00_594;
								goto BgL_loopz00_123;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_120);
				}
			}
		}

	}



/* link */
	BGL_EXPORTED_DEF obj_t BGl_linkz00zzengine_linkz00(void)
	{
		{	/* Engine/link.scm 43 */
			bgl_register_eval_srfi(CNST_TABLE_REF(0));
			BGl_installzd2initialzd2expanderz00zzexpand_installz00();
			BGl_setzd2backendz12zc0zzbackend_backendz00
				(BGl_za2targetzd2languageza2zd2zzengine_paramz00);
			{
				obj_t BgL_objectsz00_130;
				obj_t BgL_sourcesz00_131;

				BgL_objectsz00_130 = BGl_za2ozd2filesza2zd2zzengine_paramz00;
				BgL_sourcesz00_131 = BNIL;
			BgL_zc3z04anonymousza31069ze3z87_132:
				if (NULLP(BgL_objectsz00_130))
					{	/* Engine/link.scm 55 */
						obj_t BgL_arg1074z00_134;

						BgL_arg1074z00_134 = BGl_thezd2backendzd2zzbackend_backendz00();
						return
							BGl_backendzd2linkzd2objectsz00zzbackend_backendz00(
							((BgL_backendz00_bglt) BgL_arg1074z00_134), BgL_sourcesz00_131);
					}
				else
					{	/* Engine/link.scm 56 */
						obj_t BgL_objectz00_135;

						BgL_objectz00_135 = CAR(((obj_t) BgL_objectsz00_130));
						{	/* Engine/link.scm 56 */
							obj_t BgL_prefz00_136;

							BgL_prefz00_136 =
								BGl_unprofzd2srczd2namez00zzengine_linkz00
								(BGl_prefixz00zz__osz00(BgL_objectz00_135));
							{	/* Engine/link.scm 57 */
								obj_t BgL_bprefz00_137;

								BgL_bprefz00_137 = BGl_basenamez00zz__osz00(BgL_prefz00_136);
								{	/* Engine/link.scm 58 */
									obj_t BgL_scmzd2filezd2_138;

									BgL_scmzd2filezd2_138 =
										BGl_findzd2srczd2filez00zzengine_linkz00(BgL_prefz00_136,
										BgL_bprefz00_137);
									{	/* Engine/link.scm 59 */

										if (STRINGP(BgL_scmzd2filezd2_138))
											{	/* Engine/link.scm 61 */
												obj_t BgL_arg1076z00_140;
												obj_t BgL_arg1078z00_141;

												BgL_arg1076z00_140 = CDR(((obj_t) BgL_objectsz00_130));
												{	/* Engine/link.scm 61 */
													obj_t BgL_arg1079z00_142;

													BgL_arg1079z00_142 =
														MAKE_YOUNG_PAIR(BgL_scmzd2filezd2_138,
														BgL_objectz00_135);
													BgL_arg1078z00_141 =
														MAKE_YOUNG_PAIR(BgL_arg1079z00_142,
														BgL_sourcesz00_131);
												}
												{
													obj_t BgL_sourcesz00_620;
													obj_t BgL_objectsz00_619;

													BgL_objectsz00_619 = BgL_arg1076z00_140;
													BgL_sourcesz00_620 = BgL_arg1078z00_141;
													BgL_sourcesz00_131 = BgL_sourcesz00_620;
													BgL_objectsz00_130 = BgL_objectsz00_619;
													goto BgL_zc3z04anonymousza31069ze3z87_132;
												}
											}
										else
											{	/* Engine/link.scm 60 */
												{	/* Engine/link.scm 63 */
													bool_t BgL_test1499z00_621;

													{	/* Engine/link.scm 63 */
														int BgL_arg1087z00_149;

														BgL_arg1087z00_149 =
															BGl_bigloozd2warningzd2zz__paramz00();
														BgL_test1499z00_621 =
															((long) (BgL_arg1087z00_149) >= 2L);
													}
													if (BgL_test1499z00_621)
														{	/* Engine/link.scm 66 */
															obj_t BgL_arg1082z00_145;

															BgL_arg1082z00_145 =
																CAR(((obj_t) BgL_objectsz00_130));
															{	/* Engine/link.scm 64 */
																obj_t BgL_list1083z00_146;

																{	/* Engine/link.scm 64 */
																	obj_t BgL_arg1084z00_147;

																	{	/* Engine/link.scm 64 */
																		obj_t BgL_arg1085z00_148;

																		BgL_arg1085z00_148 =
																			MAKE_YOUNG_PAIR(BgL_arg1082z00_145, BNIL);
																		BgL_arg1084z00_147 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1451z00zzengine_linkz00,
																			BgL_arg1085z00_148);
																	}
																	BgL_list1083z00_146 =
																		MAKE_YOUNG_PAIR
																		(BGl_string1452z00zzengine_linkz00,
																		BgL_arg1084z00_147);
																}
																BGl_warningz00zz__errorz00(BgL_list1083z00_146);
															}
														}
													else
														{	/* Engine/link.scm 63 */
															BFALSE;
														}
												}
												{	/* Engine/link.scm 67 */
													obj_t BgL_arg1088z00_150;

													BgL_arg1088z00_150 =
														CDR(((obj_t) BgL_objectsz00_130));
													{
														obj_t BgL_objectsz00_633;

														BgL_objectsz00_633 = BgL_arg1088z00_150;
														BgL_objectsz00_130 = BgL_objectsz00_633;
														goto BgL_zc3z04anonymousza31069ze3z87_132;
													}
												}
											}
									}
								}
							}
						}
					}
			}
		}

	}



/* &link */
	obj_t BGl_z62linkz62zzengine_linkz00(obj_t BgL_envz00_515)
	{
		{	/* Engine/link.scm 43 */
			return BGl_linkz00zzengine_linkz00();
		}

	}



/* unprof-src-name */
	BGL_EXPORTED_DEF obj_t BGl_unprofzd2srczd2namez00zzengine_linkz00(obj_t
		BgL_namez00_3)
	{
		{	/* Engine/link.scm 72 */
			if (CBOOL(BGl_za2profilezd2modeza2zd2zzengine_paramz00))
				{	/* Engine/link.scm 75 */
					long BgL_lenz00_153;

					BgL_lenz00_153 = STRING_LENGTH(((obj_t) BgL_namez00_3));
					{	/* Engine/link.scm 76 */
						bool_t BgL_test1501z00_639;

						if ((BgL_lenz00_153 > 2L))
							{	/* Engine/link.scm 76 */
								if (
									(STRING_REF(
											((obj_t) BgL_namez00_3),
											(BgL_lenz00_153 - 1L)) == ((unsigned char) 'p')))
									{	/* Engine/link.scm 77 */
										BgL_test1501z00_639 =
											(STRING_REF(
												((obj_t) BgL_namez00_3),
												(BgL_lenz00_153 - 2L)) == ((unsigned char) '_'));
									}
								else
									{	/* Engine/link.scm 77 */
										BgL_test1501z00_639 = ((bool_t) 0);
									}
							}
						else
							{	/* Engine/link.scm 76 */
								BgL_test1501z00_639 = ((bool_t) 0);
							}
						if (BgL_test1501z00_639)
							{	/* Engine/link.scm 79 */
								long BgL_arg1122z00_163;

								BgL_arg1122z00_163 = (BgL_lenz00_153 - 2L);
								return
									c_substring(((obj_t) BgL_namez00_3), 0L, BgL_arg1122z00_163);
							}
						else
							{	/* Engine/link.scm 76 */
								return BgL_namez00_3;
							}
					}
				}
			else
				{	/* Engine/link.scm 73 */
					return BgL_namez00_3;
				}
		}

	}



/* &unprof-src-name */
	obj_t BGl_z62unprofzd2srczd2namez62zzengine_linkz00(obj_t BgL_envz00_516,
		obj_t BgL_namez00_517)
	{
		{	/* Engine/link.scm 72 */
			return BGl_unprofzd2srczd2namez00zzengine_linkz00(BgL_namez00_517);
		}

	}



/* find-file-for-link */
	BGL_EXPORTED_DEF obj_t BGl_findzd2filezd2forzd2linkzd2zzengine_linkz00(obj_t
		BgL_filez00_4)
	{
		{	/* Engine/link.scm 85 */
			if (fexists(BSTRING_TO_STRING(BgL_filez00_4)))
				{	/* Engine/link.scm 86 */
					return BgL_filez00_4;
				}
			else
				{	/* Engine/link.scm 86 */
					return
						BGl_findzd2filezf2pathz20zz__osz00(BgL_filez00_4,
						BGl_za2loadzd2pathza2zd2zz__evalz00);
				}
		}

	}



/* &find-file-for-link */
	obj_t BGl_z62findzd2filezd2forzd2linkzb0zzengine_linkz00(obj_t BgL_envz00_518,
		obj_t BgL_filez00_519)
	{
		{	/* Engine/link.scm 85 */
			return BGl_findzd2filezd2forzd2linkzd2zzengine_linkz00(BgL_filez00_519);
		}

	}



/* find-src-file */
	BGL_EXPORTED_DEF obj_t BGl_findzd2srczd2filez00zzengine_linkz00(obj_t
		BgL_prefixz00_5, obj_t BgL_bnamez00_6)
	{
		{	/* Engine/link.scm 93 */
			{
				obj_t BgL_suffixz00_173;
				obj_t BgL_filesz00_174;

				BgL_suffixz00_173 = BGl_za2srczd2suffixza2zd2zzengine_paramz00;
				BgL_filesz00_174 = BNIL;
			BgL_zc3z04anonymousza31129ze3z87_175:
				if (NULLP(BgL_suffixz00_173))
					{	/* Engine/link.scm 96 */
						if (NULLP(BgL_filesz00_174))
							{	/* Engine/link.scm 98 */
								return BFALSE;
							}
						else
							{	/* Engine/link.scm 98 */
								if (NULLP(CDR(BgL_filesz00_174)))
									{	/* Engine/link.scm 100 */
										return CAR(BgL_filesz00_174);
									}
								else
									{	/* Engine/link.scm 100 */
										{	/* Engine/link.scm 104 */
											obj_t BgL_arg1137z00_180;

											BgL_arg1137z00_180 = CAR(BgL_filesz00_174);
											{	/* Engine/link.scm 103 */
												obj_t BgL_list1138z00_181;

												{	/* Engine/link.scm 103 */
													obj_t BgL_arg1140z00_182;

													{	/* Engine/link.scm 103 */
														obj_t BgL_arg1141z00_183;

														{	/* Engine/link.scm 103 */
															obj_t BgL_arg1142z00_184;

															{	/* Engine/link.scm 103 */
																obj_t BgL_arg1143z00_185;

																BgL_arg1143z00_185 =
																	MAKE_YOUNG_PAIR(BgL_arg1137z00_180, BNIL);
																BgL_arg1142z00_184 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1453z00zzengine_linkz00,
																	BgL_arg1143z00_185);
															}
															BgL_arg1141z00_183 =
																MAKE_YOUNG_PAIR(BgL_bnamez00_6,
																BgL_arg1142z00_184);
														}
														BgL_arg1140z00_182 =
															MAKE_YOUNG_PAIR(BGl_string1454z00zzengine_linkz00,
															BgL_arg1141z00_183);
													}
													BgL_list1138z00_181 =
														MAKE_YOUNG_PAIR(BGl_string1452z00zzengine_linkz00,
														BgL_arg1140z00_182);
												}
												BGl_warningz00zz__errorz00(BgL_list1138z00_181);
											}
										}
										return CAR(BgL_filesz00_174);
									}
							}
					}
				else
					{	/* Engine/link.scm 106 */
						obj_t BgL_sufz00_187;

						BgL_sufz00_187 = CAR(((obj_t) BgL_suffixz00_173));
						{	/* Engine/link.scm 106 */
							obj_t BgL_fz00_188;

							BgL_fz00_188 =
								BGl_findzd2filezd2forzd2linkzd2zzengine_linkz00(string_append_3
								(BgL_prefixz00_5, BGl_string1455z00zzengine_linkz00,
									BgL_sufz00_187));
							{	/* Engine/link.scm 107 */

								if (STRINGP(BgL_fz00_188))
									{	/* Engine/link.scm 109 */
										obj_t BgL_arg1148z00_190;
										obj_t BgL_arg1149z00_191;

										BgL_arg1148z00_190 = CDR(((obj_t) BgL_suffixz00_173));
										BgL_arg1149z00_191 =
											MAKE_YOUNG_PAIR(BgL_fz00_188, BgL_filesz00_174);
										{
											obj_t BgL_filesz00_686;
											obj_t BgL_suffixz00_685;

											BgL_suffixz00_685 = BgL_arg1148z00_190;
											BgL_filesz00_686 = BgL_arg1149z00_191;
											BgL_filesz00_174 = BgL_filesz00_686;
											BgL_suffixz00_173 = BgL_suffixz00_685;
											goto BgL_zc3z04anonymousza31129ze3z87_175;
										}
									}
								else
									{	/* Engine/link.scm 110 */
										obj_t BgL_fz00_192;

										BgL_fz00_192 =
											BGl_findzd2filezd2forzd2linkzd2zzengine_linkz00
											(string_append_3(BgL_bnamez00_6,
												BGl_string1455z00zzengine_linkz00, BgL_sufz00_187));
										if (STRINGP(BgL_fz00_192))
											{	/* Engine/link.scm 112 */
												obj_t BgL_arg1152z00_194;
												obj_t BgL_arg1153z00_195;

												BgL_arg1152z00_194 = CDR(((obj_t) BgL_suffixz00_173));
												BgL_arg1153z00_195 =
													MAKE_YOUNG_PAIR(BgL_fz00_192, BgL_filesz00_174);
												{
													obj_t BgL_filesz00_695;
													obj_t BgL_suffixz00_694;

													BgL_suffixz00_694 = BgL_arg1152z00_194;
													BgL_filesz00_695 = BgL_arg1153z00_195;
													BgL_filesz00_174 = BgL_filesz00_695;
													BgL_suffixz00_173 = BgL_suffixz00_694;
													goto BgL_zc3z04anonymousza31129ze3z87_175;
												}
											}
										else
											{	/* Engine/link.scm 113 */
												obj_t BgL_arg1154z00_196;

												BgL_arg1154z00_196 = CDR(((obj_t) BgL_suffixz00_173));
												{
													obj_t BgL_suffixz00_698;

													BgL_suffixz00_698 = BgL_arg1154z00_196;
													BgL_suffixz00_173 = BgL_suffixz00_698;
													goto BgL_zc3z04anonymousza31129ze3z87_175;
												}
											}
									}
							}
						}
					}
			}
		}

	}



/* &find-src-file */
	obj_t BGl_z62findzd2srczd2filez62zzengine_linkz00(obj_t BgL_envz00_520,
		obj_t BgL_prefixz00_521, obj_t BgL_bnamez00_522)
	{
		{	/* Engine/link.scm 93 */
			return
				BGl_findzd2srczd2filez00zzengine_linkz00(BgL_prefixz00_521,
				BgL_bnamez00_522);
		}

	}



/* find-libraries */
	BGL_EXPORTED_DEF obj_t BGl_findzd2librarieszd2zzengine_linkz00(obj_t
		BgL_clausesz00_7)
	{
		{	/* Engine/link.scm 118 */
			return BGl_loopze70ze7zzengine_linkz00(BgL_clausesz00_7, BNIL);
		}

	}



/* <@anonymous:1308>~0 */
	obj_t BGl_zc3z04anonymousza31308ze3ze70z60zzengine_linkz00(obj_t
		BgL_l1057z00_306)
	{
		{	/* Engine/link.scm 134 */
			if (NULLP(BgL_l1057z00_306))
				{	/* Engine/link.scm 134 */
					return BNIL;
				}
			else
				{	/* Engine/link.scm 135 */
					obj_t BgL_arg1310z00_309;
					obj_t BgL_arg1311z00_310;

					{	/* Engine/link.scm 135 */
						obj_t BgL_includez00_311;

						BgL_includez00_311 = CAR(((obj_t) BgL_l1057z00_306));
						BgL_arg1310z00_309 =
							BGl_readzd2directiveszd2zzread_includez00(BgL_includez00_311);
					}
					{	/* Engine/link.scm 134 */
						obj_t BgL_arg1312z00_312;

						BgL_arg1312z00_312 = CDR(((obj_t) BgL_l1057z00_306));
						BgL_arg1311z00_310 =
							BGl_zc3z04anonymousza31308ze3ze70z60zzengine_linkz00
							(BgL_arg1312z00_312);
					}
					return bgl_append2(BgL_arg1310z00_309, BgL_arg1311z00_310);
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzengine_linkz00(obj_t BgL_clausesz00_202,
		obj_t BgL_librariesz00_203)
	{
		{	/* Engine/link.scm 119 */
		BGl_loopze70ze7zzengine_linkz00:
			{
				obj_t BgL_evclausesz00_210;
				obj_t BgL_restz00_211;
				obj_t BgL_includesz00_213;
				obj_t BgL_restz00_214;
				obj_t BgL_restz00_216;

				if (NULLP(BgL_clausesz00_202))
					{	/* Engine/link.scm 119 */
						return bgl_reverse_bang(BgL_librariesz00_203);
					}
				else
					{	/* Engine/link.scm 119 */
						if (PAIRP(BgL_clausesz00_202))
							{	/* Engine/link.scm 119 */
								obj_t BgL_carzd2121zd2_222;

								BgL_carzd2121zd2_222 = CAR(((obj_t) BgL_clausesz00_202));
								if (PAIRP(BgL_carzd2121zd2_222))
									{	/* Engine/link.scm 119 */
										if ((CAR(BgL_carzd2121zd2_222) == CNST_TABLE_REF(1)))
											{	/* Engine/link.scm 119 */
												obj_t BgL_arg1171z00_227;

												BgL_arg1171z00_227 = CDR(((obj_t) BgL_clausesz00_202));
												{	/* Engine/link.scm 125 */
													obj_t BgL_arg1252z00_446;

													BgL_arg1252z00_446 =
														MAKE_YOUNG_PAIR(BgL_carzd2121zd2_222,
														BgL_librariesz00_203);
													{
														obj_t BgL_librariesz00_727;
														obj_t BgL_clausesz00_726;

														BgL_clausesz00_726 = BgL_arg1171z00_227;
														BgL_librariesz00_727 = BgL_arg1252z00_446;
														BgL_librariesz00_203 = BgL_librariesz00_727;
														BgL_clausesz00_202 = BgL_clausesz00_726;
														goto BGl_loopze70ze7zzengine_linkz00;
													}
												}
											}
										else
											{	/* Engine/link.scm 119 */
												if (
													(CAR(
															((obj_t) BgL_carzd2121zd2_222)) ==
														CNST_TABLE_REF(2)))
													{	/* Engine/link.scm 119 */
														obj_t BgL_arg1182z00_231;
														obj_t BgL_arg1183z00_232;

														BgL_arg1182z00_231 =
															CDR(((obj_t) BgL_carzd2121zd2_222));
														BgL_arg1183z00_232 =
															CDR(((obj_t) BgL_clausesz00_202));
														BgL_evclausesz00_210 = BgL_arg1182z00_231;
														BgL_restz00_211 = BgL_arg1183z00_232;
														{	/* Engine/link.scm 127 */
															obj_t BgL_evlibsz00_286;

															{	/* Engine/link.scm 127 */
																obj_t BgL_list1273z00_289;

																BgL_list1273z00_289 =
																	MAKE_YOUNG_PAIR(BgL_evclausesz00_210, BNIL);
																BgL_evlibsz00_286 =
																	BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																	(BGl_proc1456z00zzengine_linkz00,
																	BgL_list1273z00_289);
															}
															{
																obj_t BgL_librariesz00_740;
																obj_t BgL_clausesz00_739;

																BgL_clausesz00_739 = BgL_restz00_211;
																BgL_librariesz00_740 =
																	BGl_appendzd221011zd2zzengine_linkz00
																	(BgL_evlibsz00_286, BgL_librariesz00_203);
																BgL_librariesz00_203 = BgL_librariesz00_740;
																BgL_clausesz00_202 = BgL_clausesz00_739;
																goto BGl_loopze70ze7zzengine_linkz00;
															}
														}
													}
												else
													{	/* Engine/link.scm 119 */
														obj_t BgL_carzd2148zd2_233;

														BgL_carzd2148zd2_233 =
															CAR(((obj_t) BgL_clausesz00_202));
														if (
															(CAR(
																	((obj_t) BgL_carzd2148zd2_233)) ==
																CNST_TABLE_REF(3)))
															{	/* Engine/link.scm 119 */
																obj_t BgL_arg1189z00_236;
																obj_t BgL_arg1190z00_237;

																BgL_arg1189z00_236 =
																	CDR(((obj_t) BgL_carzd2148zd2_233));
																BgL_arg1190z00_237 =
																	CDR(((obj_t) BgL_clausesz00_202));
																BgL_includesz00_213 = BgL_arg1189z00_236;
																BgL_restz00_214 = BgL_arg1190z00_237;
																{	/* Engine/link.scm 134 */
																	obj_t BgL_directivesz00_303;

																	BgL_directivesz00_303 =
																		BGl_zc3z04anonymousza31308ze3ze70z60zzengine_linkz00
																		(BgL_includesz00_213);
																	{
																		obj_t BgL_clausesz00_754;

																		BgL_clausesz00_754 =
																			BGl_appendzd221011zd2zzengine_linkz00
																			(BgL_directivesz00_303, BgL_restz00_214);
																		BgL_clausesz00_202 = BgL_clausesz00_754;
																		goto BGl_loopze70ze7zzengine_linkz00;
																	}
																}
															}
														else
															{	/* Engine/link.scm 119 */
																if (
																	(CAR(
																			((obj_t) BgL_carzd2148zd2_233)) ==
																		CNST_TABLE_REF(4)))
																	{	/* Engine/link.scm 119 */
																		obj_t BgL_arg1196z00_241;

																		BgL_arg1196z00_241 =
																			CDR(((obj_t) BgL_clausesz00_202));
																		BgL_restz00_216 = BgL_arg1196z00_241;
																		{	/* Engine/link.scm 139 */
																			obj_t BgL_arg1314z00_314;
																			obj_t BgL_arg1315z00_315;

																			{	/* Engine/link.scm 139 */
																				obj_t BgL_arg1316z00_316;

																				{	/* Engine/link.scm 139 */
																					obj_t BgL_arg1318z00_318;

																					BgL_arg1318z00_318 =
																						CAR(((obj_t) BgL_clausesz00_202));
																					BgL_arg1316z00_316 =
																						BGl_comptimezd2expandzf2errorz20zzexpand_epsz00
																						(BgL_arg1318z00_318);
																				}
																				{	/* Engine/link.scm 139 */
																					obj_t BgL_list1317z00_317;

																					BgL_list1317z00_317 =
																						MAKE_YOUNG_PAIR(BgL_arg1316z00_316,
																						BNIL);
																					BgL_arg1314z00_314 =
																						BgL_list1317z00_317;
																				}
																			}
																			BgL_arg1315z00_315 =
																				BGl_loopze70ze7zzengine_linkz00
																				(BgL_restz00_216, BgL_librariesz00_203);
																			{
																				obj_t BgL_librariesz00_769;
																				obj_t BgL_clausesz00_768;

																				BgL_clausesz00_768 = BgL_arg1314z00_314;
																				BgL_librariesz00_769 =
																					BgL_arg1315z00_315;
																				BgL_librariesz00_203 =
																					BgL_librariesz00_769;
																				BgL_clausesz00_202 = BgL_clausesz00_768;
																				goto BGl_loopze70ze7zzengine_linkz00;
																			}
																		}
																	}
																else
																	{	/* Engine/link.scm 119 */
																	BgL_tagzd2106zd2_218:
																		{	/* Engine/link.scm 142 */
																			obj_t BgL_arg1319z00_319;

																			BgL_arg1319z00_319 =
																				CDR(((obj_t) BgL_clausesz00_202));
																			{
																				obj_t BgL_clausesz00_772;

																				BgL_clausesz00_772 = BgL_arg1319z00_319;
																				BgL_clausesz00_202 = BgL_clausesz00_772;
																				goto BGl_loopze70ze7zzengine_linkz00;
																			}
																		}
																	}
															}
													}
											}
									}
								else
									{	/* Engine/link.scm 119 */
										goto BgL_tagzd2106zd2_218;
									}
							}
						else
							{	/* Engine/link.scm 119 */
								goto BgL_tagzd2106zd2_218;
							}
					}
			}
		}

	}



/* &find-libraries */
	obj_t BGl_z62findzd2librarieszb0zzengine_linkz00(obj_t BgL_envz00_524,
		obj_t BgL_clausesz00_525)
	{
		{	/* Engine/link.scm 118 */
			return BGl_findzd2librarieszd2zzengine_linkz00(BgL_clausesz00_525);
		}

	}



/* &<@anonymous:1274> */
	obj_t BGl_z62zc3z04anonymousza31274ze3ze5zzengine_linkz00(obj_t
		BgL_envz00_526, obj_t BgL_clausez00_527)
	{
		{	/* Engine/link.scm 127 */
			if (PAIRP(BgL_clausez00_527))
				{	/* Engine/link.scm 127 */
					if ((CAR(((obj_t) BgL_clausez00_527)) == CNST_TABLE_REF(1)))
						{	/* Engine/link.scm 129 */
							obj_t BgL_arg1306z00_547;

							BgL_arg1306z00_547 = MAKE_YOUNG_PAIR(BgL_clausez00_527, BNIL);
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1306z00_547);
						}
					else
						{	/* Engine/link.scm 127 */
							return BFALSE;
						}
				}
			else
				{	/* Engine/link.scm 127 */
					return BFALSE;
				}
		}

	}



/* find-main */
	BGL_EXPORTED_DEF obj_t BGl_findzd2mainzd2zzengine_linkz00(obj_t
		BgL_clausesz00_8)
	{
		{	/* Engine/link.scm 147 */
			{
				obj_t BgL_clausesz00_322;

				BgL_clausesz00_322 = BgL_clausesz00_8;
			BgL_zc3z04anonymousza31320ze3z87_323:
				{
					obj_t BgL_restz00_331;
					obj_t BgL_includesz00_328;
					obj_t BgL_restz00_329;

					if (NULLP(BgL_clausesz00_322))
						{	/* Engine/link.scm 148 */
							return BFALSE;
						}
					else
						{	/* Engine/link.scm 148 */
							if (PAIRP(BgL_clausesz00_322))
								{	/* Engine/link.scm 148 */
									obj_t BgL_carzd2250zd2_337;

									BgL_carzd2250zd2_337 = CAR(((obj_t) BgL_clausesz00_322));
									if (PAIRP(BgL_carzd2250zd2_337))
										{	/* Engine/link.scm 148 */
											obj_t BgL_cdrzd2254zd2_339;

											BgL_cdrzd2254zd2_339 = CDR(BgL_carzd2250zd2_337);
											if ((CAR(BgL_carzd2250zd2_337) == CNST_TABLE_REF(5)))
												{	/* Engine/link.scm 148 */
													if (PAIRP(BgL_cdrzd2254zd2_339))
														{	/* Engine/link.scm 148 */
															if (NULLP(CDR(BgL_cdrzd2254zd2_339)))
																{	/* Engine/link.scm 148 */
																	return CAR(BgL_cdrzd2254zd2_339);
																}
															else
																{	/* Engine/link.scm 148 */
																BgL_tagzd2240zd2_333:
																	{	/* Engine/link.scm 163 */
																		obj_t BgL_arg1376z00_378;

																		BgL_arg1376z00_378 =
																			CDR(((obj_t) BgL_clausesz00_322));
																		{
																			obj_t BgL_clausesz00_805;

																			BgL_clausesz00_805 = BgL_arg1376z00_378;
																			BgL_clausesz00_322 = BgL_clausesz00_805;
																			goto BgL_zc3z04anonymousza31320ze3z87_323;
																		}
																	}
																}
														}
													else
														{	/* Engine/link.scm 148 */
															goto BgL_tagzd2240zd2_333;
														}
												}
											else
												{	/* Engine/link.scm 148 */
													if (
														(CAR(
																((obj_t) BgL_carzd2250zd2_337)) ==
															CNST_TABLE_REF(3)))
														{	/* Engine/link.scm 148 */
															obj_t BgL_arg1339z00_351;
															obj_t BgL_arg1340z00_352;

															BgL_arg1339z00_351 =
																CDR(((obj_t) BgL_carzd2250zd2_337));
															BgL_arg1340z00_352 =
																CDR(((obj_t) BgL_clausesz00_322));
															BgL_includesz00_328 = BgL_arg1339z00_351;
															BgL_restz00_329 = BgL_arg1340z00_352;
															{	/* Engine/link.scm 155 */
																obj_t BgL__ortest_1052z00_361;

																{
																	obj_t BgL_list1059z00_363;

																	BgL_list1059z00_363 = BgL_includesz00_328;
																BgL_zc3z04anonymousza31353ze3z87_364:
																	if (PAIRP(BgL_list1059z00_363))
																		{	/* Engine/link.scm 155 */
																			bool_t BgL_test1528z00_817;

																			{	/* Engine/link.scm 156 */
																				obj_t BgL_includez00_370;

																				BgL_includez00_370 =
																					CAR(BgL_list1059z00_363);
																				{	/* Engine/link.scm 156 */
																					obj_t BgL_arg1367z00_371;

																					BgL_arg1367z00_371 =
																						BGl_readzd2directiveszd2zzread_includez00
																						(BgL_includez00_370);
																					BgL_test1528z00_817 =
																						CBOOL
																						(BGl_findzd2mainzd2zzengine_linkz00
																						(BgL_arg1367z00_371));
																				}
																			}
																			if (BgL_test1528z00_817)
																				{	/* Engine/link.scm 155 */
																					BgL__ortest_1052z00_361 =
																						CAR(BgL_list1059z00_363);
																				}
																			else
																				{
																					obj_t BgL_list1059z00_823;

																					BgL_list1059z00_823 =
																						CDR(BgL_list1059z00_363);
																					BgL_list1059z00_363 =
																						BgL_list1059z00_823;
																					goto
																						BgL_zc3z04anonymousza31353ze3z87_364;
																				}
																		}
																	else
																		{	/* Engine/link.scm 155 */
																			BgL__ortest_1052z00_361 = BFALSE;
																		}
																}
																if (CBOOL(BgL__ortest_1052z00_361))
																	{	/* Engine/link.scm 155 */
																		return BgL__ortest_1052z00_361;
																	}
																else
																	{
																		obj_t BgL_clausesz00_827;

																		BgL_clausesz00_827 = BgL_restz00_329;
																		BgL_clausesz00_322 = BgL_clausesz00_827;
																		goto BgL_zc3z04anonymousza31320ze3z87_323;
																	}
															}
														}
													else
														{	/* Engine/link.scm 148 */
															bool_t BgL_test1530z00_828;

															{	/* Engine/link.scm 148 */
																obj_t BgL_tmpz00_829;

																{	/* Engine/link.scm 148 */
																	obj_t BgL_pairz00_496;

																	BgL_pairz00_496 =
																		CAR(((obj_t) BgL_clausesz00_322));
																	BgL_tmpz00_829 = CAR(BgL_pairz00_496);
																}
																BgL_test1530z00_828 =
																	(BgL_tmpz00_829 == CNST_TABLE_REF(4));
															}
															if (BgL_test1530z00_828)
																{	/* Engine/link.scm 148 */
																	obj_t BgL_arg1346z00_356;

																	BgL_arg1346z00_356 =
																		CDR(((obj_t) BgL_clausesz00_322));
																	BgL_restz00_331 = BgL_arg1346z00_356;
																	{	/* Engine/link.scm 160 */
																		obj_t BgL__ortest_1053z00_373;

																		{	/* Engine/link.scm 160 */
																			obj_t BgL_arg1370z00_374;

																			{	/* Engine/link.scm 160 */
																				obj_t BgL_arg1371z00_375;

																				{	/* Engine/link.scm 160 */
																					obj_t BgL_arg1375z00_377;

																					BgL_arg1375z00_377 =
																						CAR(((obj_t) BgL_clausesz00_322));
																					BgL_arg1371z00_375 =
																						BGl_comptimezd2expandzf2errorz20zzexpand_epsz00
																						(BgL_arg1375z00_377);
																				}
																				{	/* Engine/link.scm 160 */
																					obj_t BgL_list1372z00_376;

																					BgL_list1372z00_376 =
																						MAKE_YOUNG_PAIR(BgL_arg1371z00_375,
																						BNIL);
																					BgL_arg1370z00_374 =
																						BgL_list1372z00_376;
																				}
																			}
																			BgL__ortest_1053z00_373 =
																				BGl_findzd2mainzd2zzengine_linkz00
																				(BgL_arg1370z00_374);
																		}
																		if (CBOOL(BgL__ortest_1053z00_373))
																			{	/* Engine/link.scm 160 */
																				return BgL__ortest_1053z00_373;
																			}
																		else
																			{
																				obj_t BgL_clausesz00_844;

																				BgL_clausesz00_844 = BgL_restz00_331;
																				BgL_clausesz00_322 = BgL_clausesz00_844;
																				goto
																					BgL_zc3z04anonymousza31320ze3z87_323;
																			}
																	}
																}
															else
																{	/* Engine/link.scm 148 */
																	goto BgL_tagzd2240zd2_333;
																}
														}
												}
										}
									else
										{	/* Engine/link.scm 148 */
											goto BgL_tagzd2240zd2_333;
										}
								}
							else
								{	/* Engine/link.scm 148 */
									goto BgL_tagzd2240zd2_333;
								}
						}
				}
			}
		}

	}



/* &find-main */
	obj_t BGl_z62findzd2mainzb0zzengine_linkz00(obj_t BgL_envz00_528,
		obj_t BgL_clausesz00_529)
	{
		{	/* Engine/link.scm 147 */
			return BGl_findzd2mainzd2zzengine_linkz00(BgL_clausesz00_529);
		}

	}



/* make-tmp-main */
	BGL_EXPORTED_DEF obj_t BGl_makezd2tmpzd2mainz00zzengine_linkz00(obj_t
		BgL_filez00_9, bool_t BgL_mainz00_10, obj_t BgL_modulez00_11,
		obj_t BgL_clausesz00_12, obj_t BgL_librariesz00_13)
	{
		{	/* Engine/link.scm 168 */
			{	/* Engine/link.scm 169 */
				obj_t BgL_poutz00_380;

				{	/* Engine/link.scm 169 */

					BgL_poutz00_380 =
						BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(BgL_filez00_9,
						BTRUE);
				}
				if (OUTPUT_PORTP(BgL_poutz00_380))
					{	/* Engine/link.scm 170 */
						bgl_display_string(BGl_string1457z00zzengine_linkz00,
							BgL_poutz00_380);
						bgl_display_obj(BGl_za2bigloozd2nameza2zd2zzengine_paramz00,
							BgL_poutz00_380);
						bgl_display_char(((unsigned char) 10), BgL_poutz00_380);
						bgl_display_string(BGl_string1458z00zzengine_linkz00,
							BgL_poutz00_380);
						bgl_display_char(((unsigned char) 10), BgL_poutz00_380);
						bgl_display_string(BGl_string1459z00zzengine_linkz00,
							BgL_poutz00_380);
						bgl_display_char(((unsigned char) 10), BgL_poutz00_380);
						bgl_display_char(((unsigned char) 10), BgL_poutz00_380);
						{	/* Engine/link.scm 177 */
							obj_t BgL_modz00_385;

							{	/* Engine/link.scm 178 */
								obj_t BgL_arg1408z00_387;

								{	/* Engine/link.scm 178 */
									obj_t BgL_arg1410z00_388;

									{	/* Engine/link.scm 178 */
										obj_t BgL_arg1421z00_389;
										obj_t BgL_arg1422z00_390;

										{	/* Engine/link.scm 178 */
											obj_t BgL_arg1434z00_391;

											BgL_arg1434z00_391 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(bgl_reverse(BgL_clausesz00_12), BNIL);
											BgL_arg1421z00_389 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg1434z00_391);
										}
										BgL_arg1422z00_390 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_librariesz00_13, BNIL);
										BgL_arg1410z00_388 =
											MAKE_YOUNG_PAIR(BgL_arg1421z00_389, BgL_arg1422z00_390);
									}
									BgL_arg1408z00_387 =
										MAKE_YOUNG_PAIR(BgL_modulez00_11, BgL_arg1410z00_388);
								}
								BgL_modz00_385 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1408z00_387);
							}
							bgl_display_obj(BgL_modz00_385, BgL_poutz00_380);
							bgl_display_char(((unsigned char) 10), BgL_poutz00_380);
							bgl_display_char(((unsigned char) 10), BgL_poutz00_380);
						}
						if (BgL_mainz00_10)
							{	/* Engine/link.scm 182 */
								bgl_display_string(BGl_string1460z00zzengine_linkz00,
									BgL_poutz00_380);
								bgl_display_char(((unsigned char) 10), BgL_poutz00_380);
								bgl_display_char(((unsigned char) 10), BgL_poutz00_380);
							}
						else
							{	/* Engine/link.scm 182 */
								BFALSE;
							}
						return bgl_close_output_port(BgL_poutz00_380);
					}
				else
					{	/* Engine/link.scm 170 */
						return
							BGl_errorz00zz__errorz00(BGl_string1461z00zzengine_linkz00,
							BGl_string1462z00zzengine_linkz00, BgL_filez00_9);
					}
			}
		}

	}



/* &make-tmp-main */
	obj_t BGl_z62makezd2tmpzd2mainz62zzengine_linkz00(obj_t BgL_envz00_530,
		obj_t BgL_filez00_531, obj_t BgL_mainz00_532, obj_t BgL_modulez00_533,
		obj_t BgL_clausesz00_534, obj_t BgL_librariesz00_535)
	{
		{	/* Engine/link.scm 168 */
			return
				BGl_makezd2tmpzd2mainz00zzengine_linkz00(BgL_filez00_531,
				CBOOL(BgL_mainz00_532), BgL_modulez00_533, BgL_clausesz00_534,
				BgL_librariesz00_535);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzengine_linkz00(void)
	{
		{	/* Engine/link.scm 20 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzengine_linkz00(void)
	{
		{	/* Engine/link.scm 20 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzengine_linkz00(void)
	{
		{	/* Engine/link.scm 20 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzengine_linkz00(void)
	{
		{	/* Engine/link.scm 20 */
			BGl_modulezd2initializa7ationz75zzcc_ldz00(335315213L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			BGl_modulezd2initializa7ationz75zzread_readerz00(95801752L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			BGl_modulezd2initializa7ationz75zzinit_setrcz00(32737986L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			BGl_modulezd2initializa7ationz75zzexpand_installz00(291484121L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
			return
				BGl_modulezd2initializa7ationz75zzread_includez00(236488026L,
				BSTRING_TO_STRING(BGl_string1463z00zzengine_linkz00));
		}

	}

#ifdef __cplusplus
}
#endif
