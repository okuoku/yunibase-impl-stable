/*===========================================================================*/
/*   (Liveness/types.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Liveness/types.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_LIVENESS_TYPES_TYPE_DEFINITIONS
#define BGL_LIVENESS_TYPES_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_privatez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                 *BgL_privatez00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_wideningz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_wideningz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_localzf2livenesszf2_bgl
	{
		int BgL_z52countz52;
	}                          *BgL_localzf2livenesszf2_bglt;

	typedef struct BgL_literalzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_literalzf2livenesszf2_bglt;

	typedef struct BgL_refzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                        *BgL_refzf2livenesszf2_bglt;

	typedef struct BgL_closurezf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_closurezf2livenesszf2_bglt;

	typedef struct BgL_kwotezf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                          *BgL_kwotezf2livenesszf2_bglt;

	typedef struct BgL_sequencezf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_sequencezf2livenesszf2_bglt;

	typedef struct BgL_appzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                        *BgL_appzf2livenesszf2_bglt;

	typedef struct BgL_appzd2lyzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_appzd2lyzf2livenessz20_bglt;

	typedef struct BgL_funcallzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_funcallzf2livenesszf2_bglt;

	typedef struct BgL_pragmazf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                           *BgL_pragmazf2livenesszf2_bglt;

	typedef struct BgL_getfieldzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_getfieldzf2livenesszf2_bglt;

	typedef struct BgL_setfieldzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_setfieldzf2livenesszf2_bglt;

	typedef struct BgL_wideningzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_wideningzf2livenesszf2_bglt;

	typedef struct BgL_newzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                        *BgL_newzf2livenesszf2_bglt;

	typedef struct BgL_valloczf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                           *BgL_valloczf2livenesszf2_bglt;

	typedef struct BgL_vrefzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_vrefzf2livenesszf2_bglt;

	typedef struct BgL_vsetz12zf2livenessze0_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_vsetz12zf2livenessze0_bglt;

	typedef struct BgL_vlengthzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_vlengthzf2livenesszf2_bglt;

	typedef struct BgL_instanceofzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                               *BgL_instanceofzf2livenesszf2_bglt;

	typedef struct BgL_castzd2nullzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                *BgL_castzd2nullzf2livenessz20_bglt;

	typedef struct BgL_castzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_castzf2livenesszf2_bglt;

	typedef struct BgL_setqzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_setqzf2livenesszf2_bglt;

	typedef struct BgL_conditionalzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                *BgL_conditionalzf2livenesszf2_bglt;

	typedef struct BgL_failzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_failzf2livenesszf2_bglt;

	typedef struct BgL_switchzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                           *BgL_switchzf2livenesszf2_bglt;

	typedef struct BgL_letzd2funzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                              *BgL_letzd2funzf2livenessz20_bglt;

	typedef struct BgL_letzd2varzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                              *BgL_letzd2varzf2livenessz20_bglt;

	typedef struct BgL_setzd2exzd2itzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                  *BgL_setzd2exzd2itzf2livenesszf2_bglt;

	typedef struct BgL_jumpzd2exzd2itzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                   *BgL_jumpzd2exzd2itzf2livenesszf2_bglt;

	typedef struct BgL_synczf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_synczf2livenesszf2_bglt;

	typedef struct BgL_retblockzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_retblockzf2livenesszf2_bglt;

	typedef struct BgL_returnzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                           *BgL_returnzf2livenesszf2_bglt;

	typedef struct BgL_makezd2boxzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                               *BgL_makezd2boxzf2livenessz20_bglt;

	typedef struct BgL_boxzd2refzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                              *BgL_boxzd2refzf2livenessz20_bglt;

	typedef struct BgL_boxzd2setz12zf2livenessz32_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                 *BgL_boxzd2setz12zf2livenessz32_bglt;


#endif													// BGL_LIVENESS_TYPES_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lambda3709z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2657z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2658z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_funcallzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33822ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3550z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2821z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3551z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2822z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3392z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3717z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3393z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3718z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3559z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_newz00_bglt BGl_z62lambda2668z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62lambda3560z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_vsetz12z12_bglt BGl_z62lambda2831z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33394ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3724z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_newz00_bglt BGl_z62lambda2671z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3725z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_vsetz12z12_bglt BGl_z62lambda2834z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_newz00_bglt BGl_z62lambda2674z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_vsetz12z12_bglt BGl_z62lambda2837z62zzliveness_typesz00(obj_t,
		obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t BGl_z62lambda3568z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3569z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33808ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33719ze3ze5zzliveness_typesz00(obj_t);
	static BgL_returnz00_bglt BGl_z62lambda3734z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_returnz00_bglt BGl_z62lambda3738z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2685z62zzliveness_typesz00(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	static obj_t BGl_z62lambda2686z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_jumpzd2exzd2itz00_bglt
		BGl_z62lambda3578z62zzliveness_typesz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3901z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3902z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_returnz00_bglt BGl_z62lambda3741z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_jumpzd2exzd2itz00_bglt
		BGl_z62lambda3582z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2692z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3908z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2693z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3909z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_jumpzd2exzd2itz00_bglt
		BGl_z62lambda3586z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2857z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2858z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2699z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_kwotezf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33753ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3751z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3752z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32595ze3ze5zzliveness_typesz00(obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62lambda3597z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3598z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32104ze3ze5zzliveness_typesz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_wideningzf2livenesszf2zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33770ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3760z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32936ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32774ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3761z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32758ze3ze5zzliveness_typesz00(obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_z62lambda2873z62zzliveness_typesz00(obj_t, obj_t);
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_z62lambda2874z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3768z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3769z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzliveness_typesz00(void);
	static obj_t BGl_z62lambda2880z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33488ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2881z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3775z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3776z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza33845ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33667ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2890z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32687ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2891z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_makezd2boxzd2_bglt BGl_z62lambda3785z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_makezd2boxzd2_bglt BGl_z62lambda3788z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_vlengthz00_bglt BGl_z62lambda2899z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32050ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32123ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33014ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32882ze3ze5zzliveness_typesz00(obj_t);
	static BgL_makezd2boxzd2_bglt BGl_z62lambda3791z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3799z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33790ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33121ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32214ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzliveness_typesz00(void);
	static obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32990ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32232ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32305ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33866ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33777ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33599ze3ze5zzliveness_typesz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_localzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33108ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32976ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32323ze3ze5zzliveness_typesz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_vsetz12zf2livenessze0zzliveness_typesz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_vlengthzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	extern obj_t BGl_pragmaz00zzast_nodez00;
	static obj_t BGl_z62lambda2102z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2103z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33305ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2109z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_newzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda2110z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3003z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32180ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3004z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_refz00_bglt BGl_z62lambda2118z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_switchzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static BgL_refz00_bglt BGl_z62lambda2121z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3012z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32521ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3013z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_refz00_bglt BGl_z62lambda2124z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33129ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32149ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32997ze3ze5zzliveness_typesz00(obj_t);
	static BgL_castzd2nullzd2_bglt BGl_z62lambda3021z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32611ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2133z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2134z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_castzd2nullzd2_bglt BGl_z62lambda3025z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32077ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32239ze3ze5zzliveness_typesz00(obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_castzd2nullzd2_bglt BGl_z62lambda3028z62zzliveness_typesz00(obj_t,
		obj_t);
	extern obj_t BGl_wideningz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza33889ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32701ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2140z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2141z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_appz00_bglt BGl_z62lambda2303z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32434ze3ze5zzliveness_typesz00(obj_t);
	static BgL_appz00_bglt BGl_z62lambda2306z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32418ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33058ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2147z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2148z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_valloczf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda3202z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3203z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33253ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2314z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2315z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33237ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3044z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3045z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2155z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2156z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_returnzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2321z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzliveness_typesz00(void);
	static obj_t BGl_z62zc3z04anonymousza32541ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2322z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_conditionalz00_bglt BGl_z62lambda3213z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33076ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32509ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_closurez00_bglt BGl_z62lambda2164z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32258ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32169ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3056z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2328z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_conditionalz00_bglt BGl_z62lambda3219z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3057z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_closurez00_bglt BGl_z62lambda2167z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2329z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza33433ze3ze5zzliveness_typesz00(obj_t);
	static BgL_conditionalz00_bglt BGl_z62lambda3222z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_closurez00_bglt BGl_z62lambda2170z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32097ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2337z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2338z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3067z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62lambda3068z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2178z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2179z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32292ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33507ze3ze5zzliveness_typesz00(obj_t);
	static BgL_getfieldz00_bglt BGl_z62lambda2504z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32276ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3235z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32187ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3236z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3074z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_getfieldz00_bglt BGl_z62lambda2507z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3075z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2185z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2186z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_appzd2lyzd2_bglt BGl_z62lambda2349z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_letzd2funzd2_bglt BGl_z62lambda3401z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_getfieldz00_bglt BGl_z62lambda2510z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32722ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_letzd2funzd2_bglt BGl_z62lambda3404z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_appzd2lyzd2_bglt BGl_z62lambda2352z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3243z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3244z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2192z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_letzd2funzd2_bglt BGl_z62lambda3407z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_castz00_bglt BGl_z62lambda3083z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2193z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_appzd2lyzd2_bglt BGl_z62lambda2355z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_castz00_bglt BGl_z62lambda3086z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2519z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_castz00_bglt BGl_z62lambda3089z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_castzd2nullzf2livenessz20zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33703ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32812ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32650ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2520z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33452ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3251z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32383ze3ze5zzliveness_typesz00(obj_t);
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	static obj_t BGl_z62lambda3252z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33185ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32456ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2366z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2528z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2367z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2529z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza33615ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3260z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3261z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32635ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32708ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32368ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2374z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2375z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2539z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_pragmazf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda2700z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3431z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2540z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3432z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_failz00_bglt BGl_z62lambda3270z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza33365ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2381z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_synczf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33276ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2382z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_failz00_bglt BGl_z62lambda3273z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2706z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2707z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2547z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_failz00_bglt BGl_z62lambda3277z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2548z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2388z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2389z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32742ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33544ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3441z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3442z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32564ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3606z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3607z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2716z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_setfieldz00_bglt BGl_z62lambda2557z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_funcallz00_bglt BGl_z62lambda2398z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32921ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33561ze3ze5zzliveness_typesz00(obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2720z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3450z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3613z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3451z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3614z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2723z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_setfieldz00_bglt BGl_z62lambda2562z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32476ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32549ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3293z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3294z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_setfieldz00_bglt BGl_z62lambda2565z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3458z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_getfieldzf2livenesszf2zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62lambda3459z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_setfieldzf2livenesszf2zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33740ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33651ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3622z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3623z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2732z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32493ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33295ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2733z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_letzd2varzd2_bglt BGl_z62lambda3469z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33903ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2740z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_syncz00_bglt BGl_z62lambda3632z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32907ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2741z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33385ze3ze5zzliveness_typesz00(obj_t);
	static BgL_letzd2varzd2_bglt BGl_z62lambda3472z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_vlengthz00_bglt BGl_z62lambda2905z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_syncz00_bglt BGl_z62lambda3636z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_vlengthz00_bglt BGl_z62lambda2908z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_letzd2varzd2_bglt BGl_z62lambda3476z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2585z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_syncz00_bglt BGl_z62lambda3639z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2748z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2586z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_z62lambda2749z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda3800z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_letzd2funzf2livenessz20zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33815ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33726ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32673ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33475ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3806z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3807z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2593z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62lambda2756z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2594z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3486z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2919z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2757z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3649z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3487z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_newz00zzast_nodez00;
	static obj_t BGl_z62lambda2920z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3650z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3813z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32836ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3814z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33638ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	static obj_t BGl_z62lambda3494z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2927z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3657z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3495z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2928z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_vrefz00_bglt BGl_z62lambda2766z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3658z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62lambda3820z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_refzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda3821z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2934z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_vrefz00_bglt BGl_z62lambda2772z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32659ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2935z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3665z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3666z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_vrefz00_bglt BGl_z62lambda2775z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_boxzd2refzd2_bglt BGl_z62lambda3829z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32111ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzliveness_typesz00(void);
	static obj_t BGl_z62zc3z04anonymousza33834ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_boxzd2refzd2_bglt BGl_z62lambda3832z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32587ze3ze5zzliveness_typesz00(obj_t);
	static BgL_boxzd2refzd2_bglt BGl_z62lambda3835z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3673z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2944z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3674z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2945z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2785z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2786z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_literalzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_conditionalzf2livenesszf2zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33762ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3843z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3844z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_retblockz00_bglt BGl_z62lambda3685z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzliveness_typesz00(void);
	static BgL_retblockz00_bglt BGl_z62lambda3688z62zzliveness_typesz00(obj_t,
		obj_t);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza32202ze3ze5zzliveness_typesz00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33852ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3850z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3851z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33585ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32694ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33496ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32929ze3ze5zzliveness_typesz00(obj_t);
	static BgL_retblockz00_bglt BGl_z62lambda3692z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3857z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3858z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_instanceofz00_bglt BGl_z62lambda2969z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_instanceofzf2livenesszf2zzliveness_typesz00 =
		BUNSPEC;
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_EXPORTED_DEF obj_t BGl_boxzd2refzf2livenessz20zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33005ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33691ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33675ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32946ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33659ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3864z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3865z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_instanceofz00_bglt BGl_z62lambda2974z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_instanceofz00_bglt BGl_z62lambda2977z62zzliveness_typesz00(obj_t,
		obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static BgL_boxzd2setz12zc0_bglt BGl_z62lambda3873z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_boxzd2setz12zc0_bglt BGl_z62lambda3876z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_boxzd2setz12zc0_bglt BGl_z62lambda3879z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2988z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2989z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32875ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32859ze3ze5zzliveness_typesz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	static obj_t BGl_z62lambda2995z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3887z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2996z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3888z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32892ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32787ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3894z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3895z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33204ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32135ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32330ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33221ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32063ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32225ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33027ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_letzd2varzf2livenessz20zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33859ze3ze5zzliveness_typesz00(obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza32404ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza33312ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32316ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33046ze3ze5zzliveness_typesz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_appzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33878ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_z62lambda2200z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2201z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_retblockzf2livenesszf2zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32246ze3ze5zzliveness_typesz00(obj_t);
	static BgL_localz00_bglt BGl_z62lambda2045z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32157ze3ze5zzliveness_typesz00(obj_t);
	static BgL_kwotez00_bglt BGl_z62lambda2209z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda2048z62zzliveness_typesz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzliveness_typesz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62zc3z04anonymousza33896ze3ze5zzliveness_typesz00(obj_t);
	extern obj_t BGl_appz00zzast_nodez00;
	BGL_EXPORTED_DEF obj_t BGl_appzd2lyzf2livenessz20zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32530ze3ze5zzliveness_typesz00(obj_t);
	static BgL_kwotez00_bglt BGl_z62lambda2212z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32603ze3ze5zzliveness_typesz00(obj_t);
	static BgL_localz00_bglt BGl_z62lambda2051z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_kwotez00_bglt BGl_z62lambda2215z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3106z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33138ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3107z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_vrefzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32442ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2061z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2223z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33406ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2062z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2224z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32426ze3ze5zzliveness_typesz00(obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static obj_t BGl_z62lambda3119z62zzliveness_typesz00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_z62lambda3120z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2230z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2231z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static BgL_literalz00_bglt BGl_z62lambda2071z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32354ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33245ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3127z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_literalz00_bglt BGl_z62lambda2075z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2237z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3128z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2238z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_literalz00_bglt BGl_z62lambda2078z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzliveness_typesz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzliveness_typesz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzliveness_typesz00(void);
	static BgL_funcallz00_bglt BGl_z62lambda2402z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33262ze3ze5zzliveness_typesz00(obj_t);
	static BgL_funcallz00_bglt BGl_z62lambda2405z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2244z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33319ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33157ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2245z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32339ze3ze5zzliveness_typesz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_failzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda3136z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3137z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2087z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2088z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_makezd2boxzf2livenessz20zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62lambda3303z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3304z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32194ze3ze5zzliveness_typesz00(obj_t);
	static BgL_sequencez00_bglt BGl_z62lambda2253z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2416z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2417z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33069ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32089ze3ze5zzliveness_typesz00(obj_t);
	static BgL_sequencez00_bglt BGl_z62lambda2256z62zzliveness_typesz00(obj_t,
		obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	static BgL_setqz00_bglt BGl_z62lambda3147z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2095z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2096z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_sequencez00_bglt BGl_z62lambda2259z62zzliveness_typesz00(obj_t,
		obj_t);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	static obj_t BGl_z62lambda3310z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3311z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32802ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33515ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33353ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32624ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32284ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2424z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2425z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3317z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_setqz00_bglt BGl_z62lambda3155z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3318z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2267z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_setqz00_bglt BGl_z62lambda3158z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2268z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33710ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33532ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32390ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33443ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2432z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2433z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32269ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2274z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2275z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_valuez00zzast_varz00;
	BGL_EXPORTED_DEF obj_t BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00 =
		BUNSPEC;
	extern obj_t BGl_setfieldz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza33460ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2601z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_sequencezf2livenesszf2zzliveness_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32642ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2602z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2440z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2441z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33193ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2282z62zzliveness_typesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_setqzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33177ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2283z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33088ze3ze5zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3175z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_switchz00_bglt BGl_z62lambda3338z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda3176z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2609z62zzliveness_typesz00(obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza33801ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2610z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_pragmaz00_bglt BGl_z62lambda2450z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2290z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3505z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2291z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32376ze3ze5zzliveness_typesz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_closurezf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda3506z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_pragmaz00_bglt BGl_z62lambda2453z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3183z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3184z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_wideningz00_bglt BGl_z62lambda2619z62zzliveness_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_pragmaz00_bglt BGl_z62lambda2457z62zzliveness_typesz00(obj_t,
		obj_t);
	static BgL_appz00_bglt BGl_z62lambda2299z62zzliveness_typesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33624ze3ze5zzliveness_typesz00(obj_t);
	static BgL_switchz00_bglt BGl_z62lambda3350z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3513z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_wideningz00_bglt BGl_z62lambda2622z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza33608ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3514z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3191z62zzliveness_typesz00(obj_t, obj_t);
	static BgL_switchz00_bglt BGl_z62lambda3354z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda3192z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_wideningz00_bglt BGl_z62lambda2625z62zzliveness_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2466z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2467z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32750ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33552ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32823ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32734ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza33374ze3ze5zzliveness_typesz00(obj_t);
	static BgL_setzd2exzd2itz00_bglt
		BGl_z62lambda3524z62zzliveness_typesz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2633z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3363z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2634z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3364z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2474z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2475z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_setzd2exzd2itz00_bglt
		BGl_z62lambda3529z62zzliveness_typesz00(obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62lambda2800z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2801z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2640z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2641z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static BgL_setzd2exzd2itz00_bglt
		BGl_z62lambda3533z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3372z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3373z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32468ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2483z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2484z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2648z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2649z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	BGL_EXPORTED_DEF obj_t BGl_castzf2livenesszf2zzliveness_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza33910ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda3701z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza33570ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2810z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3702z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2811z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3542z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3543z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32485ze3ze5zzliveness_typesz00(obj_t);
	static obj_t BGl_z62lambda2491z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3383z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2492z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda3708z62zzliveness_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda3384z62zzliveness_typesz00(obj_t, obj_t, obj_t);
	static obj_t __cnst[44];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4000z00zzliveness_typesz00,
		BgL_bgl_za762lambda2124za7624520z00,
		BGl_z62lambda2124z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4001z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4521za7,
		BGl_z62zc3z04anonymousza32123ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4002z00zzliveness_typesz00,
		BgL_bgl_za762lambda2121za7624522z00,
		BGl_z62lambda2121z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4003z00zzliveness_typesz00,
		BgL_bgl_za762lambda2118za7624523z00,
		BGl_z62lambda2118z62zzliveness_typesz00, 0L, BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4004z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4524za7,
		BGl_z62zc3z04anonymousza32180ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4005z00zzliveness_typesz00,
		BgL_bgl_za762lambda2179za7624525z00,
		BGl_z62lambda2179z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4006z00zzliveness_typesz00,
		BgL_bgl_za762lambda2178za7624526z00,
		BGl_z62lambda2178z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4007z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4527za7,
		BGl_z62zc3z04anonymousza32187ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4008z00zzliveness_typesz00,
		BgL_bgl_za762lambda2186za7624528z00,
		BGl_z62lambda2186z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4009z00zzliveness_typesz00,
		BgL_bgl_za762lambda2185za7624529z00,
		BGl_z62lambda2185z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4010z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4530za7,
		BGl_z62zc3z04anonymousza32194ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4011z00zzliveness_typesz00,
		BgL_bgl_za762lambda2193za7624531z00,
		BGl_z62lambda2193z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4012z00zzliveness_typesz00,
		BgL_bgl_za762lambda2192za7624532z00,
		BGl_z62lambda2192z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4013z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4533za7,
		BGl_z62zc3z04anonymousza32202ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4014z00zzliveness_typesz00,
		BgL_bgl_za762lambda2201za7624534z00,
		BGl_z62lambda2201z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4015z00zzliveness_typesz00,
		BgL_bgl_za762lambda2200za7624535z00,
		BGl_z62lambda2200z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4016z00zzliveness_typesz00,
		BgL_bgl_za762lambda2170za7624536z00,
		BGl_z62lambda2170z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4017z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4537za7,
		BGl_z62zc3z04anonymousza32169ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4018z00zzliveness_typesz00,
		BgL_bgl_za762lambda2167za7624538z00,
		BGl_z62lambda2167z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4019z00zzliveness_typesz00,
		BgL_bgl_za762lambda2164za7624539z00,
		BGl_z62lambda2164z62zzliveness_typesz00, 0L, BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4020z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4540za7,
		BGl_z62zc3z04anonymousza32225ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4021z00zzliveness_typesz00,
		BgL_bgl_za762lambda2224za7624541z00,
		BGl_z62lambda2224z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4022z00zzliveness_typesz00,
		BgL_bgl_za762lambda2223za7624542z00,
		BGl_z62lambda2223z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4023z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4543za7,
		BGl_z62zc3z04anonymousza32232ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4024z00zzliveness_typesz00,
		BgL_bgl_za762lambda2231za7624544z00,
		BGl_z62lambda2231z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4025z00zzliveness_typesz00,
		BgL_bgl_za762lambda2230za7624545z00,
		BGl_z62lambda2230z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4026z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4546za7,
		BGl_z62zc3z04anonymousza32239ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4027z00zzliveness_typesz00,
		BgL_bgl_za762lambda2238za7624547z00,
		BGl_z62lambda2238z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4028z00zzliveness_typesz00,
		BgL_bgl_za762lambda2237za7624548z00,
		BGl_z62lambda2237z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4029z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4549za7,
		BGl_z62zc3z04anonymousza32246ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4030z00zzliveness_typesz00,
		BgL_bgl_za762lambda2245za7624550z00,
		BGl_z62lambda2245z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4031z00zzliveness_typesz00,
		BgL_bgl_za762lambda2244za7624551z00,
		BGl_z62lambda2244z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4032z00zzliveness_typesz00,
		BgL_bgl_za762lambda2215za7624552z00,
		BGl_z62lambda2215z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4033z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4553za7,
		BGl_z62zc3z04anonymousza32214ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4034z00zzliveness_typesz00,
		BgL_bgl_za762lambda2212za7624554z00,
		BGl_z62lambda2212z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4035z00zzliveness_typesz00,
		BgL_bgl_za762lambda2209za7624555z00,
		BGl_z62lambda2209z62zzliveness_typesz00, 0L, BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4036z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4556za7,
		BGl_z62zc3z04anonymousza32269ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4037z00zzliveness_typesz00,
		BgL_bgl_za762lambda2268za7624557z00,
		BGl_z62lambda2268z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4038z00zzliveness_typesz00,
		BgL_bgl_za762lambda2267za7624558z00,
		BGl_z62lambda2267z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4039z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4559za7,
		BGl_z62zc3z04anonymousza32276ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4200z00zzliveness_typesz00,
		BgL_bgl_za762lambda2801za7624560z00,
		BGl_z62lambda2801z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4201z00zzliveness_typesz00,
		BgL_bgl_za762lambda2800za7624561z00,
		BGl_z62lambda2800z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4202z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4562za7,
		BGl_z62zc3z04anonymousza32812ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4040z00zzliveness_typesz00,
		BgL_bgl_za762lambda2275za7624563z00,
		BGl_z62lambda2275z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4203z00zzliveness_typesz00,
		BgL_bgl_za762lambda2811za7624564z00,
		BGl_z62lambda2811z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4041z00zzliveness_typesz00,
		BgL_bgl_za762lambda2274za7624565z00,
		BGl_z62lambda2274z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4204z00zzliveness_typesz00,
		BgL_bgl_za762lambda2810za7624566z00,
		BGl_z62lambda2810z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4042z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4567za7,
		BGl_z62zc3z04anonymousza32284ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4205z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4568za7,
		BGl_z62zc3z04anonymousza32823ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4043z00zzliveness_typesz00,
		BgL_bgl_za762lambda2283za7624569z00,
		BGl_z62lambda2283z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4206z00zzliveness_typesz00,
		BgL_bgl_za762lambda2822za7624570z00,
		BGl_z62lambda2822z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4044z00zzliveness_typesz00,
		BgL_bgl_za762lambda2282za7624571z00,
		BGl_z62lambda2282z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4207z00zzliveness_typesz00,
		BgL_bgl_za762lambda2821za7624572z00,
		BGl_z62lambda2821z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4045z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4573za7,
		BGl_z62zc3z04anonymousza32292ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4208z00zzliveness_typesz00,
		BgL_bgl_za762lambda2775za7624574z00,
		BGl_z62lambda2775z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4046z00zzliveness_typesz00,
		BgL_bgl_za762lambda2291za7624575z00,
		BGl_z62lambda2291z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4209z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4576za7,
		BGl_z62zc3z04anonymousza32774ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4047z00zzliveness_typesz00,
		BgL_bgl_za762lambda2290za7624577z00,
		BGl_z62lambda2290z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4048z00zzliveness_typesz00,
		BgL_bgl_za762lambda2259za7624578z00,
		BGl_z62lambda2259z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4049z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4579za7,
		BGl_z62zc3z04anonymousza32258ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4210z00zzliveness_typesz00,
		BgL_bgl_za762lambda2772za7624580z00,
		BGl_z62lambda2772z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4211z00zzliveness_typesz00,
		BgL_bgl_za762lambda2766za7624581z00,
		BGl_z62lambda2766z62zzliveness_typesz00, 0L, BUNSPEC, 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4212z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4582za7,
		BGl_z62zc3z04anonymousza32859ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4050z00zzliveness_typesz00,
		BgL_bgl_za762lambda2256za7624583z00,
		BGl_z62lambda2256z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4213z00zzliveness_typesz00,
		BgL_bgl_za762lambda2858za7624584z00,
		BGl_z62lambda2858z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4051z00zzliveness_typesz00,
		BgL_bgl_za762lambda2253za7624585z00,
		BGl_z62lambda2253z62zzliveness_typesz00, 0L, BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4214z00zzliveness_typesz00,
		BgL_bgl_za762lambda2857za7624586z00,
		BGl_z62lambda2857z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4052z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4587za7,
		BGl_z62zc3z04anonymousza32316ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4215z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4588za7,
		BGl_z62zc3z04anonymousza32875ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4053z00zzliveness_typesz00,
		BgL_bgl_za762lambda2315za7624589z00,
		BGl_z62lambda2315z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4216z00zzliveness_typesz00,
		BgL_bgl_za762lambda2874za7624590z00,
		BGl_z62lambda2874z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4054z00zzliveness_typesz00,
		BgL_bgl_za762lambda2314za7624591z00,
		BGl_z62lambda2314z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4217z00zzliveness_typesz00,
		BgL_bgl_za762lambda2873za7624592z00,
		BGl_z62lambda2873z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4055z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4593za7,
		BGl_z62zc3z04anonymousza32323ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4218z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4594za7,
		BGl_z62zc3z04anonymousza32882ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4056z00zzliveness_typesz00,
		BgL_bgl_za762lambda2322za7624595z00,
		BGl_z62lambda2322z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4219z00zzliveness_typesz00,
		BgL_bgl_za762lambda2881za7624596z00,
		BGl_z62lambda2881z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4057z00zzliveness_typesz00,
		BgL_bgl_za762lambda2321za7624597z00,
		BGl_z62lambda2321z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4058z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4598za7,
		BGl_z62zc3z04anonymousza32330ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4059z00zzliveness_typesz00,
		BgL_bgl_za762lambda2329za7624599z00,
		BGl_z62lambda2329z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4220z00zzliveness_typesz00,
		BgL_bgl_za762lambda2880za7624600z00,
		BGl_z62lambda2880z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4221z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4601za7,
		BGl_z62zc3z04anonymousza32892ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4222z00zzliveness_typesz00,
		BgL_bgl_za762lambda2891za7624602z00,
		BGl_z62lambda2891z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4060z00zzliveness_typesz00,
		BgL_bgl_za762lambda2328za7624603z00,
		BGl_z62lambda2328z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4223z00zzliveness_typesz00,
		BgL_bgl_za762lambda2890za7624604z00,
		BGl_z62lambda2890z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4061z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4605za7,
		BGl_z62zc3z04anonymousza32339ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4224z00zzliveness_typesz00,
		BgL_bgl_za762lambda2837za7624606z00,
		BGl_z62lambda2837z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4062z00zzliveness_typesz00,
		BgL_bgl_za762lambda2338za7624607z00,
		BGl_z62lambda2338z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4225z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4608za7,
		BGl_z62zc3z04anonymousza32836ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4063z00zzliveness_typesz00,
		BgL_bgl_za762lambda2337za7624609z00,
		BGl_z62lambda2337z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4226z00zzliveness_typesz00,
		BgL_bgl_za762lambda2834za7624610z00,
		BGl_z62lambda2834z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4064z00zzliveness_typesz00,
		BgL_bgl_za762lambda2306za7624611z00,
		BGl_z62lambda2306z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4227z00zzliveness_typesz00,
		BgL_bgl_za762lambda2831za7624612z00,
		BGl_z62lambda2831z62zzliveness_typesz00, 0L, BUNSPEC, 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4065z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4613za7,
		BGl_z62zc3z04anonymousza32305ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4228z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4614za7,
		BGl_z62zc3z04anonymousza32921ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4066z00zzliveness_typesz00,
		BgL_bgl_za762lambda2303za7624615z00,
		BGl_z62lambda2303z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4229z00zzliveness_typesz00,
		BgL_bgl_za762lambda2920za7624616z00,
		BGl_z62lambda2920z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4067z00zzliveness_typesz00,
		BgL_bgl_za762lambda2299za7624617z00,
		BGl_z62lambda2299z62zzliveness_typesz00, 0L, BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4068z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4618za7,
		BGl_z62zc3z04anonymousza32368ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4069z00zzliveness_typesz00,
		BgL_bgl_za762lambda2367za7624619z00,
		BGl_z62lambda2367z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4230z00zzliveness_typesz00,
		BgL_bgl_za762lambda2919za7624620z00,
		BGl_z62lambda2919z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4231z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4621za7,
		BGl_z62zc3z04anonymousza32929ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4232z00zzliveness_typesz00,
		BgL_bgl_za762lambda2928za7624622z00,
		BGl_z62lambda2928z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4070z00zzliveness_typesz00,
		BgL_bgl_za762lambda2366za7624623z00,
		BGl_z62lambda2366z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4233z00zzliveness_typesz00,
		BgL_bgl_za762lambda2927za7624624z00,
		BGl_z62lambda2927z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4071z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4625za7,
		BGl_z62zc3z04anonymousza32376ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4234z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4626za7,
		BGl_z62zc3z04anonymousza32936ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4072z00zzliveness_typesz00,
		BgL_bgl_za762lambda2375za7624627z00,
		BGl_z62lambda2375z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4235z00zzliveness_typesz00,
		BgL_bgl_za762lambda2935za7624628z00,
		BGl_z62lambda2935z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4073z00zzliveness_typesz00,
		BgL_bgl_za762lambda2374za7624629z00,
		BGl_z62lambda2374z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4236z00zzliveness_typesz00,
		BgL_bgl_za762lambda2934za7624630z00,
		BGl_z62lambda2934z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4074z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4631za7,
		BGl_z62zc3z04anonymousza32383ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4237z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4632za7,
		BGl_z62zc3z04anonymousza32946ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4075z00zzliveness_typesz00,
		BgL_bgl_za762lambda2382za7624633z00,
		BGl_z62lambda2382z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4238z00zzliveness_typesz00,
		BgL_bgl_za762lambda2945za7624634z00,
		BGl_z62lambda2945z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4076z00zzliveness_typesz00,
		BgL_bgl_za762lambda2381za7624635z00,
		BGl_z62lambda2381z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4239z00zzliveness_typesz00,
		BgL_bgl_za762lambda2944za7624636z00,
		BGl_z62lambda2944z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4077z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4637za7,
		BGl_z62zc3z04anonymousza32390ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4078z00zzliveness_typesz00,
		BgL_bgl_za762lambda2389za7624638z00,
		BGl_z62lambda2389z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4079z00zzliveness_typesz00,
		BgL_bgl_za762lambda2388za7624639z00,
		BGl_z62lambda2388z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4400z00zzliveness_typesz00,
		BgL_bgl_za762lambda3533za7624640z00,
		BGl_z62lambda3533z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4401z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4641za7,
		BGl_z62zc3z04anonymousza33532ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4402z00zzliveness_typesz00,
		BgL_bgl_za762lambda3529za7624642z00,
		BGl_z62lambda3529z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4240z00zzliveness_typesz00,
		BgL_bgl_za762lambda2908za7624643z00,
		BGl_z62lambda2908z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4403z00zzliveness_typesz00,
		BgL_bgl_za762lambda3524za7624644z00,
		BGl_z62lambda3524z62zzliveness_typesz00, 0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4241z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4645za7,
		BGl_z62zc3z04anonymousza32907ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4404z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4646za7,
		BGl_z62zc3z04anonymousza33599ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4242z00zzliveness_typesz00,
		BgL_bgl_za762lambda2905za7624647z00,
		BGl_z62lambda2905z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4080z00zzliveness_typesz00,
		BgL_bgl_za762lambda2355za7624648z00,
		BGl_z62lambda2355z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4405z00zzliveness_typesz00,
		BgL_bgl_za762lambda3598za7624649z00,
		BGl_z62lambda3598z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4243z00zzliveness_typesz00,
		BgL_bgl_za762lambda2899za7624650z00,
		BGl_z62lambda2899z62zzliveness_typesz00, 0L, BUNSPEC, 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4081z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4651za7,
		BGl_z62zc3z04anonymousza32354ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4406z00zzliveness_typesz00,
		BgL_bgl_za762lambda3597za7624652z00,
		BGl_z62lambda3597z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4244z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4653za7,
		BGl_z62zc3z04anonymousza32990ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4082z00zzliveness_typesz00,
		BgL_bgl_za762lambda2352za7624654z00,
		BGl_z62lambda2352z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4407z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4655za7,
		BGl_z62zc3z04anonymousza33608ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4245z00zzliveness_typesz00,
		BgL_bgl_za762lambda2989za7624656z00,
		BGl_z62lambda2989z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4083z00zzliveness_typesz00,
		BgL_bgl_za762lambda2349za7624657z00,
		BGl_z62lambda2349z62zzliveness_typesz00, 0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4408z00zzliveness_typesz00,
		BgL_bgl_za762lambda3607za7624658z00,
		BGl_z62lambda3607z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4246z00zzliveness_typesz00,
		BgL_bgl_za762lambda2988za7624659z00,
		BGl_z62lambda2988z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4084z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4660za7,
		BGl_z62zc3z04anonymousza32418ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4409z00zzliveness_typesz00,
		BgL_bgl_za762lambda3606za7624661z00,
		BGl_z62lambda3606z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4247z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4662za7,
		BGl_z62zc3z04anonymousza32997ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4085z00zzliveness_typesz00,
		BgL_bgl_za762lambda2417za7624663z00,
		BGl_z62lambda2417z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4248z00zzliveness_typesz00,
		BgL_bgl_za762lambda2996za7624664z00,
		BGl_z62lambda2996z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4086z00zzliveness_typesz00,
		BgL_bgl_za762lambda2416za7624665z00,
		BGl_z62lambda2416z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4249z00zzliveness_typesz00,
		BgL_bgl_za762lambda2995za7624666z00,
		BGl_z62lambda2995z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4087z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4667za7,
		BGl_z62zc3z04anonymousza32426ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4088z00zzliveness_typesz00,
		BgL_bgl_za762lambda2425za7624668z00,
		BGl_z62lambda2425z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4089z00zzliveness_typesz00,
		BgL_bgl_za762lambda2424za7624669z00,
		BGl_z62lambda2424z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4410z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4670za7,
		BGl_z62zc3z04anonymousza33615ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4411z00zzliveness_typesz00,
		BgL_bgl_za762lambda3614za7624671z00,
		BGl_z62lambda3614z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4412z00zzliveness_typesz00,
		BgL_bgl_za762lambda3613za7624672z00,
		BGl_z62lambda3613z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4250z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4673za7,
		BGl_z62zc3z04anonymousza33005ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4413z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4674za7,
		BGl_z62zc3z04anonymousza33624ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4251z00zzliveness_typesz00,
		BgL_bgl_za762lambda3004za7624675z00,
		BGl_z62lambda3004z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4414z00zzliveness_typesz00,
		BgL_bgl_za762lambda3623za7624676z00,
		BGl_z62lambda3623z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4252z00zzliveness_typesz00,
		BgL_bgl_za762lambda3003za7624677z00,
		BGl_z62lambda3003z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4090z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4678za7,
		BGl_z62zc3z04anonymousza32434ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4415z00zzliveness_typesz00,
		BgL_bgl_za762lambda3622za7624679z00,
		BGl_z62lambda3622z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4253z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4680za7,
		BGl_z62zc3z04anonymousza33014ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4091z00zzliveness_typesz00,
		BgL_bgl_za762lambda2433za7624681z00,
		BGl_z62lambda2433z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4416z00zzliveness_typesz00,
		BgL_bgl_za762lambda3586za7624682z00,
		BGl_z62lambda3586z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4254z00zzliveness_typesz00,
		BgL_bgl_za762lambda3013za7624683z00,
		BGl_z62lambda3013z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4092z00zzliveness_typesz00,
		BgL_bgl_za762lambda2432za7624684z00,
		BGl_z62lambda2432z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4417z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4685za7,
		BGl_z62zc3z04anonymousza33585ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4255z00zzliveness_typesz00,
		BgL_bgl_za762lambda3012za7624686z00,
		BGl_z62lambda3012z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4093z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4687za7,
		BGl_z62zc3z04anonymousza32442ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4418z00zzliveness_typesz00,
		BgL_bgl_za762lambda3582za7624688z00,
		BGl_z62lambda3582z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4256z00zzliveness_typesz00,
		BgL_bgl_za762lambda2977za7624689z00,
		BGl_z62lambda2977z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4094z00zzliveness_typesz00,
		BgL_bgl_za762lambda2441za7624690z00,
		BGl_z62lambda2441z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4419z00zzliveness_typesz00,
		BgL_bgl_za762lambda3578za7624691z00,
		BGl_z62lambda3578z62zzliveness_typesz00, 0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4257z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4692za7,
		BGl_z62zc3z04anonymousza32976ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4095z00zzliveness_typesz00,
		BgL_bgl_za762lambda2440za7624693z00,
		BGl_z62lambda2440z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4258z00zzliveness_typesz00,
		BgL_bgl_za762lambda2974za7624694z00,
		BGl_z62lambda2974z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4096z00zzliveness_typesz00,
		BgL_bgl_za762lambda2405za7624695z00,
		BGl_z62lambda2405z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4259z00zzliveness_typesz00,
		BgL_bgl_za762lambda2969za7624696z00,
		BGl_z62lambda2969z62zzliveness_typesz00, 0L, BUNSPEC, 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4097z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4697za7,
		BGl_z62zc3z04anonymousza32404ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4098z00zzliveness_typesz00,
		BgL_bgl_za762lambda2402za7624698z00,
		BGl_z62lambda2402z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4099z00zzliveness_typesz00,
		BgL_bgl_za762lambda2398za7624699z00,
		BGl_z62lambda2398z62zzliveness_typesz00, 0L, BUNSPEC, 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4420z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4700za7,
		BGl_z62zc3z04anonymousza33651ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4421z00zzliveness_typesz00,
		BgL_bgl_za762lambda3650za7624701z00,
		BGl_z62lambda3650z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4422z00zzliveness_typesz00,
		BgL_bgl_za762lambda3649za7624702z00,
		BGl_z62lambda3649z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4260z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4703za7,
		BGl_z62zc3z04anonymousza33046ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4423z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4704za7,
		BGl_z62zc3z04anonymousza33659ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4261z00zzliveness_typesz00,
		BgL_bgl_za762lambda3045za7624705z00,
		BGl_z62lambda3045z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4424z00zzliveness_typesz00,
		BgL_bgl_za762lambda3658za7624706z00,
		BGl_z62lambda3658z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4262z00zzliveness_typesz00,
		BgL_bgl_za762lambda3044za7624707z00,
		BGl_z62lambda3044z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4425z00zzliveness_typesz00,
		BgL_bgl_za762lambda3657za7624708z00,
		BGl_z62lambda3657z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4263z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4709za7,
		BGl_z62zc3z04anonymousza33058ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4426z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4710za7,
		BGl_z62zc3z04anonymousza33667ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4264z00zzliveness_typesz00,
		BgL_bgl_za762lambda3057za7624711z00,
		BGl_z62lambda3057z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4427z00zzliveness_typesz00,
		BgL_bgl_za762lambda3666za7624712z00,
		BGl_z62lambda3666z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4265z00zzliveness_typesz00,
		BgL_bgl_za762lambda3056za7624713z00,
		BGl_z62lambda3056z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4428z00zzliveness_typesz00,
		BgL_bgl_za762lambda3665za7624714z00,
		BGl_z62lambda3665z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4266z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4715za7,
		BGl_z62zc3z04anonymousza33069ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string4516z00zzliveness_typesz00,
		BgL_bgl_string4516za700za7za7l4716za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4429z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4717za7,
		BGl_z62zc3z04anonymousza33675ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4267z00zzliveness_typesz00,
		BgL_bgl_za762lambda3068za7624718z00,
		BGl_z62lambda3068z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4517z00zzliveness_typesz00,
		BgL_bgl_string4517za700za7za7l4719za7, "liveness_types", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4268z00zzliveness_typesz00,
		BgL_bgl_za762lambda3067za7624720z00,
		BGl_z62lambda3067z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string4518z00zzliveness_typesz00,
		BgL_bgl_string4518za700za7za7l4721za7,
		"_ box-set!/liveness box-ref/liveness make-box/liveness return/liveness retblock/liveness sync/liveness jump-ex-it/liveness set-ex-it/liveness let-var/liveness let-fun/liveness switch/liveness fail/liveness conditional/liveness setq/liveness cast/liveness cast-null/liveness instanceof/liveness vlength/liveness vset!/liveness vref/liveness valloc/liveness new/liveness widening/liveness setfield/liveness getfield/liveness pragma/liveness funcall/liveness app-ly/liveness app/liveness sequence/liveness kwote/liveness closure/liveness ref/liveness literal/liveness out in use pair-nil def liveness_types local/liveness int %count ",
		630);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4269z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4722za7,
		BGl_z62zc3z04anonymousza33076ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4430z00zzliveness_typesz00,
		BgL_bgl_za762lambda3674za7624723z00,
		BGl_z62lambda3674z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4431z00zzliveness_typesz00,
		BgL_bgl_za762lambda3673za7624724z00,
		BGl_z62lambda3673z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4432z00zzliveness_typesz00,
		BgL_bgl_za762lambda3639za7624725z00,
		BGl_z62lambda3639z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4270z00zzliveness_typesz00,
		BgL_bgl_za762lambda3075za7624726z00,
		BGl_z62lambda3075z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4433z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4727za7,
		BGl_z62zc3z04anonymousza33638ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4271z00zzliveness_typesz00,
		BgL_bgl_za762lambda3074za7624728z00,
		BGl_z62lambda3074z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4434z00zzliveness_typesz00,
		BgL_bgl_za762lambda3636za7624729z00,
		BGl_z62lambda3636z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4272z00zzliveness_typesz00,
		BgL_bgl_za762lambda3028za7624730z00,
		BGl_z62lambda3028z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4435z00zzliveness_typesz00,
		BgL_bgl_za762lambda3632za7624731z00,
		BGl_z62lambda3632z62zzliveness_typesz00, 0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4273z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4732za7,
		BGl_z62zc3z04anonymousza33027ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4436z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4733za7,
		BGl_z62zc3z04anonymousza33703ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4274z00zzliveness_typesz00,
		BgL_bgl_za762lambda3025za7624734z00,
		BGl_z62lambda3025z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4437z00zzliveness_typesz00,
		BgL_bgl_za762lambda3702za7624735z00,
		BGl_z62lambda3702z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4275z00zzliveness_typesz00,
		BgL_bgl_za762lambda3021za7624736z00,
		BGl_z62lambda3021z62zzliveness_typesz00, 0L, BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4438z00zzliveness_typesz00,
		BgL_bgl_za762lambda3701za7624737z00,
		BGl_z62lambda3701z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4276z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4738za7,
		BGl_z62zc3z04anonymousza33108ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4439z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4739za7,
		BGl_z62zc3z04anonymousza33710ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4277z00zzliveness_typesz00,
		BgL_bgl_za762lambda3107za7624740z00,
		BGl_z62lambda3107z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4278z00zzliveness_typesz00,
		BgL_bgl_za762lambda3106za7624741z00,
		BGl_z62lambda3106z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4279z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4742za7,
		BGl_z62zc3z04anonymousza33121ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4440z00zzliveness_typesz00,
		BgL_bgl_za762lambda3709za7624743z00,
		BGl_z62lambda3709z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4441z00zzliveness_typesz00,
		BgL_bgl_za762lambda3708za7624744z00,
		BGl_z62lambda3708z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4442z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4745za7,
		BGl_z62zc3z04anonymousza33719ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4280z00zzliveness_typesz00,
		BgL_bgl_za762lambda3120za7624746z00,
		BGl_z62lambda3120z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4443z00zzliveness_typesz00,
		BgL_bgl_za762lambda3718za7624747z00,
		BGl_z62lambda3718z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4281z00zzliveness_typesz00,
		BgL_bgl_za762lambda3119za7624748z00,
		BGl_z62lambda3119z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4444z00zzliveness_typesz00,
		BgL_bgl_za762lambda3717za7624749z00,
		BGl_z62lambda3717z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4282z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4750za7,
		BGl_z62zc3z04anonymousza33129ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4445z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4751za7,
		BGl_z62zc3z04anonymousza33726ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4283z00zzliveness_typesz00,
		BgL_bgl_za762lambda3128za7624752z00,
		BGl_z62lambda3128z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4446z00zzliveness_typesz00,
		BgL_bgl_za762lambda3725za7624753z00,
		BGl_z62lambda3725z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4284z00zzliveness_typesz00,
		BgL_bgl_za762lambda3127za7624754z00,
		BGl_z62lambda3127z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4447z00zzliveness_typesz00,
		BgL_bgl_za762lambda3724za7624755z00,
		BGl_z62lambda3724z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4285z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4756za7,
		BGl_z62zc3z04anonymousza33138ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4448z00zzliveness_typesz00,
		BgL_bgl_za762lambda3692za7624757z00,
		BGl_z62lambda3692z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4286z00zzliveness_typesz00,
		BgL_bgl_za762lambda3137za7624758z00,
		BGl_z62lambda3137z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4449z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4759za7,
		BGl_z62zc3z04anonymousza33691ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4287z00zzliveness_typesz00,
		BgL_bgl_za762lambda3136za7624760z00,
		BGl_z62lambda3136z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4288z00zzliveness_typesz00,
		BgL_bgl_za762lambda3089za7624761z00,
		BGl_z62lambda3089z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4289z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4762za7,
		BGl_z62zc3z04anonymousza33088ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4450z00zzliveness_typesz00,
		BgL_bgl_za762lambda3688za7624763z00,
		BGl_z62lambda3688z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4451z00zzliveness_typesz00,
		BgL_bgl_za762lambda3685za7624764z00,
		BGl_z62lambda3685z62zzliveness_typesz00, 0L, BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4452z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4765za7,
		BGl_z62zc3z04anonymousza33753ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4290z00zzliveness_typesz00,
		BgL_bgl_za762lambda3086za7624766z00,
		BGl_z62lambda3086z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4453z00zzliveness_typesz00,
		BgL_bgl_za762lambda3752za7624767z00,
		BGl_z62lambda3752z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4291z00zzliveness_typesz00,
		BgL_bgl_za762lambda3083za7624768z00,
		BGl_z62lambda3083z62zzliveness_typesz00, 0L, BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4454z00zzliveness_typesz00,
		BgL_bgl_za762lambda3751za7624769z00,
		BGl_z62lambda3751z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4292z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4770za7,
		BGl_z62zc3z04anonymousza33177ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4455z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4771za7,
		BGl_z62zc3z04anonymousza33762ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4293z00zzliveness_typesz00,
		BgL_bgl_za762lambda3176za7624772z00,
		BGl_z62lambda3176z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4456z00zzliveness_typesz00,
		BgL_bgl_za762lambda3761za7624773z00,
		BGl_z62lambda3761z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4294z00zzliveness_typesz00,
		BgL_bgl_za762lambda3175za7624774z00,
		BGl_z62lambda3175z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4457z00zzliveness_typesz00,
		BgL_bgl_za762lambda3760za7624775z00,
		BGl_z62lambda3760z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4295z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4776za7,
		BGl_z62zc3z04anonymousza33185ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4458z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4777za7,
		BGl_z62zc3z04anonymousza33770ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4296z00zzliveness_typesz00,
		BgL_bgl_za762lambda3184za7624778z00,
		BGl_z62lambda3184z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4459z00zzliveness_typesz00,
		BgL_bgl_za762lambda3769za7624779z00,
		BGl_z62lambda3769z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4297z00zzliveness_typesz00,
		BgL_bgl_za762lambda3183za7624780z00,
		BGl_z62lambda3183z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4298z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4781za7,
		BGl_z62zc3z04anonymousza33193ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4299z00zzliveness_typesz00,
		BgL_bgl_za762lambda3192za7624782z00,
		BGl_z62lambda3192z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4460z00zzliveness_typesz00,
		BgL_bgl_za762lambda3768za7624783z00,
		BGl_z62lambda3768z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4461z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4784za7,
		BGl_z62zc3z04anonymousza33777ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4462z00zzliveness_typesz00,
		BgL_bgl_za762lambda3776za7624785z00,
		BGl_z62lambda3776z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4463z00zzliveness_typesz00,
		BgL_bgl_za762lambda3775za7624786z00,
		BGl_z62lambda3775z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4464z00zzliveness_typesz00,
		BgL_bgl_za762lambda3741za7624787z00,
		BGl_z62lambda3741z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4465z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4788za7,
		BGl_z62zc3z04anonymousza33740ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4466z00zzliveness_typesz00,
		BgL_bgl_za762lambda3738za7624789z00,
		BGl_z62lambda3738z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4467z00zzliveness_typesz00,
		BgL_bgl_za762lambda3734za7624790z00,
		BGl_z62lambda3734z62zzliveness_typesz00, 0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4468z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4791za7,
		BGl_z62zc3z04anonymousza33801ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4469z00zzliveness_typesz00,
		BgL_bgl_za762lambda3800za7624792z00,
		BGl_z62lambda3800z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4470z00zzliveness_typesz00,
		BgL_bgl_za762lambda3799za7624793z00,
		BGl_z62lambda3799z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4471z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4794za7,
		BGl_z62zc3z04anonymousza33808ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4472z00zzliveness_typesz00,
		BgL_bgl_za762lambda3807za7624795z00,
		BGl_z62lambda3807z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4473z00zzliveness_typesz00,
		BgL_bgl_za762lambda3806za7624796z00,
		BGl_z62lambda3806z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4474z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4797za7,
		BGl_z62zc3z04anonymousza33815ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4475z00zzliveness_typesz00,
		BgL_bgl_za762lambda3814za7624798z00,
		BGl_z62lambda3814z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4476z00zzliveness_typesz00,
		BgL_bgl_za762lambda3813za7624799z00,
		BGl_z62lambda3813z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4477z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4800za7,
		BGl_z62zc3z04anonymousza33822ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4478z00zzliveness_typesz00,
		BgL_bgl_za762lambda3821za7624801z00,
		BGl_z62lambda3821z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4479z00zzliveness_typesz00,
		BgL_bgl_za762lambda3820za7624802z00,
		BGl_z62lambda3820z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4480z00zzliveness_typesz00,
		BgL_bgl_za762lambda3791za7624803z00,
		BGl_z62lambda3791z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4481z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4804za7,
		BGl_z62zc3z04anonymousza33790ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4482z00zzliveness_typesz00,
		BgL_bgl_za762lambda3788za7624805z00,
		BGl_z62lambda3788z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4483z00zzliveness_typesz00,
		BgL_bgl_za762lambda3785za7624806z00,
		BGl_z62lambda3785z62zzliveness_typesz00, 0L, BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4484z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4807za7,
		BGl_z62zc3z04anonymousza33845ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4485z00zzliveness_typesz00,
		BgL_bgl_za762lambda3844za7624808z00,
		BGl_z62lambda3844z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4486z00zzliveness_typesz00,
		BgL_bgl_za762lambda3843za7624809z00,
		BGl_z62lambda3843z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4487z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4810za7,
		BGl_z62zc3z04anonymousza33852ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4488z00zzliveness_typesz00,
		BgL_bgl_za762lambda3851za7624811z00,
		BGl_z62lambda3851z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4489z00zzliveness_typesz00,
		BgL_bgl_za762lambda3850za7624812z00,
		BGl_z62lambda3850z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4490z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4813za7,
		BGl_z62zc3z04anonymousza33859ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4491z00zzliveness_typesz00,
		BgL_bgl_za762lambda3858za7624814z00,
		BGl_z62lambda3858z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4492z00zzliveness_typesz00,
		BgL_bgl_za762lambda3857za7624815z00,
		BGl_z62lambda3857z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4493z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4816za7,
		BGl_z62zc3z04anonymousza33866ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4494z00zzliveness_typesz00,
		BgL_bgl_za762lambda3865za7624817z00,
		BGl_z62lambda3865z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4495z00zzliveness_typesz00,
		BgL_bgl_za762lambda3864za7624818z00,
		BGl_z62lambda3864z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4496z00zzliveness_typesz00,
		BgL_bgl_za762lambda3835za7624819z00,
		BGl_z62lambda3835z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4497z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4820za7,
		BGl_z62zc3z04anonymousza33834ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4498z00zzliveness_typesz00,
		BgL_bgl_za762lambda3832za7624821z00,
		BGl_z62lambda3832z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4499z00zzliveness_typesz00,
		BgL_bgl_za762lambda3829za7624822z00,
		BGl_z62lambda3829z62zzliveness_typesz00, 0L, BUNSPEC, 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3965z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4823za7,
		BGl_z62zc3z04anonymousza32063ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3966z00zzliveness_typesz00,
		BgL_bgl_za762lambda2062za7624824z00,
		BGl_z62lambda2062z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3967z00zzliveness_typesz00,
		BgL_bgl_za762lambda2061za7624825z00,
		BGl_z62lambda2061z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3968z00zzliveness_typesz00,
		BgL_bgl_za762lambda2051za7624826z00,
		BGl_z62lambda2051z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3969z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4827za7,
		BGl_z62zc3z04anonymousza32050ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3970z00zzliveness_typesz00,
		BgL_bgl_za762lambda2048za7624828z00,
		BGl_z62lambda2048z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3971z00zzliveness_typesz00,
		BgL_bgl_za762lambda2045za7624829z00,
		BGl_z62lambda2045z62zzliveness_typesz00, 0L, BUNSPEC, 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3972z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4830za7,
		BGl_z62zc3z04anonymousza32089ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3973z00zzliveness_typesz00,
		BgL_bgl_za762lambda2088za7624831z00,
		BGl_z62lambda2088z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3974z00zzliveness_typesz00,
		BgL_bgl_za762lambda2087za7624832z00,
		BGl_z62lambda2087z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3975z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4833za7,
		BGl_z62zc3z04anonymousza32097ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3976z00zzliveness_typesz00,
		BgL_bgl_za762lambda2096za7624834z00,
		BGl_z62lambda2096z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3977z00zzliveness_typesz00,
		BgL_bgl_za762lambda2095za7624835z00,
		BGl_z62lambda2095z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3978z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4836za7,
		BGl_z62zc3z04anonymousza32104ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3979z00zzliveness_typesz00,
		BgL_bgl_za762lambda2103za7624837z00,
		BGl_z62lambda2103z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3980z00zzliveness_typesz00,
		BgL_bgl_za762lambda2102za7624838z00,
		BGl_z62lambda2102z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3981z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4839za7,
		BGl_z62zc3z04anonymousza32111ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3982z00zzliveness_typesz00,
		BgL_bgl_za762lambda2110za7624840z00,
		BGl_z62lambda2110z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3983z00zzliveness_typesz00,
		BgL_bgl_za762lambda2109za7624841z00,
		BGl_z62lambda2109z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3984z00zzliveness_typesz00,
		BgL_bgl_za762lambda2078za7624842z00,
		BGl_z62lambda2078z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3985z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4843za7,
		BGl_z62zc3z04anonymousza32077ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3986z00zzliveness_typesz00,
		BgL_bgl_za762lambda2075za7624844z00,
		BGl_z62lambda2075z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3987z00zzliveness_typesz00,
		BgL_bgl_za762lambda2071za7624845z00,
		BGl_z62lambda2071z62zzliveness_typesz00, 0L, BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3988z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4846za7,
		BGl_z62zc3z04anonymousza32135ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3989z00zzliveness_typesz00,
		BgL_bgl_za762lambda2134za7624847z00,
		BGl_z62lambda2134z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3990z00zzliveness_typesz00,
		BgL_bgl_za762lambda2133za7624848z00,
		BGl_z62lambda2133z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3991z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4849za7,
		BGl_z62zc3z04anonymousza32142ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3992z00zzliveness_typesz00,
		BgL_bgl_za762lambda2141za7624850z00,
		BGl_z62lambda2141z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3993z00zzliveness_typesz00,
		BgL_bgl_za762lambda2140za7624851z00,
		BGl_z62lambda2140z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3994z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4852za7,
		BGl_z62zc3z04anonymousza32149ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3995z00zzliveness_typesz00,
		BgL_bgl_za762lambda2148za7624853z00,
		BGl_z62lambda2148z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3996z00zzliveness_typesz00,
		BgL_bgl_za762lambda2147za7624854z00,
		BGl_z62lambda2147z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3997z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4855za7,
		BGl_z62zc3z04anonymousza32157ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3998z00zzliveness_typesz00,
		BgL_bgl_za762lambda2156za7624856z00,
		BGl_z62lambda2156z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3999z00zzliveness_typesz00,
		BgL_bgl_za762lambda2155za7624857z00,
		BGl_z62lambda2155z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4100z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4858za7,
		BGl_z62zc3z04anonymousza32468ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4101z00zzliveness_typesz00,
		BgL_bgl_za762lambda2467za7624859z00,
		BGl_z62lambda2467z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4102z00zzliveness_typesz00,
		BgL_bgl_za762lambda2466za7624860z00,
		BGl_z62lambda2466z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4103z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4861za7,
		BGl_z62zc3z04anonymousza32476ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4104z00zzliveness_typesz00,
		BgL_bgl_za762lambda2475za7624862z00,
		BGl_z62lambda2475z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4105z00zzliveness_typesz00,
		BgL_bgl_za762lambda2474za7624863z00,
		BGl_z62lambda2474z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4106z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4864za7,
		BGl_z62zc3z04anonymousza32485ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4107z00zzliveness_typesz00,
		BgL_bgl_za762lambda2484za7624865z00,
		BGl_z62lambda2484z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4108z00zzliveness_typesz00,
		BgL_bgl_za762lambda2483za7624866z00,
		BGl_z62lambda2483z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4109z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4867za7,
		BGl_z62zc3z04anonymousza32493ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4110z00zzliveness_typesz00,
		BgL_bgl_za762lambda2492za7624868z00,
		BGl_z62lambda2492z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4111z00zzliveness_typesz00,
		BgL_bgl_za762lambda2491za7624869z00,
		BGl_z62lambda2491z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4112z00zzliveness_typesz00,
		BgL_bgl_za762lambda2457za7624870z00,
		BGl_z62lambda2457z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4113z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4871za7,
		BGl_z62zc3z04anonymousza32456ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4114z00zzliveness_typesz00,
		BgL_bgl_za762lambda2453za7624872z00,
		BGl_z62lambda2453z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4115z00zzliveness_typesz00,
		BgL_bgl_za762lambda2450za7624873z00,
		BGl_z62lambda2450z62zzliveness_typesz00, 0L, BUNSPEC, 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4116z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4874za7,
		BGl_z62zc3z04anonymousza32521ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4117z00zzliveness_typesz00,
		BgL_bgl_za762lambda2520za7624875z00,
		BGl_z62lambda2520z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4118z00zzliveness_typesz00,
		BgL_bgl_za762lambda2519za7624876z00,
		BGl_z62lambda2519z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4119z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4877za7,
		BGl_z62zc3z04anonymousza32530ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4120z00zzliveness_typesz00,
		BgL_bgl_za762lambda2529za7624878z00,
		BGl_z62lambda2529z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4121z00zzliveness_typesz00,
		BgL_bgl_za762lambda2528za7624879z00,
		BGl_z62lambda2528z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4122z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4880za7,
		BGl_z62zc3z04anonymousza32541ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4123z00zzliveness_typesz00,
		BgL_bgl_za762lambda2540za7624881z00,
		BGl_z62lambda2540z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4124z00zzliveness_typesz00,
		BgL_bgl_za762lambda2539za7624882z00,
		BGl_z62lambda2539z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4125z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4883za7,
		BGl_z62zc3z04anonymousza32549ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4126z00zzliveness_typesz00,
		BgL_bgl_za762lambda2548za7624884z00,
		BGl_z62lambda2548z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4127z00zzliveness_typesz00,
		BgL_bgl_za762lambda2547za7624885z00,
		BGl_z62lambda2547z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4128z00zzliveness_typesz00,
		BgL_bgl_za762lambda2510za7624886z00,
		BGl_z62lambda2510z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4129z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4887za7,
		BGl_z62zc3z04anonymousza32509ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4130z00zzliveness_typesz00,
		BgL_bgl_za762lambda2507za7624888z00,
		BGl_z62lambda2507z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4131z00zzliveness_typesz00,
		BgL_bgl_za762lambda2504za7624889z00,
		BGl_z62lambda2504z62zzliveness_typesz00, 0L, BUNSPEC, 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4132z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4890za7,
		BGl_z62zc3z04anonymousza32587ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4133z00zzliveness_typesz00,
		BgL_bgl_za762lambda2586za7624891z00,
		BGl_z62lambda2586z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4134z00zzliveness_typesz00,
		BgL_bgl_za762lambda2585za7624892z00,
		BGl_z62lambda2585z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4135z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4893za7,
		BGl_z62zc3z04anonymousza32595ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4136z00zzliveness_typesz00,
		BgL_bgl_za762lambda2594za7624894z00,
		BGl_z62lambda2594z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4137z00zzliveness_typesz00,
		BgL_bgl_za762lambda2593za7624895z00,
		BGl_z62lambda2593z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4138z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4896za7,
		BGl_z62zc3z04anonymousza32603ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4139z00zzliveness_typesz00,
		BgL_bgl_za762lambda2602za7624897z00,
		BGl_z62lambda2602z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4300z00zzliveness_typesz00,
		BgL_bgl_za762lambda3191za7624898z00,
		BGl_z62lambda3191z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4301z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4899za7,
		BGl_z62zc3z04anonymousza33204ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4302z00zzliveness_typesz00,
		BgL_bgl_za762lambda3203za7624900z00,
		BGl_z62lambda3203z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4140z00zzliveness_typesz00,
		BgL_bgl_za762lambda2601za7624901z00,
		BGl_z62lambda2601z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4303z00zzliveness_typesz00,
		BgL_bgl_za762lambda3202za7624902z00,
		BGl_z62lambda3202z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4141z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4903za7,
		BGl_z62zc3z04anonymousza32611ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4304z00zzliveness_typesz00,
		BgL_bgl_za762lambda3158za7624904z00,
		BGl_z62lambda3158z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4142z00zzliveness_typesz00,
		BgL_bgl_za762lambda2610za7624905z00,
		BGl_z62lambda2610z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4305z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4906za7,
		BGl_z62zc3z04anonymousza33157ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4143z00zzliveness_typesz00,
		BgL_bgl_za762lambda2609za7624907z00,
		BGl_z62lambda2609z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4306z00zzliveness_typesz00,
		BgL_bgl_za762lambda3155za7624908z00,
		BGl_z62lambda3155z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4144z00zzliveness_typesz00,
		BgL_bgl_za762lambda2565za7624909z00,
		BGl_z62lambda2565z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4307z00zzliveness_typesz00,
		BgL_bgl_za762lambda3147za7624910z00,
		BGl_z62lambda3147z62zzliveness_typesz00, 0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4145z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4911za7,
		BGl_z62zc3z04anonymousza32564ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4308z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4912za7,
		BGl_z62zc3z04anonymousza33237ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4146z00zzliveness_typesz00,
		BgL_bgl_za762lambda2562za7624913z00,
		BGl_z62lambda2562z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4309z00zzliveness_typesz00,
		BgL_bgl_za762lambda3236za7624914z00,
		BGl_z62lambda3236z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4147z00zzliveness_typesz00,
		BgL_bgl_za762lambda2557za7624915z00,
		BGl_z62lambda2557z62zzliveness_typesz00, 0L, BUNSPEC, 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4148z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4916za7,
		BGl_z62zc3z04anonymousza32635ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4149z00zzliveness_typesz00,
		BgL_bgl_za762lambda2634za7624917z00,
		BGl_z62lambda2634z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4310z00zzliveness_typesz00,
		BgL_bgl_za762lambda3235za7624918z00,
		BGl_z62lambda3235z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4311z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4919za7,
		BGl_z62zc3z04anonymousza33245ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4312z00zzliveness_typesz00,
		BgL_bgl_za762lambda3244za7624920z00,
		BGl_z62lambda3244z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4150z00zzliveness_typesz00,
		BgL_bgl_za762lambda2633za7624921z00,
		BGl_z62lambda2633z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4313z00zzliveness_typesz00,
		BgL_bgl_za762lambda3243za7624922z00,
		BGl_z62lambda3243z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4151z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4923za7,
		BGl_z62zc3z04anonymousza32642ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4314z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4924za7,
		BGl_z62zc3z04anonymousza33253ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4152z00zzliveness_typesz00,
		BgL_bgl_za762lambda2641za7624925z00,
		BGl_z62lambda2641z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4315z00zzliveness_typesz00,
		BgL_bgl_za762lambda3252za7624926z00,
		BGl_z62lambda3252z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4153z00zzliveness_typesz00,
		BgL_bgl_za762lambda2640za7624927z00,
		BGl_z62lambda2640z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4316z00zzliveness_typesz00,
		BgL_bgl_za762lambda3251za7624928z00,
		BGl_z62lambda3251z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4154z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4929za7,
		BGl_z62zc3z04anonymousza32650ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4317z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4930za7,
		BGl_z62zc3z04anonymousza33262ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4155z00zzliveness_typesz00,
		BgL_bgl_za762lambda2649za7624931z00,
		BGl_z62lambda2649z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4318z00zzliveness_typesz00,
		BgL_bgl_za762lambda3261za7624932z00,
		BGl_z62lambda3261z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4156z00zzliveness_typesz00,
		BgL_bgl_za762lambda2648za7624933z00,
		BGl_z62lambda2648z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4319z00zzliveness_typesz00,
		BgL_bgl_za762lambda3260za7624934z00,
		BGl_z62lambda3260z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4157z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4935za7,
		BGl_z62zc3z04anonymousza32659ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4158z00zzliveness_typesz00,
		BgL_bgl_za762lambda2658za7624936z00,
		BGl_z62lambda2658z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4159z00zzliveness_typesz00,
		BgL_bgl_za762lambda2657za7624937z00,
		BGl_z62lambda2657z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4320z00zzliveness_typesz00,
		BgL_bgl_za762lambda3222za7624938z00,
		BGl_z62lambda3222z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4321z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4939za7,
		BGl_z62zc3z04anonymousza33221ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4322z00zzliveness_typesz00,
		BgL_bgl_za762lambda3219za7624940z00,
		BGl_z62lambda3219z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4160z00zzliveness_typesz00,
		BgL_bgl_za762lambda2625za7624941z00,
		BGl_z62lambda2625z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4323z00zzliveness_typesz00,
		BgL_bgl_za762lambda3213za7624942z00,
		BGl_z62lambda3213z62zzliveness_typesz00, 0L, BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4161z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4943za7,
		BGl_z62zc3z04anonymousza32624ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4324z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4944za7,
		BGl_z62zc3z04anonymousza33295ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4162z00zzliveness_typesz00,
		BgL_bgl_za762lambda2622za7624945z00,
		BGl_z62lambda2622z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4325z00zzliveness_typesz00,
		BgL_bgl_za762lambda3294za7624946z00,
		BGl_z62lambda3294z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4163z00zzliveness_typesz00,
		BgL_bgl_za762lambda2619za7624947z00,
		BGl_z62lambda2619z62zzliveness_typesz00, 0L, BUNSPEC, 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4326z00zzliveness_typesz00,
		BgL_bgl_za762lambda3293za7624948z00,
		BGl_z62lambda3293z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4164z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4949za7,
		BGl_z62zc3z04anonymousza32687ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4327z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4950za7,
		BGl_z62zc3z04anonymousza33305ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4165z00zzliveness_typesz00,
		BgL_bgl_za762lambda2686za7624951z00,
		BGl_z62lambda2686z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4328z00zzliveness_typesz00,
		BgL_bgl_za762lambda3304za7624952z00,
		BGl_z62lambda3304z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4166z00zzliveness_typesz00,
		BgL_bgl_za762lambda2685za7624953z00,
		BGl_z62lambda2685z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4329z00zzliveness_typesz00,
		BgL_bgl_za762lambda3303za7624954z00,
		BGl_z62lambda3303z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4167z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4955za7,
		BGl_z62zc3z04anonymousza32694ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4168z00zzliveness_typesz00,
		BgL_bgl_za762lambda2693za7624956z00,
		BGl_z62lambda2693z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4169z00zzliveness_typesz00,
		BgL_bgl_za762lambda2692za7624957z00,
		BGl_z62lambda2692z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4330z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4958za7,
		BGl_z62zc3z04anonymousza33312ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4331z00zzliveness_typesz00,
		BgL_bgl_za762lambda3311za7624959z00,
		BGl_z62lambda3311z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4332z00zzliveness_typesz00,
		BgL_bgl_za762lambda3310za7624960z00,
		BGl_z62lambda3310z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4170z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4961za7,
		BGl_z62zc3z04anonymousza32701ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4333z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4962za7,
		BGl_z62zc3z04anonymousza33319ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4171z00zzliveness_typesz00,
		BgL_bgl_za762lambda2700za7624963z00,
		BGl_z62lambda2700z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4334z00zzliveness_typesz00,
		BgL_bgl_za762lambda3318za7624964z00,
		BGl_z62lambda3318z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4172z00zzliveness_typesz00,
		BgL_bgl_za762lambda2699za7624965z00,
		BGl_z62lambda2699z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4335z00zzliveness_typesz00,
		BgL_bgl_za762lambda3317za7624966z00,
		BGl_z62lambda3317z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4173z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4967za7,
		BGl_z62zc3z04anonymousza32708ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4336z00zzliveness_typesz00,
		BgL_bgl_za762lambda3277za7624968z00,
		BGl_z62lambda3277z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4174z00zzliveness_typesz00,
		BgL_bgl_za762lambda2707za7624969z00,
		BGl_z62lambda2707z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4337z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4970za7,
		BGl_z62zc3z04anonymousza33276ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4175z00zzliveness_typesz00,
		BgL_bgl_za762lambda2706za7624971z00,
		BGl_z62lambda2706z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4338z00zzliveness_typesz00,
		BgL_bgl_za762lambda3273za7624972z00,
		BGl_z62lambda3273z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4176z00zzliveness_typesz00,
		BgL_bgl_za762lambda2674za7624973z00,
		BGl_z62lambda2674z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4339z00zzliveness_typesz00,
		BgL_bgl_za762lambda3270za7624974z00,
		BGl_z62lambda3270z62zzliveness_typesz00, 0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4177z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4975za7,
		BGl_z62zc3z04anonymousza32673ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4178z00zzliveness_typesz00,
		BgL_bgl_za762lambda2671za7624976z00,
		BGl_z62lambda2671z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4179z00zzliveness_typesz00,
		BgL_bgl_za762lambda2668za7624977z00,
		BGl_z62lambda2668z62zzliveness_typesz00, 0L, BUNSPEC, 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4500z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4978za7,
		BGl_z62zc3z04anonymousza33889ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4501z00zzliveness_typesz00,
		BgL_bgl_za762lambda3888za7624979z00,
		BGl_z62lambda3888z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4502z00zzliveness_typesz00,
		BgL_bgl_za762lambda3887za7624980z00,
		BGl_z62lambda3887z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4340z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4981za7,
		BGl_z62zc3z04anonymousza33365ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4503z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4982za7,
		BGl_z62zc3z04anonymousza33896ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4341z00zzliveness_typesz00,
		BgL_bgl_za762lambda3364za7624983z00,
		BGl_z62lambda3364z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4504z00zzliveness_typesz00,
		BgL_bgl_za762lambda3895za7624984z00,
		BGl_z62lambda3895z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4342z00zzliveness_typesz00,
		BgL_bgl_za762lambda3363za7624985z00,
		BGl_z62lambda3363z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4180z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4986za7,
		BGl_z62zc3z04anonymousza32734ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4505z00zzliveness_typesz00,
		BgL_bgl_za762lambda3894za7624987z00,
		BGl_z62lambda3894z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4343z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4988za7,
		BGl_z62zc3z04anonymousza33374ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4181z00zzliveness_typesz00,
		BgL_bgl_za762lambda2733za7624989z00,
		BGl_z62lambda2733z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4506z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4990za7,
		BGl_z62zc3z04anonymousza33903ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4344z00zzliveness_typesz00,
		BgL_bgl_za762lambda3373za7624991z00,
		BGl_z62lambda3373z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4182z00zzliveness_typesz00,
		BgL_bgl_za762lambda2732za7624992z00,
		BGl_z62lambda2732z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4507z00zzliveness_typesz00,
		BgL_bgl_za762lambda3902za7624993z00,
		BGl_z62lambda3902z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4345z00zzliveness_typesz00,
		BgL_bgl_za762lambda3372za7624994z00,
		BGl_z62lambda3372z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4183z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4995za7,
		BGl_z62zc3z04anonymousza32742ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4508z00zzliveness_typesz00,
		BgL_bgl_za762lambda3901za7624996z00,
		BGl_z62lambda3901z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4346z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4997za7,
		BGl_z62zc3z04anonymousza33385ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4184z00zzliveness_typesz00,
		BgL_bgl_za762lambda2741za7624998z00,
		BGl_z62lambda2741z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4509z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo4999za7,
		BGl_z62zc3z04anonymousza33910ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4347z00zzliveness_typesz00,
		BgL_bgl_za762lambda3384za7625000z00,
		BGl_z62lambda3384z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4185z00zzliveness_typesz00,
		BgL_bgl_za762lambda2740za7625001z00,
		BGl_z62lambda2740z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4348z00zzliveness_typesz00,
		BgL_bgl_za762lambda3383za7625002z00,
		BGl_z62lambda3383z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4186z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5003za7,
		BGl_z62zc3z04anonymousza32750ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4349z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5004za7,
		BGl_z62zc3z04anonymousza33394ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4187z00zzliveness_typesz00,
		BgL_bgl_za762lambda2749za7625005z00,
		BGl_z62lambda2749z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4188z00zzliveness_typesz00,
		BgL_bgl_za762lambda2748za7625006z00,
		BGl_z62lambda2748z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4189z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5007za7,
		BGl_z62zc3z04anonymousza32758ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4510z00zzliveness_typesz00,
		BgL_bgl_za762lambda3909za7625008z00,
		BGl_z62lambda3909z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4511z00zzliveness_typesz00,
		BgL_bgl_za762lambda3908za7625009z00,
		BGl_z62lambda3908z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4512z00zzliveness_typesz00,
		BgL_bgl_za762lambda3879za7625010z00,
		BGl_z62lambda3879z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4350z00zzliveness_typesz00,
		BgL_bgl_za762lambda3393za7625011z00,
		BGl_z62lambda3393z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4513z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5012za7,
		BGl_z62zc3z04anonymousza33878ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4351z00zzliveness_typesz00,
		BgL_bgl_za762lambda3392za7625013z00,
		BGl_z62lambda3392z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4514z00zzliveness_typesz00,
		BgL_bgl_za762lambda3876za7625014z00,
		BGl_z62lambda3876z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4352z00zzliveness_typesz00,
		BgL_bgl_za762lambda3354za7625015z00,
		BGl_z62lambda3354z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4190z00zzliveness_typesz00,
		BgL_bgl_za762lambda2757za7625016z00,
		BGl_z62lambda2757z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4515z00zzliveness_typesz00,
		BgL_bgl_za762lambda3873za7625017z00,
		BGl_z62lambda3873z62zzliveness_typesz00, 0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4353z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5018za7,
		BGl_z62zc3z04anonymousza33353ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4191z00zzliveness_typesz00,
		BgL_bgl_za762lambda2756za7625019z00,
		BGl_z62lambda2756z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4354z00zzliveness_typesz00,
		BgL_bgl_za762lambda3350za7625020z00,
		BGl_z62lambda3350z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4192z00zzliveness_typesz00,
		BgL_bgl_za762lambda2723za7625021z00,
		BGl_z62lambda2723z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4355z00zzliveness_typesz00,
		BgL_bgl_za762lambda3338za7625022z00,
		BGl_z62lambda3338z62zzliveness_typesz00, 0L, BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4193z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5023za7,
		BGl_z62zc3z04anonymousza32722ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4356z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5024za7,
		BGl_z62zc3z04anonymousza33433ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4194z00zzliveness_typesz00,
		BgL_bgl_za762lambda2720za7625025z00,
		BGl_z62lambda2720z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4357z00zzliveness_typesz00,
		BgL_bgl_za762lambda3432za7625026z00,
		BGl_z62lambda3432z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4195z00zzliveness_typesz00,
		BgL_bgl_za762lambda2716za7625027z00,
		BGl_z62lambda2716z62zzliveness_typesz00, 0L, BUNSPEC, 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4358z00zzliveness_typesz00,
		BgL_bgl_za762lambda3431za7625028z00,
		BGl_z62lambda3431z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4196z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5029za7,
		BGl_z62zc3z04anonymousza32787ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4359z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5030za7,
		BGl_z62zc3z04anonymousza33443ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4197z00zzliveness_typesz00,
		BgL_bgl_za762lambda2786za7625031z00,
		BGl_z62lambda2786z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4198z00zzliveness_typesz00,
		BgL_bgl_za762lambda2785za7625032z00,
		BGl_z62lambda2785z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4199z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5033za7,
		BGl_z62zc3z04anonymousza32802ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4360z00zzliveness_typesz00,
		BgL_bgl_za762lambda3442za7625034z00,
		BGl_z62lambda3442z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4361z00zzliveness_typesz00,
		BgL_bgl_za762lambda3441za7625035z00,
		BGl_z62lambda3441z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4362z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5036za7,
		BGl_z62zc3z04anonymousza33452ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4363z00zzliveness_typesz00,
		BgL_bgl_za762lambda3451za7625037z00,
		BGl_z62lambda3451z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4364z00zzliveness_typesz00,
		BgL_bgl_za762lambda3450za7625038z00,
		BGl_z62lambda3450z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4365z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5039za7,
		BGl_z62zc3z04anonymousza33460ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4366z00zzliveness_typesz00,
		BgL_bgl_za762lambda3459za7625040z00,
		BGl_z62lambda3459z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4367z00zzliveness_typesz00,
		BgL_bgl_za762lambda3458za7625041z00,
		BGl_z62lambda3458z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4368z00zzliveness_typesz00,
		BgL_bgl_za762lambda3407za7625042z00,
		BGl_z62lambda3407z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4369z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5043za7,
		BGl_z62zc3z04anonymousza33406ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4370z00zzliveness_typesz00,
		BgL_bgl_za762lambda3404za7625044z00,
		BGl_z62lambda3404z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4371z00zzliveness_typesz00,
		BgL_bgl_za762lambda3401za7625045z00,
		BGl_z62lambda3401z62zzliveness_typesz00, 0L, BUNSPEC, 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4372z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5046za7,
		BGl_z62zc3z04anonymousza33488ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4373z00zzliveness_typesz00,
		BgL_bgl_za762lambda3487za7625047z00,
		BGl_z62lambda3487z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4374z00zzliveness_typesz00,
		BgL_bgl_za762lambda3486za7625048z00,
		BGl_z62lambda3486z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4375z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5049za7,
		BGl_z62zc3z04anonymousza33496ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4376z00zzliveness_typesz00,
		BgL_bgl_za762lambda3495za7625050z00,
		BGl_z62lambda3495z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4377z00zzliveness_typesz00,
		BgL_bgl_za762lambda3494za7625051z00,
		BGl_z62lambda3494z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4378z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5052za7,
		BGl_z62zc3z04anonymousza33507ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4379z00zzliveness_typesz00,
		BgL_bgl_za762lambda3506za7625053z00,
		BGl_z62lambda3506z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4380z00zzliveness_typesz00,
		BgL_bgl_za762lambda3505za7625054z00,
		BGl_z62lambda3505z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4381z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5055za7,
		BGl_z62zc3z04anonymousza33515ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4382z00zzliveness_typesz00,
		BgL_bgl_za762lambda3514za7625056z00,
		BGl_z62lambda3514z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4383z00zzliveness_typesz00,
		BgL_bgl_za762lambda3513za7625057z00,
		BGl_z62lambda3513z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4384z00zzliveness_typesz00,
		BgL_bgl_za762lambda3476za7625058z00,
		BGl_z62lambda3476z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4385z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5059za7,
		BGl_z62zc3z04anonymousza33475ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4386z00zzliveness_typesz00,
		BgL_bgl_za762lambda3472za7625060z00,
		BGl_z62lambda3472z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4387z00zzliveness_typesz00,
		BgL_bgl_za762lambda3469za7625061z00,
		BGl_z62lambda3469z62zzliveness_typesz00, 0L, BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4388z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5062za7,
		BGl_z62zc3z04anonymousza33544ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4389z00zzliveness_typesz00,
		BgL_bgl_za762lambda3543za7625063z00,
		BGl_z62lambda3543z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4390z00zzliveness_typesz00,
		BgL_bgl_za762lambda3542za7625064z00,
		BGl_z62lambda3542z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4391z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5065za7,
		BGl_z62zc3z04anonymousza33552ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4392z00zzliveness_typesz00,
		BgL_bgl_za762lambda3551za7625066z00,
		BGl_z62lambda3551z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4393z00zzliveness_typesz00,
		BgL_bgl_za762lambda3550za7625067z00,
		BGl_z62lambda3550z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4394z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5068za7,
		BGl_z62zc3z04anonymousza33561ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4395z00zzliveness_typesz00,
		BgL_bgl_za762lambda3560za7625069z00,
		BGl_z62lambda3560z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4396z00zzliveness_typesz00,
		BgL_bgl_za762lambda3559za7625070z00,
		BGl_z62lambda3559z62zzliveness_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4397z00zzliveness_typesz00,
		BgL_bgl_za762za7c3za704anonymo5071za7,
		BGl_z62zc3z04anonymousza33570ze3ze5zzliveness_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4398z00zzliveness_typesz00,
		BgL_bgl_za762lambda3569za7625072z00,
		BGl_z62lambda3569z62zzliveness_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4399z00zzliveness_typesz00,
		BgL_bgl_za762lambda3568za7625073z00,
		BGl_z62lambda3568z62zzliveness_typesz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_funcallzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_kwotezf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_wideningzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_localzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_vsetz12zf2livenessze0zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_vlengthzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_newzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_switchzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_valloczf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_returnzf2livenesszf2zzliveness_typesz00));
		   
			 ADD_ROOT((void *) (&BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_castzd2nullzf2livenessz20zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_pragmazf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_synczf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_getfieldzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_setfieldzf2livenesszf2zzliveness_typesz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_letzd2funzf2livenessz20zzliveness_typesz00));
		   
			 ADD_ROOT((void *) (&BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_refzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_literalzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_conditionalzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_instanceofzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_boxzd2refzf2livenessz20zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_letzd2varzf2livenessz20zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_appzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_retblockzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_appzd2lyzf2livenessz20zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_vrefzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_failzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_makezd2boxzf2livenessz20zzliveness_typesz00));
		   
			 ADD_ROOT((void
				*) (&BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_sequencezf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_setqzf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_closurezf2livenesszf2zzliveness_typesz00));
		     ADD_ROOT((void *) (&BGl_castzf2livenesszf2zzliveness_typesz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzliveness_typesz00(long
		BgL_checksumz00_9961, char *BgL_fromz00_9962)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzliveness_typesz00))
				{
					BGl_requirezd2initializa7ationz75zzliveness_typesz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzliveness_typesz00();
					BGl_libraryzd2moduleszd2initz00zzliveness_typesz00();
					BGl_cnstzd2initzd2zzliveness_typesz00();
					BGl_importedzd2moduleszd2initz00zzliveness_typesz00();
					BGl_objectzd2initzd2zzliveness_typesz00();
					return BGl_toplevelzd2initzd2zzliveness_typesz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzliveness_typesz00(void)
	{
		{	/* Liveness/types.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "liveness_types");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "liveness_types");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"liveness_types");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"liveness_types");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"liveness_types");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"liveness_types");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzliveness_typesz00(void)
	{
		{	/* Liveness/types.scm 15 */
			{	/* Liveness/types.scm 15 */
				obj_t BgL_cportz00_8504;

				{	/* Liveness/types.scm 15 */
					obj_t BgL_stringz00_8511;

					BgL_stringz00_8511 = BGl_string4518z00zzliveness_typesz00;
					{	/* Liveness/types.scm 15 */
						obj_t BgL_startz00_8512;

						BgL_startz00_8512 = BINT(0L);
						{	/* Liveness/types.scm 15 */
							obj_t BgL_endz00_8513;

							BgL_endz00_8513 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_8511)));
							{	/* Liveness/types.scm 15 */

								BgL_cportz00_8504 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_8511, BgL_startz00_8512, BgL_endz00_8513);
				}}}}
				{
					long BgL_iz00_8505;

					BgL_iz00_8505 = 43L;
				BgL_loopz00_8506:
					if ((BgL_iz00_8505 == -1L))
						{	/* Liveness/types.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Liveness/types.scm 15 */
							{	/* Liveness/types.scm 15 */
								obj_t BgL_arg4519z00_8507;

								{	/* Liveness/types.scm 15 */

									{	/* Liveness/types.scm 15 */
										obj_t BgL_locationz00_8509;

										BgL_locationz00_8509 = BBOOL(((bool_t) 0));
										{	/* Liveness/types.scm 15 */

											BgL_arg4519z00_8507 =
												BGl_readz00zz__readerz00(BgL_cportz00_8504,
												BgL_locationz00_8509);
										}
									}
								}
								{	/* Liveness/types.scm 15 */
									int BgL_tmpz00_9987;

									BgL_tmpz00_9987 = (int) (BgL_iz00_8505);
									CNST_TABLE_SET(BgL_tmpz00_9987, BgL_arg4519z00_8507);
							}}
							{	/* Liveness/types.scm 15 */
								int BgL_auxz00_8510;

								BgL_auxz00_8510 = (int) ((BgL_iz00_8505 - 1L));
								{
									long BgL_iz00_9992;

									BgL_iz00_9992 = (long) (BgL_auxz00_8510);
									BgL_iz00_8505 = BgL_iz00_9992;
									goto BgL_loopz00_8506;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzliveness_typesz00(void)
	{
		{	/* Liveness/types.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzliveness_typesz00(void)
	{
		{	/* Liveness/types.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzliveness_typesz00(void)
	{
		{	/* Liveness/types.scm 15 */
			{	/* Liveness/types.scm 26 */
				obj_t BgL_arg2042z00_1365;
				obj_t BgL_arg2044z00_1366;

				{	/* Liveness/types.scm 26 */
					obj_t BgL_v1965z00_1401;

					BgL_v1965z00_1401 = create_vector(1L);
					{	/* Liveness/types.scm 26 */
						obj_t BgL_arg2057z00_1402;

						BgL_arg2057z00_1402 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc3967z00zzliveness_typesz00,
							BGl_proc3966z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3965z00zzliveness_typesz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1965z00_1401, 0L, BgL_arg2057z00_1402);
					}
					BgL_arg2042z00_1365 = BgL_v1965z00_1401;
				}
				{	/* Liveness/types.scm 26 */
					obj_t BgL_v1966z00_1415;

					BgL_v1966z00_1415 = create_vector(0L);
					BgL_arg2044z00_1366 = BgL_v1966z00_1415;
				}
				BGl_localzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(2),
					CNST_TABLE_REF(3), BGl_localz00zzast_varz00, 56389L,
					BGl_proc3971z00zzliveness_typesz00,
					BGl_proc3970z00zzliveness_typesz00, BFALSE,
					BGl_proc3969z00zzliveness_typesz00,
					BGl_proc3968z00zzliveness_typesz00, BgL_arg2042z00_1365,
					BgL_arg2044z00_1366);
			}
			{	/* Liveness/types.scm 29 */
				obj_t BgL_arg2069z00_1424;
				obj_t BgL_arg2070z00_1425;

				{	/* Liveness/types.scm 29 */
					obj_t BgL_v1967z00_1453;

					BgL_v1967z00_1453 = create_vector(4L);
					{	/* Liveness/types.scm 29 */
						obj_t BgL_arg2082z00_1454;

						BgL_arg2082z00_1454 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc3974z00zzliveness_typesz00,
							BGl_proc3973z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3972z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1967z00_1453, 0L, BgL_arg2082z00_1454);
					}
					{	/* Liveness/types.scm 29 */
						obj_t BgL_arg2090z00_1467;

						BgL_arg2090z00_1467 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc3977z00zzliveness_typesz00,
							BGl_proc3976z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3975z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1967z00_1453, 1L, BgL_arg2090z00_1467);
					}
					{	/* Liveness/types.scm 29 */
						obj_t BgL_arg2098z00_1480;

						BgL_arg2098z00_1480 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc3980z00zzliveness_typesz00,
							BGl_proc3979z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3978z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1967z00_1453, 2L, BgL_arg2098z00_1480);
					}
					{	/* Liveness/types.scm 29 */
						obj_t BgL_arg2105z00_1493;

						BgL_arg2105z00_1493 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc3983z00zzliveness_typesz00,
							BGl_proc3982z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3981z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1967z00_1453, 3L, BgL_arg2105z00_1493);
					}
					BgL_arg2069z00_1424 = BgL_v1967z00_1453;
				}
				{	/* Liveness/types.scm 29 */
					obj_t BgL_v1968z00_1506;

					BgL_v1968z00_1506 = create_vector(0L);
					BgL_arg2070z00_1425 = BgL_v1968z00_1506;
				}
				BGl_literalzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(9),
					CNST_TABLE_REF(3), BGl_literalz00zzast_nodez00, 57234L,
					BGl_proc3987z00zzliveness_typesz00,
					BGl_proc3986z00zzliveness_typesz00, BFALSE,
					BGl_proc3985z00zzliveness_typesz00,
					BGl_proc3984z00zzliveness_typesz00, BgL_arg2069z00_1424,
					BgL_arg2070z00_1425);
			}
			{	/* Liveness/types.scm 35 */
				obj_t BgL_arg2116z00_1515;
				obj_t BgL_arg2117z00_1516;

				{	/* Liveness/types.scm 35 */
					obj_t BgL_v1969z00_1544;

					BgL_v1969z00_1544 = create_vector(4L);
					{	/* Liveness/types.scm 35 */
						obj_t BgL_arg2129z00_1545;

						BgL_arg2129z00_1545 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc3990z00zzliveness_typesz00,
							BGl_proc3989z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3988z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1969z00_1544, 0L, BgL_arg2129z00_1545);
					}
					{	/* Liveness/types.scm 35 */
						obj_t BgL_arg2136z00_1558;

						BgL_arg2136z00_1558 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc3993z00zzliveness_typesz00,
							BGl_proc3992z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3991z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1969z00_1544, 1L, BgL_arg2136z00_1558);
					}
					{	/* Liveness/types.scm 35 */
						obj_t BgL_arg2143z00_1571;

						BgL_arg2143z00_1571 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc3996z00zzliveness_typesz00,
							BGl_proc3995z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3994z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1969z00_1544, 2L, BgL_arg2143z00_1571);
					}
					{	/* Liveness/types.scm 35 */
						obj_t BgL_arg2150z00_1584;

						BgL_arg2150z00_1584 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc3999z00zzliveness_typesz00,
							BGl_proc3998z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3997z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1969z00_1544, 3L, BgL_arg2150z00_1584);
					}
					BgL_arg2116z00_1515 = BgL_v1969z00_1544;
				}
				{	/* Liveness/types.scm 35 */
					obj_t BgL_v1970z00_1597;

					BgL_v1970z00_1597 = create_vector(0L);
					BgL_arg2117z00_1516 = BgL_v1970z00_1597;
				}
				BGl_refzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(10),
					CNST_TABLE_REF(3), BGl_refz00zzast_nodez00, 10994L,
					BGl_proc4003z00zzliveness_typesz00,
					BGl_proc4002z00zzliveness_typesz00, BFALSE,
					BGl_proc4001z00zzliveness_typesz00,
					BGl_proc4000z00zzliveness_typesz00, BgL_arg2116z00_1515,
					BgL_arg2117z00_1516);
			}
			{	/* Liveness/types.scm 41 */
				obj_t BgL_arg2162z00_1606;
				obj_t BgL_arg2163z00_1607;

				{	/* Liveness/types.scm 41 */
					obj_t BgL_v1971z00_1635;

					BgL_v1971z00_1635 = create_vector(4L);
					{	/* Liveness/types.scm 41 */
						obj_t BgL_arg2174z00_1636;

						BgL_arg2174z00_1636 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4006z00zzliveness_typesz00,
							BGl_proc4005z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4004z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1971z00_1635, 0L, BgL_arg2174z00_1636);
					}
					{	/* Liveness/types.scm 41 */
						obj_t BgL_arg2181z00_1649;

						BgL_arg2181z00_1649 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4009z00zzliveness_typesz00,
							BGl_proc4008z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4007z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1971z00_1635, 1L, BgL_arg2181z00_1649);
					}
					{	/* Liveness/types.scm 41 */
						obj_t BgL_arg2188z00_1662;

						BgL_arg2188z00_1662 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4012z00zzliveness_typesz00,
							BGl_proc4011z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4010z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1971z00_1635, 2L, BgL_arg2188z00_1662);
					}
					{	/* Liveness/types.scm 41 */
						obj_t BgL_arg2196z00_1675;

						BgL_arg2196z00_1675 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4015z00zzliveness_typesz00,
							BGl_proc4014z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4013z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1971z00_1635, 3L, BgL_arg2196z00_1675);
					}
					BgL_arg2162z00_1606 = BgL_v1971z00_1635;
				}
				{	/* Liveness/types.scm 41 */
					obj_t BgL_v1972z00_1688;

					BgL_v1972z00_1688 = create_vector(0L);
					BgL_arg2163z00_1607 = BgL_v1972z00_1688;
				}
				BGl_closurezf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(11),
					CNST_TABLE_REF(3), BGl_closurez00zzast_nodez00, 38002L,
					BGl_proc4019z00zzliveness_typesz00,
					BGl_proc4018z00zzliveness_typesz00, BFALSE,
					BGl_proc4017z00zzliveness_typesz00,
					BGl_proc4016z00zzliveness_typesz00, BgL_arg2162z00_1606,
					BgL_arg2163z00_1607);
			}
			{	/* Liveness/types.scm 47 */
				obj_t BgL_arg2207z00_1697;
				obj_t BgL_arg2208z00_1698;

				{	/* Liveness/types.scm 47 */
					obj_t BgL_v1973z00_1726;

					BgL_v1973z00_1726 = create_vector(4L);
					{	/* Liveness/types.scm 47 */
						obj_t BgL_arg2219z00_1727;

						BgL_arg2219z00_1727 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4022z00zzliveness_typesz00,
							BGl_proc4021z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4020z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1973z00_1726, 0L, BgL_arg2219z00_1727);
					}
					{	/* Liveness/types.scm 47 */
						obj_t BgL_arg2226z00_1740;

						BgL_arg2226z00_1740 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4025z00zzliveness_typesz00,
							BGl_proc4024z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4023z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1973z00_1726, 1L, BgL_arg2226z00_1740);
					}
					{	/* Liveness/types.scm 47 */
						obj_t BgL_arg2233z00_1753;

						BgL_arg2233z00_1753 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4028z00zzliveness_typesz00,
							BGl_proc4027z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4026z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1973z00_1726, 2L, BgL_arg2233z00_1753);
					}
					{	/* Liveness/types.scm 47 */
						obj_t BgL_arg2240z00_1766;

						BgL_arg2240z00_1766 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4031z00zzliveness_typesz00,
							BGl_proc4030z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4029z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1973z00_1726, 3L, BgL_arg2240z00_1766);
					}
					BgL_arg2207z00_1697 = BgL_v1973z00_1726;
				}
				{	/* Liveness/types.scm 47 */
					obj_t BgL_v1974z00_1779;

					BgL_v1974z00_1779 = create_vector(0L);
					BgL_arg2208z00_1698 = BgL_v1974z00_1779;
				}
				BGl_kwotezf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(12),
					CNST_TABLE_REF(3), BGl_kwotez00zzast_nodez00, 1496L,
					BGl_proc4035z00zzliveness_typesz00,
					BGl_proc4034z00zzliveness_typesz00, BFALSE,
					BGl_proc4033z00zzliveness_typesz00,
					BGl_proc4032z00zzliveness_typesz00, BgL_arg2207z00_1697,
					BgL_arg2208z00_1698);
			}
			{	/* Liveness/types.scm 53 */
				obj_t BgL_arg2251z00_1788;
				obj_t BgL_arg2252z00_1789;

				{	/* Liveness/types.scm 53 */
					obj_t BgL_v1975z00_1821;

					BgL_v1975z00_1821 = create_vector(4L);
					{	/* Liveness/types.scm 53 */
						obj_t BgL_arg2263z00_1822;

						BgL_arg2263z00_1822 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4038z00zzliveness_typesz00,
							BGl_proc4037z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4036z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1975z00_1821, 0L, BgL_arg2263z00_1822);
					}
					{	/* Liveness/types.scm 53 */
						obj_t BgL_arg2270z00_1835;

						BgL_arg2270z00_1835 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4041z00zzliveness_typesz00,
							BGl_proc4040z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4039z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1975z00_1821, 1L, BgL_arg2270z00_1835);
					}
					{	/* Liveness/types.scm 53 */
						obj_t BgL_arg2277z00_1848;

						BgL_arg2277z00_1848 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4044z00zzliveness_typesz00,
							BGl_proc4043z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4042z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1975z00_1821, 2L, BgL_arg2277z00_1848);
					}
					{	/* Liveness/types.scm 53 */
						obj_t BgL_arg2286z00_1861;

						BgL_arg2286z00_1861 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4047z00zzliveness_typesz00,
							BGl_proc4046z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4045z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1975z00_1821, 3L, BgL_arg2286z00_1861);
					}
					BgL_arg2251z00_1788 = BgL_v1975z00_1821;
				}
				{	/* Liveness/types.scm 53 */
					obj_t BgL_v1976z00_1874;

					BgL_v1976z00_1874 = create_vector(0L);
					BgL_arg2252z00_1789 = BgL_v1976z00_1874;
				}
				BGl_sequencezf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(13),
					CNST_TABLE_REF(3), BGl_sequencez00zzast_nodez00, 2458L,
					BGl_proc4051z00zzliveness_typesz00,
					BGl_proc4050z00zzliveness_typesz00, BFALSE,
					BGl_proc4049z00zzliveness_typesz00,
					BGl_proc4048z00zzliveness_typesz00, BgL_arg2251z00_1788,
					BgL_arg2252z00_1789);
			}
			{	/* Liveness/types.scm 59 */
				obj_t BgL_arg2297z00_1883;
				obj_t BgL_arg2298z00_1884;

				{	/* Liveness/types.scm 59 */
					obj_t BgL_v1977z00_1916;

					BgL_v1977z00_1916 = create_vector(4L);
					{	/* Liveness/types.scm 59 */
						obj_t BgL_arg2310z00_1917;

						BgL_arg2310z00_1917 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4054z00zzliveness_typesz00,
							BGl_proc4053z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4052z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1977z00_1916, 0L, BgL_arg2310z00_1917);
					}
					{	/* Liveness/types.scm 59 */
						obj_t BgL_arg2317z00_1930;

						BgL_arg2317z00_1930 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4057z00zzliveness_typesz00,
							BGl_proc4056z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4055z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1977z00_1916, 1L, BgL_arg2317z00_1930);
					}
					{	/* Liveness/types.scm 59 */
						obj_t BgL_arg2324z00_1943;

						BgL_arg2324z00_1943 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4060z00zzliveness_typesz00,
							BGl_proc4059z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4058z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1977z00_1916, 2L, BgL_arg2324z00_1943);
					}
					{	/* Liveness/types.scm 59 */
						obj_t BgL_arg2331z00_1956;

						BgL_arg2331z00_1956 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4063z00zzliveness_typesz00,
							BGl_proc4062z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4061z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1977z00_1916, 3L, BgL_arg2331z00_1956);
					}
					BgL_arg2297z00_1883 = BgL_v1977z00_1916;
				}
				{	/* Liveness/types.scm 59 */
					obj_t BgL_v1978z00_1969;

					BgL_v1978z00_1969 = create_vector(0L);
					BgL_arg2298z00_1884 = BgL_v1978z00_1969;
				}
				BGl_appzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(14),
					CNST_TABLE_REF(3), BGl_appz00zzast_nodez00, 35626L,
					BGl_proc4067z00zzliveness_typesz00,
					BGl_proc4066z00zzliveness_typesz00, BFALSE,
					BGl_proc4065z00zzliveness_typesz00,
					BGl_proc4064z00zzliveness_typesz00, BgL_arg2297z00_1883,
					BgL_arg2298z00_1884);
			}
			{	/* Liveness/types.scm 65 */
				obj_t BgL_arg2346z00_1978;
				obj_t BgL_arg2348z00_1979;

				{	/* Liveness/types.scm 65 */
					obj_t BgL_v1979z00_2008;

					BgL_v1979z00_2008 = create_vector(4L);
					{	/* Liveness/types.scm 65 */
						obj_t BgL_arg2361z00_2009;

						BgL_arg2361z00_2009 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4070z00zzliveness_typesz00,
							BGl_proc4069z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4068z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1979z00_2008, 0L, BgL_arg2361z00_2009);
					}
					{	/* Liveness/types.scm 65 */
						obj_t BgL_arg2369z00_2022;

						BgL_arg2369z00_2022 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4073z00zzliveness_typesz00,
							BGl_proc4072z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4071z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1979z00_2008, 1L, BgL_arg2369z00_2022);
					}
					{	/* Liveness/types.scm 65 */
						obj_t BgL_arg2377z00_2035;

						BgL_arg2377z00_2035 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4076z00zzliveness_typesz00,
							BGl_proc4075z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4074z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1979z00_2008, 2L, BgL_arg2377z00_2035);
					}
					{	/* Liveness/types.scm 65 */
						obj_t BgL_arg2384z00_2048;

						BgL_arg2384z00_2048 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4079z00zzliveness_typesz00,
							BGl_proc4078z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4077z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1979z00_2008, 3L, BgL_arg2384z00_2048);
					}
					BgL_arg2346z00_1978 = BgL_v1979z00_2008;
				}
				{	/* Liveness/types.scm 65 */
					obj_t BgL_v1980z00_2061;

					BgL_v1980z00_2061 = create_vector(0L);
					BgL_arg2348z00_1979 = BgL_v1980z00_2061;
				}
				BGl_appzd2lyzf2livenessz20zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(15),
					CNST_TABLE_REF(3), BGl_appzd2lyzd2zzast_nodez00, 48742L,
					BGl_proc4083z00zzliveness_typesz00,
					BGl_proc4082z00zzliveness_typesz00, BFALSE,
					BGl_proc4081z00zzliveness_typesz00,
					BGl_proc4080z00zzliveness_typesz00, BgL_arg2346z00_1978,
					BgL_arg2348z00_1979);
			}
			{	/* Liveness/types.scm 71 */
				obj_t BgL_arg2396z00_2070;
				obj_t BgL_arg2397z00_2071;

				{	/* Liveness/types.scm 71 */
					obj_t BgL_v1981z00_2102;

					BgL_v1981z00_2102 = create_vector(4L);
					{	/* Liveness/types.scm 71 */
						obj_t BgL_arg2411z00_2103;

						BgL_arg2411z00_2103 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4086z00zzliveness_typesz00,
							BGl_proc4085z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4084z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1981z00_2102, 0L, BgL_arg2411z00_2103);
					}
					{	/* Liveness/types.scm 71 */
						obj_t BgL_arg2419z00_2116;

						BgL_arg2419z00_2116 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4089z00zzliveness_typesz00,
							BGl_proc4088z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4087z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1981z00_2102, 1L, BgL_arg2419z00_2116);
					}
					{	/* Liveness/types.scm 71 */
						obj_t BgL_arg2428z00_2129;

						BgL_arg2428z00_2129 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4092z00zzliveness_typesz00,
							BGl_proc4091z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4090z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1981z00_2102, 2L, BgL_arg2428z00_2129);
					}
					{	/* Liveness/types.scm 71 */
						obj_t BgL_arg2435z00_2142;

						BgL_arg2435z00_2142 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4095z00zzliveness_typesz00,
							BGl_proc4094z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4093z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1981z00_2102, 3L, BgL_arg2435z00_2142);
					}
					BgL_arg2396z00_2070 = BgL_v1981z00_2102;
				}
				{	/* Liveness/types.scm 71 */
					obj_t BgL_v1982z00_2155;

					BgL_v1982z00_2155 = create_vector(0L);
					BgL_arg2397z00_2071 = BgL_v1982z00_2155;
				}
				BGl_funcallzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(16),
					CNST_TABLE_REF(3), BGl_funcallz00zzast_nodez00, 34146L,
					BGl_proc4099z00zzliveness_typesz00,
					BGl_proc4098z00zzliveness_typesz00, BFALSE,
					BGl_proc4097z00zzliveness_typesz00,
					BGl_proc4096z00zzliveness_typesz00, BgL_arg2396z00_2070,
					BgL_arg2397z00_2071);
			}
			{	/* Liveness/types.scm 77 */
				obj_t BgL_arg2447z00_2164;
				obj_t BgL_arg2449z00_2165;

				{	/* Liveness/types.scm 77 */
					obj_t BgL_v1983z00_2198;

					BgL_v1983z00_2198 = create_vector(4L);
					{	/* Liveness/types.scm 77 */
						obj_t BgL_arg2461z00_2199;

						BgL_arg2461z00_2199 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4102z00zzliveness_typesz00,
							BGl_proc4101z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4100z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1983z00_2198, 0L, BgL_arg2461z00_2199);
					}
					{	/* Liveness/types.scm 77 */
						obj_t BgL_arg2469z00_2212;

						BgL_arg2469z00_2212 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4105z00zzliveness_typesz00,
							BGl_proc4104z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4103z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1983z00_2198, 1L, BgL_arg2469z00_2212);
					}
					{	/* Liveness/types.scm 77 */
						obj_t BgL_arg2479z00_2225;

						BgL_arg2479z00_2225 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4108z00zzliveness_typesz00,
							BGl_proc4107z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4106z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1983z00_2198, 2L, BgL_arg2479z00_2225);
					}
					{	/* Liveness/types.scm 77 */
						obj_t BgL_arg2486z00_2238;

						BgL_arg2486z00_2238 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4111z00zzliveness_typesz00,
							BGl_proc4110z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4109z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1983z00_2198, 3L, BgL_arg2486z00_2238);
					}
					BgL_arg2447z00_2164 = BgL_v1983z00_2198;
				}
				{	/* Liveness/types.scm 77 */
					obj_t BgL_v1984z00_2251;

					BgL_v1984z00_2251 = create_vector(0L);
					BgL_arg2449z00_2165 = BgL_v1984z00_2251;
				}
				BGl_pragmazf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(17),
					CNST_TABLE_REF(3), BGl_pragmaz00zzast_nodez00, 24636L,
					BGl_proc4115z00zzliveness_typesz00,
					BGl_proc4114z00zzliveness_typesz00, BFALSE,
					BGl_proc4113z00zzliveness_typesz00,
					BGl_proc4112z00zzliveness_typesz00, BgL_arg2447z00_2164,
					BgL_arg2449z00_2165);
			}
			{	/* Liveness/types.scm 83 */
				obj_t BgL_arg2502z00_2260;
				obj_t BgL_arg2503z00_2261;

				{	/* Liveness/types.scm 83 */
					obj_t BgL_v1985z00_2296;

					BgL_v1985z00_2296 = create_vector(4L);
					{	/* Liveness/types.scm 83 */
						obj_t BgL_arg2514z00_2297;

						BgL_arg2514z00_2297 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4118z00zzliveness_typesz00,
							BGl_proc4117z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4116z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1985z00_2296, 0L, BgL_arg2514z00_2297);
					}
					{	/* Liveness/types.scm 83 */
						obj_t BgL_arg2524z00_2310;

						BgL_arg2524z00_2310 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4121z00zzliveness_typesz00,
							BGl_proc4120z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4119z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1985z00_2296, 1L, BgL_arg2524z00_2310);
					}
					{	/* Liveness/types.scm 83 */
						obj_t BgL_arg2534z00_2323;

						BgL_arg2534z00_2323 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4124z00zzliveness_typesz00,
							BGl_proc4123z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4122z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1985z00_2296, 2L, BgL_arg2534z00_2323);
					}
					{	/* Liveness/types.scm 83 */
						obj_t BgL_arg2542z00_2336;

						BgL_arg2542z00_2336 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4127z00zzliveness_typesz00,
							BGl_proc4126z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4125z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1985z00_2296, 3L, BgL_arg2542z00_2336);
					}
					BgL_arg2502z00_2260 = BgL_v1985z00_2296;
				}
				{	/* Liveness/types.scm 83 */
					obj_t BgL_v1986z00_2349;

					BgL_v1986z00_2349 = create_vector(0L);
					BgL_arg2503z00_2261 = BgL_v1986z00_2349;
				}
				BGl_getfieldzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(18),
					CNST_TABLE_REF(3), BGl_getfieldz00zzast_nodez00, 35620L,
					BGl_proc4131z00zzliveness_typesz00,
					BGl_proc4130z00zzliveness_typesz00, BFALSE,
					BGl_proc4129z00zzliveness_typesz00,
					BGl_proc4128z00zzliveness_typesz00, BgL_arg2502z00_2260,
					BgL_arg2503z00_2261);
			}
			{	/* Liveness/types.scm 89 */
				obj_t BgL_arg2555z00_2358;
				obj_t BgL_arg2556z00_2359;

				{	/* Liveness/types.scm 89 */
					obj_t BgL_v1987z00_2394;

					BgL_v1987z00_2394 = create_vector(4L);
					{	/* Liveness/types.scm 89 */
						obj_t BgL_arg2572z00_2395;

						BgL_arg2572z00_2395 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4134z00zzliveness_typesz00,
							BGl_proc4133z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4132z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1987z00_2394, 0L, BgL_arg2572z00_2395);
					}
					{	/* Liveness/types.scm 89 */
						obj_t BgL_arg2589z00_2408;

						BgL_arg2589z00_2408 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4137z00zzliveness_typesz00,
							BGl_proc4136z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4135z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1987z00_2394, 1L, BgL_arg2589z00_2408);
					}
					{	/* Liveness/types.scm 89 */
						obj_t BgL_arg2596z00_2421;

						BgL_arg2596z00_2421 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4140z00zzliveness_typesz00,
							BGl_proc4139z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4138z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1987z00_2394, 2L, BgL_arg2596z00_2421);
					}
					{	/* Liveness/types.scm 89 */
						obj_t BgL_arg2604z00_2434;

						BgL_arg2604z00_2434 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4143z00zzliveness_typesz00,
							BGl_proc4142z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4141z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1987z00_2394, 3L, BgL_arg2604z00_2434);
					}
					BgL_arg2555z00_2358 = BgL_v1987z00_2394;
				}
				{	/* Liveness/types.scm 89 */
					obj_t BgL_v1988z00_2447;

					BgL_v1988z00_2447 = create_vector(0L);
					BgL_arg2556z00_2359 = BgL_v1988z00_2447;
				}
				BGl_setfieldzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(19),
					CNST_TABLE_REF(3), BGl_setfieldz00zzast_nodez00, 38284L,
					BGl_proc4147z00zzliveness_typesz00,
					BGl_proc4146z00zzliveness_typesz00, BFALSE,
					BGl_proc4145z00zzliveness_typesz00,
					BGl_proc4144z00zzliveness_typesz00, BgL_arg2555z00_2358,
					BgL_arg2556z00_2359);
			}
			{	/* Liveness/types.scm 95 */
				obj_t BgL_arg2617z00_2456;
				obj_t BgL_arg2618z00_2457;

				{	/* Liveness/types.scm 95 */
					obj_t BgL_v1989z00_2490;

					BgL_v1989z00_2490 = create_vector(4L);
					{	/* Liveness/types.scm 95 */
						obj_t BgL_arg2629z00_2491;

						BgL_arg2629z00_2491 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4150z00zzliveness_typesz00,
							BGl_proc4149z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4148z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1989z00_2490, 0L, BgL_arg2629z00_2491);
					}
					{	/* Liveness/types.scm 95 */
						obj_t BgL_arg2636z00_2504;

						BgL_arg2636z00_2504 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4153z00zzliveness_typesz00,
							BGl_proc4152z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4151z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1989z00_2490, 1L, BgL_arg2636z00_2504);
					}
					{	/* Liveness/types.scm 95 */
						obj_t BgL_arg2643z00_2517;

						BgL_arg2643z00_2517 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4156z00zzliveness_typesz00,
							BGl_proc4155z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4154z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1989z00_2490, 2L, BgL_arg2643z00_2517);
					}
					{	/* Liveness/types.scm 95 */
						obj_t BgL_arg2651z00_2530;

						BgL_arg2651z00_2530 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4159z00zzliveness_typesz00,
							BGl_proc4158z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4157z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1989z00_2490, 3L, BgL_arg2651z00_2530);
					}
					BgL_arg2617z00_2456 = BgL_v1989z00_2490;
				}
				{	/* Liveness/types.scm 95 */
					obj_t BgL_v1990z00_2543;

					BgL_v1990z00_2543 = create_vector(0L);
					BgL_arg2618z00_2457 = BgL_v1990z00_2543;
				}
				BGl_wideningzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(20),
					CNST_TABLE_REF(3), BGl_wideningz00zzast_nodez00, 57314L,
					BGl_proc4163z00zzliveness_typesz00,
					BGl_proc4162z00zzliveness_typesz00, BFALSE,
					BGl_proc4161z00zzliveness_typesz00,
					BGl_proc4160z00zzliveness_typesz00, BgL_arg2617z00_2456,
					BgL_arg2618z00_2457);
			}
			{	/* Liveness/types.scm 101 */
				obj_t BgL_arg2666z00_2552;
				obj_t BgL_arg2667z00_2553;

				{	/* Liveness/types.scm 101 */
					obj_t BgL_v1991z00_2586;

					BgL_v1991z00_2586 = create_vector(4L);
					{	/* Liveness/types.scm 101 */
						obj_t BgL_arg2680z00_2587;

						BgL_arg2680z00_2587 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4166z00zzliveness_typesz00,
							BGl_proc4165z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4164z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1991z00_2586, 0L, BgL_arg2680z00_2587);
					}
					{	/* Liveness/types.scm 101 */
						obj_t BgL_arg2688z00_2600;

						BgL_arg2688z00_2600 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4169z00zzliveness_typesz00,
							BGl_proc4168z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4167z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1991z00_2586, 1L, BgL_arg2688z00_2600);
					}
					{	/* Liveness/types.scm 101 */
						obj_t BgL_arg2695z00_2613;

						BgL_arg2695z00_2613 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4172z00zzliveness_typesz00,
							BGl_proc4171z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4170z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1991z00_2586, 2L, BgL_arg2695z00_2613);
					}
					{	/* Liveness/types.scm 101 */
						obj_t BgL_arg2702z00_2626;

						BgL_arg2702z00_2626 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4175z00zzliveness_typesz00,
							BGl_proc4174z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4173z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1991z00_2586, 3L, BgL_arg2702z00_2626);
					}
					BgL_arg2666z00_2552 = BgL_v1991z00_2586;
				}
				{	/* Liveness/types.scm 101 */
					obj_t BgL_v1992z00_2639;

					BgL_v1992z00_2639 = create_vector(0L);
					BgL_arg2667z00_2553 = BgL_v1992z00_2639;
				}
				BGl_newzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(21),
					CNST_TABLE_REF(3), BGl_newz00zzast_nodez00, 39832L,
					BGl_proc4179z00zzliveness_typesz00,
					BGl_proc4178z00zzliveness_typesz00, BFALSE,
					BGl_proc4177z00zzliveness_typesz00,
					BGl_proc4176z00zzliveness_typesz00, BgL_arg2666z00_2552,
					BgL_arg2667z00_2553);
			}
			{	/* Liveness/types.scm 107 */
				obj_t BgL_arg2714z00_2648;
				obj_t BgL_arg2715z00_2649;

				{	/* Liveness/types.scm 107 */
					obj_t BgL_v1993z00_2683;

					BgL_v1993z00_2683 = create_vector(4L);
					{	/* Liveness/types.scm 107 */
						obj_t BgL_arg2727z00_2684;

						BgL_arg2727z00_2684 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4182z00zzliveness_typesz00,
							BGl_proc4181z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4180z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1993z00_2683, 0L, BgL_arg2727z00_2684);
					}
					{	/* Liveness/types.scm 107 */
						obj_t BgL_arg2736z00_2697;

						BgL_arg2736z00_2697 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4185z00zzliveness_typesz00,
							BGl_proc4184z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4183z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1993z00_2683, 1L, BgL_arg2736z00_2697);
					}
					{	/* Liveness/types.scm 107 */
						obj_t BgL_arg2743z00_2710;

						BgL_arg2743z00_2710 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4188z00zzliveness_typesz00,
							BGl_proc4187z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4186z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1993z00_2683, 2L, BgL_arg2743z00_2710);
					}
					{	/* Liveness/types.scm 107 */
						obj_t BgL_arg2751z00_2723;

						BgL_arg2751z00_2723 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4191z00zzliveness_typesz00,
							BGl_proc4190z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4189z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1993z00_2683, 3L, BgL_arg2751z00_2723);
					}
					BgL_arg2714z00_2648 = BgL_v1993z00_2683;
				}
				{	/* Liveness/types.scm 107 */
					obj_t BgL_v1994z00_2736;

					BgL_v1994z00_2736 = create_vector(0L);
					BgL_arg2715z00_2649 = BgL_v1994z00_2736;
				}
				BGl_valloczf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(22),
					CNST_TABLE_REF(3), BGl_vallocz00zzast_nodez00, 37322L,
					BGl_proc4195z00zzliveness_typesz00,
					BGl_proc4194z00zzliveness_typesz00, BFALSE,
					BGl_proc4193z00zzliveness_typesz00,
					BGl_proc4192z00zzliveness_typesz00, BgL_arg2714z00_2648,
					BgL_arg2715z00_2649);
			}
			{	/* Liveness/types.scm 113 */
				obj_t BgL_arg2764z00_2745;
				obj_t BgL_arg2765z00_2746;

				{	/* Liveness/types.scm 113 */
					obj_t BgL_v1995z00_2782;

					BgL_v1995z00_2782 = create_vector(4L);
					{	/* Liveness/types.scm 113 */
						obj_t BgL_arg2780z00_2783;

						BgL_arg2780z00_2783 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4198z00zzliveness_typesz00,
							BGl_proc4197z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4196z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1995z00_2782, 0L, BgL_arg2780z00_2783);
					}
					{	/* Liveness/types.scm 113 */
						obj_t BgL_arg2789z00_2796;

						BgL_arg2789z00_2796 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4201z00zzliveness_typesz00,
							BGl_proc4200z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4199z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1995z00_2782, 1L, BgL_arg2789z00_2796);
					}
					{	/* Liveness/types.scm 113 */
						obj_t BgL_arg2804z00_2809;

						BgL_arg2804z00_2809 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4204z00zzliveness_typesz00,
							BGl_proc4203z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4202z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1995z00_2782, 2L, BgL_arg2804z00_2809);
					}
					{	/* Liveness/types.scm 113 */
						obj_t BgL_arg2815z00_2822;

						BgL_arg2815z00_2822 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4207z00zzliveness_typesz00,
							BGl_proc4206z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4205z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1995z00_2782, 3L, BgL_arg2815z00_2822);
					}
					BgL_arg2764z00_2745 = BgL_v1995z00_2782;
				}
				{	/* Liveness/types.scm 113 */
					obj_t BgL_v1996z00_2835;

					BgL_v1996z00_2835 = create_vector(0L);
					BgL_arg2765z00_2746 = BgL_v1996z00_2835;
				}
				BGl_vrefzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(23),
					CNST_TABLE_REF(3), BGl_vrefz00zzast_nodez00, 20454L,
					BGl_proc4211z00zzliveness_typesz00,
					BGl_proc4210z00zzliveness_typesz00, BFALSE,
					BGl_proc4209z00zzliveness_typesz00,
					BGl_proc4208z00zzliveness_typesz00, BgL_arg2764z00_2745,
					BgL_arg2765z00_2746);
			}
			{	/* Liveness/types.scm 119 */
				obj_t BgL_arg2829z00_2844;
				obj_t BgL_arg2830z00_2845;

				{	/* Liveness/types.scm 119 */
					obj_t BgL_v1997z00_2881;

					BgL_v1997z00_2881 = create_vector(4L);
					{	/* Liveness/types.scm 119 */
						obj_t BgL_arg2846z00_2882;

						BgL_arg2846z00_2882 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4214z00zzliveness_typesz00,
							BGl_proc4213z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4212z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1997z00_2881, 0L, BgL_arg2846z00_2882);
					}
					{	/* Liveness/types.scm 119 */
						obj_t BgL_arg2864z00_2895;

						BgL_arg2864z00_2895 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4217z00zzliveness_typesz00,
							BGl_proc4216z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4215z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1997z00_2881, 1L, BgL_arg2864z00_2895);
					}
					{	/* Liveness/types.scm 119 */
						obj_t BgL_arg2876z00_2908;

						BgL_arg2876z00_2908 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4220z00zzliveness_typesz00,
							BGl_proc4219z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4218z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1997z00_2881, 2L, BgL_arg2876z00_2908);
					}
					{	/* Liveness/types.scm 119 */
						obj_t BgL_arg2883z00_2921;

						BgL_arg2883z00_2921 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4223z00zzliveness_typesz00,
							BGl_proc4222z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4221z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1997z00_2881, 3L, BgL_arg2883z00_2921);
					}
					BgL_arg2829z00_2844 = BgL_v1997z00_2881;
				}
				{	/* Liveness/types.scm 119 */
					obj_t BgL_v1998z00_2934;

					BgL_v1998z00_2934 = create_vector(0L);
					BgL_arg2830z00_2845 = BgL_v1998z00_2934;
				}
				BGl_vsetz12zf2livenessze0zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(24),
					CNST_TABLE_REF(3), BGl_vsetz12z12zzast_nodez00, 29030L,
					BGl_proc4227z00zzliveness_typesz00,
					BGl_proc4226z00zzliveness_typesz00, BFALSE,
					BGl_proc4225z00zzliveness_typesz00,
					BGl_proc4224z00zzliveness_typesz00, BgL_arg2829z00_2844,
					BgL_arg2830z00_2845);
			}
			{	/* Liveness/types.scm 125 */
				obj_t BgL_arg2897z00_2943;
				obj_t BgL_arg2898z00_2944;

				{	/* Liveness/types.scm 125 */
					obj_t BgL_v1999z00_2978;

					BgL_v1999z00_2978 = create_vector(4L);
					{	/* Liveness/types.scm 125 */
						obj_t BgL_arg2915z00_2979;

						BgL_arg2915z00_2979 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4230z00zzliveness_typesz00,
							BGl_proc4229z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4228z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1999z00_2978, 0L, BgL_arg2915z00_2979);
					}
					{	/* Liveness/types.scm 125 */
						obj_t BgL_arg2923z00_2992;

						BgL_arg2923z00_2992 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4233z00zzliveness_typesz00,
							BGl_proc4232z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4231z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1999z00_2978, 1L, BgL_arg2923z00_2992);
					}
					{	/* Liveness/types.scm 125 */
						obj_t BgL_arg2930z00_3005;

						BgL_arg2930z00_3005 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4236z00zzliveness_typesz00,
							BGl_proc4235z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4234z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1999z00_2978, 2L, BgL_arg2930z00_3005);
					}
					{	/* Liveness/types.scm 125 */
						obj_t BgL_arg2940z00_3018;

						BgL_arg2940z00_3018 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4239z00zzliveness_typesz00,
							BGl_proc4238z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4237z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1999z00_2978, 3L, BgL_arg2940z00_3018);
					}
					BgL_arg2897z00_2943 = BgL_v1999z00_2978;
				}
				{	/* Liveness/types.scm 125 */
					obj_t BgL_v2000z00_3031;

					BgL_v2000z00_3031 = create_vector(0L);
					BgL_arg2898z00_2944 = BgL_v2000z00_3031;
				}
				BGl_vlengthzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(25),
					CNST_TABLE_REF(3), BGl_vlengthz00zzast_nodez00, 34172L,
					BGl_proc4243z00zzliveness_typesz00,
					BGl_proc4242z00zzliveness_typesz00, BFALSE,
					BGl_proc4241z00zzliveness_typesz00,
					BGl_proc4240z00zzliveness_typesz00, BgL_arg2897z00_2943,
					BgL_arg2898z00_2944);
			}
			{	/* Liveness/types.scm 131 */
				obj_t BgL_arg2967z00_3040;
				obj_t BgL_arg2968z00_3041;

				{	/* Liveness/types.scm 131 */
					obj_t BgL_v2001z00_3074;

					BgL_v2001z00_3074 = create_vector(4L);
					{	/* Liveness/types.scm 131 */
						obj_t BgL_arg2981z00_3075;

						BgL_arg2981z00_3075 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4246z00zzliveness_typesz00,
							BGl_proc4245z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4244z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2001z00_3074, 0L, BgL_arg2981z00_3075);
					}
					{	/* Liveness/types.scm 131 */
						obj_t BgL_arg2991z00_3088;

						BgL_arg2991z00_3088 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4249z00zzliveness_typesz00,
							BGl_proc4248z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4247z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2001z00_3074, 1L, BgL_arg2991z00_3088);
					}
					{	/* Liveness/types.scm 131 */
						obj_t BgL_arg2999z00_3101;

						BgL_arg2999z00_3101 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4252z00zzliveness_typesz00,
							BGl_proc4251z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4250z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2001z00_3074, 2L, BgL_arg2999z00_3101);
					}
					{	/* Liveness/types.scm 131 */
						obj_t BgL_arg3008z00_3114;

						BgL_arg3008z00_3114 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4255z00zzliveness_typesz00,
							BGl_proc4254z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4253z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2001z00_3074, 3L, BgL_arg3008z00_3114);
					}
					BgL_arg2967z00_3040 = BgL_v2001z00_3074;
				}
				{	/* Liveness/types.scm 131 */
					obj_t BgL_v2002z00_3127;

					BgL_v2002z00_3127 = create_vector(0L);
					BgL_arg2968z00_3041 = BgL_v2002z00_3127;
				}
				BGl_instanceofzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(26),
					CNST_TABLE_REF(3), BGl_instanceofz00zzast_nodez00, 47960L,
					BGl_proc4259z00zzliveness_typesz00,
					BGl_proc4258z00zzliveness_typesz00, BFALSE,
					BGl_proc4257z00zzliveness_typesz00,
					BGl_proc4256z00zzliveness_typesz00, BgL_arg2967z00_3040,
					BgL_arg2968z00_3041);
			}
			{	/* Liveness/types.scm 137 */
				obj_t BgL_arg3019z00_3136;
				obj_t BgL_arg3020z00_3137;

				{	/* Liveness/types.scm 137 */
					obj_t BgL_v2003z00_3169;

					BgL_v2003z00_3169 = create_vector(4L);
					{	/* Liveness/types.scm 137 */
						obj_t BgL_arg3032z00_3170;

						BgL_arg3032z00_3170 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4262z00zzliveness_typesz00,
							BGl_proc4261z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4260z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2003z00_3169, 0L, BgL_arg3032z00_3170);
					}
					{	/* Liveness/types.scm 137 */
						obj_t BgL_arg3049z00_3183;

						BgL_arg3049z00_3183 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4265z00zzliveness_typesz00,
							BGl_proc4264z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4263z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2003z00_3169, 1L, BgL_arg3049z00_3183);
					}
					{	/* Liveness/types.scm 137 */
						obj_t BgL_arg3059z00_3196;

						BgL_arg3059z00_3196 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4268z00zzliveness_typesz00,
							BGl_proc4267z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4266z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2003z00_3169, 2L, BgL_arg3059z00_3196);
					}
					{	/* Liveness/types.scm 137 */
						obj_t BgL_arg3070z00_3209;

						BgL_arg3070z00_3209 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4271z00zzliveness_typesz00,
							BGl_proc4270z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4269z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2003z00_3169, 3L, BgL_arg3070z00_3209);
					}
					BgL_arg3019z00_3136 = BgL_v2003z00_3169;
				}
				{	/* Liveness/types.scm 137 */
					obj_t BgL_v2004z00_3222;

					BgL_v2004z00_3222 = create_vector(0L);
					BgL_arg3020z00_3137 = BgL_v2004z00_3222;
				}
				BGl_castzd2nullzf2livenessz20zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(27),
					CNST_TABLE_REF(3), BGl_castzd2nullzd2zzast_nodez00, 45190L,
					BGl_proc4275z00zzliveness_typesz00,
					BGl_proc4274z00zzliveness_typesz00, BFALSE,
					BGl_proc4273z00zzliveness_typesz00,
					BGl_proc4272z00zzliveness_typesz00, BgL_arg3019z00_3136,
					BgL_arg3020z00_3137);
			}
			{	/* Liveness/types.scm 143 */
				obj_t BgL_arg3081z00_3231;
				obj_t BgL_arg3082z00_3232;

				{	/* Liveness/types.scm 143 */
					obj_t BgL_v2005z00_3260;

					BgL_v2005z00_3260 = create_vector(4L);
					{	/* Liveness/types.scm 143 */
						obj_t BgL_arg3094z00_3261;

						BgL_arg3094z00_3261 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4278z00zzliveness_typesz00,
							BGl_proc4277z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4276z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2005z00_3260, 0L, BgL_arg3094z00_3261);
					}
					{	/* Liveness/types.scm 143 */
						obj_t BgL_arg3114z00_3274;

						BgL_arg3114z00_3274 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4281z00zzliveness_typesz00,
							BGl_proc4280z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4279z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2005z00_3260, 1L, BgL_arg3114z00_3274);
					}
					{	/* Liveness/types.scm 143 */
						obj_t BgL_arg3122z00_3287;

						BgL_arg3122z00_3287 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4284z00zzliveness_typesz00,
							BGl_proc4283z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4282z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2005z00_3260, 2L, BgL_arg3122z00_3287);
					}
					{	/* Liveness/types.scm 143 */
						obj_t BgL_arg3131z00_3300;

						BgL_arg3131z00_3300 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4287z00zzliveness_typesz00,
							BGl_proc4286z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4285z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2005z00_3260, 3L, BgL_arg3131z00_3300);
					}
					BgL_arg3081z00_3231 = BgL_v2005z00_3260;
				}
				{	/* Liveness/types.scm 143 */
					obj_t BgL_v2006z00_3313;

					BgL_v2006z00_3313 = create_vector(0L);
					BgL_arg3082z00_3232 = BgL_v2006z00_3313;
				}
				BGl_castzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(28),
					CNST_TABLE_REF(3), BGl_castz00zzast_nodez00, 27062L,
					BGl_proc4291z00zzliveness_typesz00,
					BGl_proc4290z00zzliveness_typesz00, BFALSE,
					BGl_proc4289z00zzliveness_typesz00,
					BGl_proc4288z00zzliveness_typesz00, BgL_arg3081z00_3231,
					BgL_arg3082z00_3232);
			}
			{	/* Liveness/types.scm 149 */
				obj_t BgL_arg3145z00_3322;
				obj_t BgL_arg3146z00_3323;

				{	/* Liveness/types.scm 149 */
					obj_t BgL_v2007z00_3352;

					BgL_v2007z00_3352 = create_vector(4L);
					{	/* Liveness/types.scm 149 */
						obj_t BgL_arg3163z00_3353;

						BgL_arg3163z00_3353 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4294z00zzliveness_typesz00,
							BGl_proc4293z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4292z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2007z00_3352, 0L, BgL_arg3163z00_3353);
					}
					{	/* Liveness/types.scm 149 */
						obj_t BgL_arg3178z00_3366;

						BgL_arg3178z00_3366 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4297z00zzliveness_typesz00,
							BGl_proc4296z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4295z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2007z00_3352, 1L, BgL_arg3178z00_3366);
					}
					{	/* Liveness/types.scm 149 */
						obj_t BgL_arg3186z00_3379;

						BgL_arg3186z00_3379 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4300z00zzliveness_typesz00,
							BGl_proc4299z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4298z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2007z00_3352, 2L, BgL_arg3186z00_3379);
					}
					{	/* Liveness/types.scm 149 */
						obj_t BgL_arg3195z00_3392;

						BgL_arg3195z00_3392 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4303z00zzliveness_typesz00,
							BGl_proc4302z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4301z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2007z00_3352, 3L, BgL_arg3195z00_3392);
					}
					BgL_arg3145z00_3322 = BgL_v2007z00_3352;
				}
				{	/* Liveness/types.scm 149 */
					obj_t BgL_v2008z00_3405;

					BgL_v2008z00_3405 = create_vector(0L);
					BgL_arg3146z00_3323 = BgL_v2008z00_3405;
				}
				BGl_setqzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(29),
					CNST_TABLE_REF(3), BGl_setqz00zzast_nodez00, 21906L,
					BGl_proc4307z00zzliveness_typesz00,
					BGl_proc4306z00zzliveness_typesz00, BFALSE,
					BGl_proc4305z00zzliveness_typesz00,
					BGl_proc4304z00zzliveness_typesz00, BgL_arg3145z00_3322,
					BgL_arg3146z00_3323);
			}
			{	/* Liveness/types.scm 155 */
				obj_t BgL_arg3211z00_3414;
				obj_t BgL_arg3212z00_3415;

				{	/* Liveness/types.scm 155 */
					obj_t BgL_v2009z00_3447;

					BgL_v2009z00_3447 = create_vector(4L);
					{	/* Liveness/types.scm 155 */
						obj_t BgL_arg3230z00_3448;

						BgL_arg3230z00_3448 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4310z00zzliveness_typesz00,
							BGl_proc4309z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4308z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2009z00_3447, 0L, BgL_arg3230z00_3448);
					}
					{	/* Liveness/types.scm 155 */
						obj_t BgL_arg3239z00_3461;

						BgL_arg3239z00_3461 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4313z00zzliveness_typesz00,
							BGl_proc4312z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4311z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2009z00_3447, 1L, BgL_arg3239z00_3461);
					}
					{	/* Liveness/types.scm 155 */
						obj_t BgL_arg3246z00_3474;

						BgL_arg3246z00_3474 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4316z00zzliveness_typesz00,
							BGl_proc4315z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4314z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2009z00_3447, 2L, BgL_arg3246z00_3474);
					}
					{	/* Liveness/types.scm 155 */
						obj_t BgL_arg3254z00_3487;

						BgL_arg3254z00_3487 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4319z00zzliveness_typesz00,
							BGl_proc4318z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4317z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2009z00_3447, 3L, BgL_arg3254z00_3487);
					}
					BgL_arg3211z00_3414 = BgL_v2009z00_3447;
				}
				{	/* Liveness/types.scm 155 */
					obj_t BgL_v2010z00_3500;

					BgL_v2010z00_3500 = create_vector(0L);
					BgL_arg3212z00_3415 = BgL_v2010z00_3500;
				}
				BGl_conditionalzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(30),
					CNST_TABLE_REF(3), BGl_conditionalz00zzast_nodez00, 50052L,
					BGl_proc4323z00zzliveness_typesz00,
					BGl_proc4322z00zzliveness_typesz00, BFALSE,
					BGl_proc4321z00zzliveness_typesz00,
					BGl_proc4320z00zzliveness_typesz00, BgL_arg3211z00_3414,
					BgL_arg3212z00_3415);
			}
			{	/* Liveness/types.scm 161 */
				obj_t BgL_arg3268z00_3509;
				obj_t BgL_arg3269z00_3510;

				{	/* Liveness/types.scm 161 */
					obj_t BgL_v2011z00_3540;

					BgL_v2011z00_3540 = create_vector(4L);
					{	/* Liveness/types.scm 161 */
						obj_t BgL_arg3288z00_3541;

						BgL_arg3288z00_3541 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4326z00zzliveness_typesz00,
							BGl_proc4325z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4324z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2011z00_3540, 0L, BgL_arg3288z00_3541);
					}
					{	/* Liveness/types.scm 161 */
						obj_t BgL_arg3298z00_3554;

						BgL_arg3298z00_3554 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4329z00zzliveness_typesz00,
							BGl_proc4328z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4327z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2011z00_3540, 1L, BgL_arg3298z00_3554);
					}
					{	/* Liveness/types.scm 161 */
						obj_t BgL_arg3306z00_3567;

						BgL_arg3306z00_3567 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4332z00zzliveness_typesz00,
							BGl_proc4331z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4330z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2011z00_3540, 2L, BgL_arg3306z00_3567);
					}
					{	/* Liveness/types.scm 161 */
						obj_t BgL_arg3313z00_3580;

						BgL_arg3313z00_3580 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4335z00zzliveness_typesz00,
							BGl_proc4334z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4333z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2011z00_3540, 3L, BgL_arg3313z00_3580);
					}
					BgL_arg3268z00_3509 = BgL_v2011z00_3540;
				}
				{	/* Liveness/types.scm 161 */
					obj_t BgL_v2012z00_3593;

					BgL_v2012z00_3593 = create_vector(0L);
					BgL_arg3269z00_3510 = BgL_v2012z00_3593;
				}
				BGl_failzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(31),
					CNST_TABLE_REF(3), BGl_failz00zzast_nodez00, 46132L,
					BGl_proc4339z00zzliveness_typesz00,
					BGl_proc4338z00zzliveness_typesz00, BFALSE,
					BGl_proc4337z00zzliveness_typesz00,
					BGl_proc4336z00zzliveness_typesz00, BgL_arg3268z00_3509,
					BgL_arg3269z00_3510);
			}
			{	/* Liveness/types.scm 167 */
				obj_t BgL_arg3327z00_3602;
				obj_t BgL_arg3337z00_3603;

				{	/* Liveness/types.scm 167 */
					obj_t BgL_v2013z00_3635;

					BgL_v2013z00_3635 = create_vector(4L);
					{	/* Liveness/types.scm 167 */
						obj_t BgL_arg3359z00_3636;

						BgL_arg3359z00_3636 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4342z00zzliveness_typesz00,
							BGl_proc4341z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4340z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2013z00_3635, 0L, BgL_arg3359z00_3636);
					}
					{	/* Liveness/types.scm 167 */
						obj_t BgL_arg3367z00_3649;

						BgL_arg3367z00_3649 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4345z00zzliveness_typesz00,
							BGl_proc4344z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4343z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2013z00_3635, 1L, BgL_arg3367z00_3649);
					}
					{	/* Liveness/types.scm 167 */
						obj_t BgL_arg3375z00_3662;

						BgL_arg3375z00_3662 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4348z00zzliveness_typesz00,
							BGl_proc4347z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4346z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2013z00_3635, 2L, BgL_arg3375z00_3662);
					}
					{	/* Liveness/types.scm 167 */
						obj_t BgL_arg3386z00_3675;

						BgL_arg3386z00_3675 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4351z00zzliveness_typesz00,
							BGl_proc4350z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4349z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2013z00_3635, 3L, BgL_arg3386z00_3675);
					}
					BgL_arg3327z00_3602 = BgL_v2013z00_3635;
				}
				{	/* Liveness/types.scm 167 */
					obj_t BgL_v2014z00_3688;

					BgL_v2014z00_3688 = create_vector(0L);
					BgL_arg3337z00_3603 = BgL_v2014z00_3688;
				}
				BGl_switchzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(32),
					CNST_TABLE_REF(3), BGl_switchz00zzast_nodez00, 26120L,
					BGl_proc4355z00zzliveness_typesz00,
					BGl_proc4354z00zzliveness_typesz00, BFALSE,
					BGl_proc4353z00zzliveness_typesz00,
					BGl_proc4352z00zzliveness_typesz00, BgL_arg3327z00_3602,
					BgL_arg3337z00_3603);
			}
			{	/* Liveness/types.scm 173 */
				obj_t BgL_arg3399z00_3697;
				obj_t BgL_arg3400z00_3698;

				{	/* Liveness/types.scm 173 */
					obj_t BgL_v2015z00_3729;

					BgL_v2015z00_3729 = create_vector(4L);
					{	/* Liveness/types.scm 173 */
						obj_t BgL_arg3422z00_3730;

						BgL_arg3422z00_3730 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4358z00zzliveness_typesz00,
							BGl_proc4357z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4356z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2015z00_3729, 0L, BgL_arg3422z00_3730);
					}
					{	/* Liveness/types.scm 173 */
						obj_t BgL_arg3434z00_3743;

						BgL_arg3434z00_3743 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4361z00zzliveness_typesz00,
							BGl_proc4360z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4359z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2015z00_3729, 1L, BgL_arg3434z00_3743);
					}
					{	/* Liveness/types.scm 173 */
						obj_t BgL_arg3445z00_3756;

						BgL_arg3445z00_3756 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4364z00zzliveness_typesz00,
							BGl_proc4363z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4362z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2015z00_3729, 2L, BgL_arg3445z00_3756);
					}
					{	/* Liveness/types.scm 173 */
						obj_t BgL_arg3453z00_3769;

						BgL_arg3453z00_3769 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4367z00zzliveness_typesz00,
							BGl_proc4366z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4365z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2015z00_3729, 3L, BgL_arg3453z00_3769);
					}
					BgL_arg3399z00_3697 = BgL_v2015z00_3729;
				}
				{	/* Liveness/types.scm 173 */
					obj_t BgL_v2016z00_3782;

					BgL_v2016z00_3782 = create_vector(0L);
					BgL_arg3400z00_3698 = BgL_v2016z00_3782;
				}
				BGl_letzd2funzf2livenessz20zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(33),
					CNST_TABLE_REF(3), BGl_letzd2funzd2zzast_nodez00, 50934L,
					BGl_proc4371z00zzliveness_typesz00,
					BGl_proc4370z00zzliveness_typesz00, BFALSE,
					BGl_proc4369z00zzliveness_typesz00,
					BGl_proc4368z00zzliveness_typesz00, BgL_arg3399z00_3697,
					BgL_arg3400z00_3698);
			}
			{	/* Liveness/types.scm 179 */
				obj_t BgL_arg3466z00_3791;
				obj_t BgL_arg3468z00_3792;

				{	/* Liveness/types.scm 179 */
					obj_t BgL_v2017z00_3824;

					BgL_v2017z00_3824 = create_vector(4L);
					{	/* Liveness/types.scm 179 */
						obj_t BgL_arg3481z00_3825;

						BgL_arg3481z00_3825 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4374z00zzliveness_typesz00,
							BGl_proc4373z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4372z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2017z00_3824, 0L, BgL_arg3481z00_3825);
					}
					{	/* Liveness/types.scm 179 */
						obj_t BgL_arg3489z00_3838;

						BgL_arg3489z00_3838 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4377z00zzliveness_typesz00,
							BGl_proc4376z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4375z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2017z00_3824, 1L, BgL_arg3489z00_3838);
					}
					{	/* Liveness/types.scm 179 */
						obj_t BgL_arg3500z00_3851;

						BgL_arg3500z00_3851 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4380z00zzliveness_typesz00,
							BGl_proc4379z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4378z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2017z00_3824, 2L, BgL_arg3500z00_3851);
					}
					{	/* Liveness/types.scm 179 */
						obj_t BgL_arg3508z00_3864;

						BgL_arg3508z00_3864 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4383z00zzliveness_typesz00,
							BGl_proc4382z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4381z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2017z00_3824, 3L, BgL_arg3508z00_3864);
					}
					BgL_arg3466z00_3791 = BgL_v2017z00_3824;
				}
				{	/* Liveness/types.scm 179 */
					obj_t BgL_v2018z00_3877;

					BgL_v2018z00_3877 = create_vector(0L);
					BgL_arg3468z00_3792 = BgL_v2018z00_3877;
				}
				BGl_letzd2varzf2livenessz20zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(34),
					CNST_TABLE_REF(3), BGl_letzd2varzd2zzast_nodez00, 57846L,
					BGl_proc4387z00zzliveness_typesz00,
					BGl_proc4386z00zzliveness_typesz00, BFALSE,
					BGl_proc4385z00zzliveness_typesz00,
					BGl_proc4384z00zzliveness_typesz00, BgL_arg3466z00_3791,
					BgL_arg3468z00_3792);
			}
			{	/* Liveness/types.scm 185 */
				obj_t BgL_arg3522z00_3886;
				obj_t BgL_arg3523z00_3887;

				{	/* Liveness/types.scm 185 */
					obj_t BgL_v2019z00_3917;

					BgL_v2019z00_3917 = create_vector(4L);
					{	/* Liveness/types.scm 185 */
						obj_t BgL_arg3538z00_3918;

						BgL_arg3538z00_3918 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4390z00zzliveness_typesz00,
							BGl_proc4389z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4388z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2019z00_3917, 0L, BgL_arg3538z00_3918);
					}
					{	/* Liveness/types.scm 185 */
						obj_t BgL_arg3545z00_3931;

						BgL_arg3545z00_3931 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4393z00zzliveness_typesz00,
							BGl_proc4392z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4391z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2019z00_3917, 1L, BgL_arg3545z00_3931);
					}
					{	/* Liveness/types.scm 185 */
						obj_t BgL_arg3553z00_3944;

						BgL_arg3553z00_3944 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4396z00zzliveness_typesz00,
							BGl_proc4395z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4394z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2019z00_3917, 2L, BgL_arg3553z00_3944);
					}
					{	/* Liveness/types.scm 185 */
						obj_t BgL_arg3563z00_3957;

						BgL_arg3563z00_3957 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4399z00zzliveness_typesz00,
							BGl_proc4398z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4397z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2019z00_3917, 3L, BgL_arg3563z00_3957);
					}
					BgL_arg3522z00_3886 = BgL_v2019z00_3917;
				}
				{	/* Liveness/types.scm 185 */
					obj_t BgL_v2020z00_3970;

					BgL_v2020z00_3970 = create_vector(0L);
					BgL_arg3523z00_3887 = BgL_v2020z00_3970;
				}
				BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(35),
					CNST_TABLE_REF(3), BGl_setzd2exzd2itz00zzast_nodez00, 31340L,
					BGl_proc4403z00zzliveness_typesz00,
					BGl_proc4402z00zzliveness_typesz00, BFALSE,
					BGl_proc4401z00zzliveness_typesz00,
					BGl_proc4400z00zzliveness_typesz00, BgL_arg3522z00_3886,
					BgL_arg3523z00_3887);
			}
			{	/* Liveness/types.scm 191 */
				obj_t BgL_arg3576z00_3979;
				obj_t BgL_arg3577z00_3980;

				{	/* Liveness/types.scm 191 */
					obj_t BgL_v2021z00_4009;

					BgL_v2021z00_4009 = create_vector(4L);
					{	/* Liveness/types.scm 191 */
						obj_t BgL_arg3592z00_4010;

						BgL_arg3592z00_4010 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4406z00zzliveness_typesz00,
							BGl_proc4405z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4404z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2021z00_4009, 0L, BgL_arg3592z00_4010);
					}
					{	/* Liveness/types.scm 191 */
						obj_t BgL_arg3600z00_4023;

						BgL_arg3600z00_4023 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4409z00zzliveness_typesz00,
							BGl_proc4408z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4407z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2021z00_4009, 1L, BgL_arg3600z00_4023);
					}
					{	/* Liveness/types.scm 191 */
						obj_t BgL_arg3609z00_4036;

						BgL_arg3609z00_4036 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4412z00zzliveness_typesz00,
							BGl_proc4411z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4410z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2021z00_4009, 2L, BgL_arg3609z00_4036);
					}
					{	/* Liveness/types.scm 191 */
						obj_t BgL_arg3616z00_4049;

						BgL_arg3616z00_4049 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4415z00zzliveness_typesz00,
							BGl_proc4414z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4413z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2021z00_4009, 3L, BgL_arg3616z00_4049);
					}
					BgL_arg3576z00_3979 = BgL_v2021z00_4009;
				}
				{	/* Liveness/types.scm 191 */
					obj_t BgL_v2022z00_4062;

					BgL_v2022z00_4062 = create_vector(0L);
					BgL_arg3577z00_3980 = BgL_v2022z00_4062;
				}
				BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(36),
					CNST_TABLE_REF(3), BGl_jumpzd2exzd2itz00zzast_nodez00, 38476L,
					BGl_proc4419z00zzliveness_typesz00,
					BGl_proc4418z00zzliveness_typesz00, BFALSE,
					BGl_proc4417z00zzliveness_typesz00,
					BGl_proc4416z00zzliveness_typesz00, BgL_arg3576z00_3979,
					BgL_arg3577z00_3980);
			}
			{	/* Liveness/types.scm 197 */
				obj_t BgL_arg3630z00_4071;
				obj_t BgL_arg3631z00_4072;

				{	/* Liveness/types.scm 197 */
					obj_t BgL_v2023z00_4102;

					BgL_v2023z00_4102 = create_vector(4L);
					{	/* Liveness/types.scm 197 */
						obj_t BgL_arg3644z00_4103;

						BgL_arg3644z00_4103 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4422z00zzliveness_typesz00,
							BGl_proc4421z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4420z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2023z00_4102, 0L, BgL_arg3644z00_4103);
					}
					{	/* Liveness/types.scm 197 */
						obj_t BgL_arg3653z00_4116;

						BgL_arg3653z00_4116 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4425z00zzliveness_typesz00,
							BGl_proc4424z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4423z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2023z00_4102, 1L, BgL_arg3653z00_4116);
					}
					{	/* Liveness/types.scm 197 */
						obj_t BgL_arg3660z00_4129;

						BgL_arg3660z00_4129 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4428z00zzliveness_typesz00,
							BGl_proc4427z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4426z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2023z00_4102, 2L, BgL_arg3660z00_4129);
					}
					{	/* Liveness/types.scm 197 */
						obj_t BgL_arg3668z00_4142;

						BgL_arg3668z00_4142 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4431z00zzliveness_typesz00,
							BGl_proc4430z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4429z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2023z00_4102, 3L, BgL_arg3668z00_4142);
					}
					BgL_arg3630z00_4071 = BgL_v2023z00_4102;
				}
				{	/* Liveness/types.scm 197 */
					obj_t BgL_v2024z00_4155;

					BgL_v2024z00_4155 = create_vector(0L);
					BgL_arg3631z00_4072 = BgL_v2024z00_4155;
				}
				BGl_synczf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(37),
					CNST_TABLE_REF(3), BGl_syncz00zzast_nodez00, 46354L,
					BGl_proc4435z00zzliveness_typesz00,
					BGl_proc4434z00zzliveness_typesz00, BFALSE,
					BGl_proc4433z00zzliveness_typesz00,
					BGl_proc4432z00zzliveness_typesz00, BgL_arg3630z00_4071,
					BgL_arg3631z00_4072);
			}
			{	/* Liveness/types.scm 203 */
				obj_t BgL_arg3683z00_4164;
				obj_t BgL_arg3684z00_4165;

				{	/* Liveness/types.scm 203 */
					obj_t BgL_v2025z00_4193;

					BgL_v2025z00_4193 = create_vector(4L);
					{	/* Liveness/types.scm 203 */
						obj_t BgL_arg3696z00_4194;

						BgL_arg3696z00_4194 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4438z00zzliveness_typesz00,
							BGl_proc4437z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4436z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2025z00_4193, 0L, BgL_arg3696z00_4194);
					}
					{	/* Liveness/types.scm 203 */
						obj_t BgL_arg3704z00_4207;

						BgL_arg3704z00_4207 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4441z00zzliveness_typesz00,
							BGl_proc4440z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4439z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2025z00_4193, 1L, BgL_arg3704z00_4207);
					}
					{	/* Liveness/types.scm 203 */
						obj_t BgL_arg3713z00_4220;

						BgL_arg3713z00_4220 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4444z00zzliveness_typesz00,
							BGl_proc4443z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4442z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2025z00_4193, 2L, BgL_arg3713z00_4220);
					}
					{	/* Liveness/types.scm 203 */
						obj_t BgL_arg3720z00_4233;

						BgL_arg3720z00_4233 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4447z00zzliveness_typesz00,
							BGl_proc4446z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4445z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2025z00_4193, 3L, BgL_arg3720z00_4233);
					}
					BgL_arg3683z00_4164 = BgL_v2025z00_4193;
				}
				{	/* Liveness/types.scm 203 */
					obj_t BgL_v2026z00_4246;

					BgL_v2026z00_4246 = create_vector(0L);
					BgL_arg3684z00_4165 = BgL_v2026z00_4246;
				}
				BGl_retblockzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(38),
					CNST_TABLE_REF(3), BGl_retblockz00zzast_nodez00, 46592L,
					BGl_proc4451z00zzliveness_typesz00,
					BGl_proc4450z00zzliveness_typesz00, BFALSE,
					BGl_proc4449z00zzliveness_typesz00,
					BGl_proc4448z00zzliveness_typesz00, BgL_arg3683z00_4164,
					BgL_arg3684z00_4165);
			}
			{	/* Liveness/types.scm 209 */
				obj_t BgL_arg3731z00_4255;
				obj_t BgL_arg3733z00_4256;

				{	/* Liveness/types.scm 209 */
					obj_t BgL_v2027z00_4285;

					BgL_v2027z00_4285 = create_vector(4L);
					{	/* Liveness/types.scm 209 */
						obj_t BgL_arg3747z00_4286;

						BgL_arg3747z00_4286 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4454z00zzliveness_typesz00,
							BGl_proc4453z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4452z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2027z00_4285, 0L, BgL_arg3747z00_4286);
					}
					{	/* Liveness/types.scm 209 */
						obj_t BgL_arg3755z00_4299;

						BgL_arg3755z00_4299 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4457z00zzliveness_typesz00,
							BGl_proc4456z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4455z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2027z00_4285, 1L, BgL_arg3755z00_4299);
					}
					{	/* Liveness/types.scm 209 */
						obj_t BgL_arg3763z00_4312;

						BgL_arg3763z00_4312 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4460z00zzliveness_typesz00,
							BGl_proc4459z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4458z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2027z00_4285, 2L, BgL_arg3763z00_4312);
					}
					{	/* Liveness/types.scm 209 */
						obj_t BgL_arg3771z00_4325;

						BgL_arg3771z00_4325 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4463z00zzliveness_typesz00,
							BGl_proc4462z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4461z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2027z00_4285, 3L, BgL_arg3771z00_4325);
					}
					BgL_arg3731z00_4255 = BgL_v2027z00_4285;
				}
				{	/* Liveness/types.scm 209 */
					obj_t BgL_v2028z00_4338;

					BgL_v2028z00_4338 = create_vector(0L);
					BgL_arg3733z00_4256 = BgL_v2028z00_4338;
				}
				BGl_returnzf2livenesszf2zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(39),
					CNST_TABLE_REF(3), BGl_returnz00zzast_nodez00, 31084L,
					BGl_proc4467z00zzliveness_typesz00,
					BGl_proc4466z00zzliveness_typesz00, BFALSE,
					BGl_proc4465z00zzliveness_typesz00,
					BGl_proc4464z00zzliveness_typesz00, BgL_arg3731z00_4255,
					BgL_arg3733z00_4256);
			}
			{	/* Liveness/types.scm 215 */
				obj_t BgL_arg3783z00_4347;
				obj_t BgL_arg3784z00_4348;

				{	/* Liveness/types.scm 215 */
					obj_t BgL_v2029z00_4380;

					BgL_v2029z00_4380 = create_vector(4L);
					{	/* Liveness/types.scm 215 */
						obj_t BgL_arg3795z00_4381;

						BgL_arg3795z00_4381 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4470z00zzliveness_typesz00,
							BGl_proc4469z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4468z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2029z00_4380, 0L, BgL_arg3795z00_4381);
					}
					{	/* Liveness/types.scm 215 */
						obj_t BgL_arg3802z00_4394;

						BgL_arg3802z00_4394 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4473z00zzliveness_typesz00,
							BGl_proc4472z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4471z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2029z00_4380, 1L, BgL_arg3802z00_4394);
					}
					{	/* Liveness/types.scm 215 */
						obj_t BgL_arg3809z00_4407;

						BgL_arg3809z00_4407 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4476z00zzliveness_typesz00,
							BGl_proc4475z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4474z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2029z00_4380, 2L, BgL_arg3809z00_4407);
					}
					{	/* Liveness/types.scm 215 */
						obj_t BgL_arg3816z00_4420;

						BgL_arg3816z00_4420 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4479z00zzliveness_typesz00,
							BGl_proc4478z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4477z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2029z00_4380, 3L, BgL_arg3816z00_4420);
					}
					BgL_arg3783z00_4347 = BgL_v2029z00_4380;
				}
				{	/* Liveness/types.scm 215 */
					obj_t BgL_v2030z00_4433;

					BgL_v2030z00_4433 = create_vector(0L);
					BgL_arg3784z00_4348 = BgL_v2030z00_4433;
				}
				BGl_makezd2boxzf2livenessz20zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(40),
					CNST_TABLE_REF(3), BGl_makezd2boxzd2zzast_nodez00, 8964L,
					BGl_proc4483z00zzliveness_typesz00,
					BGl_proc4482z00zzliveness_typesz00, BFALSE,
					BGl_proc4481z00zzliveness_typesz00,
					BGl_proc4480z00zzliveness_typesz00, BgL_arg3783z00_4347,
					BgL_arg3784z00_4348);
			}
			{	/* Liveness/types.scm 221 */
				obj_t BgL_arg3827z00_4442;
				obj_t BgL_arg3828z00_4443;

				{	/* Liveness/types.scm 221 */
					obj_t BgL_v2031z00_4474;

					BgL_v2031z00_4474 = create_vector(4L);
					{	/* Liveness/types.scm 221 */
						obj_t BgL_arg3839z00_4475;

						BgL_arg3839z00_4475 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4486z00zzliveness_typesz00,
							BGl_proc4485z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4484z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2031z00_4474, 0L, BgL_arg3839z00_4475);
					}
					{	/* Liveness/types.scm 221 */
						obj_t BgL_arg3846z00_4488;

						BgL_arg3846z00_4488 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4489z00zzliveness_typesz00,
							BGl_proc4488z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4487z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2031z00_4474, 1L, BgL_arg3846z00_4488);
					}
					{	/* Liveness/types.scm 221 */
						obj_t BgL_arg3853z00_4501;

						BgL_arg3853z00_4501 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4492z00zzliveness_typesz00,
							BGl_proc4491z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4490z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2031z00_4474, 2L, BgL_arg3853z00_4501);
					}
					{	/* Liveness/types.scm 221 */
						obj_t BgL_arg3860z00_4514;

						BgL_arg3860z00_4514 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4495z00zzliveness_typesz00,
							BGl_proc4494z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4493z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2031z00_4474, 3L, BgL_arg3860z00_4514);
					}
					BgL_arg3827z00_4442 = BgL_v2031z00_4474;
				}
				{	/* Liveness/types.scm 221 */
					obj_t BgL_v2032z00_4527;

					BgL_v2032z00_4527 = create_vector(0L);
					BgL_arg3828z00_4443 = BgL_v2032z00_4527;
				}
				BGl_boxzd2refzf2livenessz20zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(41),
					CNST_TABLE_REF(3), BGl_boxzd2refzd2zzast_nodez00, 38662L,
					BGl_proc4499z00zzliveness_typesz00,
					BGl_proc4498z00zzliveness_typesz00, BFALSE,
					BGl_proc4497z00zzliveness_typesz00,
					BGl_proc4496z00zzliveness_typesz00, BgL_arg3827z00_4442,
					BgL_arg3828z00_4443);
			}
			{	/* Liveness/types.scm 227 */
				obj_t BgL_arg3871z00_4536;
				obj_t BgL_arg3872z00_4537;

				{	/* Liveness/types.scm 227 */
					obj_t BgL_v2033z00_4567;

					BgL_v2033z00_4567 = create_vector(4L);
					{	/* Liveness/types.scm 227 */
						obj_t BgL_arg3883z00_4568;

						BgL_arg3883z00_4568 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc4502z00zzliveness_typesz00,
							BGl_proc4501z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4500z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2033z00_4567, 0L, BgL_arg3883z00_4568);
					}
					{	/* Liveness/types.scm 227 */
						obj_t BgL_arg3890z00_4581;

						BgL_arg3890z00_4581 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc4505z00zzliveness_typesz00,
							BGl_proc4504z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4503z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2033z00_4567, 1L, BgL_arg3890z00_4581);
					}
					{	/* Liveness/types.scm 227 */
						obj_t BgL_arg3897z00_4594;

						BgL_arg3897z00_4594 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc4508z00zzliveness_typesz00,
							BGl_proc4507z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4506z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2033z00_4567, 2L, BgL_arg3897z00_4594);
					}
					{	/* Liveness/types.scm 227 */
						obj_t BgL_arg3904z00_4607;

						BgL_arg3904z00_4607 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc4511z00zzliveness_typesz00,
							BGl_proc4510z00zzliveness_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc4509z00zzliveness_typesz00, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v2033z00_4567, 3L, BgL_arg3904z00_4607);
					}
					BgL_arg3871z00_4536 = BgL_v2033z00_4567;
				}
				{	/* Liveness/types.scm 227 */
					obj_t BgL_v2034z00_4620;

					BgL_v2034z00_4620 = create_vector(0L);
					BgL_arg3872z00_4537 = BgL_v2034z00_4620;
				}
				return (BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(42),
						CNST_TABLE_REF(3), BGl_boxzd2setz12zc0zzast_nodez00, 24710L,
						BGl_proc4515z00zzliveness_typesz00,
						BGl_proc4514z00zzliveness_typesz00, BFALSE,
						BGl_proc4513z00zzliveness_typesz00,
						BGl_proc4512z00zzliveness_typesz00, BgL_arg3871z00_4536,
						BgL_arg3872z00_4537), BUNSPEC);
			}
		}

	}



/* &lambda3879 */
	BgL_boxzd2setz12zc0_bglt BGl_z62lambda3879z62zzliveness_typesz00(obj_t
		BgL_envz00_7074, obj_t BgL_o1681z00_7075)
	{
		{	/* Liveness/types.scm 227 */
			{	/* Liveness/types.scm 227 */
				long BgL_arg3880z00_8516;

				{	/* Liveness/types.scm 227 */
					obj_t BgL_arg3881z00_8517;

					{	/* Liveness/types.scm 227 */
						obj_t BgL_arg3882z00_8518;

						{	/* Liveness/types.scm 227 */
							obj_t BgL_arg1815z00_8519;
							long BgL_arg1816z00_8520;

							BgL_arg1815z00_8519 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 227 */
								long BgL_arg1817z00_8521;

								BgL_arg1817z00_8521 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_o1681z00_7075)));
								BgL_arg1816z00_8520 = (BgL_arg1817z00_8521 - OBJECT_TYPE);
							}
							BgL_arg3882z00_8518 =
								VECTOR_REF(BgL_arg1815z00_8519, BgL_arg1816z00_8520);
						}
						BgL_arg3881z00_8517 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3882z00_8518);
					}
					{	/* Liveness/types.scm 227 */
						obj_t BgL_tmpz00_10725;

						BgL_tmpz00_10725 = ((obj_t) BgL_arg3881z00_8517);
						BgL_arg3880z00_8516 = BGL_CLASS_NUM(BgL_tmpz00_10725);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_boxzd2setz12zc0_bglt) BgL_o1681z00_7075)),
					BgL_arg3880z00_8516);
			}
			{	/* Liveness/types.scm 227 */
				BgL_objectz00_bglt BgL_tmpz00_10731;

				BgL_tmpz00_10731 =
					((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_o1681z00_7075));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10731, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_o1681z00_7075));
			return
				((BgL_boxzd2setz12zc0_bglt)
				((BgL_boxzd2setz12zc0_bglt) BgL_o1681z00_7075));
		}

	}



/* &<@anonymous:3878> */
	obj_t BGl_z62zc3z04anonymousza33878ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7076, obj_t BgL_new1680z00_7077)
	{
		{	/* Liveness/types.scm 227 */
			{
				BgL_boxzd2setz12zc0_bglt BgL_auxz00_10739;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10743;

					{	/* Liveness/types.scm 227 */
						obj_t BgL_classz00_8523;

						BgL_classz00_8523 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 227 */
							obj_t BgL__ortest_1117z00_8524;

							BgL__ortest_1117z00_8524 = BGL_CLASS_NIL(BgL_classz00_8523);
							if (CBOOL(BgL__ortest_1117z00_8524))
								{	/* Liveness/types.scm 227 */
									BgL_auxz00_10743 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8524);
								}
							else
								{	/* Liveness/types.scm 227 */
									BgL_auxz00_10743 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8523));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10743), BUNSPEC);
				}
				{
					BgL_varz00_bglt BgL_auxz00_10753;

					{	/* Liveness/types.scm 227 */
						obj_t BgL_classz00_8525;

						BgL_classz00_8525 = BGl_varz00zzast_nodez00;
						{	/* Liveness/types.scm 227 */
							obj_t BgL__ortest_1117z00_8526;

							BgL__ortest_1117z00_8526 = BGL_CLASS_NIL(BgL_classz00_8525);
							if (CBOOL(BgL__ortest_1117z00_8526))
								{	/* Liveness/types.scm 227 */
									BgL_auxz00_10753 =
										((BgL_varz00_bglt) BgL__ortest_1117z00_8526);
								}
							else
								{	/* Liveness/types.scm 227 */
									BgL_auxz00_10753 =
										((BgL_varz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8525));
								}
						}
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077))))->
							BgL_varz00) = ((BgL_varz00_bglt) BgL_auxz00_10753), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_10763;

					{	/* Liveness/types.scm 227 */
						obj_t BgL_classz00_8527;

						BgL_classz00_8527 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 227 */
							obj_t BgL__ortest_1117z00_8528;

							BgL__ortest_1117z00_8528 = BGL_CLASS_NIL(BgL_classz00_8527);
							if (CBOOL(BgL__ortest_1117z00_8528))
								{	/* Liveness/types.scm 227 */
									BgL_auxz00_10763 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8528);
								}
							else
								{	/* Liveness/types.scm 227 */
									BgL_auxz00_10763 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8527));
								}
						}
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077))))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_10763), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_10773;

					{	/* Liveness/types.scm 227 */
						obj_t BgL_classz00_8529;

						BgL_classz00_8529 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 227 */
							obj_t BgL__ortest_1117z00_8530;

							BgL__ortest_1117z00_8530 = BGL_CLASS_NIL(BgL_classz00_8529);
							if (CBOOL(BgL__ortest_1117z00_8530))
								{	/* Liveness/types.scm 227 */
									BgL_auxz00_10773 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8530);
								}
							else
								{	/* Liveness/types.scm 227 */
									BgL_auxz00_10773 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8529));
								}
						}
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077))))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_10773), BUNSPEC);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10783;

					{
						obj_t BgL_auxz00_10784;

						{	/* Liveness/types.scm 227 */
							BgL_objectz00_bglt BgL_tmpz00_10785;

							BgL_tmpz00_10785 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077));
							BgL_auxz00_10784 = BGL_OBJECT_WIDENING(BgL_tmpz00_10785);
						}
						BgL_auxz00_10783 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10784);
					}
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10783))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10791;

					{
						obj_t BgL_auxz00_10792;

						{	/* Liveness/types.scm 227 */
							BgL_objectz00_bglt BgL_tmpz00_10793;

							BgL_tmpz00_10793 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077));
							BgL_auxz00_10792 = BGL_OBJECT_WIDENING(BgL_tmpz00_10793);
						}
						BgL_auxz00_10791 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10792);
					}
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10791))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10799;

					{
						obj_t BgL_auxz00_10800;

						{	/* Liveness/types.scm 227 */
							BgL_objectz00_bglt BgL_tmpz00_10801;

							BgL_tmpz00_10801 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077));
							BgL_auxz00_10800 = BGL_OBJECT_WIDENING(BgL_tmpz00_10801);
						}
						BgL_auxz00_10799 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10800);
					}
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10799))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10807;

					{
						obj_t BgL_auxz00_10808;

						{	/* Liveness/types.scm 227 */
							BgL_objectz00_bglt BgL_tmpz00_10809;

							BgL_tmpz00_10809 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077));
							BgL_auxz00_10808 = BGL_OBJECT_WIDENING(BgL_tmpz00_10809);
						}
						BgL_auxz00_10807 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10808);
					}
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10807))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_10739 = ((BgL_boxzd2setz12zc0_bglt) BgL_new1680z00_7077);
				return ((obj_t) BgL_auxz00_10739);
			}
		}

	}



/* &lambda3876 */
	BgL_boxzd2setz12zc0_bglt BGl_z62lambda3876z62zzliveness_typesz00(obj_t
		BgL_envz00_7078, obj_t BgL_o1677z00_7079)
	{
		{	/* Liveness/types.scm 227 */
			{	/* Liveness/types.scm 227 */
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_wide1679z00_8532;

				BgL_wide1679z00_8532 =
					((BgL_boxzd2setz12zf2livenessz32_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_boxzd2setz12zf2livenessz32_bgl))));
				{	/* Liveness/types.scm 227 */
					obj_t BgL_auxz00_10822;
					BgL_objectz00_bglt BgL_tmpz00_10818;

					BgL_auxz00_10822 = ((obj_t) BgL_wide1679z00_8532);
					BgL_tmpz00_10818 =
						((BgL_objectz00_bglt)
						((BgL_boxzd2setz12zc0_bglt)
							((BgL_boxzd2setz12zc0_bglt) BgL_o1677z00_7079)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10818, BgL_auxz00_10822);
				}
				((BgL_objectz00_bglt)
					((BgL_boxzd2setz12zc0_bglt)
						((BgL_boxzd2setz12zc0_bglt) BgL_o1677z00_7079)));
				{	/* Liveness/types.scm 227 */
					long BgL_arg3877z00_8533;

					BgL_arg3877z00_8533 =
						BGL_CLASS_NUM(BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_boxzd2setz12zc0_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_o1677z00_7079))),
						BgL_arg3877z00_8533);
				}
				return
					((BgL_boxzd2setz12zc0_bglt)
					((BgL_boxzd2setz12zc0_bglt)
						((BgL_boxzd2setz12zc0_bglt) BgL_o1677z00_7079)));
			}
		}

	}



/* &lambda3873 */
	BgL_boxzd2setz12zc0_bglt BGl_z62lambda3873z62zzliveness_typesz00(obj_t
		BgL_envz00_7080, obj_t BgL_loc1668z00_7081, obj_t BgL_type1669z00_7082,
		obj_t BgL_var1670z00_7083, obj_t BgL_value1671z00_7084,
		obj_t BgL_vtype1672z00_7085, obj_t BgL_def1673z00_7086,
		obj_t BgL_use1674z00_7087, obj_t BgL_in1675z00_7088,
		obj_t BgL_out1676z00_7089)
	{
		{	/* Liveness/types.scm 227 */
			{	/* Liveness/types.scm 227 */
				BgL_boxzd2setz12zc0_bglt BgL_new1855z00_8542;

				{	/* Liveness/types.scm 227 */
					BgL_boxzd2setz12zc0_bglt BgL_tmp1853z00_8543;
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_wide1854z00_8544;

					{
						BgL_boxzd2setz12zc0_bglt BgL_auxz00_10836;

						{	/* Liveness/types.scm 227 */
							BgL_boxzd2setz12zc0_bglt BgL_new1852z00_8545;

							BgL_new1852z00_8545 =
								((BgL_boxzd2setz12zc0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_boxzd2setz12zc0_bgl))));
							{	/* Liveness/types.scm 227 */
								long BgL_arg3875z00_8546;

								BgL_arg3875z00_8546 =
									BGL_CLASS_NUM(BGl_boxzd2setz12zc0zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1852z00_8545),
									BgL_arg3875z00_8546);
							}
							{	/* Liveness/types.scm 227 */
								BgL_objectz00_bglt BgL_tmpz00_10841;

								BgL_tmpz00_10841 = ((BgL_objectz00_bglt) BgL_new1852z00_8545);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10841, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1852z00_8545);
							BgL_auxz00_10836 = BgL_new1852z00_8545;
						}
						BgL_tmp1853z00_8543 = ((BgL_boxzd2setz12zc0_bglt) BgL_auxz00_10836);
					}
					BgL_wide1854z00_8544 =
						((BgL_boxzd2setz12zf2livenessz32_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_boxzd2setz12zf2livenessz32_bgl))));
					{	/* Liveness/types.scm 227 */
						obj_t BgL_auxz00_10849;
						BgL_objectz00_bglt BgL_tmpz00_10847;

						BgL_auxz00_10849 = ((obj_t) BgL_wide1854z00_8544);
						BgL_tmpz00_10847 = ((BgL_objectz00_bglt) BgL_tmp1853z00_8543);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10847, BgL_auxz00_10849);
					}
					((BgL_objectz00_bglt) BgL_tmp1853z00_8543);
					{	/* Liveness/types.scm 227 */
						long BgL_arg3874z00_8547;

						BgL_arg3874z00_8547 =
							BGL_CLASS_NUM(BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1853z00_8543), BgL_arg3874z00_8547);
					}
					BgL_new1855z00_8542 =
						((BgL_boxzd2setz12zc0_bglt) BgL_tmp1853z00_8543);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1855z00_8542)))->BgL_locz00) =
					((obj_t) BgL_loc1668z00_7081), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1855z00_8542)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1669z00_7082)),
					BUNSPEC);
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_new1855z00_8542)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_var1670z00_7083)), BUNSPEC);
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_new1855z00_8542)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_value1671z00_7084)),
					BUNSPEC);
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_new1855z00_8542)))->BgL_vtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1672z00_7085)),
					BUNSPEC);
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10871;

					{
						obj_t BgL_auxz00_10872;

						{	/* Liveness/types.scm 227 */
							BgL_objectz00_bglt BgL_tmpz00_10873;

							BgL_tmpz00_10873 = ((BgL_objectz00_bglt) BgL_new1855z00_8542);
							BgL_auxz00_10872 = BGL_OBJECT_WIDENING(BgL_tmpz00_10873);
						}
						BgL_auxz00_10871 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10872);
					}
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10871))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1673z00_7086)), BUNSPEC);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10879;

					{
						obj_t BgL_auxz00_10880;

						{	/* Liveness/types.scm 227 */
							BgL_objectz00_bglt BgL_tmpz00_10881;

							BgL_tmpz00_10881 = ((BgL_objectz00_bglt) BgL_new1855z00_8542);
							BgL_auxz00_10880 = BGL_OBJECT_WIDENING(BgL_tmpz00_10881);
						}
						BgL_auxz00_10879 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10880);
					}
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10879))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1674z00_7087)), BUNSPEC);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10887;

					{
						obj_t BgL_auxz00_10888;

						{	/* Liveness/types.scm 227 */
							BgL_objectz00_bglt BgL_tmpz00_10889;

							BgL_tmpz00_10889 = ((BgL_objectz00_bglt) BgL_new1855z00_8542);
							BgL_auxz00_10888 = BGL_OBJECT_WIDENING(BgL_tmpz00_10889);
						}
						BgL_auxz00_10887 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10888);
					}
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10887))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1675z00_7088)), BUNSPEC);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10895;

					{
						obj_t BgL_auxz00_10896;

						{	/* Liveness/types.scm 227 */
							BgL_objectz00_bglt BgL_tmpz00_10897;

							BgL_tmpz00_10897 = ((BgL_objectz00_bglt) BgL_new1855z00_8542);
							BgL_auxz00_10896 = BGL_OBJECT_WIDENING(BgL_tmpz00_10897);
						}
						BgL_auxz00_10895 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10896);
					}
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10895))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1676z00_7089)), BUNSPEC);
				}
				return BgL_new1855z00_8542;
			}
		}

	}



/* &<@anonymous:3910> */
	obj_t BGl_z62zc3z04anonymousza33910ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7090)
	{
		{	/* Liveness/types.scm 227 */
			return BNIL;
		}

	}



/* &lambda3909 */
	obj_t BGl_z62lambda3909z62zzliveness_typesz00(obj_t BgL_envz00_7091,
		obj_t BgL_oz00_7092, obj_t BgL_vz00_7093)
	{
		{	/* Liveness/types.scm 227 */
			{
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10903;

				{
					obj_t BgL_auxz00_10904;

					{	/* Liveness/types.scm 227 */
						BgL_objectz00_bglt BgL_tmpz00_10905;

						BgL_tmpz00_10905 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_oz00_7092));
						BgL_auxz00_10904 = BGL_OBJECT_WIDENING(BgL_tmpz00_10905);
					}
					BgL_auxz00_10903 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10904);
				}
				return
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10903))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7093)), BUNSPEC);
			}
		}

	}



/* &lambda3908 */
	obj_t BGl_z62lambda3908z62zzliveness_typesz00(obj_t BgL_envz00_7094,
		obj_t BgL_oz00_7095)
	{
		{	/* Liveness/types.scm 227 */
			{
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10912;

				{
					obj_t BgL_auxz00_10913;

					{	/* Liveness/types.scm 227 */
						BgL_objectz00_bglt BgL_tmpz00_10914;

						BgL_tmpz00_10914 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_oz00_7095));
						BgL_auxz00_10913 = BGL_OBJECT_WIDENING(BgL_tmpz00_10914);
					}
					BgL_auxz00_10912 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10913);
				}
				return
					(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10912))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3903> */
	obj_t BGl_z62zc3z04anonymousza33903ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7096)
	{
		{	/* Liveness/types.scm 227 */
			return BNIL;
		}

	}



/* &lambda3902 */
	obj_t BGl_z62lambda3902z62zzliveness_typesz00(obj_t BgL_envz00_7097,
		obj_t BgL_oz00_7098, obj_t BgL_vz00_7099)
	{
		{	/* Liveness/types.scm 227 */
			{
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10920;

				{
					obj_t BgL_auxz00_10921;

					{	/* Liveness/types.scm 227 */
						BgL_objectz00_bglt BgL_tmpz00_10922;

						BgL_tmpz00_10922 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_oz00_7098));
						BgL_auxz00_10921 = BGL_OBJECT_WIDENING(BgL_tmpz00_10922);
					}
					BgL_auxz00_10920 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10921);
				}
				return
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10920))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7099)), BUNSPEC);
			}
		}

	}



/* &lambda3901 */
	obj_t BGl_z62lambda3901z62zzliveness_typesz00(obj_t BgL_envz00_7100,
		obj_t BgL_oz00_7101)
	{
		{	/* Liveness/types.scm 227 */
			{
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10929;

				{
					obj_t BgL_auxz00_10930;

					{	/* Liveness/types.scm 227 */
						BgL_objectz00_bglt BgL_tmpz00_10931;

						BgL_tmpz00_10931 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_oz00_7101));
						BgL_auxz00_10930 = BGL_OBJECT_WIDENING(BgL_tmpz00_10931);
					}
					BgL_auxz00_10929 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10930);
				}
				return
					(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10929))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3896> */
	obj_t BGl_z62zc3z04anonymousza33896ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7102)
	{
		{	/* Liveness/types.scm 227 */
			return BNIL;
		}

	}



/* &lambda3895 */
	obj_t BGl_z62lambda3895z62zzliveness_typesz00(obj_t BgL_envz00_7103,
		obj_t BgL_oz00_7104, obj_t BgL_vz00_7105)
	{
		{	/* Liveness/types.scm 227 */
			{
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10937;

				{
					obj_t BgL_auxz00_10938;

					{	/* Liveness/types.scm 227 */
						BgL_objectz00_bglt BgL_tmpz00_10939;

						BgL_tmpz00_10939 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_oz00_7104));
						BgL_auxz00_10938 = BGL_OBJECT_WIDENING(BgL_tmpz00_10939);
					}
					BgL_auxz00_10937 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10938);
				}
				return
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10937))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7105)), BUNSPEC);
			}
		}

	}



/* &lambda3894 */
	obj_t BGl_z62lambda3894z62zzliveness_typesz00(obj_t BgL_envz00_7106,
		obj_t BgL_oz00_7107)
	{
		{	/* Liveness/types.scm 227 */
			{
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10946;

				{
					obj_t BgL_auxz00_10947;

					{	/* Liveness/types.scm 227 */
						BgL_objectz00_bglt BgL_tmpz00_10948;

						BgL_tmpz00_10948 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_oz00_7107));
						BgL_auxz00_10947 = BGL_OBJECT_WIDENING(BgL_tmpz00_10948);
					}
					BgL_auxz00_10946 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10947);
				}
				return
					(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10946))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3889> */
	obj_t BGl_z62zc3z04anonymousza33889ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7108)
	{
		{	/* Liveness/types.scm 227 */
			return BNIL;
		}

	}



/* &lambda3888 */
	obj_t BGl_z62lambda3888z62zzliveness_typesz00(obj_t BgL_envz00_7109,
		obj_t BgL_oz00_7110, obj_t BgL_vz00_7111)
	{
		{	/* Liveness/types.scm 227 */
			{
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10954;

				{
					obj_t BgL_auxz00_10955;

					{	/* Liveness/types.scm 227 */
						BgL_objectz00_bglt BgL_tmpz00_10956;

						BgL_tmpz00_10956 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_oz00_7110));
						BgL_auxz00_10955 = BGL_OBJECT_WIDENING(BgL_tmpz00_10956);
					}
					BgL_auxz00_10954 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10955);
				}
				return
					((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10954))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7111)), BUNSPEC);
			}
		}

	}



/* &lambda3887 */
	obj_t BGl_z62lambda3887z62zzliveness_typesz00(obj_t BgL_envz00_7112,
		obj_t BgL_oz00_7113)
	{
		{	/* Liveness/types.scm 227 */
			{
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_10963;

				{
					obj_t BgL_auxz00_10964;

					{	/* Liveness/types.scm 227 */
						BgL_objectz00_bglt BgL_tmpz00_10965;

						BgL_tmpz00_10965 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_oz00_7113));
						BgL_auxz00_10964 = BGL_OBJECT_WIDENING(BgL_tmpz00_10965);
					}
					BgL_auxz00_10963 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_10964);
				}
				return
					(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_10963))->
					BgL_defz00);
			}
		}

	}



/* &lambda3835 */
	BgL_boxzd2refzd2_bglt BGl_z62lambda3835z62zzliveness_typesz00(obj_t
		BgL_envz00_7114, obj_t BgL_o1666z00_7115)
	{
		{	/* Liveness/types.scm 221 */
			{	/* Liveness/types.scm 221 */
				long BgL_arg3836z00_8561;

				{	/* Liveness/types.scm 221 */
					obj_t BgL_arg3837z00_8562;

					{	/* Liveness/types.scm 221 */
						obj_t BgL_arg3838z00_8563;

						{	/* Liveness/types.scm 221 */
							obj_t BgL_arg1815z00_8564;
							long BgL_arg1816z00_8565;

							BgL_arg1815z00_8564 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 221 */
								long BgL_arg1817z00_8566;

								BgL_arg1817z00_8566 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_o1666z00_7115)));
								BgL_arg1816z00_8565 = (BgL_arg1817z00_8566 - OBJECT_TYPE);
							}
							BgL_arg3838z00_8563 =
								VECTOR_REF(BgL_arg1815z00_8564, BgL_arg1816z00_8565);
						}
						BgL_arg3837z00_8562 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3838z00_8563);
					}
					{	/* Liveness/types.scm 221 */
						obj_t BgL_tmpz00_10978;

						BgL_tmpz00_10978 = ((obj_t) BgL_arg3837z00_8562);
						BgL_arg3836z00_8561 = BGL_CLASS_NUM(BgL_tmpz00_10978);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_boxzd2refzd2_bglt) BgL_o1666z00_7115)), BgL_arg3836z00_8561);
			}
			{	/* Liveness/types.scm 221 */
				BgL_objectz00_bglt BgL_tmpz00_10984;

				BgL_tmpz00_10984 =
					((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_o1666z00_7115));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10984, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_o1666z00_7115));
			return
				((BgL_boxzd2refzd2_bglt) ((BgL_boxzd2refzd2_bglt) BgL_o1666z00_7115));
		}

	}



/* &<@anonymous:3834> */
	obj_t BGl_z62zc3z04anonymousza33834ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7116, obj_t BgL_new1665z00_7117)
	{
		{	/* Liveness/types.scm 221 */
			{
				BgL_boxzd2refzd2_bglt BgL_auxz00_10992;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10996;

					{	/* Liveness/types.scm 221 */
						obj_t BgL_classz00_8568;

						BgL_classz00_8568 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 221 */
							obj_t BgL__ortest_1117z00_8569;

							BgL__ortest_1117z00_8569 = BGL_CLASS_NIL(BgL_classz00_8568);
							if (CBOOL(BgL__ortest_1117z00_8569))
								{	/* Liveness/types.scm 221 */
									BgL_auxz00_10996 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8569);
								}
							else
								{	/* Liveness/types.scm 221 */
									BgL_auxz00_10996 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8568));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10996), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_boxzd2refzd2_bglt)
										BgL_new1665z00_7117))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_varz00_bglt BgL_auxz00_11012;

					{	/* Liveness/types.scm 221 */
						obj_t BgL_classz00_8570;

						BgL_classz00_8570 = BGl_varz00zzast_nodez00;
						{	/* Liveness/types.scm 221 */
							obj_t BgL__ortest_1117z00_8571;

							BgL__ortest_1117z00_8571 = BGL_CLASS_NIL(BgL_classz00_8570);
							if (CBOOL(BgL__ortest_1117z00_8571))
								{	/* Liveness/types.scm 221 */
									BgL_auxz00_11012 =
										((BgL_varz00_bglt) BgL__ortest_1117z00_8571);
								}
							else
								{	/* Liveness/types.scm 221 */
									BgL_auxz00_11012 =
										((BgL_varz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8570));
								}
						}
					}
					((((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117))))->
							BgL_varz00) = ((BgL_varz00_bglt) BgL_auxz00_11012), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_11022;

					{	/* Liveness/types.scm 221 */
						obj_t BgL_classz00_8572;

						BgL_classz00_8572 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 221 */
							obj_t BgL__ortest_1117z00_8573;

							BgL__ortest_1117z00_8573 = BGL_CLASS_NIL(BgL_classz00_8572);
							if (CBOOL(BgL__ortest_1117z00_8573))
								{	/* Liveness/types.scm 221 */
									BgL_auxz00_11022 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8573);
								}
							else
								{	/* Liveness/types.scm 221 */
									BgL_auxz00_11022 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8572));
								}
						}
					}
					((((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117))))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_11022), BUNSPEC);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11032;

					{
						obj_t BgL_auxz00_11033;

						{	/* Liveness/types.scm 221 */
							BgL_objectz00_bglt BgL_tmpz00_11034;

							BgL_tmpz00_11034 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117));
							BgL_auxz00_11033 = BGL_OBJECT_WIDENING(BgL_tmpz00_11034);
						}
						BgL_auxz00_11032 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11033);
					}
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11032))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11040;

					{
						obj_t BgL_auxz00_11041;

						{	/* Liveness/types.scm 221 */
							BgL_objectz00_bglt BgL_tmpz00_11042;

							BgL_tmpz00_11042 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117));
							BgL_auxz00_11041 = BGL_OBJECT_WIDENING(BgL_tmpz00_11042);
						}
						BgL_auxz00_11040 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11041);
					}
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11040))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11048;

					{
						obj_t BgL_auxz00_11049;

						{	/* Liveness/types.scm 221 */
							BgL_objectz00_bglt BgL_tmpz00_11050;

							BgL_tmpz00_11050 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117));
							BgL_auxz00_11049 = BGL_OBJECT_WIDENING(BgL_tmpz00_11050);
						}
						BgL_auxz00_11048 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11049);
					}
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11048))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11056;

					{
						obj_t BgL_auxz00_11057;

						{	/* Liveness/types.scm 221 */
							BgL_objectz00_bglt BgL_tmpz00_11058;

							BgL_tmpz00_11058 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117));
							BgL_auxz00_11057 = BGL_OBJECT_WIDENING(BgL_tmpz00_11058);
						}
						BgL_auxz00_11056 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11057);
					}
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11056))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_10992 = ((BgL_boxzd2refzd2_bglt) BgL_new1665z00_7117);
				return ((obj_t) BgL_auxz00_10992);
			}
		}

	}



/* &lambda3832 */
	BgL_boxzd2refzd2_bglt BGl_z62lambda3832z62zzliveness_typesz00(obj_t
		BgL_envz00_7118, obj_t BgL_o1662z00_7119)
	{
		{	/* Liveness/types.scm 221 */
			{	/* Liveness/types.scm 221 */
				BgL_boxzd2refzf2livenessz20_bglt BgL_wide1664z00_8575;

				BgL_wide1664z00_8575 =
					((BgL_boxzd2refzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_boxzd2refzf2livenessz20_bgl))));
				{	/* Liveness/types.scm 221 */
					obj_t BgL_auxz00_11071;
					BgL_objectz00_bglt BgL_tmpz00_11067;

					BgL_auxz00_11071 = ((obj_t) BgL_wide1664z00_8575);
					BgL_tmpz00_11067 =
						((BgL_objectz00_bglt)
						((BgL_boxzd2refzd2_bglt)
							((BgL_boxzd2refzd2_bglt) BgL_o1662z00_7119)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11067, BgL_auxz00_11071);
				}
				((BgL_objectz00_bglt)
					((BgL_boxzd2refzd2_bglt)
						((BgL_boxzd2refzd2_bglt) BgL_o1662z00_7119)));
				{	/* Liveness/types.scm 221 */
					long BgL_arg3833z00_8576;

					BgL_arg3833z00_8576 =
						BGL_CLASS_NUM(BGl_boxzd2refzf2livenessz20zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_boxzd2refzd2_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_o1662z00_7119))),
						BgL_arg3833z00_8576);
				}
				return
					((BgL_boxzd2refzd2_bglt)
					((BgL_boxzd2refzd2_bglt)
						((BgL_boxzd2refzd2_bglt) BgL_o1662z00_7119)));
			}
		}

	}



/* &lambda3829 */
	BgL_boxzd2refzd2_bglt BGl_z62lambda3829z62zzliveness_typesz00(obj_t
		BgL_envz00_7120, obj_t BgL_loc1652z00_7121, obj_t BgL_type1653z00_7122,
		obj_t BgL_sidezd2effect1654zd2_7123, obj_t BgL_key1655z00_7124,
		obj_t BgL_var1656z00_7125, obj_t BgL_vtype1657z00_7126,
		obj_t BgL_def1658z00_7127, obj_t BgL_use1659z00_7128,
		obj_t BgL_in1660z00_7129, obj_t BgL_out1661z00_7130)
	{
		{	/* Liveness/types.scm 221 */
			{	/* Liveness/types.scm 221 */
				BgL_boxzd2refzd2_bglt BgL_new1850z00_8584;

				{	/* Liveness/types.scm 221 */
					BgL_boxzd2refzd2_bglt BgL_tmp1848z00_8585;
					BgL_boxzd2refzf2livenessz20_bglt BgL_wide1849z00_8586;

					{
						BgL_boxzd2refzd2_bglt BgL_auxz00_11085;

						{	/* Liveness/types.scm 221 */
							BgL_boxzd2refzd2_bglt BgL_new1847z00_8587;

							BgL_new1847z00_8587 =
								((BgL_boxzd2refzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_boxzd2refzd2_bgl))));
							{	/* Liveness/types.scm 221 */
								long BgL_arg3831z00_8588;

								BgL_arg3831z00_8588 =
									BGL_CLASS_NUM(BGl_boxzd2refzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1847z00_8587),
									BgL_arg3831z00_8588);
							}
							{	/* Liveness/types.scm 221 */
								BgL_objectz00_bglt BgL_tmpz00_11090;

								BgL_tmpz00_11090 = ((BgL_objectz00_bglt) BgL_new1847z00_8587);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11090, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1847z00_8587);
							BgL_auxz00_11085 = BgL_new1847z00_8587;
						}
						BgL_tmp1848z00_8585 = ((BgL_boxzd2refzd2_bglt) BgL_auxz00_11085);
					}
					BgL_wide1849z00_8586 =
						((BgL_boxzd2refzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_boxzd2refzf2livenessz20_bgl))));
					{	/* Liveness/types.scm 221 */
						obj_t BgL_auxz00_11098;
						BgL_objectz00_bglt BgL_tmpz00_11096;

						BgL_auxz00_11098 = ((obj_t) BgL_wide1849z00_8586);
						BgL_tmpz00_11096 = ((BgL_objectz00_bglt) BgL_tmp1848z00_8585);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11096, BgL_auxz00_11098);
					}
					((BgL_objectz00_bglt) BgL_tmp1848z00_8585);
					{	/* Liveness/types.scm 221 */
						long BgL_arg3830z00_8589;

						BgL_arg3830z00_8589 =
							BGL_CLASS_NUM(BGl_boxzd2refzf2livenessz20zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1848z00_8585), BgL_arg3830z00_8589);
					}
					BgL_new1850z00_8584 = ((BgL_boxzd2refzd2_bglt) BgL_tmp1848z00_8585);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1850z00_8584)))->BgL_locz00) =
					((obj_t) BgL_loc1652z00_7121), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1850z00_8584)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1653z00_7122)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1850z00_8584)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1654zd2_7123), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1850z00_8584)))->BgL_keyz00) =
					((obj_t) BgL_key1655z00_7124), BUNSPEC);
				((((BgL_boxzd2refzd2_bglt) COBJECT(((BgL_boxzd2refzd2_bglt)
									BgL_new1850z00_8584)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_var1656z00_7125)), BUNSPEC);
				((((BgL_boxzd2refzd2_bglt) COBJECT(((BgL_boxzd2refzd2_bglt)
									BgL_new1850z00_8584)))->BgL_vtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1657z00_7126)),
					BUNSPEC);
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11121;

					{
						obj_t BgL_auxz00_11122;

						{	/* Liveness/types.scm 221 */
							BgL_objectz00_bglt BgL_tmpz00_11123;

							BgL_tmpz00_11123 = ((BgL_objectz00_bglt) BgL_new1850z00_8584);
							BgL_auxz00_11122 = BGL_OBJECT_WIDENING(BgL_tmpz00_11123);
						}
						BgL_auxz00_11121 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11122);
					}
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11121))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1658z00_7127)), BUNSPEC);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11129;

					{
						obj_t BgL_auxz00_11130;

						{	/* Liveness/types.scm 221 */
							BgL_objectz00_bglt BgL_tmpz00_11131;

							BgL_tmpz00_11131 = ((BgL_objectz00_bglt) BgL_new1850z00_8584);
							BgL_auxz00_11130 = BGL_OBJECT_WIDENING(BgL_tmpz00_11131);
						}
						BgL_auxz00_11129 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11130);
					}
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11129))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1659z00_7128)), BUNSPEC);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11137;

					{
						obj_t BgL_auxz00_11138;

						{	/* Liveness/types.scm 221 */
							BgL_objectz00_bglt BgL_tmpz00_11139;

							BgL_tmpz00_11139 = ((BgL_objectz00_bglt) BgL_new1850z00_8584);
							BgL_auxz00_11138 = BGL_OBJECT_WIDENING(BgL_tmpz00_11139);
						}
						BgL_auxz00_11137 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11138);
					}
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11137))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1660z00_7129)), BUNSPEC);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11145;

					{
						obj_t BgL_auxz00_11146;

						{	/* Liveness/types.scm 221 */
							BgL_objectz00_bglt BgL_tmpz00_11147;

							BgL_tmpz00_11147 = ((BgL_objectz00_bglt) BgL_new1850z00_8584);
							BgL_auxz00_11146 = BGL_OBJECT_WIDENING(BgL_tmpz00_11147);
						}
						BgL_auxz00_11145 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11146);
					}
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11145))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1661z00_7130)), BUNSPEC);
				}
				return BgL_new1850z00_8584;
			}
		}

	}



/* &<@anonymous:3866> */
	obj_t BGl_z62zc3z04anonymousza33866ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7131)
	{
		{	/* Liveness/types.scm 221 */
			return BNIL;
		}

	}



/* &lambda3865 */
	obj_t BGl_z62lambda3865z62zzliveness_typesz00(obj_t BgL_envz00_7132,
		obj_t BgL_oz00_7133, obj_t BgL_vz00_7134)
	{
		{	/* Liveness/types.scm 221 */
			{
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11153;

				{
					obj_t BgL_auxz00_11154;

					{	/* Liveness/types.scm 221 */
						BgL_objectz00_bglt BgL_tmpz00_11155;

						BgL_tmpz00_11155 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_oz00_7133));
						BgL_auxz00_11154 = BGL_OBJECT_WIDENING(BgL_tmpz00_11155);
					}
					BgL_auxz00_11153 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11154);
				}
				return
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11153))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7134)), BUNSPEC);
			}
		}

	}



/* &lambda3864 */
	obj_t BGl_z62lambda3864z62zzliveness_typesz00(obj_t BgL_envz00_7135,
		obj_t BgL_oz00_7136)
	{
		{	/* Liveness/types.scm 221 */
			{
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11162;

				{
					obj_t BgL_auxz00_11163;

					{	/* Liveness/types.scm 221 */
						BgL_objectz00_bglt BgL_tmpz00_11164;

						BgL_tmpz00_11164 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_oz00_7136));
						BgL_auxz00_11163 = BGL_OBJECT_WIDENING(BgL_tmpz00_11164);
					}
					BgL_auxz00_11162 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11163);
				}
				return
					(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11162))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3859> */
	obj_t BGl_z62zc3z04anonymousza33859ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7137)
	{
		{	/* Liveness/types.scm 221 */
			return BNIL;
		}

	}



/* &lambda3858 */
	obj_t BGl_z62lambda3858z62zzliveness_typesz00(obj_t BgL_envz00_7138,
		obj_t BgL_oz00_7139, obj_t BgL_vz00_7140)
	{
		{	/* Liveness/types.scm 221 */
			{
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11170;

				{
					obj_t BgL_auxz00_11171;

					{	/* Liveness/types.scm 221 */
						BgL_objectz00_bglt BgL_tmpz00_11172;

						BgL_tmpz00_11172 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_oz00_7139));
						BgL_auxz00_11171 = BGL_OBJECT_WIDENING(BgL_tmpz00_11172);
					}
					BgL_auxz00_11170 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11171);
				}
				return
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11170))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7140)), BUNSPEC);
			}
		}

	}



/* &lambda3857 */
	obj_t BGl_z62lambda3857z62zzliveness_typesz00(obj_t BgL_envz00_7141,
		obj_t BgL_oz00_7142)
	{
		{	/* Liveness/types.scm 221 */
			{
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11179;

				{
					obj_t BgL_auxz00_11180;

					{	/* Liveness/types.scm 221 */
						BgL_objectz00_bglt BgL_tmpz00_11181;

						BgL_tmpz00_11181 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_oz00_7142));
						BgL_auxz00_11180 = BGL_OBJECT_WIDENING(BgL_tmpz00_11181);
					}
					BgL_auxz00_11179 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11180);
				}
				return
					(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11179))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3852> */
	obj_t BGl_z62zc3z04anonymousza33852ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7143)
	{
		{	/* Liveness/types.scm 221 */
			return BNIL;
		}

	}



/* &lambda3851 */
	obj_t BGl_z62lambda3851z62zzliveness_typesz00(obj_t BgL_envz00_7144,
		obj_t BgL_oz00_7145, obj_t BgL_vz00_7146)
	{
		{	/* Liveness/types.scm 221 */
			{
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11187;

				{
					obj_t BgL_auxz00_11188;

					{	/* Liveness/types.scm 221 */
						BgL_objectz00_bglt BgL_tmpz00_11189;

						BgL_tmpz00_11189 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_oz00_7145));
						BgL_auxz00_11188 = BGL_OBJECT_WIDENING(BgL_tmpz00_11189);
					}
					BgL_auxz00_11187 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11188);
				}
				return
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11187))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7146)), BUNSPEC);
			}
		}

	}



/* &lambda3850 */
	obj_t BGl_z62lambda3850z62zzliveness_typesz00(obj_t BgL_envz00_7147,
		obj_t BgL_oz00_7148)
	{
		{	/* Liveness/types.scm 221 */
			{
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11196;

				{
					obj_t BgL_auxz00_11197;

					{	/* Liveness/types.scm 221 */
						BgL_objectz00_bglt BgL_tmpz00_11198;

						BgL_tmpz00_11198 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_oz00_7148));
						BgL_auxz00_11197 = BGL_OBJECT_WIDENING(BgL_tmpz00_11198);
					}
					BgL_auxz00_11196 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11197);
				}
				return
					(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11196))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3845> */
	obj_t BGl_z62zc3z04anonymousza33845ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7149)
	{
		{	/* Liveness/types.scm 221 */
			return BNIL;
		}

	}



/* &lambda3844 */
	obj_t BGl_z62lambda3844z62zzliveness_typesz00(obj_t BgL_envz00_7150,
		obj_t BgL_oz00_7151, obj_t BgL_vz00_7152)
	{
		{	/* Liveness/types.scm 221 */
			{
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11204;

				{
					obj_t BgL_auxz00_11205;

					{	/* Liveness/types.scm 221 */
						BgL_objectz00_bglt BgL_tmpz00_11206;

						BgL_tmpz00_11206 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_oz00_7151));
						BgL_auxz00_11205 = BGL_OBJECT_WIDENING(BgL_tmpz00_11206);
					}
					BgL_auxz00_11204 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11205);
				}
				return
					((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11204))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7152)), BUNSPEC);
			}
		}

	}



/* &lambda3843 */
	obj_t BGl_z62lambda3843z62zzliveness_typesz00(obj_t BgL_envz00_7153,
		obj_t BgL_oz00_7154)
	{
		{	/* Liveness/types.scm 221 */
			{
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_11213;

				{
					obj_t BgL_auxz00_11214;

					{	/* Liveness/types.scm 221 */
						BgL_objectz00_bglt BgL_tmpz00_11215;

						BgL_tmpz00_11215 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_oz00_7154));
						BgL_auxz00_11214 = BGL_OBJECT_WIDENING(BgL_tmpz00_11215);
					}
					BgL_auxz00_11213 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_11214);
				}
				return
					(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_11213))->
					BgL_defz00);
			}
		}

	}



/* &lambda3791 */
	BgL_makezd2boxzd2_bglt BGl_z62lambda3791z62zzliveness_typesz00(obj_t
		BgL_envz00_7155, obj_t BgL_o1650z00_7156)
	{
		{	/* Liveness/types.scm 215 */
			{	/* Liveness/types.scm 215 */
				long BgL_arg3792z00_8603;

				{	/* Liveness/types.scm 215 */
					obj_t BgL_arg3793z00_8604;

					{	/* Liveness/types.scm 215 */
						obj_t BgL_arg3794z00_8605;

						{	/* Liveness/types.scm 215 */
							obj_t BgL_arg1815z00_8606;
							long BgL_arg1816z00_8607;

							BgL_arg1815z00_8606 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 215 */
								long BgL_arg1817z00_8608;

								BgL_arg1817z00_8608 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_o1650z00_7156)));
								BgL_arg1816z00_8607 = (BgL_arg1817z00_8608 - OBJECT_TYPE);
							}
							BgL_arg3794z00_8605 =
								VECTOR_REF(BgL_arg1815z00_8606, BgL_arg1816z00_8607);
						}
						BgL_arg3793z00_8604 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3794z00_8605);
					}
					{	/* Liveness/types.scm 215 */
						obj_t BgL_tmpz00_11228;

						BgL_tmpz00_11228 = ((obj_t) BgL_arg3793z00_8604);
						BgL_arg3792z00_8603 = BGL_CLASS_NUM(BgL_tmpz00_11228);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_makezd2boxzd2_bglt) BgL_o1650z00_7156)), BgL_arg3792z00_8603);
			}
			{	/* Liveness/types.scm 215 */
				BgL_objectz00_bglt BgL_tmpz00_11234;

				BgL_tmpz00_11234 =
					((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_o1650z00_7156));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11234, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_o1650z00_7156));
			return
				((BgL_makezd2boxzd2_bglt) ((BgL_makezd2boxzd2_bglt) BgL_o1650z00_7156));
		}

	}



/* &<@anonymous:3790> */
	obj_t BGl_z62zc3z04anonymousza33790ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7157, obj_t BgL_new1649z00_7158)
	{
		{	/* Liveness/types.scm 215 */
			{
				BgL_makezd2boxzd2_bglt BgL_auxz00_11242;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11246;

					{	/* Liveness/types.scm 215 */
						obj_t BgL_classz00_8610;

						BgL_classz00_8610 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 215 */
							obj_t BgL__ortest_1117z00_8611;

							BgL__ortest_1117z00_8611 = BGL_CLASS_NIL(BgL_classz00_8610);
							if (CBOOL(BgL__ortest_1117z00_8611))
								{	/* Liveness/types.scm 215 */
									BgL_auxz00_11246 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8611);
								}
							else
								{	/* Liveness/types.scm 215 */
									BgL_auxz00_11246 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8610));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_11246), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_makezd2boxzd2_bglt)
										BgL_new1649z00_7158))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_11262;

					{	/* Liveness/types.scm 215 */
						obj_t BgL_classz00_8612;

						BgL_classz00_8612 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 215 */
							obj_t BgL__ortest_1117z00_8613;

							BgL__ortest_1117z00_8613 = BGL_CLASS_NIL(BgL_classz00_8612);
							if (CBOOL(BgL__ortest_1117z00_8613))
								{	/* Liveness/types.scm 215 */
									BgL_auxz00_11262 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8613);
								}
							else
								{	/* Liveness/types.scm 215 */
									BgL_auxz00_11262 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8612));
								}
						}
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158))))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_11262), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_11272;

					{	/* Liveness/types.scm 215 */
						obj_t BgL_classz00_8614;

						BgL_classz00_8614 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 215 */
							obj_t BgL__ortest_1117z00_8615;

							BgL__ortest_1117z00_8615 = BGL_CLASS_NIL(BgL_classz00_8614);
							if (CBOOL(BgL__ortest_1117z00_8615))
								{	/* Liveness/types.scm 215 */
									BgL_auxz00_11272 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8615);
								}
							else
								{	/* Liveness/types.scm 215 */
									BgL_auxz00_11272 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8614));
								}
						}
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158))))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_11272), BUNSPEC);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158))))->
						BgL_stackablez00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11285;

					{
						obj_t BgL_auxz00_11286;

						{	/* Liveness/types.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_11287;

							BgL_tmpz00_11287 =
								((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158));
							BgL_auxz00_11286 = BGL_OBJECT_WIDENING(BgL_tmpz00_11287);
						}
						BgL_auxz00_11285 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11286);
					}
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11285))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11293;

					{
						obj_t BgL_auxz00_11294;

						{	/* Liveness/types.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_11295;

							BgL_tmpz00_11295 =
								((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158));
							BgL_auxz00_11294 = BGL_OBJECT_WIDENING(BgL_tmpz00_11295);
						}
						BgL_auxz00_11293 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11294);
					}
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11293))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11301;

					{
						obj_t BgL_auxz00_11302;

						{	/* Liveness/types.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_11303;

							BgL_tmpz00_11303 =
								((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158));
							BgL_auxz00_11302 = BGL_OBJECT_WIDENING(BgL_tmpz00_11303);
						}
						BgL_auxz00_11301 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11302);
					}
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11301))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11309;

					{
						obj_t BgL_auxz00_11310;

						{	/* Liveness/types.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_11311;

							BgL_tmpz00_11311 =
								((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158));
							BgL_auxz00_11310 = BGL_OBJECT_WIDENING(BgL_tmpz00_11311);
						}
						BgL_auxz00_11309 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11310);
					}
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11309))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_11242 = ((BgL_makezd2boxzd2_bglt) BgL_new1649z00_7158);
				return ((obj_t) BgL_auxz00_11242);
			}
		}

	}



/* &lambda3788 */
	BgL_makezd2boxzd2_bglt BGl_z62lambda3788z62zzliveness_typesz00(obj_t
		BgL_envz00_7159, obj_t BgL_o1646z00_7160)
	{
		{	/* Liveness/types.scm 215 */
			{	/* Liveness/types.scm 215 */
				BgL_makezd2boxzf2livenessz20_bglt BgL_wide1648z00_8617;

				BgL_wide1648z00_8617 =
					((BgL_makezd2boxzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_makezd2boxzf2livenessz20_bgl))));
				{	/* Liveness/types.scm 215 */
					obj_t BgL_auxz00_11324;
					BgL_objectz00_bglt BgL_tmpz00_11320;

					BgL_auxz00_11324 = ((obj_t) BgL_wide1648z00_8617);
					BgL_tmpz00_11320 =
						((BgL_objectz00_bglt)
						((BgL_makezd2boxzd2_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_o1646z00_7160)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11320, BgL_auxz00_11324);
				}
				((BgL_objectz00_bglt)
					((BgL_makezd2boxzd2_bglt)
						((BgL_makezd2boxzd2_bglt) BgL_o1646z00_7160)));
				{	/* Liveness/types.scm 215 */
					long BgL_arg3789z00_8618;

					BgL_arg3789z00_8618 =
						BGL_CLASS_NUM(BGl_makezd2boxzf2livenessz20zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_makezd2boxzd2_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_o1646z00_7160))),
						BgL_arg3789z00_8618);
				}
				return
					((BgL_makezd2boxzd2_bglt)
					((BgL_makezd2boxzd2_bglt)
						((BgL_makezd2boxzd2_bglt) BgL_o1646z00_7160)));
			}
		}

	}



/* &lambda3785 */
	BgL_makezd2boxzd2_bglt BGl_z62lambda3785z62zzliveness_typesz00(obj_t
		BgL_envz00_7161, obj_t BgL_loc1635z00_7162, obj_t BgL_type1636z00_7163,
		obj_t BgL_sidezd2effect1637zd2_7164, obj_t BgL_key1638z00_7165,
		obj_t BgL_value1639z00_7166, obj_t BgL_vtype1640z00_7167,
		obj_t BgL_stackable1641z00_7168, obj_t BgL_def1642z00_7169,
		obj_t BgL_use1643z00_7170, obj_t BgL_in1644z00_7171,
		obj_t BgL_out1645z00_7172)
	{
		{	/* Liveness/types.scm 215 */
			{	/* Liveness/types.scm 215 */
				BgL_makezd2boxzd2_bglt BgL_new1845z00_8626;

				{	/* Liveness/types.scm 215 */
					BgL_makezd2boxzd2_bglt BgL_tmp1843z00_8627;
					BgL_makezd2boxzf2livenessz20_bglt BgL_wide1844z00_8628;

					{
						BgL_makezd2boxzd2_bglt BgL_auxz00_11338;

						{	/* Liveness/types.scm 215 */
							BgL_makezd2boxzd2_bglt BgL_new1842z00_8629;

							BgL_new1842z00_8629 =
								((BgL_makezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_makezd2boxzd2_bgl))));
							{	/* Liveness/types.scm 215 */
								long BgL_arg3787z00_8630;

								BgL_arg3787z00_8630 =
									BGL_CLASS_NUM(BGl_makezd2boxzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1842z00_8629),
									BgL_arg3787z00_8630);
							}
							{	/* Liveness/types.scm 215 */
								BgL_objectz00_bglt BgL_tmpz00_11343;

								BgL_tmpz00_11343 = ((BgL_objectz00_bglt) BgL_new1842z00_8629);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11343, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1842z00_8629);
							BgL_auxz00_11338 = BgL_new1842z00_8629;
						}
						BgL_tmp1843z00_8627 = ((BgL_makezd2boxzd2_bglt) BgL_auxz00_11338);
					}
					BgL_wide1844z00_8628 =
						((BgL_makezd2boxzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_makezd2boxzf2livenessz20_bgl))));
					{	/* Liveness/types.scm 215 */
						obj_t BgL_auxz00_11351;
						BgL_objectz00_bglt BgL_tmpz00_11349;

						BgL_auxz00_11351 = ((obj_t) BgL_wide1844z00_8628);
						BgL_tmpz00_11349 = ((BgL_objectz00_bglt) BgL_tmp1843z00_8627);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11349, BgL_auxz00_11351);
					}
					((BgL_objectz00_bglt) BgL_tmp1843z00_8627);
					{	/* Liveness/types.scm 215 */
						long BgL_arg3786z00_8631;

						BgL_arg3786z00_8631 =
							BGL_CLASS_NUM(BGl_makezd2boxzf2livenessz20zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1843z00_8627), BgL_arg3786z00_8631);
					}
					BgL_new1845z00_8626 = ((BgL_makezd2boxzd2_bglt) BgL_tmp1843z00_8627);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1845z00_8626)))->BgL_locz00) =
					((obj_t) BgL_loc1635z00_7162), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1845z00_8626)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1636z00_7163)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1845z00_8626)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1637zd2_7164), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1845z00_8626)))->BgL_keyz00) =
					((obj_t) BgL_key1638z00_7165), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1845z00_8626)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_value1639z00_7166)),
					BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1845z00_8626)))->BgL_vtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1640z00_7167)),
					BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1845z00_8626)))->BgL_stackablez00) =
					((obj_t) BgL_stackable1641z00_7168), BUNSPEC);
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11376;

					{
						obj_t BgL_auxz00_11377;

						{	/* Liveness/types.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_11378;

							BgL_tmpz00_11378 = ((BgL_objectz00_bglt) BgL_new1845z00_8626);
							BgL_auxz00_11377 = BGL_OBJECT_WIDENING(BgL_tmpz00_11378);
						}
						BgL_auxz00_11376 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11377);
					}
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11376))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1642z00_7169)), BUNSPEC);
				}
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11384;

					{
						obj_t BgL_auxz00_11385;

						{	/* Liveness/types.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_11386;

							BgL_tmpz00_11386 = ((BgL_objectz00_bglt) BgL_new1845z00_8626);
							BgL_auxz00_11385 = BGL_OBJECT_WIDENING(BgL_tmpz00_11386);
						}
						BgL_auxz00_11384 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11385);
					}
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11384))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1643z00_7170)), BUNSPEC);
				}
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11392;

					{
						obj_t BgL_auxz00_11393;

						{	/* Liveness/types.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_11394;

							BgL_tmpz00_11394 = ((BgL_objectz00_bglt) BgL_new1845z00_8626);
							BgL_auxz00_11393 = BGL_OBJECT_WIDENING(BgL_tmpz00_11394);
						}
						BgL_auxz00_11392 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11393);
					}
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11392))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1644z00_7171)), BUNSPEC);
				}
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11400;

					{
						obj_t BgL_auxz00_11401;

						{	/* Liveness/types.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_11402;

							BgL_tmpz00_11402 = ((BgL_objectz00_bglt) BgL_new1845z00_8626);
							BgL_auxz00_11401 = BGL_OBJECT_WIDENING(BgL_tmpz00_11402);
						}
						BgL_auxz00_11400 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11401);
					}
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11400))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1645z00_7172)), BUNSPEC);
				}
				return BgL_new1845z00_8626;
			}
		}

	}



/* &<@anonymous:3822> */
	obj_t BGl_z62zc3z04anonymousza33822ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7173)
	{
		{	/* Liveness/types.scm 215 */
			return BNIL;
		}

	}



/* &lambda3821 */
	obj_t BGl_z62lambda3821z62zzliveness_typesz00(obj_t BgL_envz00_7174,
		obj_t BgL_oz00_7175, obj_t BgL_vz00_7176)
	{
		{	/* Liveness/types.scm 215 */
			{
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11408;

				{
					obj_t BgL_auxz00_11409;

					{	/* Liveness/types.scm 215 */
						BgL_objectz00_bglt BgL_tmpz00_11410;

						BgL_tmpz00_11410 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_oz00_7175));
						BgL_auxz00_11409 = BGL_OBJECT_WIDENING(BgL_tmpz00_11410);
					}
					BgL_auxz00_11408 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11409);
				}
				return
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11408))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7176)), BUNSPEC);
			}
		}

	}



/* &lambda3820 */
	obj_t BGl_z62lambda3820z62zzliveness_typesz00(obj_t BgL_envz00_7177,
		obj_t BgL_oz00_7178)
	{
		{	/* Liveness/types.scm 215 */
			{
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11417;

				{
					obj_t BgL_auxz00_11418;

					{	/* Liveness/types.scm 215 */
						BgL_objectz00_bglt BgL_tmpz00_11419;

						BgL_tmpz00_11419 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_oz00_7178));
						BgL_auxz00_11418 = BGL_OBJECT_WIDENING(BgL_tmpz00_11419);
					}
					BgL_auxz00_11417 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11418);
				}
				return
					(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11417))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3815> */
	obj_t BGl_z62zc3z04anonymousza33815ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7179)
	{
		{	/* Liveness/types.scm 215 */
			return BNIL;
		}

	}



/* &lambda3814 */
	obj_t BGl_z62lambda3814z62zzliveness_typesz00(obj_t BgL_envz00_7180,
		obj_t BgL_oz00_7181, obj_t BgL_vz00_7182)
	{
		{	/* Liveness/types.scm 215 */
			{
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11425;

				{
					obj_t BgL_auxz00_11426;

					{	/* Liveness/types.scm 215 */
						BgL_objectz00_bglt BgL_tmpz00_11427;

						BgL_tmpz00_11427 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_oz00_7181));
						BgL_auxz00_11426 = BGL_OBJECT_WIDENING(BgL_tmpz00_11427);
					}
					BgL_auxz00_11425 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11426);
				}
				return
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11425))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7182)), BUNSPEC);
			}
		}

	}



/* &lambda3813 */
	obj_t BGl_z62lambda3813z62zzliveness_typesz00(obj_t BgL_envz00_7183,
		obj_t BgL_oz00_7184)
	{
		{	/* Liveness/types.scm 215 */
			{
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11434;

				{
					obj_t BgL_auxz00_11435;

					{	/* Liveness/types.scm 215 */
						BgL_objectz00_bglt BgL_tmpz00_11436;

						BgL_tmpz00_11436 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_oz00_7184));
						BgL_auxz00_11435 = BGL_OBJECT_WIDENING(BgL_tmpz00_11436);
					}
					BgL_auxz00_11434 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11435);
				}
				return
					(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11434))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3808> */
	obj_t BGl_z62zc3z04anonymousza33808ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7185)
	{
		{	/* Liveness/types.scm 215 */
			return BNIL;
		}

	}



/* &lambda3807 */
	obj_t BGl_z62lambda3807z62zzliveness_typesz00(obj_t BgL_envz00_7186,
		obj_t BgL_oz00_7187, obj_t BgL_vz00_7188)
	{
		{	/* Liveness/types.scm 215 */
			{
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11442;

				{
					obj_t BgL_auxz00_11443;

					{	/* Liveness/types.scm 215 */
						BgL_objectz00_bglt BgL_tmpz00_11444;

						BgL_tmpz00_11444 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_oz00_7187));
						BgL_auxz00_11443 = BGL_OBJECT_WIDENING(BgL_tmpz00_11444);
					}
					BgL_auxz00_11442 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11443);
				}
				return
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11442))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7188)), BUNSPEC);
			}
		}

	}



/* &lambda3806 */
	obj_t BGl_z62lambda3806z62zzliveness_typesz00(obj_t BgL_envz00_7189,
		obj_t BgL_oz00_7190)
	{
		{	/* Liveness/types.scm 215 */
			{
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11451;

				{
					obj_t BgL_auxz00_11452;

					{	/* Liveness/types.scm 215 */
						BgL_objectz00_bglt BgL_tmpz00_11453;

						BgL_tmpz00_11453 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_oz00_7190));
						BgL_auxz00_11452 = BGL_OBJECT_WIDENING(BgL_tmpz00_11453);
					}
					BgL_auxz00_11451 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11452);
				}
				return
					(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11451))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3801> */
	obj_t BGl_z62zc3z04anonymousza33801ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7191)
	{
		{	/* Liveness/types.scm 215 */
			return BNIL;
		}

	}



/* &lambda3800 */
	obj_t BGl_z62lambda3800z62zzliveness_typesz00(obj_t BgL_envz00_7192,
		obj_t BgL_oz00_7193, obj_t BgL_vz00_7194)
	{
		{	/* Liveness/types.scm 215 */
			{
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11459;

				{
					obj_t BgL_auxz00_11460;

					{	/* Liveness/types.scm 215 */
						BgL_objectz00_bglt BgL_tmpz00_11461;

						BgL_tmpz00_11461 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_oz00_7193));
						BgL_auxz00_11460 = BGL_OBJECT_WIDENING(BgL_tmpz00_11461);
					}
					BgL_auxz00_11459 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11460);
				}
				return
					((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11459))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7194)), BUNSPEC);
			}
		}

	}



/* &lambda3799 */
	obj_t BGl_z62lambda3799z62zzliveness_typesz00(obj_t BgL_envz00_7195,
		obj_t BgL_oz00_7196)
	{
		{	/* Liveness/types.scm 215 */
			{
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_11468;

				{
					obj_t BgL_auxz00_11469;

					{	/* Liveness/types.scm 215 */
						BgL_objectz00_bglt BgL_tmpz00_11470;

						BgL_tmpz00_11470 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_oz00_7196));
						BgL_auxz00_11469 = BGL_OBJECT_WIDENING(BgL_tmpz00_11470);
					}
					BgL_auxz00_11468 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_11469);
				}
				return
					(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_11468))->
					BgL_defz00);
			}
		}

	}



/* &lambda3741 */
	BgL_returnz00_bglt BGl_z62lambda3741z62zzliveness_typesz00(obj_t
		BgL_envz00_7197, obj_t BgL_o1633z00_7198)
	{
		{	/* Liveness/types.scm 209 */
			{	/* Liveness/types.scm 209 */
				long BgL_arg3743z00_8645;

				{	/* Liveness/types.scm 209 */
					obj_t BgL_arg3744z00_8646;

					{	/* Liveness/types.scm 209 */
						obj_t BgL_arg3745z00_8647;

						{	/* Liveness/types.scm 209 */
							obj_t BgL_arg1815z00_8648;
							long BgL_arg1816z00_8649;

							BgL_arg1815z00_8648 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 209 */
								long BgL_arg1817z00_8650;

								BgL_arg1817z00_8650 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_returnz00_bglt) BgL_o1633z00_7198)));
								BgL_arg1816z00_8649 = (BgL_arg1817z00_8650 - OBJECT_TYPE);
							}
							BgL_arg3745z00_8647 =
								VECTOR_REF(BgL_arg1815z00_8648, BgL_arg1816z00_8649);
						}
						BgL_arg3744z00_8646 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3745z00_8647);
					}
					{	/* Liveness/types.scm 209 */
						obj_t BgL_tmpz00_11483;

						BgL_tmpz00_11483 = ((obj_t) BgL_arg3744z00_8646);
						BgL_arg3743z00_8645 = BGL_CLASS_NUM(BgL_tmpz00_11483);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_returnz00_bglt) BgL_o1633z00_7198)), BgL_arg3743z00_8645);
			}
			{	/* Liveness/types.scm 209 */
				BgL_objectz00_bglt BgL_tmpz00_11489;

				BgL_tmpz00_11489 =
					((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_o1633z00_7198));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11489, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_o1633z00_7198));
			return ((BgL_returnz00_bglt) ((BgL_returnz00_bglt) BgL_o1633z00_7198));
		}

	}



/* &<@anonymous:3740> */
	obj_t BGl_z62zc3z04anonymousza33740ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7199, obj_t BgL_new1632z00_7200)
	{
		{	/* Liveness/types.scm 209 */
			{
				BgL_returnz00_bglt BgL_auxz00_11497;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_returnz00_bglt) BgL_new1632z00_7200))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11501;

					{	/* Liveness/types.scm 209 */
						obj_t BgL_classz00_8652;

						BgL_classz00_8652 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 209 */
							obj_t BgL__ortest_1117z00_8653;

							BgL__ortest_1117z00_8653 = BGL_CLASS_NIL(BgL_classz00_8652);
							if (CBOOL(BgL__ortest_1117z00_8653))
								{	/* Liveness/types.scm 209 */
									BgL_auxz00_11501 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8653);
								}
							else
								{	/* Liveness/types.scm 209 */
									BgL_auxz00_11501 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8652));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_returnz00_bglt) BgL_new1632z00_7200))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_11501), BUNSPEC);
				}
				{
					BgL_retblockz00_bglt BgL_auxz00_11511;

					{	/* Liveness/types.scm 209 */
						obj_t BgL_classz00_8654;

						BgL_classz00_8654 = BGl_retblockz00zzast_nodez00;
						{	/* Liveness/types.scm 209 */
							obj_t BgL__ortest_1117z00_8655;

							BgL__ortest_1117z00_8655 = BGL_CLASS_NIL(BgL_classz00_8654);
							if (CBOOL(BgL__ortest_1117z00_8655))
								{	/* Liveness/types.scm 209 */
									BgL_auxz00_11511 =
										((BgL_retblockz00_bglt) BgL__ortest_1117z00_8655);
								}
							else
								{	/* Liveness/types.scm 209 */
									BgL_auxz00_11511 =
										((BgL_retblockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8654));
								}
						}
					}
					((((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt)
										((BgL_returnz00_bglt) BgL_new1632z00_7200))))->
							BgL_blockz00) =
						((BgL_retblockz00_bglt) BgL_auxz00_11511), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_11521;

					{	/* Liveness/types.scm 209 */
						obj_t BgL_classz00_8656;

						BgL_classz00_8656 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 209 */
							obj_t BgL__ortest_1117z00_8657;

							BgL__ortest_1117z00_8657 = BGL_CLASS_NIL(BgL_classz00_8656);
							if (CBOOL(BgL__ortest_1117z00_8657))
								{	/* Liveness/types.scm 209 */
									BgL_auxz00_11521 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8657);
								}
							else
								{	/* Liveness/types.scm 209 */
									BgL_auxz00_11521 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8656));
								}
						}
					}
					((((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt)
										((BgL_returnz00_bglt) BgL_new1632z00_7200))))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_11521), BUNSPEC);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_11531;

					{
						obj_t BgL_auxz00_11532;

						{	/* Liveness/types.scm 209 */
							BgL_objectz00_bglt BgL_tmpz00_11533;

							BgL_tmpz00_11533 =
								((BgL_objectz00_bglt)
								((BgL_returnz00_bglt) BgL_new1632z00_7200));
							BgL_auxz00_11532 = BGL_OBJECT_WIDENING(BgL_tmpz00_11533);
						}
						BgL_auxz00_11531 =
							((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11532);
					}
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11531))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_11539;

					{
						obj_t BgL_auxz00_11540;

						{	/* Liveness/types.scm 209 */
							BgL_objectz00_bglt BgL_tmpz00_11541;

							BgL_tmpz00_11541 =
								((BgL_objectz00_bglt)
								((BgL_returnz00_bglt) BgL_new1632z00_7200));
							BgL_auxz00_11540 = BGL_OBJECT_WIDENING(BgL_tmpz00_11541);
						}
						BgL_auxz00_11539 =
							((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11540);
					}
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11539))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_11547;

					{
						obj_t BgL_auxz00_11548;

						{	/* Liveness/types.scm 209 */
							BgL_objectz00_bglt BgL_tmpz00_11549;

							BgL_tmpz00_11549 =
								((BgL_objectz00_bglt)
								((BgL_returnz00_bglt) BgL_new1632z00_7200));
							BgL_auxz00_11548 = BGL_OBJECT_WIDENING(BgL_tmpz00_11549);
						}
						BgL_auxz00_11547 =
							((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11548);
					}
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11547))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_11555;

					{
						obj_t BgL_auxz00_11556;

						{	/* Liveness/types.scm 209 */
							BgL_objectz00_bglt BgL_tmpz00_11557;

							BgL_tmpz00_11557 =
								((BgL_objectz00_bglt)
								((BgL_returnz00_bglt) BgL_new1632z00_7200));
							BgL_auxz00_11556 = BGL_OBJECT_WIDENING(BgL_tmpz00_11557);
						}
						BgL_auxz00_11555 =
							((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11556);
					}
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11555))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_11497 = ((BgL_returnz00_bglt) BgL_new1632z00_7200);
				return ((obj_t) BgL_auxz00_11497);
			}
		}

	}



/* &lambda3738 */
	BgL_returnz00_bglt BGl_z62lambda3738z62zzliveness_typesz00(obj_t
		BgL_envz00_7201, obj_t BgL_o1629z00_7202)
	{
		{	/* Liveness/types.scm 209 */
			{	/* Liveness/types.scm 209 */
				BgL_returnzf2livenesszf2_bglt BgL_wide1631z00_8659;

				BgL_wide1631z00_8659 =
					((BgL_returnzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_returnzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 209 */
					obj_t BgL_auxz00_11570;
					BgL_objectz00_bglt BgL_tmpz00_11566;

					BgL_auxz00_11570 = ((obj_t) BgL_wide1631z00_8659);
					BgL_tmpz00_11566 =
						((BgL_objectz00_bglt)
						((BgL_returnz00_bglt) ((BgL_returnz00_bglt) BgL_o1629z00_7202)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11566, BgL_auxz00_11570);
				}
				((BgL_objectz00_bglt)
					((BgL_returnz00_bglt) ((BgL_returnz00_bglt) BgL_o1629z00_7202)));
				{	/* Liveness/types.scm 209 */
					long BgL_arg3739z00_8660;

					BgL_arg3739z00_8660 =
						BGL_CLASS_NUM(BGl_returnzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_returnz00_bglt)
								((BgL_returnz00_bglt) BgL_o1629z00_7202))),
						BgL_arg3739z00_8660);
				}
				return
					((BgL_returnz00_bglt)
					((BgL_returnz00_bglt) ((BgL_returnz00_bglt) BgL_o1629z00_7202)));
			}
		}

	}



/* &lambda3734 */
	BgL_returnz00_bglt BGl_z62lambda3734z62zzliveness_typesz00(obj_t
		BgL_envz00_7203, obj_t BgL_loc1621z00_7204, obj_t BgL_type1622z00_7205,
		obj_t BgL_block1623z00_7206, obj_t BgL_value1624z00_7207,
		obj_t BgL_def1625z00_7208, obj_t BgL_use1626z00_7209,
		obj_t BgL_in1627z00_7210, obj_t BgL_out1628z00_7211)
	{
		{	/* Liveness/types.scm 209 */
			{	/* Liveness/types.scm 209 */
				BgL_returnz00_bglt BgL_new1840z00_8668;

				{	/* Liveness/types.scm 209 */
					BgL_returnz00_bglt BgL_tmp1838z00_8669;
					BgL_returnzf2livenesszf2_bglt BgL_wide1839z00_8670;

					{
						BgL_returnz00_bglt BgL_auxz00_11584;

						{	/* Liveness/types.scm 209 */
							BgL_returnz00_bglt BgL_new1837z00_8671;

							BgL_new1837z00_8671 =
								((BgL_returnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_returnz00_bgl))));
							{	/* Liveness/types.scm 209 */
								long BgL_arg3737z00_8672;

								BgL_arg3737z00_8672 = BGL_CLASS_NUM(BGl_returnz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1837z00_8671),
									BgL_arg3737z00_8672);
							}
							{	/* Liveness/types.scm 209 */
								BgL_objectz00_bglt BgL_tmpz00_11589;

								BgL_tmpz00_11589 = ((BgL_objectz00_bglt) BgL_new1837z00_8671);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11589, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1837z00_8671);
							BgL_auxz00_11584 = BgL_new1837z00_8671;
						}
						BgL_tmp1838z00_8669 = ((BgL_returnz00_bglt) BgL_auxz00_11584);
					}
					BgL_wide1839z00_8670 =
						((BgL_returnzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_returnzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 209 */
						obj_t BgL_auxz00_11597;
						BgL_objectz00_bglt BgL_tmpz00_11595;

						BgL_auxz00_11597 = ((obj_t) BgL_wide1839z00_8670);
						BgL_tmpz00_11595 = ((BgL_objectz00_bglt) BgL_tmp1838z00_8669);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11595, BgL_auxz00_11597);
					}
					((BgL_objectz00_bglt) BgL_tmp1838z00_8669);
					{	/* Liveness/types.scm 209 */
						long BgL_arg3736z00_8673;

						BgL_arg3736z00_8673 =
							BGL_CLASS_NUM(BGl_returnzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1838z00_8669), BgL_arg3736z00_8673);
					}
					BgL_new1840z00_8668 = ((BgL_returnz00_bglt) BgL_tmp1838z00_8669);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1840z00_8668)))->BgL_locz00) =
					((obj_t) BgL_loc1621z00_7204), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1840z00_8668)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1622z00_7205)),
					BUNSPEC);
				((((BgL_returnz00_bglt) COBJECT(((BgL_returnz00_bglt)
									BgL_new1840z00_8668)))->BgL_blockz00) =
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt)
							BgL_block1623z00_7206)), BUNSPEC);
				((((BgL_returnz00_bglt) COBJECT(((BgL_returnz00_bglt)
									BgL_new1840z00_8668)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_value1624z00_7207)),
					BUNSPEC);
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_11616;

					{
						obj_t BgL_auxz00_11617;

						{	/* Liveness/types.scm 209 */
							BgL_objectz00_bglt BgL_tmpz00_11618;

							BgL_tmpz00_11618 = ((BgL_objectz00_bglt) BgL_new1840z00_8668);
							BgL_auxz00_11617 = BGL_OBJECT_WIDENING(BgL_tmpz00_11618);
						}
						BgL_auxz00_11616 =
							((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11617);
					}
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11616))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1625z00_7208)), BUNSPEC);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_11624;

					{
						obj_t BgL_auxz00_11625;

						{	/* Liveness/types.scm 209 */
							BgL_objectz00_bglt BgL_tmpz00_11626;

							BgL_tmpz00_11626 = ((BgL_objectz00_bglt) BgL_new1840z00_8668);
							BgL_auxz00_11625 = BGL_OBJECT_WIDENING(BgL_tmpz00_11626);
						}
						BgL_auxz00_11624 =
							((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11625);
					}
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11624))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1626z00_7209)), BUNSPEC);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_11632;

					{
						obj_t BgL_auxz00_11633;

						{	/* Liveness/types.scm 209 */
							BgL_objectz00_bglt BgL_tmpz00_11634;

							BgL_tmpz00_11634 = ((BgL_objectz00_bglt) BgL_new1840z00_8668);
							BgL_auxz00_11633 = BGL_OBJECT_WIDENING(BgL_tmpz00_11634);
						}
						BgL_auxz00_11632 =
							((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11633);
					}
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11632))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1627z00_7210)), BUNSPEC);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_11640;

					{
						obj_t BgL_auxz00_11641;

						{	/* Liveness/types.scm 209 */
							BgL_objectz00_bglt BgL_tmpz00_11642;

							BgL_tmpz00_11642 = ((BgL_objectz00_bglt) BgL_new1840z00_8668);
							BgL_auxz00_11641 = BGL_OBJECT_WIDENING(BgL_tmpz00_11642);
						}
						BgL_auxz00_11640 =
							((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11641);
					}
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11640))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1628z00_7211)), BUNSPEC);
				}
				return BgL_new1840z00_8668;
			}
		}

	}



/* &<@anonymous:3777> */
	obj_t BGl_z62zc3z04anonymousza33777ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7212)
	{
		{	/* Liveness/types.scm 209 */
			return BNIL;
		}

	}



/* &lambda3776 */
	obj_t BGl_z62lambda3776z62zzliveness_typesz00(obj_t BgL_envz00_7213,
		obj_t BgL_oz00_7214, obj_t BgL_vz00_7215)
	{
		{	/* Liveness/types.scm 209 */
			{
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_11648;

				{
					obj_t BgL_auxz00_11649;

					{	/* Liveness/types.scm 209 */
						BgL_objectz00_bglt BgL_tmpz00_11650;

						BgL_tmpz00_11650 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_oz00_7214));
						BgL_auxz00_11649 = BGL_OBJECT_WIDENING(BgL_tmpz00_11650);
					}
					BgL_auxz00_11648 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11649);
				}
				return
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11648))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7215)), BUNSPEC);
			}
		}

	}



/* &lambda3775 */
	obj_t BGl_z62lambda3775z62zzliveness_typesz00(obj_t BgL_envz00_7216,
		obj_t BgL_oz00_7217)
	{
		{	/* Liveness/types.scm 209 */
			{
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_11657;

				{
					obj_t BgL_auxz00_11658;

					{	/* Liveness/types.scm 209 */
						BgL_objectz00_bglt BgL_tmpz00_11659;

						BgL_tmpz00_11659 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_oz00_7217));
						BgL_auxz00_11658 = BGL_OBJECT_WIDENING(BgL_tmpz00_11659);
					}
					BgL_auxz00_11657 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11658);
				}
				return
					(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11657))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3770> */
	obj_t BGl_z62zc3z04anonymousza33770ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7218)
	{
		{	/* Liveness/types.scm 209 */
			return BNIL;
		}

	}



/* &lambda3769 */
	obj_t BGl_z62lambda3769z62zzliveness_typesz00(obj_t BgL_envz00_7219,
		obj_t BgL_oz00_7220, obj_t BgL_vz00_7221)
	{
		{	/* Liveness/types.scm 209 */
			{
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_11665;

				{
					obj_t BgL_auxz00_11666;

					{	/* Liveness/types.scm 209 */
						BgL_objectz00_bglt BgL_tmpz00_11667;

						BgL_tmpz00_11667 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_oz00_7220));
						BgL_auxz00_11666 = BGL_OBJECT_WIDENING(BgL_tmpz00_11667);
					}
					BgL_auxz00_11665 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11666);
				}
				return
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11665))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7221)), BUNSPEC);
			}
		}

	}



/* &lambda3768 */
	obj_t BGl_z62lambda3768z62zzliveness_typesz00(obj_t BgL_envz00_7222,
		obj_t BgL_oz00_7223)
	{
		{	/* Liveness/types.scm 209 */
			{
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_11674;

				{
					obj_t BgL_auxz00_11675;

					{	/* Liveness/types.scm 209 */
						BgL_objectz00_bglt BgL_tmpz00_11676;

						BgL_tmpz00_11676 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_oz00_7223));
						BgL_auxz00_11675 = BGL_OBJECT_WIDENING(BgL_tmpz00_11676);
					}
					BgL_auxz00_11674 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11675);
				}
				return
					(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11674))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3762> */
	obj_t BGl_z62zc3z04anonymousza33762ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7224)
	{
		{	/* Liveness/types.scm 209 */
			return BNIL;
		}

	}



/* &lambda3761 */
	obj_t BGl_z62lambda3761z62zzliveness_typesz00(obj_t BgL_envz00_7225,
		obj_t BgL_oz00_7226, obj_t BgL_vz00_7227)
	{
		{	/* Liveness/types.scm 209 */
			{
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_11682;

				{
					obj_t BgL_auxz00_11683;

					{	/* Liveness/types.scm 209 */
						BgL_objectz00_bglt BgL_tmpz00_11684;

						BgL_tmpz00_11684 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_oz00_7226));
						BgL_auxz00_11683 = BGL_OBJECT_WIDENING(BgL_tmpz00_11684);
					}
					BgL_auxz00_11682 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11683);
				}
				return
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11682))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7227)), BUNSPEC);
			}
		}

	}



/* &lambda3760 */
	obj_t BGl_z62lambda3760z62zzliveness_typesz00(obj_t BgL_envz00_7228,
		obj_t BgL_oz00_7229)
	{
		{	/* Liveness/types.scm 209 */
			{
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_11691;

				{
					obj_t BgL_auxz00_11692;

					{	/* Liveness/types.scm 209 */
						BgL_objectz00_bglt BgL_tmpz00_11693;

						BgL_tmpz00_11693 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_oz00_7229));
						BgL_auxz00_11692 = BGL_OBJECT_WIDENING(BgL_tmpz00_11693);
					}
					BgL_auxz00_11691 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11692);
				}
				return
					(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11691))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3753> */
	obj_t BGl_z62zc3z04anonymousza33753ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7230)
	{
		{	/* Liveness/types.scm 209 */
			return BNIL;
		}

	}



/* &lambda3752 */
	obj_t BGl_z62lambda3752z62zzliveness_typesz00(obj_t BgL_envz00_7231,
		obj_t BgL_oz00_7232, obj_t BgL_vz00_7233)
	{
		{	/* Liveness/types.scm 209 */
			{
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_11699;

				{
					obj_t BgL_auxz00_11700;

					{	/* Liveness/types.scm 209 */
						BgL_objectz00_bglt BgL_tmpz00_11701;

						BgL_tmpz00_11701 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_oz00_7232));
						BgL_auxz00_11700 = BGL_OBJECT_WIDENING(BgL_tmpz00_11701);
					}
					BgL_auxz00_11699 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11700);
				}
				return
					((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11699))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7233)), BUNSPEC);
			}
		}

	}



/* &lambda3751 */
	obj_t BGl_z62lambda3751z62zzliveness_typesz00(obj_t BgL_envz00_7234,
		obj_t BgL_oz00_7235)
	{
		{	/* Liveness/types.scm 209 */
			{
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_11708;

				{
					obj_t BgL_auxz00_11709;

					{	/* Liveness/types.scm 209 */
						BgL_objectz00_bglt BgL_tmpz00_11710;

						BgL_tmpz00_11710 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_oz00_7235));
						BgL_auxz00_11709 = BGL_OBJECT_WIDENING(BgL_tmpz00_11710);
					}
					BgL_auxz00_11708 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_11709);
				}
				return
					(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11708))->
					BgL_defz00);
			}
		}

	}



/* &lambda3692 */
	BgL_retblockz00_bglt BGl_z62lambda3692z62zzliveness_typesz00(obj_t
		BgL_envz00_7236, obj_t BgL_o1619z00_7237)
	{
		{	/* Liveness/types.scm 203 */
			{	/* Liveness/types.scm 203 */
				long BgL_arg3693z00_8687;

				{	/* Liveness/types.scm 203 */
					obj_t BgL_arg3694z00_8688;

					{	/* Liveness/types.scm 203 */
						obj_t BgL_arg3695z00_8689;

						{	/* Liveness/types.scm 203 */
							obj_t BgL_arg1815z00_8690;
							long BgL_arg1816z00_8691;

							BgL_arg1815z00_8690 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 203 */
								long BgL_arg1817z00_8692;

								BgL_arg1817z00_8692 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt) BgL_o1619z00_7237)));
								BgL_arg1816z00_8691 = (BgL_arg1817z00_8692 - OBJECT_TYPE);
							}
							BgL_arg3695z00_8689 =
								VECTOR_REF(BgL_arg1815z00_8690, BgL_arg1816z00_8691);
						}
						BgL_arg3694z00_8688 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3695z00_8689);
					}
					{	/* Liveness/types.scm 203 */
						obj_t BgL_tmpz00_11723;

						BgL_tmpz00_11723 = ((obj_t) BgL_arg3694z00_8688);
						BgL_arg3693z00_8687 = BGL_CLASS_NUM(BgL_tmpz00_11723);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_retblockz00_bglt) BgL_o1619z00_7237)), BgL_arg3693z00_8687);
			}
			{	/* Liveness/types.scm 203 */
				BgL_objectz00_bglt BgL_tmpz00_11729;

				BgL_tmpz00_11729 =
					((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_o1619z00_7237));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11729, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_o1619z00_7237));
			return
				((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1619z00_7237));
		}

	}



/* &<@anonymous:3691> */
	obj_t BGl_z62zc3z04anonymousza33691ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7238, obj_t BgL_new1618z00_7239)
	{
		{	/* Liveness/types.scm 203 */
			{
				BgL_retblockz00_bglt BgL_auxz00_11737;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_retblockz00_bglt) BgL_new1618z00_7239))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11741;

					{	/* Liveness/types.scm 203 */
						obj_t BgL_classz00_8694;

						BgL_classz00_8694 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 203 */
							obj_t BgL__ortest_1117z00_8695;

							BgL__ortest_1117z00_8695 = BGL_CLASS_NIL(BgL_classz00_8694);
							if (CBOOL(BgL__ortest_1117z00_8695))
								{	/* Liveness/types.scm 203 */
									BgL_auxz00_11741 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8695);
								}
							else
								{	/* Liveness/types.scm 203 */
									BgL_auxz00_11741 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8694));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_retblockz00_bglt) BgL_new1618z00_7239))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_11741), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_11751;

					{	/* Liveness/types.scm 203 */
						obj_t BgL_classz00_8696;

						BgL_classz00_8696 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 203 */
							obj_t BgL__ortest_1117z00_8697;

							BgL__ortest_1117z00_8697 = BGL_CLASS_NIL(BgL_classz00_8696);
							if (CBOOL(BgL__ortest_1117z00_8697))
								{	/* Liveness/types.scm 203 */
									BgL_auxz00_11751 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8697);
								}
							else
								{	/* Liveness/types.scm 203 */
									BgL_auxz00_11751 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8696));
								}
						}
					}
					((((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_new1618z00_7239))))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_11751), BUNSPEC);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11761;

					{
						obj_t BgL_auxz00_11762;

						{	/* Liveness/types.scm 203 */
							BgL_objectz00_bglt BgL_tmpz00_11763;

							BgL_tmpz00_11763 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_new1618z00_7239));
							BgL_auxz00_11762 = BGL_OBJECT_WIDENING(BgL_tmpz00_11763);
						}
						BgL_auxz00_11761 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11762);
					}
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11761))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11769;

					{
						obj_t BgL_auxz00_11770;

						{	/* Liveness/types.scm 203 */
							BgL_objectz00_bglt BgL_tmpz00_11771;

							BgL_tmpz00_11771 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_new1618z00_7239));
							BgL_auxz00_11770 = BGL_OBJECT_WIDENING(BgL_tmpz00_11771);
						}
						BgL_auxz00_11769 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11770);
					}
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11769))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11777;

					{
						obj_t BgL_auxz00_11778;

						{	/* Liveness/types.scm 203 */
							BgL_objectz00_bglt BgL_tmpz00_11779;

							BgL_tmpz00_11779 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_new1618z00_7239));
							BgL_auxz00_11778 = BGL_OBJECT_WIDENING(BgL_tmpz00_11779);
						}
						BgL_auxz00_11777 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11778);
					}
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11777))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11785;

					{
						obj_t BgL_auxz00_11786;

						{	/* Liveness/types.scm 203 */
							BgL_objectz00_bglt BgL_tmpz00_11787;

							BgL_tmpz00_11787 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_new1618z00_7239));
							BgL_auxz00_11786 = BGL_OBJECT_WIDENING(BgL_tmpz00_11787);
						}
						BgL_auxz00_11785 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11786);
					}
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11785))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_11737 = ((BgL_retblockz00_bglt) BgL_new1618z00_7239);
				return ((obj_t) BgL_auxz00_11737);
			}
		}

	}



/* &lambda3688 */
	BgL_retblockz00_bglt BGl_z62lambda3688z62zzliveness_typesz00(obj_t
		BgL_envz00_7240, obj_t BgL_o1615z00_7241)
	{
		{	/* Liveness/types.scm 203 */
			{	/* Liveness/types.scm 203 */
				BgL_retblockzf2livenesszf2_bglt BgL_wide1617z00_8699;

				BgL_wide1617z00_8699 =
					((BgL_retblockzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_retblockzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 203 */
					obj_t BgL_auxz00_11800;
					BgL_objectz00_bglt BgL_tmpz00_11796;

					BgL_auxz00_11800 = ((obj_t) BgL_wide1617z00_8699);
					BgL_tmpz00_11796 =
						((BgL_objectz00_bglt)
						((BgL_retblockz00_bglt)
							((BgL_retblockz00_bglt) BgL_o1615z00_7241)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11796, BgL_auxz00_11800);
				}
				((BgL_objectz00_bglt)
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1615z00_7241)));
				{	/* Liveness/types.scm 203 */
					long BgL_arg3690z00_8700;

					BgL_arg3690z00_8700 =
						BGL_CLASS_NUM(BGl_retblockzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_retblockz00_bglt)
								((BgL_retblockz00_bglt) BgL_o1615z00_7241))),
						BgL_arg3690z00_8700);
				}
				return
					((BgL_retblockz00_bglt)
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1615z00_7241)));
			}
		}

	}



/* &lambda3685 */
	BgL_retblockz00_bglt BGl_z62lambda3685z62zzliveness_typesz00(obj_t
		BgL_envz00_7242, obj_t BgL_loc1608z00_7243, obj_t BgL_type1609z00_7244,
		obj_t BgL_body1610z00_7245, obj_t BgL_def1611z00_7246,
		obj_t BgL_use1612z00_7247, obj_t BgL_in1613z00_7248,
		obj_t BgL_out1614z00_7249)
	{
		{	/* Liveness/types.scm 203 */
			{	/* Liveness/types.scm 203 */
				BgL_retblockz00_bglt BgL_new1835z00_8707;

				{	/* Liveness/types.scm 203 */
					BgL_retblockz00_bglt BgL_tmp1833z00_8708;
					BgL_retblockzf2livenesszf2_bglt BgL_wide1834z00_8709;

					{
						BgL_retblockz00_bglt BgL_auxz00_11814;

						{	/* Liveness/types.scm 203 */
							BgL_retblockz00_bglt BgL_new1832z00_8710;

							BgL_new1832z00_8710 =
								((BgL_retblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_retblockz00_bgl))));
							{	/* Liveness/types.scm 203 */
								long BgL_arg3687z00_8711;

								BgL_arg3687z00_8711 =
									BGL_CLASS_NUM(BGl_retblockz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1832z00_8710),
									BgL_arg3687z00_8711);
							}
							{	/* Liveness/types.scm 203 */
								BgL_objectz00_bglt BgL_tmpz00_11819;

								BgL_tmpz00_11819 = ((BgL_objectz00_bglt) BgL_new1832z00_8710);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11819, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1832z00_8710);
							BgL_auxz00_11814 = BgL_new1832z00_8710;
						}
						BgL_tmp1833z00_8708 = ((BgL_retblockz00_bglt) BgL_auxz00_11814);
					}
					BgL_wide1834z00_8709 =
						((BgL_retblockzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_retblockzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 203 */
						obj_t BgL_auxz00_11827;
						BgL_objectz00_bglt BgL_tmpz00_11825;

						BgL_auxz00_11827 = ((obj_t) BgL_wide1834z00_8709);
						BgL_tmpz00_11825 = ((BgL_objectz00_bglt) BgL_tmp1833z00_8708);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11825, BgL_auxz00_11827);
					}
					((BgL_objectz00_bglt) BgL_tmp1833z00_8708);
					{	/* Liveness/types.scm 203 */
						long BgL_arg3686z00_8712;

						BgL_arg3686z00_8712 =
							BGL_CLASS_NUM(BGl_retblockzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1833z00_8708), BgL_arg3686z00_8712);
					}
					BgL_new1835z00_8707 = ((BgL_retblockz00_bglt) BgL_tmp1833z00_8708);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1835z00_8707)))->BgL_locz00) =
					((obj_t) BgL_loc1608z00_7243), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1835z00_8707)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1609z00_7244)),
					BUNSPEC);
				((((BgL_retblockz00_bglt) COBJECT(((BgL_retblockz00_bglt)
									BgL_new1835z00_8707)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1610z00_7245)),
					BUNSPEC);
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11843;

					{
						obj_t BgL_auxz00_11844;

						{	/* Liveness/types.scm 203 */
							BgL_objectz00_bglt BgL_tmpz00_11845;

							BgL_tmpz00_11845 = ((BgL_objectz00_bglt) BgL_new1835z00_8707);
							BgL_auxz00_11844 = BGL_OBJECT_WIDENING(BgL_tmpz00_11845);
						}
						BgL_auxz00_11843 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11844);
					}
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11843))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1611z00_7246)), BUNSPEC);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11851;

					{
						obj_t BgL_auxz00_11852;

						{	/* Liveness/types.scm 203 */
							BgL_objectz00_bglt BgL_tmpz00_11853;

							BgL_tmpz00_11853 = ((BgL_objectz00_bglt) BgL_new1835z00_8707);
							BgL_auxz00_11852 = BGL_OBJECT_WIDENING(BgL_tmpz00_11853);
						}
						BgL_auxz00_11851 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11852);
					}
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11851))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1612z00_7247)), BUNSPEC);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11859;

					{
						obj_t BgL_auxz00_11860;

						{	/* Liveness/types.scm 203 */
							BgL_objectz00_bglt BgL_tmpz00_11861;

							BgL_tmpz00_11861 = ((BgL_objectz00_bglt) BgL_new1835z00_8707);
							BgL_auxz00_11860 = BGL_OBJECT_WIDENING(BgL_tmpz00_11861);
						}
						BgL_auxz00_11859 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11860);
					}
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11859))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1613z00_7248)), BUNSPEC);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11867;

					{
						obj_t BgL_auxz00_11868;

						{	/* Liveness/types.scm 203 */
							BgL_objectz00_bglt BgL_tmpz00_11869;

							BgL_tmpz00_11869 = ((BgL_objectz00_bglt) BgL_new1835z00_8707);
							BgL_auxz00_11868 = BGL_OBJECT_WIDENING(BgL_tmpz00_11869);
						}
						BgL_auxz00_11867 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11868);
					}
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11867))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1614z00_7249)), BUNSPEC);
				}
				return BgL_new1835z00_8707;
			}
		}

	}



/* &<@anonymous:3726> */
	obj_t BGl_z62zc3z04anonymousza33726ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7250)
	{
		{	/* Liveness/types.scm 203 */
			return BNIL;
		}

	}



/* &lambda3725 */
	obj_t BGl_z62lambda3725z62zzliveness_typesz00(obj_t BgL_envz00_7251,
		obj_t BgL_oz00_7252, obj_t BgL_vz00_7253)
	{
		{	/* Liveness/types.scm 203 */
			{
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11875;

				{
					obj_t BgL_auxz00_11876;

					{	/* Liveness/types.scm 203 */
						BgL_objectz00_bglt BgL_tmpz00_11877;

						BgL_tmpz00_11877 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_7252));
						BgL_auxz00_11876 = BGL_OBJECT_WIDENING(BgL_tmpz00_11877);
					}
					BgL_auxz00_11875 =
						((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11876);
				}
				return
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11875))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7253)), BUNSPEC);
			}
		}

	}



/* &lambda3724 */
	obj_t BGl_z62lambda3724z62zzliveness_typesz00(obj_t BgL_envz00_7254,
		obj_t BgL_oz00_7255)
	{
		{	/* Liveness/types.scm 203 */
			{
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11884;

				{
					obj_t BgL_auxz00_11885;

					{	/* Liveness/types.scm 203 */
						BgL_objectz00_bglt BgL_tmpz00_11886;

						BgL_tmpz00_11886 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_7255));
						BgL_auxz00_11885 = BGL_OBJECT_WIDENING(BgL_tmpz00_11886);
					}
					BgL_auxz00_11884 =
						((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11885);
				}
				return
					(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11884))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3719> */
	obj_t BGl_z62zc3z04anonymousza33719ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7256)
	{
		{	/* Liveness/types.scm 203 */
			return BNIL;
		}

	}



/* &lambda3718 */
	obj_t BGl_z62lambda3718z62zzliveness_typesz00(obj_t BgL_envz00_7257,
		obj_t BgL_oz00_7258, obj_t BgL_vz00_7259)
	{
		{	/* Liveness/types.scm 203 */
			{
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11892;

				{
					obj_t BgL_auxz00_11893;

					{	/* Liveness/types.scm 203 */
						BgL_objectz00_bglt BgL_tmpz00_11894;

						BgL_tmpz00_11894 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_7258));
						BgL_auxz00_11893 = BGL_OBJECT_WIDENING(BgL_tmpz00_11894);
					}
					BgL_auxz00_11892 =
						((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11893);
				}
				return
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11892))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7259)), BUNSPEC);
			}
		}

	}



/* &lambda3717 */
	obj_t BGl_z62lambda3717z62zzliveness_typesz00(obj_t BgL_envz00_7260,
		obj_t BgL_oz00_7261)
	{
		{	/* Liveness/types.scm 203 */
			{
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11901;

				{
					obj_t BgL_auxz00_11902;

					{	/* Liveness/types.scm 203 */
						BgL_objectz00_bglt BgL_tmpz00_11903;

						BgL_tmpz00_11903 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_7261));
						BgL_auxz00_11902 = BGL_OBJECT_WIDENING(BgL_tmpz00_11903);
					}
					BgL_auxz00_11901 =
						((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11902);
				}
				return
					(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11901))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3710> */
	obj_t BGl_z62zc3z04anonymousza33710ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7262)
	{
		{	/* Liveness/types.scm 203 */
			return BNIL;
		}

	}



/* &lambda3709 */
	obj_t BGl_z62lambda3709z62zzliveness_typesz00(obj_t BgL_envz00_7263,
		obj_t BgL_oz00_7264, obj_t BgL_vz00_7265)
	{
		{	/* Liveness/types.scm 203 */
			{
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11909;

				{
					obj_t BgL_auxz00_11910;

					{	/* Liveness/types.scm 203 */
						BgL_objectz00_bglt BgL_tmpz00_11911;

						BgL_tmpz00_11911 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_7264));
						BgL_auxz00_11910 = BGL_OBJECT_WIDENING(BgL_tmpz00_11911);
					}
					BgL_auxz00_11909 =
						((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11910);
				}
				return
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11909))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7265)), BUNSPEC);
			}
		}

	}



/* &lambda3708 */
	obj_t BGl_z62lambda3708z62zzliveness_typesz00(obj_t BgL_envz00_7266,
		obj_t BgL_oz00_7267)
	{
		{	/* Liveness/types.scm 203 */
			{
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11918;

				{
					obj_t BgL_auxz00_11919;

					{	/* Liveness/types.scm 203 */
						BgL_objectz00_bglt BgL_tmpz00_11920;

						BgL_tmpz00_11920 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_7267));
						BgL_auxz00_11919 = BGL_OBJECT_WIDENING(BgL_tmpz00_11920);
					}
					BgL_auxz00_11918 =
						((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11919);
				}
				return
					(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11918))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3703> */
	obj_t BGl_z62zc3z04anonymousza33703ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7268)
	{
		{	/* Liveness/types.scm 203 */
			return BNIL;
		}

	}



/* &lambda3702 */
	obj_t BGl_z62lambda3702z62zzliveness_typesz00(obj_t BgL_envz00_7269,
		obj_t BgL_oz00_7270, obj_t BgL_vz00_7271)
	{
		{	/* Liveness/types.scm 203 */
			{
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11926;

				{
					obj_t BgL_auxz00_11927;

					{	/* Liveness/types.scm 203 */
						BgL_objectz00_bglt BgL_tmpz00_11928;

						BgL_tmpz00_11928 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_7270));
						BgL_auxz00_11927 = BGL_OBJECT_WIDENING(BgL_tmpz00_11928);
					}
					BgL_auxz00_11926 =
						((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11927);
				}
				return
					((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11926))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7271)), BUNSPEC);
			}
		}

	}



/* &lambda3701 */
	obj_t BGl_z62lambda3701z62zzliveness_typesz00(obj_t BgL_envz00_7272,
		obj_t BgL_oz00_7273)
	{
		{	/* Liveness/types.scm 203 */
			{
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_11935;

				{
					obj_t BgL_auxz00_11936;

					{	/* Liveness/types.scm 203 */
						BgL_objectz00_bglt BgL_tmpz00_11937;

						BgL_tmpz00_11937 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_7273));
						BgL_auxz00_11936 = BGL_OBJECT_WIDENING(BgL_tmpz00_11937);
					}
					BgL_auxz00_11935 =
						((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_11936);
				}
				return
					(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11935))->
					BgL_defz00);
			}
		}

	}



/* &lambda3639 */
	BgL_syncz00_bglt BGl_z62lambda3639z62zzliveness_typesz00(obj_t
		BgL_envz00_7274, obj_t BgL_o1605z00_7275)
	{
		{	/* Liveness/types.scm 197 */
			{	/* Liveness/types.scm 197 */
				long BgL_arg3640z00_8726;

				{	/* Liveness/types.scm 197 */
					obj_t BgL_arg3641z00_8727;

					{	/* Liveness/types.scm 197 */
						obj_t BgL_arg3643z00_8728;

						{	/* Liveness/types.scm 197 */
							obj_t BgL_arg1815z00_8729;
							long BgL_arg1816z00_8730;

							BgL_arg1815z00_8729 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 197 */
								long BgL_arg1817z00_8731;

								BgL_arg1817z00_8731 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_syncz00_bglt) BgL_o1605z00_7275)));
								BgL_arg1816z00_8730 = (BgL_arg1817z00_8731 - OBJECT_TYPE);
							}
							BgL_arg3643z00_8728 =
								VECTOR_REF(BgL_arg1815z00_8729, BgL_arg1816z00_8730);
						}
						BgL_arg3641z00_8727 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3643z00_8728);
					}
					{	/* Liveness/types.scm 197 */
						obj_t BgL_tmpz00_11950;

						BgL_tmpz00_11950 = ((obj_t) BgL_arg3641z00_8727);
						BgL_arg3640z00_8726 = BGL_CLASS_NUM(BgL_tmpz00_11950);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_syncz00_bglt) BgL_o1605z00_7275)), BgL_arg3640z00_8726);
			}
			{	/* Liveness/types.scm 197 */
				BgL_objectz00_bglt BgL_tmpz00_11956;

				BgL_tmpz00_11956 =
					((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_o1605z00_7275));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11956, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_o1605z00_7275));
			return ((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_o1605z00_7275));
		}

	}



/* &<@anonymous:3638> */
	obj_t BGl_z62zc3z04anonymousza33638ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7276, obj_t BgL_new1604z00_7277)
	{
		{	/* Liveness/types.scm 197 */
			{
				BgL_syncz00_bglt BgL_auxz00_11964;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_new1604z00_7277))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11968;

					{	/* Liveness/types.scm 197 */
						obj_t BgL_classz00_8733;

						BgL_classz00_8733 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 197 */
							obj_t BgL__ortest_1117z00_8734;

							BgL__ortest_1117z00_8734 = BGL_CLASS_NIL(BgL_classz00_8733);
							if (CBOOL(BgL__ortest_1117z00_8734))
								{	/* Liveness/types.scm 197 */
									BgL_auxz00_11968 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8734);
								}
							else
								{	/* Liveness/types.scm 197 */
									BgL_auxz00_11968 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8733));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_syncz00_bglt) BgL_new1604z00_7277))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_11968), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_11978;

					{	/* Liveness/types.scm 197 */
						obj_t BgL_classz00_8735;

						BgL_classz00_8735 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 197 */
							obj_t BgL__ortest_1117z00_8736;

							BgL__ortest_1117z00_8736 = BGL_CLASS_NIL(BgL_classz00_8735);
							if (CBOOL(BgL__ortest_1117z00_8736))
								{	/* Liveness/types.scm 197 */
									BgL_auxz00_11978 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8736);
								}
							else
								{	/* Liveness/types.scm 197 */
									BgL_auxz00_11978 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8735));
								}
						}
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt)
										((BgL_syncz00_bglt) BgL_new1604z00_7277))))->BgL_mutexz00) =
						((BgL_nodez00_bglt) BgL_auxz00_11978), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_11988;

					{	/* Liveness/types.scm 197 */
						obj_t BgL_classz00_8737;

						BgL_classz00_8737 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 197 */
							obj_t BgL__ortest_1117z00_8738;

							BgL__ortest_1117z00_8738 = BGL_CLASS_NIL(BgL_classz00_8737);
							if (CBOOL(BgL__ortest_1117z00_8738))
								{	/* Liveness/types.scm 197 */
									BgL_auxz00_11988 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8738);
								}
							else
								{	/* Liveness/types.scm 197 */
									BgL_auxz00_11988 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8737));
								}
						}
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt)
										((BgL_syncz00_bglt) BgL_new1604z00_7277))))->
							BgL_prelockz00) = ((BgL_nodez00_bglt) BgL_auxz00_11988), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_11998;

					{	/* Liveness/types.scm 197 */
						obj_t BgL_classz00_8739;

						BgL_classz00_8739 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 197 */
							obj_t BgL__ortest_1117z00_8740;

							BgL__ortest_1117z00_8740 = BGL_CLASS_NIL(BgL_classz00_8739);
							if (CBOOL(BgL__ortest_1117z00_8740))
								{	/* Liveness/types.scm 197 */
									BgL_auxz00_11998 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8740);
								}
							else
								{	/* Liveness/types.scm 197 */
									BgL_auxz00_11998 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8739));
								}
						}
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt)
										((BgL_syncz00_bglt) BgL_new1604z00_7277))))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_11998), BUNSPEC);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_12008;

					{
						obj_t BgL_auxz00_12009;

						{	/* Liveness/types.scm 197 */
							BgL_objectz00_bglt BgL_tmpz00_12010;

							BgL_tmpz00_12010 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_new1604z00_7277));
							BgL_auxz00_12009 = BGL_OBJECT_WIDENING(BgL_tmpz00_12010);
						}
						BgL_auxz00_12008 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12009);
					}
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12008))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_12016;

					{
						obj_t BgL_auxz00_12017;

						{	/* Liveness/types.scm 197 */
							BgL_objectz00_bglt BgL_tmpz00_12018;

							BgL_tmpz00_12018 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_new1604z00_7277));
							BgL_auxz00_12017 = BGL_OBJECT_WIDENING(BgL_tmpz00_12018);
						}
						BgL_auxz00_12016 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12017);
					}
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12016))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_12024;

					{
						obj_t BgL_auxz00_12025;

						{	/* Liveness/types.scm 197 */
							BgL_objectz00_bglt BgL_tmpz00_12026;

							BgL_tmpz00_12026 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_new1604z00_7277));
							BgL_auxz00_12025 = BGL_OBJECT_WIDENING(BgL_tmpz00_12026);
						}
						BgL_auxz00_12024 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12025);
					}
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12024))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_12032;

					{
						obj_t BgL_auxz00_12033;

						{	/* Liveness/types.scm 197 */
							BgL_objectz00_bglt BgL_tmpz00_12034;

							BgL_tmpz00_12034 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_new1604z00_7277));
							BgL_auxz00_12033 = BGL_OBJECT_WIDENING(BgL_tmpz00_12034);
						}
						BgL_auxz00_12032 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12033);
					}
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12032))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_11964 = ((BgL_syncz00_bglt) BgL_new1604z00_7277);
				return ((obj_t) BgL_auxz00_11964);
			}
		}

	}



/* &lambda3636 */
	BgL_syncz00_bglt BGl_z62lambda3636z62zzliveness_typesz00(obj_t
		BgL_envz00_7278, obj_t BgL_o1601z00_7279)
	{
		{	/* Liveness/types.scm 197 */
			{	/* Liveness/types.scm 197 */
				BgL_synczf2livenesszf2_bglt BgL_wide1603z00_8742;

				BgL_wide1603z00_8742 =
					((BgL_synczf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_synczf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 197 */
					obj_t BgL_auxz00_12047;
					BgL_objectz00_bglt BgL_tmpz00_12043;

					BgL_auxz00_12047 = ((obj_t) BgL_wide1603z00_8742);
					BgL_tmpz00_12043 =
						((BgL_objectz00_bglt)
						((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_o1601z00_7279)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12043, BgL_auxz00_12047);
				}
				((BgL_objectz00_bglt)
					((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_o1601z00_7279)));
				{	/* Liveness/types.scm 197 */
					long BgL_arg3637z00_8743;

					BgL_arg3637z00_8743 =
						BGL_CLASS_NUM(BGl_synczf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_syncz00_bglt)
								((BgL_syncz00_bglt) BgL_o1601z00_7279))), BgL_arg3637z00_8743);
				}
				return
					((BgL_syncz00_bglt)
					((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_o1601z00_7279)));
			}
		}

	}



/* &lambda3632 */
	BgL_syncz00_bglt BGl_z62lambda3632z62zzliveness_typesz00(obj_t
		BgL_envz00_7280, obj_t BgL_loc1592z00_7281, obj_t BgL_type1593z00_7282,
		obj_t BgL_mutex1594z00_7283, obj_t BgL_prelock1595z00_7284,
		obj_t BgL_body1596z00_7285, obj_t BgL_def1597z00_7286,
		obj_t BgL_use1598z00_7287, obj_t BgL_in1599z00_7288,
		obj_t BgL_out1600z00_7289)
	{
		{	/* Liveness/types.scm 197 */
			{	/* Liveness/types.scm 197 */
				BgL_syncz00_bglt BgL_new1830z00_8752;

				{	/* Liveness/types.scm 197 */
					BgL_syncz00_bglt BgL_tmp1828z00_8753;
					BgL_synczf2livenesszf2_bglt BgL_wide1829z00_8754;

					{
						BgL_syncz00_bglt BgL_auxz00_12061;

						{	/* Liveness/types.scm 197 */
							BgL_syncz00_bglt BgL_new1827z00_8755;

							BgL_new1827z00_8755 =
								((BgL_syncz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_syncz00_bgl))));
							{	/* Liveness/types.scm 197 */
								long BgL_arg3635z00_8756;

								BgL_arg3635z00_8756 = BGL_CLASS_NUM(BGl_syncz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1827z00_8755),
									BgL_arg3635z00_8756);
							}
							{	/* Liveness/types.scm 197 */
								BgL_objectz00_bglt BgL_tmpz00_12066;

								BgL_tmpz00_12066 = ((BgL_objectz00_bglt) BgL_new1827z00_8755);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12066, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1827z00_8755);
							BgL_auxz00_12061 = BgL_new1827z00_8755;
						}
						BgL_tmp1828z00_8753 = ((BgL_syncz00_bglt) BgL_auxz00_12061);
					}
					BgL_wide1829z00_8754 =
						((BgL_synczf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_synczf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 197 */
						obj_t BgL_auxz00_12074;
						BgL_objectz00_bglt BgL_tmpz00_12072;

						BgL_auxz00_12074 = ((obj_t) BgL_wide1829z00_8754);
						BgL_tmpz00_12072 = ((BgL_objectz00_bglt) BgL_tmp1828z00_8753);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12072, BgL_auxz00_12074);
					}
					((BgL_objectz00_bglt) BgL_tmp1828z00_8753);
					{	/* Liveness/types.scm 197 */
						long BgL_arg3634z00_8757;

						BgL_arg3634z00_8757 =
							BGL_CLASS_NUM(BGl_synczf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1828z00_8753), BgL_arg3634z00_8757);
					}
					BgL_new1830z00_8752 = ((BgL_syncz00_bglt) BgL_tmp1828z00_8753);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1830z00_8752)))->BgL_locz00) =
					((obj_t) BgL_loc1592z00_7281), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1830z00_8752)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1593z00_7282)),
					BUNSPEC);
				((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt)
									BgL_new1830z00_8752)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_mutex1594z00_7283)),
					BUNSPEC);
				((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt)
									BgL_new1830z00_8752)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_prelock1595z00_7284)),
					BUNSPEC);
				((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt)
									BgL_new1830z00_8752)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1596z00_7285)),
					BUNSPEC);
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_12096;

					{
						obj_t BgL_auxz00_12097;

						{	/* Liveness/types.scm 197 */
							BgL_objectz00_bglt BgL_tmpz00_12098;

							BgL_tmpz00_12098 = ((BgL_objectz00_bglt) BgL_new1830z00_8752);
							BgL_auxz00_12097 = BGL_OBJECT_WIDENING(BgL_tmpz00_12098);
						}
						BgL_auxz00_12096 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12097);
					}
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12096))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1597z00_7286)), BUNSPEC);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_12104;

					{
						obj_t BgL_auxz00_12105;

						{	/* Liveness/types.scm 197 */
							BgL_objectz00_bglt BgL_tmpz00_12106;

							BgL_tmpz00_12106 = ((BgL_objectz00_bglt) BgL_new1830z00_8752);
							BgL_auxz00_12105 = BGL_OBJECT_WIDENING(BgL_tmpz00_12106);
						}
						BgL_auxz00_12104 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12105);
					}
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12104))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1598z00_7287)), BUNSPEC);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_12112;

					{
						obj_t BgL_auxz00_12113;

						{	/* Liveness/types.scm 197 */
							BgL_objectz00_bglt BgL_tmpz00_12114;

							BgL_tmpz00_12114 = ((BgL_objectz00_bglt) BgL_new1830z00_8752);
							BgL_auxz00_12113 = BGL_OBJECT_WIDENING(BgL_tmpz00_12114);
						}
						BgL_auxz00_12112 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12113);
					}
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12112))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1599z00_7288)), BUNSPEC);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_12120;

					{
						obj_t BgL_auxz00_12121;

						{	/* Liveness/types.scm 197 */
							BgL_objectz00_bglt BgL_tmpz00_12122;

							BgL_tmpz00_12122 = ((BgL_objectz00_bglt) BgL_new1830z00_8752);
							BgL_auxz00_12121 = BGL_OBJECT_WIDENING(BgL_tmpz00_12122);
						}
						BgL_auxz00_12120 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12121);
					}
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12120))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1600z00_7289)), BUNSPEC);
				}
				return BgL_new1830z00_8752;
			}
		}

	}



/* &<@anonymous:3675> */
	obj_t BGl_z62zc3z04anonymousza33675ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7290)
	{
		{	/* Liveness/types.scm 197 */
			return BNIL;
		}

	}



/* &lambda3674 */
	obj_t BGl_z62lambda3674z62zzliveness_typesz00(obj_t BgL_envz00_7291,
		obj_t BgL_oz00_7292, obj_t BgL_vz00_7293)
	{
		{	/* Liveness/types.scm 197 */
			{
				BgL_synczf2livenesszf2_bglt BgL_auxz00_12128;

				{
					obj_t BgL_auxz00_12129;

					{	/* Liveness/types.scm 197 */
						BgL_objectz00_bglt BgL_tmpz00_12130;

						BgL_tmpz00_12130 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_7292));
						BgL_auxz00_12129 = BGL_OBJECT_WIDENING(BgL_tmpz00_12130);
					}
					BgL_auxz00_12128 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12129);
				}
				return
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12128))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7293)), BUNSPEC);
			}
		}

	}



/* &lambda3673 */
	obj_t BGl_z62lambda3673z62zzliveness_typesz00(obj_t BgL_envz00_7294,
		obj_t BgL_oz00_7295)
	{
		{	/* Liveness/types.scm 197 */
			{
				BgL_synczf2livenesszf2_bglt BgL_auxz00_12137;

				{
					obj_t BgL_auxz00_12138;

					{	/* Liveness/types.scm 197 */
						BgL_objectz00_bglt BgL_tmpz00_12139;

						BgL_tmpz00_12139 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_7295));
						BgL_auxz00_12138 = BGL_OBJECT_WIDENING(BgL_tmpz00_12139);
					}
					BgL_auxz00_12137 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12138);
				}
				return
					(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12137))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3667> */
	obj_t BGl_z62zc3z04anonymousza33667ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7296)
	{
		{	/* Liveness/types.scm 197 */
			return BNIL;
		}

	}



/* &lambda3666 */
	obj_t BGl_z62lambda3666z62zzliveness_typesz00(obj_t BgL_envz00_7297,
		obj_t BgL_oz00_7298, obj_t BgL_vz00_7299)
	{
		{	/* Liveness/types.scm 197 */
			{
				BgL_synczf2livenesszf2_bglt BgL_auxz00_12145;

				{
					obj_t BgL_auxz00_12146;

					{	/* Liveness/types.scm 197 */
						BgL_objectz00_bglt BgL_tmpz00_12147;

						BgL_tmpz00_12147 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_7298));
						BgL_auxz00_12146 = BGL_OBJECT_WIDENING(BgL_tmpz00_12147);
					}
					BgL_auxz00_12145 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12146);
				}
				return
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12145))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7299)), BUNSPEC);
			}
		}

	}



/* &lambda3665 */
	obj_t BGl_z62lambda3665z62zzliveness_typesz00(obj_t BgL_envz00_7300,
		obj_t BgL_oz00_7301)
	{
		{	/* Liveness/types.scm 197 */
			{
				BgL_synczf2livenesszf2_bglt BgL_auxz00_12154;

				{
					obj_t BgL_auxz00_12155;

					{	/* Liveness/types.scm 197 */
						BgL_objectz00_bglt BgL_tmpz00_12156;

						BgL_tmpz00_12156 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_7301));
						BgL_auxz00_12155 = BGL_OBJECT_WIDENING(BgL_tmpz00_12156);
					}
					BgL_auxz00_12154 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12155);
				}
				return
					(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12154))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3659> */
	obj_t BGl_z62zc3z04anonymousza33659ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7302)
	{
		{	/* Liveness/types.scm 197 */
			return BNIL;
		}

	}



/* &lambda3658 */
	obj_t BGl_z62lambda3658z62zzliveness_typesz00(obj_t BgL_envz00_7303,
		obj_t BgL_oz00_7304, obj_t BgL_vz00_7305)
	{
		{	/* Liveness/types.scm 197 */
			{
				BgL_synczf2livenesszf2_bglt BgL_auxz00_12162;

				{
					obj_t BgL_auxz00_12163;

					{	/* Liveness/types.scm 197 */
						BgL_objectz00_bglt BgL_tmpz00_12164;

						BgL_tmpz00_12164 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_7304));
						BgL_auxz00_12163 = BGL_OBJECT_WIDENING(BgL_tmpz00_12164);
					}
					BgL_auxz00_12162 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12163);
				}
				return
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12162))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7305)), BUNSPEC);
			}
		}

	}



/* &lambda3657 */
	obj_t BGl_z62lambda3657z62zzliveness_typesz00(obj_t BgL_envz00_7306,
		obj_t BgL_oz00_7307)
	{
		{	/* Liveness/types.scm 197 */
			{
				BgL_synczf2livenesszf2_bglt BgL_auxz00_12171;

				{
					obj_t BgL_auxz00_12172;

					{	/* Liveness/types.scm 197 */
						BgL_objectz00_bglt BgL_tmpz00_12173;

						BgL_tmpz00_12173 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_7307));
						BgL_auxz00_12172 = BGL_OBJECT_WIDENING(BgL_tmpz00_12173);
					}
					BgL_auxz00_12171 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12172);
				}
				return
					(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12171))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3651> */
	obj_t BGl_z62zc3z04anonymousza33651ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7308)
	{
		{	/* Liveness/types.scm 197 */
			return BNIL;
		}

	}



/* &lambda3650 */
	obj_t BGl_z62lambda3650z62zzliveness_typesz00(obj_t BgL_envz00_7309,
		obj_t BgL_oz00_7310, obj_t BgL_vz00_7311)
	{
		{	/* Liveness/types.scm 197 */
			{
				BgL_synczf2livenesszf2_bglt BgL_auxz00_12179;

				{
					obj_t BgL_auxz00_12180;

					{	/* Liveness/types.scm 197 */
						BgL_objectz00_bglt BgL_tmpz00_12181;

						BgL_tmpz00_12181 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_7310));
						BgL_auxz00_12180 = BGL_OBJECT_WIDENING(BgL_tmpz00_12181);
					}
					BgL_auxz00_12179 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12180);
				}
				return
					((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12179))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7311)), BUNSPEC);
			}
		}

	}



/* &lambda3649 */
	obj_t BGl_z62lambda3649z62zzliveness_typesz00(obj_t BgL_envz00_7312,
		obj_t BgL_oz00_7313)
	{
		{	/* Liveness/types.scm 197 */
			{
				BgL_synczf2livenesszf2_bglt BgL_auxz00_12188;

				{
					obj_t BgL_auxz00_12189;

					{	/* Liveness/types.scm 197 */
						BgL_objectz00_bglt BgL_tmpz00_12190;

						BgL_tmpz00_12190 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_7313));
						BgL_auxz00_12189 = BGL_OBJECT_WIDENING(BgL_tmpz00_12190);
					}
					BgL_auxz00_12188 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_12189);
				}
				return
					(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_12188))->
					BgL_defz00);
			}
		}

	}



/* &lambda3586 */
	BgL_jumpzd2exzd2itz00_bglt BGl_z62lambda3586z62zzliveness_typesz00(obj_t
		BgL_envz00_7314, obj_t BgL_o1590z00_7315)
	{
		{	/* Liveness/types.scm 191 */
			{	/* Liveness/types.scm 191 */
				long BgL_arg3587z00_8771;

				{	/* Liveness/types.scm 191 */
					obj_t BgL_arg3589z00_8772;

					{	/* Liveness/types.scm 191 */
						obj_t BgL_arg3590z00_8773;

						{	/* Liveness/types.scm 191 */
							obj_t BgL_arg1815z00_8774;
							long BgL_arg1816z00_8775;

							BgL_arg1815z00_8774 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 191 */
								long BgL_arg1817z00_8776;

								BgL_arg1817z00_8776 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_jumpzd2exzd2itz00_bglt) BgL_o1590z00_7315)));
								BgL_arg1816z00_8775 = (BgL_arg1817z00_8776 - OBJECT_TYPE);
							}
							BgL_arg3590z00_8773 =
								VECTOR_REF(BgL_arg1815z00_8774, BgL_arg1816z00_8775);
						}
						BgL_arg3589z00_8772 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3590z00_8773);
					}
					{	/* Liveness/types.scm 191 */
						obj_t BgL_tmpz00_12203;

						BgL_tmpz00_12203 = ((obj_t) BgL_arg3589z00_8772);
						BgL_arg3587z00_8771 = BGL_CLASS_NUM(BgL_tmpz00_12203);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_jumpzd2exzd2itz00_bglt) BgL_o1590z00_7315)),
					BgL_arg3587z00_8771);
			}
			{	/* Liveness/types.scm 191 */
				BgL_objectz00_bglt BgL_tmpz00_12209;

				BgL_tmpz00_12209 =
					((BgL_objectz00_bglt)
					((BgL_jumpzd2exzd2itz00_bglt) BgL_o1590z00_7315));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12209, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_o1590z00_7315));
			return
				((BgL_jumpzd2exzd2itz00_bglt)
				((BgL_jumpzd2exzd2itz00_bglt) BgL_o1590z00_7315));
		}

	}



/* &<@anonymous:3585> */
	obj_t BGl_z62zc3z04anonymousza33585ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7316, obj_t BgL_new1589z00_7317)
	{
		{	/* Liveness/types.scm 191 */
			{
				BgL_jumpzd2exzd2itz00_bglt BgL_auxz00_12217;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_jumpzd2exzd2itz00_bglt) BgL_new1589z00_7317))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_12221;

					{	/* Liveness/types.scm 191 */
						obj_t BgL_classz00_8778;

						BgL_classz00_8778 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 191 */
							obj_t BgL__ortest_1117z00_8779;

							BgL__ortest_1117z00_8779 = BGL_CLASS_NIL(BgL_classz00_8778);
							if (CBOOL(BgL__ortest_1117z00_8779))
								{	/* Liveness/types.scm 191 */
									BgL_auxz00_12221 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8779);
								}
							else
								{	/* Liveness/types.scm 191 */
									BgL_auxz00_12221 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8778));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_jumpzd2exzd2itz00_bglt) BgL_new1589z00_7317))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_12221), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_12231;

					{	/* Liveness/types.scm 191 */
						obj_t BgL_classz00_8780;

						BgL_classz00_8780 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 191 */
							obj_t BgL__ortest_1117z00_8781;

							BgL__ortest_1117z00_8781 = BGL_CLASS_NIL(BgL_classz00_8780);
							if (CBOOL(BgL__ortest_1117z00_8781))
								{	/* Liveness/types.scm 191 */
									BgL_auxz00_12231 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8781);
								}
							else
								{	/* Liveness/types.scm 191 */
									BgL_auxz00_12231 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8780));
								}
						}
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt)
										((BgL_jumpzd2exzd2itz00_bglt) BgL_new1589z00_7317))))->
							BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_12231), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_12241;

					{	/* Liveness/types.scm 191 */
						obj_t BgL_classz00_8782;

						BgL_classz00_8782 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 191 */
							obj_t BgL__ortest_1117z00_8783;

							BgL__ortest_1117z00_8783 = BGL_CLASS_NIL(BgL_classz00_8782);
							if (CBOOL(BgL__ortest_1117z00_8783))
								{	/* Liveness/types.scm 191 */
									BgL_auxz00_12241 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8783);
								}
							else
								{	/* Liveness/types.scm 191 */
									BgL_auxz00_12241 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8782));
								}
						}
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt)
										((BgL_jumpzd2exzd2itz00_bglt) BgL_new1589z00_7317))))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_12241), BUNSPEC);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12251;

					{
						obj_t BgL_auxz00_12252;

						{	/* Liveness/types.scm 191 */
							BgL_objectz00_bglt BgL_tmpz00_12253;

							BgL_tmpz00_12253 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_new1589z00_7317));
							BgL_auxz00_12252 = BGL_OBJECT_WIDENING(BgL_tmpz00_12253);
						}
						BgL_auxz00_12251 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12252);
					}
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_12251))->BgL_defz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12259;

					{
						obj_t BgL_auxz00_12260;

						{	/* Liveness/types.scm 191 */
							BgL_objectz00_bglt BgL_tmpz00_12261;

							BgL_tmpz00_12261 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_new1589z00_7317));
							BgL_auxz00_12260 = BGL_OBJECT_WIDENING(BgL_tmpz00_12261);
						}
						BgL_auxz00_12259 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12260);
					}
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_12259))->BgL_usez00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12267;

					{
						obj_t BgL_auxz00_12268;

						{	/* Liveness/types.scm 191 */
							BgL_objectz00_bglt BgL_tmpz00_12269;

							BgL_tmpz00_12269 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_new1589z00_7317));
							BgL_auxz00_12268 = BGL_OBJECT_WIDENING(BgL_tmpz00_12269);
						}
						BgL_auxz00_12267 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12268);
					}
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_12267))->BgL_inz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12275;

					{
						obj_t BgL_auxz00_12276;

						{	/* Liveness/types.scm 191 */
							BgL_objectz00_bglt BgL_tmpz00_12277;

							BgL_tmpz00_12277 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_new1589z00_7317));
							BgL_auxz00_12276 = BGL_OBJECT_WIDENING(BgL_tmpz00_12277);
						}
						BgL_auxz00_12275 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12276);
					}
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_12275))->BgL_outz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_12217 = ((BgL_jumpzd2exzd2itz00_bglt) BgL_new1589z00_7317);
				return ((obj_t) BgL_auxz00_12217);
			}
		}

	}



/* &lambda3582 */
	BgL_jumpzd2exzd2itz00_bglt BGl_z62lambda3582z62zzliveness_typesz00(obj_t
		BgL_envz00_7318, obj_t BgL_o1586z00_7319)
	{
		{	/* Liveness/types.scm 191 */
			{	/* Liveness/types.scm 191 */
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_wide1588z00_8785;

				BgL_wide1588z00_8785 =
					((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
					BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jumpzd2exzd2itzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 191 */
					obj_t BgL_auxz00_12290;
					BgL_objectz00_bglt BgL_tmpz00_12286;

					BgL_auxz00_12290 = ((obj_t) BgL_wide1588z00_8785);
					BgL_tmpz00_12286 =
						((BgL_objectz00_bglt)
						((BgL_jumpzd2exzd2itz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_o1586z00_7319)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12286, BgL_auxz00_12290);
				}
				((BgL_objectz00_bglt)
					((BgL_jumpzd2exzd2itz00_bglt)
						((BgL_jumpzd2exzd2itz00_bglt) BgL_o1586z00_7319)));
				{	/* Liveness/types.scm 191 */
					long BgL_arg3584z00_8786;

					BgL_arg3584z00_8786 =
						BGL_CLASS_NUM(BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_o1586z00_7319))),
						BgL_arg3584z00_8786);
				}
				return
					((BgL_jumpzd2exzd2itz00_bglt)
					((BgL_jumpzd2exzd2itz00_bglt)
						((BgL_jumpzd2exzd2itz00_bglt) BgL_o1586z00_7319)));
			}
		}

	}



/* &lambda3578 */
	BgL_jumpzd2exzd2itz00_bglt BGl_z62lambda3578z62zzliveness_typesz00(obj_t
		BgL_envz00_7320, obj_t BgL_loc1578z00_7321, obj_t BgL_type1579z00_7322,
		obj_t BgL_exit1580z00_7323, obj_t BgL_value1581z00_7324,
		obj_t BgL_def1582z00_7325, obj_t BgL_use1583z00_7326,
		obj_t BgL_in1584z00_7327, obj_t BgL_out1585z00_7328)
	{
		{	/* Liveness/types.scm 191 */
			{	/* Liveness/types.scm 191 */
				BgL_jumpzd2exzd2itz00_bglt BgL_new1825z00_8794;

				{	/* Liveness/types.scm 191 */
					BgL_jumpzd2exzd2itz00_bglt BgL_tmp1823z00_8795;
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_wide1824z00_8796;

					{
						BgL_jumpzd2exzd2itz00_bglt BgL_auxz00_12304;

						{	/* Liveness/types.scm 191 */
							BgL_jumpzd2exzd2itz00_bglt BgL_new1822z00_8797;

							BgL_new1822z00_8797 =
								((BgL_jumpzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_jumpzd2exzd2itz00_bgl))));
							{	/* Liveness/types.scm 191 */
								long BgL_arg3581z00_8798;

								BgL_arg3581z00_8798 =
									BGL_CLASS_NUM(BGl_jumpzd2exzd2itz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1822z00_8797),
									BgL_arg3581z00_8798);
							}
							{	/* Liveness/types.scm 191 */
								BgL_objectz00_bglt BgL_tmpz00_12309;

								BgL_tmpz00_12309 = ((BgL_objectz00_bglt) BgL_new1822z00_8797);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12309, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1822z00_8797);
							BgL_auxz00_12304 = BgL_new1822z00_8797;
						}
						BgL_tmp1823z00_8795 =
							((BgL_jumpzd2exzd2itz00_bglt) BgL_auxz00_12304);
					}
					BgL_wide1824z00_8796 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jumpzd2exzd2itzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 191 */
						obj_t BgL_auxz00_12317;
						BgL_objectz00_bglt BgL_tmpz00_12315;

						BgL_auxz00_12317 = ((obj_t) BgL_wide1824z00_8796);
						BgL_tmpz00_12315 = ((BgL_objectz00_bglt) BgL_tmp1823z00_8795);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12315, BgL_auxz00_12317);
					}
					((BgL_objectz00_bglt) BgL_tmp1823z00_8795);
					{	/* Liveness/types.scm 191 */
						long BgL_arg3580z00_8799;

						BgL_arg3580z00_8799 =
							BGL_CLASS_NUM
							(BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) BgL_tmp1823z00_8795),
							BgL_arg3580z00_8799);
					}
					BgL_new1825z00_8794 =
						((BgL_jumpzd2exzd2itz00_bglt) BgL_tmp1823z00_8795);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1825z00_8794)))->BgL_locz00) =
					((obj_t) BgL_loc1578z00_7321), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1825z00_8794)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1579z00_7322)),
					BUNSPEC);
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
									BgL_new1825z00_8794)))->BgL_exitz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_exit1580z00_7323)),
					BUNSPEC);
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
									BgL_new1825z00_8794)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_value1581z00_7324)),
					BUNSPEC);
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12336;

					{
						obj_t BgL_auxz00_12337;

						{	/* Liveness/types.scm 191 */
							BgL_objectz00_bglt BgL_tmpz00_12338;

							BgL_tmpz00_12338 = ((BgL_objectz00_bglt) BgL_new1825z00_8794);
							BgL_auxz00_12337 = BGL_OBJECT_WIDENING(BgL_tmpz00_12338);
						}
						BgL_auxz00_12336 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12337);
					}
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_12336))->BgL_defz00) =
						((obj_t) ((obj_t) BgL_def1582z00_7325)), BUNSPEC);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12344;

					{
						obj_t BgL_auxz00_12345;

						{	/* Liveness/types.scm 191 */
							BgL_objectz00_bglt BgL_tmpz00_12346;

							BgL_tmpz00_12346 = ((BgL_objectz00_bglt) BgL_new1825z00_8794);
							BgL_auxz00_12345 = BGL_OBJECT_WIDENING(BgL_tmpz00_12346);
						}
						BgL_auxz00_12344 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12345);
					}
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_12344))->BgL_usez00) =
						((obj_t) ((obj_t) BgL_use1583z00_7326)), BUNSPEC);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12352;

					{
						obj_t BgL_auxz00_12353;

						{	/* Liveness/types.scm 191 */
							BgL_objectz00_bglt BgL_tmpz00_12354;

							BgL_tmpz00_12354 = ((BgL_objectz00_bglt) BgL_new1825z00_8794);
							BgL_auxz00_12353 = BGL_OBJECT_WIDENING(BgL_tmpz00_12354);
						}
						BgL_auxz00_12352 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12353);
					}
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_12352))->BgL_inz00) =
						((obj_t) ((obj_t) BgL_in1584z00_7327)), BUNSPEC);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12360;

					{
						obj_t BgL_auxz00_12361;

						{	/* Liveness/types.scm 191 */
							BgL_objectz00_bglt BgL_tmpz00_12362;

							BgL_tmpz00_12362 = ((BgL_objectz00_bglt) BgL_new1825z00_8794);
							BgL_auxz00_12361 = BGL_OBJECT_WIDENING(BgL_tmpz00_12362);
						}
						BgL_auxz00_12360 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12361);
					}
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_12360))->BgL_outz00) =
						((obj_t) ((obj_t) BgL_out1585z00_7328)), BUNSPEC);
				}
				return BgL_new1825z00_8794;
			}
		}

	}



/* &<@anonymous:3624> */
	obj_t BGl_z62zc3z04anonymousza33624ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7329)
	{
		{	/* Liveness/types.scm 191 */
			return BNIL;
		}

	}



/* &lambda3623 */
	obj_t BGl_z62lambda3623z62zzliveness_typesz00(obj_t BgL_envz00_7330,
		obj_t BgL_oz00_7331, obj_t BgL_vz00_7332)
	{
		{	/* Liveness/types.scm 191 */
			{
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12368;

				{
					obj_t BgL_auxz00_12369;

					{	/* Liveness/types.scm 191 */
						BgL_objectz00_bglt BgL_tmpz00_12370;

						BgL_tmpz00_12370 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_7331));
						BgL_auxz00_12369 = BGL_OBJECT_WIDENING(BgL_tmpz00_12370);
					}
					BgL_auxz00_12368 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12369);
				}
				return
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_12368))->BgL_outz00) =
					((obj_t) ((obj_t) BgL_vz00_7332)), BUNSPEC);
			}
		}

	}



/* &lambda3622 */
	obj_t BGl_z62lambda3622z62zzliveness_typesz00(obj_t BgL_envz00_7333,
		obj_t BgL_oz00_7334)
	{
		{	/* Liveness/types.scm 191 */
			{
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12377;

				{
					obj_t BgL_auxz00_12378;

					{	/* Liveness/types.scm 191 */
						BgL_objectz00_bglt BgL_tmpz00_12379;

						BgL_tmpz00_12379 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_7334));
						BgL_auxz00_12378 = BGL_OBJECT_WIDENING(BgL_tmpz00_12379);
					}
					BgL_auxz00_12377 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12378);
				}
				return
					(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12377))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3615> */
	obj_t BGl_z62zc3z04anonymousza33615ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7335)
	{
		{	/* Liveness/types.scm 191 */
			return BNIL;
		}

	}



/* &lambda3614 */
	obj_t BGl_z62lambda3614z62zzliveness_typesz00(obj_t BgL_envz00_7336,
		obj_t BgL_oz00_7337, obj_t BgL_vz00_7338)
	{
		{	/* Liveness/types.scm 191 */
			{
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12385;

				{
					obj_t BgL_auxz00_12386;

					{	/* Liveness/types.scm 191 */
						BgL_objectz00_bglt BgL_tmpz00_12387;

						BgL_tmpz00_12387 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_7337));
						BgL_auxz00_12386 = BGL_OBJECT_WIDENING(BgL_tmpz00_12387);
					}
					BgL_auxz00_12385 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12386);
				}
				return
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_12385))->BgL_inz00) =
					((obj_t) ((obj_t) BgL_vz00_7338)), BUNSPEC);
			}
		}

	}



/* &lambda3613 */
	obj_t BGl_z62lambda3613z62zzliveness_typesz00(obj_t BgL_envz00_7339,
		obj_t BgL_oz00_7340)
	{
		{	/* Liveness/types.scm 191 */
			{
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12394;

				{
					obj_t BgL_auxz00_12395;

					{	/* Liveness/types.scm 191 */
						BgL_objectz00_bglt BgL_tmpz00_12396;

						BgL_tmpz00_12396 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_7340));
						BgL_auxz00_12395 = BGL_OBJECT_WIDENING(BgL_tmpz00_12396);
					}
					BgL_auxz00_12394 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12395);
				}
				return
					(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12394))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3608> */
	obj_t BGl_z62zc3z04anonymousza33608ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7341)
	{
		{	/* Liveness/types.scm 191 */
			return BNIL;
		}

	}



/* &lambda3607 */
	obj_t BGl_z62lambda3607z62zzliveness_typesz00(obj_t BgL_envz00_7342,
		obj_t BgL_oz00_7343, obj_t BgL_vz00_7344)
	{
		{	/* Liveness/types.scm 191 */
			{
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12402;

				{
					obj_t BgL_auxz00_12403;

					{	/* Liveness/types.scm 191 */
						BgL_objectz00_bglt BgL_tmpz00_12404;

						BgL_tmpz00_12404 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_7343));
						BgL_auxz00_12403 = BGL_OBJECT_WIDENING(BgL_tmpz00_12404);
					}
					BgL_auxz00_12402 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12403);
				}
				return
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_12402))->BgL_usez00) =
					((obj_t) ((obj_t) BgL_vz00_7344)), BUNSPEC);
			}
		}

	}



/* &lambda3606 */
	obj_t BGl_z62lambda3606z62zzliveness_typesz00(obj_t BgL_envz00_7345,
		obj_t BgL_oz00_7346)
	{
		{	/* Liveness/types.scm 191 */
			{
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12411;

				{
					obj_t BgL_auxz00_12412;

					{	/* Liveness/types.scm 191 */
						BgL_objectz00_bglt BgL_tmpz00_12413;

						BgL_tmpz00_12413 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_7346));
						BgL_auxz00_12412 = BGL_OBJECT_WIDENING(BgL_tmpz00_12413);
					}
					BgL_auxz00_12411 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12412);
				}
				return
					(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12411))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3599> */
	obj_t BGl_z62zc3z04anonymousza33599ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7347)
	{
		{	/* Liveness/types.scm 191 */
			return BNIL;
		}

	}



/* &lambda3598 */
	obj_t BGl_z62lambda3598z62zzliveness_typesz00(obj_t BgL_envz00_7348,
		obj_t BgL_oz00_7349, obj_t BgL_vz00_7350)
	{
		{	/* Liveness/types.scm 191 */
			{
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12419;

				{
					obj_t BgL_auxz00_12420;

					{	/* Liveness/types.scm 191 */
						BgL_objectz00_bglt BgL_tmpz00_12421;

						BgL_tmpz00_12421 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_7349));
						BgL_auxz00_12420 = BGL_OBJECT_WIDENING(BgL_tmpz00_12421);
					}
					BgL_auxz00_12419 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12420);
				}
				return
					((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_12419))->BgL_defz00) =
					((obj_t) ((obj_t) BgL_vz00_7350)), BUNSPEC);
			}
		}

	}



/* &lambda3597 */
	obj_t BGl_z62lambda3597z62zzliveness_typesz00(obj_t BgL_envz00_7351,
		obj_t BgL_oz00_7352)
	{
		{	/* Liveness/types.scm 191 */
			{
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12428;

				{
					obj_t BgL_auxz00_12429;

					{	/* Liveness/types.scm 191 */
						BgL_objectz00_bglt BgL_tmpz00_12430;

						BgL_tmpz00_12430 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_7352));
						BgL_auxz00_12429 = BGL_OBJECT_WIDENING(BgL_tmpz00_12430);
					}
					BgL_auxz00_12428 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12429);
				}
				return
					(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12428))->
					BgL_defz00);
			}
		}

	}



/* &lambda3533 */
	BgL_setzd2exzd2itz00_bglt BGl_z62lambda3533z62zzliveness_typesz00(obj_t
		BgL_envz00_7353, obj_t BgL_o1576z00_7354)
	{
		{	/* Liveness/types.scm 185 */
			{	/* Liveness/types.scm 185 */
				long BgL_arg3534z00_8813;

				{	/* Liveness/types.scm 185 */
					obj_t BgL_arg3535z00_8814;

					{	/* Liveness/types.scm 185 */
						obj_t BgL_arg3536z00_8815;

						{	/* Liveness/types.scm 185 */
							obj_t BgL_arg1815z00_8816;
							long BgL_arg1816z00_8817;

							BgL_arg1815z00_8816 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 185 */
								long BgL_arg1817z00_8818;

								BgL_arg1817z00_8818 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_setzd2exzd2itz00_bglt) BgL_o1576z00_7354)));
								BgL_arg1816z00_8817 = (BgL_arg1817z00_8818 - OBJECT_TYPE);
							}
							BgL_arg3536z00_8815 =
								VECTOR_REF(BgL_arg1815z00_8816, BgL_arg1816z00_8817);
						}
						BgL_arg3535z00_8814 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3536z00_8815);
					}
					{	/* Liveness/types.scm 185 */
						obj_t BgL_tmpz00_12443;

						BgL_tmpz00_12443 = ((obj_t) BgL_arg3535z00_8814);
						BgL_arg3534z00_8813 = BGL_CLASS_NUM(BgL_tmpz00_12443);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_setzd2exzd2itz00_bglt) BgL_o1576z00_7354)),
					BgL_arg3534z00_8813);
			}
			{	/* Liveness/types.scm 185 */
				BgL_objectz00_bglt BgL_tmpz00_12449;

				BgL_tmpz00_12449 =
					((BgL_objectz00_bglt)
					((BgL_setzd2exzd2itz00_bglt) BgL_o1576z00_7354));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12449, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_o1576z00_7354));
			return
				((BgL_setzd2exzd2itz00_bglt)
				((BgL_setzd2exzd2itz00_bglt) BgL_o1576z00_7354));
		}

	}



/* &<@anonymous:3532> */
	obj_t BGl_z62zc3z04anonymousza33532ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7355, obj_t BgL_new1575z00_7356)
	{
		{	/* Liveness/types.scm 185 */
			{
				BgL_setzd2exzd2itz00_bglt BgL_auxz00_12457;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_12461;

					{	/* Liveness/types.scm 185 */
						obj_t BgL_classz00_8820;

						BgL_classz00_8820 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 185 */
							obj_t BgL__ortest_1117z00_8821;

							BgL__ortest_1117z00_8821 = BGL_CLASS_NIL(BgL_classz00_8820);
							if (CBOOL(BgL__ortest_1117z00_8821))
								{	/* Liveness/types.scm 185 */
									BgL_auxz00_12461 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8821);
								}
							else
								{	/* Liveness/types.scm 185 */
									BgL_auxz00_12461 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8820));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_12461), BUNSPEC);
				}
				{
					BgL_varz00_bglt BgL_auxz00_12471;

					{	/* Liveness/types.scm 185 */
						obj_t BgL_classz00_8822;

						BgL_classz00_8822 = BGl_varz00zzast_nodez00;
						{	/* Liveness/types.scm 185 */
							obj_t BgL__ortest_1117z00_8823;

							BgL__ortest_1117z00_8823 = BGL_CLASS_NIL(BgL_classz00_8822);
							if (CBOOL(BgL__ortest_1117z00_8823))
								{	/* Liveness/types.scm 185 */
									BgL_auxz00_12471 =
										((BgL_varz00_bglt) BgL__ortest_1117z00_8823);
								}
							else
								{	/* Liveness/types.scm 185 */
									BgL_auxz00_12471 =
										((BgL_varz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8822));
								}
						}
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt)
										((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356))))->
							BgL_varz00) = ((BgL_varz00_bglt) BgL_auxz00_12471), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_12481;

					{	/* Liveness/types.scm 185 */
						obj_t BgL_classz00_8824;

						BgL_classz00_8824 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 185 */
							obj_t BgL__ortest_1117z00_8825;

							BgL__ortest_1117z00_8825 = BGL_CLASS_NIL(BgL_classz00_8824);
							if (CBOOL(BgL__ortest_1117z00_8825))
								{	/* Liveness/types.scm 185 */
									BgL_auxz00_12481 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8825);
								}
							else
								{	/* Liveness/types.scm 185 */
									BgL_auxz00_12481 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8824));
								}
						}
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt)
										((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356))))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_12481), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_12491;

					{	/* Liveness/types.scm 185 */
						obj_t BgL_classz00_8826;

						BgL_classz00_8826 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 185 */
							obj_t BgL__ortest_1117z00_8827;

							BgL__ortest_1117z00_8827 = BGL_CLASS_NIL(BgL_classz00_8826);
							if (CBOOL(BgL__ortest_1117z00_8827))
								{	/* Liveness/types.scm 185 */
									BgL_auxz00_12491 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8827);
								}
							else
								{	/* Liveness/types.scm 185 */
									BgL_auxz00_12491 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8826));
								}
						}
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt)
										((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356))))->
							BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_12491), BUNSPEC);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12501;

					{
						obj_t BgL_auxz00_12502;

						{	/* Liveness/types.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_12503;

							BgL_tmpz00_12503 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356));
							BgL_auxz00_12502 = BGL_OBJECT_WIDENING(BgL_tmpz00_12503);
						}
						BgL_auxz00_12501 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12502);
					}
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12501))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12509;

					{
						obj_t BgL_auxz00_12510;

						{	/* Liveness/types.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_12511;

							BgL_tmpz00_12511 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356));
							BgL_auxz00_12510 = BGL_OBJECT_WIDENING(BgL_tmpz00_12511);
						}
						BgL_auxz00_12509 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12510);
					}
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12509))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12517;

					{
						obj_t BgL_auxz00_12518;

						{	/* Liveness/types.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_12519;

							BgL_tmpz00_12519 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356));
							BgL_auxz00_12518 = BGL_OBJECT_WIDENING(BgL_tmpz00_12519);
						}
						BgL_auxz00_12517 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12518);
					}
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12517))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12525;

					{
						obj_t BgL_auxz00_12526;

						{	/* Liveness/types.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_12527;

							BgL_tmpz00_12527 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356));
							BgL_auxz00_12526 = BGL_OBJECT_WIDENING(BgL_tmpz00_12527);
						}
						BgL_auxz00_12525 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12526);
					}
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12525))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_12457 = ((BgL_setzd2exzd2itz00_bglt) BgL_new1575z00_7356);
				return ((obj_t) BgL_auxz00_12457);
			}
		}

	}



/* &lambda3529 */
	BgL_setzd2exzd2itz00_bglt BGl_z62lambda3529z62zzliveness_typesz00(obj_t
		BgL_envz00_7357, obj_t BgL_o1572z00_7358)
	{
		{	/* Liveness/types.scm 185 */
			{	/* Liveness/types.scm 185 */
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_wide1574z00_8829;

				BgL_wide1574z00_8829 =
					((BgL_setzd2exzd2itzf2livenesszf2_bglt)
					BOBJECT(GC_MALLOC(sizeof(struct
								BgL_setzd2exzd2itzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 185 */
					obj_t BgL_auxz00_12540;
					BgL_objectz00_bglt BgL_tmpz00_12536;

					BgL_auxz00_12540 = ((obj_t) BgL_wide1574z00_8829);
					BgL_tmpz00_12536 =
						((BgL_objectz00_bglt)
						((BgL_setzd2exzd2itz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_o1572z00_7358)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12536, BgL_auxz00_12540);
				}
				((BgL_objectz00_bglt)
					((BgL_setzd2exzd2itz00_bglt)
						((BgL_setzd2exzd2itz00_bglt) BgL_o1572z00_7358)));
				{	/* Liveness/types.scm 185 */
					long BgL_arg3531z00_8830;

					BgL_arg3531z00_8830 =
						BGL_CLASS_NUM(BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_o1572z00_7358))),
						BgL_arg3531z00_8830);
				}
				return
					((BgL_setzd2exzd2itz00_bglt)
					((BgL_setzd2exzd2itz00_bglt)
						((BgL_setzd2exzd2itz00_bglt) BgL_o1572z00_7358)));
			}
		}

	}



/* &lambda3524 */
	BgL_setzd2exzd2itz00_bglt BGl_z62lambda3524z62zzliveness_typesz00(obj_t
		BgL_envz00_7359, obj_t BgL_loc1563z00_7360, obj_t BgL_type1564z00_7361,
		obj_t BgL_var1565z00_7362, obj_t BgL_body1566z00_7363,
		obj_t BgL_onexit1567z00_7364, obj_t BgL_def1568z00_7365,
		obj_t BgL_use1569z00_7366, obj_t BgL_in1570z00_7367,
		obj_t BgL_out1571z00_7368)
	{
		{	/* Liveness/types.scm 185 */
			{	/* Liveness/types.scm 185 */
				BgL_setzd2exzd2itz00_bglt BgL_new1820z00_8839;

				{	/* Liveness/types.scm 185 */
					BgL_setzd2exzd2itz00_bglt BgL_tmp1818z00_8840;
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_wide1819z00_8841;

					{
						BgL_setzd2exzd2itz00_bglt BgL_auxz00_12554;

						{	/* Liveness/types.scm 185 */
							BgL_setzd2exzd2itz00_bglt BgL_new1817z00_8842;

							BgL_new1817z00_8842 =
								((BgL_setzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_setzd2exzd2itz00_bgl))));
							{	/* Liveness/types.scm 185 */
								long BgL_arg3528z00_8843;

								BgL_arg3528z00_8843 =
									BGL_CLASS_NUM(BGl_setzd2exzd2itz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1817z00_8842),
									BgL_arg3528z00_8843);
							}
							{	/* Liveness/types.scm 185 */
								BgL_objectz00_bglt BgL_tmpz00_12559;

								BgL_tmpz00_12559 = ((BgL_objectz00_bglt) BgL_new1817z00_8842);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12559, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1817z00_8842);
							BgL_auxz00_12554 = BgL_new1817z00_8842;
						}
						BgL_tmp1818z00_8840 =
							((BgL_setzd2exzd2itz00_bglt) BgL_auxz00_12554);
					}
					BgL_wide1819z00_8841 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_setzd2exzd2itzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 185 */
						obj_t BgL_auxz00_12567;
						BgL_objectz00_bglt BgL_tmpz00_12565;

						BgL_auxz00_12567 = ((obj_t) BgL_wide1819z00_8841);
						BgL_tmpz00_12565 = ((BgL_objectz00_bglt) BgL_tmp1818z00_8840);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12565, BgL_auxz00_12567);
					}
					((BgL_objectz00_bglt) BgL_tmp1818z00_8840);
					{	/* Liveness/types.scm 185 */
						long BgL_arg3525z00_8844;

						BgL_arg3525z00_8844 =
							BGL_CLASS_NUM(BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1818z00_8840), BgL_arg3525z00_8844);
					}
					BgL_new1820z00_8839 =
						((BgL_setzd2exzd2itz00_bglt) BgL_tmp1818z00_8840);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1820z00_8839)))->BgL_locz00) =
					((obj_t) BgL_loc1563z00_7360), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1820z00_8839)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1564z00_7361)),
					BUNSPEC);
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
									BgL_new1820z00_8839)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_var1565z00_7362)), BUNSPEC);
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
									BgL_new1820z00_8839)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1566z00_7363)),
					BUNSPEC);
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
									BgL_new1820z00_8839)))->BgL_onexitz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_onexit1567z00_7364)),
					BUNSPEC);
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12589;

					{
						obj_t BgL_auxz00_12590;

						{	/* Liveness/types.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_12591;

							BgL_tmpz00_12591 = ((BgL_objectz00_bglt) BgL_new1820z00_8839);
							BgL_auxz00_12590 = BGL_OBJECT_WIDENING(BgL_tmpz00_12591);
						}
						BgL_auxz00_12589 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12590);
					}
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12589))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1568z00_7365)), BUNSPEC);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12597;

					{
						obj_t BgL_auxz00_12598;

						{	/* Liveness/types.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_12599;

							BgL_tmpz00_12599 = ((BgL_objectz00_bglt) BgL_new1820z00_8839);
							BgL_auxz00_12598 = BGL_OBJECT_WIDENING(BgL_tmpz00_12599);
						}
						BgL_auxz00_12597 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12598);
					}
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12597))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1569z00_7366)), BUNSPEC);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12605;

					{
						obj_t BgL_auxz00_12606;

						{	/* Liveness/types.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_12607;

							BgL_tmpz00_12607 = ((BgL_objectz00_bglt) BgL_new1820z00_8839);
							BgL_auxz00_12606 = BGL_OBJECT_WIDENING(BgL_tmpz00_12607);
						}
						BgL_auxz00_12605 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12606);
					}
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12605))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1570z00_7367)), BUNSPEC);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12613;

					{
						obj_t BgL_auxz00_12614;

						{	/* Liveness/types.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_12615;

							BgL_tmpz00_12615 = ((BgL_objectz00_bglt) BgL_new1820z00_8839);
							BgL_auxz00_12614 = BGL_OBJECT_WIDENING(BgL_tmpz00_12615);
						}
						BgL_auxz00_12613 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12614);
					}
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12613))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1571z00_7368)), BUNSPEC);
				}
				return BgL_new1820z00_8839;
			}
		}

	}



/* &<@anonymous:3570> */
	obj_t BGl_z62zc3z04anonymousza33570ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7369)
	{
		{	/* Liveness/types.scm 185 */
			return BNIL;
		}

	}



/* &lambda3569 */
	obj_t BGl_z62lambda3569z62zzliveness_typesz00(obj_t BgL_envz00_7370,
		obj_t BgL_oz00_7371, obj_t BgL_vz00_7372)
	{
		{	/* Liveness/types.scm 185 */
			{
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12621;

				{
					obj_t BgL_auxz00_12622;

					{	/* Liveness/types.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_12623;

						BgL_tmpz00_12623 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_7371));
						BgL_auxz00_12622 = BGL_OBJECT_WIDENING(BgL_tmpz00_12623);
					}
					BgL_auxz00_12621 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12622);
				}
				return
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12621))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7372)), BUNSPEC);
			}
		}

	}



/* &lambda3568 */
	obj_t BGl_z62lambda3568z62zzliveness_typesz00(obj_t BgL_envz00_7373,
		obj_t BgL_oz00_7374)
	{
		{	/* Liveness/types.scm 185 */
			{
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12630;

				{
					obj_t BgL_auxz00_12631;

					{	/* Liveness/types.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_12632;

						BgL_tmpz00_12632 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_7374));
						BgL_auxz00_12631 = BGL_OBJECT_WIDENING(BgL_tmpz00_12632);
					}
					BgL_auxz00_12630 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12631);
				}
				return
					(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12630))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3561> */
	obj_t BGl_z62zc3z04anonymousza33561ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7375)
	{
		{	/* Liveness/types.scm 185 */
			return BNIL;
		}

	}



/* &lambda3560 */
	obj_t BGl_z62lambda3560z62zzliveness_typesz00(obj_t BgL_envz00_7376,
		obj_t BgL_oz00_7377, obj_t BgL_vz00_7378)
	{
		{	/* Liveness/types.scm 185 */
			{
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12638;

				{
					obj_t BgL_auxz00_12639;

					{	/* Liveness/types.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_12640;

						BgL_tmpz00_12640 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_7377));
						BgL_auxz00_12639 = BGL_OBJECT_WIDENING(BgL_tmpz00_12640);
					}
					BgL_auxz00_12638 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12639);
				}
				return
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12638))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7378)), BUNSPEC);
			}
		}

	}



/* &lambda3559 */
	obj_t BGl_z62lambda3559z62zzliveness_typesz00(obj_t BgL_envz00_7379,
		obj_t BgL_oz00_7380)
	{
		{	/* Liveness/types.scm 185 */
			{
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12647;

				{
					obj_t BgL_auxz00_12648;

					{	/* Liveness/types.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_12649;

						BgL_tmpz00_12649 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_7380));
						BgL_auxz00_12648 = BGL_OBJECT_WIDENING(BgL_tmpz00_12649);
					}
					BgL_auxz00_12647 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12648);
				}
				return
					(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12647))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3552> */
	obj_t BGl_z62zc3z04anonymousza33552ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7381)
	{
		{	/* Liveness/types.scm 185 */
			return BNIL;
		}

	}



/* &lambda3551 */
	obj_t BGl_z62lambda3551z62zzliveness_typesz00(obj_t BgL_envz00_7382,
		obj_t BgL_oz00_7383, obj_t BgL_vz00_7384)
	{
		{	/* Liveness/types.scm 185 */
			{
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12655;

				{
					obj_t BgL_auxz00_12656;

					{	/* Liveness/types.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_12657;

						BgL_tmpz00_12657 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_7383));
						BgL_auxz00_12656 = BGL_OBJECT_WIDENING(BgL_tmpz00_12657);
					}
					BgL_auxz00_12655 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12656);
				}
				return
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12655))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7384)), BUNSPEC);
			}
		}

	}



/* &lambda3550 */
	obj_t BGl_z62lambda3550z62zzliveness_typesz00(obj_t BgL_envz00_7385,
		obj_t BgL_oz00_7386)
	{
		{	/* Liveness/types.scm 185 */
			{
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12664;

				{
					obj_t BgL_auxz00_12665;

					{	/* Liveness/types.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_12666;

						BgL_tmpz00_12666 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_7386));
						BgL_auxz00_12665 = BGL_OBJECT_WIDENING(BgL_tmpz00_12666);
					}
					BgL_auxz00_12664 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12665);
				}
				return
					(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12664))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3544> */
	obj_t BGl_z62zc3z04anonymousza33544ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7387)
	{
		{	/* Liveness/types.scm 185 */
			return BNIL;
		}

	}



/* &lambda3543 */
	obj_t BGl_z62lambda3543z62zzliveness_typesz00(obj_t BgL_envz00_7388,
		obj_t BgL_oz00_7389, obj_t BgL_vz00_7390)
	{
		{	/* Liveness/types.scm 185 */
			{
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12672;

				{
					obj_t BgL_auxz00_12673;

					{	/* Liveness/types.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_12674;

						BgL_tmpz00_12674 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_7389));
						BgL_auxz00_12673 = BGL_OBJECT_WIDENING(BgL_tmpz00_12674);
					}
					BgL_auxz00_12672 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12673);
				}
				return
					((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12672))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7390)), BUNSPEC);
			}
		}

	}



/* &lambda3542 */
	obj_t BGl_z62lambda3542z62zzliveness_typesz00(obj_t BgL_envz00_7391,
		obj_t BgL_oz00_7392)
	{
		{	/* Liveness/types.scm 185 */
			{
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_12681;

				{
					obj_t BgL_auxz00_12682;

					{	/* Liveness/types.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_12683;

						BgL_tmpz00_12683 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_7392));
						BgL_auxz00_12682 = BGL_OBJECT_WIDENING(BgL_tmpz00_12683);
					}
					BgL_auxz00_12681 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_12682);
				}
				return
					(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12681))->
					BgL_defz00);
			}
		}

	}



/* &lambda3476 */
	BgL_letzd2varzd2_bglt BGl_z62lambda3476z62zzliveness_typesz00(obj_t
		BgL_envz00_7393, obj_t BgL_o1561z00_7394)
	{
		{	/* Liveness/types.scm 179 */
			{	/* Liveness/types.scm 179 */
				long BgL_arg3477z00_8858;

				{	/* Liveness/types.scm 179 */
					obj_t BgL_arg3479z00_8859;

					{	/* Liveness/types.scm 179 */
						obj_t BgL_arg3480z00_8860;

						{	/* Liveness/types.scm 179 */
							obj_t BgL_arg1815z00_8861;
							long BgL_arg1816z00_8862;

							BgL_arg1815z00_8861 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 179 */
								long BgL_arg1817z00_8863;

								BgL_arg1817z00_8863 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_o1561z00_7394)));
								BgL_arg1816z00_8862 = (BgL_arg1817z00_8863 - OBJECT_TYPE);
							}
							BgL_arg3480z00_8860 =
								VECTOR_REF(BgL_arg1815z00_8861, BgL_arg1816z00_8862);
						}
						BgL_arg3479z00_8859 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3480z00_8860);
					}
					{	/* Liveness/types.scm 179 */
						obj_t BgL_tmpz00_12696;

						BgL_tmpz00_12696 = ((obj_t) BgL_arg3479z00_8859);
						BgL_arg3477z00_8858 = BGL_CLASS_NUM(BgL_tmpz00_12696);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_letzd2varzd2_bglt) BgL_o1561z00_7394)), BgL_arg3477z00_8858);
			}
			{	/* Liveness/types.scm 179 */
				BgL_objectz00_bglt BgL_tmpz00_12702;

				BgL_tmpz00_12702 =
					((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_o1561z00_7394));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12702, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_o1561z00_7394));
			return
				((BgL_letzd2varzd2_bglt) ((BgL_letzd2varzd2_bglt) BgL_o1561z00_7394));
		}

	}



/* &<@anonymous:3475> */
	obj_t BGl_z62zc3z04anonymousza33475ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7395, obj_t BgL_new1560z00_7396)
	{
		{	/* Liveness/types.scm 179 */
			{
				BgL_letzd2varzd2_bglt BgL_auxz00_12710;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_12714;

					{	/* Liveness/types.scm 179 */
						obj_t BgL_classz00_8865;

						BgL_classz00_8865 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 179 */
							obj_t BgL__ortest_1117z00_8866;

							BgL__ortest_1117z00_8866 = BGL_CLASS_NIL(BgL_classz00_8865);
							if (CBOOL(BgL__ortest_1117z00_8866))
								{	/* Liveness/types.scm 179 */
									BgL_auxz00_12714 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8866);
								}
							else
								{	/* Liveness/types.scm 179 */
									BgL_auxz00_12714 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8865));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_12714), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_letzd2varzd2_bglt)
										BgL_new1560z00_7396))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_letzd2varzd2_bglt)
							COBJECT(((BgL_letzd2varzd2_bglt) ((BgL_letzd2varzd2_bglt)
										BgL_new1560z00_7396))))->BgL_bindingsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_12733;

					{	/* Liveness/types.scm 179 */
						obj_t BgL_classz00_8867;

						BgL_classz00_8867 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 179 */
							obj_t BgL__ortest_1117z00_8868;

							BgL__ortest_1117z00_8868 = BGL_CLASS_NIL(BgL_classz00_8867);
							if (CBOOL(BgL__ortest_1117z00_8868))
								{	/* Liveness/types.scm 179 */
									BgL_auxz00_12733 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8868);
								}
							else
								{	/* Liveness/types.scm 179 */
									BgL_auxz00_12733 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8867));
								}
						}
					}
					((((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt)
										((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396))))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_12733), BUNSPEC);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt)
									((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396))))->
						BgL_removablezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12746;

					{
						obj_t BgL_auxz00_12747;

						{	/* Liveness/types.scm 179 */
							BgL_objectz00_bglt BgL_tmpz00_12748;

							BgL_tmpz00_12748 =
								((BgL_objectz00_bglt)
								((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396));
							BgL_auxz00_12747 = BGL_OBJECT_WIDENING(BgL_tmpz00_12748);
						}
						BgL_auxz00_12746 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12747);
					}
					((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12746))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12754;

					{
						obj_t BgL_auxz00_12755;

						{	/* Liveness/types.scm 179 */
							BgL_objectz00_bglt BgL_tmpz00_12756;

							BgL_tmpz00_12756 =
								((BgL_objectz00_bglt)
								((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396));
							BgL_auxz00_12755 = BGL_OBJECT_WIDENING(BgL_tmpz00_12756);
						}
						BgL_auxz00_12754 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12755);
					}
					((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12754))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12762;

					{
						obj_t BgL_auxz00_12763;

						{	/* Liveness/types.scm 179 */
							BgL_objectz00_bglt BgL_tmpz00_12764;

							BgL_tmpz00_12764 =
								((BgL_objectz00_bglt)
								((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396));
							BgL_auxz00_12763 = BGL_OBJECT_WIDENING(BgL_tmpz00_12764);
						}
						BgL_auxz00_12762 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12763);
					}
					((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12762))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12770;

					{
						obj_t BgL_auxz00_12771;

						{	/* Liveness/types.scm 179 */
							BgL_objectz00_bglt BgL_tmpz00_12772;

							BgL_tmpz00_12772 =
								((BgL_objectz00_bglt)
								((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396));
							BgL_auxz00_12771 = BGL_OBJECT_WIDENING(BgL_tmpz00_12772);
						}
						BgL_auxz00_12770 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12771);
					}
					((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12770))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_12710 = ((BgL_letzd2varzd2_bglt) BgL_new1560z00_7396);
				return ((obj_t) BgL_auxz00_12710);
			}
		}

	}



/* &lambda3472 */
	BgL_letzd2varzd2_bglt BGl_z62lambda3472z62zzliveness_typesz00(obj_t
		BgL_envz00_7397, obj_t BgL_o1557z00_7398)
	{
		{	/* Liveness/types.scm 179 */
			{	/* Liveness/types.scm 179 */
				BgL_letzd2varzf2livenessz20_bglt BgL_wide1559z00_8870;

				BgL_wide1559z00_8870 =
					((BgL_letzd2varzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_letzd2varzf2livenessz20_bgl))));
				{	/* Liveness/types.scm 179 */
					obj_t BgL_auxz00_12785;
					BgL_objectz00_bglt BgL_tmpz00_12781;

					BgL_auxz00_12785 = ((obj_t) BgL_wide1559z00_8870);
					BgL_tmpz00_12781 =
						((BgL_objectz00_bglt)
						((BgL_letzd2varzd2_bglt)
							((BgL_letzd2varzd2_bglt) BgL_o1557z00_7398)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12781, BgL_auxz00_12785);
				}
				((BgL_objectz00_bglt)
					((BgL_letzd2varzd2_bglt)
						((BgL_letzd2varzd2_bglt) BgL_o1557z00_7398)));
				{	/* Liveness/types.scm 179 */
					long BgL_arg3474z00_8871;

					BgL_arg3474z00_8871 =
						BGL_CLASS_NUM(BGl_letzd2varzf2livenessz20zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_letzd2varzd2_bglt)
								((BgL_letzd2varzd2_bglt) BgL_o1557z00_7398))),
						BgL_arg3474z00_8871);
				}
				return
					((BgL_letzd2varzd2_bglt)
					((BgL_letzd2varzd2_bglt)
						((BgL_letzd2varzd2_bglt) BgL_o1557z00_7398)));
			}
		}

	}



/* &lambda3469 */
	BgL_letzd2varzd2_bglt BGl_z62lambda3469z62zzliveness_typesz00(obj_t
		BgL_envz00_7399, obj_t BgL_loc1546z00_7400, obj_t BgL_type1547z00_7401,
		obj_t BgL_sidezd2effect1548zd2_7402, obj_t BgL_key1549z00_7403,
		obj_t BgL_bindings1550z00_7404, obj_t BgL_body1551z00_7405,
		obj_t BgL_removablezf31552zf3_7406, obj_t BgL_def1553z00_7407,
		obj_t BgL_use1554z00_7408, obj_t BgL_in1555z00_7409,
		obj_t BgL_out1556z00_7410)
	{
		{	/* Liveness/types.scm 179 */
			{	/* Liveness/types.scm 179 */
				bool_t BgL_removablezf31552zf3_8874;

				BgL_removablezf31552zf3_8874 = CBOOL(BgL_removablezf31552zf3_7406);
				{	/* Liveness/types.scm 179 */
					BgL_letzd2varzd2_bglt BgL_new1815z00_8879;

					{	/* Liveness/types.scm 179 */
						BgL_letzd2varzd2_bglt BgL_tmp1813z00_8880;
						BgL_letzd2varzf2livenessz20_bglt BgL_wide1814z00_8881;

						{
							BgL_letzd2varzd2_bglt BgL_auxz00_12800;

							{	/* Liveness/types.scm 179 */
								BgL_letzd2varzd2_bglt BgL_new1812z00_8882;

								BgL_new1812z00_8882 =
									((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_letzd2varzd2_bgl))));
								{	/* Liveness/types.scm 179 */
									long BgL_arg3471z00_8883;

									BgL_arg3471z00_8883 =
										BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1812z00_8882),
										BgL_arg3471z00_8883);
								}
								{	/* Liveness/types.scm 179 */
									BgL_objectz00_bglt BgL_tmpz00_12805;

									BgL_tmpz00_12805 = ((BgL_objectz00_bglt) BgL_new1812z00_8882);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12805, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1812z00_8882);
								BgL_auxz00_12800 = BgL_new1812z00_8882;
							}
							BgL_tmp1813z00_8880 = ((BgL_letzd2varzd2_bglt) BgL_auxz00_12800);
						}
						BgL_wide1814z00_8881 =
							((BgL_letzd2varzf2livenessz20_bglt)
							BOBJECT(GC_MALLOC(sizeof(struct
										BgL_letzd2varzf2livenessz20_bgl))));
						{	/* Liveness/types.scm 179 */
							obj_t BgL_auxz00_12813;
							BgL_objectz00_bglt BgL_tmpz00_12811;

							BgL_auxz00_12813 = ((obj_t) BgL_wide1814z00_8881);
							BgL_tmpz00_12811 = ((BgL_objectz00_bglt) BgL_tmp1813z00_8880);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12811, BgL_auxz00_12813);
						}
						((BgL_objectz00_bglt) BgL_tmp1813z00_8880);
						{	/* Liveness/types.scm 179 */
							long BgL_arg3470z00_8884;

							BgL_arg3470z00_8884 =
								BGL_CLASS_NUM(BGl_letzd2varzf2livenessz20zzliveness_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1813z00_8880),
								BgL_arg3470z00_8884);
						}
						BgL_new1815z00_8879 = ((BgL_letzd2varzd2_bglt) BgL_tmp1813z00_8880);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1815z00_8879)))->BgL_locz00) =
						((obj_t) BgL_loc1546z00_7400), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1815z00_8879)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1547z00_7401)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1815z00_8879)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1548zd2_7402), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1815z00_8879)))->BgL_keyz00) =
						((obj_t) BgL_key1549z00_7403), BUNSPEC);
					((((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
										BgL_new1815z00_8879)))->BgL_bindingsz00) =
						((obj_t) BgL_bindings1550z00_7404), BUNSPEC);
					((((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
										BgL_new1815z00_8879)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1551z00_7405)),
						BUNSPEC);
					((((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
										BgL_new1815z00_8879)))->BgL_removablezf3zf3) =
						((bool_t) BgL_removablezf31552zf3_8874), BUNSPEC);
					{
						BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12837;

						{
							obj_t BgL_auxz00_12838;

							{	/* Liveness/types.scm 179 */
								BgL_objectz00_bglt BgL_tmpz00_12839;

								BgL_tmpz00_12839 = ((BgL_objectz00_bglt) BgL_new1815z00_8879);
								BgL_auxz00_12838 = BGL_OBJECT_WIDENING(BgL_tmpz00_12839);
							}
							BgL_auxz00_12837 =
								((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12838);
						}
						((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12837))->
								BgL_defz00) = ((obj_t) ((obj_t) BgL_def1553z00_7407)), BUNSPEC);
					}
					{
						BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12845;

						{
							obj_t BgL_auxz00_12846;

							{	/* Liveness/types.scm 179 */
								BgL_objectz00_bglt BgL_tmpz00_12847;

								BgL_tmpz00_12847 = ((BgL_objectz00_bglt) BgL_new1815z00_8879);
								BgL_auxz00_12846 = BGL_OBJECT_WIDENING(BgL_tmpz00_12847);
							}
							BgL_auxz00_12845 =
								((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12846);
						}
						((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12845))->
								BgL_usez00) = ((obj_t) ((obj_t) BgL_use1554z00_7408)), BUNSPEC);
					}
					{
						BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12853;

						{
							obj_t BgL_auxz00_12854;

							{	/* Liveness/types.scm 179 */
								BgL_objectz00_bglt BgL_tmpz00_12855;

								BgL_tmpz00_12855 = ((BgL_objectz00_bglt) BgL_new1815z00_8879);
								BgL_auxz00_12854 = BGL_OBJECT_WIDENING(BgL_tmpz00_12855);
							}
							BgL_auxz00_12853 =
								((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12854);
						}
						((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12853))->
								BgL_inz00) = ((obj_t) ((obj_t) BgL_in1555z00_7409)), BUNSPEC);
					}
					{
						BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12861;

						{
							obj_t BgL_auxz00_12862;

							{	/* Liveness/types.scm 179 */
								BgL_objectz00_bglt BgL_tmpz00_12863;

								BgL_tmpz00_12863 = ((BgL_objectz00_bglt) BgL_new1815z00_8879);
								BgL_auxz00_12862 = BGL_OBJECT_WIDENING(BgL_tmpz00_12863);
							}
							BgL_auxz00_12861 =
								((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12862);
						}
						((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12861))->
								BgL_outz00) = ((obj_t) ((obj_t) BgL_out1556z00_7410)), BUNSPEC);
					}
					return BgL_new1815z00_8879;
				}
			}
		}

	}



/* &<@anonymous:3515> */
	obj_t BGl_z62zc3z04anonymousza33515ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7411)
	{
		{	/* Liveness/types.scm 179 */
			return BNIL;
		}

	}



/* &lambda3514 */
	obj_t BGl_z62lambda3514z62zzliveness_typesz00(obj_t BgL_envz00_7412,
		obj_t BgL_oz00_7413, obj_t BgL_vz00_7414)
	{
		{	/* Liveness/types.scm 179 */
			{
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12869;

				{
					obj_t BgL_auxz00_12870;

					{	/* Liveness/types.scm 179 */
						BgL_objectz00_bglt BgL_tmpz00_12871;

						BgL_tmpz00_12871 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_7413));
						BgL_auxz00_12870 = BGL_OBJECT_WIDENING(BgL_tmpz00_12871);
					}
					BgL_auxz00_12869 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12870);
				}
				return
					((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12869))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7414)), BUNSPEC);
			}
		}

	}



/* &lambda3513 */
	obj_t BGl_z62lambda3513z62zzliveness_typesz00(obj_t BgL_envz00_7415,
		obj_t BgL_oz00_7416)
	{
		{	/* Liveness/types.scm 179 */
			{
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12878;

				{
					obj_t BgL_auxz00_12879;

					{	/* Liveness/types.scm 179 */
						BgL_objectz00_bglt BgL_tmpz00_12880;

						BgL_tmpz00_12880 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_7416));
						BgL_auxz00_12879 = BGL_OBJECT_WIDENING(BgL_tmpz00_12880);
					}
					BgL_auxz00_12878 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12879);
				}
				return
					(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12878))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3507> */
	obj_t BGl_z62zc3z04anonymousza33507ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7417)
	{
		{	/* Liveness/types.scm 179 */
			return BNIL;
		}

	}



/* &lambda3506 */
	obj_t BGl_z62lambda3506z62zzliveness_typesz00(obj_t BgL_envz00_7418,
		obj_t BgL_oz00_7419, obj_t BgL_vz00_7420)
	{
		{	/* Liveness/types.scm 179 */
			{
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12886;

				{
					obj_t BgL_auxz00_12887;

					{	/* Liveness/types.scm 179 */
						BgL_objectz00_bglt BgL_tmpz00_12888;

						BgL_tmpz00_12888 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_7419));
						BgL_auxz00_12887 = BGL_OBJECT_WIDENING(BgL_tmpz00_12888);
					}
					BgL_auxz00_12886 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12887);
				}
				return
					((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12886))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7420)), BUNSPEC);
			}
		}

	}



/* &lambda3505 */
	obj_t BGl_z62lambda3505z62zzliveness_typesz00(obj_t BgL_envz00_7421,
		obj_t BgL_oz00_7422)
	{
		{	/* Liveness/types.scm 179 */
			{
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12895;

				{
					obj_t BgL_auxz00_12896;

					{	/* Liveness/types.scm 179 */
						BgL_objectz00_bglt BgL_tmpz00_12897;

						BgL_tmpz00_12897 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_7422));
						BgL_auxz00_12896 = BGL_OBJECT_WIDENING(BgL_tmpz00_12897);
					}
					BgL_auxz00_12895 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12896);
				}
				return
					(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12895))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3496> */
	obj_t BGl_z62zc3z04anonymousza33496ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7423)
	{
		{	/* Liveness/types.scm 179 */
			return BNIL;
		}

	}



/* &lambda3495 */
	obj_t BGl_z62lambda3495z62zzliveness_typesz00(obj_t BgL_envz00_7424,
		obj_t BgL_oz00_7425, obj_t BgL_vz00_7426)
	{
		{	/* Liveness/types.scm 179 */
			{
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12903;

				{
					obj_t BgL_auxz00_12904;

					{	/* Liveness/types.scm 179 */
						BgL_objectz00_bglt BgL_tmpz00_12905;

						BgL_tmpz00_12905 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_7425));
						BgL_auxz00_12904 = BGL_OBJECT_WIDENING(BgL_tmpz00_12905);
					}
					BgL_auxz00_12903 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12904);
				}
				return
					((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12903))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7426)), BUNSPEC);
			}
		}

	}



/* &lambda3494 */
	obj_t BGl_z62lambda3494z62zzliveness_typesz00(obj_t BgL_envz00_7427,
		obj_t BgL_oz00_7428)
	{
		{	/* Liveness/types.scm 179 */
			{
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12912;

				{
					obj_t BgL_auxz00_12913;

					{	/* Liveness/types.scm 179 */
						BgL_objectz00_bglt BgL_tmpz00_12914;

						BgL_tmpz00_12914 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_7428));
						BgL_auxz00_12913 = BGL_OBJECT_WIDENING(BgL_tmpz00_12914);
					}
					BgL_auxz00_12912 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12913);
				}
				return
					(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12912))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3488> */
	obj_t BGl_z62zc3z04anonymousza33488ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7429)
	{
		{	/* Liveness/types.scm 179 */
			return BNIL;
		}

	}



/* &lambda3487 */
	obj_t BGl_z62lambda3487z62zzliveness_typesz00(obj_t BgL_envz00_7430,
		obj_t BgL_oz00_7431, obj_t BgL_vz00_7432)
	{
		{	/* Liveness/types.scm 179 */
			{
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12920;

				{
					obj_t BgL_auxz00_12921;

					{	/* Liveness/types.scm 179 */
						BgL_objectz00_bglt BgL_tmpz00_12922;

						BgL_tmpz00_12922 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_7431));
						BgL_auxz00_12921 = BGL_OBJECT_WIDENING(BgL_tmpz00_12922);
					}
					BgL_auxz00_12920 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12921);
				}
				return
					((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12920))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7432)), BUNSPEC);
			}
		}

	}



/* &lambda3486 */
	obj_t BGl_z62lambda3486z62zzliveness_typesz00(obj_t BgL_envz00_7433,
		obj_t BgL_oz00_7434)
	{
		{	/* Liveness/types.scm 179 */
			{
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_12929;

				{
					obj_t BgL_auxz00_12930;

					{	/* Liveness/types.scm 179 */
						BgL_objectz00_bglt BgL_tmpz00_12931;

						BgL_tmpz00_12931 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_7434));
						BgL_auxz00_12930 = BGL_OBJECT_WIDENING(BgL_tmpz00_12931);
					}
					BgL_auxz00_12929 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_12930);
				}
				return
					(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_12929))->
					BgL_defz00);
			}
		}

	}



/* &lambda3407 */
	BgL_letzd2funzd2_bglt BGl_z62lambda3407z62zzliveness_typesz00(obj_t
		BgL_envz00_7435, obj_t BgL_o1543z00_7436)
	{
		{	/* Liveness/types.scm 173 */
			{	/* Liveness/types.scm 173 */
				long BgL_arg3410z00_8898;

				{	/* Liveness/types.scm 173 */
					obj_t BgL_arg3413z00_8899;

					{	/* Liveness/types.scm 173 */
						obj_t BgL_arg3416z00_8900;

						{	/* Liveness/types.scm 173 */
							obj_t BgL_arg1815z00_8901;
							long BgL_arg1816z00_8902;

							BgL_arg1815z00_8901 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 173 */
								long BgL_arg1817z00_8903;

								BgL_arg1817z00_8903 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_letzd2funzd2_bglt) BgL_o1543z00_7436)));
								BgL_arg1816z00_8902 = (BgL_arg1817z00_8903 - OBJECT_TYPE);
							}
							BgL_arg3416z00_8900 =
								VECTOR_REF(BgL_arg1815z00_8901, BgL_arg1816z00_8902);
						}
						BgL_arg3413z00_8899 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3416z00_8900);
					}
					{	/* Liveness/types.scm 173 */
						obj_t BgL_tmpz00_12944;

						BgL_tmpz00_12944 = ((obj_t) BgL_arg3413z00_8899);
						BgL_arg3410z00_8898 = BGL_CLASS_NUM(BgL_tmpz00_12944);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_letzd2funzd2_bglt) BgL_o1543z00_7436)), BgL_arg3410z00_8898);
			}
			{	/* Liveness/types.scm 173 */
				BgL_objectz00_bglt BgL_tmpz00_12950;

				BgL_tmpz00_12950 =
					((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_o1543z00_7436));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12950, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_o1543z00_7436));
			return
				((BgL_letzd2funzd2_bglt) ((BgL_letzd2funzd2_bglt) BgL_o1543z00_7436));
		}

	}



/* &<@anonymous:3406> */
	obj_t BGl_z62zc3z04anonymousza33406ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7437, obj_t BgL_new1542z00_7438)
	{
		{	/* Liveness/types.scm 173 */
			{
				BgL_letzd2funzd2_bglt BgL_auxz00_12958;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2funzd2_bglt) BgL_new1542z00_7438))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_12962;

					{	/* Liveness/types.scm 173 */
						obj_t BgL_classz00_8905;

						BgL_classz00_8905 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 173 */
							obj_t BgL__ortest_1117z00_8906;

							BgL__ortest_1117z00_8906 = BGL_CLASS_NIL(BgL_classz00_8905);
							if (CBOOL(BgL__ortest_1117z00_8906))
								{	/* Liveness/types.scm 173 */
									BgL_auxz00_12962 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8906);
								}
							else
								{	/* Liveness/types.scm 173 */
									BgL_auxz00_12962 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8905));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2funzd2_bglt) BgL_new1542z00_7438))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_12962), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_letzd2funzd2_bglt) BgL_new1542z00_7438))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_letzd2funzd2_bglt)
										BgL_new1542z00_7438))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_letzd2funzd2_bglt)
							COBJECT(((BgL_letzd2funzd2_bglt) ((BgL_letzd2funzd2_bglt)
										BgL_new1542z00_7438))))->BgL_localsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_12981;

					{	/* Liveness/types.scm 173 */
						obj_t BgL_classz00_8907;

						BgL_classz00_8907 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 173 */
							obj_t BgL__ortest_1117z00_8908;

							BgL__ortest_1117z00_8908 = BGL_CLASS_NIL(BgL_classz00_8907);
							if (CBOOL(BgL__ortest_1117z00_8908))
								{	/* Liveness/types.scm 173 */
									BgL_auxz00_12981 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8908);
								}
							else
								{	/* Liveness/types.scm 173 */
									BgL_auxz00_12981 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8907));
								}
						}
					}
					((((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt)
										((BgL_letzd2funzd2_bglt) BgL_new1542z00_7438))))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_12981), BUNSPEC);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_12991;

					{
						obj_t BgL_auxz00_12992;

						{	/* Liveness/types.scm 173 */
							BgL_objectz00_bglt BgL_tmpz00_12993;

							BgL_tmpz00_12993 =
								((BgL_objectz00_bglt)
								((BgL_letzd2funzd2_bglt) BgL_new1542z00_7438));
							BgL_auxz00_12992 = BGL_OBJECT_WIDENING(BgL_tmpz00_12993);
						}
						BgL_auxz00_12991 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_12992);
					}
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_12991))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_12999;

					{
						obj_t BgL_auxz00_13000;

						{	/* Liveness/types.scm 173 */
							BgL_objectz00_bglt BgL_tmpz00_13001;

							BgL_tmpz00_13001 =
								((BgL_objectz00_bglt)
								((BgL_letzd2funzd2_bglt) BgL_new1542z00_7438));
							BgL_auxz00_13000 = BGL_OBJECT_WIDENING(BgL_tmpz00_13001);
						}
						BgL_auxz00_12999 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13000);
					}
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_12999))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13007;

					{
						obj_t BgL_auxz00_13008;

						{	/* Liveness/types.scm 173 */
							BgL_objectz00_bglt BgL_tmpz00_13009;

							BgL_tmpz00_13009 =
								((BgL_objectz00_bglt)
								((BgL_letzd2funzd2_bglt) BgL_new1542z00_7438));
							BgL_auxz00_13008 = BGL_OBJECT_WIDENING(BgL_tmpz00_13009);
						}
						BgL_auxz00_13007 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13008);
					}
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13007))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13015;

					{
						obj_t BgL_auxz00_13016;

						{	/* Liveness/types.scm 173 */
							BgL_objectz00_bglt BgL_tmpz00_13017;

							BgL_tmpz00_13017 =
								((BgL_objectz00_bglt)
								((BgL_letzd2funzd2_bglt) BgL_new1542z00_7438));
							BgL_auxz00_13016 = BGL_OBJECT_WIDENING(BgL_tmpz00_13017);
						}
						BgL_auxz00_13015 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13016);
					}
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13015))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_12958 = ((BgL_letzd2funzd2_bglt) BgL_new1542z00_7438);
				return ((obj_t) BgL_auxz00_12958);
			}
		}

	}



/* &lambda3404 */
	BgL_letzd2funzd2_bglt BGl_z62lambda3404z62zzliveness_typesz00(obj_t
		BgL_envz00_7439, obj_t BgL_o1539z00_7440)
	{
		{	/* Liveness/types.scm 173 */
			{	/* Liveness/types.scm 173 */
				BgL_letzd2funzf2livenessz20_bglt BgL_wide1541z00_8910;

				BgL_wide1541z00_8910 =
					((BgL_letzd2funzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_letzd2funzf2livenessz20_bgl))));
				{	/* Liveness/types.scm 173 */
					obj_t BgL_auxz00_13030;
					BgL_objectz00_bglt BgL_tmpz00_13026;

					BgL_auxz00_13030 = ((obj_t) BgL_wide1541z00_8910);
					BgL_tmpz00_13026 =
						((BgL_objectz00_bglt)
						((BgL_letzd2funzd2_bglt)
							((BgL_letzd2funzd2_bglt) BgL_o1539z00_7440)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13026, BgL_auxz00_13030);
				}
				((BgL_objectz00_bglt)
					((BgL_letzd2funzd2_bglt)
						((BgL_letzd2funzd2_bglt) BgL_o1539z00_7440)));
				{	/* Liveness/types.scm 173 */
					long BgL_arg3405z00_8911;

					BgL_arg3405z00_8911 =
						BGL_CLASS_NUM(BGl_letzd2funzf2livenessz20zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_letzd2funzd2_bglt)
								((BgL_letzd2funzd2_bglt) BgL_o1539z00_7440))),
						BgL_arg3405z00_8911);
				}
				return
					((BgL_letzd2funzd2_bglt)
					((BgL_letzd2funzd2_bglt)
						((BgL_letzd2funzd2_bglt) BgL_o1539z00_7440)));
			}
		}

	}



/* &lambda3401 */
	BgL_letzd2funzd2_bglt BGl_z62lambda3401z62zzliveness_typesz00(obj_t
		BgL_envz00_7441, obj_t BgL_loc1529z00_7442, obj_t BgL_type1530z00_7443,
		obj_t BgL_sidezd2effect1531zd2_7444, obj_t BgL_key1532z00_7445,
		obj_t BgL_locals1533z00_7446, obj_t BgL_body1534z00_7447,
		obj_t BgL_def1535z00_7448, obj_t BgL_use1536z00_7449,
		obj_t BgL_in1537z00_7450, obj_t BgL_out1538z00_7451)
	{
		{	/* Liveness/types.scm 173 */
			{	/* Liveness/types.scm 173 */
				BgL_letzd2funzd2_bglt BgL_new1810z00_8918;

				{	/* Liveness/types.scm 173 */
					BgL_letzd2funzd2_bglt BgL_tmp1808z00_8919;
					BgL_letzd2funzf2livenessz20_bglt BgL_wide1809z00_8920;

					{
						BgL_letzd2funzd2_bglt BgL_auxz00_13044;

						{	/* Liveness/types.scm 173 */
							BgL_letzd2funzd2_bglt BgL_new1807z00_8921;

							BgL_new1807z00_8921 =
								((BgL_letzd2funzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_letzd2funzd2_bgl))));
							{	/* Liveness/types.scm 173 */
								long BgL_arg3403z00_8922;

								BgL_arg3403z00_8922 =
									BGL_CLASS_NUM(BGl_letzd2funzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1807z00_8921),
									BgL_arg3403z00_8922);
							}
							{	/* Liveness/types.scm 173 */
								BgL_objectz00_bglt BgL_tmpz00_13049;

								BgL_tmpz00_13049 = ((BgL_objectz00_bglt) BgL_new1807z00_8921);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13049, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1807z00_8921);
							BgL_auxz00_13044 = BgL_new1807z00_8921;
						}
						BgL_tmp1808z00_8919 = ((BgL_letzd2funzd2_bglt) BgL_auxz00_13044);
					}
					BgL_wide1809z00_8920 =
						((BgL_letzd2funzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_letzd2funzf2livenessz20_bgl))));
					{	/* Liveness/types.scm 173 */
						obj_t BgL_auxz00_13057;
						BgL_objectz00_bglt BgL_tmpz00_13055;

						BgL_auxz00_13057 = ((obj_t) BgL_wide1809z00_8920);
						BgL_tmpz00_13055 = ((BgL_objectz00_bglt) BgL_tmp1808z00_8919);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13055, BgL_auxz00_13057);
					}
					((BgL_objectz00_bglt) BgL_tmp1808z00_8919);
					{	/* Liveness/types.scm 173 */
						long BgL_arg3402z00_8923;

						BgL_arg3402z00_8923 =
							BGL_CLASS_NUM(BGl_letzd2funzf2livenessz20zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1808z00_8919), BgL_arg3402z00_8923);
					}
					BgL_new1810z00_8918 = ((BgL_letzd2funzd2_bglt) BgL_tmp1808z00_8919);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1810z00_8918)))->BgL_locz00) =
					((obj_t) BgL_loc1529z00_7442), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1810z00_8918)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1530z00_7443)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1810z00_8918)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1531zd2_7444), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1810z00_8918)))->BgL_keyz00) =
					((obj_t) BgL_key1532z00_7445), BUNSPEC);
				((((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
									BgL_new1810z00_8918)))->BgL_localsz00) =
					((obj_t) BgL_locals1533z00_7446), BUNSPEC);
				((((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
									BgL_new1810z00_8918)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1534z00_7447)),
					BUNSPEC);
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13079;

					{
						obj_t BgL_auxz00_13080;

						{	/* Liveness/types.scm 173 */
							BgL_objectz00_bglt BgL_tmpz00_13081;

							BgL_tmpz00_13081 = ((BgL_objectz00_bglt) BgL_new1810z00_8918);
							BgL_auxz00_13080 = BGL_OBJECT_WIDENING(BgL_tmpz00_13081);
						}
						BgL_auxz00_13079 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13080);
					}
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13079))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1535z00_7448)), BUNSPEC);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13087;

					{
						obj_t BgL_auxz00_13088;

						{	/* Liveness/types.scm 173 */
							BgL_objectz00_bglt BgL_tmpz00_13089;

							BgL_tmpz00_13089 = ((BgL_objectz00_bglt) BgL_new1810z00_8918);
							BgL_auxz00_13088 = BGL_OBJECT_WIDENING(BgL_tmpz00_13089);
						}
						BgL_auxz00_13087 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13088);
					}
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13087))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1536z00_7449)), BUNSPEC);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13095;

					{
						obj_t BgL_auxz00_13096;

						{	/* Liveness/types.scm 173 */
							BgL_objectz00_bglt BgL_tmpz00_13097;

							BgL_tmpz00_13097 = ((BgL_objectz00_bglt) BgL_new1810z00_8918);
							BgL_auxz00_13096 = BGL_OBJECT_WIDENING(BgL_tmpz00_13097);
						}
						BgL_auxz00_13095 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13096);
					}
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13095))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1537z00_7450)), BUNSPEC);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13103;

					{
						obj_t BgL_auxz00_13104;

						{	/* Liveness/types.scm 173 */
							BgL_objectz00_bglt BgL_tmpz00_13105;

							BgL_tmpz00_13105 = ((BgL_objectz00_bglt) BgL_new1810z00_8918);
							BgL_auxz00_13104 = BGL_OBJECT_WIDENING(BgL_tmpz00_13105);
						}
						BgL_auxz00_13103 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13104);
					}
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13103))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1538z00_7451)), BUNSPEC);
				}
				return BgL_new1810z00_8918;
			}
		}

	}



/* &<@anonymous:3460> */
	obj_t BGl_z62zc3z04anonymousza33460ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7452)
	{
		{	/* Liveness/types.scm 173 */
			return BNIL;
		}

	}



/* &lambda3459 */
	obj_t BGl_z62lambda3459z62zzliveness_typesz00(obj_t BgL_envz00_7453,
		obj_t BgL_oz00_7454, obj_t BgL_vz00_7455)
	{
		{	/* Liveness/types.scm 173 */
			{
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13111;

				{
					obj_t BgL_auxz00_13112;

					{	/* Liveness/types.scm 173 */
						BgL_objectz00_bglt BgL_tmpz00_13113;

						BgL_tmpz00_13113 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_7454));
						BgL_auxz00_13112 = BGL_OBJECT_WIDENING(BgL_tmpz00_13113);
					}
					BgL_auxz00_13111 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13112);
				}
				return
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13111))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7455)), BUNSPEC);
			}
		}

	}



/* &lambda3458 */
	obj_t BGl_z62lambda3458z62zzliveness_typesz00(obj_t BgL_envz00_7456,
		obj_t BgL_oz00_7457)
	{
		{	/* Liveness/types.scm 173 */
			{
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13120;

				{
					obj_t BgL_auxz00_13121;

					{	/* Liveness/types.scm 173 */
						BgL_objectz00_bglt BgL_tmpz00_13122;

						BgL_tmpz00_13122 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_7457));
						BgL_auxz00_13121 = BGL_OBJECT_WIDENING(BgL_tmpz00_13122);
					}
					BgL_auxz00_13120 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13121);
				}
				return
					(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13120))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3452> */
	obj_t BGl_z62zc3z04anonymousza33452ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7458)
	{
		{	/* Liveness/types.scm 173 */
			return BNIL;
		}

	}



/* &lambda3451 */
	obj_t BGl_z62lambda3451z62zzliveness_typesz00(obj_t BgL_envz00_7459,
		obj_t BgL_oz00_7460, obj_t BgL_vz00_7461)
	{
		{	/* Liveness/types.scm 173 */
			{
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13128;

				{
					obj_t BgL_auxz00_13129;

					{	/* Liveness/types.scm 173 */
						BgL_objectz00_bglt BgL_tmpz00_13130;

						BgL_tmpz00_13130 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_7460));
						BgL_auxz00_13129 = BGL_OBJECT_WIDENING(BgL_tmpz00_13130);
					}
					BgL_auxz00_13128 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13129);
				}
				return
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13128))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7461)), BUNSPEC);
			}
		}

	}



/* &lambda3450 */
	obj_t BGl_z62lambda3450z62zzliveness_typesz00(obj_t BgL_envz00_7462,
		obj_t BgL_oz00_7463)
	{
		{	/* Liveness/types.scm 173 */
			{
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13137;

				{
					obj_t BgL_auxz00_13138;

					{	/* Liveness/types.scm 173 */
						BgL_objectz00_bglt BgL_tmpz00_13139;

						BgL_tmpz00_13139 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_7463));
						BgL_auxz00_13138 = BGL_OBJECT_WIDENING(BgL_tmpz00_13139);
					}
					BgL_auxz00_13137 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13138);
				}
				return
					(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13137))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3443> */
	obj_t BGl_z62zc3z04anonymousza33443ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7464)
	{
		{	/* Liveness/types.scm 173 */
			return BNIL;
		}

	}



/* &lambda3442 */
	obj_t BGl_z62lambda3442z62zzliveness_typesz00(obj_t BgL_envz00_7465,
		obj_t BgL_oz00_7466, obj_t BgL_vz00_7467)
	{
		{	/* Liveness/types.scm 173 */
			{
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13145;

				{
					obj_t BgL_auxz00_13146;

					{	/* Liveness/types.scm 173 */
						BgL_objectz00_bglt BgL_tmpz00_13147;

						BgL_tmpz00_13147 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_7466));
						BgL_auxz00_13146 = BGL_OBJECT_WIDENING(BgL_tmpz00_13147);
					}
					BgL_auxz00_13145 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13146);
				}
				return
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13145))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7467)), BUNSPEC);
			}
		}

	}



/* &lambda3441 */
	obj_t BGl_z62lambda3441z62zzliveness_typesz00(obj_t BgL_envz00_7468,
		obj_t BgL_oz00_7469)
	{
		{	/* Liveness/types.scm 173 */
			{
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13154;

				{
					obj_t BgL_auxz00_13155;

					{	/* Liveness/types.scm 173 */
						BgL_objectz00_bglt BgL_tmpz00_13156;

						BgL_tmpz00_13156 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_7469));
						BgL_auxz00_13155 = BGL_OBJECT_WIDENING(BgL_tmpz00_13156);
					}
					BgL_auxz00_13154 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13155);
				}
				return
					(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13154))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3433> */
	obj_t BGl_z62zc3z04anonymousza33433ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7470)
	{
		{	/* Liveness/types.scm 173 */
			return BNIL;
		}

	}



/* &lambda3432 */
	obj_t BGl_z62lambda3432z62zzliveness_typesz00(obj_t BgL_envz00_7471,
		obj_t BgL_oz00_7472, obj_t BgL_vz00_7473)
	{
		{	/* Liveness/types.scm 173 */
			{
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13162;

				{
					obj_t BgL_auxz00_13163;

					{	/* Liveness/types.scm 173 */
						BgL_objectz00_bglt BgL_tmpz00_13164;

						BgL_tmpz00_13164 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_7472));
						BgL_auxz00_13163 = BGL_OBJECT_WIDENING(BgL_tmpz00_13164);
					}
					BgL_auxz00_13162 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13163);
				}
				return
					((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13162))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7473)), BUNSPEC);
			}
		}

	}



/* &lambda3431 */
	obj_t BGl_z62lambda3431z62zzliveness_typesz00(obj_t BgL_envz00_7474,
		obj_t BgL_oz00_7475)
	{
		{	/* Liveness/types.scm 173 */
			{
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_13171;

				{
					obj_t BgL_auxz00_13172;

					{	/* Liveness/types.scm 173 */
						BgL_objectz00_bglt BgL_tmpz00_13173;

						BgL_tmpz00_13173 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_7475));
						BgL_auxz00_13172 = BGL_OBJECT_WIDENING(BgL_tmpz00_13173);
					}
					BgL_auxz00_13171 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_13172);
				}
				return
					(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_13171))->
					BgL_defz00);
			}
		}

	}



/* &lambda3354 */
	BgL_switchz00_bglt BGl_z62lambda3354z62zzliveness_typesz00(obj_t
		BgL_envz00_7476, obj_t BgL_o1527z00_7477)
	{
		{	/* Liveness/types.scm 167 */
			{	/* Liveness/types.scm 167 */
				long BgL_arg3356z00_8937;

				{	/* Liveness/types.scm 167 */
					obj_t BgL_arg3357z00_8938;

					{	/* Liveness/types.scm 167 */
						obj_t BgL_arg3358z00_8939;

						{	/* Liveness/types.scm 167 */
							obj_t BgL_arg1815z00_8940;
							long BgL_arg1816z00_8941;

							BgL_arg1815z00_8940 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 167 */
								long BgL_arg1817z00_8942;

								BgL_arg1817z00_8942 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_switchz00_bglt) BgL_o1527z00_7477)));
								BgL_arg1816z00_8941 = (BgL_arg1817z00_8942 - OBJECT_TYPE);
							}
							BgL_arg3358z00_8939 =
								VECTOR_REF(BgL_arg1815z00_8940, BgL_arg1816z00_8941);
						}
						BgL_arg3357z00_8938 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3358z00_8939);
					}
					{	/* Liveness/types.scm 167 */
						obj_t BgL_tmpz00_13186;

						BgL_tmpz00_13186 = ((obj_t) BgL_arg3357z00_8938);
						BgL_arg3356z00_8937 = BGL_CLASS_NUM(BgL_tmpz00_13186);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_switchz00_bglt) BgL_o1527z00_7477)), BgL_arg3356z00_8937);
			}
			{	/* Liveness/types.scm 167 */
				BgL_objectz00_bglt BgL_tmpz00_13192;

				BgL_tmpz00_13192 =
					((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_o1527z00_7477));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13192, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_o1527z00_7477));
			return ((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_o1527z00_7477));
		}

	}



/* &<@anonymous:3353> */
	obj_t BGl_z62zc3z04anonymousza33353ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7478, obj_t BgL_new1526z00_7479)
	{
		{	/* Liveness/types.scm 167 */
			{
				BgL_switchz00_bglt BgL_auxz00_13200;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_switchz00_bglt) BgL_new1526z00_7479))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13204;

					{	/* Liveness/types.scm 167 */
						obj_t BgL_classz00_8944;

						BgL_classz00_8944 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 167 */
							obj_t BgL__ortest_1117z00_8945;

							BgL__ortest_1117z00_8945 = BGL_CLASS_NIL(BgL_classz00_8944);
							if (CBOOL(BgL__ortest_1117z00_8945))
								{	/* Liveness/types.scm 167 */
									BgL_auxz00_13204 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8945);
								}
							else
								{	/* Liveness/types.scm 167 */
									BgL_auxz00_13204 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8944));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_switchz00_bglt) BgL_new1526z00_7479))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_13204), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_switchz00_bglt) BgL_new1526z00_7479))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_switchz00_bglt)
										BgL_new1526z00_7479))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_13220;

					{	/* Liveness/types.scm 167 */
						obj_t BgL_classz00_8946;

						BgL_classz00_8946 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 167 */
							obj_t BgL__ortest_1117z00_8947;

							BgL__ortest_1117z00_8947 = BGL_CLASS_NIL(BgL_classz00_8946);
							if (CBOOL(BgL__ortest_1117z00_8947))
								{	/* Liveness/types.scm 167 */
									BgL_auxz00_13220 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8947);
								}
							else
								{	/* Liveness/types.scm 167 */
									BgL_auxz00_13220 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8946));
								}
						}
					}
					((((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt)
										((BgL_switchz00_bglt) BgL_new1526z00_7479))))->
							BgL_testz00) = ((BgL_nodez00_bglt) BgL_auxz00_13220), BUNSPEC);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt)
									((BgL_switchz00_bglt) BgL_new1526z00_7479))))->
						BgL_clausesz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13233;

					{	/* Liveness/types.scm 167 */
						obj_t BgL_classz00_8948;

						BgL_classz00_8948 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 167 */
							obj_t BgL__ortest_1117z00_8949;

							BgL__ortest_1117z00_8949 = BGL_CLASS_NIL(BgL_classz00_8948);
							if (CBOOL(BgL__ortest_1117z00_8949))
								{	/* Liveness/types.scm 167 */
									BgL_auxz00_13233 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8949);
								}
							else
								{	/* Liveness/types.scm 167 */
									BgL_auxz00_13233 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8948));
								}
						}
					}
					((((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt)
										((BgL_switchz00_bglt) BgL_new1526z00_7479))))->
							BgL_itemzd2typezd2) =
						((BgL_typez00_bglt) BgL_auxz00_13233), BUNSPEC);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_13243;

					{
						obj_t BgL_auxz00_13244;

						{	/* Liveness/types.scm 167 */
							BgL_objectz00_bglt BgL_tmpz00_13245;

							BgL_tmpz00_13245 =
								((BgL_objectz00_bglt)
								((BgL_switchz00_bglt) BgL_new1526z00_7479));
							BgL_auxz00_13244 = BGL_OBJECT_WIDENING(BgL_tmpz00_13245);
						}
						BgL_auxz00_13243 =
							((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13244);
					}
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13243))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_13251;

					{
						obj_t BgL_auxz00_13252;

						{	/* Liveness/types.scm 167 */
							BgL_objectz00_bglt BgL_tmpz00_13253;

							BgL_tmpz00_13253 =
								((BgL_objectz00_bglt)
								((BgL_switchz00_bglt) BgL_new1526z00_7479));
							BgL_auxz00_13252 = BGL_OBJECT_WIDENING(BgL_tmpz00_13253);
						}
						BgL_auxz00_13251 =
							((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13252);
					}
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13251))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_13259;

					{
						obj_t BgL_auxz00_13260;

						{	/* Liveness/types.scm 167 */
							BgL_objectz00_bglt BgL_tmpz00_13261;

							BgL_tmpz00_13261 =
								((BgL_objectz00_bglt)
								((BgL_switchz00_bglt) BgL_new1526z00_7479));
							BgL_auxz00_13260 = BGL_OBJECT_WIDENING(BgL_tmpz00_13261);
						}
						BgL_auxz00_13259 =
							((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13260);
					}
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13259))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_13267;

					{
						obj_t BgL_auxz00_13268;

						{	/* Liveness/types.scm 167 */
							BgL_objectz00_bglt BgL_tmpz00_13269;

							BgL_tmpz00_13269 =
								((BgL_objectz00_bglt)
								((BgL_switchz00_bglt) BgL_new1526z00_7479));
							BgL_auxz00_13268 = BGL_OBJECT_WIDENING(BgL_tmpz00_13269);
						}
						BgL_auxz00_13267 =
							((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13268);
					}
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13267))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_13200 = ((BgL_switchz00_bglt) BgL_new1526z00_7479);
				return ((obj_t) BgL_auxz00_13200);
			}
		}

	}



/* &lambda3350 */
	BgL_switchz00_bglt BGl_z62lambda3350z62zzliveness_typesz00(obj_t
		BgL_envz00_7480, obj_t BgL_o1523z00_7481)
	{
		{	/* Liveness/types.scm 167 */
			{	/* Liveness/types.scm 167 */
				BgL_switchzf2livenesszf2_bglt BgL_wide1525z00_8951;

				BgL_wide1525z00_8951 =
					((BgL_switchzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_switchzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 167 */
					obj_t BgL_auxz00_13282;
					BgL_objectz00_bglt BgL_tmpz00_13278;

					BgL_auxz00_13282 = ((obj_t) BgL_wide1525z00_8951);
					BgL_tmpz00_13278 =
						((BgL_objectz00_bglt)
						((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_o1523z00_7481)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13278, BgL_auxz00_13282);
				}
				((BgL_objectz00_bglt)
					((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_o1523z00_7481)));
				{	/* Liveness/types.scm 167 */
					long BgL_arg3352z00_8952;

					BgL_arg3352z00_8952 =
						BGL_CLASS_NUM(BGl_switchzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_switchz00_bglt)
								((BgL_switchz00_bglt) BgL_o1523z00_7481))),
						BgL_arg3352z00_8952);
				}
				return
					((BgL_switchz00_bglt)
					((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_o1523z00_7481)));
			}
		}

	}



/* &lambda3338 */
	BgL_switchz00_bglt BGl_z62lambda3338z62zzliveness_typesz00(obj_t
		BgL_envz00_7482, obj_t BgL_loc1512z00_7483, obj_t BgL_type1513z00_7484,
		obj_t BgL_sidezd2effect1514zd2_7485, obj_t BgL_key1515z00_7486,
		obj_t BgL_test1516z00_7487, obj_t BgL_clauses1517z00_7488,
		obj_t BgL_itemzd2type1518zd2_7489, obj_t BgL_def1519z00_7490,
		obj_t BgL_use1520z00_7491, obj_t BgL_in1521z00_7492,
		obj_t BgL_out1522z00_7493)
	{
		{	/* Liveness/types.scm 167 */
			{	/* Liveness/types.scm 167 */
				BgL_switchz00_bglt BgL_new1805z00_8960;

				{	/* Liveness/types.scm 167 */
					BgL_switchz00_bglt BgL_tmp1803z00_8961;
					BgL_switchzf2livenesszf2_bglt BgL_wide1804z00_8962;

					{
						BgL_switchz00_bglt BgL_auxz00_13296;

						{	/* Liveness/types.scm 167 */
							BgL_switchz00_bglt BgL_new1802z00_8963;

							BgL_new1802z00_8963 =
								((BgL_switchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_switchz00_bgl))));
							{	/* Liveness/types.scm 167 */
								long BgL_arg3349z00_8964;

								BgL_arg3349z00_8964 = BGL_CLASS_NUM(BGl_switchz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1802z00_8963),
									BgL_arg3349z00_8964);
							}
							{	/* Liveness/types.scm 167 */
								BgL_objectz00_bglt BgL_tmpz00_13301;

								BgL_tmpz00_13301 = ((BgL_objectz00_bglt) BgL_new1802z00_8963);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13301, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1802z00_8963);
							BgL_auxz00_13296 = BgL_new1802z00_8963;
						}
						BgL_tmp1803z00_8961 = ((BgL_switchz00_bglt) BgL_auxz00_13296);
					}
					BgL_wide1804z00_8962 =
						((BgL_switchzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_switchzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 167 */
						obj_t BgL_auxz00_13309;
						BgL_objectz00_bglt BgL_tmpz00_13307;

						BgL_auxz00_13309 = ((obj_t) BgL_wide1804z00_8962);
						BgL_tmpz00_13307 = ((BgL_objectz00_bglt) BgL_tmp1803z00_8961);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13307, BgL_auxz00_13309);
					}
					((BgL_objectz00_bglt) BgL_tmp1803z00_8961);
					{	/* Liveness/types.scm 167 */
						long BgL_arg3339z00_8965;

						BgL_arg3339z00_8965 =
							BGL_CLASS_NUM(BGl_switchzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1803z00_8961), BgL_arg3339z00_8965);
					}
					BgL_new1805z00_8960 = ((BgL_switchz00_bglt) BgL_tmp1803z00_8961);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1805z00_8960)))->BgL_locz00) =
					((obj_t) BgL_loc1512z00_7483), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1805z00_8960)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1513z00_7484)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1805z00_8960)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1514zd2_7485), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1805z00_8960)))->BgL_keyz00) =
					((obj_t) BgL_key1515z00_7486), BUNSPEC);
				((((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
									BgL_new1805z00_8960)))->BgL_testz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_test1516z00_7487)),
					BUNSPEC);
				((((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
									BgL_new1805z00_8960)))->BgL_clausesz00) =
					((obj_t) BgL_clauses1517z00_7488), BUNSPEC);
				((((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
									BgL_new1805z00_8960)))->BgL_itemzd2typezd2) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_itemzd2type1518zd2_7489)),
					BUNSPEC);
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_13334;

					{
						obj_t BgL_auxz00_13335;

						{	/* Liveness/types.scm 167 */
							BgL_objectz00_bglt BgL_tmpz00_13336;

							BgL_tmpz00_13336 = ((BgL_objectz00_bglt) BgL_new1805z00_8960);
							BgL_auxz00_13335 = BGL_OBJECT_WIDENING(BgL_tmpz00_13336);
						}
						BgL_auxz00_13334 =
							((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13335);
					}
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13334))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1519z00_7490)), BUNSPEC);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_13342;

					{
						obj_t BgL_auxz00_13343;

						{	/* Liveness/types.scm 167 */
							BgL_objectz00_bglt BgL_tmpz00_13344;

							BgL_tmpz00_13344 = ((BgL_objectz00_bglt) BgL_new1805z00_8960);
							BgL_auxz00_13343 = BGL_OBJECT_WIDENING(BgL_tmpz00_13344);
						}
						BgL_auxz00_13342 =
							((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13343);
					}
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13342))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1520z00_7491)), BUNSPEC);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_13350;

					{
						obj_t BgL_auxz00_13351;

						{	/* Liveness/types.scm 167 */
							BgL_objectz00_bglt BgL_tmpz00_13352;

							BgL_tmpz00_13352 = ((BgL_objectz00_bglt) BgL_new1805z00_8960);
							BgL_auxz00_13351 = BGL_OBJECT_WIDENING(BgL_tmpz00_13352);
						}
						BgL_auxz00_13350 =
							((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13351);
					}
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13350))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1521z00_7492)), BUNSPEC);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_13358;

					{
						obj_t BgL_auxz00_13359;

						{	/* Liveness/types.scm 167 */
							BgL_objectz00_bglt BgL_tmpz00_13360;

							BgL_tmpz00_13360 = ((BgL_objectz00_bglt) BgL_new1805z00_8960);
							BgL_auxz00_13359 = BGL_OBJECT_WIDENING(BgL_tmpz00_13360);
						}
						BgL_auxz00_13358 =
							((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13359);
					}
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13358))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1522z00_7493)), BUNSPEC);
				}
				return BgL_new1805z00_8960;
			}
		}

	}



/* &<@anonymous:3394> */
	obj_t BGl_z62zc3z04anonymousza33394ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7494)
	{
		{	/* Liveness/types.scm 167 */
			return BNIL;
		}

	}



/* &lambda3393 */
	obj_t BGl_z62lambda3393z62zzliveness_typesz00(obj_t BgL_envz00_7495,
		obj_t BgL_oz00_7496, obj_t BgL_vz00_7497)
	{
		{	/* Liveness/types.scm 167 */
			{
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_13366;

				{
					obj_t BgL_auxz00_13367;

					{	/* Liveness/types.scm 167 */
						BgL_objectz00_bglt BgL_tmpz00_13368;

						BgL_tmpz00_13368 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_7496));
						BgL_auxz00_13367 = BGL_OBJECT_WIDENING(BgL_tmpz00_13368);
					}
					BgL_auxz00_13366 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13367);
				}
				return
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13366))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7497)), BUNSPEC);
			}
		}

	}



/* &lambda3392 */
	obj_t BGl_z62lambda3392z62zzliveness_typesz00(obj_t BgL_envz00_7498,
		obj_t BgL_oz00_7499)
	{
		{	/* Liveness/types.scm 167 */
			{
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_13375;

				{
					obj_t BgL_auxz00_13376;

					{	/* Liveness/types.scm 167 */
						BgL_objectz00_bglt BgL_tmpz00_13377;

						BgL_tmpz00_13377 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_7499));
						BgL_auxz00_13376 = BGL_OBJECT_WIDENING(BgL_tmpz00_13377);
					}
					BgL_auxz00_13375 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13376);
				}
				return
					(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13375))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3385> */
	obj_t BGl_z62zc3z04anonymousza33385ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7500)
	{
		{	/* Liveness/types.scm 167 */
			return BNIL;
		}

	}



/* &lambda3384 */
	obj_t BGl_z62lambda3384z62zzliveness_typesz00(obj_t BgL_envz00_7501,
		obj_t BgL_oz00_7502, obj_t BgL_vz00_7503)
	{
		{	/* Liveness/types.scm 167 */
			{
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_13383;

				{
					obj_t BgL_auxz00_13384;

					{	/* Liveness/types.scm 167 */
						BgL_objectz00_bglt BgL_tmpz00_13385;

						BgL_tmpz00_13385 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_7502));
						BgL_auxz00_13384 = BGL_OBJECT_WIDENING(BgL_tmpz00_13385);
					}
					BgL_auxz00_13383 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13384);
				}
				return
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13383))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7503)), BUNSPEC);
			}
		}

	}



/* &lambda3383 */
	obj_t BGl_z62lambda3383z62zzliveness_typesz00(obj_t BgL_envz00_7504,
		obj_t BgL_oz00_7505)
	{
		{	/* Liveness/types.scm 167 */
			{
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_13392;

				{
					obj_t BgL_auxz00_13393;

					{	/* Liveness/types.scm 167 */
						BgL_objectz00_bglt BgL_tmpz00_13394;

						BgL_tmpz00_13394 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_7505));
						BgL_auxz00_13393 = BGL_OBJECT_WIDENING(BgL_tmpz00_13394);
					}
					BgL_auxz00_13392 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13393);
				}
				return
					(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13392))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3374> */
	obj_t BGl_z62zc3z04anonymousza33374ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7506)
	{
		{	/* Liveness/types.scm 167 */
			return BNIL;
		}

	}



/* &lambda3373 */
	obj_t BGl_z62lambda3373z62zzliveness_typesz00(obj_t BgL_envz00_7507,
		obj_t BgL_oz00_7508, obj_t BgL_vz00_7509)
	{
		{	/* Liveness/types.scm 167 */
			{
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_13400;

				{
					obj_t BgL_auxz00_13401;

					{	/* Liveness/types.scm 167 */
						BgL_objectz00_bglt BgL_tmpz00_13402;

						BgL_tmpz00_13402 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_7508));
						BgL_auxz00_13401 = BGL_OBJECT_WIDENING(BgL_tmpz00_13402);
					}
					BgL_auxz00_13400 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13401);
				}
				return
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13400))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7509)), BUNSPEC);
			}
		}

	}



/* &lambda3372 */
	obj_t BGl_z62lambda3372z62zzliveness_typesz00(obj_t BgL_envz00_7510,
		obj_t BgL_oz00_7511)
	{
		{	/* Liveness/types.scm 167 */
			{
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_13409;

				{
					obj_t BgL_auxz00_13410;

					{	/* Liveness/types.scm 167 */
						BgL_objectz00_bglt BgL_tmpz00_13411;

						BgL_tmpz00_13411 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_7511));
						BgL_auxz00_13410 = BGL_OBJECT_WIDENING(BgL_tmpz00_13411);
					}
					BgL_auxz00_13409 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13410);
				}
				return
					(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13409))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3365> */
	obj_t BGl_z62zc3z04anonymousza33365ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7512)
	{
		{	/* Liveness/types.scm 167 */
			return BNIL;
		}

	}



/* &lambda3364 */
	obj_t BGl_z62lambda3364z62zzliveness_typesz00(obj_t BgL_envz00_7513,
		obj_t BgL_oz00_7514, obj_t BgL_vz00_7515)
	{
		{	/* Liveness/types.scm 167 */
			{
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_13417;

				{
					obj_t BgL_auxz00_13418;

					{	/* Liveness/types.scm 167 */
						BgL_objectz00_bglt BgL_tmpz00_13419;

						BgL_tmpz00_13419 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_7514));
						BgL_auxz00_13418 = BGL_OBJECT_WIDENING(BgL_tmpz00_13419);
					}
					BgL_auxz00_13417 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13418);
				}
				return
					((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13417))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7515)), BUNSPEC);
			}
		}

	}



/* &lambda3363 */
	obj_t BGl_z62lambda3363z62zzliveness_typesz00(obj_t BgL_envz00_7516,
		obj_t BgL_oz00_7517)
	{
		{	/* Liveness/types.scm 167 */
			{
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_13426;

				{
					obj_t BgL_auxz00_13427;

					{	/* Liveness/types.scm 167 */
						BgL_objectz00_bglt BgL_tmpz00_13428;

						BgL_tmpz00_13428 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_7517));
						BgL_auxz00_13427 = BGL_OBJECT_WIDENING(BgL_tmpz00_13428);
					}
					BgL_auxz00_13426 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_13427);
				}
				return
					(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13426))->
					BgL_defz00);
			}
		}

	}



/* &lambda3277 */
	BgL_failz00_bglt BGl_z62lambda3277z62zzliveness_typesz00(obj_t
		BgL_envz00_7518, obj_t BgL_o1510z00_7519)
	{
		{	/* Liveness/types.scm 161 */
			{	/* Liveness/types.scm 161 */
				long BgL_arg3281z00_8979;

				{	/* Liveness/types.scm 161 */
					obj_t BgL_arg3282z00_8980;

					{	/* Liveness/types.scm 161 */
						obj_t BgL_arg3287z00_8981;

						{	/* Liveness/types.scm 161 */
							obj_t BgL_arg1815z00_8982;
							long BgL_arg1816z00_8983;

							BgL_arg1815z00_8982 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 161 */
								long BgL_arg1817z00_8984;

								BgL_arg1817z00_8984 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_failz00_bglt) BgL_o1510z00_7519)));
								BgL_arg1816z00_8983 = (BgL_arg1817z00_8984 - OBJECT_TYPE);
							}
							BgL_arg3287z00_8981 =
								VECTOR_REF(BgL_arg1815z00_8982, BgL_arg1816z00_8983);
						}
						BgL_arg3282z00_8980 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3287z00_8981);
					}
					{	/* Liveness/types.scm 161 */
						obj_t BgL_tmpz00_13441;

						BgL_tmpz00_13441 = ((obj_t) BgL_arg3282z00_8980);
						BgL_arg3281z00_8979 = BGL_CLASS_NUM(BgL_tmpz00_13441);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_failz00_bglt) BgL_o1510z00_7519)), BgL_arg3281z00_8979);
			}
			{	/* Liveness/types.scm 161 */
				BgL_objectz00_bglt BgL_tmpz00_13447;

				BgL_tmpz00_13447 =
					((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_o1510z00_7519));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13447, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_o1510z00_7519));
			return ((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_o1510z00_7519));
		}

	}



/* &<@anonymous:3276> */
	obj_t BGl_z62zc3z04anonymousza33276ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7520, obj_t BgL_new1509z00_7521)
	{
		{	/* Liveness/types.scm 161 */
			{
				BgL_failz00_bglt BgL_auxz00_13455;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_failz00_bglt) BgL_new1509z00_7521))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13459;

					{	/* Liveness/types.scm 161 */
						obj_t BgL_classz00_8986;

						BgL_classz00_8986 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 161 */
							obj_t BgL__ortest_1117z00_8987;

							BgL__ortest_1117z00_8987 = BGL_CLASS_NIL(BgL_classz00_8986);
							if (CBOOL(BgL__ortest_1117z00_8987))
								{	/* Liveness/types.scm 161 */
									BgL_auxz00_13459 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_8987);
								}
							else
								{	/* Liveness/types.scm 161 */
									BgL_auxz00_13459 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8986));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_failz00_bglt) BgL_new1509z00_7521))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_13459), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_13469;

					{	/* Liveness/types.scm 161 */
						obj_t BgL_classz00_8988;

						BgL_classz00_8988 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 161 */
							obj_t BgL__ortest_1117z00_8989;

							BgL__ortest_1117z00_8989 = BGL_CLASS_NIL(BgL_classz00_8988);
							if (CBOOL(BgL__ortest_1117z00_8989))
								{	/* Liveness/types.scm 161 */
									BgL_auxz00_13469 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8989);
								}
							else
								{	/* Liveness/types.scm 161 */
									BgL_auxz00_13469 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8988));
								}
						}
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt)
										((BgL_failz00_bglt) BgL_new1509z00_7521))))->BgL_procz00) =
						((BgL_nodez00_bglt) BgL_auxz00_13469), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_13479;

					{	/* Liveness/types.scm 161 */
						obj_t BgL_classz00_8990;

						BgL_classz00_8990 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 161 */
							obj_t BgL__ortest_1117z00_8991;

							BgL__ortest_1117z00_8991 = BGL_CLASS_NIL(BgL_classz00_8990);
							if (CBOOL(BgL__ortest_1117z00_8991))
								{	/* Liveness/types.scm 161 */
									BgL_auxz00_13479 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8991);
								}
							else
								{	/* Liveness/types.scm 161 */
									BgL_auxz00_13479 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8990));
								}
						}
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt)
										((BgL_failz00_bglt) BgL_new1509z00_7521))))->BgL_msgz00) =
						((BgL_nodez00_bglt) BgL_auxz00_13479), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_13489;

					{	/* Liveness/types.scm 161 */
						obj_t BgL_classz00_8992;

						BgL_classz00_8992 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 161 */
							obj_t BgL__ortest_1117z00_8993;

							BgL__ortest_1117z00_8993 = BGL_CLASS_NIL(BgL_classz00_8992);
							if (CBOOL(BgL__ortest_1117z00_8993))
								{	/* Liveness/types.scm 161 */
									BgL_auxz00_13489 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_8993);
								}
							else
								{	/* Liveness/types.scm 161 */
									BgL_auxz00_13489 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_8992));
								}
						}
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt)
										((BgL_failz00_bglt) BgL_new1509z00_7521))))->BgL_objz00) =
						((BgL_nodez00_bglt) BgL_auxz00_13489), BUNSPEC);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_13499;

					{
						obj_t BgL_auxz00_13500;

						{	/* Liveness/types.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_13501;

							BgL_tmpz00_13501 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_new1509z00_7521));
							BgL_auxz00_13500 = BGL_OBJECT_WIDENING(BgL_tmpz00_13501);
						}
						BgL_auxz00_13499 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13500);
					}
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13499))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_13507;

					{
						obj_t BgL_auxz00_13508;

						{	/* Liveness/types.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_13509;

							BgL_tmpz00_13509 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_new1509z00_7521));
							BgL_auxz00_13508 = BGL_OBJECT_WIDENING(BgL_tmpz00_13509);
						}
						BgL_auxz00_13507 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13508);
					}
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13507))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_13515;

					{
						obj_t BgL_auxz00_13516;

						{	/* Liveness/types.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_13517;

							BgL_tmpz00_13517 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_new1509z00_7521));
							BgL_auxz00_13516 = BGL_OBJECT_WIDENING(BgL_tmpz00_13517);
						}
						BgL_auxz00_13515 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13516);
					}
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13515))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_13523;

					{
						obj_t BgL_auxz00_13524;

						{	/* Liveness/types.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_13525;

							BgL_tmpz00_13525 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_new1509z00_7521));
							BgL_auxz00_13524 = BGL_OBJECT_WIDENING(BgL_tmpz00_13525);
						}
						BgL_auxz00_13523 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13524);
					}
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13523))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_13455 = ((BgL_failz00_bglt) BgL_new1509z00_7521);
				return ((obj_t) BgL_auxz00_13455);
			}
		}

	}



/* &lambda3273 */
	BgL_failz00_bglt BGl_z62lambda3273z62zzliveness_typesz00(obj_t
		BgL_envz00_7522, obj_t BgL_o1506z00_7523)
	{
		{	/* Liveness/types.scm 161 */
			{	/* Liveness/types.scm 161 */
				BgL_failzf2livenesszf2_bglt BgL_wide1508z00_8995;

				BgL_wide1508z00_8995 =
					((BgL_failzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_failzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 161 */
					obj_t BgL_auxz00_13538;
					BgL_objectz00_bglt BgL_tmpz00_13534;

					BgL_auxz00_13538 = ((obj_t) BgL_wide1508z00_8995);
					BgL_tmpz00_13534 =
						((BgL_objectz00_bglt)
						((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_o1506z00_7523)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13534, BgL_auxz00_13538);
				}
				((BgL_objectz00_bglt)
					((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_o1506z00_7523)));
				{	/* Liveness/types.scm 161 */
					long BgL_arg3275z00_8996;

					BgL_arg3275z00_8996 =
						BGL_CLASS_NUM(BGl_failzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_failz00_bglt)
								((BgL_failz00_bglt) BgL_o1506z00_7523))), BgL_arg3275z00_8996);
				}
				return
					((BgL_failz00_bglt)
					((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_o1506z00_7523)));
			}
		}

	}



/* &lambda3270 */
	BgL_failz00_bglt BGl_z62lambda3270z62zzliveness_typesz00(obj_t
		BgL_envz00_7524, obj_t BgL_loc1497z00_7525, obj_t BgL_type1498z00_7526,
		obj_t BgL_proc1499z00_7527, obj_t BgL_msg1500z00_7528,
		obj_t BgL_obj1501z00_7529, obj_t BgL_def1502z00_7530,
		obj_t BgL_use1503z00_7531, obj_t BgL_in1504z00_7532,
		obj_t BgL_out1505z00_7533)
	{
		{	/* Liveness/types.scm 161 */
			{	/* Liveness/types.scm 161 */
				BgL_failz00_bglt BgL_new1800z00_9005;

				{	/* Liveness/types.scm 161 */
					BgL_failz00_bglt BgL_tmp1798z00_9006;
					BgL_failzf2livenesszf2_bglt BgL_wide1799z00_9007;

					{
						BgL_failz00_bglt BgL_auxz00_13552;

						{	/* Liveness/types.scm 161 */
							BgL_failz00_bglt BgL_new1797z00_9008;

							BgL_new1797z00_9008 =
								((BgL_failz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_failz00_bgl))));
							{	/* Liveness/types.scm 161 */
								long BgL_arg3272z00_9009;

								BgL_arg3272z00_9009 = BGL_CLASS_NUM(BGl_failz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1797z00_9008),
									BgL_arg3272z00_9009);
							}
							{	/* Liveness/types.scm 161 */
								BgL_objectz00_bglt BgL_tmpz00_13557;

								BgL_tmpz00_13557 = ((BgL_objectz00_bglt) BgL_new1797z00_9008);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13557, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1797z00_9008);
							BgL_auxz00_13552 = BgL_new1797z00_9008;
						}
						BgL_tmp1798z00_9006 = ((BgL_failz00_bglt) BgL_auxz00_13552);
					}
					BgL_wide1799z00_9007 =
						((BgL_failzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_failzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 161 */
						obj_t BgL_auxz00_13565;
						BgL_objectz00_bglt BgL_tmpz00_13563;

						BgL_auxz00_13565 = ((obj_t) BgL_wide1799z00_9007);
						BgL_tmpz00_13563 = ((BgL_objectz00_bglt) BgL_tmp1798z00_9006);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13563, BgL_auxz00_13565);
					}
					((BgL_objectz00_bglt) BgL_tmp1798z00_9006);
					{	/* Liveness/types.scm 161 */
						long BgL_arg3271z00_9010;

						BgL_arg3271z00_9010 =
							BGL_CLASS_NUM(BGl_failzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1798z00_9006), BgL_arg3271z00_9010);
					}
					BgL_new1800z00_9005 = ((BgL_failz00_bglt) BgL_tmp1798z00_9006);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1800z00_9005)))->BgL_locz00) =
					((obj_t) BgL_loc1497z00_7525), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1800z00_9005)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1498z00_7526)),
					BUNSPEC);
				((((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt)
									BgL_new1800z00_9005)))->BgL_procz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_proc1499z00_7527)),
					BUNSPEC);
				((((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt)
									BgL_new1800z00_9005)))->BgL_msgz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_msg1500z00_7528)),
					BUNSPEC);
				((((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt)
									BgL_new1800z00_9005)))->BgL_objz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_obj1501z00_7529)),
					BUNSPEC);
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_13587;

					{
						obj_t BgL_auxz00_13588;

						{	/* Liveness/types.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_13589;

							BgL_tmpz00_13589 = ((BgL_objectz00_bglt) BgL_new1800z00_9005);
							BgL_auxz00_13588 = BGL_OBJECT_WIDENING(BgL_tmpz00_13589);
						}
						BgL_auxz00_13587 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13588);
					}
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13587))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1502z00_7530)), BUNSPEC);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_13595;

					{
						obj_t BgL_auxz00_13596;

						{	/* Liveness/types.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_13597;

							BgL_tmpz00_13597 = ((BgL_objectz00_bglt) BgL_new1800z00_9005);
							BgL_auxz00_13596 = BGL_OBJECT_WIDENING(BgL_tmpz00_13597);
						}
						BgL_auxz00_13595 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13596);
					}
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13595))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1503z00_7531)), BUNSPEC);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_13603;

					{
						obj_t BgL_auxz00_13604;

						{	/* Liveness/types.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_13605;

							BgL_tmpz00_13605 = ((BgL_objectz00_bglt) BgL_new1800z00_9005);
							BgL_auxz00_13604 = BGL_OBJECT_WIDENING(BgL_tmpz00_13605);
						}
						BgL_auxz00_13603 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13604);
					}
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13603))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1504z00_7532)), BUNSPEC);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_13611;

					{
						obj_t BgL_auxz00_13612;

						{	/* Liveness/types.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_13613;

							BgL_tmpz00_13613 = ((BgL_objectz00_bglt) BgL_new1800z00_9005);
							BgL_auxz00_13612 = BGL_OBJECT_WIDENING(BgL_tmpz00_13613);
						}
						BgL_auxz00_13611 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13612);
					}
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13611))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1505z00_7533)), BUNSPEC);
				}
				return BgL_new1800z00_9005;
			}
		}

	}



/* &<@anonymous:3319> */
	obj_t BGl_z62zc3z04anonymousza33319ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7534)
	{
		{	/* Liveness/types.scm 161 */
			return BNIL;
		}

	}



/* &lambda3318 */
	obj_t BGl_z62lambda3318z62zzliveness_typesz00(obj_t BgL_envz00_7535,
		obj_t BgL_oz00_7536, obj_t BgL_vz00_7537)
	{
		{	/* Liveness/types.scm 161 */
			{
				BgL_failzf2livenesszf2_bglt BgL_auxz00_13619;

				{
					obj_t BgL_auxz00_13620;

					{	/* Liveness/types.scm 161 */
						BgL_objectz00_bglt BgL_tmpz00_13621;

						BgL_tmpz00_13621 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_oz00_7536));
						BgL_auxz00_13620 = BGL_OBJECT_WIDENING(BgL_tmpz00_13621);
					}
					BgL_auxz00_13619 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13620);
				}
				return
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13619))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7537)), BUNSPEC);
			}
		}

	}



/* &lambda3317 */
	obj_t BGl_z62lambda3317z62zzliveness_typesz00(obj_t BgL_envz00_7538,
		obj_t BgL_oz00_7539)
	{
		{	/* Liveness/types.scm 161 */
			{
				BgL_failzf2livenesszf2_bglt BgL_auxz00_13628;

				{
					obj_t BgL_auxz00_13629;

					{	/* Liveness/types.scm 161 */
						BgL_objectz00_bglt BgL_tmpz00_13630;

						BgL_tmpz00_13630 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_oz00_7539));
						BgL_auxz00_13629 = BGL_OBJECT_WIDENING(BgL_tmpz00_13630);
					}
					BgL_auxz00_13628 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13629);
				}
				return
					(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13628))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3312> */
	obj_t BGl_z62zc3z04anonymousza33312ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7540)
	{
		{	/* Liveness/types.scm 161 */
			return BNIL;
		}

	}



/* &lambda3311 */
	obj_t BGl_z62lambda3311z62zzliveness_typesz00(obj_t BgL_envz00_7541,
		obj_t BgL_oz00_7542, obj_t BgL_vz00_7543)
	{
		{	/* Liveness/types.scm 161 */
			{
				BgL_failzf2livenesszf2_bglt BgL_auxz00_13636;

				{
					obj_t BgL_auxz00_13637;

					{	/* Liveness/types.scm 161 */
						BgL_objectz00_bglt BgL_tmpz00_13638;

						BgL_tmpz00_13638 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_oz00_7542));
						BgL_auxz00_13637 = BGL_OBJECT_WIDENING(BgL_tmpz00_13638);
					}
					BgL_auxz00_13636 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13637);
				}
				return
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13636))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7543)), BUNSPEC);
			}
		}

	}



/* &lambda3310 */
	obj_t BGl_z62lambda3310z62zzliveness_typesz00(obj_t BgL_envz00_7544,
		obj_t BgL_oz00_7545)
	{
		{	/* Liveness/types.scm 161 */
			{
				BgL_failzf2livenesszf2_bglt BgL_auxz00_13645;

				{
					obj_t BgL_auxz00_13646;

					{	/* Liveness/types.scm 161 */
						BgL_objectz00_bglt BgL_tmpz00_13647;

						BgL_tmpz00_13647 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_oz00_7545));
						BgL_auxz00_13646 = BGL_OBJECT_WIDENING(BgL_tmpz00_13647);
					}
					BgL_auxz00_13645 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13646);
				}
				return
					(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13645))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3305> */
	obj_t BGl_z62zc3z04anonymousza33305ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7546)
	{
		{	/* Liveness/types.scm 161 */
			return BNIL;
		}

	}



/* &lambda3304 */
	obj_t BGl_z62lambda3304z62zzliveness_typesz00(obj_t BgL_envz00_7547,
		obj_t BgL_oz00_7548, obj_t BgL_vz00_7549)
	{
		{	/* Liveness/types.scm 161 */
			{
				BgL_failzf2livenesszf2_bglt BgL_auxz00_13653;

				{
					obj_t BgL_auxz00_13654;

					{	/* Liveness/types.scm 161 */
						BgL_objectz00_bglt BgL_tmpz00_13655;

						BgL_tmpz00_13655 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_oz00_7548));
						BgL_auxz00_13654 = BGL_OBJECT_WIDENING(BgL_tmpz00_13655);
					}
					BgL_auxz00_13653 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13654);
				}
				return
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13653))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7549)), BUNSPEC);
			}
		}

	}



/* &lambda3303 */
	obj_t BGl_z62lambda3303z62zzliveness_typesz00(obj_t BgL_envz00_7550,
		obj_t BgL_oz00_7551)
	{
		{	/* Liveness/types.scm 161 */
			{
				BgL_failzf2livenesszf2_bglt BgL_auxz00_13662;

				{
					obj_t BgL_auxz00_13663;

					{	/* Liveness/types.scm 161 */
						BgL_objectz00_bglt BgL_tmpz00_13664;

						BgL_tmpz00_13664 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_oz00_7551));
						BgL_auxz00_13663 = BGL_OBJECT_WIDENING(BgL_tmpz00_13664);
					}
					BgL_auxz00_13662 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13663);
				}
				return
					(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13662))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3295> */
	obj_t BGl_z62zc3z04anonymousza33295ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7552)
	{
		{	/* Liveness/types.scm 161 */
			return BNIL;
		}

	}



/* &lambda3294 */
	obj_t BGl_z62lambda3294z62zzliveness_typesz00(obj_t BgL_envz00_7553,
		obj_t BgL_oz00_7554, obj_t BgL_vz00_7555)
	{
		{	/* Liveness/types.scm 161 */
			{
				BgL_failzf2livenesszf2_bglt BgL_auxz00_13670;

				{
					obj_t BgL_auxz00_13671;

					{	/* Liveness/types.scm 161 */
						BgL_objectz00_bglt BgL_tmpz00_13672;

						BgL_tmpz00_13672 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_oz00_7554));
						BgL_auxz00_13671 = BGL_OBJECT_WIDENING(BgL_tmpz00_13672);
					}
					BgL_auxz00_13670 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13671);
				}
				return
					((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13670))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7555)), BUNSPEC);
			}
		}

	}



/* &lambda3293 */
	obj_t BGl_z62lambda3293z62zzliveness_typesz00(obj_t BgL_envz00_7556,
		obj_t BgL_oz00_7557)
	{
		{	/* Liveness/types.scm 161 */
			{
				BgL_failzf2livenesszf2_bglt BgL_auxz00_13679;

				{
					obj_t BgL_auxz00_13680;

					{	/* Liveness/types.scm 161 */
						BgL_objectz00_bglt BgL_tmpz00_13681;

						BgL_tmpz00_13681 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_oz00_7557));
						BgL_auxz00_13680 = BGL_OBJECT_WIDENING(BgL_tmpz00_13681);
					}
					BgL_auxz00_13679 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_13680);
				}
				return
					(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13679))->
					BgL_defz00);
			}
		}

	}



/* &lambda3222 */
	BgL_conditionalz00_bglt BGl_z62lambda3222z62zzliveness_typesz00(obj_t
		BgL_envz00_7558, obj_t BgL_o1495z00_7559)
	{
		{	/* Liveness/types.scm 155 */
			{	/* Liveness/types.scm 155 */
				long BgL_arg3223z00_9024;

				{	/* Liveness/types.scm 155 */
					obj_t BgL_arg3226z00_9025;

					{	/* Liveness/types.scm 155 */
						obj_t BgL_arg3229z00_9026;

						{	/* Liveness/types.scm 155 */
							obj_t BgL_arg1815z00_9027;
							long BgL_arg1816z00_9028;

							BgL_arg1815z00_9027 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 155 */
								long BgL_arg1817z00_9029;

								BgL_arg1817z00_9029 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_conditionalz00_bglt) BgL_o1495z00_7559)));
								BgL_arg1816z00_9028 = (BgL_arg1817z00_9029 - OBJECT_TYPE);
							}
							BgL_arg3229z00_9026 =
								VECTOR_REF(BgL_arg1815z00_9027, BgL_arg1816z00_9028);
						}
						BgL_arg3226z00_9025 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3229z00_9026);
					}
					{	/* Liveness/types.scm 155 */
						obj_t BgL_tmpz00_13694;

						BgL_tmpz00_13694 = ((obj_t) BgL_arg3226z00_9025);
						BgL_arg3223z00_9024 = BGL_CLASS_NUM(BgL_tmpz00_13694);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_conditionalz00_bglt) BgL_o1495z00_7559)),
					BgL_arg3223z00_9024);
			}
			{	/* Liveness/types.scm 155 */
				BgL_objectz00_bglt BgL_tmpz00_13700;

				BgL_tmpz00_13700 =
					((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_o1495z00_7559));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13700, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_o1495z00_7559));
			return
				((BgL_conditionalz00_bglt)
				((BgL_conditionalz00_bglt) BgL_o1495z00_7559));
		}

	}



/* &<@anonymous:3221> */
	obj_t BGl_z62zc3z04anonymousza33221ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7560, obj_t BgL_new1494z00_7561)
	{
		{	/* Liveness/types.scm 155 */
			{
				BgL_conditionalz00_bglt BgL_auxz00_13708;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_conditionalz00_bglt) BgL_new1494z00_7561))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13712;

					{	/* Liveness/types.scm 155 */
						obj_t BgL_classz00_9031;

						BgL_classz00_9031 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 155 */
							obj_t BgL__ortest_1117z00_9032;

							BgL__ortest_1117z00_9032 = BGL_CLASS_NIL(BgL_classz00_9031);
							if (CBOOL(BgL__ortest_1117z00_9032))
								{	/* Liveness/types.scm 155 */
									BgL_auxz00_13712 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9032);
								}
							else
								{	/* Liveness/types.scm 155 */
									BgL_auxz00_13712 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9031));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_conditionalz00_bglt) BgL_new1494z00_7561))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_13712), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_conditionalz00_bglt) BgL_new1494z00_7561))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_conditionalz00_bglt)
										BgL_new1494z00_7561))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_13728;

					{	/* Liveness/types.scm 155 */
						obj_t BgL_classz00_9033;

						BgL_classz00_9033 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 155 */
							obj_t BgL__ortest_1117z00_9034;

							BgL__ortest_1117z00_9034 = BGL_CLASS_NIL(BgL_classz00_9033);
							if (CBOOL(BgL__ortest_1117z00_9034))
								{	/* Liveness/types.scm 155 */
									BgL_auxz00_13728 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_9034);
								}
							else
								{	/* Liveness/types.scm 155 */
									BgL_auxz00_13728 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9033));
								}
						}
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt)
										((BgL_conditionalz00_bglt) BgL_new1494z00_7561))))->
							BgL_testz00) = ((BgL_nodez00_bglt) BgL_auxz00_13728), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_13738;

					{	/* Liveness/types.scm 155 */
						obj_t BgL_classz00_9035;

						BgL_classz00_9035 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 155 */
							obj_t BgL__ortest_1117z00_9036;

							BgL__ortest_1117z00_9036 = BGL_CLASS_NIL(BgL_classz00_9035);
							if (CBOOL(BgL__ortest_1117z00_9036))
								{	/* Liveness/types.scm 155 */
									BgL_auxz00_13738 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_9036);
								}
							else
								{	/* Liveness/types.scm 155 */
									BgL_auxz00_13738 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9035));
								}
						}
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt)
										((BgL_conditionalz00_bglt) BgL_new1494z00_7561))))->
							BgL_truez00) = ((BgL_nodez00_bglt) BgL_auxz00_13738), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_13748;

					{	/* Liveness/types.scm 155 */
						obj_t BgL_classz00_9037;

						BgL_classz00_9037 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 155 */
							obj_t BgL__ortest_1117z00_9038;

							BgL__ortest_1117z00_9038 = BGL_CLASS_NIL(BgL_classz00_9037);
							if (CBOOL(BgL__ortest_1117z00_9038))
								{	/* Liveness/types.scm 155 */
									BgL_auxz00_13748 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_9038);
								}
							else
								{	/* Liveness/types.scm 155 */
									BgL_auxz00_13748 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9037));
								}
						}
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt)
										((BgL_conditionalz00_bglt) BgL_new1494z00_7561))))->
							BgL_falsez00) = ((BgL_nodez00_bglt) BgL_auxz00_13748), BUNSPEC);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13758;

					{
						obj_t BgL_auxz00_13759;

						{	/* Liveness/types.scm 155 */
							BgL_objectz00_bglt BgL_tmpz00_13760;

							BgL_tmpz00_13760 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_new1494z00_7561));
							BgL_auxz00_13759 = BGL_OBJECT_WIDENING(BgL_tmpz00_13760);
						}
						BgL_auxz00_13758 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13759);
					}
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13758))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13766;

					{
						obj_t BgL_auxz00_13767;

						{	/* Liveness/types.scm 155 */
							BgL_objectz00_bglt BgL_tmpz00_13768;

							BgL_tmpz00_13768 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_new1494z00_7561));
							BgL_auxz00_13767 = BGL_OBJECT_WIDENING(BgL_tmpz00_13768);
						}
						BgL_auxz00_13766 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13767);
					}
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13766))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13774;

					{
						obj_t BgL_auxz00_13775;

						{	/* Liveness/types.scm 155 */
							BgL_objectz00_bglt BgL_tmpz00_13776;

							BgL_tmpz00_13776 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_new1494z00_7561));
							BgL_auxz00_13775 = BGL_OBJECT_WIDENING(BgL_tmpz00_13776);
						}
						BgL_auxz00_13774 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13775);
					}
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13774))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13782;

					{
						obj_t BgL_auxz00_13783;

						{	/* Liveness/types.scm 155 */
							BgL_objectz00_bglt BgL_tmpz00_13784;

							BgL_tmpz00_13784 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_new1494z00_7561));
							BgL_auxz00_13783 = BGL_OBJECT_WIDENING(BgL_tmpz00_13784);
						}
						BgL_auxz00_13782 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13783);
					}
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13782))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_13708 = ((BgL_conditionalz00_bglt) BgL_new1494z00_7561);
				return ((obj_t) BgL_auxz00_13708);
			}
		}

	}



/* &lambda3219 */
	BgL_conditionalz00_bglt BGl_z62lambda3219z62zzliveness_typesz00(obj_t
		BgL_envz00_7562, obj_t BgL_o1491z00_7563)
	{
		{	/* Liveness/types.scm 155 */
			{	/* Liveness/types.scm 155 */
				BgL_conditionalzf2livenesszf2_bglt BgL_wide1493z00_9040;

				BgL_wide1493z00_9040 =
					((BgL_conditionalzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_conditionalzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 155 */
					obj_t BgL_auxz00_13797;
					BgL_objectz00_bglt BgL_tmpz00_13793;

					BgL_auxz00_13797 = ((obj_t) BgL_wide1493z00_9040);
					BgL_tmpz00_13793 =
						((BgL_objectz00_bglt)
						((BgL_conditionalz00_bglt)
							((BgL_conditionalz00_bglt) BgL_o1491z00_7563)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13793, BgL_auxz00_13797);
				}
				((BgL_objectz00_bglt)
					((BgL_conditionalz00_bglt)
						((BgL_conditionalz00_bglt) BgL_o1491z00_7563)));
				{	/* Liveness/types.scm 155 */
					long BgL_arg3220z00_9041;

					BgL_arg3220z00_9041 =
						BGL_CLASS_NUM(BGl_conditionalzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_conditionalz00_bglt)
								((BgL_conditionalz00_bglt) BgL_o1491z00_7563))),
						BgL_arg3220z00_9041);
				}
				return
					((BgL_conditionalz00_bglt)
					((BgL_conditionalz00_bglt)
						((BgL_conditionalz00_bglt) BgL_o1491z00_7563)));
			}
		}

	}



/* &lambda3213 */
	BgL_conditionalz00_bglt BGl_z62lambda3213z62zzliveness_typesz00(obj_t
		BgL_envz00_7564, obj_t BgL_loc1480z00_7565, obj_t BgL_type1481z00_7566,
		obj_t BgL_sidezd2effect1482zd2_7567, obj_t BgL_key1483z00_7568,
		obj_t BgL_test1484z00_7569, obj_t BgL_true1485z00_7570,
		obj_t BgL_false1486z00_7571, obj_t BgL_def1487z00_7572,
		obj_t BgL_use1488z00_7573, obj_t BgL_in1489z00_7574,
		obj_t BgL_out1490z00_7575)
	{
		{	/* Liveness/types.scm 155 */
			{	/* Liveness/types.scm 155 */
				BgL_conditionalz00_bglt BgL_new1795z00_9050;

				{	/* Liveness/types.scm 155 */
					BgL_conditionalz00_bglt BgL_tmp1793z00_9051;
					BgL_conditionalzf2livenesszf2_bglt BgL_wide1794z00_9052;

					{
						BgL_conditionalz00_bglt BgL_auxz00_13811;

						{	/* Liveness/types.scm 155 */
							BgL_conditionalz00_bglt BgL_new1792z00_9053;

							BgL_new1792z00_9053 =
								((BgL_conditionalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_conditionalz00_bgl))));
							{	/* Liveness/types.scm 155 */
								long BgL_arg3218z00_9054;

								BgL_arg3218z00_9054 =
									BGL_CLASS_NUM(BGl_conditionalz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1792z00_9053),
									BgL_arg3218z00_9054);
							}
							{	/* Liveness/types.scm 155 */
								BgL_objectz00_bglt BgL_tmpz00_13816;

								BgL_tmpz00_13816 = ((BgL_objectz00_bglt) BgL_new1792z00_9053);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13816, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1792z00_9053);
							BgL_auxz00_13811 = BgL_new1792z00_9053;
						}
						BgL_tmp1793z00_9051 = ((BgL_conditionalz00_bglt) BgL_auxz00_13811);
					}
					BgL_wide1794z00_9052 =
						((BgL_conditionalzf2livenesszf2_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_conditionalzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 155 */
						obj_t BgL_auxz00_13824;
						BgL_objectz00_bglt BgL_tmpz00_13822;

						BgL_auxz00_13824 = ((obj_t) BgL_wide1794z00_9052);
						BgL_tmpz00_13822 = ((BgL_objectz00_bglt) BgL_tmp1793z00_9051);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13822, BgL_auxz00_13824);
					}
					((BgL_objectz00_bglt) BgL_tmp1793z00_9051);
					{	/* Liveness/types.scm 155 */
						long BgL_arg3217z00_9055;

						BgL_arg3217z00_9055 =
							BGL_CLASS_NUM(BGl_conditionalzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1793z00_9051), BgL_arg3217z00_9055);
					}
					BgL_new1795z00_9050 = ((BgL_conditionalz00_bglt) BgL_tmp1793z00_9051);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1795z00_9050)))->BgL_locz00) =
					((obj_t) BgL_loc1480z00_7565), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1795z00_9050)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1481z00_7566)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1795z00_9050)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1482zd2_7567), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1795z00_9050)))->BgL_keyz00) =
					((obj_t) BgL_key1483z00_7568), BUNSPEC);
				((((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
									BgL_new1795z00_9050)))->BgL_testz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_test1484z00_7569)),
					BUNSPEC);
				((((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
									BgL_new1795z00_9050)))->BgL_truez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_true1485z00_7570)),
					BUNSPEC);
				((((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
									BgL_new1795z00_9050)))->BgL_falsez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_false1486z00_7571)),
					BUNSPEC);
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13850;

					{
						obj_t BgL_auxz00_13851;

						{	/* Liveness/types.scm 155 */
							BgL_objectz00_bglt BgL_tmpz00_13852;

							BgL_tmpz00_13852 = ((BgL_objectz00_bglt) BgL_new1795z00_9050);
							BgL_auxz00_13851 = BGL_OBJECT_WIDENING(BgL_tmpz00_13852);
						}
						BgL_auxz00_13850 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13851);
					}
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13850))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1487z00_7572)), BUNSPEC);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13858;

					{
						obj_t BgL_auxz00_13859;

						{	/* Liveness/types.scm 155 */
							BgL_objectz00_bglt BgL_tmpz00_13860;

							BgL_tmpz00_13860 = ((BgL_objectz00_bglt) BgL_new1795z00_9050);
							BgL_auxz00_13859 = BGL_OBJECT_WIDENING(BgL_tmpz00_13860);
						}
						BgL_auxz00_13858 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13859);
					}
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13858))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1488z00_7573)), BUNSPEC);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13866;

					{
						obj_t BgL_auxz00_13867;

						{	/* Liveness/types.scm 155 */
							BgL_objectz00_bglt BgL_tmpz00_13868;

							BgL_tmpz00_13868 = ((BgL_objectz00_bglt) BgL_new1795z00_9050);
							BgL_auxz00_13867 = BGL_OBJECT_WIDENING(BgL_tmpz00_13868);
						}
						BgL_auxz00_13866 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13867);
					}
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13866))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1489z00_7574)), BUNSPEC);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13874;

					{
						obj_t BgL_auxz00_13875;

						{	/* Liveness/types.scm 155 */
							BgL_objectz00_bglt BgL_tmpz00_13876;

							BgL_tmpz00_13876 = ((BgL_objectz00_bglt) BgL_new1795z00_9050);
							BgL_auxz00_13875 = BGL_OBJECT_WIDENING(BgL_tmpz00_13876);
						}
						BgL_auxz00_13874 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13875);
					}
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13874))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1490z00_7575)), BUNSPEC);
				}
				return BgL_new1795z00_9050;
			}
		}

	}



/* &<@anonymous:3262> */
	obj_t BGl_z62zc3z04anonymousza33262ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7576)
	{
		{	/* Liveness/types.scm 155 */
			return BNIL;
		}

	}



/* &lambda3261 */
	obj_t BGl_z62lambda3261z62zzliveness_typesz00(obj_t BgL_envz00_7577,
		obj_t BgL_oz00_7578, obj_t BgL_vz00_7579)
	{
		{	/* Liveness/types.scm 155 */
			{
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13882;

				{
					obj_t BgL_auxz00_13883;

					{	/* Liveness/types.scm 155 */
						BgL_objectz00_bglt BgL_tmpz00_13884;

						BgL_tmpz00_13884 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_oz00_7578));
						BgL_auxz00_13883 = BGL_OBJECT_WIDENING(BgL_tmpz00_13884);
					}
					BgL_auxz00_13882 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13883);
				}
				return
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13882))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7579)), BUNSPEC);
			}
		}

	}



/* &lambda3260 */
	obj_t BGl_z62lambda3260z62zzliveness_typesz00(obj_t BgL_envz00_7580,
		obj_t BgL_oz00_7581)
	{
		{	/* Liveness/types.scm 155 */
			{
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13891;

				{
					obj_t BgL_auxz00_13892;

					{	/* Liveness/types.scm 155 */
						BgL_objectz00_bglt BgL_tmpz00_13893;

						BgL_tmpz00_13893 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_oz00_7581));
						BgL_auxz00_13892 = BGL_OBJECT_WIDENING(BgL_tmpz00_13893);
					}
					BgL_auxz00_13891 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13892);
				}
				return
					(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13891))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3253> */
	obj_t BGl_z62zc3z04anonymousza33253ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7582)
	{
		{	/* Liveness/types.scm 155 */
			return BNIL;
		}

	}



/* &lambda3252 */
	obj_t BGl_z62lambda3252z62zzliveness_typesz00(obj_t BgL_envz00_7583,
		obj_t BgL_oz00_7584, obj_t BgL_vz00_7585)
	{
		{	/* Liveness/types.scm 155 */
			{
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13899;

				{
					obj_t BgL_auxz00_13900;

					{	/* Liveness/types.scm 155 */
						BgL_objectz00_bglt BgL_tmpz00_13901;

						BgL_tmpz00_13901 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_oz00_7584));
						BgL_auxz00_13900 = BGL_OBJECT_WIDENING(BgL_tmpz00_13901);
					}
					BgL_auxz00_13899 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13900);
				}
				return
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13899))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7585)), BUNSPEC);
			}
		}

	}



/* &lambda3251 */
	obj_t BGl_z62lambda3251z62zzliveness_typesz00(obj_t BgL_envz00_7586,
		obj_t BgL_oz00_7587)
	{
		{	/* Liveness/types.scm 155 */
			{
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13908;

				{
					obj_t BgL_auxz00_13909;

					{	/* Liveness/types.scm 155 */
						BgL_objectz00_bglt BgL_tmpz00_13910;

						BgL_tmpz00_13910 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_oz00_7587));
						BgL_auxz00_13909 = BGL_OBJECT_WIDENING(BgL_tmpz00_13910);
					}
					BgL_auxz00_13908 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13909);
				}
				return
					(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13908))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3245> */
	obj_t BGl_z62zc3z04anonymousza33245ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7588)
	{
		{	/* Liveness/types.scm 155 */
			return BNIL;
		}

	}



/* &lambda3244 */
	obj_t BGl_z62lambda3244z62zzliveness_typesz00(obj_t BgL_envz00_7589,
		obj_t BgL_oz00_7590, obj_t BgL_vz00_7591)
	{
		{	/* Liveness/types.scm 155 */
			{
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13916;

				{
					obj_t BgL_auxz00_13917;

					{	/* Liveness/types.scm 155 */
						BgL_objectz00_bglt BgL_tmpz00_13918;

						BgL_tmpz00_13918 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_oz00_7590));
						BgL_auxz00_13917 = BGL_OBJECT_WIDENING(BgL_tmpz00_13918);
					}
					BgL_auxz00_13916 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13917);
				}
				return
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13916))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7591)), BUNSPEC);
			}
		}

	}



/* &lambda3243 */
	obj_t BGl_z62lambda3243z62zzliveness_typesz00(obj_t BgL_envz00_7592,
		obj_t BgL_oz00_7593)
	{
		{	/* Liveness/types.scm 155 */
			{
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13925;

				{
					obj_t BgL_auxz00_13926;

					{	/* Liveness/types.scm 155 */
						BgL_objectz00_bglt BgL_tmpz00_13927;

						BgL_tmpz00_13927 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_oz00_7593));
						BgL_auxz00_13926 = BGL_OBJECT_WIDENING(BgL_tmpz00_13927);
					}
					BgL_auxz00_13925 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13926);
				}
				return
					(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13925))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3237> */
	obj_t BGl_z62zc3z04anonymousza33237ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7594)
	{
		{	/* Liveness/types.scm 155 */
			return BNIL;
		}

	}



/* &lambda3236 */
	obj_t BGl_z62lambda3236z62zzliveness_typesz00(obj_t BgL_envz00_7595,
		obj_t BgL_oz00_7596, obj_t BgL_vz00_7597)
	{
		{	/* Liveness/types.scm 155 */
			{
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13933;

				{
					obj_t BgL_auxz00_13934;

					{	/* Liveness/types.scm 155 */
						BgL_objectz00_bglt BgL_tmpz00_13935;

						BgL_tmpz00_13935 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_oz00_7596));
						BgL_auxz00_13934 = BGL_OBJECT_WIDENING(BgL_tmpz00_13935);
					}
					BgL_auxz00_13933 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13934);
				}
				return
					((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13933))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7597)), BUNSPEC);
			}
		}

	}



/* &lambda3235 */
	obj_t BGl_z62lambda3235z62zzliveness_typesz00(obj_t BgL_envz00_7598,
		obj_t BgL_oz00_7599)
	{
		{	/* Liveness/types.scm 155 */
			{
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_13942;

				{
					obj_t BgL_auxz00_13943;

					{	/* Liveness/types.scm 155 */
						BgL_objectz00_bglt BgL_tmpz00_13944;

						BgL_tmpz00_13944 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_oz00_7599));
						BgL_auxz00_13943 = BGL_OBJECT_WIDENING(BgL_tmpz00_13944);
					}
					BgL_auxz00_13942 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_13943);
				}
				return
					(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_13942))->
					BgL_defz00);
			}
		}

	}



/* &lambda3158 */
	BgL_setqz00_bglt BGl_z62lambda3158z62zzliveness_typesz00(obj_t
		BgL_envz00_7600, obj_t BgL_o1477z00_7601)
	{
		{	/* Liveness/types.scm 149 */
			{	/* Liveness/types.scm 149 */
				long BgL_arg3160z00_9069;

				{	/* Liveness/types.scm 149 */
					obj_t BgL_arg3161z00_9070;

					{	/* Liveness/types.scm 149 */
						obj_t BgL_arg3162z00_9071;

						{	/* Liveness/types.scm 149 */
							obj_t BgL_arg1815z00_9072;
							long BgL_arg1816z00_9073;

							BgL_arg1815z00_9072 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 149 */
								long BgL_arg1817z00_9074;

								BgL_arg1817z00_9074 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_setqz00_bglt) BgL_o1477z00_7601)));
								BgL_arg1816z00_9073 = (BgL_arg1817z00_9074 - OBJECT_TYPE);
							}
							BgL_arg3162z00_9071 =
								VECTOR_REF(BgL_arg1815z00_9072, BgL_arg1816z00_9073);
						}
						BgL_arg3161z00_9070 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3162z00_9071);
					}
					{	/* Liveness/types.scm 149 */
						obj_t BgL_tmpz00_13957;

						BgL_tmpz00_13957 = ((obj_t) BgL_arg3161z00_9070);
						BgL_arg3160z00_9069 = BGL_CLASS_NUM(BgL_tmpz00_13957);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_setqz00_bglt) BgL_o1477z00_7601)), BgL_arg3160z00_9069);
			}
			{	/* Liveness/types.scm 149 */
				BgL_objectz00_bglt BgL_tmpz00_13963;

				BgL_tmpz00_13963 =
					((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_o1477z00_7601));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13963, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_o1477z00_7601));
			return ((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_o1477z00_7601));
		}

	}



/* &<@anonymous:3157> */
	obj_t BGl_z62zc3z04anonymousza33157ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7602, obj_t BgL_new1476z00_7603)
	{
		{	/* Liveness/types.scm 149 */
			{
				BgL_setqz00_bglt BgL_auxz00_13971;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_setqz00_bglt) BgL_new1476z00_7603))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13975;

					{	/* Liveness/types.scm 149 */
						obj_t BgL_classz00_9076;

						BgL_classz00_9076 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 149 */
							obj_t BgL__ortest_1117z00_9077;

							BgL__ortest_1117z00_9077 = BGL_CLASS_NIL(BgL_classz00_9076);
							if (CBOOL(BgL__ortest_1117z00_9077))
								{	/* Liveness/types.scm 149 */
									BgL_auxz00_13975 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9077);
								}
							else
								{	/* Liveness/types.scm 149 */
									BgL_auxz00_13975 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9076));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_setqz00_bglt) BgL_new1476z00_7603))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_13975), BUNSPEC);
				}
				{
					BgL_varz00_bglt BgL_auxz00_13985;

					{	/* Liveness/types.scm 149 */
						obj_t BgL_classz00_9078;

						BgL_classz00_9078 = BGl_varz00zzast_nodez00;
						{	/* Liveness/types.scm 149 */
							obj_t BgL__ortest_1117z00_9079;

							BgL__ortest_1117z00_9079 = BGL_CLASS_NIL(BgL_classz00_9078);
							if (CBOOL(BgL__ortest_1117z00_9079))
								{	/* Liveness/types.scm 149 */
									BgL_auxz00_13985 =
										((BgL_varz00_bglt) BgL__ortest_1117z00_9079);
								}
							else
								{	/* Liveness/types.scm 149 */
									BgL_auxz00_13985 =
										((BgL_varz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9078));
								}
						}
					}
					((((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt)
										((BgL_setqz00_bglt) BgL_new1476z00_7603))))->BgL_varz00) =
						((BgL_varz00_bglt) BgL_auxz00_13985), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_13995;

					{	/* Liveness/types.scm 149 */
						obj_t BgL_classz00_9080;

						BgL_classz00_9080 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 149 */
							obj_t BgL__ortest_1117z00_9081;

							BgL__ortest_1117z00_9081 = BGL_CLASS_NIL(BgL_classz00_9080);
							if (CBOOL(BgL__ortest_1117z00_9081))
								{	/* Liveness/types.scm 149 */
									BgL_auxz00_13995 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_9081);
								}
							else
								{	/* Liveness/types.scm 149 */
									BgL_auxz00_13995 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9080));
								}
						}
					}
					((((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt)
										((BgL_setqz00_bglt) BgL_new1476z00_7603))))->BgL_valuez00) =
						((BgL_nodez00_bglt) BgL_auxz00_13995), BUNSPEC);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_14005;

					{
						obj_t BgL_auxz00_14006;

						{	/* Liveness/types.scm 149 */
							BgL_objectz00_bglt BgL_tmpz00_14007;

							BgL_tmpz00_14007 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_new1476z00_7603));
							BgL_auxz00_14006 = BGL_OBJECT_WIDENING(BgL_tmpz00_14007);
						}
						BgL_auxz00_14005 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14006);
					}
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14005))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_14013;

					{
						obj_t BgL_auxz00_14014;

						{	/* Liveness/types.scm 149 */
							BgL_objectz00_bglt BgL_tmpz00_14015;

							BgL_tmpz00_14015 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_new1476z00_7603));
							BgL_auxz00_14014 = BGL_OBJECT_WIDENING(BgL_tmpz00_14015);
						}
						BgL_auxz00_14013 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14014);
					}
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14013))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_14021;

					{
						obj_t BgL_auxz00_14022;

						{	/* Liveness/types.scm 149 */
							BgL_objectz00_bglt BgL_tmpz00_14023;

							BgL_tmpz00_14023 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_new1476z00_7603));
							BgL_auxz00_14022 = BGL_OBJECT_WIDENING(BgL_tmpz00_14023);
						}
						BgL_auxz00_14021 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14022);
					}
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14021))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_14029;

					{
						obj_t BgL_auxz00_14030;

						{	/* Liveness/types.scm 149 */
							BgL_objectz00_bglt BgL_tmpz00_14031;

							BgL_tmpz00_14031 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_new1476z00_7603));
							BgL_auxz00_14030 = BGL_OBJECT_WIDENING(BgL_tmpz00_14031);
						}
						BgL_auxz00_14029 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14030);
					}
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14029))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_13971 = ((BgL_setqz00_bglt) BgL_new1476z00_7603);
				return ((obj_t) BgL_auxz00_13971);
			}
		}

	}



/* &lambda3155 */
	BgL_setqz00_bglt BGl_z62lambda3155z62zzliveness_typesz00(obj_t
		BgL_envz00_7604, obj_t BgL_o1473z00_7605)
	{
		{	/* Liveness/types.scm 149 */
			{	/* Liveness/types.scm 149 */
				BgL_setqzf2livenesszf2_bglt BgL_wide1475z00_9083;

				BgL_wide1475z00_9083 =
					((BgL_setqzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_setqzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 149 */
					obj_t BgL_auxz00_14044;
					BgL_objectz00_bglt BgL_tmpz00_14040;

					BgL_auxz00_14044 = ((obj_t) BgL_wide1475z00_9083);
					BgL_tmpz00_14040 =
						((BgL_objectz00_bglt)
						((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_o1473z00_7605)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14040, BgL_auxz00_14044);
				}
				((BgL_objectz00_bglt)
					((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_o1473z00_7605)));
				{	/* Liveness/types.scm 149 */
					long BgL_arg3156z00_9084;

					BgL_arg3156z00_9084 =
						BGL_CLASS_NUM(BGl_setqzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_setqz00_bglt)
								((BgL_setqz00_bglt) BgL_o1473z00_7605))), BgL_arg3156z00_9084);
				}
				return
					((BgL_setqz00_bglt)
					((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_o1473z00_7605)));
			}
		}

	}



/* &lambda3147 */
	BgL_setqz00_bglt BGl_z62lambda3147z62zzliveness_typesz00(obj_t
		BgL_envz00_7606, obj_t BgL_loc1465z00_7607, obj_t BgL_type1466z00_7608,
		obj_t BgL_var1467z00_7609, obj_t BgL_value1468z00_7610,
		obj_t BgL_def1469z00_7611, obj_t BgL_use1470z00_7612,
		obj_t BgL_in1471z00_7613, obj_t BgL_out1472z00_7614)
	{
		{	/* Liveness/types.scm 149 */
			{	/* Liveness/types.scm 149 */
				BgL_setqz00_bglt BgL_new1790z00_9092;

				{	/* Liveness/types.scm 149 */
					BgL_setqz00_bglt BgL_tmp1788z00_9093;
					BgL_setqzf2livenesszf2_bglt BgL_wide1789z00_9094;

					{
						BgL_setqz00_bglt BgL_auxz00_14058;

						{	/* Liveness/types.scm 149 */
							BgL_setqz00_bglt BgL_new1787z00_9095;

							BgL_new1787z00_9095 =
								((BgL_setqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_setqz00_bgl))));
							{	/* Liveness/types.scm 149 */
								long BgL_arg3154z00_9096;

								BgL_arg3154z00_9096 = BGL_CLASS_NUM(BGl_setqz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1787z00_9095),
									BgL_arg3154z00_9096);
							}
							{	/* Liveness/types.scm 149 */
								BgL_objectz00_bglt BgL_tmpz00_14063;

								BgL_tmpz00_14063 = ((BgL_objectz00_bglt) BgL_new1787z00_9095);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14063, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1787z00_9095);
							BgL_auxz00_14058 = BgL_new1787z00_9095;
						}
						BgL_tmp1788z00_9093 = ((BgL_setqz00_bglt) BgL_auxz00_14058);
					}
					BgL_wide1789z00_9094 =
						((BgL_setqzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_setqzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 149 */
						obj_t BgL_auxz00_14071;
						BgL_objectz00_bglt BgL_tmpz00_14069;

						BgL_auxz00_14071 = ((obj_t) BgL_wide1789z00_9094);
						BgL_tmpz00_14069 = ((BgL_objectz00_bglt) BgL_tmp1788z00_9093);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14069, BgL_auxz00_14071);
					}
					((BgL_objectz00_bglt) BgL_tmp1788z00_9093);
					{	/* Liveness/types.scm 149 */
						long BgL_arg3149z00_9097;

						BgL_arg3149z00_9097 =
							BGL_CLASS_NUM(BGl_setqzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1788z00_9093), BgL_arg3149z00_9097);
					}
					BgL_new1790z00_9092 = ((BgL_setqz00_bglt) BgL_tmp1788z00_9093);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1790z00_9092)))->BgL_locz00) =
					((obj_t) BgL_loc1465z00_7607), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1790z00_9092)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1466z00_7608)),
					BUNSPEC);
				((((BgL_setqz00_bglt) COBJECT(((BgL_setqz00_bglt)
									BgL_new1790z00_9092)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_var1467z00_7609)), BUNSPEC);
				((((BgL_setqz00_bglt) COBJECT(((BgL_setqz00_bglt)
									BgL_new1790z00_9092)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_value1468z00_7610)),
					BUNSPEC);
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_14090;

					{
						obj_t BgL_auxz00_14091;

						{	/* Liveness/types.scm 149 */
							BgL_objectz00_bglt BgL_tmpz00_14092;

							BgL_tmpz00_14092 = ((BgL_objectz00_bglt) BgL_new1790z00_9092);
							BgL_auxz00_14091 = BGL_OBJECT_WIDENING(BgL_tmpz00_14092);
						}
						BgL_auxz00_14090 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14091);
					}
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14090))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1469z00_7611)), BUNSPEC);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_14098;

					{
						obj_t BgL_auxz00_14099;

						{	/* Liveness/types.scm 149 */
							BgL_objectz00_bglt BgL_tmpz00_14100;

							BgL_tmpz00_14100 = ((BgL_objectz00_bglt) BgL_new1790z00_9092);
							BgL_auxz00_14099 = BGL_OBJECT_WIDENING(BgL_tmpz00_14100);
						}
						BgL_auxz00_14098 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14099);
					}
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14098))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1470z00_7612)), BUNSPEC);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_14106;

					{
						obj_t BgL_auxz00_14107;

						{	/* Liveness/types.scm 149 */
							BgL_objectz00_bglt BgL_tmpz00_14108;

							BgL_tmpz00_14108 = ((BgL_objectz00_bglt) BgL_new1790z00_9092);
							BgL_auxz00_14107 = BGL_OBJECT_WIDENING(BgL_tmpz00_14108);
						}
						BgL_auxz00_14106 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14107);
					}
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14106))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1471z00_7613)), BUNSPEC);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_14114;

					{
						obj_t BgL_auxz00_14115;

						{	/* Liveness/types.scm 149 */
							BgL_objectz00_bglt BgL_tmpz00_14116;

							BgL_tmpz00_14116 = ((BgL_objectz00_bglt) BgL_new1790z00_9092);
							BgL_auxz00_14115 = BGL_OBJECT_WIDENING(BgL_tmpz00_14116);
						}
						BgL_auxz00_14114 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14115);
					}
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14114))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1472z00_7614)), BUNSPEC);
				}
				return BgL_new1790z00_9092;
			}
		}

	}



/* &<@anonymous:3204> */
	obj_t BGl_z62zc3z04anonymousza33204ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7615)
	{
		{	/* Liveness/types.scm 149 */
			return BNIL;
		}

	}



/* &lambda3203 */
	obj_t BGl_z62lambda3203z62zzliveness_typesz00(obj_t BgL_envz00_7616,
		obj_t BgL_oz00_7617, obj_t BgL_vz00_7618)
	{
		{	/* Liveness/types.scm 149 */
			{
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_14122;

				{
					obj_t BgL_auxz00_14123;

					{	/* Liveness/types.scm 149 */
						BgL_objectz00_bglt BgL_tmpz00_14124;

						BgL_tmpz00_14124 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_oz00_7617));
						BgL_auxz00_14123 = BGL_OBJECT_WIDENING(BgL_tmpz00_14124);
					}
					BgL_auxz00_14122 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14123);
				}
				return
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14122))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7618)), BUNSPEC);
			}
		}

	}



/* &lambda3202 */
	obj_t BGl_z62lambda3202z62zzliveness_typesz00(obj_t BgL_envz00_7619,
		obj_t BgL_oz00_7620)
	{
		{	/* Liveness/types.scm 149 */
			{
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_14131;

				{
					obj_t BgL_auxz00_14132;

					{	/* Liveness/types.scm 149 */
						BgL_objectz00_bglt BgL_tmpz00_14133;

						BgL_tmpz00_14133 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_oz00_7620));
						BgL_auxz00_14132 = BGL_OBJECT_WIDENING(BgL_tmpz00_14133);
					}
					BgL_auxz00_14131 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14132);
				}
				return
					(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14131))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3193> */
	obj_t BGl_z62zc3z04anonymousza33193ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7621)
	{
		{	/* Liveness/types.scm 149 */
			return BNIL;
		}

	}



/* &lambda3192 */
	obj_t BGl_z62lambda3192z62zzliveness_typesz00(obj_t BgL_envz00_7622,
		obj_t BgL_oz00_7623, obj_t BgL_vz00_7624)
	{
		{	/* Liveness/types.scm 149 */
			{
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_14139;

				{
					obj_t BgL_auxz00_14140;

					{	/* Liveness/types.scm 149 */
						BgL_objectz00_bglt BgL_tmpz00_14141;

						BgL_tmpz00_14141 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_oz00_7623));
						BgL_auxz00_14140 = BGL_OBJECT_WIDENING(BgL_tmpz00_14141);
					}
					BgL_auxz00_14139 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14140);
				}
				return
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14139))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7624)), BUNSPEC);
			}
		}

	}



/* &lambda3191 */
	obj_t BGl_z62lambda3191z62zzliveness_typesz00(obj_t BgL_envz00_7625,
		obj_t BgL_oz00_7626)
	{
		{	/* Liveness/types.scm 149 */
			{
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_14148;

				{
					obj_t BgL_auxz00_14149;

					{	/* Liveness/types.scm 149 */
						BgL_objectz00_bglt BgL_tmpz00_14150;

						BgL_tmpz00_14150 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_oz00_7626));
						BgL_auxz00_14149 = BGL_OBJECT_WIDENING(BgL_tmpz00_14150);
					}
					BgL_auxz00_14148 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14149);
				}
				return
					(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14148))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3185> */
	obj_t BGl_z62zc3z04anonymousza33185ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7627)
	{
		{	/* Liveness/types.scm 149 */
			return BNIL;
		}

	}



/* &lambda3184 */
	obj_t BGl_z62lambda3184z62zzliveness_typesz00(obj_t BgL_envz00_7628,
		obj_t BgL_oz00_7629, obj_t BgL_vz00_7630)
	{
		{	/* Liveness/types.scm 149 */
			{
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_14156;

				{
					obj_t BgL_auxz00_14157;

					{	/* Liveness/types.scm 149 */
						BgL_objectz00_bglt BgL_tmpz00_14158;

						BgL_tmpz00_14158 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_oz00_7629));
						BgL_auxz00_14157 = BGL_OBJECT_WIDENING(BgL_tmpz00_14158);
					}
					BgL_auxz00_14156 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14157);
				}
				return
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14156))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7630)), BUNSPEC);
			}
		}

	}



/* &lambda3183 */
	obj_t BGl_z62lambda3183z62zzliveness_typesz00(obj_t BgL_envz00_7631,
		obj_t BgL_oz00_7632)
	{
		{	/* Liveness/types.scm 149 */
			{
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_14165;

				{
					obj_t BgL_auxz00_14166;

					{	/* Liveness/types.scm 149 */
						BgL_objectz00_bglt BgL_tmpz00_14167;

						BgL_tmpz00_14167 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_oz00_7632));
						BgL_auxz00_14166 = BGL_OBJECT_WIDENING(BgL_tmpz00_14167);
					}
					BgL_auxz00_14165 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14166);
				}
				return
					(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14165))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3177> */
	obj_t BGl_z62zc3z04anonymousza33177ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7633)
	{
		{	/* Liveness/types.scm 149 */
			return BNIL;
		}

	}



/* &lambda3176 */
	obj_t BGl_z62lambda3176z62zzliveness_typesz00(obj_t BgL_envz00_7634,
		obj_t BgL_oz00_7635, obj_t BgL_vz00_7636)
	{
		{	/* Liveness/types.scm 149 */
			{
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_14173;

				{
					obj_t BgL_auxz00_14174;

					{	/* Liveness/types.scm 149 */
						BgL_objectz00_bglt BgL_tmpz00_14175;

						BgL_tmpz00_14175 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_oz00_7635));
						BgL_auxz00_14174 = BGL_OBJECT_WIDENING(BgL_tmpz00_14175);
					}
					BgL_auxz00_14173 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14174);
				}
				return
					((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14173))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7636)), BUNSPEC);
			}
		}

	}



/* &lambda3175 */
	obj_t BGl_z62lambda3175z62zzliveness_typesz00(obj_t BgL_envz00_7637,
		obj_t BgL_oz00_7638)
	{
		{	/* Liveness/types.scm 149 */
			{
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_14182;

				{
					obj_t BgL_auxz00_14183;

					{	/* Liveness/types.scm 149 */
						BgL_objectz00_bglt BgL_tmpz00_14184;

						BgL_tmpz00_14184 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_oz00_7638));
						BgL_auxz00_14183 = BGL_OBJECT_WIDENING(BgL_tmpz00_14184);
					}
					BgL_auxz00_14182 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_14183);
				}
				return
					(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14182))->
					BgL_defz00);
			}
		}

	}



/* &lambda3089 */
	BgL_castz00_bglt BGl_z62lambda3089z62zzliveness_typesz00(obj_t
		BgL_envz00_7639, obj_t BgL_o1462z00_7640)
	{
		{	/* Liveness/types.scm 143 */
			{	/* Liveness/types.scm 143 */
				long BgL_arg3091z00_9111;

				{	/* Liveness/types.scm 143 */
					obj_t BgL_arg3092z00_9112;

					{	/* Liveness/types.scm 143 */
						obj_t BgL_arg3093z00_9113;

						{	/* Liveness/types.scm 143 */
							obj_t BgL_arg1815z00_9114;
							long BgL_arg1816z00_9115;

							BgL_arg1815z00_9114 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 143 */
								long BgL_arg1817z00_9116;

								BgL_arg1817z00_9116 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_castz00_bglt) BgL_o1462z00_7640)));
								BgL_arg1816z00_9115 = (BgL_arg1817z00_9116 - OBJECT_TYPE);
							}
							BgL_arg3093z00_9113 =
								VECTOR_REF(BgL_arg1815z00_9114, BgL_arg1816z00_9115);
						}
						BgL_arg3092z00_9112 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3093z00_9113);
					}
					{	/* Liveness/types.scm 143 */
						obj_t BgL_tmpz00_14197;

						BgL_tmpz00_14197 = ((obj_t) BgL_arg3092z00_9112);
						BgL_arg3091z00_9111 = BGL_CLASS_NUM(BgL_tmpz00_14197);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_castz00_bglt) BgL_o1462z00_7640)), BgL_arg3091z00_9111);
			}
			{	/* Liveness/types.scm 143 */
				BgL_objectz00_bglt BgL_tmpz00_14203;

				BgL_tmpz00_14203 =
					((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_o1462z00_7640));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14203, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_o1462z00_7640));
			return ((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_o1462z00_7640));
		}

	}



/* &<@anonymous:3088> */
	obj_t BGl_z62zc3z04anonymousza33088ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7641, obj_t BgL_new1461z00_7642)
	{
		{	/* Liveness/types.scm 143 */
			{
				BgL_castz00_bglt BgL_auxz00_14211;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_castz00_bglt) BgL_new1461z00_7642))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14215;

					{	/* Liveness/types.scm 143 */
						obj_t BgL_classz00_9118;

						BgL_classz00_9118 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 143 */
							obj_t BgL__ortest_1117z00_9119;

							BgL__ortest_1117z00_9119 = BGL_CLASS_NIL(BgL_classz00_9118);
							if (CBOOL(BgL__ortest_1117z00_9119))
								{	/* Liveness/types.scm 143 */
									BgL_auxz00_14215 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9119);
								}
							else
								{	/* Liveness/types.scm 143 */
									BgL_auxz00_14215 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9118));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_castz00_bglt) BgL_new1461z00_7642))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_14215), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_14225;

					{	/* Liveness/types.scm 143 */
						obj_t BgL_classz00_9120;

						BgL_classz00_9120 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 143 */
							obj_t BgL__ortest_1117z00_9121;

							BgL__ortest_1117z00_9121 = BGL_CLASS_NIL(BgL_classz00_9120);
							if (CBOOL(BgL__ortest_1117z00_9121))
								{	/* Liveness/types.scm 143 */
									BgL_auxz00_14225 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_9121);
								}
							else
								{	/* Liveness/types.scm 143 */
									BgL_auxz00_14225 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9120));
								}
						}
					}
					((((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt)
										((BgL_castz00_bglt) BgL_new1461z00_7642))))->BgL_argz00) =
						((BgL_nodez00_bglt) BgL_auxz00_14225), BUNSPEC);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_14235;

					{
						obj_t BgL_auxz00_14236;

						{	/* Liveness/types.scm 143 */
							BgL_objectz00_bglt BgL_tmpz00_14237;

							BgL_tmpz00_14237 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_new1461z00_7642));
							BgL_auxz00_14236 = BGL_OBJECT_WIDENING(BgL_tmpz00_14237);
						}
						BgL_auxz00_14235 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14236);
					}
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14235))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_14243;

					{
						obj_t BgL_auxz00_14244;

						{	/* Liveness/types.scm 143 */
							BgL_objectz00_bglt BgL_tmpz00_14245;

							BgL_tmpz00_14245 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_new1461z00_7642));
							BgL_auxz00_14244 = BGL_OBJECT_WIDENING(BgL_tmpz00_14245);
						}
						BgL_auxz00_14243 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14244);
					}
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14243))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_14251;

					{
						obj_t BgL_auxz00_14252;

						{	/* Liveness/types.scm 143 */
							BgL_objectz00_bglt BgL_tmpz00_14253;

							BgL_tmpz00_14253 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_new1461z00_7642));
							BgL_auxz00_14252 = BGL_OBJECT_WIDENING(BgL_tmpz00_14253);
						}
						BgL_auxz00_14251 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14252);
					}
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14251))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_14259;

					{
						obj_t BgL_auxz00_14260;

						{	/* Liveness/types.scm 143 */
							BgL_objectz00_bglt BgL_tmpz00_14261;

							BgL_tmpz00_14261 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_new1461z00_7642));
							BgL_auxz00_14260 = BGL_OBJECT_WIDENING(BgL_tmpz00_14261);
						}
						BgL_auxz00_14259 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14260);
					}
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14259))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_14211 = ((BgL_castz00_bglt) BgL_new1461z00_7642);
				return ((obj_t) BgL_auxz00_14211);
			}
		}

	}



/* &lambda3086 */
	BgL_castz00_bglt BGl_z62lambda3086z62zzliveness_typesz00(obj_t
		BgL_envz00_7643, obj_t BgL_o1458z00_7644)
	{
		{	/* Liveness/types.scm 143 */
			{	/* Liveness/types.scm 143 */
				BgL_castzf2livenesszf2_bglt BgL_wide1460z00_9123;

				BgL_wide1460z00_9123 =
					((BgL_castzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_castzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 143 */
					obj_t BgL_auxz00_14274;
					BgL_objectz00_bglt BgL_tmpz00_14270;

					BgL_auxz00_14274 = ((obj_t) BgL_wide1460z00_9123);
					BgL_tmpz00_14270 =
						((BgL_objectz00_bglt)
						((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_o1458z00_7644)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14270, BgL_auxz00_14274);
				}
				((BgL_objectz00_bglt)
					((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_o1458z00_7644)));
				{	/* Liveness/types.scm 143 */
					long BgL_arg3087z00_9124;

					BgL_arg3087z00_9124 =
						BGL_CLASS_NUM(BGl_castzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_castz00_bglt)
								((BgL_castz00_bglt) BgL_o1458z00_7644))), BgL_arg3087z00_9124);
				}
				return
					((BgL_castz00_bglt)
					((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_o1458z00_7644)));
			}
		}

	}



/* &lambda3083 */
	BgL_castz00_bglt BGl_z62lambda3083z62zzliveness_typesz00(obj_t
		BgL_envz00_7645, obj_t BgL_loc1451z00_7646, obj_t BgL_type1452z00_7647,
		obj_t BgL_arg1453z00_7648, obj_t BgL_def1454z00_7649,
		obj_t BgL_use1455z00_7650, obj_t BgL_in1456z00_7651,
		obj_t BgL_out1457z00_7652)
	{
		{	/* Liveness/types.scm 143 */
			{	/* Liveness/types.scm 143 */
				BgL_castz00_bglt BgL_new1785z00_9131;

				{	/* Liveness/types.scm 143 */
					BgL_castz00_bglt BgL_tmp1783z00_9132;
					BgL_castzf2livenesszf2_bglt BgL_wide1784z00_9133;

					{
						BgL_castz00_bglt BgL_auxz00_14288;

						{	/* Liveness/types.scm 143 */
							BgL_castz00_bglt BgL_new1782z00_9134;

							BgL_new1782z00_9134 =
								((BgL_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_castz00_bgl))));
							{	/* Liveness/types.scm 143 */
								long BgL_arg3085z00_9135;

								BgL_arg3085z00_9135 = BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1782z00_9134),
									BgL_arg3085z00_9135);
							}
							{	/* Liveness/types.scm 143 */
								BgL_objectz00_bglt BgL_tmpz00_14293;

								BgL_tmpz00_14293 = ((BgL_objectz00_bglt) BgL_new1782z00_9134);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14293, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1782z00_9134);
							BgL_auxz00_14288 = BgL_new1782z00_9134;
						}
						BgL_tmp1783z00_9132 = ((BgL_castz00_bglt) BgL_auxz00_14288);
					}
					BgL_wide1784z00_9133 =
						((BgL_castzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_castzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 143 */
						obj_t BgL_auxz00_14301;
						BgL_objectz00_bglt BgL_tmpz00_14299;

						BgL_auxz00_14301 = ((obj_t) BgL_wide1784z00_9133);
						BgL_tmpz00_14299 = ((BgL_objectz00_bglt) BgL_tmp1783z00_9132);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14299, BgL_auxz00_14301);
					}
					((BgL_objectz00_bglt) BgL_tmp1783z00_9132);
					{	/* Liveness/types.scm 143 */
						long BgL_arg3084z00_9136;

						BgL_arg3084z00_9136 =
							BGL_CLASS_NUM(BGl_castzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1783z00_9132), BgL_arg3084z00_9136);
					}
					BgL_new1785z00_9131 = ((BgL_castz00_bglt) BgL_tmp1783z00_9132);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1785z00_9131)))->BgL_locz00) =
					((obj_t) BgL_loc1451z00_7646), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1785z00_9131)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1452z00_7647)),
					BUNSPEC);
				((((BgL_castz00_bglt) COBJECT(((BgL_castz00_bglt)
									BgL_new1785z00_9131)))->BgL_argz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1453z00_7648)),
					BUNSPEC);
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_14317;

					{
						obj_t BgL_auxz00_14318;

						{	/* Liveness/types.scm 143 */
							BgL_objectz00_bglt BgL_tmpz00_14319;

							BgL_tmpz00_14319 = ((BgL_objectz00_bglt) BgL_new1785z00_9131);
							BgL_auxz00_14318 = BGL_OBJECT_WIDENING(BgL_tmpz00_14319);
						}
						BgL_auxz00_14317 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14318);
					}
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14317))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1454z00_7649)), BUNSPEC);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_14325;

					{
						obj_t BgL_auxz00_14326;

						{	/* Liveness/types.scm 143 */
							BgL_objectz00_bglt BgL_tmpz00_14327;

							BgL_tmpz00_14327 = ((BgL_objectz00_bglt) BgL_new1785z00_9131);
							BgL_auxz00_14326 = BGL_OBJECT_WIDENING(BgL_tmpz00_14327);
						}
						BgL_auxz00_14325 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14326);
					}
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14325))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1455z00_7650)), BUNSPEC);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_14333;

					{
						obj_t BgL_auxz00_14334;

						{	/* Liveness/types.scm 143 */
							BgL_objectz00_bglt BgL_tmpz00_14335;

							BgL_tmpz00_14335 = ((BgL_objectz00_bglt) BgL_new1785z00_9131);
							BgL_auxz00_14334 = BGL_OBJECT_WIDENING(BgL_tmpz00_14335);
						}
						BgL_auxz00_14333 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14334);
					}
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14333))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1456z00_7651)), BUNSPEC);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_14341;

					{
						obj_t BgL_auxz00_14342;

						{	/* Liveness/types.scm 143 */
							BgL_objectz00_bglt BgL_tmpz00_14343;

							BgL_tmpz00_14343 = ((BgL_objectz00_bglt) BgL_new1785z00_9131);
							BgL_auxz00_14342 = BGL_OBJECT_WIDENING(BgL_tmpz00_14343);
						}
						BgL_auxz00_14341 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14342);
					}
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14341))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1457z00_7652)), BUNSPEC);
				}
				return BgL_new1785z00_9131;
			}
		}

	}



/* &<@anonymous:3138> */
	obj_t BGl_z62zc3z04anonymousza33138ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7653)
	{
		{	/* Liveness/types.scm 143 */
			return BNIL;
		}

	}



/* &lambda3137 */
	obj_t BGl_z62lambda3137z62zzliveness_typesz00(obj_t BgL_envz00_7654,
		obj_t BgL_oz00_7655, obj_t BgL_vz00_7656)
	{
		{	/* Liveness/types.scm 143 */
			{
				BgL_castzf2livenesszf2_bglt BgL_auxz00_14349;

				{
					obj_t BgL_auxz00_14350;

					{	/* Liveness/types.scm 143 */
						BgL_objectz00_bglt BgL_tmpz00_14351;

						BgL_tmpz00_14351 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_oz00_7655));
						BgL_auxz00_14350 = BGL_OBJECT_WIDENING(BgL_tmpz00_14351);
					}
					BgL_auxz00_14349 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14350);
				}
				return
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14349))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7656)), BUNSPEC);
			}
		}

	}



/* &lambda3136 */
	obj_t BGl_z62lambda3136z62zzliveness_typesz00(obj_t BgL_envz00_7657,
		obj_t BgL_oz00_7658)
	{
		{	/* Liveness/types.scm 143 */
			{
				BgL_castzf2livenesszf2_bglt BgL_auxz00_14358;

				{
					obj_t BgL_auxz00_14359;

					{	/* Liveness/types.scm 143 */
						BgL_objectz00_bglt BgL_tmpz00_14360;

						BgL_tmpz00_14360 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_oz00_7658));
						BgL_auxz00_14359 = BGL_OBJECT_WIDENING(BgL_tmpz00_14360);
					}
					BgL_auxz00_14358 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14359);
				}
				return
					(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14358))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3129> */
	obj_t BGl_z62zc3z04anonymousza33129ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7659)
	{
		{	/* Liveness/types.scm 143 */
			return BNIL;
		}

	}



/* &lambda3128 */
	obj_t BGl_z62lambda3128z62zzliveness_typesz00(obj_t BgL_envz00_7660,
		obj_t BgL_oz00_7661, obj_t BgL_vz00_7662)
	{
		{	/* Liveness/types.scm 143 */
			{
				BgL_castzf2livenesszf2_bglt BgL_auxz00_14366;

				{
					obj_t BgL_auxz00_14367;

					{	/* Liveness/types.scm 143 */
						BgL_objectz00_bglt BgL_tmpz00_14368;

						BgL_tmpz00_14368 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_oz00_7661));
						BgL_auxz00_14367 = BGL_OBJECT_WIDENING(BgL_tmpz00_14368);
					}
					BgL_auxz00_14366 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14367);
				}
				return
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14366))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7662)), BUNSPEC);
			}
		}

	}



/* &lambda3127 */
	obj_t BGl_z62lambda3127z62zzliveness_typesz00(obj_t BgL_envz00_7663,
		obj_t BgL_oz00_7664)
	{
		{	/* Liveness/types.scm 143 */
			{
				BgL_castzf2livenesszf2_bglt BgL_auxz00_14375;

				{
					obj_t BgL_auxz00_14376;

					{	/* Liveness/types.scm 143 */
						BgL_objectz00_bglt BgL_tmpz00_14377;

						BgL_tmpz00_14377 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_oz00_7664));
						BgL_auxz00_14376 = BGL_OBJECT_WIDENING(BgL_tmpz00_14377);
					}
					BgL_auxz00_14375 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14376);
				}
				return
					(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14375))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3121> */
	obj_t BGl_z62zc3z04anonymousza33121ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7665)
	{
		{	/* Liveness/types.scm 143 */
			return BNIL;
		}

	}



/* &lambda3120 */
	obj_t BGl_z62lambda3120z62zzliveness_typesz00(obj_t BgL_envz00_7666,
		obj_t BgL_oz00_7667, obj_t BgL_vz00_7668)
	{
		{	/* Liveness/types.scm 143 */
			{
				BgL_castzf2livenesszf2_bglt BgL_auxz00_14383;

				{
					obj_t BgL_auxz00_14384;

					{	/* Liveness/types.scm 143 */
						BgL_objectz00_bglt BgL_tmpz00_14385;

						BgL_tmpz00_14385 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_oz00_7667));
						BgL_auxz00_14384 = BGL_OBJECT_WIDENING(BgL_tmpz00_14385);
					}
					BgL_auxz00_14383 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14384);
				}
				return
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14383))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7668)), BUNSPEC);
			}
		}

	}



/* &lambda3119 */
	obj_t BGl_z62lambda3119z62zzliveness_typesz00(obj_t BgL_envz00_7669,
		obj_t BgL_oz00_7670)
	{
		{	/* Liveness/types.scm 143 */
			{
				BgL_castzf2livenesszf2_bglt BgL_auxz00_14392;

				{
					obj_t BgL_auxz00_14393;

					{	/* Liveness/types.scm 143 */
						BgL_objectz00_bglt BgL_tmpz00_14394;

						BgL_tmpz00_14394 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_oz00_7670));
						BgL_auxz00_14393 = BGL_OBJECT_WIDENING(BgL_tmpz00_14394);
					}
					BgL_auxz00_14392 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14393);
				}
				return
					(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14392))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3108> */
	obj_t BGl_z62zc3z04anonymousza33108ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7671)
	{
		{	/* Liveness/types.scm 143 */
			return BNIL;
		}

	}



/* &lambda3107 */
	obj_t BGl_z62lambda3107z62zzliveness_typesz00(obj_t BgL_envz00_7672,
		obj_t BgL_oz00_7673, obj_t BgL_vz00_7674)
	{
		{	/* Liveness/types.scm 143 */
			{
				BgL_castzf2livenesszf2_bglt BgL_auxz00_14400;

				{
					obj_t BgL_auxz00_14401;

					{	/* Liveness/types.scm 143 */
						BgL_objectz00_bglt BgL_tmpz00_14402;

						BgL_tmpz00_14402 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_oz00_7673));
						BgL_auxz00_14401 = BGL_OBJECT_WIDENING(BgL_tmpz00_14402);
					}
					BgL_auxz00_14400 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14401);
				}
				return
					((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14400))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7674)), BUNSPEC);
			}
		}

	}



/* &lambda3106 */
	obj_t BGl_z62lambda3106z62zzliveness_typesz00(obj_t BgL_envz00_7675,
		obj_t BgL_oz00_7676)
	{
		{	/* Liveness/types.scm 143 */
			{
				BgL_castzf2livenesszf2_bglt BgL_auxz00_14409;

				{
					obj_t BgL_auxz00_14410;

					{	/* Liveness/types.scm 143 */
						BgL_objectz00_bglt BgL_tmpz00_14411;

						BgL_tmpz00_14411 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_oz00_7676));
						BgL_auxz00_14410 = BGL_OBJECT_WIDENING(BgL_tmpz00_14411);
					}
					BgL_auxz00_14409 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_14410);
				}
				return
					(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14409))->
					BgL_defz00);
			}
		}

	}



/* &lambda3028 */
	BgL_castzd2nullzd2_bglt BGl_z62lambda3028z62zzliveness_typesz00(obj_t
		BgL_envz00_7677, obj_t BgL_o1449z00_7678)
	{
		{	/* Liveness/types.scm 137 */
			{	/* Liveness/types.scm 137 */
				long BgL_arg3029z00_9150;

				{	/* Liveness/types.scm 137 */
					obj_t BgL_arg3030z00_9151;

					{	/* Liveness/types.scm 137 */
						obj_t BgL_arg3031z00_9152;

						{	/* Liveness/types.scm 137 */
							obj_t BgL_arg1815z00_9153;
							long BgL_arg1816z00_9154;

							BgL_arg1815z00_9153 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 137 */
								long BgL_arg1817z00_9155;

								BgL_arg1817z00_9155 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_castzd2nullzd2_bglt) BgL_o1449z00_7678)));
								BgL_arg1816z00_9154 = (BgL_arg1817z00_9155 - OBJECT_TYPE);
							}
							BgL_arg3031z00_9152 =
								VECTOR_REF(BgL_arg1815z00_9153, BgL_arg1816z00_9154);
						}
						BgL_arg3030z00_9151 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg3031z00_9152);
					}
					{	/* Liveness/types.scm 137 */
						obj_t BgL_tmpz00_14424;

						BgL_tmpz00_14424 = ((obj_t) BgL_arg3030z00_9151);
						BgL_arg3029z00_9150 = BGL_CLASS_NUM(BgL_tmpz00_14424);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_castzd2nullzd2_bglt) BgL_o1449z00_7678)),
					BgL_arg3029z00_9150);
			}
			{	/* Liveness/types.scm 137 */
				BgL_objectz00_bglt BgL_tmpz00_14430;

				BgL_tmpz00_14430 =
					((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_o1449z00_7678));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14430, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_o1449z00_7678));
			return
				((BgL_castzd2nullzd2_bglt)
				((BgL_castzd2nullzd2_bglt) BgL_o1449z00_7678));
		}

	}



/* &<@anonymous:3027> */
	obj_t BGl_z62zc3z04anonymousza33027ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7679, obj_t BgL_new1448z00_7680)
	{
		{	/* Liveness/types.scm 137 */
			{
				BgL_castzd2nullzd2_bglt BgL_auxz00_14438;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_new1448z00_7680))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14442;

					{	/* Liveness/types.scm 137 */
						obj_t BgL_classz00_9157;

						BgL_classz00_9157 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 137 */
							obj_t BgL__ortest_1117z00_9158;

							BgL__ortest_1117z00_9158 = BGL_CLASS_NIL(BgL_classz00_9157);
							if (CBOOL(BgL__ortest_1117z00_9158))
								{	/* Liveness/types.scm 137 */
									BgL_auxz00_14442 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9158);
								}
							else
								{	/* Liveness/types.scm 137 */
									BgL_auxz00_14442 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9157));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_castzd2nullzd2_bglt) BgL_new1448z00_7680))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14442), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_new1448z00_7680))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_castzd2nullzd2_bglt)
										BgL_new1448z00_7680))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_castzd2nullzd2_bglt)
										BgL_new1448z00_7680))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_castzd2nullzd2_bglt)
										BgL_new1448z00_7680))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_castzd2nullzd2_bglt)
										BgL_new1448z00_7680))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14467;

					{
						obj_t BgL_auxz00_14468;

						{	/* Liveness/types.scm 137 */
							BgL_objectz00_bglt BgL_tmpz00_14469;

							BgL_tmpz00_14469 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_new1448z00_7680));
							BgL_auxz00_14468 = BGL_OBJECT_WIDENING(BgL_tmpz00_14469);
						}
						BgL_auxz00_14467 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14468);
					}
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14467))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14475;

					{
						obj_t BgL_auxz00_14476;

						{	/* Liveness/types.scm 137 */
							BgL_objectz00_bglt BgL_tmpz00_14477;

							BgL_tmpz00_14477 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_new1448z00_7680));
							BgL_auxz00_14476 = BGL_OBJECT_WIDENING(BgL_tmpz00_14477);
						}
						BgL_auxz00_14475 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14476);
					}
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14475))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14483;

					{
						obj_t BgL_auxz00_14484;

						{	/* Liveness/types.scm 137 */
							BgL_objectz00_bglt BgL_tmpz00_14485;

							BgL_tmpz00_14485 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_new1448z00_7680));
							BgL_auxz00_14484 = BGL_OBJECT_WIDENING(BgL_tmpz00_14485);
						}
						BgL_auxz00_14483 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14484);
					}
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14483))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14491;

					{
						obj_t BgL_auxz00_14492;

						{	/* Liveness/types.scm 137 */
							BgL_objectz00_bglt BgL_tmpz00_14493;

							BgL_tmpz00_14493 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_new1448z00_7680));
							BgL_auxz00_14492 = BGL_OBJECT_WIDENING(BgL_tmpz00_14493);
						}
						BgL_auxz00_14491 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14492);
					}
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14491))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_14438 = ((BgL_castzd2nullzd2_bglt) BgL_new1448z00_7680);
				return ((obj_t) BgL_auxz00_14438);
			}
		}

	}



/* &lambda3025 */
	BgL_castzd2nullzd2_bglt BGl_z62lambda3025z62zzliveness_typesz00(obj_t
		BgL_envz00_7681, obj_t BgL_o1445z00_7682)
	{
		{	/* Liveness/types.scm 137 */
			{	/* Liveness/types.scm 137 */
				BgL_castzd2nullzf2livenessz20_bglt BgL_wide1447z00_9160;

				BgL_wide1447z00_9160 =
					((BgL_castzd2nullzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_castzd2nullzf2livenessz20_bgl))));
				{	/* Liveness/types.scm 137 */
					obj_t BgL_auxz00_14506;
					BgL_objectz00_bglt BgL_tmpz00_14502;

					BgL_auxz00_14506 = ((obj_t) BgL_wide1447z00_9160);
					BgL_tmpz00_14502 =
						((BgL_objectz00_bglt)
						((BgL_castzd2nullzd2_bglt)
							((BgL_castzd2nullzd2_bglt) BgL_o1445z00_7682)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14502, BgL_auxz00_14506);
				}
				((BgL_objectz00_bglt)
					((BgL_castzd2nullzd2_bglt)
						((BgL_castzd2nullzd2_bglt) BgL_o1445z00_7682)));
				{	/* Liveness/types.scm 137 */
					long BgL_arg3026z00_9161;

					BgL_arg3026z00_9161 =
						BGL_CLASS_NUM(BGl_castzd2nullzf2livenessz20zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_castzd2nullzd2_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_o1445z00_7682))),
						BgL_arg3026z00_9161);
				}
				return
					((BgL_castzd2nullzd2_bglt)
					((BgL_castzd2nullzd2_bglt)
						((BgL_castzd2nullzd2_bglt) BgL_o1445z00_7682)));
			}
		}

	}



/* &lambda3021 */
	BgL_castzd2nullzd2_bglt BGl_z62lambda3021z62zzliveness_typesz00(obj_t
		BgL_envz00_7683, obj_t BgL_loc1434z00_7684, obj_t BgL_type1435z00_7685,
		obj_t BgL_sidezd2effect1436zd2_7686, obj_t BgL_key1437z00_7687,
		obj_t BgL_exprza21438za2_7688, obj_t BgL_effect1439z00_7689,
		obj_t BgL_czd2format1440zd2_7690, obj_t BgL_def1441z00_7691,
		obj_t BgL_use1442z00_7692, obj_t BgL_in1443z00_7693,
		obj_t BgL_out1444z00_7694)
	{
		{	/* Liveness/types.scm 137 */
			{	/* Liveness/types.scm 137 */
				BgL_castzd2nullzd2_bglt BgL_new1780z00_9169;

				{	/* Liveness/types.scm 137 */
					BgL_castzd2nullzd2_bglt BgL_tmp1778z00_9170;
					BgL_castzd2nullzf2livenessz20_bglt BgL_wide1779z00_9171;

					{
						BgL_castzd2nullzd2_bglt BgL_auxz00_14520;

						{	/* Liveness/types.scm 137 */
							BgL_castzd2nullzd2_bglt BgL_new1777z00_9172;

							BgL_new1777z00_9172 =
								((BgL_castzd2nullzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_castzd2nullzd2_bgl))));
							{	/* Liveness/types.scm 137 */
								long BgL_arg3024z00_9173;

								BgL_arg3024z00_9173 =
									BGL_CLASS_NUM(BGl_castzd2nullzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1777z00_9172),
									BgL_arg3024z00_9173);
							}
							{	/* Liveness/types.scm 137 */
								BgL_objectz00_bglt BgL_tmpz00_14525;

								BgL_tmpz00_14525 = ((BgL_objectz00_bglt) BgL_new1777z00_9172);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14525, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1777z00_9172);
							BgL_auxz00_14520 = BgL_new1777z00_9172;
						}
						BgL_tmp1778z00_9170 = ((BgL_castzd2nullzd2_bglt) BgL_auxz00_14520);
					}
					BgL_wide1779z00_9171 =
						((BgL_castzd2nullzf2livenessz20_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_castzd2nullzf2livenessz20_bgl))));
					{	/* Liveness/types.scm 137 */
						obj_t BgL_auxz00_14533;
						BgL_objectz00_bglt BgL_tmpz00_14531;

						BgL_auxz00_14533 = ((obj_t) BgL_wide1779z00_9171);
						BgL_tmpz00_14531 = ((BgL_objectz00_bglt) BgL_tmp1778z00_9170);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14531, BgL_auxz00_14533);
					}
					((BgL_objectz00_bglt) BgL_tmp1778z00_9170);
					{	/* Liveness/types.scm 137 */
						long BgL_arg3023z00_9174;

						BgL_arg3023z00_9174 =
							BGL_CLASS_NUM(BGl_castzd2nullzf2livenessz20zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1778z00_9170), BgL_arg3023z00_9174);
					}
					BgL_new1780z00_9169 = ((BgL_castzd2nullzd2_bglt) BgL_tmp1778z00_9170);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1780z00_9169)))->BgL_locz00) =
					((obj_t) BgL_loc1434z00_7684), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1780z00_9169)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1435z00_7685)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1780z00_9169)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1436zd2_7686), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1780z00_9169)))->BgL_keyz00) =
					((obj_t) BgL_key1437z00_7687), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1780z00_9169)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21438za2_7688)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1780z00_9169)))->BgL_effectz00) =
					((obj_t) BgL_effect1439z00_7689), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1780z00_9169)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1440zd2_7690)), BUNSPEC);
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14558;

					{
						obj_t BgL_auxz00_14559;

						{	/* Liveness/types.scm 137 */
							BgL_objectz00_bglt BgL_tmpz00_14560;

							BgL_tmpz00_14560 = ((BgL_objectz00_bglt) BgL_new1780z00_9169);
							BgL_auxz00_14559 = BGL_OBJECT_WIDENING(BgL_tmpz00_14560);
						}
						BgL_auxz00_14558 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14559);
					}
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14558))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1441z00_7691)), BUNSPEC);
				}
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14566;

					{
						obj_t BgL_auxz00_14567;

						{	/* Liveness/types.scm 137 */
							BgL_objectz00_bglt BgL_tmpz00_14568;

							BgL_tmpz00_14568 = ((BgL_objectz00_bglt) BgL_new1780z00_9169);
							BgL_auxz00_14567 = BGL_OBJECT_WIDENING(BgL_tmpz00_14568);
						}
						BgL_auxz00_14566 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14567);
					}
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14566))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1442z00_7692)), BUNSPEC);
				}
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14574;

					{
						obj_t BgL_auxz00_14575;

						{	/* Liveness/types.scm 137 */
							BgL_objectz00_bglt BgL_tmpz00_14576;

							BgL_tmpz00_14576 = ((BgL_objectz00_bglt) BgL_new1780z00_9169);
							BgL_auxz00_14575 = BGL_OBJECT_WIDENING(BgL_tmpz00_14576);
						}
						BgL_auxz00_14574 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14575);
					}
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14574))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1443z00_7693)), BUNSPEC);
				}
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14582;

					{
						obj_t BgL_auxz00_14583;

						{	/* Liveness/types.scm 137 */
							BgL_objectz00_bglt BgL_tmpz00_14584;

							BgL_tmpz00_14584 = ((BgL_objectz00_bglt) BgL_new1780z00_9169);
							BgL_auxz00_14583 = BGL_OBJECT_WIDENING(BgL_tmpz00_14584);
						}
						BgL_auxz00_14582 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14583);
					}
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14582))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1444z00_7694)), BUNSPEC);
				}
				return BgL_new1780z00_9169;
			}
		}

	}



/* &<@anonymous:3076> */
	obj_t BGl_z62zc3z04anonymousza33076ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7695)
	{
		{	/* Liveness/types.scm 137 */
			return BNIL;
		}

	}



/* &lambda3075 */
	obj_t BGl_z62lambda3075z62zzliveness_typesz00(obj_t BgL_envz00_7696,
		obj_t BgL_oz00_7697, obj_t BgL_vz00_7698)
	{
		{	/* Liveness/types.scm 137 */
			{
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14590;

				{
					obj_t BgL_auxz00_14591;

					{	/* Liveness/types.scm 137 */
						BgL_objectz00_bglt BgL_tmpz00_14592;

						BgL_tmpz00_14592 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_7697));
						BgL_auxz00_14591 = BGL_OBJECT_WIDENING(BgL_tmpz00_14592);
					}
					BgL_auxz00_14590 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14591);
				}
				return
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14590))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7698)), BUNSPEC);
			}
		}

	}



/* &lambda3074 */
	obj_t BGl_z62lambda3074z62zzliveness_typesz00(obj_t BgL_envz00_7699,
		obj_t BgL_oz00_7700)
	{
		{	/* Liveness/types.scm 137 */
			{
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14599;

				{
					obj_t BgL_auxz00_14600;

					{	/* Liveness/types.scm 137 */
						BgL_objectz00_bglt BgL_tmpz00_14601;

						BgL_tmpz00_14601 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_7700));
						BgL_auxz00_14600 = BGL_OBJECT_WIDENING(BgL_tmpz00_14601);
					}
					BgL_auxz00_14599 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14600);
				}
				return
					(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14599))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3069> */
	obj_t BGl_z62zc3z04anonymousza33069ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7701)
	{
		{	/* Liveness/types.scm 137 */
			return BNIL;
		}

	}



/* &lambda3068 */
	obj_t BGl_z62lambda3068z62zzliveness_typesz00(obj_t BgL_envz00_7702,
		obj_t BgL_oz00_7703, obj_t BgL_vz00_7704)
	{
		{	/* Liveness/types.scm 137 */
			{
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14607;

				{
					obj_t BgL_auxz00_14608;

					{	/* Liveness/types.scm 137 */
						BgL_objectz00_bglt BgL_tmpz00_14609;

						BgL_tmpz00_14609 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_7703));
						BgL_auxz00_14608 = BGL_OBJECT_WIDENING(BgL_tmpz00_14609);
					}
					BgL_auxz00_14607 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14608);
				}
				return
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14607))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7704)), BUNSPEC);
			}
		}

	}



/* &lambda3067 */
	obj_t BGl_z62lambda3067z62zzliveness_typesz00(obj_t BgL_envz00_7705,
		obj_t BgL_oz00_7706)
	{
		{	/* Liveness/types.scm 137 */
			{
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14616;

				{
					obj_t BgL_auxz00_14617;

					{	/* Liveness/types.scm 137 */
						BgL_objectz00_bglt BgL_tmpz00_14618;

						BgL_tmpz00_14618 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_7706));
						BgL_auxz00_14617 = BGL_OBJECT_WIDENING(BgL_tmpz00_14618);
					}
					BgL_auxz00_14616 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14617);
				}
				return
					(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14616))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:3058> */
	obj_t BGl_z62zc3z04anonymousza33058ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7707)
	{
		{	/* Liveness/types.scm 137 */
			return BNIL;
		}

	}



/* &lambda3057 */
	obj_t BGl_z62lambda3057z62zzliveness_typesz00(obj_t BgL_envz00_7708,
		obj_t BgL_oz00_7709, obj_t BgL_vz00_7710)
	{
		{	/* Liveness/types.scm 137 */
			{
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14624;

				{
					obj_t BgL_auxz00_14625;

					{	/* Liveness/types.scm 137 */
						BgL_objectz00_bglt BgL_tmpz00_14626;

						BgL_tmpz00_14626 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_7709));
						BgL_auxz00_14625 = BGL_OBJECT_WIDENING(BgL_tmpz00_14626);
					}
					BgL_auxz00_14624 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14625);
				}
				return
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14624))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7710)), BUNSPEC);
			}
		}

	}



/* &lambda3056 */
	obj_t BGl_z62lambda3056z62zzliveness_typesz00(obj_t BgL_envz00_7711,
		obj_t BgL_oz00_7712)
	{
		{	/* Liveness/types.scm 137 */
			{
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14633;

				{
					obj_t BgL_auxz00_14634;

					{	/* Liveness/types.scm 137 */
						BgL_objectz00_bglt BgL_tmpz00_14635;

						BgL_tmpz00_14635 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_7712));
						BgL_auxz00_14634 = BGL_OBJECT_WIDENING(BgL_tmpz00_14635);
					}
					BgL_auxz00_14633 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14634);
				}
				return
					(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14633))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:3046> */
	obj_t BGl_z62zc3z04anonymousza33046ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7713)
	{
		{	/* Liveness/types.scm 137 */
			return BNIL;
		}

	}



/* &lambda3045 */
	obj_t BGl_z62lambda3045z62zzliveness_typesz00(obj_t BgL_envz00_7714,
		obj_t BgL_oz00_7715, obj_t BgL_vz00_7716)
	{
		{	/* Liveness/types.scm 137 */
			{
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14641;

				{
					obj_t BgL_auxz00_14642;

					{	/* Liveness/types.scm 137 */
						BgL_objectz00_bglt BgL_tmpz00_14643;

						BgL_tmpz00_14643 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_7715));
						BgL_auxz00_14642 = BGL_OBJECT_WIDENING(BgL_tmpz00_14643);
					}
					BgL_auxz00_14641 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14642);
				}
				return
					((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14641))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7716)), BUNSPEC);
			}
		}

	}



/* &lambda3044 */
	obj_t BGl_z62lambda3044z62zzliveness_typesz00(obj_t BgL_envz00_7717,
		obj_t BgL_oz00_7718)
	{
		{	/* Liveness/types.scm 137 */
			{
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_14650;

				{
					obj_t BgL_auxz00_14651;

					{	/* Liveness/types.scm 137 */
						BgL_objectz00_bglt BgL_tmpz00_14652;

						BgL_tmpz00_14652 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_7718));
						BgL_auxz00_14651 = BGL_OBJECT_WIDENING(BgL_tmpz00_14652);
					}
					BgL_auxz00_14650 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_14651);
				}
				return
					(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_14650))->
					BgL_defz00);
			}
		}

	}



/* &lambda2977 */
	BgL_instanceofz00_bglt BGl_z62lambda2977z62zzliveness_typesz00(obj_t
		BgL_envz00_7719, obj_t BgL_o1432z00_7720)
	{
		{	/* Liveness/types.scm 131 */
			{	/* Liveness/types.scm 131 */
				long BgL_arg2978z00_9188;

				{	/* Liveness/types.scm 131 */
					obj_t BgL_arg2979z00_9189;

					{	/* Liveness/types.scm 131 */
						obj_t BgL_arg2980z00_9190;

						{	/* Liveness/types.scm 131 */
							obj_t BgL_arg1815z00_9191;
							long BgL_arg1816z00_9192;

							BgL_arg1815z00_9191 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 131 */
								long BgL_arg1817z00_9193;

								BgL_arg1817z00_9193 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_instanceofz00_bglt) BgL_o1432z00_7720)));
								BgL_arg1816z00_9192 = (BgL_arg1817z00_9193 - OBJECT_TYPE);
							}
							BgL_arg2980z00_9190 =
								VECTOR_REF(BgL_arg1815z00_9191, BgL_arg1816z00_9192);
						}
						BgL_arg2979z00_9189 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2980z00_9190);
					}
					{	/* Liveness/types.scm 131 */
						obj_t BgL_tmpz00_14665;

						BgL_tmpz00_14665 = ((obj_t) BgL_arg2979z00_9189);
						BgL_arg2978z00_9188 = BGL_CLASS_NUM(BgL_tmpz00_14665);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_instanceofz00_bglt) BgL_o1432z00_7720)), BgL_arg2978z00_9188);
			}
			{	/* Liveness/types.scm 131 */
				BgL_objectz00_bglt BgL_tmpz00_14671;

				BgL_tmpz00_14671 =
					((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_o1432z00_7720));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14671, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_o1432z00_7720));
			return
				((BgL_instanceofz00_bglt) ((BgL_instanceofz00_bglt) BgL_o1432z00_7720));
		}

	}



/* &<@anonymous:2976> */
	obj_t BGl_z62zc3z04anonymousza32976ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7721, obj_t BgL_new1431z00_7722)
	{
		{	/* Liveness/types.scm 131 */
			{
				BgL_instanceofz00_bglt BgL_auxz00_14679;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_instanceofz00_bglt) BgL_new1431z00_7722))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14683;

					{	/* Liveness/types.scm 131 */
						obj_t BgL_classz00_9195;

						BgL_classz00_9195 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 131 */
							obj_t BgL__ortest_1117z00_9196;

							BgL__ortest_1117z00_9196 = BGL_CLASS_NIL(BgL_classz00_9195);
							if (CBOOL(BgL__ortest_1117z00_9196))
								{	/* Liveness/types.scm 131 */
									BgL_auxz00_14683 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9196);
								}
							else
								{	/* Liveness/types.scm 131 */
									BgL_auxz00_14683 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9195));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_instanceofz00_bglt) BgL_new1431z00_7722))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14683), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_instanceofz00_bglt) BgL_new1431z00_7722))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_instanceofz00_bglt)
										BgL_new1431z00_7722))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_instanceofz00_bglt)
										BgL_new1431z00_7722))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_instanceofz00_bglt)
										BgL_new1431z00_7722))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_instanceofz00_bglt)
										BgL_new1431z00_7722))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14708;

					{	/* Liveness/types.scm 131 */
						obj_t BgL_classz00_9197;

						BgL_classz00_9197 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 131 */
							obj_t BgL__ortest_1117z00_9198;

							BgL__ortest_1117z00_9198 = BGL_CLASS_NIL(BgL_classz00_9197);
							if (CBOOL(BgL__ortest_1117z00_9198))
								{	/* Liveness/types.scm 131 */
									BgL_auxz00_14708 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9198);
								}
							else
								{	/* Liveness/types.scm 131 */
									BgL_auxz00_14708 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9197));
								}
						}
					}
					((((BgL_instanceofz00_bglt) COBJECT(
									((BgL_instanceofz00_bglt)
										((BgL_instanceofz00_bglt) BgL_new1431z00_7722))))->
							BgL_classz00) = ((BgL_typez00_bglt) BgL_auxz00_14708), BUNSPEC);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14718;

					{
						obj_t BgL_auxz00_14719;

						{	/* Liveness/types.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_14720;

							BgL_tmpz00_14720 =
								((BgL_objectz00_bglt)
								((BgL_instanceofz00_bglt) BgL_new1431z00_7722));
							BgL_auxz00_14719 = BGL_OBJECT_WIDENING(BgL_tmpz00_14720);
						}
						BgL_auxz00_14718 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14719);
					}
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14718))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14726;

					{
						obj_t BgL_auxz00_14727;

						{	/* Liveness/types.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_14728;

							BgL_tmpz00_14728 =
								((BgL_objectz00_bglt)
								((BgL_instanceofz00_bglt) BgL_new1431z00_7722));
							BgL_auxz00_14727 = BGL_OBJECT_WIDENING(BgL_tmpz00_14728);
						}
						BgL_auxz00_14726 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14727);
					}
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14726))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14734;

					{
						obj_t BgL_auxz00_14735;

						{	/* Liveness/types.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_14736;

							BgL_tmpz00_14736 =
								((BgL_objectz00_bglt)
								((BgL_instanceofz00_bglt) BgL_new1431z00_7722));
							BgL_auxz00_14735 = BGL_OBJECT_WIDENING(BgL_tmpz00_14736);
						}
						BgL_auxz00_14734 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14735);
					}
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14734))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14742;

					{
						obj_t BgL_auxz00_14743;

						{	/* Liveness/types.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_14744;

							BgL_tmpz00_14744 =
								((BgL_objectz00_bglt)
								((BgL_instanceofz00_bglt) BgL_new1431z00_7722));
							BgL_auxz00_14743 = BGL_OBJECT_WIDENING(BgL_tmpz00_14744);
						}
						BgL_auxz00_14742 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14743);
					}
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14742))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_14679 = ((BgL_instanceofz00_bglt) BgL_new1431z00_7722);
				return ((obj_t) BgL_auxz00_14679);
			}
		}

	}



/* &lambda2974 */
	BgL_instanceofz00_bglt BGl_z62lambda2974z62zzliveness_typesz00(obj_t
		BgL_envz00_7723, obj_t BgL_o1428z00_7724)
	{
		{	/* Liveness/types.scm 131 */
			{	/* Liveness/types.scm 131 */
				BgL_instanceofzf2livenesszf2_bglt BgL_wide1430z00_9200;

				BgL_wide1430z00_9200 =
					((BgL_instanceofzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_instanceofzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 131 */
					obj_t BgL_auxz00_14757;
					BgL_objectz00_bglt BgL_tmpz00_14753;

					BgL_auxz00_14757 = ((obj_t) BgL_wide1430z00_9200);
					BgL_tmpz00_14753 =
						((BgL_objectz00_bglt)
						((BgL_instanceofz00_bglt)
							((BgL_instanceofz00_bglt) BgL_o1428z00_7724)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14753, BgL_auxz00_14757);
				}
				((BgL_objectz00_bglt)
					((BgL_instanceofz00_bglt)
						((BgL_instanceofz00_bglt) BgL_o1428z00_7724)));
				{	/* Liveness/types.scm 131 */
					long BgL_arg2975z00_9201;

					BgL_arg2975z00_9201 =
						BGL_CLASS_NUM(BGl_instanceofzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_instanceofz00_bglt)
								((BgL_instanceofz00_bglt) BgL_o1428z00_7724))),
						BgL_arg2975z00_9201);
				}
				return
					((BgL_instanceofz00_bglt)
					((BgL_instanceofz00_bglt)
						((BgL_instanceofz00_bglt) BgL_o1428z00_7724)));
			}
		}

	}



/* &lambda2969 */
	BgL_instanceofz00_bglt BGl_z62lambda2969z62zzliveness_typesz00(obj_t
		BgL_envz00_7725, obj_t BgL_loc1416z00_7726, obj_t BgL_type1417z00_7727,
		obj_t BgL_sidezd2effect1418zd2_7728, obj_t BgL_key1419z00_7729,
		obj_t BgL_exprza21420za2_7730, obj_t BgL_effect1421z00_7731,
		obj_t BgL_czd2format1422zd2_7732, obj_t BgL_class1423z00_7733,
		obj_t BgL_def1424z00_7734, obj_t BgL_use1425z00_7735,
		obj_t BgL_in1426z00_7736, obj_t BgL_out1427z00_7737)
	{
		{	/* Liveness/types.scm 131 */
			{	/* Liveness/types.scm 131 */
				BgL_instanceofz00_bglt BgL_new1775z00_9210;

				{	/* Liveness/types.scm 131 */
					BgL_instanceofz00_bglt BgL_tmp1773z00_9211;
					BgL_instanceofzf2livenesszf2_bglt BgL_wide1774z00_9212;

					{
						BgL_instanceofz00_bglt BgL_auxz00_14771;

						{	/* Liveness/types.scm 131 */
							BgL_instanceofz00_bglt BgL_new1772z00_9213;

							BgL_new1772z00_9213 =
								((BgL_instanceofz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_instanceofz00_bgl))));
							{	/* Liveness/types.scm 131 */
								long BgL_arg2973z00_9214;

								BgL_arg2973z00_9214 =
									BGL_CLASS_NUM(BGl_instanceofz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1772z00_9213),
									BgL_arg2973z00_9214);
							}
							{	/* Liveness/types.scm 131 */
								BgL_objectz00_bglt BgL_tmpz00_14776;

								BgL_tmpz00_14776 = ((BgL_objectz00_bglt) BgL_new1772z00_9213);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14776, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1772z00_9213);
							BgL_auxz00_14771 = BgL_new1772z00_9213;
						}
						BgL_tmp1773z00_9211 = ((BgL_instanceofz00_bglt) BgL_auxz00_14771);
					}
					BgL_wide1774z00_9212 =
						((BgL_instanceofzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_instanceofzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 131 */
						obj_t BgL_auxz00_14784;
						BgL_objectz00_bglt BgL_tmpz00_14782;

						BgL_auxz00_14784 = ((obj_t) BgL_wide1774z00_9212);
						BgL_tmpz00_14782 = ((BgL_objectz00_bglt) BgL_tmp1773z00_9211);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14782, BgL_auxz00_14784);
					}
					((BgL_objectz00_bglt) BgL_tmp1773z00_9211);
					{	/* Liveness/types.scm 131 */
						long BgL_arg2972z00_9215;

						BgL_arg2972z00_9215 =
							BGL_CLASS_NUM(BGl_instanceofzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1773z00_9211), BgL_arg2972z00_9215);
					}
					BgL_new1775z00_9210 = ((BgL_instanceofz00_bglt) BgL_tmp1773z00_9211);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1775z00_9210)))->BgL_locz00) =
					((obj_t) BgL_loc1416z00_7726), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1775z00_9210)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1417z00_7727)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1775z00_9210)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1418zd2_7728), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1775z00_9210)))->BgL_keyz00) =
					((obj_t) BgL_key1419z00_7729), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1775z00_9210)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21420za2_7730)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1775z00_9210)))->BgL_effectz00) =
					((obj_t) BgL_effect1421z00_7731), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1775z00_9210)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1422zd2_7732)), BUNSPEC);
				((((BgL_instanceofz00_bglt) COBJECT(((BgL_instanceofz00_bglt)
									BgL_new1775z00_9210)))->BgL_classz00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_class1423z00_7733)),
					BUNSPEC);
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14812;

					{
						obj_t BgL_auxz00_14813;

						{	/* Liveness/types.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_14814;

							BgL_tmpz00_14814 = ((BgL_objectz00_bglt) BgL_new1775z00_9210);
							BgL_auxz00_14813 = BGL_OBJECT_WIDENING(BgL_tmpz00_14814);
						}
						BgL_auxz00_14812 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14813);
					}
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14812))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1424z00_7734)), BUNSPEC);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14820;

					{
						obj_t BgL_auxz00_14821;

						{	/* Liveness/types.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_14822;

							BgL_tmpz00_14822 = ((BgL_objectz00_bglt) BgL_new1775z00_9210);
							BgL_auxz00_14821 = BGL_OBJECT_WIDENING(BgL_tmpz00_14822);
						}
						BgL_auxz00_14820 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14821);
					}
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14820))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1425z00_7735)), BUNSPEC);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14828;

					{
						obj_t BgL_auxz00_14829;

						{	/* Liveness/types.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_14830;

							BgL_tmpz00_14830 = ((BgL_objectz00_bglt) BgL_new1775z00_9210);
							BgL_auxz00_14829 = BGL_OBJECT_WIDENING(BgL_tmpz00_14830);
						}
						BgL_auxz00_14828 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14829);
					}
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14828))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1426z00_7736)), BUNSPEC);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14836;

					{
						obj_t BgL_auxz00_14837;

						{	/* Liveness/types.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_14838;

							BgL_tmpz00_14838 = ((BgL_objectz00_bglt) BgL_new1775z00_9210);
							BgL_auxz00_14837 = BGL_OBJECT_WIDENING(BgL_tmpz00_14838);
						}
						BgL_auxz00_14836 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14837);
					}
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14836))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1427z00_7737)), BUNSPEC);
				}
				return BgL_new1775z00_9210;
			}
		}

	}



/* &<@anonymous:3014> */
	obj_t BGl_z62zc3z04anonymousza33014ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7738)
	{
		{	/* Liveness/types.scm 131 */
			return BNIL;
		}

	}



/* &lambda3013 */
	obj_t BGl_z62lambda3013z62zzliveness_typesz00(obj_t BgL_envz00_7739,
		obj_t BgL_oz00_7740, obj_t BgL_vz00_7741)
	{
		{	/* Liveness/types.scm 131 */
			{
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14844;

				{
					obj_t BgL_auxz00_14845;

					{	/* Liveness/types.scm 131 */
						BgL_objectz00_bglt BgL_tmpz00_14846;

						BgL_tmpz00_14846 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_7740));
						BgL_auxz00_14845 = BGL_OBJECT_WIDENING(BgL_tmpz00_14846);
					}
					BgL_auxz00_14844 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14845);
				}
				return
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14844))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7741)), BUNSPEC);
			}
		}

	}



/* &lambda3012 */
	obj_t BGl_z62lambda3012z62zzliveness_typesz00(obj_t BgL_envz00_7742,
		obj_t BgL_oz00_7743)
	{
		{	/* Liveness/types.scm 131 */
			{
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14853;

				{
					obj_t BgL_auxz00_14854;

					{	/* Liveness/types.scm 131 */
						BgL_objectz00_bglt BgL_tmpz00_14855;

						BgL_tmpz00_14855 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_7743));
						BgL_auxz00_14854 = BGL_OBJECT_WIDENING(BgL_tmpz00_14855);
					}
					BgL_auxz00_14853 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14854);
				}
				return
					(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14853))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:3005> */
	obj_t BGl_z62zc3z04anonymousza33005ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7744)
	{
		{	/* Liveness/types.scm 131 */
			return BNIL;
		}

	}



/* &lambda3004 */
	obj_t BGl_z62lambda3004z62zzliveness_typesz00(obj_t BgL_envz00_7745,
		obj_t BgL_oz00_7746, obj_t BgL_vz00_7747)
	{
		{	/* Liveness/types.scm 131 */
			{
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14861;

				{
					obj_t BgL_auxz00_14862;

					{	/* Liveness/types.scm 131 */
						BgL_objectz00_bglt BgL_tmpz00_14863;

						BgL_tmpz00_14863 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_7746));
						BgL_auxz00_14862 = BGL_OBJECT_WIDENING(BgL_tmpz00_14863);
					}
					BgL_auxz00_14861 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14862);
				}
				return
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14861))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7747)), BUNSPEC);
			}
		}

	}



/* &lambda3003 */
	obj_t BGl_z62lambda3003z62zzliveness_typesz00(obj_t BgL_envz00_7748,
		obj_t BgL_oz00_7749)
	{
		{	/* Liveness/types.scm 131 */
			{
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14870;

				{
					obj_t BgL_auxz00_14871;

					{	/* Liveness/types.scm 131 */
						BgL_objectz00_bglt BgL_tmpz00_14872;

						BgL_tmpz00_14872 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_7749));
						BgL_auxz00_14871 = BGL_OBJECT_WIDENING(BgL_tmpz00_14872);
					}
					BgL_auxz00_14870 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14871);
				}
				return
					(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14870))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2997> */
	obj_t BGl_z62zc3z04anonymousza32997ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7750)
	{
		{	/* Liveness/types.scm 131 */
			return BNIL;
		}

	}



/* &lambda2996 */
	obj_t BGl_z62lambda2996z62zzliveness_typesz00(obj_t BgL_envz00_7751,
		obj_t BgL_oz00_7752, obj_t BgL_vz00_7753)
	{
		{	/* Liveness/types.scm 131 */
			{
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14878;

				{
					obj_t BgL_auxz00_14879;

					{	/* Liveness/types.scm 131 */
						BgL_objectz00_bglt BgL_tmpz00_14880;

						BgL_tmpz00_14880 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_7752));
						BgL_auxz00_14879 = BGL_OBJECT_WIDENING(BgL_tmpz00_14880);
					}
					BgL_auxz00_14878 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14879);
				}
				return
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14878))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7753)), BUNSPEC);
			}
		}

	}



/* &lambda2995 */
	obj_t BGl_z62lambda2995z62zzliveness_typesz00(obj_t BgL_envz00_7754,
		obj_t BgL_oz00_7755)
	{
		{	/* Liveness/types.scm 131 */
			{
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14887;

				{
					obj_t BgL_auxz00_14888;

					{	/* Liveness/types.scm 131 */
						BgL_objectz00_bglt BgL_tmpz00_14889;

						BgL_tmpz00_14889 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_7755));
						BgL_auxz00_14888 = BGL_OBJECT_WIDENING(BgL_tmpz00_14889);
					}
					BgL_auxz00_14887 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14888);
				}
				return
					(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14887))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2990> */
	obj_t BGl_z62zc3z04anonymousza32990ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7756)
	{
		{	/* Liveness/types.scm 131 */
			return BNIL;
		}

	}



/* &lambda2989 */
	obj_t BGl_z62lambda2989z62zzliveness_typesz00(obj_t BgL_envz00_7757,
		obj_t BgL_oz00_7758, obj_t BgL_vz00_7759)
	{
		{	/* Liveness/types.scm 131 */
			{
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14895;

				{
					obj_t BgL_auxz00_14896;

					{	/* Liveness/types.scm 131 */
						BgL_objectz00_bglt BgL_tmpz00_14897;

						BgL_tmpz00_14897 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_7758));
						BgL_auxz00_14896 = BGL_OBJECT_WIDENING(BgL_tmpz00_14897);
					}
					BgL_auxz00_14895 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14896);
				}
				return
					((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14895))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7759)), BUNSPEC);
			}
		}

	}



/* &lambda2988 */
	obj_t BGl_z62lambda2988z62zzliveness_typesz00(obj_t BgL_envz00_7760,
		obj_t BgL_oz00_7761)
	{
		{	/* Liveness/types.scm 131 */
			{
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_14904;

				{
					obj_t BgL_auxz00_14905;

					{	/* Liveness/types.scm 131 */
						BgL_objectz00_bglt BgL_tmpz00_14906;

						BgL_tmpz00_14906 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_7761));
						BgL_auxz00_14905 = BGL_OBJECT_WIDENING(BgL_tmpz00_14906);
					}
					BgL_auxz00_14904 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_14905);
				}
				return
					(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14904))->
					BgL_defz00);
			}
		}

	}



/* &lambda2908 */
	BgL_vlengthz00_bglt BGl_z62lambda2908z62zzliveness_typesz00(obj_t
		BgL_envz00_7762, obj_t BgL_o1414z00_7763)
	{
		{	/* Liveness/types.scm 125 */
			{	/* Liveness/types.scm 125 */
				long BgL_arg2912z00_9229;

				{	/* Liveness/types.scm 125 */
					obj_t BgL_arg2913z00_9230;

					{	/* Liveness/types.scm 125 */
						obj_t BgL_arg2914z00_9231;

						{	/* Liveness/types.scm 125 */
							obj_t BgL_arg1815z00_9232;
							long BgL_arg1816z00_9233;

							BgL_arg1815z00_9232 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 125 */
								long BgL_arg1817z00_9234;

								BgL_arg1817z00_9234 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vlengthz00_bglt) BgL_o1414z00_7763)));
								BgL_arg1816z00_9233 = (BgL_arg1817z00_9234 - OBJECT_TYPE);
							}
							BgL_arg2914z00_9231 =
								VECTOR_REF(BgL_arg1815z00_9232, BgL_arg1816z00_9233);
						}
						BgL_arg2913z00_9230 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2914z00_9231);
					}
					{	/* Liveness/types.scm 125 */
						obj_t BgL_tmpz00_14919;

						BgL_tmpz00_14919 = ((obj_t) BgL_arg2913z00_9230);
						BgL_arg2912z00_9229 = BGL_CLASS_NUM(BgL_tmpz00_14919);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vlengthz00_bglt) BgL_o1414z00_7763)), BgL_arg2912z00_9229);
			}
			{	/* Liveness/types.scm 125 */
				BgL_objectz00_bglt BgL_tmpz00_14925;

				BgL_tmpz00_14925 =
					((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1414z00_7763));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14925, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1414z00_7763));
			return ((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1414z00_7763));
		}

	}



/* &<@anonymous:2907> */
	obj_t BGl_z62zc3z04anonymousza32907ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7764, obj_t BgL_new1413z00_7765)
	{
		{	/* Liveness/types.scm 125 */
			{
				BgL_vlengthz00_bglt BgL_auxz00_14933;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vlengthz00_bglt) BgL_new1413z00_7765))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14937;

					{	/* Liveness/types.scm 125 */
						obj_t BgL_classz00_9236;

						BgL_classz00_9236 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 125 */
							obj_t BgL__ortest_1117z00_9237;

							BgL__ortest_1117z00_9237 = BGL_CLASS_NIL(BgL_classz00_9236);
							if (CBOOL(BgL__ortest_1117z00_9237))
								{	/* Liveness/types.scm 125 */
									BgL_auxz00_14937 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9237);
								}
							else
								{	/* Liveness/types.scm 125 */
									BgL_auxz00_14937 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9236));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vlengthz00_bglt) BgL_new1413z00_7765))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14937), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vlengthz00_bglt) BgL_new1413z00_7765))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vlengthz00_bglt)
										BgL_new1413z00_7765))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vlengthz00_bglt)
										BgL_new1413z00_7765))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vlengthz00_bglt)
										BgL_new1413z00_7765))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vlengthz00_bglt)
										BgL_new1413z00_7765))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14962;

					{	/* Liveness/types.scm 125 */
						obj_t BgL_classz00_9238;

						BgL_classz00_9238 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 125 */
							obj_t BgL__ortest_1117z00_9239;

							BgL__ortest_1117z00_9239 = BGL_CLASS_NIL(BgL_classz00_9238);
							if (CBOOL(BgL__ortest_1117z00_9239))
								{	/* Liveness/types.scm 125 */
									BgL_auxz00_14962 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9239);
								}
							else
								{	/* Liveness/types.scm 125 */
									BgL_auxz00_14962 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9238));
								}
						}
					}
					((((BgL_vlengthz00_bglt) COBJECT(
									((BgL_vlengthz00_bglt)
										((BgL_vlengthz00_bglt) BgL_new1413z00_7765))))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_14962), BUNSPEC);
				}
				((((BgL_vlengthz00_bglt) COBJECT(
								((BgL_vlengthz00_bglt)
									((BgL_vlengthz00_bglt) BgL_new1413z00_7765))))->
						BgL_ftypez00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_14975;

					{
						obj_t BgL_auxz00_14976;

						{	/* Liveness/types.scm 125 */
							BgL_objectz00_bglt BgL_tmpz00_14977;

							BgL_tmpz00_14977 =
								((BgL_objectz00_bglt)
								((BgL_vlengthz00_bglt) BgL_new1413z00_7765));
							BgL_auxz00_14976 = BGL_OBJECT_WIDENING(BgL_tmpz00_14977);
						}
						BgL_auxz00_14975 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_14976);
					}
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14975))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_14983;

					{
						obj_t BgL_auxz00_14984;

						{	/* Liveness/types.scm 125 */
							BgL_objectz00_bglt BgL_tmpz00_14985;

							BgL_tmpz00_14985 =
								((BgL_objectz00_bglt)
								((BgL_vlengthz00_bglt) BgL_new1413z00_7765));
							BgL_auxz00_14984 = BGL_OBJECT_WIDENING(BgL_tmpz00_14985);
						}
						BgL_auxz00_14983 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_14984);
					}
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14983))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_14991;

					{
						obj_t BgL_auxz00_14992;

						{	/* Liveness/types.scm 125 */
							BgL_objectz00_bglt BgL_tmpz00_14993;

							BgL_tmpz00_14993 =
								((BgL_objectz00_bglt)
								((BgL_vlengthz00_bglt) BgL_new1413z00_7765));
							BgL_auxz00_14992 = BGL_OBJECT_WIDENING(BgL_tmpz00_14993);
						}
						BgL_auxz00_14991 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_14992);
					}
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14991))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_14999;

					{
						obj_t BgL_auxz00_15000;

						{	/* Liveness/types.scm 125 */
							BgL_objectz00_bglt BgL_tmpz00_15001;

							BgL_tmpz00_15001 =
								((BgL_objectz00_bglt)
								((BgL_vlengthz00_bglt) BgL_new1413z00_7765));
							BgL_auxz00_15000 = BGL_OBJECT_WIDENING(BgL_tmpz00_15001);
						}
						BgL_auxz00_14999 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15000);
					}
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_14999))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_14933 = ((BgL_vlengthz00_bglt) BgL_new1413z00_7765);
				return ((obj_t) BgL_auxz00_14933);
			}
		}

	}



/* &lambda2905 */
	BgL_vlengthz00_bglt BGl_z62lambda2905z62zzliveness_typesz00(obj_t
		BgL_envz00_7766, obj_t BgL_o1410z00_7767)
	{
		{	/* Liveness/types.scm 125 */
			{	/* Liveness/types.scm 125 */
				BgL_vlengthzf2livenesszf2_bglt BgL_wide1412z00_9241;

				BgL_wide1412z00_9241 =
					((BgL_vlengthzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_vlengthzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 125 */
					obj_t BgL_auxz00_15014;
					BgL_objectz00_bglt BgL_tmpz00_15010;

					BgL_auxz00_15014 = ((obj_t) BgL_wide1412z00_9241);
					BgL_tmpz00_15010 =
						((BgL_objectz00_bglt)
						((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1410z00_7767)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15010, BgL_auxz00_15014);
				}
				((BgL_objectz00_bglt)
					((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1410z00_7767)));
				{	/* Liveness/types.scm 125 */
					long BgL_arg2906z00_9242;

					BgL_arg2906z00_9242 =
						BGL_CLASS_NUM(BGl_vlengthzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vlengthz00_bglt)
								((BgL_vlengthz00_bglt) BgL_o1410z00_7767))),
						BgL_arg2906z00_9242);
				}
				return
					((BgL_vlengthz00_bglt)
					((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1410z00_7767)));
			}
		}

	}



/* &lambda2899 */
	BgL_vlengthz00_bglt BGl_z62lambda2899z62zzliveness_typesz00(obj_t
		BgL_envz00_7768, obj_t BgL_loc1397z00_7769, obj_t BgL_type1398z00_7770,
		obj_t BgL_sidezd2effect1399zd2_7771, obj_t BgL_key1400z00_7772,
		obj_t BgL_exprza21401za2_7773, obj_t BgL_effect1402z00_7774,
		obj_t BgL_czd2format1403zd2_7775, obj_t BgL_vtype1404z00_7776,
		obj_t BgL_ftype1405z00_7777, obj_t BgL_def1406z00_7778,
		obj_t BgL_use1407z00_7779, obj_t BgL_in1408z00_7780,
		obj_t BgL_out1409z00_7781)
	{
		{	/* Liveness/types.scm 125 */
			{	/* Liveness/types.scm 125 */
				BgL_vlengthz00_bglt BgL_new1770z00_9251;

				{	/* Liveness/types.scm 125 */
					BgL_vlengthz00_bglt BgL_tmp1768z00_9252;
					BgL_vlengthzf2livenesszf2_bglt BgL_wide1769z00_9253;

					{
						BgL_vlengthz00_bglt BgL_auxz00_15028;

						{	/* Liveness/types.scm 125 */
							BgL_vlengthz00_bglt BgL_new1767z00_9254;

							BgL_new1767z00_9254 =
								((BgL_vlengthz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vlengthz00_bgl))));
							{	/* Liveness/types.scm 125 */
								long BgL_arg2904z00_9255;

								BgL_arg2904z00_9255 =
									BGL_CLASS_NUM(BGl_vlengthz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1767z00_9254),
									BgL_arg2904z00_9255);
							}
							{	/* Liveness/types.scm 125 */
								BgL_objectz00_bglt BgL_tmpz00_15033;

								BgL_tmpz00_15033 = ((BgL_objectz00_bglt) BgL_new1767z00_9254);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15033, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1767z00_9254);
							BgL_auxz00_15028 = BgL_new1767z00_9254;
						}
						BgL_tmp1768z00_9252 = ((BgL_vlengthz00_bglt) BgL_auxz00_15028);
					}
					BgL_wide1769z00_9253 =
						((BgL_vlengthzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vlengthzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 125 */
						obj_t BgL_auxz00_15041;
						BgL_objectz00_bglt BgL_tmpz00_15039;

						BgL_auxz00_15041 = ((obj_t) BgL_wide1769z00_9253);
						BgL_tmpz00_15039 = ((BgL_objectz00_bglt) BgL_tmp1768z00_9252);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15039, BgL_auxz00_15041);
					}
					((BgL_objectz00_bglt) BgL_tmp1768z00_9252);
					{	/* Liveness/types.scm 125 */
						long BgL_arg2903z00_9256;

						BgL_arg2903z00_9256 =
							BGL_CLASS_NUM(BGl_vlengthzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1768z00_9252), BgL_arg2903z00_9256);
					}
					BgL_new1770z00_9251 = ((BgL_vlengthz00_bglt) BgL_tmp1768z00_9252);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1770z00_9251)))->BgL_locz00) =
					((obj_t) BgL_loc1397z00_7769), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1770z00_9251)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1398z00_7770)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1770z00_9251)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1399zd2_7771), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1770z00_9251)))->BgL_keyz00) =
					((obj_t) BgL_key1400z00_7772), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1770z00_9251)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21401za2_7773)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1770z00_9251)))->BgL_effectz00) =
					((obj_t) BgL_effect1402z00_7774), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1770z00_9251)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1403zd2_7775)), BUNSPEC);
				((((BgL_vlengthz00_bglt) COBJECT(((BgL_vlengthz00_bglt)
									BgL_new1770z00_9251)))->BgL_vtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1404z00_7776)),
					BUNSPEC);
				((((BgL_vlengthz00_bglt) COBJECT(((BgL_vlengthz00_bglt)
									BgL_new1770z00_9251)))->BgL_ftypez00) =
					((obj_t) BgL_ftype1405z00_7777), BUNSPEC);
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15071;

					{
						obj_t BgL_auxz00_15072;

						{	/* Liveness/types.scm 125 */
							BgL_objectz00_bglt BgL_tmpz00_15073;

							BgL_tmpz00_15073 = ((BgL_objectz00_bglt) BgL_new1770z00_9251);
							BgL_auxz00_15072 = BGL_OBJECT_WIDENING(BgL_tmpz00_15073);
						}
						BgL_auxz00_15071 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15072);
					}
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15071))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1406z00_7778)), BUNSPEC);
				}
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15079;

					{
						obj_t BgL_auxz00_15080;

						{	/* Liveness/types.scm 125 */
							BgL_objectz00_bglt BgL_tmpz00_15081;

							BgL_tmpz00_15081 = ((BgL_objectz00_bglt) BgL_new1770z00_9251);
							BgL_auxz00_15080 = BGL_OBJECT_WIDENING(BgL_tmpz00_15081);
						}
						BgL_auxz00_15079 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15080);
					}
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15079))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1407z00_7779)), BUNSPEC);
				}
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15087;

					{
						obj_t BgL_auxz00_15088;

						{	/* Liveness/types.scm 125 */
							BgL_objectz00_bglt BgL_tmpz00_15089;

							BgL_tmpz00_15089 = ((BgL_objectz00_bglt) BgL_new1770z00_9251);
							BgL_auxz00_15088 = BGL_OBJECT_WIDENING(BgL_tmpz00_15089);
						}
						BgL_auxz00_15087 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15088);
					}
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15087))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1408z00_7780)), BUNSPEC);
				}
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15095;

					{
						obj_t BgL_auxz00_15096;

						{	/* Liveness/types.scm 125 */
							BgL_objectz00_bglt BgL_tmpz00_15097;

							BgL_tmpz00_15097 = ((BgL_objectz00_bglt) BgL_new1770z00_9251);
							BgL_auxz00_15096 = BGL_OBJECT_WIDENING(BgL_tmpz00_15097);
						}
						BgL_auxz00_15095 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15096);
					}
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15095))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1409z00_7781)), BUNSPEC);
				}
				return BgL_new1770z00_9251;
			}
		}

	}



/* &<@anonymous:2946> */
	obj_t BGl_z62zc3z04anonymousza32946ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7782)
	{
		{	/* Liveness/types.scm 125 */
			return BNIL;
		}

	}



/* &lambda2945 */
	obj_t BGl_z62lambda2945z62zzliveness_typesz00(obj_t BgL_envz00_7783,
		obj_t BgL_oz00_7784, obj_t BgL_vz00_7785)
	{
		{	/* Liveness/types.scm 125 */
			{
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15103;

				{
					obj_t BgL_auxz00_15104;

					{	/* Liveness/types.scm 125 */
						BgL_objectz00_bglt BgL_tmpz00_15105;

						BgL_tmpz00_15105 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_7784));
						BgL_auxz00_15104 = BGL_OBJECT_WIDENING(BgL_tmpz00_15105);
					}
					BgL_auxz00_15103 =
						((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15104);
				}
				return
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15103))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7785)), BUNSPEC);
			}
		}

	}



/* &lambda2944 */
	obj_t BGl_z62lambda2944z62zzliveness_typesz00(obj_t BgL_envz00_7786,
		obj_t BgL_oz00_7787)
	{
		{	/* Liveness/types.scm 125 */
			{
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15112;

				{
					obj_t BgL_auxz00_15113;

					{	/* Liveness/types.scm 125 */
						BgL_objectz00_bglt BgL_tmpz00_15114;

						BgL_tmpz00_15114 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_7787));
						BgL_auxz00_15113 = BGL_OBJECT_WIDENING(BgL_tmpz00_15114);
					}
					BgL_auxz00_15112 =
						((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15113);
				}
				return
					(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15112))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2936> */
	obj_t BGl_z62zc3z04anonymousza32936ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7788)
	{
		{	/* Liveness/types.scm 125 */
			return BNIL;
		}

	}



/* &lambda2935 */
	obj_t BGl_z62lambda2935z62zzliveness_typesz00(obj_t BgL_envz00_7789,
		obj_t BgL_oz00_7790, obj_t BgL_vz00_7791)
	{
		{	/* Liveness/types.scm 125 */
			{
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15120;

				{
					obj_t BgL_auxz00_15121;

					{	/* Liveness/types.scm 125 */
						BgL_objectz00_bglt BgL_tmpz00_15122;

						BgL_tmpz00_15122 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_7790));
						BgL_auxz00_15121 = BGL_OBJECT_WIDENING(BgL_tmpz00_15122);
					}
					BgL_auxz00_15120 =
						((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15121);
				}
				return
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15120))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7791)), BUNSPEC);
			}
		}

	}



/* &lambda2934 */
	obj_t BGl_z62lambda2934z62zzliveness_typesz00(obj_t BgL_envz00_7792,
		obj_t BgL_oz00_7793)
	{
		{	/* Liveness/types.scm 125 */
			{
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15129;

				{
					obj_t BgL_auxz00_15130;

					{	/* Liveness/types.scm 125 */
						BgL_objectz00_bglt BgL_tmpz00_15131;

						BgL_tmpz00_15131 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_7793));
						BgL_auxz00_15130 = BGL_OBJECT_WIDENING(BgL_tmpz00_15131);
					}
					BgL_auxz00_15129 =
						((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15130);
				}
				return
					(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15129))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2929> */
	obj_t BGl_z62zc3z04anonymousza32929ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7794)
	{
		{	/* Liveness/types.scm 125 */
			return BNIL;
		}

	}



/* &lambda2928 */
	obj_t BGl_z62lambda2928z62zzliveness_typesz00(obj_t BgL_envz00_7795,
		obj_t BgL_oz00_7796, obj_t BgL_vz00_7797)
	{
		{	/* Liveness/types.scm 125 */
			{
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15137;

				{
					obj_t BgL_auxz00_15138;

					{	/* Liveness/types.scm 125 */
						BgL_objectz00_bglt BgL_tmpz00_15139;

						BgL_tmpz00_15139 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_7796));
						BgL_auxz00_15138 = BGL_OBJECT_WIDENING(BgL_tmpz00_15139);
					}
					BgL_auxz00_15137 =
						((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15138);
				}
				return
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15137))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7797)), BUNSPEC);
			}
		}

	}



/* &lambda2927 */
	obj_t BGl_z62lambda2927z62zzliveness_typesz00(obj_t BgL_envz00_7798,
		obj_t BgL_oz00_7799)
	{
		{	/* Liveness/types.scm 125 */
			{
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15146;

				{
					obj_t BgL_auxz00_15147;

					{	/* Liveness/types.scm 125 */
						BgL_objectz00_bglt BgL_tmpz00_15148;

						BgL_tmpz00_15148 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_7799));
						BgL_auxz00_15147 = BGL_OBJECT_WIDENING(BgL_tmpz00_15148);
					}
					BgL_auxz00_15146 =
						((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15147);
				}
				return
					(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15146))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2921> */
	obj_t BGl_z62zc3z04anonymousza32921ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7800)
	{
		{	/* Liveness/types.scm 125 */
			return BNIL;
		}

	}



/* &lambda2920 */
	obj_t BGl_z62lambda2920z62zzliveness_typesz00(obj_t BgL_envz00_7801,
		obj_t BgL_oz00_7802, obj_t BgL_vz00_7803)
	{
		{	/* Liveness/types.scm 125 */
			{
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15154;

				{
					obj_t BgL_auxz00_15155;

					{	/* Liveness/types.scm 125 */
						BgL_objectz00_bglt BgL_tmpz00_15156;

						BgL_tmpz00_15156 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_7802));
						BgL_auxz00_15155 = BGL_OBJECT_WIDENING(BgL_tmpz00_15156);
					}
					BgL_auxz00_15154 =
						((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15155);
				}
				return
					((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15154))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7803)), BUNSPEC);
			}
		}

	}



/* &lambda2919 */
	obj_t BGl_z62lambda2919z62zzliveness_typesz00(obj_t BgL_envz00_7804,
		obj_t BgL_oz00_7805)
	{
		{	/* Liveness/types.scm 125 */
			{
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_15163;

				{
					obj_t BgL_auxz00_15164;

					{	/* Liveness/types.scm 125 */
						BgL_objectz00_bglt BgL_tmpz00_15165;

						BgL_tmpz00_15165 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_7805));
						BgL_auxz00_15164 = BGL_OBJECT_WIDENING(BgL_tmpz00_15165);
					}
					BgL_auxz00_15163 =
						((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_15164);
				}
				return
					(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15163))->
					BgL_defz00);
			}
		}

	}



/* &lambda2837 */
	BgL_vsetz12z12_bglt BGl_z62lambda2837z62zzliveness_typesz00(obj_t
		BgL_envz00_7806, obj_t BgL_o1395z00_7807)
	{
		{	/* Liveness/types.scm 119 */
			{	/* Liveness/types.scm 119 */
				long BgL_arg2838z00_9270;

				{	/* Liveness/types.scm 119 */
					obj_t BgL_arg2839z00_9271;

					{	/* Liveness/types.scm 119 */
						obj_t BgL_arg2844z00_9272;

						{	/* Liveness/types.scm 119 */
							obj_t BgL_arg1815z00_9273;
							long BgL_arg1816z00_9274;

							BgL_arg1815z00_9273 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 119 */
								long BgL_arg1817z00_9275;

								BgL_arg1817z00_9275 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vsetz12z12_bglt) BgL_o1395z00_7807)));
								BgL_arg1816z00_9274 = (BgL_arg1817z00_9275 - OBJECT_TYPE);
							}
							BgL_arg2844z00_9272 =
								VECTOR_REF(BgL_arg1815z00_9273, BgL_arg1816z00_9274);
						}
						BgL_arg2839z00_9271 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2844z00_9272);
					}
					{	/* Liveness/types.scm 119 */
						obj_t BgL_tmpz00_15178;

						BgL_tmpz00_15178 = ((obj_t) BgL_arg2839z00_9271);
						BgL_arg2838z00_9270 = BGL_CLASS_NUM(BgL_tmpz00_15178);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vsetz12z12_bglt) BgL_o1395z00_7807)), BgL_arg2838z00_9270);
			}
			{	/* Liveness/types.scm 119 */
				BgL_objectz00_bglt BgL_tmpz00_15184;

				BgL_tmpz00_15184 =
					((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_o1395z00_7807));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15184, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_o1395z00_7807));
			return ((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_o1395z00_7807));
		}

	}



/* &<@anonymous:2836> */
	obj_t BGl_z62zc3z04anonymousza32836ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7808, obj_t BgL_new1394z00_7809)
	{
		{	/* Liveness/types.scm 119 */
			{
				BgL_vsetz12z12_bglt BgL_auxz00_15192;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vsetz12z12_bglt) BgL_new1394z00_7809))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_15196;

					{	/* Liveness/types.scm 119 */
						obj_t BgL_classz00_9277;

						BgL_classz00_9277 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 119 */
							obj_t BgL__ortest_1117z00_9278;

							BgL__ortest_1117z00_9278 = BGL_CLASS_NIL(BgL_classz00_9277);
							if (CBOOL(BgL__ortest_1117z00_9278))
								{	/* Liveness/types.scm 119 */
									BgL_auxz00_15196 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9278);
								}
							else
								{	/* Liveness/types.scm 119 */
									BgL_auxz00_15196 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9277));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vsetz12z12_bglt) BgL_new1394z00_7809))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_15196), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vsetz12z12_bglt) BgL_new1394z00_7809))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vsetz12z12_bglt)
										BgL_new1394z00_7809))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vsetz12z12_bglt)
										BgL_new1394z00_7809))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vsetz12z12_bglt)
										BgL_new1394z00_7809))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vsetz12z12_bglt)
										BgL_new1394z00_7809))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_15221;

					{	/* Liveness/types.scm 119 */
						obj_t BgL_classz00_9279;

						BgL_classz00_9279 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 119 */
							obj_t BgL__ortest_1117z00_9280;

							BgL__ortest_1117z00_9280 = BGL_CLASS_NIL(BgL_classz00_9279);
							if (CBOOL(BgL__ortest_1117z00_9280))
								{	/* Liveness/types.scm 119 */
									BgL_auxz00_15221 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9280);
								}
							else
								{	/* Liveness/types.scm 119 */
									BgL_auxz00_15221 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9279));
								}
						}
					}
					((((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt)
										((BgL_vsetz12z12_bglt) BgL_new1394z00_7809))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_15221), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_15231;

					{	/* Liveness/types.scm 119 */
						obj_t BgL_classz00_9281;

						BgL_classz00_9281 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 119 */
							obj_t BgL__ortest_1117z00_9282;

							BgL__ortest_1117z00_9282 = BGL_CLASS_NIL(BgL_classz00_9281);
							if (CBOOL(BgL__ortest_1117z00_9282))
								{	/* Liveness/types.scm 119 */
									BgL_auxz00_15231 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9282);
								}
							else
								{	/* Liveness/types.scm 119 */
									BgL_auxz00_15231 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9281));
								}
						}
					}
					((((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt)
										((BgL_vsetz12z12_bglt) BgL_new1394z00_7809))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_15231), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_15241;

					{	/* Liveness/types.scm 119 */
						obj_t BgL_classz00_9283;

						BgL_classz00_9283 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 119 */
							obj_t BgL__ortest_1117z00_9284;

							BgL__ortest_1117z00_9284 = BGL_CLASS_NIL(BgL_classz00_9283);
							if (CBOOL(BgL__ortest_1117z00_9284))
								{	/* Liveness/types.scm 119 */
									BgL_auxz00_15241 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9284);
								}
							else
								{	/* Liveness/types.scm 119 */
									BgL_auxz00_15241 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9283));
								}
						}
					}
					((((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt)
										((BgL_vsetz12z12_bglt) BgL_new1394z00_7809))))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_15241), BUNSPEC);
				}
				((((BgL_vsetz12z12_bglt) COBJECT(
								((BgL_vsetz12z12_bglt)
									((BgL_vsetz12z12_bglt) BgL_new1394z00_7809))))->
						BgL_unsafez00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15254;

					{
						obj_t BgL_auxz00_15255;

						{	/* Liveness/types.scm 119 */
							BgL_objectz00_bglt BgL_tmpz00_15256;

							BgL_tmpz00_15256 =
								((BgL_objectz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_new1394z00_7809));
							BgL_auxz00_15255 = BGL_OBJECT_WIDENING(BgL_tmpz00_15256);
						}
						BgL_auxz00_15254 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15255);
					}
					((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15254))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15262;

					{
						obj_t BgL_auxz00_15263;

						{	/* Liveness/types.scm 119 */
							BgL_objectz00_bglt BgL_tmpz00_15264;

							BgL_tmpz00_15264 =
								((BgL_objectz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_new1394z00_7809));
							BgL_auxz00_15263 = BGL_OBJECT_WIDENING(BgL_tmpz00_15264);
						}
						BgL_auxz00_15262 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15263);
					}
					((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15262))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15270;

					{
						obj_t BgL_auxz00_15271;

						{	/* Liveness/types.scm 119 */
							BgL_objectz00_bglt BgL_tmpz00_15272;

							BgL_tmpz00_15272 =
								((BgL_objectz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_new1394z00_7809));
							BgL_auxz00_15271 = BGL_OBJECT_WIDENING(BgL_tmpz00_15272);
						}
						BgL_auxz00_15270 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15271);
					}
					((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15270))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15278;

					{
						obj_t BgL_auxz00_15279;

						{	/* Liveness/types.scm 119 */
							BgL_objectz00_bglt BgL_tmpz00_15280;

							BgL_tmpz00_15280 =
								((BgL_objectz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_new1394z00_7809));
							BgL_auxz00_15279 = BGL_OBJECT_WIDENING(BgL_tmpz00_15280);
						}
						BgL_auxz00_15278 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15279);
					}
					((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15278))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_15192 = ((BgL_vsetz12z12_bglt) BgL_new1394z00_7809);
				return ((obj_t) BgL_auxz00_15192);
			}
		}

	}



/* &lambda2834 */
	BgL_vsetz12z12_bglt BGl_z62lambda2834z62zzliveness_typesz00(obj_t
		BgL_envz00_7810, obj_t BgL_o1391z00_7811)
	{
		{	/* Liveness/types.scm 119 */
			{	/* Liveness/types.scm 119 */
				BgL_vsetz12zf2livenessze0_bglt BgL_wide1393z00_9286;

				BgL_wide1393z00_9286 =
					((BgL_vsetz12zf2livenessze0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_vsetz12zf2livenessze0_bgl))));
				{	/* Liveness/types.scm 119 */
					obj_t BgL_auxz00_15293;
					BgL_objectz00_bglt BgL_tmpz00_15289;

					BgL_auxz00_15293 = ((obj_t) BgL_wide1393z00_9286);
					BgL_tmpz00_15289 =
						((BgL_objectz00_bglt)
						((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_o1391z00_7811)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15289, BgL_auxz00_15293);
				}
				((BgL_objectz00_bglt)
					((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_o1391z00_7811)));
				{	/* Liveness/types.scm 119 */
					long BgL_arg2835z00_9287;

					BgL_arg2835z00_9287 =
						BGL_CLASS_NUM(BGl_vsetz12zf2livenessze0zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vsetz12z12_bglt)
								((BgL_vsetz12z12_bglt) BgL_o1391z00_7811))),
						BgL_arg2835z00_9287);
				}
				return
					((BgL_vsetz12z12_bglt)
					((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_o1391z00_7811)));
			}
		}

	}



/* &lambda2831 */
	BgL_vsetz12z12_bglt BGl_z62lambda2831z62zzliveness_typesz00(obj_t
		BgL_envz00_7812, obj_t BgL_loc1376z00_7813, obj_t BgL_type1377z00_7814,
		obj_t BgL_sidezd2effect1378zd2_7815, obj_t BgL_key1379z00_7816,
		obj_t BgL_exprza21380za2_7817, obj_t BgL_effect1381z00_7818,
		obj_t BgL_czd2format1382zd2_7819, obj_t BgL_ftype1383z00_7820,
		obj_t BgL_otype1384z00_7821, obj_t BgL_vtype1385z00_7822,
		obj_t BgL_unsafe1386z00_7823, obj_t BgL_def1387z00_7824,
		obj_t BgL_use1388z00_7825, obj_t BgL_in1389z00_7826,
		obj_t BgL_out1390z00_7827)
	{
		{	/* Liveness/types.scm 119 */
			{	/* Liveness/types.scm 119 */
				bool_t BgL_unsafe1386z00_9294;

				BgL_unsafe1386z00_9294 = CBOOL(BgL_unsafe1386z00_7823);
				{	/* Liveness/types.scm 119 */
					BgL_vsetz12z12_bglt BgL_new1765z00_9299;

					{	/* Liveness/types.scm 119 */
						BgL_vsetz12z12_bglt BgL_tmp1763z00_9300;
						BgL_vsetz12zf2livenessze0_bglt BgL_wide1764z00_9301;

						{
							BgL_vsetz12z12_bglt BgL_auxz00_15308;

							{	/* Liveness/types.scm 119 */
								BgL_vsetz12z12_bglt BgL_new1762z00_9302;

								BgL_new1762z00_9302 =
									((BgL_vsetz12z12_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_vsetz12z12_bgl))));
								{	/* Liveness/types.scm 119 */
									long BgL_arg2833z00_9303;

									BgL_arg2833z00_9303 =
										BGL_CLASS_NUM(BGl_vsetz12z12zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1762z00_9302),
										BgL_arg2833z00_9303);
								}
								{	/* Liveness/types.scm 119 */
									BgL_objectz00_bglt BgL_tmpz00_15313;

									BgL_tmpz00_15313 = ((BgL_objectz00_bglt) BgL_new1762z00_9302);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15313, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1762z00_9302);
								BgL_auxz00_15308 = BgL_new1762z00_9302;
							}
							BgL_tmp1763z00_9300 = ((BgL_vsetz12z12_bglt) BgL_auxz00_15308);
						}
						BgL_wide1764z00_9301 =
							((BgL_vsetz12zf2livenessze0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_vsetz12zf2livenessze0_bgl))));
						{	/* Liveness/types.scm 119 */
							obj_t BgL_auxz00_15321;
							BgL_objectz00_bglt BgL_tmpz00_15319;

							BgL_auxz00_15321 = ((obj_t) BgL_wide1764z00_9301);
							BgL_tmpz00_15319 = ((BgL_objectz00_bglt) BgL_tmp1763z00_9300);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15319, BgL_auxz00_15321);
						}
						((BgL_objectz00_bglt) BgL_tmp1763z00_9300);
						{	/* Liveness/types.scm 119 */
							long BgL_arg2832z00_9304;

							BgL_arg2832z00_9304 =
								BGL_CLASS_NUM(BGl_vsetz12zf2livenessze0zzliveness_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1763z00_9300),
								BgL_arg2832z00_9304);
						}
						BgL_new1765z00_9299 = ((BgL_vsetz12z12_bglt) BgL_tmp1763z00_9300);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1765z00_9299)))->BgL_locz00) =
						((obj_t) BgL_loc1376z00_7813), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1765z00_9299)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1377z00_7814)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1765z00_9299)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1378zd2_7815), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1765z00_9299)))->BgL_keyz00) =
						((obj_t) BgL_key1379z00_7816), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1765z00_9299)))->BgL_exprza2za2) =
						((obj_t) ((obj_t) BgL_exprza21380za2_7817)), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1765z00_9299)))->BgL_effectz00) =
						((obj_t) BgL_effect1381z00_7818), BUNSPEC);
					((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
										BgL_new1765z00_9299)))->BgL_czd2formatzd2) =
						((obj_t) ((obj_t) BgL_czd2format1382zd2_7819)), BUNSPEC);
					((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
										BgL_new1765z00_9299)))->BgL_ftypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1383z00_7820)),
						BUNSPEC);
					((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
										BgL_new1765z00_9299)))->BgL_otypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1384z00_7821)),
						BUNSPEC);
					((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
										BgL_new1765z00_9299)))->BgL_vtypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1385z00_7822)),
						BUNSPEC);
					((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
										BgL_new1765z00_9299)))->BgL_unsafez00) =
						((bool_t) BgL_unsafe1386z00_9294), BUNSPEC);
					{
						BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15357;

						{
							obj_t BgL_auxz00_15358;

							{	/* Liveness/types.scm 119 */
								BgL_objectz00_bglt BgL_tmpz00_15359;

								BgL_tmpz00_15359 = ((BgL_objectz00_bglt) BgL_new1765z00_9299);
								BgL_auxz00_15358 = BGL_OBJECT_WIDENING(BgL_tmpz00_15359);
							}
							BgL_auxz00_15357 =
								((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15358);
						}
						((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15357))->
								BgL_defz00) = ((obj_t) ((obj_t) BgL_def1387z00_7824)), BUNSPEC);
					}
					{
						BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15365;

						{
							obj_t BgL_auxz00_15366;

							{	/* Liveness/types.scm 119 */
								BgL_objectz00_bglt BgL_tmpz00_15367;

								BgL_tmpz00_15367 = ((BgL_objectz00_bglt) BgL_new1765z00_9299);
								BgL_auxz00_15366 = BGL_OBJECT_WIDENING(BgL_tmpz00_15367);
							}
							BgL_auxz00_15365 =
								((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15366);
						}
						((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15365))->
								BgL_usez00) = ((obj_t) ((obj_t) BgL_use1388z00_7825)), BUNSPEC);
					}
					{
						BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15373;

						{
							obj_t BgL_auxz00_15374;

							{	/* Liveness/types.scm 119 */
								BgL_objectz00_bglt BgL_tmpz00_15375;

								BgL_tmpz00_15375 = ((BgL_objectz00_bglt) BgL_new1765z00_9299);
								BgL_auxz00_15374 = BGL_OBJECT_WIDENING(BgL_tmpz00_15375);
							}
							BgL_auxz00_15373 =
								((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15374);
						}
						((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15373))->
								BgL_inz00) = ((obj_t) ((obj_t) BgL_in1389z00_7826)), BUNSPEC);
					}
					{
						BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15381;

						{
							obj_t BgL_auxz00_15382;

							{	/* Liveness/types.scm 119 */
								BgL_objectz00_bglt BgL_tmpz00_15383;

								BgL_tmpz00_15383 = ((BgL_objectz00_bglt) BgL_new1765z00_9299);
								BgL_auxz00_15382 = BGL_OBJECT_WIDENING(BgL_tmpz00_15383);
							}
							BgL_auxz00_15381 =
								((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15382);
						}
						((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15381))->
								BgL_outz00) = ((obj_t) ((obj_t) BgL_out1390z00_7827)), BUNSPEC);
					}
					return BgL_new1765z00_9299;
				}
			}
		}

	}



/* &<@anonymous:2892> */
	obj_t BGl_z62zc3z04anonymousza32892ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7828)
	{
		{	/* Liveness/types.scm 119 */
			return BNIL;
		}

	}



/* &lambda2891 */
	obj_t BGl_z62lambda2891z62zzliveness_typesz00(obj_t BgL_envz00_7829,
		obj_t BgL_oz00_7830, obj_t BgL_vz00_7831)
	{
		{	/* Liveness/types.scm 119 */
			{
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15389;

				{
					obj_t BgL_auxz00_15390;

					{	/* Liveness/types.scm 119 */
						BgL_objectz00_bglt BgL_tmpz00_15391;

						BgL_tmpz00_15391 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_7830));
						BgL_auxz00_15390 = BGL_OBJECT_WIDENING(BgL_tmpz00_15391);
					}
					BgL_auxz00_15389 =
						((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15390);
				}
				return
					((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15389))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7831)), BUNSPEC);
			}
		}

	}



/* &lambda2890 */
	obj_t BGl_z62lambda2890z62zzliveness_typesz00(obj_t BgL_envz00_7832,
		obj_t BgL_oz00_7833)
	{
		{	/* Liveness/types.scm 119 */
			{
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15398;

				{
					obj_t BgL_auxz00_15399;

					{	/* Liveness/types.scm 119 */
						BgL_objectz00_bglt BgL_tmpz00_15400;

						BgL_tmpz00_15400 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_7833));
						BgL_auxz00_15399 = BGL_OBJECT_WIDENING(BgL_tmpz00_15400);
					}
					BgL_auxz00_15398 =
						((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15399);
				}
				return
					(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15398))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2882> */
	obj_t BGl_z62zc3z04anonymousza32882ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7834)
	{
		{	/* Liveness/types.scm 119 */
			return BNIL;
		}

	}



/* &lambda2881 */
	obj_t BGl_z62lambda2881z62zzliveness_typesz00(obj_t BgL_envz00_7835,
		obj_t BgL_oz00_7836, obj_t BgL_vz00_7837)
	{
		{	/* Liveness/types.scm 119 */
			{
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15406;

				{
					obj_t BgL_auxz00_15407;

					{	/* Liveness/types.scm 119 */
						BgL_objectz00_bglt BgL_tmpz00_15408;

						BgL_tmpz00_15408 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_7836));
						BgL_auxz00_15407 = BGL_OBJECT_WIDENING(BgL_tmpz00_15408);
					}
					BgL_auxz00_15406 =
						((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15407);
				}
				return
					((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15406))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7837)), BUNSPEC);
			}
		}

	}



/* &lambda2880 */
	obj_t BGl_z62lambda2880z62zzliveness_typesz00(obj_t BgL_envz00_7838,
		obj_t BgL_oz00_7839)
	{
		{	/* Liveness/types.scm 119 */
			{
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15415;

				{
					obj_t BgL_auxz00_15416;

					{	/* Liveness/types.scm 119 */
						BgL_objectz00_bglt BgL_tmpz00_15417;

						BgL_tmpz00_15417 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_7839));
						BgL_auxz00_15416 = BGL_OBJECT_WIDENING(BgL_tmpz00_15417);
					}
					BgL_auxz00_15415 =
						((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15416);
				}
				return
					(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15415))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2875> */
	obj_t BGl_z62zc3z04anonymousza32875ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7840)
	{
		{	/* Liveness/types.scm 119 */
			return BNIL;
		}

	}



/* &lambda2874 */
	obj_t BGl_z62lambda2874z62zzliveness_typesz00(obj_t BgL_envz00_7841,
		obj_t BgL_oz00_7842, obj_t BgL_vz00_7843)
	{
		{	/* Liveness/types.scm 119 */
			{
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15423;

				{
					obj_t BgL_auxz00_15424;

					{	/* Liveness/types.scm 119 */
						BgL_objectz00_bglt BgL_tmpz00_15425;

						BgL_tmpz00_15425 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_7842));
						BgL_auxz00_15424 = BGL_OBJECT_WIDENING(BgL_tmpz00_15425);
					}
					BgL_auxz00_15423 =
						((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15424);
				}
				return
					((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15423))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7843)), BUNSPEC);
			}
		}

	}



/* &lambda2873 */
	obj_t BGl_z62lambda2873z62zzliveness_typesz00(obj_t BgL_envz00_7844,
		obj_t BgL_oz00_7845)
	{
		{	/* Liveness/types.scm 119 */
			{
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15432;

				{
					obj_t BgL_auxz00_15433;

					{	/* Liveness/types.scm 119 */
						BgL_objectz00_bglt BgL_tmpz00_15434;

						BgL_tmpz00_15434 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_7845));
						BgL_auxz00_15433 = BGL_OBJECT_WIDENING(BgL_tmpz00_15434);
					}
					BgL_auxz00_15432 =
						((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15433);
				}
				return
					(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15432))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2859> */
	obj_t BGl_z62zc3z04anonymousza32859ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7846)
	{
		{	/* Liveness/types.scm 119 */
			return BNIL;
		}

	}



/* &lambda2858 */
	obj_t BGl_z62lambda2858z62zzliveness_typesz00(obj_t BgL_envz00_7847,
		obj_t BgL_oz00_7848, obj_t BgL_vz00_7849)
	{
		{	/* Liveness/types.scm 119 */
			{
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15440;

				{
					obj_t BgL_auxz00_15441;

					{	/* Liveness/types.scm 119 */
						BgL_objectz00_bglt BgL_tmpz00_15442;

						BgL_tmpz00_15442 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_7848));
						BgL_auxz00_15441 = BGL_OBJECT_WIDENING(BgL_tmpz00_15442);
					}
					BgL_auxz00_15440 =
						((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15441);
				}
				return
					((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15440))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7849)), BUNSPEC);
			}
		}

	}



/* &lambda2857 */
	obj_t BGl_z62lambda2857z62zzliveness_typesz00(obj_t BgL_envz00_7850,
		obj_t BgL_oz00_7851)
	{
		{	/* Liveness/types.scm 119 */
			{
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_15449;

				{
					obj_t BgL_auxz00_15450;

					{	/* Liveness/types.scm 119 */
						BgL_objectz00_bglt BgL_tmpz00_15451;

						BgL_tmpz00_15451 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_7851));
						BgL_auxz00_15450 = BGL_OBJECT_WIDENING(BgL_tmpz00_15451);
					}
					BgL_auxz00_15449 =
						((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_15450);
				}
				return
					(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_15449))->
					BgL_defz00);
			}
		}

	}



/* &lambda2775 */
	BgL_vrefz00_bglt BGl_z62lambda2775z62zzliveness_typesz00(obj_t
		BgL_envz00_7852, obj_t BgL_o1374z00_7853)
	{
		{	/* Liveness/types.scm 113 */
			{	/* Liveness/types.scm 113 */
				long BgL_arg2776z00_9318;

				{	/* Liveness/types.scm 113 */
					obj_t BgL_arg2777z00_9319;

					{	/* Liveness/types.scm 113 */
						obj_t BgL_arg2778z00_9320;

						{	/* Liveness/types.scm 113 */
							obj_t BgL_arg1815z00_9321;
							long BgL_arg1816z00_9322;

							BgL_arg1815z00_9321 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 113 */
								long BgL_arg1817z00_9323;

								BgL_arg1817z00_9323 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt) BgL_o1374z00_7853)));
								BgL_arg1816z00_9322 = (BgL_arg1817z00_9323 - OBJECT_TYPE);
							}
							BgL_arg2778z00_9320 =
								VECTOR_REF(BgL_arg1815z00_9321, BgL_arg1816z00_9322);
						}
						BgL_arg2777z00_9319 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2778z00_9320);
					}
					{	/* Liveness/types.scm 113 */
						obj_t BgL_tmpz00_15464;

						BgL_tmpz00_15464 = ((obj_t) BgL_arg2777z00_9319);
						BgL_arg2776z00_9318 = BGL_CLASS_NUM(BgL_tmpz00_15464);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vrefz00_bglt) BgL_o1374z00_7853)), BgL_arg2776z00_9318);
			}
			{	/* Liveness/types.scm 113 */
				BgL_objectz00_bglt BgL_tmpz00_15470;

				BgL_tmpz00_15470 =
					((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_o1374z00_7853));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15470, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_o1374z00_7853));
			return ((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_o1374z00_7853));
		}

	}



/* &<@anonymous:2774> */
	obj_t BGl_z62zc3z04anonymousza32774ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7854, obj_t BgL_new1373z00_7855)
	{
		{	/* Liveness/types.scm 113 */
			{
				BgL_vrefz00_bglt BgL_auxz00_15478;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vrefz00_bglt) BgL_new1373z00_7855))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_15482;

					{	/* Liveness/types.scm 113 */
						obj_t BgL_classz00_9325;

						BgL_classz00_9325 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 113 */
							obj_t BgL__ortest_1117z00_9326;

							BgL__ortest_1117z00_9326 = BGL_CLASS_NIL(BgL_classz00_9325);
							if (CBOOL(BgL__ortest_1117z00_9326))
								{	/* Liveness/types.scm 113 */
									BgL_auxz00_15482 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9326);
								}
							else
								{	/* Liveness/types.scm 113 */
									BgL_auxz00_15482 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9325));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vrefz00_bglt) BgL_new1373z00_7855))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_15482), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vrefz00_bglt) BgL_new1373z00_7855))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vrefz00_bglt)
										BgL_new1373z00_7855))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vrefz00_bglt)
										BgL_new1373z00_7855))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vrefz00_bglt)
										BgL_new1373z00_7855))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vrefz00_bglt)
										BgL_new1373z00_7855))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_15507;

					{	/* Liveness/types.scm 113 */
						obj_t BgL_classz00_9327;

						BgL_classz00_9327 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 113 */
							obj_t BgL__ortest_1117z00_9328;

							BgL__ortest_1117z00_9328 = BGL_CLASS_NIL(BgL_classz00_9327);
							if (CBOOL(BgL__ortest_1117z00_9328))
								{	/* Liveness/types.scm 113 */
									BgL_auxz00_15507 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9328);
								}
							else
								{	/* Liveness/types.scm 113 */
									BgL_auxz00_15507 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9327));
								}
						}
					}
					((((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt)
										((BgL_vrefz00_bglt) BgL_new1373z00_7855))))->BgL_ftypez00) =
						((BgL_typez00_bglt) BgL_auxz00_15507), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_15517;

					{	/* Liveness/types.scm 113 */
						obj_t BgL_classz00_9329;

						BgL_classz00_9329 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 113 */
							obj_t BgL__ortest_1117z00_9330;

							BgL__ortest_1117z00_9330 = BGL_CLASS_NIL(BgL_classz00_9329);
							if (CBOOL(BgL__ortest_1117z00_9330))
								{	/* Liveness/types.scm 113 */
									BgL_auxz00_15517 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9330);
								}
							else
								{	/* Liveness/types.scm 113 */
									BgL_auxz00_15517 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9329));
								}
						}
					}
					((((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt)
										((BgL_vrefz00_bglt) BgL_new1373z00_7855))))->BgL_otypez00) =
						((BgL_typez00_bglt) BgL_auxz00_15517), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_15527;

					{	/* Liveness/types.scm 113 */
						obj_t BgL_classz00_9331;

						BgL_classz00_9331 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 113 */
							obj_t BgL__ortest_1117z00_9332;

							BgL__ortest_1117z00_9332 = BGL_CLASS_NIL(BgL_classz00_9331);
							if (CBOOL(BgL__ortest_1117z00_9332))
								{	/* Liveness/types.scm 113 */
									BgL_auxz00_15527 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9332);
								}
							else
								{	/* Liveness/types.scm 113 */
									BgL_auxz00_15527 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9331));
								}
						}
					}
					((((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt)
										((BgL_vrefz00_bglt) BgL_new1373z00_7855))))->BgL_vtypez00) =
						((BgL_typez00_bglt) BgL_auxz00_15527), BUNSPEC);
				}
				((((BgL_vrefz00_bglt) COBJECT(
								((BgL_vrefz00_bglt)
									((BgL_vrefz00_bglt) BgL_new1373z00_7855))))->BgL_unsafez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15540;

					{
						obj_t BgL_auxz00_15541;

						{	/* Liveness/types.scm 113 */
							BgL_objectz00_bglt BgL_tmpz00_15542;

							BgL_tmpz00_15542 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_new1373z00_7855));
							BgL_auxz00_15541 = BGL_OBJECT_WIDENING(BgL_tmpz00_15542);
						}
						BgL_auxz00_15540 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15541);
					}
					((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15540))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15548;

					{
						obj_t BgL_auxz00_15549;

						{	/* Liveness/types.scm 113 */
							BgL_objectz00_bglt BgL_tmpz00_15550;

							BgL_tmpz00_15550 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_new1373z00_7855));
							BgL_auxz00_15549 = BGL_OBJECT_WIDENING(BgL_tmpz00_15550);
						}
						BgL_auxz00_15548 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15549);
					}
					((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15548))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15556;

					{
						obj_t BgL_auxz00_15557;

						{	/* Liveness/types.scm 113 */
							BgL_objectz00_bglt BgL_tmpz00_15558;

							BgL_tmpz00_15558 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_new1373z00_7855));
							BgL_auxz00_15557 = BGL_OBJECT_WIDENING(BgL_tmpz00_15558);
						}
						BgL_auxz00_15556 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15557);
					}
					((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15556))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15564;

					{
						obj_t BgL_auxz00_15565;

						{	/* Liveness/types.scm 113 */
							BgL_objectz00_bglt BgL_tmpz00_15566;

							BgL_tmpz00_15566 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_new1373z00_7855));
							BgL_auxz00_15565 = BGL_OBJECT_WIDENING(BgL_tmpz00_15566);
						}
						BgL_auxz00_15564 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15565);
					}
					((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15564))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_15478 = ((BgL_vrefz00_bglt) BgL_new1373z00_7855);
				return ((obj_t) BgL_auxz00_15478);
			}
		}

	}



/* &lambda2772 */
	BgL_vrefz00_bglt BGl_z62lambda2772z62zzliveness_typesz00(obj_t
		BgL_envz00_7856, obj_t BgL_o1370z00_7857)
	{
		{	/* Liveness/types.scm 113 */
			{	/* Liveness/types.scm 113 */
				BgL_vrefzf2livenesszf2_bglt BgL_wide1372z00_9334;

				BgL_wide1372z00_9334 =
					((BgL_vrefzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_vrefzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 113 */
					obj_t BgL_auxz00_15579;
					BgL_objectz00_bglt BgL_tmpz00_15575;

					BgL_auxz00_15579 = ((obj_t) BgL_wide1372z00_9334);
					BgL_tmpz00_15575 =
						((BgL_objectz00_bglt)
						((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_o1370z00_7857)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15575, BgL_auxz00_15579);
				}
				((BgL_objectz00_bglt)
					((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_o1370z00_7857)));
				{	/* Liveness/types.scm 113 */
					long BgL_arg2773z00_9335;

					BgL_arg2773z00_9335 =
						BGL_CLASS_NUM(BGl_vrefzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vrefz00_bglt)
								((BgL_vrefz00_bglt) BgL_o1370z00_7857))), BgL_arg2773z00_9335);
				}
				return
					((BgL_vrefz00_bglt)
					((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_o1370z00_7857)));
			}
		}

	}



/* &lambda2766 */
	BgL_vrefz00_bglt BGl_z62lambda2766z62zzliveness_typesz00(obj_t
		BgL_envz00_7858, obj_t BgL_loc1355z00_7859, obj_t BgL_type1356z00_7860,
		obj_t BgL_sidezd2effect1357zd2_7861, obj_t BgL_key1358z00_7862,
		obj_t BgL_exprza21359za2_7863, obj_t BgL_effect1360z00_7864,
		obj_t BgL_czd2format1361zd2_7865, obj_t BgL_ftype1362z00_7866,
		obj_t BgL_otype1363z00_7867, obj_t BgL_vtype1364z00_7868,
		obj_t BgL_unsafe1365z00_7869, obj_t BgL_def1366z00_7870,
		obj_t BgL_use1367z00_7871, obj_t BgL_in1368z00_7872,
		obj_t BgL_out1369z00_7873)
	{
		{	/* Liveness/types.scm 113 */
			{	/* Liveness/types.scm 113 */
				bool_t BgL_unsafe1365z00_9342;

				BgL_unsafe1365z00_9342 = CBOOL(BgL_unsafe1365z00_7869);
				{	/* Liveness/types.scm 113 */
					BgL_vrefz00_bglt BgL_new1760z00_9347;

					{	/* Liveness/types.scm 113 */
						BgL_vrefz00_bglt BgL_tmp1758z00_9348;
						BgL_vrefzf2livenesszf2_bglt BgL_wide1759z00_9349;

						{
							BgL_vrefz00_bglt BgL_auxz00_15594;

							{	/* Liveness/types.scm 113 */
								BgL_vrefz00_bglt BgL_new1757z00_9350;

								BgL_new1757z00_9350 =
									((BgL_vrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_vrefz00_bgl))));
								{	/* Liveness/types.scm 113 */
									long BgL_arg2771z00_9351;

									BgL_arg2771z00_9351 = BGL_CLASS_NUM(BGl_vrefz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1757z00_9350),
										BgL_arg2771z00_9351);
								}
								{	/* Liveness/types.scm 113 */
									BgL_objectz00_bglt BgL_tmpz00_15599;

									BgL_tmpz00_15599 = ((BgL_objectz00_bglt) BgL_new1757z00_9350);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15599, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1757z00_9350);
								BgL_auxz00_15594 = BgL_new1757z00_9350;
							}
							BgL_tmp1758z00_9348 = ((BgL_vrefz00_bglt) BgL_auxz00_15594);
						}
						BgL_wide1759z00_9349 =
							((BgL_vrefzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_vrefzf2livenesszf2_bgl))));
						{	/* Liveness/types.scm 113 */
							obj_t BgL_auxz00_15607;
							BgL_objectz00_bglt BgL_tmpz00_15605;

							BgL_auxz00_15607 = ((obj_t) BgL_wide1759z00_9349);
							BgL_tmpz00_15605 = ((BgL_objectz00_bglt) BgL_tmp1758z00_9348);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15605, BgL_auxz00_15607);
						}
						((BgL_objectz00_bglt) BgL_tmp1758z00_9348);
						{	/* Liveness/types.scm 113 */
							long BgL_arg2767z00_9352;

							BgL_arg2767z00_9352 =
								BGL_CLASS_NUM(BGl_vrefzf2livenesszf2zzliveness_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1758z00_9348),
								BgL_arg2767z00_9352);
						}
						BgL_new1760z00_9347 = ((BgL_vrefz00_bglt) BgL_tmp1758z00_9348);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1760z00_9347)))->BgL_locz00) =
						((obj_t) BgL_loc1355z00_7859), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1760z00_9347)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1356z00_7860)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1760z00_9347)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1357zd2_7861), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1760z00_9347)))->BgL_keyz00) =
						((obj_t) BgL_key1358z00_7862), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1760z00_9347)))->BgL_exprza2za2) =
						((obj_t) ((obj_t) BgL_exprza21359za2_7863)), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1760z00_9347)))->BgL_effectz00) =
						((obj_t) BgL_effect1360z00_7864), BUNSPEC);
					((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
										BgL_new1760z00_9347)))->BgL_czd2formatzd2) =
						((obj_t) ((obj_t) BgL_czd2format1361zd2_7865)), BUNSPEC);
					((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
										BgL_new1760z00_9347)))->BgL_ftypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1362z00_7866)),
						BUNSPEC);
					((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
										BgL_new1760z00_9347)))->BgL_otypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1363z00_7867)),
						BUNSPEC);
					((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
										BgL_new1760z00_9347)))->BgL_vtypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1364z00_7868)),
						BUNSPEC);
					((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
										BgL_new1760z00_9347)))->BgL_unsafez00) =
						((bool_t) BgL_unsafe1365z00_9342), BUNSPEC);
					{
						BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15643;

						{
							obj_t BgL_auxz00_15644;

							{	/* Liveness/types.scm 113 */
								BgL_objectz00_bglt BgL_tmpz00_15645;

								BgL_tmpz00_15645 = ((BgL_objectz00_bglt) BgL_new1760z00_9347);
								BgL_auxz00_15644 = BGL_OBJECT_WIDENING(BgL_tmpz00_15645);
							}
							BgL_auxz00_15643 =
								((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15644);
						}
						((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15643))->
								BgL_defz00) = ((obj_t) ((obj_t) BgL_def1366z00_7870)), BUNSPEC);
					}
					{
						BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15651;

						{
							obj_t BgL_auxz00_15652;

							{	/* Liveness/types.scm 113 */
								BgL_objectz00_bglt BgL_tmpz00_15653;

								BgL_tmpz00_15653 = ((BgL_objectz00_bglt) BgL_new1760z00_9347);
								BgL_auxz00_15652 = BGL_OBJECT_WIDENING(BgL_tmpz00_15653);
							}
							BgL_auxz00_15651 =
								((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15652);
						}
						((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15651))->
								BgL_usez00) = ((obj_t) ((obj_t) BgL_use1367z00_7871)), BUNSPEC);
					}
					{
						BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15659;

						{
							obj_t BgL_auxz00_15660;

							{	/* Liveness/types.scm 113 */
								BgL_objectz00_bglt BgL_tmpz00_15661;

								BgL_tmpz00_15661 = ((BgL_objectz00_bglt) BgL_new1760z00_9347);
								BgL_auxz00_15660 = BGL_OBJECT_WIDENING(BgL_tmpz00_15661);
							}
							BgL_auxz00_15659 =
								((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15660);
						}
						((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15659))->
								BgL_inz00) = ((obj_t) ((obj_t) BgL_in1368z00_7872)), BUNSPEC);
					}
					{
						BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15667;

						{
							obj_t BgL_auxz00_15668;

							{	/* Liveness/types.scm 113 */
								BgL_objectz00_bglt BgL_tmpz00_15669;

								BgL_tmpz00_15669 = ((BgL_objectz00_bglt) BgL_new1760z00_9347);
								BgL_auxz00_15668 = BGL_OBJECT_WIDENING(BgL_tmpz00_15669);
							}
							BgL_auxz00_15667 =
								((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15668);
						}
						((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15667))->
								BgL_outz00) = ((obj_t) ((obj_t) BgL_out1369z00_7873)), BUNSPEC);
					}
					return BgL_new1760z00_9347;
				}
			}
		}

	}



/* &<@anonymous:2823> */
	obj_t BGl_z62zc3z04anonymousza32823ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7874)
	{
		{	/* Liveness/types.scm 113 */
			return BNIL;
		}

	}



/* &lambda2822 */
	obj_t BGl_z62lambda2822z62zzliveness_typesz00(obj_t BgL_envz00_7875,
		obj_t BgL_oz00_7876, obj_t BgL_vz00_7877)
	{
		{	/* Liveness/types.scm 113 */
			{
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15675;

				{
					obj_t BgL_auxz00_15676;

					{	/* Liveness/types.scm 113 */
						BgL_objectz00_bglt BgL_tmpz00_15677;

						BgL_tmpz00_15677 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_7876));
						BgL_auxz00_15676 = BGL_OBJECT_WIDENING(BgL_tmpz00_15677);
					}
					BgL_auxz00_15675 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15676);
				}
				return
					((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15675))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7877)), BUNSPEC);
			}
		}

	}



/* &lambda2821 */
	obj_t BGl_z62lambda2821z62zzliveness_typesz00(obj_t BgL_envz00_7878,
		obj_t BgL_oz00_7879)
	{
		{	/* Liveness/types.scm 113 */
			{
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15684;

				{
					obj_t BgL_auxz00_15685;

					{	/* Liveness/types.scm 113 */
						BgL_objectz00_bglt BgL_tmpz00_15686;

						BgL_tmpz00_15686 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_7879));
						BgL_auxz00_15685 = BGL_OBJECT_WIDENING(BgL_tmpz00_15686);
					}
					BgL_auxz00_15684 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15685);
				}
				return
					(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15684))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2812> */
	obj_t BGl_z62zc3z04anonymousza32812ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7880)
	{
		{	/* Liveness/types.scm 113 */
			return BNIL;
		}

	}



/* &lambda2811 */
	obj_t BGl_z62lambda2811z62zzliveness_typesz00(obj_t BgL_envz00_7881,
		obj_t BgL_oz00_7882, obj_t BgL_vz00_7883)
	{
		{	/* Liveness/types.scm 113 */
			{
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15692;

				{
					obj_t BgL_auxz00_15693;

					{	/* Liveness/types.scm 113 */
						BgL_objectz00_bglt BgL_tmpz00_15694;

						BgL_tmpz00_15694 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_7882));
						BgL_auxz00_15693 = BGL_OBJECT_WIDENING(BgL_tmpz00_15694);
					}
					BgL_auxz00_15692 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15693);
				}
				return
					((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15692))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7883)), BUNSPEC);
			}
		}

	}



/* &lambda2810 */
	obj_t BGl_z62lambda2810z62zzliveness_typesz00(obj_t BgL_envz00_7884,
		obj_t BgL_oz00_7885)
	{
		{	/* Liveness/types.scm 113 */
			{
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15701;

				{
					obj_t BgL_auxz00_15702;

					{	/* Liveness/types.scm 113 */
						BgL_objectz00_bglt BgL_tmpz00_15703;

						BgL_tmpz00_15703 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_7885));
						BgL_auxz00_15702 = BGL_OBJECT_WIDENING(BgL_tmpz00_15703);
					}
					BgL_auxz00_15701 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15702);
				}
				return
					(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15701))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2802> */
	obj_t BGl_z62zc3z04anonymousza32802ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7886)
	{
		{	/* Liveness/types.scm 113 */
			return BNIL;
		}

	}



/* &lambda2801 */
	obj_t BGl_z62lambda2801z62zzliveness_typesz00(obj_t BgL_envz00_7887,
		obj_t BgL_oz00_7888, obj_t BgL_vz00_7889)
	{
		{	/* Liveness/types.scm 113 */
			{
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15709;

				{
					obj_t BgL_auxz00_15710;

					{	/* Liveness/types.scm 113 */
						BgL_objectz00_bglt BgL_tmpz00_15711;

						BgL_tmpz00_15711 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_7888));
						BgL_auxz00_15710 = BGL_OBJECT_WIDENING(BgL_tmpz00_15711);
					}
					BgL_auxz00_15709 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15710);
				}
				return
					((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15709))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7889)), BUNSPEC);
			}
		}

	}



/* &lambda2800 */
	obj_t BGl_z62lambda2800z62zzliveness_typesz00(obj_t BgL_envz00_7890,
		obj_t BgL_oz00_7891)
	{
		{	/* Liveness/types.scm 113 */
			{
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15718;

				{
					obj_t BgL_auxz00_15719;

					{	/* Liveness/types.scm 113 */
						BgL_objectz00_bglt BgL_tmpz00_15720;

						BgL_tmpz00_15720 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_7891));
						BgL_auxz00_15719 = BGL_OBJECT_WIDENING(BgL_tmpz00_15720);
					}
					BgL_auxz00_15718 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15719);
				}
				return
					(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15718))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2787> */
	obj_t BGl_z62zc3z04anonymousza32787ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7892)
	{
		{	/* Liveness/types.scm 113 */
			return BNIL;
		}

	}



/* &lambda2786 */
	obj_t BGl_z62lambda2786z62zzliveness_typesz00(obj_t BgL_envz00_7893,
		obj_t BgL_oz00_7894, obj_t BgL_vz00_7895)
	{
		{	/* Liveness/types.scm 113 */
			{
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15726;

				{
					obj_t BgL_auxz00_15727;

					{	/* Liveness/types.scm 113 */
						BgL_objectz00_bglt BgL_tmpz00_15728;

						BgL_tmpz00_15728 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_7894));
						BgL_auxz00_15727 = BGL_OBJECT_WIDENING(BgL_tmpz00_15728);
					}
					BgL_auxz00_15726 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15727);
				}
				return
					((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15726))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7895)), BUNSPEC);
			}
		}

	}



/* &lambda2785 */
	obj_t BGl_z62lambda2785z62zzliveness_typesz00(obj_t BgL_envz00_7896,
		obj_t BgL_oz00_7897)
	{
		{	/* Liveness/types.scm 113 */
			{
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_15735;

				{
					obj_t BgL_auxz00_15736;

					{	/* Liveness/types.scm 113 */
						BgL_objectz00_bglt BgL_tmpz00_15737;

						BgL_tmpz00_15737 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_7897));
						BgL_auxz00_15736 = BGL_OBJECT_WIDENING(BgL_tmpz00_15737);
					}
					BgL_auxz00_15735 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_15736);
				}
				return
					(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_15735))->
					BgL_defz00);
			}
		}

	}



/* &lambda2723 */
	BgL_vallocz00_bglt BGl_z62lambda2723z62zzliveness_typesz00(obj_t
		BgL_envz00_7898, obj_t BgL_o1353z00_7899)
	{
		{	/* Liveness/types.scm 107 */
			{	/* Liveness/types.scm 107 */
				long BgL_arg2724z00_9366;

				{	/* Liveness/types.scm 107 */
					obj_t BgL_arg2725z00_9367;

					{	/* Liveness/types.scm 107 */
						obj_t BgL_arg2726z00_9368;

						{	/* Liveness/types.scm 107 */
							obj_t BgL_arg1815z00_9369;
							long BgL_arg1816z00_9370;

							BgL_arg1815z00_9369 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 107 */
								long BgL_arg1817z00_9371;

								BgL_arg1817z00_9371 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt) BgL_o1353z00_7899)));
								BgL_arg1816z00_9370 = (BgL_arg1817z00_9371 - OBJECT_TYPE);
							}
							BgL_arg2726z00_9368 =
								VECTOR_REF(BgL_arg1815z00_9369, BgL_arg1816z00_9370);
						}
						BgL_arg2725z00_9367 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2726z00_9368);
					}
					{	/* Liveness/types.scm 107 */
						obj_t BgL_tmpz00_15750;

						BgL_tmpz00_15750 = ((obj_t) BgL_arg2725z00_9367);
						BgL_arg2724z00_9366 = BGL_CLASS_NUM(BgL_tmpz00_15750);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vallocz00_bglt) BgL_o1353z00_7899)), BgL_arg2724z00_9366);
			}
			{	/* Liveness/types.scm 107 */
				BgL_objectz00_bglt BgL_tmpz00_15756;

				BgL_tmpz00_15756 =
					((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_o1353z00_7899));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15756, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_o1353z00_7899));
			return ((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1353z00_7899));
		}

	}



/* &<@anonymous:2722> */
	obj_t BGl_z62zc3z04anonymousza32722ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7900, obj_t BgL_new1352z00_7901)
	{
		{	/* Liveness/types.scm 107 */
			{
				BgL_vallocz00_bglt BgL_auxz00_15764;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vallocz00_bglt) BgL_new1352z00_7901))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_15768;

					{	/* Liveness/types.scm 107 */
						obj_t BgL_classz00_9373;

						BgL_classz00_9373 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 107 */
							obj_t BgL__ortest_1117z00_9374;

							BgL__ortest_1117z00_9374 = BGL_CLASS_NIL(BgL_classz00_9373);
							if (CBOOL(BgL__ortest_1117z00_9374))
								{	/* Liveness/types.scm 107 */
									BgL_auxz00_15768 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9374);
								}
							else
								{	/* Liveness/types.scm 107 */
									BgL_auxz00_15768 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9373));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vallocz00_bglt) BgL_new1352z00_7901))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_15768), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vallocz00_bglt) BgL_new1352z00_7901))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vallocz00_bglt)
										BgL_new1352z00_7901))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1352z00_7901))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1352z00_7901))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1352z00_7901))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_15793;

					{	/* Liveness/types.scm 107 */
						obj_t BgL_classz00_9375;

						BgL_classz00_9375 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 107 */
							obj_t BgL__ortest_1117z00_9376;

							BgL__ortest_1117z00_9376 = BGL_CLASS_NIL(BgL_classz00_9375);
							if (CBOOL(BgL__ortest_1117z00_9376))
								{	/* Liveness/types.scm 107 */
									BgL_auxz00_15793 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9376);
								}
							else
								{	/* Liveness/types.scm 107 */
									BgL_auxz00_15793 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9375));
								}
						}
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_new1352z00_7901))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_15793), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_15803;

					{	/* Liveness/types.scm 107 */
						obj_t BgL_classz00_9377;

						BgL_classz00_9377 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 107 */
							obj_t BgL__ortest_1117z00_9378;

							BgL__ortest_1117z00_9378 = BGL_CLASS_NIL(BgL_classz00_9377);
							if (CBOOL(BgL__ortest_1117z00_9378))
								{	/* Liveness/types.scm 107 */
									BgL_auxz00_15803 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9378);
								}
							else
								{	/* Liveness/types.scm 107 */
									BgL_auxz00_15803 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9377));
								}
						}
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_new1352z00_7901))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_15803), BUNSPEC);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_15813;

					{
						obj_t BgL_auxz00_15814;

						{	/* Liveness/types.scm 107 */
							BgL_objectz00_bglt BgL_tmpz00_15815;

							BgL_tmpz00_15815 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1352z00_7901));
							BgL_auxz00_15814 = BGL_OBJECT_WIDENING(BgL_tmpz00_15815);
						}
						BgL_auxz00_15813 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15814);
					}
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15813))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_15821;

					{
						obj_t BgL_auxz00_15822;

						{	/* Liveness/types.scm 107 */
							BgL_objectz00_bglt BgL_tmpz00_15823;

							BgL_tmpz00_15823 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1352z00_7901));
							BgL_auxz00_15822 = BGL_OBJECT_WIDENING(BgL_tmpz00_15823);
						}
						BgL_auxz00_15821 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15822);
					}
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15821))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_15829;

					{
						obj_t BgL_auxz00_15830;

						{	/* Liveness/types.scm 107 */
							BgL_objectz00_bglt BgL_tmpz00_15831;

							BgL_tmpz00_15831 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1352z00_7901));
							BgL_auxz00_15830 = BGL_OBJECT_WIDENING(BgL_tmpz00_15831);
						}
						BgL_auxz00_15829 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15830);
					}
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15829))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_15837;

					{
						obj_t BgL_auxz00_15838;

						{	/* Liveness/types.scm 107 */
							BgL_objectz00_bglt BgL_tmpz00_15839;

							BgL_tmpz00_15839 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1352z00_7901));
							BgL_auxz00_15838 = BGL_OBJECT_WIDENING(BgL_tmpz00_15839);
						}
						BgL_auxz00_15837 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15838);
					}
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15837))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_15764 = ((BgL_vallocz00_bglt) BgL_new1352z00_7901);
				return ((obj_t) BgL_auxz00_15764);
			}
		}

	}



/* &lambda2720 */
	BgL_vallocz00_bglt BGl_z62lambda2720z62zzliveness_typesz00(obj_t
		BgL_envz00_7902, obj_t BgL_o1349z00_7903)
	{
		{	/* Liveness/types.scm 107 */
			{	/* Liveness/types.scm 107 */
				BgL_valloczf2livenesszf2_bglt BgL_wide1351z00_9380;

				BgL_wide1351z00_9380 =
					((BgL_valloczf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_valloczf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 107 */
					obj_t BgL_auxz00_15852;
					BgL_objectz00_bglt BgL_tmpz00_15848;

					BgL_auxz00_15852 = ((obj_t) BgL_wide1351z00_9380);
					BgL_tmpz00_15848 =
						((BgL_objectz00_bglt)
						((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1349z00_7903)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15848, BgL_auxz00_15852);
				}
				((BgL_objectz00_bglt)
					((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1349z00_7903)));
				{	/* Liveness/types.scm 107 */
					long BgL_arg2721z00_9381;

					BgL_arg2721z00_9381 =
						BGL_CLASS_NUM(BGl_valloczf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vallocz00_bglt)
								((BgL_vallocz00_bglt) BgL_o1349z00_7903))),
						BgL_arg2721z00_9381);
				}
				return
					((BgL_vallocz00_bglt)
					((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1349z00_7903)));
			}
		}

	}



/* &lambda2716 */
	BgL_vallocz00_bglt BGl_z62lambda2716z62zzliveness_typesz00(obj_t
		BgL_envz00_7904, obj_t BgL_loc1336z00_7905, obj_t BgL_type1337z00_7906,
		obj_t BgL_sidezd2effect1338zd2_7907, obj_t BgL_key1339z00_7908,
		obj_t BgL_exprza21340za2_7909, obj_t BgL_effect1341z00_7910,
		obj_t BgL_czd2format1342zd2_7911, obj_t BgL_ftype1343z00_7912,
		obj_t BgL_otype1344z00_7913, obj_t BgL_def1345z00_7914,
		obj_t BgL_use1346z00_7915, obj_t BgL_in1347z00_7916,
		obj_t BgL_out1348z00_7917)
	{
		{	/* Liveness/types.scm 107 */
			{	/* Liveness/types.scm 107 */
				BgL_vallocz00_bglt BgL_new1755z00_9391;

				{	/* Liveness/types.scm 107 */
					BgL_vallocz00_bglt BgL_tmp1753z00_9392;
					BgL_valloczf2livenesszf2_bglt BgL_wide1754z00_9393;

					{
						BgL_vallocz00_bglt BgL_auxz00_15866;

						{	/* Liveness/types.scm 107 */
							BgL_vallocz00_bglt BgL_new1752z00_9394;

							BgL_new1752z00_9394 =
								((BgL_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vallocz00_bgl))));
							{	/* Liveness/types.scm 107 */
								long BgL_arg2719z00_9395;

								BgL_arg2719z00_9395 = BGL_CLASS_NUM(BGl_vallocz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1752z00_9394),
									BgL_arg2719z00_9395);
							}
							{	/* Liveness/types.scm 107 */
								BgL_objectz00_bglt BgL_tmpz00_15871;

								BgL_tmpz00_15871 = ((BgL_objectz00_bglt) BgL_new1752z00_9394);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15871, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1752z00_9394);
							BgL_auxz00_15866 = BgL_new1752z00_9394;
						}
						BgL_tmp1753z00_9392 = ((BgL_vallocz00_bglt) BgL_auxz00_15866);
					}
					BgL_wide1754z00_9393 =
						((BgL_valloczf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_valloczf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 107 */
						obj_t BgL_auxz00_15879;
						BgL_objectz00_bglt BgL_tmpz00_15877;

						BgL_auxz00_15879 = ((obj_t) BgL_wide1754z00_9393);
						BgL_tmpz00_15877 = ((BgL_objectz00_bglt) BgL_tmp1753z00_9392);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_15877, BgL_auxz00_15879);
					}
					((BgL_objectz00_bglt) BgL_tmp1753z00_9392);
					{	/* Liveness/types.scm 107 */
						long BgL_arg2717z00_9396;

						BgL_arg2717z00_9396 =
							BGL_CLASS_NUM(BGl_valloczf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1753z00_9392), BgL_arg2717z00_9396);
					}
					BgL_new1755z00_9391 = ((BgL_vallocz00_bglt) BgL_tmp1753z00_9392);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1755z00_9391)))->BgL_locz00) =
					((obj_t) BgL_loc1336z00_7905), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1755z00_9391)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1337z00_7906)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1755z00_9391)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1338zd2_7907), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1755z00_9391)))->BgL_keyz00) =
					((obj_t) BgL_key1339z00_7908), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1755z00_9391)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21340za2_7909)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1755z00_9391)))->BgL_effectz00) =
					((obj_t) BgL_effect1341z00_7910), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1755z00_9391)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1342zd2_7911)), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1755z00_9391)))->BgL_ftypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1343z00_7912)),
					BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1755z00_9391)))->BgL_otypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1344z00_7913)),
					BUNSPEC);
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_15910;

					{
						obj_t BgL_auxz00_15911;

						{	/* Liveness/types.scm 107 */
							BgL_objectz00_bglt BgL_tmpz00_15912;

							BgL_tmpz00_15912 = ((BgL_objectz00_bglt) BgL_new1755z00_9391);
							BgL_auxz00_15911 = BGL_OBJECT_WIDENING(BgL_tmpz00_15912);
						}
						BgL_auxz00_15910 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15911);
					}
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15910))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1345z00_7914)), BUNSPEC);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_15918;

					{
						obj_t BgL_auxz00_15919;

						{	/* Liveness/types.scm 107 */
							BgL_objectz00_bglt BgL_tmpz00_15920;

							BgL_tmpz00_15920 = ((BgL_objectz00_bglt) BgL_new1755z00_9391);
							BgL_auxz00_15919 = BGL_OBJECT_WIDENING(BgL_tmpz00_15920);
						}
						BgL_auxz00_15918 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15919);
					}
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15918))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1346z00_7915)), BUNSPEC);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_15926;

					{
						obj_t BgL_auxz00_15927;

						{	/* Liveness/types.scm 107 */
							BgL_objectz00_bglt BgL_tmpz00_15928;

							BgL_tmpz00_15928 = ((BgL_objectz00_bglt) BgL_new1755z00_9391);
							BgL_auxz00_15927 = BGL_OBJECT_WIDENING(BgL_tmpz00_15928);
						}
						BgL_auxz00_15926 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15927);
					}
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15926))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1347z00_7916)), BUNSPEC);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_15934;

					{
						obj_t BgL_auxz00_15935;

						{	/* Liveness/types.scm 107 */
							BgL_objectz00_bglt BgL_tmpz00_15936;

							BgL_tmpz00_15936 = ((BgL_objectz00_bglt) BgL_new1755z00_9391);
							BgL_auxz00_15935 = BGL_OBJECT_WIDENING(BgL_tmpz00_15936);
						}
						BgL_auxz00_15934 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15935);
					}
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15934))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1348z00_7917)), BUNSPEC);
				}
				return BgL_new1755z00_9391;
			}
		}

	}



/* &<@anonymous:2758> */
	obj_t BGl_z62zc3z04anonymousza32758ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7918)
	{
		{	/* Liveness/types.scm 107 */
			return BNIL;
		}

	}



/* &lambda2757 */
	obj_t BGl_z62lambda2757z62zzliveness_typesz00(obj_t BgL_envz00_7919,
		obj_t BgL_oz00_7920, obj_t BgL_vz00_7921)
	{
		{	/* Liveness/types.scm 107 */
			{
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_15942;

				{
					obj_t BgL_auxz00_15943;

					{	/* Liveness/types.scm 107 */
						BgL_objectz00_bglt BgL_tmpz00_15944;

						BgL_tmpz00_15944 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_7920));
						BgL_auxz00_15943 = BGL_OBJECT_WIDENING(BgL_tmpz00_15944);
					}
					BgL_auxz00_15942 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15943);
				}
				return
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15942))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7921)), BUNSPEC);
			}
		}

	}



/* &lambda2756 */
	obj_t BGl_z62lambda2756z62zzliveness_typesz00(obj_t BgL_envz00_7922,
		obj_t BgL_oz00_7923)
	{
		{	/* Liveness/types.scm 107 */
			{
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_15951;

				{
					obj_t BgL_auxz00_15952;

					{	/* Liveness/types.scm 107 */
						BgL_objectz00_bglt BgL_tmpz00_15953;

						BgL_tmpz00_15953 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_7923));
						BgL_auxz00_15952 = BGL_OBJECT_WIDENING(BgL_tmpz00_15953);
					}
					BgL_auxz00_15951 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15952);
				}
				return
					(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15951))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2750> */
	obj_t BGl_z62zc3z04anonymousza32750ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7924)
	{
		{	/* Liveness/types.scm 107 */
			return BNIL;
		}

	}



/* &lambda2749 */
	obj_t BGl_z62lambda2749z62zzliveness_typesz00(obj_t BgL_envz00_7925,
		obj_t BgL_oz00_7926, obj_t BgL_vz00_7927)
	{
		{	/* Liveness/types.scm 107 */
			{
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_15959;

				{
					obj_t BgL_auxz00_15960;

					{	/* Liveness/types.scm 107 */
						BgL_objectz00_bglt BgL_tmpz00_15961;

						BgL_tmpz00_15961 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_7926));
						BgL_auxz00_15960 = BGL_OBJECT_WIDENING(BgL_tmpz00_15961);
					}
					BgL_auxz00_15959 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15960);
				}
				return
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15959))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7927)), BUNSPEC);
			}
		}

	}



/* &lambda2748 */
	obj_t BGl_z62lambda2748z62zzliveness_typesz00(obj_t BgL_envz00_7928,
		obj_t BgL_oz00_7929)
	{
		{	/* Liveness/types.scm 107 */
			{
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_15968;

				{
					obj_t BgL_auxz00_15969;

					{	/* Liveness/types.scm 107 */
						BgL_objectz00_bglt BgL_tmpz00_15970;

						BgL_tmpz00_15970 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_7929));
						BgL_auxz00_15969 = BGL_OBJECT_WIDENING(BgL_tmpz00_15970);
					}
					BgL_auxz00_15968 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15969);
				}
				return
					(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15968))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2742> */
	obj_t BGl_z62zc3z04anonymousza32742ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7930)
	{
		{	/* Liveness/types.scm 107 */
			return BNIL;
		}

	}



/* &lambda2741 */
	obj_t BGl_z62lambda2741z62zzliveness_typesz00(obj_t BgL_envz00_7931,
		obj_t BgL_oz00_7932, obj_t BgL_vz00_7933)
	{
		{	/* Liveness/types.scm 107 */
			{
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_15976;

				{
					obj_t BgL_auxz00_15977;

					{	/* Liveness/types.scm 107 */
						BgL_objectz00_bglt BgL_tmpz00_15978;

						BgL_tmpz00_15978 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_7932));
						BgL_auxz00_15977 = BGL_OBJECT_WIDENING(BgL_tmpz00_15978);
					}
					BgL_auxz00_15976 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15977);
				}
				return
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15976))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7933)), BUNSPEC);
			}
		}

	}



/* &lambda2740 */
	obj_t BGl_z62lambda2740z62zzliveness_typesz00(obj_t BgL_envz00_7934,
		obj_t BgL_oz00_7935)
	{
		{	/* Liveness/types.scm 107 */
			{
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_15985;

				{
					obj_t BgL_auxz00_15986;

					{	/* Liveness/types.scm 107 */
						BgL_objectz00_bglt BgL_tmpz00_15987;

						BgL_tmpz00_15987 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_7935));
						BgL_auxz00_15986 = BGL_OBJECT_WIDENING(BgL_tmpz00_15987);
					}
					BgL_auxz00_15985 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15986);
				}
				return
					(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15985))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2734> */
	obj_t BGl_z62zc3z04anonymousza32734ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7936)
	{
		{	/* Liveness/types.scm 107 */
			return BNIL;
		}

	}



/* &lambda2733 */
	obj_t BGl_z62lambda2733z62zzliveness_typesz00(obj_t BgL_envz00_7937,
		obj_t BgL_oz00_7938, obj_t BgL_vz00_7939)
	{
		{	/* Liveness/types.scm 107 */
			{
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_15993;

				{
					obj_t BgL_auxz00_15994;

					{	/* Liveness/types.scm 107 */
						BgL_objectz00_bglt BgL_tmpz00_15995;

						BgL_tmpz00_15995 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_7938));
						BgL_auxz00_15994 = BGL_OBJECT_WIDENING(BgL_tmpz00_15995);
					}
					BgL_auxz00_15993 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_15994);
				}
				return
					((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_15993))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7939)), BUNSPEC);
			}
		}

	}



/* &lambda2732 */
	obj_t BGl_z62lambda2732z62zzliveness_typesz00(obj_t BgL_envz00_7940,
		obj_t BgL_oz00_7941)
	{
		{	/* Liveness/types.scm 107 */
			{
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_16002;

				{
					obj_t BgL_auxz00_16003;

					{	/* Liveness/types.scm 107 */
						BgL_objectz00_bglt BgL_tmpz00_16004;

						BgL_tmpz00_16004 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_7941));
						BgL_auxz00_16003 = BGL_OBJECT_WIDENING(BgL_tmpz00_16004);
					}
					BgL_auxz00_16002 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_16003);
				}
				return
					(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_16002))->
					BgL_defz00);
			}
		}

	}



/* &lambda2674 */
	BgL_newz00_bglt BGl_z62lambda2674z62zzliveness_typesz00(obj_t BgL_envz00_7942,
		obj_t BgL_o1334z00_7943)
	{
		{	/* Liveness/types.scm 101 */
			{	/* Liveness/types.scm 101 */
				long BgL_arg2675z00_9410;

				{	/* Liveness/types.scm 101 */
					obj_t BgL_arg2676z00_9411;

					{	/* Liveness/types.scm 101 */
						obj_t BgL_arg2678z00_9412;

						{	/* Liveness/types.scm 101 */
							obj_t BgL_arg1815z00_9413;
							long BgL_arg1816z00_9414;

							BgL_arg1815z00_9413 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 101 */
								long BgL_arg1817z00_9415;

								BgL_arg1817z00_9415 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_o1334z00_7943)));
								BgL_arg1816z00_9414 = (BgL_arg1817z00_9415 - OBJECT_TYPE);
							}
							BgL_arg2678z00_9412 =
								VECTOR_REF(BgL_arg1815z00_9413, BgL_arg1816z00_9414);
						}
						BgL_arg2676z00_9411 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2678z00_9412);
					}
					{	/* Liveness/types.scm 101 */
						obj_t BgL_tmpz00_16017;

						BgL_tmpz00_16017 = ((obj_t) BgL_arg2676z00_9411);
						BgL_arg2675z00_9410 = BGL_CLASS_NUM(BgL_tmpz00_16017);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_newz00_bglt) BgL_o1334z00_7943)), BgL_arg2675z00_9410);
			}
			{	/* Liveness/types.scm 101 */
				BgL_objectz00_bglt BgL_tmpz00_16023;

				BgL_tmpz00_16023 =
					((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_o1334z00_7943));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16023, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_o1334z00_7943));
			return ((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_o1334z00_7943));
		}

	}



/* &<@anonymous:2673> */
	obj_t BGl_z62zc3z04anonymousza32673ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7944, obj_t BgL_new1333z00_7945)
	{
		{	/* Liveness/types.scm 101 */
			{
				BgL_newz00_bglt BgL_auxz00_16031;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_newz00_bglt) BgL_new1333z00_7945))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_16035;

					{	/* Liveness/types.scm 101 */
						obj_t BgL_classz00_9417;

						BgL_classz00_9417 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 101 */
							obj_t BgL__ortest_1117z00_9418;

							BgL__ortest_1117z00_9418 = BGL_CLASS_NIL(BgL_classz00_9417);
							if (CBOOL(BgL__ortest_1117z00_9418))
								{	/* Liveness/types.scm 101 */
									BgL_auxz00_16035 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9418);
								}
							else
								{	/* Liveness/types.scm 101 */
									BgL_auxz00_16035 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9417));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_newz00_bglt) BgL_new1333z00_7945))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_16035), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_newz00_bglt) BgL_new1333z00_7945))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_newz00_bglt)
										BgL_new1333z00_7945))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) ((BgL_newz00_bglt)
										BgL_new1333z00_7945))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) ((BgL_newz00_bglt)
										BgL_new1333z00_7945))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_newz00_bglt)
										BgL_new1333z00_7945))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				((((BgL_newz00_bglt) COBJECT(((BgL_newz00_bglt) ((BgL_newz00_bglt)
										BgL_new1333z00_7945))))->BgL_argszd2typezd2) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_16063;

					{
						obj_t BgL_auxz00_16064;

						{	/* Liveness/types.scm 101 */
							BgL_objectz00_bglt BgL_tmpz00_16065;

							BgL_tmpz00_16065 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_new1333z00_7945));
							BgL_auxz00_16064 = BGL_OBJECT_WIDENING(BgL_tmpz00_16065);
						}
						BgL_auxz00_16063 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16064);
					}
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16063))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_16071;

					{
						obj_t BgL_auxz00_16072;

						{	/* Liveness/types.scm 101 */
							BgL_objectz00_bglt BgL_tmpz00_16073;

							BgL_tmpz00_16073 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_new1333z00_7945));
							BgL_auxz00_16072 = BGL_OBJECT_WIDENING(BgL_tmpz00_16073);
						}
						BgL_auxz00_16071 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16072);
					}
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16071))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_16079;

					{
						obj_t BgL_auxz00_16080;

						{	/* Liveness/types.scm 101 */
							BgL_objectz00_bglt BgL_tmpz00_16081;

							BgL_tmpz00_16081 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_new1333z00_7945));
							BgL_auxz00_16080 = BGL_OBJECT_WIDENING(BgL_tmpz00_16081);
						}
						BgL_auxz00_16079 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16080);
					}
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16079))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_16087;

					{
						obj_t BgL_auxz00_16088;

						{	/* Liveness/types.scm 101 */
							BgL_objectz00_bglt BgL_tmpz00_16089;

							BgL_tmpz00_16089 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_new1333z00_7945));
							BgL_auxz00_16088 = BGL_OBJECT_WIDENING(BgL_tmpz00_16089);
						}
						BgL_auxz00_16087 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16088);
					}
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16087))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_16031 = ((BgL_newz00_bglt) BgL_new1333z00_7945);
				return ((obj_t) BgL_auxz00_16031);
			}
		}

	}



/* &lambda2671 */
	BgL_newz00_bglt BGl_z62lambda2671z62zzliveness_typesz00(obj_t BgL_envz00_7946,
		obj_t BgL_o1330z00_7947)
	{
		{	/* Liveness/types.scm 101 */
			{	/* Liveness/types.scm 101 */
				BgL_newzf2livenesszf2_bglt BgL_wide1332z00_9420;

				BgL_wide1332z00_9420 =
					((BgL_newzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_newzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 101 */
					obj_t BgL_auxz00_16102;
					BgL_objectz00_bglt BgL_tmpz00_16098;

					BgL_auxz00_16102 = ((obj_t) BgL_wide1332z00_9420);
					BgL_tmpz00_16098 =
						((BgL_objectz00_bglt)
						((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_o1330z00_7947)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16098, BgL_auxz00_16102);
				}
				((BgL_objectz00_bglt)
					((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_o1330z00_7947)));
				{	/* Liveness/types.scm 101 */
					long BgL_arg2672z00_9421;

					BgL_arg2672z00_9421 =
						BGL_CLASS_NUM(BGl_newzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_newz00_bglt)
								((BgL_newz00_bglt) BgL_o1330z00_7947))), BgL_arg2672z00_9421);
				}
				return
					((BgL_newz00_bglt)
					((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_o1330z00_7947)));
			}
		}

	}



/* &lambda2668 */
	BgL_newz00_bglt BGl_z62lambda2668z62zzliveness_typesz00(obj_t BgL_envz00_7948,
		obj_t BgL_loc1318z00_7949, obj_t BgL_type1319z00_7950,
		obj_t BgL_sidezd2effect1320zd2_7951, obj_t BgL_key1321z00_7952,
		obj_t BgL_exprza21322za2_7953, obj_t BgL_effect1323z00_7954,
		obj_t BgL_czd2format1324zd2_7955, obj_t BgL_argszd2type1325zd2_7956,
		obj_t BgL_def1326z00_7957, obj_t BgL_use1327z00_7958,
		obj_t BgL_in1328z00_7959, obj_t BgL_out1329z00_7960)
	{
		{	/* Liveness/types.scm 101 */
			{	/* Liveness/types.scm 101 */
				BgL_newz00_bglt BgL_new1750z00_9430;

				{	/* Liveness/types.scm 101 */
					BgL_newz00_bglt BgL_tmp1748z00_9431;
					BgL_newzf2livenesszf2_bglt BgL_wide1749z00_9432;

					{
						BgL_newz00_bglt BgL_auxz00_16116;

						{	/* Liveness/types.scm 101 */
							BgL_newz00_bglt BgL_new1747z00_9433;

							BgL_new1747z00_9433 =
								((BgL_newz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_newz00_bgl))));
							{	/* Liveness/types.scm 101 */
								long BgL_arg2670z00_9434;

								BgL_arg2670z00_9434 = BGL_CLASS_NUM(BGl_newz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1747z00_9433),
									BgL_arg2670z00_9434);
							}
							{	/* Liveness/types.scm 101 */
								BgL_objectz00_bglt BgL_tmpz00_16121;

								BgL_tmpz00_16121 = ((BgL_objectz00_bglt) BgL_new1747z00_9433);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16121, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1747z00_9433);
							BgL_auxz00_16116 = BgL_new1747z00_9433;
						}
						BgL_tmp1748z00_9431 = ((BgL_newz00_bglt) BgL_auxz00_16116);
					}
					BgL_wide1749z00_9432 =
						((BgL_newzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_newzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 101 */
						obj_t BgL_auxz00_16129;
						BgL_objectz00_bglt BgL_tmpz00_16127;

						BgL_auxz00_16129 = ((obj_t) BgL_wide1749z00_9432);
						BgL_tmpz00_16127 = ((BgL_objectz00_bglt) BgL_tmp1748z00_9431);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16127, BgL_auxz00_16129);
					}
					((BgL_objectz00_bglt) BgL_tmp1748z00_9431);
					{	/* Liveness/types.scm 101 */
						long BgL_arg2669z00_9435;

						BgL_arg2669z00_9435 =
							BGL_CLASS_NUM(BGl_newzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1748z00_9431), BgL_arg2669z00_9435);
					}
					BgL_new1750z00_9430 = ((BgL_newz00_bglt) BgL_tmp1748z00_9431);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1750z00_9430)))->BgL_locz00) =
					((obj_t) BgL_loc1318z00_7949), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1750z00_9430)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1319z00_7950)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1750z00_9430)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1320zd2_7951), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1750z00_9430)))->BgL_keyz00) =
					((obj_t) BgL_key1321z00_7952), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1750z00_9430)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21322za2_7953)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1750z00_9430)))->BgL_effectz00) =
					((obj_t) BgL_effect1323z00_7954), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1750z00_9430)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1324zd2_7955)), BUNSPEC);
				((((BgL_newz00_bglt) COBJECT(((BgL_newz00_bglt) BgL_new1750z00_9430)))->
						BgL_argszd2typezd2) =
					((obj_t) ((obj_t) BgL_argszd2type1325zd2_7956)), BUNSPEC);
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_16157;

					{
						obj_t BgL_auxz00_16158;

						{	/* Liveness/types.scm 101 */
							BgL_objectz00_bglt BgL_tmpz00_16159;

							BgL_tmpz00_16159 = ((BgL_objectz00_bglt) BgL_new1750z00_9430);
							BgL_auxz00_16158 = BGL_OBJECT_WIDENING(BgL_tmpz00_16159);
						}
						BgL_auxz00_16157 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16158);
					}
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16157))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1326z00_7957)), BUNSPEC);
				}
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_16165;

					{
						obj_t BgL_auxz00_16166;

						{	/* Liveness/types.scm 101 */
							BgL_objectz00_bglt BgL_tmpz00_16167;

							BgL_tmpz00_16167 = ((BgL_objectz00_bglt) BgL_new1750z00_9430);
							BgL_auxz00_16166 = BGL_OBJECT_WIDENING(BgL_tmpz00_16167);
						}
						BgL_auxz00_16165 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16166);
					}
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16165))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1327z00_7958)), BUNSPEC);
				}
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_16173;

					{
						obj_t BgL_auxz00_16174;

						{	/* Liveness/types.scm 101 */
							BgL_objectz00_bglt BgL_tmpz00_16175;

							BgL_tmpz00_16175 = ((BgL_objectz00_bglt) BgL_new1750z00_9430);
							BgL_auxz00_16174 = BGL_OBJECT_WIDENING(BgL_tmpz00_16175);
						}
						BgL_auxz00_16173 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16174);
					}
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16173))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1328z00_7959)), BUNSPEC);
				}
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_16181;

					{
						obj_t BgL_auxz00_16182;

						{	/* Liveness/types.scm 101 */
							BgL_objectz00_bglt BgL_tmpz00_16183;

							BgL_tmpz00_16183 = ((BgL_objectz00_bglt) BgL_new1750z00_9430);
							BgL_auxz00_16182 = BGL_OBJECT_WIDENING(BgL_tmpz00_16183);
						}
						BgL_auxz00_16181 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16182);
					}
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16181))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1329z00_7960)), BUNSPEC);
				}
				return BgL_new1750z00_9430;
			}
		}

	}



/* &<@anonymous:2708> */
	obj_t BGl_z62zc3z04anonymousza32708ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7961)
	{
		{	/* Liveness/types.scm 101 */
			return BNIL;
		}

	}



/* &lambda2707 */
	obj_t BGl_z62lambda2707z62zzliveness_typesz00(obj_t BgL_envz00_7962,
		obj_t BgL_oz00_7963, obj_t BgL_vz00_7964)
	{
		{	/* Liveness/types.scm 101 */
			{
				BgL_newzf2livenesszf2_bglt BgL_auxz00_16189;

				{
					obj_t BgL_auxz00_16190;

					{	/* Liveness/types.scm 101 */
						BgL_objectz00_bglt BgL_tmpz00_16191;

						BgL_tmpz00_16191 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_7963));
						BgL_auxz00_16190 = BGL_OBJECT_WIDENING(BgL_tmpz00_16191);
					}
					BgL_auxz00_16189 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16190);
				}
				return
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16189))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_7964)), BUNSPEC);
			}
		}

	}



/* &lambda2706 */
	obj_t BGl_z62lambda2706z62zzliveness_typesz00(obj_t BgL_envz00_7965,
		obj_t BgL_oz00_7966)
	{
		{	/* Liveness/types.scm 101 */
			{
				BgL_newzf2livenesszf2_bglt BgL_auxz00_16198;

				{
					obj_t BgL_auxz00_16199;

					{	/* Liveness/types.scm 101 */
						BgL_objectz00_bglt BgL_tmpz00_16200;

						BgL_tmpz00_16200 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_7966));
						BgL_auxz00_16199 = BGL_OBJECT_WIDENING(BgL_tmpz00_16200);
					}
					BgL_auxz00_16198 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16199);
				}
				return
					(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16198))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2701> */
	obj_t BGl_z62zc3z04anonymousza32701ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7967)
	{
		{	/* Liveness/types.scm 101 */
			return BNIL;
		}

	}



/* &lambda2700 */
	obj_t BGl_z62lambda2700z62zzliveness_typesz00(obj_t BgL_envz00_7968,
		obj_t BgL_oz00_7969, obj_t BgL_vz00_7970)
	{
		{	/* Liveness/types.scm 101 */
			{
				BgL_newzf2livenesszf2_bglt BgL_auxz00_16206;

				{
					obj_t BgL_auxz00_16207;

					{	/* Liveness/types.scm 101 */
						BgL_objectz00_bglt BgL_tmpz00_16208;

						BgL_tmpz00_16208 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_7969));
						BgL_auxz00_16207 = BGL_OBJECT_WIDENING(BgL_tmpz00_16208);
					}
					BgL_auxz00_16206 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16207);
				}
				return
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16206))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_7970)), BUNSPEC);
			}
		}

	}



/* &lambda2699 */
	obj_t BGl_z62lambda2699z62zzliveness_typesz00(obj_t BgL_envz00_7971,
		obj_t BgL_oz00_7972)
	{
		{	/* Liveness/types.scm 101 */
			{
				BgL_newzf2livenesszf2_bglt BgL_auxz00_16215;

				{
					obj_t BgL_auxz00_16216;

					{	/* Liveness/types.scm 101 */
						BgL_objectz00_bglt BgL_tmpz00_16217;

						BgL_tmpz00_16217 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_7972));
						BgL_auxz00_16216 = BGL_OBJECT_WIDENING(BgL_tmpz00_16217);
					}
					BgL_auxz00_16215 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16216);
				}
				return
					(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16215))->BgL_inz00);
			}
		}

	}



/* &<@anonymous:2694> */
	obj_t BGl_z62zc3z04anonymousza32694ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7973)
	{
		{	/* Liveness/types.scm 101 */
			return BNIL;
		}

	}



/* &lambda2693 */
	obj_t BGl_z62lambda2693z62zzliveness_typesz00(obj_t BgL_envz00_7974,
		obj_t BgL_oz00_7975, obj_t BgL_vz00_7976)
	{
		{	/* Liveness/types.scm 101 */
			{
				BgL_newzf2livenesszf2_bglt BgL_auxz00_16223;

				{
					obj_t BgL_auxz00_16224;

					{	/* Liveness/types.scm 101 */
						BgL_objectz00_bglt BgL_tmpz00_16225;

						BgL_tmpz00_16225 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_7975));
						BgL_auxz00_16224 = BGL_OBJECT_WIDENING(BgL_tmpz00_16225);
					}
					BgL_auxz00_16223 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16224);
				}
				return
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16223))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_7976)), BUNSPEC);
			}
		}

	}



/* &lambda2692 */
	obj_t BGl_z62lambda2692z62zzliveness_typesz00(obj_t BgL_envz00_7977,
		obj_t BgL_oz00_7978)
	{
		{	/* Liveness/types.scm 101 */
			{
				BgL_newzf2livenesszf2_bglt BgL_auxz00_16232;

				{
					obj_t BgL_auxz00_16233;

					{	/* Liveness/types.scm 101 */
						BgL_objectz00_bglt BgL_tmpz00_16234;

						BgL_tmpz00_16234 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_7978));
						BgL_auxz00_16233 = BGL_OBJECT_WIDENING(BgL_tmpz00_16234);
					}
					BgL_auxz00_16232 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16233);
				}
				return
					(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16232))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2687> */
	obj_t BGl_z62zc3z04anonymousza32687ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7979)
	{
		{	/* Liveness/types.scm 101 */
			return BNIL;
		}

	}



/* &lambda2686 */
	obj_t BGl_z62lambda2686z62zzliveness_typesz00(obj_t BgL_envz00_7980,
		obj_t BgL_oz00_7981, obj_t BgL_vz00_7982)
	{
		{	/* Liveness/types.scm 101 */
			{
				BgL_newzf2livenesszf2_bglt BgL_auxz00_16240;

				{
					obj_t BgL_auxz00_16241;

					{	/* Liveness/types.scm 101 */
						BgL_objectz00_bglt BgL_tmpz00_16242;

						BgL_tmpz00_16242 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_7981));
						BgL_auxz00_16241 = BGL_OBJECT_WIDENING(BgL_tmpz00_16242);
					}
					BgL_auxz00_16240 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16241);
				}
				return
					((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16240))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_7982)), BUNSPEC);
			}
		}

	}



/* &lambda2685 */
	obj_t BGl_z62lambda2685z62zzliveness_typesz00(obj_t BgL_envz00_7983,
		obj_t BgL_oz00_7984)
	{
		{	/* Liveness/types.scm 101 */
			{
				BgL_newzf2livenesszf2_bglt BgL_auxz00_16249;

				{
					obj_t BgL_auxz00_16250;

					{	/* Liveness/types.scm 101 */
						BgL_objectz00_bglt BgL_tmpz00_16251;

						BgL_tmpz00_16251 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_7984));
						BgL_auxz00_16250 = BGL_OBJECT_WIDENING(BgL_tmpz00_16251);
					}
					BgL_auxz00_16249 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_16250);
				}
				return
					(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16249))->
					BgL_defz00);
			}
		}

	}



/* &lambda2625 */
	BgL_wideningz00_bglt BGl_z62lambda2625z62zzliveness_typesz00(obj_t
		BgL_envz00_7985, obj_t BgL_o1316z00_7986)
	{
		{	/* Liveness/types.scm 95 */
			{	/* Liveness/types.scm 95 */
				long BgL_arg2626z00_9449;

				{	/* Liveness/types.scm 95 */
					obj_t BgL_arg2627z00_9450;

					{	/* Liveness/types.scm 95 */
						obj_t BgL_arg2628z00_9451;

						{	/* Liveness/types.scm 95 */
							obj_t BgL_arg1815z00_9452;
							long BgL_arg1816z00_9453;

							BgL_arg1815z00_9452 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 95 */
								long BgL_arg1817z00_9454;

								BgL_arg1817z00_9454 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_wideningz00_bglt) BgL_o1316z00_7986)));
								BgL_arg1816z00_9453 = (BgL_arg1817z00_9454 - OBJECT_TYPE);
							}
							BgL_arg2628z00_9451 =
								VECTOR_REF(BgL_arg1815z00_9452, BgL_arg1816z00_9453);
						}
						BgL_arg2627z00_9450 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2628z00_9451);
					}
					{	/* Liveness/types.scm 95 */
						obj_t BgL_tmpz00_16264;

						BgL_tmpz00_16264 = ((obj_t) BgL_arg2627z00_9450);
						BgL_arg2626z00_9449 = BGL_CLASS_NUM(BgL_tmpz00_16264);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_wideningz00_bglt) BgL_o1316z00_7986)), BgL_arg2626z00_9449);
			}
			{	/* Liveness/types.scm 95 */
				BgL_objectz00_bglt BgL_tmpz00_16270;

				BgL_tmpz00_16270 =
					((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_o1316z00_7986));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16270, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_o1316z00_7986));
			return
				((BgL_wideningz00_bglt) ((BgL_wideningz00_bglt) BgL_o1316z00_7986));
		}

	}



/* &<@anonymous:2624> */
	obj_t BGl_z62zc3z04anonymousza32624ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_7987, obj_t BgL_new1315z00_7988)
	{
		{	/* Liveness/types.scm 95 */
			{
				BgL_wideningz00_bglt BgL_auxz00_16278;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_wideningz00_bglt) BgL_new1315z00_7988))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_16282;

					{	/* Liveness/types.scm 95 */
						obj_t BgL_classz00_9456;

						BgL_classz00_9456 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 95 */
							obj_t BgL__ortest_1117z00_9457;

							BgL__ortest_1117z00_9457 = BGL_CLASS_NIL(BgL_classz00_9456);
							if (CBOOL(BgL__ortest_1117z00_9457))
								{	/* Liveness/types.scm 95 */
									BgL_auxz00_16282 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9457);
								}
							else
								{	/* Liveness/types.scm 95 */
									BgL_auxz00_16282 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9456));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_wideningz00_bglt) BgL_new1315z00_7988))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_16282), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_wideningz00_bglt) BgL_new1315z00_7988))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_wideningz00_bglt)
										BgL_new1315z00_7988))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_wideningz00_bglt)
										BgL_new1315z00_7988))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_wideningz00_bglt)
										BgL_new1315z00_7988))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_wideningz00_bglt)
										BgL_new1315z00_7988))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_16307;

					{	/* Liveness/types.scm 95 */
						obj_t BgL_classz00_9458;

						BgL_classz00_9458 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 95 */
							obj_t BgL__ortest_1117z00_9459;

							BgL__ortest_1117z00_9459 = BGL_CLASS_NIL(BgL_classz00_9458);
							if (CBOOL(BgL__ortest_1117z00_9459))
								{	/* Liveness/types.scm 95 */
									BgL_auxz00_16307 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9459);
								}
							else
								{	/* Liveness/types.scm 95 */
									BgL_auxz00_16307 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9458));
								}
						}
					}
					((((BgL_wideningz00_bglt) COBJECT(
									((BgL_wideningz00_bglt)
										((BgL_wideningz00_bglt) BgL_new1315z00_7988))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_16307), BUNSPEC);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16317;

					{
						obj_t BgL_auxz00_16318;

						{	/* Liveness/types.scm 95 */
							BgL_objectz00_bglt BgL_tmpz00_16319;

							BgL_tmpz00_16319 =
								((BgL_objectz00_bglt)
								((BgL_wideningz00_bglt) BgL_new1315z00_7988));
							BgL_auxz00_16318 = BGL_OBJECT_WIDENING(BgL_tmpz00_16319);
						}
						BgL_auxz00_16317 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16318);
					}
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16317))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16325;

					{
						obj_t BgL_auxz00_16326;

						{	/* Liveness/types.scm 95 */
							BgL_objectz00_bglt BgL_tmpz00_16327;

							BgL_tmpz00_16327 =
								((BgL_objectz00_bglt)
								((BgL_wideningz00_bglt) BgL_new1315z00_7988));
							BgL_auxz00_16326 = BGL_OBJECT_WIDENING(BgL_tmpz00_16327);
						}
						BgL_auxz00_16325 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16326);
					}
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16325))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16333;

					{
						obj_t BgL_auxz00_16334;

						{	/* Liveness/types.scm 95 */
							BgL_objectz00_bglt BgL_tmpz00_16335;

							BgL_tmpz00_16335 =
								((BgL_objectz00_bglt)
								((BgL_wideningz00_bglt) BgL_new1315z00_7988));
							BgL_auxz00_16334 = BGL_OBJECT_WIDENING(BgL_tmpz00_16335);
						}
						BgL_auxz00_16333 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16334);
					}
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16333))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16341;

					{
						obj_t BgL_auxz00_16342;

						{	/* Liveness/types.scm 95 */
							BgL_objectz00_bglt BgL_tmpz00_16343;

							BgL_tmpz00_16343 =
								((BgL_objectz00_bglt)
								((BgL_wideningz00_bglt) BgL_new1315z00_7988));
							BgL_auxz00_16342 = BGL_OBJECT_WIDENING(BgL_tmpz00_16343);
						}
						BgL_auxz00_16341 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16342);
					}
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16341))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_16278 = ((BgL_wideningz00_bglt) BgL_new1315z00_7988);
				return ((obj_t) BgL_auxz00_16278);
			}
		}

	}



/* &lambda2622 */
	BgL_wideningz00_bglt BGl_z62lambda2622z62zzliveness_typesz00(obj_t
		BgL_envz00_7989, obj_t BgL_o1312z00_7990)
	{
		{	/* Liveness/types.scm 95 */
			{	/* Liveness/types.scm 95 */
				BgL_wideningzf2livenesszf2_bglt BgL_wide1314z00_9461;

				BgL_wide1314z00_9461 =
					((BgL_wideningzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_wideningzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 95 */
					obj_t BgL_auxz00_16356;
					BgL_objectz00_bglt BgL_tmpz00_16352;

					BgL_auxz00_16356 = ((obj_t) BgL_wide1314z00_9461);
					BgL_tmpz00_16352 =
						((BgL_objectz00_bglt)
						((BgL_wideningz00_bglt)
							((BgL_wideningz00_bglt) BgL_o1312z00_7990)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16352, BgL_auxz00_16356);
				}
				((BgL_objectz00_bglt)
					((BgL_wideningz00_bglt) ((BgL_wideningz00_bglt) BgL_o1312z00_7990)));
				{	/* Liveness/types.scm 95 */
					long BgL_arg2623z00_9462;

					BgL_arg2623z00_9462 =
						BGL_CLASS_NUM(BGl_wideningzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_wideningz00_bglt)
								((BgL_wideningz00_bglt) BgL_o1312z00_7990))),
						BgL_arg2623z00_9462);
				}
				return
					((BgL_wideningz00_bglt)
					((BgL_wideningz00_bglt) ((BgL_wideningz00_bglt) BgL_o1312z00_7990)));
			}
		}

	}



/* &lambda2619 */
	BgL_wideningz00_bglt BGl_z62lambda2619z62zzliveness_typesz00(obj_t
		BgL_envz00_7991, obj_t BgL_loc1300z00_7992, obj_t BgL_type1301z00_7993,
		obj_t BgL_sidezd2effect1302zd2_7994, obj_t BgL_key1303z00_7995,
		obj_t BgL_exprza21304za2_7996, obj_t BgL_effect1305z00_7997,
		obj_t BgL_czd2format1306zd2_7998, obj_t BgL_otype1307z00_7999,
		obj_t BgL_def1308z00_8000, obj_t BgL_use1309z00_8001,
		obj_t BgL_in1310z00_8002, obj_t BgL_out1311z00_8003)
	{
		{	/* Liveness/types.scm 95 */
			{	/* Liveness/types.scm 95 */
				BgL_wideningz00_bglt BgL_new1745z00_9471;

				{	/* Liveness/types.scm 95 */
					BgL_wideningz00_bglt BgL_tmp1743z00_9472;
					BgL_wideningzf2livenesszf2_bglt BgL_wide1744z00_9473;

					{
						BgL_wideningz00_bglt BgL_auxz00_16370;

						{	/* Liveness/types.scm 95 */
							BgL_wideningz00_bglt BgL_new1742z00_9474;

							BgL_new1742z00_9474 =
								((BgL_wideningz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_wideningz00_bgl))));
							{	/* Liveness/types.scm 95 */
								long BgL_arg2621z00_9475;

								BgL_arg2621z00_9475 =
									BGL_CLASS_NUM(BGl_wideningz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1742z00_9474),
									BgL_arg2621z00_9475);
							}
							{	/* Liveness/types.scm 95 */
								BgL_objectz00_bglt BgL_tmpz00_16375;

								BgL_tmpz00_16375 = ((BgL_objectz00_bglt) BgL_new1742z00_9474);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16375, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1742z00_9474);
							BgL_auxz00_16370 = BgL_new1742z00_9474;
						}
						BgL_tmp1743z00_9472 = ((BgL_wideningz00_bglt) BgL_auxz00_16370);
					}
					BgL_wide1744z00_9473 =
						((BgL_wideningzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_wideningzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 95 */
						obj_t BgL_auxz00_16383;
						BgL_objectz00_bglt BgL_tmpz00_16381;

						BgL_auxz00_16383 = ((obj_t) BgL_wide1744z00_9473);
						BgL_tmpz00_16381 = ((BgL_objectz00_bglt) BgL_tmp1743z00_9472);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16381, BgL_auxz00_16383);
					}
					((BgL_objectz00_bglt) BgL_tmp1743z00_9472);
					{	/* Liveness/types.scm 95 */
						long BgL_arg2620z00_9476;

						BgL_arg2620z00_9476 =
							BGL_CLASS_NUM(BGl_wideningzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1743z00_9472), BgL_arg2620z00_9476);
					}
					BgL_new1745z00_9471 = ((BgL_wideningz00_bglt) BgL_tmp1743z00_9472);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1745z00_9471)))->BgL_locz00) =
					((obj_t) BgL_loc1300z00_7992), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1745z00_9471)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1301z00_7993)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1745z00_9471)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1302zd2_7994), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1745z00_9471)))->BgL_keyz00) =
					((obj_t) BgL_key1303z00_7995), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1745z00_9471)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21304za2_7996)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1745z00_9471)))->BgL_effectz00) =
					((obj_t) BgL_effect1305z00_7997), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1745z00_9471)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1306zd2_7998)), BUNSPEC);
				((((BgL_wideningz00_bglt) COBJECT(((BgL_wideningz00_bglt)
									BgL_new1745z00_9471)))->BgL_otypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1307z00_7999)),
					BUNSPEC);
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16411;

					{
						obj_t BgL_auxz00_16412;

						{	/* Liveness/types.scm 95 */
							BgL_objectz00_bglt BgL_tmpz00_16413;

							BgL_tmpz00_16413 = ((BgL_objectz00_bglt) BgL_new1745z00_9471);
							BgL_auxz00_16412 = BGL_OBJECT_WIDENING(BgL_tmpz00_16413);
						}
						BgL_auxz00_16411 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16412);
					}
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16411))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1308z00_8000)), BUNSPEC);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16419;

					{
						obj_t BgL_auxz00_16420;

						{	/* Liveness/types.scm 95 */
							BgL_objectz00_bglt BgL_tmpz00_16421;

							BgL_tmpz00_16421 = ((BgL_objectz00_bglt) BgL_new1745z00_9471);
							BgL_auxz00_16420 = BGL_OBJECT_WIDENING(BgL_tmpz00_16421);
						}
						BgL_auxz00_16419 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16420);
					}
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16419))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1309z00_8001)), BUNSPEC);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16427;

					{
						obj_t BgL_auxz00_16428;

						{	/* Liveness/types.scm 95 */
							BgL_objectz00_bglt BgL_tmpz00_16429;

							BgL_tmpz00_16429 = ((BgL_objectz00_bglt) BgL_new1745z00_9471);
							BgL_auxz00_16428 = BGL_OBJECT_WIDENING(BgL_tmpz00_16429);
						}
						BgL_auxz00_16427 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16428);
					}
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16427))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1310z00_8002)), BUNSPEC);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16435;

					{
						obj_t BgL_auxz00_16436;

						{	/* Liveness/types.scm 95 */
							BgL_objectz00_bglt BgL_tmpz00_16437;

							BgL_tmpz00_16437 = ((BgL_objectz00_bglt) BgL_new1745z00_9471);
							BgL_auxz00_16436 = BGL_OBJECT_WIDENING(BgL_tmpz00_16437);
						}
						BgL_auxz00_16435 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16436);
					}
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16435))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1311z00_8003)), BUNSPEC);
				}
				return BgL_new1745z00_9471;
			}
		}

	}



/* &<@anonymous:2659> */
	obj_t BGl_z62zc3z04anonymousza32659ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8004)
	{
		{	/* Liveness/types.scm 95 */
			return BNIL;
		}

	}



/* &lambda2658 */
	obj_t BGl_z62lambda2658z62zzliveness_typesz00(obj_t BgL_envz00_8005,
		obj_t BgL_oz00_8006, obj_t BgL_vz00_8007)
	{
		{	/* Liveness/types.scm 95 */
			{
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16443;

				{
					obj_t BgL_auxz00_16444;

					{	/* Liveness/types.scm 95 */
						BgL_objectz00_bglt BgL_tmpz00_16445;

						BgL_tmpz00_16445 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_oz00_8006));
						BgL_auxz00_16444 = BGL_OBJECT_WIDENING(BgL_tmpz00_16445);
					}
					BgL_auxz00_16443 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16444);
				}
				return
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16443))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8007)), BUNSPEC);
			}
		}

	}



/* &lambda2657 */
	obj_t BGl_z62lambda2657z62zzliveness_typesz00(obj_t BgL_envz00_8008,
		obj_t BgL_oz00_8009)
	{
		{	/* Liveness/types.scm 95 */
			{
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16452;

				{
					obj_t BgL_auxz00_16453;

					{	/* Liveness/types.scm 95 */
						BgL_objectz00_bglt BgL_tmpz00_16454;

						BgL_tmpz00_16454 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_oz00_8009));
						BgL_auxz00_16453 = BGL_OBJECT_WIDENING(BgL_tmpz00_16454);
					}
					BgL_auxz00_16452 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16453);
				}
				return
					(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16452))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2650> */
	obj_t BGl_z62zc3z04anonymousza32650ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8010)
	{
		{	/* Liveness/types.scm 95 */
			return BNIL;
		}

	}



/* &lambda2649 */
	obj_t BGl_z62lambda2649z62zzliveness_typesz00(obj_t BgL_envz00_8011,
		obj_t BgL_oz00_8012, obj_t BgL_vz00_8013)
	{
		{	/* Liveness/types.scm 95 */
			{
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16460;

				{
					obj_t BgL_auxz00_16461;

					{	/* Liveness/types.scm 95 */
						BgL_objectz00_bglt BgL_tmpz00_16462;

						BgL_tmpz00_16462 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_oz00_8012));
						BgL_auxz00_16461 = BGL_OBJECT_WIDENING(BgL_tmpz00_16462);
					}
					BgL_auxz00_16460 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16461);
				}
				return
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16460))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8013)), BUNSPEC);
			}
		}

	}



/* &lambda2648 */
	obj_t BGl_z62lambda2648z62zzliveness_typesz00(obj_t BgL_envz00_8014,
		obj_t BgL_oz00_8015)
	{
		{	/* Liveness/types.scm 95 */
			{
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16469;

				{
					obj_t BgL_auxz00_16470;

					{	/* Liveness/types.scm 95 */
						BgL_objectz00_bglt BgL_tmpz00_16471;

						BgL_tmpz00_16471 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_oz00_8015));
						BgL_auxz00_16470 = BGL_OBJECT_WIDENING(BgL_tmpz00_16471);
					}
					BgL_auxz00_16469 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16470);
				}
				return
					(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16469))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2642> */
	obj_t BGl_z62zc3z04anonymousza32642ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8016)
	{
		{	/* Liveness/types.scm 95 */
			return BNIL;
		}

	}



/* &lambda2641 */
	obj_t BGl_z62lambda2641z62zzliveness_typesz00(obj_t BgL_envz00_8017,
		obj_t BgL_oz00_8018, obj_t BgL_vz00_8019)
	{
		{	/* Liveness/types.scm 95 */
			{
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16477;

				{
					obj_t BgL_auxz00_16478;

					{	/* Liveness/types.scm 95 */
						BgL_objectz00_bglt BgL_tmpz00_16479;

						BgL_tmpz00_16479 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_oz00_8018));
						BgL_auxz00_16478 = BGL_OBJECT_WIDENING(BgL_tmpz00_16479);
					}
					BgL_auxz00_16477 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16478);
				}
				return
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16477))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8019)), BUNSPEC);
			}
		}

	}



/* &lambda2640 */
	obj_t BGl_z62lambda2640z62zzliveness_typesz00(obj_t BgL_envz00_8020,
		obj_t BgL_oz00_8021)
	{
		{	/* Liveness/types.scm 95 */
			{
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16486;

				{
					obj_t BgL_auxz00_16487;

					{	/* Liveness/types.scm 95 */
						BgL_objectz00_bglt BgL_tmpz00_16488;

						BgL_tmpz00_16488 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_oz00_8021));
						BgL_auxz00_16487 = BGL_OBJECT_WIDENING(BgL_tmpz00_16488);
					}
					BgL_auxz00_16486 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16487);
				}
				return
					(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16486))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2635> */
	obj_t BGl_z62zc3z04anonymousza32635ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8022)
	{
		{	/* Liveness/types.scm 95 */
			return BNIL;
		}

	}



/* &lambda2634 */
	obj_t BGl_z62lambda2634z62zzliveness_typesz00(obj_t BgL_envz00_8023,
		obj_t BgL_oz00_8024, obj_t BgL_vz00_8025)
	{
		{	/* Liveness/types.scm 95 */
			{
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16494;

				{
					obj_t BgL_auxz00_16495;

					{	/* Liveness/types.scm 95 */
						BgL_objectz00_bglt BgL_tmpz00_16496;

						BgL_tmpz00_16496 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_oz00_8024));
						BgL_auxz00_16495 = BGL_OBJECT_WIDENING(BgL_tmpz00_16496);
					}
					BgL_auxz00_16494 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16495);
				}
				return
					((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16494))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8025)), BUNSPEC);
			}
		}

	}



/* &lambda2633 */
	obj_t BGl_z62lambda2633z62zzliveness_typesz00(obj_t BgL_envz00_8026,
		obj_t BgL_oz00_8027)
	{
		{	/* Liveness/types.scm 95 */
			{
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_16503;

				{
					obj_t BgL_auxz00_16504;

					{	/* Liveness/types.scm 95 */
						BgL_objectz00_bglt BgL_tmpz00_16505;

						BgL_tmpz00_16505 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_oz00_8027));
						BgL_auxz00_16504 = BGL_OBJECT_WIDENING(BgL_tmpz00_16505);
					}
					BgL_auxz00_16503 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_16504);
				}
				return
					(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16503))->
					BgL_defz00);
			}
		}

	}



/* &lambda2565 */
	BgL_setfieldz00_bglt BGl_z62lambda2565z62zzliveness_typesz00(obj_t
		BgL_envz00_8028, obj_t BgL_o1298z00_8029)
	{
		{	/* Liveness/types.scm 89 */
			{	/* Liveness/types.scm 89 */
				long BgL_arg2566z00_9490;

				{	/* Liveness/types.scm 89 */
					obj_t BgL_arg2567z00_9491;

					{	/* Liveness/types.scm 89 */
						obj_t BgL_arg2568z00_9492;

						{	/* Liveness/types.scm 89 */
							obj_t BgL_arg1815z00_9493;
							long BgL_arg1816z00_9494;

							BgL_arg1815z00_9493 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 89 */
								long BgL_arg1817z00_9495;

								BgL_arg1817z00_9495 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_setfieldz00_bglt) BgL_o1298z00_8029)));
								BgL_arg1816z00_9494 = (BgL_arg1817z00_9495 - OBJECT_TYPE);
							}
							BgL_arg2568z00_9492 =
								VECTOR_REF(BgL_arg1815z00_9493, BgL_arg1816z00_9494);
						}
						BgL_arg2567z00_9491 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2568z00_9492);
					}
					{	/* Liveness/types.scm 89 */
						obj_t BgL_tmpz00_16518;

						BgL_tmpz00_16518 = ((obj_t) BgL_arg2567z00_9491);
						BgL_arg2566z00_9490 = BGL_CLASS_NUM(BgL_tmpz00_16518);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_setfieldz00_bglt) BgL_o1298z00_8029)), BgL_arg2566z00_9490);
			}
			{	/* Liveness/types.scm 89 */
				BgL_objectz00_bglt BgL_tmpz00_16524;

				BgL_tmpz00_16524 =
					((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1298z00_8029));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16524, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1298z00_8029));
			return
				((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1298z00_8029));
		}

	}



/* &<@anonymous:2564> */
	obj_t BGl_z62zc3z04anonymousza32564ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8030, obj_t BgL_new1297z00_8031)
	{
		{	/* Liveness/types.scm 89 */
			{
				BgL_setfieldz00_bglt BgL_auxz00_16532;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_setfieldz00_bglt) BgL_new1297z00_8031))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_16536;

					{	/* Liveness/types.scm 89 */
						obj_t BgL_classz00_9497;

						BgL_classz00_9497 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 89 */
							obj_t BgL__ortest_1117z00_9498;

							BgL__ortest_1117z00_9498 = BGL_CLASS_NIL(BgL_classz00_9497);
							if (CBOOL(BgL__ortest_1117z00_9498))
								{	/* Liveness/types.scm 89 */
									BgL_auxz00_16536 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9498);
								}
							else
								{	/* Liveness/types.scm 89 */
									BgL_auxz00_16536 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9497));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_setfieldz00_bglt) BgL_new1297z00_8031))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_16536), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_setfieldz00_bglt) BgL_new1297z00_8031))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1297z00_8031))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1297z00_8031))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1297z00_8031))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1297z00_8031))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				((((BgL_setfieldz00_bglt)
							COBJECT(((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1297z00_8031))))->BgL_fnamez00) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_16564;

					{	/* Liveness/types.scm 89 */
						obj_t BgL_classz00_9499;

						BgL_classz00_9499 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 89 */
							obj_t BgL__ortest_1117z00_9500;

							BgL__ortest_1117z00_9500 = BGL_CLASS_NIL(BgL_classz00_9499);
							if (CBOOL(BgL__ortest_1117z00_9500))
								{	/* Liveness/types.scm 89 */
									BgL_auxz00_16564 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9500);
								}
							else
								{	/* Liveness/types.scm 89 */
									BgL_auxz00_16564 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9499));
								}
						}
					}
					((((BgL_setfieldz00_bglt) COBJECT(
									((BgL_setfieldz00_bglt)
										((BgL_setfieldz00_bglt) BgL_new1297z00_8031))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_16564), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_16574;

					{	/* Liveness/types.scm 89 */
						obj_t BgL_classz00_9501;

						BgL_classz00_9501 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 89 */
							obj_t BgL__ortest_1117z00_9502;

							BgL__ortest_1117z00_9502 = BGL_CLASS_NIL(BgL_classz00_9501);
							if (CBOOL(BgL__ortest_1117z00_9502))
								{	/* Liveness/types.scm 89 */
									BgL_auxz00_16574 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9502);
								}
							else
								{	/* Liveness/types.scm 89 */
									BgL_auxz00_16574 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9501));
								}
						}
					}
					((((BgL_setfieldz00_bglt) COBJECT(
									((BgL_setfieldz00_bglt)
										((BgL_setfieldz00_bglt) BgL_new1297z00_8031))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_16574), BUNSPEC);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16584;

					{
						obj_t BgL_auxz00_16585;

						{	/* Liveness/types.scm 89 */
							BgL_objectz00_bglt BgL_tmpz00_16586;

							BgL_tmpz00_16586 =
								((BgL_objectz00_bglt)
								((BgL_setfieldz00_bglt) BgL_new1297z00_8031));
							BgL_auxz00_16585 = BGL_OBJECT_WIDENING(BgL_tmpz00_16586);
						}
						BgL_auxz00_16584 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16585);
					}
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16584))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16592;

					{
						obj_t BgL_auxz00_16593;

						{	/* Liveness/types.scm 89 */
							BgL_objectz00_bglt BgL_tmpz00_16594;

							BgL_tmpz00_16594 =
								((BgL_objectz00_bglt)
								((BgL_setfieldz00_bglt) BgL_new1297z00_8031));
							BgL_auxz00_16593 = BGL_OBJECT_WIDENING(BgL_tmpz00_16594);
						}
						BgL_auxz00_16592 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16593);
					}
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16592))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16600;

					{
						obj_t BgL_auxz00_16601;

						{	/* Liveness/types.scm 89 */
							BgL_objectz00_bglt BgL_tmpz00_16602;

							BgL_tmpz00_16602 =
								((BgL_objectz00_bglt)
								((BgL_setfieldz00_bglt) BgL_new1297z00_8031));
							BgL_auxz00_16601 = BGL_OBJECT_WIDENING(BgL_tmpz00_16602);
						}
						BgL_auxz00_16600 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16601);
					}
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16600))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16608;

					{
						obj_t BgL_auxz00_16609;

						{	/* Liveness/types.scm 89 */
							BgL_objectz00_bglt BgL_tmpz00_16610;

							BgL_tmpz00_16610 =
								((BgL_objectz00_bglt)
								((BgL_setfieldz00_bglt) BgL_new1297z00_8031));
							BgL_auxz00_16609 = BGL_OBJECT_WIDENING(BgL_tmpz00_16610);
						}
						BgL_auxz00_16608 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16609);
					}
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16608))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_16532 = ((BgL_setfieldz00_bglt) BgL_new1297z00_8031);
				return ((obj_t) BgL_auxz00_16532);
			}
		}

	}



/* &lambda2562 */
	BgL_setfieldz00_bglt BGl_z62lambda2562z62zzliveness_typesz00(obj_t
		BgL_envz00_8032, obj_t BgL_o1294z00_8033)
	{
		{	/* Liveness/types.scm 89 */
			{	/* Liveness/types.scm 89 */
				BgL_setfieldzf2livenesszf2_bglt BgL_wide1296z00_9504;

				BgL_wide1296z00_9504 =
					((BgL_setfieldzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_setfieldzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 89 */
					obj_t BgL_auxz00_16623;
					BgL_objectz00_bglt BgL_tmpz00_16619;

					BgL_auxz00_16623 = ((obj_t) BgL_wide1296z00_9504);
					BgL_tmpz00_16619 =
						((BgL_objectz00_bglt)
						((BgL_setfieldz00_bglt)
							((BgL_setfieldz00_bglt) BgL_o1294z00_8033)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16619, BgL_auxz00_16623);
				}
				((BgL_objectz00_bglt)
					((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1294z00_8033)));
				{	/* Liveness/types.scm 89 */
					long BgL_arg2563z00_9505;

					BgL_arg2563z00_9505 =
						BGL_CLASS_NUM(BGl_setfieldzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_setfieldz00_bglt)
								((BgL_setfieldz00_bglt) BgL_o1294z00_8033))),
						BgL_arg2563z00_9505);
				}
				return
					((BgL_setfieldz00_bglt)
					((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1294z00_8033)));
			}
		}

	}



/* &lambda2557 */
	BgL_setfieldz00_bglt BGl_z62lambda2557z62zzliveness_typesz00(obj_t
		BgL_envz00_8034, obj_t BgL_loc1280z00_8035, obj_t BgL_type1281z00_8036,
		obj_t BgL_sidezd2effect1282zd2_8037, obj_t BgL_key1283z00_8038,
		obj_t BgL_exprza21284za2_8039, obj_t BgL_effect1285z00_8040,
		obj_t BgL_czd2format1286zd2_8041, obj_t BgL_fname1287z00_8042,
		obj_t BgL_ftype1288z00_8043, obj_t BgL_otype1289z00_8044,
		obj_t BgL_def1290z00_8045, obj_t BgL_use1291z00_8046,
		obj_t BgL_in1292z00_8047, obj_t BgL_out1293z00_8048)
	{
		{	/* Liveness/types.scm 89 */
			{	/* Liveness/types.scm 89 */
				BgL_setfieldz00_bglt BgL_new1740z00_9516;

				{	/* Liveness/types.scm 89 */
					BgL_setfieldz00_bglt BgL_tmp1738z00_9517;
					BgL_setfieldzf2livenesszf2_bglt BgL_wide1739z00_9518;

					{
						BgL_setfieldz00_bglt BgL_auxz00_16637;

						{	/* Liveness/types.scm 89 */
							BgL_setfieldz00_bglt BgL_new1737z00_9519;

							BgL_new1737z00_9519 =
								((BgL_setfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_setfieldz00_bgl))));
							{	/* Liveness/types.scm 89 */
								long BgL_arg2561z00_9520;

								BgL_arg2561z00_9520 =
									BGL_CLASS_NUM(BGl_setfieldz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1737z00_9519),
									BgL_arg2561z00_9520);
							}
							{	/* Liveness/types.scm 89 */
								BgL_objectz00_bglt BgL_tmpz00_16642;

								BgL_tmpz00_16642 = ((BgL_objectz00_bglt) BgL_new1737z00_9519);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16642, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1737z00_9519);
							BgL_auxz00_16637 = BgL_new1737z00_9519;
						}
						BgL_tmp1738z00_9517 = ((BgL_setfieldz00_bglt) BgL_auxz00_16637);
					}
					BgL_wide1739z00_9518 =
						((BgL_setfieldzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_setfieldzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 89 */
						obj_t BgL_auxz00_16650;
						BgL_objectz00_bglt BgL_tmpz00_16648;

						BgL_auxz00_16650 = ((obj_t) BgL_wide1739z00_9518);
						BgL_tmpz00_16648 = ((BgL_objectz00_bglt) BgL_tmp1738z00_9517);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16648, BgL_auxz00_16650);
					}
					((BgL_objectz00_bglt) BgL_tmp1738z00_9517);
					{	/* Liveness/types.scm 89 */
						long BgL_arg2560z00_9521;

						BgL_arg2560z00_9521 =
							BGL_CLASS_NUM(BGl_setfieldzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1738z00_9517), BgL_arg2560z00_9521);
					}
					BgL_new1740z00_9516 = ((BgL_setfieldz00_bglt) BgL_tmp1738z00_9517);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1740z00_9516)))->BgL_locz00) =
					((obj_t) BgL_loc1280z00_8035), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1740z00_9516)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1281z00_8036)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1740z00_9516)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1282zd2_8037), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1740z00_9516)))->BgL_keyz00) =
					((obj_t) BgL_key1283z00_8038), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1740z00_9516)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21284za2_8039)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1740z00_9516)))->BgL_effectz00) =
					((obj_t) BgL_effect1285z00_8040), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1740z00_9516)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1286zd2_8041)), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
									BgL_new1740z00_9516)))->BgL_fnamez00) =
					((obj_t) ((obj_t) BgL_fname1287z00_8042)), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
									BgL_new1740z00_9516)))->BgL_ftypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1288z00_8043)),
					BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
									BgL_new1740z00_9516)))->BgL_otypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1289z00_8044)),
					BUNSPEC);
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16684;

					{
						obj_t BgL_auxz00_16685;

						{	/* Liveness/types.scm 89 */
							BgL_objectz00_bglt BgL_tmpz00_16686;

							BgL_tmpz00_16686 = ((BgL_objectz00_bglt) BgL_new1740z00_9516);
							BgL_auxz00_16685 = BGL_OBJECT_WIDENING(BgL_tmpz00_16686);
						}
						BgL_auxz00_16684 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16685);
					}
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16684))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1290z00_8045)), BUNSPEC);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16692;

					{
						obj_t BgL_auxz00_16693;

						{	/* Liveness/types.scm 89 */
							BgL_objectz00_bglt BgL_tmpz00_16694;

							BgL_tmpz00_16694 = ((BgL_objectz00_bglt) BgL_new1740z00_9516);
							BgL_auxz00_16693 = BGL_OBJECT_WIDENING(BgL_tmpz00_16694);
						}
						BgL_auxz00_16692 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16693);
					}
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16692))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1291z00_8046)), BUNSPEC);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16700;

					{
						obj_t BgL_auxz00_16701;

						{	/* Liveness/types.scm 89 */
							BgL_objectz00_bglt BgL_tmpz00_16702;

							BgL_tmpz00_16702 = ((BgL_objectz00_bglt) BgL_new1740z00_9516);
							BgL_auxz00_16701 = BGL_OBJECT_WIDENING(BgL_tmpz00_16702);
						}
						BgL_auxz00_16700 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16701);
					}
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16700))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1292z00_8047)), BUNSPEC);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16708;

					{
						obj_t BgL_auxz00_16709;

						{	/* Liveness/types.scm 89 */
							BgL_objectz00_bglt BgL_tmpz00_16710;

							BgL_tmpz00_16710 = ((BgL_objectz00_bglt) BgL_new1740z00_9516);
							BgL_auxz00_16709 = BGL_OBJECT_WIDENING(BgL_tmpz00_16710);
						}
						BgL_auxz00_16708 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16709);
					}
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16708))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1293z00_8048)), BUNSPEC);
				}
				return BgL_new1740z00_9516;
			}
		}

	}



/* &<@anonymous:2611> */
	obj_t BGl_z62zc3z04anonymousza32611ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8049)
	{
		{	/* Liveness/types.scm 89 */
			return BNIL;
		}

	}



/* &lambda2610 */
	obj_t BGl_z62lambda2610z62zzliveness_typesz00(obj_t BgL_envz00_8050,
		obj_t BgL_oz00_8051, obj_t BgL_vz00_8052)
	{
		{	/* Liveness/types.scm 89 */
			{
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16716;

				{
					obj_t BgL_auxz00_16717;

					{	/* Liveness/types.scm 89 */
						BgL_objectz00_bglt BgL_tmpz00_16718;

						BgL_tmpz00_16718 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_8051));
						BgL_auxz00_16717 = BGL_OBJECT_WIDENING(BgL_tmpz00_16718);
					}
					BgL_auxz00_16716 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16717);
				}
				return
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16716))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8052)), BUNSPEC);
			}
		}

	}



/* &lambda2609 */
	obj_t BGl_z62lambda2609z62zzliveness_typesz00(obj_t BgL_envz00_8053,
		obj_t BgL_oz00_8054)
	{
		{	/* Liveness/types.scm 89 */
			{
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16725;

				{
					obj_t BgL_auxz00_16726;

					{	/* Liveness/types.scm 89 */
						BgL_objectz00_bglt BgL_tmpz00_16727;

						BgL_tmpz00_16727 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_8054));
						BgL_auxz00_16726 = BGL_OBJECT_WIDENING(BgL_tmpz00_16727);
					}
					BgL_auxz00_16725 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16726);
				}
				return
					(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16725))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2603> */
	obj_t BGl_z62zc3z04anonymousza32603ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8055)
	{
		{	/* Liveness/types.scm 89 */
			return BNIL;
		}

	}



/* &lambda2602 */
	obj_t BGl_z62lambda2602z62zzliveness_typesz00(obj_t BgL_envz00_8056,
		obj_t BgL_oz00_8057, obj_t BgL_vz00_8058)
	{
		{	/* Liveness/types.scm 89 */
			{
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16733;

				{
					obj_t BgL_auxz00_16734;

					{	/* Liveness/types.scm 89 */
						BgL_objectz00_bglt BgL_tmpz00_16735;

						BgL_tmpz00_16735 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_8057));
						BgL_auxz00_16734 = BGL_OBJECT_WIDENING(BgL_tmpz00_16735);
					}
					BgL_auxz00_16733 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16734);
				}
				return
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16733))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8058)), BUNSPEC);
			}
		}

	}



/* &lambda2601 */
	obj_t BGl_z62lambda2601z62zzliveness_typesz00(obj_t BgL_envz00_8059,
		obj_t BgL_oz00_8060)
	{
		{	/* Liveness/types.scm 89 */
			{
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16742;

				{
					obj_t BgL_auxz00_16743;

					{	/* Liveness/types.scm 89 */
						BgL_objectz00_bglt BgL_tmpz00_16744;

						BgL_tmpz00_16744 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_8060));
						BgL_auxz00_16743 = BGL_OBJECT_WIDENING(BgL_tmpz00_16744);
					}
					BgL_auxz00_16742 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16743);
				}
				return
					(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16742))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2595> */
	obj_t BGl_z62zc3z04anonymousza32595ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8061)
	{
		{	/* Liveness/types.scm 89 */
			return BNIL;
		}

	}



/* &lambda2594 */
	obj_t BGl_z62lambda2594z62zzliveness_typesz00(obj_t BgL_envz00_8062,
		obj_t BgL_oz00_8063, obj_t BgL_vz00_8064)
	{
		{	/* Liveness/types.scm 89 */
			{
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16750;

				{
					obj_t BgL_auxz00_16751;

					{	/* Liveness/types.scm 89 */
						BgL_objectz00_bglt BgL_tmpz00_16752;

						BgL_tmpz00_16752 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_8063));
						BgL_auxz00_16751 = BGL_OBJECT_WIDENING(BgL_tmpz00_16752);
					}
					BgL_auxz00_16750 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16751);
				}
				return
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16750))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8064)), BUNSPEC);
			}
		}

	}



/* &lambda2593 */
	obj_t BGl_z62lambda2593z62zzliveness_typesz00(obj_t BgL_envz00_8065,
		obj_t BgL_oz00_8066)
	{
		{	/* Liveness/types.scm 89 */
			{
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16759;

				{
					obj_t BgL_auxz00_16760;

					{	/* Liveness/types.scm 89 */
						BgL_objectz00_bglt BgL_tmpz00_16761;

						BgL_tmpz00_16761 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_8066));
						BgL_auxz00_16760 = BGL_OBJECT_WIDENING(BgL_tmpz00_16761);
					}
					BgL_auxz00_16759 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16760);
				}
				return
					(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16759))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2587> */
	obj_t BGl_z62zc3z04anonymousza32587ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8067)
	{
		{	/* Liveness/types.scm 89 */
			return BNIL;
		}

	}



/* &lambda2586 */
	obj_t BGl_z62lambda2586z62zzliveness_typesz00(obj_t BgL_envz00_8068,
		obj_t BgL_oz00_8069, obj_t BgL_vz00_8070)
	{
		{	/* Liveness/types.scm 89 */
			{
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16767;

				{
					obj_t BgL_auxz00_16768;

					{	/* Liveness/types.scm 89 */
						BgL_objectz00_bglt BgL_tmpz00_16769;

						BgL_tmpz00_16769 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_8069));
						BgL_auxz00_16768 = BGL_OBJECT_WIDENING(BgL_tmpz00_16769);
					}
					BgL_auxz00_16767 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16768);
				}
				return
					((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16767))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8070)), BUNSPEC);
			}
		}

	}



/* &lambda2585 */
	obj_t BGl_z62lambda2585z62zzliveness_typesz00(obj_t BgL_envz00_8071,
		obj_t BgL_oz00_8072)
	{
		{	/* Liveness/types.scm 89 */
			{
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_16776;

				{
					obj_t BgL_auxz00_16777;

					{	/* Liveness/types.scm 89 */
						BgL_objectz00_bglt BgL_tmpz00_16778;

						BgL_tmpz00_16778 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_8072));
						BgL_auxz00_16777 = BGL_OBJECT_WIDENING(BgL_tmpz00_16778);
					}
					BgL_auxz00_16776 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_16777);
				}
				return
					(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16776))->
					BgL_defz00);
			}
		}

	}



/* &lambda2510 */
	BgL_getfieldz00_bglt BGl_z62lambda2510z62zzliveness_typesz00(obj_t
		BgL_envz00_8073, obj_t BgL_o1278z00_8074)
	{
		{	/* Liveness/types.scm 83 */
			{	/* Liveness/types.scm 83 */
				long BgL_arg2511z00_9535;

				{	/* Liveness/types.scm 83 */
					obj_t BgL_arg2512z00_9536;

					{	/* Liveness/types.scm 83 */
						obj_t BgL_arg2513z00_9537;

						{	/* Liveness/types.scm 83 */
							obj_t BgL_arg1815z00_9538;
							long BgL_arg1816z00_9539;

							BgL_arg1815z00_9538 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 83 */
								long BgL_arg1817z00_9540;

								BgL_arg1817z00_9540 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_getfieldz00_bglt) BgL_o1278z00_8074)));
								BgL_arg1816z00_9539 = (BgL_arg1817z00_9540 - OBJECT_TYPE);
							}
							BgL_arg2513z00_9537 =
								VECTOR_REF(BgL_arg1815z00_9538, BgL_arg1816z00_9539);
						}
						BgL_arg2512z00_9536 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2513z00_9537);
					}
					{	/* Liveness/types.scm 83 */
						obj_t BgL_tmpz00_16791;

						BgL_tmpz00_16791 = ((obj_t) BgL_arg2512z00_9536);
						BgL_arg2511z00_9535 = BGL_CLASS_NUM(BgL_tmpz00_16791);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_getfieldz00_bglt) BgL_o1278z00_8074)), BgL_arg2511z00_9535);
			}
			{	/* Liveness/types.scm 83 */
				BgL_objectz00_bglt BgL_tmpz00_16797;

				BgL_tmpz00_16797 =
					((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1278z00_8074));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16797, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1278z00_8074));
			return
				((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1278z00_8074));
		}

	}



/* &<@anonymous:2509> */
	obj_t BGl_z62zc3z04anonymousza32509ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8075, obj_t BgL_new1277z00_8076)
	{
		{	/* Liveness/types.scm 83 */
			{
				BgL_getfieldz00_bglt BgL_auxz00_16805;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_getfieldz00_bglt) BgL_new1277z00_8076))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_16809;

					{	/* Liveness/types.scm 83 */
						obj_t BgL_classz00_9542;

						BgL_classz00_9542 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 83 */
							obj_t BgL__ortest_1117z00_9543;

							BgL__ortest_1117z00_9543 = BGL_CLASS_NIL(BgL_classz00_9542);
							if (CBOOL(BgL__ortest_1117z00_9543))
								{	/* Liveness/types.scm 83 */
									BgL_auxz00_16809 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9543);
								}
							else
								{	/* Liveness/types.scm 83 */
									BgL_auxz00_16809 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9542));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_getfieldz00_bglt) BgL_new1277z00_8076))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_16809), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_getfieldz00_bglt) BgL_new1277z00_8076))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1277z00_8076))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1277z00_8076))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1277z00_8076))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1277z00_8076))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				((((BgL_getfieldz00_bglt)
							COBJECT(((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1277z00_8076))))->BgL_fnamez00) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_16837;

					{	/* Liveness/types.scm 83 */
						obj_t BgL_classz00_9544;

						BgL_classz00_9544 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 83 */
							obj_t BgL__ortest_1117z00_9545;

							BgL__ortest_1117z00_9545 = BGL_CLASS_NIL(BgL_classz00_9544);
							if (CBOOL(BgL__ortest_1117z00_9545))
								{	/* Liveness/types.scm 83 */
									BgL_auxz00_16837 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9545);
								}
							else
								{	/* Liveness/types.scm 83 */
									BgL_auxz00_16837 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9544));
								}
						}
					}
					((((BgL_getfieldz00_bglt) COBJECT(
									((BgL_getfieldz00_bglt)
										((BgL_getfieldz00_bglt) BgL_new1277z00_8076))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_16837), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_16847;

					{	/* Liveness/types.scm 83 */
						obj_t BgL_classz00_9546;

						BgL_classz00_9546 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 83 */
							obj_t BgL__ortest_1117z00_9547;

							BgL__ortest_1117z00_9547 = BGL_CLASS_NIL(BgL_classz00_9546);
							if (CBOOL(BgL__ortest_1117z00_9547))
								{	/* Liveness/types.scm 83 */
									BgL_auxz00_16847 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9547);
								}
							else
								{	/* Liveness/types.scm 83 */
									BgL_auxz00_16847 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9546));
								}
						}
					}
					((((BgL_getfieldz00_bglt) COBJECT(
									((BgL_getfieldz00_bglt)
										((BgL_getfieldz00_bglt) BgL_new1277z00_8076))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_16847), BUNSPEC);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16857;

					{
						obj_t BgL_auxz00_16858;

						{	/* Liveness/types.scm 83 */
							BgL_objectz00_bglt BgL_tmpz00_16859;

							BgL_tmpz00_16859 =
								((BgL_objectz00_bglt)
								((BgL_getfieldz00_bglt) BgL_new1277z00_8076));
							BgL_auxz00_16858 = BGL_OBJECT_WIDENING(BgL_tmpz00_16859);
						}
						BgL_auxz00_16857 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16858);
					}
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16857))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16865;

					{
						obj_t BgL_auxz00_16866;

						{	/* Liveness/types.scm 83 */
							BgL_objectz00_bglt BgL_tmpz00_16867;

							BgL_tmpz00_16867 =
								((BgL_objectz00_bglt)
								((BgL_getfieldz00_bglt) BgL_new1277z00_8076));
							BgL_auxz00_16866 = BGL_OBJECT_WIDENING(BgL_tmpz00_16867);
						}
						BgL_auxz00_16865 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16866);
					}
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16865))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16873;

					{
						obj_t BgL_auxz00_16874;

						{	/* Liveness/types.scm 83 */
							BgL_objectz00_bglt BgL_tmpz00_16875;

							BgL_tmpz00_16875 =
								((BgL_objectz00_bglt)
								((BgL_getfieldz00_bglt) BgL_new1277z00_8076));
							BgL_auxz00_16874 = BGL_OBJECT_WIDENING(BgL_tmpz00_16875);
						}
						BgL_auxz00_16873 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16874);
					}
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16873))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16881;

					{
						obj_t BgL_auxz00_16882;

						{	/* Liveness/types.scm 83 */
							BgL_objectz00_bglt BgL_tmpz00_16883;

							BgL_tmpz00_16883 =
								((BgL_objectz00_bglt)
								((BgL_getfieldz00_bglt) BgL_new1277z00_8076));
							BgL_auxz00_16882 = BGL_OBJECT_WIDENING(BgL_tmpz00_16883);
						}
						BgL_auxz00_16881 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16882);
					}
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16881))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_16805 = ((BgL_getfieldz00_bglt) BgL_new1277z00_8076);
				return ((obj_t) BgL_auxz00_16805);
			}
		}

	}



/* &lambda2507 */
	BgL_getfieldz00_bglt BGl_z62lambda2507z62zzliveness_typesz00(obj_t
		BgL_envz00_8077, obj_t BgL_o1274z00_8078)
	{
		{	/* Liveness/types.scm 83 */
			{	/* Liveness/types.scm 83 */
				BgL_getfieldzf2livenesszf2_bglt BgL_wide1276z00_9549;

				BgL_wide1276z00_9549 =
					((BgL_getfieldzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_getfieldzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 83 */
					obj_t BgL_auxz00_16896;
					BgL_objectz00_bglt BgL_tmpz00_16892;

					BgL_auxz00_16896 = ((obj_t) BgL_wide1276z00_9549);
					BgL_tmpz00_16892 =
						((BgL_objectz00_bglt)
						((BgL_getfieldz00_bglt)
							((BgL_getfieldz00_bglt) BgL_o1274z00_8078)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16892, BgL_auxz00_16896);
				}
				((BgL_objectz00_bglt)
					((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1274z00_8078)));
				{	/* Liveness/types.scm 83 */
					long BgL_arg2508z00_9550;

					BgL_arg2508z00_9550 =
						BGL_CLASS_NUM(BGl_getfieldzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_getfieldz00_bglt)
								((BgL_getfieldz00_bglt) BgL_o1274z00_8078))),
						BgL_arg2508z00_9550);
				}
				return
					((BgL_getfieldz00_bglt)
					((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1274z00_8078)));
			}
		}

	}



/* &lambda2504 */
	BgL_getfieldz00_bglt BGl_z62lambda2504z62zzliveness_typesz00(obj_t
		BgL_envz00_8079, obj_t BgL_loc1260z00_8080, obj_t BgL_type1261z00_8081,
		obj_t BgL_sidezd2effect1262zd2_8082, obj_t BgL_key1263z00_8083,
		obj_t BgL_exprza21264za2_8084, obj_t BgL_effect1265z00_8085,
		obj_t BgL_czd2format1266zd2_8086, obj_t BgL_fname1267z00_8087,
		obj_t BgL_ftype1268z00_8088, obj_t BgL_otype1269z00_8089,
		obj_t BgL_def1270z00_8090, obj_t BgL_use1271z00_8091,
		obj_t BgL_in1272z00_8092, obj_t BgL_out1273z00_8093)
	{
		{	/* Liveness/types.scm 83 */
			{	/* Liveness/types.scm 83 */
				BgL_getfieldz00_bglt BgL_new1735z00_9561;

				{	/* Liveness/types.scm 83 */
					BgL_getfieldz00_bglt BgL_tmp1733z00_9562;
					BgL_getfieldzf2livenesszf2_bglt BgL_wide1734z00_9563;

					{
						BgL_getfieldz00_bglt BgL_auxz00_16910;

						{	/* Liveness/types.scm 83 */
							BgL_getfieldz00_bglt BgL_new1732z00_9564;

							BgL_new1732z00_9564 =
								((BgL_getfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_getfieldz00_bgl))));
							{	/* Liveness/types.scm 83 */
								long BgL_arg2506z00_9565;

								BgL_arg2506z00_9565 =
									BGL_CLASS_NUM(BGl_getfieldz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1732z00_9564),
									BgL_arg2506z00_9565);
							}
							{	/* Liveness/types.scm 83 */
								BgL_objectz00_bglt BgL_tmpz00_16915;

								BgL_tmpz00_16915 = ((BgL_objectz00_bglt) BgL_new1732z00_9564);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16915, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1732z00_9564);
							BgL_auxz00_16910 = BgL_new1732z00_9564;
						}
						BgL_tmp1733z00_9562 = ((BgL_getfieldz00_bglt) BgL_auxz00_16910);
					}
					BgL_wide1734z00_9563 =
						((BgL_getfieldzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_getfieldzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 83 */
						obj_t BgL_auxz00_16923;
						BgL_objectz00_bglt BgL_tmpz00_16921;

						BgL_auxz00_16923 = ((obj_t) BgL_wide1734z00_9563);
						BgL_tmpz00_16921 = ((BgL_objectz00_bglt) BgL_tmp1733z00_9562);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_16921, BgL_auxz00_16923);
					}
					((BgL_objectz00_bglt) BgL_tmp1733z00_9562);
					{	/* Liveness/types.scm 83 */
						long BgL_arg2505z00_9566;

						BgL_arg2505z00_9566 =
							BGL_CLASS_NUM(BGl_getfieldzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1733z00_9562), BgL_arg2505z00_9566);
					}
					BgL_new1735z00_9561 = ((BgL_getfieldz00_bglt) BgL_tmp1733z00_9562);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1735z00_9561)))->BgL_locz00) =
					((obj_t) BgL_loc1260z00_8080), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1735z00_9561)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1261z00_8081)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1735z00_9561)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1262zd2_8082), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1735z00_9561)))->BgL_keyz00) =
					((obj_t) BgL_key1263z00_8083), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1735z00_9561)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21264za2_8084)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1735z00_9561)))->BgL_effectz00) =
					((obj_t) BgL_effect1265z00_8085), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1735z00_9561)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1266zd2_8086)), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
									BgL_new1735z00_9561)))->BgL_fnamez00) =
					((obj_t) ((obj_t) BgL_fname1267z00_8087)), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
									BgL_new1735z00_9561)))->BgL_ftypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1268z00_8088)),
					BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
									BgL_new1735z00_9561)))->BgL_otypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1269z00_8089)),
					BUNSPEC);
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16957;

					{
						obj_t BgL_auxz00_16958;

						{	/* Liveness/types.scm 83 */
							BgL_objectz00_bglt BgL_tmpz00_16959;

							BgL_tmpz00_16959 = ((BgL_objectz00_bglt) BgL_new1735z00_9561);
							BgL_auxz00_16958 = BGL_OBJECT_WIDENING(BgL_tmpz00_16959);
						}
						BgL_auxz00_16957 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16958);
					}
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16957))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1270z00_8090)), BUNSPEC);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16965;

					{
						obj_t BgL_auxz00_16966;

						{	/* Liveness/types.scm 83 */
							BgL_objectz00_bglt BgL_tmpz00_16967;

							BgL_tmpz00_16967 = ((BgL_objectz00_bglt) BgL_new1735z00_9561);
							BgL_auxz00_16966 = BGL_OBJECT_WIDENING(BgL_tmpz00_16967);
						}
						BgL_auxz00_16965 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16966);
					}
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16965))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1271z00_8091)), BUNSPEC);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16973;

					{
						obj_t BgL_auxz00_16974;

						{	/* Liveness/types.scm 83 */
							BgL_objectz00_bglt BgL_tmpz00_16975;

							BgL_tmpz00_16975 = ((BgL_objectz00_bglt) BgL_new1735z00_9561);
							BgL_auxz00_16974 = BGL_OBJECT_WIDENING(BgL_tmpz00_16975);
						}
						BgL_auxz00_16973 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16974);
					}
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16973))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1272z00_8092)), BUNSPEC);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16981;

					{
						obj_t BgL_auxz00_16982;

						{	/* Liveness/types.scm 83 */
							BgL_objectz00_bglt BgL_tmpz00_16983;

							BgL_tmpz00_16983 = ((BgL_objectz00_bglt) BgL_new1735z00_9561);
							BgL_auxz00_16982 = BGL_OBJECT_WIDENING(BgL_tmpz00_16983);
						}
						BgL_auxz00_16981 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16982);
					}
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16981))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1273z00_8093)), BUNSPEC);
				}
				return BgL_new1735z00_9561;
			}
		}

	}



/* &<@anonymous:2549> */
	obj_t BGl_z62zc3z04anonymousza32549ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8094)
	{
		{	/* Liveness/types.scm 83 */
			return BNIL;
		}

	}



/* &lambda2548 */
	obj_t BGl_z62lambda2548z62zzliveness_typesz00(obj_t BgL_envz00_8095,
		obj_t BgL_oz00_8096, obj_t BgL_vz00_8097)
	{
		{	/* Liveness/types.scm 83 */
			{
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16989;

				{
					obj_t BgL_auxz00_16990;

					{	/* Liveness/types.scm 83 */
						BgL_objectz00_bglt BgL_tmpz00_16991;

						BgL_tmpz00_16991 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_8096));
						BgL_auxz00_16990 = BGL_OBJECT_WIDENING(BgL_tmpz00_16991);
					}
					BgL_auxz00_16989 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16990);
				}
				return
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16989))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8097)), BUNSPEC);
			}
		}

	}



/* &lambda2547 */
	obj_t BGl_z62lambda2547z62zzliveness_typesz00(obj_t BgL_envz00_8098,
		obj_t BgL_oz00_8099)
	{
		{	/* Liveness/types.scm 83 */
			{
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_16998;

				{
					obj_t BgL_auxz00_16999;

					{	/* Liveness/types.scm 83 */
						BgL_objectz00_bglt BgL_tmpz00_17000;

						BgL_tmpz00_17000 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_8099));
						BgL_auxz00_16999 = BGL_OBJECT_WIDENING(BgL_tmpz00_17000);
					}
					BgL_auxz00_16998 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_16999);
				}
				return
					(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_16998))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2541> */
	obj_t BGl_z62zc3z04anonymousza32541ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8100)
	{
		{	/* Liveness/types.scm 83 */
			return BNIL;
		}

	}



/* &lambda2540 */
	obj_t BGl_z62lambda2540z62zzliveness_typesz00(obj_t BgL_envz00_8101,
		obj_t BgL_oz00_8102, obj_t BgL_vz00_8103)
	{
		{	/* Liveness/types.scm 83 */
			{
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_17006;

				{
					obj_t BgL_auxz00_17007;

					{	/* Liveness/types.scm 83 */
						BgL_objectz00_bglt BgL_tmpz00_17008;

						BgL_tmpz00_17008 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_8102));
						BgL_auxz00_17007 = BGL_OBJECT_WIDENING(BgL_tmpz00_17008);
					}
					BgL_auxz00_17006 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_17007);
				}
				return
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17006))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8103)), BUNSPEC);
			}
		}

	}



/* &lambda2539 */
	obj_t BGl_z62lambda2539z62zzliveness_typesz00(obj_t BgL_envz00_8104,
		obj_t BgL_oz00_8105)
	{
		{	/* Liveness/types.scm 83 */
			{
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_17015;

				{
					obj_t BgL_auxz00_17016;

					{	/* Liveness/types.scm 83 */
						BgL_objectz00_bglt BgL_tmpz00_17017;

						BgL_tmpz00_17017 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_8105));
						BgL_auxz00_17016 = BGL_OBJECT_WIDENING(BgL_tmpz00_17017);
					}
					BgL_auxz00_17015 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_17016);
				}
				return
					(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17015))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2530> */
	obj_t BGl_z62zc3z04anonymousza32530ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8106)
	{
		{	/* Liveness/types.scm 83 */
			return BNIL;
		}

	}



/* &lambda2529 */
	obj_t BGl_z62lambda2529z62zzliveness_typesz00(obj_t BgL_envz00_8107,
		obj_t BgL_oz00_8108, obj_t BgL_vz00_8109)
	{
		{	/* Liveness/types.scm 83 */
			{
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_17023;

				{
					obj_t BgL_auxz00_17024;

					{	/* Liveness/types.scm 83 */
						BgL_objectz00_bglt BgL_tmpz00_17025;

						BgL_tmpz00_17025 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_8108));
						BgL_auxz00_17024 = BGL_OBJECT_WIDENING(BgL_tmpz00_17025);
					}
					BgL_auxz00_17023 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_17024);
				}
				return
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17023))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8109)), BUNSPEC);
			}
		}

	}



/* &lambda2528 */
	obj_t BGl_z62lambda2528z62zzliveness_typesz00(obj_t BgL_envz00_8110,
		obj_t BgL_oz00_8111)
	{
		{	/* Liveness/types.scm 83 */
			{
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_17032;

				{
					obj_t BgL_auxz00_17033;

					{	/* Liveness/types.scm 83 */
						BgL_objectz00_bglt BgL_tmpz00_17034;

						BgL_tmpz00_17034 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_8111));
						BgL_auxz00_17033 = BGL_OBJECT_WIDENING(BgL_tmpz00_17034);
					}
					BgL_auxz00_17032 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_17033);
				}
				return
					(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17032))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2521> */
	obj_t BGl_z62zc3z04anonymousza32521ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8112)
	{
		{	/* Liveness/types.scm 83 */
			return BNIL;
		}

	}



/* &lambda2520 */
	obj_t BGl_z62lambda2520z62zzliveness_typesz00(obj_t BgL_envz00_8113,
		obj_t BgL_oz00_8114, obj_t BgL_vz00_8115)
	{
		{	/* Liveness/types.scm 83 */
			{
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_17040;

				{
					obj_t BgL_auxz00_17041;

					{	/* Liveness/types.scm 83 */
						BgL_objectz00_bglt BgL_tmpz00_17042;

						BgL_tmpz00_17042 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_8114));
						BgL_auxz00_17041 = BGL_OBJECT_WIDENING(BgL_tmpz00_17042);
					}
					BgL_auxz00_17040 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_17041);
				}
				return
					((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17040))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8115)), BUNSPEC);
			}
		}

	}



/* &lambda2519 */
	obj_t BGl_z62lambda2519z62zzliveness_typesz00(obj_t BgL_envz00_8116,
		obj_t BgL_oz00_8117)
	{
		{	/* Liveness/types.scm 83 */
			{
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_17049;

				{
					obj_t BgL_auxz00_17050;

					{	/* Liveness/types.scm 83 */
						BgL_objectz00_bglt BgL_tmpz00_17051;

						BgL_tmpz00_17051 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_8117));
						BgL_auxz00_17050 = BGL_OBJECT_WIDENING(BgL_tmpz00_17051);
					}
					BgL_auxz00_17049 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_17050);
				}
				return
					(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17049))->
					BgL_defz00);
			}
		}

	}



/* &lambda2457 */
	BgL_pragmaz00_bglt BGl_z62lambda2457z62zzliveness_typesz00(obj_t
		BgL_envz00_8118, obj_t BgL_o1258z00_8119)
	{
		{	/* Liveness/types.scm 77 */
			{	/* Liveness/types.scm 77 */
				long BgL_arg2458z00_9580;

				{	/* Liveness/types.scm 77 */
					obj_t BgL_arg2459z00_9581;

					{	/* Liveness/types.scm 77 */
						obj_t BgL_arg2460z00_9582;

						{	/* Liveness/types.scm 77 */
							obj_t BgL_arg1815z00_9583;
							long BgL_arg1816z00_9584;

							BgL_arg1815z00_9583 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 77 */
								long BgL_arg1817z00_9585;

								BgL_arg1817z00_9585 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_pragmaz00_bglt) BgL_o1258z00_8119)));
								BgL_arg1816z00_9584 = (BgL_arg1817z00_9585 - OBJECT_TYPE);
							}
							BgL_arg2460z00_9582 =
								VECTOR_REF(BgL_arg1815z00_9583, BgL_arg1816z00_9584);
						}
						BgL_arg2459z00_9581 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2460z00_9582);
					}
					{	/* Liveness/types.scm 77 */
						obj_t BgL_tmpz00_17064;

						BgL_tmpz00_17064 = ((obj_t) BgL_arg2459z00_9581);
						BgL_arg2458z00_9580 = BGL_CLASS_NUM(BgL_tmpz00_17064);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_pragmaz00_bglt) BgL_o1258z00_8119)), BgL_arg2458z00_9580);
			}
			{	/* Liveness/types.scm 77 */
				BgL_objectz00_bglt BgL_tmpz00_17070;

				BgL_tmpz00_17070 =
					((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1258z00_8119));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17070, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1258z00_8119));
			return ((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1258z00_8119));
		}

	}



/* &<@anonymous:2456> */
	obj_t BGl_z62zc3z04anonymousza32456ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8120, obj_t BgL_new1257z00_8121)
	{
		{	/* Liveness/types.scm 77 */
			{
				BgL_pragmaz00_bglt BgL_auxz00_17078;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_pragmaz00_bglt) BgL_new1257z00_8121))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_17082;

					{	/* Liveness/types.scm 77 */
						obj_t BgL_classz00_9587;

						BgL_classz00_9587 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 77 */
							obj_t BgL__ortest_1117z00_9588;

							BgL__ortest_1117z00_9588 = BGL_CLASS_NIL(BgL_classz00_9587);
							if (CBOOL(BgL__ortest_1117z00_9588))
								{	/* Liveness/types.scm 77 */
									BgL_auxz00_17082 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9588);
								}
							else
								{	/* Liveness/types.scm 77 */
									BgL_auxz00_17082 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9587));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_pragmaz00_bglt) BgL_new1257z00_8121))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_17082), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_pragmaz00_bglt) BgL_new1257z00_8121))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1257z00_8121))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1257z00_8121))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1257z00_8121))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_pragmaz00_bglt)
							COBJECT(((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1257z00_8121))))->BgL_formatz00) =
					((obj_t) BGl_string4516z00zzliveness_typesz00), BUNSPEC);
				((((BgL_pragmaz00_bglt)
							COBJECT(((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1257z00_8121))))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(43)), BUNSPEC);
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17111;

					{
						obj_t BgL_auxz00_17112;

						{	/* Liveness/types.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_17113;

							BgL_tmpz00_17113 =
								((BgL_objectz00_bglt)
								((BgL_pragmaz00_bglt) BgL_new1257z00_8121));
							BgL_auxz00_17112 = BGL_OBJECT_WIDENING(BgL_tmpz00_17113);
						}
						BgL_auxz00_17111 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17112);
					}
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17111))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17119;

					{
						obj_t BgL_auxz00_17120;

						{	/* Liveness/types.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_17121;

							BgL_tmpz00_17121 =
								((BgL_objectz00_bglt)
								((BgL_pragmaz00_bglt) BgL_new1257z00_8121));
							BgL_auxz00_17120 = BGL_OBJECT_WIDENING(BgL_tmpz00_17121);
						}
						BgL_auxz00_17119 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17120);
					}
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17119))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17127;

					{
						obj_t BgL_auxz00_17128;

						{	/* Liveness/types.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_17129;

							BgL_tmpz00_17129 =
								((BgL_objectz00_bglt)
								((BgL_pragmaz00_bglt) BgL_new1257z00_8121));
							BgL_auxz00_17128 = BGL_OBJECT_WIDENING(BgL_tmpz00_17129);
						}
						BgL_auxz00_17127 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17128);
					}
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17127))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17135;

					{
						obj_t BgL_auxz00_17136;

						{	/* Liveness/types.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_17137;

							BgL_tmpz00_17137 =
								((BgL_objectz00_bglt)
								((BgL_pragmaz00_bglt) BgL_new1257z00_8121));
							BgL_auxz00_17136 = BGL_OBJECT_WIDENING(BgL_tmpz00_17137);
						}
						BgL_auxz00_17135 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17136);
					}
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17135))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_17078 = ((BgL_pragmaz00_bglt) BgL_new1257z00_8121);
				return ((obj_t) BgL_auxz00_17078);
			}
		}

	}



/* &lambda2453 */
	BgL_pragmaz00_bglt BGl_z62lambda2453z62zzliveness_typesz00(obj_t
		BgL_envz00_8122, obj_t BgL_o1254z00_8123)
	{
		{	/* Liveness/types.scm 77 */
			{	/* Liveness/types.scm 77 */
				BgL_pragmazf2livenesszf2_bglt BgL_wide1256z00_9590;

				BgL_wide1256z00_9590 =
					((BgL_pragmazf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_pragmazf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 77 */
					obj_t BgL_auxz00_17150;
					BgL_objectz00_bglt BgL_tmpz00_17146;

					BgL_auxz00_17150 = ((obj_t) BgL_wide1256z00_9590);
					BgL_tmpz00_17146 =
						((BgL_objectz00_bglt)
						((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1254z00_8123)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17146, BgL_auxz00_17150);
				}
				((BgL_objectz00_bglt)
					((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1254z00_8123)));
				{	/* Liveness/types.scm 77 */
					long BgL_arg2455z00_9591;

					BgL_arg2455z00_9591 =
						BGL_CLASS_NUM(BGl_pragmazf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_pragmaz00_bglt)
								((BgL_pragmaz00_bglt) BgL_o1254z00_8123))),
						BgL_arg2455z00_9591);
				}
				return
					((BgL_pragmaz00_bglt)
					((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1254z00_8123)));
			}
		}

	}



/* &lambda2450 */
	BgL_pragmaz00_bglt BGl_z62lambda2450z62zzliveness_typesz00(obj_t
		BgL_envz00_8124, obj_t BgL_loc1242z00_8125, obj_t BgL_type1243z00_8126,
		obj_t BgL_sidezd2effect1244zd2_8127, obj_t BgL_key1245z00_8128,
		obj_t BgL_exprza21246za2_8129, obj_t BgL_effect1247z00_8130,
		obj_t BgL_format1248z00_8131, obj_t BgL_srfi01249z00_8132,
		obj_t BgL_def1250z00_8133, obj_t BgL_use1251z00_8134,
		obj_t BgL_in1252z00_8135, obj_t BgL_out1253z00_8136)
	{
		{	/* Liveness/types.scm 77 */
			{	/* Liveness/types.scm 77 */
				BgL_pragmaz00_bglt BgL_new1730z00_9600;

				{	/* Liveness/types.scm 77 */
					BgL_pragmaz00_bglt BgL_tmp1728z00_9601;
					BgL_pragmazf2livenesszf2_bglt BgL_wide1729z00_9602;

					{
						BgL_pragmaz00_bglt BgL_auxz00_17164;

						{	/* Liveness/types.scm 77 */
							BgL_pragmaz00_bglt BgL_new1727z00_9603;

							BgL_new1727z00_9603 =
								((BgL_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_pragmaz00_bgl))));
							{	/* Liveness/types.scm 77 */
								long BgL_arg2452z00_9604;

								BgL_arg2452z00_9604 = BGL_CLASS_NUM(BGl_pragmaz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1727z00_9603),
									BgL_arg2452z00_9604);
							}
							{	/* Liveness/types.scm 77 */
								BgL_objectz00_bglt BgL_tmpz00_17169;

								BgL_tmpz00_17169 = ((BgL_objectz00_bglt) BgL_new1727z00_9603);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17169, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1727z00_9603);
							BgL_auxz00_17164 = BgL_new1727z00_9603;
						}
						BgL_tmp1728z00_9601 = ((BgL_pragmaz00_bglt) BgL_auxz00_17164);
					}
					BgL_wide1729z00_9602 =
						((BgL_pragmazf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_pragmazf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 77 */
						obj_t BgL_auxz00_17177;
						BgL_objectz00_bglt BgL_tmpz00_17175;

						BgL_auxz00_17177 = ((obj_t) BgL_wide1729z00_9602);
						BgL_tmpz00_17175 = ((BgL_objectz00_bglt) BgL_tmp1728z00_9601);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17175, BgL_auxz00_17177);
					}
					((BgL_objectz00_bglt) BgL_tmp1728z00_9601);
					{	/* Liveness/types.scm 77 */
						long BgL_arg2451z00_9605;

						BgL_arg2451z00_9605 =
							BGL_CLASS_NUM(BGl_pragmazf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1728z00_9601), BgL_arg2451z00_9605);
					}
					BgL_new1730z00_9600 = ((BgL_pragmaz00_bglt) BgL_tmp1728z00_9601);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1730z00_9600)))->BgL_locz00) =
					((obj_t) BgL_loc1242z00_8125), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1730z00_9600)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1243z00_8126)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1730z00_9600)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1244zd2_8127), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1730z00_9600)))->BgL_keyz00) =
					((obj_t) BgL_key1245z00_8128), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1730z00_9600)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21246za2_8129)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1730z00_9600)))->BgL_effectz00) =
					((obj_t) BgL_effect1247z00_8130), BUNSPEC);
				((((BgL_pragmaz00_bglt) COBJECT(((BgL_pragmaz00_bglt)
									BgL_new1730z00_9600)))->BgL_formatz00) =
					((obj_t) ((obj_t) BgL_format1248z00_8131)), BUNSPEC);
				((((BgL_pragmaz00_bglt) COBJECT(((BgL_pragmaz00_bglt)
									BgL_new1730z00_9600)))->BgL_srfi0z00) =
					((obj_t) ((obj_t) BgL_srfi01249z00_8132)), BUNSPEC);
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17205;

					{
						obj_t BgL_auxz00_17206;

						{	/* Liveness/types.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_17207;

							BgL_tmpz00_17207 = ((BgL_objectz00_bglt) BgL_new1730z00_9600);
							BgL_auxz00_17206 = BGL_OBJECT_WIDENING(BgL_tmpz00_17207);
						}
						BgL_auxz00_17205 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17206);
					}
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17205))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1250z00_8133)), BUNSPEC);
				}
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17213;

					{
						obj_t BgL_auxz00_17214;

						{	/* Liveness/types.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_17215;

							BgL_tmpz00_17215 = ((BgL_objectz00_bglt) BgL_new1730z00_9600);
							BgL_auxz00_17214 = BGL_OBJECT_WIDENING(BgL_tmpz00_17215);
						}
						BgL_auxz00_17213 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17214);
					}
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17213))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1251z00_8134)), BUNSPEC);
				}
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17221;

					{
						obj_t BgL_auxz00_17222;

						{	/* Liveness/types.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_17223;

							BgL_tmpz00_17223 = ((BgL_objectz00_bglt) BgL_new1730z00_9600);
							BgL_auxz00_17222 = BGL_OBJECT_WIDENING(BgL_tmpz00_17223);
						}
						BgL_auxz00_17221 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17222);
					}
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17221))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1252z00_8135)), BUNSPEC);
				}
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17229;

					{
						obj_t BgL_auxz00_17230;

						{	/* Liveness/types.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_17231;

							BgL_tmpz00_17231 = ((BgL_objectz00_bglt) BgL_new1730z00_9600);
							BgL_auxz00_17230 = BGL_OBJECT_WIDENING(BgL_tmpz00_17231);
						}
						BgL_auxz00_17229 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17230);
					}
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17229))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1253z00_8136)), BUNSPEC);
				}
				return BgL_new1730z00_9600;
			}
		}

	}



/* &<@anonymous:2493> */
	obj_t BGl_z62zc3z04anonymousza32493ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8137)
	{
		{	/* Liveness/types.scm 77 */
			return BNIL;
		}

	}



/* &lambda2492 */
	obj_t BGl_z62lambda2492z62zzliveness_typesz00(obj_t BgL_envz00_8138,
		obj_t BgL_oz00_8139, obj_t BgL_vz00_8140)
	{
		{	/* Liveness/types.scm 77 */
			{
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17237;

				{
					obj_t BgL_auxz00_17238;

					{	/* Liveness/types.scm 77 */
						BgL_objectz00_bglt BgL_tmpz00_17239;

						BgL_tmpz00_17239 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_8139));
						BgL_auxz00_17238 = BGL_OBJECT_WIDENING(BgL_tmpz00_17239);
					}
					BgL_auxz00_17237 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17238);
				}
				return
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17237))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8140)), BUNSPEC);
			}
		}

	}



/* &lambda2491 */
	obj_t BGl_z62lambda2491z62zzliveness_typesz00(obj_t BgL_envz00_8141,
		obj_t BgL_oz00_8142)
	{
		{	/* Liveness/types.scm 77 */
			{
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17246;

				{
					obj_t BgL_auxz00_17247;

					{	/* Liveness/types.scm 77 */
						BgL_objectz00_bglt BgL_tmpz00_17248;

						BgL_tmpz00_17248 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_8142));
						BgL_auxz00_17247 = BGL_OBJECT_WIDENING(BgL_tmpz00_17248);
					}
					BgL_auxz00_17246 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17247);
				}
				return
					(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17246))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2485> */
	obj_t BGl_z62zc3z04anonymousza32485ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8143)
	{
		{	/* Liveness/types.scm 77 */
			return BNIL;
		}

	}



/* &lambda2484 */
	obj_t BGl_z62lambda2484z62zzliveness_typesz00(obj_t BgL_envz00_8144,
		obj_t BgL_oz00_8145, obj_t BgL_vz00_8146)
	{
		{	/* Liveness/types.scm 77 */
			{
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17254;

				{
					obj_t BgL_auxz00_17255;

					{	/* Liveness/types.scm 77 */
						BgL_objectz00_bglt BgL_tmpz00_17256;

						BgL_tmpz00_17256 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_8145));
						BgL_auxz00_17255 = BGL_OBJECT_WIDENING(BgL_tmpz00_17256);
					}
					BgL_auxz00_17254 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17255);
				}
				return
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17254))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8146)), BUNSPEC);
			}
		}

	}



/* &lambda2483 */
	obj_t BGl_z62lambda2483z62zzliveness_typesz00(obj_t BgL_envz00_8147,
		obj_t BgL_oz00_8148)
	{
		{	/* Liveness/types.scm 77 */
			{
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17263;

				{
					obj_t BgL_auxz00_17264;

					{	/* Liveness/types.scm 77 */
						BgL_objectz00_bglt BgL_tmpz00_17265;

						BgL_tmpz00_17265 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_8148));
						BgL_auxz00_17264 = BGL_OBJECT_WIDENING(BgL_tmpz00_17265);
					}
					BgL_auxz00_17263 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17264);
				}
				return
					(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17263))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2476> */
	obj_t BGl_z62zc3z04anonymousza32476ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8149)
	{
		{	/* Liveness/types.scm 77 */
			return BNIL;
		}

	}



/* &lambda2475 */
	obj_t BGl_z62lambda2475z62zzliveness_typesz00(obj_t BgL_envz00_8150,
		obj_t BgL_oz00_8151, obj_t BgL_vz00_8152)
	{
		{	/* Liveness/types.scm 77 */
			{
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17271;

				{
					obj_t BgL_auxz00_17272;

					{	/* Liveness/types.scm 77 */
						BgL_objectz00_bglt BgL_tmpz00_17273;

						BgL_tmpz00_17273 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_8151));
						BgL_auxz00_17272 = BGL_OBJECT_WIDENING(BgL_tmpz00_17273);
					}
					BgL_auxz00_17271 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17272);
				}
				return
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17271))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8152)), BUNSPEC);
			}
		}

	}



/* &lambda2474 */
	obj_t BGl_z62lambda2474z62zzliveness_typesz00(obj_t BgL_envz00_8153,
		obj_t BgL_oz00_8154)
	{
		{	/* Liveness/types.scm 77 */
			{
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17280;

				{
					obj_t BgL_auxz00_17281;

					{	/* Liveness/types.scm 77 */
						BgL_objectz00_bglt BgL_tmpz00_17282;

						BgL_tmpz00_17282 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_8154));
						BgL_auxz00_17281 = BGL_OBJECT_WIDENING(BgL_tmpz00_17282);
					}
					BgL_auxz00_17280 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17281);
				}
				return
					(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17280))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2468> */
	obj_t BGl_z62zc3z04anonymousza32468ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8155)
	{
		{	/* Liveness/types.scm 77 */
			return BNIL;
		}

	}



/* &lambda2467 */
	obj_t BGl_z62lambda2467z62zzliveness_typesz00(obj_t BgL_envz00_8156,
		obj_t BgL_oz00_8157, obj_t BgL_vz00_8158)
	{
		{	/* Liveness/types.scm 77 */
			{
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17288;

				{
					obj_t BgL_auxz00_17289;

					{	/* Liveness/types.scm 77 */
						BgL_objectz00_bglt BgL_tmpz00_17290;

						BgL_tmpz00_17290 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_8157));
						BgL_auxz00_17289 = BGL_OBJECT_WIDENING(BgL_tmpz00_17290);
					}
					BgL_auxz00_17288 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17289);
				}
				return
					((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17288))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8158)), BUNSPEC);
			}
		}

	}



/* &lambda2466 */
	obj_t BGl_z62lambda2466z62zzliveness_typesz00(obj_t BgL_envz00_8159,
		obj_t BgL_oz00_8160)
	{
		{	/* Liveness/types.scm 77 */
			{
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_17297;

				{
					obj_t BgL_auxz00_17298;

					{	/* Liveness/types.scm 77 */
						BgL_objectz00_bglt BgL_tmpz00_17299;

						BgL_tmpz00_17299 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_8160));
						BgL_auxz00_17298 = BGL_OBJECT_WIDENING(BgL_tmpz00_17299);
					}
					BgL_auxz00_17297 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_17298);
				}
				return
					(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_17297))->
					BgL_defz00);
			}
		}

	}



/* &lambda2405 */
	BgL_funcallz00_bglt BGl_z62lambda2405z62zzliveness_typesz00(obj_t
		BgL_envz00_8161, obj_t BgL_o1240z00_8162)
	{
		{	/* Liveness/types.scm 71 */
			{	/* Liveness/types.scm 71 */
				long BgL_arg2407z00_9619;

				{	/* Liveness/types.scm 71 */
					obj_t BgL_arg2408z00_9620;

					{	/* Liveness/types.scm 71 */
						obj_t BgL_arg2410z00_9621;

						{	/* Liveness/types.scm 71 */
							obj_t BgL_arg1815z00_9622;
							long BgL_arg1816z00_9623;

							BgL_arg1815z00_9622 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 71 */
								long BgL_arg1817z00_9624;

								BgL_arg1817z00_9624 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_funcallz00_bglt) BgL_o1240z00_8162)));
								BgL_arg1816z00_9623 = (BgL_arg1817z00_9624 - OBJECT_TYPE);
							}
							BgL_arg2410z00_9621 =
								VECTOR_REF(BgL_arg1815z00_9622, BgL_arg1816z00_9623);
						}
						BgL_arg2408z00_9620 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2410z00_9621);
					}
					{	/* Liveness/types.scm 71 */
						obj_t BgL_tmpz00_17312;

						BgL_tmpz00_17312 = ((obj_t) BgL_arg2408z00_9620);
						BgL_arg2407z00_9619 = BGL_CLASS_NUM(BgL_tmpz00_17312);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_funcallz00_bglt) BgL_o1240z00_8162)), BgL_arg2407z00_9619);
			}
			{	/* Liveness/types.scm 71 */
				BgL_objectz00_bglt BgL_tmpz00_17318;

				BgL_tmpz00_17318 =
					((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_o1240z00_8162));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17318, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_o1240z00_8162));
			return ((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_o1240z00_8162));
		}

	}



/* &<@anonymous:2404> */
	obj_t BGl_z62zc3z04anonymousza32404ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8163, obj_t BgL_new1239z00_8164)
	{
		{	/* Liveness/types.scm 71 */
			{
				BgL_funcallz00_bglt BgL_auxz00_17326;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_funcallz00_bglt) BgL_new1239z00_8164))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_17330;

					{	/* Liveness/types.scm 71 */
						obj_t BgL_classz00_9626;

						BgL_classz00_9626 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 71 */
							obj_t BgL__ortest_1117z00_9627;

							BgL__ortest_1117z00_9627 = BGL_CLASS_NIL(BgL_classz00_9626);
							if (CBOOL(BgL__ortest_1117z00_9627))
								{	/* Liveness/types.scm 71 */
									BgL_auxz00_17330 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9627);
								}
							else
								{	/* Liveness/types.scm 71 */
									BgL_auxz00_17330 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9626));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_funcallz00_bglt) BgL_new1239z00_8164))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_17330), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_17340;

					{	/* Liveness/types.scm 71 */
						obj_t BgL_classz00_9628;

						BgL_classz00_9628 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 71 */
							obj_t BgL__ortest_1117z00_9629;

							BgL__ortest_1117z00_9629 = BGL_CLASS_NIL(BgL_classz00_9628);
							if (CBOOL(BgL__ortest_1117z00_9629))
								{	/* Liveness/types.scm 71 */
									BgL_auxz00_17340 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_9629);
								}
							else
								{	/* Liveness/types.scm 71 */
									BgL_auxz00_17340 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9628));
								}
						}
					}
					((((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_new1239z00_8164))))->
							BgL_funz00) = ((BgL_nodez00_bglt) BgL_auxz00_17340), BUNSPEC);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt)
									((BgL_funcallz00_bglt) BgL_new1239z00_8164))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funcallz00_bglt)
							COBJECT(((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt)
										BgL_new1239z00_8164))))->BgL_strengthz00) =
					((obj_t) CNST_TABLE_REF(43)), BUNSPEC);
				((((BgL_funcallz00_bglt)
							COBJECT(((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt)
										BgL_new1239z00_8164))))->BgL_functionsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17360;

					{
						obj_t BgL_auxz00_17361;

						{	/* Liveness/types.scm 71 */
							BgL_objectz00_bglt BgL_tmpz00_17362;

							BgL_tmpz00_17362 =
								((BgL_objectz00_bglt)
								((BgL_funcallz00_bglt) BgL_new1239z00_8164));
							BgL_auxz00_17361 = BGL_OBJECT_WIDENING(BgL_tmpz00_17362);
						}
						BgL_auxz00_17360 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17361);
					}
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17360))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17368;

					{
						obj_t BgL_auxz00_17369;

						{	/* Liveness/types.scm 71 */
							BgL_objectz00_bglt BgL_tmpz00_17370;

							BgL_tmpz00_17370 =
								((BgL_objectz00_bglt)
								((BgL_funcallz00_bglt) BgL_new1239z00_8164));
							BgL_auxz00_17369 = BGL_OBJECT_WIDENING(BgL_tmpz00_17370);
						}
						BgL_auxz00_17368 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17369);
					}
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17368))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17376;

					{
						obj_t BgL_auxz00_17377;

						{	/* Liveness/types.scm 71 */
							BgL_objectz00_bglt BgL_tmpz00_17378;

							BgL_tmpz00_17378 =
								((BgL_objectz00_bglt)
								((BgL_funcallz00_bglt) BgL_new1239z00_8164));
							BgL_auxz00_17377 = BGL_OBJECT_WIDENING(BgL_tmpz00_17378);
						}
						BgL_auxz00_17376 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17377);
					}
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17376))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17384;

					{
						obj_t BgL_auxz00_17385;

						{	/* Liveness/types.scm 71 */
							BgL_objectz00_bglt BgL_tmpz00_17386;

							BgL_tmpz00_17386 =
								((BgL_objectz00_bglt)
								((BgL_funcallz00_bglt) BgL_new1239z00_8164));
							BgL_auxz00_17385 = BGL_OBJECT_WIDENING(BgL_tmpz00_17386);
						}
						BgL_auxz00_17384 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17385);
					}
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17384))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_17326 = ((BgL_funcallz00_bglt) BgL_new1239z00_8164);
				return ((obj_t) BgL_auxz00_17326);
			}
		}

	}



/* &lambda2402 */
	BgL_funcallz00_bglt BGl_z62lambda2402z62zzliveness_typesz00(obj_t
		BgL_envz00_8165, obj_t BgL_o1236z00_8166)
	{
		{	/* Liveness/types.scm 71 */
			{	/* Liveness/types.scm 71 */
				BgL_funcallzf2livenesszf2_bglt BgL_wide1238z00_9631;

				BgL_wide1238z00_9631 =
					((BgL_funcallzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_funcallzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 71 */
					obj_t BgL_auxz00_17399;
					BgL_objectz00_bglt BgL_tmpz00_17395;

					BgL_auxz00_17399 = ((obj_t) BgL_wide1238z00_9631);
					BgL_tmpz00_17395 =
						((BgL_objectz00_bglt)
						((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_o1236z00_8166)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17395, BgL_auxz00_17399);
				}
				((BgL_objectz00_bglt)
					((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_o1236z00_8166)));
				{	/* Liveness/types.scm 71 */
					long BgL_arg2403z00_9632;

					BgL_arg2403z00_9632 =
						BGL_CLASS_NUM(BGl_funcallzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_funcallz00_bglt)
								((BgL_funcallz00_bglt) BgL_o1236z00_8166))),
						BgL_arg2403z00_9632);
				}
				return
					((BgL_funcallz00_bglt)
					((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_o1236z00_8166)));
			}
		}

	}



/* &lambda2398 */
	BgL_funcallz00_bglt BGl_z62lambda2398z62zzliveness_typesz00(obj_t
		BgL_envz00_8167, obj_t BgL_loc1226z00_8168, obj_t BgL_type1227z00_8169,
		obj_t BgL_fun1228z00_8170, obj_t BgL_args1229z00_8171,
		obj_t BgL_strength1230z00_8172, obj_t BgL_functions1231z00_8173,
		obj_t BgL_def1232z00_8174, obj_t BgL_use1233z00_8175,
		obj_t BgL_in1234z00_8176, obj_t BgL_out1235z00_8177)
	{
		{	/* Liveness/types.scm 71 */
			{	/* Liveness/types.scm 71 */
				BgL_funcallz00_bglt BgL_new1725z00_9640;

				{	/* Liveness/types.scm 71 */
					BgL_funcallz00_bglt BgL_tmp1723z00_9641;
					BgL_funcallzf2livenesszf2_bglt BgL_wide1724z00_9642;

					{
						BgL_funcallz00_bglt BgL_auxz00_17413;

						{	/* Liveness/types.scm 71 */
							BgL_funcallz00_bglt BgL_new1722z00_9643;

							BgL_new1722z00_9643 =
								((BgL_funcallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_funcallz00_bgl))));
							{	/* Liveness/types.scm 71 */
								long BgL_arg2401z00_9644;

								BgL_arg2401z00_9644 =
									BGL_CLASS_NUM(BGl_funcallz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1722z00_9643),
									BgL_arg2401z00_9644);
							}
							{	/* Liveness/types.scm 71 */
								BgL_objectz00_bglt BgL_tmpz00_17418;

								BgL_tmpz00_17418 = ((BgL_objectz00_bglt) BgL_new1722z00_9643);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17418, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1722z00_9643);
							BgL_auxz00_17413 = BgL_new1722z00_9643;
						}
						BgL_tmp1723z00_9641 = ((BgL_funcallz00_bglt) BgL_auxz00_17413);
					}
					BgL_wide1724z00_9642 =
						((BgL_funcallzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_funcallzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 71 */
						obj_t BgL_auxz00_17426;
						BgL_objectz00_bglt BgL_tmpz00_17424;

						BgL_auxz00_17426 = ((obj_t) BgL_wide1724z00_9642);
						BgL_tmpz00_17424 = ((BgL_objectz00_bglt) BgL_tmp1723z00_9641);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17424, BgL_auxz00_17426);
					}
					((BgL_objectz00_bglt) BgL_tmp1723z00_9641);
					{	/* Liveness/types.scm 71 */
						long BgL_arg2399z00_9645;

						BgL_arg2399z00_9645 =
							BGL_CLASS_NUM(BGl_funcallzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1723z00_9641), BgL_arg2399z00_9645);
					}
					BgL_new1725z00_9640 = ((BgL_funcallz00_bglt) BgL_tmp1723z00_9641);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1725z00_9640)))->BgL_locz00) =
					((obj_t) BgL_loc1226z00_8168), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1725z00_9640)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1227z00_8169)),
					BUNSPEC);
				((((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
									BgL_new1725z00_9640)))->BgL_funz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_fun1228z00_8170)),
					BUNSPEC);
				((((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
									BgL_new1725z00_9640)))->BgL_argsz00) =
					((obj_t) BgL_args1229z00_8171), BUNSPEC);
				((((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
									BgL_new1725z00_9640)))->BgL_strengthz00) =
					((obj_t) ((obj_t) BgL_strength1230z00_8172)), BUNSPEC);
				((((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
									BgL_new1725z00_9640)))->BgL_functionsz00) =
					((obj_t) BgL_functions1231z00_8173), BUNSPEC);
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17449;

					{
						obj_t BgL_auxz00_17450;

						{	/* Liveness/types.scm 71 */
							BgL_objectz00_bglt BgL_tmpz00_17451;

							BgL_tmpz00_17451 = ((BgL_objectz00_bglt) BgL_new1725z00_9640);
							BgL_auxz00_17450 = BGL_OBJECT_WIDENING(BgL_tmpz00_17451);
						}
						BgL_auxz00_17449 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17450);
					}
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17449))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1232z00_8174)), BUNSPEC);
				}
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17457;

					{
						obj_t BgL_auxz00_17458;

						{	/* Liveness/types.scm 71 */
							BgL_objectz00_bglt BgL_tmpz00_17459;

							BgL_tmpz00_17459 = ((BgL_objectz00_bglt) BgL_new1725z00_9640);
							BgL_auxz00_17458 = BGL_OBJECT_WIDENING(BgL_tmpz00_17459);
						}
						BgL_auxz00_17457 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17458);
					}
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17457))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1233z00_8175)), BUNSPEC);
				}
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17465;

					{
						obj_t BgL_auxz00_17466;

						{	/* Liveness/types.scm 71 */
							BgL_objectz00_bglt BgL_tmpz00_17467;

							BgL_tmpz00_17467 = ((BgL_objectz00_bglt) BgL_new1725z00_9640);
							BgL_auxz00_17466 = BGL_OBJECT_WIDENING(BgL_tmpz00_17467);
						}
						BgL_auxz00_17465 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17466);
					}
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17465))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1234z00_8176)), BUNSPEC);
				}
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17473;

					{
						obj_t BgL_auxz00_17474;

						{	/* Liveness/types.scm 71 */
							BgL_objectz00_bglt BgL_tmpz00_17475;

							BgL_tmpz00_17475 = ((BgL_objectz00_bglt) BgL_new1725z00_9640);
							BgL_auxz00_17474 = BGL_OBJECT_WIDENING(BgL_tmpz00_17475);
						}
						BgL_auxz00_17473 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17474);
					}
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17473))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1235z00_8177)), BUNSPEC);
				}
				return BgL_new1725z00_9640;
			}
		}

	}



/* &<@anonymous:2442> */
	obj_t BGl_z62zc3z04anonymousza32442ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8178)
	{
		{	/* Liveness/types.scm 71 */
			return BNIL;
		}

	}



/* &lambda2441 */
	obj_t BGl_z62lambda2441z62zzliveness_typesz00(obj_t BgL_envz00_8179,
		obj_t BgL_oz00_8180, obj_t BgL_vz00_8181)
	{
		{	/* Liveness/types.scm 71 */
			{
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17481;

				{
					obj_t BgL_auxz00_17482;

					{	/* Liveness/types.scm 71 */
						BgL_objectz00_bglt BgL_tmpz00_17483;

						BgL_tmpz00_17483 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_oz00_8180));
						BgL_auxz00_17482 = BGL_OBJECT_WIDENING(BgL_tmpz00_17483);
					}
					BgL_auxz00_17481 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17482);
				}
				return
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17481))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8181)), BUNSPEC);
			}
		}

	}



/* &lambda2440 */
	obj_t BGl_z62lambda2440z62zzliveness_typesz00(obj_t BgL_envz00_8182,
		obj_t BgL_oz00_8183)
	{
		{	/* Liveness/types.scm 71 */
			{
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17490;

				{
					obj_t BgL_auxz00_17491;

					{	/* Liveness/types.scm 71 */
						BgL_objectz00_bglt BgL_tmpz00_17492;

						BgL_tmpz00_17492 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_oz00_8183));
						BgL_auxz00_17491 = BGL_OBJECT_WIDENING(BgL_tmpz00_17492);
					}
					BgL_auxz00_17490 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17491);
				}
				return
					(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17490))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2434> */
	obj_t BGl_z62zc3z04anonymousza32434ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8184)
	{
		{	/* Liveness/types.scm 71 */
			return BNIL;
		}

	}



/* &lambda2433 */
	obj_t BGl_z62lambda2433z62zzliveness_typesz00(obj_t BgL_envz00_8185,
		obj_t BgL_oz00_8186, obj_t BgL_vz00_8187)
	{
		{	/* Liveness/types.scm 71 */
			{
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17498;

				{
					obj_t BgL_auxz00_17499;

					{	/* Liveness/types.scm 71 */
						BgL_objectz00_bglt BgL_tmpz00_17500;

						BgL_tmpz00_17500 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_oz00_8186));
						BgL_auxz00_17499 = BGL_OBJECT_WIDENING(BgL_tmpz00_17500);
					}
					BgL_auxz00_17498 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17499);
				}
				return
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17498))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8187)), BUNSPEC);
			}
		}

	}



/* &lambda2432 */
	obj_t BGl_z62lambda2432z62zzliveness_typesz00(obj_t BgL_envz00_8188,
		obj_t BgL_oz00_8189)
	{
		{	/* Liveness/types.scm 71 */
			{
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17507;

				{
					obj_t BgL_auxz00_17508;

					{	/* Liveness/types.scm 71 */
						BgL_objectz00_bglt BgL_tmpz00_17509;

						BgL_tmpz00_17509 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_oz00_8189));
						BgL_auxz00_17508 = BGL_OBJECT_WIDENING(BgL_tmpz00_17509);
					}
					BgL_auxz00_17507 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17508);
				}
				return
					(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17507))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2426> */
	obj_t BGl_z62zc3z04anonymousza32426ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8190)
	{
		{	/* Liveness/types.scm 71 */
			return BNIL;
		}

	}



/* &lambda2425 */
	obj_t BGl_z62lambda2425z62zzliveness_typesz00(obj_t BgL_envz00_8191,
		obj_t BgL_oz00_8192, obj_t BgL_vz00_8193)
	{
		{	/* Liveness/types.scm 71 */
			{
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17515;

				{
					obj_t BgL_auxz00_17516;

					{	/* Liveness/types.scm 71 */
						BgL_objectz00_bglt BgL_tmpz00_17517;

						BgL_tmpz00_17517 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_oz00_8192));
						BgL_auxz00_17516 = BGL_OBJECT_WIDENING(BgL_tmpz00_17517);
					}
					BgL_auxz00_17515 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17516);
				}
				return
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17515))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8193)), BUNSPEC);
			}
		}

	}



/* &lambda2424 */
	obj_t BGl_z62lambda2424z62zzliveness_typesz00(obj_t BgL_envz00_8194,
		obj_t BgL_oz00_8195)
	{
		{	/* Liveness/types.scm 71 */
			{
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17524;

				{
					obj_t BgL_auxz00_17525;

					{	/* Liveness/types.scm 71 */
						BgL_objectz00_bglt BgL_tmpz00_17526;

						BgL_tmpz00_17526 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_oz00_8195));
						BgL_auxz00_17525 = BGL_OBJECT_WIDENING(BgL_tmpz00_17526);
					}
					BgL_auxz00_17524 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17525);
				}
				return
					(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17524))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2418> */
	obj_t BGl_z62zc3z04anonymousza32418ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8196)
	{
		{	/* Liveness/types.scm 71 */
			return BNIL;
		}

	}



/* &lambda2417 */
	obj_t BGl_z62lambda2417z62zzliveness_typesz00(obj_t BgL_envz00_8197,
		obj_t BgL_oz00_8198, obj_t BgL_vz00_8199)
	{
		{	/* Liveness/types.scm 71 */
			{
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17532;

				{
					obj_t BgL_auxz00_17533;

					{	/* Liveness/types.scm 71 */
						BgL_objectz00_bglt BgL_tmpz00_17534;

						BgL_tmpz00_17534 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_oz00_8198));
						BgL_auxz00_17533 = BGL_OBJECT_WIDENING(BgL_tmpz00_17534);
					}
					BgL_auxz00_17532 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17533);
				}
				return
					((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17532))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8199)), BUNSPEC);
			}
		}

	}



/* &lambda2416 */
	obj_t BGl_z62lambda2416z62zzliveness_typesz00(obj_t BgL_envz00_8200,
		obj_t BgL_oz00_8201)
	{
		{	/* Liveness/types.scm 71 */
			{
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_17541;

				{
					obj_t BgL_auxz00_17542;

					{	/* Liveness/types.scm 71 */
						BgL_objectz00_bglt BgL_tmpz00_17543;

						BgL_tmpz00_17543 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_oz00_8201));
						BgL_auxz00_17542 = BGL_OBJECT_WIDENING(BgL_tmpz00_17543);
					}
					BgL_auxz00_17541 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_17542);
				}
				return
					(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17541))->
					BgL_defz00);
			}
		}

	}



/* &lambda2355 */
	BgL_appzd2lyzd2_bglt BGl_z62lambda2355z62zzliveness_typesz00(obj_t
		BgL_envz00_8202, obj_t BgL_o1224z00_8203)
	{
		{	/* Liveness/types.scm 65 */
			{	/* Liveness/types.scm 65 */
				long BgL_arg2356z00_9659;

				{	/* Liveness/types.scm 65 */
					obj_t BgL_arg2357z00_9660;

					{	/* Liveness/types.scm 65 */
						obj_t BgL_arg2358z00_9661;

						{	/* Liveness/types.scm 65 */
							obj_t BgL_arg1815z00_9662;
							long BgL_arg1816z00_9663;

							BgL_arg1815z00_9662 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 65 */
								long BgL_arg1817z00_9664;

								BgL_arg1817z00_9664 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_o1224z00_8203)));
								BgL_arg1816z00_9663 = (BgL_arg1817z00_9664 - OBJECT_TYPE);
							}
							BgL_arg2358z00_9661 =
								VECTOR_REF(BgL_arg1815z00_9662, BgL_arg1816z00_9663);
						}
						BgL_arg2357z00_9660 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2358z00_9661);
					}
					{	/* Liveness/types.scm 65 */
						obj_t BgL_tmpz00_17556;

						BgL_tmpz00_17556 = ((obj_t) BgL_arg2357z00_9660);
						BgL_arg2356z00_9659 = BGL_CLASS_NUM(BgL_tmpz00_17556);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_appzd2lyzd2_bglt) BgL_o1224z00_8203)), BgL_arg2356z00_9659);
			}
			{	/* Liveness/types.scm 65 */
				BgL_objectz00_bglt BgL_tmpz00_17562;

				BgL_tmpz00_17562 =
					((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_o1224z00_8203));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17562, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_o1224z00_8203));
			return
				((BgL_appzd2lyzd2_bglt) ((BgL_appzd2lyzd2_bglt) BgL_o1224z00_8203));
		}

	}



/* &<@anonymous:2354> */
	obj_t BGl_z62zc3z04anonymousza32354ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8204, obj_t BgL_new1223z00_8205)
	{
		{	/* Liveness/types.scm 65 */
			{
				BgL_appzd2lyzd2_bglt BgL_auxz00_17570;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_new1223z00_8205))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_17574;

					{	/* Liveness/types.scm 65 */
						obj_t BgL_classz00_9666;

						BgL_classz00_9666 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 65 */
							obj_t BgL__ortest_1117z00_9667;

							BgL__ortest_1117z00_9667 = BGL_CLASS_NIL(BgL_classz00_9666);
							if (CBOOL(BgL__ortest_1117z00_9667))
								{	/* Liveness/types.scm 65 */
									BgL_auxz00_17574 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9667);
								}
							else
								{	/* Liveness/types.scm 65 */
									BgL_auxz00_17574 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9666));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_new1223z00_8205))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_17574), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_17584;

					{	/* Liveness/types.scm 65 */
						obj_t BgL_classz00_9668;

						BgL_classz00_9668 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 65 */
							obj_t BgL__ortest_1117z00_9669;

							BgL__ortest_1117z00_9669 = BGL_CLASS_NIL(BgL_classz00_9668);
							if (CBOOL(BgL__ortest_1117z00_9669))
								{	/* Liveness/types.scm 65 */
									BgL_auxz00_17584 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_9669);
								}
							else
								{	/* Liveness/types.scm 65 */
									BgL_auxz00_17584 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9668));
								}
						}
					}
					((((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_new1223z00_8205))))->
							BgL_funz00) = ((BgL_nodez00_bglt) BgL_auxz00_17584), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_17594;

					{	/* Liveness/types.scm 65 */
						obj_t BgL_classz00_9670;

						BgL_classz00_9670 = BGl_nodez00zzast_nodez00;
						{	/* Liveness/types.scm 65 */
							obj_t BgL__ortest_1117z00_9671;

							BgL__ortest_1117z00_9671 = BGL_CLASS_NIL(BgL_classz00_9670);
							if (CBOOL(BgL__ortest_1117z00_9671))
								{	/* Liveness/types.scm 65 */
									BgL_auxz00_17594 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_9671);
								}
							else
								{	/* Liveness/types.scm 65 */
									BgL_auxz00_17594 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9670));
								}
						}
					}
					((((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_new1223z00_8205))))->
							BgL_argz00) = ((BgL_nodez00_bglt) BgL_auxz00_17594), BUNSPEC);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17604;

					{
						obj_t BgL_auxz00_17605;

						{	/* Liveness/types.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_17606;

							BgL_tmpz00_17606 =
								((BgL_objectz00_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_new1223z00_8205));
							BgL_auxz00_17605 = BGL_OBJECT_WIDENING(BgL_tmpz00_17606);
						}
						BgL_auxz00_17604 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17605);
					}
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17604))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17612;

					{
						obj_t BgL_auxz00_17613;

						{	/* Liveness/types.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_17614;

							BgL_tmpz00_17614 =
								((BgL_objectz00_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_new1223z00_8205));
							BgL_auxz00_17613 = BGL_OBJECT_WIDENING(BgL_tmpz00_17614);
						}
						BgL_auxz00_17612 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17613);
					}
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17612))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17620;

					{
						obj_t BgL_auxz00_17621;

						{	/* Liveness/types.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_17622;

							BgL_tmpz00_17622 =
								((BgL_objectz00_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_new1223z00_8205));
							BgL_auxz00_17621 = BGL_OBJECT_WIDENING(BgL_tmpz00_17622);
						}
						BgL_auxz00_17620 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17621);
					}
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17620))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17628;

					{
						obj_t BgL_auxz00_17629;

						{	/* Liveness/types.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_17630;

							BgL_tmpz00_17630 =
								((BgL_objectz00_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_new1223z00_8205));
							BgL_auxz00_17629 = BGL_OBJECT_WIDENING(BgL_tmpz00_17630);
						}
						BgL_auxz00_17628 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17629);
					}
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17628))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_17570 = ((BgL_appzd2lyzd2_bglt) BgL_new1223z00_8205);
				return ((obj_t) BgL_auxz00_17570);
			}
		}

	}



/* &lambda2352 */
	BgL_appzd2lyzd2_bglt BGl_z62lambda2352z62zzliveness_typesz00(obj_t
		BgL_envz00_8206, obj_t BgL_o1220z00_8207)
	{
		{	/* Liveness/types.scm 65 */
			{	/* Liveness/types.scm 65 */
				BgL_appzd2lyzf2livenessz20_bglt BgL_wide1222z00_9673;

				BgL_wide1222z00_9673 =
					((BgL_appzd2lyzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_appzd2lyzf2livenessz20_bgl))));
				{	/* Liveness/types.scm 65 */
					obj_t BgL_auxz00_17643;
					BgL_objectz00_bglt BgL_tmpz00_17639;

					BgL_auxz00_17643 = ((obj_t) BgL_wide1222z00_9673);
					BgL_tmpz00_17639 =
						((BgL_objectz00_bglt)
						((BgL_appzd2lyzd2_bglt)
							((BgL_appzd2lyzd2_bglt) BgL_o1220z00_8207)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17639, BgL_auxz00_17643);
				}
				((BgL_objectz00_bglt)
					((BgL_appzd2lyzd2_bglt) ((BgL_appzd2lyzd2_bglt) BgL_o1220z00_8207)));
				{	/* Liveness/types.scm 65 */
					long BgL_arg2353z00_9674;

					BgL_arg2353z00_9674 =
						BGL_CLASS_NUM(BGl_appzd2lyzf2livenessz20zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_appzd2lyzd2_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_o1220z00_8207))),
						BgL_arg2353z00_9674);
				}
				return
					((BgL_appzd2lyzd2_bglt)
					((BgL_appzd2lyzd2_bglt) ((BgL_appzd2lyzd2_bglt) BgL_o1220z00_8207)));
			}
		}

	}



/* &lambda2349 */
	BgL_appzd2lyzd2_bglt BGl_z62lambda2349z62zzliveness_typesz00(obj_t
		BgL_envz00_8208, obj_t BgL_loc1211z00_8209, obj_t BgL_type1212z00_8210,
		obj_t BgL_fun1213z00_8211, obj_t BgL_arg1215z00_8212,
		obj_t BgL_def1216z00_8213, obj_t BgL_use1217z00_8214,
		obj_t BgL_in1218z00_8215, obj_t BgL_out1219z00_8216)
	{
		{	/* Liveness/types.scm 65 */
			{	/* Liveness/types.scm 65 */
				BgL_appzd2lyzd2_bglt BgL_new1720z00_9682;

				{	/* Liveness/types.scm 65 */
					BgL_appzd2lyzd2_bglt BgL_tmp1718z00_9683;
					BgL_appzd2lyzf2livenessz20_bglt BgL_wide1719z00_9684;

					{
						BgL_appzd2lyzd2_bglt BgL_auxz00_17657;

						{	/* Liveness/types.scm 65 */
							BgL_appzd2lyzd2_bglt BgL_new1717z00_9685;

							BgL_new1717z00_9685 =
								((BgL_appzd2lyzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_appzd2lyzd2_bgl))));
							{	/* Liveness/types.scm 65 */
								long BgL_arg2351z00_9686;

								BgL_arg2351z00_9686 =
									BGL_CLASS_NUM(BGl_appzd2lyzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1717z00_9685),
									BgL_arg2351z00_9686);
							}
							{	/* Liveness/types.scm 65 */
								BgL_objectz00_bglt BgL_tmpz00_17662;

								BgL_tmpz00_17662 = ((BgL_objectz00_bglt) BgL_new1717z00_9685);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17662, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1717z00_9685);
							BgL_auxz00_17657 = BgL_new1717z00_9685;
						}
						BgL_tmp1718z00_9683 = ((BgL_appzd2lyzd2_bglt) BgL_auxz00_17657);
					}
					BgL_wide1719z00_9684 =
						((BgL_appzd2lyzf2livenessz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_appzd2lyzf2livenessz20_bgl))));
					{	/* Liveness/types.scm 65 */
						obj_t BgL_auxz00_17670;
						BgL_objectz00_bglt BgL_tmpz00_17668;

						BgL_auxz00_17670 = ((obj_t) BgL_wide1719z00_9684);
						BgL_tmpz00_17668 = ((BgL_objectz00_bglt) BgL_tmp1718z00_9683);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17668, BgL_auxz00_17670);
					}
					((BgL_objectz00_bglt) BgL_tmp1718z00_9683);
					{	/* Liveness/types.scm 65 */
						long BgL_arg2350z00_9687;

						BgL_arg2350z00_9687 =
							BGL_CLASS_NUM(BGl_appzd2lyzf2livenessz20zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1718z00_9683), BgL_arg2350z00_9687);
					}
					BgL_new1720z00_9682 = ((BgL_appzd2lyzd2_bglt) BgL_tmp1718z00_9683);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1720z00_9682)))->BgL_locz00) =
					((obj_t) BgL_loc1211z00_8209), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1720z00_9682)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1212z00_8210)),
					BUNSPEC);
				((((BgL_appzd2lyzd2_bglt) COBJECT(((BgL_appzd2lyzd2_bglt)
									BgL_new1720z00_9682)))->BgL_funz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_fun1213z00_8211)),
					BUNSPEC);
				((((BgL_appzd2lyzd2_bglt) COBJECT(((BgL_appzd2lyzd2_bglt)
									BgL_new1720z00_9682)))->BgL_argz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1215z00_8212)),
					BUNSPEC);
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17689;

					{
						obj_t BgL_auxz00_17690;

						{	/* Liveness/types.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_17691;

							BgL_tmpz00_17691 = ((BgL_objectz00_bglt) BgL_new1720z00_9682);
							BgL_auxz00_17690 = BGL_OBJECT_WIDENING(BgL_tmpz00_17691);
						}
						BgL_auxz00_17689 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17690);
					}
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17689))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1216z00_8213)), BUNSPEC);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17697;

					{
						obj_t BgL_auxz00_17698;

						{	/* Liveness/types.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_17699;

							BgL_tmpz00_17699 = ((BgL_objectz00_bglt) BgL_new1720z00_9682);
							BgL_auxz00_17698 = BGL_OBJECT_WIDENING(BgL_tmpz00_17699);
						}
						BgL_auxz00_17697 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17698);
					}
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17697))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1217z00_8214)), BUNSPEC);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17705;

					{
						obj_t BgL_auxz00_17706;

						{	/* Liveness/types.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_17707;

							BgL_tmpz00_17707 = ((BgL_objectz00_bglt) BgL_new1720z00_9682);
							BgL_auxz00_17706 = BGL_OBJECT_WIDENING(BgL_tmpz00_17707);
						}
						BgL_auxz00_17705 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17706);
					}
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17705))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1218z00_8215)), BUNSPEC);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17713;

					{
						obj_t BgL_auxz00_17714;

						{	/* Liveness/types.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_17715;

							BgL_tmpz00_17715 = ((BgL_objectz00_bglt) BgL_new1720z00_9682);
							BgL_auxz00_17714 = BGL_OBJECT_WIDENING(BgL_tmpz00_17715);
						}
						BgL_auxz00_17713 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17714);
					}
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17713))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1219z00_8216)), BUNSPEC);
				}
				return BgL_new1720z00_9682;
			}
		}

	}



/* &<@anonymous:2390> */
	obj_t BGl_z62zc3z04anonymousza32390ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8217)
	{
		{	/* Liveness/types.scm 65 */
			return BNIL;
		}

	}



/* &lambda2389 */
	obj_t BGl_z62lambda2389z62zzliveness_typesz00(obj_t BgL_envz00_8218,
		obj_t BgL_oz00_8219, obj_t BgL_vz00_8220)
	{
		{	/* Liveness/types.scm 65 */
			{
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17721;

				{
					obj_t BgL_auxz00_17722;

					{	/* Liveness/types.scm 65 */
						BgL_objectz00_bglt BgL_tmpz00_17723;

						BgL_tmpz00_17723 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_oz00_8219));
						BgL_auxz00_17722 = BGL_OBJECT_WIDENING(BgL_tmpz00_17723);
					}
					BgL_auxz00_17721 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17722);
				}
				return
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17721))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8220)), BUNSPEC);
			}
		}

	}



/* &lambda2388 */
	obj_t BGl_z62lambda2388z62zzliveness_typesz00(obj_t BgL_envz00_8221,
		obj_t BgL_oz00_8222)
	{
		{	/* Liveness/types.scm 65 */
			{
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17730;

				{
					obj_t BgL_auxz00_17731;

					{	/* Liveness/types.scm 65 */
						BgL_objectz00_bglt BgL_tmpz00_17732;

						BgL_tmpz00_17732 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_oz00_8222));
						BgL_auxz00_17731 = BGL_OBJECT_WIDENING(BgL_tmpz00_17732);
					}
					BgL_auxz00_17730 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17731);
				}
				return
					(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17730))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2383> */
	obj_t BGl_z62zc3z04anonymousza32383ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8223)
	{
		{	/* Liveness/types.scm 65 */
			return BNIL;
		}

	}



/* &lambda2382 */
	obj_t BGl_z62lambda2382z62zzliveness_typesz00(obj_t BgL_envz00_8224,
		obj_t BgL_oz00_8225, obj_t BgL_vz00_8226)
	{
		{	/* Liveness/types.scm 65 */
			{
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17738;

				{
					obj_t BgL_auxz00_17739;

					{	/* Liveness/types.scm 65 */
						BgL_objectz00_bglt BgL_tmpz00_17740;

						BgL_tmpz00_17740 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_oz00_8225));
						BgL_auxz00_17739 = BGL_OBJECT_WIDENING(BgL_tmpz00_17740);
					}
					BgL_auxz00_17738 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17739);
				}
				return
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17738))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8226)), BUNSPEC);
			}
		}

	}



/* &lambda2381 */
	obj_t BGl_z62lambda2381z62zzliveness_typesz00(obj_t BgL_envz00_8227,
		obj_t BgL_oz00_8228)
	{
		{	/* Liveness/types.scm 65 */
			{
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17747;

				{
					obj_t BgL_auxz00_17748;

					{	/* Liveness/types.scm 65 */
						BgL_objectz00_bglt BgL_tmpz00_17749;

						BgL_tmpz00_17749 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_oz00_8228));
						BgL_auxz00_17748 = BGL_OBJECT_WIDENING(BgL_tmpz00_17749);
					}
					BgL_auxz00_17747 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17748);
				}
				return
					(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17747))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2376> */
	obj_t BGl_z62zc3z04anonymousza32376ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8229)
	{
		{	/* Liveness/types.scm 65 */
			return BNIL;
		}

	}



/* &lambda2375 */
	obj_t BGl_z62lambda2375z62zzliveness_typesz00(obj_t BgL_envz00_8230,
		obj_t BgL_oz00_8231, obj_t BgL_vz00_8232)
	{
		{	/* Liveness/types.scm 65 */
			{
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17755;

				{
					obj_t BgL_auxz00_17756;

					{	/* Liveness/types.scm 65 */
						BgL_objectz00_bglt BgL_tmpz00_17757;

						BgL_tmpz00_17757 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_oz00_8231));
						BgL_auxz00_17756 = BGL_OBJECT_WIDENING(BgL_tmpz00_17757);
					}
					BgL_auxz00_17755 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17756);
				}
				return
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17755))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8232)), BUNSPEC);
			}
		}

	}



/* &lambda2374 */
	obj_t BGl_z62lambda2374z62zzliveness_typesz00(obj_t BgL_envz00_8233,
		obj_t BgL_oz00_8234)
	{
		{	/* Liveness/types.scm 65 */
			{
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17764;

				{
					obj_t BgL_auxz00_17765;

					{	/* Liveness/types.scm 65 */
						BgL_objectz00_bglt BgL_tmpz00_17766;

						BgL_tmpz00_17766 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_oz00_8234));
						BgL_auxz00_17765 = BGL_OBJECT_WIDENING(BgL_tmpz00_17766);
					}
					BgL_auxz00_17764 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17765);
				}
				return
					(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17764))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2368> */
	obj_t BGl_z62zc3z04anonymousza32368ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8235)
	{
		{	/* Liveness/types.scm 65 */
			return BNIL;
		}

	}



/* &lambda2367 */
	obj_t BGl_z62lambda2367z62zzliveness_typesz00(obj_t BgL_envz00_8236,
		obj_t BgL_oz00_8237, obj_t BgL_vz00_8238)
	{
		{	/* Liveness/types.scm 65 */
			{
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17772;

				{
					obj_t BgL_auxz00_17773;

					{	/* Liveness/types.scm 65 */
						BgL_objectz00_bglt BgL_tmpz00_17774;

						BgL_tmpz00_17774 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_oz00_8237));
						BgL_auxz00_17773 = BGL_OBJECT_WIDENING(BgL_tmpz00_17774);
					}
					BgL_auxz00_17772 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17773);
				}
				return
					((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17772))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8238)), BUNSPEC);
			}
		}

	}



/* &lambda2366 */
	obj_t BGl_z62lambda2366z62zzliveness_typesz00(obj_t BgL_envz00_8239,
		obj_t BgL_oz00_8240)
	{
		{	/* Liveness/types.scm 65 */
			{
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_17781;

				{
					obj_t BgL_auxz00_17782;

					{	/* Liveness/types.scm 65 */
						BgL_objectz00_bglt BgL_tmpz00_17783;

						BgL_tmpz00_17783 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_oz00_8240));
						BgL_auxz00_17782 = BGL_OBJECT_WIDENING(BgL_tmpz00_17783);
					}
					BgL_auxz00_17781 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_17782);
				}
				return
					(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_17781))->
					BgL_defz00);
			}
		}

	}



/* &lambda2306 */
	BgL_appz00_bglt BGl_z62lambda2306z62zzliveness_typesz00(obj_t BgL_envz00_8241,
		obj_t BgL_o1209z00_8242)
	{
		{	/* Liveness/types.scm 59 */
			{	/* Liveness/types.scm 59 */
				long BgL_arg2307z00_9701;

				{	/* Liveness/types.scm 59 */
					obj_t BgL_arg2308z00_9702;

					{	/* Liveness/types.scm 59 */
						obj_t BgL_arg2309z00_9703;

						{	/* Liveness/types.scm 59 */
							obj_t BgL_arg1815z00_9704;
							long BgL_arg1816z00_9705;

							BgL_arg1815z00_9704 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 59 */
								long BgL_arg1817z00_9706;

								BgL_arg1817z00_9706 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_o1209z00_8242)));
								BgL_arg1816z00_9705 = (BgL_arg1817z00_9706 - OBJECT_TYPE);
							}
							BgL_arg2309z00_9703 =
								VECTOR_REF(BgL_arg1815z00_9704, BgL_arg1816z00_9705);
						}
						BgL_arg2308z00_9702 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2309z00_9703);
					}
					{	/* Liveness/types.scm 59 */
						obj_t BgL_tmpz00_17796;

						BgL_tmpz00_17796 = ((obj_t) BgL_arg2308z00_9702);
						BgL_arg2307z00_9701 = BGL_CLASS_NUM(BgL_tmpz00_17796);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_appz00_bglt) BgL_o1209z00_8242)), BgL_arg2307z00_9701);
			}
			{	/* Liveness/types.scm 59 */
				BgL_objectz00_bglt BgL_tmpz00_17802;

				BgL_tmpz00_17802 =
					((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_o1209z00_8242));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17802, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_o1209z00_8242));
			return ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_o1209z00_8242));
		}

	}



/* &<@anonymous:2305> */
	obj_t BGl_z62zc3z04anonymousza32305ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8243, obj_t BgL_new1208z00_8244)
	{
		{	/* Liveness/types.scm 59 */
			{
				BgL_appz00_bglt BgL_auxz00_17810;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_appz00_bglt) BgL_new1208z00_8244))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_17814;

					{	/* Liveness/types.scm 59 */
						obj_t BgL_classz00_9708;

						BgL_classz00_9708 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 59 */
							obj_t BgL__ortest_1117z00_9709;

							BgL__ortest_1117z00_9709 = BGL_CLASS_NIL(BgL_classz00_9708);
							if (CBOOL(BgL__ortest_1117z00_9709))
								{	/* Liveness/types.scm 59 */
									BgL_auxz00_17814 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9709);
								}
							else
								{	/* Liveness/types.scm 59 */
									BgL_auxz00_17814 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9708));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appz00_bglt) BgL_new1208z00_8244))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_17814), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_appz00_bglt) BgL_new1208z00_8244))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_appz00_bglt)
										BgL_new1208z00_8244))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_varz00_bglt BgL_auxz00_17830;

					{	/* Liveness/types.scm 59 */
						obj_t BgL_classz00_9710;

						BgL_classz00_9710 = BGl_varz00zzast_nodez00;
						{	/* Liveness/types.scm 59 */
							obj_t BgL__ortest_1117z00_9711;

							BgL__ortest_1117z00_9711 = BGL_CLASS_NIL(BgL_classz00_9710);
							if (CBOOL(BgL__ortest_1117z00_9711))
								{	/* Liveness/types.scm 59 */
									BgL_auxz00_17830 =
										((BgL_varz00_bglt) BgL__ortest_1117z00_9711);
								}
							else
								{	/* Liveness/types.scm 59 */
									BgL_auxz00_17830 =
										((BgL_varz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9710));
								}
						}
					}
					((((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_new1208z00_8244))))->BgL_funz00) =
						((BgL_varz00_bglt) BgL_auxz00_17830), BUNSPEC);
				}
				((((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_new1208z00_8244))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) ((BgL_appz00_bglt)
										BgL_new1208z00_8244))))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_17846;

					{
						obj_t BgL_auxz00_17847;

						{	/* Liveness/types.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_17848;

							BgL_tmpz00_17848 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_new1208z00_8244));
							BgL_auxz00_17847 = BGL_OBJECT_WIDENING(BgL_tmpz00_17848);
						}
						BgL_auxz00_17846 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17847);
					}
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17846))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_17854;

					{
						obj_t BgL_auxz00_17855;

						{	/* Liveness/types.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_17856;

							BgL_tmpz00_17856 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_new1208z00_8244));
							BgL_auxz00_17855 = BGL_OBJECT_WIDENING(BgL_tmpz00_17856);
						}
						BgL_auxz00_17854 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17855);
					}
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17854))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_17862;

					{
						obj_t BgL_auxz00_17863;

						{	/* Liveness/types.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_17864;

							BgL_tmpz00_17864 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_new1208z00_8244));
							BgL_auxz00_17863 = BGL_OBJECT_WIDENING(BgL_tmpz00_17864);
						}
						BgL_auxz00_17862 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17863);
					}
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17862))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_17870;

					{
						obj_t BgL_auxz00_17871;

						{	/* Liveness/types.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_17872;

							BgL_tmpz00_17872 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_new1208z00_8244));
							BgL_auxz00_17871 = BGL_OBJECT_WIDENING(BgL_tmpz00_17872);
						}
						BgL_auxz00_17870 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17871);
					}
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17870))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_17810 = ((BgL_appz00_bglt) BgL_new1208z00_8244);
				return ((obj_t) BgL_auxz00_17810);
			}
		}

	}



/* &lambda2303 */
	BgL_appz00_bglt BGl_z62lambda2303z62zzliveness_typesz00(obj_t BgL_envz00_8245,
		obj_t BgL_o1205z00_8246)
	{
		{	/* Liveness/types.scm 59 */
			{	/* Liveness/types.scm 59 */
				BgL_appzf2livenesszf2_bglt BgL_wide1207z00_9713;

				BgL_wide1207z00_9713 =
					((BgL_appzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_appzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 59 */
					obj_t BgL_auxz00_17885;
					BgL_objectz00_bglt BgL_tmpz00_17881;

					BgL_auxz00_17885 = ((obj_t) BgL_wide1207z00_9713);
					BgL_tmpz00_17881 =
						((BgL_objectz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_o1205z00_8246)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17881, BgL_auxz00_17885);
				}
				((BgL_objectz00_bglt)
					((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_o1205z00_8246)));
				{	/* Liveness/types.scm 59 */
					long BgL_arg2304z00_9714;

					BgL_arg2304z00_9714 =
						BGL_CLASS_NUM(BGl_appzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_o1205z00_8246))), BgL_arg2304z00_9714);
				}
				return
					((BgL_appz00_bglt)
					((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_o1205z00_8246)));
			}
		}

	}



/* &lambda2299 */
	BgL_appz00_bglt BGl_z62lambda2299z62zzliveness_typesz00(obj_t BgL_envz00_8247,
		obj_t BgL_loc1194z00_8248, obj_t BgL_type1195z00_8249,
		obj_t BgL_sidezd2effect1196zd2_8250, obj_t BgL_key1197z00_8251,
		obj_t BgL_fun1198z00_8252, obj_t BgL_args1199z00_8253,
		obj_t BgL_stackable1200z00_8254, obj_t BgL_def1201z00_8255,
		obj_t BgL_use1202z00_8256, obj_t BgL_in1203z00_8257,
		obj_t BgL_out1204z00_8258)
	{
		{	/* Liveness/types.scm 59 */
			{	/* Liveness/types.scm 59 */
				BgL_appz00_bglt BgL_new1715z00_9721;

				{	/* Liveness/types.scm 59 */
					BgL_appz00_bglt BgL_tmp1713z00_9722;
					BgL_appzf2livenesszf2_bglt BgL_wide1714z00_9723;

					{
						BgL_appz00_bglt BgL_auxz00_17899;

						{	/* Liveness/types.scm 59 */
							BgL_appz00_bglt BgL_new1712z00_9724;

							BgL_new1712z00_9724 =
								((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_appz00_bgl))));
							{	/* Liveness/types.scm 59 */
								long BgL_arg2302z00_9725;

								BgL_arg2302z00_9725 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1712z00_9724),
									BgL_arg2302z00_9725);
							}
							{	/* Liveness/types.scm 59 */
								BgL_objectz00_bglt BgL_tmpz00_17904;

								BgL_tmpz00_17904 = ((BgL_objectz00_bglt) BgL_new1712z00_9724);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17904, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1712z00_9724);
							BgL_auxz00_17899 = BgL_new1712z00_9724;
						}
						BgL_tmp1713z00_9722 = ((BgL_appz00_bglt) BgL_auxz00_17899);
					}
					BgL_wide1714z00_9723 =
						((BgL_appzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_appzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 59 */
						obj_t BgL_auxz00_17912;
						BgL_objectz00_bglt BgL_tmpz00_17910;

						BgL_auxz00_17912 = ((obj_t) BgL_wide1714z00_9723);
						BgL_tmpz00_17910 = ((BgL_objectz00_bglt) BgL_tmp1713z00_9722);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_17910, BgL_auxz00_17912);
					}
					((BgL_objectz00_bglt) BgL_tmp1713z00_9722);
					{	/* Liveness/types.scm 59 */
						long BgL_arg2301z00_9726;

						BgL_arg2301z00_9726 =
							BGL_CLASS_NUM(BGl_appzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1713z00_9722), BgL_arg2301z00_9726);
					}
					BgL_new1715z00_9721 = ((BgL_appz00_bglt) BgL_tmp1713z00_9722);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1715z00_9721)))->BgL_locz00) =
					((obj_t) BgL_loc1194z00_8248), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1715z00_9721)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1195z00_8249)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1715z00_9721)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1196zd2_8250), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1715z00_9721)))->BgL_keyz00) =
					((obj_t) BgL_key1197z00_8251), BUNSPEC);
				((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_new1715z00_9721)))->
						BgL_funz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_fun1198z00_8252)), BUNSPEC);
				((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_new1715z00_9721)))->
						BgL_argsz00) = ((obj_t) BgL_args1199z00_8253), BUNSPEC);
				((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_new1715z00_9721)))->
						BgL_stackablez00) = ((obj_t) BgL_stackable1200z00_8254), BUNSPEC);
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_17936;

					{
						obj_t BgL_auxz00_17937;

						{	/* Liveness/types.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_17938;

							BgL_tmpz00_17938 = ((BgL_objectz00_bglt) BgL_new1715z00_9721);
							BgL_auxz00_17937 = BGL_OBJECT_WIDENING(BgL_tmpz00_17938);
						}
						BgL_auxz00_17936 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17937);
					}
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17936))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1201z00_8255)), BUNSPEC);
				}
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_17944;

					{
						obj_t BgL_auxz00_17945;

						{	/* Liveness/types.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_17946;

							BgL_tmpz00_17946 = ((BgL_objectz00_bglt) BgL_new1715z00_9721);
							BgL_auxz00_17945 = BGL_OBJECT_WIDENING(BgL_tmpz00_17946);
						}
						BgL_auxz00_17944 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17945);
					}
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17944))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1202z00_8256)), BUNSPEC);
				}
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_17952;

					{
						obj_t BgL_auxz00_17953;

						{	/* Liveness/types.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_17954;

							BgL_tmpz00_17954 = ((BgL_objectz00_bglt) BgL_new1715z00_9721);
							BgL_auxz00_17953 = BGL_OBJECT_WIDENING(BgL_tmpz00_17954);
						}
						BgL_auxz00_17952 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17953);
					}
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17952))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1203z00_8257)), BUNSPEC);
				}
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_17960;

					{
						obj_t BgL_auxz00_17961;

						{	/* Liveness/types.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_17962;

							BgL_tmpz00_17962 = ((BgL_objectz00_bglt) BgL_new1715z00_9721);
							BgL_auxz00_17961 = BGL_OBJECT_WIDENING(BgL_tmpz00_17962);
						}
						BgL_auxz00_17960 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17961);
					}
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17960))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1204z00_8258)), BUNSPEC);
				}
				return BgL_new1715z00_9721;
			}
		}

	}



/* &<@anonymous:2339> */
	obj_t BGl_z62zc3z04anonymousza32339ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8259)
	{
		{	/* Liveness/types.scm 59 */
			return BNIL;
		}

	}



/* &lambda2338 */
	obj_t BGl_z62lambda2338z62zzliveness_typesz00(obj_t BgL_envz00_8260,
		obj_t BgL_oz00_8261, obj_t BgL_vz00_8262)
	{
		{	/* Liveness/types.scm 59 */
			{
				BgL_appzf2livenesszf2_bglt BgL_auxz00_17968;

				{
					obj_t BgL_auxz00_17969;

					{	/* Liveness/types.scm 59 */
						BgL_objectz00_bglt BgL_tmpz00_17970;

						BgL_tmpz00_17970 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_8261));
						BgL_auxz00_17969 = BGL_OBJECT_WIDENING(BgL_tmpz00_17970);
					}
					BgL_auxz00_17968 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17969);
				}
				return
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17968))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8262)), BUNSPEC);
			}
		}

	}



/* &lambda2337 */
	obj_t BGl_z62lambda2337z62zzliveness_typesz00(obj_t BgL_envz00_8263,
		obj_t BgL_oz00_8264)
	{
		{	/* Liveness/types.scm 59 */
			{
				BgL_appzf2livenesszf2_bglt BgL_auxz00_17977;

				{
					obj_t BgL_auxz00_17978;

					{	/* Liveness/types.scm 59 */
						BgL_objectz00_bglt BgL_tmpz00_17979;

						BgL_tmpz00_17979 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_8264));
						BgL_auxz00_17978 = BGL_OBJECT_WIDENING(BgL_tmpz00_17979);
					}
					BgL_auxz00_17977 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17978);
				}
				return
					(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17977))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2330> */
	obj_t BGl_z62zc3z04anonymousza32330ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8265)
	{
		{	/* Liveness/types.scm 59 */
			return BNIL;
		}

	}



/* &lambda2329 */
	obj_t BGl_z62lambda2329z62zzliveness_typesz00(obj_t BgL_envz00_8266,
		obj_t BgL_oz00_8267, obj_t BgL_vz00_8268)
	{
		{	/* Liveness/types.scm 59 */
			{
				BgL_appzf2livenesszf2_bglt BgL_auxz00_17985;

				{
					obj_t BgL_auxz00_17986;

					{	/* Liveness/types.scm 59 */
						BgL_objectz00_bglt BgL_tmpz00_17987;

						BgL_tmpz00_17987 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_8267));
						BgL_auxz00_17986 = BGL_OBJECT_WIDENING(BgL_tmpz00_17987);
					}
					BgL_auxz00_17985 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17986);
				}
				return
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17985))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8268)), BUNSPEC);
			}
		}

	}



/* &lambda2328 */
	obj_t BGl_z62lambda2328z62zzliveness_typesz00(obj_t BgL_envz00_8269,
		obj_t BgL_oz00_8270)
	{
		{	/* Liveness/types.scm 59 */
			{
				BgL_appzf2livenesszf2_bglt BgL_auxz00_17994;

				{
					obj_t BgL_auxz00_17995;

					{	/* Liveness/types.scm 59 */
						BgL_objectz00_bglt BgL_tmpz00_17996;

						BgL_tmpz00_17996 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_8270));
						BgL_auxz00_17995 = BGL_OBJECT_WIDENING(BgL_tmpz00_17996);
					}
					BgL_auxz00_17994 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_17995);
				}
				return
					(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_17994))->BgL_inz00);
			}
		}

	}



/* &<@anonymous:2323> */
	obj_t BGl_z62zc3z04anonymousza32323ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8271)
	{
		{	/* Liveness/types.scm 59 */
			return BNIL;
		}

	}



/* &lambda2322 */
	obj_t BGl_z62lambda2322z62zzliveness_typesz00(obj_t BgL_envz00_8272,
		obj_t BgL_oz00_8273, obj_t BgL_vz00_8274)
	{
		{	/* Liveness/types.scm 59 */
			{
				BgL_appzf2livenesszf2_bglt BgL_auxz00_18002;

				{
					obj_t BgL_auxz00_18003;

					{	/* Liveness/types.scm 59 */
						BgL_objectz00_bglt BgL_tmpz00_18004;

						BgL_tmpz00_18004 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_8273));
						BgL_auxz00_18003 = BGL_OBJECT_WIDENING(BgL_tmpz00_18004);
					}
					BgL_auxz00_18002 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_18003);
				}
				return
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18002))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8274)), BUNSPEC);
			}
		}

	}



/* &lambda2321 */
	obj_t BGl_z62lambda2321z62zzliveness_typesz00(obj_t BgL_envz00_8275,
		obj_t BgL_oz00_8276)
	{
		{	/* Liveness/types.scm 59 */
			{
				BgL_appzf2livenesszf2_bglt BgL_auxz00_18011;

				{
					obj_t BgL_auxz00_18012;

					{	/* Liveness/types.scm 59 */
						BgL_objectz00_bglt BgL_tmpz00_18013;

						BgL_tmpz00_18013 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_8276));
						BgL_auxz00_18012 = BGL_OBJECT_WIDENING(BgL_tmpz00_18013);
					}
					BgL_auxz00_18011 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_18012);
				}
				return
					(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18011))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2316> */
	obj_t BGl_z62zc3z04anonymousza32316ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8277)
	{
		{	/* Liveness/types.scm 59 */
			return BNIL;
		}

	}



/* &lambda2315 */
	obj_t BGl_z62lambda2315z62zzliveness_typesz00(obj_t BgL_envz00_8278,
		obj_t BgL_oz00_8279, obj_t BgL_vz00_8280)
	{
		{	/* Liveness/types.scm 59 */
			{
				BgL_appzf2livenesszf2_bglt BgL_auxz00_18019;

				{
					obj_t BgL_auxz00_18020;

					{	/* Liveness/types.scm 59 */
						BgL_objectz00_bglt BgL_tmpz00_18021;

						BgL_tmpz00_18021 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_8279));
						BgL_auxz00_18020 = BGL_OBJECT_WIDENING(BgL_tmpz00_18021);
					}
					BgL_auxz00_18019 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_18020);
				}
				return
					((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18019))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8280)), BUNSPEC);
			}
		}

	}



/* &lambda2314 */
	obj_t BGl_z62lambda2314z62zzliveness_typesz00(obj_t BgL_envz00_8281,
		obj_t BgL_oz00_8282)
	{
		{	/* Liveness/types.scm 59 */
			{
				BgL_appzf2livenesszf2_bglt BgL_auxz00_18028;

				{
					obj_t BgL_auxz00_18029;

					{	/* Liveness/types.scm 59 */
						BgL_objectz00_bglt BgL_tmpz00_18030;

						BgL_tmpz00_18030 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_oz00_8282));
						BgL_auxz00_18029 = BGL_OBJECT_WIDENING(BgL_tmpz00_18030);
					}
					BgL_auxz00_18028 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_18029);
				}
				return
					(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18028))->
					BgL_defz00);
			}
		}

	}



/* &lambda2259 */
	BgL_sequencez00_bglt BGl_z62lambda2259z62zzliveness_typesz00(obj_t
		BgL_envz00_8283, obj_t BgL_o1192z00_8284)
	{
		{	/* Liveness/types.scm 53 */
			{	/* Liveness/types.scm 53 */
				long BgL_arg2260z00_9740;

				{	/* Liveness/types.scm 53 */
					obj_t BgL_arg2261z00_9741;

					{	/* Liveness/types.scm 53 */
						obj_t BgL_arg2262z00_9742;

						{	/* Liveness/types.scm 53 */
							obj_t BgL_arg1815z00_9743;
							long BgL_arg1816z00_9744;

							BgL_arg1815z00_9743 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 53 */
								long BgL_arg1817z00_9745;

								BgL_arg1817z00_9745 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt) BgL_o1192z00_8284)));
								BgL_arg1816z00_9744 = (BgL_arg1817z00_9745 - OBJECT_TYPE);
							}
							BgL_arg2262z00_9742 =
								VECTOR_REF(BgL_arg1815z00_9743, BgL_arg1816z00_9744);
						}
						BgL_arg2261z00_9741 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2262z00_9742);
					}
					{	/* Liveness/types.scm 53 */
						obj_t BgL_tmpz00_18043;

						BgL_tmpz00_18043 = ((obj_t) BgL_arg2261z00_9741);
						BgL_arg2260z00_9740 = BGL_CLASS_NUM(BgL_tmpz00_18043);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_sequencez00_bglt) BgL_o1192z00_8284)), BgL_arg2260z00_9740);
			}
			{	/* Liveness/types.scm 53 */
				BgL_objectz00_bglt BgL_tmpz00_18049;

				BgL_tmpz00_18049 =
					((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_o1192z00_8284));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18049, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_o1192z00_8284));
			return
				((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt) BgL_o1192z00_8284));
		}

	}



/* &<@anonymous:2258> */
	obj_t BGl_z62zc3z04anonymousza32258ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8285, obj_t BgL_new1191z00_8286)
	{
		{	/* Liveness/types.scm 53 */
			{
				BgL_sequencez00_bglt BgL_auxz00_18057;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_sequencez00_bglt) BgL_new1191z00_8286))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_18061;

					{	/* Liveness/types.scm 53 */
						obj_t BgL_classz00_9747;

						BgL_classz00_9747 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 53 */
							obj_t BgL__ortest_1117z00_9748;

							BgL__ortest_1117z00_9748 = BGL_CLASS_NIL(BgL_classz00_9747);
							if (CBOOL(BgL__ortest_1117z00_9748))
								{	/* Liveness/types.scm 53 */
									BgL_auxz00_18061 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9748);
								}
							else
								{	/* Liveness/types.scm 53 */
									BgL_auxz00_18061 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9747));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_sequencez00_bglt) BgL_new1191z00_8286))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_18061), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_sequencez00_bglt) BgL_new1191z00_8286))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_sequencez00_bglt)
										BgL_new1191z00_8286))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sequencez00_bglt)
							COBJECT(((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt)
										BgL_new1191z00_8286))))->BgL_nodesz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_sequencez00_bglt)
							COBJECT(((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt)
										BgL_new1191z00_8286))))->BgL_unsafez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_sequencez00_bglt)
							COBJECT(((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt)
										BgL_new1191z00_8286))))->BgL_metaz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18086;

					{
						obj_t BgL_auxz00_18087;

						{	/* Liveness/types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_18088;

							BgL_tmpz00_18088 =
								((BgL_objectz00_bglt)
								((BgL_sequencez00_bglt) BgL_new1191z00_8286));
							BgL_auxz00_18087 = BGL_OBJECT_WIDENING(BgL_tmpz00_18088);
						}
						BgL_auxz00_18086 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18087);
					}
					((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18086))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18094;

					{
						obj_t BgL_auxz00_18095;

						{	/* Liveness/types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_18096;

							BgL_tmpz00_18096 =
								((BgL_objectz00_bglt)
								((BgL_sequencez00_bglt) BgL_new1191z00_8286));
							BgL_auxz00_18095 = BGL_OBJECT_WIDENING(BgL_tmpz00_18096);
						}
						BgL_auxz00_18094 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18095);
					}
					((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18094))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18102;

					{
						obj_t BgL_auxz00_18103;

						{	/* Liveness/types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_18104;

							BgL_tmpz00_18104 =
								((BgL_objectz00_bglt)
								((BgL_sequencez00_bglt) BgL_new1191z00_8286));
							BgL_auxz00_18103 = BGL_OBJECT_WIDENING(BgL_tmpz00_18104);
						}
						BgL_auxz00_18102 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18103);
					}
					((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18102))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18110;

					{
						obj_t BgL_auxz00_18111;

						{	/* Liveness/types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_18112;

							BgL_tmpz00_18112 =
								((BgL_objectz00_bglt)
								((BgL_sequencez00_bglt) BgL_new1191z00_8286));
							BgL_auxz00_18111 = BGL_OBJECT_WIDENING(BgL_tmpz00_18112);
						}
						BgL_auxz00_18110 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18111);
					}
					((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18110))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_18057 = ((BgL_sequencez00_bglt) BgL_new1191z00_8286);
				return ((obj_t) BgL_auxz00_18057);
			}
		}

	}



/* &lambda2256 */
	BgL_sequencez00_bglt BGl_z62lambda2256z62zzliveness_typesz00(obj_t
		BgL_envz00_8287, obj_t BgL_o1188z00_8288)
	{
		{	/* Liveness/types.scm 53 */
			{	/* Liveness/types.scm 53 */
				BgL_sequencezf2livenesszf2_bglt BgL_wide1190z00_9750;

				BgL_wide1190z00_9750 =
					((BgL_sequencezf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sequencezf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 53 */
					obj_t BgL_auxz00_18125;
					BgL_objectz00_bglt BgL_tmpz00_18121;

					BgL_auxz00_18125 = ((obj_t) BgL_wide1190z00_9750);
					BgL_tmpz00_18121 =
						((BgL_objectz00_bglt)
						((BgL_sequencez00_bglt)
							((BgL_sequencez00_bglt) BgL_o1188z00_8288)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18121, BgL_auxz00_18125);
				}
				((BgL_objectz00_bglt)
					((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt) BgL_o1188z00_8288)));
				{	/* Liveness/types.scm 53 */
					long BgL_arg2257z00_9751;

					BgL_arg2257z00_9751 =
						BGL_CLASS_NUM(BGl_sequencezf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_sequencez00_bglt)
								((BgL_sequencez00_bglt) BgL_o1188z00_8288))),
						BgL_arg2257z00_9751);
				}
				return
					((BgL_sequencez00_bglt)
					((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt) BgL_o1188z00_8288)));
			}
		}

	}



/* &lambda2253 */
	BgL_sequencez00_bglt BGl_z62lambda2253z62zzliveness_typesz00(obj_t
		BgL_envz00_8289, obj_t BgL_loc1177z00_8290, obj_t BgL_type1178z00_8291,
		obj_t BgL_sidezd2effect1179zd2_8292, obj_t BgL_key1180z00_8293,
		obj_t BgL_nodes1181z00_8294, obj_t BgL_unsafe1182z00_8295,
		obj_t BgL_meta1183z00_8296, obj_t BgL_def1184z00_8297,
		obj_t BgL_use1185z00_8298, obj_t BgL_in1186z00_8299,
		obj_t BgL_out1187z00_8300)
	{
		{	/* Liveness/types.scm 53 */
			{	/* Liveness/types.scm 53 */
				bool_t BgL_unsafe1182z00_9754;

				BgL_unsafe1182z00_9754 = CBOOL(BgL_unsafe1182z00_8295);
				{	/* Liveness/types.scm 53 */
					BgL_sequencez00_bglt BgL_new1710z00_9760;

					{	/* Liveness/types.scm 53 */
						BgL_sequencez00_bglt BgL_tmp1708z00_9761;
						BgL_sequencezf2livenesszf2_bglt BgL_wide1709z00_9762;

						{
							BgL_sequencez00_bglt BgL_auxz00_18140;

							{	/* Liveness/types.scm 53 */
								BgL_sequencez00_bglt BgL_new1707z00_9763;

								BgL_new1707z00_9763 =
									((BgL_sequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sequencez00_bgl))));
								{	/* Liveness/types.scm 53 */
									long BgL_arg2255z00_9764;

									BgL_arg2255z00_9764 =
										BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1707z00_9763),
										BgL_arg2255z00_9764);
								}
								{	/* Liveness/types.scm 53 */
									BgL_objectz00_bglt BgL_tmpz00_18145;

									BgL_tmpz00_18145 = ((BgL_objectz00_bglt) BgL_new1707z00_9763);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18145, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1707z00_9763);
								BgL_auxz00_18140 = BgL_new1707z00_9763;
							}
							BgL_tmp1708z00_9761 = ((BgL_sequencez00_bglt) BgL_auxz00_18140);
						}
						BgL_wide1709z00_9762 =
							((BgL_sequencezf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sequencezf2livenesszf2_bgl))));
						{	/* Liveness/types.scm 53 */
							obj_t BgL_auxz00_18153;
							BgL_objectz00_bglt BgL_tmpz00_18151;

							BgL_auxz00_18153 = ((obj_t) BgL_wide1709z00_9762);
							BgL_tmpz00_18151 = ((BgL_objectz00_bglt) BgL_tmp1708z00_9761);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18151, BgL_auxz00_18153);
						}
						((BgL_objectz00_bglt) BgL_tmp1708z00_9761);
						{	/* Liveness/types.scm 53 */
							long BgL_arg2254z00_9765;

							BgL_arg2254z00_9765 =
								BGL_CLASS_NUM(BGl_sequencezf2livenesszf2zzliveness_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1708z00_9761),
								BgL_arg2254z00_9765);
						}
						BgL_new1710z00_9760 = ((BgL_sequencez00_bglt) BgL_tmp1708z00_9761);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1710z00_9760)))->BgL_locz00) =
						((obj_t) BgL_loc1177z00_8290), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1710z00_9760)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1178z00_8291)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1710z00_9760)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1179zd2_8292), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1710z00_9760)))->BgL_keyz00) =
						((obj_t) BgL_key1180z00_8293), BUNSPEC);
					((((BgL_sequencez00_bglt) COBJECT(((BgL_sequencez00_bglt)
										BgL_new1710z00_9760)))->BgL_nodesz00) =
						((obj_t) ((obj_t) BgL_nodes1181z00_8294)), BUNSPEC);
					((((BgL_sequencez00_bglt) COBJECT(((BgL_sequencez00_bglt)
										BgL_new1710z00_9760)))->BgL_unsafez00) =
						((bool_t) BgL_unsafe1182z00_9754), BUNSPEC);
					((((BgL_sequencez00_bglt) COBJECT(((BgL_sequencez00_bglt)
										BgL_new1710z00_9760)))->BgL_metaz00) =
						((obj_t) ((obj_t) BgL_meta1183z00_8296)), BUNSPEC);
					{
						BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18178;

						{
							obj_t BgL_auxz00_18179;

							{	/* Liveness/types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_18180;

								BgL_tmpz00_18180 = ((BgL_objectz00_bglt) BgL_new1710z00_9760);
								BgL_auxz00_18179 = BGL_OBJECT_WIDENING(BgL_tmpz00_18180);
							}
							BgL_auxz00_18178 =
								((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18179);
						}
						((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18178))->
								BgL_defz00) = ((obj_t) ((obj_t) BgL_def1184z00_8297)), BUNSPEC);
					}
					{
						BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18186;

						{
							obj_t BgL_auxz00_18187;

							{	/* Liveness/types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_18188;

								BgL_tmpz00_18188 = ((BgL_objectz00_bglt) BgL_new1710z00_9760);
								BgL_auxz00_18187 = BGL_OBJECT_WIDENING(BgL_tmpz00_18188);
							}
							BgL_auxz00_18186 =
								((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18187);
						}
						((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18186))->
								BgL_usez00) = ((obj_t) ((obj_t) BgL_use1185z00_8298)), BUNSPEC);
					}
					{
						BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18194;

						{
							obj_t BgL_auxz00_18195;

							{	/* Liveness/types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_18196;

								BgL_tmpz00_18196 = ((BgL_objectz00_bglt) BgL_new1710z00_9760);
								BgL_auxz00_18195 = BGL_OBJECT_WIDENING(BgL_tmpz00_18196);
							}
							BgL_auxz00_18194 =
								((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18195);
						}
						((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18194))->
								BgL_inz00) = ((obj_t) ((obj_t) BgL_in1186z00_8299)), BUNSPEC);
					}
					{
						BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18202;

						{
							obj_t BgL_auxz00_18203;

							{	/* Liveness/types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_18204;

								BgL_tmpz00_18204 = ((BgL_objectz00_bglt) BgL_new1710z00_9760);
								BgL_auxz00_18203 = BGL_OBJECT_WIDENING(BgL_tmpz00_18204);
							}
							BgL_auxz00_18202 =
								((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18203);
						}
						((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18202))->
								BgL_outz00) = ((obj_t) ((obj_t) BgL_out1187z00_8300)), BUNSPEC);
					}
					return BgL_new1710z00_9760;
				}
			}
		}

	}



/* &<@anonymous:2292> */
	obj_t BGl_z62zc3z04anonymousza32292ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8301)
	{
		{	/* Liveness/types.scm 53 */
			return BNIL;
		}

	}



/* &lambda2291 */
	obj_t BGl_z62lambda2291z62zzliveness_typesz00(obj_t BgL_envz00_8302,
		obj_t BgL_oz00_8303, obj_t BgL_vz00_8304)
	{
		{	/* Liveness/types.scm 53 */
			{
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18210;

				{
					obj_t BgL_auxz00_18211;

					{	/* Liveness/types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_18212;

						BgL_tmpz00_18212 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_8303));
						BgL_auxz00_18211 = BGL_OBJECT_WIDENING(BgL_tmpz00_18212);
					}
					BgL_auxz00_18210 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18211);
				}
				return
					((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18210))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8304)), BUNSPEC);
			}
		}

	}



/* &lambda2290 */
	obj_t BGl_z62lambda2290z62zzliveness_typesz00(obj_t BgL_envz00_8305,
		obj_t BgL_oz00_8306)
	{
		{	/* Liveness/types.scm 53 */
			{
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18219;

				{
					obj_t BgL_auxz00_18220;

					{	/* Liveness/types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_18221;

						BgL_tmpz00_18221 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_8306));
						BgL_auxz00_18220 = BGL_OBJECT_WIDENING(BgL_tmpz00_18221);
					}
					BgL_auxz00_18219 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18220);
				}
				return
					(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18219))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2284> */
	obj_t BGl_z62zc3z04anonymousza32284ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8307)
	{
		{	/* Liveness/types.scm 53 */
			return BNIL;
		}

	}



/* &lambda2283 */
	obj_t BGl_z62lambda2283z62zzliveness_typesz00(obj_t BgL_envz00_8308,
		obj_t BgL_oz00_8309, obj_t BgL_vz00_8310)
	{
		{	/* Liveness/types.scm 53 */
			{
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18227;

				{
					obj_t BgL_auxz00_18228;

					{	/* Liveness/types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_18229;

						BgL_tmpz00_18229 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_8309));
						BgL_auxz00_18228 = BGL_OBJECT_WIDENING(BgL_tmpz00_18229);
					}
					BgL_auxz00_18227 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18228);
				}
				return
					((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18227))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8310)), BUNSPEC);
			}
		}

	}



/* &lambda2282 */
	obj_t BGl_z62lambda2282z62zzliveness_typesz00(obj_t BgL_envz00_8311,
		obj_t BgL_oz00_8312)
	{
		{	/* Liveness/types.scm 53 */
			{
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18236;

				{
					obj_t BgL_auxz00_18237;

					{	/* Liveness/types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_18238;

						BgL_tmpz00_18238 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_8312));
						BgL_auxz00_18237 = BGL_OBJECT_WIDENING(BgL_tmpz00_18238);
					}
					BgL_auxz00_18236 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18237);
				}
				return
					(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18236))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2276> */
	obj_t BGl_z62zc3z04anonymousza32276ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8313)
	{
		{	/* Liveness/types.scm 53 */
			return BNIL;
		}

	}



/* &lambda2275 */
	obj_t BGl_z62lambda2275z62zzliveness_typesz00(obj_t BgL_envz00_8314,
		obj_t BgL_oz00_8315, obj_t BgL_vz00_8316)
	{
		{	/* Liveness/types.scm 53 */
			{
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18244;

				{
					obj_t BgL_auxz00_18245;

					{	/* Liveness/types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_18246;

						BgL_tmpz00_18246 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_8315));
						BgL_auxz00_18245 = BGL_OBJECT_WIDENING(BgL_tmpz00_18246);
					}
					BgL_auxz00_18244 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18245);
				}
				return
					((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18244))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8316)), BUNSPEC);
			}
		}

	}



/* &lambda2274 */
	obj_t BGl_z62lambda2274z62zzliveness_typesz00(obj_t BgL_envz00_8317,
		obj_t BgL_oz00_8318)
	{
		{	/* Liveness/types.scm 53 */
			{
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18253;

				{
					obj_t BgL_auxz00_18254;

					{	/* Liveness/types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_18255;

						BgL_tmpz00_18255 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_8318));
						BgL_auxz00_18254 = BGL_OBJECT_WIDENING(BgL_tmpz00_18255);
					}
					BgL_auxz00_18253 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18254);
				}
				return
					(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18253))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2269> */
	obj_t BGl_z62zc3z04anonymousza32269ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8319)
	{
		{	/* Liveness/types.scm 53 */
			return BNIL;
		}

	}



/* &lambda2268 */
	obj_t BGl_z62lambda2268z62zzliveness_typesz00(obj_t BgL_envz00_8320,
		obj_t BgL_oz00_8321, obj_t BgL_vz00_8322)
	{
		{	/* Liveness/types.scm 53 */
			{
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18261;

				{
					obj_t BgL_auxz00_18262;

					{	/* Liveness/types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_18263;

						BgL_tmpz00_18263 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_8321));
						BgL_auxz00_18262 = BGL_OBJECT_WIDENING(BgL_tmpz00_18263);
					}
					BgL_auxz00_18261 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18262);
				}
				return
					((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18261))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8322)), BUNSPEC);
			}
		}

	}



/* &lambda2267 */
	obj_t BGl_z62lambda2267z62zzliveness_typesz00(obj_t BgL_envz00_8323,
		obj_t BgL_oz00_8324)
	{
		{	/* Liveness/types.scm 53 */
			{
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_18270;

				{
					obj_t BgL_auxz00_18271;

					{	/* Liveness/types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_18272;

						BgL_tmpz00_18272 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_8324));
						BgL_auxz00_18271 = BGL_OBJECT_WIDENING(BgL_tmpz00_18272);
					}
					BgL_auxz00_18270 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_18271);
				}
				return
					(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18270))->
					BgL_defz00);
			}
		}

	}



/* &lambda2215 */
	BgL_kwotez00_bglt BGl_z62lambda2215z62zzliveness_typesz00(obj_t
		BgL_envz00_8325, obj_t BgL_o1175z00_8326)
	{
		{	/* Liveness/types.scm 47 */
			{	/* Liveness/types.scm 47 */
				long BgL_arg2216z00_9779;

				{	/* Liveness/types.scm 47 */
					obj_t BgL_arg2217z00_9780;

					{	/* Liveness/types.scm 47 */
						obj_t BgL_arg2218z00_9781;

						{	/* Liveness/types.scm 47 */
							obj_t BgL_arg1815z00_9782;
							long BgL_arg1816z00_9783;

							BgL_arg1815z00_9782 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 47 */
								long BgL_arg1817z00_9784;

								BgL_arg1817z00_9784 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_kwotez00_bglt) BgL_o1175z00_8326)));
								BgL_arg1816z00_9783 = (BgL_arg1817z00_9784 - OBJECT_TYPE);
							}
							BgL_arg2218z00_9781 =
								VECTOR_REF(BgL_arg1815z00_9782, BgL_arg1816z00_9783);
						}
						BgL_arg2217z00_9780 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2218z00_9781);
					}
					{	/* Liveness/types.scm 47 */
						obj_t BgL_tmpz00_18285;

						BgL_tmpz00_18285 = ((obj_t) BgL_arg2217z00_9780);
						BgL_arg2216z00_9779 = BGL_CLASS_NUM(BgL_tmpz00_18285);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_kwotez00_bglt) BgL_o1175z00_8326)), BgL_arg2216z00_9779);
			}
			{	/* Liveness/types.scm 47 */
				BgL_objectz00_bglt BgL_tmpz00_18291;

				BgL_tmpz00_18291 =
					((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_o1175z00_8326));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18291, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_o1175z00_8326));
			return ((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_o1175z00_8326));
		}

	}



/* &<@anonymous:2214> */
	obj_t BGl_z62zc3z04anonymousza32214ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8327, obj_t BgL_new1174z00_8328)
	{
		{	/* Liveness/types.scm 47 */
			{
				BgL_kwotez00_bglt BgL_auxz00_18299;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_kwotez00_bglt) BgL_new1174z00_8328))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_18303;

					{	/* Liveness/types.scm 47 */
						obj_t BgL_classz00_9786;

						BgL_classz00_9786 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 47 */
							obj_t BgL__ortest_1117z00_9787;

							BgL__ortest_1117z00_9787 = BGL_CLASS_NIL(BgL_classz00_9786);
							if (CBOOL(BgL__ortest_1117z00_9787))
								{	/* Liveness/types.scm 47 */
									BgL_auxz00_18303 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9787);
								}
							else
								{	/* Liveness/types.scm 47 */
									BgL_auxz00_18303 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9786));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_kwotez00_bglt) BgL_new1174z00_8328))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_18303), BUNSPEC);
				}
				((((BgL_kwotez00_bglt) COBJECT(
								((BgL_kwotez00_bglt)
									((BgL_kwotez00_bglt) BgL_new1174z00_8328))))->BgL_valuez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18316;

					{
						obj_t BgL_auxz00_18317;

						{	/* Liveness/types.scm 47 */
							BgL_objectz00_bglt BgL_tmpz00_18318;

							BgL_tmpz00_18318 =
								((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt) BgL_new1174z00_8328));
							BgL_auxz00_18317 = BGL_OBJECT_WIDENING(BgL_tmpz00_18318);
						}
						BgL_auxz00_18316 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18317);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18316))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18324;

					{
						obj_t BgL_auxz00_18325;

						{	/* Liveness/types.scm 47 */
							BgL_objectz00_bglt BgL_tmpz00_18326;

							BgL_tmpz00_18326 =
								((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt) BgL_new1174z00_8328));
							BgL_auxz00_18325 = BGL_OBJECT_WIDENING(BgL_tmpz00_18326);
						}
						BgL_auxz00_18324 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18325);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18324))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18332;

					{
						obj_t BgL_auxz00_18333;

						{	/* Liveness/types.scm 47 */
							BgL_objectz00_bglt BgL_tmpz00_18334;

							BgL_tmpz00_18334 =
								((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt) BgL_new1174z00_8328));
							BgL_auxz00_18333 = BGL_OBJECT_WIDENING(BgL_tmpz00_18334);
						}
						BgL_auxz00_18332 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18333);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18332))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18340;

					{
						obj_t BgL_auxz00_18341;

						{	/* Liveness/types.scm 47 */
							BgL_objectz00_bglt BgL_tmpz00_18342;

							BgL_tmpz00_18342 =
								((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt) BgL_new1174z00_8328));
							BgL_auxz00_18341 = BGL_OBJECT_WIDENING(BgL_tmpz00_18342);
						}
						BgL_auxz00_18340 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18341);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18340))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_18299 = ((BgL_kwotez00_bglt) BgL_new1174z00_8328);
				return ((obj_t) BgL_auxz00_18299);
			}
		}

	}



/* &lambda2212 */
	BgL_kwotez00_bglt BGl_z62lambda2212z62zzliveness_typesz00(obj_t
		BgL_envz00_8329, obj_t BgL_o1171z00_8330)
	{
		{	/* Liveness/types.scm 47 */
			{	/* Liveness/types.scm 47 */
				BgL_kwotezf2livenesszf2_bglt BgL_wide1173z00_9789;

				BgL_wide1173z00_9789 =
					((BgL_kwotezf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_kwotezf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 47 */
					obj_t BgL_auxz00_18355;
					BgL_objectz00_bglt BgL_tmpz00_18351;

					BgL_auxz00_18355 = ((obj_t) BgL_wide1173z00_9789);
					BgL_tmpz00_18351 =
						((BgL_objectz00_bglt)
						((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_o1171z00_8330)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18351, BgL_auxz00_18355);
				}
				((BgL_objectz00_bglt)
					((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_o1171z00_8330)));
				{	/* Liveness/types.scm 47 */
					long BgL_arg2213z00_9790;

					BgL_arg2213z00_9790 =
						BGL_CLASS_NUM(BGl_kwotezf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_kwotez00_bglt)
								((BgL_kwotez00_bglt) BgL_o1171z00_8330))), BgL_arg2213z00_9790);
				}
				return
					((BgL_kwotez00_bglt)
					((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_o1171z00_8330)));
			}
		}

	}



/* &lambda2209 */
	BgL_kwotez00_bglt BGl_z62lambda2209z62zzliveness_typesz00(obj_t
		BgL_envz00_8331, obj_t BgL_loc1164z00_8332, obj_t BgL_type1165z00_8333,
		obj_t BgL_value1166z00_8334, obj_t BgL_def1167z00_8335,
		obj_t BgL_use1168z00_8336, obj_t BgL_in1169z00_8337,
		obj_t BgL_out1170z00_8338)
	{
		{	/* Liveness/types.scm 47 */
			{	/* Liveness/types.scm 47 */
				BgL_kwotez00_bglt BgL_new1705z00_9796;

				{	/* Liveness/types.scm 47 */
					BgL_kwotez00_bglt BgL_tmp1703z00_9797;
					BgL_kwotezf2livenesszf2_bglt BgL_wide1704z00_9798;

					{
						BgL_kwotez00_bglt BgL_auxz00_18369;

						{	/* Liveness/types.scm 47 */
							BgL_kwotez00_bglt BgL_new1702z00_9799;

							BgL_new1702z00_9799 =
								((BgL_kwotez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_kwotez00_bgl))));
							{	/* Liveness/types.scm 47 */
								long BgL_arg2211z00_9800;

								BgL_arg2211z00_9800 = BGL_CLASS_NUM(BGl_kwotez00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1702z00_9799),
									BgL_arg2211z00_9800);
							}
							{	/* Liveness/types.scm 47 */
								BgL_objectz00_bglt BgL_tmpz00_18374;

								BgL_tmpz00_18374 = ((BgL_objectz00_bglt) BgL_new1702z00_9799);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18374, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1702z00_9799);
							BgL_auxz00_18369 = BgL_new1702z00_9799;
						}
						BgL_tmp1703z00_9797 = ((BgL_kwotez00_bglt) BgL_auxz00_18369);
					}
					BgL_wide1704z00_9798 =
						((BgL_kwotezf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_kwotezf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 47 */
						obj_t BgL_auxz00_18382;
						BgL_objectz00_bglt BgL_tmpz00_18380;

						BgL_auxz00_18382 = ((obj_t) BgL_wide1704z00_9798);
						BgL_tmpz00_18380 = ((BgL_objectz00_bglt) BgL_tmp1703z00_9797);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18380, BgL_auxz00_18382);
					}
					((BgL_objectz00_bglt) BgL_tmp1703z00_9797);
					{	/* Liveness/types.scm 47 */
						long BgL_arg2210z00_9801;

						BgL_arg2210z00_9801 =
							BGL_CLASS_NUM(BGl_kwotezf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1703z00_9797), BgL_arg2210z00_9801);
					}
					BgL_new1705z00_9796 = ((BgL_kwotez00_bglt) BgL_tmp1703z00_9797);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1705z00_9796)))->BgL_locz00) =
					((obj_t) BgL_loc1164z00_8332), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1705z00_9796)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1165z00_8333)),
					BUNSPEC);
				((((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt)
									BgL_new1705z00_9796)))->BgL_valuez00) =
					((obj_t) BgL_value1166z00_8334), BUNSPEC);
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18397;

					{
						obj_t BgL_auxz00_18398;

						{	/* Liveness/types.scm 47 */
							BgL_objectz00_bglt BgL_tmpz00_18399;

							BgL_tmpz00_18399 = ((BgL_objectz00_bglt) BgL_new1705z00_9796);
							BgL_auxz00_18398 = BGL_OBJECT_WIDENING(BgL_tmpz00_18399);
						}
						BgL_auxz00_18397 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18398);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18397))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1167z00_8335)), BUNSPEC);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18405;

					{
						obj_t BgL_auxz00_18406;

						{	/* Liveness/types.scm 47 */
							BgL_objectz00_bglt BgL_tmpz00_18407;

							BgL_tmpz00_18407 = ((BgL_objectz00_bglt) BgL_new1705z00_9796);
							BgL_auxz00_18406 = BGL_OBJECT_WIDENING(BgL_tmpz00_18407);
						}
						BgL_auxz00_18405 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18406);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18405))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1168z00_8336)), BUNSPEC);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18413;

					{
						obj_t BgL_auxz00_18414;

						{	/* Liveness/types.scm 47 */
							BgL_objectz00_bglt BgL_tmpz00_18415;

							BgL_tmpz00_18415 = ((BgL_objectz00_bglt) BgL_new1705z00_9796);
							BgL_auxz00_18414 = BGL_OBJECT_WIDENING(BgL_tmpz00_18415);
						}
						BgL_auxz00_18413 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18414);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18413))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1169z00_8337)), BUNSPEC);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18421;

					{
						obj_t BgL_auxz00_18422;

						{	/* Liveness/types.scm 47 */
							BgL_objectz00_bglt BgL_tmpz00_18423;

							BgL_tmpz00_18423 = ((BgL_objectz00_bglt) BgL_new1705z00_9796);
							BgL_auxz00_18422 = BGL_OBJECT_WIDENING(BgL_tmpz00_18423);
						}
						BgL_auxz00_18421 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18422);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18421))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1170z00_8338)), BUNSPEC);
				}
				return BgL_new1705z00_9796;
			}
		}

	}



/* &<@anonymous:2246> */
	obj_t BGl_z62zc3z04anonymousza32246ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8339)
	{
		{	/* Liveness/types.scm 47 */
			return BNIL;
		}

	}



/* &lambda2245 */
	obj_t BGl_z62lambda2245z62zzliveness_typesz00(obj_t BgL_envz00_8340,
		obj_t BgL_oz00_8341, obj_t BgL_vz00_8342)
	{
		{	/* Liveness/types.scm 47 */
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18429;

				{
					obj_t BgL_auxz00_18430;

					{	/* Liveness/types.scm 47 */
						BgL_objectz00_bglt BgL_tmpz00_18431;

						BgL_tmpz00_18431 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_oz00_8341));
						BgL_auxz00_18430 = BGL_OBJECT_WIDENING(BgL_tmpz00_18431);
					}
					BgL_auxz00_18429 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18430);
				}
				return
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18429))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8342)), BUNSPEC);
			}
		}

	}



/* &lambda2244 */
	obj_t BGl_z62lambda2244z62zzliveness_typesz00(obj_t BgL_envz00_8343,
		obj_t BgL_oz00_8344)
	{
		{	/* Liveness/types.scm 47 */
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18438;

				{
					obj_t BgL_auxz00_18439;

					{	/* Liveness/types.scm 47 */
						BgL_objectz00_bglt BgL_tmpz00_18440;

						BgL_tmpz00_18440 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_oz00_8344));
						BgL_auxz00_18439 = BGL_OBJECT_WIDENING(BgL_tmpz00_18440);
					}
					BgL_auxz00_18438 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18439);
				}
				return
					(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18438))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2239> */
	obj_t BGl_z62zc3z04anonymousza32239ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8345)
	{
		{	/* Liveness/types.scm 47 */
			return BNIL;
		}

	}



/* &lambda2238 */
	obj_t BGl_z62lambda2238z62zzliveness_typesz00(obj_t BgL_envz00_8346,
		obj_t BgL_oz00_8347, obj_t BgL_vz00_8348)
	{
		{	/* Liveness/types.scm 47 */
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18446;

				{
					obj_t BgL_auxz00_18447;

					{	/* Liveness/types.scm 47 */
						BgL_objectz00_bglt BgL_tmpz00_18448;

						BgL_tmpz00_18448 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_oz00_8347));
						BgL_auxz00_18447 = BGL_OBJECT_WIDENING(BgL_tmpz00_18448);
					}
					BgL_auxz00_18446 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18447);
				}
				return
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18446))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8348)), BUNSPEC);
			}
		}

	}



/* &lambda2237 */
	obj_t BGl_z62lambda2237z62zzliveness_typesz00(obj_t BgL_envz00_8349,
		obj_t BgL_oz00_8350)
	{
		{	/* Liveness/types.scm 47 */
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18455;

				{
					obj_t BgL_auxz00_18456;

					{	/* Liveness/types.scm 47 */
						BgL_objectz00_bglt BgL_tmpz00_18457;

						BgL_tmpz00_18457 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_oz00_8350));
						BgL_auxz00_18456 = BGL_OBJECT_WIDENING(BgL_tmpz00_18457);
					}
					BgL_auxz00_18455 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18456);
				}
				return
					(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18455))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2232> */
	obj_t BGl_z62zc3z04anonymousza32232ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8351)
	{
		{	/* Liveness/types.scm 47 */
			return BNIL;
		}

	}



/* &lambda2231 */
	obj_t BGl_z62lambda2231z62zzliveness_typesz00(obj_t BgL_envz00_8352,
		obj_t BgL_oz00_8353, obj_t BgL_vz00_8354)
	{
		{	/* Liveness/types.scm 47 */
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18463;

				{
					obj_t BgL_auxz00_18464;

					{	/* Liveness/types.scm 47 */
						BgL_objectz00_bglt BgL_tmpz00_18465;

						BgL_tmpz00_18465 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_oz00_8353));
						BgL_auxz00_18464 = BGL_OBJECT_WIDENING(BgL_tmpz00_18465);
					}
					BgL_auxz00_18463 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18464);
				}
				return
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18463))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8354)), BUNSPEC);
			}
		}

	}



/* &lambda2230 */
	obj_t BGl_z62lambda2230z62zzliveness_typesz00(obj_t BgL_envz00_8355,
		obj_t BgL_oz00_8356)
	{
		{	/* Liveness/types.scm 47 */
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18472;

				{
					obj_t BgL_auxz00_18473;

					{	/* Liveness/types.scm 47 */
						BgL_objectz00_bglt BgL_tmpz00_18474;

						BgL_tmpz00_18474 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_oz00_8356));
						BgL_auxz00_18473 = BGL_OBJECT_WIDENING(BgL_tmpz00_18474);
					}
					BgL_auxz00_18472 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18473);
				}
				return
					(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18472))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2225> */
	obj_t BGl_z62zc3z04anonymousza32225ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8357)
	{
		{	/* Liveness/types.scm 47 */
			return BNIL;
		}

	}



/* &lambda2224 */
	obj_t BGl_z62lambda2224z62zzliveness_typesz00(obj_t BgL_envz00_8358,
		obj_t BgL_oz00_8359, obj_t BgL_vz00_8360)
	{
		{	/* Liveness/types.scm 47 */
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18480;

				{
					obj_t BgL_auxz00_18481;

					{	/* Liveness/types.scm 47 */
						BgL_objectz00_bglt BgL_tmpz00_18482;

						BgL_tmpz00_18482 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_oz00_8359));
						BgL_auxz00_18481 = BGL_OBJECT_WIDENING(BgL_tmpz00_18482);
					}
					BgL_auxz00_18480 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18481);
				}
				return
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18480))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8360)), BUNSPEC);
			}
		}

	}



/* &lambda2223 */
	obj_t BGl_z62lambda2223z62zzliveness_typesz00(obj_t BgL_envz00_8361,
		obj_t BgL_oz00_8362)
	{
		{	/* Liveness/types.scm 47 */
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_18489;

				{
					obj_t BgL_auxz00_18490;

					{	/* Liveness/types.scm 47 */
						BgL_objectz00_bglt BgL_tmpz00_18491;

						BgL_tmpz00_18491 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_oz00_8362));
						BgL_auxz00_18490 = BGL_OBJECT_WIDENING(BgL_tmpz00_18491);
					}
					BgL_auxz00_18489 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_18490);
				}
				return
					(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18489))->
					BgL_defz00);
			}
		}

	}



/* &lambda2170 */
	BgL_closurez00_bglt BGl_z62lambda2170z62zzliveness_typesz00(obj_t
		BgL_envz00_8363, obj_t BgL_o1162z00_8364)
	{
		{	/* Liveness/types.scm 41 */
			{	/* Liveness/types.scm 41 */
				long BgL_arg2171z00_9815;

				{	/* Liveness/types.scm 41 */
					obj_t BgL_arg2172z00_9816;

					{	/* Liveness/types.scm 41 */
						obj_t BgL_arg2173z00_9817;

						{	/* Liveness/types.scm 41 */
							obj_t BgL_arg1815z00_9818;
							long BgL_arg1816z00_9819;

							BgL_arg1815z00_9818 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 41 */
								long BgL_arg1817z00_9820;

								BgL_arg1817z00_9820 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_closurez00_bglt) BgL_o1162z00_8364)));
								BgL_arg1816z00_9819 = (BgL_arg1817z00_9820 - OBJECT_TYPE);
							}
							BgL_arg2173z00_9817 =
								VECTOR_REF(BgL_arg1815z00_9818, BgL_arg1816z00_9819);
						}
						BgL_arg2172z00_9816 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2173z00_9817);
					}
					{	/* Liveness/types.scm 41 */
						obj_t BgL_tmpz00_18504;

						BgL_tmpz00_18504 = ((obj_t) BgL_arg2172z00_9816);
						BgL_arg2171z00_9815 = BGL_CLASS_NUM(BgL_tmpz00_18504);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_closurez00_bglt) BgL_o1162z00_8364)), BgL_arg2171z00_9815);
			}
			{	/* Liveness/types.scm 41 */
				BgL_objectz00_bglt BgL_tmpz00_18510;

				BgL_tmpz00_18510 =
					((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_o1162z00_8364));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18510, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_o1162z00_8364));
			return ((BgL_closurez00_bglt) ((BgL_closurez00_bglt) BgL_o1162z00_8364));
		}

	}



/* &<@anonymous:2169> */
	obj_t BGl_z62zc3z04anonymousza32169ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8365, obj_t BgL_new1161z00_8366)
	{
		{	/* Liveness/types.scm 41 */
			{
				BgL_closurez00_bglt BgL_auxz00_18518;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_closurez00_bglt) BgL_new1161z00_8366))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_18522;

					{	/* Liveness/types.scm 41 */
						obj_t BgL_classz00_9822;

						BgL_classz00_9822 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 41 */
							obj_t BgL__ortest_1117z00_9823;

							BgL__ortest_1117z00_9823 = BGL_CLASS_NIL(BgL_classz00_9822);
							if (CBOOL(BgL__ortest_1117z00_9823))
								{	/* Liveness/types.scm 41 */
									BgL_auxz00_18522 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9823);
								}
							else
								{	/* Liveness/types.scm 41 */
									BgL_auxz00_18522 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9822));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_closurez00_bglt) BgL_new1161z00_8366))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_18522), BUNSPEC);
				}
				{
					BgL_variablez00_bglt BgL_auxz00_18532;

					{	/* Liveness/types.scm 41 */
						obj_t BgL_classz00_9824;

						BgL_classz00_9824 = BGl_variablez00zzast_varz00;
						{	/* Liveness/types.scm 41 */
							obj_t BgL__ortest_1117z00_9825;

							BgL__ortest_1117z00_9825 = BGL_CLASS_NIL(BgL_classz00_9824);
							if (CBOOL(BgL__ortest_1117z00_9825))
								{	/* Liveness/types.scm 41 */
									BgL_auxz00_18532 =
										((BgL_variablez00_bglt) BgL__ortest_1117z00_9825);
								}
							else
								{	/* Liveness/types.scm 41 */
									BgL_auxz00_18532 =
										((BgL_variablez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9824));
								}
						}
					}
					((((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt)
										((BgL_closurez00_bglt) BgL_new1161z00_8366))))->
							BgL_variablez00) =
						((BgL_variablez00_bglt) BgL_auxz00_18532), BUNSPEC);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_18542;

					{
						obj_t BgL_auxz00_18543;

						{	/* Liveness/types.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_18544;

							BgL_tmpz00_18544 =
								((BgL_objectz00_bglt)
								((BgL_closurez00_bglt) BgL_new1161z00_8366));
							BgL_auxz00_18543 = BGL_OBJECT_WIDENING(BgL_tmpz00_18544);
						}
						BgL_auxz00_18542 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18543);
					}
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18542))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_18550;

					{
						obj_t BgL_auxz00_18551;

						{	/* Liveness/types.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_18552;

							BgL_tmpz00_18552 =
								((BgL_objectz00_bglt)
								((BgL_closurez00_bglt) BgL_new1161z00_8366));
							BgL_auxz00_18551 = BGL_OBJECT_WIDENING(BgL_tmpz00_18552);
						}
						BgL_auxz00_18550 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18551);
					}
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18550))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_18558;

					{
						obj_t BgL_auxz00_18559;

						{	/* Liveness/types.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_18560;

							BgL_tmpz00_18560 =
								((BgL_objectz00_bglt)
								((BgL_closurez00_bglt) BgL_new1161z00_8366));
							BgL_auxz00_18559 = BGL_OBJECT_WIDENING(BgL_tmpz00_18560);
						}
						BgL_auxz00_18558 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18559);
					}
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18558))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_18566;

					{
						obj_t BgL_auxz00_18567;

						{	/* Liveness/types.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_18568;

							BgL_tmpz00_18568 =
								((BgL_objectz00_bglt)
								((BgL_closurez00_bglt) BgL_new1161z00_8366));
							BgL_auxz00_18567 = BGL_OBJECT_WIDENING(BgL_tmpz00_18568);
						}
						BgL_auxz00_18566 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18567);
					}
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18566))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_18518 = ((BgL_closurez00_bglt) BgL_new1161z00_8366);
				return ((obj_t) BgL_auxz00_18518);
			}
		}

	}



/* &lambda2167 */
	BgL_closurez00_bglt BGl_z62lambda2167z62zzliveness_typesz00(obj_t
		BgL_envz00_8367, obj_t BgL_o1158z00_8368)
	{
		{	/* Liveness/types.scm 41 */
			{	/* Liveness/types.scm 41 */
				BgL_closurezf2livenesszf2_bglt BgL_wide1160z00_9827;

				BgL_wide1160z00_9827 =
					((BgL_closurezf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_closurezf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 41 */
					obj_t BgL_auxz00_18581;
					BgL_objectz00_bglt BgL_tmpz00_18577;

					BgL_auxz00_18581 = ((obj_t) BgL_wide1160z00_9827);
					BgL_tmpz00_18577 =
						((BgL_objectz00_bglt)
						((BgL_closurez00_bglt) ((BgL_closurez00_bglt) BgL_o1158z00_8368)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18577, BgL_auxz00_18581);
				}
				((BgL_objectz00_bglt)
					((BgL_closurez00_bglt) ((BgL_closurez00_bglt) BgL_o1158z00_8368)));
				{	/* Liveness/types.scm 41 */
					long BgL_arg2168z00_9828;

					BgL_arg2168z00_9828 =
						BGL_CLASS_NUM(BGl_closurezf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_closurez00_bglt)
								((BgL_closurez00_bglt) BgL_o1158z00_8368))),
						BgL_arg2168z00_9828);
				}
				return
					((BgL_closurez00_bglt)
					((BgL_closurez00_bglt) ((BgL_closurez00_bglt) BgL_o1158z00_8368)));
			}
		}

	}



/* &lambda2164 */
	BgL_closurez00_bglt BGl_z62lambda2164z62zzliveness_typesz00(obj_t
		BgL_envz00_8369, obj_t BgL_loc1151z00_8370, obj_t BgL_type1152z00_8371,
		obj_t BgL_variable1153z00_8372, obj_t BgL_def1154z00_8373,
		obj_t BgL_use1155z00_8374, obj_t BgL_in1156z00_8375,
		obj_t BgL_out1157z00_8376)
	{
		{	/* Liveness/types.scm 41 */
			{	/* Liveness/types.scm 41 */
				BgL_closurez00_bglt BgL_new1700z00_9835;

				{	/* Liveness/types.scm 41 */
					BgL_closurez00_bglt BgL_tmp1698z00_9836;
					BgL_closurezf2livenesszf2_bglt BgL_wide1699z00_9837;

					{
						BgL_closurez00_bglt BgL_auxz00_18595;

						{	/* Liveness/types.scm 41 */
							BgL_closurez00_bglt BgL_new1697z00_9838;

							BgL_new1697z00_9838 =
								((BgL_closurez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_closurez00_bgl))));
							{	/* Liveness/types.scm 41 */
								long BgL_arg2166z00_9839;

								BgL_arg2166z00_9839 =
									BGL_CLASS_NUM(BGl_closurez00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1697z00_9838),
									BgL_arg2166z00_9839);
							}
							{	/* Liveness/types.scm 41 */
								BgL_objectz00_bglt BgL_tmpz00_18600;

								BgL_tmpz00_18600 = ((BgL_objectz00_bglt) BgL_new1697z00_9838);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18600, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1697z00_9838);
							BgL_auxz00_18595 = BgL_new1697z00_9838;
						}
						BgL_tmp1698z00_9836 = ((BgL_closurez00_bglt) BgL_auxz00_18595);
					}
					BgL_wide1699z00_9837 =
						((BgL_closurezf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_closurezf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 41 */
						obj_t BgL_auxz00_18608;
						BgL_objectz00_bglt BgL_tmpz00_18606;

						BgL_auxz00_18608 = ((obj_t) BgL_wide1699z00_9837);
						BgL_tmpz00_18606 = ((BgL_objectz00_bglt) BgL_tmp1698z00_9836);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18606, BgL_auxz00_18608);
					}
					((BgL_objectz00_bglt) BgL_tmp1698z00_9836);
					{	/* Liveness/types.scm 41 */
						long BgL_arg2165z00_9840;

						BgL_arg2165z00_9840 =
							BGL_CLASS_NUM(BGl_closurezf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1698z00_9836), BgL_arg2165z00_9840);
					}
					BgL_new1700z00_9835 = ((BgL_closurez00_bglt) BgL_tmp1698z00_9836);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1700z00_9835)))->BgL_locz00) =
					((obj_t) BgL_loc1151z00_8370), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1700z00_9835)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1152z00_8371)),
					BUNSPEC);
				((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt) BgL_new1700z00_9835)))->
						BgL_variablez00) =
					((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
							BgL_variable1153z00_8372)), BUNSPEC);
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_18624;

					{
						obj_t BgL_auxz00_18625;

						{	/* Liveness/types.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_18626;

							BgL_tmpz00_18626 = ((BgL_objectz00_bglt) BgL_new1700z00_9835);
							BgL_auxz00_18625 = BGL_OBJECT_WIDENING(BgL_tmpz00_18626);
						}
						BgL_auxz00_18624 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18625);
					}
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18624))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1154z00_8373)), BUNSPEC);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_18632;

					{
						obj_t BgL_auxz00_18633;

						{	/* Liveness/types.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_18634;

							BgL_tmpz00_18634 = ((BgL_objectz00_bglt) BgL_new1700z00_9835);
							BgL_auxz00_18633 = BGL_OBJECT_WIDENING(BgL_tmpz00_18634);
						}
						BgL_auxz00_18632 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18633);
					}
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18632))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1155z00_8374)), BUNSPEC);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_18640;

					{
						obj_t BgL_auxz00_18641;

						{	/* Liveness/types.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_18642;

							BgL_tmpz00_18642 = ((BgL_objectz00_bglt) BgL_new1700z00_9835);
							BgL_auxz00_18641 = BGL_OBJECT_WIDENING(BgL_tmpz00_18642);
						}
						BgL_auxz00_18640 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18641);
					}
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18640))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1156z00_8375)), BUNSPEC);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_18648;

					{
						obj_t BgL_auxz00_18649;

						{	/* Liveness/types.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_18650;

							BgL_tmpz00_18650 = ((BgL_objectz00_bglt) BgL_new1700z00_9835);
							BgL_auxz00_18649 = BGL_OBJECT_WIDENING(BgL_tmpz00_18650);
						}
						BgL_auxz00_18648 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18649);
					}
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18648))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1157z00_8376)), BUNSPEC);
				}
				return BgL_new1700z00_9835;
			}
		}

	}



/* &<@anonymous:2202> */
	obj_t BGl_z62zc3z04anonymousza32202ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8377)
	{
		{	/* Liveness/types.scm 41 */
			return BNIL;
		}

	}



/* &lambda2201 */
	obj_t BGl_z62lambda2201z62zzliveness_typesz00(obj_t BgL_envz00_8378,
		obj_t BgL_oz00_8379, obj_t BgL_vz00_8380)
	{
		{	/* Liveness/types.scm 41 */
			{
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_18656;

				{
					obj_t BgL_auxz00_18657;

					{	/* Liveness/types.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_18658;

						BgL_tmpz00_18658 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_oz00_8379));
						BgL_auxz00_18657 = BGL_OBJECT_WIDENING(BgL_tmpz00_18658);
					}
					BgL_auxz00_18656 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18657);
				}
				return
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18656))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8380)), BUNSPEC);
			}
		}

	}



/* &lambda2200 */
	obj_t BGl_z62lambda2200z62zzliveness_typesz00(obj_t BgL_envz00_8381,
		obj_t BgL_oz00_8382)
	{
		{	/* Liveness/types.scm 41 */
			{
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_18665;

				{
					obj_t BgL_auxz00_18666;

					{	/* Liveness/types.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_18667;

						BgL_tmpz00_18667 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_oz00_8382));
						BgL_auxz00_18666 = BGL_OBJECT_WIDENING(BgL_tmpz00_18667);
					}
					BgL_auxz00_18665 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18666);
				}
				return
					(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18665))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2194> */
	obj_t BGl_z62zc3z04anonymousza32194ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8383)
	{
		{	/* Liveness/types.scm 41 */
			return BNIL;
		}

	}



/* &lambda2193 */
	obj_t BGl_z62lambda2193z62zzliveness_typesz00(obj_t BgL_envz00_8384,
		obj_t BgL_oz00_8385, obj_t BgL_vz00_8386)
	{
		{	/* Liveness/types.scm 41 */
			{
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_18673;

				{
					obj_t BgL_auxz00_18674;

					{	/* Liveness/types.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_18675;

						BgL_tmpz00_18675 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_oz00_8385));
						BgL_auxz00_18674 = BGL_OBJECT_WIDENING(BgL_tmpz00_18675);
					}
					BgL_auxz00_18673 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18674);
				}
				return
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18673))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8386)), BUNSPEC);
			}
		}

	}



/* &lambda2192 */
	obj_t BGl_z62lambda2192z62zzliveness_typesz00(obj_t BgL_envz00_8387,
		obj_t BgL_oz00_8388)
	{
		{	/* Liveness/types.scm 41 */
			{
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_18682;

				{
					obj_t BgL_auxz00_18683;

					{	/* Liveness/types.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_18684;

						BgL_tmpz00_18684 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_oz00_8388));
						BgL_auxz00_18683 = BGL_OBJECT_WIDENING(BgL_tmpz00_18684);
					}
					BgL_auxz00_18682 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18683);
				}
				return
					(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18682))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2187> */
	obj_t BGl_z62zc3z04anonymousza32187ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8389)
	{
		{	/* Liveness/types.scm 41 */
			return BNIL;
		}

	}



/* &lambda2186 */
	obj_t BGl_z62lambda2186z62zzliveness_typesz00(obj_t BgL_envz00_8390,
		obj_t BgL_oz00_8391, obj_t BgL_vz00_8392)
	{
		{	/* Liveness/types.scm 41 */
			{
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_18690;

				{
					obj_t BgL_auxz00_18691;

					{	/* Liveness/types.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_18692;

						BgL_tmpz00_18692 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_oz00_8391));
						BgL_auxz00_18691 = BGL_OBJECT_WIDENING(BgL_tmpz00_18692);
					}
					BgL_auxz00_18690 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18691);
				}
				return
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18690))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8392)), BUNSPEC);
			}
		}

	}



/* &lambda2185 */
	obj_t BGl_z62lambda2185z62zzliveness_typesz00(obj_t BgL_envz00_8393,
		obj_t BgL_oz00_8394)
	{
		{	/* Liveness/types.scm 41 */
			{
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_18699;

				{
					obj_t BgL_auxz00_18700;

					{	/* Liveness/types.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_18701;

						BgL_tmpz00_18701 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_oz00_8394));
						BgL_auxz00_18700 = BGL_OBJECT_WIDENING(BgL_tmpz00_18701);
					}
					BgL_auxz00_18699 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18700);
				}
				return
					(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18699))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2180> */
	obj_t BGl_z62zc3z04anonymousza32180ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8395)
	{
		{	/* Liveness/types.scm 41 */
			return BNIL;
		}

	}



/* &lambda2179 */
	obj_t BGl_z62lambda2179z62zzliveness_typesz00(obj_t BgL_envz00_8396,
		obj_t BgL_oz00_8397, obj_t BgL_vz00_8398)
	{
		{	/* Liveness/types.scm 41 */
			{
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_18707;

				{
					obj_t BgL_auxz00_18708;

					{	/* Liveness/types.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_18709;

						BgL_tmpz00_18709 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_oz00_8397));
						BgL_auxz00_18708 = BGL_OBJECT_WIDENING(BgL_tmpz00_18709);
					}
					BgL_auxz00_18707 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18708);
				}
				return
					((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18707))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8398)), BUNSPEC);
			}
		}

	}



/* &lambda2178 */
	obj_t BGl_z62lambda2178z62zzliveness_typesz00(obj_t BgL_envz00_8399,
		obj_t BgL_oz00_8400)
	{
		{	/* Liveness/types.scm 41 */
			{
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_18716;

				{
					obj_t BgL_auxz00_18717;

					{	/* Liveness/types.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_18718;

						BgL_tmpz00_18718 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_oz00_8400));
						BgL_auxz00_18717 = BGL_OBJECT_WIDENING(BgL_tmpz00_18718);
					}
					BgL_auxz00_18716 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_18717);
				}
				return
					(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_18716))->
					BgL_defz00);
			}
		}

	}



/* &lambda2124 */
	BgL_refz00_bglt BGl_z62lambda2124z62zzliveness_typesz00(obj_t BgL_envz00_8401,
		obj_t BgL_o1149z00_8402)
	{
		{	/* Liveness/types.scm 35 */
			{	/* Liveness/types.scm 35 */
				long BgL_arg2125z00_9854;

				{	/* Liveness/types.scm 35 */
					obj_t BgL_arg2126z00_9855;

					{	/* Liveness/types.scm 35 */
						obj_t BgL_arg2127z00_9856;

						{	/* Liveness/types.scm 35 */
							obj_t BgL_arg1815z00_9857;
							long BgL_arg1816z00_9858;

							BgL_arg1815z00_9857 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 35 */
								long BgL_arg1817z00_9859;

								BgL_arg1817z00_9859 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_o1149z00_8402)));
								BgL_arg1816z00_9858 = (BgL_arg1817z00_9859 - OBJECT_TYPE);
							}
							BgL_arg2127z00_9856 =
								VECTOR_REF(BgL_arg1815z00_9857, BgL_arg1816z00_9858);
						}
						BgL_arg2126z00_9855 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2127z00_9856);
					}
					{	/* Liveness/types.scm 35 */
						obj_t BgL_tmpz00_18731;

						BgL_tmpz00_18731 = ((obj_t) BgL_arg2126z00_9855);
						BgL_arg2125z00_9854 = BGL_CLASS_NUM(BgL_tmpz00_18731);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_refz00_bglt) BgL_o1149z00_8402)), BgL_arg2125z00_9854);
			}
			{	/* Liveness/types.scm 35 */
				BgL_objectz00_bglt BgL_tmpz00_18737;

				BgL_tmpz00_18737 =
					((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_o1149z00_8402));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18737, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_o1149z00_8402));
			return ((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_o1149z00_8402));
		}

	}



/* &<@anonymous:2123> */
	obj_t BGl_z62zc3z04anonymousza32123ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8403, obj_t BgL_new1148z00_8404)
	{
		{	/* Liveness/types.scm 35 */
			{
				BgL_refz00_bglt BgL_auxz00_18745;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_refz00_bglt) BgL_new1148z00_8404))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_18749;

					{	/* Liveness/types.scm 35 */
						obj_t BgL_classz00_9861;

						BgL_classz00_9861 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 35 */
							obj_t BgL__ortest_1117z00_9862;

							BgL__ortest_1117z00_9862 = BGL_CLASS_NIL(BgL_classz00_9861);
							if (CBOOL(BgL__ortest_1117z00_9862))
								{	/* Liveness/types.scm 35 */
									BgL_auxz00_18749 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9862);
								}
							else
								{	/* Liveness/types.scm 35 */
									BgL_auxz00_18749 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9861));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_refz00_bglt) BgL_new1148z00_8404))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_18749), BUNSPEC);
				}
				{
					BgL_variablez00_bglt BgL_auxz00_18759;

					{	/* Liveness/types.scm 35 */
						obj_t BgL_classz00_9863;

						BgL_classz00_9863 = BGl_variablez00zzast_varz00;
						{	/* Liveness/types.scm 35 */
							obj_t BgL__ortest_1117z00_9864;

							BgL__ortest_1117z00_9864 = BGL_CLASS_NIL(BgL_classz00_9863);
							if (CBOOL(BgL__ortest_1117z00_9864))
								{	/* Liveness/types.scm 35 */
									BgL_auxz00_18759 =
										((BgL_variablez00_bglt) BgL__ortest_1117z00_9864);
								}
							else
								{	/* Liveness/types.scm 35 */
									BgL_auxz00_18759 =
										((BgL_variablez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9863));
								}
						}
					}
					((((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt)
										((BgL_refz00_bglt) BgL_new1148z00_8404))))->
							BgL_variablez00) =
						((BgL_variablez00_bglt) BgL_auxz00_18759), BUNSPEC);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_18769;

					{
						obj_t BgL_auxz00_18770;

						{	/* Liveness/types.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_18771;

							BgL_tmpz00_18771 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_new1148z00_8404));
							BgL_auxz00_18770 = BGL_OBJECT_WIDENING(BgL_tmpz00_18771);
						}
						BgL_auxz00_18769 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18770);
					}
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18769))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_18777;

					{
						obj_t BgL_auxz00_18778;

						{	/* Liveness/types.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_18779;

							BgL_tmpz00_18779 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_new1148z00_8404));
							BgL_auxz00_18778 = BGL_OBJECT_WIDENING(BgL_tmpz00_18779);
						}
						BgL_auxz00_18777 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18778);
					}
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18777))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_18785;

					{
						obj_t BgL_auxz00_18786;

						{	/* Liveness/types.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_18787;

							BgL_tmpz00_18787 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_new1148z00_8404));
							BgL_auxz00_18786 = BGL_OBJECT_WIDENING(BgL_tmpz00_18787);
						}
						BgL_auxz00_18785 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18786);
					}
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18785))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_18793;

					{
						obj_t BgL_auxz00_18794;

						{	/* Liveness/types.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_18795;

							BgL_tmpz00_18795 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_new1148z00_8404));
							BgL_auxz00_18794 = BGL_OBJECT_WIDENING(BgL_tmpz00_18795);
						}
						BgL_auxz00_18793 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18794);
					}
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18793))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_18745 = ((BgL_refz00_bglt) BgL_new1148z00_8404);
				return ((obj_t) BgL_auxz00_18745);
			}
		}

	}



/* &lambda2121 */
	BgL_refz00_bglt BGl_z62lambda2121z62zzliveness_typesz00(obj_t BgL_envz00_8405,
		obj_t BgL_o1145z00_8406)
	{
		{	/* Liveness/types.scm 35 */
			{	/* Liveness/types.scm 35 */
				BgL_refzf2livenesszf2_bglt BgL_wide1147z00_9866;

				BgL_wide1147z00_9866 =
					((BgL_refzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_refzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 35 */
					obj_t BgL_auxz00_18808;
					BgL_objectz00_bglt BgL_tmpz00_18804;

					BgL_auxz00_18808 = ((obj_t) BgL_wide1147z00_9866);
					BgL_tmpz00_18804 =
						((BgL_objectz00_bglt)
						((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_o1145z00_8406)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18804, BgL_auxz00_18808);
				}
				((BgL_objectz00_bglt)
					((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_o1145z00_8406)));
				{	/* Liveness/types.scm 35 */
					long BgL_arg2122z00_9867;

					BgL_arg2122z00_9867 =
						BGL_CLASS_NUM(BGl_refzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_refz00_bglt)
								((BgL_refz00_bglt) BgL_o1145z00_8406))), BgL_arg2122z00_9867);
				}
				return
					((BgL_refz00_bglt)
					((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_o1145z00_8406)));
			}
		}

	}



/* &lambda2118 */
	BgL_refz00_bglt BGl_z62lambda2118z62zzliveness_typesz00(obj_t BgL_envz00_8407,
		obj_t BgL_loc1138z00_8408, obj_t BgL_type1139z00_8409,
		obj_t BgL_variable1140z00_8410, obj_t BgL_def1141z00_8411,
		obj_t BgL_use1142z00_8412, obj_t BgL_in1143z00_8413,
		obj_t BgL_out1144z00_8414)
	{
		{	/* Liveness/types.scm 35 */
			{	/* Liveness/types.scm 35 */
				BgL_refz00_bglt BgL_new1695z00_9874;

				{	/* Liveness/types.scm 35 */
					BgL_refz00_bglt BgL_tmp1693z00_9875;
					BgL_refzf2livenesszf2_bglt BgL_wide1694z00_9876;

					{
						BgL_refz00_bglt BgL_auxz00_18822;

						{	/* Liveness/types.scm 35 */
							BgL_refz00_bglt BgL_new1692z00_9877;

							BgL_new1692z00_9877 =
								((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_refz00_bgl))));
							{	/* Liveness/types.scm 35 */
								long BgL_arg2120z00_9878;

								BgL_arg2120z00_9878 = BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1692z00_9877),
									BgL_arg2120z00_9878);
							}
							{	/* Liveness/types.scm 35 */
								BgL_objectz00_bglt BgL_tmpz00_18827;

								BgL_tmpz00_18827 = ((BgL_objectz00_bglt) BgL_new1692z00_9877);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18827, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1692z00_9877);
							BgL_auxz00_18822 = BgL_new1692z00_9877;
						}
						BgL_tmp1693z00_9875 = ((BgL_refz00_bglt) BgL_auxz00_18822);
					}
					BgL_wide1694z00_9876 =
						((BgL_refzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_refzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 35 */
						obj_t BgL_auxz00_18835;
						BgL_objectz00_bglt BgL_tmpz00_18833;

						BgL_auxz00_18835 = ((obj_t) BgL_wide1694z00_9876);
						BgL_tmpz00_18833 = ((BgL_objectz00_bglt) BgL_tmp1693z00_9875);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18833, BgL_auxz00_18835);
					}
					((BgL_objectz00_bglt) BgL_tmp1693z00_9875);
					{	/* Liveness/types.scm 35 */
						long BgL_arg2119z00_9879;

						BgL_arg2119z00_9879 =
							BGL_CLASS_NUM(BGl_refzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1693z00_9875), BgL_arg2119z00_9879);
					}
					BgL_new1695z00_9874 = ((BgL_refz00_bglt) BgL_tmp1693z00_9875);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1695z00_9874)))->BgL_locz00) =
					((obj_t) BgL_loc1138z00_8408), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1695z00_9874)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1139z00_8409)),
					BUNSPEC);
				((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt) BgL_new1695z00_9874)))->
						BgL_variablez00) =
					((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
							BgL_variable1140z00_8410)), BUNSPEC);
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_18851;

					{
						obj_t BgL_auxz00_18852;

						{	/* Liveness/types.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_18853;

							BgL_tmpz00_18853 = ((BgL_objectz00_bglt) BgL_new1695z00_9874);
							BgL_auxz00_18852 = BGL_OBJECT_WIDENING(BgL_tmpz00_18853);
						}
						BgL_auxz00_18851 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18852);
					}
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18851))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1141z00_8411)), BUNSPEC);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_18859;

					{
						obj_t BgL_auxz00_18860;

						{	/* Liveness/types.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_18861;

							BgL_tmpz00_18861 = ((BgL_objectz00_bglt) BgL_new1695z00_9874);
							BgL_auxz00_18860 = BGL_OBJECT_WIDENING(BgL_tmpz00_18861);
						}
						BgL_auxz00_18859 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18860);
					}
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18859))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1142z00_8412)), BUNSPEC);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_18867;

					{
						obj_t BgL_auxz00_18868;

						{	/* Liveness/types.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_18869;

							BgL_tmpz00_18869 = ((BgL_objectz00_bglt) BgL_new1695z00_9874);
							BgL_auxz00_18868 = BGL_OBJECT_WIDENING(BgL_tmpz00_18869);
						}
						BgL_auxz00_18867 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18868);
					}
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18867))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1143z00_8413)), BUNSPEC);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_18875;

					{
						obj_t BgL_auxz00_18876;

						{	/* Liveness/types.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_18877;

							BgL_tmpz00_18877 = ((BgL_objectz00_bglt) BgL_new1695z00_9874);
							BgL_auxz00_18876 = BGL_OBJECT_WIDENING(BgL_tmpz00_18877);
						}
						BgL_auxz00_18875 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18876);
					}
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18875))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1144z00_8414)), BUNSPEC);
				}
				return BgL_new1695z00_9874;
			}
		}

	}



/* &<@anonymous:2157> */
	obj_t BGl_z62zc3z04anonymousza32157ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8415)
	{
		{	/* Liveness/types.scm 35 */
			return BNIL;
		}

	}



/* &lambda2156 */
	obj_t BGl_z62lambda2156z62zzliveness_typesz00(obj_t BgL_envz00_8416,
		obj_t BgL_oz00_8417, obj_t BgL_vz00_8418)
	{
		{	/* Liveness/types.scm 35 */
			{
				BgL_refzf2livenesszf2_bglt BgL_auxz00_18883;

				{
					obj_t BgL_auxz00_18884;

					{	/* Liveness/types.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_18885;

						BgL_tmpz00_18885 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_oz00_8417));
						BgL_auxz00_18884 = BGL_OBJECT_WIDENING(BgL_tmpz00_18885);
					}
					BgL_auxz00_18883 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18884);
				}
				return
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18883))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8418)), BUNSPEC);
			}
		}

	}



/* &lambda2155 */
	obj_t BGl_z62lambda2155z62zzliveness_typesz00(obj_t BgL_envz00_8419,
		obj_t BgL_oz00_8420)
	{
		{	/* Liveness/types.scm 35 */
			{
				BgL_refzf2livenesszf2_bglt BgL_auxz00_18892;

				{
					obj_t BgL_auxz00_18893;

					{	/* Liveness/types.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_18894;

						BgL_tmpz00_18894 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_oz00_8420));
						BgL_auxz00_18893 = BGL_OBJECT_WIDENING(BgL_tmpz00_18894);
					}
					BgL_auxz00_18892 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18893);
				}
				return
					(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18892))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2149> */
	obj_t BGl_z62zc3z04anonymousza32149ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8421)
	{
		{	/* Liveness/types.scm 35 */
			return BNIL;
		}

	}



/* &lambda2148 */
	obj_t BGl_z62lambda2148z62zzliveness_typesz00(obj_t BgL_envz00_8422,
		obj_t BgL_oz00_8423, obj_t BgL_vz00_8424)
	{
		{	/* Liveness/types.scm 35 */
			{
				BgL_refzf2livenesszf2_bglt BgL_auxz00_18900;

				{
					obj_t BgL_auxz00_18901;

					{	/* Liveness/types.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_18902;

						BgL_tmpz00_18902 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_oz00_8423));
						BgL_auxz00_18901 = BGL_OBJECT_WIDENING(BgL_tmpz00_18902);
					}
					BgL_auxz00_18900 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18901);
				}
				return
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18900))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8424)), BUNSPEC);
			}
		}

	}



/* &lambda2147 */
	obj_t BGl_z62lambda2147z62zzliveness_typesz00(obj_t BgL_envz00_8425,
		obj_t BgL_oz00_8426)
	{
		{	/* Liveness/types.scm 35 */
			{
				BgL_refzf2livenesszf2_bglt BgL_auxz00_18909;

				{
					obj_t BgL_auxz00_18910;

					{	/* Liveness/types.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_18911;

						BgL_tmpz00_18911 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_oz00_8426));
						BgL_auxz00_18910 = BGL_OBJECT_WIDENING(BgL_tmpz00_18911);
					}
					BgL_auxz00_18909 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18910);
				}
				return
					(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18909))->BgL_inz00);
			}
		}

	}



/* &<@anonymous:2142> */
	obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8427)
	{
		{	/* Liveness/types.scm 35 */
			return BNIL;
		}

	}



/* &lambda2141 */
	obj_t BGl_z62lambda2141z62zzliveness_typesz00(obj_t BgL_envz00_8428,
		obj_t BgL_oz00_8429, obj_t BgL_vz00_8430)
	{
		{	/* Liveness/types.scm 35 */
			{
				BgL_refzf2livenesszf2_bglt BgL_auxz00_18917;

				{
					obj_t BgL_auxz00_18918;

					{	/* Liveness/types.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_18919;

						BgL_tmpz00_18919 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_oz00_8429));
						BgL_auxz00_18918 = BGL_OBJECT_WIDENING(BgL_tmpz00_18919);
					}
					BgL_auxz00_18917 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18918);
				}
				return
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18917))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8430)), BUNSPEC);
			}
		}

	}



/* &lambda2140 */
	obj_t BGl_z62lambda2140z62zzliveness_typesz00(obj_t BgL_envz00_8431,
		obj_t BgL_oz00_8432)
	{
		{	/* Liveness/types.scm 35 */
			{
				BgL_refzf2livenesszf2_bglt BgL_auxz00_18926;

				{
					obj_t BgL_auxz00_18927;

					{	/* Liveness/types.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_18928;

						BgL_tmpz00_18928 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_oz00_8432));
						BgL_auxz00_18927 = BGL_OBJECT_WIDENING(BgL_tmpz00_18928);
					}
					BgL_auxz00_18926 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18927);
				}
				return
					(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18926))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2135> */
	obj_t BGl_z62zc3z04anonymousza32135ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8433)
	{
		{	/* Liveness/types.scm 35 */
			return BNIL;
		}

	}



/* &lambda2134 */
	obj_t BGl_z62lambda2134z62zzliveness_typesz00(obj_t BgL_envz00_8434,
		obj_t BgL_oz00_8435, obj_t BgL_vz00_8436)
	{
		{	/* Liveness/types.scm 35 */
			{
				BgL_refzf2livenesszf2_bglt BgL_auxz00_18934;

				{
					obj_t BgL_auxz00_18935;

					{	/* Liveness/types.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_18936;

						BgL_tmpz00_18936 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_oz00_8435));
						BgL_auxz00_18935 = BGL_OBJECT_WIDENING(BgL_tmpz00_18936);
					}
					BgL_auxz00_18934 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18935);
				}
				return
					((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18934))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8436)), BUNSPEC);
			}
		}

	}



/* &lambda2133 */
	obj_t BGl_z62lambda2133z62zzliveness_typesz00(obj_t BgL_envz00_8437,
		obj_t BgL_oz00_8438)
	{
		{	/* Liveness/types.scm 35 */
			{
				BgL_refzf2livenesszf2_bglt BgL_auxz00_18943;

				{
					obj_t BgL_auxz00_18944;

					{	/* Liveness/types.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_18945;

						BgL_tmpz00_18945 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_oz00_8438));
						BgL_auxz00_18944 = BGL_OBJECT_WIDENING(BgL_tmpz00_18945);
					}
					BgL_auxz00_18943 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_18944);
				}
				return
					(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18943))->
					BgL_defz00);
			}
		}

	}



/* &lambda2078 */
	BgL_literalz00_bglt BGl_z62lambda2078z62zzliveness_typesz00(obj_t
		BgL_envz00_8439, obj_t BgL_o1136z00_8440)
	{
		{	/* Liveness/types.scm 29 */
			{	/* Liveness/types.scm 29 */
				long BgL_arg2079z00_9893;

				{	/* Liveness/types.scm 29 */
					obj_t BgL_arg2080z00_9894;

					{	/* Liveness/types.scm 29 */
						obj_t BgL_arg2081z00_9895;

						{	/* Liveness/types.scm 29 */
							obj_t BgL_arg1815z00_9896;
							long BgL_arg1816z00_9897;

							BgL_arg1815z00_9896 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 29 */
								long BgL_arg1817z00_9898;

								BgL_arg1817z00_9898 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_literalz00_bglt) BgL_o1136z00_8440)));
								BgL_arg1816z00_9897 = (BgL_arg1817z00_9898 - OBJECT_TYPE);
							}
							BgL_arg2081z00_9895 =
								VECTOR_REF(BgL_arg1815z00_9896, BgL_arg1816z00_9897);
						}
						BgL_arg2080z00_9894 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2081z00_9895);
					}
					{	/* Liveness/types.scm 29 */
						obj_t BgL_tmpz00_18958;

						BgL_tmpz00_18958 = ((obj_t) BgL_arg2080z00_9894);
						BgL_arg2079z00_9893 = BGL_CLASS_NUM(BgL_tmpz00_18958);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_literalz00_bglt) BgL_o1136z00_8440)), BgL_arg2079z00_9893);
			}
			{	/* Liveness/types.scm 29 */
				BgL_objectz00_bglt BgL_tmpz00_18964;

				BgL_tmpz00_18964 =
					((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_o1136z00_8440));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_18964, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_o1136z00_8440));
			return ((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_o1136z00_8440));
		}

	}



/* &<@anonymous:2077> */
	obj_t BGl_z62zc3z04anonymousza32077ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8441, obj_t BgL_new1135z00_8442)
	{
		{	/* Liveness/types.scm 29 */
			{
				BgL_literalz00_bglt BgL_auxz00_18972;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_literalz00_bglt) BgL_new1135z00_8442))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_18976;

					{	/* Liveness/types.scm 29 */
						obj_t BgL_classz00_9900;

						BgL_classz00_9900 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 29 */
							obj_t BgL__ortest_1117z00_9901;

							BgL__ortest_1117z00_9901 = BGL_CLASS_NIL(BgL_classz00_9900);
							if (CBOOL(BgL__ortest_1117z00_9901))
								{	/* Liveness/types.scm 29 */
									BgL_auxz00_18976 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9901);
								}
							else
								{	/* Liveness/types.scm 29 */
									BgL_auxz00_18976 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9900));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_literalz00_bglt) BgL_new1135z00_8442))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_18976), BUNSPEC);
				}
				((((BgL_atomz00_bglt) COBJECT(
								((BgL_atomz00_bglt)
									((BgL_literalz00_bglt) BgL_new1135z00_8442))))->
						BgL_valuez00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_18989;

					{
						obj_t BgL_auxz00_18990;

						{	/* Liveness/types.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_18991;

							BgL_tmpz00_18991 =
								((BgL_objectz00_bglt)
								((BgL_literalz00_bglt) BgL_new1135z00_8442));
							BgL_auxz00_18990 = BGL_OBJECT_WIDENING(BgL_tmpz00_18991);
						}
						BgL_auxz00_18989 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_18990);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18989))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_18997;

					{
						obj_t BgL_auxz00_18998;

						{	/* Liveness/types.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_18999;

							BgL_tmpz00_18999 =
								((BgL_objectz00_bglt)
								((BgL_literalz00_bglt) BgL_new1135z00_8442));
							BgL_auxz00_18998 = BGL_OBJECT_WIDENING(BgL_tmpz00_18999);
						}
						BgL_auxz00_18997 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_18998);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_18997))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_19005;

					{
						obj_t BgL_auxz00_19006;

						{	/* Liveness/types.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_19007;

							BgL_tmpz00_19007 =
								((BgL_objectz00_bglt)
								((BgL_literalz00_bglt) BgL_new1135z00_8442));
							BgL_auxz00_19006 = BGL_OBJECT_WIDENING(BgL_tmpz00_19007);
						}
						BgL_auxz00_19005 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19006);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19005))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_19013;

					{
						obj_t BgL_auxz00_19014;

						{	/* Liveness/types.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_19015;

							BgL_tmpz00_19015 =
								((BgL_objectz00_bglt)
								((BgL_literalz00_bglt) BgL_new1135z00_8442));
							BgL_auxz00_19014 = BGL_OBJECT_WIDENING(BgL_tmpz00_19015);
						}
						BgL_auxz00_19013 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19014);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19013))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_18972 = ((BgL_literalz00_bglt) BgL_new1135z00_8442);
				return ((obj_t) BgL_auxz00_18972);
			}
		}

	}



/* &lambda2075 */
	BgL_literalz00_bglt BGl_z62lambda2075z62zzliveness_typesz00(obj_t
		BgL_envz00_8443, obj_t BgL_o1132z00_8444)
	{
		{	/* Liveness/types.scm 29 */
			{	/* Liveness/types.scm 29 */
				BgL_literalzf2livenesszf2_bglt BgL_wide1134z00_9903;

				BgL_wide1134z00_9903 =
					((BgL_literalzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_literalzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 29 */
					obj_t BgL_auxz00_19028;
					BgL_objectz00_bglt BgL_tmpz00_19024;

					BgL_auxz00_19028 = ((obj_t) BgL_wide1134z00_9903);
					BgL_tmpz00_19024 =
						((BgL_objectz00_bglt)
						((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_o1132z00_8444)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_19024, BgL_auxz00_19028);
				}
				((BgL_objectz00_bglt)
					((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_o1132z00_8444)));
				{	/* Liveness/types.scm 29 */
					long BgL_arg2076z00_9904;

					BgL_arg2076z00_9904 =
						BGL_CLASS_NUM(BGl_literalzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_literalz00_bglt)
								((BgL_literalz00_bglt) BgL_o1132z00_8444))),
						BgL_arg2076z00_9904);
				}
				return
					((BgL_literalz00_bglt)
					((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_o1132z00_8444)));
			}
		}

	}



/* &lambda2071 */
	BgL_literalz00_bglt BGl_z62lambda2071z62zzliveness_typesz00(obj_t
		BgL_envz00_8445, obj_t BgL_loc1125z00_8446, obj_t BgL_type1126z00_8447,
		obj_t BgL_value1127z00_8448, obj_t BgL_def1128z00_8449,
		obj_t BgL_use1129z00_8450, obj_t BgL_in1130z00_8451,
		obj_t BgL_out1131z00_8452)
	{
		{	/* Liveness/types.scm 29 */
			{	/* Liveness/types.scm 29 */
				BgL_literalz00_bglt BgL_new1690z00_9910;

				{	/* Liveness/types.scm 29 */
					BgL_literalz00_bglt BgL_tmp1688z00_9911;
					BgL_literalzf2livenesszf2_bglt BgL_wide1689z00_9912;

					{
						BgL_literalz00_bglt BgL_auxz00_19042;

						{	/* Liveness/types.scm 29 */
							BgL_literalz00_bglt BgL_new1687z00_9913;

							BgL_new1687z00_9913 =
								((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_literalz00_bgl))));
							{	/* Liveness/types.scm 29 */
								long BgL_arg2074z00_9914;

								BgL_arg2074z00_9914 =
									BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1687z00_9913),
									BgL_arg2074z00_9914);
							}
							{	/* Liveness/types.scm 29 */
								BgL_objectz00_bglt BgL_tmpz00_19047;

								BgL_tmpz00_19047 = ((BgL_objectz00_bglt) BgL_new1687z00_9913);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_19047, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1687z00_9913);
							BgL_auxz00_19042 = BgL_new1687z00_9913;
						}
						BgL_tmp1688z00_9911 = ((BgL_literalz00_bglt) BgL_auxz00_19042);
					}
					BgL_wide1689z00_9912 =
						((BgL_literalzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_literalzf2livenesszf2_bgl))));
					{	/* Liveness/types.scm 29 */
						obj_t BgL_auxz00_19055;
						BgL_objectz00_bglt BgL_tmpz00_19053;

						BgL_auxz00_19055 = ((obj_t) BgL_wide1689z00_9912);
						BgL_tmpz00_19053 = ((BgL_objectz00_bglt) BgL_tmp1688z00_9911);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_19053, BgL_auxz00_19055);
					}
					((BgL_objectz00_bglt) BgL_tmp1688z00_9911);
					{	/* Liveness/types.scm 29 */
						long BgL_arg2072z00_9915;

						BgL_arg2072z00_9915 =
							BGL_CLASS_NUM(BGl_literalzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1688z00_9911), BgL_arg2072z00_9915);
					}
					BgL_new1690z00_9910 = ((BgL_literalz00_bglt) BgL_tmp1688z00_9911);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1690z00_9910)))->BgL_locz00) =
					((obj_t) BgL_loc1125z00_8446), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1690z00_9910)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1126z00_8447)),
					BUNSPEC);
				((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
									BgL_new1690z00_9910)))->BgL_valuez00) =
					((obj_t) BgL_value1127z00_8448), BUNSPEC);
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_19070;

					{
						obj_t BgL_auxz00_19071;

						{	/* Liveness/types.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_19072;

							BgL_tmpz00_19072 = ((BgL_objectz00_bglt) BgL_new1690z00_9910);
							BgL_auxz00_19071 = BGL_OBJECT_WIDENING(BgL_tmpz00_19072);
						}
						BgL_auxz00_19070 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19071);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19070))->
							BgL_defz00) = ((obj_t) ((obj_t) BgL_def1128z00_8449)), BUNSPEC);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_19078;

					{
						obj_t BgL_auxz00_19079;

						{	/* Liveness/types.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_19080;

							BgL_tmpz00_19080 = ((BgL_objectz00_bglt) BgL_new1690z00_9910);
							BgL_auxz00_19079 = BGL_OBJECT_WIDENING(BgL_tmpz00_19080);
						}
						BgL_auxz00_19078 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19079);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19078))->
							BgL_usez00) = ((obj_t) ((obj_t) BgL_use1129z00_8450)), BUNSPEC);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_19086;

					{
						obj_t BgL_auxz00_19087;

						{	/* Liveness/types.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_19088;

							BgL_tmpz00_19088 = ((BgL_objectz00_bglt) BgL_new1690z00_9910);
							BgL_auxz00_19087 = BGL_OBJECT_WIDENING(BgL_tmpz00_19088);
						}
						BgL_auxz00_19086 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19087);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19086))->
							BgL_inz00) = ((obj_t) ((obj_t) BgL_in1130z00_8451)), BUNSPEC);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_19094;

					{
						obj_t BgL_auxz00_19095;

						{	/* Liveness/types.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_19096;

							BgL_tmpz00_19096 = ((BgL_objectz00_bglt) BgL_new1690z00_9910);
							BgL_auxz00_19095 = BGL_OBJECT_WIDENING(BgL_tmpz00_19096);
						}
						BgL_auxz00_19094 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19095);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19094))->
							BgL_outz00) = ((obj_t) ((obj_t) BgL_out1131z00_8452)), BUNSPEC);
				}
				return BgL_new1690z00_9910;
			}
		}

	}



/* &<@anonymous:2111> */
	obj_t BGl_z62zc3z04anonymousza32111ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8453)
	{
		{	/* Liveness/types.scm 29 */
			return BNIL;
		}

	}



/* &lambda2110 */
	obj_t BGl_z62lambda2110z62zzliveness_typesz00(obj_t BgL_envz00_8454,
		obj_t BgL_oz00_8455, obj_t BgL_vz00_8456)
	{
		{	/* Liveness/types.scm 29 */
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_19102;

				{
					obj_t BgL_auxz00_19103;

					{	/* Liveness/types.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_19104;

						BgL_tmpz00_19104 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_oz00_8455));
						BgL_auxz00_19103 = BGL_OBJECT_WIDENING(BgL_tmpz00_19104);
					}
					BgL_auxz00_19102 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19103);
				}
				return
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19102))->
						BgL_outz00) = ((obj_t) ((obj_t) BgL_vz00_8456)), BUNSPEC);
			}
		}

	}



/* &lambda2109 */
	obj_t BGl_z62lambda2109z62zzliveness_typesz00(obj_t BgL_envz00_8457,
		obj_t BgL_oz00_8458)
	{
		{	/* Liveness/types.scm 29 */
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_19111;

				{
					obj_t BgL_auxz00_19112;

					{	/* Liveness/types.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_19113;

						BgL_tmpz00_19113 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_oz00_8458));
						BgL_auxz00_19112 = BGL_OBJECT_WIDENING(BgL_tmpz00_19113);
					}
					BgL_auxz00_19111 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19112);
				}
				return
					(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19111))->
					BgL_outz00);
			}
		}

	}



/* &<@anonymous:2104> */
	obj_t BGl_z62zc3z04anonymousza32104ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8459)
	{
		{	/* Liveness/types.scm 29 */
			return BNIL;
		}

	}



/* &lambda2103 */
	obj_t BGl_z62lambda2103z62zzliveness_typesz00(obj_t BgL_envz00_8460,
		obj_t BgL_oz00_8461, obj_t BgL_vz00_8462)
	{
		{	/* Liveness/types.scm 29 */
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_19119;

				{
					obj_t BgL_auxz00_19120;

					{	/* Liveness/types.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_19121;

						BgL_tmpz00_19121 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_oz00_8461));
						BgL_auxz00_19120 = BGL_OBJECT_WIDENING(BgL_tmpz00_19121);
					}
					BgL_auxz00_19119 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19120);
				}
				return
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19119))->
						BgL_inz00) = ((obj_t) ((obj_t) BgL_vz00_8462)), BUNSPEC);
			}
		}

	}



/* &lambda2102 */
	obj_t BGl_z62lambda2102z62zzliveness_typesz00(obj_t BgL_envz00_8463,
		obj_t BgL_oz00_8464)
	{
		{	/* Liveness/types.scm 29 */
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_19128;

				{
					obj_t BgL_auxz00_19129;

					{	/* Liveness/types.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_19130;

						BgL_tmpz00_19130 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_oz00_8464));
						BgL_auxz00_19129 = BGL_OBJECT_WIDENING(BgL_tmpz00_19130);
					}
					BgL_auxz00_19128 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19129);
				}
				return
					(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19128))->
					BgL_inz00);
			}
		}

	}



/* &<@anonymous:2097> */
	obj_t BGl_z62zc3z04anonymousza32097ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8465)
	{
		{	/* Liveness/types.scm 29 */
			return BNIL;
		}

	}



/* &lambda2096 */
	obj_t BGl_z62lambda2096z62zzliveness_typesz00(obj_t BgL_envz00_8466,
		obj_t BgL_oz00_8467, obj_t BgL_vz00_8468)
	{
		{	/* Liveness/types.scm 29 */
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_19136;

				{
					obj_t BgL_auxz00_19137;

					{	/* Liveness/types.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_19138;

						BgL_tmpz00_19138 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_oz00_8467));
						BgL_auxz00_19137 = BGL_OBJECT_WIDENING(BgL_tmpz00_19138);
					}
					BgL_auxz00_19136 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19137);
				}
				return
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19136))->
						BgL_usez00) = ((obj_t) ((obj_t) BgL_vz00_8468)), BUNSPEC);
			}
		}

	}



/* &lambda2095 */
	obj_t BGl_z62lambda2095z62zzliveness_typesz00(obj_t BgL_envz00_8469,
		obj_t BgL_oz00_8470)
	{
		{	/* Liveness/types.scm 29 */
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_19145;

				{
					obj_t BgL_auxz00_19146;

					{	/* Liveness/types.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_19147;

						BgL_tmpz00_19147 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_oz00_8470));
						BgL_auxz00_19146 = BGL_OBJECT_WIDENING(BgL_tmpz00_19147);
					}
					BgL_auxz00_19145 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19146);
				}
				return
					(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19145))->
					BgL_usez00);
			}
		}

	}



/* &<@anonymous:2089> */
	obj_t BGl_z62zc3z04anonymousza32089ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8471)
	{
		{	/* Liveness/types.scm 29 */
			return BNIL;
		}

	}



/* &lambda2088 */
	obj_t BGl_z62lambda2088z62zzliveness_typesz00(obj_t BgL_envz00_8472,
		obj_t BgL_oz00_8473, obj_t BgL_vz00_8474)
	{
		{	/* Liveness/types.scm 29 */
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_19153;

				{
					obj_t BgL_auxz00_19154;

					{	/* Liveness/types.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_19155;

						BgL_tmpz00_19155 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_oz00_8473));
						BgL_auxz00_19154 = BGL_OBJECT_WIDENING(BgL_tmpz00_19155);
					}
					BgL_auxz00_19153 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19154);
				}
				return
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19153))->
						BgL_defz00) = ((obj_t) ((obj_t) BgL_vz00_8474)), BUNSPEC);
			}
		}

	}



/* &lambda2087 */
	obj_t BGl_z62lambda2087z62zzliveness_typesz00(obj_t BgL_envz00_8475,
		obj_t BgL_oz00_8476)
	{
		{	/* Liveness/types.scm 29 */
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_19162;

				{
					obj_t BgL_auxz00_19163;

					{	/* Liveness/types.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_19164;

						BgL_tmpz00_19164 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_oz00_8476));
						BgL_auxz00_19163 = BGL_OBJECT_WIDENING(BgL_tmpz00_19164);
					}
					BgL_auxz00_19162 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_19163);
				}
				return
					(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19162))->
					BgL_defz00);
			}
		}

	}



/* &lambda2051 */
	BgL_localz00_bglt BGl_z62lambda2051z62zzliveness_typesz00(obj_t
		BgL_envz00_8477, obj_t BgL_o1123z00_8478)
	{
		{	/* Liveness/types.scm 26 */
			{	/* Liveness/types.scm 26 */
				long BgL_arg2052z00_9929;

				{	/* Liveness/types.scm 26 */
					obj_t BgL_arg2055z00_9930;

					{	/* Liveness/types.scm 26 */
						obj_t BgL_arg2056z00_9931;

						{	/* Liveness/types.scm 26 */
							obj_t BgL_arg1815z00_9932;
							long BgL_arg1816z00_9933;

							BgL_arg1815z00_9932 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Liveness/types.scm 26 */
								long BgL_arg1817z00_9934;

								BgL_arg1817z00_9934 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1123z00_8478)));
								BgL_arg1816z00_9933 = (BgL_arg1817z00_9934 - OBJECT_TYPE);
							}
							BgL_arg2056z00_9931 =
								VECTOR_REF(BgL_arg1815z00_9932, BgL_arg1816z00_9933);
						}
						BgL_arg2055z00_9930 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2056z00_9931);
					}
					{	/* Liveness/types.scm 26 */
						obj_t BgL_tmpz00_19177;

						BgL_tmpz00_19177 = ((obj_t) BgL_arg2055z00_9930);
						BgL_arg2052z00_9929 = BGL_CLASS_NUM(BgL_tmpz00_19177);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1123z00_8478)), BgL_arg2052z00_9929);
			}
			{	/* Liveness/types.scm 26 */
				BgL_objectz00_bglt BgL_tmpz00_19183;

				BgL_tmpz00_19183 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1123z00_8478));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_19183, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1123z00_8478));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1123z00_8478));
		}

	}



/* &<@anonymous:2050> */
	obj_t BGl_z62zc3z04anonymousza32050ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8479, obj_t BgL_new1122z00_8480)
	{
		{	/* Liveness/types.scm 26 */
			{
				BgL_localz00_bglt BgL_auxz00_19191;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1122z00_8480))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(43)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_8480))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_19199;

					{	/* Liveness/types.scm 26 */
						obj_t BgL_classz00_9936;

						BgL_classz00_9936 = BGl_typez00zztype_typez00;
						{	/* Liveness/types.scm 26 */
							obj_t BgL__ortest_1117z00_9937;

							BgL__ortest_1117z00_9937 = BGL_CLASS_NIL(BgL_classz00_9936);
							if (CBOOL(BgL__ortest_1117z00_9937))
								{	/* Liveness/types.scm 26 */
									BgL_auxz00_19199 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9937);
								}
							else
								{	/* Liveness/types.scm 26 */
									BgL_auxz00_19199 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9936));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1122z00_8480))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_19199), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_19209;

					{	/* Liveness/types.scm 26 */
						obj_t BgL_classz00_9938;

						BgL_classz00_9938 = BGl_valuez00zzast_varz00;
						{	/* Liveness/types.scm 26 */
							obj_t BgL__ortest_1117z00_9939;

							BgL__ortest_1117z00_9939 = BGL_CLASS_NIL(BgL_classz00_9938);
							if (CBOOL(BgL__ortest_1117z00_9939))
								{	/* Liveness/types.scm 26 */
									BgL_auxz00_19209 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_9939);
								}
							else
								{	/* Liveness/types.scm 26 */
									BgL_auxz00_19209 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9938));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1122z00_8480))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_19209), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1122z00_8480))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_8480))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_8480))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_8480))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_8480))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_8480))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_8480))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_8480))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1122z00_8480))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2livenesszf2_bglt BgL_auxz00_19246;

					{
						obj_t BgL_auxz00_19247;

						{	/* Liveness/types.scm 26 */
							BgL_objectz00_bglt BgL_tmpz00_19248;

							BgL_tmpz00_19248 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1122z00_8480));
							BgL_auxz00_19247 = BGL_OBJECT_WIDENING(BgL_tmpz00_19248);
						}
						BgL_auxz00_19246 =
							((BgL_localzf2livenesszf2_bglt) BgL_auxz00_19247);
					}
					((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19246))->
							BgL_z52countz52) = ((int) (int) (0L)), BUNSPEC);
				}
				BgL_auxz00_19191 = ((BgL_localz00_bglt) BgL_new1122z00_8480);
				return ((obj_t) BgL_auxz00_19191);
			}
		}

	}



/* &lambda2048 */
	BgL_localz00_bglt BGl_z62lambda2048z62zzliveness_typesz00(obj_t
		BgL_envz00_8481, obj_t BgL_o1118z00_8482)
	{
		{	/* Liveness/types.scm 26 */
			{	/* Liveness/types.scm 26 */
				BgL_localzf2livenesszf2_bglt BgL_wide1120z00_9941;

				BgL_wide1120z00_9941 =
					((BgL_localzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2livenesszf2_bgl))));
				{	/* Liveness/types.scm 26 */
					obj_t BgL_auxz00_19262;
					BgL_objectz00_bglt BgL_tmpz00_19258;

					BgL_auxz00_19262 = ((obj_t) BgL_wide1120z00_9941);
					BgL_tmpz00_19258 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1118z00_8482)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_19258, BgL_auxz00_19262);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1118z00_8482)));
				{	/* Liveness/types.scm 26 */
					long BgL_arg2049z00_9942;

					BgL_arg2049z00_9942 =
						BGL_CLASS_NUM(BGl_localzf2livenesszf2zzliveness_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1118z00_8482))), BgL_arg2049z00_9942);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1118z00_8482)));
			}
		}

	}



/* &lambda2045 */
	BgL_localz00_bglt BGl_z62lambda2045z62zzliveness_typesz00(obj_t
		BgL_envz00_8483, obj_t BgL_id1104z00_8484, obj_t BgL_name1105z00_8485,
		obj_t BgL_type1106z00_8486, obj_t BgL_value1107z00_8487,
		obj_t BgL_access1108z00_8488, obj_t BgL_fastzd2alpha1109zd2_8489,
		obj_t BgL_removable1110z00_8490, obj_t BgL_occurrence1111z00_8491,
		obj_t BgL_occurrencew1112z00_8492, obj_t BgL_userzf31113zf3_8493,
		obj_t BgL_key1114z00_8494, obj_t BgL_valzd2noescape1115zd2_8495,
		obj_t BgL_volatile1116z00_8496, obj_t BgL_z52count1117z52_8497)
	{
		{	/* Liveness/types.scm 26 */
			{	/* Liveness/types.scm 26 */
				long BgL_occurrence1111z00_9946;
				long BgL_occurrencew1112z00_9947;
				bool_t BgL_userzf31113zf3_9948;
				long BgL_key1114z00_9949;
				bool_t BgL_volatile1116z00_9950;
				int BgL_z52count1117z52_9951;

				BgL_occurrence1111z00_9946 = (long) CINT(BgL_occurrence1111z00_8491);
				BgL_occurrencew1112z00_9947 = (long) CINT(BgL_occurrencew1112z00_8492);
				BgL_userzf31113zf3_9948 = CBOOL(BgL_userzf31113zf3_8493);
				BgL_key1114z00_9949 = (long) CINT(BgL_key1114z00_8494);
				BgL_volatile1116z00_9950 = CBOOL(BgL_volatile1116z00_8496);
				BgL_z52count1117z52_9951 = CINT(BgL_z52count1117z52_8497);
				{	/* Liveness/types.scm 26 */
					BgL_localz00_bglt BgL_new1685z00_9952;

					{	/* Liveness/types.scm 26 */
						BgL_localz00_bglt BgL_tmp1683z00_9953;
						BgL_localzf2livenesszf2_bglt BgL_wide1684z00_9954;

						{
							BgL_localz00_bglt BgL_auxz00_19282;

							{	/* Liveness/types.scm 26 */
								BgL_localz00_bglt BgL_new1682z00_9955;

								BgL_new1682z00_9955 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Liveness/types.scm 26 */
									long BgL_arg2047z00_9956;

									BgL_arg2047z00_9956 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1682z00_9955),
										BgL_arg2047z00_9956);
								}
								{	/* Liveness/types.scm 26 */
									BgL_objectz00_bglt BgL_tmpz00_19287;

									BgL_tmpz00_19287 = ((BgL_objectz00_bglt) BgL_new1682z00_9955);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_19287, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1682z00_9955);
								BgL_auxz00_19282 = BgL_new1682z00_9955;
							}
							BgL_tmp1683z00_9953 = ((BgL_localz00_bglt) BgL_auxz00_19282);
						}
						BgL_wide1684z00_9954 =
							((BgL_localzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2livenesszf2_bgl))));
						{	/* Liveness/types.scm 26 */
							obj_t BgL_auxz00_19295;
							BgL_objectz00_bglt BgL_tmpz00_19293;

							BgL_auxz00_19295 = ((obj_t) BgL_wide1684z00_9954);
							BgL_tmpz00_19293 = ((BgL_objectz00_bglt) BgL_tmp1683z00_9953);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_19293, BgL_auxz00_19295);
						}
						((BgL_objectz00_bglt) BgL_tmp1683z00_9953);
						{	/* Liveness/types.scm 26 */
							long BgL_arg2046z00_9957;

							BgL_arg2046z00_9957 =
								BGL_CLASS_NUM(BGl_localzf2livenesszf2zzliveness_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1683z00_9953),
								BgL_arg2046z00_9957);
						}
						BgL_new1685z00_9952 = ((BgL_localz00_bglt) BgL_tmp1683z00_9953);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1685z00_9952)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1104z00_8484)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1685z00_9952)))->BgL_namez00) =
						((obj_t) BgL_name1105z00_8485), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1685z00_9952)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1106z00_8486)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1685z00_9952)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1107z00_8487)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1685z00_9952)))->BgL_accessz00) =
						((obj_t) BgL_access1108z00_8488), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1685z00_9952)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1109zd2_8489), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1685z00_9952)))->BgL_removablez00) =
						((obj_t) BgL_removable1110z00_8490), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1685z00_9952)))->BgL_occurrencez00) =
						((long) BgL_occurrence1111z00_9946), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1685z00_9952)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1112z00_9947), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1685z00_9952)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31113zf3_9948), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1685z00_9952)))->BgL_keyz00) =
						((long) BgL_key1114z00_9949), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1685z00_9952)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1115zd2_8495), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1685z00_9952)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1116z00_9950), BUNSPEC);
					{
						BgL_localzf2livenesszf2_bglt BgL_auxz00_19332;

						{
							obj_t BgL_auxz00_19333;

							{	/* Liveness/types.scm 26 */
								BgL_objectz00_bglt BgL_tmpz00_19334;

								BgL_tmpz00_19334 = ((BgL_objectz00_bglt) BgL_new1685z00_9952);
								BgL_auxz00_19333 = BGL_OBJECT_WIDENING(BgL_tmpz00_19334);
							}
							BgL_auxz00_19332 =
								((BgL_localzf2livenesszf2_bglt) BgL_auxz00_19333);
						}
						((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19332))->
								BgL_z52countz52) = ((int) BgL_z52count1117z52_9951), BUNSPEC);
					}
					return BgL_new1685z00_9952;
				}
			}
		}

	}



/* &<@anonymous:2063> */
	obj_t BGl_z62zc3z04anonymousza32063ze3ze5zzliveness_typesz00(obj_t
		BgL_envz00_8498)
	{
		{	/* Liveness/types.scm 26 */
			return BINT(0L);
		}

	}



/* &lambda2062 */
	obj_t BGl_z62lambda2062z62zzliveness_typesz00(obj_t BgL_envz00_8499,
		obj_t BgL_oz00_8500, obj_t BgL_vz00_8501)
	{
		{	/* Liveness/types.scm 26 */
			{	/* Liveness/types.scm 26 */
				int BgL_vz00_9959;

				BgL_vz00_9959 = CINT(BgL_vz00_8501);
				{
					BgL_localzf2livenesszf2_bglt BgL_auxz00_19341;

					{
						obj_t BgL_auxz00_19342;

						{	/* Liveness/types.scm 26 */
							BgL_objectz00_bglt BgL_tmpz00_19343;

							BgL_tmpz00_19343 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_8500));
							BgL_auxz00_19342 = BGL_OBJECT_WIDENING(BgL_tmpz00_19343);
						}
						BgL_auxz00_19341 =
							((BgL_localzf2livenesszf2_bglt) BgL_auxz00_19342);
					}
					return
						((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19341))->
							BgL_z52countz52) = ((int) BgL_vz00_9959), BUNSPEC);
		}}}

	}



/* &lambda2061 */
	obj_t BGl_z62lambda2061z62zzliveness_typesz00(obj_t BgL_envz00_8502,
		obj_t BgL_oz00_8503)
	{
		{	/* Liveness/types.scm 26 */
			{	/* Liveness/types.scm 26 */
				int BgL_tmpz00_19349;

				{
					BgL_localzf2livenesszf2_bglt BgL_auxz00_19350;

					{
						obj_t BgL_auxz00_19351;

						{	/* Liveness/types.scm 26 */
							BgL_objectz00_bglt BgL_tmpz00_19352;

							BgL_tmpz00_19352 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_8503));
							BgL_auxz00_19351 = BGL_OBJECT_WIDENING(BgL_tmpz00_19352);
						}
						BgL_auxz00_19350 =
							((BgL_localzf2livenesszf2_bglt) BgL_auxz00_19351);
					}
					BgL_tmpz00_19349 =
						(((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_19350))->
						BgL_z52countz52);
				}
				return BINT(BgL_tmpz00_19349);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzliveness_typesz00(void)
	{
		{	/* Liveness/types.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzliveness_typesz00(void)
	{
		{	/* Liveness/types.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzliveness_typesz00(void)
	{
		{	/* Liveness/types.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string4517z00zzliveness_typesz00));
		}

	}

#ifdef __cplusplus
}
#endif
