/*===========================================================================*/
/*   (Liveness/set.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Liveness/set.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_LIVENESS_SET_TYPE_DEFINITIONS
#define BGL_LIVENESS_SET_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_localzf2livenesszf2_bgl
	{
		int BgL_z52countz52;
	}                          *BgL_localzf2livenesszf2_bglt;


#endif													// BGL_LIVENESS_SET_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_unionza2za2zzliveness_setz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_union3z00zzliveness_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzliveness_setz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_disjonctionz00zzliveness_setz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzliveness_setz00(void);
	static obj_t BGl_genericzd2initzd2zzliveness_setz00(void);
	BGL_EXPORTED_DECL obj_t BGl_addz00zzliveness_setz00(BgL_localz00_bglt, obj_t);
	static obj_t BGl_objectzd2initzd2zzliveness_setz00(void);
	BGL_EXPORTED_DECL obj_t BGl_intersectionza2za2zzliveness_setz00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_intersectionz00zzliveness_setz00(obj_t, obj_t);
	static long BGl_getzd2markzd2zzliveness_setz00(void);
	static obj_t BGl_z62unionza2zc0zzliveness_setz00(obj_t, obj_t);
	static obj_t BGl_z62union3z62zzliveness_setz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzliveness_setz00(void);
	static obj_t BGl_z62subsetzf3z91zzliveness_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62disjonctionz62zzliveness_setz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_localzf2livenesszf2zzliveness_typesz00;
	static long BGl_za2markza2z00zzliveness_setz00 = 0L;
	BGL_EXPORTED_DECL obj_t BGl_unionz00zzliveness_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzliveness_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzliveness_typesz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62unionz62zzliveness_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzliveness_setz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzliveness_setz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzliveness_setz00(void);
	static obj_t BGl_z62inzf3z91zzliveness_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62intersectionz62zzliveness_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62intersectionza2zc0zzliveness_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_subsetzf3zf3zzliveness_setz00(obj_t, obj_t);
	static obj_t BGl_z62addz62zzliveness_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_variablezd2markz12ze70z27zzliveness_setz00(obj_t);
	static obj_t BGl_variablezd2markz12ze71z27zzliveness_setz00(obj_t, long,
		obj_t);
	static obj_t BGl_variablezd2markz12ze72z27zzliveness_setz00(obj_t, long,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_inzf3zf3zzliveness_setz00(BgL_localz00_bglt,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_intersectionzd2envzd2zzliveness_setz00,
		BgL_bgl_za762intersectionza71843z00,
		BGl_z62intersectionz62zzliveness_setz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_addzd2envzd2zzliveness_setz00,
		BgL_bgl_za762addza762za7za7liven1844z00, BGl_z62addz62zzliveness_setz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_intersectionza2zd2envz70zzliveness_setz00,
		BgL_bgl_za762intersectionza71845z00, va_generic_entry,
		BGl_z62intersectionza2zc0zzliveness_setz00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_unionza2zd2envz70zzliveness_setz00,
		BgL_bgl_za762unionza7a2za7c0za7za71846za7, va_generic_entry,
		BGl_z62unionza2zc0zzliveness_setz00, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string1842z00zzliveness_setz00,
		BgL_bgl_string1842za700za7za7l1847za7, "liveness_set", 12);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_subsetzf3zd2envz21zzliveness_setz00,
		BgL_bgl_za762subsetza7f3za791za71848z00,
		BGl_z62subsetzf3z91zzliveness_setz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_union3zd2envzd2zzliveness_setz00,
		BgL_bgl_za762union3za762za7za7li1849z00, BGl_z62union3z62zzliveness_setz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_disjonctionzd2envzd2zzliveness_setz00,
		BgL_bgl_za762disjonctionza761850z00, BGl_z62disjonctionz62zzliveness_setz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_unionzd2envzd2zzliveness_setz00,
		BgL_bgl_za762unionza762za7za7liv1851z00, BGl_z62unionz62zzliveness_setz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_inzf3zd2envz21zzliveness_setz00,
		BgL_bgl_za762inza7f3za791za7za7liv1852za7, BGl_z62inzf3z91zzliveness_setz00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzliveness_setz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzliveness_setz00(long
		BgL_checksumz00_2005, char *BgL_fromz00_2006)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzliveness_setz00))
				{
					BGl_requirezd2initializa7ationz75zzliveness_setz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzliveness_setz00();
					BGl_libraryzd2moduleszd2initz00zzliveness_setz00();
					BGl_importedzd2moduleszd2initz00zzliveness_setz00();
					return BGl_toplevelzd2initzd2zzliveness_setz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzliveness_setz00(void)
	{
		{	/* Liveness/set.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "liveness_set");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"liveness_set");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"liveness_set");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "liveness_set");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"liveness_set");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "liveness_set");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzliveness_setz00(void)
	{
		{	/* Liveness/set.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzliveness_setz00(void)
	{
		{	/* Liveness/set.scm 15 */
			return (BGl_za2markza2z00zzliveness_setz00 = 1024L, BUNSPEC);
		}

	}



/* get-mark */
	long BGl_getzd2markzd2zzliveness_setz00(void)
	{
		{	/* Liveness/set.scm 47 */
			BGl_za2markza2z00zzliveness_setz00 =
				(1L + BGl_za2markza2z00zzliveness_setz00);
			return BGl_za2markza2z00zzliveness_setz00;
		}

	}



/* in? */
	BGL_EXPORTED_DEF obj_t BGl_inzf3zf3zzliveness_setz00(BgL_localz00_bglt
		BgL_elz00_4, obj_t BgL_setz00_5)
	{
		{	/* Liveness/set.scm 61 */
			{	/* Liveness/set.scm 62 */
				bool_t BgL_test1854z00_2022;

				{	/* Liveness/set.scm 62 */
					obj_t BgL_classz00_1823;

					BgL_classz00_1823 = BGl_localzf2livenesszf2zzliveness_typesz00;
					{	/* Liveness/set.scm 62 */
						BgL_objectz00_bglt BgL_arg1807z00_1825;

						{	/* Liveness/set.scm 62 */
							obj_t BgL_tmpz00_2023;

							BgL_tmpz00_2023 = ((obj_t) BgL_elz00_4);
							BgL_arg1807z00_1825 = (BgL_objectz00_bglt) (BgL_tmpz00_2023);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Liveness/set.scm 62 */
								long BgL_idxz00_1831;

								BgL_idxz00_1831 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1825);
								BgL_test1854z00_2022 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_1831 + 3L)) == BgL_classz00_1823);
							}
						else
							{	/* Liveness/set.scm 62 */
								bool_t BgL_res1840z00_1856;

								{	/* Liveness/set.scm 62 */
									obj_t BgL_oclassz00_1839;

									{	/* Liveness/set.scm 62 */
										obj_t BgL_arg1815z00_1847;
										long BgL_arg1816z00_1848;

										BgL_arg1815z00_1847 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Liveness/set.scm 62 */
											long BgL_arg1817z00_1849;

											BgL_arg1817z00_1849 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1825);
											BgL_arg1816z00_1848 = (BgL_arg1817z00_1849 - OBJECT_TYPE);
										}
										BgL_oclassz00_1839 =
											VECTOR_REF(BgL_arg1815z00_1847, BgL_arg1816z00_1848);
									}
									{	/* Liveness/set.scm 62 */
										bool_t BgL__ortest_1115z00_1840;

										BgL__ortest_1115z00_1840 =
											(BgL_classz00_1823 == BgL_oclassz00_1839);
										if (BgL__ortest_1115z00_1840)
											{	/* Liveness/set.scm 62 */
												BgL_res1840z00_1856 = BgL__ortest_1115z00_1840;
											}
										else
											{	/* Liveness/set.scm 62 */
												long BgL_odepthz00_1841;

												{	/* Liveness/set.scm 62 */
													obj_t BgL_arg1804z00_1842;

													BgL_arg1804z00_1842 = (BgL_oclassz00_1839);
													BgL_odepthz00_1841 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_1842);
												}
												if ((3L < BgL_odepthz00_1841))
													{	/* Liveness/set.scm 62 */
														obj_t BgL_arg1802z00_1844;

														{	/* Liveness/set.scm 62 */
															obj_t BgL_arg1803z00_1845;

															BgL_arg1803z00_1845 = (BgL_oclassz00_1839);
															BgL_arg1802z00_1844 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1845,
																3L);
														}
														BgL_res1840z00_1856 =
															(BgL_arg1802z00_1844 == BgL_classz00_1823);
													}
												else
													{	/* Liveness/set.scm 62 */
														BgL_res1840z00_1856 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test1854z00_2022 = BgL_res1840z00_1856;
							}
					}
				}
				if (BgL_test1854z00_2022)
					{	/* Liveness/set.scm 62 */
						return
							BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
							((obj_t) BgL_elz00_4), BgL_setz00_5);
					}
				else
					{	/* Liveness/set.scm 62 */
						return BFALSE;
					}
			}
		}

	}



/* &in? */
	obj_t BGl_z62inzf3z91zzliveness_setz00(obj_t BgL_envz00_1969,
		obj_t BgL_elz00_1970, obj_t BgL_setz00_1971)
	{
		{	/* Liveness/set.scm 61 */
			return
				BGl_inzf3zf3zzliveness_setz00(
				((BgL_localz00_bglt) BgL_elz00_1970), BgL_setz00_1971);
		}

	}



/* subset? */
	BGL_EXPORTED_DEF obj_t BGl_subsetzf3zf3zzliveness_setz00(obj_t BgL_set1z00_6,
		obj_t BgL_set2z00_7)
	{
		{	/* Liveness/set.scm 70 */
			{
				obj_t BgL_l1271z00_1383;

				BgL_l1271z00_1383 = BgL_set1z00_6;
			BgL_zc3z04anonymousza31335ze3z87_1384:
				if (PAIRP(BgL_l1271z00_1383))
					{	/* Liveness/set.scm 76 */
						{	/* Liveness/set.scm 76 */
							obj_t BgL_arg1339z00_1386;

							BgL_arg1339z00_1386 = CAR(BgL_l1271z00_1383);
							{
								BgL_localzf2livenesszf2_bglt BgL_auxz00_2052;

								{
									obj_t BgL_auxz00_2053;

									{	/* Liveness/set.scm 56 */
										BgL_objectz00_bglt BgL_tmpz00_2054;

										BgL_tmpz00_2054 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_arg1339z00_1386));
										BgL_auxz00_2053 = BGL_OBJECT_WIDENING(BgL_tmpz00_2054);
									}
									BgL_auxz00_2052 =
										((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2053);
								}
								((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2052))->
										BgL_z52countz52) = ((int) (int) (0L)), BUNSPEC);
						}}
						{
							obj_t BgL_l1271z00_2061;

							BgL_l1271z00_2061 = CDR(BgL_l1271z00_1383);
							BgL_l1271z00_1383 = BgL_l1271z00_2061;
							goto BgL_zc3z04anonymousza31335ze3z87_1384;
						}
					}
				else
					{	/* Liveness/set.scm 76 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1273z00_1390;

				BgL_l1273z00_1390 = BgL_set2z00_7;
			BgL_zc3z04anonymousza31341ze3z87_1391:
				if (PAIRP(BgL_l1273z00_1390))
					{	/* Liveness/set.scm 77 */
						{	/* Liveness/set.scm 77 */
							obj_t BgL_arg1343z00_1393;

							BgL_arg1343z00_1393 = CAR(BgL_l1273z00_1390);
							{
								BgL_localzf2livenesszf2_bglt BgL_auxz00_2066;

								{
									obj_t BgL_auxz00_2067;

									{	/* Liveness/set.scm 74 */
										BgL_objectz00_bglt BgL_tmpz00_2068;

										BgL_tmpz00_2068 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_arg1343z00_1393));
										BgL_auxz00_2067 = BGL_OBJECT_WIDENING(BgL_tmpz00_2068);
									}
									BgL_auxz00_2066 =
										((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2067);
								}
								((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2066))->
										BgL_z52countz52) = ((int) (int) (1L)), BUNSPEC);
						}}
						{
							obj_t BgL_l1273z00_2075;

							BgL_l1273z00_2075 = CDR(BgL_l1273z00_1390);
							BgL_l1273z00_1390 = BgL_l1273z00_2075;
							goto BgL_zc3z04anonymousza31341ze3z87_1391;
						}
					}
				else
					{	/* Liveness/set.scm 77 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1275z00_1397;

				{	/* Liveness/set.scm 79 */
					bool_t BgL_tmpz00_2077;

					BgL_l1275z00_1397 = BgL_set1z00_6;
				BgL_zc3z04anonymousza31347ze3z87_1398:
					if (NULLP(BgL_l1275z00_1397))
						{	/* Liveness/set.scm 79 */
							BgL_tmpz00_2077 = ((bool_t) 1);
						}
					else
						{	/* Liveness/set.scm 79 */
							bool_t BgL_test1861z00_2080;

							{	/* Liveness/set.scm 80 */
								BgL_localz00_bglt BgL_i1144z00_1404;

								BgL_i1144z00_1404 =
									((BgL_localz00_bglt) CAR(((obj_t) BgL_l1275z00_1397)));
								{	/* Liveness/set.scm 81 */
									int BgL_arg1351z00_1405;

									{
										BgL_localzf2livenesszf2_bglt BgL_auxz00_2084;

										{
											obj_t BgL_auxz00_2085;

											{	/* Liveness/set.scm 81 */
												BgL_objectz00_bglt BgL_tmpz00_2086;

												BgL_tmpz00_2086 =
													((BgL_objectz00_bglt) BgL_i1144z00_1404);
												BgL_auxz00_2085 = BGL_OBJECT_WIDENING(BgL_tmpz00_2086);
											}
											BgL_auxz00_2084 =
												((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2085);
										}
										BgL_arg1351z00_1405 =
											(((BgL_localzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_2084))->BgL_z52countz52);
									}
									BgL_test1861z00_2080 = ((long) (BgL_arg1351z00_1405) == 1L);
							}}
							if (BgL_test1861z00_2080)
								{
									obj_t BgL_l1275z00_2093;

									BgL_l1275z00_2093 = CDR(((obj_t) BgL_l1275z00_1397));
									BgL_l1275z00_1397 = BgL_l1275z00_2093;
									goto BgL_zc3z04anonymousza31347ze3z87_1398;
								}
							else
								{	/* Liveness/set.scm 79 */
									BgL_tmpz00_2077 = ((bool_t) 0);
								}
						}
					return BBOOL(BgL_tmpz00_2077);
				}
			}
		}

	}



/* &subset? */
	obj_t BGl_z62subsetzf3z91zzliveness_setz00(obj_t BgL_envz00_1972,
		obj_t BgL_set1z00_1973, obj_t BgL_set2z00_1974)
	{
		{	/* Liveness/set.scm 70 */
			return
				BGl_subsetzf3zf3zzliveness_setz00(BgL_set1z00_1973, BgL_set2z00_1974);
		}

	}



/* union */
	BGL_EXPORTED_DEF obj_t BGl_unionz00zzliveness_setz00(obj_t BgL_l1z00_8,
		obj_t BgL_l2z00_9)
	{
		{	/* Liveness/set.scm 89 */
			{	/* Liveness/set.scm 91 */
				struct bgl_cell BgL_box1862_2003z00;
				obj_t BgL_resz00_2003;

				BgL_resz00_2003 = MAKE_CELL_STACK(BNIL, BgL_box1862_2003z00);
				{	/* Liveness/set.scm 93 */
					long BgL_markz00_1412;

					BgL_markz00_1412 = BGl_getzd2markzd2zzliveness_setz00();
					if (NULLP(BgL_l1z00_8))
						{	/* Liveness/set.scm 102 */
							return BgL_l2z00_9;
						}
					else
						{	/* Liveness/set.scm 102 */
							if (NULLP(BgL_l2z00_9))
								{	/* Liveness/set.scm 104 */
									return BgL_l1z00_8;
								}
							else
								{	/* Liveness/set.scm 104 */
									{
										obj_t BgL_l1278z00_1417;

										BgL_l1278z00_1417 = BgL_l1z00_8;
									BgL_zc3z04anonymousza31356ze3z87_1418:
										if (PAIRP(BgL_l1278z00_1417))
											{	/* Liveness/set.scm 107 */
												BGl_variablezd2markz12ze72z27zzliveness_setz00
													(BgL_resz00_2003, BgL_markz00_1412,
													CAR(BgL_l1278z00_1417));
												{
													obj_t BgL_l1278z00_2107;

													BgL_l1278z00_2107 = CDR(BgL_l1278z00_1417);
													BgL_l1278z00_1417 = BgL_l1278z00_2107;
													goto BgL_zc3z04anonymousza31356ze3z87_1418;
												}
											}
										else
											{	/* Liveness/set.scm 107 */
												((bool_t) 1);
											}
									}
									{
										obj_t BgL_l1280z00_1424;

										BgL_l1280z00_1424 = BgL_l2z00_9;
									BgL_zc3z04anonymousza31365ze3z87_1425:
										if (PAIRP(BgL_l1280z00_1424))
											{	/* Liveness/set.scm 108 */
												BGl_variablezd2markz12ze72z27zzliveness_setz00
													(BgL_resz00_2003, BgL_markz00_1412,
													CAR(BgL_l1280z00_1424));
												{
													obj_t BgL_l1280z00_2113;

													BgL_l1280z00_2113 = CDR(BgL_l1280z00_1424);
													BgL_l1280z00_1424 = BgL_l1280z00_2113;
													goto BgL_zc3z04anonymousza31365ze3z87_1425;
												}
											}
										else
											{	/* Liveness/set.scm 108 */
												((bool_t) 1);
											}
									}
									return ((obj_t) ((obj_t) CELL_REF(BgL_resz00_2003)));
								}
						}
				}
			}
		}

	}



/* variable-mark!~2 */
	obj_t BGl_variablezd2markz12ze72z27zzliveness_setz00(obj_t BgL_resz00_2001,
		long BgL_markz00_2000, obj_t BgL_vz00_1430)
	{
		{	/* Liveness/set.scm 99 */
			{	/* Liveness/set.scm 97 */
				bool_t BgL_test1867z00_2116;

				{	/* Liveness/set.scm 97 */
					int BgL_arg1408z00_1435;

					{
						BgL_localzf2livenesszf2_bglt BgL_auxz00_2117;

						{
							obj_t BgL_auxz00_2118;

							{	/* Liveness/set.scm 97 */
								BgL_objectz00_bglt BgL_tmpz00_2119;

								BgL_tmpz00_2119 =
									((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_1430));
								BgL_auxz00_2118 = BGL_OBJECT_WIDENING(BgL_tmpz00_2119);
							}
							BgL_auxz00_2117 =
								((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2118);
						}
						BgL_arg1408z00_1435 =
							(((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2117))->
							BgL_z52countz52);
					}
					BgL_test1867z00_2116 =
						((long) (BgL_arg1408z00_1435) == BgL_markz00_2000);
				}
				if (BgL_test1867z00_2116)
					{	/* Liveness/set.scm 97 */
						return BFALSE;
					}
				else
					{	/* Liveness/set.scm 97 */
						{
							BgL_localzf2livenesszf2_bglt BgL_auxz00_2127;

							{
								obj_t BgL_auxz00_2128;

								{	/* Liveness/set.scm 98 */
									BgL_objectz00_bglt BgL_tmpz00_2129;

									BgL_tmpz00_2129 =
										((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_1430));
									BgL_auxz00_2128 = BGL_OBJECT_WIDENING(BgL_tmpz00_2129);
								}
								BgL_auxz00_2127 =
									((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2128);
							}
							((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2127))->
									BgL_z52countz52) = ((int) (int) (BgL_markz00_2000)), BUNSPEC);
						}
						{	/* Liveness/set.scm 99 */
							obj_t BgL_auxz00_2002;

							BgL_auxz00_2002 =
								MAKE_YOUNG_PAIR(BgL_vz00_1430,
								((obj_t) ((obj_t) CELL_REF(BgL_resz00_2001))));
							return CELL_SET(BgL_resz00_2001, BgL_auxz00_2002);
						}
					}
			}
		}

	}



/* &union */
	obj_t BGl_z62unionz62zzliveness_setz00(obj_t BgL_envz00_1975,
		obj_t BgL_l1z00_1976, obj_t BgL_l2z00_1977)
	{
		{	/* Liveness/set.scm 89 */
			return BGl_unionz00zzliveness_setz00(BgL_l1z00_1976, BgL_l2z00_1977);
		}

	}



/* union3 */
	BGL_EXPORTED_DEF obj_t BGl_union3z00zzliveness_setz00(obj_t BgL_l1z00_10,
		obj_t BgL_l2z00_11, obj_t BgL_l3z00_12)
	{
		{	/* Liveness/set.scm 116 */
			{	/* Liveness/set.scm 118 */
				struct bgl_cell BgL_box1868_1998z00;
				obj_t BgL_resz00_1998;

				BgL_resz00_1998 = MAKE_CELL_STACK(BNIL, BgL_box1868_1998z00);
				{	/* Liveness/set.scm 120 */
					long BgL_markz00_1438;

					BgL_markz00_1438 = BGl_getzd2markzd2zzliveness_setz00();
					if (NULLP(BgL_l1z00_10))
						{	/* Liveness/set.scm 129 */
							return BGl_unionz00zzliveness_setz00(BgL_l2z00_11, BgL_l3z00_12);
						}
					else
						{	/* Liveness/set.scm 129 */
							if (NULLP(BgL_l2z00_11))
								{	/* Liveness/set.scm 131 */
									return
										BGl_unionz00zzliveness_setz00(BgL_l1z00_10, BgL_l3z00_12);
								}
							else
								{	/* Liveness/set.scm 131 */
									if (NULLP(BgL_l3z00_12))
										{	/* Liveness/set.scm 133 */
											return
												BGl_unionz00zzliveness_setz00(BgL_l1z00_10,
												BgL_l2z00_11);
										}
									else
										{	/* Liveness/set.scm 133 */
											{
												obj_t BgL_l1282z00_1444;

												BgL_l1282z00_1444 = BgL_l1z00_10;
											BgL_zc3z04anonymousza31413ze3z87_1445:
												if (PAIRP(BgL_l1282z00_1444))
													{	/* Liveness/set.scm 136 */
														BGl_variablezd2markz12ze71z27zzliveness_setz00
															(BgL_resz00_1998, BgL_markz00_1438,
															CAR(BgL_l1282z00_1444));
														{
															obj_t BgL_l1282z00_2153;

															BgL_l1282z00_2153 = CDR(BgL_l1282z00_1444);
															BgL_l1282z00_1444 = BgL_l1282z00_2153;
															goto BgL_zc3z04anonymousza31413ze3z87_1445;
														}
													}
												else
													{	/* Liveness/set.scm 136 */
														((bool_t) 1);
													}
											}
											{
												obj_t BgL_l1284z00_1451;

												BgL_l1284z00_1451 = BgL_l2z00_11;
											BgL_zc3z04anonymousza31423ze3z87_1452:
												if (PAIRP(BgL_l1284z00_1451))
													{	/* Liveness/set.scm 137 */
														BGl_variablezd2markz12ze71z27zzliveness_setz00
															(BgL_resz00_1998, BgL_markz00_1438,
															CAR(BgL_l1284z00_1451));
														{
															obj_t BgL_l1284z00_2159;

															BgL_l1284z00_2159 = CDR(BgL_l1284z00_1451);
															BgL_l1284z00_1451 = BgL_l1284z00_2159;
															goto BgL_zc3z04anonymousza31423ze3z87_1452;
														}
													}
												else
													{	/* Liveness/set.scm 137 */
														((bool_t) 1);
													}
											}
											{
												obj_t BgL_l1286z00_1458;

												BgL_l1286z00_1458 = BgL_l3z00_12;
											BgL_zc3z04anonymousza31438ze3z87_1459:
												if (PAIRP(BgL_l1286z00_1458))
													{	/* Liveness/set.scm 138 */
														BGl_variablezd2markz12ze71z27zzliveness_setz00
															(BgL_resz00_1998, BgL_markz00_1438,
															CAR(BgL_l1286z00_1458));
														{
															obj_t BgL_l1286z00_2165;

															BgL_l1286z00_2165 = CDR(BgL_l1286z00_1458);
															BgL_l1286z00_1458 = BgL_l1286z00_2165;
															goto BgL_zc3z04anonymousza31438ze3z87_1459;
														}
													}
												else
													{	/* Liveness/set.scm 138 */
														((bool_t) 1);
													}
											}
											return ((obj_t) ((obj_t) CELL_REF(BgL_resz00_1998)));
										}
								}
						}
				}
			}
		}

	}



/* variable-mark!~1 */
	obj_t BGl_variablezd2markz12ze71z27zzliveness_setz00(obj_t BgL_resz00_1996,
		long BgL_markz00_1995, obj_t BgL_vz00_1464)
	{
		{	/* Liveness/set.scm 126 */
			{	/* Liveness/set.scm 124 */
				bool_t BgL_test1875z00_2168;

				{	/* Liveness/set.scm 124 */
					int BgL_arg1485z00_1469;

					{
						BgL_localzf2livenesszf2_bglt BgL_auxz00_2169;

						{
							obj_t BgL_auxz00_2170;

							{	/* Liveness/set.scm 124 */
								BgL_objectz00_bglt BgL_tmpz00_2171;

								BgL_tmpz00_2171 =
									((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_1464));
								BgL_auxz00_2170 = BGL_OBJECT_WIDENING(BgL_tmpz00_2171);
							}
							BgL_auxz00_2169 =
								((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2170);
						}
						BgL_arg1485z00_1469 =
							(((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2169))->
							BgL_z52countz52);
					}
					BgL_test1875z00_2168 =
						((long) (BgL_arg1485z00_1469) == BgL_markz00_1995);
				}
				if (BgL_test1875z00_2168)
					{	/* Liveness/set.scm 124 */
						{
							BgL_localzf2livenesszf2_bglt BgL_auxz00_2179;

							{
								obj_t BgL_auxz00_2180;

								{	/* Liveness/set.scm 125 */
									BgL_objectz00_bglt BgL_tmpz00_2181;

									BgL_tmpz00_2181 =
										((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_1464));
									BgL_auxz00_2180 = BGL_OBJECT_WIDENING(BgL_tmpz00_2181);
								}
								BgL_auxz00_2179 =
									((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2180);
							}
							((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2179))->
									BgL_z52countz52) = ((int) (int) (BgL_markz00_1995)), BUNSPEC);
						}
						{	/* Liveness/set.scm 126 */
							obj_t BgL_auxz00_1997;

							BgL_auxz00_1997 =
								MAKE_YOUNG_PAIR(BgL_vz00_1464,
								((obj_t) ((obj_t) CELL_REF(BgL_resz00_1996))));
							return CELL_SET(BgL_resz00_1996, BgL_auxz00_1997);
						}
					}
				else
					{	/* Liveness/set.scm 124 */
						return BFALSE;
					}
			}
		}

	}



/* &union3 */
	obj_t BGl_z62union3z62zzliveness_setz00(obj_t BgL_envz00_1978,
		obj_t BgL_l1z00_1979, obj_t BgL_l2z00_1980, obj_t BgL_l3z00_1981)
	{
		{	/* Liveness/set.scm 116 */
			return
				BGl_union3z00zzliveness_setz00(BgL_l1z00_1979, BgL_l2z00_1980,
				BgL_l3z00_1981);
		}

	}



/* union* */
	BGL_EXPORTED_DEF obj_t BGl_unionza2za2zzliveness_setz00(obj_t BgL_lsz00_13)
	{
		{	/* Liveness/set.scm 144 */
			{	/* Liveness/set.scm 146 */
				obj_t BgL_resz00_1471;

				BgL_resz00_1471 = BNIL;
				{	/* Liveness/set.scm 148 */
					long BgL_markz00_1472;

					BgL_markz00_1472 = BGl_getzd2markzd2zzliveness_setz00();
					{
						obj_t BgL_vz00_1502;

						{
							obj_t BgL_l1290z00_1475;

							BgL_l1290z00_1475 = BgL_lsz00_13;
						BgL_zc3z04anonymousza31486ze3z87_1476:
							if (PAIRP(BgL_l1290z00_1475))
								{	/* Liveness/set.scm 156 */
									{	/* Liveness/set.scm 156 */
										obj_t BgL_lz00_1478;

										BgL_lz00_1478 = CAR(BgL_l1290z00_1475);
										{
											obj_t BgL_l1288z00_1480;

											BgL_l1288z00_1480 = BgL_lz00_1478;
										BgL_zc3z04anonymousza31488ze3z87_1481:
											if (PAIRP(BgL_l1288z00_1480))
												{	/* Liveness/set.scm 156 */
													{	/* Liveness/set.scm 156 */
														obj_t BgL_arg1502z00_1483;

														BgL_arg1502z00_1483 = CAR(BgL_l1288z00_1480);
														{
															BgL_localzf2livenesszf2_bglt BgL_auxz00_2198;

															{
																obj_t BgL_auxz00_2199;

																{	/* Liveness/set.scm 56 */
																	BgL_objectz00_bglt BgL_tmpz00_2200;

																	BgL_tmpz00_2200 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt) BgL_arg1502z00_1483));
																	BgL_auxz00_2199 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2200);
																}
																BgL_auxz00_2198 =
																	((BgL_localzf2livenesszf2_bglt)
																	BgL_auxz00_2199);
															}
															((((BgL_localzf2livenesszf2_bglt)
																		COBJECT(BgL_auxz00_2198))->
																	BgL_z52countz52) =
																((int) (int) (0L)), BUNSPEC);
													}}
													{
														obj_t BgL_l1288z00_2207;

														BgL_l1288z00_2207 = CDR(BgL_l1288z00_1480);
														BgL_l1288z00_1480 = BgL_l1288z00_2207;
														goto BgL_zc3z04anonymousza31488ze3z87_1481;
													}
												}
											else
												{	/* Liveness/set.scm 156 */
													((bool_t) 1);
												}
										}
									}
									{
										obj_t BgL_l1290z00_2209;

										BgL_l1290z00_2209 = CDR(BgL_l1290z00_1475);
										BgL_l1290z00_1475 = BgL_l1290z00_2209;
										goto BgL_zc3z04anonymousza31486ze3z87_1476;
									}
								}
							else
								{	/* Liveness/set.scm 156 */
									((bool_t) 1);
								}
						}
						{
							obj_t BgL_l1294z00_1489;

							BgL_l1294z00_1489 = BgL_lsz00_13;
						BgL_zc3z04anonymousza31514ze3z87_1490:
							if (PAIRP(BgL_l1294z00_1489))
								{	/* Liveness/set.scm 157 */
									{	/* Liveness/set.scm 157 */
										obj_t BgL_lz00_1492;

										BgL_lz00_1492 = CAR(BgL_l1294z00_1489);
										{
											obj_t BgL_l1292z00_1494;

											BgL_l1292z00_1494 = BgL_lz00_1492;
										BgL_zc3z04anonymousza31516ze3z87_1495:
											if (PAIRP(BgL_l1292z00_1494))
												{	/* Liveness/set.scm 157 */
													BgL_vz00_1502 = CAR(BgL_l1292z00_1494);
													{	/* Liveness/set.scm 152 */
														bool_t BgL_test1880z00_2216;

														{	/* Liveness/set.scm 152 */
															int BgL_arg1552z00_1507;

															{
																BgL_localzf2livenesszf2_bglt BgL_auxz00_2217;

																{
																	obj_t BgL_auxz00_2218;

																	{	/* Liveness/set.scm 152 */
																		BgL_objectz00_bglt BgL_tmpz00_2219;

																		BgL_tmpz00_2219 =
																			((BgL_objectz00_bglt)
																			((BgL_localz00_bglt) BgL_vz00_1502));
																		BgL_auxz00_2218 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2219);
																	}
																	BgL_auxz00_2217 =
																		((BgL_localzf2livenesszf2_bglt)
																		BgL_auxz00_2218);
																}
																BgL_arg1552z00_1507 =
																	(((BgL_localzf2livenesszf2_bglt)
																		COBJECT(BgL_auxz00_2217))->BgL_z52countz52);
															}
															BgL_test1880z00_2216 =
																((long) (BgL_arg1552z00_1507) == 0L);
														}
														if (BgL_test1880z00_2216)
															{	/* Liveness/set.scm 152 */
																{
																	BgL_localzf2livenesszf2_bglt BgL_auxz00_2227;

																	{
																		obj_t BgL_auxz00_2228;

																		{	/* Liveness/set.scm 153 */
																			BgL_objectz00_bglt BgL_tmpz00_2229;

																			BgL_tmpz00_2229 =
																				((BgL_objectz00_bglt)
																				((BgL_localz00_bglt) BgL_vz00_1502));
																			BgL_auxz00_2228 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2229);
																		}
																		BgL_auxz00_2227 =
																			((BgL_localzf2livenesszf2_bglt)
																			BgL_auxz00_2228);
																	}
																	((((BgL_localzf2livenesszf2_bglt)
																				COBJECT(BgL_auxz00_2227))->
																			BgL_z52countz52) =
																		((int) (int) (1L)), BUNSPEC);
																}
																BgL_resz00_1471 =
																	MAKE_YOUNG_PAIR(BgL_vz00_1502,
																	BgL_resz00_1471);
															}
														else
															{	/* Liveness/set.scm 152 */
																BFALSE;
															}
													}
													{
														obj_t BgL_l1292z00_2238;

														BgL_l1292z00_2238 = CDR(BgL_l1292z00_1494);
														BgL_l1292z00_1494 = BgL_l1292z00_2238;
														goto BgL_zc3z04anonymousza31516ze3z87_1495;
													}
												}
											else
												{	/* Liveness/set.scm 157 */
													((bool_t) 1);
												}
										}
									}
									{
										obj_t BgL_l1294z00_2240;

										BgL_l1294z00_2240 = CDR(BgL_l1294z00_1489);
										BgL_l1294z00_1489 = BgL_l1294z00_2240;
										goto BgL_zc3z04anonymousza31514ze3z87_1490;
									}
								}
							else
								{	/* Liveness/set.scm 157 */
									((bool_t) 1);
								}
						}
						return BgL_resz00_1471;
					}
				}
			}
		}

	}



/* &union* */
	obj_t BGl_z62unionza2zc0zzliveness_setz00(obj_t BgL_envz00_1982,
		obj_t BgL_lsz00_1983)
	{
		{	/* Liveness/set.scm 144 */
			return BGl_unionza2za2zzliveness_setz00(BgL_lsz00_1983);
		}

	}



/* add */
	BGL_EXPORTED_DEF obj_t BGl_addz00zzliveness_setz00(BgL_localz00_bglt
		BgL_elz00_14, obj_t BgL_lz00_15)
	{
		{	/* Liveness/set.scm 163 */
			{	/* Liveness/set.scm 164 */
				obj_t BgL_arg1553z00_1902;

				{	/* Liveness/set.scm 164 */
					obj_t BgL_list1554z00_1903;

					BgL_list1554z00_1903 = MAKE_YOUNG_PAIR(((obj_t) BgL_elz00_14), BNIL);
					BgL_arg1553z00_1902 = BgL_list1554z00_1903;
				}
				return BGl_unionz00zzliveness_setz00(BgL_arg1553z00_1902, BgL_lz00_15);
			}
		}

	}



/* &add */
	obj_t BGl_z62addz62zzliveness_setz00(obj_t BgL_envz00_1984,
		obj_t BgL_elz00_1985, obj_t BgL_lz00_1986)
	{
		{	/* Liveness/set.scm 163 */
			return
				BGl_addz00zzliveness_setz00(
				((BgL_localz00_bglt) BgL_elz00_1985), BgL_lz00_1986);
		}

	}



/* intersection */
	BGL_EXPORTED_DEF obj_t BGl_intersectionz00zzliveness_setz00(obj_t
		BgL_l1z00_16, obj_t BgL_l2z00_17)
	{
		{	/* Liveness/set.scm 171 */
			{
				obj_t BgL_l1296z00_1513;

				BgL_l1296z00_1513 = BgL_l1z00_16;
			BgL_zc3z04anonymousza31555ze3z87_1514:
				if (PAIRP(BgL_l1296z00_1513))
					{	/* Liveness/set.scm 177 */
						{	/* Liveness/set.scm 177 */
							obj_t BgL_arg1559z00_1516;

							BgL_arg1559z00_1516 = CAR(BgL_l1296z00_1513);
							{
								BgL_localzf2livenesszf2_bglt BgL_auxz00_2251;

								{
									obj_t BgL_auxz00_2252;

									{	/* Liveness/set.scm 56 */
										BgL_objectz00_bglt BgL_tmpz00_2253;

										BgL_tmpz00_2253 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_arg1559z00_1516));
										BgL_auxz00_2252 = BGL_OBJECT_WIDENING(BgL_tmpz00_2253);
									}
									BgL_auxz00_2251 =
										((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2252);
								}
								((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2251))->
										BgL_z52countz52) = ((int) (int) (0L)), BUNSPEC);
						}}
						{
							obj_t BgL_l1296z00_2260;

							BgL_l1296z00_2260 = CDR(BgL_l1296z00_1513);
							BgL_l1296z00_1513 = BgL_l1296z00_2260;
							goto BgL_zc3z04anonymousza31555ze3z87_1514;
						}
					}
				else
					{	/* Liveness/set.scm 177 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1298z00_1520;

				BgL_l1298z00_1520 = BgL_l2z00_17;
			BgL_zc3z04anonymousza31562ze3z87_1521:
				if (PAIRP(BgL_l1298z00_1520))
					{	/* Liveness/set.scm 178 */
						{	/* Liveness/set.scm 178 */
							obj_t BgL_arg1564z00_1523;

							BgL_arg1564z00_1523 = CAR(BgL_l1298z00_1520);
							{
								BgL_localzf2livenesszf2_bglt BgL_auxz00_2265;

								{
									obj_t BgL_auxz00_2266;

									{	/* Liveness/set.scm 56 */
										BgL_objectz00_bglt BgL_tmpz00_2267;

										BgL_tmpz00_2267 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_arg1564z00_1523));
										BgL_auxz00_2266 = BGL_OBJECT_WIDENING(BgL_tmpz00_2267);
									}
									BgL_auxz00_2265 =
										((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2266);
								}
								((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2265))->
										BgL_z52countz52) = ((int) (int) (0L)), BUNSPEC);
						}}
						{
							obj_t BgL_l1298z00_2274;

							BgL_l1298z00_2274 = CDR(BgL_l1298z00_1520);
							BgL_l1298z00_1520 = BgL_l1298z00_2274;
							goto BgL_zc3z04anonymousza31562ze3z87_1521;
						}
					}
				else
					{	/* Liveness/set.scm 178 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1300z00_1527;

				BgL_l1300z00_1527 = BgL_l1z00_16;
			BgL_zc3z04anonymousza31566ze3z87_1528:
				if (PAIRP(BgL_l1300z00_1527))
					{	/* Liveness/set.scm 179 */
						BGl_variablezd2markz12ze70z27zzliveness_setz00(CAR
							(BgL_l1300z00_1527));
						{
							obj_t BgL_l1300z00_2280;

							BgL_l1300z00_2280 = CDR(BgL_l1300z00_1527);
							BgL_l1300z00_1527 = BgL_l1300z00_2280;
							goto BgL_zc3z04anonymousza31566ze3z87_1528;
						}
					}
				else
					{	/* Liveness/set.scm 179 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1302z00_1534;

				BgL_l1302z00_1534 = BgL_l2z00_17;
			BgL_zc3z04anonymousza31574ze3z87_1535:
				if (PAIRP(BgL_l1302z00_1534))
					{	/* Liveness/set.scm 180 */
						BGl_variablezd2markz12ze70z27zzliveness_setz00(CAR
							(BgL_l1302z00_1534));
						{
							obj_t BgL_l1302z00_2286;

							BgL_l1302z00_2286 = CDR(BgL_l1302z00_1534);
							BgL_l1302z00_1534 = BgL_l1302z00_2286;
							goto BgL_zc3z04anonymousza31574ze3z87_1535;
						}
					}
				else
					{	/* Liveness/set.scm 180 */
						((bool_t) 1);
					}
			}
			{	/* Liveness/set.scm 182 */
				obj_t BgL_hook1308z00_1540;

				BgL_hook1308z00_1540 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
				{
					obj_t BgL_l1305z00_1542;
					obj_t BgL_h1306z00_1543;

					BgL_l1305z00_1542 = BgL_l1z00_16;
					BgL_h1306z00_1543 = BgL_hook1308z00_1540;
				BgL_zc3z04anonymousza31585ze3z87_1544:
					if (NULLP(BgL_l1305z00_1542))
						{	/* Liveness/set.scm 182 */
							return CDR(BgL_hook1308z00_1540);
						}
					else
						{	/* Liveness/set.scm 182 */
							bool_t BgL_test1886z00_2292;

							{	/* Liveness/set.scm 183 */
								BgL_localz00_bglt BgL_i1149z00_1555;

								BgL_i1149z00_1555 =
									((BgL_localz00_bglt) CAR(((obj_t) BgL_l1305z00_1542)));
								{	/* Liveness/set.scm 184 */
									int BgL_arg1602z00_1556;

									{
										BgL_localzf2livenesszf2_bglt BgL_auxz00_2296;

										{
											obj_t BgL_auxz00_2297;

											{	/* Liveness/set.scm 184 */
												BgL_objectz00_bglt BgL_tmpz00_2298;

												BgL_tmpz00_2298 =
													((BgL_objectz00_bglt) BgL_i1149z00_1555);
												BgL_auxz00_2297 = BGL_OBJECT_WIDENING(BgL_tmpz00_2298);
											}
											BgL_auxz00_2296 =
												((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2297);
										}
										BgL_arg1602z00_1556 =
											(((BgL_localzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_2296))->BgL_z52countz52);
									}
									BgL_test1886z00_2292 = ((long) (BgL_arg1602z00_1556) == 2L);
							}}
							if (BgL_test1886z00_2292)
								{	/* Liveness/set.scm 182 */
									obj_t BgL_nh1307z00_1550;

									{	/* Liveness/set.scm 182 */
										obj_t BgL_arg1594z00_1552;

										BgL_arg1594z00_1552 = CAR(((obj_t) BgL_l1305z00_1542));
										BgL_nh1307z00_1550 =
											MAKE_YOUNG_PAIR(BgL_arg1594z00_1552, BNIL);
									}
									SET_CDR(BgL_h1306z00_1543, BgL_nh1307z00_1550);
									{	/* Liveness/set.scm 182 */
										obj_t BgL_arg1593z00_1551;

										BgL_arg1593z00_1551 = CDR(((obj_t) BgL_l1305z00_1542));
										{
											obj_t BgL_h1306z00_2312;
											obj_t BgL_l1305z00_2311;

											BgL_l1305z00_2311 = BgL_arg1593z00_1551;
											BgL_h1306z00_2312 = BgL_nh1307z00_1550;
											BgL_h1306z00_1543 = BgL_h1306z00_2312;
											BgL_l1305z00_1542 = BgL_l1305z00_2311;
											goto BgL_zc3z04anonymousza31585ze3z87_1544;
										}
									}
								}
							else
								{	/* Liveness/set.scm 182 */
									obj_t BgL_arg1595z00_1553;

									BgL_arg1595z00_1553 = CDR(((obj_t) BgL_l1305z00_1542));
									{
										obj_t BgL_l1305z00_2315;

										BgL_l1305z00_2315 = BgL_arg1595z00_1553;
										BgL_l1305z00_1542 = BgL_l1305z00_2315;
										goto BgL_zc3z04anonymousza31585ze3z87_1544;
									}
								}
						}
				}
			}
		}

	}



/* variable-mark!~0 */
	obj_t BGl_variablezd2markz12ze70z27zzliveness_setz00(obj_t BgL_vz00_1558)
	{
		{	/* Liveness/set.scm 175 */
			{
				int BgL_auxz00_2323;
				BgL_localzf2livenesszf2_bglt BgL_auxz00_2316;

				{	/* Liveness/set.scm 175 */
					int BgL_arg1605z00_1561;

					{
						BgL_localzf2livenesszf2_bglt BgL_auxz00_2324;

						{
							obj_t BgL_auxz00_2325;

							{	/* Liveness/set.scm 175 */
								BgL_objectz00_bglt BgL_tmpz00_2326;

								BgL_tmpz00_2326 =
									((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_1558));
								BgL_auxz00_2325 = BGL_OBJECT_WIDENING(BgL_tmpz00_2326);
							}
							BgL_auxz00_2324 =
								((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2325);
						}
						BgL_arg1605z00_1561 =
							(((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2324))->
							BgL_z52countz52);
					}
					BgL_auxz00_2323 = (int) ((1L + (long) (BgL_arg1605z00_1561)));
				}
				{
					obj_t BgL_auxz00_2317;

					{	/* Liveness/set.scm 175 */
						BgL_objectz00_bglt BgL_tmpz00_2318;

						BgL_tmpz00_2318 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_1558));
						BgL_auxz00_2317 = BGL_OBJECT_WIDENING(BgL_tmpz00_2318);
					}
					BgL_auxz00_2316 = ((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2317);
				}
				return
					((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_2316))->
						BgL_z52countz52) = ((int) BgL_auxz00_2323), BUNSPEC);
		}}

	}



/* &intersection */
	obj_t BGl_z62intersectionz62zzliveness_setz00(obj_t BgL_envz00_1987,
		obj_t BgL_l1z00_1988, obj_t BgL_l2z00_1989)
	{
		{	/* Liveness/set.scm 171 */
			return
				BGl_intersectionz00zzliveness_setz00(BgL_l1z00_1988, BgL_l2z00_1989);
		}

	}



/* intersection* */
	BGL_EXPORTED_DEF obj_t BGl_intersectionza2za2zzliveness_setz00(obj_t
		BgL_lsz00_18)
	{
		{	/* Liveness/set.scm 192 */
			{
				obj_t BgL_lz00_1611;
				obj_t BgL_vz00_1607;

				{
					obj_t BgL_l1313z00_1566;

					BgL_l1313z00_1566 = BgL_lsz00_18;
				BgL_zc3z04anonymousza31606ze3z87_1567:
					if (PAIRP(BgL_l1313z00_1566))
						{	/* Liveness/set.scm 201 */
							{	/* Liveness/set.scm 201 */
								obj_t BgL_lz00_1569;

								BgL_lz00_1569 = CAR(BgL_l1313z00_1566);
								{
									obj_t BgL_l1311z00_1571;

									BgL_l1311z00_1571 = BgL_lz00_1569;
								BgL_zc3z04anonymousza31608ze3z87_1572:
									if (PAIRP(BgL_l1311z00_1571))
										{	/* Liveness/set.scm 201 */
											{	/* Liveness/set.scm 201 */
												obj_t BgL_arg1611z00_1574;

												BgL_arg1611z00_1574 = CAR(BgL_l1311z00_1571);
												{
													BgL_localzf2livenesszf2_bglt BgL_auxz00_2343;

													{
														obj_t BgL_auxz00_2344;

														{	/* Liveness/set.scm 56 */
															BgL_objectz00_bglt BgL_tmpz00_2345;

															BgL_tmpz00_2345 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_arg1611z00_1574));
															BgL_auxz00_2344 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2345);
														}
														BgL_auxz00_2343 =
															((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2344);
													}
													((((BgL_localzf2livenesszf2_bglt)
																COBJECT(BgL_auxz00_2343))->BgL_z52countz52) =
														((int) (int) (0L)), BUNSPEC);
											}}
											{
												obj_t BgL_l1311z00_2352;

												BgL_l1311z00_2352 = CDR(BgL_l1311z00_1571);
												BgL_l1311z00_1571 = BgL_l1311z00_2352;
												goto BgL_zc3z04anonymousza31608ze3z87_1572;
											}
										}
									else
										{	/* Liveness/set.scm 201 */
											((bool_t) 1);
										}
								}
							}
							{
								obj_t BgL_l1313z00_2354;

								BgL_l1313z00_2354 = CDR(BgL_l1313z00_1566);
								BgL_l1313z00_1566 = BgL_l1313z00_2354;
								goto BgL_zc3z04anonymousza31606ze3z87_1567;
							}
						}
					else
						{	/* Liveness/set.scm 201 */
							((bool_t) 1);
						}
				}
				{
					obj_t BgL_l1315z00_1580;

					BgL_l1315z00_1580 = BgL_lsz00_18;
				BgL_zc3z04anonymousza31616ze3z87_1581:
					if (PAIRP(BgL_l1315z00_1580))
						{	/* Liveness/set.scm 202 */
							BgL_lz00_1611 = CAR(BgL_l1315z00_1580);
							{
								obj_t BgL_l1309z00_1614;

								BgL_l1309z00_1614 = BgL_lz00_1611;
							BgL_zc3z04anonymousza31663ze3z87_1615:
								if (PAIRP(BgL_l1309z00_1614))
									{	/* Liveness/set.scm 199 */
										BgL_vz00_1607 = CAR(BgL_l1309z00_1614);
										{
											int BgL_auxz00_2367;
											BgL_localzf2livenesszf2_bglt BgL_auxz00_2360;

											{	/* Liveness/set.scm 196 */
												int BgL_arg1661z00_1610;

												{
													BgL_localzf2livenesszf2_bglt BgL_auxz00_2368;

													{
														obj_t BgL_auxz00_2369;

														{	/* Liveness/set.scm 196 */
															BgL_objectz00_bglt BgL_tmpz00_2370;

															BgL_tmpz00_2370 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_vz00_1607));
															BgL_auxz00_2369 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2370);
														}
														BgL_auxz00_2368 =
															((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2369);
													}
													BgL_arg1661z00_1610 =
														(((BgL_localzf2livenesszf2_bglt)
															COBJECT(BgL_auxz00_2368))->BgL_z52countz52);
												}
												BgL_auxz00_2367 =
													(int) ((1L + (long) (BgL_arg1661z00_1610)));
											}
											{
												obj_t BgL_auxz00_2361;

												{	/* Liveness/set.scm 196 */
													BgL_objectz00_bglt BgL_tmpz00_2362;

													BgL_tmpz00_2362 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_vz00_1607));
													BgL_auxz00_2361 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2362);
												}
												BgL_auxz00_2360 =
													((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2361);
											}
											((((BgL_localzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_2360))->BgL_z52countz52) =
												((int) BgL_auxz00_2367), BUNSPEC);
										}
										{
											obj_t BgL_l1309z00_2381;

											BgL_l1309z00_2381 = CDR(BgL_l1309z00_1614);
											BgL_l1309z00_1614 = BgL_l1309z00_2381;
											goto BgL_zc3z04anonymousza31663ze3z87_1615;
										}
									}
								else
									{	/* Liveness/set.scm 199 */
										((bool_t) 1);
									}
							}
							{
								obj_t BgL_l1315z00_2384;

								BgL_l1315z00_2384 = CDR(BgL_l1315z00_1580);
								BgL_l1315z00_1580 = BgL_l1315z00_2384;
								goto BgL_zc3z04anonymousza31616ze3z87_1581;
							}
						}
					else
						{	/* Liveness/set.scm 202 */
							((bool_t) 1);
						}
				}
				{	/* Liveness/set.scm 204 */
					long BgL_z52countz52_1586;

					BgL_z52countz52_1586 = bgl_list_length(BgL_lsz00_18);
					if ((BgL_z52countz52_1586 >= BGl_za2markza2z00zzliveness_setz00))
						{	/* Liveness/set.scm 205 */
							BGl_za2markza2z00zzliveness_setz00 = (BgL_z52countz52_1586 + 1L);
						}
					else
						{	/* Liveness/set.scm 205 */
							BFALSE;
						}
					{	/* Liveness/set.scm 207 */
						obj_t BgL_hook1321z00_1588;

						BgL_hook1321z00_1588 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{	/* Liveness/set.scm 210 */
							obj_t BgL_g1322z00_1589;

							BgL_g1322z00_1589 = CAR(((obj_t) BgL_lsz00_18));
							{
								obj_t BgL_l1318z00_1591;
								obj_t BgL_h1319z00_1592;

								BgL_l1318z00_1591 = BgL_g1322z00_1589;
								BgL_h1319z00_1592 = BgL_hook1321z00_1588;
							BgL_zc3z04anonymousza31629ze3z87_1593:
								if (NULLP(BgL_l1318z00_1591))
									{	/* Liveness/set.scm 210 */
										return CDR(BgL_hook1321z00_1588);
									}
								else
									{	/* Liveness/set.scm 210 */
										bool_t BgL_test1893z00_2396;

										{	/* Liveness/set.scm 208 */
											BgL_localz00_bglt BgL_i1151z00_1604;

											BgL_i1151z00_1604 =
												((BgL_localz00_bglt) CAR(((obj_t) BgL_l1318z00_1591)));
											{	/* Liveness/set.scm 209 */
												int BgL_arg1654z00_1605;

												{
													BgL_localzf2livenesszf2_bglt BgL_auxz00_2400;

													{
														obj_t BgL_auxz00_2401;

														{	/* Liveness/set.scm 209 */
															BgL_objectz00_bglt BgL_tmpz00_2402;

															BgL_tmpz00_2402 =
																((BgL_objectz00_bglt) BgL_i1151z00_1604);
															BgL_auxz00_2401 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2402);
														}
														BgL_auxz00_2400 =
															((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2401);
													}
													BgL_arg1654z00_1605 =
														(((BgL_localzf2livenesszf2_bglt)
															COBJECT(BgL_auxz00_2400))->BgL_z52countz52);
												}
												BgL_test1893z00_2396 =
													(
													(long) (BgL_arg1654z00_1605) == BgL_z52countz52_1586);
										}}
										if (BgL_test1893z00_2396)
											{	/* Liveness/set.scm 210 */
												obj_t BgL_nh1320z00_1599;

												{	/* Liveness/set.scm 210 */
													obj_t BgL_arg1650z00_1601;

													BgL_arg1650z00_1601 =
														CAR(((obj_t) BgL_l1318z00_1591));
													BgL_nh1320z00_1599 =
														MAKE_YOUNG_PAIR(BgL_arg1650z00_1601, BNIL);
												}
												SET_CDR(BgL_h1319z00_1592, BgL_nh1320z00_1599);
												{	/* Liveness/set.scm 210 */
													obj_t BgL_arg1646z00_1600;

													BgL_arg1646z00_1600 =
														CDR(((obj_t) BgL_l1318z00_1591));
													{
														obj_t BgL_h1319z00_2416;
														obj_t BgL_l1318z00_2415;

														BgL_l1318z00_2415 = BgL_arg1646z00_1600;
														BgL_h1319z00_2416 = BgL_nh1320z00_1599;
														BgL_h1319z00_1592 = BgL_h1319z00_2416;
														BgL_l1318z00_1591 = BgL_l1318z00_2415;
														goto BgL_zc3z04anonymousza31629ze3z87_1593;
													}
												}
											}
										else
											{	/* Liveness/set.scm 210 */
												obj_t BgL_arg1651z00_1602;

												BgL_arg1651z00_1602 = CDR(((obj_t) BgL_l1318z00_1591));
												{
													obj_t BgL_l1318z00_2419;

													BgL_l1318z00_2419 = BgL_arg1651z00_1602;
													BgL_l1318z00_1591 = BgL_l1318z00_2419;
													goto BgL_zc3z04anonymousza31629ze3z87_1593;
												}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &intersection* */
	obj_t BGl_z62intersectionza2zc0zzliveness_setz00(obj_t BgL_envz00_1990,
		obj_t BgL_lsz00_1991)
	{
		{	/* Liveness/set.scm 192 */
			return BGl_intersectionza2za2zzliveness_setz00(BgL_lsz00_1991);
		}

	}



/* disjonction */
	BGL_EXPORTED_DEF obj_t BGl_disjonctionz00zzliveness_setz00(obj_t BgL_l1z00_19,
		obj_t BgL_l2z00_20)
	{
		{	/* Liveness/set.scm 217 */
			{	/* Liveness/set.scm 219 */
				long BgL_markz00_1622;

				BgL_markz00_1622 = BGl_getzd2markzd2zzliveness_setz00();
				if (NULLP(BgL_l2z00_20))
					{	/* Liveness/set.scm 225 */
						return BgL_l1z00_19;
					}
				else
					{	/* Liveness/set.scm 225 */
						{
							obj_t BgL_l1323z00_1626;

							BgL_l1323z00_1626 = BgL_l2z00_20;
						BgL_zc3z04anonymousza31680ze3z87_1627:
							if (PAIRP(BgL_l1323z00_1626))
								{	/* Liveness/set.scm 228 */
									{	/* Liveness/set.scm 228 */
										obj_t BgL_arg1688z00_1629;

										BgL_arg1688z00_1629 = CAR(BgL_l1323z00_1626);
										{
											BgL_localzf2livenesszf2_bglt BgL_auxz00_2427;

											{
												obj_t BgL_auxz00_2428;

												{	/* Liveness/set.scm 223 */
													BgL_objectz00_bglt BgL_tmpz00_2429;

													BgL_tmpz00_2429 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_arg1688z00_1629));
													BgL_auxz00_2428 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2429);
												}
												BgL_auxz00_2427 =
													((BgL_localzf2livenesszf2_bglt) BgL_auxz00_2428);
											}
											((((BgL_localzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_2427))->BgL_z52countz52) =
												((int) (int) (BgL_markz00_1622)), BUNSPEC);
									}}
									{
										obj_t BgL_l1323z00_2436;

										BgL_l1323z00_2436 = CDR(BgL_l1323z00_1626);
										BgL_l1323z00_1626 = BgL_l1323z00_2436;
										goto BgL_zc3z04anonymousza31680ze3z87_1627;
									}
								}
							else
								{	/* Liveness/set.scm 228 */
									((bool_t) 1);
								}
						}
						{	/* Liveness/set.scm 229 */
							obj_t BgL_hook1329z00_1632;

							BgL_hook1329z00_1632 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
							{
								obj_t BgL_l1326z00_1634;
								obj_t BgL_h1327z00_1635;

								BgL_l1326z00_1634 = BgL_l1z00_19;
								BgL_h1327z00_1635 = BgL_hook1329z00_1632;
							BgL_zc3z04anonymousza31690ze3z87_1636:
								if (NULLP(BgL_l1326z00_1634))
									{	/* Liveness/set.scm 229 */
										return CDR(BgL_hook1329z00_1632);
									}
								else
									{	/* Liveness/set.scm 229 */
										bool_t BgL_test1897z00_2442;

										{	/* Liveness/set.scm 230 */
											BgL_localz00_bglt BgL_i1153z00_1649;

											BgL_i1153z00_1649 =
												((BgL_localz00_bglt) CAR(((obj_t) BgL_l1326z00_1634)));
											{	/* Liveness/set.scm 231 */
												bool_t BgL_test1898z00_2446;

												{	/* Liveness/set.scm 231 */
													int BgL_arg1705z00_1651;

													{
														BgL_localzf2livenesszf2_bglt BgL_auxz00_2447;

														{
															obj_t BgL_auxz00_2448;

															{	/* Liveness/set.scm 231 */
																BgL_objectz00_bglt BgL_tmpz00_2449;

																BgL_tmpz00_2449 =
																	((BgL_objectz00_bglt) BgL_i1153z00_1649);
																BgL_auxz00_2448 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2449);
															}
															BgL_auxz00_2447 =
																((BgL_localzf2livenesszf2_bglt)
																BgL_auxz00_2448);
														}
														BgL_arg1705z00_1651 =
															(((BgL_localzf2livenesszf2_bglt)
																COBJECT(BgL_auxz00_2447))->BgL_z52countz52);
													}
													BgL_test1898z00_2446 =
														((long) (BgL_arg1705z00_1651) == BgL_markz00_1622);
												}
												if (BgL_test1898z00_2446)
													{	/* Liveness/set.scm 231 */
														BgL_test1897z00_2442 = ((bool_t) 0);
													}
												else
													{	/* Liveness/set.scm 231 */
														BgL_test1897z00_2442 = ((bool_t) 1);
													}
											}
										}
										if (BgL_test1897z00_2442)
											{	/* Liveness/set.scm 229 */
												obj_t BgL_nh1328z00_1644;

												{	/* Liveness/set.scm 229 */
													obj_t BgL_arg1702z00_1646;

													BgL_arg1702z00_1646 =
														CAR(((obj_t) BgL_l1326z00_1634));
													BgL_nh1328z00_1644 =
														MAKE_YOUNG_PAIR(BgL_arg1702z00_1646, BNIL);
												}
												SET_CDR(BgL_h1327z00_1635, BgL_nh1328z00_1644);
												{	/* Liveness/set.scm 229 */
													obj_t BgL_arg1701z00_1645;

													BgL_arg1701z00_1645 =
														CDR(((obj_t) BgL_l1326z00_1634));
													{
														obj_t BgL_h1327z00_2463;
														obj_t BgL_l1326z00_2462;

														BgL_l1326z00_2462 = BgL_arg1701z00_1645;
														BgL_h1327z00_2463 = BgL_nh1328z00_1644;
														BgL_h1327z00_1635 = BgL_h1327z00_2463;
														BgL_l1326z00_1634 = BgL_l1326z00_2462;
														goto BgL_zc3z04anonymousza31690ze3z87_1636;
													}
												}
											}
										else
											{	/* Liveness/set.scm 229 */
												obj_t BgL_arg1703z00_1647;

												BgL_arg1703z00_1647 = CDR(((obj_t) BgL_l1326z00_1634));
												{
													obj_t BgL_l1326z00_2466;

													BgL_l1326z00_2466 = BgL_arg1703z00_1647;
													BgL_l1326z00_1634 = BgL_l1326z00_2466;
													goto BgL_zc3z04anonymousza31690ze3z87_1636;
												}
											}
									}
							}
						}
					}
			}
		}

	}



/* &disjonction */
	obj_t BGl_z62disjonctionz62zzliveness_setz00(obj_t BgL_envz00_1992,
		obj_t BgL_l1z00_1993, obj_t BgL_l2z00_1994)
	{
		{	/* Liveness/set.scm 217 */
			return
				BGl_disjonctionz00zzliveness_setz00(BgL_l1z00_1993, BgL_l2z00_1994);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzliveness_setz00(void)
	{
		{	/* Liveness/set.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzliveness_setz00(void)
	{
		{	/* Liveness/set.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzliveness_setz00(void)
	{
		{	/* Liveness/set.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzliveness_setz00(void)
	{
		{	/* Liveness/set.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
			return
				BGl_modulezd2initializa7ationz75zzliveness_typesz00(0L,
				BSTRING_TO_STRING(BGl_string1842z00zzliveness_setz00));
		}

	}

#ifdef __cplusplus
}
#endif
