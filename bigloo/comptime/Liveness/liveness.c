/*===========================================================================*/
/*   (Liveness/liveness.scm)                                                 */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Liveness/liveness.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_LIVENESS_LIVENESS_TYPE_DEFINITIONS
#define BGL_LIVENESS_LIVENESS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_wideningz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_wideningz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_localzf2livenesszf2_bgl
	{
		int BgL_z52countz52;
	}                          *BgL_localzf2livenesszf2_bglt;

	typedef struct BgL_literalzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_literalzf2livenesszf2_bglt;

	typedef struct BgL_refzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                        *BgL_refzf2livenesszf2_bglt;

	typedef struct BgL_closurezf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_closurezf2livenesszf2_bglt;

	typedef struct BgL_kwotezf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                          *BgL_kwotezf2livenesszf2_bglt;

	typedef struct BgL_sequencezf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_sequencezf2livenesszf2_bglt;

	typedef struct BgL_appzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                        *BgL_appzf2livenesszf2_bglt;

	typedef struct BgL_appzd2lyzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_appzd2lyzf2livenessz20_bglt;

	typedef struct BgL_funcallzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_funcallzf2livenesszf2_bglt;

	typedef struct BgL_pragmazf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                           *BgL_pragmazf2livenesszf2_bglt;

	typedef struct BgL_getfieldzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_getfieldzf2livenesszf2_bglt;

	typedef struct BgL_setfieldzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_setfieldzf2livenesszf2_bglt;

	typedef struct BgL_wideningzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_wideningzf2livenesszf2_bglt;

	typedef struct BgL_newzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                        *BgL_newzf2livenesszf2_bglt;

	typedef struct BgL_valloczf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                           *BgL_valloczf2livenesszf2_bglt;

	typedef struct BgL_vrefzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_vrefzf2livenesszf2_bglt;

	typedef struct BgL_vsetz12zf2livenessze0_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_vsetz12zf2livenessze0_bglt;

	typedef struct BgL_vlengthzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                            *BgL_vlengthzf2livenesszf2_bglt;

	typedef struct BgL_instanceofzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                               *BgL_instanceofzf2livenesszf2_bglt;

	typedef struct BgL_castzd2nullzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                *BgL_castzd2nullzf2livenessz20_bglt;

	typedef struct BgL_castzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_castzf2livenesszf2_bglt;

	typedef struct BgL_setqzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_setqzf2livenesszf2_bglt;

	typedef struct BgL_conditionalzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                *BgL_conditionalzf2livenesszf2_bglt;

	typedef struct BgL_failzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_failzf2livenesszf2_bglt;

	typedef struct BgL_switchzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                           *BgL_switchzf2livenesszf2_bglt;

	typedef struct BgL_letzd2funzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                              *BgL_letzd2funzf2livenessz20_bglt;

	typedef struct BgL_letzd2varzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                              *BgL_letzd2varzf2livenessz20_bglt;

	typedef struct BgL_setzd2exzd2itzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                  *BgL_setzd2exzd2itzf2livenesszf2_bglt;

	typedef struct BgL_jumpzd2exzd2itzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                   *BgL_jumpzd2exzd2itzf2livenesszf2_bglt;

	typedef struct BgL_synczf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                         *BgL_synczf2livenesszf2_bglt;

	typedef struct BgL_retblockzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                             *BgL_retblockzf2livenesszf2_bglt;

	typedef struct BgL_returnzf2livenesszf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                           *BgL_returnzf2livenesszf2_bglt;

	typedef struct BgL_makezd2boxzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                               *BgL_makezd2boxzf2livenessz20_bglt;

	typedef struct BgL_boxzd2refzf2livenessz20_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                              *BgL_boxzd2refzf2livenessz20_bglt;

	typedef struct BgL_boxzd2setz12zf2livenessz32_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_usez00;
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                                 *BgL_boxzd2setz12zf2livenessz32_bglt;


#endif													// BGL_LIVENESS_LIVENESS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_funcallzf2livenesszf2zzliveness_typesz00;
	static obj_t BGl_z62defusezd2valloc1945zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutz12zd2sequencezf2live1875z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2literal1839zb0zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_unionza2za2zzliveness_setz00(obj_t);
	static obj_t
		BGl_z62defusezd2instanceofzf2li1979z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2getfieldzf2live1917z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2sequencezf2live1873z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2jumpzd2exzd2itzf2liv2066z42zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_union3z00zzliveness_setz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t
		BGl_z62inoutzd2synczf2liveness2114z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2switchzf2livenes2032z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t
		BGl_z62defusezd2getfieldzf2live1915z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2instanceofzf2li1981z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutz12zd2refzf2liveness1851z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2new1937zb0zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2appzf2liveness1883z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzliveness_livenessz00 =
		BUNSPEC;
	extern obj_t BGl_letzd2funzf2livenessz20zzliveness_typesz00;
	static obj_t
		BGl_z62inoutzd2valloczf2livenes1951z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t
		BGl_z62inoutz12zd2setfieldzf2live1925z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutzd2vrefzf2liveness1959z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_newz00zzast_nodez00;
	static obj_t
		BGl_z62defusezd2setfieldzf2live1923z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	static obj_t
		BGl_z62inoutz12zd2failzf2liveness2022z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62defusezd2vsetz121961za2zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2failzf2liveness2020z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2setzd2exzd2itzf2live2058z42zzliveness_livenessz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2newzf2liveness1941z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_refzf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_kwotezf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_disjonctionz00zzliveness_setz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzliveness_livenessz00(void);
	static obj_t
		BGl_z62inoutz12zd2valloczf2livene1949z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2instanceof1977zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2extern1903zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2valloczf2livene1947z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t
		BGl_z62inoutzd2instanceofzf2liv1983z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2vlengthzf2livene1975z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_literalzf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_conditionalzf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_wideningzf2livenesszf2zzliveness_typesz00;
	static obj_t BGl_z62defusezd2retblock2068zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutzd2boxzd2refzf2livene2098z90zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzliveness_livenessz00(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	extern obj_t BGl_addz00zzliveness_setz00(BgL_localz00_bglt, obj_t);
	static obj_t
		BGl_z62inoutzd2setfieldzf2liven1927z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2setzd2exzd2it2051zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzliveness_livenessz00(void);
	static obj_t BGl_z62defusezd2appzd2ly1887z62zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_intersectionza2za2zzliveness_setz00(obj_t);
	extern obj_t BGl_instanceofzf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_boxzd2refzf2livenessz20zzliveness_typesz00;
	extern obj_t BGl_castz00zzast_nodez00;
	static obj_t BGl_z62livenesszd2livezb0zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static bool_t BGl_inoutza2z12zb0zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2funcallzf2livene1901z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2setq2001zb0zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_inoutz12z12zzliveness_livenessz00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_z62defusezd2vref1953zb0zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2widening1929zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2closure1855zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutz12zd2setqzf2liveness2005z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutz12zd2wideningzf2live1933z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2jumpzd2exzd2it2060zb0zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2setqzf2liveness2003z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2funcall1895zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2appzd2lyzf2livene1889z90zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2conditional2009zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2wideningzf2live1931z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2letzd2fun2034z62zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_intersectionz00zzliveness_setz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2appzd2lyzf2livene1891z82zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	static obj_t
		BGl_z62inoutzd2conditionalzf2li2016z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2pragmazf2livenes1911z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2letzd2funzf2livene2040z90zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2fail2018zb0zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzliveness_livenessz00(void);
	static obj_t
		BGl_z62inoutz12zd2retblockzf2live2072z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2retblockzf2live2070z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t
		BGl_z62inoutz12zd2funcallzf2liven1899z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2sync2108zb0zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2sequence1871zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2funcallzf2liven1897z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2boxzd2setz12zf2liven2106z82zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_localzf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_letzd2varzf2livenessz20zzliveness_typesz00;
	static obj_t
		BGl_z62inoutz12zd2synczf2liveness2112z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutzd2sequencezf2liven1877z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2vsetz12zf2livenes1965z42zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2synczf2liveness2110z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2vsetz12zf2livenes1963z50zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_vsetz12zf2livenessze0zzliveness_typesz00;
	static obj_t
		BGl_z62inoutzd2kwotezf2liveness1869z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2vlengthzf2liven1973z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutz12zd2vrefzf2liveness1957z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2vlengthzf2liven1971z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2vrefzf2liveness1955z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_vlengthzf2livenesszf2zzliveness_typesz00;
	static obj_t BGl_z62defusezd2boxzd2setz122100z70zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	static obj_t BGl_z62defusezd2app1879zb0zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2pragma1905zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutz12zd2letzd2funzf2liven2038z82zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_appzf2livenesszf2zzliveness_typesz00;
	static obj_t BGl_defusez00zzliveness_livenessz00(BgL_nodez00_bglt);
	static obj_t
		BGl_z62inoutzd2castzf2liveness1999z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2letzd2funzf2liven2036z90zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_newzf2livenesszf2zzliveness_typesz00;
	static obj_t
		BGl_z62inoutz12zd2makezd2boxzf2live2088z82zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_unionz00zzliveness_setz00(obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2makezd2boxzf2live2086z90zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	extern obj_t BGl_switchzf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_retblockzf2livenesszf2zzliveness_typesz00;
	static obj_t BGl_defusezd2seqzd2zzliveness_livenessz00(obj_t);
	static obj_t
		BGl_z62inoutzd2wideningzf2liven1935z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62inoutz12z70zzliveness_livenessz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzliveness_livenessz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzliveness_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzliveness_typesz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	static obj_t
		BGl_z62defusezd2switchzf2livene2028z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_defuseza2za2zzliveness_livenessz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_livenesszd2livezd2zzliveness_livenessz00(BgL_nodez00_bglt);
	static obj_t BGl_z62defusezd2kwote1863zb0zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzf2livenessz20zzliveness_typesz00;
	static obj_t
		BGl_z62inoutz12zd2switchzf2livene2030z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_inoutz00zzliveness_livenessz00(BgL_nodez00_bglt);
	static obj_t BGl_z62defuse1832z62zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2returnzf2livenes2082z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_wideningz00zzast_nodez00;
	static obj_t BGl_z62livenesszd2sfunz12za2zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2ref1847zb0zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_vrefzf2livenesszf2zzliveness_typesz00;
	static obj_t
		BGl_z62inoutz12zd2literalzf2liven1843z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static obj_t
		BGl_z62inoutz12zd2boxzd2refzf2liven2096z82zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2literalzf2liven1841z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_valloczf2livenesszf2zzliveness_typesz00;
	static obj_t
		BGl_z62defusezd2refzf2liveness1849z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2boxzd2refzf2liven2094z90zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_z62inoutzd2refzf2liveness1853z42zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t
		BGl_z62inoutz12zd2boxzd2setz12zf2live2104z90zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62inoutzd2appzf2liveness1885z42zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutzd2retblockzf2liven2074z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2boxzd2setz12zf2live2102z82zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_returnzf2livenesszf2zzliveness_typesz00;
	static obj_t
		BGl_z62defusezd2appzf2liveness1881z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzliveness_livenessz00(void);
	static obj_t
		BGl_z62inoutzd2failzf2liveness2024z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2pragmazf2livene1909z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2getfield1913zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutz12zd2kwotezf2livenes1867z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2pragmazf2livene1907z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzliveness_livenessz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzliveness_livenessz00(void);
	static obj_t BGl_z62defusezd2makezd2box2084z62zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2kwotezf2livenes1865z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2letzd2var2043z62zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_failzf2livenesszf2zzliveness_typesz00;
	static obj_t
		BGl_z62inoutzd2letzd2varzf2livene2049z90zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static obj_t
		BGl_z62inoutz12zd2jumpzd2exzd2itzf2li2064z50zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2newzf2liveness1939z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62defusezd2switch2026zb0zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_makezd2boxzf2livenessz20zzliveness_typesz00;
	static obj_t
		BGl_z62defusezd2jumpzd2exzd2itzf2li2062z42zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2returnzf2livene2078z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t BGl_z62inoutzd2newzf2liveness1943z42zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t BGl_z62inoutz62zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00;
	static obj_t
		BGl_z62inoutz12zd2returnzf2livene2080z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutzd2closurezf2livene1861z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2setzd2exzd2itzf2liv2055z50zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	static obj_t
		BGl_z62inoutzd2appzd2lyzf2livenes1893z90zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2setzd2exzd2itzf2liv2053z42zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2vlength1969zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutzd2getfieldzf2liven1919z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2castzd2nullzf2live1991z90zzliveness_livenessz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_livenesszd2sfunz12zc0zzliveness_livenessz00(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62inoutz12zd2conditionalzf2l2014z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62defusezd2return2076zb0zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2conditionalzf2l2012z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00;
	static obj_t
		BGl_z62inoutz12zd2castzd2nullzf2liv1989z82zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_castzd2nullzf2livenessz20zzliveness_typesz00;
	static obj_t
		BGl_z62inoutzd2makezd2boxzf2liven2090z90zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	extern obj_t BGl_sequencezf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	static obj_t
		BGl_z62defusezd2castzd2nullzf2liv1987z90zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_setqzf2livenesszf2zzliveness_typesz00;
	static obj_t BGl_z62defusez62zzliveness_livenessz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62inoutz121834z70zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2setfield1921zb0zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_closurezf2livenesszf2zzliveness_typesz00;
	static obj_t
		BGl_z62inoutzd2setqzf2liveness2007z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2closurezf2liven1859z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62inout1836z62zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62defusezd2closurezf2liven1857z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_pragmazf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_synczf2livenesszf2zzliveness_typesz00;
	static obj_t BGl_z62defusezd2castzd2null1985z62zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t BGl_z62defusezd2boxzd2ref2092z62zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inoutz12zd2letzd2varzf2liven2047z82zzliveness_livenessz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62inoutz12zd2castzf2liveness1997z50zzliveness_livenessz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62defusezd2letzd2varzf2liven2045z90zzliveness_livenessz00(obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t
		BGl_z62defusezd2castzf2liveness1995z42zzliveness_livenessz00(obj_t, obj_t);
	extern obj_t BGl_getfieldzf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_castzf2livenesszf2zzliveness_typesz00;
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	extern obj_t BGl_setfieldzf2livenesszf2zzliveness_typesz00;
	static obj_t BGl_z62defusezd2cast1993zb0zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2literalzf2livene1845z42zzliveness_livenessz00(obj_t, obj_t);
	static obj_t
		BGl_z62inoutzd2vsetz12zf2liveness1967z50zzliveness_livenessz00(obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_livenesszd2sfunz12zd2envz12zzliveness_livenessz00,
		BgL_bgl_za762livenessza7d2sf2818z00,
		BGl_z62livenesszd2sfunz12za2zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za770za7za72819za7,
		BGl_z62inoutz12z70zzliveness_livenessz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_livenesszd2livezd2envz00zzliveness_livenessz00,
		BgL_bgl_za762livenessza7d2li2820z00,
		BGl_z62livenesszd2livezb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2700z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2app12821z00,
		BGl_z62defusezd2app1879zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2701z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2appza72822za7,
		BGl_z62defusezd2appzf2liveness1881z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2702z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2ap2823za7,
		BGl_z62inoutz12zd2appzf2liveness1883z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2703z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2appza7f2824za7,
		BGl_z62inoutzd2appzf2liveness1885z42zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2704z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2appza72825za7,
		BGl_z62defusezd2appzd2ly1887z62zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2705z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2appza72826za7,
		BGl_z62defusezd2appzd2lyzf2livene1889z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2706z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2ap2827za7,
		BGl_z62inoutz12zd2appzd2lyzf2livene1891z82zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2707z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2appza7d2828za7,
		BGl_z62inoutzd2appzd2lyzf2livenes1893z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2708z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2func2829z00,
		BGl_z62defusezd2funcall1895zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2709z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2func2830z00,
		BGl_z62defusezd2funcallzf2liven1897z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2710z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2fu2831za7,
		BGl_z62inoutz12zd2funcallzf2liven1899z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2711z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2funca2832z00,
		BGl_z62inoutzd2funcallzf2livene1901z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2712z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2exte2833z00,
		BGl_z62defusezd2extern1903zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2713z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2prag2834z00,
		BGl_z62defusezd2pragma1905zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2714z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2prag2835z00,
		BGl_z62defusezd2pragmazf2livene1907z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2715z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2pr2836za7,
		BGl_z62inoutz12zd2pragmazf2livene1909z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2716z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2pragm2837z00,
		BGl_z62inoutzd2pragmazf2livenes1911z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2717z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2getf2838z00,
		BGl_z62defusezd2getfield1913zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2718z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2getf2839z00,
		BGl_z62defusezd2getfieldzf2live1915z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2719z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2ge2840za7,
		BGl_z62inoutz12zd2getfieldzf2live1917z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2800z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2retur2841z00,
		BGl_z62inoutzd2returnzf2livenes2082z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2801z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2make2842z00,
		BGl_z62defusezd2makezd2box2084z62zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2720z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2getfi2843z00,
		BGl_z62inoutzd2getfieldzf2liven1919z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2802z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2make2844z00,
		BGl_z62defusezd2makezd2boxzf2live2086z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2721z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2setf2845z00,
		BGl_z62defusezd2setfield1921zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2803z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2ma2846za7,
		BGl_z62inoutz12zd2makezd2boxzf2live2088z82zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2722z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2setf2847z00,
		BGl_z62defusezd2setfieldzf2live1923z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2804z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2makeza72848za7,
		BGl_z62inoutzd2makezd2boxzf2liven2090z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2723z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2se2849za7,
		BGl_z62inoutz12zd2setfieldzf2live1925z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2805z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2boxza72850za7,
		BGl_z62defusezd2boxzd2ref2092z62zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2724z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2setfi2851z00,
		BGl_z62inoutzd2setfieldzf2liven1927z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2806z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2boxza72852za7,
		BGl_z62defusezd2boxzd2refzf2liven2094z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2725z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2wide2853z00,
		BGl_z62defusezd2widening1929zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2807z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2bo2854za7,
		BGl_z62inoutz12zd2boxzd2refzf2liven2096z82zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2726z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2wide2855z00,
		BGl_z62defusezd2wideningzf2live1931z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2808z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2boxza7d2856za7,
		BGl_z62inoutzd2boxzd2refzf2livene2098z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2727z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2wi2857za7,
		BGl_z62inoutz12zd2wideningzf2live1933z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2809z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2boxza72858za7,
		BGl_z62defusezd2boxzd2setz122100z70zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2728z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2widen2859z00,
		BGl_z62inoutzd2wideningzf2liven1935z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2729z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2new12860z00,
		BGl_z62defusezd2new1937zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2817z00zzliveness_livenessz00,
		BgL_bgl_string2817za700za7za7l2861za7, "liveness_liveness", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2810z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2boxza72862za7,
		BGl_z62defusezd2boxzd2setz12zf2live2102z82zzliveness_livenessz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2811z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2bo2863za7,
		BGl_z62inoutz12zd2boxzd2setz12zf2live2104z90zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2730z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2newza72864za7,
		BGl_z62defusezd2newzf2liveness1939z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2812z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2boxza7d2865za7,
		BGl_z62inoutzd2boxzd2setz12zf2liven2106z82zzliveness_livenessz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2731z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2ne2866za7,
		BGl_z62inoutz12zd2newzf2liveness1941z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2813z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2sync2867z00,
		BGl_z62defusezd2sync2108zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2732z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2newza7f2868za7,
		BGl_z62inoutzd2newzf2liveness1943z42zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2814z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2sync2869z00,
		BGl_z62defusezd2synczf2liveness2110z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2733z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2vall2870z00,
		BGl_z62defusezd2valloc1945zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2815z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2sy2871za7,
		BGl_z62inoutz12zd2synczf2liveness2112z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2734z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2vall2872z00,
		BGl_z62defusezd2valloczf2livene1947z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2816z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2syncza72873za7,
		BGl_z62inoutzd2synczf2liveness2114z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2735z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2va2874za7,
		BGl_z62inoutz12zd2valloczf2livene1949z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_inoutzd2envzd2zzliveness_livenessz00,
		BgL_bgl_za762inoutza762za7za7liv2875z00,
		BGl_z62inoutz62zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2736z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2vallo2876z00,
		BGl_z62inoutzd2valloczf2livenes1951z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2737z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2vref2877z00,
		BGl_z62defusezd2vref1953zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2738z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2vref2878z00,
		BGl_z62defusezd2vrefzf2liveness1955z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2739z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2vr2879za7,
		BGl_z62inoutz12zd2vrefzf2liveness1957z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2669z00zzliveness_livenessz00,
		BgL_bgl_string2669za700za7za7l2880za7, "in=", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2740z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2vrefza72881za7,
		BGl_z62inoutzd2vrefzf2liveness1959z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2741z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2vset2882z00,
		BGl_z62defusezd2vsetz121961za2zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2742z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2vset2883z00,
		BGl_z62defusezd2vsetz12zf2livenes1963z50zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2743z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2vs2884za7,
		BGl_z62inoutz12zd2vsetz12zf2livenes1965z42zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2744z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2vsetza72885za7,
		BGl_z62inoutzd2vsetz12zf2liveness1967z50zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2745z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2vlen2886z00,
		BGl_z62defusezd2vlength1969zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2670z00zzliveness_livenessz00,
		BgL_bgl_string2670za700za7za7l2887za7, "out=", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2746z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2vlen2888z00,
		BGl_z62defusezd2vlengthzf2liven1971z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2747z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2vl2889za7,
		BGl_z62inoutz12zd2vlengthzf2liven1973z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2672z00zzliveness_livenessz00,
		BgL_bgl_string2672za700za7za7l2890za7, "defuse1832", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2748z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2vleng2891z00,
		BGl_z62inoutzd2vlengthzf2livene1975z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2749z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2inst2892z00,
		BGl_z62defusezd2instanceof1977zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2674z00zzliveness_livenessz00,
		BgL_bgl_string2674za700za7za7l2893za7, "inout!1834", 10);
	      DEFINE_STRING(BGl_string2676z00zzliveness_livenessz00,
		BgL_bgl_string2676za700za7za7l2894za7, "inout1836", 9);
	      DEFINE_STRING(BGl_string2678z00zzliveness_livenessz00,
		BgL_bgl_string2678za700za7za7l2895za7, "defuse", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2750z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2inst2896z00,
		BGl_z62defusezd2instanceofzf2li1979z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2751z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2in2897za7,
		BGl_z62inoutz12zd2instanceofzf2li1981z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2752z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2insta2898z00,
		BGl_z62inoutzd2instanceofzf2liv1983z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2671z00zzliveness_livenessz00,
		BgL_bgl_za762defuse1832za7622899z00,
		BGl_z62defuse1832z62zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2753z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2cast2900z00,
		BGl_z62defusezd2castzd2null1985z62zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2754z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2cast2901z00,
		BGl_z62defusezd2castzd2nullzf2liv1987z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2673z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7121834za72902za7,
		BGl_z62inoutz121834z70zzliveness_livenessz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2755z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2ca2903za7,
		BGl_z62inoutz12zd2castzd2nullzf2liv1989z82zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2756z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2castza72904za7,
		BGl_z62inoutzd2castzd2nullzf2live1991z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2681z00zzliveness_livenessz00,
		BgL_bgl_string2681za700za7za7l2905za7, "inout!", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2675z00zzliveness_livenessz00,
		BgL_bgl_za762inout1836za762za72906za7,
		BGl_z62inout1836z62zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2757z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2cast2907z00,
		BGl_z62defusezd2cast1993zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2758z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2cast2908z00,
		BGl_z62defusezd2castzf2liveness1995z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2683z00zzliveness_livenessz00,
		BgL_bgl_string2683za700za7za7l2909za7, "inout", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2677z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2lite2910z00,
		BGl_z62defusezd2literal1839zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2759z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2ca2911za7,
		BGl_z62inoutz12zd2castzf2liveness1997z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2679z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2lite2912z00,
		BGl_z62defusezd2literalzf2liven1841z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2760z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2castza72913za7,
		BGl_z62inoutzd2castzf2liveness1999z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2761z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2setq2914z00,
		BGl_z62defusezd2setq2001zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2680z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2li2915za7,
		BGl_z62inoutz12zd2literalzf2liven1843z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2762z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2setq2916z00,
		BGl_z62defusezd2setqzf2liveness2003z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2763z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2se2917za7,
		BGl_z62inoutz12zd2setqzf2liveness2005z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2682z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2liter2918z00,
		BGl_z62inoutzd2literalzf2livene1845z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2764z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2setqza72919za7,
		BGl_z62inoutzd2setqzf2liveness2007z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2765z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2cond2920z00,
		BGl_z62defusezd2conditional2009zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2684z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2ref12921z00,
		BGl_z62defusezd2ref1847zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2766z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2cond2922z00,
		BGl_z62defusezd2conditionalzf2l2012z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2685z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2refza72923za7,
		BGl_z62defusezd2refzf2liveness1849z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2767z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2co2924za7,
		BGl_z62inoutz12zd2conditionalzf2l2014z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2686z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2re2925za7,
		BGl_z62inoutz12zd2refzf2liveness1851z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2768z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2condi2926z00,
		BGl_z62inoutzd2conditionalzf2li2016z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2687z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2refza7f2927za7,
		BGl_z62inoutzd2refzf2liveness1853z42zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2769z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2fail2928z00,
		BGl_z62defusezd2fail2018zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2688z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2clos2929z00,
		BGl_z62defusezd2closure1855zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2689z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2clos2930z00,
		BGl_z62defusezd2closurezf2liven1857z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2770z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2fail2931z00,
		BGl_z62defusezd2failzf2liveness2020z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2771z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2fa2932za7,
		BGl_z62inoutz12zd2failzf2liveness2022z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2690z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2cl2933za7,
		BGl_z62inoutz12zd2closurezf2liven1859z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2772z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2failza72934za7,
		BGl_z62inoutzd2failzf2liveness2024z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2691z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2closu2935z00,
		BGl_z62inoutzd2closurezf2livene1861z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2773z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2swit2936z00,
		BGl_z62defusezd2switch2026zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2692z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2kwot2937z00,
		BGl_z62defusezd2kwote1863zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2774z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2swit2938z00,
		BGl_z62defusezd2switchzf2livene2028z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2693z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2kwot2939z00,
		BGl_z62defusezd2kwotezf2livenes1865z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2775z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2sw2940za7,
		BGl_z62inoutz12zd2switchzf2livene2030z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2694z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2kw2941za7,
		BGl_z62inoutz12zd2kwotezf2livenes1867z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2776z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2switc2942z00,
		BGl_z62inoutzd2switchzf2livenes2032z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2695z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2kwote2943z00,
		BGl_z62inoutzd2kwotezf2liveness1869z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2777z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2letza72944za7,
		BGl_z62defusezd2letzd2fun2034z62zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2696z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2sequ2945z00,
		BGl_z62defusezd2sequence1871zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2778z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2letza72946za7,
		BGl_z62defusezd2letzd2funzf2liven2036z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2697z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2sequ2947z00,
		BGl_z62defusezd2sequencezf2live1873z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2779z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2le2948za7,
		BGl_z62inoutz12zd2letzd2funzf2liven2038z82zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2698z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2se2949za7,
		BGl_z62inoutz12zd2sequencezf2live1875z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2699z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2seque2950z00,
		BGl_z62inoutzd2sequencezf2liven1877z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2780z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2letza7d2951za7,
		BGl_z62inoutzd2letzd2funzf2livene2040z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2781z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2letza72952za7,
		BGl_z62defusezd2letzd2var2043z62zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2782z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2letza72953za7,
		BGl_z62defusezd2letzd2varzf2liven2045z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2783z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2le2954za7,
		BGl_z62inoutz12zd2letzd2varzf2liven2047z82zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2784z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2letza7d2955za7,
		BGl_z62inoutzd2letzd2varzf2livene2049z90zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2785z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2setza72956za7,
		BGl_z62defusezd2setzd2exzd2it2051zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2786z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2setza72957za7,
		BGl_z62defusezd2setzd2exzd2itzf2liv2053z42zzliveness_livenessz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2787z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2se2958za7,
		BGl_z62inoutz12zd2setzd2exzd2itzf2liv2055z50zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2788z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2setza7d2959za7,
		BGl_z62inoutzd2setzd2exzd2itzf2live2058z42zzliveness_livenessz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2789z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2jump2960z00,
		BGl_z62defusezd2jumpzd2exzd2it2060zb0zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2790z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2jump2961z00,
		BGl_z62defusezd2jumpzd2exzd2itzf2li2062z42zzliveness_livenessz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2791z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2ju2962za7,
		BGl_z62inoutz12zd2jumpzd2exzd2itzf2li2064z50zzliveness_livenessz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2792z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2jumpza72963za7,
		BGl_z62inoutzd2jumpzd2exzd2itzf2liv2066z42zzliveness_livenessz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2793z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2retb2964z00,
		BGl_z62defusezd2retblock2068zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2794z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2retb2965z00,
		BGl_z62defusezd2retblockzf2live2070z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2795z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2re2966za7,
		BGl_z62inoutz12zd2retblockzf2live2072z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2796z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza7d2retbl2967z00,
		BGl_z62inoutzd2retblockzf2liven2074z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2797z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2retu2968z00,
		BGl_z62defusezd2return2076zb0zzliveness_livenessz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2798z00zzliveness_livenessz00,
		BgL_bgl_za762defuseza7d2retu2969z00,
		BGl_z62defusezd2returnzf2livene2078z42zzliveness_livenessz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2799z00zzliveness_livenessz00,
		BgL_bgl_za762inoutza712za7d2re2970za7,
		BGl_z62inoutz12zd2returnzf2livene2080z50zzliveness_livenessz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_defusezd2envzd2zzliveness_livenessz00,
		BgL_bgl_za762defuseza762za7za7li2971z00,
		BGl_z62defusez62zzliveness_livenessz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzliveness_livenessz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzliveness_livenessz00(long
		BgL_checksumz00_5810, char *BgL_fromz00_5811)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzliveness_livenessz00))
				{
					BGl_requirezd2initializa7ationz75zzliveness_livenessz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzliveness_livenessz00();
					BGl_libraryzd2moduleszd2initz00zzliveness_livenessz00();
					BGl_importedzd2moduleszd2initz00zzliveness_livenessz00();
					BGl_genericzd2initzd2zzliveness_livenessz00();
					BGl_methodzd2initzd2zzliveness_livenessz00();
					return BGl_toplevelzd2initzd2zzliveness_livenessz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzliveness_livenessz00(void)
	{
		{	/* Liveness/liveness.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "liveness_liveness");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"liveness_liveness");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"liveness_liveness");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"liveness_liveness");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "liveness_liveness");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"liveness_liveness");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"liveness_liveness");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "liveness_liveness");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzliveness_livenessz00(void)
	{
		{	/* Liveness/liveness.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzliveness_livenessz00(void)
	{
		{	/* Liveness/liveness.scm 15 */
			return BUNSPEC;
		}

	}



/* liveness-sfun! */
	BGL_EXPORTED_DEF obj_t
		BGl_livenesszd2sfunz12zc0zzliveness_livenessz00(BgL_sfunz00_bglt
		BgL_valuez00_3)
	{
		{	/* Liveness/liveness.scm 43 */
			{	/* Liveness/liveness.scm 48 */
				obj_t BgL_g1554z00_1547;

				BgL_g1554z00_1547 =
					(((BgL_sfunz00_bglt) COBJECT(BgL_valuez00_3))->BgL_argsz00);
				{
					obj_t BgL_l1552z00_1549;

					BgL_l1552z00_1549 = BgL_g1554z00_1547;
				BgL_zc3z04anonymousza32118ze3z87_1550:
					if (PAIRP(BgL_l1552z00_1549))
						{	/* Liveness/liveness.scm 48 */
							{	/* Liveness/liveness.scm 48 */
								obj_t BgL_lz00_1552;

								BgL_lz00_1552 = CAR(BgL_l1552z00_1549);
								{	/* Liveness/liveness.scm 48 */
									BgL_localzf2livenesszf2_bglt BgL_wide1146z00_1555;

									BgL_wide1146z00_1555 =
										((BgL_localzf2livenesszf2_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_localzf2livenesszf2_bgl))));
									{	/* Liveness/liveness.scm 48 */
										obj_t BgL_auxz00_5839;
										BgL_objectz00_bglt BgL_tmpz00_5835;

										BgL_auxz00_5839 = ((obj_t) BgL_wide1146z00_1555);
										BgL_tmpz00_5835 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_lz00_1552)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5835, BgL_auxz00_5839);
									}
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_lz00_1552)));
									{	/* Liveness/liveness.scm 48 */
										long BgL_arg2120z00_1556;

										BgL_arg2120z00_1556 =
											BGL_CLASS_NUM(BGl_localzf2livenesszf2zzliveness_typesz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_lz00_1552))),
											BgL_arg2120z00_1556);
									}
									((BgL_localz00_bglt)
										((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_lz00_1552)));
								}
								{
									BgL_localzf2livenesszf2_bglt BgL_auxz00_5853;

									{
										obj_t BgL_auxz00_5854;

										{	/* Liveness/liveness.scm 48 */
											BgL_objectz00_bglt BgL_tmpz00_5855;

											BgL_tmpz00_5855 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_lz00_1552)));
											BgL_auxz00_5854 = BGL_OBJECT_WIDENING(BgL_tmpz00_5855);
										}
										BgL_auxz00_5853 =
											((BgL_localzf2livenesszf2_bglt) BgL_auxz00_5854);
									}
									((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_5853))->
											BgL_z52countz52) = ((int) (int) (0L)), BUNSPEC);
								}
								((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_lz00_1552));
							}
							{
								obj_t BgL_l1552z00_5865;

								BgL_l1552z00_5865 = CDR(BgL_l1552z00_1549);
								BgL_l1552z00_1549 = BgL_l1552z00_5865;
								goto BgL_zc3z04anonymousza32118ze3z87_1550;
							}
						}
					else
						{	/* Liveness/liveness.scm 48 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 50 */
				obj_t BgL_arg2122z00_1560;

				BgL_arg2122z00_1560 =
					(((BgL_sfunz00_bglt) COBJECT(BgL_valuez00_3))->BgL_bodyz00);
				BGl_defusez00zzliveness_livenessz00(
					((BgL_nodez00_bglt) BgL_arg2122z00_1560));
			}
			{	/* Liveness/liveness.scm 52 */
				obj_t BgL_arg2123z00_1561;

				BgL_arg2123z00_1561 =
					(((BgL_sfunz00_bglt) COBJECT(BgL_valuez00_3))->BgL_bodyz00);
				return
					BGl_inoutz12z12zzliveness_livenessz00(
					((BgL_nodez00_bglt) BgL_arg2123z00_1561), BNIL);
			}
		}

	}



/* &liveness-sfun! */
	obj_t BGl_z62livenesszd2sfunz12za2zzliveness_livenessz00(obj_t
		BgL_envz00_4411, obj_t BgL_valuez00_4412)
	{
		{	/* Liveness/liveness.scm 43 */
			return
				BGl_livenesszd2sfunz12zc0zzliveness_livenessz00(
				((BgL_sfunz00_bglt) BgL_valuez00_4412));
		}

	}



/* liveness-live */
	BGL_EXPORTED_DEF obj_t
		BGl_livenesszd2livezd2zzliveness_livenessz00(BgL_nodez00_bglt BgL_nodez00_5)
	{
		{	/* Liveness/liveness.scm 66 */
			{	/* Liveness/liveness.scm 67 */
				obj_t BgL_inz00_1562;

				BgL_inz00_1562 = BGl_inoutz00zzliveness_livenessz00(BgL_nodez00_5);
				{	/* Liveness/liveness.scm 68 */
					obj_t BgL_outz00_1563;

					{	/* Liveness/liveness.scm 69 */
						obj_t BgL_tmpz00_3450;

						{	/* Liveness/liveness.scm 69 */
							int BgL_tmpz00_5876;

							BgL_tmpz00_5876 = (int) (1L);
							BgL_tmpz00_3450 = BGL_MVALUES_VAL(BgL_tmpz00_5876);
						}
						{	/* Liveness/liveness.scm 69 */
							int BgL_tmpz00_5879;

							BgL_tmpz00_5879 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5879, BUNSPEC);
						}
						BgL_outz00_1563 = BgL_tmpz00_3450;
					}
					{	/* Liveness/liveness.scm 69 */
						obj_t BgL_arg2124z00_1564;

						BgL_arg2124z00_1564 = BGl_shapez00zztools_shapez00(BgL_inz00_1562);
						{	/* Liveness/liveness.scm 69 */
							obj_t BgL_list2125z00_1565;

							{	/* Liveness/liveness.scm 69 */
								obj_t BgL_arg2126z00_1566;

								BgL_arg2126z00_1566 =
									MAKE_YOUNG_PAIR(BgL_arg2124z00_1564, BNIL);
								BgL_list2125z00_1565 =
									MAKE_YOUNG_PAIR(BGl_string2669z00zzliveness_livenessz00,
									BgL_arg2126z00_1566);
							} ((bool_t) 0);
					}}
					{	/* Liveness/liveness.scm 70 */
						obj_t BgL_arg2127z00_1567;

						BgL_arg2127z00_1567 = BGl_shapez00zztools_shapez00(BgL_outz00_1563);
						{	/* Liveness/liveness.scm 70 */
							obj_t BgL_list2128z00_1568;

							{	/* Liveness/liveness.scm 70 */
								obj_t BgL_arg2129z00_1569;

								BgL_arg2129z00_1569 =
									MAKE_YOUNG_PAIR(BgL_arg2127z00_1567, BNIL);
								BgL_list2128z00_1568 =
									MAKE_YOUNG_PAIR(BGl_string2670z00zzliveness_livenessz00,
									BgL_arg2129z00_1569);
							} ((bool_t) 0);
					}}
					return
						BGl_intersectionz00zzliveness_setz00(BgL_inz00_1562,
						BgL_outz00_1563);
				}
			}
		}

	}



/* &liveness-live */
	obj_t BGl_z62livenesszd2livezb0zzliveness_livenessz00(obj_t BgL_envz00_4413,
		obj_t BgL_nodez00_4414)
	{
		{	/* Liveness/liveness.scm 66 */
			return
				BGl_livenesszd2livezd2zzliveness_livenessz00(
				((BgL_nodez00_bglt) BgL_nodez00_4414));
		}

	}



/* defuse* */
	obj_t BGl_defuseza2za2zzliveness_livenessz00(obj_t BgL_nodesz00_6,
		obj_t BgL_defz00_7, obj_t BgL_usez00_8)
	{
		{	/* Liveness/liveness.scm 76 */
			{
				obj_t BgL_nodesz00_1571;
				obj_t BgL_defz00_1572;
				obj_t BgL_usez00_1573;

				BgL_nodesz00_1571 = BgL_nodesz00_6;
				BgL_defz00_1572 = BgL_defz00_7;
				BgL_usez00_1573 = BgL_usez00_8;
			BgL_zc3z04anonymousza32130ze3z87_1574:
				if (NULLP(BgL_nodesz00_1571))
					{	/* Liveness/liveness.scm 80 */
						{	/* Liveness/liveness.scm 81 */
							int BgL_tmpz00_5893;

							BgL_tmpz00_5893 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5893);
						}
						{	/* Liveness/liveness.scm 81 */
							int BgL_tmpz00_5896;

							BgL_tmpz00_5896 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5896, BgL_usez00_1573);
						}
						return BgL_defz00_1572;
					}
				else
					{	/* Liveness/liveness.scm 82 */
						obj_t BgL_dz00_1578;

						{	/* Liveness/liveness.scm 83 */
							obj_t BgL_arg2135z00_1583;

							BgL_arg2135z00_1583 = CAR(((obj_t) BgL_nodesz00_1571));
							BgL_dz00_1578 =
								BGl_defusez00zzliveness_livenessz00(
								((BgL_nodez00_bglt) BgL_arg2135z00_1583));
						}
						{	/* Liveness/liveness.scm 83 */
							obj_t BgL_uz00_1579;

							{	/* Liveness/liveness.scm 84 */
								obj_t BgL_tmpz00_3452;

								{	/* Liveness/liveness.scm 84 */
									int BgL_tmpz00_5903;

									BgL_tmpz00_5903 = (int) (1L);
									BgL_tmpz00_3452 = BGL_MVALUES_VAL(BgL_tmpz00_5903);
								}
								{	/* Liveness/liveness.scm 84 */
									int BgL_tmpz00_5906;

									BgL_tmpz00_5906 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_5906, BUNSPEC);
								}
								BgL_uz00_1579 = BgL_tmpz00_3452;
							}
							{	/* Liveness/liveness.scm 84 */
								obj_t BgL_arg2132z00_1580;
								obj_t BgL_arg2133z00_1581;
								obj_t BgL_arg2134z00_1582;

								BgL_arg2132z00_1580 = CDR(((obj_t) BgL_nodesz00_1571));
								BgL_arg2133z00_1581 =
									BGl_unionz00zzliveness_setz00(BgL_dz00_1578, BgL_defz00_1572);
								BgL_arg2134z00_1582 =
									BGl_unionz00zzliveness_setz00(BgL_uz00_1579, BgL_usez00_1573);
								{
									obj_t BgL_usez00_5915;
									obj_t BgL_defz00_5914;
									obj_t BgL_nodesz00_5913;

									BgL_nodesz00_5913 = BgL_arg2132z00_1580;
									BgL_defz00_5914 = BgL_arg2133z00_1581;
									BgL_usez00_5915 = BgL_arg2134z00_1582;
									BgL_usez00_1573 = BgL_usez00_5915;
									BgL_defz00_1572 = BgL_defz00_5914;
									BgL_nodesz00_1571 = BgL_nodesz00_5913;
									goto BgL_zc3z04anonymousza32130ze3z87_1574;
								}
							}
						}
					}
			}
		}

	}



/* defuse-seq */
	obj_t BGl_defusezd2seqzd2zzliveness_livenessz00(obj_t BgL_nodesz00_9)
	{
		{	/* Liveness/liveness.scm 89 */
			{
				obj_t BgL_nodesz00_1588;
				obj_t BgL_defz00_1589;
				obj_t BgL_usez00_1590;

				BgL_nodesz00_1588 = BgL_nodesz00_9;
				BgL_defz00_1589 = BNIL;
				BgL_usez00_1590 = BNIL;
			BgL_zc3z04anonymousza32136ze3z87_1591:
				if (NULLP(BgL_nodesz00_1588))
					{	/* Liveness/liveness.scm 93 */
						{	/* Liveness/liveness.scm 94 */
							int BgL_tmpz00_5918;

							BgL_tmpz00_5918 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5918);
						}
						{	/* Liveness/liveness.scm 94 */
							int BgL_tmpz00_5921;

							BgL_tmpz00_5921 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5921, BgL_usez00_1590);
						}
						return BgL_defz00_1589;
					}
				else
					{	/* Liveness/liveness.scm 95 */
						obj_t BgL_dz00_1595;

						{	/* Liveness/liveness.scm 96 */
							obj_t BgL_arg2143z00_1601;

							BgL_arg2143z00_1601 = CAR(((obj_t) BgL_nodesz00_1588));
							BgL_dz00_1595 =
								BGl_defusez00zzliveness_livenessz00(
								((BgL_nodez00_bglt) BgL_arg2143z00_1601));
						}
						{	/* Liveness/liveness.scm 96 */
							obj_t BgL_uz00_1596;

							{	/* Liveness/liveness.scm 97 */
								obj_t BgL_tmpz00_3455;

								{	/* Liveness/liveness.scm 97 */
									int BgL_tmpz00_5928;

									BgL_tmpz00_5928 = (int) (1L);
									BgL_tmpz00_3455 = BGL_MVALUES_VAL(BgL_tmpz00_5928);
								}
								{	/* Liveness/liveness.scm 97 */
									int BgL_tmpz00_5931;

									BgL_tmpz00_5931 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_5931, BUNSPEC);
								}
								BgL_uz00_1596 = BgL_tmpz00_3455;
							}
							{	/* Liveness/liveness.scm 97 */
								obj_t BgL_arg2138z00_1597;
								obj_t BgL_arg2139z00_1598;
								obj_t BgL_arg2141z00_1599;

								BgL_arg2138z00_1597 = CDR(((obj_t) BgL_nodesz00_1588));
								BgL_arg2139z00_1598 =
									BGl_unionz00zzliveness_setz00(BgL_defz00_1589, BgL_dz00_1595);
								BgL_arg2141z00_1599 =
									BGl_unionz00zzliveness_setz00(BgL_usez00_1590,
									BGl_disjonctionz00zzliveness_setz00(BgL_uz00_1596,
										BgL_defz00_1589));
								{
									obj_t BgL_usez00_5941;
									obj_t BgL_defz00_5940;
									obj_t BgL_nodesz00_5939;

									BgL_nodesz00_5939 = BgL_arg2138z00_1597;
									BgL_defz00_5940 = BgL_arg2139z00_1598;
									BgL_usez00_5941 = BgL_arg2141z00_1599;
									BgL_usez00_1590 = BgL_usez00_5941;
									BgL_defz00_1589 = BgL_defz00_5940;
									BgL_nodesz00_1588 = BgL_nodesz00_5939;
									goto BgL_zc3z04anonymousza32136ze3z87_1591;
								}
							}
						}
					}
			}
		}

	}



/* inout*! */
	bool_t BGl_inoutza2z12zb0zzliveness_livenessz00(obj_t BgL_nodesz00_10,
		obj_t BgL_outz00_11)
	{
		{	/* Liveness/liveness.scm 104 */
			{	/* Liveness/liveness.scm 105 */
				obj_t BgL_g1150z00_1603;

				BgL_g1150z00_1603 = bgl_reverse(BgL_nodesz00_10);
				{
					obj_t BgL_sedonz00_1605;
					obj_t BgL_outz00_1606;

					BgL_sedonz00_1605 = BgL_g1150z00_1603;
					BgL_outz00_1606 = BgL_outz00_11;
				BgL_zc3z04anonymousza32144ze3z87_1607:
					if (PAIRP(BgL_sedonz00_1605))
						{	/* Liveness/liveness.scm 108 */
							obj_t BgL_ninz00_1609;

							{	/* Liveness/liveness.scm 109 */
								obj_t BgL_arg2147z00_1612;

								BgL_arg2147z00_1612 = CAR(BgL_sedonz00_1605);
								BgL_ninz00_1609 =
									BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_arg2147z00_1612), BgL_outz00_1606);
							}
							{	/* Liveness/liveness.scm 109 */
								obj_t BgL_noutz00_1610;

								{	/* Liveness/liveness.scm 110 */
									obj_t BgL_tmpz00_3458;

									{	/* Liveness/liveness.scm 110 */
										int BgL_tmpz00_5948;

										BgL_tmpz00_5948 = (int) (1L);
										BgL_tmpz00_3458 = BGL_MVALUES_VAL(BgL_tmpz00_5948);
									}
									{	/* Liveness/liveness.scm 110 */
										int BgL_tmpz00_5951;

										BgL_tmpz00_5951 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_5951, BUNSPEC);
									}
									BgL_noutz00_1610 = BgL_tmpz00_3458;
								}
								{
									obj_t BgL_outz00_5956;
									obj_t BgL_sedonz00_5954;

									BgL_sedonz00_5954 = CDR(BgL_sedonz00_1605);
									BgL_outz00_5956 = BgL_ninz00_1609;
									BgL_outz00_1606 = BgL_outz00_5956;
									BgL_sedonz00_1605 = BgL_sedonz00_5954;
									goto BgL_zc3z04anonymousza32144ze3z87_1607;
								}
							}
						}
					else
						{	/* Liveness/liveness.scm 107 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzliveness_livenessz00(void)
	{
		{	/* Liveness/liveness.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzliveness_livenessz00(void)
	{
		{	/* Liveness/liveness.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_proc2671z00zzliveness_livenessz00, BGl_nodez00zzast_nodez00,
				BGl_string2672z00zzliveness_livenessz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_proc2673z00zzliveness_livenessz00, BGl_nodez00zzast_nodez00,
				BGl_string2674z00zzliveness_livenessz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_proc2675z00zzliveness_livenessz00, BGl_nodez00zzast_nodez00,
				BGl_string2676z00zzliveness_livenessz00);
		}

	}



/* &inout1836 */
	obj_t BGl_z62inout1836z62zzliveness_livenessz00(obj_t BgL_envz00_4418,
		obj_t BgL_nz00_4419)
	{
		{	/* Liveness/liveness.scm 124 */
			{	/* Liveness/liveness.scm 125 */
				int BgL_tmpz00_5960;

				BgL_tmpz00_5960 = (int) (2L);
				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5960);
			}
			{	/* Liveness/liveness.scm 125 */
				int BgL_tmpz00_5963;

				BgL_tmpz00_5963 = (int) (1L);
				BGL_MVALUES_VAL_SET(BgL_tmpz00_5963, BNIL);
			}
			return BNIL;
		}

	}



/* &inout!1834 */
	obj_t BGl_z62inoutz121834z70zzliveness_livenessz00(obj_t BgL_envz00_4420,
		obj_t BgL_nz00_4421, obj_t BgL_outz00_4422)
	{
		{	/* Liveness/liveness.scm 121 */
			{	/* Liveness/liveness.scm 122 */
				int BgL_tmpz00_5966;

				BgL_tmpz00_5966 = (int) (2L);
				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5966);
			}
			{	/* Liveness/liveness.scm 122 */
				int BgL_tmpz00_5969;

				BgL_tmpz00_5969 = (int) (1L);
				BGL_MVALUES_VAL_SET(BgL_tmpz00_5969, BgL_outz00_4422);
			}
			return BgL_outz00_4422;
		}

	}



/* &defuse1832 */
	obj_t BGl_z62defuse1832z62zzliveness_livenessz00(obj_t BgL_envz00_4423,
		obj_t BgL_nz00_4424)
	{
		{	/* Liveness/liveness.scm 118 */
			{	/* Liveness/liveness.scm 119 */
				int BgL_tmpz00_5972;

				BgL_tmpz00_5972 = (int) (2L);
				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5972);
			}
			{	/* Liveness/liveness.scm 119 */
				int BgL_tmpz00_5975;

				BgL_tmpz00_5975 = (int) (1L);
				BGL_MVALUES_VAL_SET(BgL_tmpz00_5975, BNIL);
			}
			return BNIL;
		}

	}



/* defuse */
	obj_t BGl_defusez00zzliveness_livenessz00(BgL_nodez00_bglt BgL_nz00_12)
	{
		{	/* Liveness/liveness.scm 118 */
			{	/* Liveness/liveness.scm 118 */
				obj_t BgL_method1833z00_1633;

				{	/* Liveness/liveness.scm 118 */
					obj_t BgL_res2652z00_3492;

					{	/* Liveness/liveness.scm 118 */
						long BgL_objzd2classzd2numz00_3463;

						BgL_objzd2classzd2numz00_3463 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_12));
						{	/* Liveness/liveness.scm 118 */
							obj_t BgL_arg1811z00_3464;

							BgL_arg1811z00_3464 =
								PROCEDURE_REF(BGl_defusezd2envzd2zzliveness_livenessz00,
								(int) (1L));
							{	/* Liveness/liveness.scm 118 */
								int BgL_offsetz00_3467;

								BgL_offsetz00_3467 = (int) (BgL_objzd2classzd2numz00_3463);
								{	/* Liveness/liveness.scm 118 */
									long BgL_offsetz00_3468;

									BgL_offsetz00_3468 =
										((long) (BgL_offsetz00_3467) - OBJECT_TYPE);
									{	/* Liveness/liveness.scm 118 */
										long BgL_modz00_3469;

										BgL_modz00_3469 =
											(BgL_offsetz00_3468 >> (int) ((long) ((int) (4L))));
										{	/* Liveness/liveness.scm 118 */
											long BgL_restz00_3471;

											BgL_restz00_3471 =
												(BgL_offsetz00_3468 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Liveness/liveness.scm 118 */

												{	/* Liveness/liveness.scm 118 */
													obj_t BgL_bucketz00_3473;

													BgL_bucketz00_3473 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3464), BgL_modz00_3469);
													BgL_res2652z00_3492 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3473), BgL_restz00_3471);
					}}}}}}}}
					BgL_method1833z00_1633 = BgL_res2652z00_3492;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1833z00_1633, ((obj_t) BgL_nz00_12));
			}
		}

	}



/* &defuse */
	obj_t BGl_z62defusez62zzliveness_livenessz00(obj_t BgL_envz00_4425,
		obj_t BgL_nz00_4426)
	{
		{	/* Liveness/liveness.scm 118 */
			return
				BGl_defusez00zzliveness_livenessz00(((BgL_nodez00_bglt) BgL_nz00_4426));
		}

	}



/* inout! */
	obj_t BGl_inoutz12z12zzliveness_livenessz00(BgL_nodez00_bglt BgL_nz00_13,
		obj_t BgL_outz00_14)
	{
		{	/* Liveness/liveness.scm 121 */
			{	/* Liveness/liveness.scm 121 */
				obj_t BgL_method1835z00_1634;

				{	/* Liveness/liveness.scm 121 */
					obj_t BgL_res2657z00_3523;

					{	/* Liveness/liveness.scm 121 */
						long BgL_objzd2classzd2numz00_3494;

						BgL_objzd2classzd2numz00_3494 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_13));
						{	/* Liveness/liveness.scm 121 */
							obj_t BgL_arg1811z00_3495;

							BgL_arg1811z00_3495 =
								PROCEDURE_REF(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
								(int) (1L));
							{	/* Liveness/liveness.scm 121 */
								int BgL_offsetz00_3498;

								BgL_offsetz00_3498 = (int) (BgL_objzd2classzd2numz00_3494);
								{	/* Liveness/liveness.scm 121 */
									long BgL_offsetz00_3499;

									BgL_offsetz00_3499 =
										((long) (BgL_offsetz00_3498) - OBJECT_TYPE);
									{	/* Liveness/liveness.scm 121 */
										long BgL_modz00_3500;

										BgL_modz00_3500 =
											(BgL_offsetz00_3499 >> (int) ((long) ((int) (4L))));
										{	/* Liveness/liveness.scm 121 */
											long BgL_restz00_3502;

											BgL_restz00_3502 =
												(BgL_offsetz00_3499 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Liveness/liveness.scm 121 */

												{	/* Liveness/liveness.scm 121 */
													obj_t BgL_bucketz00_3504;

													BgL_bucketz00_3504 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3495), BgL_modz00_3500);
													BgL_res2657z00_3523 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3504), BgL_restz00_3502);
					}}}}}}}}
					BgL_method1835z00_1634 = BgL_res2657z00_3523;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1835z00_1634,
					((obj_t) BgL_nz00_13), BgL_outz00_14);
			}
		}

	}



/* &inout! */
	obj_t BGl_z62inoutz12z70zzliveness_livenessz00(obj_t BgL_envz00_4427,
		obj_t BgL_nz00_4428, obj_t BgL_outz00_4429)
	{
		{	/* Liveness/liveness.scm 121 */
			return
				BGl_inoutz12z12zzliveness_livenessz00(
				((BgL_nodez00_bglt) BgL_nz00_4428), BgL_outz00_4429);
		}

	}



/* inout */
	obj_t BGl_inoutz00zzliveness_livenessz00(BgL_nodez00_bglt BgL_nz00_15)
	{
		{	/* Liveness/liveness.scm 124 */
			{	/* Liveness/liveness.scm 124 */
				obj_t BgL_method1837z00_1635;

				{	/* Liveness/liveness.scm 124 */
					obj_t BgL_res2662z00_3554;

					{	/* Liveness/liveness.scm 124 */
						long BgL_objzd2classzd2numz00_3525;

						BgL_objzd2classzd2numz00_3525 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_15));
						{	/* Liveness/liveness.scm 124 */
							obj_t BgL_arg1811z00_3526;

							BgL_arg1811z00_3526 =
								PROCEDURE_REF(BGl_inoutzd2envzd2zzliveness_livenessz00,
								(int) (1L));
							{	/* Liveness/liveness.scm 124 */
								int BgL_offsetz00_3529;

								BgL_offsetz00_3529 = (int) (BgL_objzd2classzd2numz00_3525);
								{	/* Liveness/liveness.scm 124 */
									long BgL_offsetz00_3530;

									BgL_offsetz00_3530 =
										((long) (BgL_offsetz00_3529) - OBJECT_TYPE);
									{	/* Liveness/liveness.scm 124 */
										long BgL_modz00_3531;

										BgL_modz00_3531 =
											(BgL_offsetz00_3530 >> (int) ((long) ((int) (4L))));
										{	/* Liveness/liveness.scm 124 */
											long BgL_restz00_3533;

											BgL_restz00_3533 =
												(BgL_offsetz00_3530 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Liveness/liveness.scm 124 */

												{	/* Liveness/liveness.scm 124 */
													obj_t BgL_bucketz00_3535;

													BgL_bucketz00_3535 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3526), BgL_modz00_3531);
													BgL_res2662z00_3554 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3535), BgL_restz00_3533);
					}}}}}}}}
					BgL_method1837z00_1635 = BgL_res2662z00_3554;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1837z00_1635, ((obj_t) BgL_nz00_15));
			}
		}

	}



/* &inout */
	obj_t BGl_z62inoutz62zzliveness_livenessz00(obj_t BgL_envz00_4430,
		obj_t BgL_nz00_4431)
	{
		{	/* Liveness/liveness.scm 124 */
			return
				BGl_inoutz00zzliveness_livenessz00(((BgL_nodez00_bglt) BgL_nz00_4431));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzliveness_livenessz00(void)
	{
		{	/* Liveness/liveness.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_literalz00zzast_nodez00,
				BGl_proc2677z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_literalzf2livenesszf2zzliveness_typesz00,
				BGl_proc2679z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_literalzf2livenesszf2zzliveness_typesz00,
				BGl_proc2680z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_literalzf2livenesszf2zzliveness_typesz00,
				BGl_proc2682z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_refz00zzast_nodez00,
				BGl_proc2684z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_refzf2livenesszf2zzliveness_typesz00,
				BGl_proc2685z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_refzf2livenesszf2zzliveness_typesz00,
				BGl_proc2686z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_refzf2livenesszf2zzliveness_typesz00,
				BGl_proc2687z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_closurez00zzast_nodez00,
				BGl_proc2688z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_closurezf2livenesszf2zzliveness_typesz00,
				BGl_proc2689z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_closurezf2livenesszf2zzliveness_typesz00,
				BGl_proc2690z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_closurezf2livenesszf2zzliveness_typesz00,
				BGl_proc2691z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_kwotez00zzast_nodez00,
				BGl_proc2692z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_kwotezf2livenesszf2zzliveness_typesz00,
				BGl_proc2693z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_kwotezf2livenesszf2zzliveness_typesz00,
				BGl_proc2694z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_kwotezf2livenesszf2zzliveness_typesz00,
				BGl_proc2695z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2696z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_sequencezf2livenesszf2zzliveness_typesz00,
				BGl_proc2697z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_sequencezf2livenesszf2zzliveness_typesz00,
				BGl_proc2698z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_sequencezf2livenesszf2zzliveness_typesz00,
				BGl_proc2699z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_appz00zzast_nodez00,
				BGl_proc2700z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_appzf2livenesszf2zzliveness_typesz00,
				BGl_proc2701z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_appzf2livenesszf2zzliveness_typesz00,
				BGl_proc2702z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_appzf2livenesszf2zzliveness_typesz00,
				BGl_proc2703z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2704z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_appzd2lyzf2livenessz20zzliveness_typesz00,
				BGl_proc2705z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_appzd2lyzf2livenessz20zzliveness_typesz00,
				BGl_proc2706z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_appzd2lyzf2livenessz20zzliveness_typesz00,
				BGl_proc2707z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_funcallz00zzast_nodez00,
				BGl_proc2708z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_funcallzf2livenesszf2zzliveness_typesz00,
				BGl_proc2709z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_funcallzf2livenesszf2zzliveness_typesz00,
				BGl_proc2710z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_funcallzf2livenesszf2zzliveness_typesz00,
				BGl_proc2711z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_externz00zzast_nodez00,
				BGl_proc2712z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_pragmaz00zzast_nodez00,
				BGl_proc2713z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_pragmazf2livenesszf2zzliveness_typesz00,
				BGl_proc2714z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_pragmazf2livenesszf2zzliveness_typesz00,
				BGl_proc2715z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_pragmazf2livenesszf2zzliveness_typesz00,
				BGl_proc2716z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_getfieldz00zzast_nodez00, BGl_proc2717z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_getfieldzf2livenesszf2zzliveness_typesz00,
				BGl_proc2718z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_getfieldzf2livenesszf2zzliveness_typesz00,
				BGl_proc2719z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_getfieldzf2livenesszf2zzliveness_typesz00,
				BGl_proc2720z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_setfieldz00zzast_nodez00, BGl_proc2721z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_setfieldzf2livenesszf2zzliveness_typesz00,
				BGl_proc2722z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_setfieldzf2livenesszf2zzliveness_typesz00,
				BGl_proc2723z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_setfieldzf2livenesszf2zzliveness_typesz00,
				BGl_proc2724z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_wideningz00zzast_nodez00, BGl_proc2725z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_wideningzf2livenesszf2zzliveness_typesz00,
				BGl_proc2726z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_wideningzf2livenesszf2zzliveness_typesz00,
				BGl_proc2727z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_wideningzf2livenesszf2zzliveness_typesz00,
				BGl_proc2728z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_newz00zzast_nodez00,
				BGl_proc2729z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_newzf2livenesszf2zzliveness_typesz00,
				BGl_proc2730z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_newzf2livenesszf2zzliveness_typesz00,
				BGl_proc2731z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_newzf2livenesszf2zzliveness_typesz00,
				BGl_proc2732z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_vallocz00zzast_nodez00,
				BGl_proc2733z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_valloczf2livenesszf2zzliveness_typesz00,
				BGl_proc2734z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_valloczf2livenesszf2zzliveness_typesz00,
				BGl_proc2735z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_valloczf2livenesszf2zzliveness_typesz00,
				BGl_proc2736z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_vrefz00zzast_nodez00,
				BGl_proc2737z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_vrefzf2livenesszf2zzliveness_typesz00,
				BGl_proc2738z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_vrefzf2livenesszf2zzliveness_typesz00,
				BGl_proc2739z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_vrefzf2livenesszf2zzliveness_typesz00,
				BGl_proc2740z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_vsetz12z12zzast_nodez00,
				BGl_proc2741z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_vsetz12zf2livenessze0zzliveness_typesz00,
				BGl_proc2742z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_vsetz12zf2livenessze0zzliveness_typesz00,
				BGl_proc2743z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_vsetz12zf2livenessze0zzliveness_typesz00,
				BGl_proc2744z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_vlengthz00zzast_nodez00,
				BGl_proc2745z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_vlengthzf2livenesszf2zzliveness_typesz00,
				BGl_proc2746z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_vlengthzf2livenesszf2zzliveness_typesz00,
				BGl_proc2747z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_vlengthzf2livenesszf2zzliveness_typesz00,
				BGl_proc2748z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_instanceofz00zzast_nodez00, BGl_proc2749z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_instanceofzf2livenesszf2zzliveness_typesz00,
				BGl_proc2750z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_instanceofzf2livenesszf2zzliveness_typesz00,
				BGl_proc2751z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_instanceofzf2livenesszf2zzliveness_typesz00,
				BGl_proc2752z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_castzd2nullzd2zzast_nodez00, BGl_proc2753z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_castzd2nullzf2livenessz20zzliveness_typesz00,
				BGl_proc2754z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_castzd2nullzf2livenessz20zzliveness_typesz00,
				BGl_proc2755z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_castzd2nullzf2livenessz20zzliveness_typesz00,
				BGl_proc2756z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_castz00zzast_nodez00,
				BGl_proc2757z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_castzf2livenesszf2zzliveness_typesz00,
				BGl_proc2758z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_castzf2livenesszf2zzliveness_typesz00,
				BGl_proc2759z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_castzf2livenesszf2zzliveness_typesz00,
				BGl_proc2760z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_setqz00zzast_nodez00,
				BGl_proc2761z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_setqzf2livenesszf2zzliveness_typesz00,
				BGl_proc2762z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_setqzf2livenesszf2zzliveness_typesz00,
				BGl_proc2763z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_setqzf2livenesszf2zzliveness_typesz00,
				BGl_proc2764z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2765z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_conditionalzf2livenesszf2zzliveness_typesz00,
				BGl_proc2766z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_conditionalzf2livenesszf2zzliveness_typesz00,
				BGl_proc2767z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_conditionalzf2livenesszf2zzliveness_typesz00,
				BGl_proc2768z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_failz00zzast_nodez00,
				BGl_proc2769z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_failzf2livenesszf2zzliveness_typesz00,
				BGl_proc2770z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_failzf2livenesszf2zzliveness_typesz00,
				BGl_proc2771z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_failzf2livenesszf2zzliveness_typesz00,
				BGl_proc2772z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_switchz00zzast_nodez00,
				BGl_proc2773z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_switchzf2livenesszf2zzliveness_typesz00,
				BGl_proc2774z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_switchzf2livenesszf2zzliveness_typesz00,
				BGl_proc2775z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_switchzf2livenesszf2zzliveness_typesz00,
				BGl_proc2776z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2777z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_letzd2funzf2livenessz20zzliveness_typesz00,
				BGl_proc2778z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_letzd2funzf2livenessz20zzliveness_typesz00,
				BGl_proc2779z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_letzd2funzf2livenessz20zzliveness_typesz00,
				BGl_proc2780z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2781z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_letzd2varzf2livenessz20zzliveness_typesz00,
				BGl_proc2782z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_letzd2varzf2livenessz20zzliveness_typesz00,
				BGl_proc2783z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_letzd2varzf2livenessz20zzliveness_typesz00,
				BGl_proc2784z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc2785z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00,
				BGl_proc2786z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00,
				BGl_proc2787z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00,
				BGl_proc2788z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc2789z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00,
				BGl_proc2790z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00,
				BGl_proc2791z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00,
				BGl_proc2792z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_retblockz00zzast_nodez00, BGl_proc2793z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_retblockzf2livenesszf2zzliveness_typesz00,
				BGl_proc2794z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_retblockzf2livenesszf2zzliveness_typesz00,
				BGl_proc2795z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_retblockzf2livenesszf2zzliveness_typesz00,
				BGl_proc2796z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_returnz00zzast_nodez00,
				BGl_proc2797z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_returnzf2livenesszf2zzliveness_typesz00,
				BGl_proc2798z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_returnzf2livenesszf2zzliveness_typesz00,
				BGl_proc2799z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_returnzf2livenesszf2zzliveness_typesz00,
				BGl_proc2800z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2801z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_makezd2boxzf2livenessz20zzliveness_typesz00,
				BGl_proc2802z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_makezd2boxzf2livenessz20zzliveness_typesz00,
				BGl_proc2803z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_makezd2boxzf2livenessz20zzliveness_typesz00,
				BGl_proc2804z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2805z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_boxzd2refzf2livenessz20zzliveness_typesz00,
				BGl_proc2806z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_boxzd2refzf2livenessz20zzliveness_typesz00,
				BGl_proc2807z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_boxzd2refzf2livenessz20zzliveness_typesz00,
				BGl_proc2808z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2809z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00,
				BGl_proc2810z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00,
				BGl_proc2811z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00,
				BGl_proc2812z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00, BGl_syncz00zzast_nodez00,
				BGl_proc2813z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_defusezd2envzd2zzliveness_livenessz00,
				BGl_synczf2livenesszf2zzliveness_typesz00,
				BGl_proc2814z00zzliveness_livenessz00,
				BGl_string2678z00zzliveness_livenessz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutz12zd2envzc0zzliveness_livenessz00,
				BGl_synczf2livenesszf2zzliveness_typesz00,
				BGl_proc2815z00zzliveness_livenessz00,
				BGl_string2681z00zzliveness_livenessz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inoutzd2envzd2zzliveness_livenessz00,
				BGl_synczf2livenesszf2zzliveness_typesz00,
				BGl_proc2816z00zzliveness_livenessz00,
				BGl_string2683z00zzliveness_livenessz00);
		}

	}



/* &inout-sync/liveness2114 */
	obj_t BGl_z62inoutzd2synczf2liveness2114z42zzliveness_livenessz00(obj_t
		BgL_envz00_4569, obj_t BgL_nz00_4570)
	{
		{	/* Liveness/liveness.scm 1130 */
			{	/* Liveness/liveness.scm 1132 */
				obj_t BgL_val0_1830z00_4881;
				obj_t BgL_val1_1831z00_4882;

				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_6212;

					{
						obj_t BgL_auxz00_6213;

						{	/* Liveness/liveness.scm 1132 */
							BgL_objectz00_bglt BgL_tmpz00_6214;

							BgL_tmpz00_6214 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4570));
							BgL_auxz00_6213 = BGL_OBJECT_WIDENING(BgL_tmpz00_6214);
						}
						BgL_auxz00_6212 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6213);
					}
					BgL_val0_1830z00_4881 =
						(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6212))->
						BgL_inz00);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_6220;

					{
						obj_t BgL_auxz00_6221;

						{	/* Liveness/liveness.scm 1132 */
							BgL_objectz00_bglt BgL_tmpz00_6222;

							BgL_tmpz00_6222 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4570));
							BgL_auxz00_6221 = BGL_OBJECT_WIDENING(BgL_tmpz00_6222);
						}
						BgL_auxz00_6220 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6221);
					}
					BgL_val1_1831z00_4882 =
						(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6220))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1132 */
					int BgL_tmpz00_6228;

					BgL_tmpz00_6228 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6228);
				}
				{	/* Liveness/liveness.scm 1132 */
					int BgL_tmpz00_6231;

					BgL_tmpz00_6231 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6231, BgL_val1_1831z00_4882);
				}
				return BgL_val0_1830z00_4881;
			}
		}

	}



/* &inout!-sync/liveness2112 */
	obj_t BGl_z62inoutz12zd2synczf2liveness2112z50zzliveness_livenessz00(obj_t
		BgL_envz00_4571, obj_t BgL_nz00_4572, obj_t BgL_oz00_4573)
	{
		{	/* Liveness/liveness.scm 1123 */
			{
				BgL_synczf2livenesszf2_bglt BgL_auxz00_6234;

				{
					obj_t BgL_auxz00_6235;

					{	/* Liveness/liveness.scm 1125 */
						BgL_objectz00_bglt BgL_tmpz00_6236;

						BgL_tmpz00_6236 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4572));
						BgL_auxz00_6235 = BGL_OBJECT_WIDENING(BgL_tmpz00_6236);
					}
					BgL_auxz00_6234 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6235);
				}
				((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6234))->
						BgL_outz00) = ((obj_t) BgL_oz00_4573), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_6249;
				BgL_synczf2livenesszf2_bglt BgL_auxz00_6242;

				{	/* Liveness/liveness.scm 1126 */
					obj_t BgL_arg2567z00_4884;
					obj_t BgL_arg2568z00_4885;

					{
						BgL_synczf2livenesszf2_bglt BgL_auxz00_6250;

						{
							obj_t BgL_auxz00_6251;

							{	/* Liveness/liveness.scm 1126 */
								BgL_objectz00_bglt BgL_tmpz00_6252;

								BgL_tmpz00_6252 =
									((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4572));
								BgL_auxz00_6251 = BGL_OBJECT_WIDENING(BgL_tmpz00_6252);
							}
							BgL_auxz00_6250 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6251);
						}
						BgL_arg2567z00_4884 =
							(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6250))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 1126 */
						obj_t BgL_arg2572z00_4886;
						obj_t BgL_arg2578z00_4887;

						{
							BgL_synczf2livenesszf2_bglt BgL_auxz00_6258;

							{
								obj_t BgL_auxz00_6259;

								{	/* Liveness/liveness.scm 1126 */
									BgL_objectz00_bglt BgL_tmpz00_6260;

									BgL_tmpz00_6260 =
										((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4572));
									BgL_auxz00_6259 = BGL_OBJECT_WIDENING(BgL_tmpz00_6260);
								}
								BgL_auxz00_6258 =
									((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6259);
							}
							BgL_arg2572z00_4886 =
								(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6258))->
								BgL_outz00);
						}
						{
							BgL_synczf2livenesszf2_bglt BgL_auxz00_6266;

							{
								obj_t BgL_auxz00_6267;

								{	/* Liveness/liveness.scm 1126 */
									BgL_objectz00_bglt BgL_tmpz00_6268;

									BgL_tmpz00_6268 =
										((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4572));
									BgL_auxz00_6267 = BGL_OBJECT_WIDENING(BgL_tmpz00_6268);
								}
								BgL_auxz00_6266 =
									((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6267);
							}
							BgL_arg2578z00_4887 =
								(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6266))->
								BgL_defz00);
						}
						BgL_arg2568z00_4885 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2572z00_4886,
							BgL_arg2578z00_4887);
					}
					BgL_auxz00_6249 =
						BGl_unionz00zzliveness_setz00(BgL_arg2567z00_4884,
						BgL_arg2568z00_4885);
				}
				{
					obj_t BgL_auxz00_6243;

					{	/* Liveness/liveness.scm 1126 */
						BgL_objectz00_bglt BgL_tmpz00_6244;

						BgL_tmpz00_6244 =
							((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4572));
						BgL_auxz00_6243 = BGL_OBJECT_WIDENING(BgL_tmpz00_6244);
					}
					BgL_auxz00_6242 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6243);
				}
				((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6242))->BgL_inz00) =
					((obj_t) BgL_auxz00_6249), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 1127 */
				BgL_nodez00_bglt BgL_arg2579z00_4888;

				BgL_arg2579z00_4888 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt)
								((BgL_syncz00_bglt) BgL_nz00_4572))))->BgL_bodyz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2579z00_4888,
					BgL_oz00_4573);
			}
			{	/* Liveness/liveness.scm 1128 */
				obj_t BgL_val0_1828z00_4889;
				obj_t BgL_val1_1829z00_4890;

				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_6281;

					{
						obj_t BgL_auxz00_6282;

						{	/* Liveness/liveness.scm 1128 */
							BgL_objectz00_bglt BgL_tmpz00_6283;

							BgL_tmpz00_6283 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4572));
							BgL_auxz00_6282 = BGL_OBJECT_WIDENING(BgL_tmpz00_6283);
						}
						BgL_auxz00_6281 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6282);
					}
					BgL_val0_1828z00_4889 =
						(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6281))->
						BgL_inz00);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_6289;

					{
						obj_t BgL_auxz00_6290;

						{	/* Liveness/liveness.scm 1128 */
							BgL_objectz00_bglt BgL_tmpz00_6291;

							BgL_tmpz00_6291 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4572));
							BgL_auxz00_6290 = BGL_OBJECT_WIDENING(BgL_tmpz00_6291);
						}
						BgL_auxz00_6289 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6290);
					}
					BgL_val1_1829z00_4890 =
						(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6289))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1128 */
					int BgL_tmpz00_6297;

					BgL_tmpz00_6297 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6297);
				}
				{	/* Liveness/liveness.scm 1128 */
					int BgL_tmpz00_6300;

					BgL_tmpz00_6300 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6300, BgL_val1_1829z00_4890);
				}
				return BgL_val0_1828z00_4889;
			}
		}

	}



/* &defuse-sync/liveness2110 */
	obj_t BGl_z62defusezd2synczf2liveness2110z42zzliveness_livenessz00(obj_t
		BgL_envz00_4574, obj_t BgL_nz00_4575)
	{
		{	/* Liveness/liveness.scm 1119 */
			{	/* Liveness/liveness.scm 1121 */
				obj_t BgL_val0_1826z00_4892;
				obj_t BgL_val1_1827z00_4893;

				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_6303;

					{
						obj_t BgL_auxz00_6304;

						{	/* Liveness/liveness.scm 1121 */
							BgL_objectz00_bglt BgL_tmpz00_6305;

							BgL_tmpz00_6305 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4575));
							BgL_auxz00_6304 = BGL_OBJECT_WIDENING(BgL_tmpz00_6305);
						}
						BgL_auxz00_6303 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6304);
					}
					BgL_val0_1826z00_4892 =
						(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6303))->
						BgL_defz00);
				}
				{
					BgL_synczf2livenesszf2_bglt BgL_auxz00_6311;

					{
						obj_t BgL_auxz00_6312;

						{	/* Liveness/liveness.scm 1121 */
							BgL_objectz00_bglt BgL_tmpz00_6313;

							BgL_tmpz00_6313 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4575));
							BgL_auxz00_6312 = BGL_OBJECT_WIDENING(BgL_tmpz00_6313);
						}
						BgL_auxz00_6311 = ((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6312);
					}
					BgL_val1_1827z00_4893 =
						(((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6311))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 1121 */
					int BgL_tmpz00_6319;

					BgL_tmpz00_6319 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6319);
				}
				{	/* Liveness/liveness.scm 1121 */
					int BgL_tmpz00_6322;

					BgL_tmpz00_6322 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6322, BgL_val1_1827z00_4893);
				}
				return BgL_val0_1826z00_4892;
			}
		}

	}



/* &defuse-sync2108 */
	obj_t BGl_z62defusezd2sync2108zb0zzliveness_livenessz00(obj_t BgL_envz00_4576,
		obj_t BgL_nz00_4577)
	{
		{	/* Liveness/liveness.scm 1110 */
			{	/* Liveness/liveness.scm 1112 */
				obj_t BgL_defz00_4895;

				{	/* Liveness/liveness.scm 1113 */
					obj_t BgL_arg2560z00_4896;

					{	/* Liveness/liveness.scm 1113 */
						BgL_nodez00_bglt BgL_arg2561z00_4897;
						BgL_nodez00_bglt BgL_arg2562z00_4898;
						BgL_nodez00_bglt BgL_arg2563z00_4899;

						BgL_arg2561z00_4897 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_4577)))->BgL_prelockz00);
						BgL_arg2562z00_4898 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_4577)))->BgL_mutexz00);
						BgL_arg2563z00_4899 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_4577)))->BgL_bodyz00);
						{	/* Liveness/liveness.scm 1113 */
							obj_t BgL_list2564z00_4900;

							{	/* Liveness/liveness.scm 1113 */
								obj_t BgL_arg2565z00_4901;

								{	/* Liveness/liveness.scm 1113 */
									obj_t BgL_arg2566z00_4902;

									BgL_arg2566z00_4902 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_arg2563z00_4899), BNIL);
									BgL_arg2565z00_4901 =
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_arg2562z00_4898), BgL_arg2566z00_4902);
								}
								BgL_list2564z00_4900 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg2561z00_4897), BgL_arg2565z00_4901);
							}
							BgL_arg2560z00_4896 = BgL_list2564z00_4900;
						}
					}
					BgL_defz00_4895 =
						BGl_defusezd2seqzd2zzliveness_livenessz00(BgL_arg2560z00_4896);
				}
				{	/* Liveness/liveness.scm 1113 */
					obj_t BgL_usez00_4903;

					{	/* Liveness/liveness.scm 1114 */
						obj_t BgL_tmpz00_4904;

						{	/* Liveness/liveness.scm 1114 */
							int BgL_tmpz00_6338;

							BgL_tmpz00_6338 = (int) (1L);
							BgL_tmpz00_4904 = BGL_MVALUES_VAL(BgL_tmpz00_6338);
						}
						{	/* Liveness/liveness.scm 1114 */
							int BgL_tmpz00_6341;

							BgL_tmpz00_6341 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6341, BUNSPEC);
						}
						BgL_usez00_4903 = BgL_tmpz00_4904;
					}
					{	/* Liveness/liveness.scm 1115 */
						BgL_syncz00_bglt BgL_arg2556z00_4905;

						{	/* Liveness/liveness.scm 1115 */
							BgL_synczf2livenesszf2_bglt BgL_wide1436z00_4906;

							BgL_wide1436z00_4906 =
								((BgL_synczf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_synczf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 1115 */
								obj_t BgL_auxz00_6349;
								BgL_objectz00_bglt BgL_tmpz00_6345;

								BgL_auxz00_6349 = ((obj_t) BgL_wide1436z00_4906);
								BgL_tmpz00_6345 =
									((BgL_objectz00_bglt)
									((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4577)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6345, BgL_auxz00_6349);
							}
							((BgL_objectz00_bglt)
								((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4577)));
							{	/* Liveness/liveness.scm 1115 */
								long BgL_arg2557z00_4907;

								BgL_arg2557z00_4907 =
									BGL_CLASS_NUM(BGl_synczf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_syncz00_bglt)
											((BgL_syncz00_bglt) BgL_nz00_4577))),
									BgL_arg2557z00_4907);
							}
							((BgL_syncz00_bglt)
								((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4577)));
						}
						{
							BgL_synczf2livenesszf2_bglt BgL_auxz00_6363;

							{
								obj_t BgL_auxz00_6364;

								{	/* Liveness/liveness.scm 1116 */
									BgL_objectz00_bglt BgL_tmpz00_6365;

									BgL_tmpz00_6365 =
										((BgL_objectz00_bglt)
										((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4577)));
									BgL_auxz00_6364 = BGL_OBJECT_WIDENING(BgL_tmpz00_6365);
								}
								BgL_auxz00_6363 =
									((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6364);
							}
							((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6363))->
									BgL_defz00) = ((obj_t) BgL_defz00_4895), BUNSPEC);
						}
						{
							BgL_synczf2livenesszf2_bglt BgL_auxz00_6372;

							{
								obj_t BgL_auxz00_6373;

								{	/* Liveness/liveness.scm 1117 */
									BgL_objectz00_bglt BgL_tmpz00_6374;

									BgL_tmpz00_6374 =
										((BgL_objectz00_bglt)
										((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4577)));
									BgL_auxz00_6373 = BGL_OBJECT_WIDENING(BgL_tmpz00_6374);
								}
								BgL_auxz00_6372 =
									((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6373);
							}
							((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6372))->
									BgL_usez00) = ((obj_t) BgL_usez00_4903), BUNSPEC);
						}
						{
							BgL_synczf2livenesszf2_bglt BgL_auxz00_6381;

							{
								obj_t BgL_auxz00_6382;

								{	/* Liveness/liveness.scm 1113 */
									BgL_objectz00_bglt BgL_tmpz00_6383;

									BgL_tmpz00_6383 =
										((BgL_objectz00_bglt)
										((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4577)));
									BgL_auxz00_6382 = BGL_OBJECT_WIDENING(BgL_tmpz00_6383);
								}
								BgL_auxz00_6381 =
									((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6382);
							}
							((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6381))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_synczf2livenesszf2_bglt BgL_auxz00_6390;

							{
								obj_t BgL_auxz00_6391;

								{	/* Liveness/liveness.scm 1113 */
									BgL_objectz00_bglt BgL_tmpz00_6392;

									BgL_tmpz00_6392 =
										((BgL_objectz00_bglt)
										((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4577)));
									BgL_auxz00_6391 = BGL_OBJECT_WIDENING(BgL_tmpz00_6392);
								}
								BgL_auxz00_6390 =
									((BgL_synczf2livenesszf2_bglt) BgL_auxz00_6391);
							}
							((((BgL_synczf2livenesszf2_bglt) COBJECT(BgL_auxz00_6390))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2556z00_4905 =
							((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nz00_4577));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2556z00_4905));
					}
				}
			}
		}

	}



/* &inout-box-set!/liven2106 */
	obj_t BGl_z62inoutzd2boxzd2setz12zf2liven2106z82zzliveness_livenessz00(obj_t
		BgL_envz00_4578, obj_t BgL_nz00_4579)
	{
		{	/* Liveness/liveness.scm 1103 */
			{	/* Liveness/liveness.scm 1105 */
				obj_t BgL_val0_1824z00_4909;
				obj_t BgL_val1_1825z00_4910;

				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6403;

					{
						obj_t BgL_auxz00_6404;

						{	/* Liveness/liveness.scm 1105 */
							BgL_objectz00_bglt BgL_tmpz00_6405;

							BgL_tmpz00_6405 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4579));
							BgL_auxz00_6404 = BGL_OBJECT_WIDENING(BgL_tmpz00_6405);
						}
						BgL_auxz00_6403 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6404);
					}
					BgL_val0_1824z00_4909 =
						(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_6403))->
						BgL_inz00);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6411;

					{
						obj_t BgL_auxz00_6412;

						{	/* Liveness/liveness.scm 1105 */
							BgL_objectz00_bglt BgL_tmpz00_6413;

							BgL_tmpz00_6413 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4579));
							BgL_auxz00_6412 = BGL_OBJECT_WIDENING(BgL_tmpz00_6413);
						}
						BgL_auxz00_6411 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6412);
					}
					BgL_val1_1825z00_4910 =
						(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_6411))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1105 */
					int BgL_tmpz00_6419;

					BgL_tmpz00_6419 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6419);
				}
				{	/* Liveness/liveness.scm 1105 */
					int BgL_tmpz00_6422;

					BgL_tmpz00_6422 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6422, BgL_val1_1825z00_4910);
				}
				return BgL_val0_1824z00_4909;
			}
		}

	}



/* &inout!-box-set!/live2104 */
	obj_t BGl_z62inoutz12zd2boxzd2setz12zf2live2104z90zzliveness_livenessz00(obj_t
		BgL_envz00_4580, obj_t BgL_nz00_4581, obj_t BgL_oz00_4582)
	{
		{	/* Liveness/liveness.scm 1096 */
			{
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6425;

				{
					obj_t BgL_auxz00_6426;

					{	/* Liveness/liveness.scm 1098 */
						BgL_objectz00_bglt BgL_tmpz00_6427;

						BgL_tmpz00_6427 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4581));
						BgL_auxz00_6426 = BGL_OBJECT_WIDENING(BgL_tmpz00_6427);
					}
					BgL_auxz00_6425 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6426);
				}
				((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_6425))->
						BgL_outz00) = ((obj_t) BgL_oz00_4582), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_6440;
				BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6433;

				{	/* Liveness/liveness.scm 1099 */
					obj_t BgL_arg2551z00_4912;
					obj_t BgL_arg2552z00_4913;

					{
						BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6441;

						{
							obj_t BgL_auxz00_6442;

							{	/* Liveness/liveness.scm 1099 */
								BgL_objectz00_bglt BgL_tmpz00_6443;

								BgL_tmpz00_6443 =
									((BgL_objectz00_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4581));
								BgL_auxz00_6442 = BGL_OBJECT_WIDENING(BgL_tmpz00_6443);
							}
							BgL_auxz00_6441 =
								((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6442);
						}
						BgL_arg2551z00_4912 =
							(((BgL_boxzd2setz12zf2livenessz32_bglt)
								COBJECT(BgL_auxz00_6441))->BgL_usez00);
					}
					{	/* Liveness/liveness.scm 1099 */
						obj_t BgL_arg2553z00_4914;
						obj_t BgL_arg2554z00_4915;

						{
							BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6449;

							{
								obj_t BgL_auxz00_6450;

								{	/* Liveness/liveness.scm 1099 */
									BgL_objectz00_bglt BgL_tmpz00_6451;

									BgL_tmpz00_6451 =
										((BgL_objectz00_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4581));
									BgL_auxz00_6450 = BGL_OBJECT_WIDENING(BgL_tmpz00_6451);
								}
								BgL_auxz00_6449 =
									((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6450);
							}
							BgL_arg2553z00_4914 =
								(((BgL_boxzd2setz12zf2livenessz32_bglt)
									COBJECT(BgL_auxz00_6449))->BgL_outz00);
						}
						{
							BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6457;

							{
								obj_t BgL_auxz00_6458;

								{	/* Liveness/liveness.scm 1099 */
									BgL_objectz00_bglt BgL_tmpz00_6459;

									BgL_tmpz00_6459 =
										((BgL_objectz00_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4581));
									BgL_auxz00_6458 = BGL_OBJECT_WIDENING(BgL_tmpz00_6459);
								}
								BgL_auxz00_6457 =
									((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6458);
							}
							BgL_arg2554z00_4915 =
								(((BgL_boxzd2setz12zf2livenessz32_bglt)
									COBJECT(BgL_auxz00_6457))->BgL_defz00);
						}
						BgL_arg2552z00_4913 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2553z00_4914,
							BgL_arg2554z00_4915);
					}
					BgL_auxz00_6440 =
						BGl_unionz00zzliveness_setz00(BgL_arg2551z00_4912,
						BgL_arg2552z00_4913);
				}
				{
					obj_t BgL_auxz00_6434;

					{	/* Liveness/liveness.scm 1099 */
						BgL_objectz00_bglt BgL_tmpz00_6435;

						BgL_tmpz00_6435 =
							((BgL_objectz00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4581));
						BgL_auxz00_6434 = BGL_OBJECT_WIDENING(BgL_tmpz00_6435);
					}
					BgL_auxz00_6433 =
						((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6434);
				}
				((((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_6433))->
						BgL_inz00) = ((obj_t) BgL_auxz00_6440), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 1100 */
				BgL_nodez00_bglt BgL_arg2555z00_4916;

				BgL_arg2555z00_4916 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4581))))->BgL_valuez00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2555z00_4916,
					BgL_oz00_4582);
			}
			{	/* Liveness/liveness.scm 1101 */
				obj_t BgL_val0_1822z00_4917;
				obj_t BgL_val1_1823z00_4918;

				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6472;

					{
						obj_t BgL_auxz00_6473;

						{	/* Liveness/liveness.scm 1101 */
							BgL_objectz00_bglt BgL_tmpz00_6474;

							BgL_tmpz00_6474 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4581));
							BgL_auxz00_6473 = BGL_OBJECT_WIDENING(BgL_tmpz00_6474);
						}
						BgL_auxz00_6472 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6473);
					}
					BgL_val0_1822z00_4917 =
						(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_6472))->
						BgL_inz00);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6480;

					{
						obj_t BgL_auxz00_6481;

						{	/* Liveness/liveness.scm 1101 */
							BgL_objectz00_bglt BgL_tmpz00_6482;

							BgL_tmpz00_6482 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4581));
							BgL_auxz00_6481 = BGL_OBJECT_WIDENING(BgL_tmpz00_6482);
						}
						BgL_auxz00_6480 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6481);
					}
					BgL_val1_1823z00_4918 =
						(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_6480))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1101 */
					int BgL_tmpz00_6488;

					BgL_tmpz00_6488 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6488);
				}
				{	/* Liveness/liveness.scm 1101 */
					int BgL_tmpz00_6491;

					BgL_tmpz00_6491 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6491, BgL_val1_1823z00_4918);
				}
				return BgL_val0_1822z00_4917;
			}
		}

	}



/* &defuse-box-set!/live2102 */
	obj_t BGl_z62defusezd2boxzd2setz12zf2live2102z82zzliveness_livenessz00(obj_t
		BgL_envz00_4583, obj_t BgL_nz00_4584)
	{
		{	/* Liveness/liveness.scm 1092 */
			{	/* Liveness/liveness.scm 1094 */
				obj_t BgL_val0_1820z00_4920;
				obj_t BgL_val1_1821z00_4921;

				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6494;

					{
						obj_t BgL_auxz00_6495;

						{	/* Liveness/liveness.scm 1094 */
							BgL_objectz00_bglt BgL_tmpz00_6496;

							BgL_tmpz00_6496 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4584));
							BgL_auxz00_6495 = BGL_OBJECT_WIDENING(BgL_tmpz00_6496);
						}
						BgL_auxz00_6494 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6495);
					}
					BgL_val0_1820z00_4920 =
						(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_6494))->
						BgL_defz00);
				}
				{
					BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6502;

					{
						obj_t BgL_auxz00_6503;

						{	/* Liveness/liveness.scm 1094 */
							BgL_objectz00_bglt BgL_tmpz00_6504;

							BgL_tmpz00_6504 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4584));
							BgL_auxz00_6503 = BGL_OBJECT_WIDENING(BgL_tmpz00_6504);
						}
						BgL_auxz00_6502 =
							((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6503);
					}
					BgL_val1_1821z00_4921 =
						(((BgL_boxzd2setz12zf2livenessz32_bglt) COBJECT(BgL_auxz00_6502))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 1094 */
					int BgL_tmpz00_6510;

					BgL_tmpz00_6510 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6510);
				}
				{	/* Liveness/liveness.scm 1094 */
					int BgL_tmpz00_6513;

					BgL_tmpz00_6513 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6513, BgL_val1_1821z00_4921);
				}
				return BgL_val0_1820z00_4920;
			}
		}

	}



/* &defuse-box-set!2100 */
	obj_t BGl_z62defusezd2boxzd2setz122100z70zzliveness_livenessz00(obj_t
		BgL_envz00_4585, obj_t BgL_nz00_4586)
	{
		{	/* Liveness/liveness.scm 1081 */
			{	/* Liveness/liveness.scm 1083 */
				obj_t BgL_defvaluez00_4923;

				BgL_defvaluez00_4923 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586)))->BgL_valuez00));
				{	/* Liveness/liveness.scm 1084 */
					obj_t BgL_usevaluez00_4924;

					{	/* Liveness/liveness.scm 1085 */
						obj_t BgL_tmpz00_4925;

						{	/* Liveness/liveness.scm 1085 */
							int BgL_tmpz00_6519;

							BgL_tmpz00_6519 = (int) (1L);
							BgL_tmpz00_4925 = BGL_MVALUES_VAL(BgL_tmpz00_6519);
						}
						{	/* Liveness/liveness.scm 1085 */
							int BgL_tmpz00_6522;

							BgL_tmpz00_6522 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6522, BUNSPEC);
						}
						BgL_usevaluez00_4924 = BgL_tmpz00_4925;
					}
					{	/* Liveness/liveness.scm 1085 */
						obj_t BgL_defvarz00_4926;

						{	/* Liveness/liveness.scm 1086 */
							BgL_varz00_bglt BgL_arg2548z00_4927;

							BgL_arg2548z00_4927 =
								(((BgL_boxzd2setz12zc0_bglt) COBJECT(
										((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586)))->BgL_varz00);
							BgL_defvarz00_4926 =
								BGl_defusez00zzliveness_livenessz00(
								((BgL_nodez00_bglt) BgL_arg2548z00_4927));
						}
						{	/* Liveness/liveness.scm 1086 */
							obj_t BgL_usevarz00_4928;

							{	/* Liveness/liveness.scm 1087 */
								obj_t BgL_tmpz00_4929;

								{	/* Liveness/liveness.scm 1087 */
									int BgL_tmpz00_6529;

									BgL_tmpz00_6529 = (int) (1L);
									BgL_tmpz00_4929 = BGL_MVALUES_VAL(BgL_tmpz00_6529);
								}
								{	/* Liveness/liveness.scm 1087 */
									int BgL_tmpz00_6532;

									BgL_tmpz00_6532 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_6532, BUNSPEC);
								}
								BgL_usevarz00_4928 = BgL_tmpz00_4929;
							}
							{	/* Liveness/liveness.scm 1088 */
								BgL_boxzd2setz12zc0_bglt BgL_arg2545z00_4930;

								{	/* Liveness/liveness.scm 1088 */
									BgL_boxzd2setz12zf2livenessz32_bglt BgL_wide1428z00_4931;

									BgL_wide1428z00_4931 =
										((BgL_boxzd2setz12zf2livenessz32_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_boxzd2setz12zf2livenessz32_bgl))));
									{	/* Liveness/liveness.scm 1088 */
										obj_t BgL_auxz00_6540;
										BgL_objectz00_bglt BgL_tmpz00_6536;

										BgL_auxz00_6540 = ((obj_t) BgL_wide1428z00_4931);
										BgL_tmpz00_6536 =
											((BgL_objectz00_bglt)
											((BgL_boxzd2setz12zc0_bglt)
												((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6536, BgL_auxz00_6540);
									}
									((BgL_objectz00_bglt)
										((BgL_boxzd2setz12zc0_bglt)
											((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586)));
									{	/* Liveness/liveness.scm 1088 */
										long BgL_arg2546z00_4932;

										BgL_arg2546z00_4932 =
											BGL_CLASS_NUM
											(BGl_boxzd2setz12zf2livenessz32zzliveness_typesz00);
										BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
													(BgL_boxzd2setz12zc0_bglt) ((BgL_boxzd2setz12zc0_bglt)
														BgL_nz00_4586))), BgL_arg2546z00_4932);
									}
									((BgL_boxzd2setz12zc0_bglt)
										((BgL_boxzd2setz12zc0_bglt)
											((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586)));
								}
								{
									BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6554;

									{
										obj_t BgL_auxz00_6555;

										{	/* Liveness/liveness.scm 1089 */
											BgL_objectz00_bglt BgL_tmpz00_6556;

											BgL_tmpz00_6556 =
												((BgL_objectz00_bglt)
												((BgL_boxzd2setz12zc0_bglt)
													((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586)));
											BgL_auxz00_6555 = BGL_OBJECT_WIDENING(BgL_tmpz00_6556);
										}
										BgL_auxz00_6554 =
											((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6555);
									}
									((((BgL_boxzd2setz12zf2livenessz32_bglt)
												COBJECT(BgL_auxz00_6554))->BgL_defz00) =
										((obj_t) BGl_unionz00zzliveness_setz00(BgL_defvaluez00_4923,
												BgL_defvarz00_4926)), BUNSPEC);
								}
								{
									BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6564;

									{
										obj_t BgL_auxz00_6565;

										{	/* Liveness/liveness.scm 1090 */
											BgL_objectz00_bglt BgL_tmpz00_6566;

											BgL_tmpz00_6566 =
												((BgL_objectz00_bglt)
												((BgL_boxzd2setz12zc0_bglt)
													((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586)));
											BgL_auxz00_6565 = BGL_OBJECT_WIDENING(BgL_tmpz00_6566);
										}
										BgL_auxz00_6564 =
											((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6565);
									}
									((((BgL_boxzd2setz12zf2livenessz32_bglt)
												COBJECT(BgL_auxz00_6564))->BgL_usez00) =
										((obj_t) BGl_unionz00zzliveness_setz00(BgL_usevaluez00_4924,
												BgL_usevarz00_4928)), BUNSPEC);
								}
								{
									BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6574;

									{
										obj_t BgL_auxz00_6575;

										{	/* Liveness/liveness.scm 1086 */
											BgL_objectz00_bglt BgL_tmpz00_6576;

											BgL_tmpz00_6576 =
												((BgL_objectz00_bglt)
												((BgL_boxzd2setz12zc0_bglt)
													((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586)));
											BgL_auxz00_6575 = BGL_OBJECT_WIDENING(BgL_tmpz00_6576);
										}
										BgL_auxz00_6574 =
											((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6575);
									}
									((((BgL_boxzd2setz12zf2livenessz32_bglt)
												COBJECT(BgL_auxz00_6574))->BgL_inz00) =
										((obj_t) BNIL), BUNSPEC);
								}
								{
									BgL_boxzd2setz12zf2livenessz32_bglt BgL_auxz00_6583;

									{
										obj_t BgL_auxz00_6584;

										{	/* Liveness/liveness.scm 1086 */
											BgL_objectz00_bglt BgL_tmpz00_6585;

											BgL_tmpz00_6585 =
												((BgL_objectz00_bglt)
												((BgL_boxzd2setz12zc0_bglt)
													((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586)));
											BgL_auxz00_6584 = BGL_OBJECT_WIDENING(BgL_tmpz00_6585);
										}
										BgL_auxz00_6583 =
											((BgL_boxzd2setz12zf2livenessz32_bglt) BgL_auxz00_6584);
									}
									((((BgL_boxzd2setz12zf2livenessz32_bglt)
												COBJECT(BgL_auxz00_6583))->BgL_outz00) =
										((obj_t) BNIL), BUNSPEC);
								}
								BgL_arg2545z00_4930 =
									((BgL_boxzd2setz12zc0_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_4586));
								return
									BGl_defusez00zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_arg2545z00_4930));
							}
						}
					}
				}
			}
		}

	}



/* &inout-box-ref/livene2098 */
	obj_t BGl_z62inoutzd2boxzd2refzf2livene2098z90zzliveness_livenessz00(obj_t
		BgL_envz00_4587, obj_t BgL_nz00_4588)
	{
		{	/* Liveness/liveness.scm 1074 */
			{	/* Liveness/liveness.scm 1076 */
				obj_t BgL_val0_1818z00_4934;
				obj_t BgL_val1_1819z00_4935;

				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6596;

					{
						obj_t BgL_auxz00_6597;

						{	/* Liveness/liveness.scm 1076 */
							BgL_objectz00_bglt BgL_tmpz00_6598;

							BgL_tmpz00_6598 =
								((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_4588));
							BgL_auxz00_6597 = BGL_OBJECT_WIDENING(BgL_tmpz00_6598);
						}
						BgL_auxz00_6596 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6597);
					}
					BgL_val0_1818z00_4934 =
						(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6596))->
						BgL_inz00);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6604;

					{
						obj_t BgL_auxz00_6605;

						{	/* Liveness/liveness.scm 1076 */
							BgL_objectz00_bglt BgL_tmpz00_6606;

							BgL_tmpz00_6606 =
								((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_4588));
							BgL_auxz00_6605 = BGL_OBJECT_WIDENING(BgL_tmpz00_6606);
						}
						BgL_auxz00_6604 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6605);
					}
					BgL_val1_1819z00_4935 =
						(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6604))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1076 */
					int BgL_tmpz00_6612;

					BgL_tmpz00_6612 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6612);
				}
				{	/* Liveness/liveness.scm 1076 */
					int BgL_tmpz00_6615;

					BgL_tmpz00_6615 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6615, BgL_val1_1819z00_4935);
				}
				return BgL_val0_1818z00_4934;
			}
		}

	}



/* &inout!-box-ref/liven2096 */
	obj_t BGl_z62inoutz12zd2boxzd2refzf2liven2096z82zzliveness_livenessz00(obj_t
		BgL_envz00_4589, obj_t BgL_nz00_4590, obj_t BgL_oz00_4591)
	{
		{	/* Liveness/liveness.scm 1067 */
			{
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6618;

				{
					obj_t BgL_auxz00_6619;

					{	/* Liveness/liveness.scm 1069 */
						BgL_objectz00_bglt BgL_tmpz00_6620;

						BgL_tmpz00_6620 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_4590));
						BgL_auxz00_6619 = BGL_OBJECT_WIDENING(BgL_tmpz00_6620);
					}
					BgL_auxz00_6618 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6619);
				}
				((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6618))->
						BgL_outz00) = ((obj_t) BgL_oz00_4591), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_6633;
				BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6626;

				{	/* Liveness/liveness.scm 1070 */
					obj_t BgL_arg2538z00_4937;
					obj_t BgL_arg2539z00_4938;

					{
						BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6634;

						{
							obj_t BgL_auxz00_6635;

							{	/* Liveness/liveness.scm 1070 */
								BgL_objectz00_bglt BgL_tmpz00_6636;

								BgL_tmpz00_6636 =
									((BgL_objectz00_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_nz00_4590));
								BgL_auxz00_6635 = BGL_OBJECT_WIDENING(BgL_tmpz00_6636);
							}
							BgL_auxz00_6634 =
								((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6635);
						}
						BgL_arg2538z00_4937 =
							(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6634))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 1070 */
						obj_t BgL_arg2540z00_4939;
						obj_t BgL_arg2542z00_4940;

						{
							BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6642;

							{
								obj_t BgL_auxz00_6643;

								{	/* Liveness/liveness.scm 1070 */
									BgL_objectz00_bglt BgL_tmpz00_6644;

									BgL_tmpz00_6644 =
										((BgL_objectz00_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nz00_4590));
									BgL_auxz00_6643 = BGL_OBJECT_WIDENING(BgL_tmpz00_6644);
								}
								BgL_auxz00_6642 =
									((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6643);
							}
							BgL_arg2540z00_4939 =
								(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6642))->
								BgL_outz00);
						}
						{
							BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6650;

							{
								obj_t BgL_auxz00_6651;

								{	/* Liveness/liveness.scm 1070 */
									BgL_objectz00_bglt BgL_tmpz00_6652;

									BgL_tmpz00_6652 =
										((BgL_objectz00_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nz00_4590));
									BgL_auxz00_6651 = BGL_OBJECT_WIDENING(BgL_tmpz00_6652);
								}
								BgL_auxz00_6650 =
									((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6651);
							}
							BgL_arg2542z00_4940 =
								(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6650))->
								BgL_defz00);
						}
						BgL_arg2539z00_4938 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2540z00_4939,
							BgL_arg2542z00_4940);
					}
					BgL_auxz00_6633 =
						BGl_unionz00zzliveness_setz00(BgL_arg2538z00_4937,
						BgL_arg2539z00_4938);
				}
				{
					obj_t BgL_auxz00_6627;

					{	/* Liveness/liveness.scm 1070 */
						BgL_objectz00_bglt BgL_tmpz00_6628;

						BgL_tmpz00_6628 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_4590));
						BgL_auxz00_6627 = BGL_OBJECT_WIDENING(BgL_tmpz00_6628);
					}
					BgL_auxz00_6626 =
						((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6627);
				}
				((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6626))->
						BgL_inz00) = ((obj_t) BgL_auxz00_6633), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 1071 */
				BgL_varz00_bglt BgL_arg2543z00_4941;

				BgL_arg2543z00_4941 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_nz00_4590))))->BgL_varz00);
				BGl_inoutz12z12zzliveness_livenessz00(
					((BgL_nodez00_bglt) BgL_arg2543z00_4941), BgL_oz00_4591);
			}
			{	/* Liveness/liveness.scm 1072 */
				obj_t BgL_val0_1816z00_4942;
				obj_t BgL_val1_1817z00_4943;

				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6666;

					{
						obj_t BgL_auxz00_6667;

						{	/* Liveness/liveness.scm 1072 */
							BgL_objectz00_bglt BgL_tmpz00_6668;

							BgL_tmpz00_6668 =
								((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_4590));
							BgL_auxz00_6667 = BGL_OBJECT_WIDENING(BgL_tmpz00_6668);
						}
						BgL_auxz00_6666 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6667);
					}
					BgL_val0_1816z00_4942 =
						(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6666))->
						BgL_inz00);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6674;

					{
						obj_t BgL_auxz00_6675;

						{	/* Liveness/liveness.scm 1072 */
							BgL_objectz00_bglt BgL_tmpz00_6676;

							BgL_tmpz00_6676 =
								((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_4590));
							BgL_auxz00_6675 = BGL_OBJECT_WIDENING(BgL_tmpz00_6676);
						}
						BgL_auxz00_6674 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6675);
					}
					BgL_val1_1817z00_4943 =
						(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6674))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1072 */
					int BgL_tmpz00_6682;

					BgL_tmpz00_6682 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6682);
				}
				{	/* Liveness/liveness.scm 1072 */
					int BgL_tmpz00_6685;

					BgL_tmpz00_6685 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6685, BgL_val1_1817z00_4943);
				}
				return BgL_val0_1816z00_4942;
			}
		}

	}



/* &defuse-box-ref/liven2094 */
	obj_t BGl_z62defusezd2boxzd2refzf2liven2094z90zzliveness_livenessz00(obj_t
		BgL_envz00_4592, obj_t BgL_nz00_4593)
	{
		{	/* Liveness/liveness.scm 1063 */
			{	/* Liveness/liveness.scm 1065 */
				obj_t BgL_val0_1814z00_4945;
				obj_t BgL_val1_1815z00_4946;

				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6688;

					{
						obj_t BgL_auxz00_6689;

						{	/* Liveness/liveness.scm 1065 */
							BgL_objectz00_bglt BgL_tmpz00_6690;

							BgL_tmpz00_6690 =
								((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_4593));
							BgL_auxz00_6689 = BGL_OBJECT_WIDENING(BgL_tmpz00_6690);
						}
						BgL_auxz00_6688 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6689);
					}
					BgL_val0_1814z00_4945 =
						(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6688))->
						BgL_defz00);
				}
				{
					BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6696;

					{
						obj_t BgL_auxz00_6697;

						{	/* Liveness/liveness.scm 1065 */
							BgL_objectz00_bglt BgL_tmpz00_6698;

							BgL_tmpz00_6698 =
								((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_4593));
							BgL_auxz00_6697 = BGL_OBJECT_WIDENING(BgL_tmpz00_6698);
						}
						BgL_auxz00_6696 =
							((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6697);
					}
					BgL_val1_1815z00_4946 =
						(((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6696))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 1065 */
					int BgL_tmpz00_6704;

					BgL_tmpz00_6704 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6704);
				}
				{	/* Liveness/liveness.scm 1065 */
					int BgL_tmpz00_6707;

					BgL_tmpz00_6707 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6707, BgL_val1_1815z00_4946);
				}
				return BgL_val0_1814z00_4945;
			}
		}

	}



/* &defuse-box-ref2092 */
	obj_t BGl_z62defusezd2boxzd2ref2092z62zzliveness_livenessz00(obj_t
		BgL_envz00_4594, obj_t BgL_nz00_4595)
	{
		{	/* Liveness/liveness.scm 1054 */
			{	/* Liveness/liveness.scm 1056 */
				obj_t BgL_defz00_4948;

				{	/* Liveness/liveness.scm 1057 */
					BgL_varz00_bglt BgL_arg2537z00_4949;

					BgL_arg2537z00_4949 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nz00_4595)))->BgL_varz00);
					BgL_defz00_4948 =
						BGl_defusez00zzliveness_livenessz00(
						((BgL_nodez00_bglt) BgL_arg2537z00_4949));
				}
				{	/* Liveness/liveness.scm 1057 */
					obj_t BgL_usez00_4950;

					{	/* Liveness/liveness.scm 1058 */
						obj_t BgL_tmpz00_4951;

						{	/* Liveness/liveness.scm 1058 */
							int BgL_tmpz00_6714;

							BgL_tmpz00_6714 = (int) (1L);
							BgL_tmpz00_4951 = BGL_MVALUES_VAL(BgL_tmpz00_6714);
						}
						{	/* Liveness/liveness.scm 1058 */
							int BgL_tmpz00_6717;

							BgL_tmpz00_6717 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6717, BUNSPEC);
						}
						BgL_usez00_4950 = BgL_tmpz00_4951;
					}
					{	/* Liveness/liveness.scm 1059 */
						BgL_boxzd2refzd2_bglt BgL_arg2534z00_4952;

						{	/* Liveness/liveness.scm 1059 */
							BgL_boxzd2refzf2livenessz20_bglt BgL_wide1420z00_4953;

							BgL_wide1420z00_4953 =
								((BgL_boxzd2refzf2livenessz20_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_boxzd2refzf2livenessz20_bgl))));
							{	/* Liveness/liveness.scm 1059 */
								obj_t BgL_auxz00_6725;
								BgL_objectz00_bglt BgL_tmpz00_6721;

								BgL_auxz00_6725 = ((obj_t) BgL_wide1420z00_4953);
								BgL_tmpz00_6721 =
									((BgL_objectz00_bglt)
									((BgL_boxzd2refzd2_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nz00_4595)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6721, BgL_auxz00_6725);
							}
							((BgL_objectz00_bglt)
								((BgL_boxzd2refzd2_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_nz00_4595)));
							{	/* Liveness/liveness.scm 1059 */
								long BgL_arg2536z00_4954;

								BgL_arg2536z00_4954 =
									BGL_CLASS_NUM(BGl_boxzd2refzf2livenessz20zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_boxzd2refzd2_bglt)
											((BgL_boxzd2refzd2_bglt) BgL_nz00_4595))),
									BgL_arg2536z00_4954);
							}
							((BgL_boxzd2refzd2_bglt)
								((BgL_boxzd2refzd2_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_nz00_4595)));
						}
						{
							BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6739;

							{
								obj_t BgL_auxz00_6740;

								{	/* Liveness/liveness.scm 1061 */
									BgL_objectz00_bglt BgL_tmpz00_6741;

									BgL_tmpz00_6741 =
										((BgL_objectz00_bglt)
										((BgL_boxzd2refzd2_bglt)
											((BgL_boxzd2refzd2_bglt) BgL_nz00_4595)));
									BgL_auxz00_6740 = BGL_OBJECT_WIDENING(BgL_tmpz00_6741);
								}
								BgL_auxz00_6739 =
									((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6740);
							}
							((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6739))->
									BgL_defz00) = ((obj_t) BgL_defz00_4948), BUNSPEC);
						}
						{
							BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6748;

							{
								obj_t BgL_auxz00_6749;

								{	/* Liveness/liveness.scm 1060 */
									BgL_objectz00_bglt BgL_tmpz00_6750;

									BgL_tmpz00_6750 =
										((BgL_objectz00_bglt)
										((BgL_boxzd2refzd2_bglt)
											((BgL_boxzd2refzd2_bglt) BgL_nz00_4595)));
									BgL_auxz00_6749 = BGL_OBJECT_WIDENING(BgL_tmpz00_6750);
								}
								BgL_auxz00_6748 =
									((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6749);
							}
							((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6748))->
									BgL_usez00) = ((obj_t) BgL_usez00_4950), BUNSPEC);
						}
						{
							BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6757;

							{
								obj_t BgL_auxz00_6758;

								{	/* Liveness/liveness.scm 1057 */
									BgL_objectz00_bglt BgL_tmpz00_6759;

									BgL_tmpz00_6759 =
										((BgL_objectz00_bglt)
										((BgL_boxzd2refzd2_bglt)
											((BgL_boxzd2refzd2_bglt) BgL_nz00_4595)));
									BgL_auxz00_6758 = BGL_OBJECT_WIDENING(BgL_tmpz00_6759);
								}
								BgL_auxz00_6757 =
									((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6758);
							}
							((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6757))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_boxzd2refzf2livenessz20_bglt BgL_auxz00_6766;

							{
								obj_t BgL_auxz00_6767;

								{	/* Liveness/liveness.scm 1057 */
									BgL_objectz00_bglt BgL_tmpz00_6768;

									BgL_tmpz00_6768 =
										((BgL_objectz00_bglt)
										((BgL_boxzd2refzd2_bglt)
											((BgL_boxzd2refzd2_bglt) BgL_nz00_4595)));
									BgL_auxz00_6767 = BGL_OBJECT_WIDENING(BgL_tmpz00_6768);
								}
								BgL_auxz00_6766 =
									((BgL_boxzd2refzf2livenessz20_bglt) BgL_auxz00_6767);
							}
							((((BgL_boxzd2refzf2livenessz20_bglt) COBJECT(BgL_auxz00_6766))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2534z00_4952 =
							((BgL_boxzd2refzd2_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_4595));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2534z00_4952));
					}
				}
			}
		}

	}



/* &inout-make-box/liven2090 */
	obj_t BGl_z62inoutzd2makezd2boxzf2liven2090z90zzliveness_livenessz00(obj_t
		BgL_envz00_4596, obj_t BgL_nz00_4597)
	{
		{	/* Liveness/liveness.scm 1047 */
			{	/* Liveness/liveness.scm 1049 */
				obj_t BgL_val0_1812z00_4956;
				obj_t BgL_val1_1813z00_4957;

				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6779;

					{
						obj_t BgL_auxz00_6780;

						{	/* Liveness/liveness.scm 1049 */
							BgL_objectz00_bglt BgL_tmpz00_6781;

							BgL_tmpz00_6781 =
								((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_4597));
							BgL_auxz00_6780 = BGL_OBJECT_WIDENING(BgL_tmpz00_6781);
						}
						BgL_auxz00_6779 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6780);
					}
					BgL_val0_1812z00_4956 =
						(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6779))->
						BgL_inz00);
				}
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6787;

					{
						obj_t BgL_auxz00_6788;

						{	/* Liveness/liveness.scm 1049 */
							BgL_objectz00_bglt BgL_tmpz00_6789;

							BgL_tmpz00_6789 =
								((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_4597));
							BgL_auxz00_6788 = BGL_OBJECT_WIDENING(BgL_tmpz00_6789);
						}
						BgL_auxz00_6787 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6788);
					}
					BgL_val1_1813z00_4957 =
						(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6787))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1049 */
					int BgL_tmpz00_6795;

					BgL_tmpz00_6795 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6795);
				}
				{	/* Liveness/liveness.scm 1049 */
					int BgL_tmpz00_6798;

					BgL_tmpz00_6798 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6798, BgL_val1_1813z00_4957);
				}
				return BgL_val0_1812z00_4956;
			}
		}

	}



/* &inout!-make-box/live2088 */
	obj_t BGl_z62inoutz12zd2makezd2boxzf2live2088z82zzliveness_livenessz00(obj_t
		BgL_envz00_4598, obj_t BgL_nz00_4599, obj_t BgL_oz00_4600)
	{
		{	/* Liveness/liveness.scm 1040 */
			{
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6801;

				{
					obj_t BgL_auxz00_6802;

					{	/* Liveness/liveness.scm 1042 */
						BgL_objectz00_bglt BgL_tmpz00_6803;

						BgL_tmpz00_6803 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_4599));
						BgL_auxz00_6802 = BGL_OBJECT_WIDENING(BgL_tmpz00_6803);
					}
					BgL_auxz00_6801 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6802);
				}
				((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6801))->
						BgL_outz00) = ((obj_t) BgL_oz00_4600), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_6816;
				BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6809;

				{	/* Liveness/liveness.scm 1043 */
					obj_t BgL_arg2525z00_4959;
					obj_t BgL_arg2526z00_4960;

					{
						BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6817;

						{
							obj_t BgL_auxz00_6818;

							{	/* Liveness/liveness.scm 1043 */
								BgL_objectz00_bglt BgL_tmpz00_6819;

								BgL_tmpz00_6819 =
									((BgL_objectz00_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nz00_4599));
								BgL_auxz00_6818 = BGL_OBJECT_WIDENING(BgL_tmpz00_6819);
							}
							BgL_auxz00_6817 =
								((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6818);
						}
						BgL_arg2525z00_4959 =
							(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6817))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 1043 */
						obj_t BgL_arg2527z00_4961;
						obj_t BgL_arg2528z00_4962;

						{
							BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6825;

							{
								obj_t BgL_auxz00_6826;

								{	/* Liveness/liveness.scm 1043 */
									BgL_objectz00_bglt BgL_tmpz00_6827;

									BgL_tmpz00_6827 =
										((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_nz00_4599));
									BgL_auxz00_6826 = BGL_OBJECT_WIDENING(BgL_tmpz00_6827);
								}
								BgL_auxz00_6825 =
									((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6826);
							}
							BgL_arg2527z00_4961 =
								(((BgL_makezd2boxzf2livenessz20_bglt)
									COBJECT(BgL_auxz00_6825))->BgL_outz00);
						}
						{
							BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6833;

							{
								obj_t BgL_auxz00_6834;

								{	/* Liveness/liveness.scm 1043 */
									BgL_objectz00_bglt BgL_tmpz00_6835;

									BgL_tmpz00_6835 =
										((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_nz00_4599));
									BgL_auxz00_6834 = BGL_OBJECT_WIDENING(BgL_tmpz00_6835);
								}
								BgL_auxz00_6833 =
									((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6834);
							}
							BgL_arg2528z00_4962 =
								(((BgL_makezd2boxzf2livenessz20_bglt)
									COBJECT(BgL_auxz00_6833))->BgL_defz00);
						}
						BgL_arg2526z00_4960 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2527z00_4961,
							BgL_arg2528z00_4962);
					}
					BgL_auxz00_6816 =
						BGl_unionz00zzliveness_setz00(BgL_arg2525z00_4959,
						BgL_arg2526z00_4960);
				}
				{
					obj_t BgL_auxz00_6810;

					{	/* Liveness/liveness.scm 1043 */
						BgL_objectz00_bglt BgL_tmpz00_6811;

						BgL_tmpz00_6811 =
							((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_4599));
						BgL_auxz00_6810 = BGL_OBJECT_WIDENING(BgL_tmpz00_6811);
					}
					BgL_auxz00_6809 =
						((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6810);
				}
				((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6809))->
						BgL_inz00) = ((obj_t) BgL_auxz00_6816), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 1044 */
				BgL_nodez00_bglt BgL_arg2529z00_4963;

				BgL_arg2529z00_4963 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_nz00_4599))))->BgL_valuez00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2529z00_4963,
					BgL_oz00_4600);
			}
			{	/* Liveness/liveness.scm 1045 */
				obj_t BgL_val0_1810z00_4964;
				obj_t BgL_val1_1811z00_4965;

				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6848;

					{
						obj_t BgL_auxz00_6849;

						{	/* Liveness/liveness.scm 1045 */
							BgL_objectz00_bglt BgL_tmpz00_6850;

							BgL_tmpz00_6850 =
								((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_4599));
							BgL_auxz00_6849 = BGL_OBJECT_WIDENING(BgL_tmpz00_6850);
						}
						BgL_auxz00_6848 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6849);
					}
					BgL_val0_1810z00_4964 =
						(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6848))->
						BgL_inz00);
				}
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6856;

					{
						obj_t BgL_auxz00_6857;

						{	/* Liveness/liveness.scm 1045 */
							BgL_objectz00_bglt BgL_tmpz00_6858;

							BgL_tmpz00_6858 =
								((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_4599));
							BgL_auxz00_6857 = BGL_OBJECT_WIDENING(BgL_tmpz00_6858);
						}
						BgL_auxz00_6856 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6857);
					}
					BgL_val1_1811z00_4965 =
						(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6856))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1045 */
					int BgL_tmpz00_6864;

					BgL_tmpz00_6864 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6864);
				}
				{	/* Liveness/liveness.scm 1045 */
					int BgL_tmpz00_6867;

					BgL_tmpz00_6867 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6867, BgL_val1_1811z00_4965);
				}
				return BgL_val0_1810z00_4964;
			}
		}

	}



/* &defuse-make-box/live2086 */
	obj_t BGl_z62defusezd2makezd2boxzf2live2086z90zzliveness_livenessz00(obj_t
		BgL_envz00_4601, obj_t BgL_nz00_4602)
	{
		{	/* Liveness/liveness.scm 1036 */
			{	/* Liveness/liveness.scm 1038 */
				obj_t BgL_val0_1808z00_4967;
				obj_t BgL_val1_1809z00_4968;

				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6870;

					{
						obj_t BgL_auxz00_6871;

						{	/* Liveness/liveness.scm 1038 */
							BgL_objectz00_bglt BgL_tmpz00_6872;

							BgL_tmpz00_6872 =
								((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_4602));
							BgL_auxz00_6871 = BGL_OBJECT_WIDENING(BgL_tmpz00_6872);
						}
						BgL_auxz00_6870 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6871);
					}
					BgL_val0_1808z00_4967 =
						(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6870))->
						BgL_defz00);
				}
				{
					BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6878;

					{
						obj_t BgL_auxz00_6879;

						{	/* Liveness/liveness.scm 1038 */
							BgL_objectz00_bglt BgL_tmpz00_6880;

							BgL_tmpz00_6880 =
								((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_4602));
							BgL_auxz00_6879 = BGL_OBJECT_WIDENING(BgL_tmpz00_6880);
						}
						BgL_auxz00_6878 =
							((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6879);
					}
					BgL_val1_1809z00_4968 =
						(((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6878))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 1038 */
					int BgL_tmpz00_6886;

					BgL_tmpz00_6886 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6886);
				}
				{	/* Liveness/liveness.scm 1038 */
					int BgL_tmpz00_6889;

					BgL_tmpz00_6889 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6889, BgL_val1_1809z00_4968);
				}
				return BgL_val0_1808z00_4967;
			}
		}

	}



/* &defuse-make-box2084 */
	obj_t BGl_z62defusezd2makezd2box2084z62zzliveness_livenessz00(obj_t
		BgL_envz00_4603, obj_t BgL_nz00_4604)
	{
		{	/* Liveness/liveness.scm 1027 */
			{	/* Liveness/liveness.scm 1029 */
				obj_t BgL_defz00_4970;

				BgL_defz00_4970 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nz00_4604)))->BgL_valuez00));
				{	/* Liveness/liveness.scm 1030 */
					obj_t BgL_usez00_4971;

					{	/* Liveness/liveness.scm 1031 */
						obj_t BgL_tmpz00_4972;

						{	/* Liveness/liveness.scm 1031 */
							int BgL_tmpz00_6895;

							BgL_tmpz00_6895 = (int) (1L);
							BgL_tmpz00_4972 = BGL_MVALUES_VAL(BgL_tmpz00_6895);
						}
						{	/* Liveness/liveness.scm 1031 */
							int BgL_tmpz00_6898;

							BgL_tmpz00_6898 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6898, BUNSPEC);
						}
						BgL_usez00_4971 = BgL_tmpz00_4972;
					}
					{	/* Liveness/liveness.scm 1032 */
						BgL_makezd2boxzd2_bglt BgL_arg2519z00_4973;

						{	/* Liveness/liveness.scm 1032 */
							BgL_makezd2boxzf2livenessz20_bglt BgL_wide1412z00_4974;

							BgL_wide1412z00_4974 =
								((BgL_makezd2boxzf2livenessz20_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_makezd2boxzf2livenessz20_bgl))));
							{	/* Liveness/liveness.scm 1032 */
								obj_t BgL_auxz00_6906;
								BgL_objectz00_bglt BgL_tmpz00_6902;

								BgL_auxz00_6906 = ((obj_t) BgL_wide1412z00_4974);
								BgL_tmpz00_6902 =
									((BgL_objectz00_bglt)
									((BgL_makezd2boxzd2_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_nz00_4604)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6902, BgL_auxz00_6906);
							}
							((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nz00_4604)));
							{	/* Liveness/liveness.scm 1032 */
								long BgL_arg2521z00_4975;

								BgL_arg2521z00_4975 =
									BGL_CLASS_NUM
									(BGl_makezd2boxzf2livenessz20zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
											(BgL_makezd2boxzd2_bglt) ((BgL_makezd2boxzd2_bglt)
												BgL_nz00_4604))), BgL_arg2521z00_4975);
							}
							((BgL_makezd2boxzd2_bglt)
								((BgL_makezd2boxzd2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nz00_4604)));
						}
						{
							BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6920;

							{
								obj_t BgL_auxz00_6921;

								{	/* Liveness/liveness.scm 1033 */
									BgL_objectz00_bglt BgL_tmpz00_6922;

									BgL_tmpz00_6922 =
										((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_nz00_4604)));
									BgL_auxz00_6921 = BGL_OBJECT_WIDENING(BgL_tmpz00_6922);
								}
								BgL_auxz00_6920 =
									((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6921);
							}
							((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6920))->
									BgL_defz00) = ((obj_t) BgL_defz00_4970), BUNSPEC);
						}
						{
							BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6929;

							{
								obj_t BgL_auxz00_6930;

								{	/* Liveness/liveness.scm 1034 */
									BgL_objectz00_bglt BgL_tmpz00_6931;

									BgL_tmpz00_6931 =
										((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_nz00_4604)));
									BgL_auxz00_6930 = BGL_OBJECT_WIDENING(BgL_tmpz00_6931);
								}
								BgL_auxz00_6929 =
									((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6930);
							}
							((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6929))->
									BgL_usez00) = ((obj_t) BgL_usez00_4971), BUNSPEC);
						}
						{
							BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6938;

							{
								obj_t BgL_auxz00_6939;

								{	/* Liveness/liveness.scm 1030 */
									BgL_objectz00_bglt BgL_tmpz00_6940;

									BgL_tmpz00_6940 =
										((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_nz00_4604)));
									BgL_auxz00_6939 = BGL_OBJECT_WIDENING(BgL_tmpz00_6940);
								}
								BgL_auxz00_6938 =
									((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6939);
							}
							((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6938))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_makezd2boxzf2livenessz20_bglt BgL_auxz00_6947;

							{
								obj_t BgL_auxz00_6948;

								{	/* Liveness/liveness.scm 1030 */
									BgL_objectz00_bglt BgL_tmpz00_6949;

									BgL_tmpz00_6949 =
										((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_nz00_4604)));
									BgL_auxz00_6948 = BGL_OBJECT_WIDENING(BgL_tmpz00_6949);
								}
								BgL_auxz00_6947 =
									((BgL_makezd2boxzf2livenessz20_bglt) BgL_auxz00_6948);
							}
							((((BgL_makezd2boxzf2livenessz20_bglt) COBJECT(BgL_auxz00_6947))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2519z00_4973 =
							((BgL_makezd2boxzd2_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_nz00_4604));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2519z00_4973));
					}
				}
			}
		}

	}



/* &inout-return/livenes2082 */
	obj_t BGl_z62inoutzd2returnzf2livenes2082z42zzliveness_livenessz00(obj_t
		BgL_envz00_4605, obj_t BgL_nz00_4606)
	{
		{	/* Liveness/liveness.scm 1020 */
			{	/* Liveness/liveness.scm 1022 */
				obj_t BgL_val0_1806z00_4977;
				obj_t BgL_val1_1807z00_4978;

				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_6960;

					{
						obj_t BgL_auxz00_6961;

						{	/* Liveness/liveness.scm 1022 */
							BgL_objectz00_bglt BgL_tmpz00_6962;

							BgL_tmpz00_6962 =
								((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4606));
							BgL_auxz00_6961 = BGL_OBJECT_WIDENING(BgL_tmpz00_6962);
						}
						BgL_auxz00_6960 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_6961);
					}
					BgL_val0_1806z00_4977 =
						(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_6960))->
						BgL_inz00);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_6968;

					{
						obj_t BgL_auxz00_6969;

						{	/* Liveness/liveness.scm 1022 */
							BgL_objectz00_bglt BgL_tmpz00_6970;

							BgL_tmpz00_6970 =
								((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4606));
							BgL_auxz00_6969 = BGL_OBJECT_WIDENING(BgL_tmpz00_6970);
						}
						BgL_auxz00_6968 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_6969);
					}
					BgL_val1_1807z00_4978 =
						(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_6968))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1022 */
					int BgL_tmpz00_6976;

					BgL_tmpz00_6976 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6976);
				}
				{	/* Liveness/liveness.scm 1022 */
					int BgL_tmpz00_6979;

					BgL_tmpz00_6979 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_6979, BgL_val1_1807z00_4978);
				}
				return BgL_val0_1806z00_4977;
			}
		}

	}



/* &inout!-return/livene2080 */
	obj_t BGl_z62inoutz12zd2returnzf2livene2080z50zzliveness_livenessz00(obj_t
		BgL_envz00_4607, obj_t BgL_nz00_4608, obj_t BgL_oz00_4609)
	{
		{	/* Liveness/liveness.scm 1013 */
			{
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_6982;

				{
					obj_t BgL_auxz00_6983;

					{	/* Liveness/liveness.scm 1015 */
						BgL_objectz00_bglt BgL_tmpz00_6984;

						BgL_tmpz00_6984 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4608));
						BgL_auxz00_6983 = BGL_OBJECT_WIDENING(BgL_tmpz00_6984);
					}
					BgL_auxz00_6982 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_6983);
				}
				((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_6982))->
						BgL_outz00) = ((obj_t) BgL_oz00_4609), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_6997;
				BgL_returnzf2livenesszf2_bglt BgL_auxz00_6990;

				{	/* Liveness/liveness.scm 1016 */
					obj_t BgL_arg2513z00_4980;
					obj_t BgL_arg2514z00_4981;

					{
						BgL_returnzf2livenesszf2_bglt BgL_auxz00_6998;

						{
							obj_t BgL_auxz00_6999;

							{	/* Liveness/liveness.scm 1016 */
								BgL_objectz00_bglt BgL_tmpz00_7000;

								BgL_tmpz00_7000 =
									((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4608));
								BgL_auxz00_6999 = BGL_OBJECT_WIDENING(BgL_tmpz00_7000);
							}
							BgL_auxz00_6998 =
								((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_6999);
						}
						BgL_arg2513z00_4980 =
							(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_6998))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 1016 */
						obj_t BgL_arg2515z00_4982;
						obj_t BgL_arg2516z00_4983;

						{
							BgL_returnzf2livenesszf2_bglt BgL_auxz00_7006;

							{
								obj_t BgL_auxz00_7007;

								{	/* Liveness/liveness.scm 1016 */
									BgL_objectz00_bglt BgL_tmpz00_7008;

									BgL_tmpz00_7008 =
										((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4608));
									BgL_auxz00_7007 = BGL_OBJECT_WIDENING(BgL_tmpz00_7008);
								}
								BgL_auxz00_7006 =
									((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7007);
							}
							BgL_arg2515z00_4982 =
								(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7006))->
								BgL_outz00);
						}
						{
							BgL_returnzf2livenesszf2_bglt BgL_auxz00_7014;

							{
								obj_t BgL_auxz00_7015;

								{	/* Liveness/liveness.scm 1016 */
									BgL_objectz00_bglt BgL_tmpz00_7016;

									BgL_tmpz00_7016 =
										((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4608));
									BgL_auxz00_7015 = BGL_OBJECT_WIDENING(BgL_tmpz00_7016);
								}
								BgL_auxz00_7014 =
									((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7015);
							}
							BgL_arg2516z00_4983 =
								(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7014))->
								BgL_defz00);
						}
						BgL_arg2514z00_4981 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2515z00_4982,
							BgL_arg2516z00_4983);
					}
					BgL_auxz00_6997 =
						BGl_unionz00zzliveness_setz00(BgL_arg2513z00_4980,
						BgL_arg2514z00_4981);
				}
				{
					obj_t BgL_auxz00_6991;

					{	/* Liveness/liveness.scm 1016 */
						BgL_objectz00_bglt BgL_tmpz00_6992;

						BgL_tmpz00_6992 =
							((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4608));
						BgL_auxz00_6991 = BGL_OBJECT_WIDENING(BgL_tmpz00_6992);
					}
					BgL_auxz00_6990 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_6991);
				}
				((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_6990))->
						BgL_inz00) = ((obj_t) BgL_auxz00_6997), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 1017 */
				BgL_nodez00_bglt BgL_arg2518z00_4984;

				BgL_arg2518z00_4984 =
					(((BgL_returnz00_bglt) COBJECT(
							((BgL_returnz00_bglt)
								((BgL_returnz00_bglt) BgL_nz00_4608))))->BgL_valuez00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2518z00_4984,
					BgL_oz00_4609);
			}
			{	/* Liveness/liveness.scm 1018 */
				obj_t BgL_val0_1804z00_4985;
				obj_t BgL_val1_1805z00_4986;

				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_7029;

					{
						obj_t BgL_auxz00_7030;

						{	/* Liveness/liveness.scm 1018 */
							BgL_objectz00_bglt BgL_tmpz00_7031;

							BgL_tmpz00_7031 =
								((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4608));
							BgL_auxz00_7030 = BGL_OBJECT_WIDENING(BgL_tmpz00_7031);
						}
						BgL_auxz00_7029 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7030);
					}
					BgL_val0_1804z00_4985 =
						(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7029))->
						BgL_inz00);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_7037;

					{
						obj_t BgL_auxz00_7038;

						{	/* Liveness/liveness.scm 1018 */
							BgL_objectz00_bglt BgL_tmpz00_7039;

							BgL_tmpz00_7039 =
								((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4608));
							BgL_auxz00_7038 = BGL_OBJECT_WIDENING(BgL_tmpz00_7039);
						}
						BgL_auxz00_7037 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7038);
					}
					BgL_val1_1805z00_4986 =
						(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7037))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 1018 */
					int BgL_tmpz00_7045;

					BgL_tmpz00_7045 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7045);
				}
				{	/* Liveness/liveness.scm 1018 */
					int BgL_tmpz00_7048;

					BgL_tmpz00_7048 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7048, BgL_val1_1805z00_4986);
				}
				return BgL_val0_1804z00_4985;
			}
		}

	}



/* &defuse-return/livene2078 */
	obj_t BGl_z62defusezd2returnzf2livene2078z42zzliveness_livenessz00(obj_t
		BgL_envz00_4610, obj_t BgL_nz00_4611)
	{
		{	/* Liveness/liveness.scm 1009 */
			{	/* Liveness/liveness.scm 1011 */
				obj_t BgL_val0_1802z00_4988;
				obj_t BgL_val1_1803z00_4989;

				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_7051;

					{
						obj_t BgL_auxz00_7052;

						{	/* Liveness/liveness.scm 1011 */
							BgL_objectz00_bglt BgL_tmpz00_7053;

							BgL_tmpz00_7053 =
								((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4611));
							BgL_auxz00_7052 = BGL_OBJECT_WIDENING(BgL_tmpz00_7053);
						}
						BgL_auxz00_7051 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7052);
					}
					BgL_val0_1802z00_4988 =
						(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7051))->
						BgL_defz00);
				}
				{
					BgL_returnzf2livenesszf2_bglt BgL_auxz00_7059;

					{
						obj_t BgL_auxz00_7060;

						{	/* Liveness/liveness.scm 1011 */
							BgL_objectz00_bglt BgL_tmpz00_7061;

							BgL_tmpz00_7061 =
								((BgL_objectz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4611));
							BgL_auxz00_7060 = BGL_OBJECT_WIDENING(BgL_tmpz00_7061);
						}
						BgL_auxz00_7059 = ((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7060);
					}
					BgL_val1_1803z00_4989 =
						(((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7059))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 1011 */
					int BgL_tmpz00_7067;

					BgL_tmpz00_7067 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7067);
				}
				{	/* Liveness/liveness.scm 1011 */
					int BgL_tmpz00_7070;

					BgL_tmpz00_7070 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7070, BgL_val1_1803z00_4989);
				}
				return BgL_val0_1802z00_4988;
			}
		}

	}



/* &defuse-return2076 */
	obj_t BGl_z62defusezd2return2076zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4612, obj_t BgL_nz00_4613)
	{
		{	/* Liveness/liveness.scm 1000 */
			{	/* Liveness/liveness.scm 1002 */
				obj_t BgL_defz00_4991;

				BgL_defz00_4991 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nz00_4613)))->BgL_valuez00));
				{	/* Liveness/liveness.scm 1003 */
					obj_t BgL_usez00_4992;

					{	/* Liveness/liveness.scm 1004 */
						obj_t BgL_tmpz00_4993;

						{	/* Liveness/liveness.scm 1004 */
							int BgL_tmpz00_7076;

							BgL_tmpz00_7076 = (int) (1L);
							BgL_tmpz00_4993 = BGL_MVALUES_VAL(BgL_tmpz00_7076);
						}
						{	/* Liveness/liveness.scm 1004 */
							int BgL_tmpz00_7079;

							BgL_tmpz00_7079 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7079, BUNSPEC);
						}
						BgL_usez00_4992 = BgL_tmpz00_4993;
					}
					{	/* Liveness/liveness.scm 1005 */
						BgL_returnz00_bglt BgL_arg2510z00_4994;

						{	/* Liveness/liveness.scm 1005 */
							BgL_returnzf2livenesszf2_bglt BgL_wide1404z00_4995;

							BgL_wide1404z00_4995 =
								((BgL_returnzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_returnzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 1005 */
								obj_t BgL_auxz00_7087;
								BgL_objectz00_bglt BgL_tmpz00_7083;

								BgL_auxz00_7087 = ((obj_t) BgL_wide1404z00_4995);
								BgL_tmpz00_7083 =
									((BgL_objectz00_bglt)
									((BgL_returnz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4613)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7083, BgL_auxz00_7087);
							}
							((BgL_objectz00_bglt)
								((BgL_returnz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4613)));
							{	/* Liveness/liveness.scm 1005 */
								long BgL_arg2511z00_4996;

								BgL_arg2511z00_4996 =
									BGL_CLASS_NUM(BGl_returnzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_returnz00_bglt)
											((BgL_returnz00_bglt) BgL_nz00_4613))),
									BgL_arg2511z00_4996);
							}
							((BgL_returnz00_bglt)
								((BgL_returnz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4613)));
						}
						{
							BgL_returnzf2livenesszf2_bglt BgL_auxz00_7101;

							{
								obj_t BgL_auxz00_7102;

								{	/* Liveness/liveness.scm 1006 */
									BgL_objectz00_bglt BgL_tmpz00_7103;

									BgL_tmpz00_7103 =
										((BgL_objectz00_bglt)
										((BgL_returnz00_bglt)
											((BgL_returnz00_bglt) BgL_nz00_4613)));
									BgL_auxz00_7102 = BGL_OBJECT_WIDENING(BgL_tmpz00_7103);
								}
								BgL_auxz00_7101 =
									((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7102);
							}
							((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7101))->
									BgL_defz00) = ((obj_t) BgL_defz00_4991), BUNSPEC);
						}
						{
							BgL_returnzf2livenesszf2_bglt BgL_auxz00_7110;

							{
								obj_t BgL_auxz00_7111;

								{	/* Liveness/liveness.scm 1007 */
									BgL_objectz00_bglt BgL_tmpz00_7112;

									BgL_tmpz00_7112 =
										((BgL_objectz00_bglt)
										((BgL_returnz00_bglt)
											((BgL_returnz00_bglt) BgL_nz00_4613)));
									BgL_auxz00_7111 = BGL_OBJECT_WIDENING(BgL_tmpz00_7112);
								}
								BgL_auxz00_7110 =
									((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7111);
							}
							((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7110))->
									BgL_usez00) = ((obj_t) BgL_usez00_4992), BUNSPEC);
						}
						{
							BgL_returnzf2livenesszf2_bglt BgL_auxz00_7119;

							{
								obj_t BgL_auxz00_7120;

								{	/* Liveness/liveness.scm 1003 */
									BgL_objectz00_bglt BgL_tmpz00_7121;

									BgL_tmpz00_7121 =
										((BgL_objectz00_bglt)
										((BgL_returnz00_bglt)
											((BgL_returnz00_bglt) BgL_nz00_4613)));
									BgL_auxz00_7120 = BGL_OBJECT_WIDENING(BgL_tmpz00_7121);
								}
								BgL_auxz00_7119 =
									((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7120);
							}
							((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7119))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_returnzf2livenesszf2_bglt BgL_auxz00_7128;

							{
								obj_t BgL_auxz00_7129;

								{	/* Liveness/liveness.scm 1003 */
									BgL_objectz00_bglt BgL_tmpz00_7130;

									BgL_tmpz00_7130 =
										((BgL_objectz00_bglt)
										((BgL_returnz00_bglt)
											((BgL_returnz00_bglt) BgL_nz00_4613)));
									BgL_auxz00_7129 = BGL_OBJECT_WIDENING(BgL_tmpz00_7130);
								}
								BgL_auxz00_7128 =
									((BgL_returnzf2livenesszf2_bglt) BgL_auxz00_7129);
							}
							((((BgL_returnzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7128))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2510z00_4994 =
							((BgL_returnz00_bglt) ((BgL_returnz00_bglt) BgL_nz00_4613));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2510z00_4994));
					}
				}
			}
		}

	}



/* &inout-retblock/liven2074 */
	obj_t BGl_z62inoutzd2retblockzf2liven2074z42zzliveness_livenessz00(obj_t
		BgL_envz00_4614, obj_t BgL_nz00_4615)
	{
		{	/* Liveness/liveness.scm 993 */
			{	/* Liveness/liveness.scm 995 */
				obj_t BgL_val0_1800z00_4998;
				obj_t BgL_val1_1801z00_4999;

				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7141;

					{
						obj_t BgL_auxz00_7142;

						{	/* Liveness/liveness.scm 995 */
							BgL_objectz00_bglt BgL_tmpz00_7143;

							BgL_tmpz00_7143 =
								((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4615));
							BgL_auxz00_7142 = BGL_OBJECT_WIDENING(BgL_tmpz00_7143);
						}
						BgL_auxz00_7141 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7142);
					}
					BgL_val0_1800z00_4998 =
						(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7141))->
						BgL_inz00);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7149;

					{
						obj_t BgL_auxz00_7150;

						{	/* Liveness/liveness.scm 995 */
							BgL_objectz00_bglt BgL_tmpz00_7151;

							BgL_tmpz00_7151 =
								((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4615));
							BgL_auxz00_7150 = BGL_OBJECT_WIDENING(BgL_tmpz00_7151);
						}
						BgL_auxz00_7149 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7150);
					}
					BgL_val1_1801z00_4999 =
						(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7149))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 995 */
					int BgL_tmpz00_7157;

					BgL_tmpz00_7157 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7157);
				}
				{	/* Liveness/liveness.scm 995 */
					int BgL_tmpz00_7160;

					BgL_tmpz00_7160 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7160, BgL_val1_1801z00_4999);
				}
				return BgL_val0_1800z00_4998;
			}
		}

	}



/* &inout!-retblock/live2072 */
	obj_t BGl_z62inoutz12zd2retblockzf2live2072z50zzliveness_livenessz00(obj_t
		BgL_envz00_4616, obj_t BgL_nz00_4617, obj_t BgL_oz00_4618)
	{
		{	/* Liveness/liveness.scm 986 */
			{
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7163;

				{
					obj_t BgL_auxz00_7164;

					{	/* Liveness/liveness.scm 988 */
						BgL_objectz00_bglt BgL_tmpz00_7165;

						BgL_tmpz00_7165 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4617));
						BgL_auxz00_7164 = BGL_OBJECT_WIDENING(BgL_tmpz00_7165);
					}
					BgL_auxz00_7163 = ((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7164);
				}
				((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7163))->
						BgL_outz00) = ((obj_t) BgL_oz00_4618), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_7178;
				BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7171;

				{	/* Liveness/liveness.scm 989 */
					obj_t BgL_arg2503z00_5001;
					obj_t BgL_arg2505z00_5002;

					{
						BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7179;

						{
							obj_t BgL_auxz00_7180;

							{	/* Liveness/liveness.scm 989 */
								BgL_objectz00_bglt BgL_tmpz00_7181;

								BgL_tmpz00_7181 =
									((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4617));
								BgL_auxz00_7180 = BGL_OBJECT_WIDENING(BgL_tmpz00_7181);
							}
							BgL_auxz00_7179 =
								((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7180);
						}
						BgL_arg2503z00_5001 =
							(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7179))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 989 */
						obj_t BgL_arg2506z00_5003;
						obj_t BgL_arg2508z00_5004;

						{
							BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7187;

							{
								obj_t BgL_auxz00_7188;

								{	/* Liveness/liveness.scm 989 */
									BgL_objectz00_bglt BgL_tmpz00_7189;

									BgL_tmpz00_7189 =
										((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt) BgL_nz00_4617));
									BgL_auxz00_7188 = BGL_OBJECT_WIDENING(BgL_tmpz00_7189);
								}
								BgL_auxz00_7187 =
									((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7188);
							}
							BgL_arg2506z00_5003 =
								(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7187))->
								BgL_outz00);
						}
						{
							BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7195;

							{
								obj_t BgL_auxz00_7196;

								{	/* Liveness/liveness.scm 989 */
									BgL_objectz00_bglt BgL_tmpz00_7197;

									BgL_tmpz00_7197 =
										((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt) BgL_nz00_4617));
									BgL_auxz00_7196 = BGL_OBJECT_WIDENING(BgL_tmpz00_7197);
								}
								BgL_auxz00_7195 =
									((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7196);
							}
							BgL_arg2508z00_5004 =
								(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7195))->
								BgL_defz00);
						}
						BgL_arg2505z00_5002 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2506z00_5003,
							BgL_arg2508z00_5004);
					}
					BgL_auxz00_7178 =
						BGl_unionz00zzliveness_setz00(BgL_arg2503z00_5001,
						BgL_arg2505z00_5002);
				}
				{
					obj_t BgL_auxz00_7172;

					{	/* Liveness/liveness.scm 989 */
						BgL_objectz00_bglt BgL_tmpz00_7173;

						BgL_tmpz00_7173 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4617));
						BgL_auxz00_7172 = BGL_OBJECT_WIDENING(BgL_tmpz00_7173);
					}
					BgL_auxz00_7171 = ((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7172);
				}
				((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7171))->
						BgL_inz00) = ((obj_t) BgL_auxz00_7178), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 990 */
				BgL_nodez00_bglt BgL_arg2509z00_5005;

				BgL_arg2509z00_5005 =
					(((BgL_retblockz00_bglt) COBJECT(
							((BgL_retblockz00_bglt)
								((BgL_retblockz00_bglt) BgL_nz00_4617))))->BgL_bodyz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2509z00_5005,
					BgL_oz00_4618);
			}
			{	/* Liveness/liveness.scm 991 */
				obj_t BgL_val0_1798z00_5006;
				obj_t BgL_val1_1799z00_5007;

				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7210;

					{
						obj_t BgL_auxz00_7211;

						{	/* Liveness/liveness.scm 991 */
							BgL_objectz00_bglt BgL_tmpz00_7212;

							BgL_tmpz00_7212 =
								((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4617));
							BgL_auxz00_7211 = BGL_OBJECT_WIDENING(BgL_tmpz00_7212);
						}
						BgL_auxz00_7210 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7211);
					}
					BgL_val0_1798z00_5006 =
						(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7210))->
						BgL_inz00);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7218;

					{
						obj_t BgL_auxz00_7219;

						{	/* Liveness/liveness.scm 991 */
							BgL_objectz00_bglt BgL_tmpz00_7220;

							BgL_tmpz00_7220 =
								((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4617));
							BgL_auxz00_7219 = BGL_OBJECT_WIDENING(BgL_tmpz00_7220);
						}
						BgL_auxz00_7218 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7219);
					}
					BgL_val1_1799z00_5007 =
						(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7218))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 991 */
					int BgL_tmpz00_7226;

					BgL_tmpz00_7226 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7226);
				}
				{	/* Liveness/liveness.scm 991 */
					int BgL_tmpz00_7229;

					BgL_tmpz00_7229 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7229, BgL_val1_1799z00_5007);
				}
				return BgL_val0_1798z00_5006;
			}
		}

	}



/* &defuse-retblock/live2070 */
	obj_t BGl_z62defusezd2retblockzf2live2070z42zzliveness_livenessz00(obj_t
		BgL_envz00_4619, obj_t BgL_nz00_4620)
	{
		{	/* Liveness/liveness.scm 982 */
			{	/* Liveness/liveness.scm 984 */
				obj_t BgL_val0_1796z00_5009;
				obj_t BgL_val1_1797z00_5010;

				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7232;

					{
						obj_t BgL_auxz00_7233;

						{	/* Liveness/liveness.scm 984 */
							BgL_objectz00_bglt BgL_tmpz00_7234;

							BgL_tmpz00_7234 =
								((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4620));
							BgL_auxz00_7233 = BGL_OBJECT_WIDENING(BgL_tmpz00_7234);
						}
						BgL_auxz00_7232 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7233);
					}
					BgL_val0_1796z00_5009 =
						(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7232))->
						BgL_defz00);
				}
				{
					BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7240;

					{
						obj_t BgL_auxz00_7241;

						{	/* Liveness/liveness.scm 984 */
							BgL_objectz00_bglt BgL_tmpz00_7242;

							BgL_tmpz00_7242 =
								((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4620));
							BgL_auxz00_7241 = BGL_OBJECT_WIDENING(BgL_tmpz00_7242);
						}
						BgL_auxz00_7240 =
							((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7241);
					}
					BgL_val1_1797z00_5010 =
						(((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7240))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 984 */
					int BgL_tmpz00_7248;

					BgL_tmpz00_7248 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7248);
				}
				{	/* Liveness/liveness.scm 984 */
					int BgL_tmpz00_7251;

					BgL_tmpz00_7251 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7251, BgL_val1_1797z00_5010);
				}
				return BgL_val0_1796z00_5009;
			}
		}

	}



/* &defuse-retblock2068 */
	obj_t BGl_z62defusezd2retblock2068zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4621, obj_t BgL_nz00_4622)
	{
		{	/* Liveness/liveness.scm 973 */
			{	/* Liveness/liveness.scm 975 */
				obj_t BgL_defz00_5012;

				BgL_defz00_5012 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nz00_4622)))->BgL_bodyz00));
				{	/* Liveness/liveness.scm 976 */
					obj_t BgL_usez00_5013;

					{	/* Liveness/liveness.scm 977 */
						obj_t BgL_tmpz00_5014;

						{	/* Liveness/liveness.scm 977 */
							int BgL_tmpz00_7257;

							BgL_tmpz00_7257 = (int) (1L);
							BgL_tmpz00_5014 = BGL_MVALUES_VAL(BgL_tmpz00_7257);
						}
						{	/* Liveness/liveness.scm 977 */
							int BgL_tmpz00_7260;

							BgL_tmpz00_7260 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7260, BUNSPEC);
						}
						BgL_usez00_5013 = BgL_tmpz00_5014;
					}
					{	/* Liveness/liveness.scm 978 */
						BgL_retblockz00_bglt BgL_arg2500z00_5015;

						{	/* Liveness/liveness.scm 978 */
							BgL_retblockzf2livenesszf2_bglt BgL_wide1396z00_5016;

							BgL_wide1396z00_5016 =
								((BgL_retblockzf2livenesszf2_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_retblockzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 978 */
								obj_t BgL_auxz00_7268;
								BgL_objectz00_bglt BgL_tmpz00_7264;

								BgL_auxz00_7268 = ((obj_t) BgL_wide1396z00_5016);
								BgL_tmpz00_7264 =
									((BgL_objectz00_bglt)
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_nz00_4622)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7264, BgL_auxz00_7268);
							}
							((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt)
									((BgL_retblockz00_bglt) BgL_nz00_4622)));
							{	/* Liveness/liveness.scm 978 */
								long BgL_arg2501z00_5017;

								BgL_arg2501z00_5017 =
									BGL_CLASS_NUM(BGl_retblockzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt)
											((BgL_retblockz00_bglt) BgL_nz00_4622))),
									BgL_arg2501z00_5017);
							}
							((BgL_retblockz00_bglt)
								((BgL_retblockz00_bglt)
									((BgL_retblockz00_bglt) BgL_nz00_4622)));
						}
						{
							BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7282;

							{
								obj_t BgL_auxz00_7283;

								{	/* Liveness/liveness.scm 979 */
									BgL_objectz00_bglt BgL_tmpz00_7284;

									BgL_tmpz00_7284 =
										((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt)
											((BgL_retblockz00_bglt) BgL_nz00_4622)));
									BgL_auxz00_7283 = BGL_OBJECT_WIDENING(BgL_tmpz00_7284);
								}
								BgL_auxz00_7282 =
									((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7283);
							}
							((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7282))->
									BgL_defz00) = ((obj_t) BgL_defz00_5012), BUNSPEC);
						}
						{
							BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7291;

							{
								obj_t BgL_auxz00_7292;

								{	/* Liveness/liveness.scm 980 */
									BgL_objectz00_bglt BgL_tmpz00_7293;

									BgL_tmpz00_7293 =
										((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt)
											((BgL_retblockz00_bglt) BgL_nz00_4622)));
									BgL_auxz00_7292 = BGL_OBJECT_WIDENING(BgL_tmpz00_7293);
								}
								BgL_auxz00_7291 =
									((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7292);
							}
							((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7291))->
									BgL_usez00) = ((obj_t) BgL_usez00_5013), BUNSPEC);
						}
						{
							BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7300;

							{
								obj_t BgL_auxz00_7301;

								{	/* Liveness/liveness.scm 976 */
									BgL_objectz00_bglt BgL_tmpz00_7302;

									BgL_tmpz00_7302 =
										((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt)
											((BgL_retblockz00_bglt) BgL_nz00_4622)));
									BgL_auxz00_7301 = BGL_OBJECT_WIDENING(BgL_tmpz00_7302);
								}
								BgL_auxz00_7300 =
									((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7301);
							}
							((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7300))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_retblockzf2livenesszf2_bglt BgL_auxz00_7309;

							{
								obj_t BgL_auxz00_7310;

								{	/* Liveness/liveness.scm 976 */
									BgL_objectz00_bglt BgL_tmpz00_7311;

									BgL_tmpz00_7311 =
										((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt)
											((BgL_retblockz00_bglt) BgL_nz00_4622)));
									BgL_auxz00_7310 = BGL_OBJECT_WIDENING(BgL_tmpz00_7311);
								}
								BgL_auxz00_7309 =
									((BgL_retblockzf2livenesszf2_bglt) BgL_auxz00_7310);
							}
							((((BgL_retblockzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7309))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2500z00_5015 =
							((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_4622));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2500z00_5015));
					}
				}
			}
		}

	}



/* &inout-jump-ex-it/liv2066 */
	obj_t BGl_z62inoutzd2jumpzd2exzd2itzf2liv2066z42zzliveness_livenessz00(obj_t
		BgL_envz00_4623, obj_t BgL_nz00_4624)
	{
		{	/* Liveness/liveness.scm 966 */
			{	/* Liveness/liveness.scm 968 */
				obj_t BgL_val0_1794z00_5019;
				obj_t BgL_val1_1795z00_5020;

				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7322;

					{
						obj_t BgL_auxz00_7323;

						{	/* Liveness/liveness.scm 968 */
							BgL_objectz00_bglt BgL_tmpz00_7324;

							BgL_tmpz00_7324 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4624));
							BgL_auxz00_7323 = BGL_OBJECT_WIDENING(BgL_tmpz00_7324);
						}
						BgL_auxz00_7322 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7323);
					}
					BgL_val0_1794z00_5019 =
						(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_7322))->BgL_inz00);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7330;

					{
						obj_t BgL_auxz00_7331;

						{	/* Liveness/liveness.scm 968 */
							BgL_objectz00_bglt BgL_tmpz00_7332;

							BgL_tmpz00_7332 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4624));
							BgL_auxz00_7331 = BGL_OBJECT_WIDENING(BgL_tmpz00_7332);
						}
						BgL_auxz00_7330 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7331);
					}
					BgL_val1_1795z00_5020 =
						(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_7330))->BgL_outz00);
				}
				{	/* Liveness/liveness.scm 968 */
					int BgL_tmpz00_7338;

					BgL_tmpz00_7338 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7338);
				}
				{	/* Liveness/liveness.scm 968 */
					int BgL_tmpz00_7341;

					BgL_tmpz00_7341 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7341, BgL_val1_1795z00_5020);
				}
				return BgL_val0_1794z00_5019;
			}
		}

	}



/* &inout!-jump-ex-it/li2064 */
	obj_t BGl_z62inoutz12zd2jumpzd2exzd2itzf2li2064z50zzliveness_livenessz00(obj_t
		BgL_envz00_4625, obj_t BgL_nz00_4626, obj_t BgL_oz00_4627)
	{
		{	/* Liveness/liveness.scm 958 */
			{
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7344;

				{
					obj_t BgL_auxz00_7345;

					{	/* Liveness/liveness.scm 961 */
						BgL_objectz00_bglt BgL_tmpz00_7346;

						BgL_tmpz00_7346 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4626));
						BgL_auxz00_7345 = BGL_OBJECT_WIDENING(BgL_tmpz00_7346);
					}
					BgL_auxz00_7344 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7345);
				}
				((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7344))->
						BgL_outz00) = ((obj_t) BgL_oz00_4627), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_7359;
				BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7352;

				{	/* Liveness/liveness.scm 962 */
					obj_t BgL_arg2491z00_5022;
					obj_t BgL_arg2492z00_5023;

					{
						BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7360;

						{
							obj_t BgL_auxz00_7361;

							{	/* Liveness/liveness.scm 962 */
								BgL_objectz00_bglt BgL_tmpz00_7362;

								BgL_tmpz00_7362 =
									((BgL_objectz00_bglt)
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4626));
								BgL_auxz00_7361 = BGL_OBJECT_WIDENING(BgL_tmpz00_7362);
							}
							BgL_auxz00_7360 =
								((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7361);
						}
						BgL_arg2491z00_5022 =
							(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_7360))->BgL_usez00);
					}
					{	/* Liveness/liveness.scm 962 */
						obj_t BgL_arg2493z00_5024;
						obj_t BgL_arg2495z00_5025;

						{
							BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7368;

							{
								obj_t BgL_auxz00_7369;

								{	/* Liveness/liveness.scm 962 */
									BgL_objectz00_bglt BgL_tmpz00_7370;

									BgL_tmpz00_7370 =
										((BgL_objectz00_bglt)
										((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4626));
									BgL_auxz00_7369 = BGL_OBJECT_WIDENING(BgL_tmpz00_7370);
								}
								BgL_auxz00_7368 =
									((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7369);
							}
							BgL_arg2493z00_5024 =
								(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
									COBJECT(BgL_auxz00_7368))->BgL_outz00);
						}
						{
							BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7376;

							{
								obj_t BgL_auxz00_7377;

								{	/* Liveness/liveness.scm 962 */
									BgL_objectz00_bglt BgL_tmpz00_7378;

									BgL_tmpz00_7378 =
										((BgL_objectz00_bglt)
										((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4626));
									BgL_auxz00_7377 = BGL_OBJECT_WIDENING(BgL_tmpz00_7378);
								}
								BgL_auxz00_7376 =
									((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7377);
							}
							BgL_arg2495z00_5025 =
								(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
									COBJECT(BgL_auxz00_7376))->BgL_defz00);
						}
						BgL_arg2492z00_5023 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2493z00_5024,
							BgL_arg2495z00_5025);
					}
					BgL_auxz00_7359 =
						BGl_unionz00zzliveness_setz00(BgL_arg2491z00_5022,
						BgL_arg2492z00_5023);
				}
				{
					obj_t BgL_auxz00_7353;

					{	/* Liveness/liveness.scm 962 */
						BgL_objectz00_bglt BgL_tmpz00_7354;

						BgL_tmpz00_7354 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4626));
						BgL_auxz00_7353 = BGL_OBJECT_WIDENING(BgL_tmpz00_7354);
					}
					BgL_auxz00_7352 =
						((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7353);
				}
				((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7352))->
						BgL_inz00) = ((obj_t) BgL_auxz00_7359), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 963 */
				BgL_nodez00_bglt BgL_arg2497z00_5026;

				BgL_arg2497z00_5026 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4626))))->BgL_valuez00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2497z00_5026,
					BgL_oz00_4627);
			}
			{	/* Liveness/liveness.scm 964 */
				obj_t BgL_val0_1792z00_5027;
				obj_t BgL_val1_1793z00_5028;

				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7391;

					{
						obj_t BgL_auxz00_7392;

						{	/* Liveness/liveness.scm 964 */
							BgL_objectz00_bglt BgL_tmpz00_7393;

							BgL_tmpz00_7393 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4626));
							BgL_auxz00_7392 = BGL_OBJECT_WIDENING(BgL_tmpz00_7393);
						}
						BgL_auxz00_7391 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7392);
					}
					BgL_val0_1792z00_5027 =
						(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_7391))->BgL_inz00);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7399;

					{
						obj_t BgL_auxz00_7400;

						{	/* Liveness/liveness.scm 964 */
							BgL_objectz00_bglt BgL_tmpz00_7401;

							BgL_tmpz00_7401 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4626));
							BgL_auxz00_7400 = BGL_OBJECT_WIDENING(BgL_tmpz00_7401);
						}
						BgL_auxz00_7399 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7400);
					}
					BgL_val1_1793z00_5028 =
						(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_7399))->BgL_outz00);
				}
				{	/* Liveness/liveness.scm 964 */
					int BgL_tmpz00_7407;

					BgL_tmpz00_7407 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7407);
				}
				{	/* Liveness/liveness.scm 964 */
					int BgL_tmpz00_7410;

					BgL_tmpz00_7410 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7410, BgL_val1_1793z00_5028);
				}
				return BgL_val0_1792z00_5027;
			}
		}

	}



/* &defuse-jump-ex-it/li2062 */
	obj_t BGl_z62defusezd2jumpzd2exzd2itzf2li2062z42zzliveness_livenessz00(obj_t
		BgL_envz00_4628, obj_t BgL_nz00_4629)
	{
		{	/* Liveness/liveness.scm 954 */
			{	/* Liveness/liveness.scm 956 */
				obj_t BgL_val0_1790z00_5030;
				obj_t BgL_val1_1791z00_5031;

				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7413;

					{
						obj_t BgL_auxz00_7414;

						{	/* Liveness/liveness.scm 956 */
							BgL_objectz00_bglt BgL_tmpz00_7415;

							BgL_tmpz00_7415 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4629));
							BgL_auxz00_7414 = BGL_OBJECT_WIDENING(BgL_tmpz00_7415);
						}
						BgL_auxz00_7413 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7414);
					}
					BgL_val0_1790z00_5030 =
						(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_7413))->BgL_defz00);
				}
				{
					BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7421;

					{
						obj_t BgL_auxz00_7422;

						{	/* Liveness/liveness.scm 956 */
							BgL_objectz00_bglt BgL_tmpz00_7423;

							BgL_tmpz00_7423 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4629));
							BgL_auxz00_7422 = BGL_OBJECT_WIDENING(BgL_tmpz00_7423);
						}
						BgL_auxz00_7421 =
							((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7422);
					}
					BgL_val1_1791z00_5031 =
						(((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
							COBJECT(BgL_auxz00_7421))->BgL_usez00);
				}
				{	/* Liveness/liveness.scm 956 */
					int BgL_tmpz00_7429;

					BgL_tmpz00_7429 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7429);
				}
				{	/* Liveness/liveness.scm 956 */
					int BgL_tmpz00_7432;

					BgL_tmpz00_7432 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7432, BgL_val1_1791z00_5031);
				}
				return BgL_val0_1790z00_5030;
			}
		}

	}



/* &defuse-jump-ex-it2060 */
	obj_t BGl_z62defusezd2jumpzd2exzd2it2060zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4630, obj_t BgL_nz00_4631)
	{
		{	/* Liveness/liveness.scm 943 */
			{	/* Liveness/liveness.scm 945 */
				obj_t BgL_defexitz00_5033;

				BgL_defexitz00_5033 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631)))->BgL_exitz00));
				{	/* Liveness/liveness.scm 946 */
					obj_t BgL_useexitz00_5034;

					{	/* Liveness/liveness.scm 947 */
						obj_t BgL_tmpz00_5035;

						{	/* Liveness/liveness.scm 947 */
							int BgL_tmpz00_7438;

							BgL_tmpz00_7438 = (int) (1L);
							BgL_tmpz00_5035 = BGL_MVALUES_VAL(BgL_tmpz00_7438);
						}
						{	/* Liveness/liveness.scm 947 */
							int BgL_tmpz00_7441;

							BgL_tmpz00_7441 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7441, BUNSPEC);
						}
						BgL_useexitz00_5034 = BgL_tmpz00_5035;
					}
					{	/* Liveness/liveness.scm 947 */
						obj_t BgL_defvaluez00_5036;

						BgL_defvaluez00_5036 =
							BGl_defusez00zzliveness_livenessz00(
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
										((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631)))->
								BgL_valuez00));
						{	/* Liveness/liveness.scm 948 */
							obj_t BgL_usevaluez00_5037;

							{	/* Liveness/liveness.scm 949 */
								obj_t BgL_tmpz00_5038;

								{	/* Liveness/liveness.scm 949 */
									int BgL_tmpz00_7447;

									BgL_tmpz00_7447 = (int) (1L);
									BgL_tmpz00_5038 = BGL_MVALUES_VAL(BgL_tmpz00_7447);
								}
								{	/* Liveness/liveness.scm 949 */
									int BgL_tmpz00_7450;

									BgL_tmpz00_7450 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_7450, BUNSPEC);
								}
								BgL_usevaluez00_5037 = BgL_tmpz00_5038;
							}
							{	/* Liveness/liveness.scm 950 */
								BgL_jumpzd2exzd2itz00_bglt BgL_arg2486z00_5039;

								{	/* Liveness/liveness.scm 950 */
									BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_wide1388z00_5040;

									BgL_wide1388z00_5040 =
										((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_jumpzd2exzd2itzf2livenesszf2_bgl))));
									{	/* Liveness/liveness.scm 950 */
										obj_t BgL_auxz00_7458;
										BgL_objectz00_bglt BgL_tmpz00_7454;

										BgL_auxz00_7458 = ((obj_t) BgL_wide1388z00_5040);
										BgL_tmpz00_7454 =
											((BgL_objectz00_bglt)
											((BgL_jumpzd2exzd2itz00_bglt)
												((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7454, BgL_auxz00_7458);
									}
									((BgL_objectz00_bglt)
										((BgL_jumpzd2exzd2itz00_bglt)
											((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631)));
									{	/* Liveness/liveness.scm 950 */
										long BgL_arg2487z00_5041;

										BgL_arg2487z00_5041 =
											BGL_CLASS_NUM
											(BGl_jumpzd2exzd2itzf2livenesszf2zzliveness_typesz00);
										BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
													(BgL_jumpzd2exzd2itz00_bglt) (
														(BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631))),
											BgL_arg2487z00_5041);
									}
									((BgL_jumpzd2exzd2itz00_bglt)
										((BgL_jumpzd2exzd2itz00_bglt)
											((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631)));
								}
								{
									BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7472;

									{
										obj_t BgL_auxz00_7473;

										{	/* Liveness/liveness.scm 951 */
											BgL_objectz00_bglt BgL_tmpz00_7474;

											BgL_tmpz00_7474 =
												((BgL_objectz00_bglt)
												((BgL_jumpzd2exzd2itz00_bglt)
													((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631)));
											BgL_auxz00_7473 = BGL_OBJECT_WIDENING(BgL_tmpz00_7474);
										}
										BgL_auxz00_7472 =
											((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7473);
									}
									((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_7472))->BgL_defz00) =
										((obj_t) BGl_unionz00zzliveness_setz00(BgL_defexitz00_5033,
												BgL_defvaluez00_5036)), BUNSPEC);
								}
								{
									BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7482;

									{
										obj_t BgL_auxz00_7483;

										{	/* Liveness/liveness.scm 952 */
											BgL_objectz00_bglt BgL_tmpz00_7484;

											BgL_tmpz00_7484 =
												((BgL_objectz00_bglt)
												((BgL_jumpzd2exzd2itz00_bglt)
													((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631)));
											BgL_auxz00_7483 = BGL_OBJECT_WIDENING(BgL_tmpz00_7484);
										}
										BgL_auxz00_7482 =
											((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7483);
									}
									((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_7482))->BgL_usez00) =
										((obj_t) BGl_unionz00zzliveness_setz00(BgL_useexitz00_5034,
												BgL_usevaluez00_5037)), BUNSPEC);
								}
								{
									BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7492;

									{
										obj_t BgL_auxz00_7493;

										{	/* Liveness/liveness.scm 948 */
											BgL_objectz00_bglt BgL_tmpz00_7494;

											BgL_tmpz00_7494 =
												((BgL_objectz00_bglt)
												((BgL_jumpzd2exzd2itz00_bglt)
													((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631)));
											BgL_auxz00_7493 = BGL_OBJECT_WIDENING(BgL_tmpz00_7494);
										}
										BgL_auxz00_7492 =
											((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7493);
									}
									((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_7492))->BgL_inz00) =
										((obj_t) BNIL), BUNSPEC);
								}
								{
									BgL_jumpzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7501;

									{
										obj_t BgL_auxz00_7502;

										{	/* Liveness/liveness.scm 948 */
											BgL_objectz00_bglt BgL_tmpz00_7503;

											BgL_tmpz00_7503 =
												((BgL_objectz00_bglt)
												((BgL_jumpzd2exzd2itz00_bglt)
													((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631)));
											BgL_auxz00_7502 = BGL_OBJECT_WIDENING(BgL_tmpz00_7503);
										}
										BgL_auxz00_7501 =
											((BgL_jumpzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7502);
									}
									((((BgL_jumpzd2exzd2itzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_7501))->BgL_outz00) =
										((obj_t) BNIL), BUNSPEC);
								}
								BgL_arg2486z00_5039 =
									((BgL_jumpzd2exzd2itz00_bglt)
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_4631));
								return
									BGl_defusez00zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_arg2486z00_5039));
							}
						}
					}
				}
			}
		}

	}



/* &inout-set-ex-it/live2058 */
	obj_t BGl_z62inoutzd2setzd2exzd2itzf2live2058z42zzliveness_livenessz00(obj_t
		BgL_envz00_4632, obj_t BgL_nz00_4633)
	{
		{	/* Liveness/liveness.scm 936 */
			{	/* Liveness/liveness.scm 938 */
				obj_t BgL_val0_1788z00_5043;
				obj_t BgL_val1_1789z00_5044;

				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7514;

					{
						obj_t BgL_auxz00_7515;

						{	/* Liveness/liveness.scm 938 */
							BgL_objectz00_bglt BgL_tmpz00_7516;

							BgL_tmpz00_7516 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4633));
							BgL_auxz00_7515 = BGL_OBJECT_WIDENING(BgL_tmpz00_7516);
						}
						BgL_auxz00_7514 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7515);
					}
					BgL_val0_1788z00_5043 =
						(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7514))->
						BgL_inz00);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7522;

					{
						obj_t BgL_auxz00_7523;

						{	/* Liveness/liveness.scm 938 */
							BgL_objectz00_bglt BgL_tmpz00_7524;

							BgL_tmpz00_7524 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4633));
							BgL_auxz00_7523 = BGL_OBJECT_WIDENING(BgL_tmpz00_7524);
						}
						BgL_auxz00_7522 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7523);
					}
					BgL_val1_1789z00_5044 =
						(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7522))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 938 */
					int BgL_tmpz00_7530;

					BgL_tmpz00_7530 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7530);
				}
				{	/* Liveness/liveness.scm 938 */
					int BgL_tmpz00_7533;

					BgL_tmpz00_7533 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7533, BgL_val1_1789z00_5044);
				}
				return BgL_val0_1788z00_5043;
			}
		}

	}



/* &inout!-set-ex-it/liv2055 */
	obj_t BGl_z62inoutz12zd2setzd2exzd2itzf2liv2055z50zzliveness_livenessz00(obj_t
		BgL_envz00_4634, obj_t BgL_nz00_4635, obj_t BgL_oz00_4636)
	{
		{	/* Liveness/liveness.scm 928 */
			{
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7536;

				{
					obj_t BgL_auxz00_7537;

					{	/* Liveness/liveness.scm 931 */
						BgL_objectz00_bglt BgL_tmpz00_7538;

						BgL_tmpz00_7538 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4635));
						BgL_auxz00_7537 = BGL_OBJECT_WIDENING(BgL_tmpz00_7538);
					}
					BgL_auxz00_7536 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7537);
				}
				((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7536))->
						BgL_outz00) = ((obj_t) BgL_oz00_4636), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_7551;
				BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7544;

				{	/* Liveness/liveness.scm 932 */
					obj_t BgL_arg2480z00_5046;
					obj_t BgL_arg2481z00_5047;

					{
						BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7552;

						{
							obj_t BgL_auxz00_7553;

							{	/* Liveness/liveness.scm 932 */
								BgL_objectz00_bglt BgL_tmpz00_7554;

								BgL_tmpz00_7554 =
									((BgL_objectz00_bglt)
									((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4635));
								BgL_auxz00_7553 = BGL_OBJECT_WIDENING(BgL_tmpz00_7554);
							}
							BgL_auxz00_7552 =
								((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7553);
						}
						BgL_arg2480z00_5046 =
							(((BgL_setzd2exzd2itzf2livenesszf2_bglt)
								COBJECT(BgL_auxz00_7552))->BgL_usez00);
					}
					{	/* Liveness/liveness.scm 932 */
						obj_t BgL_arg2482z00_5048;
						obj_t BgL_arg2483z00_5049;

						{
							BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7560;

							{
								obj_t BgL_auxz00_7561;

								{	/* Liveness/liveness.scm 932 */
									BgL_objectz00_bglt BgL_tmpz00_7562;

									BgL_tmpz00_7562 =
										((BgL_objectz00_bglt)
										((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4635));
									BgL_auxz00_7561 = BGL_OBJECT_WIDENING(BgL_tmpz00_7562);
								}
								BgL_auxz00_7560 =
									((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7561);
							}
							BgL_arg2482z00_5048 =
								(((BgL_setzd2exzd2itzf2livenesszf2_bglt)
									COBJECT(BgL_auxz00_7560))->BgL_outz00);
						}
						{
							BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7568;

							{
								obj_t BgL_auxz00_7569;

								{	/* Liveness/liveness.scm 932 */
									BgL_objectz00_bglt BgL_tmpz00_7570;

									BgL_tmpz00_7570 =
										((BgL_objectz00_bglt)
										((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4635));
									BgL_auxz00_7569 = BGL_OBJECT_WIDENING(BgL_tmpz00_7570);
								}
								BgL_auxz00_7568 =
									((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7569);
							}
							BgL_arg2483z00_5049 =
								(((BgL_setzd2exzd2itzf2livenesszf2_bglt)
									COBJECT(BgL_auxz00_7568))->BgL_defz00);
						}
						BgL_arg2481z00_5047 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2482z00_5048,
							BgL_arg2483z00_5049);
					}
					BgL_auxz00_7551 =
						BGl_unionz00zzliveness_setz00(BgL_arg2480z00_5046,
						BgL_arg2481z00_5047);
				}
				{
					obj_t BgL_auxz00_7545;

					{	/* Liveness/liveness.scm 932 */
						BgL_objectz00_bglt BgL_tmpz00_7546;

						BgL_tmpz00_7546 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4635));
						BgL_auxz00_7545 = BGL_OBJECT_WIDENING(BgL_tmpz00_7546);
					}
					BgL_auxz00_7544 =
						((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7545);
				}
				((((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7544))->
						BgL_inz00) = ((obj_t) BgL_auxz00_7551), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 933 */
				BgL_nodez00_bglt BgL_arg2484z00_5050;

				BgL_arg2484z00_5050 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4635))))->BgL_bodyz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2484z00_5050,
					BgL_oz00_4636);
			}
			{	/* Liveness/liveness.scm 934 */
				obj_t BgL_val0_1786z00_5051;
				obj_t BgL_val1_1787z00_5052;

				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7583;

					{
						obj_t BgL_auxz00_7584;

						{	/* Liveness/liveness.scm 934 */
							BgL_objectz00_bglt BgL_tmpz00_7585;

							BgL_tmpz00_7585 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4635));
							BgL_auxz00_7584 = BGL_OBJECT_WIDENING(BgL_tmpz00_7585);
						}
						BgL_auxz00_7583 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7584);
					}
					BgL_val0_1786z00_5051 =
						(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7583))->
						BgL_inz00);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7591;

					{
						obj_t BgL_auxz00_7592;

						{	/* Liveness/liveness.scm 934 */
							BgL_objectz00_bglt BgL_tmpz00_7593;

							BgL_tmpz00_7593 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4635));
							BgL_auxz00_7592 = BGL_OBJECT_WIDENING(BgL_tmpz00_7593);
						}
						BgL_auxz00_7591 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7592);
					}
					BgL_val1_1787z00_5052 =
						(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7591))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 934 */
					int BgL_tmpz00_7599;

					BgL_tmpz00_7599 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7599);
				}
				{	/* Liveness/liveness.scm 934 */
					int BgL_tmpz00_7602;

					BgL_tmpz00_7602 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7602, BgL_val1_1787z00_5052);
				}
				return BgL_val0_1786z00_5051;
			}
		}

	}



/* &defuse-set-ex-it/liv2053 */
	obj_t BGl_z62defusezd2setzd2exzd2itzf2liv2053z42zzliveness_livenessz00(obj_t
		BgL_envz00_4637, obj_t BgL_nz00_4638)
	{
		{	/* Liveness/liveness.scm 924 */
			{	/* Liveness/liveness.scm 926 */
				obj_t BgL_val0_1784z00_5054;
				obj_t BgL_val1_1785z00_5055;

				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7605;

					{
						obj_t BgL_auxz00_7606;

						{	/* Liveness/liveness.scm 926 */
							BgL_objectz00_bglt BgL_tmpz00_7607;

							BgL_tmpz00_7607 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4638));
							BgL_auxz00_7606 = BGL_OBJECT_WIDENING(BgL_tmpz00_7607);
						}
						BgL_auxz00_7605 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7606);
					}
					BgL_val0_1784z00_5054 =
						(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7605))->
						BgL_defz00);
				}
				{
					BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7613;

					{
						obj_t BgL_auxz00_7614;

						{	/* Liveness/liveness.scm 926 */
							BgL_objectz00_bglt BgL_tmpz00_7615;

							BgL_tmpz00_7615 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4638));
							BgL_auxz00_7614 = BGL_OBJECT_WIDENING(BgL_tmpz00_7615);
						}
						BgL_auxz00_7613 =
							((BgL_setzd2exzd2itzf2livenesszf2_bglt) BgL_auxz00_7614);
					}
					BgL_val1_1785z00_5055 =
						(((BgL_setzd2exzd2itzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7613))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 926 */
					int BgL_tmpz00_7621;

					BgL_tmpz00_7621 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7621);
				}
				{	/* Liveness/liveness.scm 926 */
					int BgL_tmpz00_7624;

					BgL_tmpz00_7624 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7624, BgL_val1_1785z00_5055);
				}
				return BgL_val0_1784z00_5054;
			}
		}

	}



/* &defuse-set-ex-it2051 */
	obj_t BGl_z62defusezd2setzd2exzd2it2051zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4639, obj_t BgL_nz00_4640)
	{
		{	/* Liveness/liveness.scm 909 */
			{	/* Liveness/liveness.scm 911 */
				BgL_varz00_bglt BgL_i1373z00_5057;

				BgL_i1373z00_5057 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)))->BgL_varz00);
				{	/* Liveness/liveness.scm 912 */
					BgL_localz00_bglt BgL_tmp1374z00_5058;

					BgL_tmp1374z00_5058 =
						((BgL_localz00_bglt)
						(((BgL_varz00_bglt) COBJECT(BgL_i1373z00_5057))->BgL_variablez00));
					{	/* Liveness/liveness.scm 912 */
						BgL_localzf2livenesszf2_bglt BgL_wide1376z00_5059;

						BgL_wide1376z00_5059 =
							((BgL_localzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2livenesszf2_bgl))));
						{	/* Liveness/liveness.scm 912 */
							obj_t BgL_auxz00_7635;
							BgL_objectz00_bglt BgL_tmpz00_7632;

							BgL_auxz00_7635 = ((obj_t) BgL_wide1376z00_5059);
							BgL_tmpz00_7632 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_tmp1374z00_5058));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7632, BgL_auxz00_7635);
						}
						((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_tmp1374z00_5058));
						{	/* Liveness/liveness.scm 912 */
							long BgL_arg2466z00_5060;

							BgL_arg2466z00_5060 =
								BGL_CLASS_NUM(BGl_localzf2livenesszf2zzliveness_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_localz00_bglt) BgL_tmp1374z00_5058)),
								BgL_arg2466z00_5060);
						}
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_tmp1374z00_5058));
					}
					{
						BgL_localzf2livenesszf2_bglt BgL_auxz00_7646;

						{
							obj_t BgL_auxz00_7647;

							{	/* Liveness/liveness.scm 912 */
								BgL_objectz00_bglt BgL_tmpz00_7648;

								BgL_tmpz00_7648 =
									((BgL_objectz00_bglt)
									((BgL_localz00_bglt) BgL_tmp1374z00_5058));
								BgL_auxz00_7647 = BGL_OBJECT_WIDENING(BgL_tmpz00_7648);
							}
							BgL_auxz00_7646 =
								((BgL_localzf2livenesszf2_bglt) BgL_auxz00_7647);
						}
						((((BgL_localzf2livenesszf2_bglt) COBJECT(BgL_auxz00_7646))->
								BgL_z52countz52) = ((int) (int) (0L)), BUNSPEC);
					}
					((BgL_localz00_bglt) BgL_tmp1374z00_5058);
			}}
			{	/* Liveness/liveness.scm 913 */
				obj_t BgL_defvarz00_5061;

				{	/* Liveness/liveness.scm 914 */
					BgL_varz00_bglt BgL_arg2479z00_5062;

					BgL_arg2479z00_5062 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)))->BgL_varz00);
					BgL_defvarz00_5061 =
						BGl_defusez00zzliveness_livenessz00(
						((BgL_nodez00_bglt) BgL_arg2479z00_5062));
				}
				{	/* Liveness/liveness.scm 914 */
					obj_t BgL_usevarz00_5063;

					{	/* Liveness/liveness.scm 915 */
						obj_t BgL_tmpz00_5064;

						{	/* Liveness/liveness.scm 915 */
							int BgL_tmpz00_7660;

							BgL_tmpz00_7660 = (int) (1L);
							BgL_tmpz00_5064 = BGL_MVALUES_VAL(BgL_tmpz00_7660);
						}
						{	/* Liveness/liveness.scm 915 */
							int BgL_tmpz00_7663;

							BgL_tmpz00_7663 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7663, BUNSPEC);
						}
						BgL_usevarz00_5063 = BgL_tmpz00_5064;
					}
					{	/* Liveness/liveness.scm 915 */
						obj_t BgL_defbodyz00_5065;

						BgL_defbodyz00_5065 =
							BGl_defusez00zzliveness_livenessz00(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)))->
								BgL_bodyz00));
						{	/* Liveness/liveness.scm 916 */
							obj_t BgL_usebodyz00_5066;

							{	/* Liveness/liveness.scm 917 */
								obj_t BgL_tmpz00_5067;

								{	/* Liveness/liveness.scm 917 */
									int BgL_tmpz00_7669;

									BgL_tmpz00_7669 = (int) (1L);
									BgL_tmpz00_5067 = BGL_MVALUES_VAL(BgL_tmpz00_7669);
								}
								{	/* Liveness/liveness.scm 917 */
									int BgL_tmpz00_7672;

									BgL_tmpz00_7672 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_7672, BUNSPEC);
								}
								BgL_usebodyz00_5066 = BgL_tmpz00_5067;
							}
							{	/* Liveness/liveness.scm 917 */
								obj_t BgL_defonxz00_5068;

								BgL_defonxz00_5068 =
									BGl_defusez00zzliveness_livenessz00(
									(((BgL_setzd2exzd2itz00_bglt) COBJECT(
												((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)))->
										BgL_onexitz00));
								{	/* Liveness/liveness.scm 918 */
									obj_t BgL_useonxz00_5069;

									{	/* Liveness/liveness.scm 919 */
										obj_t BgL_tmpz00_5070;

										{	/* Liveness/liveness.scm 919 */
											int BgL_tmpz00_7678;

											BgL_tmpz00_7678 = (int) (1L);
											BgL_tmpz00_5070 = BGL_MVALUES_VAL(BgL_tmpz00_7678);
										}
										{	/* Liveness/liveness.scm 919 */
											int BgL_tmpz00_7681;

											BgL_tmpz00_7681 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_7681, BUNSPEC);
										}
										BgL_useonxz00_5069 = BgL_tmpz00_5070;
									}
									{	/* Liveness/liveness.scm 920 */
										BgL_setzd2exzd2itz00_bglt BgL_arg2469z00_5071;

										{	/* Liveness/liveness.scm 920 */
											BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_wide1380z00_5072;

											BgL_wide1380z00_5072 =
												((BgL_setzd2exzd2itzf2livenesszf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_setzd2exzd2itzf2livenesszf2_bgl))));
											{	/* Liveness/liveness.scm 920 */
												obj_t BgL_auxz00_7689;
												BgL_objectz00_bglt BgL_tmpz00_7685;

												BgL_auxz00_7689 = ((obj_t) BgL_wide1380z00_5072);
												BgL_tmpz00_7685 =
													((BgL_objectz00_bglt)
													((BgL_setzd2exzd2itz00_bglt)
														((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7685,
													BgL_auxz00_7689);
											}
											((BgL_objectz00_bglt)
												((BgL_setzd2exzd2itz00_bglt)
													((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)));
											{	/* Liveness/liveness.scm 920 */
												long BgL_arg2470z00_5073;

												BgL_arg2470z00_5073 =
													BGL_CLASS_NUM
													(BGl_setzd2exzd2itzf2livenesszf2zzliveness_typesz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_setzd2exzd2itz00_bglt) (
																(BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640))),
													BgL_arg2470z00_5073);
											}
											((BgL_setzd2exzd2itz00_bglt)
												((BgL_setzd2exzd2itz00_bglt)
													((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)));
										}
										{
											obj_t BgL_auxz00_7711;
											BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7703;

											{	/* Liveness/liveness.scm 921 */
												obj_t BgL_arg2471z00_5074;

												BgL_arg2471z00_5074 =
													BGl_intersectionz00zzliveness_setz00
													(BgL_defbodyz00_5065, BgL_defonxz00_5068);
												BgL_auxz00_7711 =
													BGl_unionz00zzliveness_setz00(BgL_defvarz00_5061,
													BgL_arg2471z00_5074);
											}
											{
												obj_t BgL_auxz00_7704;

												{	/* Liveness/liveness.scm 921 */
													BgL_objectz00_bglt BgL_tmpz00_7705;

													BgL_tmpz00_7705 =
														((BgL_objectz00_bglt)
														((BgL_setzd2exzd2itz00_bglt)
															((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)));
													BgL_auxz00_7704 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7705);
												}
												BgL_auxz00_7703 =
													((BgL_setzd2exzd2itzf2livenesszf2_bglt)
													BgL_auxz00_7704);
											}
											((((BgL_setzd2exzd2itzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_7703))->BgL_defz00) =
												((obj_t) BgL_auxz00_7711), BUNSPEC);
										}
										{
											obj_t BgL_auxz00_7723;
											BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7715;

											{	/* Liveness/liveness.scm 922 */
												obj_t BgL_arg2473z00_5075;

												{	/* Liveness/liveness.scm 922 */
													obj_t BgL_arg2474z00_5076;

													BgL_arg2474z00_5076 =
														BGl_unionz00zzliveness_setz00(BgL_usebodyz00_5066,
														BgL_useonxz00_5069);
													BgL_arg2473z00_5075 =
														BGl_disjonctionz00zzliveness_setz00
														(BgL_arg2474z00_5076, BgL_defvarz00_5061);
												}
												BgL_auxz00_7723 =
													BGl_unionz00zzliveness_setz00(BgL_usevarz00_5063,
													BgL_arg2473z00_5075);
											}
											{
												obj_t BgL_auxz00_7716;

												{	/* Liveness/liveness.scm 922 */
													BgL_objectz00_bglt BgL_tmpz00_7717;

													BgL_tmpz00_7717 =
														((BgL_objectz00_bglt)
														((BgL_setzd2exzd2itz00_bglt)
															((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)));
													BgL_auxz00_7716 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7717);
												}
												BgL_auxz00_7715 =
													((BgL_setzd2exzd2itzf2livenesszf2_bglt)
													BgL_auxz00_7716);
											}
											((((BgL_setzd2exzd2itzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_7715))->BgL_usez00) =
												((obj_t) BgL_auxz00_7723), BUNSPEC);
										}
										{
											BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7728;

											{
												obj_t BgL_auxz00_7729;

												{	/* Liveness/liveness.scm 918 */
													BgL_objectz00_bglt BgL_tmpz00_7730;

													BgL_tmpz00_7730 =
														((BgL_objectz00_bglt)
														((BgL_setzd2exzd2itz00_bglt)
															((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)));
													BgL_auxz00_7729 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7730);
												}
												BgL_auxz00_7728 =
													((BgL_setzd2exzd2itzf2livenesszf2_bglt)
													BgL_auxz00_7729);
											}
											((((BgL_setzd2exzd2itzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_7728))->BgL_inz00) =
												((obj_t) BNIL), BUNSPEC);
										}
										{
											BgL_setzd2exzd2itzf2livenesszf2_bglt BgL_auxz00_7737;

											{
												obj_t BgL_auxz00_7738;

												{	/* Liveness/liveness.scm 918 */
													BgL_objectz00_bglt BgL_tmpz00_7739;

													BgL_tmpz00_7739 =
														((BgL_objectz00_bglt)
														((BgL_setzd2exzd2itz00_bglt)
															((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640)));
													BgL_auxz00_7738 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7739);
												}
												BgL_auxz00_7737 =
													((BgL_setzd2exzd2itzf2livenesszf2_bglt)
													BgL_auxz00_7738);
											}
											((((BgL_setzd2exzd2itzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_7737))->BgL_outz00) =
												((obj_t) BNIL), BUNSPEC);
										}
										BgL_arg2469z00_5071 =
											((BgL_setzd2exzd2itz00_bglt)
											((BgL_setzd2exzd2itz00_bglt) BgL_nz00_4640));
										return
											BGl_defusez00zzliveness_livenessz00(
											((BgL_nodez00_bglt) BgL_arg2469z00_5071));
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &inout-let-var/livene2049 */
	obj_t BGl_z62inoutzd2letzd2varzf2livene2049z90zzliveness_livenessz00(obj_t
		BgL_envz00_4641, obj_t BgL_nz00_4642)
	{
		{	/* Liveness/liveness.scm 902 */
			{	/* Liveness/liveness.scm 904 */
				obj_t BgL_val0_1782z00_5078;
				obj_t BgL_val1_1783z00_5079;

				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7750;

					{
						obj_t BgL_auxz00_7751;

						{	/* Liveness/liveness.scm 904 */
							BgL_objectz00_bglt BgL_tmpz00_7752;

							BgL_tmpz00_7752 =
								((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_4642));
							BgL_auxz00_7751 = BGL_OBJECT_WIDENING(BgL_tmpz00_7752);
						}
						BgL_auxz00_7750 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7751);
					}
					BgL_val0_1782z00_5078 =
						(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7750))->
						BgL_inz00);
				}
				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7758;

					{
						obj_t BgL_auxz00_7759;

						{	/* Liveness/liveness.scm 904 */
							BgL_objectz00_bglt BgL_tmpz00_7760;

							BgL_tmpz00_7760 =
								((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_4642));
							BgL_auxz00_7759 = BGL_OBJECT_WIDENING(BgL_tmpz00_7760);
						}
						BgL_auxz00_7758 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7759);
					}
					BgL_val1_1783z00_5079 =
						(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7758))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 904 */
					int BgL_tmpz00_7766;

					BgL_tmpz00_7766 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7766);
				}
				{	/* Liveness/liveness.scm 904 */
					int BgL_tmpz00_7769;

					BgL_tmpz00_7769 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7769, BgL_val1_1783z00_5079);
				}
				return BgL_val0_1782z00_5078;
			}
		}

	}



/* &inout!-let-var/liven2047 */
	obj_t BGl_z62inoutz12zd2letzd2varzf2liven2047z82zzliveness_livenessz00(obj_t
		BgL_envz00_4643, obj_t BgL_nz00_4644, obj_t BgL_oz00_4645)
	{
		{	/* Liveness/liveness.scm 887 */
			{
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7772;

				{
					obj_t BgL_auxz00_7773;

					{	/* Liveness/liveness.scm 891 */
						BgL_objectz00_bglt BgL_tmpz00_7774;

						BgL_tmpz00_7774 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_4644));
						BgL_auxz00_7773 = BGL_OBJECT_WIDENING(BgL_tmpz00_7774);
					}
					BgL_auxz00_7772 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7773);
				}
				((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7772))->
						BgL_outz00) = ((obj_t) BgL_oz00_4645), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_7787;
				BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7780;

				{	/* Liveness/liveness.scm 892 */
					obj_t BgL_arg2453z00_5081;
					obj_t BgL_arg2455z00_5082;

					{
						BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7788;

						{
							obj_t BgL_auxz00_7789;

							{	/* Liveness/liveness.scm 892 */
								BgL_objectz00_bglt BgL_tmpz00_7790;

								BgL_tmpz00_7790 =
									((BgL_objectz00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nz00_4644));
								BgL_auxz00_7789 = BGL_OBJECT_WIDENING(BgL_tmpz00_7790);
							}
							BgL_auxz00_7788 =
								((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7789);
						}
						BgL_arg2453z00_5081 =
							(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7788))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 892 */
						obj_t BgL_arg2456z00_5083;
						obj_t BgL_arg2457z00_5084;

						{
							BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7796;

							{
								obj_t BgL_auxz00_7797;

								{	/* Liveness/liveness.scm 892 */
									BgL_objectz00_bglt BgL_tmpz00_7798;

									BgL_tmpz00_7798 =
										((BgL_objectz00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_nz00_4644));
									BgL_auxz00_7797 = BGL_OBJECT_WIDENING(BgL_tmpz00_7798);
								}
								BgL_auxz00_7796 =
									((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7797);
							}
							BgL_arg2456z00_5083 =
								(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7796))->
								BgL_outz00);
						}
						{
							BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7804;

							{
								obj_t BgL_auxz00_7805;

								{	/* Liveness/liveness.scm 892 */
									BgL_objectz00_bglt BgL_tmpz00_7806;

									BgL_tmpz00_7806 =
										((BgL_objectz00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_nz00_4644));
									BgL_auxz00_7805 = BGL_OBJECT_WIDENING(BgL_tmpz00_7806);
								}
								BgL_auxz00_7804 =
									((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7805);
							}
							BgL_arg2457z00_5084 =
								(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7804))->
								BgL_defz00);
						}
						BgL_arg2455z00_5082 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2456z00_5083,
							BgL_arg2457z00_5084);
					}
					BgL_auxz00_7787 =
						BGl_unionz00zzliveness_setz00(BgL_arg2453z00_5081,
						BgL_arg2455z00_5082);
				}
				{
					obj_t BgL_auxz00_7781;

					{	/* Liveness/liveness.scm 892 */
						BgL_objectz00_bglt BgL_tmpz00_7782;

						BgL_tmpz00_7782 =
							((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_4644));
						BgL_auxz00_7781 = BGL_OBJECT_WIDENING(BgL_tmpz00_7782);
					}
					BgL_auxz00_7780 =
						((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7781);
				}
				((((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7780))->
						BgL_inz00) = ((obj_t) BgL_auxz00_7787), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 893 */
				obj_t BgL_defz00_5085;

				{	/* Liveness/liveness.scm 894 */
					BgL_nodez00_bglt BgL_arg2465z00_5086;

					BgL_arg2465z00_5086 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nz00_4644))))->BgL_bodyz00);
					BgL_defz00_5085 =
						BGl_defusez00zzliveness_livenessz00(BgL_arg2465z00_5086);
				}
				{	/* Liveness/liveness.scm 894 */
					obj_t BgL_usez00_5087;

					{	/* Liveness/liveness.scm 895 */
						obj_t BgL_tmpz00_5088;

						{	/* Liveness/liveness.scm 895 */
							int BgL_tmpz00_7819;

							BgL_tmpz00_7819 = (int) (1L);
							BgL_tmpz00_5088 = BGL_MVALUES_VAL(BgL_tmpz00_7819);
						}
						{	/* Liveness/liveness.scm 895 */
							int BgL_tmpz00_7822;

							BgL_tmpz00_7822 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7822, BUNSPEC);
						}
						BgL_usez00_5087 = BgL_tmpz00_5088;
					}
					{	/* Liveness/liveness.scm 895 */
						obj_t BgL_inz00_5089;

						{	/* Liveness/liveness.scm 896 */
							BgL_nodez00_bglt BgL_arg2462z00_5090;
							obj_t BgL_arg2463z00_5091;

							BgL_arg2462z00_5090 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt)
											((BgL_letzd2varzd2_bglt) BgL_nz00_4644))))->BgL_bodyz00);
							{
								BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7828;

								{
									obj_t BgL_auxz00_7829;

									{	/* Liveness/liveness.scm 896 */
										BgL_objectz00_bglt BgL_tmpz00_7830;

										BgL_tmpz00_7830 =
											((BgL_objectz00_bglt)
											((BgL_letzd2varzd2_bglt) BgL_nz00_4644));
										BgL_auxz00_7829 = BGL_OBJECT_WIDENING(BgL_tmpz00_7830);
									}
									BgL_auxz00_7828 =
										((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7829);
								}
								BgL_arg2463z00_5091 =
									(((BgL_letzd2varzf2livenessz20_bglt)
										COBJECT(BgL_auxz00_7828))->BgL_outz00);
							}
							BgL_inz00_5089 =
								BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2462z00_5090,
								BgL_arg2463z00_5091);
						}
						{	/* Liveness/liveness.scm 896 */
							obj_t BgL_outz00_5092;

							{	/* Liveness/liveness.scm 897 */
								obj_t BgL_tmpz00_5093;

								{	/* Liveness/liveness.scm 897 */
									int BgL_tmpz00_7837;

									BgL_tmpz00_7837 = (int) (1L);
									BgL_tmpz00_5093 = BGL_MVALUES_VAL(BgL_tmpz00_7837);
								}
								{	/* Liveness/liveness.scm 897 */
									int BgL_tmpz00_7840;

									BgL_tmpz00_7840 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_7840, BUNSPEC);
								}
								BgL_outz00_5092 = BgL_tmpz00_5093;
							}
							{	/* Liveness/liveness.scm 897 */
								obj_t BgL_g1779z00_5094;

								BgL_g1779z00_5094 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nz00_4644))))->
									BgL_bindingsz00);
								{
									obj_t BgL_l1777z00_5096;

									BgL_l1777z00_5096 = BgL_g1779z00_5094;
								BgL_zc3z04anonymousza32458ze3z87_5095:
									if (PAIRP(BgL_l1777z00_5096))
										{	/* Liveness/liveness.scm 897 */
											{	/* Liveness/liveness.scm 898 */
												obj_t BgL_bz00_5097;

												BgL_bz00_5097 = CAR(BgL_l1777z00_5096);
												{	/* Liveness/liveness.scm 898 */
													obj_t BgL_arg2460z00_5098;

													BgL_arg2460z00_5098 = CDR(((obj_t) BgL_bz00_5097));
													BGl_inoutz12z12zzliveness_livenessz00(
														((BgL_nodez00_bglt) BgL_arg2460z00_5098),
														BgL_inz00_5089);
												}
											}
											{
												obj_t BgL_l1777z00_7853;

												BgL_l1777z00_7853 = CDR(BgL_l1777z00_5096);
												BgL_l1777z00_5096 = BgL_l1777z00_7853;
												goto BgL_zc3z04anonymousza32458ze3z87_5095;
											}
										}
									else
										{	/* Liveness/liveness.scm 897 */
											((bool_t) 1);
										}
								}
							}
						}
					}
				}
			}
			{	/* Liveness/liveness.scm 900 */
				obj_t BgL_val0_1780z00_5099;
				obj_t BgL_val1_1781z00_5100;

				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7855;

					{
						obj_t BgL_auxz00_7856;

						{	/* Liveness/liveness.scm 900 */
							BgL_objectz00_bglt BgL_tmpz00_7857;

							BgL_tmpz00_7857 =
								((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_4644));
							BgL_auxz00_7856 = BGL_OBJECT_WIDENING(BgL_tmpz00_7857);
						}
						BgL_auxz00_7855 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7856);
					}
					BgL_val0_1780z00_5099 =
						(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7855))->
						BgL_inz00);
				}
				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7863;

					{
						obj_t BgL_auxz00_7864;

						{	/* Liveness/liveness.scm 900 */
							BgL_objectz00_bglt BgL_tmpz00_7865;

							BgL_tmpz00_7865 =
								((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_4644));
							BgL_auxz00_7864 = BGL_OBJECT_WIDENING(BgL_tmpz00_7865);
						}
						BgL_auxz00_7863 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7864);
					}
					BgL_val1_1781z00_5100 =
						(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7863))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 900 */
					int BgL_tmpz00_7871;

					BgL_tmpz00_7871 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7871);
				}
				{	/* Liveness/liveness.scm 900 */
					int BgL_tmpz00_7874;

					BgL_tmpz00_7874 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7874, BgL_val1_1781z00_5100);
				}
				return BgL_val0_1780z00_5099;
			}
		}

	}



/* &defuse-let-var/liven2045 */
	obj_t BGl_z62defusezd2letzd2varzf2liven2045z90zzliveness_livenessz00(obj_t
		BgL_envz00_4646, obj_t BgL_nz00_4647)
	{
		{	/* Liveness/liveness.scm 883 */
			{	/* Liveness/liveness.scm 885 */
				obj_t BgL_val0_1775z00_5102;
				obj_t BgL_val1_1776z00_5103;

				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7877;

					{
						obj_t BgL_auxz00_7878;

						{	/* Liveness/liveness.scm 885 */
							BgL_objectz00_bglt BgL_tmpz00_7879;

							BgL_tmpz00_7879 =
								((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_4647));
							BgL_auxz00_7878 = BGL_OBJECT_WIDENING(BgL_tmpz00_7879);
						}
						BgL_auxz00_7877 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7878);
					}
					BgL_val0_1775z00_5102 =
						(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7877))->
						BgL_defz00);
				}
				{
					BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7885;

					{
						obj_t BgL_auxz00_7886;

						{	/* Liveness/liveness.scm 885 */
							BgL_objectz00_bglt BgL_tmpz00_7887;

							BgL_tmpz00_7887 =
								((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_4647));
							BgL_auxz00_7886 = BGL_OBJECT_WIDENING(BgL_tmpz00_7887);
						}
						BgL_auxz00_7885 =
							((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7886);
					}
					BgL_val1_1776z00_5103 =
						(((BgL_letzd2varzf2livenessz20_bglt) COBJECT(BgL_auxz00_7885))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 885 */
					int BgL_tmpz00_7893;

					BgL_tmpz00_7893 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7893);
				}
				{	/* Liveness/liveness.scm 885 */
					int BgL_tmpz00_7896;

					BgL_tmpz00_7896 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_7896, BgL_val1_1776z00_5103);
				}
				return BgL_val0_1775z00_5102;
			}
		}

	}



/* &defuse-let-var2043 */
	obj_t BGl_z62defusezd2letzd2var2043z62zzliveness_livenessz00(obj_t
		BgL_envz00_4648, obj_t BgL_nz00_4649)
	{
		{	/* Liveness/liveness.scm 861 */
			{	/* Liveness/liveness.scm 865 */
				obj_t BgL_defbindingsz00_5105;
				obj_t BgL_usebindingsz00_5106;

				BgL_defbindingsz00_5105 = BNIL;
				BgL_usebindingsz00_5106 = BNIL;
				{	/* Liveness/liveness.scm 867 */
					obj_t BgL_g1771z00_5107;

					BgL_g1771z00_5107 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_4649)))->BgL_bindingsz00);
					{
						obj_t BgL_l1769z00_5109;

						BgL_l1769z00_5109 = BgL_g1771z00_5107;
					BgL_zc3z04anonymousza32439ze3z87_5108:
						if (PAIRP(BgL_l1769z00_5109))
							{	/* Liveness/liveness.scm 867 */
								{	/* Liveness/liveness.scm 868 */
									obj_t BgL_bz00_5110;

									BgL_bz00_5110 = CAR(BgL_l1769z00_5109);
									{	/* Liveness/liveness.scm 868 */
										BgL_localz00_bglt BgL_tmp1361z00_5111;

										BgL_tmp1361z00_5111 =
											((BgL_localz00_bglt) CAR(((obj_t) BgL_bz00_5110)));
										{	/* Liveness/liveness.scm 868 */
											BgL_localzf2livenesszf2_bglt BgL_wide1363z00_5112;

											BgL_wide1363z00_5112 =
												((BgL_localzf2livenesszf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_localzf2livenesszf2_bgl))));
											{	/* Liveness/liveness.scm 868 */
												obj_t BgL_auxz00_7911;
												BgL_objectz00_bglt BgL_tmpz00_7908;

												BgL_auxz00_7911 = ((obj_t) BgL_wide1363z00_5112);
												BgL_tmpz00_7908 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt) BgL_tmp1361z00_5111));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7908,
													BgL_auxz00_7911);
											}
											((BgL_objectz00_bglt)
												((BgL_localz00_bglt) BgL_tmp1361z00_5111));
											{	/* Liveness/liveness.scm 868 */
												long BgL_arg2442z00_5113;

												BgL_arg2442z00_5113 =
													BGL_CLASS_NUM
													(BGl_localzf2livenesszf2zzliveness_typesz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_localz00_bglt) BgL_tmp1361z00_5111)),
													BgL_arg2442z00_5113);
											}
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_tmp1361z00_5111));
										}
										{
											BgL_localzf2livenesszf2_bglt BgL_auxz00_7922;

											{
												obj_t BgL_auxz00_7923;

												{	/* Liveness/liveness.scm 868 */
													BgL_objectz00_bglt BgL_tmpz00_7924;

													BgL_tmpz00_7924 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_tmp1361z00_5111));
													BgL_auxz00_7923 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7924);
												}
												BgL_auxz00_7922 =
													((BgL_localzf2livenesszf2_bglt) BgL_auxz00_7923);
											}
											((((BgL_localzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_7922))->BgL_z52countz52) =
												((int) (int) (0L)), BUNSPEC);
										}
										((BgL_localz00_bglt) BgL_tmp1361z00_5111);
								}}
								{
									obj_t BgL_l1769z00_7932;

									BgL_l1769z00_7932 = CDR(BgL_l1769z00_5109);
									BgL_l1769z00_5109 = BgL_l1769z00_7932;
									goto BgL_zc3z04anonymousza32439ze3z87_5108;
								}
							}
						else
							{	/* Liveness/liveness.scm 867 */
								((bool_t) 1);
							}
					}
				}
				{	/* Liveness/liveness.scm 870 */
					obj_t BgL_g1774z00_5114;

					BgL_g1774z00_5114 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_4649)))->BgL_bindingsz00);
					{
						obj_t BgL_l1772z00_5116;

						BgL_l1772z00_5116 = BgL_g1774z00_5114;
					BgL_zc3z04anonymousza32444ze3z87_5115:
						if (PAIRP(BgL_l1772z00_5116))
							{	/* Liveness/liveness.scm 870 */
								{	/* Liveness/liveness.scm 871 */
									obj_t BgL_bz00_5117;

									BgL_bz00_5117 = CAR(BgL_l1772z00_5116);
									{	/* Liveness/liveness.scm 871 */
										obj_t BgL_defz00_5118;

										{	/* Liveness/liveness.scm 872 */
											obj_t BgL_arg2446z00_5119;

											BgL_arg2446z00_5119 = CDR(((obj_t) BgL_bz00_5117));
											BgL_defz00_5118 =
												BGl_defusez00zzliveness_livenessz00(
												((BgL_nodez00_bglt) BgL_arg2446z00_5119));
										}
										{	/* Liveness/liveness.scm 872 */
											obj_t BgL_usez00_5120;

											{	/* Liveness/liveness.scm 873 */
												obj_t BgL_tmpz00_5121;

												{	/* Liveness/liveness.scm 873 */
													int BgL_tmpz00_7943;

													BgL_tmpz00_7943 = (int) (1L);
													BgL_tmpz00_5121 = BGL_MVALUES_VAL(BgL_tmpz00_7943);
												}
												{	/* Liveness/liveness.scm 873 */
													int BgL_tmpz00_7946;

													BgL_tmpz00_7946 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_7946, BUNSPEC);
												}
												BgL_usez00_5120 = BgL_tmpz00_5121;
											}
											BgL_defbindingsz00_5105 =
												BGl_unionz00zzliveness_setz00(BgL_defz00_5118,
												BgL_defbindingsz00_5105);
											BgL_usebindingsz00_5106 =
												BGl_unionz00zzliveness_setz00(BgL_usez00_5120,
												BgL_usebindingsz00_5106);
								}}}
								{
									obj_t BgL_l1772z00_7951;

									BgL_l1772z00_7951 = CDR(BgL_l1772z00_5116);
									BgL_l1772z00_5116 = BgL_l1772z00_7951;
									goto BgL_zc3z04anonymousza32444ze3z87_5115;
								}
							}
						else
							{	/* Liveness/liveness.scm 870 */
								((bool_t) 1);
							}
					}
				}
				{	/* Liveness/liveness.scm 876 */
					obj_t BgL_defbodyz00_5122;

					BgL_defbodyz00_5122 =
						BGl_defusez00zzliveness_livenessz00(
						(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nz00_4649)))->BgL_bodyz00));
					{	/* Liveness/liveness.scm 877 */
						obj_t BgL_usebodyz00_5123;

						{	/* Liveness/liveness.scm 878 */
							obj_t BgL_tmpz00_5124;

							{	/* Liveness/liveness.scm 878 */
								int BgL_tmpz00_7956;

								BgL_tmpz00_7956 = (int) (1L);
								BgL_tmpz00_5124 = BGL_MVALUES_VAL(BgL_tmpz00_7956);
							}
							{	/* Liveness/liveness.scm 878 */
								int BgL_tmpz00_7959;

								BgL_tmpz00_7959 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_7959, BUNSPEC);
							}
							BgL_usebodyz00_5123 = BgL_tmpz00_5124;
						}
						{	/* Liveness/liveness.scm 879 */
							BgL_letzd2varzd2_bglt BgL_arg2449z00_5125;

							{	/* Liveness/liveness.scm 879 */
								BgL_letzd2varzf2livenessz20_bglt BgL_wide1367z00_5126;

								BgL_wide1367z00_5126 =
									((BgL_letzd2varzf2livenessz20_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_letzd2varzf2livenessz20_bgl))));
								{	/* Liveness/liveness.scm 879 */
									obj_t BgL_auxz00_7967;
									BgL_objectz00_bglt BgL_tmpz00_7963;

									BgL_auxz00_7967 = ((obj_t) BgL_wide1367z00_5126);
									BgL_tmpz00_7963 =
										((BgL_objectz00_bglt)
										((BgL_letzd2varzd2_bglt)
											((BgL_letzd2varzd2_bglt) BgL_nz00_4649)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7963, BgL_auxz00_7967);
								}
								((BgL_objectz00_bglt)
									((BgL_letzd2varzd2_bglt)
										((BgL_letzd2varzd2_bglt) BgL_nz00_4649)));
								{	/* Liveness/liveness.scm 879 */
									long BgL_arg2450z00_5127;

									BgL_arg2450z00_5127 =
										BGL_CLASS_NUM
										(BGl_letzd2varzf2livenessz20zzliveness_typesz00);
									BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
												(BgL_letzd2varzd2_bglt) ((BgL_letzd2varzd2_bglt)
													BgL_nz00_4649))), BgL_arg2450z00_5127);
								}
								((BgL_letzd2varzd2_bglt)
									((BgL_letzd2varzd2_bglt)
										((BgL_letzd2varzd2_bglt) BgL_nz00_4649)));
							}
							{
								BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7981;

								{
									obj_t BgL_auxz00_7982;

									{	/* Liveness/liveness.scm 880 */
										BgL_objectz00_bglt BgL_tmpz00_7983;

										BgL_tmpz00_7983 =
											((BgL_objectz00_bglt)
											((BgL_letzd2varzd2_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nz00_4649)));
										BgL_auxz00_7982 = BGL_OBJECT_WIDENING(BgL_tmpz00_7983);
									}
									BgL_auxz00_7981 =
										((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7982);
								}
								((((BgL_letzd2varzf2livenessz20_bglt)
											COBJECT(BgL_auxz00_7981))->BgL_defz00) =
									((obj_t)
										BGl_unionz00zzliveness_setz00(BgL_defbindingsz00_5105,
											BgL_defbodyz00_5122)), BUNSPEC);
							}
							{
								obj_t BgL_auxz00_7999;
								BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_7991;

								{	/* Liveness/liveness.scm 881 */
									obj_t BgL_arg2451z00_5128;

									BgL_arg2451z00_5128 =
										BGl_disjonctionz00zzliveness_setz00(BgL_usebodyz00_5123,
										BgL_defbindingsz00_5105);
									BgL_auxz00_7999 =
										BGl_unionz00zzliveness_setz00(BgL_usebindingsz00_5106,
										BgL_arg2451z00_5128);
								}
								{
									obj_t BgL_auxz00_7992;

									{	/* Liveness/liveness.scm 881 */
										BgL_objectz00_bglt BgL_tmpz00_7993;

										BgL_tmpz00_7993 =
											((BgL_objectz00_bglt)
											((BgL_letzd2varzd2_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nz00_4649)));
										BgL_auxz00_7992 = BGL_OBJECT_WIDENING(BgL_tmpz00_7993);
									}
									BgL_auxz00_7991 =
										((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_7992);
								}
								((((BgL_letzd2varzf2livenessz20_bglt)
											COBJECT(BgL_auxz00_7991))->BgL_usez00) =
									((obj_t) BgL_auxz00_7999), BUNSPEC);
							}
							{
								BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_8003;

								{
									obj_t BgL_auxz00_8004;

									{	/* Liveness/liveness.scm 877 */
										BgL_objectz00_bglt BgL_tmpz00_8005;

										BgL_tmpz00_8005 =
											((BgL_objectz00_bglt)
											((BgL_letzd2varzd2_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nz00_4649)));
										BgL_auxz00_8004 = BGL_OBJECT_WIDENING(BgL_tmpz00_8005);
									}
									BgL_auxz00_8003 =
										((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_8004);
								}
								((((BgL_letzd2varzf2livenessz20_bglt)
											COBJECT(BgL_auxz00_8003))->BgL_inz00) =
									((obj_t) BNIL), BUNSPEC);
							}
							{
								BgL_letzd2varzf2livenessz20_bglt BgL_auxz00_8012;

								{
									obj_t BgL_auxz00_8013;

									{	/* Liveness/liveness.scm 877 */
										BgL_objectz00_bglt BgL_tmpz00_8014;

										BgL_tmpz00_8014 =
											((BgL_objectz00_bglt)
											((BgL_letzd2varzd2_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nz00_4649)));
										BgL_auxz00_8013 = BGL_OBJECT_WIDENING(BgL_tmpz00_8014);
									}
									BgL_auxz00_8012 =
										((BgL_letzd2varzf2livenessz20_bglt) BgL_auxz00_8013);
								}
								((((BgL_letzd2varzf2livenessz20_bglt)
											COBJECT(BgL_auxz00_8012))->BgL_outz00) =
									((obj_t) BNIL), BUNSPEC);
							}
							BgL_arg2449z00_5125 =
								((BgL_letzd2varzd2_bglt)
								((BgL_letzd2varzd2_bglt) BgL_nz00_4649));
							return
								BGl_defusez00zzliveness_livenessz00(
								((BgL_nodez00_bglt) BgL_arg2449z00_5125));
						}
					}
				}
			}
		}

	}



/* &inout-let-fun/livene2040 */
	obj_t BGl_z62inoutzd2letzd2funzf2livene2040z90zzliveness_livenessz00(obj_t
		BgL_envz00_4650, obj_t BgL_nz00_4651)
	{
		{	/* Liveness/liveness.scm 854 */
			{	/* Liveness/liveness.scm 856 */
				obj_t BgL_val0_1767z00_5130;
				obj_t BgL_val1_1768z00_5131;

				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8025;

					{
						obj_t BgL_auxz00_8026;

						{	/* Liveness/liveness.scm 856 */
							BgL_objectz00_bglt BgL_tmpz00_8027;

							BgL_tmpz00_8027 =
								((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_4651));
							BgL_auxz00_8026 = BGL_OBJECT_WIDENING(BgL_tmpz00_8027);
						}
						BgL_auxz00_8025 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8026);
					}
					BgL_val0_1767z00_5130 =
						(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8025))->
						BgL_inz00);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8033;

					{
						obj_t BgL_auxz00_8034;

						{	/* Liveness/liveness.scm 856 */
							BgL_objectz00_bglt BgL_tmpz00_8035;

							BgL_tmpz00_8035 =
								((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_4651));
							BgL_auxz00_8034 = BGL_OBJECT_WIDENING(BgL_tmpz00_8035);
						}
						BgL_auxz00_8033 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8034);
					}
					BgL_val1_1768z00_5131 =
						(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8033))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 856 */
					int BgL_tmpz00_8041;

					BgL_tmpz00_8041 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8041);
				}
				{	/* Liveness/liveness.scm 856 */
					int BgL_tmpz00_8044;

					BgL_tmpz00_8044 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8044, BgL_val1_1768z00_5131);
				}
				return BgL_val0_1767z00_5130;
			}
		}

	}



/* &inout!-let-fun/liven2038 */
	obj_t BGl_z62inoutz12zd2letzd2funzf2liven2038z82zzliveness_livenessz00(obj_t
		BgL_envz00_4652, obj_t BgL_nz00_4653, obj_t BgL_oz00_4654)
	{
		{	/* Liveness/liveness.scm 841 */
			{
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8047;

				{
					obj_t BgL_auxz00_8048;

					{	/* Liveness/liveness.scm 844 */
						BgL_objectz00_bglt BgL_tmpz00_8049;

						BgL_tmpz00_8049 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_4653));
						BgL_auxz00_8048 = BGL_OBJECT_WIDENING(BgL_tmpz00_8049);
					}
					BgL_auxz00_8047 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8048);
				}
				((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8047))->
						BgL_outz00) = ((obj_t) BgL_oz00_4654), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_8062;
				BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8055;

				{	/* Liveness/liveness.scm 845 */
					obj_t BgL_arg2429z00_5133;
					obj_t BgL_arg2430z00_5134;

					{
						BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8063;

						{
							obj_t BgL_auxz00_8064;

							{	/* Liveness/liveness.scm 845 */
								BgL_objectz00_bglt BgL_tmpz00_8065;

								BgL_tmpz00_8065 =
									((BgL_objectz00_bglt)
									((BgL_letzd2funzd2_bglt) BgL_nz00_4653));
								BgL_auxz00_8064 = BGL_OBJECT_WIDENING(BgL_tmpz00_8065);
							}
							BgL_auxz00_8063 =
								((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8064);
						}
						BgL_arg2429z00_5133 =
							(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8063))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 845 */
						obj_t BgL_arg2431z00_5135;
						obj_t BgL_arg2432z00_5136;

						{
							BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8071;

							{
								obj_t BgL_auxz00_8072;

								{	/* Liveness/liveness.scm 845 */
									BgL_objectz00_bglt BgL_tmpz00_8073;

									BgL_tmpz00_8073 =
										((BgL_objectz00_bglt)
										((BgL_letzd2funzd2_bglt) BgL_nz00_4653));
									BgL_auxz00_8072 = BGL_OBJECT_WIDENING(BgL_tmpz00_8073);
								}
								BgL_auxz00_8071 =
									((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8072);
							}
							BgL_arg2431z00_5135 =
								(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8071))->
								BgL_outz00);
						}
						{
							BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8079;

							{
								obj_t BgL_auxz00_8080;

								{	/* Liveness/liveness.scm 845 */
									BgL_objectz00_bglt BgL_tmpz00_8081;

									BgL_tmpz00_8081 =
										((BgL_objectz00_bglt)
										((BgL_letzd2funzd2_bglt) BgL_nz00_4653));
									BgL_auxz00_8080 = BGL_OBJECT_WIDENING(BgL_tmpz00_8081);
								}
								BgL_auxz00_8079 =
									((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8080);
							}
							BgL_arg2432z00_5136 =
								(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8079))->
								BgL_defz00);
						}
						BgL_arg2430z00_5134 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2431z00_5135,
							BgL_arg2432z00_5136);
					}
					BgL_auxz00_8062 =
						BGl_unionz00zzliveness_setz00(BgL_arg2429z00_5133,
						BgL_arg2430z00_5134);
				}
				{
					obj_t BgL_auxz00_8056;

					{	/* Liveness/liveness.scm 845 */
						BgL_objectz00_bglt BgL_tmpz00_8057;

						BgL_tmpz00_8057 =
							((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_4653));
						BgL_auxz00_8056 = BGL_OBJECT_WIDENING(BgL_tmpz00_8057);
					}
					BgL_auxz00_8055 =
						((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8056);
				}
				((((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8055))->
						BgL_inz00) = ((obj_t) BgL_auxz00_8062), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 846 */
				obj_t BgL_g1764z00_5137;

				BgL_g1764z00_5137 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt)
								((BgL_letzd2funzd2_bglt) BgL_nz00_4653))))->BgL_localsz00);
				{
					obj_t BgL_l1762z00_5139;

					BgL_l1762z00_5139 = BgL_g1764z00_5137;
				BgL_zc3z04anonymousza32433ze3z87_5138:
					if (PAIRP(BgL_l1762z00_5139))
						{	/* Liveness/liveness.scm 846 */
							{	/* Liveness/liveness.scm 847 */
								obj_t BgL_funz00_5140;

								BgL_funz00_5140 = CAR(BgL_l1762z00_5139);
								{	/* Liveness/liveness.scm 848 */
									BgL_sfunz00_bglt BgL_i1358z00_5141;

									BgL_i1358z00_5141 =
										((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_funz00_5140))))->
											BgL_valuez00));
									{	/* Liveness/liveness.scm 849 */
										obj_t BgL_arg2435z00_5142;

										BgL_arg2435z00_5142 =
											(((BgL_sfunz00_bglt) COBJECT(BgL_i1358z00_5141))->
											BgL_bodyz00);
										BGl_inoutz12z12zzliveness_livenessz00(((BgL_nodez00_bglt)
												BgL_arg2435z00_5142), BgL_oz00_4654);
									}
								}
							}
							{
								obj_t BgL_l1762z00_8103;

								BgL_l1762z00_8103 = CDR(BgL_l1762z00_5139);
								BgL_l1762z00_5139 = BgL_l1762z00_8103;
								goto BgL_zc3z04anonymousza32433ze3z87_5138;
							}
						}
					else
						{	/* Liveness/liveness.scm 846 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 851 */
				BgL_nodez00_bglt BgL_arg2438z00_5143;

				BgL_arg2438z00_5143 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt)
								((BgL_letzd2funzd2_bglt) BgL_nz00_4653))))->BgL_bodyz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2438z00_5143,
					BgL_oz00_4654);
			}
			{	/* Liveness/liveness.scm 852 */
				obj_t BgL_val0_1765z00_5144;
				obj_t BgL_val1_1766z00_5145;

				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8109;

					{
						obj_t BgL_auxz00_8110;

						{	/* Liveness/liveness.scm 852 */
							BgL_objectz00_bglt BgL_tmpz00_8111;

							BgL_tmpz00_8111 =
								((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_4653));
							BgL_auxz00_8110 = BGL_OBJECT_WIDENING(BgL_tmpz00_8111);
						}
						BgL_auxz00_8109 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8110);
					}
					BgL_val0_1765z00_5144 =
						(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8109))->
						BgL_inz00);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8117;

					{
						obj_t BgL_auxz00_8118;

						{	/* Liveness/liveness.scm 852 */
							BgL_objectz00_bglt BgL_tmpz00_8119;

							BgL_tmpz00_8119 =
								((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_4653));
							BgL_auxz00_8118 = BGL_OBJECT_WIDENING(BgL_tmpz00_8119);
						}
						BgL_auxz00_8117 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8118);
					}
					BgL_val1_1766z00_5145 =
						(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8117))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 852 */
					int BgL_tmpz00_8125;

					BgL_tmpz00_8125 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8125);
				}
				{	/* Liveness/liveness.scm 852 */
					int BgL_tmpz00_8128;

					BgL_tmpz00_8128 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8128, BgL_val1_1766z00_5145);
				}
				return BgL_val0_1765z00_5144;
			}
		}

	}



/* &defuse-let-fun/liven2036 */
	obj_t BGl_z62defusezd2letzd2funzf2liven2036z90zzliveness_livenessz00(obj_t
		BgL_envz00_4655, obj_t BgL_nz00_4656)
	{
		{	/* Liveness/liveness.scm 837 */
			{	/* Liveness/liveness.scm 839 */
				obj_t BgL_val0_1760z00_5147;
				obj_t BgL_val1_1761z00_5148;

				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8131;

					{
						obj_t BgL_auxz00_8132;

						{	/* Liveness/liveness.scm 839 */
							BgL_objectz00_bglt BgL_tmpz00_8133;

							BgL_tmpz00_8133 =
								((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_4656));
							BgL_auxz00_8132 = BGL_OBJECT_WIDENING(BgL_tmpz00_8133);
						}
						BgL_auxz00_8131 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8132);
					}
					BgL_val0_1760z00_5147 =
						(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8131))->
						BgL_defz00);
				}
				{
					BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8139;

					{
						obj_t BgL_auxz00_8140;

						{	/* Liveness/liveness.scm 839 */
							BgL_objectz00_bglt BgL_tmpz00_8141;

							BgL_tmpz00_8141 =
								((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_4656));
							BgL_auxz00_8140 = BGL_OBJECT_WIDENING(BgL_tmpz00_8141);
						}
						BgL_auxz00_8139 =
							((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8140);
					}
					BgL_val1_1761z00_5148 =
						(((BgL_letzd2funzf2livenessz20_bglt) COBJECT(BgL_auxz00_8139))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 839 */
					int BgL_tmpz00_8147;

					BgL_tmpz00_8147 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8147);
				}
				{	/* Liveness/liveness.scm 839 */
					int BgL_tmpz00_8150;

					BgL_tmpz00_8150 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8150, BgL_val1_1761z00_5148);
				}
				return BgL_val0_1760z00_5147;
			}
		}

	}



/* &defuse-let-fun2034 */
	obj_t BGl_z62defusezd2letzd2fun2034z62zzliveness_livenessz00(obj_t
		BgL_envz00_4657, obj_t BgL_nz00_4658)
	{
		{	/* Liveness/liveness.scm 814 */
			{	/* Liveness/liveness.scm 818 */
				obj_t BgL_defbindingsz00_5150;
				obj_t BgL_usebindingsz00_5151;

				BgL_defbindingsz00_5150 = BNIL;
				BgL_usebindingsz00_5151 = BNIL;
				{	/* Liveness/liveness.scm 820 */
					obj_t BgL_g1756z00_5152;

					BgL_g1756z00_5152 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_4658)))->BgL_localsz00);
					{
						obj_t BgL_l1754z00_5154;

						BgL_l1754z00_5154 = BgL_g1756z00_5152;
					BgL_zc3z04anonymousza32416ze3z87_5153:
						if (PAIRP(BgL_l1754z00_5154))
							{	/* Liveness/liveness.scm 820 */
								{	/* Liveness/liveness.scm 821 */
									obj_t BgL_funz00_5155;

									BgL_funz00_5155 = CAR(BgL_l1754z00_5154);
									{	/* Liveness/liveness.scm 821 */
										BgL_localzf2livenesszf2_bglt BgL_wide1348z00_5156;

										BgL_wide1348z00_5156 =
											((BgL_localzf2livenesszf2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_localzf2livenesszf2_bgl))));
										{	/* Liveness/liveness.scm 821 */
											obj_t BgL_auxz00_8163;
											BgL_objectz00_bglt BgL_tmpz00_8159;

											BgL_auxz00_8163 = ((obj_t) BgL_wide1348z00_5156);
											BgL_tmpz00_8159 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_funz00_5155)));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8159, BgL_auxz00_8163);
										}
										((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_funz00_5155)));
										{	/* Liveness/liveness.scm 821 */
											long BgL_arg2418z00_5157;

											BgL_arg2418z00_5157 =
												BGL_CLASS_NUM
												(BGl_localzf2livenesszf2zzliveness_typesz00);
											BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
														(BgL_localz00_bglt) ((BgL_localz00_bglt)
															BgL_funz00_5155))), BgL_arg2418z00_5157);
										}
										((BgL_localz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_funz00_5155)));
									}
									{
										BgL_localzf2livenesszf2_bglt BgL_auxz00_8177;

										{
											obj_t BgL_auxz00_8178;

											{	/* Liveness/liveness.scm 821 */
												BgL_objectz00_bglt BgL_tmpz00_8179;

												BgL_tmpz00_8179 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_funz00_5155)));
												BgL_auxz00_8178 = BGL_OBJECT_WIDENING(BgL_tmpz00_8179);
											}
											BgL_auxz00_8177 =
												((BgL_localzf2livenesszf2_bglt) BgL_auxz00_8178);
										}
										((((BgL_localzf2livenesszf2_bglt)
													COBJECT(BgL_auxz00_8177))->BgL_z52countz52) =
											((int) (int) (0L)), BUNSPEC);
									}
									((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_funz00_5155));
								}
								{
									obj_t BgL_l1754z00_8189;

									BgL_l1754z00_8189 = CDR(BgL_l1754z00_5154);
									BgL_l1754z00_5154 = BgL_l1754z00_8189;
									goto BgL_zc3z04anonymousza32416ze3z87_5153;
								}
							}
						else
							{	/* Liveness/liveness.scm 820 */
								((bool_t) 1);
							}
					}
				}
				{	/* Liveness/liveness.scm 823 */
					obj_t BgL_g1759z00_5158;

					BgL_g1759z00_5158 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_4658)))->BgL_localsz00);
					{
						obj_t BgL_l1757z00_5160;

						BgL_l1757z00_5160 = BgL_g1759z00_5158;
					BgL_zc3z04anonymousza32420ze3z87_5159:
						if (PAIRP(BgL_l1757z00_5160))
							{	/* Liveness/liveness.scm 823 */
								{	/* Liveness/liveness.scm 824 */
									obj_t BgL_funz00_5161;

									BgL_funz00_5161 = CAR(BgL_l1757z00_5160);
									{	/* Liveness/liveness.scm 824 */
										obj_t BgL_defz00_5162;

										{	/* Liveness/liveness.scm 826 */
											BgL_valuez00_bglt BgL_arg2423z00_5163;

											BgL_arg2423z00_5163 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_funz00_5161))))->
												BgL_valuez00);
											BgL_defz00_5162 =
												BGl_livenesszd2sfunz12zc0zzliveness_livenessz00((
													(BgL_sfunz00_bglt) BgL_arg2423z00_5163));
										}
										{	/* Liveness/liveness.scm 825 */
											obj_t BgL_usez00_5164;

											{	/* Liveness/liveness.scm 827 */
												obj_t BgL_tmpz00_5165;

												{	/* Liveness/liveness.scm 827 */
													int BgL_tmpz00_8201;

													BgL_tmpz00_8201 = (int) (1L);
													BgL_tmpz00_5165 = BGL_MVALUES_VAL(BgL_tmpz00_8201);
												}
												{	/* Liveness/liveness.scm 827 */
													int BgL_tmpz00_8204;

													BgL_tmpz00_8204 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_8204, BUNSPEC);
												}
												BgL_usez00_5164 = BgL_tmpz00_5165;
											}
											BgL_defbindingsz00_5150 =
												BGl_unionz00zzliveness_setz00(BgL_defz00_5162,
												BgL_defbindingsz00_5150);
											BgL_usebindingsz00_5151 =
												BGl_unionz00zzliveness_setz00(BgL_usez00_5164,
												BgL_usebindingsz00_5151);
								}}}
								{
									obj_t BgL_l1757z00_8209;

									BgL_l1757z00_8209 = CDR(BgL_l1757z00_5160);
									BgL_l1757z00_5160 = BgL_l1757z00_8209;
									goto BgL_zc3z04anonymousza32420ze3z87_5159;
								}
							}
						else
							{	/* Liveness/liveness.scm 823 */
								((bool_t) 1);
							}
					}
				}
				{	/* Liveness/liveness.scm 830 */
					obj_t BgL_defbodyz00_5166;

					BgL_defbodyz00_5166 =
						BGl_defusez00zzliveness_livenessz00(
						(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nz00_4658)))->BgL_bodyz00));
					{	/* Liveness/liveness.scm 831 */
						obj_t BgL_usebodyz00_5167;

						{	/* Liveness/liveness.scm 832 */
							obj_t BgL_tmpz00_5168;

							{	/* Liveness/liveness.scm 832 */
								int BgL_tmpz00_8214;

								BgL_tmpz00_8214 = (int) (1L);
								BgL_tmpz00_5168 = BGL_MVALUES_VAL(BgL_tmpz00_8214);
							}
							{	/* Liveness/liveness.scm 832 */
								int BgL_tmpz00_8217;

								BgL_tmpz00_8217 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_8217, BUNSPEC);
							}
							BgL_usebodyz00_5167 = BgL_tmpz00_5168;
						}
						{	/* Liveness/liveness.scm 833 */
							BgL_letzd2funzd2_bglt BgL_arg2425z00_5169;

							{	/* Liveness/liveness.scm 833 */
								BgL_letzd2funzf2livenessz20_bglt BgL_wide1353z00_5170;

								BgL_wide1353z00_5170 =
									((BgL_letzd2funzf2livenessz20_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_letzd2funzf2livenessz20_bgl))));
								{	/* Liveness/liveness.scm 833 */
									obj_t BgL_auxz00_8225;
									BgL_objectz00_bglt BgL_tmpz00_8221;

									BgL_auxz00_8225 = ((obj_t) BgL_wide1353z00_5170);
									BgL_tmpz00_8221 =
										((BgL_objectz00_bglt)
										((BgL_letzd2funzd2_bglt)
											((BgL_letzd2funzd2_bglt) BgL_nz00_4658)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8221, BgL_auxz00_8225);
								}
								((BgL_objectz00_bglt)
									((BgL_letzd2funzd2_bglt)
										((BgL_letzd2funzd2_bglt) BgL_nz00_4658)));
								{	/* Liveness/liveness.scm 833 */
									long BgL_arg2426z00_5171;

									BgL_arg2426z00_5171 =
										BGL_CLASS_NUM
										(BGl_letzd2funzf2livenessz20zzliveness_typesz00);
									BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
												(BgL_letzd2funzd2_bglt) ((BgL_letzd2funzd2_bglt)
													BgL_nz00_4658))), BgL_arg2426z00_5171);
								}
								((BgL_letzd2funzd2_bglt)
									((BgL_letzd2funzd2_bglt)
										((BgL_letzd2funzd2_bglt) BgL_nz00_4658)));
							}
							{
								BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8239;

								{
									obj_t BgL_auxz00_8240;

									{	/* Liveness/liveness.scm 834 */
										BgL_objectz00_bglt BgL_tmpz00_8241;

										BgL_tmpz00_8241 =
											((BgL_objectz00_bglt)
											((BgL_letzd2funzd2_bglt)
												((BgL_letzd2funzd2_bglt) BgL_nz00_4658)));
										BgL_auxz00_8240 = BGL_OBJECT_WIDENING(BgL_tmpz00_8241);
									}
									BgL_auxz00_8239 =
										((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8240);
								}
								((((BgL_letzd2funzf2livenessz20_bglt)
											COBJECT(BgL_auxz00_8239))->BgL_defz00) =
									((obj_t) BGl_unionz00zzliveness_setz00(BgL_defbodyz00_5166,
											BgL_defbindingsz00_5150)), BUNSPEC);
							}
							{
								BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8249;

								{
									obj_t BgL_auxz00_8250;

									{	/* Liveness/liveness.scm 835 */
										BgL_objectz00_bglt BgL_tmpz00_8251;

										BgL_tmpz00_8251 =
											((BgL_objectz00_bglt)
											((BgL_letzd2funzd2_bglt)
												((BgL_letzd2funzd2_bglt) BgL_nz00_4658)));
										BgL_auxz00_8250 = BGL_OBJECT_WIDENING(BgL_tmpz00_8251);
									}
									BgL_auxz00_8249 =
										((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8250);
								}
								((((BgL_letzd2funzf2livenessz20_bglt)
											COBJECT(BgL_auxz00_8249))->BgL_usez00) =
									((obj_t) BGl_unionz00zzliveness_setz00(BgL_usebodyz00_5167,
											BgL_usebindingsz00_5151)), BUNSPEC);
							}
							{
								BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8259;

								{
									obj_t BgL_auxz00_8260;

									{	/* Liveness/liveness.scm 831 */
										BgL_objectz00_bglt BgL_tmpz00_8261;

										BgL_tmpz00_8261 =
											((BgL_objectz00_bglt)
											((BgL_letzd2funzd2_bglt)
												((BgL_letzd2funzd2_bglt) BgL_nz00_4658)));
										BgL_auxz00_8260 = BGL_OBJECT_WIDENING(BgL_tmpz00_8261);
									}
									BgL_auxz00_8259 =
										((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8260);
								}
								((((BgL_letzd2funzf2livenessz20_bglt)
											COBJECT(BgL_auxz00_8259))->BgL_inz00) =
									((obj_t) BNIL), BUNSPEC);
							}
							{
								BgL_letzd2funzf2livenessz20_bglt BgL_auxz00_8268;

								{
									obj_t BgL_auxz00_8269;

									{	/* Liveness/liveness.scm 831 */
										BgL_objectz00_bglt BgL_tmpz00_8270;

										BgL_tmpz00_8270 =
											((BgL_objectz00_bglt)
											((BgL_letzd2funzd2_bglt)
												((BgL_letzd2funzd2_bglt) BgL_nz00_4658)));
										BgL_auxz00_8269 = BGL_OBJECT_WIDENING(BgL_tmpz00_8270);
									}
									BgL_auxz00_8268 =
										((BgL_letzd2funzf2livenessz20_bglt) BgL_auxz00_8269);
								}
								((((BgL_letzd2funzf2livenessz20_bglt)
											COBJECT(BgL_auxz00_8268))->BgL_outz00) =
									((obj_t) BNIL), BUNSPEC);
							}
							BgL_arg2425z00_5169 =
								((BgL_letzd2funzd2_bglt)
								((BgL_letzd2funzd2_bglt) BgL_nz00_4658));
							return
								BGl_defusez00zzliveness_livenessz00(
								((BgL_nodez00_bglt) BgL_arg2425z00_5169));
						}
					}
				}
			}
		}

	}



/* &inout-switch/livenes2032 */
	obj_t BGl_z62inoutzd2switchzf2livenes2032z42zzliveness_livenessz00(obj_t
		BgL_envz00_4659, obj_t BgL_nz00_4660)
	{
		{	/* Liveness/liveness.scm 807 */
			{	/* Liveness/liveness.scm 809 */
				obj_t BgL_val0_1752z00_5173;
				obj_t BgL_val1_1753z00_5174;

				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_8281;

					{
						obj_t BgL_auxz00_8282;

						{	/* Liveness/liveness.scm 809 */
							BgL_objectz00_bglt BgL_tmpz00_8283;

							BgL_tmpz00_8283 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4660));
							BgL_auxz00_8282 = BGL_OBJECT_WIDENING(BgL_tmpz00_8283);
						}
						BgL_auxz00_8281 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8282);
					}
					BgL_val0_1752z00_5173 =
						(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8281))->
						BgL_inz00);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_8289;

					{
						obj_t BgL_auxz00_8290;

						{	/* Liveness/liveness.scm 809 */
							BgL_objectz00_bglt BgL_tmpz00_8291;

							BgL_tmpz00_8291 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4660));
							BgL_auxz00_8290 = BGL_OBJECT_WIDENING(BgL_tmpz00_8291);
						}
						BgL_auxz00_8289 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8290);
					}
					BgL_val1_1753z00_5174 =
						(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8289))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 809 */
					int BgL_tmpz00_8297;

					BgL_tmpz00_8297 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8297);
				}
				{	/* Liveness/liveness.scm 809 */
					int BgL_tmpz00_8300;

					BgL_tmpz00_8300 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8300, BgL_val1_1753z00_5174);
				}
				return BgL_val0_1752z00_5173;
			}
		}

	}



/* &inout!-switch/livene2030 */
	obj_t BGl_z62inoutz12zd2switchzf2livene2030z50zzliveness_livenessz00(obj_t
		BgL_envz00_4661, obj_t BgL_nz00_4662, obj_t BgL_oz00_4663)
	{
		{	/* Liveness/liveness.scm 796 */
			{
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_8303;

				{
					obj_t BgL_auxz00_8304;

					{	/* Liveness/liveness.scm 799 */
						BgL_objectz00_bglt BgL_tmpz00_8305;

						BgL_tmpz00_8305 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4662));
						BgL_auxz00_8304 = BGL_OBJECT_WIDENING(BgL_tmpz00_8305);
					}
					BgL_auxz00_8303 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8304);
				}
				((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8303))->
						BgL_outz00) = ((obj_t) BgL_oz00_4663), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_8318;
				BgL_switchzf2livenesszf2_bglt BgL_auxz00_8311;

				{	/* Liveness/liveness.scm 800 */
					obj_t BgL_arg2404z00_5176;
					obj_t BgL_arg2405z00_5177;

					{
						BgL_switchzf2livenesszf2_bglt BgL_auxz00_8319;

						{
							obj_t BgL_auxz00_8320;

							{	/* Liveness/liveness.scm 800 */
								BgL_objectz00_bglt BgL_tmpz00_8321;

								BgL_tmpz00_8321 =
									((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4662));
								BgL_auxz00_8320 = BGL_OBJECT_WIDENING(BgL_tmpz00_8321);
							}
							BgL_auxz00_8319 =
								((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8320);
						}
						BgL_arg2404z00_5176 =
							(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8319))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 800 */
						obj_t BgL_arg2407z00_5178;
						obj_t BgL_arg2408z00_5179;

						{
							BgL_switchzf2livenesszf2_bglt BgL_auxz00_8327;

							{
								obj_t BgL_auxz00_8328;

								{	/* Liveness/liveness.scm 800 */
									BgL_objectz00_bglt BgL_tmpz00_8329;

									BgL_tmpz00_8329 =
										((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4662));
									BgL_auxz00_8328 = BGL_OBJECT_WIDENING(BgL_tmpz00_8329);
								}
								BgL_auxz00_8327 =
									((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8328);
							}
							BgL_arg2407z00_5178 =
								(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8327))->
								BgL_outz00);
						}
						{
							BgL_switchzf2livenesszf2_bglt BgL_auxz00_8335;

							{
								obj_t BgL_auxz00_8336;

								{	/* Liveness/liveness.scm 800 */
									BgL_objectz00_bglt BgL_tmpz00_8337;

									BgL_tmpz00_8337 =
										((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4662));
									BgL_auxz00_8336 = BGL_OBJECT_WIDENING(BgL_tmpz00_8337);
								}
								BgL_auxz00_8335 =
									((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8336);
							}
							BgL_arg2408z00_5179 =
								(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8335))->
								BgL_defz00);
						}
						BgL_arg2405z00_5177 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2407z00_5178,
							BgL_arg2408z00_5179);
					}
					BgL_auxz00_8318 =
						BGl_unionz00zzliveness_setz00(BgL_arg2404z00_5176,
						BgL_arg2405z00_5177);
				}
				{
					obj_t BgL_auxz00_8312;

					{	/* Liveness/liveness.scm 800 */
						BgL_objectz00_bglt BgL_tmpz00_8313;

						BgL_tmpz00_8313 =
							((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4662));
						BgL_auxz00_8312 = BGL_OBJECT_WIDENING(BgL_tmpz00_8313);
					}
					BgL_auxz00_8311 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8312);
				}
				((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8311))->
						BgL_inz00) = ((obj_t) BgL_auxz00_8318), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 803 */
				BgL_nodez00_bglt BgL_arg2410z00_5180;

				BgL_arg2410z00_5180 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt)
								((BgL_switchz00_bglt) BgL_nz00_4662))))->BgL_testz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2410z00_5180,
					BgL_oz00_4663);
			}
			{	/* Liveness/liveness.scm 804 */
				obj_t BgL_g1749z00_5181;

				BgL_g1749z00_5181 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt)
								((BgL_switchz00_bglt) BgL_nz00_4662))))->BgL_clausesz00);
				{
					obj_t BgL_l1747z00_5183;

					BgL_l1747z00_5183 = BgL_g1749z00_5181;
				BgL_zc3z04anonymousza32411ze3z87_5182:
					if (PAIRP(BgL_l1747z00_5183))
						{	/* Liveness/liveness.scm 804 */
							{	/* Liveness/liveness.scm 804 */
								obj_t BgL_cz00_5184;

								BgL_cz00_5184 = CAR(BgL_l1747z00_5183);
								{	/* Liveness/liveness.scm 804 */
									obj_t BgL_arg2414z00_5185;

									BgL_arg2414z00_5185 = CDR(((obj_t) BgL_cz00_5184));
									BGl_inoutz12z12zzliveness_livenessz00(
										((BgL_nodez00_bglt) BgL_arg2414z00_5185), BgL_oz00_4663);
								}
							}
							{
								obj_t BgL_l1747z00_8360;

								BgL_l1747z00_8360 = CDR(BgL_l1747z00_5183);
								BgL_l1747z00_5183 = BgL_l1747z00_8360;
								goto BgL_zc3z04anonymousza32411ze3z87_5182;
							}
						}
					else
						{	/* Liveness/liveness.scm 804 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 805 */
				obj_t BgL_val0_1750z00_5186;
				obj_t BgL_val1_1751z00_5187;

				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_8362;

					{
						obj_t BgL_auxz00_8363;

						{	/* Liveness/liveness.scm 805 */
							BgL_objectz00_bglt BgL_tmpz00_8364;

							BgL_tmpz00_8364 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4662));
							BgL_auxz00_8363 = BGL_OBJECT_WIDENING(BgL_tmpz00_8364);
						}
						BgL_auxz00_8362 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8363);
					}
					BgL_val0_1750z00_5186 =
						(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8362))->
						BgL_inz00);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_8370;

					{
						obj_t BgL_auxz00_8371;

						{	/* Liveness/liveness.scm 805 */
							BgL_objectz00_bglt BgL_tmpz00_8372;

							BgL_tmpz00_8372 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4662));
							BgL_auxz00_8371 = BGL_OBJECT_WIDENING(BgL_tmpz00_8372);
						}
						BgL_auxz00_8370 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8371);
					}
					BgL_val1_1751z00_5187 =
						(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8370))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 805 */
					int BgL_tmpz00_8378;

					BgL_tmpz00_8378 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8378);
				}
				{	/* Liveness/liveness.scm 805 */
					int BgL_tmpz00_8381;

					BgL_tmpz00_8381 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8381, BgL_val1_1751z00_5187);
				}
				return BgL_val0_1750z00_5186;
			}
		}

	}



/* &defuse-switch/livene2028 */
	obj_t BGl_z62defusezd2switchzf2livene2028z42zzliveness_livenessz00(obj_t
		BgL_envz00_4664, obj_t BgL_nz00_4665)
	{
		{	/* Liveness/liveness.scm 792 */
			{	/* Liveness/liveness.scm 794 */
				obj_t BgL_val0_1745z00_5189;
				obj_t BgL_val1_1746z00_5190;

				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_8384;

					{
						obj_t BgL_auxz00_8385;

						{	/* Liveness/liveness.scm 794 */
							BgL_objectz00_bglt BgL_tmpz00_8386;

							BgL_tmpz00_8386 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4665));
							BgL_auxz00_8385 = BGL_OBJECT_WIDENING(BgL_tmpz00_8386);
						}
						BgL_auxz00_8384 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8385);
					}
					BgL_val0_1745z00_5189 =
						(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8384))->
						BgL_defz00);
				}
				{
					BgL_switchzf2livenesszf2_bglt BgL_auxz00_8392;

					{
						obj_t BgL_auxz00_8393;

						{	/* Liveness/liveness.scm 794 */
							BgL_objectz00_bglt BgL_tmpz00_8394;

							BgL_tmpz00_8394 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4665));
							BgL_auxz00_8393 = BGL_OBJECT_WIDENING(BgL_tmpz00_8394);
						}
						BgL_auxz00_8392 = ((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8393);
					}
					BgL_val1_1746z00_5190 =
						(((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8392))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 794 */
					int BgL_tmpz00_8400;

					BgL_tmpz00_8400 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8400);
				}
				{	/* Liveness/liveness.scm 794 */
					int BgL_tmpz00_8403;

					BgL_tmpz00_8403 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8403, BgL_val1_1746z00_5190);
				}
				return BgL_val0_1745z00_5189;
			}
		}

	}



/* &defuse-switch2026 */
	obj_t BGl_z62defusezd2switch2026zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4666, obj_t BgL_nz00_4667)
	{
		{	/* Liveness/liveness.scm 774 */
			{	/* Liveness/liveness.scm 776 */
				obj_t BgL_deftestz00_5192;

				BgL_deftestz00_5192 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nz00_4667)))->BgL_testz00));
				{	/* Liveness/liveness.scm 777 */
					obj_t BgL_usetestz00_5193;

					{	/* Liveness/liveness.scm 779 */
						obj_t BgL_tmpz00_5194;

						{	/* Liveness/liveness.scm 779 */
							int BgL_tmpz00_8409;

							BgL_tmpz00_8409 = (int) (1L);
							BgL_tmpz00_5194 = BGL_MVALUES_VAL(BgL_tmpz00_8409);
						}
						{	/* Liveness/liveness.scm 779 */
							int BgL_tmpz00_8412;

							BgL_tmpz00_8412 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_8412, BUNSPEC);
						}
						BgL_usetestz00_5193 = BgL_tmpz00_5194;
					}
					{	/* Liveness/liveness.scm 779 */
						obj_t BgL_defsz00_5195;
						obj_t BgL_usesz00_5196;

						BgL_defsz00_5195 = BNIL;
						BgL_usesz00_5196 = BNIL;
						{	/* Liveness/liveness.scm 781 */
							obj_t BgL_g1744z00_5197;

							BgL_g1744z00_5197 =
								(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nz00_4667)))->BgL_clausesz00);
							{
								obj_t BgL_l1742z00_5199;

								BgL_l1742z00_5199 = BgL_g1744z00_5197;
							BgL_zc3z04anonymousza32390ze3z87_5198:
								if (PAIRP(BgL_l1742z00_5199))
									{	/* Liveness/liveness.scm 781 */
										{	/* Liveness/liveness.scm 782 */
											obj_t BgL_clausez00_5200;

											BgL_clausez00_5200 = CAR(BgL_l1742z00_5199);
											{	/* Liveness/liveness.scm 782 */
												obj_t BgL_defz00_5201;

												{	/* Liveness/liveness.scm 783 */
													obj_t BgL_arg2392z00_5202;

													BgL_arg2392z00_5202 =
														CDR(((obj_t) BgL_clausez00_5200));
													BgL_defz00_5201 =
														BGl_defusez00zzliveness_livenessz00(
														((BgL_nodez00_bglt) BgL_arg2392z00_5202));
												}
												{	/* Liveness/liveness.scm 783 */
													obj_t BgL_usez00_5203;

													{	/* Liveness/liveness.scm 784 */
														obj_t BgL_tmpz00_5204;

														{	/* Liveness/liveness.scm 784 */
															int BgL_tmpz00_8424;

															BgL_tmpz00_8424 = (int) (1L);
															BgL_tmpz00_5204 =
																BGL_MVALUES_VAL(BgL_tmpz00_8424);
														}
														{	/* Liveness/liveness.scm 784 */
															int BgL_tmpz00_8427;

															BgL_tmpz00_8427 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_8427, BUNSPEC);
														}
														BgL_usez00_5203 = BgL_tmpz00_5204;
													}
													BgL_defsz00_5195 =
														MAKE_YOUNG_PAIR(BgL_defz00_5201, BgL_defsz00_5195);
													BgL_usesz00_5196 =
														MAKE_YOUNG_PAIR(BgL_usez00_5203, BgL_usesz00_5196);
										}}}
										{
											obj_t BgL_l1742z00_8432;

											BgL_l1742z00_8432 = CDR(BgL_l1742z00_5199);
											BgL_l1742z00_5199 = BgL_l1742z00_8432;
											goto BgL_zc3z04anonymousza32390ze3z87_5198;
										}
									}
								else
									{	/* Liveness/liveness.scm 781 */
										((bool_t) 1);
									}
							}
						}
						{	/* Liveness/liveness.scm 788 */
							BgL_switchz00_bglt BgL_arg2395z00_5205;

							{	/* Liveness/liveness.scm 788 */
								BgL_switchzf2livenesszf2_bglt BgL_wide1340z00_5206;

								BgL_wide1340z00_5206 =
									((BgL_switchzf2livenesszf2_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_switchzf2livenesszf2_bgl))));
								{	/* Liveness/liveness.scm 788 */
									obj_t BgL_auxz00_8439;
									BgL_objectz00_bglt BgL_tmpz00_8435;

									BgL_auxz00_8439 = ((obj_t) BgL_wide1340z00_5206);
									BgL_tmpz00_8435 =
										((BgL_objectz00_bglt)
										((BgL_switchz00_bglt)
											((BgL_switchz00_bglt) BgL_nz00_4667)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8435, BgL_auxz00_8439);
								}
								((BgL_objectz00_bglt)
									((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4667)));
								{	/* Liveness/liveness.scm 788 */
									long BgL_arg2396z00_5207;

									BgL_arg2396z00_5207 =
										BGL_CLASS_NUM(BGl_switchzf2livenesszf2zzliveness_typesz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_switchz00_bglt)
												((BgL_switchz00_bglt) BgL_nz00_4667))),
										BgL_arg2396z00_5207);
								}
								((BgL_switchz00_bglt)
									((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4667)));
							}
							{
								obj_t BgL_auxz00_8461;
								BgL_switchzf2livenesszf2_bglt BgL_auxz00_8453;

								{	/* Liveness/liveness.scm 789 */
									obj_t BgL_arg2397z00_5208;

									{	/* Liveness/liveness.scm 789 */
										obj_t BgL_runner2398z00_5209;

										BgL_runner2398z00_5209 = BgL_defsz00_5195;
										BgL_arg2397z00_5208 =
											BGl_intersectionza2za2zzliveness_setz00
											(BgL_runner2398z00_5209);
									}
									BgL_auxz00_8461 =
										BGl_unionz00zzliveness_setz00(BgL_deftestz00_5192,
										BgL_arg2397z00_5208);
								}
								{
									obj_t BgL_auxz00_8454;

									{	/* Liveness/liveness.scm 789 */
										BgL_objectz00_bglt BgL_tmpz00_8455;

										BgL_tmpz00_8455 =
											((BgL_objectz00_bglt)
											((BgL_switchz00_bglt)
												((BgL_switchz00_bglt) BgL_nz00_4667)));
										BgL_auxz00_8454 = BGL_OBJECT_WIDENING(BgL_tmpz00_8455);
									}
									BgL_auxz00_8453 =
										((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8454);
								}
								((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8453))->
										BgL_defz00) = ((obj_t) BgL_auxz00_8461), BUNSPEC);
							}
							{
								obj_t BgL_auxz00_8473;
								BgL_switchzf2livenesszf2_bglt BgL_auxz00_8465;

								{	/* Liveness/liveness.scm 790 */
									obj_t BgL_arg2399z00_5210;

									{	/* Liveness/liveness.scm 790 */
										obj_t BgL_arg2401z00_5211;

										{	/* Liveness/liveness.scm 790 */
											obj_t BgL_runner2402z00_5212;

											BgL_runner2402z00_5212 = BgL_usesz00_5196;
											BgL_arg2401z00_5211 =
												BGl_unionza2za2zzliveness_setz00
												(BgL_runner2402z00_5212);
										}
										BgL_arg2399z00_5210 =
											BGl_disjonctionz00zzliveness_setz00(BgL_arg2401z00_5211,
											BgL_deftestz00_5192);
									}
									BgL_auxz00_8473 =
										BGl_unionz00zzliveness_setz00(BgL_usetestz00_5193,
										BgL_arg2399z00_5210);
								}
								{
									obj_t BgL_auxz00_8466;

									{	/* Liveness/liveness.scm 790 */
										BgL_objectz00_bglt BgL_tmpz00_8467;

										BgL_tmpz00_8467 =
											((BgL_objectz00_bglt)
											((BgL_switchz00_bglt)
												((BgL_switchz00_bglt) BgL_nz00_4667)));
										BgL_auxz00_8466 = BGL_OBJECT_WIDENING(BgL_tmpz00_8467);
									}
									BgL_auxz00_8465 =
										((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8466);
								}
								((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8465))->
										BgL_usez00) = ((obj_t) BgL_auxz00_8473), BUNSPEC);
							}
							{
								BgL_switchzf2livenesszf2_bglt BgL_auxz00_8478;

								{
									obj_t BgL_auxz00_8479;

									{	/* Liveness/liveness.scm 779 */
										BgL_objectz00_bglt BgL_tmpz00_8480;

										BgL_tmpz00_8480 =
											((BgL_objectz00_bglt)
											((BgL_switchz00_bglt)
												((BgL_switchz00_bglt) BgL_nz00_4667)));
										BgL_auxz00_8479 = BGL_OBJECT_WIDENING(BgL_tmpz00_8480);
									}
									BgL_auxz00_8478 =
										((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8479);
								}
								((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8478))->
										BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
							}
							{
								BgL_switchzf2livenesszf2_bglt BgL_auxz00_8487;

								{
									obj_t BgL_auxz00_8488;

									{	/* Liveness/liveness.scm 779 */
										BgL_objectz00_bglt BgL_tmpz00_8489;

										BgL_tmpz00_8489 =
											((BgL_objectz00_bglt)
											((BgL_switchz00_bglt)
												((BgL_switchz00_bglt) BgL_nz00_4667)));
										BgL_auxz00_8488 = BGL_OBJECT_WIDENING(BgL_tmpz00_8489);
									}
									BgL_auxz00_8487 =
										((BgL_switchzf2livenesszf2_bglt) BgL_auxz00_8488);
								}
								((((BgL_switchzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8487))->
										BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
							}
							BgL_arg2395z00_5205 =
								((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_nz00_4667));
							return
								BGl_defusez00zzliveness_livenessz00(
								((BgL_nodez00_bglt) BgL_arg2395z00_5205));
						}
					}
				}
			}
		}

	}



/* &inout-fail/liveness2024 */
	obj_t BGl_z62inoutzd2failzf2liveness2024z42zzliveness_livenessz00(obj_t
		BgL_envz00_4668, obj_t BgL_nz00_4669)
	{
		{	/* Liveness/liveness.scm 767 */
			{	/* Liveness/liveness.scm 769 */
				obj_t BgL_val0_1740z00_5214;
				obj_t BgL_val1_1741z00_5215;

				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_8500;

					{
						obj_t BgL_auxz00_8501;

						{	/* Liveness/liveness.scm 769 */
							BgL_objectz00_bglt BgL_tmpz00_8502;

							BgL_tmpz00_8502 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4669));
							BgL_auxz00_8501 = BGL_OBJECT_WIDENING(BgL_tmpz00_8502);
						}
						BgL_auxz00_8500 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8501);
					}
					BgL_val0_1740z00_5214 =
						(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8500))->
						BgL_inz00);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_8508;

					{
						obj_t BgL_auxz00_8509;

						{	/* Liveness/liveness.scm 769 */
							BgL_objectz00_bglt BgL_tmpz00_8510;

							BgL_tmpz00_8510 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4669));
							BgL_auxz00_8509 = BGL_OBJECT_WIDENING(BgL_tmpz00_8510);
						}
						BgL_auxz00_8508 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8509);
					}
					BgL_val1_1741z00_5215 =
						(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8508))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 769 */
					int BgL_tmpz00_8516;

					BgL_tmpz00_8516 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8516);
				}
				{	/* Liveness/liveness.scm 769 */
					int BgL_tmpz00_8519;

					BgL_tmpz00_8519 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8519, BgL_val1_1741z00_5215);
				}
				return BgL_val0_1740z00_5214;
			}
		}

	}



/* &inout!-fail/liveness2022 */
	obj_t BGl_z62inoutz12zd2failzf2liveness2022z50zzliveness_livenessz00(obj_t
		BgL_envz00_4670, obj_t BgL_nz00_4671, obj_t BgL_oz00_4672)
	{
		{	/* Liveness/liveness.scm 757 */
			{
				BgL_failzf2livenesszf2_bglt BgL_auxz00_8522;

				{
					obj_t BgL_auxz00_8523;

					{	/* Liveness/liveness.scm 760 */
						BgL_objectz00_bglt BgL_tmpz00_8524;

						BgL_tmpz00_8524 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4671));
						BgL_auxz00_8523 = BGL_OBJECT_WIDENING(BgL_tmpz00_8524);
					}
					BgL_auxz00_8522 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8523);
				}
				((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8522))->
						BgL_outz00) = ((obj_t) BgL_oz00_4672), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_8537;
				BgL_failzf2livenesszf2_bglt BgL_auxz00_8530;

				{	/* Liveness/liveness.scm 761 */
					obj_t BgL_arg2383z00_5217;
					obj_t BgL_arg2384z00_5218;

					{
						BgL_failzf2livenesszf2_bglt BgL_auxz00_8538;

						{
							obj_t BgL_auxz00_8539;

							{	/* Liveness/liveness.scm 761 */
								BgL_objectz00_bglt BgL_tmpz00_8540;

								BgL_tmpz00_8540 =
									((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4671));
								BgL_auxz00_8539 = BGL_OBJECT_WIDENING(BgL_tmpz00_8540);
							}
							BgL_auxz00_8538 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8539);
						}
						BgL_arg2383z00_5217 =
							(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8538))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 761 */
						obj_t BgL_arg2385z00_5219;
						obj_t BgL_arg2386z00_5220;

						{
							BgL_failzf2livenesszf2_bglt BgL_auxz00_8546;

							{
								obj_t BgL_auxz00_8547;

								{	/* Liveness/liveness.scm 761 */
									BgL_objectz00_bglt BgL_tmpz00_8548;

									BgL_tmpz00_8548 =
										((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4671));
									BgL_auxz00_8547 = BGL_OBJECT_WIDENING(BgL_tmpz00_8548);
								}
								BgL_auxz00_8546 =
									((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8547);
							}
							BgL_arg2385z00_5219 =
								(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8546))->
								BgL_outz00);
						}
						{
							BgL_failzf2livenesszf2_bglt BgL_auxz00_8554;

							{
								obj_t BgL_auxz00_8555;

								{	/* Liveness/liveness.scm 761 */
									BgL_objectz00_bglt BgL_tmpz00_8556;

									BgL_tmpz00_8556 =
										((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4671));
									BgL_auxz00_8555 = BGL_OBJECT_WIDENING(BgL_tmpz00_8556);
								}
								BgL_auxz00_8554 =
									((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8555);
							}
							BgL_arg2386z00_5220 =
								(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8554))->
								BgL_defz00);
						}
						BgL_arg2384z00_5218 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2385z00_5219,
							BgL_arg2386z00_5220);
					}
					BgL_auxz00_8537 =
						BGl_unionz00zzliveness_setz00(BgL_arg2383z00_5217,
						BgL_arg2384z00_5218);
				}
				{
					obj_t BgL_auxz00_8531;

					{	/* Liveness/liveness.scm 761 */
						BgL_objectz00_bglt BgL_tmpz00_8532;

						BgL_tmpz00_8532 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4671));
						BgL_auxz00_8531 = BGL_OBJECT_WIDENING(BgL_tmpz00_8532);
					}
					BgL_auxz00_8530 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8531);
				}
				((((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8530))->BgL_inz00) =
					((obj_t) BgL_auxz00_8537), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 762 */
				BgL_nodez00_bglt BgL_arg2387z00_5221;

				BgL_arg2387z00_5221 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt)
								((BgL_failz00_bglt) BgL_nz00_4671))))->BgL_procz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2387z00_5221,
					BgL_oz00_4672);
			}
			{	/* Liveness/liveness.scm 763 */
				BgL_nodez00_bglt BgL_arg2388z00_5222;

				BgL_arg2388z00_5222 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt)
								((BgL_failz00_bglt) BgL_nz00_4671))))->BgL_msgz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2388z00_5222,
					BgL_oz00_4672);
			}
			{	/* Liveness/liveness.scm 764 */
				BgL_nodez00_bglt BgL_arg2389z00_5223;

				BgL_arg2389z00_5223 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt)
								((BgL_failz00_bglt) BgL_nz00_4671))))->BgL_objz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2389z00_5223,
					BgL_oz00_4672);
			}
			{	/* Liveness/liveness.scm 765 */
				obj_t BgL_val0_1738z00_5224;
				obj_t BgL_val1_1739z00_5225;

				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_8577;

					{
						obj_t BgL_auxz00_8578;

						{	/* Liveness/liveness.scm 765 */
							BgL_objectz00_bglt BgL_tmpz00_8579;

							BgL_tmpz00_8579 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4671));
							BgL_auxz00_8578 = BGL_OBJECT_WIDENING(BgL_tmpz00_8579);
						}
						BgL_auxz00_8577 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8578);
					}
					BgL_val0_1738z00_5224 =
						(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8577))->
						BgL_inz00);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_8585;

					{
						obj_t BgL_auxz00_8586;

						{	/* Liveness/liveness.scm 765 */
							BgL_objectz00_bglt BgL_tmpz00_8587;

							BgL_tmpz00_8587 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4671));
							BgL_auxz00_8586 = BGL_OBJECT_WIDENING(BgL_tmpz00_8587);
						}
						BgL_auxz00_8585 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8586);
					}
					BgL_val1_1739z00_5225 =
						(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8585))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 765 */
					int BgL_tmpz00_8593;

					BgL_tmpz00_8593 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8593);
				}
				{	/* Liveness/liveness.scm 765 */
					int BgL_tmpz00_8596;

					BgL_tmpz00_8596 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8596, BgL_val1_1739z00_5225);
				}
				return BgL_val0_1738z00_5224;
			}
		}

	}



/* &defuse-fail/liveness2020 */
	obj_t BGl_z62defusezd2failzf2liveness2020z42zzliveness_livenessz00(obj_t
		BgL_envz00_4673, obj_t BgL_nz00_4674)
	{
		{	/* Liveness/liveness.scm 753 */
			{	/* Liveness/liveness.scm 755 */
				obj_t BgL_val0_1736z00_5227;
				obj_t BgL_val1_1737z00_5228;

				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_8599;

					{
						obj_t BgL_auxz00_8600;

						{	/* Liveness/liveness.scm 755 */
							BgL_objectz00_bglt BgL_tmpz00_8601;

							BgL_tmpz00_8601 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4674));
							BgL_auxz00_8600 = BGL_OBJECT_WIDENING(BgL_tmpz00_8601);
						}
						BgL_auxz00_8599 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8600);
					}
					BgL_val0_1736z00_5227 =
						(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8599))->
						BgL_defz00);
				}
				{
					BgL_failzf2livenesszf2_bglt BgL_auxz00_8607;

					{
						obj_t BgL_auxz00_8608;

						{	/* Liveness/liveness.scm 755 */
							BgL_objectz00_bglt BgL_tmpz00_8609;

							BgL_tmpz00_8609 =
								((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4674));
							BgL_auxz00_8608 = BGL_OBJECT_WIDENING(BgL_tmpz00_8609);
						}
						BgL_auxz00_8607 = ((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8608);
					}
					BgL_val1_1737z00_5228 =
						(((BgL_failzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8607))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 755 */
					int BgL_tmpz00_8615;

					BgL_tmpz00_8615 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8615);
				}
				{	/* Liveness/liveness.scm 755 */
					int BgL_tmpz00_8618;

					BgL_tmpz00_8618 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8618, BgL_val1_1737z00_5228);
				}
				return BgL_val0_1736z00_5227;
			}
		}

	}



/* &defuse-fail2018 */
	obj_t BGl_z62defusezd2fail2018zb0zzliveness_livenessz00(obj_t BgL_envz00_4675,
		obj_t BgL_nz00_4676)
	{
		{	/* Liveness/liveness.scm 740 */
			{	/* Liveness/liveness.scm 742 */
				obj_t BgL_defprocz00_5230;

				BgL_defprocz00_5230 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_4676)))->BgL_procz00));
				{	/* Liveness/liveness.scm 743 */
					obj_t BgL_useprocz00_5231;

					{	/* Liveness/liveness.scm 744 */
						obj_t BgL_tmpz00_5232;

						{	/* Liveness/liveness.scm 744 */
							int BgL_tmpz00_8624;

							BgL_tmpz00_8624 = (int) (1L);
							BgL_tmpz00_5232 = BGL_MVALUES_VAL(BgL_tmpz00_8624);
						}
						{	/* Liveness/liveness.scm 744 */
							int BgL_tmpz00_8627;

							BgL_tmpz00_8627 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_8627, BUNSPEC);
						}
						BgL_useprocz00_5231 = BgL_tmpz00_5232;
					}
					{	/* Liveness/liveness.scm 744 */
						obj_t BgL_defmsgz00_5233;

						BgL_defmsgz00_5233 =
							BGl_defusez00zzliveness_livenessz00(
							(((BgL_failz00_bglt) COBJECT(
										((BgL_failz00_bglt) BgL_nz00_4676)))->BgL_msgz00));
						{	/* Liveness/liveness.scm 745 */
							obj_t BgL_usemsgz00_5234;

							{	/* Liveness/liveness.scm 746 */
								obj_t BgL_tmpz00_5235;

								{	/* Liveness/liveness.scm 746 */
									int BgL_tmpz00_8633;

									BgL_tmpz00_8633 = (int) (1L);
									BgL_tmpz00_5235 = BGL_MVALUES_VAL(BgL_tmpz00_8633);
								}
								{	/* Liveness/liveness.scm 746 */
									int BgL_tmpz00_8636;

									BgL_tmpz00_8636 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_8636, BUNSPEC);
								}
								BgL_usemsgz00_5234 = BgL_tmpz00_5235;
							}
							{	/* Liveness/liveness.scm 746 */
								obj_t BgL_defobjz00_5236;

								BgL_defobjz00_5236 =
									BGl_defusez00zzliveness_livenessz00(
									(((BgL_failz00_bglt) COBJECT(
												((BgL_failz00_bglt) BgL_nz00_4676)))->BgL_objz00));
								{	/* Liveness/liveness.scm 747 */
									obj_t BgL_useobjz00_5237;

									{	/* Liveness/liveness.scm 748 */
										obj_t BgL_tmpz00_5238;

										{	/* Liveness/liveness.scm 748 */
											int BgL_tmpz00_8642;

											BgL_tmpz00_8642 = (int) (1L);
											BgL_tmpz00_5238 = BGL_MVALUES_VAL(BgL_tmpz00_8642);
										}
										{	/* Liveness/liveness.scm 748 */
											int BgL_tmpz00_8645;

											BgL_tmpz00_8645 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_8645, BUNSPEC);
										}
										BgL_useobjz00_5237 = BgL_tmpz00_5238;
									}
									{	/* Liveness/liveness.scm 749 */
										BgL_failz00_bglt BgL_arg2378z00_5239;

										{	/* Liveness/liveness.scm 749 */
											BgL_failzf2livenesszf2_bglt BgL_wide1332z00_5240;

											BgL_wide1332z00_5240 =
												((BgL_failzf2livenesszf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_failzf2livenesszf2_bgl))));
											{	/* Liveness/liveness.scm 749 */
												obj_t BgL_auxz00_8653;
												BgL_objectz00_bglt BgL_tmpz00_8649;

												BgL_auxz00_8653 = ((obj_t) BgL_wide1332z00_5240);
												BgL_tmpz00_8649 =
													((BgL_objectz00_bglt)
													((BgL_failz00_bglt)
														((BgL_failz00_bglt) BgL_nz00_4676)));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8649,
													BgL_auxz00_8653);
											}
											((BgL_objectz00_bglt)
												((BgL_failz00_bglt)
													((BgL_failz00_bglt) BgL_nz00_4676)));
											{	/* Liveness/liveness.scm 749 */
												long BgL_arg2379z00_5241;

												BgL_arg2379z00_5241 =
													BGL_CLASS_NUM
													(BGl_failzf2livenesszf2zzliveness_typesz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_failz00_bglt) ((BgL_failz00_bglt)
																BgL_nz00_4676))), BgL_arg2379z00_5241);
											}
											((BgL_failz00_bglt)
												((BgL_failz00_bglt)
													((BgL_failz00_bglt) BgL_nz00_4676)));
										}
										{
											BgL_failzf2livenesszf2_bglt BgL_auxz00_8667;

											{
												obj_t BgL_auxz00_8668;

												{	/* Liveness/liveness.scm 750 */
													BgL_objectz00_bglt BgL_tmpz00_8669;

													BgL_tmpz00_8669 =
														((BgL_objectz00_bglt)
														((BgL_failz00_bglt)
															((BgL_failz00_bglt) BgL_nz00_4676)));
													BgL_auxz00_8668 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8669);
												}
												BgL_auxz00_8667 =
													((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8668);
											}
											((((BgL_failzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_8667))->BgL_defz00) =
												((obj_t)
													BGl_union3z00zzliveness_setz00(BgL_defprocz00_5230,
														BgL_defmsgz00_5233, BgL_defobjz00_5236)), BUNSPEC);
										}
										{
											BgL_failzf2livenesszf2_bglt BgL_auxz00_8677;

											{
												obj_t BgL_auxz00_8678;

												{	/* Liveness/liveness.scm 751 */
													BgL_objectz00_bglt BgL_tmpz00_8679;

													BgL_tmpz00_8679 =
														((BgL_objectz00_bglt)
														((BgL_failz00_bglt)
															((BgL_failz00_bglt) BgL_nz00_4676)));
													BgL_auxz00_8678 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8679);
												}
												BgL_auxz00_8677 =
													((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8678);
											}
											((((BgL_failzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_8677))->BgL_usez00) =
												((obj_t)
													BGl_union3z00zzliveness_setz00(BgL_useprocz00_5231,
														BgL_usemsgz00_5234, BgL_useobjz00_5237)), BUNSPEC);
										}
										{
											BgL_failzf2livenesszf2_bglt BgL_auxz00_8687;

											{
												obj_t BgL_auxz00_8688;

												{	/* Liveness/liveness.scm 747 */
													BgL_objectz00_bglt BgL_tmpz00_8689;

													BgL_tmpz00_8689 =
														((BgL_objectz00_bglt)
														((BgL_failz00_bglt)
															((BgL_failz00_bglt) BgL_nz00_4676)));
													BgL_auxz00_8688 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8689);
												}
												BgL_auxz00_8687 =
													((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8688);
											}
											((((BgL_failzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_8687))->BgL_inz00) =
												((obj_t) BNIL), BUNSPEC);
										}
										{
											BgL_failzf2livenesszf2_bglt BgL_auxz00_8696;

											{
												obj_t BgL_auxz00_8697;

												{	/* Liveness/liveness.scm 747 */
													BgL_objectz00_bglt BgL_tmpz00_8698;

													BgL_tmpz00_8698 =
														((BgL_objectz00_bglt)
														((BgL_failz00_bglt)
															((BgL_failz00_bglt) BgL_nz00_4676)));
													BgL_auxz00_8697 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8698);
												}
												BgL_auxz00_8696 =
													((BgL_failzf2livenesszf2_bglt) BgL_auxz00_8697);
											}
											((((BgL_failzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_8696))->BgL_outz00) =
												((obj_t) BNIL), BUNSPEC);
										}
										BgL_arg2378z00_5239 =
											((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_nz00_4676));
										return
											BGl_defusez00zzliveness_livenessz00(
											((BgL_nodez00_bglt) BgL_arg2378z00_5239));
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &inout-conditional/li2016 */
	obj_t BGl_z62inoutzd2conditionalzf2li2016z42zzliveness_livenessz00(obj_t
		BgL_envz00_4677, obj_t BgL_nz00_4678)
	{
		{	/* Liveness/liveness.scm 733 */
			{	/* Liveness/liveness.scm 735 */
				obj_t BgL_val0_1734z00_5243;
				obj_t BgL_val1_1735z00_5244;

				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8709;

					{
						obj_t BgL_auxz00_8710;

						{	/* Liveness/liveness.scm 735 */
							BgL_objectz00_bglt BgL_tmpz00_8711;

							BgL_tmpz00_8711 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nz00_4678));
							BgL_auxz00_8710 = BGL_OBJECT_WIDENING(BgL_tmpz00_8711);
						}
						BgL_auxz00_8709 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8710);
					}
					BgL_val0_1734z00_5243 =
						(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8709))->
						BgL_inz00);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8717;

					{
						obj_t BgL_auxz00_8718;

						{	/* Liveness/liveness.scm 735 */
							BgL_objectz00_bglt BgL_tmpz00_8719;

							BgL_tmpz00_8719 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nz00_4678));
							BgL_auxz00_8718 = BGL_OBJECT_WIDENING(BgL_tmpz00_8719);
						}
						BgL_auxz00_8717 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8718);
					}
					BgL_val1_1735z00_5244 =
						(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8717))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 735 */
					int BgL_tmpz00_8725;

					BgL_tmpz00_8725 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8725);
				}
				{	/* Liveness/liveness.scm 735 */
					int BgL_tmpz00_8728;

					BgL_tmpz00_8728 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8728, BgL_val1_1735z00_5244);
				}
				return BgL_val0_1734z00_5243;
			}
		}

	}



/* &inout!-conditional/l2014 */
	obj_t BGl_z62inoutz12zd2conditionalzf2l2014z50zzliveness_livenessz00(obj_t
		BgL_envz00_4679, obj_t BgL_nz00_4680, obj_t BgL_oz00_4681)
	{
		{	/* Liveness/liveness.scm 719 */
			{
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8731;

				{
					obj_t BgL_auxz00_8732;

					{	/* Liveness/liveness.scm 722 */
						BgL_objectz00_bglt BgL_tmpz00_8733;

						BgL_tmpz00_8733 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_nz00_4680));
						BgL_auxz00_8732 = BGL_OBJECT_WIDENING(BgL_tmpz00_8733);
					}
					BgL_auxz00_8731 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8732);
				}
				((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8731))->
						BgL_outz00) = ((obj_t) BgL_oz00_4681), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 723 */
				BgL_nodez00_bglt BgL_arg2365z00_5246;

				BgL_arg2365z00_5246 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nz00_4680))))->BgL_truez00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2365z00_5246,
					BgL_oz00_4681);
			}
			{	/* Liveness/liveness.scm 724 */
				BgL_nodez00_bglt BgL_arg2366z00_5247;

				BgL_arg2366z00_5247 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nz00_4680))))->BgL_falsez00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2366z00_5247,
					BgL_oz00_4681);
			}
			{	/* Liveness/liveness.scm 725 */
				obj_t BgL_tdefz00_5248;

				{	/* Liveness/liveness.scm 726 */
					BgL_nodez00_bglt BgL_arg2373z00_5249;

					BgL_arg2373z00_5249 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt)
									((BgL_conditionalz00_bglt) BgL_nz00_4680))))->BgL_truez00);
					BgL_tdefz00_5248 =
						BGl_defusez00zzliveness_livenessz00(BgL_arg2373z00_5249);
				}
				{	/* Liveness/liveness.scm 726 */
					obj_t BgL_tusez00_5250;

					{	/* Liveness/liveness.scm 727 */
						obj_t BgL_tmpz00_5251;

						{	/* Liveness/liveness.scm 727 */
							int BgL_tmpz00_8751;

							BgL_tmpz00_8751 = (int) (1L);
							BgL_tmpz00_5251 = BGL_MVALUES_VAL(BgL_tmpz00_8751);
						}
						{	/* Liveness/liveness.scm 727 */
							int BgL_tmpz00_8754;

							BgL_tmpz00_8754 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_8754, BUNSPEC);
						}
						BgL_tusez00_5250 = BgL_tmpz00_5251;
					}
					{	/* Liveness/liveness.scm 727 */
						obj_t BgL_fdefz00_5252;

						{	/* Liveness/liveness.scm 728 */
							BgL_nodez00_bglt BgL_arg2371z00_5253;

							BgL_arg2371z00_5253 =
								(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt)
											((BgL_conditionalz00_bglt) BgL_nz00_4680))))->
								BgL_falsez00);
							BgL_fdefz00_5252 =
								BGl_defusez00zzliveness_livenessz00(BgL_arg2371z00_5253);
						}
						{	/* Liveness/liveness.scm 728 */
							obj_t BgL_fusez00_5254;

							{	/* Liveness/liveness.scm 729 */
								obj_t BgL_tmpz00_5255;

								{	/* Liveness/liveness.scm 729 */
									int BgL_tmpz00_8761;

									BgL_tmpz00_8761 = (int) (1L);
									BgL_tmpz00_5255 = BGL_MVALUES_VAL(BgL_tmpz00_8761);
								}
								{	/* Liveness/liveness.scm 729 */
									int BgL_tmpz00_8764;

									BgL_tmpz00_8764 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_8764, BUNSPEC);
								}
								BgL_fusez00_5254 = BgL_tmpz00_5255;
							}
							{	/* Liveness/liveness.scm 729 */
								BgL_nodez00_bglt BgL_arg2367z00_5256;
								obj_t BgL_arg2368z00_5257;

								BgL_arg2367z00_5256 =
									(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt)
												((BgL_conditionalz00_bglt) BgL_nz00_4680))))->
									BgL_testz00);
								{	/* Liveness/liveness.scm 729 */
									obj_t BgL_arg2369z00_5258;
									obj_t BgL_arg2370z00_5259;

									{
										BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8770;

										{
											obj_t BgL_auxz00_8771;

											{	/* Liveness/liveness.scm 729 */
												BgL_objectz00_bglt BgL_tmpz00_8772;

												BgL_tmpz00_8772 =
													((BgL_objectz00_bglt)
													((BgL_conditionalz00_bglt) BgL_nz00_4680));
												BgL_auxz00_8771 = BGL_OBJECT_WIDENING(BgL_tmpz00_8772);
											}
											BgL_auxz00_8770 =
												((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8771);
										}
										BgL_arg2369z00_5258 =
											(((BgL_conditionalzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_8770))->BgL_outz00);
									}
									BgL_arg2370z00_5259 =
										BGl_intersectionz00zzliveness_setz00(BgL_tdefz00_5248,
										BgL_fdefz00_5252);
									BgL_arg2368z00_5257 =
										BGl_disjonctionz00zzliveness_setz00(BgL_arg2369z00_5258,
										BgL_arg2370z00_5259);
								}
								BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2367z00_5256,
									BgL_arg2368z00_5257);
			}}}}}
			{
				obj_t BgL_auxz00_8788;
				BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8781;

				{	/* Liveness/liveness.scm 730 */
					obj_t BgL_arg2374z00_5260;
					obj_t BgL_arg2375z00_5261;

					{
						BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8789;

						{
							obj_t BgL_auxz00_8790;

							{	/* Liveness/liveness.scm 730 */
								BgL_objectz00_bglt BgL_tmpz00_8791;

								BgL_tmpz00_8791 =
									((BgL_objectz00_bglt)
									((BgL_conditionalz00_bglt) BgL_nz00_4680));
								BgL_auxz00_8790 = BGL_OBJECT_WIDENING(BgL_tmpz00_8791);
							}
							BgL_auxz00_8789 =
								((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8790);
						}
						BgL_arg2374z00_5260 =
							(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8789))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 730 */
						obj_t BgL_arg2376z00_5262;
						obj_t BgL_arg2377z00_5263;

						{
							BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8797;

							{
								obj_t BgL_auxz00_8798;

								{	/* Liveness/liveness.scm 730 */
									BgL_objectz00_bglt BgL_tmpz00_8799;

									BgL_tmpz00_8799 =
										((BgL_objectz00_bglt)
										((BgL_conditionalz00_bglt) BgL_nz00_4680));
									BgL_auxz00_8798 = BGL_OBJECT_WIDENING(BgL_tmpz00_8799);
								}
								BgL_auxz00_8797 =
									((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8798);
							}
							BgL_arg2376z00_5262 =
								(((BgL_conditionalzf2livenesszf2_bglt)
									COBJECT(BgL_auxz00_8797))->BgL_outz00);
						}
						{
							BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8805;

							{
								obj_t BgL_auxz00_8806;

								{	/* Liveness/liveness.scm 730 */
									BgL_objectz00_bglt BgL_tmpz00_8807;

									BgL_tmpz00_8807 =
										((BgL_objectz00_bglt)
										((BgL_conditionalz00_bglt) BgL_nz00_4680));
									BgL_auxz00_8806 = BGL_OBJECT_WIDENING(BgL_tmpz00_8807);
								}
								BgL_auxz00_8805 =
									((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8806);
							}
							BgL_arg2377z00_5263 =
								(((BgL_conditionalzf2livenesszf2_bglt)
									COBJECT(BgL_auxz00_8805))->BgL_defz00);
						}
						BgL_arg2375z00_5261 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2376z00_5262,
							BgL_arg2377z00_5263);
					}
					BgL_auxz00_8788 =
						BGl_unionz00zzliveness_setz00(BgL_arg2374z00_5260,
						BgL_arg2375z00_5261);
				}
				{
					obj_t BgL_auxz00_8782;

					{	/* Liveness/liveness.scm 730 */
						BgL_objectz00_bglt BgL_tmpz00_8783;

						BgL_tmpz00_8783 =
							((BgL_objectz00_bglt) ((BgL_conditionalz00_bglt) BgL_nz00_4680));
						BgL_auxz00_8782 = BGL_OBJECT_WIDENING(BgL_tmpz00_8783);
					}
					BgL_auxz00_8781 =
						((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8782);
				}
				((((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8781))->
						BgL_inz00) = ((obj_t) BgL_auxz00_8788), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 731 */
				obj_t BgL_val0_1732z00_5264;
				obj_t BgL_val1_1733z00_5265;

				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8816;

					{
						obj_t BgL_auxz00_8817;

						{	/* Liveness/liveness.scm 731 */
							BgL_objectz00_bglt BgL_tmpz00_8818;

							BgL_tmpz00_8818 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nz00_4680));
							BgL_auxz00_8817 = BGL_OBJECT_WIDENING(BgL_tmpz00_8818);
						}
						BgL_auxz00_8816 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8817);
					}
					BgL_val0_1732z00_5264 =
						(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8816))->
						BgL_inz00);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8824;

					{
						obj_t BgL_auxz00_8825;

						{	/* Liveness/liveness.scm 731 */
							BgL_objectz00_bglt BgL_tmpz00_8826;

							BgL_tmpz00_8826 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nz00_4680));
							BgL_auxz00_8825 = BGL_OBJECT_WIDENING(BgL_tmpz00_8826);
						}
						BgL_auxz00_8824 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8825);
					}
					BgL_val1_1733z00_5265 =
						(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8824))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 731 */
					int BgL_tmpz00_8832;

					BgL_tmpz00_8832 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8832);
				}
				{	/* Liveness/liveness.scm 731 */
					int BgL_tmpz00_8835;

					BgL_tmpz00_8835 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8835, BgL_val1_1733z00_5265);
				}
				return BgL_val0_1732z00_5264;
			}
		}

	}



/* &defuse-conditional/l2012 */
	obj_t BGl_z62defusezd2conditionalzf2l2012z42zzliveness_livenessz00(obj_t
		BgL_envz00_4682, obj_t BgL_nz00_4683)
	{
		{	/* Liveness/liveness.scm 715 */
			{	/* Liveness/liveness.scm 717 */
				obj_t BgL_val0_1730z00_5267;
				obj_t BgL_val1_1731z00_5268;

				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8838;

					{
						obj_t BgL_auxz00_8839;

						{	/* Liveness/liveness.scm 717 */
							BgL_objectz00_bglt BgL_tmpz00_8840;

							BgL_tmpz00_8840 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nz00_4683));
							BgL_auxz00_8839 = BGL_OBJECT_WIDENING(BgL_tmpz00_8840);
						}
						BgL_auxz00_8838 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8839);
					}
					BgL_val0_1730z00_5267 =
						(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8838))->
						BgL_defz00);
				}
				{
					BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8846;

					{
						obj_t BgL_auxz00_8847;

						{	/* Liveness/liveness.scm 717 */
							BgL_objectz00_bglt BgL_tmpz00_8848;

							BgL_tmpz00_8848 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nz00_4683));
							BgL_auxz00_8847 = BGL_OBJECT_WIDENING(BgL_tmpz00_8848);
						}
						BgL_auxz00_8846 =
							((BgL_conditionalzf2livenesszf2_bglt) BgL_auxz00_8847);
					}
					BgL_val1_1731z00_5268 =
						(((BgL_conditionalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8846))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 717 */
					int BgL_tmpz00_8854;

					BgL_tmpz00_8854 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8854);
				}
				{	/* Liveness/liveness.scm 717 */
					int BgL_tmpz00_8857;

					BgL_tmpz00_8857 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8857, BgL_val1_1731z00_5268);
				}
				return BgL_val0_1730z00_5267;
			}
		}

	}



/* &defuse-conditional2009 */
	obj_t BGl_z62defusezd2conditional2009zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4684, obj_t BgL_nz00_4685)
	{
		{	/* Liveness/liveness.scm 702 */
			{	/* Liveness/liveness.scm 704 */
				obj_t BgL_deftestz00_5270;

				BgL_deftestz00_5270 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_4685)))->BgL_testz00));
				{	/* Liveness/liveness.scm 705 */
					obj_t BgL_usetestz00_5271;

					{	/* Liveness/liveness.scm 706 */
						obj_t BgL_tmpz00_5272;

						{	/* Liveness/liveness.scm 706 */
							int BgL_tmpz00_8863;

							BgL_tmpz00_8863 = (int) (1L);
							BgL_tmpz00_5272 = BGL_MVALUES_VAL(BgL_tmpz00_8863);
						}
						{	/* Liveness/liveness.scm 706 */
							int BgL_tmpz00_8866;

							BgL_tmpz00_8866 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_8866, BUNSPEC);
						}
						BgL_usetestz00_5271 = BgL_tmpz00_5272;
					}
					{	/* Liveness/liveness.scm 706 */
						obj_t BgL_deftruez00_5273;

						BgL_deftruez00_5273 =
							BGl_defusez00zzliveness_livenessz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nz00_4685)))->BgL_truez00));
						{	/* Liveness/liveness.scm 707 */
							obj_t BgL_usetruez00_5274;

							{	/* Liveness/liveness.scm 708 */
								obj_t BgL_tmpz00_5275;

								{	/* Liveness/liveness.scm 708 */
									int BgL_tmpz00_8872;

									BgL_tmpz00_8872 = (int) (1L);
									BgL_tmpz00_5275 = BGL_MVALUES_VAL(BgL_tmpz00_8872);
								}
								{	/* Liveness/liveness.scm 708 */
									int BgL_tmpz00_8875;

									BgL_tmpz00_8875 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_8875, BUNSPEC);
								}
								BgL_usetruez00_5274 = BgL_tmpz00_5275;
							}
							{	/* Liveness/liveness.scm 708 */
								obj_t BgL_deffalsez00_5276;

								BgL_deffalsez00_5276 =
									BGl_defusez00zzliveness_livenessz00(
									(((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nz00_4685)))->
										BgL_falsez00));
								{	/* Liveness/liveness.scm 709 */
									obj_t BgL_usefalsez00_5277;

									{	/* Liveness/liveness.scm 710 */
										obj_t BgL_tmpz00_5278;

										{	/* Liveness/liveness.scm 710 */
											int BgL_tmpz00_8881;

											BgL_tmpz00_8881 = (int) (1L);
											BgL_tmpz00_5278 = BGL_MVALUES_VAL(BgL_tmpz00_8881);
										}
										{	/* Liveness/liveness.scm 710 */
											int BgL_tmpz00_8884;

											BgL_tmpz00_8884 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_8884, BUNSPEC);
										}
										BgL_usefalsez00_5277 = BgL_tmpz00_5278;
									}
									{	/* Liveness/liveness.scm 711 */
										BgL_conditionalz00_bglt BgL_arg2354z00_5279;

										{	/* Liveness/liveness.scm 711 */
											BgL_conditionalzf2livenesszf2_bglt BgL_wide1324z00_5280;

											BgL_wide1324z00_5280 =
												((BgL_conditionalzf2livenesszf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_conditionalzf2livenesszf2_bgl))));
											{	/* Liveness/liveness.scm 711 */
												obj_t BgL_auxz00_8892;
												BgL_objectz00_bglt BgL_tmpz00_8888;

												BgL_auxz00_8892 = ((obj_t) BgL_wide1324z00_5280);
												BgL_tmpz00_8888 =
													((BgL_objectz00_bglt)
													((BgL_conditionalz00_bglt)
														((BgL_conditionalz00_bglt) BgL_nz00_4685)));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8888,
													BgL_auxz00_8892);
											}
											((BgL_objectz00_bglt)
												((BgL_conditionalz00_bglt)
													((BgL_conditionalz00_bglt) BgL_nz00_4685)));
											{	/* Liveness/liveness.scm 711 */
												long BgL_arg2355z00_5281;

												BgL_arg2355z00_5281 =
													BGL_CLASS_NUM
													(BGl_conditionalzf2livenesszf2zzliveness_typesz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_conditionalz00_bglt) (
																(BgL_conditionalz00_bglt) BgL_nz00_4685))),
													BgL_arg2355z00_5281);
											}
											((BgL_conditionalz00_bglt)
												((BgL_conditionalz00_bglt)
													((BgL_conditionalz00_bglt) BgL_nz00_4685)));
										}
										{
											obj_t BgL_auxz00_8914;
											BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8906;

											{	/* Liveness/liveness.scm 712 */
												obj_t BgL_arg2356z00_5282;

												BgL_arg2356z00_5282 =
													BGl_intersectionz00zzliveness_setz00
													(BgL_deftruez00_5273, BgL_deffalsez00_5276);
												BgL_auxz00_8914 =
													BGl_unionz00zzliveness_setz00(BgL_deftestz00_5270,
													BgL_arg2356z00_5282);
											}
											{
												obj_t BgL_auxz00_8907;

												{	/* Liveness/liveness.scm 712 */
													BgL_objectz00_bglt BgL_tmpz00_8908;

													BgL_tmpz00_8908 =
														((BgL_objectz00_bglt)
														((BgL_conditionalz00_bglt)
															((BgL_conditionalz00_bglt) BgL_nz00_4685)));
													BgL_auxz00_8907 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8908);
												}
												BgL_auxz00_8906 =
													((BgL_conditionalzf2livenesszf2_bglt)
													BgL_auxz00_8907);
											}
											((((BgL_conditionalzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_8906))->BgL_defz00) =
												((obj_t) BgL_auxz00_8914), BUNSPEC);
										}
										{
											obj_t BgL_auxz00_8926;
											BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8918;

											{	/* Liveness/liveness.scm 713 */
												obj_t BgL_arg2357z00_5283;

												{	/* Liveness/liveness.scm 713 */
													obj_t BgL_arg2358z00_5284;

													BgL_arg2358z00_5284 =
														BGl_unionz00zzliveness_setz00(BgL_usetruez00_5274,
														BgL_usefalsez00_5277);
													BgL_arg2357z00_5283 =
														BGl_disjonctionz00zzliveness_setz00
														(BgL_arg2358z00_5284, BgL_deftestz00_5270);
												}
												BgL_auxz00_8926 =
													BGl_unionz00zzliveness_setz00(BgL_usetestz00_5271,
													BgL_arg2357z00_5283);
											}
											{
												obj_t BgL_auxz00_8919;

												{	/* Liveness/liveness.scm 713 */
													BgL_objectz00_bglt BgL_tmpz00_8920;

													BgL_tmpz00_8920 =
														((BgL_objectz00_bglt)
														((BgL_conditionalz00_bglt)
															((BgL_conditionalz00_bglt) BgL_nz00_4685)));
													BgL_auxz00_8919 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8920);
												}
												BgL_auxz00_8918 =
													((BgL_conditionalzf2livenesszf2_bglt)
													BgL_auxz00_8919);
											}
											((((BgL_conditionalzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_8918))->BgL_usez00) =
												((obj_t) BgL_auxz00_8926), BUNSPEC);
										}
										{
											BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8931;

											{
												obj_t BgL_auxz00_8932;

												{	/* Liveness/liveness.scm 709 */
													BgL_objectz00_bglt BgL_tmpz00_8933;

													BgL_tmpz00_8933 =
														((BgL_objectz00_bglt)
														((BgL_conditionalz00_bglt)
															((BgL_conditionalz00_bglt) BgL_nz00_4685)));
													BgL_auxz00_8932 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8933);
												}
												BgL_auxz00_8931 =
													((BgL_conditionalzf2livenesszf2_bglt)
													BgL_auxz00_8932);
											}
											((((BgL_conditionalzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_8931))->BgL_inz00) =
												((obj_t) BNIL), BUNSPEC);
										}
										{
											BgL_conditionalzf2livenesszf2_bglt BgL_auxz00_8940;

											{
												obj_t BgL_auxz00_8941;

												{	/* Liveness/liveness.scm 709 */
													BgL_objectz00_bglt BgL_tmpz00_8942;

													BgL_tmpz00_8942 =
														((BgL_objectz00_bglt)
														((BgL_conditionalz00_bglt)
															((BgL_conditionalz00_bglt) BgL_nz00_4685)));
													BgL_auxz00_8941 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8942);
												}
												BgL_auxz00_8940 =
													((BgL_conditionalzf2livenesszf2_bglt)
													BgL_auxz00_8941);
											}
											((((BgL_conditionalzf2livenesszf2_bglt)
														COBJECT(BgL_auxz00_8940))->BgL_outz00) =
												((obj_t) BNIL), BUNSPEC);
										}
										BgL_arg2354z00_5279 =
											((BgL_conditionalz00_bglt)
											((BgL_conditionalz00_bglt) BgL_nz00_4685));
										return
											BGl_defusez00zzliveness_livenessz00(
											((BgL_nodez00_bglt) BgL_arg2354z00_5279));
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &inout-setq/liveness2007 */
	obj_t BGl_z62inoutzd2setqzf2liveness2007z42zzliveness_livenessz00(obj_t
		BgL_envz00_4686, obj_t BgL_nz00_4687)
	{
		{	/* Liveness/liveness.scm 695 */
			{	/* Liveness/liveness.scm 697 */
				obj_t BgL_val0_1728z00_5286;
				obj_t BgL_val1_1729z00_5287;

				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_8953;

					{
						obj_t BgL_auxz00_8954;

						{	/* Liveness/liveness.scm 697 */
							BgL_objectz00_bglt BgL_tmpz00_8955;

							BgL_tmpz00_8955 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4687));
							BgL_auxz00_8954 = BGL_OBJECT_WIDENING(BgL_tmpz00_8955);
						}
						BgL_auxz00_8953 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_8954);
					}
					BgL_val0_1728z00_5286 =
						(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8953))->
						BgL_inz00);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_8961;

					{
						obj_t BgL_auxz00_8962;

						{	/* Liveness/liveness.scm 697 */
							BgL_objectz00_bglt BgL_tmpz00_8963;

							BgL_tmpz00_8963 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4687));
							BgL_auxz00_8962 = BGL_OBJECT_WIDENING(BgL_tmpz00_8963);
						}
						BgL_auxz00_8961 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_8962);
					}
					BgL_val1_1729z00_5287 =
						(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8961))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 697 */
					int BgL_tmpz00_8969;

					BgL_tmpz00_8969 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8969);
				}
				{	/* Liveness/liveness.scm 697 */
					int BgL_tmpz00_8972;

					BgL_tmpz00_8972 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_8972, BgL_val1_1729z00_5287);
				}
				return BgL_val0_1728z00_5286;
			}
		}

	}



/* &inout!-setq/liveness2005 */
	obj_t BGl_z62inoutz12zd2setqzf2liveness2005z50zzliveness_livenessz00(obj_t
		BgL_envz00_4688, obj_t BgL_nz00_4689, obj_t BgL_oz00_4690)
	{
		{	/* Liveness/liveness.scm 689 */
			{
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_8975;

				{
					obj_t BgL_auxz00_8976;

					{	/* Liveness/liveness.scm 691 */
						BgL_objectz00_bglt BgL_tmpz00_8977;

						BgL_tmpz00_8977 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4689));
						BgL_auxz00_8976 = BGL_OBJECT_WIDENING(BgL_tmpz00_8977);
					}
					BgL_auxz00_8975 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_8976);
				}
				((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8975))->
						BgL_outz00) = ((obj_t) BgL_oz00_4690), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_8990;
				BgL_setqzf2livenesszf2_bglt BgL_auxz00_8983;

				{	/* Liveness/liveness.scm 692 */
					obj_t BgL_arg2350z00_5289;
					obj_t BgL_arg2351z00_5290;

					{
						BgL_setqzf2livenesszf2_bglt BgL_auxz00_8991;

						{
							obj_t BgL_auxz00_8992;

							{	/* Liveness/liveness.scm 692 */
								BgL_objectz00_bglt BgL_tmpz00_8993;

								BgL_tmpz00_8993 =
									((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4689));
								BgL_auxz00_8992 = BGL_OBJECT_WIDENING(BgL_tmpz00_8993);
							}
							BgL_auxz00_8991 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_8992);
						}
						BgL_arg2350z00_5289 =
							(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8991))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 692 */
						obj_t BgL_arg2352z00_5291;
						obj_t BgL_arg2353z00_5292;

						{
							BgL_setqzf2livenesszf2_bglt BgL_auxz00_8999;

							{
								obj_t BgL_auxz00_9000;

								{	/* Liveness/liveness.scm 692 */
									BgL_objectz00_bglt BgL_tmpz00_9001;

									BgL_tmpz00_9001 =
										((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4689));
									BgL_auxz00_9000 = BGL_OBJECT_WIDENING(BgL_tmpz00_9001);
								}
								BgL_auxz00_8999 =
									((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9000);
							}
							BgL_arg2352z00_5291 =
								(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8999))->
								BgL_outz00);
						}
						{
							BgL_setqzf2livenesszf2_bglt BgL_auxz00_9007;

							{
								obj_t BgL_auxz00_9008;

								{	/* Liveness/liveness.scm 692 */
									BgL_objectz00_bglt BgL_tmpz00_9009;

									BgL_tmpz00_9009 =
										((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4689));
									BgL_auxz00_9008 = BGL_OBJECT_WIDENING(BgL_tmpz00_9009);
								}
								BgL_auxz00_9007 =
									((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9008);
							}
							BgL_arg2353z00_5292 =
								(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9007))->
								BgL_defz00);
						}
						BgL_arg2351z00_5290 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2352z00_5291,
							BgL_arg2353z00_5292);
					}
					BgL_auxz00_8990 =
						BGl_unionz00zzliveness_setz00(BgL_arg2350z00_5289,
						BgL_arg2351z00_5290);
				}
				{
					obj_t BgL_auxz00_8984;

					{	/* Liveness/liveness.scm 692 */
						BgL_objectz00_bglt BgL_tmpz00_8985;

						BgL_tmpz00_8985 =
							((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4689));
						BgL_auxz00_8984 = BGL_OBJECT_WIDENING(BgL_tmpz00_8985);
					}
					BgL_auxz00_8983 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_8984);
				}
				((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_8983))->BgL_inz00) =
					((obj_t) BgL_auxz00_8990), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 693 */
				obj_t BgL_val0_1726z00_5293;
				obj_t BgL_val1_1727z00_5294;

				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_9018;

					{
						obj_t BgL_auxz00_9019;

						{	/* Liveness/liveness.scm 693 */
							BgL_objectz00_bglt BgL_tmpz00_9020;

							BgL_tmpz00_9020 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4689));
							BgL_auxz00_9019 = BGL_OBJECT_WIDENING(BgL_tmpz00_9020);
						}
						BgL_auxz00_9018 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9019);
					}
					BgL_val0_1726z00_5293 =
						(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9018))->
						BgL_inz00);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_9026;

					{
						obj_t BgL_auxz00_9027;

						{	/* Liveness/liveness.scm 693 */
							BgL_objectz00_bglt BgL_tmpz00_9028;

							BgL_tmpz00_9028 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4689));
							BgL_auxz00_9027 = BGL_OBJECT_WIDENING(BgL_tmpz00_9028);
						}
						BgL_auxz00_9026 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9027);
					}
					BgL_val1_1727z00_5294 =
						(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9026))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 693 */
					int BgL_tmpz00_9034;

					BgL_tmpz00_9034 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9034);
				}
				{	/* Liveness/liveness.scm 693 */
					int BgL_tmpz00_9037;

					BgL_tmpz00_9037 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9037, BgL_val1_1727z00_5294);
				}
				return BgL_val0_1726z00_5293;
			}
		}

	}



/* &defuse-setq/liveness2003 */
	obj_t BGl_z62defusezd2setqzf2liveness2003z42zzliveness_livenessz00(obj_t
		BgL_envz00_4691, obj_t BgL_nz00_4692)
	{
		{	/* Liveness/liveness.scm 685 */
			{	/* Liveness/liveness.scm 687 */
				obj_t BgL_val0_1724z00_5296;
				obj_t BgL_val1_1725z00_5297;

				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_9040;

					{
						obj_t BgL_auxz00_9041;

						{	/* Liveness/liveness.scm 687 */
							BgL_objectz00_bglt BgL_tmpz00_9042;

							BgL_tmpz00_9042 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4692));
							BgL_auxz00_9041 = BGL_OBJECT_WIDENING(BgL_tmpz00_9042);
						}
						BgL_auxz00_9040 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9041);
					}
					BgL_val0_1724z00_5296 =
						(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9040))->
						BgL_defz00);
				}
				{
					BgL_setqzf2livenesszf2_bglt BgL_auxz00_9048;

					{
						obj_t BgL_auxz00_9049;

						{	/* Liveness/liveness.scm 687 */
							BgL_objectz00_bglt BgL_tmpz00_9050;

							BgL_tmpz00_9050 =
								((BgL_objectz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4692));
							BgL_auxz00_9049 = BGL_OBJECT_WIDENING(BgL_tmpz00_9050);
						}
						BgL_auxz00_9048 = ((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9049);
					}
					BgL_val1_1725z00_5297 =
						(((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9048))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 687 */
					int BgL_tmpz00_9056;

					BgL_tmpz00_9056 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9056);
				}
				{	/* Liveness/liveness.scm 687 */
					int BgL_tmpz00_9059;

					BgL_tmpz00_9059 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9059, BgL_val1_1725z00_5297);
				}
				return BgL_val0_1724z00_5296;
			}
		}

	}



/* &defuse-setq2001 */
	obj_t BGl_z62defusezd2setq2001zb0zzliveness_livenessz00(obj_t BgL_envz00_4693,
		obj_t BgL_nz00_4694)
	{
		{	/* Liveness/liveness.scm 673 */
			{	/* Liveness/liveness.scm 675 */
				obj_t BgL_defvaluez00_5299;

				BgL_defvaluez00_5299 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_4694)))->BgL_valuez00));
				{	/* Liveness/liveness.scm 676 */
					obj_t BgL_usevaluez00_5300;

					{	/* Liveness/liveness.scm 677 */
						obj_t BgL_tmpz00_5301;

						{	/* Liveness/liveness.scm 677 */
							int BgL_tmpz00_9065;

							BgL_tmpz00_9065 = (int) (1L);
							BgL_tmpz00_5301 = BGL_MVALUES_VAL(BgL_tmpz00_9065);
						}
						{	/* Liveness/liveness.scm 677 */
							int BgL_tmpz00_9068;

							BgL_tmpz00_9068 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_9068, BUNSPEC);
						}
						BgL_usevaluez00_5300 = BgL_tmpz00_5301;
					}
					{	/* Liveness/liveness.scm 677 */
						BgL_varz00_bglt BgL_i1313z00_5302;

						BgL_i1313z00_5302 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nz00_4694)))->BgL_varz00);
						{	/* Liveness/liveness.scm 678 */
							bool_t BgL_test2985z00_9073;

							{	/* Liveness/liveness.scm 678 */
								BgL_variablez00_bglt BgL_arg2345z00_5303;

								BgL_arg2345z00_5303 =
									(((BgL_varz00_bglt) COBJECT(BgL_i1313z00_5302))->
									BgL_variablez00);
								{	/* Liveness/liveness.scm 678 */
									obj_t BgL_classz00_5304;

									BgL_classz00_5304 =
										BGl_localzf2livenesszf2zzliveness_typesz00;
									{	/* Liveness/liveness.scm 678 */
										BgL_objectz00_bglt BgL_arg1807z00_5305;

										{	/* Liveness/liveness.scm 678 */
											obj_t BgL_tmpz00_9075;

											BgL_tmpz00_9075 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg2345z00_5303));
											BgL_arg1807z00_5305 =
												(BgL_objectz00_bglt) (BgL_tmpz00_9075);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Liveness/liveness.scm 678 */
												long BgL_idxz00_5306;

												BgL_idxz00_5306 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5305);
												BgL_test2985z00_9073 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5306 + 3L)) == BgL_classz00_5304);
											}
										else
											{	/* Liveness/liveness.scm 678 */
												bool_t BgL_res2667z00_5309;

												{	/* Liveness/liveness.scm 678 */
													obj_t BgL_oclassz00_5310;

													{	/* Liveness/liveness.scm 678 */
														obj_t BgL_arg1815z00_5311;
														long BgL_arg1816z00_5312;

														BgL_arg1815z00_5311 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Liveness/liveness.scm 678 */
															long BgL_arg1817z00_5313;

															BgL_arg1817z00_5313 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5305);
															BgL_arg1816z00_5312 =
																(BgL_arg1817z00_5313 - OBJECT_TYPE);
														}
														BgL_oclassz00_5310 =
															VECTOR_REF(BgL_arg1815z00_5311,
															BgL_arg1816z00_5312);
													}
													{	/* Liveness/liveness.scm 678 */
														bool_t BgL__ortest_1115z00_5314;

														BgL__ortest_1115z00_5314 =
															(BgL_classz00_5304 == BgL_oclassz00_5310);
														if (BgL__ortest_1115z00_5314)
															{	/* Liveness/liveness.scm 678 */
																BgL_res2667z00_5309 = BgL__ortest_1115z00_5314;
															}
														else
															{	/* Liveness/liveness.scm 678 */
																long BgL_odepthz00_5315;

																{	/* Liveness/liveness.scm 678 */
																	obj_t BgL_arg1804z00_5316;

																	BgL_arg1804z00_5316 = (BgL_oclassz00_5310);
																	BgL_odepthz00_5315 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5316);
																}
																if ((3L < BgL_odepthz00_5315))
																	{	/* Liveness/liveness.scm 678 */
																		obj_t BgL_arg1802z00_5317;

																		{	/* Liveness/liveness.scm 678 */
																			obj_t BgL_arg1803z00_5318;

																			BgL_arg1803z00_5318 =
																				(BgL_oclassz00_5310);
																			BgL_arg1802z00_5317 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5318, 3L);
																		}
																		BgL_res2667z00_5309 =
																			(BgL_arg1802z00_5317 ==
																			BgL_classz00_5304);
																	}
																else
																	{	/* Liveness/liveness.scm 678 */
																		BgL_res2667z00_5309 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2985z00_9073 = BgL_res2667z00_5309;
											}
									}
								}
							}
							if (BgL_test2985z00_9073)
								{	/* Liveness/liveness.scm 679 */
									BgL_variablez00_bglt BgL_arg2342z00_5319;

									BgL_arg2342z00_5319 =
										(((BgL_varz00_bglt) COBJECT(BgL_i1313z00_5302))->
										BgL_variablez00);
									BgL_defvaluez00_5299 =
										BGl_addz00zzliveness_setz00(((BgL_localz00_bglt)
											BgL_arg2342z00_5319), BgL_defvaluez00_5299);
								}
							else
								{	/* Liveness/liveness.scm 678 */
									BFALSE;
								}
						}
						{	/* Liveness/liveness.scm 681 */
							BgL_setqz00_bglt BgL_arg2346z00_5320;

							{	/* Liveness/liveness.scm 681 */
								BgL_setqzf2livenesszf2_bglt BgL_wide1316z00_5321;

								BgL_wide1316z00_5321 =
									((BgL_setqzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_setqzf2livenesszf2_bgl))));
								{	/* Liveness/liveness.scm 681 */
									obj_t BgL_auxz00_9106;
									BgL_objectz00_bglt BgL_tmpz00_9102;

									BgL_auxz00_9106 = ((obj_t) BgL_wide1316z00_5321);
									BgL_tmpz00_9102 =
										((BgL_objectz00_bglt)
										((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4694)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9102, BgL_auxz00_9106);
								}
								((BgL_objectz00_bglt)
									((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4694)));
								{	/* Liveness/liveness.scm 681 */
									long BgL_arg2348z00_5322;

									BgL_arg2348z00_5322 =
										BGL_CLASS_NUM(BGl_setqzf2livenesszf2zzliveness_typesz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_setqz00_bglt)
												((BgL_setqz00_bglt) BgL_nz00_4694))),
										BgL_arg2348z00_5322);
								}
								((BgL_setqz00_bglt)
									((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4694)));
							}
							{
								BgL_setqzf2livenesszf2_bglt BgL_auxz00_9120;

								{
									obj_t BgL_auxz00_9121;

									{	/* Liveness/liveness.scm 682 */
										BgL_objectz00_bglt BgL_tmpz00_9122;

										BgL_tmpz00_9122 =
											((BgL_objectz00_bglt)
											((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4694)));
										BgL_auxz00_9121 = BGL_OBJECT_WIDENING(BgL_tmpz00_9122);
									}
									BgL_auxz00_9120 =
										((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9121);
								}
								((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9120))->
										BgL_defz00) = ((obj_t) BgL_defvaluez00_5299), BUNSPEC);
							}
							{
								BgL_setqzf2livenesszf2_bglt BgL_auxz00_9129;

								{
									obj_t BgL_auxz00_9130;

									{	/* Liveness/liveness.scm 683 */
										BgL_objectz00_bglt BgL_tmpz00_9131;

										BgL_tmpz00_9131 =
											((BgL_objectz00_bglt)
											((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4694)));
										BgL_auxz00_9130 = BGL_OBJECT_WIDENING(BgL_tmpz00_9131);
									}
									BgL_auxz00_9129 =
										((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9130);
								}
								((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9129))->
										BgL_usez00) = ((obj_t) BgL_usevaluez00_5300), BUNSPEC);
							}
							{
								BgL_setqzf2livenesszf2_bglt BgL_auxz00_9138;

								{
									obj_t BgL_auxz00_9139;

									{	/* Liveness/liveness.scm 677 */
										BgL_objectz00_bglt BgL_tmpz00_9140;

										BgL_tmpz00_9140 =
											((BgL_objectz00_bglt)
											((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4694)));
										BgL_auxz00_9139 = BGL_OBJECT_WIDENING(BgL_tmpz00_9140);
									}
									BgL_auxz00_9138 =
										((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9139);
								}
								((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9138))->
										BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
							}
							{
								BgL_setqzf2livenesszf2_bglt BgL_auxz00_9147;

								{
									obj_t BgL_auxz00_9148;

									{	/* Liveness/liveness.scm 677 */
										BgL_objectz00_bglt BgL_tmpz00_9149;

										BgL_tmpz00_9149 =
											((BgL_objectz00_bglt)
											((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4694)));
										BgL_auxz00_9148 = BGL_OBJECT_WIDENING(BgL_tmpz00_9149);
									}
									BgL_auxz00_9147 =
										((BgL_setqzf2livenesszf2_bglt) BgL_auxz00_9148);
								}
								((((BgL_setqzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9147))->
										BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
							}
							BgL_arg2346z00_5320 =
								((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nz00_4694));
							return
								BGl_defusez00zzliveness_livenessz00(
								((BgL_nodez00_bglt) BgL_arg2346z00_5320));
						}
					}
				}
			}
		}

	}



/* &inout-cast/liveness1999 */
	obj_t BGl_z62inoutzd2castzf2liveness1999z42zzliveness_livenessz00(obj_t
		BgL_envz00_4695, obj_t BgL_nz00_4696)
	{
		{	/* Liveness/liveness.scm 666 */
			{	/* Liveness/liveness.scm 668 */
				obj_t BgL_val0_1722z00_5324;
				obj_t BgL_val1_1723z00_5325;

				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_9160;

					{
						obj_t BgL_auxz00_9161;

						{	/* Liveness/liveness.scm 668 */
							BgL_objectz00_bglt BgL_tmpz00_9162;

							BgL_tmpz00_9162 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4696));
							BgL_auxz00_9161 = BGL_OBJECT_WIDENING(BgL_tmpz00_9162);
						}
						BgL_auxz00_9160 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9161);
					}
					BgL_val0_1722z00_5324 =
						(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9160))->
						BgL_inz00);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_9168;

					{
						obj_t BgL_auxz00_9169;

						{	/* Liveness/liveness.scm 668 */
							BgL_objectz00_bglt BgL_tmpz00_9170;

							BgL_tmpz00_9170 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4696));
							BgL_auxz00_9169 = BGL_OBJECT_WIDENING(BgL_tmpz00_9170);
						}
						BgL_auxz00_9168 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9169);
					}
					BgL_val1_1723z00_5325 =
						(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9168))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 668 */
					int BgL_tmpz00_9176;

					BgL_tmpz00_9176 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9176);
				}
				{	/* Liveness/liveness.scm 668 */
					int BgL_tmpz00_9179;

					BgL_tmpz00_9179 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9179, BgL_val1_1723z00_5325);
				}
				return BgL_val0_1722z00_5324;
			}
		}

	}



/* &inout!-cast/liveness1997 */
	obj_t BGl_z62inoutz12zd2castzf2liveness1997z50zzliveness_livenessz00(obj_t
		BgL_envz00_4697, obj_t BgL_nz00_4698, obj_t BgL_oz00_4699)
	{
		{	/* Liveness/liveness.scm 659 */
			{
				BgL_castzf2livenesszf2_bglt BgL_auxz00_9182;

				{
					obj_t BgL_auxz00_9183;

					{	/* Liveness/liveness.scm 661 */
						BgL_objectz00_bglt BgL_tmpz00_9184;

						BgL_tmpz00_9184 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4698));
						BgL_auxz00_9183 = BGL_OBJECT_WIDENING(BgL_tmpz00_9184);
					}
					BgL_auxz00_9182 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9183);
				}
				((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9182))->
						BgL_outz00) = ((obj_t) BgL_oz00_4699), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_9197;
				BgL_castzf2livenesszf2_bglt BgL_auxz00_9190;

				{	/* Liveness/liveness.scm 662 */
					obj_t BgL_arg2335z00_5327;
					obj_t BgL_arg2336z00_5328;

					{
						BgL_castzf2livenesszf2_bglt BgL_auxz00_9198;

						{
							obj_t BgL_auxz00_9199;

							{	/* Liveness/liveness.scm 662 */
								BgL_objectz00_bglt BgL_tmpz00_9200;

								BgL_tmpz00_9200 =
									((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4698));
								BgL_auxz00_9199 = BGL_OBJECT_WIDENING(BgL_tmpz00_9200);
							}
							BgL_auxz00_9198 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9199);
						}
						BgL_arg2335z00_5327 =
							(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9198))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 662 */
						obj_t BgL_arg2337z00_5329;
						obj_t BgL_arg2338z00_5330;

						{
							BgL_castzf2livenesszf2_bglt BgL_auxz00_9206;

							{
								obj_t BgL_auxz00_9207;

								{	/* Liveness/liveness.scm 662 */
									BgL_objectz00_bglt BgL_tmpz00_9208;

									BgL_tmpz00_9208 =
										((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4698));
									BgL_auxz00_9207 = BGL_OBJECT_WIDENING(BgL_tmpz00_9208);
								}
								BgL_auxz00_9206 =
									((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9207);
							}
							BgL_arg2337z00_5329 =
								(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9206))->
								BgL_outz00);
						}
						{
							BgL_castzf2livenesszf2_bglt BgL_auxz00_9214;

							{
								obj_t BgL_auxz00_9215;

								{	/* Liveness/liveness.scm 662 */
									BgL_objectz00_bglt BgL_tmpz00_9216;

									BgL_tmpz00_9216 =
										((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4698));
									BgL_auxz00_9215 = BGL_OBJECT_WIDENING(BgL_tmpz00_9216);
								}
								BgL_auxz00_9214 =
									((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9215);
							}
							BgL_arg2338z00_5330 =
								(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9214))->
								BgL_defz00);
						}
						BgL_arg2336z00_5328 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2337z00_5329,
							BgL_arg2338z00_5330);
					}
					BgL_auxz00_9197 =
						BGl_unionz00zzliveness_setz00(BgL_arg2335z00_5327,
						BgL_arg2336z00_5328);
				}
				{
					obj_t BgL_auxz00_9191;

					{	/* Liveness/liveness.scm 662 */
						BgL_objectz00_bglt BgL_tmpz00_9192;

						BgL_tmpz00_9192 =
							((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4698));
						BgL_auxz00_9191 = BGL_OBJECT_WIDENING(BgL_tmpz00_9192);
					}
					BgL_auxz00_9190 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9191);
				}
				((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9190))->BgL_inz00) =
					((obj_t) BgL_auxz00_9197), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 663 */
				BgL_nodez00_bglt BgL_arg2339z00_5331;

				BgL_arg2339z00_5331 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt)
								((BgL_castz00_bglt) BgL_nz00_4698))))->BgL_argz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2339z00_5331,
					BgL_oz00_4699);
			}
			{	/* Liveness/liveness.scm 664 */
				obj_t BgL_val0_1720z00_5332;
				obj_t BgL_val1_1721z00_5333;

				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_9229;

					{
						obj_t BgL_auxz00_9230;

						{	/* Liveness/liveness.scm 664 */
							BgL_objectz00_bglt BgL_tmpz00_9231;

							BgL_tmpz00_9231 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4698));
							BgL_auxz00_9230 = BGL_OBJECT_WIDENING(BgL_tmpz00_9231);
						}
						BgL_auxz00_9229 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9230);
					}
					BgL_val0_1720z00_5332 =
						(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9229))->
						BgL_inz00);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_9237;

					{
						obj_t BgL_auxz00_9238;

						{	/* Liveness/liveness.scm 664 */
							BgL_objectz00_bglt BgL_tmpz00_9239;

							BgL_tmpz00_9239 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4698));
							BgL_auxz00_9238 = BGL_OBJECT_WIDENING(BgL_tmpz00_9239);
						}
						BgL_auxz00_9237 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9238);
					}
					BgL_val1_1721z00_5333 =
						(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9237))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 664 */
					int BgL_tmpz00_9245;

					BgL_tmpz00_9245 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9245);
				}
				{	/* Liveness/liveness.scm 664 */
					int BgL_tmpz00_9248;

					BgL_tmpz00_9248 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9248, BgL_val1_1721z00_5333);
				}
				return BgL_val0_1720z00_5332;
			}
		}

	}



/* &defuse-cast/liveness1995 */
	obj_t BGl_z62defusezd2castzf2liveness1995z42zzliveness_livenessz00(obj_t
		BgL_envz00_4700, obj_t BgL_nz00_4701)
	{
		{	/* Liveness/liveness.scm 655 */
			{	/* Liveness/liveness.scm 657 */
				obj_t BgL_val0_1718z00_5335;
				obj_t BgL_val1_1719z00_5336;

				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_9251;

					{
						obj_t BgL_auxz00_9252;

						{	/* Liveness/liveness.scm 657 */
							BgL_objectz00_bglt BgL_tmpz00_9253;

							BgL_tmpz00_9253 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4701));
							BgL_auxz00_9252 = BGL_OBJECT_WIDENING(BgL_tmpz00_9253);
						}
						BgL_auxz00_9251 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9252);
					}
					BgL_val0_1718z00_5335 =
						(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9251))->
						BgL_defz00);
				}
				{
					BgL_castzf2livenesszf2_bglt BgL_auxz00_9259;

					{
						obj_t BgL_auxz00_9260;

						{	/* Liveness/liveness.scm 657 */
							BgL_objectz00_bglt BgL_tmpz00_9261;

							BgL_tmpz00_9261 =
								((BgL_objectz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4701));
							BgL_auxz00_9260 = BGL_OBJECT_WIDENING(BgL_tmpz00_9261);
						}
						BgL_auxz00_9259 = ((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9260);
					}
					BgL_val1_1719z00_5336 =
						(((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9259))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 657 */
					int BgL_tmpz00_9267;

					BgL_tmpz00_9267 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9267);
				}
				{	/* Liveness/liveness.scm 657 */
					int BgL_tmpz00_9270;

					BgL_tmpz00_9270 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9270, BgL_val1_1719z00_5336);
				}
				return BgL_val0_1718z00_5335;
			}
		}

	}



/* &defuse-cast1993 */
	obj_t BGl_z62defusezd2cast1993zb0zzliveness_livenessz00(obj_t BgL_envz00_4702,
		obj_t BgL_nz00_4703)
	{
		{	/* Liveness/liveness.scm 646 */
			{	/* Liveness/liveness.scm 648 */
				obj_t BgL_defz00_5338;

				BgL_defz00_5338 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nz00_4703)))->BgL_argz00));
				{	/* Liveness/liveness.scm 649 */
					obj_t BgL_usez00_5339;

					{	/* Liveness/liveness.scm 650 */
						obj_t BgL_tmpz00_5340;

						{	/* Liveness/liveness.scm 650 */
							int BgL_tmpz00_9276;

							BgL_tmpz00_9276 = (int) (1L);
							BgL_tmpz00_5340 = BGL_MVALUES_VAL(BgL_tmpz00_9276);
						}
						{	/* Liveness/liveness.scm 650 */
							int BgL_tmpz00_9279;

							BgL_tmpz00_9279 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_9279, BUNSPEC);
						}
						BgL_usez00_5339 = BgL_tmpz00_5340;
					}
					{	/* Liveness/liveness.scm 651 */
						BgL_castz00_bglt BgL_arg2330z00_5341;

						{	/* Liveness/liveness.scm 651 */
							BgL_castzf2livenesszf2_bglt BgL_wide1307z00_5342;

							BgL_wide1307z00_5342 =
								((BgL_castzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_castzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 651 */
								obj_t BgL_auxz00_9287;
								BgL_objectz00_bglt BgL_tmpz00_9283;

								BgL_auxz00_9287 = ((obj_t) BgL_wide1307z00_5342);
								BgL_tmpz00_9283 =
									((BgL_objectz00_bglt)
									((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4703)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9283, BgL_auxz00_9287);
							}
							((BgL_objectz00_bglt)
								((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4703)));
							{	/* Liveness/liveness.scm 651 */
								long BgL_arg2331z00_5343;

								BgL_arg2331z00_5343 =
									BGL_CLASS_NUM(BGl_castzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_castz00_bglt)
											((BgL_castz00_bglt) BgL_nz00_4703))),
									BgL_arg2331z00_5343);
							}
							((BgL_castz00_bglt)
								((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4703)));
						}
						{
							BgL_castzf2livenesszf2_bglt BgL_auxz00_9301;

							{
								obj_t BgL_auxz00_9302;

								{	/* Liveness/liveness.scm 652 */
									BgL_objectz00_bglt BgL_tmpz00_9303;

									BgL_tmpz00_9303 =
										((BgL_objectz00_bglt)
										((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4703)));
									BgL_auxz00_9302 = BGL_OBJECT_WIDENING(BgL_tmpz00_9303);
								}
								BgL_auxz00_9301 =
									((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9302);
							}
							((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9301))->
									BgL_defz00) = ((obj_t) BgL_defz00_5338), BUNSPEC);
						}
						{
							BgL_castzf2livenesszf2_bglt BgL_auxz00_9310;

							{
								obj_t BgL_auxz00_9311;

								{	/* Liveness/liveness.scm 653 */
									BgL_objectz00_bglt BgL_tmpz00_9312;

									BgL_tmpz00_9312 =
										((BgL_objectz00_bglt)
										((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4703)));
									BgL_auxz00_9311 = BGL_OBJECT_WIDENING(BgL_tmpz00_9312);
								}
								BgL_auxz00_9310 =
									((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9311);
							}
							((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9310))->
									BgL_usez00) = ((obj_t) BgL_usez00_5339), BUNSPEC);
						}
						{
							BgL_castzf2livenesszf2_bglt BgL_auxz00_9319;

							{
								obj_t BgL_auxz00_9320;

								{	/* Liveness/liveness.scm 649 */
									BgL_objectz00_bglt BgL_tmpz00_9321;

									BgL_tmpz00_9321 =
										((BgL_objectz00_bglt)
										((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4703)));
									BgL_auxz00_9320 = BGL_OBJECT_WIDENING(BgL_tmpz00_9321);
								}
								BgL_auxz00_9319 =
									((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9320);
							}
							((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9319))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_castzf2livenesszf2_bglt BgL_auxz00_9328;

							{
								obj_t BgL_auxz00_9329;

								{	/* Liveness/liveness.scm 649 */
									BgL_objectz00_bglt BgL_tmpz00_9330;

									BgL_tmpz00_9330 =
										((BgL_objectz00_bglt)
										((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4703)));
									BgL_auxz00_9329 = BGL_OBJECT_WIDENING(BgL_tmpz00_9330);
								}
								BgL_auxz00_9328 =
									((BgL_castzf2livenesszf2_bglt) BgL_auxz00_9329);
							}
							((((BgL_castzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9328))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2330z00_5341 =
							((BgL_castz00_bglt) ((BgL_castz00_bglt) BgL_nz00_4703));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2330z00_5341));
					}
				}
			}
		}

	}



/* &inout-cast-null/live1991 */
	obj_t BGl_z62inoutzd2castzd2nullzf2live1991z90zzliveness_livenessz00(obj_t
		BgL_envz00_4704, obj_t BgL_nz00_4705)
	{
		{	/* Liveness/liveness.scm 639 */
			{	/* Liveness/liveness.scm 641 */
				obj_t BgL_val0_1716z00_5345;
				obj_t BgL_val1_1717z00_5346;

				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9341;

					{
						obj_t BgL_auxz00_9342;

						{	/* Liveness/liveness.scm 641 */
							BgL_objectz00_bglt BgL_tmpz00_9343;

							BgL_tmpz00_9343 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nz00_4705));
							BgL_auxz00_9342 = BGL_OBJECT_WIDENING(BgL_tmpz00_9343);
						}
						BgL_auxz00_9341 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9342);
					}
					BgL_val0_1716z00_5345 =
						(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_9341))->
						BgL_inz00);
				}
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9349;

					{
						obj_t BgL_auxz00_9350;

						{	/* Liveness/liveness.scm 641 */
							BgL_objectz00_bglt BgL_tmpz00_9351;

							BgL_tmpz00_9351 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nz00_4705));
							BgL_auxz00_9350 = BGL_OBJECT_WIDENING(BgL_tmpz00_9351);
						}
						BgL_auxz00_9349 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9350);
					}
					BgL_val1_1717z00_5346 =
						(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_9349))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 641 */
					int BgL_tmpz00_9357;

					BgL_tmpz00_9357 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9357);
				}
				{	/* Liveness/liveness.scm 641 */
					int BgL_tmpz00_9360;

					BgL_tmpz00_9360 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9360, BgL_val1_1717z00_5346);
				}
				return BgL_val0_1716z00_5345;
			}
		}

	}



/* &inout!-cast-null/liv1989 */
	obj_t BGl_z62inoutz12zd2castzd2nullzf2liv1989z82zzliveness_livenessz00(obj_t
		BgL_envz00_4706, obj_t BgL_nz00_4707, obj_t BgL_oz00_4708)
	{
		{	/* Liveness/liveness.scm 632 */
			{
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9363;

				{
					obj_t BgL_auxz00_9364;

					{	/* Liveness/liveness.scm 634 */
						BgL_objectz00_bglt BgL_tmpz00_9365;

						BgL_tmpz00_9365 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_nz00_4707));
						BgL_auxz00_9364 = BGL_OBJECT_WIDENING(BgL_tmpz00_9365);
					}
					BgL_auxz00_9363 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9364);
				}
				((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_9363))->
						BgL_outz00) = ((obj_t) BgL_oz00_4708), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_9378;
				BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9371;

				{	/* Liveness/liveness.scm 635 */
					obj_t BgL_arg2321z00_5348;
					obj_t BgL_arg2323z00_5349;

					{
						BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9379;

						{
							obj_t BgL_auxz00_9380;

							{	/* Liveness/liveness.scm 635 */
								BgL_objectz00_bglt BgL_tmpz00_9381;

								BgL_tmpz00_9381 =
									((BgL_objectz00_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_nz00_4707));
								BgL_auxz00_9380 = BGL_OBJECT_WIDENING(BgL_tmpz00_9381);
							}
							BgL_auxz00_9379 =
								((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9380);
						}
						BgL_arg2321z00_5348 =
							(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_9379))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 635 */
						obj_t BgL_arg2324z00_5350;
						obj_t BgL_arg2325z00_5351;

						{
							BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9387;

							{
								obj_t BgL_auxz00_9388;

								{	/* Liveness/liveness.scm 635 */
									BgL_objectz00_bglt BgL_tmpz00_9389;

									BgL_tmpz00_9389 =
										((BgL_objectz00_bglt)
										((BgL_castzd2nullzd2_bglt) BgL_nz00_4707));
									BgL_auxz00_9388 = BGL_OBJECT_WIDENING(BgL_tmpz00_9389);
								}
								BgL_auxz00_9387 =
									((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9388);
							}
							BgL_arg2324z00_5350 =
								(((BgL_castzd2nullzf2livenessz20_bglt)
									COBJECT(BgL_auxz00_9387))->BgL_outz00);
						}
						{
							BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9395;

							{
								obj_t BgL_auxz00_9396;

								{	/* Liveness/liveness.scm 635 */
									BgL_objectz00_bglt BgL_tmpz00_9397;

									BgL_tmpz00_9397 =
										((BgL_objectz00_bglt)
										((BgL_castzd2nullzd2_bglt) BgL_nz00_4707));
									BgL_auxz00_9396 = BGL_OBJECT_WIDENING(BgL_tmpz00_9397);
								}
								BgL_auxz00_9395 =
									((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9396);
							}
							BgL_arg2325z00_5351 =
								(((BgL_castzd2nullzf2livenessz20_bglt)
									COBJECT(BgL_auxz00_9395))->BgL_defz00);
						}
						BgL_arg2323z00_5349 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2324z00_5350,
							BgL_arg2325z00_5351);
					}
					BgL_auxz00_9378 =
						BGl_unionz00zzliveness_setz00(BgL_arg2321z00_5348,
						BgL_arg2323z00_5349);
				}
				{
					obj_t BgL_auxz00_9372;

					{	/* Liveness/liveness.scm 635 */
						BgL_objectz00_bglt BgL_tmpz00_9373;

						BgL_tmpz00_9373 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_nz00_4707));
						BgL_auxz00_9372 = BGL_OBJECT_WIDENING(BgL_tmpz00_9373);
					}
					BgL_auxz00_9371 =
						((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9372);
				}
				((((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_9371))->
						BgL_inz00) = ((obj_t) BgL_auxz00_9378), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 636 */
				obj_t BgL_g1713z00_5352;

				BgL_g1713z00_5352 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nz00_4707))))->BgL_exprza2za2);
				{
					obj_t BgL_l1711z00_5354;

					BgL_l1711z00_5354 = BgL_g1713z00_5352;
				BgL_zc3z04anonymousza32326ze3z87_5353:
					if (PAIRP(BgL_l1711z00_5354))
						{	/* Liveness/liveness.scm 636 */
							{	/* Liveness/liveness.scm 636 */
								obj_t BgL_ez00_5355;

								BgL_ez00_5355 = CAR(BgL_l1711z00_5354);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5355), BgL_oz00_4708);
							}
							{
								obj_t BgL_l1711z00_9414;

								BgL_l1711z00_9414 = CDR(BgL_l1711z00_5354);
								BgL_l1711z00_5354 = BgL_l1711z00_9414;
								goto BgL_zc3z04anonymousza32326ze3z87_5353;
							}
						}
					else
						{	/* Liveness/liveness.scm 636 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 637 */
				obj_t BgL_val0_1714z00_5356;
				obj_t BgL_val1_1715z00_5357;

				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9416;

					{
						obj_t BgL_auxz00_9417;

						{	/* Liveness/liveness.scm 637 */
							BgL_objectz00_bglt BgL_tmpz00_9418;

							BgL_tmpz00_9418 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nz00_4707));
							BgL_auxz00_9417 = BGL_OBJECT_WIDENING(BgL_tmpz00_9418);
						}
						BgL_auxz00_9416 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9417);
					}
					BgL_val0_1714z00_5356 =
						(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_9416))->
						BgL_inz00);
				}
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9424;

					{
						obj_t BgL_auxz00_9425;

						{	/* Liveness/liveness.scm 637 */
							BgL_objectz00_bglt BgL_tmpz00_9426;

							BgL_tmpz00_9426 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nz00_4707));
							BgL_auxz00_9425 = BGL_OBJECT_WIDENING(BgL_tmpz00_9426);
						}
						BgL_auxz00_9424 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9425);
					}
					BgL_val1_1715z00_5357 =
						(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_9424))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 637 */
					int BgL_tmpz00_9432;

					BgL_tmpz00_9432 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9432);
				}
				{	/* Liveness/liveness.scm 637 */
					int BgL_tmpz00_9435;

					BgL_tmpz00_9435 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9435, BgL_val1_1715z00_5357);
				}
				return BgL_val0_1714z00_5356;
			}
		}

	}



/* &defuse-cast-null/liv1987 */
	obj_t BGl_z62defusezd2castzd2nullzf2liv1987z90zzliveness_livenessz00(obj_t
		BgL_envz00_4709, obj_t BgL_nz00_4710)
	{
		{	/* Liveness/liveness.scm 628 */
			{	/* Liveness/liveness.scm 630 */
				obj_t BgL_val0_1709z00_5359;
				obj_t BgL_val1_1710z00_5360;

				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9438;

					{
						obj_t BgL_auxz00_9439;

						{	/* Liveness/liveness.scm 630 */
							BgL_objectz00_bglt BgL_tmpz00_9440;

							BgL_tmpz00_9440 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nz00_4710));
							BgL_auxz00_9439 = BGL_OBJECT_WIDENING(BgL_tmpz00_9440);
						}
						BgL_auxz00_9438 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9439);
					}
					BgL_val0_1709z00_5359 =
						(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_9438))->
						BgL_defz00);
				}
				{
					BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9446;

					{
						obj_t BgL_auxz00_9447;

						{	/* Liveness/liveness.scm 630 */
							BgL_objectz00_bglt BgL_tmpz00_9448;

							BgL_tmpz00_9448 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nz00_4710));
							BgL_auxz00_9447 = BGL_OBJECT_WIDENING(BgL_tmpz00_9448);
						}
						BgL_auxz00_9446 =
							((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9447);
					}
					BgL_val1_1710z00_5360 =
						(((BgL_castzd2nullzf2livenessz20_bglt) COBJECT(BgL_auxz00_9446))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 630 */
					int BgL_tmpz00_9454;

					BgL_tmpz00_9454 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9454);
				}
				{	/* Liveness/liveness.scm 630 */
					int BgL_tmpz00_9457;

					BgL_tmpz00_9457 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9457, BgL_val1_1710z00_5360);
				}
				return BgL_val0_1709z00_5359;
			}
		}

	}



/* &defuse-cast-null1985 */
	obj_t BGl_z62defusezd2castzd2null1985z62zzliveness_livenessz00(obj_t
		BgL_envz00_4711, obj_t BgL_nz00_4712)
	{
		{	/* Liveness/liveness.scm 619 */
			{	/* Liveness/liveness.scm 621 */
				obj_t BgL_defz00_5362;

				{	/* Liveness/liveness.scm 622 */
					obj_t BgL_arg2320z00_5363;

					BgL_arg2320z00_5363 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_nz00_4712))))->BgL_exprza2za2);
					BgL_defz00_5362 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2320z00_5363, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 622 */
					obj_t BgL_usez00_5364;

					{	/* Liveness/liveness.scm 623 */
						obj_t BgL_tmpz00_5365;

						{	/* Liveness/liveness.scm 623 */
							int BgL_tmpz00_9464;

							BgL_tmpz00_9464 = (int) (1L);
							BgL_tmpz00_5365 = BGL_MVALUES_VAL(BgL_tmpz00_9464);
						}
						{	/* Liveness/liveness.scm 623 */
							int BgL_tmpz00_9467;

							BgL_tmpz00_9467 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_9467, BUNSPEC);
						}
						BgL_usez00_5364 = BgL_tmpz00_5365;
					}
					{	/* Liveness/liveness.scm 624 */
						BgL_castzd2nullzd2_bglt BgL_arg2318z00_5366;

						{	/* Liveness/liveness.scm 624 */
							BgL_castzd2nullzf2livenessz20_bglt BgL_wide1299z00_5367;

							BgL_wide1299z00_5367 =
								((BgL_castzd2nullzf2livenessz20_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_castzd2nullzf2livenessz20_bgl))));
							{	/* Liveness/liveness.scm 624 */
								obj_t BgL_auxz00_9475;
								BgL_objectz00_bglt BgL_tmpz00_9471;

								BgL_auxz00_9475 = ((obj_t) BgL_wide1299z00_5367);
								BgL_tmpz00_9471 =
									((BgL_objectz00_bglt)
									((BgL_castzd2nullzd2_bglt)
										((BgL_castzd2nullzd2_bglt) BgL_nz00_4712)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9471, BgL_auxz00_9475);
							}
							((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_nz00_4712)));
							{	/* Liveness/liveness.scm 624 */
								long BgL_arg2319z00_5368;

								BgL_arg2319z00_5368 =
									BGL_CLASS_NUM
									(BGl_castzd2nullzf2livenessz20zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
											(BgL_castzd2nullzd2_bglt) ((BgL_castzd2nullzd2_bglt)
												BgL_nz00_4712))), BgL_arg2319z00_5368);
							}
							((BgL_castzd2nullzd2_bglt)
								((BgL_castzd2nullzd2_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_nz00_4712)));
						}
						{
							BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9489;

							{
								obj_t BgL_auxz00_9490;

								{	/* Liveness/liveness.scm 625 */
									BgL_objectz00_bglt BgL_tmpz00_9491;

									BgL_tmpz00_9491 =
										((BgL_objectz00_bglt)
										((BgL_castzd2nullzd2_bglt)
											((BgL_castzd2nullzd2_bglt) BgL_nz00_4712)));
									BgL_auxz00_9490 = BGL_OBJECT_WIDENING(BgL_tmpz00_9491);
								}
								BgL_auxz00_9489 =
									((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9490);
							}
							((((BgL_castzd2nullzf2livenessz20_bglt)
										COBJECT(BgL_auxz00_9489))->BgL_defz00) =
								((obj_t) BgL_defz00_5362), BUNSPEC);
						}
						{
							BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9498;

							{
								obj_t BgL_auxz00_9499;

								{	/* Liveness/liveness.scm 626 */
									BgL_objectz00_bglt BgL_tmpz00_9500;

									BgL_tmpz00_9500 =
										((BgL_objectz00_bglt)
										((BgL_castzd2nullzd2_bglt)
											((BgL_castzd2nullzd2_bglt) BgL_nz00_4712)));
									BgL_auxz00_9499 = BGL_OBJECT_WIDENING(BgL_tmpz00_9500);
								}
								BgL_auxz00_9498 =
									((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9499);
							}
							((((BgL_castzd2nullzf2livenessz20_bglt)
										COBJECT(BgL_auxz00_9498))->BgL_usez00) =
								((obj_t) BgL_usez00_5364), BUNSPEC);
						}
						{
							BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9507;

							{
								obj_t BgL_auxz00_9508;

								{	/* Liveness/liveness.scm 622 */
									BgL_objectz00_bglt BgL_tmpz00_9509;

									BgL_tmpz00_9509 =
										((BgL_objectz00_bglt)
										((BgL_castzd2nullzd2_bglt)
											((BgL_castzd2nullzd2_bglt) BgL_nz00_4712)));
									BgL_auxz00_9508 = BGL_OBJECT_WIDENING(BgL_tmpz00_9509);
								}
								BgL_auxz00_9507 =
									((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9508);
							}
							((((BgL_castzd2nullzf2livenessz20_bglt)
										COBJECT(BgL_auxz00_9507))->BgL_inz00) =
								((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_castzd2nullzf2livenessz20_bglt BgL_auxz00_9516;

							{
								obj_t BgL_auxz00_9517;

								{	/* Liveness/liveness.scm 622 */
									BgL_objectz00_bglt BgL_tmpz00_9518;

									BgL_tmpz00_9518 =
										((BgL_objectz00_bglt)
										((BgL_castzd2nullzd2_bglt)
											((BgL_castzd2nullzd2_bglt) BgL_nz00_4712)));
									BgL_auxz00_9517 = BGL_OBJECT_WIDENING(BgL_tmpz00_9518);
								}
								BgL_auxz00_9516 =
									((BgL_castzd2nullzf2livenessz20_bglt) BgL_auxz00_9517);
							}
							((((BgL_castzd2nullzf2livenessz20_bglt)
										COBJECT(BgL_auxz00_9516))->BgL_outz00) =
								((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2318z00_5366 =
							((BgL_castzd2nullzd2_bglt)
							((BgL_castzd2nullzd2_bglt) BgL_nz00_4712));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2318z00_5366));
					}
				}
			}
		}

	}



/* &inout-instanceof/liv1983 */
	obj_t BGl_z62inoutzd2instanceofzf2liv1983z42zzliveness_livenessz00(obj_t
		BgL_envz00_4713, obj_t BgL_nz00_4714)
	{
		{	/* Liveness/liveness.scm 612 */
			{	/* Liveness/liveness.scm 614 */
				obj_t BgL_val0_1707z00_5370;
				obj_t BgL_val1_1708z00_5371;

				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9529;

					{
						obj_t BgL_auxz00_9530;

						{	/* Liveness/liveness.scm 614 */
							BgL_objectz00_bglt BgL_tmpz00_9531;

							BgL_tmpz00_9531 =
								((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_nz00_4714));
							BgL_auxz00_9530 = BGL_OBJECT_WIDENING(BgL_tmpz00_9531);
						}
						BgL_auxz00_9529 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9530);
					}
					BgL_val0_1707z00_5370 =
						(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9529))->
						BgL_inz00);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9537;

					{
						obj_t BgL_auxz00_9538;

						{	/* Liveness/liveness.scm 614 */
							BgL_objectz00_bglt BgL_tmpz00_9539;

							BgL_tmpz00_9539 =
								((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_nz00_4714));
							BgL_auxz00_9538 = BGL_OBJECT_WIDENING(BgL_tmpz00_9539);
						}
						BgL_auxz00_9537 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9538);
					}
					BgL_val1_1708z00_5371 =
						(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9537))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 614 */
					int BgL_tmpz00_9545;

					BgL_tmpz00_9545 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9545);
				}
				{	/* Liveness/liveness.scm 614 */
					int BgL_tmpz00_9548;

					BgL_tmpz00_9548 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9548, BgL_val1_1708z00_5371);
				}
				return BgL_val0_1707z00_5370;
			}
		}

	}



/* &inout!-instanceof/li1981 */
	obj_t BGl_z62inoutz12zd2instanceofzf2li1981z50zzliveness_livenessz00(obj_t
		BgL_envz00_4715, obj_t BgL_nz00_4716, obj_t BgL_oz00_4717)
	{
		{	/* Liveness/liveness.scm 605 */
			{
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9551;

				{
					obj_t BgL_auxz00_9552;

					{	/* Liveness/liveness.scm 607 */
						BgL_objectz00_bglt BgL_tmpz00_9553;

						BgL_tmpz00_9553 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_nz00_4716));
						BgL_auxz00_9552 = BGL_OBJECT_WIDENING(BgL_tmpz00_9553);
					}
					BgL_auxz00_9551 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9552);
				}
				((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9551))->
						BgL_outz00) = ((obj_t) BgL_oz00_4717), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_9566;
				BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9559;

				{	/* Liveness/liveness.scm 608 */
					obj_t BgL_arg2311z00_5373;
					obj_t BgL_arg2312z00_5374;

					{
						BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9567;

						{
							obj_t BgL_auxz00_9568;

							{	/* Liveness/liveness.scm 608 */
								BgL_objectz00_bglt BgL_tmpz00_9569;

								BgL_tmpz00_9569 =
									((BgL_objectz00_bglt)
									((BgL_instanceofz00_bglt) BgL_nz00_4716));
								BgL_auxz00_9568 = BGL_OBJECT_WIDENING(BgL_tmpz00_9569);
							}
							BgL_auxz00_9567 =
								((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9568);
						}
						BgL_arg2311z00_5373 =
							(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9567))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 608 */
						obj_t BgL_arg2313z00_5375;
						obj_t BgL_arg2314z00_5376;

						{
							BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9575;

							{
								obj_t BgL_auxz00_9576;

								{	/* Liveness/liveness.scm 608 */
									BgL_objectz00_bglt BgL_tmpz00_9577;

									BgL_tmpz00_9577 =
										((BgL_objectz00_bglt)
										((BgL_instanceofz00_bglt) BgL_nz00_4716));
									BgL_auxz00_9576 = BGL_OBJECT_WIDENING(BgL_tmpz00_9577);
								}
								BgL_auxz00_9575 =
									((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9576);
							}
							BgL_arg2313z00_5375 =
								(((BgL_instanceofzf2livenesszf2_bglt)
									COBJECT(BgL_auxz00_9575))->BgL_outz00);
						}
						{
							BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9583;

							{
								obj_t BgL_auxz00_9584;

								{	/* Liveness/liveness.scm 608 */
									BgL_objectz00_bglt BgL_tmpz00_9585;

									BgL_tmpz00_9585 =
										((BgL_objectz00_bglt)
										((BgL_instanceofz00_bglt) BgL_nz00_4716));
									BgL_auxz00_9584 = BGL_OBJECT_WIDENING(BgL_tmpz00_9585);
								}
								BgL_auxz00_9583 =
									((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9584);
							}
							BgL_arg2314z00_5376 =
								(((BgL_instanceofzf2livenesszf2_bglt)
									COBJECT(BgL_auxz00_9583))->BgL_defz00);
						}
						BgL_arg2312z00_5374 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2313z00_5375,
							BgL_arg2314z00_5376);
					}
					BgL_auxz00_9566 =
						BGl_unionz00zzliveness_setz00(BgL_arg2311z00_5373,
						BgL_arg2312z00_5374);
				}
				{
					obj_t BgL_auxz00_9560;

					{	/* Liveness/liveness.scm 608 */
						BgL_objectz00_bglt BgL_tmpz00_9561;

						BgL_tmpz00_9561 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_nz00_4716));
						BgL_auxz00_9560 = BGL_OBJECT_WIDENING(BgL_tmpz00_9561);
					}
					BgL_auxz00_9559 =
						((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9560);
				}
				((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9559))->
						BgL_inz00) = ((obj_t) BgL_auxz00_9566), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 609 */
				obj_t BgL_g1704z00_5377;

				BgL_g1704z00_5377 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_instanceofz00_bglt) BgL_nz00_4716))))->BgL_exprza2za2);
				{
					obj_t BgL_l1702z00_5379;

					BgL_l1702z00_5379 = BgL_g1704z00_5377;
				BgL_zc3z04anonymousza32315ze3z87_5378:
					if (PAIRP(BgL_l1702z00_5379))
						{	/* Liveness/liveness.scm 609 */
							{	/* Liveness/liveness.scm 609 */
								obj_t BgL_ez00_5380;

								BgL_ez00_5380 = CAR(BgL_l1702z00_5379);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5380), BgL_oz00_4717);
							}
							{
								obj_t BgL_l1702z00_9602;

								BgL_l1702z00_9602 = CDR(BgL_l1702z00_5379);
								BgL_l1702z00_5379 = BgL_l1702z00_9602;
								goto BgL_zc3z04anonymousza32315ze3z87_5378;
							}
						}
					else
						{	/* Liveness/liveness.scm 609 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 610 */
				obj_t BgL_val0_1705z00_5381;
				obj_t BgL_val1_1706z00_5382;

				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9604;

					{
						obj_t BgL_auxz00_9605;

						{	/* Liveness/liveness.scm 610 */
							BgL_objectz00_bglt BgL_tmpz00_9606;

							BgL_tmpz00_9606 =
								((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_nz00_4716));
							BgL_auxz00_9605 = BGL_OBJECT_WIDENING(BgL_tmpz00_9606);
						}
						BgL_auxz00_9604 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9605);
					}
					BgL_val0_1705z00_5381 =
						(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9604))->
						BgL_inz00);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9612;

					{
						obj_t BgL_auxz00_9613;

						{	/* Liveness/liveness.scm 610 */
							BgL_objectz00_bglt BgL_tmpz00_9614;

							BgL_tmpz00_9614 =
								((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_nz00_4716));
							BgL_auxz00_9613 = BGL_OBJECT_WIDENING(BgL_tmpz00_9614);
						}
						BgL_auxz00_9612 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9613);
					}
					BgL_val1_1706z00_5382 =
						(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9612))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 610 */
					int BgL_tmpz00_9620;

					BgL_tmpz00_9620 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9620);
				}
				{	/* Liveness/liveness.scm 610 */
					int BgL_tmpz00_9623;

					BgL_tmpz00_9623 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9623, BgL_val1_1706z00_5382);
				}
				return BgL_val0_1705z00_5381;
			}
		}

	}



/* &defuse-instanceof/li1979 */
	obj_t BGl_z62defusezd2instanceofzf2li1979z42zzliveness_livenessz00(obj_t
		BgL_envz00_4718, obj_t BgL_nz00_4719)
	{
		{	/* Liveness/liveness.scm 601 */
			{	/* Liveness/liveness.scm 603 */
				obj_t BgL_val0_1700z00_5384;
				obj_t BgL_val1_1701z00_5385;

				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9626;

					{
						obj_t BgL_auxz00_9627;

						{	/* Liveness/liveness.scm 603 */
							BgL_objectz00_bglt BgL_tmpz00_9628;

							BgL_tmpz00_9628 =
								((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_nz00_4719));
							BgL_auxz00_9627 = BGL_OBJECT_WIDENING(BgL_tmpz00_9628);
						}
						BgL_auxz00_9626 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9627);
					}
					BgL_val0_1700z00_5384 =
						(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9626))->
						BgL_defz00);
				}
				{
					BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9634;

					{
						obj_t BgL_auxz00_9635;

						{	/* Liveness/liveness.scm 603 */
							BgL_objectz00_bglt BgL_tmpz00_9636;

							BgL_tmpz00_9636 =
								((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_nz00_4719));
							BgL_auxz00_9635 = BGL_OBJECT_WIDENING(BgL_tmpz00_9636);
						}
						BgL_auxz00_9634 =
							((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9635);
					}
					BgL_val1_1701z00_5385 =
						(((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9634))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 603 */
					int BgL_tmpz00_9642;

					BgL_tmpz00_9642 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9642);
				}
				{	/* Liveness/liveness.scm 603 */
					int BgL_tmpz00_9645;

					BgL_tmpz00_9645 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9645, BgL_val1_1701z00_5385);
				}
				return BgL_val0_1700z00_5384;
			}
		}

	}



/* &defuse-instanceof1977 */
	obj_t BGl_z62defusezd2instanceof1977zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4720, obj_t BgL_nz00_4721)
	{
		{	/* Liveness/liveness.scm 592 */
			{	/* Liveness/liveness.scm 594 */
				obj_t BgL_defz00_5387;

				{	/* Liveness/liveness.scm 595 */
					obj_t BgL_arg2310z00_5388;

					BgL_arg2310z00_5388 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_instanceofz00_bglt) BgL_nz00_4721))))->BgL_exprza2za2);
					BgL_defz00_5387 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2310z00_5388, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 595 */
					obj_t BgL_usez00_5389;

					{	/* Liveness/liveness.scm 596 */
						obj_t BgL_tmpz00_5390;

						{	/* Liveness/liveness.scm 596 */
							int BgL_tmpz00_9652;

							BgL_tmpz00_9652 = (int) (1L);
							BgL_tmpz00_5390 = BGL_MVALUES_VAL(BgL_tmpz00_9652);
						}
						{	/* Liveness/liveness.scm 596 */
							int BgL_tmpz00_9655;

							BgL_tmpz00_9655 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_9655, BUNSPEC);
						}
						BgL_usez00_5389 = BgL_tmpz00_5390;
					}
					{	/* Liveness/liveness.scm 597 */
						BgL_instanceofz00_bglt BgL_arg2308z00_5391;

						{	/* Liveness/liveness.scm 597 */
							BgL_instanceofzf2livenesszf2_bglt BgL_wide1291z00_5392;

							BgL_wide1291z00_5392 =
								((BgL_instanceofzf2livenesszf2_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_instanceofzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 597 */
								obj_t BgL_auxz00_9663;
								BgL_objectz00_bglt BgL_tmpz00_9659;

								BgL_auxz00_9663 = ((obj_t) BgL_wide1291z00_5392);
								BgL_tmpz00_9659 =
									((BgL_objectz00_bglt)
									((BgL_instanceofz00_bglt)
										((BgL_instanceofz00_bglt) BgL_nz00_4721)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9659, BgL_auxz00_9663);
							}
							((BgL_objectz00_bglt)
								((BgL_instanceofz00_bglt)
									((BgL_instanceofz00_bglt) BgL_nz00_4721)));
							{	/* Liveness/liveness.scm 597 */
								long BgL_arg2309z00_5393;

								BgL_arg2309z00_5393 =
									BGL_CLASS_NUM
									(BGl_instanceofzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
											(BgL_instanceofz00_bglt) ((BgL_instanceofz00_bglt)
												BgL_nz00_4721))), BgL_arg2309z00_5393);
							}
							((BgL_instanceofz00_bglt)
								((BgL_instanceofz00_bglt)
									((BgL_instanceofz00_bglt) BgL_nz00_4721)));
						}
						{
							BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9677;

							{
								obj_t BgL_auxz00_9678;

								{	/* Liveness/liveness.scm 598 */
									BgL_objectz00_bglt BgL_tmpz00_9679;

									BgL_tmpz00_9679 =
										((BgL_objectz00_bglt)
										((BgL_instanceofz00_bglt)
											((BgL_instanceofz00_bglt) BgL_nz00_4721)));
									BgL_auxz00_9678 = BGL_OBJECT_WIDENING(BgL_tmpz00_9679);
								}
								BgL_auxz00_9677 =
									((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9678);
							}
							((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9677))->
									BgL_defz00) = ((obj_t) BgL_defz00_5387), BUNSPEC);
						}
						{
							BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9686;

							{
								obj_t BgL_auxz00_9687;

								{	/* Liveness/liveness.scm 599 */
									BgL_objectz00_bglt BgL_tmpz00_9688;

									BgL_tmpz00_9688 =
										((BgL_objectz00_bglt)
										((BgL_instanceofz00_bglt)
											((BgL_instanceofz00_bglt) BgL_nz00_4721)));
									BgL_auxz00_9687 = BGL_OBJECT_WIDENING(BgL_tmpz00_9688);
								}
								BgL_auxz00_9686 =
									((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9687);
							}
							((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9686))->
									BgL_usez00) = ((obj_t) BgL_usez00_5389), BUNSPEC);
						}
						{
							BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9695;

							{
								obj_t BgL_auxz00_9696;

								{	/* Liveness/liveness.scm 595 */
									BgL_objectz00_bglt BgL_tmpz00_9697;

									BgL_tmpz00_9697 =
										((BgL_objectz00_bglt)
										((BgL_instanceofz00_bglt)
											((BgL_instanceofz00_bglt) BgL_nz00_4721)));
									BgL_auxz00_9696 = BGL_OBJECT_WIDENING(BgL_tmpz00_9697);
								}
								BgL_auxz00_9695 =
									((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9696);
							}
							((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9695))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_instanceofzf2livenesszf2_bglt BgL_auxz00_9704;

							{
								obj_t BgL_auxz00_9705;

								{	/* Liveness/liveness.scm 595 */
									BgL_objectz00_bglt BgL_tmpz00_9706;

									BgL_tmpz00_9706 =
										((BgL_objectz00_bglt)
										((BgL_instanceofz00_bglt)
											((BgL_instanceofz00_bglt) BgL_nz00_4721)));
									BgL_auxz00_9705 = BGL_OBJECT_WIDENING(BgL_tmpz00_9706);
								}
								BgL_auxz00_9704 =
									((BgL_instanceofzf2livenesszf2_bglt) BgL_auxz00_9705);
							}
							((((BgL_instanceofzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9704))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2308z00_5391 =
							((BgL_instanceofz00_bglt)
							((BgL_instanceofz00_bglt) BgL_nz00_4721));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2308z00_5391));
					}
				}
			}
		}

	}



/* &inout-vlength/livene1975 */
	obj_t BGl_z62inoutzd2vlengthzf2livene1975z42zzliveness_livenessz00(obj_t
		BgL_envz00_4722, obj_t BgL_nz00_4723)
	{
		{	/* Liveness/liveness.scm 585 */
			{	/* Liveness/liveness.scm 587 */
				obj_t BgL_val0_1698z00_5395;
				obj_t BgL_val1_1699z00_5396;

				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9717;

					{
						obj_t BgL_auxz00_9718;

						{	/* Liveness/liveness.scm 587 */
							BgL_objectz00_bglt BgL_tmpz00_9719;

							BgL_tmpz00_9719 =
								((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4723));
							BgL_auxz00_9718 = BGL_OBJECT_WIDENING(BgL_tmpz00_9719);
						}
						BgL_auxz00_9717 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9718);
					}
					BgL_val0_1698z00_5395 =
						(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9717))->
						BgL_inz00);
				}
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9725;

					{
						obj_t BgL_auxz00_9726;

						{	/* Liveness/liveness.scm 587 */
							BgL_objectz00_bglt BgL_tmpz00_9727;

							BgL_tmpz00_9727 =
								((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4723));
							BgL_auxz00_9726 = BGL_OBJECT_WIDENING(BgL_tmpz00_9727);
						}
						BgL_auxz00_9725 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9726);
					}
					BgL_val1_1699z00_5396 =
						(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9725))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 587 */
					int BgL_tmpz00_9733;

					BgL_tmpz00_9733 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9733);
				}
				{	/* Liveness/liveness.scm 587 */
					int BgL_tmpz00_9736;

					BgL_tmpz00_9736 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9736, BgL_val1_1699z00_5396);
				}
				return BgL_val0_1698z00_5395;
			}
		}

	}



/* &inout!-vlength/liven1973 */
	obj_t BGl_z62inoutz12zd2vlengthzf2liven1973z50zzliveness_livenessz00(obj_t
		BgL_envz00_4724, obj_t BgL_nz00_4725, obj_t BgL_oz00_4726)
	{
		{	/* Liveness/liveness.scm 578 */
			{
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9739;

				{
					obj_t BgL_auxz00_9740;

					{	/* Liveness/liveness.scm 580 */
						BgL_objectz00_bglt BgL_tmpz00_9741;

						BgL_tmpz00_9741 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4725));
						BgL_auxz00_9740 = BGL_OBJECT_WIDENING(BgL_tmpz00_9741);
					}
					BgL_auxz00_9739 = ((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9740);
				}
				((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9739))->
						BgL_outz00) = ((obj_t) BgL_oz00_4726), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_9754;
				BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9747;

				{	/* Liveness/liveness.scm 581 */
					obj_t BgL_arg2299z00_5398;
					obj_t BgL_arg2301z00_5399;

					{
						BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9755;

						{
							obj_t BgL_auxz00_9756;

							{	/* Liveness/liveness.scm 581 */
								BgL_objectz00_bglt BgL_tmpz00_9757;

								BgL_tmpz00_9757 =
									((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4725));
								BgL_auxz00_9756 = BGL_OBJECT_WIDENING(BgL_tmpz00_9757);
							}
							BgL_auxz00_9755 =
								((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9756);
						}
						BgL_arg2299z00_5398 =
							(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9755))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 581 */
						obj_t BgL_arg2302z00_5400;
						obj_t BgL_arg2304z00_5401;

						{
							BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9763;

							{
								obj_t BgL_auxz00_9764;

								{	/* Liveness/liveness.scm 581 */
									BgL_objectz00_bglt BgL_tmpz00_9765;

									BgL_tmpz00_9765 =
										((BgL_objectz00_bglt)
										((BgL_vlengthz00_bglt) BgL_nz00_4725));
									BgL_auxz00_9764 = BGL_OBJECT_WIDENING(BgL_tmpz00_9765);
								}
								BgL_auxz00_9763 =
									((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9764);
							}
							BgL_arg2302z00_5400 =
								(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9763))->
								BgL_outz00);
						}
						{
							BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9771;

							{
								obj_t BgL_auxz00_9772;

								{	/* Liveness/liveness.scm 581 */
									BgL_objectz00_bglt BgL_tmpz00_9773;

									BgL_tmpz00_9773 =
										((BgL_objectz00_bglt)
										((BgL_vlengthz00_bglt) BgL_nz00_4725));
									BgL_auxz00_9772 = BGL_OBJECT_WIDENING(BgL_tmpz00_9773);
								}
								BgL_auxz00_9771 =
									((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9772);
							}
							BgL_arg2304z00_5401 =
								(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9771))->
								BgL_defz00);
						}
						BgL_arg2301z00_5399 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2302z00_5400,
							BgL_arg2304z00_5401);
					}
					BgL_auxz00_9754 =
						BGl_unionz00zzliveness_setz00(BgL_arg2299z00_5398,
						BgL_arg2301z00_5399);
				}
				{
					obj_t BgL_auxz00_9748;

					{	/* Liveness/liveness.scm 581 */
						BgL_objectz00_bglt BgL_tmpz00_9749;

						BgL_tmpz00_9749 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4725));
						BgL_auxz00_9748 = BGL_OBJECT_WIDENING(BgL_tmpz00_9749);
					}
					BgL_auxz00_9747 = ((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9748);
				}
				((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9747))->
						BgL_inz00) = ((obj_t) BgL_auxz00_9754), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 582 */
				obj_t BgL_g1695z00_5402;

				BgL_g1695z00_5402 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vlengthz00_bglt) BgL_nz00_4725))))->BgL_exprza2za2);
				{
					obj_t BgL_l1693z00_5404;

					BgL_l1693z00_5404 = BgL_g1695z00_5402;
				BgL_zc3z04anonymousza32305ze3z87_5403:
					if (PAIRP(BgL_l1693z00_5404))
						{	/* Liveness/liveness.scm 582 */
							{	/* Liveness/liveness.scm 582 */
								obj_t BgL_ez00_5405;

								BgL_ez00_5405 = CAR(BgL_l1693z00_5404);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5405), BgL_oz00_4726);
							}
							{
								obj_t BgL_l1693z00_9790;

								BgL_l1693z00_9790 = CDR(BgL_l1693z00_5404);
								BgL_l1693z00_5404 = BgL_l1693z00_9790;
								goto BgL_zc3z04anonymousza32305ze3z87_5403;
							}
						}
					else
						{	/* Liveness/liveness.scm 582 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 583 */
				obj_t BgL_val0_1696z00_5406;
				obj_t BgL_val1_1697z00_5407;

				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9792;

					{
						obj_t BgL_auxz00_9793;

						{	/* Liveness/liveness.scm 583 */
							BgL_objectz00_bglt BgL_tmpz00_9794;

							BgL_tmpz00_9794 =
								((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4725));
							BgL_auxz00_9793 = BGL_OBJECT_WIDENING(BgL_tmpz00_9794);
						}
						BgL_auxz00_9792 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9793);
					}
					BgL_val0_1696z00_5406 =
						(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9792))->
						BgL_inz00);
				}
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9800;

					{
						obj_t BgL_auxz00_9801;

						{	/* Liveness/liveness.scm 583 */
							BgL_objectz00_bglt BgL_tmpz00_9802;

							BgL_tmpz00_9802 =
								((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4725));
							BgL_auxz00_9801 = BGL_OBJECT_WIDENING(BgL_tmpz00_9802);
						}
						BgL_auxz00_9800 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9801);
					}
					BgL_val1_1697z00_5407 =
						(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9800))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 583 */
					int BgL_tmpz00_9808;

					BgL_tmpz00_9808 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9808);
				}
				{	/* Liveness/liveness.scm 583 */
					int BgL_tmpz00_9811;

					BgL_tmpz00_9811 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9811, BgL_val1_1697z00_5407);
				}
				return BgL_val0_1696z00_5406;
			}
		}

	}



/* &defuse-vlength/liven1971 */
	obj_t BGl_z62defusezd2vlengthzf2liven1971z42zzliveness_livenessz00(obj_t
		BgL_envz00_4727, obj_t BgL_nz00_4728)
	{
		{	/* Liveness/liveness.scm 574 */
			{	/* Liveness/liveness.scm 576 */
				obj_t BgL_val0_1691z00_5409;
				obj_t BgL_val1_1692z00_5410;

				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9814;

					{
						obj_t BgL_auxz00_9815;

						{	/* Liveness/liveness.scm 576 */
							BgL_objectz00_bglt BgL_tmpz00_9816;

							BgL_tmpz00_9816 =
								((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4728));
							BgL_auxz00_9815 = BGL_OBJECT_WIDENING(BgL_tmpz00_9816);
						}
						BgL_auxz00_9814 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9815);
					}
					BgL_val0_1691z00_5409 =
						(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9814))->
						BgL_defz00);
				}
				{
					BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9822;

					{
						obj_t BgL_auxz00_9823;

						{	/* Liveness/liveness.scm 576 */
							BgL_objectz00_bglt BgL_tmpz00_9824;

							BgL_tmpz00_9824 =
								((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4728));
							BgL_auxz00_9823 = BGL_OBJECT_WIDENING(BgL_tmpz00_9824);
						}
						BgL_auxz00_9822 =
							((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9823);
					}
					BgL_val1_1692z00_5410 =
						(((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9822))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 576 */
					int BgL_tmpz00_9830;

					BgL_tmpz00_9830 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9830);
				}
				{	/* Liveness/liveness.scm 576 */
					int BgL_tmpz00_9833;

					BgL_tmpz00_9833 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9833, BgL_val1_1692z00_5410);
				}
				return BgL_val0_1691z00_5409;
			}
		}

	}



/* &defuse-vlength1969 */
	obj_t BGl_z62defusezd2vlength1969zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4729, obj_t BgL_nz00_4730)
	{
		{	/* Liveness/liveness.scm 565 */
			{	/* Liveness/liveness.scm 567 */
				obj_t BgL_defz00_5412;

				{	/* Liveness/liveness.scm 568 */
					obj_t BgL_arg2298z00_5413;

					BgL_arg2298z00_5413 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vlengthz00_bglt) BgL_nz00_4730))))->BgL_exprza2za2);
					BgL_defz00_5412 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2298z00_5413, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 568 */
					obj_t BgL_usez00_5414;

					{	/* Liveness/liveness.scm 569 */
						obj_t BgL_tmpz00_5415;

						{	/* Liveness/liveness.scm 569 */
							int BgL_tmpz00_9840;

							BgL_tmpz00_9840 = (int) (1L);
							BgL_tmpz00_5415 = BGL_MVALUES_VAL(BgL_tmpz00_9840);
						}
						{	/* Liveness/liveness.scm 569 */
							int BgL_tmpz00_9843;

							BgL_tmpz00_9843 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_9843, BUNSPEC);
						}
						BgL_usez00_5414 = BgL_tmpz00_5415;
					}
					{	/* Liveness/liveness.scm 570 */
						BgL_vlengthz00_bglt BgL_arg2296z00_5416;

						{	/* Liveness/liveness.scm 570 */
							BgL_vlengthzf2livenesszf2_bglt BgL_wide1283z00_5417;

							BgL_wide1283z00_5417 =
								((BgL_vlengthzf2livenesszf2_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vlengthzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 570 */
								obj_t BgL_auxz00_9851;
								BgL_objectz00_bglt BgL_tmpz00_9847;

								BgL_auxz00_9851 = ((obj_t) BgL_wide1283z00_5417);
								BgL_tmpz00_9847 =
									((BgL_objectz00_bglt)
									((BgL_vlengthz00_bglt)
										((BgL_vlengthz00_bglt) BgL_nz00_4730)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9847, BgL_auxz00_9851);
							}
							((BgL_objectz00_bglt)
								((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4730)));
							{	/* Liveness/liveness.scm 570 */
								long BgL_arg2297z00_5418;

								BgL_arg2297z00_5418 =
									BGL_CLASS_NUM(BGl_vlengthzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_vlengthz00_bglt)
											((BgL_vlengthz00_bglt) BgL_nz00_4730))),
									BgL_arg2297z00_5418);
							}
							((BgL_vlengthz00_bglt)
								((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4730)));
						}
						{
							BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9865;

							{
								obj_t BgL_auxz00_9866;

								{	/* Liveness/liveness.scm 571 */
									BgL_objectz00_bglt BgL_tmpz00_9867;

									BgL_tmpz00_9867 =
										((BgL_objectz00_bglt)
										((BgL_vlengthz00_bglt)
											((BgL_vlengthz00_bglt) BgL_nz00_4730)));
									BgL_auxz00_9866 = BGL_OBJECT_WIDENING(BgL_tmpz00_9867);
								}
								BgL_auxz00_9865 =
									((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9866);
							}
							((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9865))->
									BgL_defz00) = ((obj_t) BgL_defz00_5412), BUNSPEC);
						}
						{
							BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9874;

							{
								obj_t BgL_auxz00_9875;

								{	/* Liveness/liveness.scm 572 */
									BgL_objectz00_bglt BgL_tmpz00_9876;

									BgL_tmpz00_9876 =
										((BgL_objectz00_bglt)
										((BgL_vlengthz00_bglt)
											((BgL_vlengthz00_bglt) BgL_nz00_4730)));
									BgL_auxz00_9875 = BGL_OBJECT_WIDENING(BgL_tmpz00_9876);
								}
								BgL_auxz00_9874 =
									((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9875);
							}
							((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9874))->
									BgL_usez00) = ((obj_t) BgL_usez00_5414), BUNSPEC);
						}
						{
							BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9883;

							{
								obj_t BgL_auxz00_9884;

								{	/* Liveness/liveness.scm 568 */
									BgL_objectz00_bglt BgL_tmpz00_9885;

									BgL_tmpz00_9885 =
										((BgL_objectz00_bglt)
										((BgL_vlengthz00_bglt)
											((BgL_vlengthz00_bglt) BgL_nz00_4730)));
									BgL_auxz00_9884 = BGL_OBJECT_WIDENING(BgL_tmpz00_9885);
								}
								BgL_auxz00_9883 =
									((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9884);
							}
							((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9883))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_vlengthzf2livenesszf2_bglt BgL_auxz00_9892;

							{
								obj_t BgL_auxz00_9893;

								{	/* Liveness/liveness.scm 568 */
									BgL_objectz00_bglt BgL_tmpz00_9894;

									BgL_tmpz00_9894 =
										((BgL_objectz00_bglt)
										((BgL_vlengthz00_bglt)
											((BgL_vlengthz00_bglt) BgL_nz00_4730)));
									BgL_auxz00_9893 = BGL_OBJECT_WIDENING(BgL_tmpz00_9894);
								}
								BgL_auxz00_9892 =
									((BgL_vlengthzf2livenesszf2_bglt) BgL_auxz00_9893);
							}
							((((BgL_vlengthzf2livenesszf2_bglt) COBJECT(BgL_auxz00_9892))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2296z00_5416 =
							((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_nz00_4730));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2296z00_5416));
					}
				}
			}
		}

	}



/* &inout-vset!/liveness1967 */
	obj_t BGl_z62inoutzd2vsetz12zf2liveness1967z50zzliveness_livenessz00(obj_t
		BgL_envz00_4731, obj_t BgL_nz00_4732)
	{
		{	/* Liveness/liveness.scm 558 */
			{	/* Liveness/liveness.scm 560 */
				obj_t BgL_val0_1689z00_5420;
				obj_t BgL_val1_1690z00_5421;

				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_9905;

					{
						obj_t BgL_auxz00_9906;

						{	/* Liveness/liveness.scm 560 */
							BgL_objectz00_bglt BgL_tmpz00_9907;

							BgL_tmpz00_9907 =
								((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4732));
							BgL_auxz00_9906 = BGL_OBJECT_WIDENING(BgL_tmpz00_9907);
						}
						BgL_auxz00_9905 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_9906);
					}
					BgL_val0_1689z00_5420 =
						(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_9905))->
						BgL_inz00);
				}
				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_9913;

					{
						obj_t BgL_auxz00_9914;

						{	/* Liveness/liveness.scm 560 */
							BgL_objectz00_bglt BgL_tmpz00_9915;

							BgL_tmpz00_9915 =
								((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4732));
							BgL_auxz00_9914 = BGL_OBJECT_WIDENING(BgL_tmpz00_9915);
						}
						BgL_auxz00_9913 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_9914);
					}
					BgL_val1_1690z00_5421 =
						(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_9913))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 560 */
					int BgL_tmpz00_9921;

					BgL_tmpz00_9921 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9921);
				}
				{	/* Liveness/liveness.scm 560 */
					int BgL_tmpz00_9924;

					BgL_tmpz00_9924 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9924, BgL_val1_1690z00_5421);
				}
				return BgL_val0_1689z00_5420;
			}
		}

	}



/* &inout!-vset!/livenes1965 */
	obj_t BGl_z62inoutz12zd2vsetz12zf2livenes1965z42zzliveness_livenessz00(obj_t
		BgL_envz00_4733, obj_t BgL_nz00_4734, obj_t BgL_oz00_4735)
	{
		{	/* Liveness/liveness.scm 551 */
			{
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_9927;

				{
					obj_t BgL_auxz00_9928;

					{	/* Liveness/liveness.scm 553 */
						BgL_objectz00_bglt BgL_tmpz00_9929;

						BgL_tmpz00_9929 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4734));
						BgL_auxz00_9928 = BGL_OBJECT_WIDENING(BgL_tmpz00_9929);
					}
					BgL_auxz00_9927 = ((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_9928);
				}
				((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_9927))->
						BgL_outz00) = ((obj_t) BgL_oz00_4735), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_9942;
				BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_9935;

				{	/* Liveness/liveness.scm 554 */
					obj_t BgL_arg2289z00_5423;
					obj_t BgL_arg2290z00_5424;

					{
						BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_9943;

						{
							obj_t BgL_auxz00_9944;

							{	/* Liveness/liveness.scm 554 */
								BgL_objectz00_bglt BgL_tmpz00_9945;

								BgL_tmpz00_9945 =
									((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4734));
								BgL_auxz00_9944 = BGL_OBJECT_WIDENING(BgL_tmpz00_9945);
							}
							BgL_auxz00_9943 =
								((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_9944);
						}
						BgL_arg2289z00_5423 =
							(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_9943))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 554 */
						obj_t BgL_arg2291z00_5425;
						obj_t BgL_arg2292z00_5426;

						{
							BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_9951;

							{
								obj_t BgL_auxz00_9952;

								{	/* Liveness/liveness.scm 554 */
									BgL_objectz00_bglt BgL_tmpz00_9953;

									BgL_tmpz00_9953 =
										((BgL_objectz00_bglt)
										((BgL_vsetz12z12_bglt) BgL_nz00_4734));
									BgL_auxz00_9952 = BGL_OBJECT_WIDENING(BgL_tmpz00_9953);
								}
								BgL_auxz00_9951 =
									((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_9952);
							}
							BgL_arg2291z00_5425 =
								(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_9951))->
								BgL_outz00);
						}
						{
							BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_9959;

							{
								obj_t BgL_auxz00_9960;

								{	/* Liveness/liveness.scm 554 */
									BgL_objectz00_bglt BgL_tmpz00_9961;

									BgL_tmpz00_9961 =
										((BgL_objectz00_bglt)
										((BgL_vsetz12z12_bglt) BgL_nz00_4734));
									BgL_auxz00_9960 = BGL_OBJECT_WIDENING(BgL_tmpz00_9961);
								}
								BgL_auxz00_9959 =
									((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_9960);
							}
							BgL_arg2292z00_5426 =
								(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_9959))->
								BgL_defz00);
						}
						BgL_arg2290z00_5424 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2291z00_5425,
							BgL_arg2292z00_5426);
					}
					BgL_auxz00_9942 =
						BGl_unionz00zzliveness_setz00(BgL_arg2289z00_5423,
						BgL_arg2290z00_5424);
				}
				{
					obj_t BgL_auxz00_9936;

					{	/* Liveness/liveness.scm 554 */
						BgL_objectz00_bglt BgL_tmpz00_9937;

						BgL_tmpz00_9937 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4734));
						BgL_auxz00_9936 = BGL_OBJECT_WIDENING(BgL_tmpz00_9937);
					}
					BgL_auxz00_9935 = ((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_9936);
				}
				((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_9935))->
						BgL_inz00) = ((obj_t) BgL_auxz00_9942), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 555 */
				obj_t BgL_g1686z00_5427;

				BgL_g1686z00_5427 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_nz00_4734))))->BgL_exprza2za2);
				{
					obj_t BgL_l1684z00_5429;

					BgL_l1684z00_5429 = BgL_g1686z00_5427;
				BgL_zc3z04anonymousza32293ze3z87_5428:
					if (PAIRP(BgL_l1684z00_5429))
						{	/* Liveness/liveness.scm 555 */
							{	/* Liveness/liveness.scm 555 */
								obj_t BgL_ez00_5430;

								BgL_ez00_5430 = CAR(BgL_l1684z00_5429);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5430), BgL_oz00_4735);
							}
							{
								obj_t BgL_l1684z00_9978;

								BgL_l1684z00_9978 = CDR(BgL_l1684z00_5429);
								BgL_l1684z00_5429 = BgL_l1684z00_9978;
								goto BgL_zc3z04anonymousza32293ze3z87_5428;
							}
						}
					else
						{	/* Liveness/liveness.scm 555 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 556 */
				obj_t BgL_val0_1687z00_5431;
				obj_t BgL_val1_1688z00_5432;

				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_9980;

					{
						obj_t BgL_auxz00_9981;

						{	/* Liveness/liveness.scm 556 */
							BgL_objectz00_bglt BgL_tmpz00_9982;

							BgL_tmpz00_9982 =
								((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4734));
							BgL_auxz00_9981 = BGL_OBJECT_WIDENING(BgL_tmpz00_9982);
						}
						BgL_auxz00_9980 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_9981);
					}
					BgL_val0_1687z00_5431 =
						(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_9980))->
						BgL_inz00);
				}
				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_9988;

					{
						obj_t BgL_auxz00_9989;

						{	/* Liveness/liveness.scm 556 */
							BgL_objectz00_bglt BgL_tmpz00_9990;

							BgL_tmpz00_9990 =
								((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4734));
							BgL_auxz00_9989 = BGL_OBJECT_WIDENING(BgL_tmpz00_9990);
						}
						BgL_auxz00_9988 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_9989);
					}
					BgL_val1_1688z00_5432 =
						(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_9988))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 556 */
					int BgL_tmpz00_9996;

					BgL_tmpz00_9996 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9996);
				}
				{	/* Liveness/liveness.scm 556 */
					int BgL_tmpz00_9999;

					BgL_tmpz00_9999 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9999, BgL_val1_1688z00_5432);
				}
				return BgL_val0_1687z00_5431;
			}
		}

	}



/* &defuse-vset!/livenes1963 */
	obj_t BGl_z62defusezd2vsetz12zf2livenes1963z50zzliveness_livenessz00(obj_t
		BgL_envz00_4736, obj_t BgL_nz00_4737)
	{
		{	/* Liveness/liveness.scm 547 */
			{	/* Liveness/liveness.scm 549 */
				obj_t BgL_val0_1682z00_5434;
				obj_t BgL_val1_1683z00_5435;

				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_10002;

					{
						obj_t BgL_auxz00_10003;

						{	/* Liveness/liveness.scm 549 */
							BgL_objectz00_bglt BgL_tmpz00_10004;

							BgL_tmpz00_10004 =
								((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4737));
							BgL_auxz00_10003 = BGL_OBJECT_WIDENING(BgL_tmpz00_10004);
						}
						BgL_auxz00_10002 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_10003);
					}
					BgL_val0_1682z00_5434 =
						(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_10002))->
						BgL_defz00);
				}
				{
					BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_10010;

					{
						obj_t BgL_auxz00_10011;

						{	/* Liveness/liveness.scm 549 */
							BgL_objectz00_bglt BgL_tmpz00_10012;

							BgL_tmpz00_10012 =
								((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4737));
							BgL_auxz00_10011 = BGL_OBJECT_WIDENING(BgL_tmpz00_10012);
						}
						BgL_auxz00_10010 =
							((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_10011);
					}
					BgL_val1_1683z00_5435 =
						(((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_10010))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 549 */
					int BgL_tmpz00_10018;

					BgL_tmpz00_10018 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10018);
				}
				{	/* Liveness/liveness.scm 549 */
					int BgL_tmpz00_10021;

					BgL_tmpz00_10021 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10021, BgL_val1_1683z00_5435);
				}
				return BgL_val0_1682z00_5434;
			}
		}

	}



/* &defuse-vset!1961 */
	obj_t BGl_z62defusezd2vsetz121961za2zzliveness_livenessz00(obj_t
		BgL_envz00_4738, obj_t BgL_nz00_4739)
	{
		{	/* Liveness/liveness.scm 538 */
			{	/* Liveness/liveness.scm 540 */
				obj_t BgL_defz00_5437;

				{	/* Liveness/liveness.scm 541 */
					obj_t BgL_arg2288z00_5438;

					BgL_arg2288z00_5438 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vsetz12z12_bglt) BgL_nz00_4739))))->BgL_exprza2za2);
					BgL_defz00_5437 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2288z00_5438, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 541 */
					obj_t BgL_usez00_5439;

					{	/* Liveness/liveness.scm 542 */
						obj_t BgL_tmpz00_5440;

						{	/* Liveness/liveness.scm 542 */
							int BgL_tmpz00_10028;

							BgL_tmpz00_10028 = (int) (1L);
							BgL_tmpz00_5440 = BGL_MVALUES_VAL(BgL_tmpz00_10028);
						}
						{	/* Liveness/liveness.scm 542 */
							int BgL_tmpz00_10031;

							BgL_tmpz00_10031 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_10031, BUNSPEC);
						}
						BgL_usez00_5439 = BgL_tmpz00_5440;
					}
					{	/* Liveness/liveness.scm 543 */
						BgL_vsetz12z12_bglt BgL_arg2286z00_5441;

						{	/* Liveness/liveness.scm 543 */
							BgL_vsetz12zf2livenessze0_bglt BgL_wide1275z00_5442;

							BgL_wide1275z00_5442 =
								((BgL_vsetz12zf2livenessze0_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vsetz12zf2livenessze0_bgl))));
							{	/* Liveness/liveness.scm 543 */
								obj_t BgL_auxz00_10039;
								BgL_objectz00_bglt BgL_tmpz00_10035;

								BgL_auxz00_10039 = ((obj_t) BgL_wide1275z00_5442);
								BgL_tmpz00_10035 =
									((BgL_objectz00_bglt)
									((BgL_vsetz12z12_bglt)
										((BgL_vsetz12z12_bglt) BgL_nz00_4739)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10035, BgL_auxz00_10039);
							}
							((BgL_objectz00_bglt)
								((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4739)));
							{	/* Liveness/liveness.scm 543 */
								long BgL_arg2287z00_5443;

								BgL_arg2287z00_5443 =
									BGL_CLASS_NUM(BGl_vsetz12zf2livenessze0zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_vsetz12z12_bglt)
											((BgL_vsetz12z12_bglt) BgL_nz00_4739))),
									BgL_arg2287z00_5443);
							}
							((BgL_vsetz12z12_bglt)
								((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4739)));
						}
						{
							BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_10053;

							{
								obj_t BgL_auxz00_10054;

								{	/* Liveness/liveness.scm 544 */
									BgL_objectz00_bglt BgL_tmpz00_10055;

									BgL_tmpz00_10055 =
										((BgL_objectz00_bglt)
										((BgL_vsetz12z12_bglt)
											((BgL_vsetz12z12_bglt) BgL_nz00_4739)));
									BgL_auxz00_10054 = BGL_OBJECT_WIDENING(BgL_tmpz00_10055);
								}
								BgL_auxz00_10053 =
									((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_10054);
							}
							((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_10053))->
									BgL_defz00) = ((obj_t) BgL_defz00_5437), BUNSPEC);
						}
						{
							BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_10062;

							{
								obj_t BgL_auxz00_10063;

								{	/* Liveness/liveness.scm 545 */
									BgL_objectz00_bglt BgL_tmpz00_10064;

									BgL_tmpz00_10064 =
										((BgL_objectz00_bglt)
										((BgL_vsetz12z12_bglt)
											((BgL_vsetz12z12_bglt) BgL_nz00_4739)));
									BgL_auxz00_10063 = BGL_OBJECT_WIDENING(BgL_tmpz00_10064);
								}
								BgL_auxz00_10062 =
									((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_10063);
							}
							((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_10062))->
									BgL_usez00) = ((obj_t) BgL_usez00_5439), BUNSPEC);
						}
						{
							BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_10071;

							{
								obj_t BgL_auxz00_10072;

								{	/* Liveness/liveness.scm 541 */
									BgL_objectz00_bglt BgL_tmpz00_10073;

									BgL_tmpz00_10073 =
										((BgL_objectz00_bglt)
										((BgL_vsetz12z12_bglt)
											((BgL_vsetz12z12_bglt) BgL_nz00_4739)));
									BgL_auxz00_10072 = BGL_OBJECT_WIDENING(BgL_tmpz00_10073);
								}
								BgL_auxz00_10071 =
									((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_10072);
							}
							((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_10071))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_vsetz12zf2livenessze0_bglt BgL_auxz00_10080;

							{
								obj_t BgL_auxz00_10081;

								{	/* Liveness/liveness.scm 541 */
									BgL_objectz00_bglt BgL_tmpz00_10082;

									BgL_tmpz00_10082 =
										((BgL_objectz00_bglt)
										((BgL_vsetz12z12_bglt)
											((BgL_vsetz12z12_bglt) BgL_nz00_4739)));
									BgL_auxz00_10081 = BGL_OBJECT_WIDENING(BgL_tmpz00_10082);
								}
								BgL_auxz00_10080 =
									((BgL_vsetz12zf2livenessze0_bglt) BgL_auxz00_10081);
							}
							((((BgL_vsetz12zf2livenessze0_bglt) COBJECT(BgL_auxz00_10080))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2286z00_5441 =
							((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_nz00_4739));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2286z00_5441));
					}
				}
			}
		}

	}



/* &inout-vref/liveness1959 */
	obj_t BGl_z62inoutzd2vrefzf2liveness1959z42zzliveness_livenessz00(obj_t
		BgL_envz00_4740, obj_t BgL_nz00_4741)
	{
		{	/* Liveness/liveness.scm 531 */
			{	/* Liveness/liveness.scm 533 */
				obj_t BgL_val0_1680z00_5445;
				obj_t BgL_val1_1681z00_5446;

				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10093;

					{
						obj_t BgL_auxz00_10094;

						{	/* Liveness/liveness.scm 533 */
							BgL_objectz00_bglt BgL_tmpz00_10095;

							BgL_tmpz00_10095 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4741));
							BgL_auxz00_10094 = BGL_OBJECT_WIDENING(BgL_tmpz00_10095);
						}
						BgL_auxz00_10093 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10094);
					}
					BgL_val0_1680z00_5445 =
						(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10093))->
						BgL_inz00);
				}
				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10101;

					{
						obj_t BgL_auxz00_10102;

						{	/* Liveness/liveness.scm 533 */
							BgL_objectz00_bglt BgL_tmpz00_10103;

							BgL_tmpz00_10103 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4741));
							BgL_auxz00_10102 = BGL_OBJECT_WIDENING(BgL_tmpz00_10103);
						}
						BgL_auxz00_10101 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10102);
					}
					BgL_val1_1681z00_5446 =
						(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10101))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 533 */
					int BgL_tmpz00_10109;

					BgL_tmpz00_10109 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10109);
				}
				{	/* Liveness/liveness.scm 533 */
					int BgL_tmpz00_10112;

					BgL_tmpz00_10112 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10112, BgL_val1_1681z00_5446);
				}
				return BgL_val0_1680z00_5445;
			}
		}

	}



/* &inout!-vref/liveness1957 */
	obj_t BGl_z62inoutz12zd2vrefzf2liveness1957z50zzliveness_livenessz00(obj_t
		BgL_envz00_4742, obj_t BgL_nz00_4743, obj_t BgL_oz00_4744)
	{
		{	/* Liveness/liveness.scm 524 */
			{
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10115;

				{
					obj_t BgL_auxz00_10116;

					{	/* Liveness/liveness.scm 526 */
						BgL_objectz00_bglt BgL_tmpz00_10117;

						BgL_tmpz00_10117 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4743));
						BgL_auxz00_10116 = BGL_OBJECT_WIDENING(BgL_tmpz00_10117);
					}
					BgL_auxz00_10115 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10116);
				}
				((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10115))->
						BgL_outz00) = ((obj_t) BgL_oz00_4744), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_10130;
				BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10123;

				{	/* Liveness/liveness.scm 527 */
					obj_t BgL_arg2277z00_5448;
					obj_t BgL_arg2279z00_5449;

					{
						BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10131;

						{
							obj_t BgL_auxz00_10132;

							{	/* Liveness/liveness.scm 527 */
								BgL_objectz00_bglt BgL_tmpz00_10133;

								BgL_tmpz00_10133 =
									((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4743));
								BgL_auxz00_10132 = BGL_OBJECT_WIDENING(BgL_tmpz00_10133);
							}
							BgL_auxz00_10131 =
								((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10132);
						}
						BgL_arg2277z00_5448 =
							(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10131))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 527 */
						obj_t BgL_arg2280z00_5450;
						obj_t BgL_arg2281z00_5451;

						{
							BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10139;

							{
								obj_t BgL_auxz00_10140;

								{	/* Liveness/liveness.scm 527 */
									BgL_objectz00_bglt BgL_tmpz00_10141;

									BgL_tmpz00_10141 =
										((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4743));
									BgL_auxz00_10140 = BGL_OBJECT_WIDENING(BgL_tmpz00_10141);
								}
								BgL_auxz00_10139 =
									((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10140);
							}
							BgL_arg2280z00_5450 =
								(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10139))->
								BgL_outz00);
						}
						{
							BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10147;

							{
								obj_t BgL_auxz00_10148;

								{	/* Liveness/liveness.scm 527 */
									BgL_objectz00_bglt BgL_tmpz00_10149;

									BgL_tmpz00_10149 =
										((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4743));
									BgL_auxz00_10148 = BGL_OBJECT_WIDENING(BgL_tmpz00_10149);
								}
								BgL_auxz00_10147 =
									((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10148);
							}
							BgL_arg2281z00_5451 =
								(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10147))->
								BgL_defz00);
						}
						BgL_arg2279z00_5449 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2280z00_5450,
							BgL_arg2281z00_5451);
					}
					BgL_auxz00_10130 =
						BGl_unionz00zzliveness_setz00(BgL_arg2277z00_5448,
						BgL_arg2279z00_5449);
				}
				{
					obj_t BgL_auxz00_10124;

					{	/* Liveness/liveness.scm 527 */
						BgL_objectz00_bglt BgL_tmpz00_10125;

						BgL_tmpz00_10125 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4743));
						BgL_auxz00_10124 = BGL_OBJECT_WIDENING(BgL_tmpz00_10125);
					}
					BgL_auxz00_10123 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10124);
				}
				((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10123))->
						BgL_inz00) = ((obj_t) BgL_auxz00_10130), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 528 */
				obj_t BgL_g1677z00_5452;

				BgL_g1677z00_5452 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vrefz00_bglt) BgL_nz00_4743))))->BgL_exprza2za2);
				{
					obj_t BgL_l1675z00_5454;

					BgL_l1675z00_5454 = BgL_g1677z00_5452;
				BgL_zc3z04anonymousza32282ze3z87_5453:
					if (PAIRP(BgL_l1675z00_5454))
						{	/* Liveness/liveness.scm 528 */
							{	/* Liveness/liveness.scm 528 */
								obj_t BgL_ez00_5455;

								BgL_ez00_5455 = CAR(BgL_l1675z00_5454);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5455), BgL_oz00_4744);
							}
							{
								obj_t BgL_l1675z00_10166;

								BgL_l1675z00_10166 = CDR(BgL_l1675z00_5454);
								BgL_l1675z00_5454 = BgL_l1675z00_10166;
								goto BgL_zc3z04anonymousza32282ze3z87_5453;
							}
						}
					else
						{	/* Liveness/liveness.scm 528 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 529 */
				obj_t BgL_val0_1678z00_5456;
				obj_t BgL_val1_1679z00_5457;

				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10168;

					{
						obj_t BgL_auxz00_10169;

						{	/* Liveness/liveness.scm 529 */
							BgL_objectz00_bglt BgL_tmpz00_10170;

							BgL_tmpz00_10170 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4743));
							BgL_auxz00_10169 = BGL_OBJECT_WIDENING(BgL_tmpz00_10170);
						}
						BgL_auxz00_10168 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10169);
					}
					BgL_val0_1678z00_5456 =
						(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10168))->
						BgL_inz00);
				}
				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10176;

					{
						obj_t BgL_auxz00_10177;

						{	/* Liveness/liveness.scm 529 */
							BgL_objectz00_bglt BgL_tmpz00_10178;

							BgL_tmpz00_10178 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4743));
							BgL_auxz00_10177 = BGL_OBJECT_WIDENING(BgL_tmpz00_10178);
						}
						BgL_auxz00_10176 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10177);
					}
					BgL_val1_1679z00_5457 =
						(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10176))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 529 */
					int BgL_tmpz00_10184;

					BgL_tmpz00_10184 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10184);
				}
				{	/* Liveness/liveness.scm 529 */
					int BgL_tmpz00_10187;

					BgL_tmpz00_10187 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10187, BgL_val1_1679z00_5457);
				}
				return BgL_val0_1678z00_5456;
			}
		}

	}



/* &defuse-vref/liveness1955 */
	obj_t BGl_z62defusezd2vrefzf2liveness1955z42zzliveness_livenessz00(obj_t
		BgL_envz00_4745, obj_t BgL_nz00_4746)
	{
		{	/* Liveness/liveness.scm 520 */
			{	/* Liveness/liveness.scm 522 */
				obj_t BgL_val0_1673z00_5459;
				obj_t BgL_val1_1674z00_5460;

				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10190;

					{
						obj_t BgL_auxz00_10191;

						{	/* Liveness/liveness.scm 522 */
							BgL_objectz00_bglt BgL_tmpz00_10192;

							BgL_tmpz00_10192 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4746));
							BgL_auxz00_10191 = BGL_OBJECT_WIDENING(BgL_tmpz00_10192);
						}
						BgL_auxz00_10190 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10191);
					}
					BgL_val0_1673z00_5459 =
						(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10190))->
						BgL_defz00);
				}
				{
					BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10198;

					{
						obj_t BgL_auxz00_10199;

						{	/* Liveness/liveness.scm 522 */
							BgL_objectz00_bglt BgL_tmpz00_10200;

							BgL_tmpz00_10200 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4746));
							BgL_auxz00_10199 = BGL_OBJECT_WIDENING(BgL_tmpz00_10200);
						}
						BgL_auxz00_10198 = ((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10199);
					}
					BgL_val1_1674z00_5460 =
						(((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10198))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 522 */
					int BgL_tmpz00_10206;

					BgL_tmpz00_10206 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10206);
				}
				{	/* Liveness/liveness.scm 522 */
					int BgL_tmpz00_10209;

					BgL_tmpz00_10209 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10209, BgL_val1_1674z00_5460);
				}
				return BgL_val0_1673z00_5459;
			}
		}

	}



/* &defuse-vref1953 */
	obj_t BGl_z62defusezd2vref1953zb0zzliveness_livenessz00(obj_t BgL_envz00_4747,
		obj_t BgL_nz00_4748)
	{
		{	/* Liveness/liveness.scm 511 */
			{	/* Liveness/liveness.scm 513 */
				obj_t BgL_defz00_5462;

				{	/* Liveness/liveness.scm 514 */
					obj_t BgL_arg2276z00_5463;

					BgL_arg2276z00_5463 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vrefz00_bglt) BgL_nz00_4748))))->BgL_exprza2za2);
					BgL_defz00_5462 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2276z00_5463, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 514 */
					obj_t BgL_usez00_5464;

					{	/* Liveness/liveness.scm 515 */
						obj_t BgL_tmpz00_5465;

						{	/* Liveness/liveness.scm 515 */
							int BgL_tmpz00_10216;

							BgL_tmpz00_10216 = (int) (1L);
							BgL_tmpz00_5465 = BGL_MVALUES_VAL(BgL_tmpz00_10216);
						}
						{	/* Liveness/liveness.scm 515 */
							int BgL_tmpz00_10219;

							BgL_tmpz00_10219 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_10219, BUNSPEC);
						}
						BgL_usez00_5464 = BgL_tmpz00_5465;
					}
					{	/* Liveness/liveness.scm 516 */
						BgL_vrefz00_bglt BgL_arg2274z00_5466;

						{	/* Liveness/liveness.scm 516 */
							BgL_vrefzf2livenesszf2_bglt BgL_wide1267z00_5467;

							BgL_wide1267z00_5467 =
								((BgL_vrefzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vrefzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 516 */
								obj_t BgL_auxz00_10227;
								BgL_objectz00_bglt BgL_tmpz00_10223;

								BgL_auxz00_10227 = ((obj_t) BgL_wide1267z00_5467);
								BgL_tmpz00_10223 =
									((BgL_objectz00_bglt)
									((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4748)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10223, BgL_auxz00_10227);
							}
							((BgL_objectz00_bglt)
								((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4748)));
							{	/* Liveness/liveness.scm 516 */
								long BgL_arg2275z00_5468;

								BgL_arg2275z00_5468 =
									BGL_CLASS_NUM(BGl_vrefzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt)
											((BgL_vrefz00_bglt) BgL_nz00_4748))),
									BgL_arg2275z00_5468);
							}
							((BgL_vrefz00_bglt)
								((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4748)));
						}
						{
							BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10241;

							{
								obj_t BgL_auxz00_10242;

								{	/* Liveness/liveness.scm 517 */
									BgL_objectz00_bglt BgL_tmpz00_10243;

									BgL_tmpz00_10243 =
										((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4748)));
									BgL_auxz00_10242 = BGL_OBJECT_WIDENING(BgL_tmpz00_10243);
								}
								BgL_auxz00_10241 =
									((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10242);
							}
							((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10241))->
									BgL_defz00) = ((obj_t) BgL_defz00_5462), BUNSPEC);
						}
						{
							BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10250;

							{
								obj_t BgL_auxz00_10251;

								{	/* Liveness/liveness.scm 518 */
									BgL_objectz00_bglt BgL_tmpz00_10252;

									BgL_tmpz00_10252 =
										((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4748)));
									BgL_auxz00_10251 = BGL_OBJECT_WIDENING(BgL_tmpz00_10252);
								}
								BgL_auxz00_10250 =
									((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10251);
							}
							((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10250))->
									BgL_usez00) = ((obj_t) BgL_usez00_5464), BUNSPEC);
						}
						{
							BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10259;

							{
								obj_t BgL_auxz00_10260;

								{	/* Liveness/liveness.scm 514 */
									BgL_objectz00_bglt BgL_tmpz00_10261;

									BgL_tmpz00_10261 =
										((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4748)));
									BgL_auxz00_10260 = BGL_OBJECT_WIDENING(BgL_tmpz00_10261);
								}
								BgL_auxz00_10259 =
									((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10260);
							}
							((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10259))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_vrefzf2livenesszf2_bglt BgL_auxz00_10268;

							{
								obj_t BgL_auxz00_10269;

								{	/* Liveness/liveness.scm 514 */
									BgL_objectz00_bglt BgL_tmpz00_10270;

									BgL_tmpz00_10270 =
										((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4748)));
									BgL_auxz00_10269 = BGL_OBJECT_WIDENING(BgL_tmpz00_10270);
								}
								BgL_auxz00_10268 =
									((BgL_vrefzf2livenesszf2_bglt) BgL_auxz00_10269);
							}
							((((BgL_vrefzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10268))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2274z00_5466 =
							((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nz00_4748));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2274z00_5466));
					}
				}
			}
		}

	}



/* &inout-valloc/livenes1951 */
	obj_t BGl_z62inoutzd2valloczf2livenes1951z42zzliveness_livenessz00(obj_t
		BgL_envz00_4749, obj_t BgL_nz00_4750)
	{
		{	/* Liveness/liveness.scm 504 */
			{	/* Liveness/liveness.scm 506 */
				obj_t BgL_val0_1671z00_5470;
				obj_t BgL_val1_1672z00_5471;

				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_10281;

					{
						obj_t BgL_auxz00_10282;

						{	/* Liveness/liveness.scm 506 */
							BgL_objectz00_bglt BgL_tmpz00_10283;

							BgL_tmpz00_10283 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4750));
							BgL_auxz00_10282 = BGL_OBJECT_WIDENING(BgL_tmpz00_10283);
						}
						BgL_auxz00_10281 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10282);
					}
					BgL_val0_1671z00_5470 =
						(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10281))->
						BgL_inz00);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_10289;

					{
						obj_t BgL_auxz00_10290;

						{	/* Liveness/liveness.scm 506 */
							BgL_objectz00_bglt BgL_tmpz00_10291;

							BgL_tmpz00_10291 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4750));
							BgL_auxz00_10290 = BGL_OBJECT_WIDENING(BgL_tmpz00_10291);
						}
						BgL_auxz00_10289 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10290);
					}
					BgL_val1_1672z00_5471 =
						(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10289))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 506 */
					int BgL_tmpz00_10297;

					BgL_tmpz00_10297 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10297);
				}
				{	/* Liveness/liveness.scm 506 */
					int BgL_tmpz00_10300;

					BgL_tmpz00_10300 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10300, BgL_val1_1672z00_5471);
				}
				return BgL_val0_1671z00_5470;
			}
		}

	}



/* &inout!-valloc/livene1949 */
	obj_t BGl_z62inoutz12zd2valloczf2livene1949z50zzliveness_livenessz00(obj_t
		BgL_envz00_4751, obj_t BgL_nz00_4752, obj_t BgL_oz00_4753)
	{
		{	/* Liveness/liveness.scm 497 */
			{
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_10303;

				{
					obj_t BgL_auxz00_10304;

					{	/* Liveness/liveness.scm 499 */
						BgL_objectz00_bglt BgL_tmpz00_10305;

						BgL_tmpz00_10305 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4752));
						BgL_auxz00_10304 = BGL_OBJECT_WIDENING(BgL_tmpz00_10305);
					}
					BgL_auxz00_10303 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10304);
				}
				((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10303))->
						BgL_outz00) = ((obj_t) BgL_oz00_4753), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_10318;
				BgL_valloczf2livenesszf2_bglt BgL_auxz00_10311;

				{	/* Liveness/liveness.scm 500 */
					obj_t BgL_arg2267z00_5473;
					obj_t BgL_arg2268z00_5474;

					{
						BgL_valloczf2livenesszf2_bglt BgL_auxz00_10319;

						{
							obj_t BgL_auxz00_10320;

							{	/* Liveness/liveness.scm 500 */
								BgL_objectz00_bglt BgL_tmpz00_10321;

								BgL_tmpz00_10321 =
									((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4752));
								BgL_auxz00_10320 = BGL_OBJECT_WIDENING(BgL_tmpz00_10321);
							}
							BgL_auxz00_10319 =
								((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10320);
						}
						BgL_arg2267z00_5473 =
							(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10319))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 500 */
						obj_t BgL_arg2269z00_5475;
						obj_t BgL_arg2270z00_5476;

						{
							BgL_valloczf2livenesszf2_bglt BgL_auxz00_10327;

							{
								obj_t BgL_auxz00_10328;

								{	/* Liveness/liveness.scm 500 */
									BgL_objectz00_bglt BgL_tmpz00_10329;

									BgL_tmpz00_10329 =
										((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4752));
									BgL_auxz00_10328 = BGL_OBJECT_WIDENING(BgL_tmpz00_10329);
								}
								BgL_auxz00_10327 =
									((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10328);
							}
							BgL_arg2269z00_5475 =
								(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10327))->
								BgL_outz00);
						}
						{
							BgL_valloczf2livenesszf2_bglt BgL_auxz00_10335;

							{
								obj_t BgL_auxz00_10336;

								{	/* Liveness/liveness.scm 500 */
									BgL_objectz00_bglt BgL_tmpz00_10337;

									BgL_tmpz00_10337 =
										((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4752));
									BgL_auxz00_10336 = BGL_OBJECT_WIDENING(BgL_tmpz00_10337);
								}
								BgL_auxz00_10335 =
									((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10336);
							}
							BgL_arg2270z00_5476 =
								(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10335))->
								BgL_defz00);
						}
						BgL_arg2268z00_5474 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2269z00_5475,
							BgL_arg2270z00_5476);
					}
					BgL_auxz00_10318 =
						BGl_unionz00zzliveness_setz00(BgL_arg2267z00_5473,
						BgL_arg2268z00_5474);
				}
				{
					obj_t BgL_auxz00_10312;

					{	/* Liveness/liveness.scm 500 */
						BgL_objectz00_bglt BgL_tmpz00_10313;

						BgL_tmpz00_10313 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4752));
						BgL_auxz00_10312 = BGL_OBJECT_WIDENING(BgL_tmpz00_10313);
					}
					BgL_auxz00_10311 = ((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10312);
				}
				((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10311))->
						BgL_inz00) = ((obj_t) BgL_auxz00_10318), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 501 */
				obj_t BgL_g1668z00_5477;

				BgL_g1668z00_5477 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vallocz00_bglt) BgL_nz00_4752))))->BgL_exprza2za2);
				{
					obj_t BgL_l1666z00_5479;

					BgL_l1666z00_5479 = BgL_g1668z00_5477;
				BgL_zc3z04anonymousza32271ze3z87_5478:
					if (PAIRP(BgL_l1666z00_5479))
						{	/* Liveness/liveness.scm 501 */
							{	/* Liveness/liveness.scm 501 */
								obj_t BgL_ez00_5480;

								BgL_ez00_5480 = CAR(BgL_l1666z00_5479);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5480), BgL_oz00_4753);
							}
							{
								obj_t BgL_l1666z00_10354;

								BgL_l1666z00_10354 = CDR(BgL_l1666z00_5479);
								BgL_l1666z00_5479 = BgL_l1666z00_10354;
								goto BgL_zc3z04anonymousza32271ze3z87_5478;
							}
						}
					else
						{	/* Liveness/liveness.scm 501 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 502 */
				obj_t BgL_val0_1669z00_5481;
				obj_t BgL_val1_1670z00_5482;

				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_10356;

					{
						obj_t BgL_auxz00_10357;

						{	/* Liveness/liveness.scm 502 */
							BgL_objectz00_bglt BgL_tmpz00_10358;

							BgL_tmpz00_10358 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4752));
							BgL_auxz00_10357 = BGL_OBJECT_WIDENING(BgL_tmpz00_10358);
						}
						BgL_auxz00_10356 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10357);
					}
					BgL_val0_1669z00_5481 =
						(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10356))->
						BgL_inz00);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_10364;

					{
						obj_t BgL_auxz00_10365;

						{	/* Liveness/liveness.scm 502 */
							BgL_objectz00_bglt BgL_tmpz00_10366;

							BgL_tmpz00_10366 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4752));
							BgL_auxz00_10365 = BGL_OBJECT_WIDENING(BgL_tmpz00_10366);
						}
						BgL_auxz00_10364 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10365);
					}
					BgL_val1_1670z00_5482 =
						(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10364))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 502 */
					int BgL_tmpz00_10372;

					BgL_tmpz00_10372 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10372);
				}
				{	/* Liveness/liveness.scm 502 */
					int BgL_tmpz00_10375;

					BgL_tmpz00_10375 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10375, BgL_val1_1670z00_5482);
				}
				return BgL_val0_1669z00_5481;
			}
		}

	}



/* &defuse-valloc/livene1947 */
	obj_t BGl_z62defusezd2valloczf2livene1947z42zzliveness_livenessz00(obj_t
		BgL_envz00_4754, obj_t BgL_nz00_4755)
	{
		{	/* Liveness/liveness.scm 493 */
			{	/* Liveness/liveness.scm 495 */
				obj_t BgL_val0_1664z00_5484;
				obj_t BgL_val1_1665z00_5485;

				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_10378;

					{
						obj_t BgL_auxz00_10379;

						{	/* Liveness/liveness.scm 495 */
							BgL_objectz00_bglt BgL_tmpz00_10380;

							BgL_tmpz00_10380 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4755));
							BgL_auxz00_10379 = BGL_OBJECT_WIDENING(BgL_tmpz00_10380);
						}
						BgL_auxz00_10378 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10379);
					}
					BgL_val0_1664z00_5484 =
						(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10378))->
						BgL_defz00);
				}
				{
					BgL_valloczf2livenesszf2_bglt BgL_auxz00_10386;

					{
						obj_t BgL_auxz00_10387;

						{	/* Liveness/liveness.scm 495 */
							BgL_objectz00_bglt BgL_tmpz00_10388;

							BgL_tmpz00_10388 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4755));
							BgL_auxz00_10387 = BGL_OBJECT_WIDENING(BgL_tmpz00_10388);
						}
						BgL_auxz00_10386 =
							((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10387);
					}
					BgL_val1_1665z00_5485 =
						(((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10386))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 495 */
					int BgL_tmpz00_10394;

					BgL_tmpz00_10394 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10394);
				}
				{	/* Liveness/liveness.scm 495 */
					int BgL_tmpz00_10397;

					BgL_tmpz00_10397 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10397, BgL_val1_1665z00_5485);
				}
				return BgL_val0_1664z00_5484;
			}
		}

	}



/* &defuse-valloc1945 */
	obj_t BGl_z62defusezd2valloc1945zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4756, obj_t BgL_nz00_4757)
	{
		{	/* Liveness/liveness.scm 484 */
			{	/* Liveness/liveness.scm 486 */
				obj_t BgL_defz00_5487;

				{	/* Liveness/liveness.scm 487 */
					obj_t BgL_arg2266z00_5488;

					BgL_arg2266z00_5488 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vallocz00_bglt) BgL_nz00_4757))))->BgL_exprza2za2);
					BgL_defz00_5487 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2266z00_5488, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 487 */
					obj_t BgL_usez00_5489;

					{	/* Liveness/liveness.scm 488 */
						obj_t BgL_tmpz00_5490;

						{	/* Liveness/liveness.scm 488 */
							int BgL_tmpz00_10404;

							BgL_tmpz00_10404 = (int) (1L);
							BgL_tmpz00_5490 = BGL_MVALUES_VAL(BgL_tmpz00_10404);
						}
						{	/* Liveness/liveness.scm 488 */
							int BgL_tmpz00_10407;

							BgL_tmpz00_10407 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_10407, BUNSPEC);
						}
						BgL_usez00_5489 = BgL_tmpz00_5490;
					}
					{	/* Liveness/liveness.scm 489 */
						BgL_vallocz00_bglt BgL_arg2264z00_5491;

						{	/* Liveness/liveness.scm 489 */
							BgL_valloczf2livenesszf2_bglt BgL_wide1258z00_5492;

							BgL_wide1258z00_5492 =
								((BgL_valloczf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_valloczf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 489 */
								obj_t BgL_auxz00_10415;
								BgL_objectz00_bglt BgL_tmpz00_10411;

								BgL_auxz00_10415 = ((obj_t) BgL_wide1258z00_5492);
								BgL_tmpz00_10411 =
									((BgL_objectz00_bglt)
									((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4757)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10411, BgL_auxz00_10415);
							}
							((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4757)));
							{	/* Liveness/liveness.scm 489 */
								long BgL_arg2265z00_5493;

								BgL_arg2265z00_5493 =
									BGL_CLASS_NUM(BGl_valloczf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt)
											((BgL_vallocz00_bglt) BgL_nz00_4757))),
									BgL_arg2265z00_5493);
							}
							((BgL_vallocz00_bglt)
								((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4757)));
						}
						{
							BgL_valloczf2livenesszf2_bglt BgL_auxz00_10429;

							{
								obj_t BgL_auxz00_10430;

								{	/* Liveness/liveness.scm 490 */
									BgL_objectz00_bglt BgL_tmpz00_10431;

									BgL_tmpz00_10431 =
										((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt)
											((BgL_vallocz00_bglt) BgL_nz00_4757)));
									BgL_auxz00_10430 = BGL_OBJECT_WIDENING(BgL_tmpz00_10431);
								}
								BgL_auxz00_10429 =
									((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10430);
							}
							((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10429))->
									BgL_defz00) = ((obj_t) BgL_defz00_5487), BUNSPEC);
						}
						{
							BgL_valloczf2livenesszf2_bglt BgL_auxz00_10438;

							{
								obj_t BgL_auxz00_10439;

								{	/* Liveness/liveness.scm 491 */
									BgL_objectz00_bglt BgL_tmpz00_10440;

									BgL_tmpz00_10440 =
										((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt)
											((BgL_vallocz00_bglt) BgL_nz00_4757)));
									BgL_auxz00_10439 = BGL_OBJECT_WIDENING(BgL_tmpz00_10440);
								}
								BgL_auxz00_10438 =
									((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10439);
							}
							((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10438))->
									BgL_usez00) = ((obj_t) BgL_usez00_5489), BUNSPEC);
						}
						{
							BgL_valloczf2livenesszf2_bglt BgL_auxz00_10447;

							{
								obj_t BgL_auxz00_10448;

								{	/* Liveness/liveness.scm 487 */
									BgL_objectz00_bglt BgL_tmpz00_10449;

									BgL_tmpz00_10449 =
										((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt)
											((BgL_vallocz00_bglt) BgL_nz00_4757)));
									BgL_auxz00_10448 = BGL_OBJECT_WIDENING(BgL_tmpz00_10449);
								}
								BgL_auxz00_10447 =
									((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10448);
							}
							((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10447))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_valloczf2livenesszf2_bglt BgL_auxz00_10456;

							{
								obj_t BgL_auxz00_10457;

								{	/* Liveness/liveness.scm 487 */
									BgL_objectz00_bglt BgL_tmpz00_10458;

									BgL_tmpz00_10458 =
										((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt)
											((BgL_vallocz00_bglt) BgL_nz00_4757)));
									BgL_auxz00_10457 = BGL_OBJECT_WIDENING(BgL_tmpz00_10458);
								}
								BgL_auxz00_10456 =
									((BgL_valloczf2livenesszf2_bglt) BgL_auxz00_10457);
							}
							((((BgL_valloczf2livenesszf2_bglt) COBJECT(BgL_auxz00_10456))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2264z00_5491 =
							((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nz00_4757));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2264z00_5491));
					}
				}
			}
		}

	}



/* &inout-new/liveness1943 */
	obj_t BGl_z62inoutzd2newzf2liveness1943z42zzliveness_livenessz00(obj_t
		BgL_envz00_4758, obj_t BgL_nz00_4759)
	{
		{	/* Liveness/liveness.scm 477 */
			{	/* Liveness/liveness.scm 479 */
				obj_t BgL_val0_1662z00_5495;
				obj_t BgL_val1_1663z00_5496;

				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_10469;

					{
						obj_t BgL_auxz00_10470;

						{	/* Liveness/liveness.scm 479 */
							BgL_objectz00_bglt BgL_tmpz00_10471;

							BgL_tmpz00_10471 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4759));
							BgL_auxz00_10470 = BGL_OBJECT_WIDENING(BgL_tmpz00_10471);
						}
						BgL_auxz00_10469 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10470);
					}
					BgL_val0_1662z00_5495 =
						(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10469))->
						BgL_inz00);
				}
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_10477;

					{
						obj_t BgL_auxz00_10478;

						{	/* Liveness/liveness.scm 479 */
							BgL_objectz00_bglt BgL_tmpz00_10479;

							BgL_tmpz00_10479 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4759));
							BgL_auxz00_10478 = BGL_OBJECT_WIDENING(BgL_tmpz00_10479);
						}
						BgL_auxz00_10477 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10478);
					}
					BgL_val1_1663z00_5496 =
						(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10477))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 479 */
					int BgL_tmpz00_10485;

					BgL_tmpz00_10485 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10485);
				}
				{	/* Liveness/liveness.scm 479 */
					int BgL_tmpz00_10488;

					BgL_tmpz00_10488 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10488, BgL_val1_1663z00_5496);
				}
				return BgL_val0_1662z00_5495;
			}
		}

	}



/* &inout!-new/liveness1941 */
	obj_t BGl_z62inoutz12zd2newzf2liveness1941z50zzliveness_livenessz00(obj_t
		BgL_envz00_4760, obj_t BgL_nz00_4761, obj_t BgL_oz00_4762)
	{
		{	/* Liveness/liveness.scm 470 */
			{
				BgL_newzf2livenesszf2_bglt BgL_auxz00_10491;

				{
					obj_t BgL_auxz00_10492;

					{	/* Liveness/liveness.scm 472 */
						BgL_objectz00_bglt BgL_tmpz00_10493;

						BgL_tmpz00_10493 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4761));
						BgL_auxz00_10492 = BGL_OBJECT_WIDENING(BgL_tmpz00_10493);
					}
					BgL_auxz00_10491 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10492);
				}
				((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10491))->
						BgL_outz00) = ((obj_t) BgL_oz00_4762), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_10506;
				BgL_newzf2livenesszf2_bglt BgL_auxz00_10499;

				{	/* Liveness/liveness.scm 473 */
					obj_t BgL_arg2257z00_5498;
					obj_t BgL_arg2258z00_5499;

					{
						BgL_newzf2livenesszf2_bglt BgL_auxz00_10507;

						{
							obj_t BgL_auxz00_10508;

							{	/* Liveness/liveness.scm 473 */
								BgL_objectz00_bglt BgL_tmpz00_10509;

								BgL_tmpz00_10509 =
									((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4761));
								BgL_auxz00_10508 = BGL_OBJECT_WIDENING(BgL_tmpz00_10509);
							}
							BgL_auxz00_10507 =
								((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10508);
						}
						BgL_arg2257z00_5498 =
							(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10507))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 473 */
						obj_t BgL_arg2259z00_5500;
						obj_t BgL_arg2260z00_5501;

						{
							BgL_newzf2livenesszf2_bglt BgL_auxz00_10515;

							{
								obj_t BgL_auxz00_10516;

								{	/* Liveness/liveness.scm 473 */
									BgL_objectz00_bglt BgL_tmpz00_10517;

									BgL_tmpz00_10517 =
										((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4761));
									BgL_auxz00_10516 = BGL_OBJECT_WIDENING(BgL_tmpz00_10517);
								}
								BgL_auxz00_10515 =
									((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10516);
							}
							BgL_arg2259z00_5500 =
								(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10515))->
								BgL_outz00);
						}
						{
							BgL_newzf2livenesszf2_bglt BgL_auxz00_10523;

							{
								obj_t BgL_auxz00_10524;

								{	/* Liveness/liveness.scm 473 */
									BgL_objectz00_bglt BgL_tmpz00_10525;

									BgL_tmpz00_10525 =
										((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4761));
									BgL_auxz00_10524 = BGL_OBJECT_WIDENING(BgL_tmpz00_10525);
								}
								BgL_auxz00_10523 =
									((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10524);
							}
							BgL_arg2260z00_5501 =
								(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10523))->
								BgL_defz00);
						}
						BgL_arg2258z00_5499 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2259z00_5500,
							BgL_arg2260z00_5501);
					}
					BgL_auxz00_10506 =
						BGl_unionz00zzliveness_setz00(BgL_arg2257z00_5498,
						BgL_arg2258z00_5499);
				}
				{
					obj_t BgL_auxz00_10500;

					{	/* Liveness/liveness.scm 473 */
						BgL_objectz00_bglt BgL_tmpz00_10501;

						BgL_tmpz00_10501 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4761));
						BgL_auxz00_10500 = BGL_OBJECT_WIDENING(BgL_tmpz00_10501);
					}
					BgL_auxz00_10499 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10500);
				}
				((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10499))->BgL_inz00) =
					((obj_t) BgL_auxz00_10506), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 474 */
				obj_t BgL_g1659z00_5502;

				BgL_g1659z00_5502 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_newz00_bglt) BgL_nz00_4761))))->BgL_exprza2za2);
				{
					obj_t BgL_l1657z00_5504;

					BgL_l1657z00_5504 = BgL_g1659z00_5502;
				BgL_zc3z04anonymousza32261ze3z87_5503:
					if (PAIRP(BgL_l1657z00_5504))
						{	/* Liveness/liveness.scm 474 */
							{	/* Liveness/liveness.scm 474 */
								obj_t BgL_ez00_5505;

								BgL_ez00_5505 = CAR(BgL_l1657z00_5504);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5505), BgL_oz00_4762);
							}
							{
								obj_t BgL_l1657z00_10542;

								BgL_l1657z00_10542 = CDR(BgL_l1657z00_5504);
								BgL_l1657z00_5504 = BgL_l1657z00_10542;
								goto BgL_zc3z04anonymousza32261ze3z87_5503;
							}
						}
					else
						{	/* Liveness/liveness.scm 474 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 475 */
				obj_t BgL_val0_1660z00_5506;
				obj_t BgL_val1_1661z00_5507;

				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_10544;

					{
						obj_t BgL_auxz00_10545;

						{	/* Liveness/liveness.scm 475 */
							BgL_objectz00_bglt BgL_tmpz00_10546;

							BgL_tmpz00_10546 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4761));
							BgL_auxz00_10545 = BGL_OBJECT_WIDENING(BgL_tmpz00_10546);
						}
						BgL_auxz00_10544 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10545);
					}
					BgL_val0_1660z00_5506 =
						(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10544))->
						BgL_inz00);
				}
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_10552;

					{
						obj_t BgL_auxz00_10553;

						{	/* Liveness/liveness.scm 475 */
							BgL_objectz00_bglt BgL_tmpz00_10554;

							BgL_tmpz00_10554 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4761));
							BgL_auxz00_10553 = BGL_OBJECT_WIDENING(BgL_tmpz00_10554);
						}
						BgL_auxz00_10552 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10553);
					}
					BgL_val1_1661z00_5507 =
						(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10552))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 475 */
					int BgL_tmpz00_10560;

					BgL_tmpz00_10560 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10560);
				}
				{	/* Liveness/liveness.scm 475 */
					int BgL_tmpz00_10563;

					BgL_tmpz00_10563 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10563, BgL_val1_1661z00_5507);
				}
				return BgL_val0_1660z00_5506;
			}
		}

	}



/* &defuse-new/liveness1939 */
	obj_t BGl_z62defusezd2newzf2liveness1939z42zzliveness_livenessz00(obj_t
		BgL_envz00_4763, obj_t BgL_nz00_4764)
	{
		{	/* Liveness/liveness.scm 466 */
			{	/* Liveness/liveness.scm 468 */
				obj_t BgL_val0_1655z00_5509;
				obj_t BgL_val1_1656z00_5510;

				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_10566;

					{
						obj_t BgL_auxz00_10567;

						{	/* Liveness/liveness.scm 468 */
							BgL_objectz00_bglt BgL_tmpz00_10568;

							BgL_tmpz00_10568 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4764));
							BgL_auxz00_10567 = BGL_OBJECT_WIDENING(BgL_tmpz00_10568);
						}
						BgL_auxz00_10566 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10567);
					}
					BgL_val0_1655z00_5509 =
						(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10566))->
						BgL_defz00);
				}
				{
					BgL_newzf2livenesszf2_bglt BgL_auxz00_10574;

					{
						obj_t BgL_auxz00_10575;

						{	/* Liveness/liveness.scm 468 */
							BgL_objectz00_bglt BgL_tmpz00_10576;

							BgL_tmpz00_10576 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4764));
							BgL_auxz00_10575 = BGL_OBJECT_WIDENING(BgL_tmpz00_10576);
						}
						BgL_auxz00_10574 = ((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10575);
					}
					BgL_val1_1656z00_5510 =
						(((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10574))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 468 */
					int BgL_tmpz00_10582;

					BgL_tmpz00_10582 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10582);
				}
				{	/* Liveness/liveness.scm 468 */
					int BgL_tmpz00_10585;

					BgL_tmpz00_10585 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10585, BgL_val1_1656z00_5510);
				}
				return BgL_val0_1655z00_5509;
			}
		}

	}



/* &defuse-new1937 */
	obj_t BGl_z62defusezd2new1937zb0zzliveness_livenessz00(obj_t BgL_envz00_4765,
		obj_t BgL_nz00_4766)
	{
		{	/* Liveness/liveness.scm 457 */
			{	/* Liveness/liveness.scm 459 */
				obj_t BgL_defz00_5512;

				{	/* Liveness/liveness.scm 460 */
					obj_t BgL_arg2256z00_5513;

					BgL_arg2256z00_5513 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_newz00_bglt) BgL_nz00_4766))))->BgL_exprza2za2);
					BgL_defz00_5512 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2256z00_5513, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 460 */
					obj_t BgL_usez00_5514;

					{	/* Liveness/liveness.scm 461 */
						obj_t BgL_tmpz00_5515;

						{	/* Liveness/liveness.scm 461 */
							int BgL_tmpz00_10592;

							BgL_tmpz00_10592 = (int) (1L);
							BgL_tmpz00_5515 = BGL_MVALUES_VAL(BgL_tmpz00_10592);
						}
						{	/* Liveness/liveness.scm 461 */
							int BgL_tmpz00_10595;

							BgL_tmpz00_10595 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_10595, BUNSPEC);
						}
						BgL_usez00_5514 = BgL_tmpz00_5515;
					}
					{	/* Liveness/liveness.scm 462 */
						BgL_newz00_bglt BgL_arg2254z00_5516;

						{	/* Liveness/liveness.scm 462 */
							BgL_newzf2livenesszf2_bglt BgL_wide1249z00_5517;

							BgL_wide1249z00_5517 =
								((BgL_newzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_newzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 462 */
								obj_t BgL_auxz00_10603;
								BgL_objectz00_bglt BgL_tmpz00_10599;

								BgL_auxz00_10603 = ((obj_t) BgL_wide1249z00_5517);
								BgL_tmpz00_10599 =
									((BgL_objectz00_bglt)
									((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4766)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10599, BgL_auxz00_10603);
							}
							((BgL_objectz00_bglt)
								((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4766)));
							{	/* Liveness/liveness.scm 462 */
								long BgL_arg2255z00_5518;

								BgL_arg2255z00_5518 =
									BGL_CLASS_NUM(BGl_newzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_newz00_bglt)
											((BgL_newz00_bglt) BgL_nz00_4766))), BgL_arg2255z00_5518);
							}
							((BgL_newz00_bglt)
								((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4766)));
						}
						{
							BgL_newzf2livenesszf2_bglt BgL_auxz00_10617;

							{
								obj_t BgL_auxz00_10618;

								{	/* Liveness/liveness.scm 463 */
									BgL_objectz00_bglt BgL_tmpz00_10619;

									BgL_tmpz00_10619 =
										((BgL_objectz00_bglt)
										((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4766)));
									BgL_auxz00_10618 = BGL_OBJECT_WIDENING(BgL_tmpz00_10619);
								}
								BgL_auxz00_10617 =
									((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10618);
							}
							((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10617))->
									BgL_defz00) = ((obj_t) BgL_defz00_5512), BUNSPEC);
						}
						{
							BgL_newzf2livenesszf2_bglt BgL_auxz00_10626;

							{
								obj_t BgL_auxz00_10627;

								{	/* Liveness/liveness.scm 464 */
									BgL_objectz00_bglt BgL_tmpz00_10628;

									BgL_tmpz00_10628 =
										((BgL_objectz00_bglt)
										((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4766)));
									BgL_auxz00_10627 = BGL_OBJECT_WIDENING(BgL_tmpz00_10628);
								}
								BgL_auxz00_10626 =
									((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10627);
							}
							((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10626))->
									BgL_usez00) = ((obj_t) BgL_usez00_5514), BUNSPEC);
						}
						{
							BgL_newzf2livenesszf2_bglt BgL_auxz00_10635;

							{
								obj_t BgL_auxz00_10636;

								{	/* Liveness/liveness.scm 460 */
									BgL_objectz00_bglt BgL_tmpz00_10637;

									BgL_tmpz00_10637 =
										((BgL_objectz00_bglt)
										((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4766)));
									BgL_auxz00_10636 = BGL_OBJECT_WIDENING(BgL_tmpz00_10637);
								}
								BgL_auxz00_10635 =
									((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10636);
							}
							((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10635))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_newzf2livenesszf2_bglt BgL_auxz00_10644;

							{
								obj_t BgL_auxz00_10645;

								{	/* Liveness/liveness.scm 460 */
									BgL_objectz00_bglt BgL_tmpz00_10646;

									BgL_tmpz00_10646 =
										((BgL_objectz00_bglt)
										((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4766)));
									BgL_auxz00_10645 = BGL_OBJECT_WIDENING(BgL_tmpz00_10646);
								}
								BgL_auxz00_10644 =
									((BgL_newzf2livenesszf2_bglt) BgL_auxz00_10645);
							}
							((((BgL_newzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10644))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2254z00_5516 =
							((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nz00_4766));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2254z00_5516));
					}
				}
			}
		}

	}



/* &inout-widening/liven1935 */
	obj_t BGl_z62inoutzd2wideningzf2liven1935z42zzliveness_livenessz00(obj_t
		BgL_envz00_4767, obj_t BgL_nz00_4768)
	{
		{	/* Liveness/liveness.scm 450 */
			{	/* Liveness/liveness.scm 452 */
				obj_t BgL_val0_1653z00_5520;
				obj_t BgL_val1_1654z00_5521;

				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10657;

					{
						obj_t BgL_auxz00_10658;

						{	/* Liveness/liveness.scm 452 */
							BgL_objectz00_bglt BgL_tmpz00_10659;

							BgL_tmpz00_10659 =
								((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4768));
							BgL_auxz00_10658 = BGL_OBJECT_WIDENING(BgL_tmpz00_10659);
						}
						BgL_auxz00_10657 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10658);
					}
					BgL_val0_1653z00_5520 =
						(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10657))->
						BgL_inz00);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10665;

					{
						obj_t BgL_auxz00_10666;

						{	/* Liveness/liveness.scm 452 */
							BgL_objectz00_bglt BgL_tmpz00_10667;

							BgL_tmpz00_10667 =
								((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4768));
							BgL_auxz00_10666 = BGL_OBJECT_WIDENING(BgL_tmpz00_10667);
						}
						BgL_auxz00_10665 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10666);
					}
					BgL_val1_1654z00_5521 =
						(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10665))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 452 */
					int BgL_tmpz00_10673;

					BgL_tmpz00_10673 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10673);
				}
				{	/* Liveness/liveness.scm 452 */
					int BgL_tmpz00_10676;

					BgL_tmpz00_10676 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10676, BgL_val1_1654z00_5521);
				}
				return BgL_val0_1653z00_5520;
			}
		}

	}



/* &inout!-widening/live1933 */
	obj_t BGl_z62inoutz12zd2wideningzf2live1933z50zzliveness_livenessz00(obj_t
		BgL_envz00_4769, obj_t BgL_nz00_4770, obj_t BgL_oz00_4771)
	{
		{	/* Liveness/liveness.scm 443 */
			{
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10679;

				{
					obj_t BgL_auxz00_10680;

					{	/* Liveness/liveness.scm 445 */
						BgL_objectz00_bglt BgL_tmpz00_10681;

						BgL_tmpz00_10681 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4770));
						BgL_auxz00_10680 = BGL_OBJECT_WIDENING(BgL_tmpz00_10681);
					}
					BgL_auxz00_10679 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10680);
				}
				((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10679))->
						BgL_outz00) = ((obj_t) BgL_oz00_4771), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_10694;
				BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10687;

				{	/* Liveness/liveness.scm 446 */
					obj_t BgL_arg2247z00_5523;
					obj_t BgL_arg2248z00_5524;

					{
						BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10695;

						{
							obj_t BgL_auxz00_10696;

							{	/* Liveness/liveness.scm 446 */
								BgL_objectz00_bglt BgL_tmpz00_10697;

								BgL_tmpz00_10697 =
									((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4770));
								BgL_auxz00_10696 = BGL_OBJECT_WIDENING(BgL_tmpz00_10697);
							}
							BgL_auxz00_10695 =
								((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10696);
						}
						BgL_arg2247z00_5523 =
							(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10695))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 446 */
						obj_t BgL_arg2249z00_5525;
						obj_t BgL_arg2250z00_5526;

						{
							BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10703;

							{
								obj_t BgL_auxz00_10704;

								{	/* Liveness/liveness.scm 446 */
									BgL_objectz00_bglt BgL_tmpz00_10705;

									BgL_tmpz00_10705 =
										((BgL_objectz00_bglt)
										((BgL_wideningz00_bglt) BgL_nz00_4770));
									BgL_auxz00_10704 = BGL_OBJECT_WIDENING(BgL_tmpz00_10705);
								}
								BgL_auxz00_10703 =
									((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10704);
							}
							BgL_arg2249z00_5525 =
								(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10703))->
								BgL_outz00);
						}
						{
							BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10711;

							{
								obj_t BgL_auxz00_10712;

								{	/* Liveness/liveness.scm 446 */
									BgL_objectz00_bglt BgL_tmpz00_10713;

									BgL_tmpz00_10713 =
										((BgL_objectz00_bglt)
										((BgL_wideningz00_bglt) BgL_nz00_4770));
									BgL_auxz00_10712 = BGL_OBJECT_WIDENING(BgL_tmpz00_10713);
								}
								BgL_auxz00_10711 =
									((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10712);
							}
							BgL_arg2250z00_5526 =
								(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10711))->
								BgL_defz00);
						}
						BgL_arg2248z00_5524 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2249z00_5525,
							BgL_arg2250z00_5526);
					}
					BgL_auxz00_10694 =
						BGl_unionz00zzliveness_setz00(BgL_arg2247z00_5523,
						BgL_arg2248z00_5524);
				}
				{
					obj_t BgL_auxz00_10688;

					{	/* Liveness/liveness.scm 446 */
						BgL_objectz00_bglt BgL_tmpz00_10689;

						BgL_tmpz00_10689 =
							((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4770));
						BgL_auxz00_10688 = BGL_OBJECT_WIDENING(BgL_tmpz00_10689);
					}
					BgL_auxz00_10687 =
						((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10688);
				}
				((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10687))->
						BgL_inz00) = ((obj_t) BgL_auxz00_10694), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 447 */
				obj_t BgL_g1650z00_5527;

				BgL_g1650z00_5527 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_wideningz00_bglt) BgL_nz00_4770))))->BgL_exprza2za2);
				{
					obj_t BgL_l1648z00_5529;

					BgL_l1648z00_5529 = BgL_g1650z00_5527;
				BgL_zc3z04anonymousza32251ze3z87_5528:
					if (PAIRP(BgL_l1648z00_5529))
						{	/* Liveness/liveness.scm 447 */
							{	/* Liveness/liveness.scm 447 */
								obj_t BgL_ez00_5530;

								BgL_ez00_5530 = CAR(BgL_l1648z00_5529);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5530), BgL_oz00_4771);
							}
							{
								obj_t BgL_l1648z00_10730;

								BgL_l1648z00_10730 = CDR(BgL_l1648z00_5529);
								BgL_l1648z00_5529 = BgL_l1648z00_10730;
								goto BgL_zc3z04anonymousza32251ze3z87_5528;
							}
						}
					else
						{	/* Liveness/liveness.scm 447 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 448 */
				obj_t BgL_val0_1651z00_5531;
				obj_t BgL_val1_1652z00_5532;

				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10732;

					{
						obj_t BgL_auxz00_10733;

						{	/* Liveness/liveness.scm 448 */
							BgL_objectz00_bglt BgL_tmpz00_10734;

							BgL_tmpz00_10734 =
								((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4770));
							BgL_auxz00_10733 = BGL_OBJECT_WIDENING(BgL_tmpz00_10734);
						}
						BgL_auxz00_10732 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10733);
					}
					BgL_val0_1651z00_5531 =
						(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10732))->
						BgL_inz00);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10740;

					{
						obj_t BgL_auxz00_10741;

						{	/* Liveness/liveness.scm 448 */
							BgL_objectz00_bglt BgL_tmpz00_10742;

							BgL_tmpz00_10742 =
								((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4770));
							BgL_auxz00_10741 = BGL_OBJECT_WIDENING(BgL_tmpz00_10742);
						}
						BgL_auxz00_10740 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10741);
					}
					BgL_val1_1652z00_5532 =
						(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10740))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 448 */
					int BgL_tmpz00_10748;

					BgL_tmpz00_10748 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10748);
				}
				{	/* Liveness/liveness.scm 448 */
					int BgL_tmpz00_10751;

					BgL_tmpz00_10751 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10751, BgL_val1_1652z00_5532);
				}
				return BgL_val0_1651z00_5531;
			}
		}

	}



/* &defuse-widening/live1931 */
	obj_t BGl_z62defusezd2wideningzf2live1931z42zzliveness_livenessz00(obj_t
		BgL_envz00_4772, obj_t BgL_nz00_4773)
	{
		{	/* Liveness/liveness.scm 439 */
			{	/* Liveness/liveness.scm 441 */
				obj_t BgL_val0_1646z00_5534;
				obj_t BgL_val1_1647z00_5535;

				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10754;

					{
						obj_t BgL_auxz00_10755;

						{	/* Liveness/liveness.scm 441 */
							BgL_objectz00_bglt BgL_tmpz00_10756;

							BgL_tmpz00_10756 =
								((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4773));
							BgL_auxz00_10755 = BGL_OBJECT_WIDENING(BgL_tmpz00_10756);
						}
						BgL_auxz00_10754 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10755);
					}
					BgL_val0_1646z00_5534 =
						(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10754))->
						BgL_defz00);
				}
				{
					BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10762;

					{
						obj_t BgL_auxz00_10763;

						{	/* Liveness/liveness.scm 441 */
							BgL_objectz00_bglt BgL_tmpz00_10764;

							BgL_tmpz00_10764 =
								((BgL_objectz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4773));
							BgL_auxz00_10763 = BGL_OBJECT_WIDENING(BgL_tmpz00_10764);
						}
						BgL_auxz00_10762 =
							((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10763);
					}
					BgL_val1_1647z00_5535 =
						(((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10762))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 441 */
					int BgL_tmpz00_10770;

					BgL_tmpz00_10770 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10770);
				}
				{	/* Liveness/liveness.scm 441 */
					int BgL_tmpz00_10773;

					BgL_tmpz00_10773 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10773, BgL_val1_1647z00_5535);
				}
				return BgL_val0_1646z00_5534;
			}
		}

	}



/* &defuse-widening1929 */
	obj_t BGl_z62defusezd2widening1929zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4774, obj_t BgL_nz00_4775)
	{
		{	/* Liveness/liveness.scm 430 */
			{	/* Liveness/liveness.scm 432 */
				obj_t BgL_defz00_5537;

				{	/* Liveness/liveness.scm 433 */
					obj_t BgL_arg2246z00_5538;

					BgL_arg2246z00_5538 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_wideningz00_bglt) BgL_nz00_4775))))->BgL_exprza2za2);
					BgL_defz00_5537 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2246z00_5538, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 433 */
					obj_t BgL_usez00_5539;

					{	/* Liveness/liveness.scm 434 */
						obj_t BgL_tmpz00_5540;

						{	/* Liveness/liveness.scm 434 */
							int BgL_tmpz00_10780;

							BgL_tmpz00_10780 = (int) (1L);
							BgL_tmpz00_5540 = BGL_MVALUES_VAL(BgL_tmpz00_10780);
						}
						{	/* Liveness/liveness.scm 434 */
							int BgL_tmpz00_10783;

							BgL_tmpz00_10783 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_10783, BUNSPEC);
						}
						BgL_usez00_5539 = BgL_tmpz00_5540;
					}
					{	/* Liveness/liveness.scm 435 */
						BgL_wideningz00_bglt BgL_arg2244z00_5541;

						{	/* Liveness/liveness.scm 435 */
							BgL_wideningzf2livenesszf2_bglt BgL_wide1241z00_5542;

							BgL_wide1241z00_5542 =
								((BgL_wideningzf2livenesszf2_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_wideningzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 435 */
								obj_t BgL_auxz00_10791;
								BgL_objectz00_bglt BgL_tmpz00_10787;

								BgL_auxz00_10791 = ((obj_t) BgL_wide1241z00_5542);
								BgL_tmpz00_10787 =
									((BgL_objectz00_bglt)
									((BgL_wideningz00_bglt)
										((BgL_wideningz00_bglt) BgL_nz00_4775)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10787, BgL_auxz00_10791);
							}
							((BgL_objectz00_bglt)
								((BgL_wideningz00_bglt)
									((BgL_wideningz00_bglt) BgL_nz00_4775)));
							{	/* Liveness/liveness.scm 435 */
								long BgL_arg2245z00_5543;

								BgL_arg2245z00_5543 =
									BGL_CLASS_NUM(BGl_wideningzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_wideningz00_bglt)
											((BgL_wideningz00_bglt) BgL_nz00_4775))),
									BgL_arg2245z00_5543);
							}
							((BgL_wideningz00_bglt)
								((BgL_wideningz00_bglt)
									((BgL_wideningz00_bglt) BgL_nz00_4775)));
						}
						{
							BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10805;

							{
								obj_t BgL_auxz00_10806;

								{	/* Liveness/liveness.scm 436 */
									BgL_objectz00_bglt BgL_tmpz00_10807;

									BgL_tmpz00_10807 =
										((BgL_objectz00_bglt)
										((BgL_wideningz00_bglt)
											((BgL_wideningz00_bglt) BgL_nz00_4775)));
									BgL_auxz00_10806 = BGL_OBJECT_WIDENING(BgL_tmpz00_10807);
								}
								BgL_auxz00_10805 =
									((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10806);
							}
							((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10805))->
									BgL_defz00) = ((obj_t) BgL_defz00_5537), BUNSPEC);
						}
						{
							BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10814;

							{
								obj_t BgL_auxz00_10815;

								{	/* Liveness/liveness.scm 437 */
									BgL_objectz00_bglt BgL_tmpz00_10816;

									BgL_tmpz00_10816 =
										((BgL_objectz00_bglt)
										((BgL_wideningz00_bglt)
											((BgL_wideningz00_bglt) BgL_nz00_4775)));
									BgL_auxz00_10815 = BGL_OBJECT_WIDENING(BgL_tmpz00_10816);
								}
								BgL_auxz00_10814 =
									((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10815);
							}
							((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10814))->
									BgL_usez00) = ((obj_t) BgL_usez00_5539), BUNSPEC);
						}
						{
							BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10823;

							{
								obj_t BgL_auxz00_10824;

								{	/* Liveness/liveness.scm 433 */
									BgL_objectz00_bglt BgL_tmpz00_10825;

									BgL_tmpz00_10825 =
										((BgL_objectz00_bglt)
										((BgL_wideningz00_bglt)
											((BgL_wideningz00_bglt) BgL_nz00_4775)));
									BgL_auxz00_10824 = BGL_OBJECT_WIDENING(BgL_tmpz00_10825);
								}
								BgL_auxz00_10823 =
									((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10824);
							}
							((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10823))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_wideningzf2livenesszf2_bglt BgL_auxz00_10832;

							{
								obj_t BgL_auxz00_10833;

								{	/* Liveness/liveness.scm 433 */
									BgL_objectz00_bglt BgL_tmpz00_10834;

									BgL_tmpz00_10834 =
										((BgL_objectz00_bglt)
										((BgL_wideningz00_bglt)
											((BgL_wideningz00_bglt) BgL_nz00_4775)));
									BgL_auxz00_10833 = BGL_OBJECT_WIDENING(BgL_tmpz00_10834);
								}
								BgL_auxz00_10832 =
									((BgL_wideningzf2livenesszf2_bglt) BgL_auxz00_10833);
							}
							((((BgL_wideningzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10832))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2244z00_5541 =
							((BgL_wideningz00_bglt) ((BgL_wideningz00_bglt) BgL_nz00_4775));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2244z00_5541));
					}
				}
			}
		}

	}



/* &inout-setfield/liven1927 */
	obj_t BGl_z62inoutzd2setfieldzf2liven1927z42zzliveness_livenessz00(obj_t
		BgL_envz00_4776, obj_t BgL_nz00_4777)
	{
		{	/* Liveness/liveness.scm 423 */
			{	/* Liveness/liveness.scm 425 */
				obj_t BgL_val0_1644z00_5545;
				obj_t BgL_val1_1645z00_5546;

				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10845;

					{
						obj_t BgL_auxz00_10846;

						{	/* Liveness/liveness.scm 425 */
							BgL_objectz00_bglt BgL_tmpz00_10847;

							BgL_tmpz00_10847 =
								((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4777));
							BgL_auxz00_10846 = BGL_OBJECT_WIDENING(BgL_tmpz00_10847);
						}
						BgL_auxz00_10845 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10846);
					}
					BgL_val0_1644z00_5545 =
						(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10845))->
						BgL_inz00);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10853;

					{
						obj_t BgL_auxz00_10854;

						{	/* Liveness/liveness.scm 425 */
							BgL_objectz00_bglt BgL_tmpz00_10855;

							BgL_tmpz00_10855 =
								((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4777));
							BgL_auxz00_10854 = BGL_OBJECT_WIDENING(BgL_tmpz00_10855);
						}
						BgL_auxz00_10853 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10854);
					}
					BgL_val1_1645z00_5546 =
						(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10853))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 425 */
					int BgL_tmpz00_10861;

					BgL_tmpz00_10861 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10861);
				}
				{	/* Liveness/liveness.scm 425 */
					int BgL_tmpz00_10864;

					BgL_tmpz00_10864 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10864, BgL_val1_1645z00_5546);
				}
				return BgL_val0_1644z00_5545;
			}
		}

	}



/* &inout!-setfield/live1925 */
	obj_t BGl_z62inoutz12zd2setfieldzf2live1925z50zzliveness_livenessz00(obj_t
		BgL_envz00_4778, obj_t BgL_nz00_4779, obj_t BgL_oz00_4780)
	{
		{	/* Liveness/liveness.scm 416 */
			{
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10867;

				{
					obj_t BgL_auxz00_10868;

					{	/* Liveness/liveness.scm 418 */
						BgL_objectz00_bglt BgL_tmpz00_10869;

						BgL_tmpz00_10869 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4779));
						BgL_auxz00_10868 = BGL_OBJECT_WIDENING(BgL_tmpz00_10869);
					}
					BgL_auxz00_10867 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10868);
				}
				((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10867))->
						BgL_outz00) = ((obj_t) BgL_oz00_4780), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_10882;
				BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10875;

				{	/* Liveness/liveness.scm 419 */
					obj_t BgL_arg2237z00_5548;
					obj_t BgL_arg2238z00_5549;

					{
						BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10883;

						{
							obj_t BgL_auxz00_10884;

							{	/* Liveness/liveness.scm 419 */
								BgL_objectz00_bglt BgL_tmpz00_10885;

								BgL_tmpz00_10885 =
									((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4779));
								BgL_auxz00_10884 = BGL_OBJECT_WIDENING(BgL_tmpz00_10885);
							}
							BgL_auxz00_10883 =
								((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10884);
						}
						BgL_arg2237z00_5548 =
							(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10883))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 419 */
						obj_t BgL_arg2239z00_5550;
						obj_t BgL_arg2240z00_5551;

						{
							BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10891;

							{
								obj_t BgL_auxz00_10892;

								{	/* Liveness/liveness.scm 419 */
									BgL_objectz00_bglt BgL_tmpz00_10893;

									BgL_tmpz00_10893 =
										((BgL_objectz00_bglt)
										((BgL_setfieldz00_bglt) BgL_nz00_4779));
									BgL_auxz00_10892 = BGL_OBJECT_WIDENING(BgL_tmpz00_10893);
								}
								BgL_auxz00_10891 =
									((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10892);
							}
							BgL_arg2239z00_5550 =
								(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10891))->
								BgL_outz00);
						}
						{
							BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10899;

							{
								obj_t BgL_auxz00_10900;

								{	/* Liveness/liveness.scm 419 */
									BgL_objectz00_bglt BgL_tmpz00_10901;

									BgL_tmpz00_10901 =
										((BgL_objectz00_bglt)
										((BgL_setfieldz00_bglt) BgL_nz00_4779));
									BgL_auxz00_10900 = BGL_OBJECT_WIDENING(BgL_tmpz00_10901);
								}
								BgL_auxz00_10899 =
									((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10900);
							}
							BgL_arg2240z00_5551 =
								(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10899))->
								BgL_defz00);
						}
						BgL_arg2238z00_5549 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2239z00_5550,
							BgL_arg2240z00_5551);
					}
					BgL_auxz00_10882 =
						BGl_unionz00zzliveness_setz00(BgL_arg2237z00_5548,
						BgL_arg2238z00_5549);
				}
				{
					obj_t BgL_auxz00_10876;

					{	/* Liveness/liveness.scm 419 */
						BgL_objectz00_bglt BgL_tmpz00_10877;

						BgL_tmpz00_10877 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4779));
						BgL_auxz00_10876 = BGL_OBJECT_WIDENING(BgL_tmpz00_10877);
					}
					BgL_auxz00_10875 =
						((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10876);
				}
				((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10875))->
						BgL_inz00) = ((obj_t) BgL_auxz00_10882), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 420 */
				obj_t BgL_g1641z00_5552;

				BgL_g1641z00_5552 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_setfieldz00_bglt) BgL_nz00_4779))))->BgL_exprza2za2);
				{
					obj_t BgL_l1639z00_5554;

					BgL_l1639z00_5554 = BgL_g1641z00_5552;
				BgL_zc3z04anonymousza32241ze3z87_5553:
					if (PAIRP(BgL_l1639z00_5554))
						{	/* Liveness/liveness.scm 420 */
							{	/* Liveness/liveness.scm 420 */
								obj_t BgL_ez00_5555;

								BgL_ez00_5555 = CAR(BgL_l1639z00_5554);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5555), BgL_oz00_4780);
							}
							{
								obj_t BgL_l1639z00_10918;

								BgL_l1639z00_10918 = CDR(BgL_l1639z00_5554);
								BgL_l1639z00_5554 = BgL_l1639z00_10918;
								goto BgL_zc3z04anonymousza32241ze3z87_5553;
							}
						}
					else
						{	/* Liveness/liveness.scm 420 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 421 */
				obj_t BgL_val0_1642z00_5556;
				obj_t BgL_val1_1643z00_5557;

				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10920;

					{
						obj_t BgL_auxz00_10921;

						{	/* Liveness/liveness.scm 421 */
							BgL_objectz00_bglt BgL_tmpz00_10922;

							BgL_tmpz00_10922 =
								((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4779));
							BgL_auxz00_10921 = BGL_OBJECT_WIDENING(BgL_tmpz00_10922);
						}
						BgL_auxz00_10920 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10921);
					}
					BgL_val0_1642z00_5556 =
						(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10920))->
						BgL_inz00);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10928;

					{
						obj_t BgL_auxz00_10929;

						{	/* Liveness/liveness.scm 421 */
							BgL_objectz00_bglt BgL_tmpz00_10930;

							BgL_tmpz00_10930 =
								((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4779));
							BgL_auxz00_10929 = BGL_OBJECT_WIDENING(BgL_tmpz00_10930);
						}
						BgL_auxz00_10928 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10929);
					}
					BgL_val1_1643z00_5557 =
						(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10928))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 421 */
					int BgL_tmpz00_10936;

					BgL_tmpz00_10936 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10936);
				}
				{	/* Liveness/liveness.scm 421 */
					int BgL_tmpz00_10939;

					BgL_tmpz00_10939 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10939, BgL_val1_1643z00_5557);
				}
				return BgL_val0_1642z00_5556;
			}
		}

	}



/* &defuse-setfield/live1923 */
	obj_t BGl_z62defusezd2setfieldzf2live1923z42zzliveness_livenessz00(obj_t
		BgL_envz00_4781, obj_t BgL_nz00_4782)
	{
		{	/* Liveness/liveness.scm 412 */
			{	/* Liveness/liveness.scm 414 */
				obj_t BgL_val0_1637z00_5559;
				obj_t BgL_val1_1638z00_5560;

				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10942;

					{
						obj_t BgL_auxz00_10943;

						{	/* Liveness/liveness.scm 414 */
							BgL_objectz00_bglt BgL_tmpz00_10944;

							BgL_tmpz00_10944 =
								((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4782));
							BgL_auxz00_10943 = BGL_OBJECT_WIDENING(BgL_tmpz00_10944);
						}
						BgL_auxz00_10942 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10943);
					}
					BgL_val0_1637z00_5559 =
						(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10942))->
						BgL_defz00);
				}
				{
					BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10950;

					{
						obj_t BgL_auxz00_10951;

						{	/* Liveness/liveness.scm 414 */
							BgL_objectz00_bglt BgL_tmpz00_10952;

							BgL_tmpz00_10952 =
								((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4782));
							BgL_auxz00_10951 = BGL_OBJECT_WIDENING(BgL_tmpz00_10952);
						}
						BgL_auxz00_10950 =
							((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10951);
					}
					BgL_val1_1638z00_5560 =
						(((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10950))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 414 */
					int BgL_tmpz00_10958;

					BgL_tmpz00_10958 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_10958);
				}
				{	/* Liveness/liveness.scm 414 */
					int BgL_tmpz00_10961;

					BgL_tmpz00_10961 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_10961, BgL_val1_1638z00_5560);
				}
				return BgL_val0_1637z00_5559;
			}
		}

	}



/* &defuse-setfield1921 */
	obj_t BGl_z62defusezd2setfield1921zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4783, obj_t BgL_nz00_4784)
	{
		{	/* Liveness/liveness.scm 403 */
			{	/* Liveness/liveness.scm 405 */
				obj_t BgL_defz00_5562;

				{	/* Liveness/liveness.scm 406 */
					obj_t BgL_arg2236z00_5563;

					BgL_arg2236z00_5563 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_setfieldz00_bglt) BgL_nz00_4784))))->BgL_exprza2za2);
					BgL_defz00_5562 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2236z00_5563, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 406 */
					obj_t BgL_usez00_5564;

					{	/* Liveness/liveness.scm 407 */
						obj_t BgL_tmpz00_5565;

						{	/* Liveness/liveness.scm 407 */
							int BgL_tmpz00_10968;

							BgL_tmpz00_10968 = (int) (1L);
							BgL_tmpz00_5565 = BGL_MVALUES_VAL(BgL_tmpz00_10968);
						}
						{	/* Liveness/liveness.scm 407 */
							int BgL_tmpz00_10971;

							BgL_tmpz00_10971 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_10971, BUNSPEC);
						}
						BgL_usez00_5564 = BgL_tmpz00_5565;
					}
					{	/* Liveness/liveness.scm 408 */
						BgL_setfieldz00_bglt BgL_arg2234z00_5566;

						{	/* Liveness/liveness.scm 408 */
							BgL_setfieldzf2livenesszf2_bglt BgL_wide1233z00_5567;

							BgL_wide1233z00_5567 =
								((BgL_setfieldzf2livenesszf2_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_setfieldzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 408 */
								obj_t BgL_auxz00_10979;
								BgL_objectz00_bglt BgL_tmpz00_10975;

								BgL_auxz00_10979 = ((obj_t) BgL_wide1233z00_5567);
								BgL_tmpz00_10975 =
									((BgL_objectz00_bglt)
									((BgL_setfieldz00_bglt)
										((BgL_setfieldz00_bglt) BgL_nz00_4784)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10975, BgL_auxz00_10979);
							}
							((BgL_objectz00_bglt)
								((BgL_setfieldz00_bglt)
									((BgL_setfieldz00_bglt) BgL_nz00_4784)));
							{	/* Liveness/liveness.scm 408 */
								long BgL_arg2235z00_5568;

								BgL_arg2235z00_5568 =
									BGL_CLASS_NUM(BGl_setfieldzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_setfieldz00_bglt)
											((BgL_setfieldz00_bglt) BgL_nz00_4784))),
									BgL_arg2235z00_5568);
							}
							((BgL_setfieldz00_bglt)
								((BgL_setfieldz00_bglt)
									((BgL_setfieldz00_bglt) BgL_nz00_4784)));
						}
						{
							BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_10993;

							{
								obj_t BgL_auxz00_10994;

								{	/* Liveness/liveness.scm 409 */
									BgL_objectz00_bglt BgL_tmpz00_10995;

									BgL_tmpz00_10995 =
										((BgL_objectz00_bglt)
										((BgL_setfieldz00_bglt)
											((BgL_setfieldz00_bglt) BgL_nz00_4784)));
									BgL_auxz00_10994 = BGL_OBJECT_WIDENING(BgL_tmpz00_10995);
								}
								BgL_auxz00_10993 =
									((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_10994);
							}
							((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_10993))->
									BgL_defz00) = ((obj_t) BgL_defz00_5562), BUNSPEC);
						}
						{
							BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_11002;

							{
								obj_t BgL_auxz00_11003;

								{	/* Liveness/liveness.scm 410 */
									BgL_objectz00_bglt BgL_tmpz00_11004;

									BgL_tmpz00_11004 =
										((BgL_objectz00_bglt)
										((BgL_setfieldz00_bglt)
											((BgL_setfieldz00_bglt) BgL_nz00_4784)));
									BgL_auxz00_11003 = BGL_OBJECT_WIDENING(BgL_tmpz00_11004);
								}
								BgL_auxz00_11002 =
									((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_11003);
							}
							((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11002))->
									BgL_usez00) = ((obj_t) BgL_usez00_5564), BUNSPEC);
						}
						{
							BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_11011;

							{
								obj_t BgL_auxz00_11012;

								{	/* Liveness/liveness.scm 406 */
									BgL_objectz00_bglt BgL_tmpz00_11013;

									BgL_tmpz00_11013 =
										((BgL_objectz00_bglt)
										((BgL_setfieldz00_bglt)
											((BgL_setfieldz00_bglt) BgL_nz00_4784)));
									BgL_auxz00_11012 = BGL_OBJECT_WIDENING(BgL_tmpz00_11013);
								}
								BgL_auxz00_11011 =
									((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_11012);
							}
							((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11011))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_setfieldzf2livenesszf2_bglt BgL_auxz00_11020;

							{
								obj_t BgL_auxz00_11021;

								{	/* Liveness/liveness.scm 406 */
									BgL_objectz00_bglt BgL_tmpz00_11022;

									BgL_tmpz00_11022 =
										((BgL_objectz00_bglt)
										((BgL_setfieldz00_bglt)
											((BgL_setfieldz00_bglt) BgL_nz00_4784)));
									BgL_auxz00_11021 = BGL_OBJECT_WIDENING(BgL_tmpz00_11022);
								}
								BgL_auxz00_11020 =
									((BgL_setfieldzf2livenesszf2_bglt) BgL_auxz00_11021);
							}
							((((BgL_setfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11020))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2234z00_5566 =
							((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_nz00_4784));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2234z00_5566));
					}
				}
			}
		}

	}



/* &inout-getfield/liven1919 */
	obj_t BGl_z62inoutzd2getfieldzf2liven1919z42zzliveness_livenessz00(obj_t
		BgL_envz00_4785, obj_t BgL_nz00_4786)
	{
		{	/* Liveness/liveness.scm 396 */
			{	/* Liveness/liveness.scm 398 */
				obj_t BgL_val0_1635z00_5570;
				obj_t BgL_val1_1636z00_5571;

				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11033;

					{
						obj_t BgL_auxz00_11034;

						{	/* Liveness/liveness.scm 398 */
							BgL_objectz00_bglt BgL_tmpz00_11035;

							BgL_tmpz00_11035 =
								((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4786));
							BgL_auxz00_11034 = BGL_OBJECT_WIDENING(BgL_tmpz00_11035);
						}
						BgL_auxz00_11033 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11034);
					}
					BgL_val0_1635z00_5570 =
						(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11033))->
						BgL_inz00);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11041;

					{
						obj_t BgL_auxz00_11042;

						{	/* Liveness/liveness.scm 398 */
							BgL_objectz00_bglt BgL_tmpz00_11043;

							BgL_tmpz00_11043 =
								((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4786));
							BgL_auxz00_11042 = BGL_OBJECT_WIDENING(BgL_tmpz00_11043);
						}
						BgL_auxz00_11041 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11042);
					}
					BgL_val1_1636z00_5571 =
						(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11041))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 398 */
					int BgL_tmpz00_11049;

					BgL_tmpz00_11049 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11049);
				}
				{	/* Liveness/liveness.scm 398 */
					int BgL_tmpz00_11052;

					BgL_tmpz00_11052 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11052, BgL_val1_1636z00_5571);
				}
				return BgL_val0_1635z00_5570;
			}
		}

	}



/* &inout!-getfield/live1917 */
	obj_t BGl_z62inoutz12zd2getfieldzf2live1917z50zzliveness_livenessz00(obj_t
		BgL_envz00_4787, obj_t BgL_nz00_4788, obj_t BgL_oz00_4789)
	{
		{	/* Liveness/liveness.scm 389 */
			{
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11055;

				{
					obj_t BgL_auxz00_11056;

					{	/* Liveness/liveness.scm 391 */
						BgL_objectz00_bglt BgL_tmpz00_11057;

						BgL_tmpz00_11057 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4788));
						BgL_auxz00_11056 = BGL_OBJECT_WIDENING(BgL_tmpz00_11057);
					}
					BgL_auxz00_11055 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11056);
				}
				((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11055))->
						BgL_outz00) = ((obj_t) BgL_oz00_4789), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_11070;
				BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11063;

				{	/* Liveness/liveness.scm 392 */
					obj_t BgL_arg2227z00_5573;
					obj_t BgL_arg2228z00_5574;

					{
						BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11071;

						{
							obj_t BgL_auxz00_11072;

							{	/* Liveness/liveness.scm 392 */
								BgL_objectz00_bglt BgL_tmpz00_11073;

								BgL_tmpz00_11073 =
									((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4788));
								BgL_auxz00_11072 = BGL_OBJECT_WIDENING(BgL_tmpz00_11073);
							}
							BgL_auxz00_11071 =
								((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11072);
						}
						BgL_arg2227z00_5573 =
							(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11071))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 392 */
						obj_t BgL_arg2229z00_5575;
						obj_t BgL_arg2230z00_5576;

						{
							BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11079;

							{
								obj_t BgL_auxz00_11080;

								{	/* Liveness/liveness.scm 392 */
									BgL_objectz00_bglt BgL_tmpz00_11081;

									BgL_tmpz00_11081 =
										((BgL_objectz00_bglt)
										((BgL_getfieldz00_bglt) BgL_nz00_4788));
									BgL_auxz00_11080 = BGL_OBJECT_WIDENING(BgL_tmpz00_11081);
								}
								BgL_auxz00_11079 =
									((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11080);
							}
							BgL_arg2229z00_5575 =
								(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11079))->
								BgL_outz00);
						}
						{
							BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11087;

							{
								obj_t BgL_auxz00_11088;

								{	/* Liveness/liveness.scm 392 */
									BgL_objectz00_bglt BgL_tmpz00_11089;

									BgL_tmpz00_11089 =
										((BgL_objectz00_bglt)
										((BgL_getfieldz00_bglt) BgL_nz00_4788));
									BgL_auxz00_11088 = BGL_OBJECT_WIDENING(BgL_tmpz00_11089);
								}
								BgL_auxz00_11087 =
									((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11088);
							}
							BgL_arg2230z00_5576 =
								(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11087))->
								BgL_defz00);
						}
						BgL_arg2228z00_5574 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2229z00_5575,
							BgL_arg2230z00_5576);
					}
					BgL_auxz00_11070 =
						BGl_unionz00zzliveness_setz00(BgL_arg2227z00_5573,
						BgL_arg2228z00_5574);
				}
				{
					obj_t BgL_auxz00_11064;

					{	/* Liveness/liveness.scm 392 */
						BgL_objectz00_bglt BgL_tmpz00_11065;

						BgL_tmpz00_11065 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4788));
						BgL_auxz00_11064 = BGL_OBJECT_WIDENING(BgL_tmpz00_11065);
					}
					BgL_auxz00_11063 =
						((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11064);
				}
				((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11063))->
						BgL_inz00) = ((obj_t) BgL_auxz00_11070), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 393 */
				obj_t BgL_g1632z00_5577;

				BgL_g1632z00_5577 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_getfieldz00_bglt) BgL_nz00_4788))))->BgL_exprza2za2);
				{
					obj_t BgL_l1630z00_5579;

					BgL_l1630z00_5579 = BgL_g1632z00_5577;
				BgL_zc3z04anonymousza32231ze3z87_5578:
					if (PAIRP(BgL_l1630z00_5579))
						{	/* Liveness/liveness.scm 393 */
							{	/* Liveness/liveness.scm 393 */
								obj_t BgL_ez00_5580;

								BgL_ez00_5580 = CAR(BgL_l1630z00_5579);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5580), BgL_oz00_4789);
							}
							{
								obj_t BgL_l1630z00_11106;

								BgL_l1630z00_11106 = CDR(BgL_l1630z00_5579);
								BgL_l1630z00_5579 = BgL_l1630z00_11106;
								goto BgL_zc3z04anonymousza32231ze3z87_5578;
							}
						}
					else
						{	/* Liveness/liveness.scm 393 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 394 */
				obj_t BgL_val0_1633z00_5581;
				obj_t BgL_val1_1634z00_5582;

				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11108;

					{
						obj_t BgL_auxz00_11109;

						{	/* Liveness/liveness.scm 394 */
							BgL_objectz00_bglt BgL_tmpz00_11110;

							BgL_tmpz00_11110 =
								((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4788));
							BgL_auxz00_11109 = BGL_OBJECT_WIDENING(BgL_tmpz00_11110);
						}
						BgL_auxz00_11108 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11109);
					}
					BgL_val0_1633z00_5581 =
						(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11108))->
						BgL_inz00);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11116;

					{
						obj_t BgL_auxz00_11117;

						{	/* Liveness/liveness.scm 394 */
							BgL_objectz00_bglt BgL_tmpz00_11118;

							BgL_tmpz00_11118 =
								((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4788));
							BgL_auxz00_11117 = BGL_OBJECT_WIDENING(BgL_tmpz00_11118);
						}
						BgL_auxz00_11116 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11117);
					}
					BgL_val1_1634z00_5582 =
						(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11116))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 394 */
					int BgL_tmpz00_11124;

					BgL_tmpz00_11124 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11124);
				}
				{	/* Liveness/liveness.scm 394 */
					int BgL_tmpz00_11127;

					BgL_tmpz00_11127 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11127, BgL_val1_1634z00_5582);
				}
				return BgL_val0_1633z00_5581;
			}
		}

	}



/* &defuse-getfield/live1915 */
	obj_t BGl_z62defusezd2getfieldzf2live1915z42zzliveness_livenessz00(obj_t
		BgL_envz00_4790, obj_t BgL_nz00_4791)
	{
		{	/* Liveness/liveness.scm 385 */
			{	/* Liveness/liveness.scm 387 */
				obj_t BgL_val0_1628z00_5584;
				obj_t BgL_val1_1629z00_5585;

				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11130;

					{
						obj_t BgL_auxz00_11131;

						{	/* Liveness/liveness.scm 387 */
							BgL_objectz00_bglt BgL_tmpz00_11132;

							BgL_tmpz00_11132 =
								((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4791));
							BgL_auxz00_11131 = BGL_OBJECT_WIDENING(BgL_tmpz00_11132);
						}
						BgL_auxz00_11130 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11131);
					}
					BgL_val0_1628z00_5584 =
						(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11130))->
						BgL_defz00);
				}
				{
					BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11138;

					{
						obj_t BgL_auxz00_11139;

						{	/* Liveness/liveness.scm 387 */
							BgL_objectz00_bglt BgL_tmpz00_11140;

							BgL_tmpz00_11140 =
								((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4791));
							BgL_auxz00_11139 = BGL_OBJECT_WIDENING(BgL_tmpz00_11140);
						}
						BgL_auxz00_11138 =
							((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11139);
					}
					BgL_val1_1629z00_5585 =
						(((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11138))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 387 */
					int BgL_tmpz00_11146;

					BgL_tmpz00_11146 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11146);
				}
				{	/* Liveness/liveness.scm 387 */
					int BgL_tmpz00_11149;

					BgL_tmpz00_11149 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11149, BgL_val1_1629z00_5585);
				}
				return BgL_val0_1628z00_5584;
			}
		}

	}



/* &defuse-getfield1913 */
	obj_t BGl_z62defusezd2getfield1913zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4792, obj_t BgL_nz00_4793)
	{
		{	/* Liveness/liveness.scm 376 */
			{	/* Liveness/liveness.scm 378 */
				obj_t BgL_defz00_5587;

				{	/* Liveness/liveness.scm 379 */
					obj_t BgL_arg2226z00_5588;

					BgL_arg2226z00_5588 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_getfieldz00_bglt) BgL_nz00_4793))))->BgL_exprza2za2);
					BgL_defz00_5587 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2226z00_5588, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 379 */
					obj_t BgL_usez00_5589;

					{	/* Liveness/liveness.scm 380 */
						obj_t BgL_tmpz00_5590;

						{	/* Liveness/liveness.scm 380 */
							int BgL_tmpz00_11156;

							BgL_tmpz00_11156 = (int) (1L);
							BgL_tmpz00_5590 = BGL_MVALUES_VAL(BgL_tmpz00_11156);
						}
						{	/* Liveness/liveness.scm 380 */
							int BgL_tmpz00_11159;

							BgL_tmpz00_11159 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11159, BUNSPEC);
						}
						BgL_usez00_5589 = BgL_tmpz00_5590;
					}
					{	/* Liveness/liveness.scm 381 */
						BgL_getfieldz00_bglt BgL_arg2224z00_5591;

						{	/* Liveness/liveness.scm 381 */
							BgL_getfieldzf2livenesszf2_bglt BgL_wide1225z00_5592;

							BgL_wide1225z00_5592 =
								((BgL_getfieldzf2livenesszf2_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_getfieldzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 381 */
								obj_t BgL_auxz00_11167;
								BgL_objectz00_bglt BgL_tmpz00_11163;

								BgL_auxz00_11167 = ((obj_t) BgL_wide1225z00_5592);
								BgL_tmpz00_11163 =
									((BgL_objectz00_bglt)
									((BgL_getfieldz00_bglt)
										((BgL_getfieldz00_bglt) BgL_nz00_4793)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11163, BgL_auxz00_11167);
							}
							((BgL_objectz00_bglt)
								((BgL_getfieldz00_bglt)
									((BgL_getfieldz00_bglt) BgL_nz00_4793)));
							{	/* Liveness/liveness.scm 381 */
								long BgL_arg2225z00_5593;

								BgL_arg2225z00_5593 =
									BGL_CLASS_NUM(BGl_getfieldzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_getfieldz00_bglt)
											((BgL_getfieldz00_bglt) BgL_nz00_4793))),
									BgL_arg2225z00_5593);
							}
							((BgL_getfieldz00_bglt)
								((BgL_getfieldz00_bglt)
									((BgL_getfieldz00_bglt) BgL_nz00_4793)));
						}
						{
							BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11181;

							{
								obj_t BgL_auxz00_11182;

								{	/* Liveness/liveness.scm 382 */
									BgL_objectz00_bglt BgL_tmpz00_11183;

									BgL_tmpz00_11183 =
										((BgL_objectz00_bglt)
										((BgL_getfieldz00_bglt)
											((BgL_getfieldz00_bglt) BgL_nz00_4793)));
									BgL_auxz00_11182 = BGL_OBJECT_WIDENING(BgL_tmpz00_11183);
								}
								BgL_auxz00_11181 =
									((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11182);
							}
							((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11181))->
									BgL_defz00) = ((obj_t) BgL_defz00_5587), BUNSPEC);
						}
						{
							BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11190;

							{
								obj_t BgL_auxz00_11191;

								{	/* Liveness/liveness.scm 383 */
									BgL_objectz00_bglt BgL_tmpz00_11192;

									BgL_tmpz00_11192 =
										((BgL_objectz00_bglt)
										((BgL_getfieldz00_bglt)
											((BgL_getfieldz00_bglt) BgL_nz00_4793)));
									BgL_auxz00_11191 = BGL_OBJECT_WIDENING(BgL_tmpz00_11192);
								}
								BgL_auxz00_11190 =
									((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11191);
							}
							((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11190))->
									BgL_usez00) = ((obj_t) BgL_usez00_5589), BUNSPEC);
						}
						{
							BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11199;

							{
								obj_t BgL_auxz00_11200;

								{	/* Liveness/liveness.scm 379 */
									BgL_objectz00_bglt BgL_tmpz00_11201;

									BgL_tmpz00_11201 =
										((BgL_objectz00_bglt)
										((BgL_getfieldz00_bglt)
											((BgL_getfieldz00_bglt) BgL_nz00_4793)));
									BgL_auxz00_11200 = BGL_OBJECT_WIDENING(BgL_tmpz00_11201);
								}
								BgL_auxz00_11199 =
									((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11200);
							}
							((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11199))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_getfieldzf2livenesszf2_bglt BgL_auxz00_11208;

							{
								obj_t BgL_auxz00_11209;

								{	/* Liveness/liveness.scm 379 */
									BgL_objectz00_bglt BgL_tmpz00_11210;

									BgL_tmpz00_11210 =
										((BgL_objectz00_bglt)
										((BgL_getfieldz00_bglt)
											((BgL_getfieldz00_bglt) BgL_nz00_4793)));
									BgL_auxz00_11209 = BGL_OBJECT_WIDENING(BgL_tmpz00_11210);
								}
								BgL_auxz00_11208 =
									((BgL_getfieldzf2livenesszf2_bglt) BgL_auxz00_11209);
							}
							((((BgL_getfieldzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11208))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2224z00_5591 =
							((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_nz00_4793));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2224z00_5591));
					}
				}
			}
		}

	}



/* &inout-pragma/livenes1911 */
	obj_t BGl_z62inoutzd2pragmazf2livenes1911z42zzliveness_livenessz00(obj_t
		BgL_envz00_4794, obj_t BgL_nz00_4795)
	{
		{	/* Liveness/liveness.scm 369 */
			{	/* Liveness/liveness.scm 371 */
				obj_t BgL_val0_1626z00_5595;
				obj_t BgL_val1_1627z00_5596;

				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11221;

					{
						obj_t BgL_auxz00_11222;

						{	/* Liveness/liveness.scm 371 */
							BgL_objectz00_bglt BgL_tmpz00_11223;

							BgL_tmpz00_11223 =
								((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4795));
							BgL_auxz00_11222 = BGL_OBJECT_WIDENING(BgL_tmpz00_11223);
						}
						BgL_auxz00_11221 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11222);
					}
					BgL_val0_1626z00_5595 =
						(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11221))->
						BgL_inz00);
				}
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11229;

					{
						obj_t BgL_auxz00_11230;

						{	/* Liveness/liveness.scm 371 */
							BgL_objectz00_bglt BgL_tmpz00_11231;

							BgL_tmpz00_11231 =
								((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4795));
							BgL_auxz00_11230 = BGL_OBJECT_WIDENING(BgL_tmpz00_11231);
						}
						BgL_auxz00_11229 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11230);
					}
					BgL_val1_1627z00_5596 =
						(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11229))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 371 */
					int BgL_tmpz00_11237;

					BgL_tmpz00_11237 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11237);
				}
				{	/* Liveness/liveness.scm 371 */
					int BgL_tmpz00_11240;

					BgL_tmpz00_11240 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11240, BgL_val1_1627z00_5596);
				}
				return BgL_val0_1626z00_5595;
			}
		}

	}



/* &inout!-pragma/livene1909 */
	obj_t BGl_z62inoutz12zd2pragmazf2livene1909z50zzliveness_livenessz00(obj_t
		BgL_envz00_4796, obj_t BgL_nz00_4797, obj_t BgL_oz00_4798)
	{
		{	/* Liveness/liveness.scm 362 */
			{
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11243;

				{
					obj_t BgL_auxz00_11244;

					{	/* Liveness/liveness.scm 364 */
						BgL_objectz00_bglt BgL_tmpz00_11245;

						BgL_tmpz00_11245 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4797));
						BgL_auxz00_11244 = BGL_OBJECT_WIDENING(BgL_tmpz00_11245);
					}
					BgL_auxz00_11243 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11244);
				}
				((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11243))->
						BgL_outz00) = ((obj_t) BgL_oz00_4798), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_11258;
				BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11251;

				{	/* Liveness/liveness.scm 365 */
					obj_t BgL_arg2217z00_5598;
					obj_t BgL_arg2218z00_5599;

					{
						BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11259;

						{
							obj_t BgL_auxz00_11260;

							{	/* Liveness/liveness.scm 365 */
								BgL_objectz00_bglt BgL_tmpz00_11261;

								BgL_tmpz00_11261 =
									((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4797));
								BgL_auxz00_11260 = BGL_OBJECT_WIDENING(BgL_tmpz00_11261);
							}
							BgL_auxz00_11259 =
								((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11260);
						}
						BgL_arg2217z00_5598 =
							(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11259))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 365 */
						obj_t BgL_arg2219z00_5600;
						obj_t BgL_arg2220z00_5601;

						{
							BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11267;

							{
								obj_t BgL_auxz00_11268;

								{	/* Liveness/liveness.scm 365 */
									BgL_objectz00_bglt BgL_tmpz00_11269;

									BgL_tmpz00_11269 =
										((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4797));
									BgL_auxz00_11268 = BGL_OBJECT_WIDENING(BgL_tmpz00_11269);
								}
								BgL_auxz00_11267 =
									((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11268);
							}
							BgL_arg2219z00_5600 =
								(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11267))->
								BgL_outz00);
						}
						{
							BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11275;

							{
								obj_t BgL_auxz00_11276;

								{	/* Liveness/liveness.scm 365 */
									BgL_objectz00_bglt BgL_tmpz00_11277;

									BgL_tmpz00_11277 =
										((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4797));
									BgL_auxz00_11276 = BGL_OBJECT_WIDENING(BgL_tmpz00_11277);
								}
								BgL_auxz00_11275 =
									((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11276);
							}
							BgL_arg2220z00_5601 =
								(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11275))->
								BgL_defz00);
						}
						BgL_arg2218z00_5599 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2219z00_5600,
							BgL_arg2220z00_5601);
					}
					BgL_auxz00_11258 =
						BGl_unionz00zzliveness_setz00(BgL_arg2217z00_5598,
						BgL_arg2218z00_5599);
				}
				{
					obj_t BgL_auxz00_11252;

					{	/* Liveness/liveness.scm 365 */
						BgL_objectz00_bglt BgL_tmpz00_11253;

						BgL_tmpz00_11253 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4797));
						BgL_auxz00_11252 = BGL_OBJECT_WIDENING(BgL_tmpz00_11253);
					}
					BgL_auxz00_11251 = ((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11252);
				}
				((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11251))->
						BgL_inz00) = ((obj_t) BgL_auxz00_11258), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 366 */
				obj_t BgL_g1623z00_5602;

				BgL_g1623z00_5602 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_pragmaz00_bglt) BgL_nz00_4797))))->BgL_exprza2za2);
				{
					obj_t BgL_l1621z00_5604;

					BgL_l1621z00_5604 = BgL_g1623z00_5602;
				BgL_zc3z04anonymousza32221ze3z87_5603:
					if (PAIRP(BgL_l1621z00_5604))
						{	/* Liveness/liveness.scm 366 */
							{	/* Liveness/liveness.scm 366 */
								obj_t BgL_ez00_5605;

								BgL_ez00_5605 = CAR(BgL_l1621z00_5604);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_ez00_5605), BgL_oz00_4798);
							}
							{
								obj_t BgL_l1621z00_11294;

								BgL_l1621z00_11294 = CDR(BgL_l1621z00_5604);
								BgL_l1621z00_5604 = BgL_l1621z00_11294;
								goto BgL_zc3z04anonymousza32221ze3z87_5603;
							}
						}
					else
						{	/* Liveness/liveness.scm 366 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 367 */
				obj_t BgL_val0_1624z00_5606;
				obj_t BgL_val1_1625z00_5607;

				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11296;

					{
						obj_t BgL_auxz00_11297;

						{	/* Liveness/liveness.scm 367 */
							BgL_objectz00_bglt BgL_tmpz00_11298;

							BgL_tmpz00_11298 =
								((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4797));
							BgL_auxz00_11297 = BGL_OBJECT_WIDENING(BgL_tmpz00_11298);
						}
						BgL_auxz00_11296 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11297);
					}
					BgL_val0_1624z00_5606 =
						(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11296))->
						BgL_inz00);
				}
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11304;

					{
						obj_t BgL_auxz00_11305;

						{	/* Liveness/liveness.scm 367 */
							BgL_objectz00_bglt BgL_tmpz00_11306;

							BgL_tmpz00_11306 =
								((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4797));
							BgL_auxz00_11305 = BGL_OBJECT_WIDENING(BgL_tmpz00_11306);
						}
						BgL_auxz00_11304 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11305);
					}
					BgL_val1_1625z00_5607 =
						(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11304))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 367 */
					int BgL_tmpz00_11312;

					BgL_tmpz00_11312 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11312);
				}
				{	/* Liveness/liveness.scm 367 */
					int BgL_tmpz00_11315;

					BgL_tmpz00_11315 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11315, BgL_val1_1625z00_5607);
				}
				return BgL_val0_1624z00_5606;
			}
		}

	}



/* &defuse-pragma/livene1907 */
	obj_t BGl_z62defusezd2pragmazf2livene1907z42zzliveness_livenessz00(obj_t
		BgL_envz00_4799, obj_t BgL_nz00_4800)
	{
		{	/* Liveness/liveness.scm 358 */
			{	/* Liveness/liveness.scm 360 */
				obj_t BgL_val0_1619z00_5609;
				obj_t BgL_val1_1620z00_5610;

				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11318;

					{
						obj_t BgL_auxz00_11319;

						{	/* Liveness/liveness.scm 360 */
							BgL_objectz00_bglt BgL_tmpz00_11320;

							BgL_tmpz00_11320 =
								((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4800));
							BgL_auxz00_11319 = BGL_OBJECT_WIDENING(BgL_tmpz00_11320);
						}
						BgL_auxz00_11318 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11319);
					}
					BgL_val0_1619z00_5609 =
						(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11318))->
						BgL_defz00);
				}
				{
					BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11326;

					{
						obj_t BgL_auxz00_11327;

						{	/* Liveness/liveness.scm 360 */
							BgL_objectz00_bglt BgL_tmpz00_11328;

							BgL_tmpz00_11328 =
								((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4800));
							BgL_auxz00_11327 = BGL_OBJECT_WIDENING(BgL_tmpz00_11328);
						}
						BgL_auxz00_11326 =
							((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11327);
					}
					BgL_val1_1620z00_5610 =
						(((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11326))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 360 */
					int BgL_tmpz00_11334;

					BgL_tmpz00_11334 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11334);
				}
				{	/* Liveness/liveness.scm 360 */
					int BgL_tmpz00_11337;

					BgL_tmpz00_11337 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11337, BgL_val1_1620z00_5610);
				}
				return BgL_val0_1619z00_5609;
			}
		}

	}



/* &defuse-pragma1905 */
	obj_t BGl_z62defusezd2pragma1905zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4801, obj_t BgL_nz00_4802)
	{
		{	/* Liveness/liveness.scm 349 */
			{	/* Liveness/liveness.scm 351 */
				obj_t BgL_defz00_5612;

				{	/* Liveness/liveness.scm 352 */
					obj_t BgL_arg2216z00_5613;

					BgL_arg2216z00_5613 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_pragmaz00_bglt) BgL_nz00_4802))))->BgL_exprza2za2);
					BgL_defz00_5612 =
						BGl_defuseza2za2zzliveness_livenessz00(BgL_arg2216z00_5613, BNIL,
						BNIL);
				}
				{	/* Liveness/liveness.scm 352 */
					obj_t BgL_usez00_5614;

					{	/* Liveness/liveness.scm 353 */
						obj_t BgL_tmpz00_5615;

						{	/* Liveness/liveness.scm 353 */
							int BgL_tmpz00_11344;

							BgL_tmpz00_11344 = (int) (1L);
							BgL_tmpz00_5615 = BGL_MVALUES_VAL(BgL_tmpz00_11344);
						}
						{	/* Liveness/liveness.scm 353 */
							int BgL_tmpz00_11347;

							BgL_tmpz00_11347 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11347, BUNSPEC);
						}
						BgL_usez00_5614 = BgL_tmpz00_5615;
					}
					{	/* Liveness/liveness.scm 354 */
						BgL_pragmaz00_bglt BgL_arg2214z00_5616;

						{	/* Liveness/liveness.scm 354 */
							BgL_pragmazf2livenesszf2_bglt BgL_wide1217z00_5617;

							BgL_wide1217z00_5617 =
								((BgL_pragmazf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_pragmazf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 354 */
								obj_t BgL_auxz00_11355;
								BgL_objectz00_bglt BgL_tmpz00_11351;

								BgL_auxz00_11355 = ((obj_t) BgL_wide1217z00_5617);
								BgL_tmpz00_11351 =
									((BgL_objectz00_bglt)
									((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4802)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11351, BgL_auxz00_11355);
							}
							((BgL_objectz00_bglt)
								((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4802)));
							{	/* Liveness/liveness.scm 354 */
								long BgL_arg2215z00_5618;

								BgL_arg2215z00_5618 =
									BGL_CLASS_NUM(BGl_pragmazf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_pragmaz00_bglt)
											((BgL_pragmaz00_bglt) BgL_nz00_4802))),
									BgL_arg2215z00_5618);
							}
							((BgL_pragmaz00_bglt)
								((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4802)));
						}
						{
							BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11369;

							{
								obj_t BgL_auxz00_11370;

								{	/* Liveness/liveness.scm 355 */
									BgL_objectz00_bglt BgL_tmpz00_11371;

									BgL_tmpz00_11371 =
										((BgL_objectz00_bglt)
										((BgL_pragmaz00_bglt)
											((BgL_pragmaz00_bglt) BgL_nz00_4802)));
									BgL_auxz00_11370 = BGL_OBJECT_WIDENING(BgL_tmpz00_11371);
								}
								BgL_auxz00_11369 =
									((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11370);
							}
							((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11369))->
									BgL_defz00) = ((obj_t) BgL_defz00_5612), BUNSPEC);
						}
						{
							BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11378;

							{
								obj_t BgL_auxz00_11379;

								{	/* Liveness/liveness.scm 356 */
									BgL_objectz00_bglt BgL_tmpz00_11380;

									BgL_tmpz00_11380 =
										((BgL_objectz00_bglt)
										((BgL_pragmaz00_bglt)
											((BgL_pragmaz00_bglt) BgL_nz00_4802)));
									BgL_auxz00_11379 = BGL_OBJECT_WIDENING(BgL_tmpz00_11380);
								}
								BgL_auxz00_11378 =
									((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11379);
							}
							((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11378))->
									BgL_usez00) = ((obj_t) BgL_usez00_5614), BUNSPEC);
						}
						{
							BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11387;

							{
								obj_t BgL_auxz00_11388;

								{	/* Liveness/liveness.scm 352 */
									BgL_objectz00_bglt BgL_tmpz00_11389;

									BgL_tmpz00_11389 =
										((BgL_objectz00_bglt)
										((BgL_pragmaz00_bglt)
											((BgL_pragmaz00_bglt) BgL_nz00_4802)));
									BgL_auxz00_11388 = BGL_OBJECT_WIDENING(BgL_tmpz00_11389);
								}
								BgL_auxz00_11387 =
									((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11388);
							}
							((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11387))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_pragmazf2livenesszf2_bglt BgL_auxz00_11396;

							{
								obj_t BgL_auxz00_11397;

								{	/* Liveness/liveness.scm 352 */
									BgL_objectz00_bglt BgL_tmpz00_11398;

									BgL_tmpz00_11398 =
										((BgL_objectz00_bglt)
										((BgL_pragmaz00_bglt)
											((BgL_pragmaz00_bglt) BgL_nz00_4802)));
									BgL_auxz00_11397 = BGL_OBJECT_WIDENING(BgL_tmpz00_11398);
								}
								BgL_auxz00_11396 =
									((BgL_pragmazf2livenesszf2_bglt) BgL_auxz00_11397);
							}
							((((BgL_pragmazf2livenesszf2_bglt) COBJECT(BgL_auxz00_11396))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2214z00_5616 =
							((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nz00_4802));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2214z00_5616));
					}
				}
			}
		}

	}



/* &defuse-extern1903 */
	obj_t BGl_z62defusezd2extern1903zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4803, obj_t BgL_nz00_4804)
	{
		{	/* Liveness/liveness.scm 342 */
			return
				BGl_defuseza2za2zzliveness_livenessz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nz00_4804)))->BgL_exprza2za2), BNIL,
				BNIL);
		}

	}



/* &inout-funcall/livene1901 */
	obj_t BGl_z62inoutzd2funcallzf2livene1901z42zzliveness_livenessz00(obj_t
		BgL_envz00_4805, obj_t BgL_nz00_4806)
	{
		{	/* Liveness/liveness.scm 335 */
			{	/* Liveness/liveness.scm 337 */
				obj_t BgL_val0_1617z00_5621;
				obj_t BgL_val1_1618z00_5622;

				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11412;

					{
						obj_t BgL_auxz00_11413;

						{	/* Liveness/liveness.scm 337 */
							BgL_objectz00_bglt BgL_tmpz00_11414;

							BgL_tmpz00_11414 =
								((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4806));
							BgL_auxz00_11413 = BGL_OBJECT_WIDENING(BgL_tmpz00_11414);
						}
						BgL_auxz00_11412 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11413);
					}
					BgL_val0_1617z00_5621 =
						(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11412))->
						BgL_inz00);
				}
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11420;

					{
						obj_t BgL_auxz00_11421;

						{	/* Liveness/liveness.scm 337 */
							BgL_objectz00_bglt BgL_tmpz00_11422;

							BgL_tmpz00_11422 =
								((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4806));
							BgL_auxz00_11421 = BGL_OBJECT_WIDENING(BgL_tmpz00_11422);
						}
						BgL_auxz00_11420 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11421);
					}
					BgL_val1_1618z00_5622 =
						(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11420))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 337 */
					int BgL_tmpz00_11428;

					BgL_tmpz00_11428 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11428);
				}
				{	/* Liveness/liveness.scm 337 */
					int BgL_tmpz00_11431;

					BgL_tmpz00_11431 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11431, BgL_val1_1618z00_5622);
				}
				return BgL_val0_1617z00_5621;
			}
		}

	}



/* &inout!-funcall/liven1899 */
	obj_t BGl_z62inoutz12zd2funcallzf2liven1899z50zzliveness_livenessz00(obj_t
		BgL_envz00_4807, obj_t BgL_nz00_4808, obj_t BgL_oz00_4809)
	{
		{	/* Liveness/liveness.scm 327 */
			{
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11434;

				{
					obj_t BgL_auxz00_11435;

					{	/* Liveness/liveness.scm 330 */
						BgL_objectz00_bglt BgL_tmpz00_11436;

						BgL_tmpz00_11436 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4808));
						BgL_auxz00_11435 = BGL_OBJECT_WIDENING(BgL_tmpz00_11436);
					}
					BgL_auxz00_11434 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11435);
				}
				((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11434))->
						BgL_outz00) = ((obj_t) BgL_oz00_4809), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_11449;
				BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11442;

				{	/* Liveness/liveness.scm 331 */
					obj_t BgL_arg2206z00_5624;
					obj_t BgL_arg2207z00_5625;

					{
						BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11450;

						{
							obj_t BgL_auxz00_11451;

							{	/* Liveness/liveness.scm 331 */
								BgL_objectz00_bglt BgL_tmpz00_11452;

								BgL_tmpz00_11452 =
									((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4808));
								BgL_auxz00_11451 = BGL_OBJECT_WIDENING(BgL_tmpz00_11452);
							}
							BgL_auxz00_11450 =
								((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11451);
						}
						BgL_arg2206z00_5624 =
							(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11450))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 331 */
						obj_t BgL_arg2208z00_5626;
						obj_t BgL_arg2209z00_5627;

						{
							BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11458;

							{
								obj_t BgL_auxz00_11459;

								{	/* Liveness/liveness.scm 331 */
									BgL_objectz00_bglt BgL_tmpz00_11460;

									BgL_tmpz00_11460 =
										((BgL_objectz00_bglt)
										((BgL_funcallz00_bglt) BgL_nz00_4808));
									BgL_auxz00_11459 = BGL_OBJECT_WIDENING(BgL_tmpz00_11460);
								}
								BgL_auxz00_11458 =
									((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11459);
							}
							BgL_arg2208z00_5626 =
								(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11458))->
								BgL_outz00);
						}
						{
							BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11466;

							{
								obj_t BgL_auxz00_11467;

								{	/* Liveness/liveness.scm 331 */
									BgL_objectz00_bglt BgL_tmpz00_11468;

									BgL_tmpz00_11468 =
										((BgL_objectz00_bglt)
										((BgL_funcallz00_bglt) BgL_nz00_4808));
									BgL_auxz00_11467 = BGL_OBJECT_WIDENING(BgL_tmpz00_11468);
								}
								BgL_auxz00_11466 =
									((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11467);
							}
							BgL_arg2209z00_5627 =
								(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11466))->
								BgL_defz00);
						}
						BgL_arg2207z00_5625 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2208z00_5626,
							BgL_arg2209z00_5627);
					}
					BgL_auxz00_11449 =
						BGl_unionz00zzliveness_setz00(BgL_arg2206z00_5624,
						BgL_arg2207z00_5625);
				}
				{
					obj_t BgL_auxz00_11443;

					{	/* Liveness/liveness.scm 331 */
						BgL_objectz00_bglt BgL_tmpz00_11444;

						BgL_tmpz00_11444 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4808));
						BgL_auxz00_11443 = BGL_OBJECT_WIDENING(BgL_tmpz00_11444);
					}
					BgL_auxz00_11442 =
						((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11443);
				}
				((((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11442))->
						BgL_inz00) = ((obj_t) BgL_auxz00_11449), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 332 */
				obj_t BgL_g1614z00_5628;

				BgL_g1614z00_5628 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt)
								((BgL_funcallz00_bglt) BgL_nz00_4808))))->BgL_argsz00);
				{
					obj_t BgL_l1612z00_5630;

					BgL_l1612z00_5630 = BgL_g1614z00_5628;
				BgL_zc3z04anonymousza32210ze3z87_5629:
					if (PAIRP(BgL_l1612z00_5630))
						{	/* Liveness/liveness.scm 332 */
							{	/* Liveness/liveness.scm 332 */
								obj_t BgL_az00_5631;

								BgL_az00_5631 = CAR(BgL_l1612z00_5630);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_az00_5631), BgL_oz00_4809);
							}
							{
								obj_t BgL_l1612z00_11485;

								BgL_l1612z00_11485 = CDR(BgL_l1612z00_5630);
								BgL_l1612z00_5630 = BgL_l1612z00_11485;
								goto BgL_zc3z04anonymousza32210ze3z87_5629;
							}
						}
					else
						{	/* Liveness/liveness.scm 332 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 333 */
				obj_t BgL_val0_1615z00_5632;
				obj_t BgL_val1_1616z00_5633;

				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11487;

					{
						obj_t BgL_auxz00_11488;

						{	/* Liveness/liveness.scm 333 */
							BgL_objectz00_bglt BgL_tmpz00_11489;

							BgL_tmpz00_11489 =
								((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4808));
							BgL_auxz00_11488 = BGL_OBJECT_WIDENING(BgL_tmpz00_11489);
						}
						BgL_auxz00_11487 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11488);
					}
					BgL_val0_1615z00_5632 =
						(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11487))->
						BgL_inz00);
				}
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11495;

					{
						obj_t BgL_auxz00_11496;

						{	/* Liveness/liveness.scm 333 */
							BgL_objectz00_bglt BgL_tmpz00_11497;

							BgL_tmpz00_11497 =
								((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4808));
							BgL_auxz00_11496 = BGL_OBJECT_WIDENING(BgL_tmpz00_11497);
						}
						BgL_auxz00_11495 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11496);
					}
					BgL_val1_1616z00_5633 =
						(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11495))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 333 */
					int BgL_tmpz00_11503;

					BgL_tmpz00_11503 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11503);
				}
				{	/* Liveness/liveness.scm 333 */
					int BgL_tmpz00_11506;

					BgL_tmpz00_11506 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11506, BgL_val1_1616z00_5633);
				}
				return BgL_val0_1615z00_5632;
			}
		}

	}



/* &defuse-funcall/liven1897 */
	obj_t BGl_z62defusezd2funcallzf2liven1897z42zzliveness_livenessz00(obj_t
		BgL_envz00_4810, obj_t BgL_nz00_4811)
	{
		{	/* Liveness/liveness.scm 323 */
			{	/* Liveness/liveness.scm 325 */
				obj_t BgL_val0_1610z00_5635;
				obj_t BgL_val1_1611z00_5636;

				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11509;

					{
						obj_t BgL_auxz00_11510;

						{	/* Liveness/liveness.scm 325 */
							BgL_objectz00_bglt BgL_tmpz00_11511;

							BgL_tmpz00_11511 =
								((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4811));
							BgL_auxz00_11510 = BGL_OBJECT_WIDENING(BgL_tmpz00_11511);
						}
						BgL_auxz00_11509 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11510);
					}
					BgL_val0_1610z00_5635 =
						(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11509))->
						BgL_defz00);
				}
				{
					BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11517;

					{
						obj_t BgL_auxz00_11518;

						{	/* Liveness/liveness.scm 325 */
							BgL_objectz00_bglt BgL_tmpz00_11519;

							BgL_tmpz00_11519 =
								((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4811));
							BgL_auxz00_11518 = BGL_OBJECT_WIDENING(BgL_tmpz00_11519);
						}
						BgL_auxz00_11517 =
							((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11518);
					}
					BgL_val1_1611z00_5636 =
						(((BgL_funcallzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11517))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 325 */
					int BgL_tmpz00_11525;

					BgL_tmpz00_11525 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11525);
				}
				{	/* Liveness/liveness.scm 325 */
					int BgL_tmpz00_11528;

					BgL_tmpz00_11528 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11528, BgL_val1_1611z00_5636);
				}
				return BgL_val0_1610z00_5635;
			}
		}

	}



/* &defuse-funcall1895 */
	obj_t BGl_z62defusezd2funcall1895zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4812, obj_t BgL_nz00_4813)
	{
		{	/* Liveness/liveness.scm 312 */
			{	/* Liveness/liveness.scm 314 */
				obj_t BgL_dz00_5638;

				BgL_dz00_5638 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nz00_4813)))->BgL_funz00));
				{	/* Liveness/liveness.scm 315 */
					obj_t BgL_uz00_5639;

					{	/* Liveness/liveness.scm 316 */
						obj_t BgL_tmpz00_5640;

						{	/* Liveness/liveness.scm 316 */
							int BgL_tmpz00_11534;

							BgL_tmpz00_11534 = (int) (1L);
							BgL_tmpz00_5640 = BGL_MVALUES_VAL(BgL_tmpz00_11534);
						}
						{	/* Liveness/liveness.scm 316 */
							int BgL_tmpz00_11537;

							BgL_tmpz00_11537 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11537, BUNSPEC);
						}
						BgL_uz00_5639 = BgL_tmpz00_5640;
					}
					{	/* Liveness/liveness.scm 316 */
						obj_t BgL_defz00_5641;

						BgL_defz00_5641 =
							BGl_defuseza2za2zzliveness_livenessz00(
							(((BgL_funcallz00_bglt) COBJECT(
										((BgL_funcallz00_bglt) BgL_nz00_4813)))->BgL_argsz00),
							BgL_dz00_5638, BgL_uz00_5639);
						{	/* Liveness/liveness.scm 317 */
							obj_t BgL_usez00_5642;

							{	/* Liveness/liveness.scm 318 */
								obj_t BgL_tmpz00_5643;

								{	/* Liveness/liveness.scm 318 */
									int BgL_tmpz00_11543;

									BgL_tmpz00_11543 = (int) (1L);
									BgL_tmpz00_5643 = BGL_MVALUES_VAL(BgL_tmpz00_11543);
								}
								{	/* Liveness/liveness.scm 318 */
									int BgL_tmpz00_11546;

									BgL_tmpz00_11546 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_11546, BUNSPEC);
								}
								BgL_usez00_5642 = BgL_tmpz00_5643;
							}
							{	/* Liveness/liveness.scm 319 */
								BgL_funcallz00_bglt BgL_arg2202z00_5644;

								{	/* Liveness/liveness.scm 319 */
									BgL_funcallzf2livenesszf2_bglt BgL_wide1208z00_5645;

									BgL_wide1208z00_5645 =
										((BgL_funcallzf2livenesszf2_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_funcallzf2livenesszf2_bgl))));
									{	/* Liveness/liveness.scm 319 */
										obj_t BgL_auxz00_11554;
										BgL_objectz00_bglt BgL_tmpz00_11550;

										BgL_auxz00_11554 = ((obj_t) BgL_wide1208z00_5645);
										BgL_tmpz00_11550 =
											((BgL_objectz00_bglt)
											((BgL_funcallz00_bglt)
												((BgL_funcallz00_bglt) BgL_nz00_4813)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11550, BgL_auxz00_11554);
									}
									((BgL_objectz00_bglt)
										((BgL_funcallz00_bglt)
											((BgL_funcallz00_bglt) BgL_nz00_4813)));
									{	/* Liveness/liveness.scm 319 */
										long BgL_arg2203z00_5646;

										BgL_arg2203z00_5646 =
											BGL_CLASS_NUM
											(BGl_funcallzf2livenesszf2zzliveness_typesz00);
										BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
													(BgL_funcallz00_bglt) ((BgL_funcallz00_bglt)
														BgL_nz00_4813))), BgL_arg2203z00_5646);
									}
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt)
											((BgL_funcallz00_bglt) BgL_nz00_4813)));
								}
								{
									BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11568;

									{
										obj_t BgL_auxz00_11569;

										{	/* Liveness/liveness.scm 320 */
											BgL_objectz00_bglt BgL_tmpz00_11570;

											BgL_tmpz00_11570 =
												((BgL_objectz00_bglt)
												((BgL_funcallz00_bglt)
													((BgL_funcallz00_bglt) BgL_nz00_4813)));
											BgL_auxz00_11569 = BGL_OBJECT_WIDENING(BgL_tmpz00_11570);
										}
										BgL_auxz00_11568 =
											((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11569);
									}
									((((BgL_funcallzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_11568))->BgL_defz00) =
										((obj_t) BgL_defz00_5641), BUNSPEC);
								}
								{
									BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11577;

									{
										obj_t BgL_auxz00_11578;

										{	/* Liveness/liveness.scm 321 */
											BgL_objectz00_bglt BgL_tmpz00_11579;

											BgL_tmpz00_11579 =
												((BgL_objectz00_bglt)
												((BgL_funcallz00_bglt)
													((BgL_funcallz00_bglt) BgL_nz00_4813)));
											BgL_auxz00_11578 = BGL_OBJECT_WIDENING(BgL_tmpz00_11579);
										}
										BgL_auxz00_11577 =
											((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11578);
									}
									((((BgL_funcallzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_11577))->BgL_usez00) =
										((obj_t) BgL_usez00_5642), BUNSPEC);
								}
								{
									BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11586;

									{
										obj_t BgL_auxz00_11587;

										{	/* Liveness/liveness.scm 317 */
											BgL_objectz00_bglt BgL_tmpz00_11588;

											BgL_tmpz00_11588 =
												((BgL_objectz00_bglt)
												((BgL_funcallz00_bglt)
													((BgL_funcallz00_bglt) BgL_nz00_4813)));
											BgL_auxz00_11587 = BGL_OBJECT_WIDENING(BgL_tmpz00_11588);
										}
										BgL_auxz00_11586 =
											((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11587);
									}
									((((BgL_funcallzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_11586))->BgL_inz00) =
										((obj_t) BNIL), BUNSPEC);
								}
								{
									BgL_funcallzf2livenesszf2_bglt BgL_auxz00_11595;

									{
										obj_t BgL_auxz00_11596;

										{	/* Liveness/liveness.scm 317 */
											BgL_objectz00_bglt BgL_tmpz00_11597;

											BgL_tmpz00_11597 =
												((BgL_objectz00_bglt)
												((BgL_funcallz00_bglt)
													((BgL_funcallz00_bglt) BgL_nz00_4813)));
											BgL_auxz00_11596 = BGL_OBJECT_WIDENING(BgL_tmpz00_11597);
										}
										BgL_auxz00_11595 =
											((BgL_funcallzf2livenesszf2_bglt) BgL_auxz00_11596);
									}
									((((BgL_funcallzf2livenesszf2_bglt)
												COBJECT(BgL_auxz00_11595))->BgL_outz00) =
										((obj_t) BNIL), BUNSPEC);
								}
								BgL_arg2202z00_5644 =
									((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_4813));
								return
									BGl_defusez00zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_arg2202z00_5644));
							}
						}
					}
				}
			}
		}

	}



/* &inout-app-ly/livenes1893 */
	obj_t BGl_z62inoutzd2appzd2lyzf2livenes1893z90zzliveness_livenessz00(obj_t
		BgL_envz00_4814, obj_t BgL_nz00_4815)
	{
		{	/* Liveness/liveness.scm 305 */
			{	/* Liveness/liveness.scm 307 */
				obj_t BgL_val0_1608z00_5648;
				obj_t BgL_val1_1609z00_5649;

				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11608;

					{
						obj_t BgL_auxz00_11609;

						{	/* Liveness/liveness.scm 307 */
							BgL_objectz00_bglt BgL_tmpz00_11610;

							BgL_tmpz00_11610 =
								((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_4815));
							BgL_auxz00_11609 = BGL_OBJECT_WIDENING(BgL_tmpz00_11610);
						}
						BgL_auxz00_11608 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11609);
					}
					BgL_val0_1608z00_5648 =
						(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11608))->
						BgL_inz00);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11616;

					{
						obj_t BgL_auxz00_11617;

						{	/* Liveness/liveness.scm 307 */
							BgL_objectz00_bglt BgL_tmpz00_11618;

							BgL_tmpz00_11618 =
								((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_4815));
							BgL_auxz00_11617 = BGL_OBJECT_WIDENING(BgL_tmpz00_11618);
						}
						BgL_auxz00_11616 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11617);
					}
					BgL_val1_1609z00_5649 =
						(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11616))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 307 */
					int BgL_tmpz00_11624;

					BgL_tmpz00_11624 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11624);
				}
				{	/* Liveness/liveness.scm 307 */
					int BgL_tmpz00_11627;

					BgL_tmpz00_11627 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11627, BgL_val1_1609z00_5649);
				}
				return BgL_val0_1608z00_5648;
			}
		}

	}



/* &inout!-app-ly/livene1891 */
	obj_t BGl_z62inoutz12zd2appzd2lyzf2livene1891z82zzliveness_livenessz00(obj_t
		BgL_envz00_4816, obj_t BgL_nz00_4817, obj_t BgL_oz00_4818)
	{
		{	/* Liveness/liveness.scm 297 */
			{
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11630;

				{
					obj_t BgL_auxz00_11631;

					{	/* Liveness/liveness.scm 300 */
						BgL_objectz00_bglt BgL_tmpz00_11632;

						BgL_tmpz00_11632 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_4817));
						BgL_auxz00_11631 = BGL_OBJECT_WIDENING(BgL_tmpz00_11632);
					}
					BgL_auxz00_11630 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11631);
				}
				((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11630))->
						BgL_outz00) = ((obj_t) BgL_oz00_4818), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_11645;
				BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11638;

				{	/* Liveness/liveness.scm 301 */
					obj_t BgL_arg2197z00_5651;
					obj_t BgL_arg2198z00_5652;

					{
						BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11646;

						{
							obj_t BgL_auxz00_11647;

							{	/* Liveness/liveness.scm 301 */
								BgL_objectz00_bglt BgL_tmpz00_11648;

								BgL_tmpz00_11648 =
									((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_4817));
								BgL_auxz00_11647 = BGL_OBJECT_WIDENING(BgL_tmpz00_11648);
							}
							BgL_auxz00_11646 =
								((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11647);
						}
						BgL_arg2197z00_5651 =
							(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11646))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 301 */
						obj_t BgL_arg2199z00_5653;
						obj_t BgL_arg2200z00_5654;

						{
							BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11654;

							{
								obj_t BgL_auxz00_11655;

								{	/* Liveness/liveness.scm 301 */
									BgL_objectz00_bglt BgL_tmpz00_11656;

									BgL_tmpz00_11656 =
										((BgL_objectz00_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nz00_4817));
									BgL_auxz00_11655 = BGL_OBJECT_WIDENING(BgL_tmpz00_11656);
								}
								BgL_auxz00_11654 =
									((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11655);
							}
							BgL_arg2199z00_5653 =
								(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11654))->
								BgL_outz00);
						}
						{
							BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11662;

							{
								obj_t BgL_auxz00_11663;

								{	/* Liveness/liveness.scm 301 */
									BgL_objectz00_bglt BgL_tmpz00_11664;

									BgL_tmpz00_11664 =
										((BgL_objectz00_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nz00_4817));
									BgL_auxz00_11663 = BGL_OBJECT_WIDENING(BgL_tmpz00_11664);
								}
								BgL_auxz00_11662 =
									((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11663);
							}
							BgL_arg2200z00_5654 =
								(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11662))->
								BgL_defz00);
						}
						BgL_arg2198z00_5652 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2199z00_5653,
							BgL_arg2200z00_5654);
					}
					BgL_auxz00_11645 =
						BGl_unionz00zzliveness_setz00(BgL_arg2197z00_5651,
						BgL_arg2198z00_5652);
				}
				{
					obj_t BgL_auxz00_11639;

					{	/* Liveness/liveness.scm 301 */
						BgL_objectz00_bglt BgL_tmpz00_11640;

						BgL_tmpz00_11640 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_4817));
						BgL_auxz00_11639 = BGL_OBJECT_WIDENING(BgL_tmpz00_11640);
					}
					BgL_auxz00_11638 =
						((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11639);
				}
				((((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11638))->
						BgL_inz00) = ((obj_t) BgL_auxz00_11645), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 302 */
				BgL_nodez00_bglt BgL_arg2201z00_5655;

				BgL_arg2201z00_5655 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_nz00_4817))))->BgL_argz00);
				BGl_inoutz12z12zzliveness_livenessz00(BgL_arg2201z00_5655,
					BgL_oz00_4818);
			}
			{	/* Liveness/liveness.scm 303 */
				obj_t BgL_val0_1606z00_5656;
				obj_t BgL_val1_1607z00_5657;

				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11677;

					{
						obj_t BgL_auxz00_11678;

						{	/* Liveness/liveness.scm 303 */
							BgL_objectz00_bglt BgL_tmpz00_11679;

							BgL_tmpz00_11679 =
								((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_4817));
							BgL_auxz00_11678 = BGL_OBJECT_WIDENING(BgL_tmpz00_11679);
						}
						BgL_auxz00_11677 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11678);
					}
					BgL_val0_1606z00_5656 =
						(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11677))->
						BgL_inz00);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11685;

					{
						obj_t BgL_auxz00_11686;

						{	/* Liveness/liveness.scm 303 */
							BgL_objectz00_bglt BgL_tmpz00_11687;

							BgL_tmpz00_11687 =
								((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_4817));
							BgL_auxz00_11686 = BGL_OBJECT_WIDENING(BgL_tmpz00_11687);
						}
						BgL_auxz00_11685 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11686);
					}
					BgL_val1_1607z00_5657 =
						(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11685))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 303 */
					int BgL_tmpz00_11693;

					BgL_tmpz00_11693 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11693);
				}
				{	/* Liveness/liveness.scm 303 */
					int BgL_tmpz00_11696;

					BgL_tmpz00_11696 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11696, BgL_val1_1607z00_5657);
				}
				return BgL_val0_1606z00_5656;
			}
		}

	}



/* &defuse-app-ly/livene1889 */
	obj_t BGl_z62defusezd2appzd2lyzf2livene1889z90zzliveness_livenessz00(obj_t
		BgL_envz00_4819, obj_t BgL_nz00_4820)
	{
		{	/* Liveness/liveness.scm 293 */
			{	/* Liveness/liveness.scm 295 */
				obj_t BgL_val0_1604z00_5659;
				obj_t BgL_val1_1605z00_5660;

				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11699;

					{
						obj_t BgL_auxz00_11700;

						{	/* Liveness/liveness.scm 295 */
							BgL_objectz00_bglt BgL_tmpz00_11701;

							BgL_tmpz00_11701 =
								((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_4820));
							BgL_auxz00_11700 = BGL_OBJECT_WIDENING(BgL_tmpz00_11701);
						}
						BgL_auxz00_11699 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11700);
					}
					BgL_val0_1604z00_5659 =
						(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11699))->
						BgL_defz00);
				}
				{
					BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11707;

					{
						obj_t BgL_auxz00_11708;

						{	/* Liveness/liveness.scm 295 */
							BgL_objectz00_bglt BgL_tmpz00_11709;

							BgL_tmpz00_11709 =
								((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_4820));
							BgL_auxz00_11708 = BGL_OBJECT_WIDENING(BgL_tmpz00_11709);
						}
						BgL_auxz00_11707 =
							((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11708);
					}
					BgL_val1_1605z00_5660 =
						(((BgL_appzd2lyzf2livenessz20_bglt) COBJECT(BgL_auxz00_11707))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 295 */
					int BgL_tmpz00_11715;

					BgL_tmpz00_11715 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11715);
				}
				{	/* Liveness/liveness.scm 295 */
					int BgL_tmpz00_11718;

					BgL_tmpz00_11718 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11718, BgL_val1_1605z00_5660);
				}
				return BgL_val0_1604z00_5659;
			}
		}

	}



/* &defuse-app-ly1887 */
	obj_t BGl_z62defusezd2appzd2ly1887z62zzliveness_livenessz00(obj_t
		BgL_envz00_4821, obj_t BgL_nz00_4822)
	{
		{	/* Liveness/liveness.scm 282 */
			{	/* Liveness/liveness.scm 284 */
				obj_t BgL_deffunz00_5662;

				BgL_deffunz00_5662 =
					BGl_defusez00zzliveness_livenessz00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_4822)))->BgL_funz00));
				{	/* Liveness/liveness.scm 285 */
					obj_t BgL_usefunz00_5663;

					{	/* Liveness/liveness.scm 286 */
						obj_t BgL_tmpz00_5664;

						{	/* Liveness/liveness.scm 286 */
							int BgL_tmpz00_11724;

							BgL_tmpz00_11724 = (int) (1L);
							BgL_tmpz00_5664 = BGL_MVALUES_VAL(BgL_tmpz00_11724);
						}
						{	/* Liveness/liveness.scm 286 */
							int BgL_tmpz00_11727;

							BgL_tmpz00_11727 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11727, BUNSPEC);
						}
						BgL_usefunz00_5663 = BgL_tmpz00_5664;
					}
					{	/* Liveness/liveness.scm 286 */
						obj_t BgL_defargz00_5665;

						BgL_defargz00_5665 =
							BGl_defusez00zzliveness_livenessz00(
							(((BgL_appzd2lyzd2_bglt) COBJECT(
										((BgL_appzd2lyzd2_bglt) BgL_nz00_4822)))->BgL_argz00));
						{	/* Liveness/liveness.scm 287 */
							obj_t BgL_useargz00_5666;

							{	/* Liveness/liveness.scm 288 */
								obj_t BgL_tmpz00_5667;

								{	/* Liveness/liveness.scm 288 */
									int BgL_tmpz00_11733;

									BgL_tmpz00_11733 = (int) (1L);
									BgL_tmpz00_5667 = BGL_MVALUES_VAL(BgL_tmpz00_11733);
								}
								{	/* Liveness/liveness.scm 288 */
									int BgL_tmpz00_11736;

									BgL_tmpz00_11736 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_11736, BUNSPEC);
								}
								BgL_useargz00_5666 = BgL_tmpz00_5667;
							}
							{	/* Liveness/liveness.scm 289 */
								BgL_appzd2lyzd2_bglt BgL_arg2192z00_5668;

								{	/* Liveness/liveness.scm 289 */
									BgL_appzd2lyzf2livenessz20_bglt BgL_wide1200z00_5669;

									BgL_wide1200z00_5669 =
										((BgL_appzd2lyzf2livenessz20_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_appzd2lyzf2livenessz20_bgl))));
									{	/* Liveness/liveness.scm 289 */
										obj_t BgL_auxz00_11744;
										BgL_objectz00_bglt BgL_tmpz00_11740;

										BgL_auxz00_11744 = ((obj_t) BgL_wide1200z00_5669);
										BgL_tmpz00_11740 =
											((BgL_objectz00_bglt)
											((BgL_appzd2lyzd2_bglt)
												((BgL_appzd2lyzd2_bglt) BgL_nz00_4822)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11740, BgL_auxz00_11744);
									}
									((BgL_objectz00_bglt)
										((BgL_appzd2lyzd2_bglt)
											((BgL_appzd2lyzd2_bglt) BgL_nz00_4822)));
									{	/* Liveness/liveness.scm 289 */
										long BgL_arg2193z00_5670;

										BgL_arg2193z00_5670 =
											BGL_CLASS_NUM
											(BGl_appzd2lyzf2livenessz20zzliveness_typesz00);
										BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
													(BgL_appzd2lyzd2_bglt) ((BgL_appzd2lyzd2_bglt)
														BgL_nz00_4822))), BgL_arg2193z00_5670);
									}
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt)
											((BgL_appzd2lyzd2_bglt) BgL_nz00_4822)));
								}
								{
									BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11758;

									{
										obj_t BgL_auxz00_11759;

										{	/* Liveness/liveness.scm 290 */
											BgL_objectz00_bglt BgL_tmpz00_11760;

											BgL_tmpz00_11760 =
												((BgL_objectz00_bglt)
												((BgL_appzd2lyzd2_bglt)
													((BgL_appzd2lyzd2_bglt) BgL_nz00_4822)));
											BgL_auxz00_11759 = BGL_OBJECT_WIDENING(BgL_tmpz00_11760);
										}
										BgL_auxz00_11758 =
											((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11759);
									}
									((((BgL_appzd2lyzf2livenessz20_bglt)
												COBJECT(BgL_auxz00_11758))->BgL_defz00) =
										((obj_t) BGl_unionz00zzliveness_setz00(BgL_deffunz00_5662,
												BgL_defargz00_5665)), BUNSPEC);
								}
								{
									BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11768;

									{
										obj_t BgL_auxz00_11769;

										{	/* Liveness/liveness.scm 291 */
											BgL_objectz00_bglt BgL_tmpz00_11770;

											BgL_tmpz00_11770 =
												((BgL_objectz00_bglt)
												((BgL_appzd2lyzd2_bglt)
													((BgL_appzd2lyzd2_bglt) BgL_nz00_4822)));
											BgL_auxz00_11769 = BGL_OBJECT_WIDENING(BgL_tmpz00_11770);
										}
										BgL_auxz00_11768 =
											((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11769);
									}
									((((BgL_appzd2lyzf2livenessz20_bglt)
												COBJECT(BgL_auxz00_11768))->BgL_usez00) =
										((obj_t) BGl_unionz00zzliveness_setz00(BgL_usefunz00_5663,
												BgL_useargz00_5666)), BUNSPEC);
								}
								{
									BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11778;

									{
										obj_t BgL_auxz00_11779;

										{	/* Liveness/liveness.scm 287 */
											BgL_objectz00_bglt BgL_tmpz00_11780;

											BgL_tmpz00_11780 =
												((BgL_objectz00_bglt)
												((BgL_appzd2lyzd2_bglt)
													((BgL_appzd2lyzd2_bglt) BgL_nz00_4822)));
											BgL_auxz00_11779 = BGL_OBJECT_WIDENING(BgL_tmpz00_11780);
										}
										BgL_auxz00_11778 =
											((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11779);
									}
									((((BgL_appzd2lyzf2livenessz20_bglt)
												COBJECT(BgL_auxz00_11778))->BgL_inz00) =
										((obj_t) BNIL), BUNSPEC);
								}
								{
									BgL_appzd2lyzf2livenessz20_bglt BgL_auxz00_11787;

									{
										obj_t BgL_auxz00_11788;

										{	/* Liveness/liveness.scm 287 */
											BgL_objectz00_bglt BgL_tmpz00_11789;

											BgL_tmpz00_11789 =
												((BgL_objectz00_bglt)
												((BgL_appzd2lyzd2_bglt)
													((BgL_appzd2lyzd2_bglt) BgL_nz00_4822)));
											BgL_auxz00_11788 = BGL_OBJECT_WIDENING(BgL_tmpz00_11789);
										}
										BgL_auxz00_11787 =
											((BgL_appzd2lyzf2livenessz20_bglt) BgL_auxz00_11788);
									}
									((((BgL_appzd2lyzf2livenessz20_bglt)
												COBJECT(BgL_auxz00_11787))->BgL_outz00) =
										((obj_t) BNIL), BUNSPEC);
								}
								BgL_arg2192z00_5668 =
									((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nz00_4822));
								return
									BGl_defusez00zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_arg2192z00_5668));
							}
						}
					}
				}
			}
		}

	}



/* &inout-app/liveness1885 */
	obj_t BGl_z62inoutzd2appzf2liveness1885z42zzliveness_livenessz00(obj_t
		BgL_envz00_4823, obj_t BgL_nz00_4824)
	{
		{	/* Liveness/liveness.scm 275 */
			{	/* Liveness/liveness.scm 277 */
				obj_t BgL_val0_1602z00_5672;
				obj_t BgL_val1_1603z00_5673;

				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_11800;

					{
						obj_t BgL_auxz00_11801;

						{	/* Liveness/liveness.scm 277 */
							BgL_objectz00_bglt BgL_tmpz00_11802;

							BgL_tmpz00_11802 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4824));
							BgL_auxz00_11801 = BGL_OBJECT_WIDENING(BgL_tmpz00_11802);
						}
						BgL_auxz00_11800 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11801);
					}
					BgL_val0_1602z00_5672 =
						(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11800))->
						BgL_inz00);
				}
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_11808;

					{
						obj_t BgL_auxz00_11809;

						{	/* Liveness/liveness.scm 277 */
							BgL_objectz00_bglt BgL_tmpz00_11810;

							BgL_tmpz00_11810 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4824));
							BgL_auxz00_11809 = BGL_OBJECT_WIDENING(BgL_tmpz00_11810);
						}
						BgL_auxz00_11808 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11809);
					}
					BgL_val1_1603z00_5673 =
						(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11808))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 277 */
					int BgL_tmpz00_11816;

					BgL_tmpz00_11816 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11816);
				}
				{	/* Liveness/liveness.scm 277 */
					int BgL_tmpz00_11819;

					BgL_tmpz00_11819 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11819, BgL_val1_1603z00_5673);
				}
				return BgL_val0_1602z00_5672;
			}
		}

	}



/* &inout!-app/liveness1883 */
	obj_t BGl_z62inoutz12zd2appzf2liveness1883z50zzliveness_livenessz00(obj_t
		BgL_envz00_4825, obj_t BgL_nz00_4826, obj_t BgL_oz00_4827)
	{
		{	/* Liveness/liveness.scm 266 */
			{
				BgL_appzf2livenesszf2_bglt BgL_auxz00_11822;

				{
					obj_t BgL_auxz00_11823;

					{	/* Liveness/liveness.scm 270 */
						BgL_objectz00_bglt BgL_tmpz00_11824;

						BgL_tmpz00_11824 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4826));
						BgL_auxz00_11823 = BGL_OBJECT_WIDENING(BgL_tmpz00_11824);
					}
					BgL_auxz00_11822 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11823);
				}
				((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11822))->
						BgL_outz00) = ((obj_t) BgL_oz00_4827), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_11837;
				BgL_appzf2livenesszf2_bglt BgL_auxz00_11830;

				{	/* Liveness/liveness.scm 271 */
					obj_t BgL_arg2185z00_5675;
					obj_t BgL_arg2186z00_5676;

					{
						BgL_appzf2livenesszf2_bglt BgL_auxz00_11838;

						{
							obj_t BgL_auxz00_11839;

							{	/* Liveness/liveness.scm 271 */
								BgL_objectz00_bglt BgL_tmpz00_11840;

								BgL_tmpz00_11840 =
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4826));
								BgL_auxz00_11839 = BGL_OBJECT_WIDENING(BgL_tmpz00_11840);
							}
							BgL_auxz00_11838 =
								((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11839);
						}
						BgL_arg2185z00_5675 =
							(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11838))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 271 */
						obj_t BgL_arg2187z00_5677;
						obj_t BgL_arg2188z00_5678;

						{
							BgL_appzf2livenesszf2_bglt BgL_auxz00_11846;

							{
								obj_t BgL_auxz00_11847;

								{	/* Liveness/liveness.scm 271 */
									BgL_objectz00_bglt BgL_tmpz00_11848;

									BgL_tmpz00_11848 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4826));
									BgL_auxz00_11847 = BGL_OBJECT_WIDENING(BgL_tmpz00_11848);
								}
								BgL_auxz00_11846 =
									((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11847);
							}
							BgL_arg2187z00_5677 =
								(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11846))->
								BgL_outz00);
						}
						{
							BgL_appzf2livenesszf2_bglt BgL_auxz00_11854;

							{
								obj_t BgL_auxz00_11855;

								{	/* Liveness/liveness.scm 271 */
									BgL_objectz00_bglt BgL_tmpz00_11856;

									BgL_tmpz00_11856 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4826));
									BgL_auxz00_11855 = BGL_OBJECT_WIDENING(BgL_tmpz00_11856);
								}
								BgL_auxz00_11854 =
									((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11855);
							}
							BgL_arg2188z00_5678 =
								(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11854))->
								BgL_defz00);
						}
						BgL_arg2186z00_5676 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2187z00_5677,
							BgL_arg2188z00_5678);
					}
					BgL_auxz00_11837 =
						BGl_unionz00zzliveness_setz00(BgL_arg2185z00_5675,
						BgL_arg2186z00_5676);
				}
				{
					obj_t BgL_auxz00_11831;

					{	/* Liveness/liveness.scm 271 */
						BgL_objectz00_bglt BgL_tmpz00_11832;

						BgL_tmpz00_11832 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4826));
						BgL_auxz00_11831 = BGL_OBJECT_WIDENING(BgL_tmpz00_11832);
					}
					BgL_auxz00_11830 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11831);
				}
				((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11830))->BgL_inz00) =
					((obj_t) BgL_auxz00_11837), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 272 */
				obj_t BgL_g1599z00_5679;

				BgL_g1599z00_5679 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nz00_4826))))->BgL_argsz00);
				{
					obj_t BgL_l1597z00_5681;

					BgL_l1597z00_5681 = BgL_g1599z00_5679;
				BgL_zc3z04anonymousza32189ze3z87_5680:
					if (PAIRP(BgL_l1597z00_5681))
						{	/* Liveness/liveness.scm 272 */
							{	/* Liveness/liveness.scm 272 */
								obj_t BgL_az00_5682;

								BgL_az00_5682 = CAR(BgL_l1597z00_5681);
								BGl_inoutz12z12zzliveness_livenessz00(
									((BgL_nodez00_bglt) BgL_az00_5682), BgL_oz00_4827);
							}
							{
								obj_t BgL_l1597z00_11873;

								BgL_l1597z00_11873 = CDR(BgL_l1597z00_5681);
								BgL_l1597z00_5681 = BgL_l1597z00_11873;
								goto BgL_zc3z04anonymousza32189ze3z87_5680;
							}
						}
					else
						{	/* Liveness/liveness.scm 272 */
							((bool_t) 1);
						}
				}
			}
			{	/* Liveness/liveness.scm 273 */
				obj_t BgL_val0_1600z00_5683;
				obj_t BgL_val1_1601z00_5684;

				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_11875;

					{
						obj_t BgL_auxz00_11876;

						{	/* Liveness/liveness.scm 273 */
							BgL_objectz00_bglt BgL_tmpz00_11877;

							BgL_tmpz00_11877 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4826));
							BgL_auxz00_11876 = BGL_OBJECT_WIDENING(BgL_tmpz00_11877);
						}
						BgL_auxz00_11875 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11876);
					}
					BgL_val0_1600z00_5683 =
						(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11875))->
						BgL_inz00);
				}
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_11883;

					{
						obj_t BgL_auxz00_11884;

						{	/* Liveness/liveness.scm 273 */
							BgL_objectz00_bglt BgL_tmpz00_11885;

							BgL_tmpz00_11885 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4826));
							BgL_auxz00_11884 = BGL_OBJECT_WIDENING(BgL_tmpz00_11885);
						}
						BgL_auxz00_11883 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11884);
					}
					BgL_val1_1601z00_5684 =
						(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11883))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 273 */
					int BgL_tmpz00_11891;

					BgL_tmpz00_11891 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11891);
				}
				{	/* Liveness/liveness.scm 273 */
					int BgL_tmpz00_11894;

					BgL_tmpz00_11894 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11894, BgL_val1_1601z00_5684);
				}
				return BgL_val0_1600z00_5683;
			}
		}

	}



/* &defuse-app/liveness1881 */
	obj_t BGl_z62defusezd2appzf2liveness1881z42zzliveness_livenessz00(obj_t
		BgL_envz00_4828, obj_t BgL_nz00_4829)
	{
		{	/* Liveness/liveness.scm 262 */
			{	/* Liveness/liveness.scm 264 */
				obj_t BgL_val0_1595z00_5686;
				obj_t BgL_val1_1596z00_5687;

				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_11897;

					{
						obj_t BgL_auxz00_11898;

						{	/* Liveness/liveness.scm 264 */
							BgL_objectz00_bglt BgL_tmpz00_11899;

							BgL_tmpz00_11899 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4829));
							BgL_auxz00_11898 = BGL_OBJECT_WIDENING(BgL_tmpz00_11899);
						}
						BgL_auxz00_11897 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11898);
					}
					BgL_val0_1595z00_5686 =
						(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11897))->
						BgL_defz00);
				}
				{
					BgL_appzf2livenesszf2_bglt BgL_auxz00_11905;

					{
						obj_t BgL_auxz00_11906;

						{	/* Liveness/liveness.scm 264 */
							BgL_objectz00_bglt BgL_tmpz00_11907;

							BgL_tmpz00_11907 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4829));
							BgL_auxz00_11906 = BGL_OBJECT_WIDENING(BgL_tmpz00_11907);
						}
						BgL_auxz00_11905 = ((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11906);
					}
					BgL_val1_1596z00_5687 =
						(((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11905))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 264 */
					int BgL_tmpz00_11913;

					BgL_tmpz00_11913 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11913);
				}
				{	/* Liveness/liveness.scm 264 */
					int BgL_tmpz00_11916;

					BgL_tmpz00_11916 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11916, BgL_val1_1596z00_5687);
				}
				return BgL_val0_1595z00_5686;
			}
		}

	}



/* &defuse-app1879 */
	obj_t BGl_z62defusezd2app1879zb0zzliveness_livenessz00(obj_t BgL_envz00_4830,
		obj_t BgL_nz00_4831)
	{
		{	/* Liveness/liveness.scm 252 */
			{	/* Liveness/liveness.scm 255 */
				obj_t BgL_defz00_5689;

				BgL_defz00_5689 =
					BGl_defuseza2za2zzliveness_livenessz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nz00_4831)))->BgL_argsz00), BNIL, BNIL);
				{	/* Liveness/liveness.scm 256 */
					obj_t BgL_usez00_5690;

					{	/* Liveness/liveness.scm 257 */
						obj_t BgL_tmpz00_5691;

						{	/* Liveness/liveness.scm 257 */
							int BgL_tmpz00_11922;

							BgL_tmpz00_11922 = (int) (1L);
							BgL_tmpz00_5691 = BGL_MVALUES_VAL(BgL_tmpz00_11922);
						}
						{	/* Liveness/liveness.scm 257 */
							int BgL_tmpz00_11925;

							BgL_tmpz00_11925 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11925, BUNSPEC);
						}
						BgL_usez00_5690 = BgL_tmpz00_5691;
					}
					{	/* Liveness/liveness.scm 258 */
						BgL_appz00_bglt BgL_arg2182z00_5692;

						{	/* Liveness/liveness.scm 258 */
							BgL_appzf2livenesszf2_bglt BgL_wide1192z00_5693;

							BgL_wide1192z00_5693 =
								((BgL_appzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_appzf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 258 */
								obj_t BgL_auxz00_11933;
								BgL_objectz00_bglt BgL_tmpz00_11929;

								BgL_auxz00_11933 = ((obj_t) BgL_wide1192z00_5693);
								BgL_tmpz00_11929 =
									((BgL_objectz00_bglt)
									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4831)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11929, BgL_auxz00_11933);
							}
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4831)));
							{	/* Liveness/liveness.scm 258 */
								long BgL_arg2183z00_5694;

								BgL_arg2183z00_5694 =
									BGL_CLASS_NUM(BGl_appzf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) BgL_nz00_4831))), BgL_arg2183z00_5694);
							}
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4831)));
						}
						{
							BgL_appzf2livenesszf2_bglt BgL_auxz00_11947;

							{
								obj_t BgL_auxz00_11948;

								{	/* Liveness/liveness.scm 259 */
									BgL_objectz00_bglt BgL_tmpz00_11949;

									BgL_tmpz00_11949 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4831)));
									BgL_auxz00_11948 = BGL_OBJECT_WIDENING(BgL_tmpz00_11949);
								}
								BgL_auxz00_11947 =
									((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11948);
							}
							((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11947))->
									BgL_defz00) = ((obj_t) BgL_defz00_5689), BUNSPEC);
						}
						{
							BgL_appzf2livenesszf2_bglt BgL_auxz00_11956;

							{
								obj_t BgL_auxz00_11957;

								{	/* Liveness/liveness.scm 260 */
									BgL_objectz00_bglt BgL_tmpz00_11958;

									BgL_tmpz00_11958 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4831)));
									BgL_auxz00_11957 = BGL_OBJECT_WIDENING(BgL_tmpz00_11958);
								}
								BgL_auxz00_11956 =
									((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11957);
							}
							((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11956))->
									BgL_usez00) = ((obj_t) BgL_usez00_5690), BUNSPEC);
						}
						{
							BgL_appzf2livenesszf2_bglt BgL_auxz00_11965;

							{
								obj_t BgL_auxz00_11966;

								{	/* Liveness/liveness.scm 256 */
									BgL_objectz00_bglt BgL_tmpz00_11967;

									BgL_tmpz00_11967 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4831)));
									BgL_auxz00_11966 = BGL_OBJECT_WIDENING(BgL_tmpz00_11967);
								}
								BgL_auxz00_11965 =
									((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11966);
							}
							((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11965))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_appzf2livenesszf2_bglt BgL_auxz00_11974;

							{
								obj_t BgL_auxz00_11975;

								{	/* Liveness/liveness.scm 256 */
									BgL_objectz00_bglt BgL_tmpz00_11976;

									BgL_tmpz00_11976 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4831)));
									BgL_auxz00_11975 = BGL_OBJECT_WIDENING(BgL_tmpz00_11976);
								}
								BgL_auxz00_11974 =
									((BgL_appzf2livenesszf2_bglt) BgL_auxz00_11975);
							}
							((((BgL_appzf2livenesszf2_bglt) COBJECT(BgL_auxz00_11974))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2182z00_5692 =
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nz00_4831));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2182z00_5692));
					}
				}
			}
		}

	}



/* &inout-sequence/liven1877 */
	obj_t BGl_z62inoutzd2sequencezf2liven1877z42zzliveness_livenessz00(obj_t
		BgL_envz00_4832, obj_t BgL_nz00_4833)
	{
		{	/* Liveness/liveness.scm 245 */
			{	/* Liveness/liveness.scm 247 */
				obj_t BgL_val0_1593z00_5696;
				obj_t BgL_val1_1594z00_5697;

				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_11987;

					{
						obj_t BgL_auxz00_11988;

						{	/* Liveness/liveness.scm 247 */
							BgL_objectz00_bglt BgL_tmpz00_11989;

							BgL_tmpz00_11989 =
								((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4833));
							BgL_auxz00_11988 = BGL_OBJECT_WIDENING(BgL_tmpz00_11989);
						}
						BgL_auxz00_11987 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_11988);
					}
					BgL_val0_1593z00_5696 =
						(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_11987))->
						BgL_inz00);
				}
				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_11995;

					{
						obj_t BgL_auxz00_11996;

						{	/* Liveness/liveness.scm 247 */
							BgL_objectz00_bglt BgL_tmpz00_11997;

							BgL_tmpz00_11997 =
								((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4833));
							BgL_auxz00_11996 = BGL_OBJECT_WIDENING(BgL_tmpz00_11997);
						}
						BgL_auxz00_11995 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_11996);
					}
					BgL_val1_1594z00_5697 =
						(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_11995))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 247 */
					int BgL_tmpz00_12003;

					BgL_tmpz00_12003 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12003);
				}
				{	/* Liveness/liveness.scm 247 */
					int BgL_tmpz00_12006;

					BgL_tmpz00_12006 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12006, BgL_val1_1594z00_5697);
				}
				return BgL_val0_1593z00_5696;
			}
		}

	}



/* &inout!-sequence/live1875 */
	obj_t BGl_z62inoutz12zd2sequencezf2live1875z50zzliveness_livenessz00(obj_t
		BgL_envz00_4834, obj_t BgL_nz00_4835, obj_t BgL_oz00_4836)
	{
		{	/* Liveness/liveness.scm 237 */
			{
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12009;

				{
					obj_t BgL_auxz00_12010;

					{	/* Liveness/liveness.scm 240 */
						BgL_objectz00_bglt BgL_tmpz00_12011;

						BgL_tmpz00_12011 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4835));
						BgL_auxz00_12010 = BGL_OBJECT_WIDENING(BgL_tmpz00_12011);
					}
					BgL_auxz00_12009 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12010);
				}
				((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12009))->
						BgL_outz00) = ((obj_t) BgL_oz00_4836), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_12024;
				BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12017;

				{	/* Liveness/liveness.scm 241 */
					obj_t BgL_arg2177z00_5699;
					obj_t BgL_arg2178z00_5700;

					{
						BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12025;

						{
							obj_t BgL_auxz00_12026;

							{	/* Liveness/liveness.scm 241 */
								BgL_objectz00_bglt BgL_tmpz00_12027;

								BgL_tmpz00_12027 =
									((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4835));
								BgL_auxz00_12026 = BGL_OBJECT_WIDENING(BgL_tmpz00_12027);
							}
							BgL_auxz00_12025 =
								((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12026);
						}
						BgL_arg2177z00_5699 =
							(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12025))->
							BgL_usez00);
					}
					{	/* Liveness/liveness.scm 241 */
						obj_t BgL_arg2179z00_5701;
						obj_t BgL_arg2180z00_5702;

						{
							BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12033;

							{
								obj_t BgL_auxz00_12034;

								{	/* Liveness/liveness.scm 241 */
									BgL_objectz00_bglt BgL_tmpz00_12035;

									BgL_tmpz00_12035 =
										((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt) BgL_nz00_4835));
									BgL_auxz00_12034 = BGL_OBJECT_WIDENING(BgL_tmpz00_12035);
								}
								BgL_auxz00_12033 =
									((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12034);
							}
							BgL_arg2179z00_5701 =
								(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12033))->
								BgL_outz00);
						}
						{
							BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12041;

							{
								obj_t BgL_auxz00_12042;

								{	/* Liveness/liveness.scm 241 */
									BgL_objectz00_bglt BgL_tmpz00_12043;

									BgL_tmpz00_12043 =
										((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt) BgL_nz00_4835));
									BgL_auxz00_12042 = BGL_OBJECT_WIDENING(BgL_tmpz00_12043);
								}
								BgL_auxz00_12041 =
									((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12042);
							}
							BgL_arg2180z00_5702 =
								(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12041))->
								BgL_defz00);
						}
						BgL_arg2178z00_5700 =
							BGl_disjonctionz00zzliveness_setz00(BgL_arg2179z00_5701,
							BgL_arg2180z00_5702);
					}
					BgL_auxz00_12024 =
						BGl_unionz00zzliveness_setz00(BgL_arg2177z00_5699,
						BgL_arg2178z00_5700);
				}
				{
					obj_t BgL_auxz00_12018;

					{	/* Liveness/liveness.scm 241 */
						BgL_objectz00_bglt BgL_tmpz00_12019;

						BgL_tmpz00_12019 =
							((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4835));
						BgL_auxz00_12018 = BGL_OBJECT_WIDENING(BgL_tmpz00_12019);
					}
					BgL_auxz00_12017 =
						((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12018);
				}
				((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12017))->
						BgL_inz00) = ((obj_t) BgL_auxz00_12024), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 242 */
				obj_t BgL_arg2181z00_5703;

				BgL_arg2181z00_5703 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt)
								((BgL_sequencez00_bglt) BgL_nz00_4835))))->BgL_nodesz00);
				BGl_inoutza2z12zb0zzliveness_livenessz00(BgL_arg2181z00_5703,
					BgL_oz00_4836);
			}
			{	/* Liveness/liveness.scm 243 */
				obj_t BgL_val0_1591z00_5704;
				obj_t BgL_val1_1592z00_5705;

				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12056;

					{
						obj_t BgL_auxz00_12057;

						{	/* Liveness/liveness.scm 243 */
							BgL_objectz00_bglt BgL_tmpz00_12058;

							BgL_tmpz00_12058 =
								((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4835));
							BgL_auxz00_12057 = BGL_OBJECT_WIDENING(BgL_tmpz00_12058);
						}
						BgL_auxz00_12056 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12057);
					}
					BgL_val0_1591z00_5704 =
						(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12056))->
						BgL_inz00);
				}
				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12064;

					{
						obj_t BgL_auxz00_12065;

						{	/* Liveness/liveness.scm 243 */
							BgL_objectz00_bglt BgL_tmpz00_12066;

							BgL_tmpz00_12066 =
								((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4835));
							BgL_auxz00_12065 = BGL_OBJECT_WIDENING(BgL_tmpz00_12066);
						}
						BgL_auxz00_12064 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12065);
					}
					BgL_val1_1592z00_5705 =
						(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12064))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 243 */
					int BgL_tmpz00_12072;

					BgL_tmpz00_12072 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12072);
				}
				{	/* Liveness/liveness.scm 243 */
					int BgL_tmpz00_12075;

					BgL_tmpz00_12075 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12075, BgL_val1_1592z00_5705);
				}
				return BgL_val0_1591z00_5704;
			}
		}

	}



/* &defuse-sequence/live1873 */
	obj_t BGl_z62defusezd2sequencezf2live1873z42zzliveness_livenessz00(obj_t
		BgL_envz00_4837, obj_t BgL_nz00_4838)
	{
		{	/* Liveness/liveness.scm 233 */
			{	/* Liveness/liveness.scm 235 */
				obj_t BgL_val0_1589z00_5707;
				obj_t BgL_val1_1590z00_5708;

				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12078;

					{
						obj_t BgL_auxz00_12079;

						{	/* Liveness/liveness.scm 235 */
							BgL_objectz00_bglt BgL_tmpz00_12080;

							BgL_tmpz00_12080 =
								((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4838));
							BgL_auxz00_12079 = BGL_OBJECT_WIDENING(BgL_tmpz00_12080);
						}
						BgL_auxz00_12078 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12079);
					}
					BgL_val0_1589z00_5707 =
						(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12078))->
						BgL_defz00);
				}
				{
					BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12086;

					{
						obj_t BgL_auxz00_12087;

						{	/* Liveness/liveness.scm 235 */
							BgL_objectz00_bglt BgL_tmpz00_12088;

							BgL_tmpz00_12088 =
								((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4838));
							BgL_auxz00_12087 = BGL_OBJECT_WIDENING(BgL_tmpz00_12088);
						}
						BgL_auxz00_12086 =
							((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12087);
					}
					BgL_val1_1590z00_5708 =
						(((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12086))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 235 */
					int BgL_tmpz00_12094;

					BgL_tmpz00_12094 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12094);
				}
				{	/* Liveness/liveness.scm 235 */
					int BgL_tmpz00_12097;

					BgL_tmpz00_12097 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12097, BgL_val1_1590z00_5708);
				}
				return BgL_val0_1589z00_5707;
			}
		}

	}



/* &defuse-sequence1871 */
	obj_t BGl_z62defusezd2sequence1871zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4839, obj_t BgL_nz00_4840)
	{
		{	/* Liveness/liveness.scm 224 */
			{	/* Liveness/liveness.scm 226 */
				obj_t BgL_defz00_5710;

				BgL_defz00_5710 =
					BGl_defusezd2seqzd2zzliveness_livenessz00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nz00_4840)))->BgL_nodesz00));
				{	/* Liveness/liveness.scm 227 */
					obj_t BgL_usez00_5711;

					{	/* Liveness/liveness.scm 228 */
						obj_t BgL_tmpz00_5712;

						{	/* Liveness/liveness.scm 228 */
							int BgL_tmpz00_12103;

							BgL_tmpz00_12103 = (int) (1L);
							BgL_tmpz00_5712 = BGL_MVALUES_VAL(BgL_tmpz00_12103);
						}
						{	/* Liveness/liveness.scm 228 */
							int BgL_tmpz00_12106;

							BgL_tmpz00_12106 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_12106, BUNSPEC);
						}
						BgL_usez00_5711 = BgL_tmpz00_5712;
					}
					{	/* Liveness/liveness.scm 229 */
						BgL_sequencez00_bglt BgL_arg2174z00_5713;

						{	/* Liveness/liveness.scm 229 */
							BgL_sequencezf2livenesszf2_bglt BgL_wide1184z00_5714;

							BgL_wide1184z00_5714 =
								((BgL_sequencezf2livenesszf2_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sequencezf2livenesszf2_bgl))));
							{	/* Liveness/liveness.scm 229 */
								obj_t BgL_auxz00_12114;
								BgL_objectz00_bglt BgL_tmpz00_12110;

								BgL_auxz00_12114 = ((obj_t) BgL_wide1184z00_5714);
								BgL_tmpz00_12110 =
									((BgL_objectz00_bglt)
									((BgL_sequencez00_bglt)
										((BgL_sequencez00_bglt) BgL_nz00_4840)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12110, BgL_auxz00_12114);
							}
							((BgL_objectz00_bglt)
								((BgL_sequencez00_bglt)
									((BgL_sequencez00_bglt) BgL_nz00_4840)));
							{	/* Liveness/liveness.scm 229 */
								long BgL_arg2175z00_5715;

								BgL_arg2175z00_5715 =
									BGL_CLASS_NUM(BGl_sequencezf2livenesszf2zzliveness_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt)
											((BgL_sequencez00_bglt) BgL_nz00_4840))),
									BgL_arg2175z00_5715);
							}
							((BgL_sequencez00_bglt)
								((BgL_sequencez00_bglt)
									((BgL_sequencez00_bglt) BgL_nz00_4840)));
						}
						{
							BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12128;

							{
								obj_t BgL_auxz00_12129;

								{	/* Liveness/liveness.scm 230 */
									BgL_objectz00_bglt BgL_tmpz00_12130;

									BgL_tmpz00_12130 =
										((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt)
											((BgL_sequencez00_bglt) BgL_nz00_4840)));
									BgL_auxz00_12129 = BGL_OBJECT_WIDENING(BgL_tmpz00_12130);
								}
								BgL_auxz00_12128 =
									((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12129);
							}
							((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12128))->
									BgL_defz00) = ((obj_t) BgL_defz00_5710), BUNSPEC);
						}
						{
							BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12137;

							{
								obj_t BgL_auxz00_12138;

								{	/* Liveness/liveness.scm 231 */
									BgL_objectz00_bglt BgL_tmpz00_12139;

									BgL_tmpz00_12139 =
										((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt)
											((BgL_sequencez00_bglt) BgL_nz00_4840)));
									BgL_auxz00_12138 = BGL_OBJECT_WIDENING(BgL_tmpz00_12139);
								}
								BgL_auxz00_12137 =
									((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12138);
							}
							((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12137))->
									BgL_usez00) = ((obj_t) BgL_usez00_5711), BUNSPEC);
						}
						{
							BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12146;

							{
								obj_t BgL_auxz00_12147;

								{	/* Liveness/liveness.scm 227 */
									BgL_objectz00_bglt BgL_tmpz00_12148;

									BgL_tmpz00_12148 =
										((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt)
											((BgL_sequencez00_bglt) BgL_nz00_4840)));
									BgL_auxz00_12147 = BGL_OBJECT_WIDENING(BgL_tmpz00_12148);
								}
								BgL_auxz00_12146 =
									((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12147);
							}
							((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12146))->
									BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_sequencezf2livenesszf2_bglt BgL_auxz00_12155;

							{
								obj_t BgL_auxz00_12156;

								{	/* Liveness/liveness.scm 227 */
									BgL_objectz00_bglt BgL_tmpz00_12157;

									BgL_tmpz00_12157 =
										((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt)
											((BgL_sequencez00_bglt) BgL_nz00_4840)));
									BgL_auxz00_12156 = BGL_OBJECT_WIDENING(BgL_tmpz00_12157);
								}
								BgL_auxz00_12155 =
									((BgL_sequencezf2livenesszf2_bglt) BgL_auxz00_12156);
							}
							((((BgL_sequencezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12155))->
									BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
						}
						BgL_arg2174z00_5713 =
							((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_4840));
						return
							BGl_defusez00zzliveness_livenessz00(
							((BgL_nodez00_bglt) BgL_arg2174z00_5713));
					}
				}
			}
		}

	}



/* &inout-kwote/liveness1869 */
	obj_t BGl_z62inoutzd2kwotezf2liveness1869z42zzliveness_livenessz00(obj_t
		BgL_envz00_4841, obj_t BgL_nz00_4842)
	{
		{	/* Liveness/liveness.scm 217 */
			{	/* Liveness/liveness.scm 219 */
				obj_t BgL_val0_1587z00_5717;
				obj_t BgL_val1_1588z00_5718;

				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12168;

					{
						obj_t BgL_auxz00_12169;

						{	/* Liveness/liveness.scm 219 */
							BgL_objectz00_bglt BgL_tmpz00_12170;

							BgL_tmpz00_12170 =
								((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4842));
							BgL_auxz00_12169 = BGL_OBJECT_WIDENING(BgL_tmpz00_12170);
						}
						BgL_auxz00_12168 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12169);
					}
					BgL_val0_1587z00_5717 =
						(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12168))->
						BgL_inz00);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12176;

					{
						obj_t BgL_auxz00_12177;

						{	/* Liveness/liveness.scm 219 */
							BgL_objectz00_bglt BgL_tmpz00_12178;

							BgL_tmpz00_12178 =
								((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4842));
							BgL_auxz00_12177 = BGL_OBJECT_WIDENING(BgL_tmpz00_12178);
						}
						BgL_auxz00_12176 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12177);
					}
					BgL_val1_1588z00_5718 =
						(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12176))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 219 */
					int BgL_tmpz00_12184;

					BgL_tmpz00_12184 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12184);
				}
				{	/* Liveness/liveness.scm 219 */
					int BgL_tmpz00_12187;

					BgL_tmpz00_12187 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12187, BgL_val1_1588z00_5718);
				}
				return BgL_val0_1587z00_5717;
			}
		}

	}



/* &inout!-kwote/livenes1867 */
	obj_t BGl_z62inoutz12zd2kwotezf2livenes1867z50zzliveness_livenessz00(obj_t
		BgL_envz00_4843, obj_t BgL_nz00_4844, obj_t BgL_oz00_4845)
	{
		{	/* Liveness/liveness.scm 210 */
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12190;

				{
					obj_t BgL_auxz00_12191;

					{	/* Liveness/liveness.scm 212 */
						BgL_objectz00_bglt BgL_tmpz00_12192;

						BgL_tmpz00_12192 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4844));
						BgL_auxz00_12191 = BGL_OBJECT_WIDENING(BgL_tmpz00_12192);
					}
					BgL_auxz00_12190 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12191);
				}
				((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12190))->
						BgL_outz00) = ((obj_t) BgL_oz00_4845), BUNSPEC);
			}
			{
				BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12198;

				{
					obj_t BgL_auxz00_12199;

					{	/* Liveness/liveness.scm 213 */
						BgL_objectz00_bglt BgL_tmpz00_12200;

						BgL_tmpz00_12200 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4844));
						BgL_auxz00_12199 = BGL_OBJECT_WIDENING(BgL_tmpz00_12200);
					}
					BgL_auxz00_12198 = ((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12199);
				}
				((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12198))->
						BgL_inz00) = ((obj_t) BgL_oz00_4845), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 215 */
				obj_t BgL_val0_1585z00_5720;
				obj_t BgL_val1_1586z00_5721;

				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12206;

					{
						obj_t BgL_auxz00_12207;

						{	/* Liveness/liveness.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_12208;

							BgL_tmpz00_12208 =
								((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4844));
							BgL_auxz00_12207 = BGL_OBJECT_WIDENING(BgL_tmpz00_12208);
						}
						BgL_auxz00_12206 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12207);
					}
					BgL_val0_1585z00_5720 =
						(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12206))->
						BgL_inz00);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12214;

					{
						obj_t BgL_auxz00_12215;

						{	/* Liveness/liveness.scm 215 */
							BgL_objectz00_bglt BgL_tmpz00_12216;

							BgL_tmpz00_12216 =
								((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4844));
							BgL_auxz00_12215 = BGL_OBJECT_WIDENING(BgL_tmpz00_12216);
						}
						BgL_auxz00_12214 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12215);
					}
					BgL_val1_1586z00_5721 =
						(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12214))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 215 */
					int BgL_tmpz00_12222;

					BgL_tmpz00_12222 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12222);
				}
				{	/* Liveness/liveness.scm 215 */
					int BgL_tmpz00_12225;

					BgL_tmpz00_12225 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12225, BgL_val1_1586z00_5721);
				}
				return BgL_val0_1585z00_5720;
			}
		}

	}



/* &defuse-kwote/livenes1865 */
	obj_t BGl_z62defusezd2kwotezf2livenes1865z42zzliveness_livenessz00(obj_t
		BgL_envz00_4846, obj_t BgL_nz00_4847)
	{
		{	/* Liveness/liveness.scm 206 */
			{	/* Liveness/liveness.scm 208 */
				obj_t BgL_val0_1583z00_5723;
				obj_t BgL_val1_1584z00_5724;

				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12228;

					{
						obj_t BgL_auxz00_12229;

						{	/* Liveness/liveness.scm 208 */
							BgL_objectz00_bglt BgL_tmpz00_12230;

							BgL_tmpz00_12230 =
								((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4847));
							BgL_auxz00_12229 = BGL_OBJECT_WIDENING(BgL_tmpz00_12230);
						}
						BgL_auxz00_12228 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12229);
					}
					BgL_val0_1583z00_5723 =
						(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12228))->
						BgL_defz00);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12236;

					{
						obj_t BgL_auxz00_12237;

						{	/* Liveness/liveness.scm 208 */
							BgL_objectz00_bglt BgL_tmpz00_12238;

							BgL_tmpz00_12238 =
								((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4847));
							BgL_auxz00_12237 = BGL_OBJECT_WIDENING(BgL_tmpz00_12238);
						}
						BgL_auxz00_12236 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12237);
					}
					BgL_val1_1584z00_5724 =
						(((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12236))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 208 */
					int BgL_tmpz00_12244;

					BgL_tmpz00_12244 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12244);
				}
				{	/* Liveness/liveness.scm 208 */
					int BgL_tmpz00_12247;

					BgL_tmpz00_12247 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12247, BgL_val1_1584z00_5724);
				}
				return BgL_val0_1583z00_5723;
			}
		}

	}



/* &defuse-kwote1863 */
	obj_t BGl_z62defusezd2kwote1863zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4848, obj_t BgL_nz00_4849)
	{
		{	/* Liveness/liveness.scm 203 */
			{	/* Liveness/liveness.scm 204 */
				BgL_kwotez00_bglt BgL_arg2172z00_5726;

				{	/* Liveness/liveness.scm 204 */
					BgL_kwotezf2livenesszf2_bglt BgL_wide1176z00_5727;

					BgL_wide1176z00_5727 =
						((BgL_kwotezf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_kwotezf2livenesszf2_bgl))));
					{	/* Liveness/liveness.scm 204 */
						obj_t BgL_auxz00_12255;
						BgL_objectz00_bglt BgL_tmpz00_12251;

						BgL_auxz00_12255 = ((obj_t) BgL_wide1176z00_5727);
						BgL_tmpz00_12251 =
							((BgL_objectz00_bglt)
							((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4849)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12251, BgL_auxz00_12255);
					}
					((BgL_objectz00_bglt)
						((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4849)));
					{	/* Liveness/liveness.scm 204 */
						long BgL_arg2173z00_5728;

						BgL_arg2173z00_5728 =
							BGL_CLASS_NUM(BGl_kwotezf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt)
									((BgL_kwotez00_bglt) BgL_nz00_4849))), BgL_arg2173z00_5728);
					}
					((BgL_kwotez00_bglt)
						((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4849)));
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12269;

					{
						obj_t BgL_auxz00_12270;

						{	/* Liveness/liveness.scm 204 */
							BgL_objectz00_bglt BgL_tmpz00_12271;

							BgL_tmpz00_12271 =
								((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4849)));
							BgL_auxz00_12270 = BGL_OBJECT_WIDENING(BgL_tmpz00_12271);
						}
						BgL_auxz00_12269 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12270);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12269))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12278;

					{
						obj_t BgL_auxz00_12279;

						{	/* Liveness/liveness.scm 204 */
							BgL_objectz00_bglt BgL_tmpz00_12280;

							BgL_tmpz00_12280 =
								((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4849)));
							BgL_auxz00_12279 = BGL_OBJECT_WIDENING(BgL_tmpz00_12280);
						}
						BgL_auxz00_12278 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12279);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12278))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12287;

					{
						obj_t BgL_auxz00_12288;

						{	/* Liveness/liveness.scm 204 */
							BgL_objectz00_bglt BgL_tmpz00_12289;

							BgL_tmpz00_12289 =
								((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4849)));
							BgL_auxz00_12288 = BGL_OBJECT_WIDENING(BgL_tmpz00_12289);
						}
						BgL_auxz00_12287 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12288);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12287))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_kwotezf2livenesszf2_bglt BgL_auxz00_12296;

					{
						obj_t BgL_auxz00_12297;

						{	/* Liveness/liveness.scm 204 */
							BgL_objectz00_bglt BgL_tmpz00_12298;

							BgL_tmpz00_12298 =
								((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4849)));
							BgL_auxz00_12297 = BGL_OBJECT_WIDENING(BgL_tmpz00_12298);
						}
						BgL_auxz00_12296 =
							((BgL_kwotezf2livenesszf2_bglt) BgL_auxz00_12297);
					}
					((((BgL_kwotezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12296))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_arg2172z00_5726 =
					((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nz00_4849));
				return
					BGl_defusez00zzliveness_livenessz00(
					((BgL_nodez00_bglt) BgL_arg2172z00_5726));
			}
		}

	}



/* &inout-closure/livene1861 */
	obj_t BGl_z62inoutzd2closurezf2livene1861z42zzliveness_livenessz00(obj_t
		BgL_envz00_4850, obj_t BgL_nz00_4851)
	{
		{	/* Liveness/liveness.scm 196 */
			{	/* Liveness/liveness.scm 198 */
				obj_t BgL_val0_1581z00_5730;
				obj_t BgL_val1_1582z00_5731;

				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_12309;

					{
						obj_t BgL_auxz00_12310;

						{	/* Liveness/liveness.scm 198 */
							BgL_objectz00_bglt BgL_tmpz00_12311;

							BgL_tmpz00_12311 =
								((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4851));
							BgL_auxz00_12310 = BGL_OBJECT_WIDENING(BgL_tmpz00_12311);
						}
						BgL_auxz00_12309 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12310);
					}
					BgL_val0_1581z00_5730 =
						(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12309))->
						BgL_inz00);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_12317;

					{
						obj_t BgL_auxz00_12318;

						{	/* Liveness/liveness.scm 198 */
							BgL_objectz00_bglt BgL_tmpz00_12319;

							BgL_tmpz00_12319 =
								((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4851));
							BgL_auxz00_12318 = BGL_OBJECT_WIDENING(BgL_tmpz00_12319);
						}
						BgL_auxz00_12317 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12318);
					}
					BgL_val1_1582z00_5731 =
						(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12317))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 198 */
					int BgL_tmpz00_12325;

					BgL_tmpz00_12325 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12325);
				}
				{	/* Liveness/liveness.scm 198 */
					int BgL_tmpz00_12328;

					BgL_tmpz00_12328 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12328, BgL_val1_1582z00_5731);
				}
				return BgL_val0_1581z00_5730;
			}
		}

	}



/* &inout!-closure/liven1859 */
	obj_t BGl_z62inoutz12zd2closurezf2liven1859z50zzliveness_livenessz00(obj_t
		BgL_envz00_4852, obj_t BgL_nz00_4853, obj_t BgL_oz00_4854)
	{
		{	/* Liveness/liveness.scm 189 */
			{
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_12331;

				{
					obj_t BgL_auxz00_12332;

					{	/* Liveness/liveness.scm 191 */
						BgL_objectz00_bglt BgL_tmpz00_12333;

						BgL_tmpz00_12333 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4853));
						BgL_auxz00_12332 = BGL_OBJECT_WIDENING(BgL_tmpz00_12333);
					}
					BgL_auxz00_12331 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12332);
				}
				((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12331))->
						BgL_outz00) = ((obj_t) BgL_oz00_4854), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_12346;
				BgL_closurezf2livenesszf2_bglt BgL_auxz00_12339;

				{	/* Liveness/liveness.scm 193 */
					obj_t BgL_arg2170z00_5733;
					obj_t BgL_arg2171z00_5734;

					{
						BgL_closurezf2livenesszf2_bglt BgL_auxz00_12347;

						{
							obj_t BgL_auxz00_12348;

							{	/* Liveness/liveness.scm 193 */
								BgL_objectz00_bglt BgL_tmpz00_12349;

								BgL_tmpz00_12349 =
									((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4853));
								BgL_auxz00_12348 = BGL_OBJECT_WIDENING(BgL_tmpz00_12349);
							}
							BgL_auxz00_12347 =
								((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12348);
						}
						BgL_arg2170z00_5733 =
							(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12347))->
							BgL_usez00);
					}
					{
						BgL_closurezf2livenesszf2_bglt BgL_auxz00_12355;

						{
							obj_t BgL_auxz00_12356;

							{	/* Liveness/liveness.scm 193 */
								BgL_objectz00_bglt BgL_tmpz00_12357;

								BgL_tmpz00_12357 =
									((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4853));
								BgL_auxz00_12356 = BGL_OBJECT_WIDENING(BgL_tmpz00_12357);
							}
							BgL_auxz00_12355 =
								((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12356);
						}
						BgL_arg2171z00_5734 =
							(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12355))->
							BgL_outz00);
					}
					BgL_auxz00_12346 =
						BGl_unionz00zzliveness_setz00(BgL_arg2170z00_5733,
						BgL_arg2171z00_5734);
				}
				{
					obj_t BgL_auxz00_12340;

					{	/* Liveness/liveness.scm 193 */
						BgL_objectz00_bglt BgL_tmpz00_12341;

						BgL_tmpz00_12341 =
							((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4853));
						BgL_auxz00_12340 = BGL_OBJECT_WIDENING(BgL_tmpz00_12341);
					}
					BgL_auxz00_12339 =
						((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12340);
				}
				((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12339))->
						BgL_inz00) = ((obj_t) BgL_auxz00_12346), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 194 */
				obj_t BgL_val0_1579z00_5735;
				obj_t BgL_val1_1580z00_5736;

				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_12365;

					{
						obj_t BgL_auxz00_12366;

						{	/* Liveness/liveness.scm 194 */
							BgL_objectz00_bglt BgL_tmpz00_12367;

							BgL_tmpz00_12367 =
								((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4853));
							BgL_auxz00_12366 = BGL_OBJECT_WIDENING(BgL_tmpz00_12367);
						}
						BgL_auxz00_12365 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12366);
					}
					BgL_val0_1579z00_5735 =
						(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12365))->
						BgL_inz00);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_12373;

					{
						obj_t BgL_auxz00_12374;

						{	/* Liveness/liveness.scm 194 */
							BgL_objectz00_bglt BgL_tmpz00_12375;

							BgL_tmpz00_12375 =
								((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4853));
							BgL_auxz00_12374 = BGL_OBJECT_WIDENING(BgL_tmpz00_12375);
						}
						BgL_auxz00_12373 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12374);
					}
					BgL_val1_1580z00_5736 =
						(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12373))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 194 */
					int BgL_tmpz00_12381;

					BgL_tmpz00_12381 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12381);
				}
				{	/* Liveness/liveness.scm 194 */
					int BgL_tmpz00_12384;

					BgL_tmpz00_12384 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12384, BgL_val1_1580z00_5736);
				}
				return BgL_val0_1579z00_5735;
			}
		}

	}



/* &defuse-closure/liven1857 */
	obj_t BGl_z62defusezd2closurezf2liven1857z42zzliveness_livenessz00(obj_t
		BgL_envz00_4855, obj_t BgL_nz00_4856)
	{
		{	/* Liveness/liveness.scm 185 */
			{	/* Liveness/liveness.scm 187 */
				obj_t BgL_val0_1577z00_5738;
				obj_t BgL_val1_1578z00_5739;

				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_12387;

					{
						obj_t BgL_auxz00_12388;

						{	/* Liveness/liveness.scm 187 */
							BgL_objectz00_bglt BgL_tmpz00_12389;

							BgL_tmpz00_12389 =
								((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4856));
							BgL_auxz00_12388 = BGL_OBJECT_WIDENING(BgL_tmpz00_12389);
						}
						BgL_auxz00_12387 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12388);
					}
					BgL_val0_1577z00_5738 =
						(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12387))->
						BgL_defz00);
				}
				{
					BgL_closurezf2livenesszf2_bglt BgL_auxz00_12395;

					{
						obj_t BgL_auxz00_12396;

						{	/* Liveness/liveness.scm 187 */
							BgL_objectz00_bglt BgL_tmpz00_12397;

							BgL_tmpz00_12397 =
								((BgL_objectz00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4856));
							BgL_auxz00_12396 = BGL_OBJECT_WIDENING(BgL_tmpz00_12397);
						}
						BgL_auxz00_12395 =
							((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12396);
					}
					BgL_val1_1578z00_5739 =
						(((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12395))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 187 */
					int BgL_tmpz00_12403;

					BgL_tmpz00_12403 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12403);
				}
				{	/* Liveness/liveness.scm 187 */
					int BgL_tmpz00_12406;

					BgL_tmpz00_12406 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12406, BgL_val1_1578z00_5739);
				}
				return BgL_val0_1577z00_5738;
			}
		}

	}



/* &defuse-closure1855 */
	obj_t BGl_z62defusezd2closure1855zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4857, obj_t BgL_nz00_4858)
	{
		{	/* Liveness/liveness.scm 177 */
			{	/* Liveness/liveness.scm 179 */
				obj_t BgL_usez00_5741;

				{	/* Liveness/liveness.scm 179 */
					bool_t BgL_test3002z00_12409;

					{	/* Liveness/liveness.scm 179 */
						BgL_variablez00_bglt BgL_arg2169z00_5742;

						BgL_arg2169z00_5742 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt)
										((BgL_closurez00_bglt) BgL_nz00_4858))))->BgL_variablez00);
						{	/* Liveness/liveness.scm 179 */
							obj_t BgL_classz00_5743;

							BgL_classz00_5743 = BGl_localz00zzast_varz00;
							{	/* Liveness/liveness.scm 179 */
								BgL_objectz00_bglt BgL_arg1807z00_5744;

								{	/* Liveness/liveness.scm 179 */
									obj_t BgL_tmpz00_12413;

									BgL_tmpz00_12413 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg2169z00_5742));
									BgL_arg1807z00_5744 = (BgL_objectz00_bglt) (BgL_tmpz00_12413);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Liveness/liveness.scm 179 */
										long BgL_idxz00_5745;

										BgL_idxz00_5745 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5744);
										BgL_test3002z00_12409 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5745 + 2L)) == BgL_classz00_5743);
									}
								else
									{	/* Liveness/liveness.scm 179 */
										bool_t BgL_res2665z00_5748;

										{	/* Liveness/liveness.scm 179 */
											obj_t BgL_oclassz00_5749;

											{	/* Liveness/liveness.scm 179 */
												obj_t BgL_arg1815z00_5750;
												long BgL_arg1816z00_5751;

												BgL_arg1815z00_5750 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Liveness/liveness.scm 179 */
													long BgL_arg1817z00_5752;

													BgL_arg1817z00_5752 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5744);
													BgL_arg1816z00_5751 =
														(BgL_arg1817z00_5752 - OBJECT_TYPE);
												}
												BgL_oclassz00_5749 =
													VECTOR_REF(BgL_arg1815z00_5750, BgL_arg1816z00_5751);
											}
											{	/* Liveness/liveness.scm 179 */
												bool_t BgL__ortest_1115z00_5753;

												BgL__ortest_1115z00_5753 =
													(BgL_classz00_5743 == BgL_oclassz00_5749);
												if (BgL__ortest_1115z00_5753)
													{	/* Liveness/liveness.scm 179 */
														BgL_res2665z00_5748 = BgL__ortest_1115z00_5753;
													}
												else
													{	/* Liveness/liveness.scm 179 */
														long BgL_odepthz00_5754;

														{	/* Liveness/liveness.scm 179 */
															obj_t BgL_arg1804z00_5755;

															BgL_arg1804z00_5755 = (BgL_oclassz00_5749);
															BgL_odepthz00_5754 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5755);
														}
														if ((2L < BgL_odepthz00_5754))
															{	/* Liveness/liveness.scm 179 */
																obj_t BgL_arg1802z00_5756;

																{	/* Liveness/liveness.scm 179 */
																	obj_t BgL_arg1803z00_5757;

																	BgL_arg1803z00_5757 = (BgL_oclassz00_5749);
																	BgL_arg1802z00_5756 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5757,
																		2L);
																}
																BgL_res2665z00_5748 =
																	(BgL_arg1802z00_5756 == BgL_classz00_5743);
															}
														else
															{	/* Liveness/liveness.scm 179 */
																BgL_res2665z00_5748 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3002z00_12409 = BgL_res2665z00_5748;
									}
							}
						}
					}
					if (BgL_test3002z00_12409)
						{	/* Liveness/liveness.scm 179 */
							BgL_variablez00_bglt BgL_arg2167z00_5758;

							BgL_arg2167z00_5758 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt)
											((BgL_closurez00_bglt) BgL_nz00_4858))))->
								BgL_variablez00);
							{	/* Liveness/liveness.scm 179 */
								obj_t BgL_list2168z00_5759;

								BgL_list2168z00_5759 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg2167z00_5758), BNIL);
								BgL_usez00_5741 = BgL_list2168z00_5759;
							}
						}
					else
						{	/* Liveness/liveness.scm 179 */
							BgL_usez00_5741 = BNIL;
						}
				}
				{	/* Liveness/liveness.scm 181 */
					BgL_closurez00_bglt BgL_arg2163z00_5760;

					{	/* Liveness/liveness.scm 181 */
						BgL_closurezf2livenesszf2_bglt BgL_wide1169z00_5761;

						BgL_wide1169z00_5761 =
							((BgL_closurezf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_closurezf2livenesszf2_bgl))));
						{	/* Liveness/liveness.scm 181 */
							obj_t BgL_auxz00_12446;
							BgL_objectz00_bglt BgL_tmpz00_12442;

							BgL_auxz00_12446 = ((obj_t) BgL_wide1169z00_5761);
							BgL_tmpz00_12442 =
								((BgL_objectz00_bglt)
								((BgL_closurez00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4858)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12442, BgL_auxz00_12446);
						}
						((BgL_objectz00_bglt)
							((BgL_closurez00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4858)));
						{	/* Liveness/liveness.scm 181 */
							long BgL_arg2164z00_5762;

							BgL_arg2164z00_5762 =
								BGL_CLASS_NUM(BGl_closurezf2livenesszf2zzliveness_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_closurez00_bglt)
										((BgL_closurez00_bglt) BgL_nz00_4858))),
								BgL_arg2164z00_5762);
						}
						((BgL_closurez00_bglt)
							((BgL_closurez00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4858)));
					}
					{
						BgL_closurezf2livenesszf2_bglt BgL_auxz00_12460;

						{
							obj_t BgL_auxz00_12461;

							{	/* Liveness/liveness.scm 182 */
								BgL_objectz00_bglt BgL_tmpz00_12462;

								BgL_tmpz00_12462 =
									((BgL_objectz00_bglt)
									((BgL_closurez00_bglt)
										((BgL_closurez00_bglt) BgL_nz00_4858)));
								BgL_auxz00_12461 = BGL_OBJECT_WIDENING(BgL_tmpz00_12462);
							}
							BgL_auxz00_12460 =
								((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12461);
						}
						((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12460))->
								BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
					}
					{
						BgL_closurezf2livenesszf2_bglt BgL_auxz00_12469;

						{
							obj_t BgL_auxz00_12470;

							{	/* Liveness/liveness.scm 183 */
								BgL_objectz00_bglt BgL_tmpz00_12471;

								BgL_tmpz00_12471 =
									((BgL_objectz00_bglt)
									((BgL_closurez00_bglt)
										((BgL_closurez00_bglt) BgL_nz00_4858)));
								BgL_auxz00_12470 = BGL_OBJECT_WIDENING(BgL_tmpz00_12471);
							}
							BgL_auxz00_12469 =
								((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12470);
						}
						((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12469))->
								BgL_usez00) = ((obj_t) BgL_usez00_5741), BUNSPEC);
					}
					{
						BgL_closurezf2livenesszf2_bglt BgL_auxz00_12478;

						{
							obj_t BgL_auxz00_12479;

							{	/* Liveness/liveness.scm 179 */
								BgL_objectz00_bglt BgL_tmpz00_12480;

								BgL_tmpz00_12480 =
									((BgL_objectz00_bglt)
									((BgL_closurez00_bglt)
										((BgL_closurez00_bglt) BgL_nz00_4858)));
								BgL_auxz00_12479 = BGL_OBJECT_WIDENING(BgL_tmpz00_12480);
							}
							BgL_auxz00_12478 =
								((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12479);
						}
						((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12478))->
								BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
					}
					{
						BgL_closurezf2livenesszf2_bglt BgL_auxz00_12487;

						{
							obj_t BgL_auxz00_12488;

							{	/* Liveness/liveness.scm 179 */
								BgL_objectz00_bglt BgL_tmpz00_12489;

								BgL_tmpz00_12489 =
									((BgL_objectz00_bglt)
									((BgL_closurez00_bglt)
										((BgL_closurez00_bglt) BgL_nz00_4858)));
								BgL_auxz00_12488 = BGL_OBJECT_WIDENING(BgL_tmpz00_12489);
							}
							BgL_auxz00_12487 =
								((BgL_closurezf2livenesszf2_bglt) BgL_auxz00_12488);
						}
						((((BgL_closurezf2livenesszf2_bglt) COBJECT(BgL_auxz00_12487))->
								BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
					}
					BgL_arg2163z00_5760 =
						((BgL_closurez00_bglt) ((BgL_closurez00_bglt) BgL_nz00_4858));
					return
						BGl_defusez00zzliveness_livenessz00(
						((BgL_nodez00_bglt) BgL_arg2163z00_5760));
				}
			}
		}

	}



/* &inout-ref/liveness1853 */
	obj_t BGl_z62inoutzd2refzf2liveness1853z42zzliveness_livenessz00(obj_t
		BgL_envz00_4859, obj_t BgL_nz00_4860)
	{
		{	/* Liveness/liveness.scm 170 */
			{	/* Liveness/liveness.scm 172 */
				obj_t BgL_val0_1575z00_5764;
				obj_t BgL_val1_1576z00_5765;

				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_12500;

					{
						obj_t BgL_auxz00_12501;

						{	/* Liveness/liveness.scm 172 */
							BgL_objectz00_bglt BgL_tmpz00_12502;

							BgL_tmpz00_12502 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4860));
							BgL_auxz00_12501 = BGL_OBJECT_WIDENING(BgL_tmpz00_12502);
						}
						BgL_auxz00_12500 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12501);
					}
					BgL_val0_1575z00_5764 =
						(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12500))->
						BgL_inz00);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_12508;

					{
						obj_t BgL_auxz00_12509;

						{	/* Liveness/liveness.scm 172 */
							BgL_objectz00_bglt BgL_tmpz00_12510;

							BgL_tmpz00_12510 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4860));
							BgL_auxz00_12509 = BGL_OBJECT_WIDENING(BgL_tmpz00_12510);
						}
						BgL_auxz00_12508 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12509);
					}
					BgL_val1_1576z00_5765 =
						(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12508))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 172 */
					int BgL_tmpz00_12516;

					BgL_tmpz00_12516 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12516);
				}
				{	/* Liveness/liveness.scm 172 */
					int BgL_tmpz00_12519;

					BgL_tmpz00_12519 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12519, BgL_val1_1576z00_5765);
				}
				return BgL_val0_1575z00_5764;
			}
		}

	}



/* &inout!-ref/liveness1851 */
	obj_t BGl_z62inoutz12zd2refzf2liveness1851z50zzliveness_livenessz00(obj_t
		BgL_envz00_4861, obj_t BgL_nz00_4862, obj_t BgL_oz00_4863)
	{
		{	/* Liveness/liveness.scm 163 */
			{
				BgL_refzf2livenesszf2_bglt BgL_auxz00_12522;

				{
					obj_t BgL_auxz00_12523;

					{	/* Liveness/liveness.scm 165 */
						BgL_objectz00_bglt BgL_tmpz00_12524;

						BgL_tmpz00_12524 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4862));
						BgL_auxz00_12523 = BGL_OBJECT_WIDENING(BgL_tmpz00_12524);
					}
					BgL_auxz00_12522 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12523);
				}
				((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12522))->
						BgL_outz00) = ((obj_t) BgL_oz00_4863), BUNSPEC);
			}
			{
				obj_t BgL_auxz00_12537;
				BgL_refzf2livenesszf2_bglt BgL_auxz00_12530;

				{	/* Liveness/liveness.scm 167 */
					obj_t BgL_arg2161z00_5767;
					obj_t BgL_arg2162z00_5768;

					{
						BgL_refzf2livenesszf2_bglt BgL_auxz00_12538;

						{
							obj_t BgL_auxz00_12539;

							{	/* Liveness/liveness.scm 167 */
								BgL_objectz00_bglt BgL_tmpz00_12540;

								BgL_tmpz00_12540 =
									((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4862));
								BgL_auxz00_12539 = BGL_OBJECT_WIDENING(BgL_tmpz00_12540);
							}
							BgL_auxz00_12538 =
								((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12539);
						}
						BgL_arg2161z00_5767 =
							(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12538))->
							BgL_usez00);
					}
					{
						BgL_refzf2livenesszf2_bglt BgL_auxz00_12546;

						{
							obj_t BgL_auxz00_12547;

							{	/* Liveness/liveness.scm 167 */
								BgL_objectz00_bglt BgL_tmpz00_12548;

								BgL_tmpz00_12548 =
									((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4862));
								BgL_auxz00_12547 = BGL_OBJECT_WIDENING(BgL_tmpz00_12548);
							}
							BgL_auxz00_12546 =
								((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12547);
						}
						BgL_arg2162z00_5768 =
							(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12546))->
							BgL_outz00);
					}
					BgL_auxz00_12537 =
						BGl_unionz00zzliveness_setz00(BgL_arg2161z00_5767,
						BgL_arg2162z00_5768);
				}
				{
					obj_t BgL_auxz00_12531;

					{	/* Liveness/liveness.scm 167 */
						BgL_objectz00_bglt BgL_tmpz00_12532;

						BgL_tmpz00_12532 =
							((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4862));
						BgL_auxz00_12531 = BGL_OBJECT_WIDENING(BgL_tmpz00_12532);
					}
					BgL_auxz00_12530 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12531);
				}
				((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12530))->BgL_inz00) =
					((obj_t) BgL_auxz00_12537), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 168 */
				obj_t BgL_val0_1573z00_5769;
				obj_t BgL_val1_1574z00_5770;

				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_12556;

					{
						obj_t BgL_auxz00_12557;

						{	/* Liveness/liveness.scm 168 */
							BgL_objectz00_bglt BgL_tmpz00_12558;

							BgL_tmpz00_12558 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4862));
							BgL_auxz00_12557 = BGL_OBJECT_WIDENING(BgL_tmpz00_12558);
						}
						BgL_auxz00_12556 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12557);
					}
					BgL_val0_1573z00_5769 =
						(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12556))->
						BgL_inz00);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_12564;

					{
						obj_t BgL_auxz00_12565;

						{	/* Liveness/liveness.scm 168 */
							BgL_objectz00_bglt BgL_tmpz00_12566;

							BgL_tmpz00_12566 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4862));
							BgL_auxz00_12565 = BGL_OBJECT_WIDENING(BgL_tmpz00_12566);
						}
						BgL_auxz00_12564 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12565);
					}
					BgL_val1_1574z00_5770 =
						(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12564))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 168 */
					int BgL_tmpz00_12572;

					BgL_tmpz00_12572 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12572);
				}
				{	/* Liveness/liveness.scm 168 */
					int BgL_tmpz00_12575;

					BgL_tmpz00_12575 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12575, BgL_val1_1574z00_5770);
				}
				return BgL_val0_1573z00_5769;
			}
		}

	}



/* &defuse-ref/liveness1849 */
	obj_t BGl_z62defusezd2refzf2liveness1849z42zzliveness_livenessz00(obj_t
		BgL_envz00_4864, obj_t BgL_nz00_4865)
	{
		{	/* Liveness/liveness.scm 159 */
			{	/* Liveness/liveness.scm 161 */
				obj_t BgL_val0_1571z00_5772;
				obj_t BgL_val1_1572z00_5773;

				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_12578;

					{
						obj_t BgL_auxz00_12579;

						{	/* Liveness/liveness.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_12580;

							BgL_tmpz00_12580 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4865));
							BgL_auxz00_12579 = BGL_OBJECT_WIDENING(BgL_tmpz00_12580);
						}
						BgL_auxz00_12578 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12579);
					}
					BgL_val0_1571z00_5772 =
						(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12578))->
						BgL_defz00);
				}
				{
					BgL_refzf2livenesszf2_bglt BgL_auxz00_12586;

					{
						obj_t BgL_auxz00_12587;

						{	/* Liveness/liveness.scm 161 */
							BgL_objectz00_bglt BgL_tmpz00_12588;

							BgL_tmpz00_12588 =
								((BgL_objectz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4865));
							BgL_auxz00_12587 = BGL_OBJECT_WIDENING(BgL_tmpz00_12588);
						}
						BgL_auxz00_12586 = ((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12587);
					}
					BgL_val1_1572z00_5773 =
						(((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12586))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 161 */
					int BgL_tmpz00_12594;

					BgL_tmpz00_12594 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12594);
				}
				{	/* Liveness/liveness.scm 161 */
					int BgL_tmpz00_12597;

					BgL_tmpz00_12597 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12597, BgL_val1_1572z00_5773);
				}
				return BgL_val0_1571z00_5772;
			}
		}

	}



/* &defuse-ref1847 */
	obj_t BGl_z62defusezd2ref1847zb0zzliveness_livenessz00(obj_t BgL_envz00_4866,
		obj_t BgL_nz00_4867)
	{
		{	/* Liveness/liveness.scm 151 */
			{	/* Liveness/liveness.scm 153 */
				obj_t BgL_usez00_5775;

				{	/* Liveness/liveness.scm 153 */
					bool_t BgL_test3006z00_12600;

					{	/* Liveness/liveness.scm 153 */
						BgL_variablez00_bglt BgL_arg2160z00_5776;

						BgL_arg2160z00_5776 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt)
										((BgL_refz00_bglt) BgL_nz00_4867))))->BgL_variablez00);
						{	/* Liveness/liveness.scm 153 */
							obj_t BgL_classz00_5777;

							BgL_classz00_5777 = BGl_localz00zzast_varz00;
							{	/* Liveness/liveness.scm 153 */
								BgL_objectz00_bglt BgL_arg1807z00_5778;

								{	/* Liveness/liveness.scm 153 */
									obj_t BgL_tmpz00_12604;

									BgL_tmpz00_12604 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg2160z00_5776));
									BgL_arg1807z00_5778 = (BgL_objectz00_bglt) (BgL_tmpz00_12604);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Liveness/liveness.scm 153 */
										long BgL_idxz00_5779;

										BgL_idxz00_5779 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5778);
										BgL_test3006z00_12600 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5779 + 2L)) == BgL_classz00_5777);
									}
								else
									{	/* Liveness/liveness.scm 153 */
										bool_t BgL_res2663z00_5782;

										{	/* Liveness/liveness.scm 153 */
											obj_t BgL_oclassz00_5783;

											{	/* Liveness/liveness.scm 153 */
												obj_t BgL_arg1815z00_5784;
												long BgL_arg1816z00_5785;

												BgL_arg1815z00_5784 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Liveness/liveness.scm 153 */
													long BgL_arg1817z00_5786;

													BgL_arg1817z00_5786 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5778);
													BgL_arg1816z00_5785 =
														(BgL_arg1817z00_5786 - OBJECT_TYPE);
												}
												BgL_oclassz00_5783 =
													VECTOR_REF(BgL_arg1815z00_5784, BgL_arg1816z00_5785);
											}
											{	/* Liveness/liveness.scm 153 */
												bool_t BgL__ortest_1115z00_5787;

												BgL__ortest_1115z00_5787 =
													(BgL_classz00_5777 == BgL_oclassz00_5783);
												if (BgL__ortest_1115z00_5787)
													{	/* Liveness/liveness.scm 153 */
														BgL_res2663z00_5782 = BgL__ortest_1115z00_5787;
													}
												else
													{	/* Liveness/liveness.scm 153 */
														long BgL_odepthz00_5788;

														{	/* Liveness/liveness.scm 153 */
															obj_t BgL_arg1804z00_5789;

															BgL_arg1804z00_5789 = (BgL_oclassz00_5783);
															BgL_odepthz00_5788 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5789);
														}
														if ((2L < BgL_odepthz00_5788))
															{	/* Liveness/liveness.scm 153 */
																obj_t BgL_arg1802z00_5790;

																{	/* Liveness/liveness.scm 153 */
																	obj_t BgL_arg1803z00_5791;

																	BgL_arg1803z00_5791 = (BgL_oclassz00_5783);
																	BgL_arg1802z00_5790 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5791,
																		2L);
																}
																BgL_res2663z00_5782 =
																	(BgL_arg1802z00_5790 == BgL_classz00_5777);
															}
														else
															{	/* Liveness/liveness.scm 153 */
																BgL_res2663z00_5782 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3006z00_12600 = BgL_res2663z00_5782;
									}
							}
						}
					}
					if (BgL_test3006z00_12600)
						{	/* Liveness/liveness.scm 153 */
							BgL_variablez00_bglt BgL_arg2158z00_5792;

							BgL_arg2158z00_5792 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt)
											((BgL_refz00_bglt) BgL_nz00_4867))))->BgL_variablez00);
							{	/* Liveness/liveness.scm 153 */
								obj_t BgL_list2159z00_5793;

								BgL_list2159z00_5793 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg2158z00_5792), BNIL);
								BgL_usez00_5775 = BgL_list2159z00_5793;
							}
						}
					else
						{	/* Liveness/liveness.scm 153 */
							BgL_usez00_5775 = BNIL;
						}
				}
				{	/* Liveness/liveness.scm 155 */
					BgL_refz00_bglt BgL_arg2154z00_5794;

					{	/* Liveness/liveness.scm 155 */
						BgL_refzf2livenesszf2_bglt BgL_wide1161z00_5795;

						BgL_wide1161z00_5795 =
							((BgL_refzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_refzf2livenesszf2_bgl))));
						{	/* Liveness/liveness.scm 155 */
							obj_t BgL_auxz00_12637;
							BgL_objectz00_bglt BgL_tmpz00_12633;

							BgL_auxz00_12637 = ((obj_t) BgL_wide1161z00_5795);
							BgL_tmpz00_12633 =
								((BgL_objectz00_bglt)
								((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4867)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12633, BgL_auxz00_12637);
						}
						((BgL_objectz00_bglt)
							((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4867)));
						{	/* Liveness/liveness.scm 155 */
							long BgL_arg2155z00_5796;

							BgL_arg2155z00_5796 =
								BGL_CLASS_NUM(BGl_refzf2livenesszf2zzliveness_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_refz00_bglt)
										((BgL_refz00_bglt) BgL_nz00_4867))), BgL_arg2155z00_5796);
						}
						((BgL_refz00_bglt)
							((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4867)));
					}
					{
						BgL_refzf2livenesszf2_bglt BgL_auxz00_12651;

						{
							obj_t BgL_auxz00_12652;

							{	/* Liveness/liveness.scm 156 */
								BgL_objectz00_bglt BgL_tmpz00_12653;

								BgL_tmpz00_12653 =
									((BgL_objectz00_bglt)
									((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4867)));
								BgL_auxz00_12652 = BGL_OBJECT_WIDENING(BgL_tmpz00_12653);
							}
							BgL_auxz00_12651 =
								((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12652);
						}
						((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12651))->
								BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
					}
					{
						BgL_refzf2livenesszf2_bglt BgL_auxz00_12660;

						{
							obj_t BgL_auxz00_12661;

							{	/* Liveness/liveness.scm 157 */
								BgL_objectz00_bglt BgL_tmpz00_12662;

								BgL_tmpz00_12662 =
									((BgL_objectz00_bglt)
									((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4867)));
								BgL_auxz00_12661 = BGL_OBJECT_WIDENING(BgL_tmpz00_12662);
							}
							BgL_auxz00_12660 =
								((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12661);
						}
						((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12660))->
								BgL_usez00) = ((obj_t) BgL_usez00_5775), BUNSPEC);
					}
					{
						BgL_refzf2livenesszf2_bglt BgL_auxz00_12669;

						{
							obj_t BgL_auxz00_12670;

							{	/* Liveness/liveness.scm 153 */
								BgL_objectz00_bglt BgL_tmpz00_12671;

								BgL_tmpz00_12671 =
									((BgL_objectz00_bglt)
									((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4867)));
								BgL_auxz00_12670 = BGL_OBJECT_WIDENING(BgL_tmpz00_12671);
							}
							BgL_auxz00_12669 =
								((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12670);
						}
						((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12669))->
								BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
					}
					{
						BgL_refzf2livenesszf2_bglt BgL_auxz00_12678;

						{
							obj_t BgL_auxz00_12679;

							{	/* Liveness/liveness.scm 153 */
								BgL_objectz00_bglt BgL_tmpz00_12680;

								BgL_tmpz00_12680 =
									((BgL_objectz00_bglt)
									((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4867)));
								BgL_auxz00_12679 = BGL_OBJECT_WIDENING(BgL_tmpz00_12680);
							}
							BgL_auxz00_12678 =
								((BgL_refzf2livenesszf2_bglt) BgL_auxz00_12679);
						}
						((((BgL_refzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12678))->
								BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
					}
					BgL_arg2154z00_5794 =
						((BgL_refz00_bglt) ((BgL_refz00_bglt) BgL_nz00_4867));
					return
						BGl_defusez00zzliveness_livenessz00(
						((BgL_nodez00_bglt) BgL_arg2154z00_5794));
				}
			}
		}

	}



/* &inout-literal/livene1845 */
	obj_t BGl_z62inoutzd2literalzf2livene1845z42zzliveness_livenessz00(obj_t
		BgL_envz00_4868, obj_t BgL_nz00_4869)
	{
		{	/* Liveness/liveness.scm 144 */
			{	/* Liveness/liveness.scm 146 */
				obj_t BgL_val0_1569z00_5798;
				obj_t BgL_val1_1570z00_5799;

				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12691;

					{
						obj_t BgL_auxz00_12692;

						{	/* Liveness/liveness.scm 146 */
							BgL_objectz00_bglt BgL_tmpz00_12693;

							BgL_tmpz00_12693 =
								((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4869));
							BgL_auxz00_12692 = BGL_OBJECT_WIDENING(BgL_tmpz00_12693);
						}
						BgL_auxz00_12691 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12692);
					}
					BgL_val0_1569z00_5798 =
						(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12691))->
						BgL_inz00);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12699;

					{
						obj_t BgL_auxz00_12700;

						{	/* Liveness/liveness.scm 146 */
							BgL_objectz00_bglt BgL_tmpz00_12701;

							BgL_tmpz00_12701 =
								((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4869));
							BgL_auxz00_12700 = BGL_OBJECT_WIDENING(BgL_tmpz00_12701);
						}
						BgL_auxz00_12699 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12700);
					}
					BgL_val1_1570z00_5799 =
						(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12699))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 146 */
					int BgL_tmpz00_12707;

					BgL_tmpz00_12707 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12707);
				}
				{	/* Liveness/liveness.scm 146 */
					int BgL_tmpz00_12710;

					BgL_tmpz00_12710 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12710, BgL_val1_1570z00_5799);
				}
				return BgL_val0_1569z00_5798;
			}
		}

	}



/* &inout!-literal/liven1843 */
	obj_t BGl_z62inoutz12zd2literalzf2liven1843z50zzliveness_livenessz00(obj_t
		BgL_envz00_4870, obj_t BgL_nz00_4871, obj_t BgL_oz00_4872)
	{
		{	/* Liveness/liveness.scm 137 */
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_12713;

				{
					obj_t BgL_auxz00_12714;

					{	/* Liveness/liveness.scm 139 */
						BgL_objectz00_bglt BgL_tmpz00_12715;

						BgL_tmpz00_12715 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4871));
						BgL_auxz00_12714 = BGL_OBJECT_WIDENING(BgL_tmpz00_12715);
					}
					BgL_auxz00_12713 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12714);
				}
				((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12713))->
						BgL_outz00) = ((obj_t) BgL_oz00_4872), BUNSPEC);
			}
			{
				BgL_literalzf2livenesszf2_bglt BgL_auxz00_12721;

				{
					obj_t BgL_auxz00_12722;

					{	/* Liveness/liveness.scm 140 */
						BgL_objectz00_bglt BgL_tmpz00_12723;

						BgL_tmpz00_12723 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4871));
						BgL_auxz00_12722 = BGL_OBJECT_WIDENING(BgL_tmpz00_12723);
					}
					BgL_auxz00_12721 =
						((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12722);
				}
				((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12721))->
						BgL_inz00) = ((obj_t) BgL_oz00_4872), BUNSPEC);
			}
			{	/* Liveness/liveness.scm 142 */
				obj_t BgL_val0_1567z00_5801;
				obj_t BgL_val1_1568z00_5802;

				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12729;

					{
						obj_t BgL_auxz00_12730;

						{	/* Liveness/liveness.scm 142 */
							BgL_objectz00_bglt BgL_tmpz00_12731;

							BgL_tmpz00_12731 =
								((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4871));
							BgL_auxz00_12730 = BGL_OBJECT_WIDENING(BgL_tmpz00_12731);
						}
						BgL_auxz00_12729 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12730);
					}
					BgL_val0_1567z00_5801 =
						(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12729))->
						BgL_inz00);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12737;

					{
						obj_t BgL_auxz00_12738;

						{	/* Liveness/liveness.scm 142 */
							BgL_objectz00_bglt BgL_tmpz00_12739;

							BgL_tmpz00_12739 =
								((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4871));
							BgL_auxz00_12738 = BGL_OBJECT_WIDENING(BgL_tmpz00_12739);
						}
						BgL_auxz00_12737 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12738);
					}
					BgL_val1_1568z00_5802 =
						(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12737))->
						BgL_outz00);
				}
				{	/* Liveness/liveness.scm 142 */
					int BgL_tmpz00_12745;

					BgL_tmpz00_12745 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12745);
				}
				{	/* Liveness/liveness.scm 142 */
					int BgL_tmpz00_12748;

					BgL_tmpz00_12748 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12748, BgL_val1_1568z00_5802);
				}
				return BgL_val0_1567z00_5801;
			}
		}

	}



/* &defuse-literal/liven1841 */
	obj_t BGl_z62defusezd2literalzf2liven1841z42zzliveness_livenessz00(obj_t
		BgL_envz00_4873, obj_t BgL_nz00_4874)
	{
		{	/* Liveness/liveness.scm 133 */
			{	/* Liveness/liveness.scm 135 */
				obj_t BgL_val0_1565z00_5804;
				obj_t BgL_val1_1566z00_5805;

				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12751;

					{
						obj_t BgL_auxz00_12752;

						{	/* Liveness/liveness.scm 135 */
							BgL_objectz00_bglt BgL_tmpz00_12753;

							BgL_tmpz00_12753 =
								((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4874));
							BgL_auxz00_12752 = BGL_OBJECT_WIDENING(BgL_tmpz00_12753);
						}
						BgL_auxz00_12751 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12752);
					}
					BgL_val0_1565z00_5804 =
						(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12751))->
						BgL_defz00);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12759;

					{
						obj_t BgL_auxz00_12760;

						{	/* Liveness/liveness.scm 135 */
							BgL_objectz00_bglt BgL_tmpz00_12761;

							BgL_tmpz00_12761 =
								((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4874));
							BgL_auxz00_12760 = BGL_OBJECT_WIDENING(BgL_tmpz00_12761);
						}
						BgL_auxz00_12759 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12760);
					}
					BgL_val1_1566z00_5805 =
						(((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12759))->
						BgL_usez00);
				}
				{	/* Liveness/liveness.scm 135 */
					int BgL_tmpz00_12767;

					BgL_tmpz00_12767 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12767);
				}
				{	/* Liveness/liveness.scm 135 */
					int BgL_tmpz00_12770;

					BgL_tmpz00_12770 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_12770, BgL_val1_1566z00_5805);
				}
				return BgL_val0_1565z00_5804;
			}
		}

	}



/* &defuse-literal1839 */
	obj_t BGl_z62defusezd2literal1839zb0zzliveness_livenessz00(obj_t
		BgL_envz00_4875, obj_t BgL_nz00_4876)
	{
		{	/* Liveness/liveness.scm 130 */
			{	/* Liveness/liveness.scm 131 */
				BgL_literalz00_bglt BgL_arg2151z00_5807;

				{	/* Liveness/liveness.scm 131 */
					BgL_literalzf2livenesszf2_bglt BgL_wide1153z00_5808;

					BgL_wide1153z00_5808 =
						((BgL_literalzf2livenesszf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_literalzf2livenesszf2_bgl))));
					{	/* Liveness/liveness.scm 131 */
						obj_t BgL_auxz00_12778;
						BgL_objectz00_bglt BgL_tmpz00_12774;

						BgL_auxz00_12778 = ((obj_t) BgL_wide1153z00_5808);
						BgL_tmpz00_12774 =
							((BgL_objectz00_bglt)
							((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4876)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12774, BgL_auxz00_12778);
					}
					((BgL_objectz00_bglt)
						((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4876)));
					{	/* Liveness/liveness.scm 131 */
						long BgL_arg2152z00_5809;

						BgL_arg2152z00_5809 =
							BGL_CLASS_NUM(BGl_literalzf2livenesszf2zzliveness_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_literalz00_bglt)
									((BgL_literalz00_bglt) BgL_nz00_4876))), BgL_arg2152z00_5809);
					}
					((BgL_literalz00_bglt)
						((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4876)));
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12792;

					{
						obj_t BgL_auxz00_12793;

						{	/* Liveness/liveness.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_12794;

							BgL_tmpz00_12794 =
								((BgL_objectz00_bglt)
								((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4876)));
							BgL_auxz00_12793 = BGL_OBJECT_WIDENING(BgL_tmpz00_12794);
						}
						BgL_auxz00_12792 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12793);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12792))->
							BgL_defz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12801;

					{
						obj_t BgL_auxz00_12802;

						{	/* Liveness/liveness.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_12803;

							BgL_tmpz00_12803 =
								((BgL_objectz00_bglt)
								((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4876)));
							BgL_auxz00_12802 = BGL_OBJECT_WIDENING(BgL_tmpz00_12803);
						}
						BgL_auxz00_12801 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12802);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12801))->
							BgL_usez00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12810;

					{
						obj_t BgL_auxz00_12811;

						{	/* Liveness/liveness.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_12812;

							BgL_tmpz00_12812 =
								((BgL_objectz00_bglt)
								((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4876)));
							BgL_auxz00_12811 = BGL_OBJECT_WIDENING(BgL_tmpz00_12812);
						}
						BgL_auxz00_12810 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12811);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12810))->
							BgL_inz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_literalzf2livenesszf2_bglt BgL_auxz00_12819;

					{
						obj_t BgL_auxz00_12820;

						{	/* Liveness/liveness.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_12821;

							BgL_tmpz00_12821 =
								((BgL_objectz00_bglt)
								((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4876)));
							BgL_auxz00_12820 = BGL_OBJECT_WIDENING(BgL_tmpz00_12821);
						}
						BgL_auxz00_12819 =
							((BgL_literalzf2livenesszf2_bglt) BgL_auxz00_12820);
					}
					((((BgL_literalzf2livenesszf2_bglt) COBJECT(BgL_auxz00_12819))->
							BgL_outz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_arg2151z00_5807 =
					((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nz00_4876));
				return
					BGl_defusez00zzliveness_livenessz00(
					((BgL_nodez00_bglt) BgL_arg2151z00_5807));
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzliveness_livenessz00(void)
	{
		{	/* Liveness/liveness.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			BGl_modulezd2initializa7ationz75zzliveness_typesz00(0L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
			return
				BGl_modulezd2initializa7ationz75zzliveness_setz00(332381975L,
				BSTRING_TO_STRING(BGl_string2817z00zzliveness_livenessz00));
		}

	}

#ifdef __cplusplus
}
#endif
