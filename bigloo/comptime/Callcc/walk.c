/*===========================================================================*/
/*   (Callcc/walk.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Callcc/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CALLCC_WALK_TYPE_DEFINITIONS
#define BGL_CALLCC_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_localzf2cellzf2_bgl
	{
	}                      *BgL_localzf2cellzf2_bglt;


#endif													// BGL_CALLCC_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt
		BGl_z62callccz12zd2sequence1327za2zzcallcc_walkz00(obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcallcc_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62callccz12zd2app1331za2zzcallcc_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static BgL_nodez00_bglt BGl_z62callccz12z70zzcallcc_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzcallcc_walkz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_callccza2z12zb0zzcallcc_walkz00(obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzcallcc_walkz00(void);
	extern obj_t BGl_za2cellza2z00zztype_cachez00;
	static obj_t BGl_objectzd2initzd2zzcallcc_walkz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62callccz12zd2atom1321za2zzcallcc_walkz00(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_callcczd2walkz12zc0zzcallcc_walkz00(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2conditional1344za2zzcallcc_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2letzd2fun1350z70zzcallcc_walkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcallcc_walkz00(void);
	static obj_t BGl_z62callccz121318z70zzcallcc_walkz00(obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2appzd2ly1333z70zzcallcc_walkz00(obj_t, obj_t);
	static bool_t BGl_celledzf3zf3zzcallcc_walkz00(BgL_variablez00_bglt);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_localzf2cellzf2zzcallcc_walkz00 = BUNSPEC;
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static BgL_nodez00_bglt BGl_cellzd2formalszd2zzcallcc_walkz00(obj_t,
		BgL_nodez00_bglt);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2extern1337za2zzcallcc_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2switch1348za2zzcallcc_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcallcc_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_callccz12z12zzcallcc_walkz00(BgL_nodez00_bglt);
	static BgL_nodez00_bglt BGl_z62callccz12zd2kwote1323za2zzcallcc_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2setzd2exzd2it1354za2zzcallcc_walkz00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2funcall1335za2zzcallcc_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62callccz12zd2cast1340za2zzcallcc_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcallcc_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcallcc_walkz00(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_z62callcczd2walkz12za2zzcallcc_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzcallcc_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcallcc_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2letzd2var1352z70zzcallcc_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62callccz12zd2setq1342za2zzcallcc_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62callccz12zd2sync1329za2zzcallcc_walkz00(obj_t,
		obj_t);
	static obj_t BGl_celledzd2bindingszd2zzcallcc_walkz00(obj_t);
	static BgL_nodez00_bglt BGl_z62callccz12zd2fail1346za2zzcallcc_walkz00(obj_t,
		obj_t);
	static bool_t BGl_callcczd2funz12zc0zzcallcc_walkz00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2boxzd2ref1360z70zzcallcc_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2makezd2box1358z70zzcallcc_walkz00(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_valuez00zzast_varz00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2boxzd2setz121362z62zzcallcc_walkz00(obj_t, obj_t);
	static BgL_makezd2boxzd2_bglt
		BGl_azd2makezd2cellz00zzcallcc_walkz00(BgL_nodez00_bglt,
		BgL_variablez00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62callccz12zd2var1325za2zzcallcc_walkz00(obj_t,
		obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62callccz12zd2jumpzd2exzd2it1356za2zzcallcc_walkz00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1664z62zzcallcc_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza31682ze3ze5zzcallcc_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1679z62zzcallcc_walkz00(obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00(BgL_typez00_bglt);
	static BgL_localz00_bglt BGl_z62lambda1683z62zzcallcc_walkz00(obj_t, obj_t);
	static obj_t __cnst[9];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_callcczd2walkz12zd2envz12zzcallcc_walkz00,
		BgL_bgl_za762callccza7d2walk1958z00,
		BGl_z62callcczd2walkz12za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1918z00zzcallcc_walkz00,
		BgL_bgl_string1918za700za7za7c1959za7, "Callcc", 6);
	      DEFINE_STRING(BGl_string1919z00zzcallcc_walkz00,
		BgL_bgl_string1919za700za7za7c1960za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1920z00zzcallcc_walkz00,
		BgL_bgl_string1920za700za7za7c1961za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1921z00zzcallcc_walkz00,
		BgL_bgl_string1921za700za7za7c1962za7, " error", 6);
	      DEFINE_STRING(BGl_string1922z00zzcallcc_walkz00,
		BgL_bgl_string1922za700za7za7c1963za7, "s", 1);
	      DEFINE_STRING(BGl_string1923z00zzcallcc_walkz00,
		BgL_bgl_string1923za700za7za7c1964za7, "", 0);
	      DEFINE_STRING(BGl_string1924z00zzcallcc_walkz00,
		BgL_bgl_string1924za700za7za7c1965za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1925z00zzcallcc_walkz00,
		BgL_bgl_string1925za700za7za7c1966za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1931z00zzcallcc_walkz00,
		BgL_bgl_string1931za700za7za7c1967za7, "callcc!1318", 11);
	      DEFINE_STRING(BGl_string1932z00zzcallcc_walkz00,
		BgL_bgl_string1932za700za7za7c1968za7, "No method for this object", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1926z00zzcallcc_walkz00,
		BgL_bgl_za762lambda1683za7621969z00, BGl_z62lambda1683z62zzcallcc_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1927z00zzcallcc_walkz00,
		BgL_bgl_za762za7c3za704anonymo1970za7,
		BGl_z62zc3z04anonymousza31682ze3ze5zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1934z00zzcallcc_walkz00,
		BgL_bgl_string1934za700za7za7c1971za7, "callcc!", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1928z00zzcallcc_walkz00,
		BgL_bgl_za762lambda1679za7621972z00, BGl_z62lambda1679z62zzcallcc_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1929z00zzcallcc_walkz00,
		BgL_bgl_za762lambda1664za7621973z00, BGl_z62lambda1664z62zzcallcc_walkz00,
		0L, BUNSPEC, 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1930z00zzcallcc_walkz00,
		BgL_bgl_za762callccza71213181974z00,
		BGl_z62callccz121318z70zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1933z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2a1975za7,
		BGl_z62callccz12zd2atom1321za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1935z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2k1976za7,
		BGl_z62callccz12zd2kwote1323za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1936z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2v1977za7,
		BGl_z62callccz12zd2var1325za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1937z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2s1978za7,
		BGl_z62callccz12zd2sequence1327za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1938z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2s1979za7,
		BGl_z62callccz12zd2sync1329za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1939z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2a1980za7,
		BGl_z62callccz12zd2app1331za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1940z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2a1981za7,
		BGl_z62callccz12zd2appzd2ly1333z70zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1941z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2f1982za7,
		BGl_z62callccz12zd2funcall1335za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1942z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2e1983za7,
		BGl_z62callccz12zd2extern1337za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1943z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2c1984za7,
		BGl_z62callccz12zd2cast1340za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1944z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2s1985za7,
		BGl_z62callccz12zd2setq1342za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1945z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2c1986za7,
		BGl_z62callccz12zd2conditional1344za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1946z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2f1987za7,
		BGl_z62callccz12zd2fail1346za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1947z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2s1988za7,
		BGl_z62callccz12zd2switch1348za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1948z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2l1989za7,
		BGl_z62callccz12zd2letzd2fun1350z70zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1955z00zzcallcc_walkz00,
		BgL_bgl_string1955za700za7za7c1990za7, "callcc_walk", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1949z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2l1991za7,
		BGl_z62callccz12zd2letzd2var1352z70zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1956z00zzcallcc_walkz00,
		BgL_bgl_string1956za700za7za7c1992za7,
		"aux callcc!1318 _ callcc_walk local/cell done write cell-callcc pass-started ",
		77);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1950z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2s1993za7,
		BGl_z62callccz12zd2setzd2exzd2it1354za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1951z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2j1994za7,
		BGl_z62callccz12zd2jumpzd2exzd2it1356za2zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1952z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2m1995za7,
		BGl_z62callccz12zd2makezd2box1358z70zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1953z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2b1996za7,
		BGl_z62callccz12zd2boxzd2ref1360z70zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1954z00zzcallcc_walkz00,
		BgL_bgl_za762callccza712za7d2b1997za7,
		BGl_z62callccz12zd2boxzd2setz121362z62zzcallcc_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_callccz12zd2envzc0zzcallcc_walkz00,
		BgL_bgl_za762callccza712za770za71998z00,
		BGl_z62callccz12z70zzcallcc_walkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcallcc_walkz00));
		     ADD_ROOT((void *) (&BGl_localzf2cellzf2zzcallcc_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcallcc_walkz00(long
		BgL_checksumz00_2781, char *BgL_fromz00_2782)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcallcc_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzcallcc_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcallcc_walkz00();
					BGl_libraryzd2moduleszd2initz00zzcallcc_walkz00();
					BGl_cnstzd2initzd2zzcallcc_walkz00();
					BGl_importedzd2moduleszd2initz00zzcallcc_walkz00();
					BGl_objectzd2initzd2zzcallcc_walkz00();
					BGl_genericzd2initzd2zzcallcc_walkz00();
					BGl_methodzd2initzd2zzcallcc_walkz00();
					return BGl_toplevelzd2initzd2zzcallcc_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcallcc_walkz00(void)
	{
		{	/* Callcc/walk.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "callcc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"callcc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"callcc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "callcc_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "callcc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "callcc_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "callcc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "callcc_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "callcc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"callcc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"callcc_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "callcc_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcallcc_walkz00(void)
	{
		{	/* Callcc/walk.scm 16 */
			{	/* Callcc/walk.scm 16 */
				obj_t BgL_cportz00_2570;

				{	/* Callcc/walk.scm 16 */
					obj_t BgL_stringz00_2577;

					BgL_stringz00_2577 = BGl_string1956z00zzcallcc_walkz00;
					{	/* Callcc/walk.scm 16 */
						obj_t BgL_startz00_2578;

						BgL_startz00_2578 = BINT(0L);
						{	/* Callcc/walk.scm 16 */
							obj_t BgL_endz00_2579;

							BgL_endz00_2579 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2577)));
							{	/* Callcc/walk.scm 16 */

								BgL_cportz00_2570 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2577, BgL_startz00_2578, BgL_endz00_2579);
				}}}}
				{
					long BgL_iz00_2571;

					BgL_iz00_2571 = 8L;
				BgL_loopz00_2572:
					if ((BgL_iz00_2571 == -1L))
						{	/* Callcc/walk.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Callcc/walk.scm 16 */
							{	/* Callcc/walk.scm 16 */
								obj_t BgL_arg1957z00_2573;

								{	/* Callcc/walk.scm 16 */

									{	/* Callcc/walk.scm 16 */
										obj_t BgL_locationz00_2575;

										BgL_locationz00_2575 = BBOOL(((bool_t) 0));
										{	/* Callcc/walk.scm 16 */

											BgL_arg1957z00_2573 =
												BGl_readz00zz__readerz00(BgL_cportz00_2570,
												BgL_locationz00_2575);
										}
									}
								}
								{	/* Callcc/walk.scm 16 */
									int BgL_tmpz00_2815;

									BgL_tmpz00_2815 = (int) (BgL_iz00_2571);
									CNST_TABLE_SET(BgL_tmpz00_2815, BgL_arg1957z00_2573);
							}}
							{	/* Callcc/walk.scm 16 */
								int BgL_auxz00_2576;

								BgL_auxz00_2576 = (int) ((BgL_iz00_2571 - 1L));
								{
									long BgL_iz00_2820;

									BgL_iz00_2820 = (long) (BgL_auxz00_2576);
									BgL_iz00_2571 = BgL_iz00_2820;
									goto BgL_loopz00_2572;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcallcc_walkz00(void)
	{
		{	/* Callcc/walk.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcallcc_walkz00(void)
	{
		{	/* Callcc/walk.scm 16 */
			return BUNSPEC;
		}

	}



/* callcc-walk! */
	BGL_EXPORTED_DEF obj_t BGl_callcczd2walkz12zc0zzcallcc_walkz00(obj_t
		BgL_globalsz00_48)
	{
		{	/* Callcc/walk.scm 31 */
			{	/* Callcc/walk.scm 32 */
				obj_t BgL_list1372z00_1434;

				{	/* Callcc/walk.scm 32 */
					obj_t BgL_arg1375z00_1435;

					{	/* Callcc/walk.scm 32 */
						obj_t BgL_arg1376z00_1436;

						BgL_arg1376z00_1436 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1375z00_1435 =
							MAKE_YOUNG_PAIR(BGl_string1918z00zzcallcc_walkz00,
							BgL_arg1376z00_1436);
					}
					BgL_list1372z00_1434 =
						MAKE_YOUNG_PAIR(BGl_string1919z00zzcallcc_walkz00,
						BgL_arg1375z00_1435);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1372z00_1434);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1918z00zzcallcc_walkz00;
			{	/* Callcc/walk.scm 32 */
				obj_t BgL_g1130z00_1437;

				BgL_g1130z00_1437 = BNIL;
				{
					obj_t BgL_hooksz00_1440;
					obj_t BgL_hnamesz00_1441;

					BgL_hooksz00_1440 = BgL_g1130z00_1437;
					BgL_hnamesz00_1441 = BNIL;
				BgL_zc3z04anonymousza31377ze3z87_1442:
					if (NULLP(BgL_hooksz00_1440))
						{	/* Callcc/walk.scm 32 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Callcc/walk.scm 32 */
							bool_t BgL_test2002z00_2833;

							{	/* Callcc/walk.scm 32 */
								obj_t BgL_fun1423z00_1449;

								BgL_fun1423z00_1449 = CAR(((obj_t) BgL_hooksz00_1440));
								BgL_test2002z00_2833 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1423z00_1449));
							}
							if (BgL_test2002z00_2833)
								{	/* Callcc/walk.scm 32 */
									obj_t BgL_arg1408z00_1446;
									obj_t BgL_arg1410z00_1447;

									BgL_arg1408z00_1446 = CDR(((obj_t) BgL_hooksz00_1440));
									BgL_arg1410z00_1447 = CDR(((obj_t) BgL_hnamesz00_1441));
									{
										obj_t BgL_hnamesz00_2845;
										obj_t BgL_hooksz00_2844;

										BgL_hooksz00_2844 = BgL_arg1408z00_1446;
										BgL_hnamesz00_2845 = BgL_arg1410z00_1447;
										BgL_hnamesz00_1441 = BgL_hnamesz00_2845;
										BgL_hooksz00_1440 = BgL_hooksz00_2844;
										goto BgL_zc3z04anonymousza31377ze3z87_1442;
									}
								}
							else
								{	/* Callcc/walk.scm 32 */
									obj_t BgL_arg1421z00_1448;

									BgL_arg1421z00_1448 = CAR(((obj_t) BgL_hnamesz00_1441));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1918z00zzcallcc_walkz00,
										BGl_string1920z00zzcallcc_walkz00, BgL_arg1421z00_1448);
								}
						}
				}
			}
			if (((long) CINT(BGl_za2compilerzd2debugza2zd2zzengine_paramz00) >= 2L))
				{	/* Callcc/walk.scm 34 */
					BGl_za2compilerzd2debugza2zd2zzengine_paramz00 = BINT(1L);
				}
			else
				{	/* Callcc/walk.scm 34 */
					BFALSE;
				}
			{
				obj_t BgL_l1295z00_1454;

				BgL_l1295z00_1454 = BgL_globalsz00_48;
			BgL_zc3z04anonymousza31426ze3z87_1455:
				if (PAIRP(BgL_l1295z00_1454))
					{	/* Callcc/walk.scm 36 */
						BGl_callcczd2funz12zc0zzcallcc_walkz00(CAR(BgL_l1295z00_1454));
						{
							obj_t BgL_l1295z00_2857;

							BgL_l1295z00_2857 = CDR(BgL_l1295z00_1454);
							BgL_l1295z00_1454 = BgL_l1295z00_2857;
							goto BgL_zc3z04anonymousza31426ze3z87_1455;
						}
					}
				else
					{	/* Callcc/walk.scm 36 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Callcc/walk.scm 37 */
					{	/* Callcc/walk.scm 37 */
						obj_t BgL_port1297z00_1462;

						{	/* Callcc/walk.scm 37 */
							obj_t BgL_tmpz00_2862;

							BgL_tmpz00_2862 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1297z00_1462 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2862);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1297z00_1462);
						bgl_display_string(BGl_string1921z00zzcallcc_walkz00,
							BgL_port1297z00_1462);
						{	/* Callcc/walk.scm 37 */
							obj_t BgL_arg1448z00_1463;

							{	/* Callcc/walk.scm 37 */
								bool_t BgL_test2006z00_2867;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Callcc/walk.scm 37 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Callcc/walk.scm 37 */
												BgL_test2006z00_2867 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Callcc/walk.scm 37 */
												BgL_test2006z00_2867 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Callcc/walk.scm 37 */
										BgL_test2006z00_2867 = ((bool_t) 0);
									}
								if (BgL_test2006z00_2867)
									{	/* Callcc/walk.scm 37 */
										BgL_arg1448z00_1463 = BGl_string1922z00zzcallcc_walkz00;
									}
								else
									{	/* Callcc/walk.scm 37 */
										BgL_arg1448z00_1463 = BGl_string1923z00zzcallcc_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1448z00_1463, BgL_port1297z00_1462);
						}
						bgl_display_string(BGl_string1924z00zzcallcc_walkz00,
							BgL_port1297z00_1462);
						bgl_display_char(((unsigned char) 10), BgL_port1297z00_1462);
					}
					{	/* Callcc/walk.scm 37 */
						obj_t BgL_list1451z00_1467;

						BgL_list1451z00_1467 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1451z00_1467);
					}
				}
			else
				{	/* Callcc/walk.scm 37 */
					obj_t BgL_g1132z00_1468;

					BgL_g1132z00_1468 = BNIL;
					{
						obj_t BgL_hooksz00_1471;
						obj_t BgL_hnamesz00_1472;

						BgL_hooksz00_1471 = BgL_g1132z00_1468;
						BgL_hnamesz00_1472 = BNIL;
					BgL_zc3z04anonymousza31452ze3z87_1473:
						if (NULLP(BgL_hooksz00_1471))
							{	/* Callcc/walk.scm 37 */
								return BgL_globalsz00_48;
							}
						else
							{	/* Callcc/walk.scm 37 */
								bool_t BgL_test2010z00_2884;

								{	/* Callcc/walk.scm 37 */
									obj_t BgL_fun1487z00_1480;

									BgL_fun1487z00_1480 = CAR(((obj_t) BgL_hooksz00_1471));
									BgL_test2010z00_2884 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1487z00_1480));
								}
								if (BgL_test2010z00_2884)
									{	/* Callcc/walk.scm 37 */
										obj_t BgL_arg1472z00_1477;
										obj_t BgL_arg1473z00_1478;

										BgL_arg1472z00_1477 = CDR(((obj_t) BgL_hooksz00_1471));
										BgL_arg1473z00_1478 = CDR(((obj_t) BgL_hnamesz00_1472));
										{
											obj_t BgL_hnamesz00_2896;
											obj_t BgL_hooksz00_2895;

											BgL_hooksz00_2895 = BgL_arg1472z00_1477;
											BgL_hnamesz00_2896 = BgL_arg1473z00_1478;
											BgL_hnamesz00_1472 = BgL_hnamesz00_2896;
											BgL_hooksz00_1471 = BgL_hooksz00_2895;
											goto BgL_zc3z04anonymousza31452ze3z87_1473;
										}
									}
								else
									{	/* Callcc/walk.scm 37 */
										obj_t BgL_arg1485z00_1479;

										BgL_arg1485z00_1479 = CAR(((obj_t) BgL_hnamesz00_1472));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string1925z00zzcallcc_walkz00, BgL_arg1485z00_1479);
									}
							}
					}
				}
		}

	}



/* &callcc-walk! */
	obj_t BGl_z62callcczd2walkz12za2zzcallcc_walkz00(obj_t BgL_envz00_2476,
		obj_t BgL_globalsz00_2477)
	{
		{	/* Callcc/walk.scm 31 */
			return BGl_callcczd2walkz12zc0zzcallcc_walkz00(BgL_globalsz00_2477);
		}

	}



/* callcc-fun! */
	bool_t BGl_callcczd2funz12zc0zzcallcc_walkz00(obj_t BgL_varz00_49)
	{
		{	/* Callcc/walk.scm 42 */
			{	/* Callcc/walk.scm 43 */
				BgL_valuez00_bglt BgL_funz00_1483;

				BgL_funz00_1483 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_49)))->BgL_valuez00);
				{	/* Callcc/walk.scm 43 */
					obj_t BgL_bodyz00_1484;

					BgL_bodyz00_1484 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1483)))->BgL_bodyz00);
					{	/* Callcc/walk.scm 44 */
						obj_t BgL_celledz00_1485;

						{	/* Callcc/walk.scm 45 */
							obj_t BgL_arg1544z00_1505;

							BgL_arg1544z00_1505 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_funz00_1483)))->BgL_argsz00);
							BgL_celledz00_1485 =
								BGl_celledzd2bindingszd2zzcallcc_walkz00(BgL_arg1544z00_1505);
						}
						{	/* Callcc/walk.scm 45 */

							{
								obj_t BgL_l1298z00_1487;

								BgL_l1298z00_1487 = BgL_celledz00_1485;
							BgL_zc3z04anonymousza31489ze3z87_1488:
								if (PAIRP(BgL_l1298z00_1487))
									{	/* Callcc/walk.scm 47 */
										{	/* Callcc/walk.scm 48 */
											obj_t BgL_wzd2bzd2_1490;

											BgL_wzd2bzd2_1490 = CAR(BgL_l1298z00_1487);
											{	/* Callcc/walk.scm 48 */
												obj_t BgL_arg1502z00_1491;
												obj_t BgL_arg1509z00_1492;

												BgL_arg1502z00_1491 = CAR(((obj_t) BgL_wzd2bzd2_1490));
												BgL_arg1509z00_1492 = CDR(((obj_t) BgL_wzd2bzd2_1490));
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_arg1502z00_1491))))->
														BgL_fastzd2alphazd2) =
													((obj_t) BgL_arg1509z00_1492), BUNSPEC);
											}
										}
										{
											obj_t BgL_l1298z00_2918;

											BgL_l1298z00_2918 = CDR(BgL_l1298z00_1487);
											BgL_l1298z00_1487 = BgL_l1298z00_2918;
											goto BgL_zc3z04anonymousza31489ze3z87_1488;
										}
									}
								else
									{	/* Callcc/walk.scm 47 */
										((bool_t) 1);
									}
							}
							{	/* Callcc/walk.scm 50 */
								BgL_nodez00_bglt BgL_arg1514z00_1495;

								{	/* Callcc/walk.scm 50 */
									BgL_nodez00_bglt BgL_arg1516z00_1496;

									BgL_arg1516z00_1496 =
										BGl_callccz12z12zzcallcc_walkz00(
										((BgL_nodez00_bglt) BgL_bodyz00_1484));
									BgL_arg1514z00_1495 =
										BGl_cellzd2formalszd2zzcallcc_walkz00(BgL_celledz00_1485,
										BgL_arg1516z00_1496);
								}
								((((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1483)))->BgL_bodyz00) =
									((obj_t) ((obj_t) BgL_arg1514z00_1495)), BUNSPEC);
							}
							{
								obj_t BgL_l1300z00_1498;

								BgL_l1300z00_1498 = BgL_celledz00_1485;
							BgL_zc3z04anonymousza31517ze3z87_1499:
								if (PAIRP(BgL_l1300z00_1498))
									{	/* Callcc/walk.scm 52 */
										{	/* Callcc/walk.scm 53 */
											obj_t BgL_wzd2bzd2_1501;

											BgL_wzd2bzd2_1501 = CAR(BgL_l1300z00_1498);
											{	/* Callcc/walk.scm 53 */
												obj_t BgL_arg1535z00_1502;

												BgL_arg1535z00_1502 = CAR(((obj_t) BgL_wzd2bzd2_1501));
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_arg1535z00_1502))))->
														BgL_fastzd2alphazd2) = ((obj_t) BUNSPEC), BUNSPEC);
											}
										}
										{
											obj_t BgL_l1300z00_2934;

											BgL_l1300z00_2934 = CDR(BgL_l1300z00_1498);
											BgL_l1300z00_1498 = BgL_l1300z00_2934;
											goto BgL_zc3z04anonymousza31517ze3z87_1499;
										}
									}
								else
									{	/* Callcc/walk.scm 52 */
										return ((bool_t) 1);
									}
							}
						}
					}
				}
			}
		}

	}



/* celled-bindings */
	obj_t BGl_celledzd2bindingszd2zzcallcc_walkz00(obj_t BgL_formalsz00_50)
	{
		{	/* Callcc/walk.scm 59 */
			{
				obj_t BgL_celledz00_1508;
				obj_t BgL_formalsz00_1509;

				BgL_celledz00_1508 = BNIL;
				BgL_formalsz00_1509 = BgL_formalsz00_50;
			BgL_zc3z04anonymousza31545ze3z87_1510:
				if (NULLP(BgL_formalsz00_1509))
					{	/* Callcc/walk.scm 63 */
						return BgL_celledz00_1508;
					}
				else
					{	/* Callcc/walk.scm 65 */
						bool_t BgL_test2014z00_2938;

						{	/* Callcc/walk.scm 65 */
							obj_t BgL_arg1584z00_1531;

							BgL_arg1584z00_1531 = CAR(((obj_t) BgL_formalsz00_1509));
							BgL_test2014z00_2938 =
								BGl_celledzf3zf3zzcallcc_walkz00(
								((BgL_variablez00_bglt) BgL_arg1584z00_1531));
						}
						if (BgL_test2014z00_2938)
							{	/* Callcc/walk.scm 68 */
								BgL_typez00_bglt BgL_vtypez00_1514;

								BgL_vtypez00_1514 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt)
													CAR(((obj_t) BgL_formalsz00_1509))))))->BgL_typez00);
								{	/* Callcc/walk.scm 68 */
									obj_t BgL_ntypez00_1515;

									if (
										(((obj_t) BgL_vtypez00_1514) ==
											BGl_za2_za2z00zztype_cachez00))
										{	/* Callcc/walk.scm 70 */
											BgL_ntypez00_1515 = BGl_za2objza2z00zztype_cachez00;
										}
									else
										{	/* Callcc/walk.scm 70 */
											if (BGl_bigloozd2typezf3z21zztype_typez00
												(BgL_vtypez00_1514))
												{	/* Callcc/walk.scm 71 */
													BgL_ntypez00_1515 = ((obj_t) BgL_vtypez00_1514);
												}
											else
												{	/* Callcc/walk.scm 71 */
													BgL_ntypez00_1515 =
														((obj_t)
														BGl_getzd2bigloozd2typez00zztype_cachez00
														(BgL_vtypez00_1514));
												}
										}
									{	/* Callcc/walk.scm 69 */
										BgL_localz00_bglt BgL_varz00_1516;

										{	/* Callcc/walk.scm 73 */
											obj_t BgL_arg1571z00_1526;

											BgL_arg1571z00_1526 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt)
																CAR(
																	((obj_t) BgL_formalsz00_1509))))))->
												BgL_idz00);
											BgL_varz00_1516 =
												BGl_makezd2localzd2svarz00zzast_localz00
												(BgL_arg1571z00_1526,
												((BgL_typez00_bglt) BGl_za2cellza2z00zztype_cachez00));
										}
										{	/* Callcc/walk.scm 73 */
											obj_t BgL_ozd2nzd2_1517;

											{	/* Callcc/walk.scm 74 */
												obj_t BgL_arg1565z00_1525;

												BgL_arg1565z00_1525 =
													CAR(((obj_t) BgL_formalsz00_1509));
												BgL_ozd2nzd2_1517 =
													MAKE_YOUNG_PAIR(BgL_arg1565z00_1525,
													((obj_t) BgL_varz00_1516));
											}
											{	/* Callcc/walk.scm 74 */

												{	/* Callcc/walk.scm 75 */
													BgL_localzf2cellzf2_bglt BgL_wide1137z00_1520;

													BgL_wide1137z00_1520 =
														((BgL_localzf2cellzf2_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_localzf2cellzf2_bgl))));
													{	/* Callcc/walk.scm 75 */
														obj_t BgL_auxz00_2971;
														BgL_objectz00_bglt BgL_tmpz00_2968;

														BgL_auxz00_2971 = ((obj_t) BgL_wide1137z00_1520);
														BgL_tmpz00_2968 =
															((BgL_objectz00_bglt)
															((BgL_localz00_bglt) BgL_varz00_1516));
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2968,
															BgL_auxz00_2971);
													}
													((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_varz00_1516));
													{	/* Callcc/walk.scm 75 */
														long BgL_arg1559z00_1521;

														BgL_arg1559z00_1521 =
															BGL_CLASS_NUM
															(BGl_localzf2cellzf2zzcallcc_walkz00);
														BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																	(BgL_localz00_bglt) BgL_varz00_1516)),
															BgL_arg1559z00_1521);
													}
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_varz00_1516));
												}
												((BgL_localz00_bglt) BgL_varz00_1516);
												{	/* Callcc/walk.scm 76 */
													obj_t BgL_arg1561z00_1523;
													obj_t BgL_arg1564z00_1524;

													BgL_arg1561z00_1523 =
														MAKE_YOUNG_PAIR(BgL_ozd2nzd2_1517,
														BgL_celledz00_1508);
													BgL_arg1564z00_1524 =
														CDR(((obj_t) BgL_formalsz00_1509));
													{
														obj_t BgL_formalsz00_2987;
														obj_t BgL_celledz00_2986;

														BgL_celledz00_2986 = BgL_arg1561z00_1523;
														BgL_formalsz00_2987 = BgL_arg1564z00_1524;
														BgL_formalsz00_1509 = BgL_formalsz00_2987;
														BgL_celledz00_1508 = BgL_celledz00_2986;
														goto BgL_zc3z04anonymousza31545ze3z87_1510;
													}
												}
											}
										}
									}
								}
							}
						else
							{	/* Callcc/walk.scm 66 */
								obj_t BgL_arg1576z00_1530;

								BgL_arg1576z00_1530 = CDR(((obj_t) BgL_formalsz00_1509));
								{
									obj_t BgL_formalsz00_2990;

									BgL_formalsz00_2990 = BgL_arg1576z00_1530;
									BgL_formalsz00_1509 = BgL_formalsz00_2990;
									goto BgL_zc3z04anonymousza31545ze3z87_1510;
								}
							}
					}
			}
		}

	}



/* cell-formals */
	BgL_nodez00_bglt BGl_cellzd2formalszd2zzcallcc_walkz00(obj_t BgL_celledz00_51,
		BgL_nodez00_bglt BgL_bodyz00_52)
	{
		{	/* Callcc/walk.scm 81 */
			if (NULLP(BgL_celledz00_51))
				{	/* Callcc/walk.scm 82 */
					return BgL_bodyz00_52;
				}
			else
				{	/* Callcc/walk.scm 84 */
					obj_t BgL_locz00_1534;

					BgL_locz00_1534 =
						(((BgL_nodez00_bglt) COBJECT(BgL_bodyz00_52))->BgL_locz00);
					{	/* Callcc/walk.scm 85 */
						BgL_letzd2varzd2_bglt BgL_new1140z00_1535;

						{	/* Callcc/walk.scm 86 */
							BgL_letzd2varzd2_bglt BgL_new1139z00_1557;

							BgL_new1139z00_1557 =
								((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_letzd2varzd2_bgl))));
							{	/* Callcc/walk.scm 86 */
								long BgL_arg1609z00_1558;

								BgL_arg1609z00_1558 =
									BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1139z00_1557),
									BgL_arg1609z00_1558);
							}
							{	/* Callcc/walk.scm 86 */
								BgL_objectz00_bglt BgL_tmpz00_2998;

								BgL_tmpz00_2998 = ((BgL_objectz00_bglt) BgL_new1139z00_1557);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2998, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1139z00_1557);
							BgL_new1140z00_1535 = BgL_new1139z00_1557;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1140z00_1535)))->BgL_locz00) =
							((obj_t) BgL_locz00_1534), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1140z00_1535)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT(BgL_bodyz00_52))->BgL_typez00)), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1140z00_1535)))->BgL_sidezd2effectzd2) =
							((obj_t) BUNSPEC), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1140z00_1535)))->BgL_keyz00) =
							((obj_t) BINT(-1L)), BUNSPEC);
						{
							obj_t BgL_auxz00_3012;

							{	/* Callcc/walk.scm 89 */
								obj_t BgL_head1304z00_1538;

								BgL_head1304z00_1538 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1302z00_1540;
									obj_t BgL_tail1305z00_1541;

									BgL_l1302z00_1540 = BgL_celledz00_51;
									BgL_tail1305z00_1541 = BgL_head1304z00_1538;
								BgL_zc3z04anonymousza31587ze3z87_1542:
									if (NULLP(BgL_l1302z00_1540))
										{	/* Callcc/walk.scm 89 */
											BgL_auxz00_3012 = CDR(BgL_head1304z00_1538);
										}
									else
										{	/* Callcc/walk.scm 89 */
											obj_t BgL_newtail1306z00_1544;

											{	/* Callcc/walk.scm 89 */
												obj_t BgL_arg1591z00_1546;

												{	/* Callcc/walk.scm 89 */
													obj_t BgL_ozd2nzd2_1547;

													BgL_ozd2nzd2_1547 = CAR(((obj_t) BgL_l1302z00_1540));
													{	/* Callcc/walk.scm 90 */
														obj_t BgL_arg1593z00_1548;
														BgL_makezd2boxzd2_bglt BgL_arg1594z00_1549;

														BgL_arg1593z00_1548 =
															CDR(((obj_t) BgL_ozd2nzd2_1547));
														{	/* Callcc/walk.scm 92 */
															BgL_refz00_bglt BgL_arg1595z00_1550;
															obj_t BgL_arg1602z00_1551;

															{	/* Callcc/walk.scm 92 */
																BgL_refz00_bglt BgL_new1142z00_1552;

																{	/* Callcc/walk.scm 95 */
																	BgL_refz00_bglt BgL_new1141z00_1554;

																	BgL_new1141z00_1554 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Callcc/walk.scm 95 */
																		long BgL_arg1606z00_1555;

																		{	/* Callcc/walk.scm 95 */
																			obj_t BgL_classz00_2060;

																			BgL_classz00_2060 =
																				BGl_refz00zzast_nodez00;
																			BgL_arg1606z00_1555 =
																				BGL_CLASS_NUM(BgL_classz00_2060);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1141z00_1554),
																			BgL_arg1606z00_1555);
																	}
																	{	/* Callcc/walk.scm 95 */
																		BgL_objectz00_bglt BgL_tmpz00_3025;

																		BgL_tmpz00_3025 =
																			((BgL_objectz00_bglt)
																			BgL_new1141z00_1554);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3025,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1141z00_1554);
																	BgL_new1142z00_1552 = BgL_new1141z00_1554;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1142z00_1552)))->BgL_locz00) =
																	((obj_t) BgL_locz00_1534), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1142z00_1552)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						CAR(((obj_t)
																								BgL_ozd2nzd2_1547)))))->
																			BgL_typez00)), BUNSPEC);
																((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																					BgL_new1142z00_1552)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt) CAR(((obj_t)
																					BgL_ozd2nzd2_1547)))), BUNSPEC);
																BgL_arg1595z00_1550 = BgL_new1142z00_1552;
															}
															BgL_arg1602z00_1551 =
																CAR(((obj_t) BgL_ozd2nzd2_1547));
															BgL_arg1594z00_1549 =
																BGl_azd2makezd2cellz00zzcallcc_walkz00(
																((BgL_nodez00_bglt) BgL_arg1595z00_1550),
																((BgL_variablez00_bglt) BgL_arg1602z00_1551));
														}
														BgL_arg1591z00_1546 =
															MAKE_YOUNG_PAIR(BgL_arg1593z00_1548,
															((obj_t) BgL_arg1594z00_1549));
												}}
												BgL_newtail1306z00_1544 =
													MAKE_YOUNG_PAIR(BgL_arg1591z00_1546, BNIL);
											}
											SET_CDR(BgL_tail1305z00_1541, BgL_newtail1306z00_1544);
											{	/* Callcc/walk.scm 89 */
												obj_t BgL_arg1589z00_1545;

												BgL_arg1589z00_1545 = CDR(((obj_t) BgL_l1302z00_1540));
												{
													obj_t BgL_tail1305z00_3054;
													obj_t BgL_l1302z00_3053;

													BgL_l1302z00_3053 = BgL_arg1589z00_1545;
													BgL_tail1305z00_3054 = BgL_newtail1306z00_1544;
													BgL_tail1305z00_1541 = BgL_tail1305z00_3054;
													BgL_l1302z00_1540 = BgL_l1302z00_3053;
													goto BgL_zc3z04anonymousza31587ze3z87_1542;
												}
											}
										}
								}
							}
							((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1140z00_1535))->
									BgL_bindingsz00) = ((obj_t) BgL_auxz00_3012), BUNSPEC);
						}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1140z00_1535))->
								BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_bodyz00_52), BUNSPEC);
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1140z00_1535))->
								BgL_removablezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
						return ((BgL_nodez00_bglt) BgL_new1140z00_1535);
					}
				}
		}

	}



/* a-make-cell */
	BgL_makezd2boxzd2_bglt BGl_azd2makezd2cellz00zzcallcc_walkz00(BgL_nodez00_bglt
		BgL_nodez00_53, BgL_variablez00_bglt BgL_variablez00_54)
	{
		{	/* Callcc/walk.scm 102 */
			{	/* Callcc/walk.scm 104 */
				obj_t BgL_vz00_2071;

				BgL_vz00_2071 = CNST_TABLE_REF(1);
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_variablez00_54))))->BgL_accessz00) =
					((obj_t) BgL_vz00_2071), BUNSPEC);
			}
			{	/* Callcc/walk.scm 105 */
				BgL_localzf2cellzf2_bglt BgL_wide1146z00_1562;

				BgL_wide1146z00_1562 =
					((BgL_localzf2cellzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2cellzf2_bgl))));
				{	/* Callcc/walk.scm 105 */
					obj_t BgL_auxz00_3068;
					BgL_objectz00_bglt BgL_tmpz00_3064;

					BgL_auxz00_3068 = ((obj_t) BgL_wide1146z00_1562);
					BgL_tmpz00_3064 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_variablez00_54)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3064, BgL_auxz00_3068);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_variablez00_54)));
				{	/* Callcc/walk.scm 105 */
					long BgL_arg1611z00_1563;

					BgL_arg1611z00_1563 =
						BGL_CLASS_NUM(BGl_localzf2cellzf2zzcallcc_walkz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_variablez00_54))),
						BgL_arg1611z00_1563);
				}
				((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_variablez00_54)));
			}
			((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_variablez00_54));
			{	/* Callcc/walk.scm 106 */
				BgL_makezd2boxzd2_bglt BgL_new1149z00_1565;

				{	/* Callcc/walk.scm 109 */
					BgL_makezd2boxzd2_bglt BgL_new1148z00_1567;

					BgL_new1148z00_1567 =
						((BgL_makezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_makezd2boxzd2_bgl))));
					{	/* Callcc/walk.scm 109 */
						long BgL_arg1615z00_1568;

						BgL_arg1615z00_1568 = BGL_CLASS_NUM(BGl_makezd2boxzd2zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1148z00_1567), BgL_arg1615z00_1568);
					}
					{	/* Callcc/walk.scm 109 */
						BgL_objectz00_bglt BgL_tmpz00_3088;

						BgL_tmpz00_3088 = ((BgL_objectz00_bglt) BgL_new1148z00_1567);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3088, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1148z00_1567);
					BgL_new1149z00_1565 = BgL_new1148z00_1567;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1149z00_1565)))->BgL_locz00) =
					((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_53))->BgL_locz00)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1149z00_1565)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt)
							BGl_za2cellza2z00zztype_cachez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1149z00_1565)))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1149z00_1565)))->BgL_keyz00) =
					((obj_t) BINT(-1L)), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1149z00_1565))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_nodez00_53), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3104;

					{	/* Callcc/walk.scm 108 */
						BgL_typez00_bglt BgL_arg1613z00_1566;

						BgL_arg1613z00_1566 =
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_54))->
							BgL_typez00);
						BgL_auxz00_3104 =
							BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
							(BgL_arg1613z00_1566);
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1149z00_1565))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_3104), BUNSPEC);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1149z00_1565))->
						BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
				return BgL_new1149z00_1565;
			}
		}

	}



/* celled? */
	bool_t BGl_celledzf3zf3zzcallcc_walkz00(BgL_variablez00_bglt BgL_varz00_55)
	{
		{	/* Callcc/walk.scm 115 */
			{	/* Callcc/walk.scm 116 */
				bool_t BgL__ortest_1150z00_1569;

				{	/* Callcc/walk.scm 116 */
					obj_t BgL_classz00_2081;

					BgL_classz00_2081 = BGl_localzf2cellzf2zzcallcc_walkz00;
					{	/* Callcc/walk.scm 116 */
						obj_t BgL_oclassz00_2083;

						{	/* Callcc/walk.scm 116 */
							obj_t BgL_arg1815z00_2085;
							long BgL_arg1816z00_2086;

							BgL_arg1815z00_2085 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Callcc/walk.scm 116 */
								long BgL_arg1817z00_2087;

								BgL_arg1817z00_2087 =
									BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_varz00_55));
								BgL_arg1816z00_2086 = (BgL_arg1817z00_2087 - OBJECT_TYPE);
							}
							BgL_oclassz00_2083 =
								VECTOR_REF(BgL_arg1815z00_2085, BgL_arg1816z00_2086);
						}
						BgL__ortest_1150z00_1569 =
							(BgL_oclassz00_2083 == BgL_classz00_2081);
				}}
				if (BgL__ortest_1150z00_1569)
					{	/* Callcc/walk.scm 116 */
						return BgL__ortest_1150z00_1569;
					}
				else
					{	/* Callcc/walk.scm 116 */
						if (
							((((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt) BgL_varz00_55))))->
									BgL_accessz00) == CNST_TABLE_REF(2)))
							{	/* Callcc/walk.scm 118 */
								bool_t BgL__ortest_1152z00_1571;

								{	/* Callcc/walk.scm 118 */
									BgL_typez00_bglt BgL_arg1625z00_1573;

									BgL_arg1625z00_1573 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_varz00_55))))->BgL_typez00);
									BgL__ortest_1152z00_1571 =
										(
										((obj_t) BgL_arg1625z00_1573) ==
										BGl_za2_za2z00zztype_cachez00);
								}
								if (BgL__ortest_1152z00_1571)
									{	/* Callcc/walk.scm 118 */
										return BgL__ortest_1152z00_1571;
									}
								else
									{	/* Callcc/walk.scm 119 */
										BgL_typez00_bglt BgL_arg1616z00_1572;

										BgL_arg1616z00_1572 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_varz00_55))))->
											BgL_typez00);
										return
											BGl_bigloozd2typezf3z21zztype_typez00
											(BgL_arg1616z00_1572);
									}
							}
						else
							{	/* Callcc/walk.scm 117 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* callcc*! */
	obj_t BGl_callccza2z12zb0zzcallcc_walkz00(obj_t BgL_nodeza2za2_78)
	{
		{	/* Callcc/walk.scm 359 */
		BGl_callccza2z12zb0zzcallcc_walkz00:
			if (NULLP(BgL_nodeza2za2_78))
				{	/* Callcc/walk.scm 360 */
					return CNST_TABLE_REF(3);
				}
			else
				{	/* Callcc/walk.scm 360 */
					{	/* Callcc/walk.scm 363 */
						BgL_nodez00_bglt BgL_arg1629z00_1576;

						{	/* Callcc/walk.scm 363 */
							obj_t BgL_arg1630z00_1577;

							BgL_arg1630z00_1577 = CAR(((obj_t) BgL_nodeza2za2_78));
							BgL_arg1629z00_1576 =
								BGl_callccz12z12zzcallcc_walkz00(
								((BgL_nodez00_bglt) BgL_arg1630z00_1577));
						}
						{	/* Callcc/walk.scm 363 */
							obj_t BgL_auxz00_3141;
							obj_t BgL_tmpz00_3139;

							BgL_auxz00_3141 = ((obj_t) BgL_arg1629z00_1576);
							BgL_tmpz00_3139 = ((obj_t) BgL_nodeza2za2_78);
							SET_CAR(BgL_tmpz00_3139, BgL_auxz00_3141);
						}
					}
					{	/* Callcc/walk.scm 364 */
						obj_t BgL_arg1642z00_1578;

						BgL_arg1642z00_1578 = CDR(((obj_t) BgL_nodeza2za2_78));
						{
							obj_t BgL_nodeza2za2_3146;

							BgL_nodeza2za2_3146 = BgL_arg1642z00_1578;
							BgL_nodeza2za2_78 = BgL_nodeza2za2_3146;
							goto BGl_callccza2z12zb0zzcallcc_walkz00;
						}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcallcc_walkz00(void)
	{
		{	/* Callcc/walk.scm 16 */
			{	/* Callcc/walk.scm 25 */
				obj_t BgL_arg1661z00_1583;
				obj_t BgL_arg1663z00_1584;

				{	/* Callcc/walk.scm 25 */
					obj_t BgL_v1316z00_1618;

					BgL_v1316z00_1618 = create_vector(0L);
					BgL_arg1661z00_1583 = BgL_v1316z00_1618;
				}
				{	/* Callcc/walk.scm 25 */
					obj_t BgL_v1317z00_1619;

					BgL_v1317z00_1619 = create_vector(0L);
					BgL_arg1663z00_1584 = BgL_v1317z00_1619;
				}
				return (BGl_localzf2cellzf2zzcallcc_walkz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(4),
						CNST_TABLE_REF(5), BGl_localz00zzast_varz00, 4868L,
						BGl_proc1929z00zzcallcc_walkz00, BGl_proc1928z00zzcallcc_walkz00,
						BFALSE, BGl_proc1927z00zzcallcc_walkz00,
						BGl_proc1926z00zzcallcc_walkz00, BgL_arg1661z00_1583,
						BgL_arg1663z00_1584), BUNSPEC);
			}
		}

	}



/* &lambda1683 */
	BgL_localz00_bglt BGl_z62lambda1683z62zzcallcc_walkz00(obj_t BgL_envz00_2482,
		obj_t BgL_o1120z00_2483)
	{
		{	/* Callcc/walk.scm 25 */
			{	/* Callcc/walk.scm 25 */
				long BgL_arg1688z00_2582;

				{	/* Callcc/walk.scm 25 */
					obj_t BgL_arg1689z00_2583;

					{	/* Callcc/walk.scm 25 */
						obj_t BgL_arg1691z00_2584;

						{	/* Callcc/walk.scm 25 */
							obj_t BgL_arg1815z00_2585;
							long BgL_arg1816z00_2586;

							BgL_arg1815z00_2585 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Callcc/walk.scm 25 */
								long BgL_arg1817z00_2587;

								BgL_arg1817z00_2587 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1120z00_2483)));
								BgL_arg1816z00_2586 = (BgL_arg1817z00_2587 - OBJECT_TYPE);
							}
							BgL_arg1691z00_2584 =
								VECTOR_REF(BgL_arg1815z00_2585, BgL_arg1816z00_2586);
						}
						BgL_arg1689z00_2583 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1691z00_2584);
					}
					{	/* Callcc/walk.scm 25 */
						obj_t BgL_tmpz00_3159;

						BgL_tmpz00_3159 = ((obj_t) BgL_arg1689z00_2583);
						BgL_arg1688z00_2582 = BGL_CLASS_NUM(BgL_tmpz00_3159);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1120z00_2483)), BgL_arg1688z00_2582);
			}
			{	/* Callcc/walk.scm 25 */
				BgL_objectz00_bglt BgL_tmpz00_3165;

				BgL_tmpz00_3165 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1120z00_2483));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3165, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1120z00_2483));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1120z00_2483));
		}

	}



/* &<@anonymous:1682> */
	obj_t BGl_z62zc3z04anonymousza31682ze3ze5zzcallcc_walkz00(obj_t
		BgL_envz00_2484, obj_t BgL_new1119z00_2485)
	{
		{	/* Callcc/walk.scm 25 */
			{
				BgL_localz00_bglt BgL_auxz00_3173;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1119z00_2485))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(6)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2485))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3181;

					{	/* Callcc/walk.scm 25 */
						obj_t BgL_classz00_2589;

						BgL_classz00_2589 = BGl_typez00zztype_typez00;
						{	/* Callcc/walk.scm 25 */
							obj_t BgL__ortest_1117z00_2590;

							BgL__ortest_1117z00_2590 = BGL_CLASS_NIL(BgL_classz00_2589);
							if (CBOOL(BgL__ortest_1117z00_2590))
								{	/* Callcc/walk.scm 25 */
									BgL_auxz00_3181 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_2590);
								}
							else
								{	/* Callcc/walk.scm 25 */
									BgL_auxz00_3181 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2589));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1119z00_2485))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_3181), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_3191;

					{	/* Callcc/walk.scm 25 */
						obj_t BgL_classz00_2591;

						BgL_classz00_2591 = BGl_valuez00zzast_varz00;
						{	/* Callcc/walk.scm 25 */
							obj_t BgL__ortest_1117z00_2592;

							BgL__ortest_1117z00_2592 = BGL_CLASS_NIL(BgL_classz00_2591);
							if (CBOOL(BgL__ortest_1117z00_2592))
								{	/* Callcc/walk.scm 25 */
									BgL_auxz00_3191 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_2592);
								}
							else
								{	/* Callcc/walk.scm 25 */
									BgL_auxz00_3191 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2591));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1119z00_2485))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_3191), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1119z00_2485))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2485))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2485))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2485))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2485))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2485))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2485))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2485))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1119z00_2485))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_3173 = ((BgL_localz00_bglt) BgL_new1119z00_2485);
				return ((obj_t) BgL_auxz00_3173);
			}
		}

	}



/* &lambda1679 */
	BgL_localz00_bglt BGl_z62lambda1679z62zzcallcc_walkz00(obj_t BgL_envz00_2486,
		obj_t BgL_o1116z00_2487)
	{
		{	/* Callcc/walk.scm 25 */
			{	/* Callcc/walk.scm 25 */
				BgL_localzf2cellzf2_bglt BgL_wide1118z00_2594;

				BgL_wide1118z00_2594 =
					((BgL_localzf2cellzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2cellzf2_bgl))));
				{	/* Callcc/walk.scm 25 */
					obj_t BgL_auxz00_3235;
					BgL_objectz00_bglt BgL_tmpz00_3231;

					BgL_auxz00_3235 = ((obj_t) BgL_wide1118z00_2594);
					BgL_tmpz00_3231 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1116z00_2487)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3231, BgL_auxz00_3235);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1116z00_2487)));
				{	/* Callcc/walk.scm 25 */
					long BgL_arg1681z00_2595;

					BgL_arg1681z00_2595 =
						BGL_CLASS_NUM(BGl_localzf2cellzf2zzcallcc_walkz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1116z00_2487))), BgL_arg1681z00_2595);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1116z00_2487)));
			}
		}

	}



/* &lambda1664 */
	BgL_localz00_bglt BGl_z62lambda1664z62zzcallcc_walkz00(obj_t BgL_envz00_2488,
		obj_t BgL_id1103z00_2489, obj_t BgL_name1104z00_2490,
		obj_t BgL_type1105z00_2491, obj_t BgL_value1106z00_2492,
		obj_t BgL_access1107z00_2493, obj_t BgL_fastzd2alpha1108zd2_2494,
		obj_t BgL_removable1109z00_2495, obj_t BgL_occurrence1110z00_2496,
		obj_t BgL_occurrencew1111z00_2497, obj_t BgL_userzf31112zf3_2498,
		obj_t BgL_key1113z00_2499, obj_t BgL_valzd2noescape1114zd2_2500,
		obj_t BgL_volatile1115z00_2501)
	{
		{	/* Callcc/walk.scm 25 */
			{	/* Callcc/walk.scm 25 */
				long BgL_occurrence1110z00_2599;
				long BgL_occurrencew1111z00_2600;
				bool_t BgL_userzf31112zf3_2601;
				long BgL_key1113z00_2602;
				bool_t BgL_volatile1115z00_2603;

				BgL_occurrence1110z00_2599 = (long) CINT(BgL_occurrence1110z00_2496);
				BgL_occurrencew1111z00_2600 = (long) CINT(BgL_occurrencew1111z00_2497);
				BgL_userzf31112zf3_2601 = CBOOL(BgL_userzf31112zf3_2498);
				BgL_key1113z00_2602 = (long) CINT(BgL_key1113z00_2499);
				BgL_volatile1115z00_2603 = CBOOL(BgL_volatile1115z00_2501);
				{	/* Callcc/walk.scm 25 */
					BgL_localz00_bglt BgL_new1178z00_2604;

					{	/* Callcc/walk.scm 25 */
						BgL_localz00_bglt BgL_tmp1176z00_2605;
						BgL_localzf2cellzf2_bglt BgL_wide1177z00_2606;

						{
							BgL_localz00_bglt BgL_auxz00_3254;

							{	/* Callcc/walk.scm 25 */
								BgL_localz00_bglt BgL_new1175z00_2607;

								BgL_new1175z00_2607 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Callcc/walk.scm 25 */
									long BgL_arg1678z00_2608;

									BgL_arg1678z00_2608 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1175z00_2607),
										BgL_arg1678z00_2608);
								}
								{	/* Callcc/walk.scm 25 */
									BgL_objectz00_bglt BgL_tmpz00_3259;

									BgL_tmpz00_3259 = ((BgL_objectz00_bglt) BgL_new1175z00_2607);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3259, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1175z00_2607);
								BgL_auxz00_3254 = BgL_new1175z00_2607;
							}
							BgL_tmp1176z00_2605 = ((BgL_localz00_bglt) BgL_auxz00_3254);
						}
						BgL_wide1177z00_2606 =
							((BgL_localzf2cellzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2cellzf2_bgl))));
						{	/* Callcc/walk.scm 25 */
							obj_t BgL_auxz00_3267;
							BgL_objectz00_bglt BgL_tmpz00_3265;

							BgL_auxz00_3267 = ((obj_t) BgL_wide1177z00_2606);
							BgL_tmpz00_3265 = ((BgL_objectz00_bglt) BgL_tmp1176z00_2605);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3265, BgL_auxz00_3267);
						}
						((BgL_objectz00_bglt) BgL_tmp1176z00_2605);
						{	/* Callcc/walk.scm 25 */
							long BgL_arg1675z00_2609;

							BgL_arg1675z00_2609 =
								BGL_CLASS_NUM(BGl_localzf2cellzf2zzcallcc_walkz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1176z00_2605),
								BgL_arg1675z00_2609);
						}
						BgL_new1178z00_2604 = ((BgL_localz00_bglt) BgL_tmp1176z00_2605);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1178z00_2604)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1103z00_2489)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1178z00_2604)))->BgL_namez00) =
						((obj_t) BgL_name1104z00_2490), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1178z00_2604)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1105z00_2491)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1178z00_2604)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1106z00_2492)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1178z00_2604)))->BgL_accessz00) =
						((obj_t) BgL_access1107z00_2493), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1178z00_2604)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1108zd2_2494), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1178z00_2604)))->BgL_removablez00) =
						((obj_t) BgL_removable1109z00_2495), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1178z00_2604)))->BgL_occurrencez00) =
						((long) BgL_occurrence1110z00_2599), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1178z00_2604)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1111z00_2600), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1178z00_2604)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31112zf3_2601), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1178z00_2604)))->BgL_keyz00) =
						((long) BgL_key1113z00_2602), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1178z00_2604)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1114zd2_2500), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1178z00_2604)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1115z00_2603), BUNSPEC);
					return BgL_new1178z00_2604;
				}
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcallcc_walkz00(void)
	{
		{	/* Callcc/walk.scm 16 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00,
				BGl_proc1930z00zzcallcc_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string1931z00zzcallcc_walkz00);
		}

	}



/* &callcc!1318 */
	obj_t BGl_z62callccz121318z70zzcallcc_walkz00(obj_t BgL_envz00_2503,
		obj_t BgL_nodez00_2504)
	{
		{	/* Callcc/walk.scm 124 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(7),
				BGl_string1932z00zzcallcc_walkz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2504)));
		}

	}



/* callcc! */
	BgL_nodez00_bglt BGl_callccz12z12zzcallcc_walkz00(BgL_nodez00_bglt
		BgL_nodez00_56)
	{
		{	/* Callcc/walk.scm 124 */
			{	/* Callcc/walk.scm 124 */
				obj_t BgL_method1319z00_1628;

				{	/* Callcc/walk.scm 124 */
					obj_t BgL_res1909z00_2158;

					{	/* Callcc/walk.scm 124 */
						long BgL_objzd2classzd2numz00_2129;

						BgL_objzd2classzd2numz00_2129 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_56));
						{	/* Callcc/walk.scm 124 */
							obj_t BgL_arg1811z00_2130;

							BgL_arg1811z00_2130 =
								PROCEDURE_REF(BGl_callccz12zd2envzc0zzcallcc_walkz00,
								(int) (1L));
							{	/* Callcc/walk.scm 124 */
								int BgL_offsetz00_2133;

								BgL_offsetz00_2133 = (int) (BgL_objzd2classzd2numz00_2129);
								{	/* Callcc/walk.scm 124 */
									long BgL_offsetz00_2134;

									BgL_offsetz00_2134 =
										((long) (BgL_offsetz00_2133) - OBJECT_TYPE);
									{	/* Callcc/walk.scm 124 */
										long BgL_modz00_2135;

										BgL_modz00_2135 =
											(BgL_offsetz00_2134 >> (int) ((long) ((int) (4L))));
										{	/* Callcc/walk.scm 124 */
											long BgL_restz00_2137;

											BgL_restz00_2137 =
												(BgL_offsetz00_2134 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Callcc/walk.scm 124 */

												{	/* Callcc/walk.scm 124 */
													obj_t BgL_bucketz00_2139;

													BgL_bucketz00_2139 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2130), BgL_modz00_2135);
													BgL_res1909z00_2158 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2139), BgL_restz00_2137);
					}}}}}}}}
					BgL_method1319z00_1628 = BgL_res1909z00_2158;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1319z00_1628,
						((obj_t) BgL_nodez00_56)));
			}
		}

	}



/* &callcc! */
	BgL_nodez00_bglt BGl_z62callccz12z70zzcallcc_walkz00(obj_t BgL_envz00_2505,
		obj_t BgL_nodez00_2506)
	{
		{	/* Callcc/walk.scm 124 */
			return
				BGl_callccz12z12zzcallcc_walkz00(((BgL_nodez00_bglt) BgL_nodez00_2506));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcallcc_walkz00(void)
	{
		{	/* Callcc/walk.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_atomz00zzast_nodez00,
				BGl_proc1933z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_kwotez00zzast_nodez00,
				BGl_proc1935z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_varz00zzast_nodez00,
				BGl_proc1936z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc1937z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc1938z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc1939z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc1940z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc1941z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc1942z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc1943z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc1944z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1945z00zzcallcc_walkz00,
				BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc1946z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc1947z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc1948z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1949z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1950z00zzcallcc_walkz00,
				BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1951z00zzcallcc_walkz00,
				BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc1952z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc1953z00zzcallcc_walkz00, BGl_string1934z00zzcallcc_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_callccz12zd2envzc0zzcallcc_walkz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1954z00zzcallcc_walkz00,
				BGl_string1934z00zzcallcc_walkz00);
		}

	}



/* &callcc!-box-set!1362 */
	BgL_nodez00_bglt BGl_z62callccz12zd2boxzd2setz121362z62zzcallcc_walkz00(obj_t
		BgL_envz00_2528, obj_t BgL_nodez00_2529)
	{
		{	/* Callcc/walk.scm 351 */
			{
				BgL_nodez00_bglt BgL_auxz00_3363;

				{	/* Callcc/walk.scm 353 */
					BgL_nodez00_bglt BgL_arg1839z00_2612;

					BgL_arg1839z00_2612 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2529)))->BgL_valuez00);
					BgL_auxz00_3363 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1839z00_2612);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2529)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3363), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2529));
		}

	}



/* &callcc!-box-ref1360 */
	BgL_nodez00_bglt BGl_z62callccz12zd2boxzd2ref1360z70zzcallcc_walkz00(obj_t
		BgL_envz00_2530, obj_t BgL_nodez00_2531)
	{
		{	/* Callcc/walk.scm 345 */
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2531));
		}

	}



/* &callcc!-make-box1358 */
	BgL_nodez00_bglt BGl_z62callccz12zd2makezd2box1358z70zzcallcc_walkz00(obj_t
		BgL_envz00_2532, obj_t BgL_nodez00_2533)
	{
		{	/* Callcc/walk.scm 338 */
			((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2533)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_callccz12z12zzcallcc_walkz00((((BgL_makezd2boxzd2_bglt)
								COBJECT(((BgL_makezd2boxzd2_bglt) BgL_nodez00_2533)))->
							BgL_valuez00))), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2533));
		}

	}



/* &callcc!-jump-ex-it1356 */
	BgL_nodez00_bglt
		BGl_z62callccz12zd2jumpzd2exzd2it1356za2zzcallcc_walkz00(obj_t
		BgL_envz00_2534, obj_t BgL_nodez00_2535)
	{
		{	/* Callcc/walk.scm 329 */
			{
				BgL_nodez00_bglt BgL_auxz00_3380;

				{	/* Callcc/walk.scm 331 */
					BgL_nodez00_bglt BgL_arg1835z00_2616;

					BgL_arg1835z00_2616 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2535)))->BgL_exitz00);
					BgL_auxz00_3380 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1835z00_2616);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2535)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3380), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3386;

				{	/* Callcc/walk.scm 332 */
					BgL_nodez00_bglt BgL_arg1836z00_2617;

					BgL_arg1836z00_2617 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2535)))->
						BgL_valuez00);
					BgL_auxz00_3386 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1836z00_2617);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2535)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_3386), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2535));
		}

	}



/* &callcc!-set-ex-it1354 */
	BgL_nodez00_bglt BGl_z62callccz12zd2setzd2exzd2it1354za2zzcallcc_walkz00(obj_t
		BgL_envz00_2536, obj_t BgL_nodez00_2537)
	{
		{	/* Callcc/walk.scm 321 */
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2537)))->BgL_onexitz00) =
				((BgL_nodez00_bglt)
					BGl_callccz12z12zzcallcc_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2537)))->
							BgL_onexitz00))), BUNSPEC);
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
								BgL_nodez00_2537)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_callccz12z12zzcallcc_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2537)))->
							BgL_bodyz00))), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
					BgL_nodez00_2537));
		}

	}



/* &callcc!-let-var1352 */
	BgL_nodez00_bglt BGl_z62callccz12zd2letzd2var1352z70zzcallcc_walkz00(obj_t
		BgL_envz00_2538, obj_t BgL_nodez00_2539)
	{
		{	/* Callcc/walk.scm 303 */
			{
				BgL_nodez00_bglt BgL_auxz00_3406;

				{	/* Callcc/walk.scm 305 */
					BgL_nodez00_bglt BgL_arg1806z00_2620;

					BgL_arg1806z00_2620 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2539)))->BgL_bodyz00);
					BgL_auxz00_3406 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1806z00_2620);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2539)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3406), BUNSPEC);
			}
			{	/* Callcc/walk.scm 306 */
				obj_t BgL_g1315z00_2621;

				BgL_g1315z00_2621 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2539)))->BgL_bindingsz00);
				{
					obj_t BgL_l1313z00_2623;

					BgL_l1313z00_2623 = BgL_g1315z00_2621;
				BgL_zc3z04anonymousza31807ze3z87_2622:
					if (PAIRP(BgL_l1313z00_2623))
						{	/* Callcc/walk.scm 306 */
							{	/* Callcc/walk.scm 307 */
								obj_t BgL_bindingz00_2624;

								BgL_bindingz00_2624 = CAR(BgL_l1313z00_2623);
								{	/* Callcc/walk.scm 307 */
									obj_t BgL_varz00_2625;
									obj_t BgL_valz00_2626;

									BgL_varz00_2625 = CAR(((obj_t) BgL_bindingz00_2624));
									BgL_valz00_2626 = CDR(((obj_t) BgL_bindingz00_2624));
									{	/* Callcc/walk.scm 309 */
										BgL_nodez00_bglt BgL_arg1812z00_2627;

										BgL_arg1812z00_2627 =
											BGl_callccz12z12zzcallcc_walkz00(
											((BgL_nodez00_bglt) BgL_valz00_2626));
										{	/* Callcc/walk.scm 309 */
											obj_t BgL_auxz00_3425;
											obj_t BgL_tmpz00_3423;

											BgL_auxz00_3425 = ((obj_t) BgL_arg1812z00_2627);
											BgL_tmpz00_3423 = ((obj_t) BgL_bindingz00_2624);
											SET_CDR(BgL_tmpz00_3423, BgL_auxz00_3425);
										}
									}
									if (BGl_celledzf3zf3zzcallcc_walkz00(
											((BgL_variablez00_bglt) BgL_varz00_2625)))
										{	/* Callcc/walk.scm 310 */
											{	/* Callcc/walk.scm 313 */
												BgL_makezd2boxzd2_bglt BgL_arg1820z00_2628;

												{	/* Callcc/walk.scm 313 */
													obj_t BgL_arg1822z00_2629;

													BgL_arg1822z00_2629 =
														CDR(((obj_t) BgL_bindingz00_2624));
													BgL_arg1820z00_2628 =
														BGl_azd2makezd2cellz00zzcallcc_walkz00(
														((BgL_nodez00_bglt) BgL_arg1822z00_2629),
														((BgL_variablez00_bglt) BgL_varz00_2625));
												}
												{	/* Callcc/walk.scm 312 */
													obj_t BgL_auxz00_3438;
													obj_t BgL_tmpz00_3436;

													BgL_auxz00_3438 = ((obj_t) BgL_arg1820z00_2628);
													BgL_tmpz00_3436 = ((obj_t) BgL_bindingz00_2624);
													SET_CDR(BgL_tmpz00_3436, BgL_auxz00_3438);
												}
											}
											{	/* Callcc/walk.scm 314 */
												BgL_typez00_bglt BgL_vz00_2630;

												BgL_vz00_2630 =
													((BgL_typez00_bglt) BGl_za2cellza2z00zztype_cachez00);
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_varz00_2625))))->
														BgL_typez00) =
													((BgL_typez00_bglt) BgL_vz00_2630), BUNSPEC);
											}
										}
									else
										{	/* Callcc/walk.scm 310 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1313z00_3445;

								BgL_l1313z00_3445 = CDR(BgL_l1313z00_2623);
								BgL_l1313z00_2623 = BgL_l1313z00_3445;
								goto BgL_zc3z04anonymousza31807ze3z87_2622;
							}
						}
					else
						{	/* Callcc/walk.scm 306 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2539));
		}

	}



/* &callcc!-let-fun1350 */
	BgL_nodez00_bglt BGl_z62callccz12zd2letzd2fun1350z70zzcallcc_walkz00(obj_t
		BgL_envz00_2540, obj_t BgL_nodez00_2541)
	{
		{	/* Callcc/walk.scm 294 */
			{	/* Callcc/walk.scm 296 */
				obj_t BgL_g1312z00_2632;

				BgL_g1312z00_2632 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2541)))->BgL_localsz00);
				{
					obj_t BgL_l1310z00_2634;

					BgL_l1310z00_2634 = BgL_g1312z00_2632;
				BgL_zc3z04anonymousza31774ze3z87_2633:
					if (PAIRP(BgL_l1310z00_2634))
						{	/* Callcc/walk.scm 296 */
							BGl_callcczd2funz12zc0zzcallcc_walkz00(CAR(BgL_l1310z00_2634));
							{
								obj_t BgL_l1310z00_3455;

								BgL_l1310z00_3455 = CDR(BgL_l1310z00_2634);
								BgL_l1310z00_2634 = BgL_l1310z00_3455;
								goto BgL_zc3z04anonymousza31774ze3z87_2633;
							}
						}
					else
						{	/* Callcc/walk.scm 296 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3457;

				{	/* Callcc/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg1805z00_2635;

					BgL_arg1805z00_2635 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2541)))->BgL_bodyz00);
					BgL_auxz00_3457 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1805z00_2635);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2541)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3457), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2541));
		}

	}



/* &callcc!-switch1348 */
	BgL_nodez00_bglt BGl_z62callccz12zd2switch1348za2zzcallcc_walkz00(obj_t
		BgL_envz00_2542, obj_t BgL_nodez00_2543)
	{
		{	/* Callcc/walk.scm 283 */
			{
				BgL_nodez00_bglt BgL_auxz00_3465;

				{	/* Callcc/walk.scm 285 */
					BgL_nodez00_bglt BgL_arg1767z00_2637;

					BgL_arg1767z00_2637 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2543)))->BgL_testz00);
					BgL_auxz00_3465 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1767z00_2637);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2543)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3465), BUNSPEC);
			}
			{	/* Callcc/walk.scm 286 */
				obj_t BgL_g1309z00_2638;

				BgL_g1309z00_2638 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2543)))->BgL_clausesz00);
				{
					obj_t BgL_l1307z00_2640;

					BgL_l1307z00_2640 = BgL_g1309z00_2638;
				BgL_zc3z04anonymousza31768ze3z87_2639:
					if (PAIRP(BgL_l1307z00_2640))
						{	/* Callcc/walk.scm 286 */
							{	/* Callcc/walk.scm 287 */
								obj_t BgL_clausez00_2641;

								BgL_clausez00_2641 = CAR(BgL_l1307z00_2640);
								{	/* Callcc/walk.scm 287 */
									BgL_nodez00_bglt BgL_arg1770z00_2642;

									{	/* Callcc/walk.scm 287 */
										obj_t BgL_arg1771z00_2643;

										BgL_arg1771z00_2643 = CDR(((obj_t) BgL_clausez00_2641));
										BgL_arg1770z00_2642 =
											BGl_callccz12z12zzcallcc_walkz00(
											((BgL_nodez00_bglt) BgL_arg1771z00_2643));
									}
									{	/* Callcc/walk.scm 287 */
										obj_t BgL_auxz00_3482;
										obj_t BgL_tmpz00_3480;

										BgL_auxz00_3482 = ((obj_t) BgL_arg1770z00_2642);
										BgL_tmpz00_3480 = ((obj_t) BgL_clausez00_2641);
										SET_CDR(BgL_tmpz00_3480, BgL_auxz00_3482);
									}
								}
							}
							{
								obj_t BgL_l1307z00_3485;

								BgL_l1307z00_3485 = CDR(BgL_l1307z00_2640);
								BgL_l1307z00_2640 = BgL_l1307z00_3485;
								goto BgL_zc3z04anonymousza31768ze3z87_2639;
							}
						}
					else
						{	/* Callcc/walk.scm 286 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2543));
		}

	}



/* &callcc!-fail1346 */
	BgL_nodez00_bglt BGl_z62callccz12zd2fail1346za2zzcallcc_walkz00(obj_t
		BgL_envz00_2544, obj_t BgL_nodez00_2545)
	{
		{	/* Callcc/walk.scm 273 */
			{
				BgL_nodez00_bglt BgL_auxz00_3489;

				{	/* Callcc/walk.scm 275 */
					BgL_nodez00_bglt BgL_arg1761z00_2645;

					BgL_arg1761z00_2645 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2545)))->BgL_procz00);
					BgL_auxz00_3489 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1761z00_2645);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2545)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3489), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3495;

				{	/* Callcc/walk.scm 276 */
					BgL_nodez00_bglt BgL_arg1762z00_2646;

					BgL_arg1762z00_2646 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2545)))->BgL_msgz00);
					BgL_auxz00_3495 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1762z00_2646);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2545)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3495), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3501;

				{	/* Callcc/walk.scm 277 */
					BgL_nodez00_bglt BgL_arg1765z00_2647;

					BgL_arg1765z00_2647 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2545)))->BgL_objz00);
					BgL_auxz00_3501 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1765z00_2647);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2545)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3501), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2545));
		}

	}



/* &callcc!-conditional1344 */
	BgL_nodez00_bglt BGl_z62callccz12zd2conditional1344za2zzcallcc_walkz00(obj_t
		BgL_envz00_2546, obj_t BgL_nodez00_2547)
	{
		{	/* Callcc/walk.scm 263 */
			{
				BgL_nodez00_bglt BgL_auxz00_3509;

				{	/* Callcc/walk.scm 265 */
					BgL_nodez00_bglt BgL_arg1753z00_2649;

					BgL_arg1753z00_2649 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2547)))->BgL_testz00);
					BgL_auxz00_3509 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1753z00_2649);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2547)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3509), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3515;

				{	/* Callcc/walk.scm 266 */
					BgL_nodez00_bglt BgL_arg1754z00_2650;

					BgL_arg1754z00_2650 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2547)))->BgL_truez00);
					BgL_auxz00_3515 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1754z00_2650);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2547)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3515), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3521;

				{	/* Callcc/walk.scm 267 */
					BgL_nodez00_bglt BgL_arg1755z00_2651;

					BgL_arg1755z00_2651 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2547)))->BgL_falsez00);
					BgL_auxz00_3521 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1755z00_2651);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2547)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3521), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2547));
		}

	}



/* &callcc!-setq1342 */
	BgL_nodez00_bglt BGl_z62callccz12zd2setq1342za2zzcallcc_walkz00(obj_t
		BgL_envz00_2548, obj_t BgL_nodez00_2549)
	{
		{	/* Callcc/walk.scm 228 */
			((((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2549)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_callccz12z12zzcallcc_walkz00((((BgL_setqz00_bglt)
								COBJECT(((BgL_setqz00_bglt) BgL_nodez00_2549)))->
							BgL_valuez00))), BUNSPEC);
			{	/* Callcc/walk.scm 230 */
				BgL_variablez00_bglt BgL_varz00_2653;

				BgL_varz00_2653 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_2549)))->BgL_varz00)))->
					BgL_variablez00);
				{	/* Callcc/walk.scm 230 */
					BgL_typez00_bglt BgL_vtypez00_2654;

					BgL_vtypez00_2654 =
						(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2653))->BgL_typez00);
					{	/* Callcc/walk.scm 231 */

						{	/* Callcc/walk.scm 232 */
							obj_t BgL_g1161z00_2655;

							BgL_g1161z00_2655 =
								(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2653))->
								BgL_fastzd2alphazd2);
							{
								obj_t BgL_varz00_2657;
								obj_t BgL_alphaz00_2658;

								BgL_varz00_2657 = ((obj_t) BgL_varz00_2653);
								BgL_alphaz00_2658 = BgL_g1161z00_2655;
							BgL_loopz00_2656:
								{	/* Callcc/walk.scm 234 */
									bool_t BgL_test2029z00_3539;

									{	/* Callcc/walk.scm 234 */
										obj_t BgL_classz00_2659;

										BgL_classz00_2659 = BGl_localz00zzast_varz00;
										if (BGL_OBJECTP(BgL_alphaz00_2658))
											{	/* Callcc/walk.scm 234 */
												BgL_objectz00_bglt BgL_arg1807z00_2660;

												BgL_arg1807z00_2660 =
													(BgL_objectz00_bglt) (BgL_alphaz00_2658);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Callcc/walk.scm 234 */
														long BgL_idxz00_2661;

														BgL_idxz00_2661 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2660);
														BgL_test2029z00_3539 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2661 + 2L)) == BgL_classz00_2659);
													}
												else
													{	/* Callcc/walk.scm 234 */
														bool_t BgL_res1913z00_2664;

														{	/* Callcc/walk.scm 234 */
															obj_t BgL_oclassz00_2665;

															{	/* Callcc/walk.scm 234 */
																obj_t BgL_arg1815z00_2666;
																long BgL_arg1816z00_2667;

																BgL_arg1815z00_2666 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Callcc/walk.scm 234 */
																	long BgL_arg1817z00_2668;

																	BgL_arg1817z00_2668 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2660);
																	BgL_arg1816z00_2667 =
																		(BgL_arg1817z00_2668 - OBJECT_TYPE);
																}
																BgL_oclassz00_2665 =
																	VECTOR_REF(BgL_arg1815z00_2666,
																	BgL_arg1816z00_2667);
															}
															{	/* Callcc/walk.scm 234 */
																bool_t BgL__ortest_1115z00_2669;

																BgL__ortest_1115z00_2669 =
																	(BgL_classz00_2659 == BgL_oclassz00_2665);
																if (BgL__ortest_1115z00_2669)
																	{	/* Callcc/walk.scm 234 */
																		BgL_res1913z00_2664 =
																			BgL__ortest_1115z00_2669;
																	}
																else
																	{	/* Callcc/walk.scm 234 */
																		long BgL_odepthz00_2670;

																		{	/* Callcc/walk.scm 234 */
																			obj_t BgL_arg1804z00_2671;

																			BgL_arg1804z00_2671 =
																				(BgL_oclassz00_2665);
																			BgL_odepthz00_2670 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2671);
																		}
																		if ((2L < BgL_odepthz00_2670))
																			{	/* Callcc/walk.scm 234 */
																				obj_t BgL_arg1802z00_2672;

																				{	/* Callcc/walk.scm 234 */
																					obj_t BgL_arg1803z00_2673;

																					BgL_arg1803z00_2673 =
																						(BgL_oclassz00_2665);
																					BgL_arg1802z00_2672 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2673, 2L);
																				}
																				BgL_res1913z00_2664 =
																					(BgL_arg1802z00_2672 ==
																					BgL_classz00_2659);
																			}
																		else
																			{	/* Callcc/walk.scm 234 */
																				BgL_res1913z00_2664 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2029z00_3539 = BgL_res1913z00_2664;
													}
											}
										else
											{	/* Callcc/walk.scm 234 */
												BgL_test2029z00_3539 = ((bool_t) 0);
											}
									}
									if (BgL_test2029z00_3539)
										{	/* Callcc/walk.scm 234 */
											{	/* Callcc/walk.scm 236 */
												BgL_varz00_bglt BgL_arg1739z00_2674;

												BgL_arg1739z00_2674 =
													(((BgL_setqz00_bglt) COBJECT(
															((BgL_setqz00_bglt) BgL_nodez00_2549)))->
													BgL_varz00);
												((((BgL_varz00_bglt) COBJECT(BgL_arg1739z00_2674))->
														BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															BgL_alphaz00_2658)), BUNSPEC);
											}
											{	/* Callcc/walk.scm 237 */
												obj_t BgL_arg1740z00_2675;

												BgL_arg1740z00_2675 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_alphaz00_2658)))->
													BgL_fastzd2alphazd2);
												{
													obj_t BgL_alphaz00_3569;
													obj_t BgL_varz00_3568;

													BgL_varz00_3568 = BgL_alphaz00_2658;
													BgL_alphaz00_3569 = BgL_arg1740z00_2675;
													BgL_alphaz00_2658 = BgL_alphaz00_3569;
													BgL_varz00_2657 = BgL_varz00_3568;
													goto BgL_loopz00_2656;
												}
											}
										}
									else
										{	/* Callcc/walk.scm 239 */
											bool_t BgL_test2034z00_3570;

											{	/* Callcc/walk.scm 239 */
												obj_t BgL_classz00_2676;

												BgL_classz00_2676 = BGl_globalz00zzast_varz00;
												if (BGL_OBJECTP(BgL_varz00_2657))
													{	/* Callcc/walk.scm 239 */
														BgL_objectz00_bglt BgL_arg1807z00_2677;

														BgL_arg1807z00_2677 =
															(BgL_objectz00_bglt) (BgL_varz00_2657);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Callcc/walk.scm 239 */
																long BgL_idxz00_2678;

																BgL_idxz00_2678 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2677);
																BgL_test2034z00_3570 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2678 + 2L)) ==
																	BgL_classz00_2676);
															}
														else
															{	/* Callcc/walk.scm 239 */
																bool_t BgL_res1914z00_2681;

																{	/* Callcc/walk.scm 239 */
																	obj_t BgL_oclassz00_2682;

																	{	/* Callcc/walk.scm 239 */
																		obj_t BgL_arg1815z00_2683;
																		long BgL_arg1816z00_2684;

																		BgL_arg1815z00_2683 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Callcc/walk.scm 239 */
																			long BgL_arg1817z00_2685;

																			BgL_arg1817z00_2685 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2677);
																			BgL_arg1816z00_2684 =
																				(BgL_arg1817z00_2685 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2682 =
																			VECTOR_REF(BgL_arg1815z00_2683,
																			BgL_arg1816z00_2684);
																	}
																	{	/* Callcc/walk.scm 239 */
																		bool_t BgL__ortest_1115z00_2686;

																		BgL__ortest_1115z00_2686 =
																			(BgL_classz00_2676 == BgL_oclassz00_2682);
																		if (BgL__ortest_1115z00_2686)
																			{	/* Callcc/walk.scm 239 */
																				BgL_res1914z00_2681 =
																					BgL__ortest_1115z00_2686;
																			}
																		else
																			{	/* Callcc/walk.scm 239 */
																				long BgL_odepthz00_2687;

																				{	/* Callcc/walk.scm 239 */
																					obj_t BgL_arg1804z00_2688;

																					BgL_arg1804z00_2688 =
																						(BgL_oclassz00_2682);
																					BgL_odepthz00_2687 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2688);
																				}
																				if ((2L < BgL_odepthz00_2687))
																					{	/* Callcc/walk.scm 239 */
																						obj_t BgL_arg1802z00_2689;

																						{	/* Callcc/walk.scm 239 */
																							obj_t BgL_arg1803z00_2690;

																							BgL_arg1803z00_2690 =
																								(BgL_oclassz00_2682);
																							BgL_arg1802z00_2689 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2690, 2L);
																						}
																						BgL_res1914z00_2681 =
																							(BgL_arg1802z00_2689 ==
																							BgL_classz00_2676);
																					}
																				else
																					{	/* Callcc/walk.scm 239 */
																						BgL_res1914z00_2681 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2034z00_3570 = BgL_res1914z00_2681;
															}
													}
												else
													{	/* Callcc/walk.scm 239 */
														BgL_test2034z00_3570 = ((bool_t) 0);
													}
											}
											if (BgL_test2034z00_3570)
												{	/* Callcc/walk.scm 239 */
													return
														((BgL_nodez00_bglt)
														((BgL_setqz00_bglt) BgL_nodez00_2549));
												}
											else
												{	/* Callcc/walk.scm 239 */
													if (BGl_celledzf3zf3zzcallcc_walkz00(
															((BgL_variablez00_bglt) BgL_varz00_2657)))
														{	/* Callcc/walk.scm 244 */
															BgL_localz00_bglt BgL_azd2varzd2_2691;
															obj_t BgL_locz00_2692;

															BgL_azd2varzd2_2691 =
																BGl_makezd2localzd2svarz00zzast_localz00
																(CNST_TABLE_REF(8),
																((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00));
															BgL_locz00_2692 =
																(((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt) (
																				(BgL_setqz00_bglt)
																				BgL_nodez00_2549))))->BgL_locz00);
															{	/* Callcc/walk.scm 246 */
																BgL_letzd2varzd2_bglt BgL_new1163z00_2693;

																{	/* Callcc/walk.scm 248 */
																	BgL_letzd2varzd2_bglt BgL_new1162z00_2694;

																	BgL_new1162z00_2694 =
																		((BgL_letzd2varzd2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_letzd2varzd2_bgl))));
																	{	/* Callcc/walk.scm 248 */
																		long BgL_arg1751z00_2695;

																		BgL_arg1751z00_2695 =
																			BGL_CLASS_NUM
																			(BGl_letzd2varzd2zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1162z00_2694),
																			BgL_arg1751z00_2695);
																	}
																	{	/* Callcc/walk.scm 248 */
																		BgL_objectz00_bglt BgL_tmpz00_3608;

																		BgL_tmpz00_3608 =
																			((BgL_objectz00_bglt)
																			BgL_new1162z00_2694);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3608,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1162z00_2694);
																	BgL_new1163z00_2693 = BgL_new1162z00_2694;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1163z00_2693)))->BgL_locz00) =
																	((obj_t) BgL_locz00_2692), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1163z00_2693)))->BgL_typez00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BGl_za2unspecza2z00zztype_cachez00)),
																	BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1163z00_2693)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1163z00_2693)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																{
																	obj_t BgL_auxz00_3622;

																	{	/* Callcc/walk.scm 249 */
																		obj_t BgL_arg1746z00_2696;

																		{	/* Callcc/walk.scm 249 */
																			BgL_nodez00_bglt BgL_arg1748z00_2697;

																			BgL_arg1748z00_2697 =
																				(((BgL_setqz00_bglt) COBJECT(
																						((BgL_setqz00_bglt)
																							BgL_nodez00_2549)))->
																				BgL_valuez00);
																			BgL_arg1746z00_2696 =
																				MAKE_YOUNG_PAIR(((obj_t)
																					BgL_azd2varzd2_2691),
																				((obj_t) BgL_arg1748z00_2697));
																		}
																		{	/* Callcc/walk.scm 249 */
																			obj_t BgL_list1747z00_2698;

																			BgL_list1747z00_2698 =
																				MAKE_YOUNG_PAIR(BgL_arg1746z00_2696,
																				BNIL);
																			BgL_auxz00_3622 = BgL_list1747z00_2698;
																	}}
																	((((BgL_letzd2varzd2_bglt)
																				COBJECT(BgL_new1163z00_2693))->
																			BgL_bindingsz00) =
																		((obj_t) BgL_auxz00_3622), BUNSPEC);
																}
																{
																	BgL_nodez00_bglt BgL_auxz00_3630;

																	{	/* Callcc/walk.scm 250 */
																		BgL_boxzd2setz12zc0_bglt
																			BgL_new1165z00_2699;
																		{	/* Callcc/walk.scm 253 */
																			BgL_boxzd2setz12zc0_bglt
																				BgL_new1164z00_2700;
																			BgL_new1164z00_2700 =
																				((BgL_boxzd2setz12zc0_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_boxzd2setz12zc0_bgl))));
																			{	/* Callcc/walk.scm 253 */
																				long BgL_arg1750z00_2701;

																				{	/* Callcc/walk.scm 253 */
																					obj_t BgL_classz00_2702;

																					BgL_classz00_2702 =
																						BGl_boxzd2setz12zc0zzast_nodez00;
																					BgL_arg1750z00_2701 =
																						BGL_CLASS_NUM(BgL_classz00_2702);
																				}
																				BGL_OBJECT_CLASS_NUM_SET(
																					((BgL_objectz00_bglt)
																						BgL_new1164z00_2700),
																					BgL_arg1750z00_2701);
																			}
																			{	/* Callcc/walk.scm 253 */
																				BgL_objectz00_bglt BgL_tmpz00_3635;

																				BgL_tmpz00_3635 =
																					((BgL_objectz00_bglt)
																					BgL_new1164z00_2700);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3635,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1164z00_2700);
																			BgL_new1165z00_2699 = BgL_new1164z00_2700;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1165z00_2699)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_2692), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1165z00_2699)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2unspecza2z00zztype_cachez00)),
																			BUNSPEC);
																		((((BgL_boxzd2setz12zc0_bglt)
																					COBJECT(BgL_new1165z00_2699))->
																				BgL_varz00) =
																			((BgL_varz00_bglt) (((BgL_setqz00_bglt)
																						COBJECT(((BgL_setqz00_bglt)
																								BgL_nodez00_2549)))->
																					BgL_varz00)), BUNSPEC);
																		{
																			BgL_nodez00_bglt BgL_auxz00_3647;

																			{	/* Callcc/walk.scm 255 */
																				BgL_refz00_bglt BgL_new1167z00_2703;

																				{	/* Callcc/walk.scm 257 */
																					BgL_refz00_bglt BgL_new1166z00_2704;

																					BgL_new1166z00_2704 =
																						((BgL_refz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_refz00_bgl))));
																					{	/* Callcc/walk.scm 257 */
																						long BgL_arg1749z00_2705;

																						{	/* Callcc/walk.scm 257 */
																							obj_t BgL_classz00_2706;

																							BgL_classz00_2706 =
																								BGl_refz00zzast_nodez00;
																							BgL_arg1749z00_2705 =
																								BGL_CLASS_NUM
																								(BgL_classz00_2706);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1166z00_2704),
																							BgL_arg1749z00_2705);
																					}
																					{	/* Callcc/walk.scm 257 */
																						BgL_objectz00_bglt BgL_tmpz00_3652;

																						BgL_tmpz00_3652 =
																							((BgL_objectz00_bglt)
																							BgL_new1166z00_2704);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_3652, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1166z00_2704);
																					BgL_new1167z00_2703 =
																						BgL_new1166z00_2704;
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1167z00_2703)))->
																						BgL_locz00) =
																					((obj_t) BgL_locz00_2692), BUNSPEC);
																				((((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_new1167z00_2703)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) ((
																								(BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_azd2varzd2_2691)))->
																							BgL_typez00)), BUNSPEC);
																				((((BgL_varz00_bglt)
																							COBJECT(((BgL_varz00_bglt)
																									BgL_new1167z00_2703)))->
																						BgL_variablez00) =
																					((BgL_variablez00_bglt) (
																							(BgL_variablez00_bglt)
																							BgL_azd2varzd2_2691)), BUNSPEC);
																				BgL_auxz00_3647 =
																					((BgL_nodez00_bglt)
																					BgL_new1167z00_2703);
																			}
																			((((BgL_boxzd2setz12zc0_bglt)
																						COBJECT(BgL_new1165z00_2699))->
																					BgL_valuez00) =
																				((BgL_nodez00_bglt) BgL_auxz00_3647),
																				BUNSPEC);
																		}
																		((((BgL_boxzd2setz12zc0_bglt)
																					COBJECT(BgL_new1165z00_2699))->
																				BgL_vtypez00) =
																			((BgL_typez00_bglt)
																				BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
																				(BgL_vtypez00_2654)), BUNSPEC);
																		BgL_auxz00_3630 =
																			((BgL_nodez00_bglt) BgL_new1165z00_2699);
																	}
																	((((BgL_letzd2varzd2_bglt)
																				COBJECT(BgL_new1163z00_2693))->
																			BgL_bodyz00) =
																		((BgL_nodez00_bglt) BgL_auxz00_3630),
																		BUNSPEC);
																}
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1163z00_2693))->
																		BgL_removablezf3zf3) =
																	((bool_t) ((bool_t) 1)), BUNSPEC);
																return ((BgL_nodez00_bglt) BgL_new1163z00_2693);
															}
														}
													else
														{	/* Callcc/walk.scm 241 */
															return
																((BgL_nodez00_bglt)
																((BgL_setqz00_bglt) BgL_nodez00_2549));
														}
												}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &callcc!-cast1340 */
	BgL_nodez00_bglt BGl_z62callccz12zd2cast1340za2zzcallcc_walkz00(obj_t
		BgL_envz00_2550, obj_t BgL_nodez00_2551)
	{
		{	/* Callcc/walk.scm 221 */
			((((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2551)))->BgL_argz00) =
				((BgL_nodez00_bglt)
					BGl_callccz12z12zzcallcc_walkz00((((BgL_castz00_bglt)
								COBJECT(((BgL_castz00_bglt) BgL_nodez00_2551)))->BgL_argz00))),
				BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2551));
		}

	}



/* &callcc!-extern1337 */
	BgL_nodez00_bglt BGl_z62callccz12zd2extern1337za2zzcallcc_walkz00(obj_t
		BgL_envz00_2552, obj_t BgL_nodez00_2553)
	{
		{	/* Callcc/walk.scm 214 */
			BGl_callccza2z12zb0zzcallcc_walkz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2553)))->BgL_exprza2za2));
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2553));
		}

	}



/* &callcc!-funcall1335 */
	BgL_nodez00_bglt BGl_z62callccz12zd2funcall1335za2zzcallcc_walkz00(obj_t
		BgL_envz00_2554, obj_t BgL_nodez00_2555)
	{
		{	/* Callcc/walk.scm 205 */
			{
				BgL_nodez00_bglt BgL_auxz00_3688;

				{	/* Callcc/walk.scm 207 */
					BgL_nodez00_bglt BgL_arg1720z00_2710;

					BgL_arg1720z00_2710 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2555)))->BgL_funz00);
					BgL_auxz00_3688 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1720z00_2710);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2555)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3688), BUNSPEC);
			}
			BGl_callccza2z12zb0zzcallcc_walkz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2555)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2555));
		}

	}



/* &callcc!-app-ly1333 */
	BgL_nodez00_bglt BGl_z62callccz12zd2appzd2ly1333z70zzcallcc_walkz00(obj_t
		BgL_envz00_2556, obj_t BgL_nodez00_2557)
	{
		{	/* Callcc/walk.scm 196 */
			{
				BgL_nodez00_bglt BgL_auxz00_3699;

				{	/* Callcc/walk.scm 198 */
					BgL_nodez00_bglt BgL_arg1717z00_2712;

					BgL_arg1717z00_2712 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2557)))->BgL_funz00);
					BgL_auxz00_3699 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1717z00_2712);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2557)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3699), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3705;

				{	/* Callcc/walk.scm 199 */
					BgL_nodez00_bglt BgL_arg1718z00_2713;

					BgL_arg1718z00_2713 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2557)))->BgL_argz00);
					BgL_auxz00_3705 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1718z00_2713);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2557)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3705), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2557));
		}

	}



/* &callcc!-app1331 */
	BgL_nodez00_bglt BGl_z62callccz12zd2app1331za2zzcallcc_walkz00(obj_t
		BgL_envz00_2558, obj_t BgL_nodez00_2559)
	{
		{	/* Callcc/walk.scm 188 */
			BGl_callccza2z12zb0zzcallcc_walkz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2559)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2559));
		}

	}



/* &callcc!-sync1329 */
	BgL_nodez00_bglt BGl_z62callccz12zd2sync1329za2zzcallcc_walkz00(obj_t
		BgL_envz00_2560, obj_t BgL_nodez00_2561)
	{
		{	/* Callcc/walk.scm 178 */
			{
				BgL_nodez00_bglt BgL_auxz00_3718;

				{	/* Callcc/walk.scm 180 */
					BgL_nodez00_bglt BgL_arg1709z00_2716;

					BgL_arg1709z00_2716 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2561)))->BgL_mutexz00);
					BgL_auxz00_3718 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1709z00_2716);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2561)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3718), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3724;

				{	/* Callcc/walk.scm 181 */
					BgL_nodez00_bglt BgL_arg1710z00_2717;

					BgL_arg1710z00_2717 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2561)))->BgL_prelockz00);
					BgL_auxz00_3724 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1710z00_2717);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2561)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3724), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3730;

				{	/* Callcc/walk.scm 182 */
					BgL_nodez00_bglt BgL_arg1711z00_2718;

					BgL_arg1711z00_2718 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2561)))->BgL_bodyz00);
					BgL_auxz00_3730 =
						BGl_callccz12z12zzcallcc_walkz00(BgL_arg1711z00_2718);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2561)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3730), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2561));
		}

	}



/* &callcc!-sequence1327 */
	BgL_nodez00_bglt BGl_z62callccz12zd2sequence1327za2zzcallcc_walkz00(obj_t
		BgL_envz00_2562, obj_t BgL_nodez00_2563)
	{
		{	/* Callcc/walk.scm 171 */
			BGl_callccza2z12zb0zzcallcc_walkz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2563)))->BgL_nodesz00));
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2563));
		}

	}



/* &callcc!-var1325 */
	BgL_nodez00_bglt BGl_z62callccz12zd2var1325za2zzcallcc_walkz00(obj_t
		BgL_envz00_2564, obj_t BgL_nodez00_2565)
	{
		{	/* Callcc/walk.scm 141 */
			{	/* Callcc/walk.scm 142 */
				BgL_variablez00_bglt BgL_varz00_2721;

				BgL_varz00_2721 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2565)))->BgL_variablez00);
				{	/* Callcc/walk.scm 142 */
					BgL_typez00_bglt BgL_vtypez00_2722;

					BgL_vtypez00_2722 =
						(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2721))->BgL_typez00);
					{	/* Callcc/walk.scm 143 */

						{	/* Callcc/walk.scm 144 */
							obj_t BgL_g1153z00_2723;

							BgL_g1153z00_2723 =
								(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2721))->
								BgL_fastzd2alphazd2);
							{
								obj_t BgL_varz00_2725;
								obj_t BgL_alphaz00_2726;

								BgL_varz00_2725 = ((obj_t) BgL_varz00_2721);
								BgL_alphaz00_2726 = BgL_g1153z00_2723;
							BgL_loopz00_2724:
								{	/* Callcc/walk.scm 146 */
									bool_t BgL_test2040z00_3747;

									{	/* Callcc/walk.scm 146 */
										obj_t BgL_classz00_2727;

										BgL_classz00_2727 = BGl_localz00zzast_varz00;
										if (BGL_OBJECTP(BgL_alphaz00_2726))
											{	/* Callcc/walk.scm 146 */
												BgL_objectz00_bglt BgL_arg1807z00_2728;

												BgL_arg1807z00_2728 =
													(BgL_objectz00_bglt) (BgL_alphaz00_2726);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Callcc/walk.scm 146 */
														long BgL_idxz00_2729;

														BgL_idxz00_2729 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2728);
														BgL_test2040z00_3747 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2729 + 2L)) == BgL_classz00_2727);
													}
												else
													{	/* Callcc/walk.scm 146 */
														bool_t BgL_res1910z00_2732;

														{	/* Callcc/walk.scm 146 */
															obj_t BgL_oclassz00_2733;

															{	/* Callcc/walk.scm 146 */
																obj_t BgL_arg1815z00_2734;
																long BgL_arg1816z00_2735;

																BgL_arg1815z00_2734 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Callcc/walk.scm 146 */
																	long BgL_arg1817z00_2736;

																	BgL_arg1817z00_2736 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2728);
																	BgL_arg1816z00_2735 =
																		(BgL_arg1817z00_2736 - OBJECT_TYPE);
																}
																BgL_oclassz00_2733 =
																	VECTOR_REF(BgL_arg1815z00_2734,
																	BgL_arg1816z00_2735);
															}
															{	/* Callcc/walk.scm 146 */
																bool_t BgL__ortest_1115z00_2737;

																BgL__ortest_1115z00_2737 =
																	(BgL_classz00_2727 == BgL_oclassz00_2733);
																if (BgL__ortest_1115z00_2737)
																	{	/* Callcc/walk.scm 146 */
																		BgL_res1910z00_2732 =
																			BgL__ortest_1115z00_2737;
																	}
																else
																	{	/* Callcc/walk.scm 146 */
																		long BgL_odepthz00_2738;

																		{	/* Callcc/walk.scm 146 */
																			obj_t BgL_arg1804z00_2739;

																			BgL_arg1804z00_2739 =
																				(BgL_oclassz00_2733);
																			BgL_odepthz00_2738 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2739);
																		}
																		if ((2L < BgL_odepthz00_2738))
																			{	/* Callcc/walk.scm 146 */
																				obj_t BgL_arg1802z00_2740;

																				{	/* Callcc/walk.scm 146 */
																					obj_t BgL_arg1803z00_2741;

																					BgL_arg1803z00_2741 =
																						(BgL_oclassz00_2733);
																					BgL_arg1802z00_2740 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2741, 2L);
																				}
																				BgL_res1910z00_2732 =
																					(BgL_arg1802z00_2740 ==
																					BgL_classz00_2727);
																			}
																		else
																			{	/* Callcc/walk.scm 146 */
																				BgL_res1910z00_2732 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2040z00_3747 = BgL_res1910z00_2732;
													}
											}
										else
											{	/* Callcc/walk.scm 146 */
												BgL_test2040z00_3747 = ((bool_t) 0);
											}
									}
									if (BgL_test2040z00_3747)
										{	/* Callcc/walk.scm 146 */
											((((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_nodez00_2565)))->
													BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_alphaz00_2726)), BUNSPEC);
											{	/* Callcc/walk.scm 149 */
												obj_t BgL_arg1699z00_2742;

												BgL_arg1699z00_2742 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_alphaz00_2726)))->
													BgL_fastzd2alphazd2);
												{
													obj_t BgL_alphaz00_3776;
													obj_t BgL_varz00_3775;

													BgL_varz00_3775 = BgL_alphaz00_2726;
													BgL_alphaz00_3776 = BgL_arg1699z00_2742;
													BgL_alphaz00_2726 = BgL_alphaz00_3776;
													BgL_varz00_2725 = BgL_varz00_3775;
													goto BgL_loopz00_2724;
												}
											}
										}
									else
										{	/* Callcc/walk.scm 151 */
											bool_t BgL_test2045z00_3777;

											{	/* Callcc/walk.scm 151 */
												obj_t BgL_classz00_2743;

												BgL_classz00_2743 = BGl_localz00zzast_varz00;
												if (BGL_OBJECTP(BgL_alphaz00_2726))
													{	/* Callcc/walk.scm 151 */
														BgL_objectz00_bglt BgL_arg1807z00_2744;

														BgL_arg1807z00_2744 =
															(BgL_objectz00_bglt) (BgL_alphaz00_2726);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Callcc/walk.scm 151 */
																long BgL_idxz00_2745;

																BgL_idxz00_2745 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2744);
																BgL_test2045z00_3777 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2745 + 2L)) ==
																	BgL_classz00_2743);
															}
														else
															{	/* Callcc/walk.scm 151 */
																bool_t BgL_res1911z00_2748;

																{	/* Callcc/walk.scm 151 */
																	obj_t BgL_oclassz00_2749;

																	{	/* Callcc/walk.scm 151 */
																		obj_t BgL_arg1815z00_2750;
																		long BgL_arg1816z00_2751;

																		BgL_arg1815z00_2750 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Callcc/walk.scm 151 */
																			long BgL_arg1817z00_2752;

																			BgL_arg1817z00_2752 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2744);
																			BgL_arg1816z00_2751 =
																				(BgL_arg1817z00_2752 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2749 =
																			VECTOR_REF(BgL_arg1815z00_2750,
																			BgL_arg1816z00_2751);
																	}
																	{	/* Callcc/walk.scm 151 */
																		bool_t BgL__ortest_1115z00_2753;

																		BgL__ortest_1115z00_2753 =
																			(BgL_classz00_2743 == BgL_oclassz00_2749);
																		if (BgL__ortest_1115z00_2753)
																			{	/* Callcc/walk.scm 151 */
																				BgL_res1911z00_2748 =
																					BgL__ortest_1115z00_2753;
																			}
																		else
																			{	/* Callcc/walk.scm 151 */
																				long BgL_odepthz00_2754;

																				{	/* Callcc/walk.scm 151 */
																					obj_t BgL_arg1804z00_2755;

																					BgL_arg1804z00_2755 =
																						(BgL_oclassz00_2749);
																					BgL_odepthz00_2754 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2755);
																				}
																				if ((2L < BgL_odepthz00_2754))
																					{	/* Callcc/walk.scm 151 */
																						obj_t BgL_arg1802z00_2756;

																						{	/* Callcc/walk.scm 151 */
																							obj_t BgL_arg1803z00_2757;

																							BgL_arg1803z00_2757 =
																								(BgL_oclassz00_2749);
																							BgL_arg1802z00_2756 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2757, 2L);
																						}
																						BgL_res1911z00_2748 =
																							(BgL_arg1802z00_2756 ==
																							BgL_classz00_2743);
																					}
																				else
																					{	/* Callcc/walk.scm 151 */
																						BgL_res1911z00_2748 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2045z00_3777 = BgL_res1911z00_2748;
															}
													}
												else
													{	/* Callcc/walk.scm 151 */
														BgL_test2045z00_3777 = ((bool_t) 0);
													}
											}
											if (BgL_test2045z00_3777)
												{	/* Callcc/walk.scm 151 */
													((((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_nodez00_2565)))->
															BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BgL_alphaz00_2726)), BUNSPEC);
													{	/* Callcc/walk.scm 153 */
														BgL_typez00_bglt BgL_arg1701z00_2758;

														BgL_arg1701z00_2758 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt) BgL_alphaz00_2726)))->
															BgL_typez00);
														((((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
																				BgL_nodez00_2565))))->BgL_typez00) =
															((BgL_typez00_bglt) BgL_arg1701z00_2758),
															BUNSPEC);
													}
													return
														BGl_callccz12z12zzcallcc_walkz00(
														((BgL_nodez00_bglt)
															((BgL_varz00_bglt) BgL_nodez00_2565)));
												}
											else
												{	/* Callcc/walk.scm 155 */
													bool_t BgL_test2050z00_3811;

													{	/* Callcc/walk.scm 155 */
														obj_t BgL_classz00_2759;

														BgL_classz00_2759 = BGl_globalz00zzast_varz00;
														if (BGL_OBJECTP(BgL_varz00_2725))
															{	/* Callcc/walk.scm 155 */
																BgL_objectz00_bglt BgL_arg1807z00_2760;

																BgL_arg1807z00_2760 =
																	(BgL_objectz00_bglt) (BgL_varz00_2725);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Callcc/walk.scm 155 */
																		long BgL_idxz00_2761;

																		BgL_idxz00_2761 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2760);
																		BgL_test2050z00_3811 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2761 + 2L)) ==
																			BgL_classz00_2759);
																	}
																else
																	{	/* Callcc/walk.scm 155 */
																		bool_t BgL_res1912z00_2764;

																		{	/* Callcc/walk.scm 155 */
																			obj_t BgL_oclassz00_2765;

																			{	/* Callcc/walk.scm 155 */
																				obj_t BgL_arg1815z00_2766;
																				long BgL_arg1816z00_2767;

																				BgL_arg1815z00_2766 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Callcc/walk.scm 155 */
																					long BgL_arg1817z00_2768;

																					BgL_arg1817z00_2768 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2760);
																					BgL_arg1816z00_2767 =
																						(BgL_arg1817z00_2768 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2765 =
																					VECTOR_REF(BgL_arg1815z00_2766,
																					BgL_arg1816z00_2767);
																			}
																			{	/* Callcc/walk.scm 155 */
																				bool_t BgL__ortest_1115z00_2769;

																				BgL__ortest_1115z00_2769 =
																					(BgL_classz00_2759 ==
																					BgL_oclassz00_2765);
																				if (BgL__ortest_1115z00_2769)
																					{	/* Callcc/walk.scm 155 */
																						BgL_res1912z00_2764 =
																							BgL__ortest_1115z00_2769;
																					}
																				else
																					{	/* Callcc/walk.scm 155 */
																						long BgL_odepthz00_2770;

																						{	/* Callcc/walk.scm 155 */
																							obj_t BgL_arg1804z00_2771;

																							BgL_arg1804z00_2771 =
																								(BgL_oclassz00_2765);
																							BgL_odepthz00_2770 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2771);
																						}
																						if ((2L < BgL_odepthz00_2770))
																							{	/* Callcc/walk.scm 155 */
																								obj_t BgL_arg1802z00_2772;

																								{	/* Callcc/walk.scm 155 */
																									obj_t BgL_arg1803z00_2773;

																									BgL_arg1803z00_2773 =
																										(BgL_oclassz00_2765);
																									BgL_arg1802z00_2772 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2773, 2L);
																								}
																								BgL_res1912z00_2764 =
																									(BgL_arg1802z00_2772 ==
																									BgL_classz00_2759);
																							}
																						else
																							{	/* Callcc/walk.scm 155 */
																								BgL_res1912z00_2764 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2050z00_3811 = BgL_res1912z00_2764;
																	}
															}
														else
															{	/* Callcc/walk.scm 155 */
																BgL_test2050z00_3811 = ((bool_t) 0);
															}
													}
													if (BgL_test2050z00_3811)
														{	/* Callcc/walk.scm 155 */
															return
																((BgL_nodez00_bglt)
																((BgL_varz00_bglt) BgL_nodez00_2565));
														}
													else
														{	/* Callcc/walk.scm 155 */
															if (BGl_celledzf3zf3zzcallcc_walkz00(
																	((BgL_variablez00_bglt) BgL_varz00_2725)))
																{	/* Callcc/walk.scm 160 */
																	BgL_typez00_bglt BgL_vtypez00_2774;

																	BgL_vtypez00_2774 =
																		BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
																		(BgL_vtypez00_2722);
																	{	/* Callcc/walk.scm 161 */
																		BgL_typez00_bglt BgL_vz00_2775;

																		BgL_vz00_2775 =
																			((BgL_typez00_bglt)
																			BGl_za2cellza2z00zztype_cachez00);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_varz00_bglt)
																								BgL_nodez00_2565))))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_vz00_2775),
																			BUNSPEC);
																	}
																	{	/* Callcc/walk.scm 162 */
																		BgL_boxzd2refzd2_bglt BgL_new1156z00_2776;

																		{	/* Callcc/walk.scm 165 */
																			BgL_boxzd2refzd2_bglt BgL_new1154z00_2777;

																			BgL_new1154z00_2777 =
																				((BgL_boxzd2refzd2_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_boxzd2refzd2_bgl))));
																			{	/* Callcc/walk.scm 165 */
																				long BgL_arg1705z00_2778;

																				BgL_arg1705z00_2778 =
																					BGL_CLASS_NUM
																					(BGl_boxzd2refzd2zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1154z00_2777),
																					BgL_arg1705z00_2778);
																			}
																			{	/* Callcc/walk.scm 165 */
																				BgL_objectz00_bglt BgL_tmpz00_3848;

																				BgL_tmpz00_3848 =
																					((BgL_objectz00_bglt)
																					BgL_new1154z00_2777);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3848,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1154z00_2777);
																			BgL_new1156z00_2776 = BgL_new1154z00_2777;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1156z00_2776)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_varz00_bglt)
																									BgL_nodez00_2565))))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1156z00_2776)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_vtypez00_2774),
																			BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1156z00_2776)))->
																				BgL_sidezd2effectzd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1156z00_2776)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		((((BgL_boxzd2refzd2_bglt)
																					COBJECT(BgL_new1156z00_2776))->
																				BgL_varz00) =
																			((BgL_varz00_bglt) ((BgL_varz00_bglt)
																					BgL_nodez00_2565)), BUNSPEC);
																		((((BgL_boxzd2refzd2_bglt)
																					COBJECT(BgL_new1156z00_2776))->
																				BgL_vtypez00) =
																			((BgL_typez00_bglt) BgL_vtypez00_2774),
																			BUNSPEC);
																		return ((BgL_nodez00_bglt)
																			BgL_new1156z00_2776);
																	}
																}
															else
																{	/* Callcc/walk.scm 157 */
																	return
																		((BgL_nodez00_bglt)
																		((BgL_varz00_bglt) BgL_nodez00_2565));
																}
														}
												}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &callcc!-kwote1323 */
	BgL_nodez00_bglt BGl_z62callccz12zd2kwote1323za2zzcallcc_walkz00(obj_t
		BgL_envz00_2566, obj_t BgL_nodez00_2567)
	{
		{	/* Callcc/walk.scm 135 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_2567));
		}

	}



/* &callcc!-atom1321 */
	BgL_nodez00_bglt BGl_z62callccz12zd2atom1321za2zzcallcc_walkz00(obj_t
		BgL_envz00_2568, obj_t BgL_nodez00_2569)
	{
		{	/* Callcc/walk.scm 129 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_2569));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcallcc_walkz00(void)
	{
		{	/* Callcc/walk.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1955z00zzcallcc_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
