/*===========================================================================*/
/*   (Read/jvm.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Read/jvm.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_READ_JVM_TYPE_DEFINITIONS
#define BGL_READ_JVM_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_READ_JVM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t);
	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzread_jvmz00 = BUNSPEC;
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmzd2classzd2withzd2directoryzd2zzread_jvmz00(obj_t);
	static obj_t
		BGl_addzd2currentzd2modulezd2qualifiedzd2typezd2namez12zc0zzread_jvmz00
		(void);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzread_jvmz00(void);
	static obj_t BGl_genericzd2initzd2zzread_jvmz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzread_jvmz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_dirnamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_dozd2readzd2jfilez00zzread_jvmz00(obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_basenamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62modulezd2ze3qualifiedzd2typez81zzread_jvmz00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzread_jvmz00(void);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31253ze3ze5zzread_jvmz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_za2jvmzd2markza2zd2zzread_jvmz00 = BUNSPEC;
	BGL_IMPORT bool_t BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t);
	BGL_IMPORT bool_t bigloo_strncmp(obj_t, obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza31184ze3ze5zzread_jvmz00(obj_t);
	static obj_t BGl_z62jvmzd2classzd2sanszd2directoryzb0zzread_jvmz00(obj_t,
		obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_IMPORT obj_t BGl_bigloozd2modulezd2resolverz00zz__modulez00(void);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_enginez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__modulez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sourcezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t);
	BGL_IMPORT bool_t fexists(char *);
	static obj_t BGl_z62addzd2qualifiedzd2typez12z70zzread_jvmz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzread_jvmz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzread_jvmz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzread_jvmz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzread_jvmz00(void);
	BGL_IMPORT obj_t BGl_dumpzd2tracezd2stackz00zz__errorz00(obj_t, obj_t);
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	extern obj_t BGl_backendz00zzbackend_backendz00;
	extern obj_t BGl_za2jvmzd2directoryza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	extern obj_t BGl_za2qualifiedzd2typezd2filezd2defaultza2zd2zzengine_paramz00;
	static obj_t BGl_z62jvmzd2classzd2withzd2directoryzb0zzread_jvmz00(obj_t,
		obj_t);
	extern obj_t BGl_za2qualifiedzd2typezd2fileza2z00zzengine_paramz00;
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	BGL_EXPORTED_DECL obj_t BGl_readzd2jfilezd2zzread_jvmz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmzd2classzd2sanszd2directoryzd2zzread_jvmz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62readzd2jfilezb0zzread_jvmz00(obj_t);
	static obj_t BGl_z62sourcezd2ze3qualifiedzd2typez81zzread_jvmz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t
		BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t BGl_za2accesszd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[5];


	   
		 
		DEFINE_STRING(BGl_string1310z00zzread_jvmz00,
		BgL_bgl_string1310za700za7za7r1333za7, ", for identifier `", 18);
	      DEFINE_STRING(BGl_string1311z00zzread_jvmz00,
		BgL_bgl_string1311za700za7za7r1334za7, "'", 1);
	      DEFINE_STRING(BGl_string1312z00zzread_jvmz00,
		BgL_bgl_string1312za700za7za7r1335za7, "empty name for module -- ", 25);
	      DEFINE_STRING(BGl_string1313z00zzread_jvmz00,
		BgL_bgl_string1313za700za7za7r1336za7, "add-qualified-type!", 19);
	      DEFINE_STRING(BGl_string1314z00zzread_jvmz00,
		BgL_bgl_string1314za700za7za7r1337za7, "\n  identifier=", 14);
	      DEFINE_STRING(BGl_string1315z00zzread_jvmz00,
		BgL_bgl_string1315za700za7za7r1338za7, "\n", 1);
	      DEFINE_STRING(BGl_string1316z00zzread_jvmz00,
		BgL_bgl_string1316za700za7za7r1339za7, "\n  new qualified type=", 22);
	      DEFINE_STRING(BGl_string1317z00zzread_jvmz00,
		BgL_bgl_string1317za700za7za7r1340za7, "\n  old qualified type=", 22);
	      DEFINE_STRING(BGl_string1318z00zzread_jvmz00,
		BgL_bgl_string1318za700za7za7r1341za7,
		"qualified type redefinition:\n  module/class=", 44);
	      DEFINE_STRING(BGl_string1319z00zzread_jvmz00,
		BgL_bgl_string1319za700za7za7r1342za7, "]", 1);
	      DEFINE_STRING(BGl_string1320z00zzread_jvmz00,
		BgL_bgl_string1320za700za7za7r1343za7, "      [reading jfile ", 21);
	      DEFINE_STRING(BGl_string1321z00zzread_jvmz00,
		BgL_bgl_string1321za700za7za7r1344za7, "Can't open jfile", 16);
	      DEFINE_STRING(BGl_string1322z00zzread_jvmz00,
		BgL_bgl_string1322za700za7za7r1345za7, "Can't find jfile", 16);
	      DEFINE_STRING(BGl_string1323z00zzread_jvmz00,
		BgL_bgl_string1323za700za7za7r1346za7, "Illegal jfile format", 20);
	      DEFINE_STRING(BGl_string1324z00zzread_jvmz00,
		BgL_bgl_string1324za700za7za7r1347za7, ".", 1);
	      DEFINE_STRING(BGl_string1325z00zzread_jvmz00,
		BgL_bgl_string1325za700za7za7r1348za7,
		"Can't find qualified type name for module `", 43);
	      DEFINE_STRING(BGl_string1326z00zzread_jvmz00,
		BgL_bgl_string1326za700za7za7r1349za7, "',", 2);
	      DEFINE_STRING(BGl_string1327z00zzread_jvmz00,
		BgL_bgl_string1327za700za7za7r1350za7, "'.", 2);
	      DEFINE_STRING(BGl_string1328z00zzread_jvmz00,
		BgL_bgl_string1328za700za7za7r1351za7, "Using name `", 12);
	      DEFINE_STRING(BGl_string1330z00zzread_jvmz00,
		BgL_bgl_string1330za700za7za7r1352za7, "read_jvm", 8);
	      DEFINE_STRING(BGl_string1331z00zzread_jvmz00,
		BgL_bgl_string1331za700za7za7r1353za7,
		"module ld done read-jfile jvm-qtype ", 36);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1329z00zzread_jvmz00,
		BgL_bgl_za762za7c3za704anonymo1354za7,
		BGl_z62zc3z04anonymousza31253ze3ze5zzread_jvmz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2qualifiedzd2typez12zd2envzc0zzread_jvmz00,
		BgL_bgl_za762addza7d2qualifi1355z00, va_generic_entry,
		BGl_z62addzd2qualifiedzd2typez12z70zzread_jvmz00, BUNSPEC, -3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_readzd2jfilezd2envz00zzread_jvmz00,
		BgL_bgl_za762readza7d2jfileza71356za7, BGl_z62readzd2jfilezb0zzread_jvmz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmzd2classzd2sanszd2directoryzd2envz00zzread_jvmz00,
		BgL_bgl_za762jvmza7d2classza7d1357za7,
		BGl_z62jvmzd2classzd2sanszd2directoryzb0zzread_jvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sourcezd2ze3qualifiedzd2typezd2envz31zzread_jvmz00,
		BgL_bgl_za762sourceza7d2za7e3q1358za7,
		BGl_z62sourcezd2ze3qualifiedzd2typez81zzread_jvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmzd2classzd2withzd2directoryzd2envz00zzread_jvmz00,
		BgL_bgl_za762jvmza7d2classza7d1359za7,
		BGl_z62jvmzd2classzd2withzd2directoryzb0zzread_jvmz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2ze3qualifiedzd2typezd2envz31zzread_jvmz00,
		BgL_bgl_za762moduleza7d2za7e3q1360za7,
		BGl_z62modulezd2ze3qualifiedzd2typez81zzread_jvmz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1309z00zzread_jvmz00,
		BgL_bgl_string1309za700za7za7r1361za7, "", 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzread_jvmz00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2markza2zd2zzread_jvmz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long
		BgL_checksumz00_552, char *BgL_fromz00_553)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzread_jvmz00))
				{
					BGl_requirezd2initializa7ationz75zzread_jvmz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzread_jvmz00();
					BGl_libraryzd2moduleszd2initz00zzread_jvmz00();
					BGl_cnstzd2initzd2zzread_jvmz00();
					BGl_importedzd2moduleszd2initz00zzread_jvmz00();
					return BGl_toplevelzd2initzd2zzread_jvmz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzread_jvmz00(void)
	{
		{	/* Read/jvm.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__modulez00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"read_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "read_jvm");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"read_jvm");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzread_jvmz00(void)
	{
		{	/* Read/jvm.scm 15 */
			{	/* Read/jvm.scm 15 */
				obj_t BgL_cportz00_538;

				{	/* Read/jvm.scm 15 */
					obj_t BgL_stringz00_545;

					BgL_stringz00_545 = BGl_string1331z00zzread_jvmz00;
					{	/* Read/jvm.scm 15 */
						obj_t BgL_startz00_546;

						BgL_startz00_546 = BINT(0L);
						{	/* Read/jvm.scm 15 */
							obj_t BgL_endz00_547;

							BgL_endz00_547 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_545)));
							{	/* Read/jvm.scm 15 */

								BgL_cportz00_538 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_545, BgL_startz00_546, BgL_endz00_547);
				}}}}
				{
					long BgL_iz00_539;

					BgL_iz00_539 = 4L;
				BgL_loopz00_540:
					if ((BgL_iz00_539 == -1L))
						{	/* Read/jvm.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Read/jvm.scm 15 */
							{	/* Read/jvm.scm 15 */
								obj_t BgL_arg1332z00_541;

								{	/* Read/jvm.scm 15 */

									{	/* Read/jvm.scm 15 */
										obj_t BgL_locationz00_543;

										BgL_locationz00_543 = BBOOL(((bool_t) 0));
										{	/* Read/jvm.scm 15 */

											BgL_arg1332z00_541 =
												BGl_readz00zz__readerz00(BgL_cportz00_538,
												BgL_locationz00_543);
										}
									}
								}
								{	/* Read/jvm.scm 15 */
									int BgL_tmpz00_587;

									BgL_tmpz00_587 = (int) (BgL_iz00_539);
									CNST_TABLE_SET(BgL_tmpz00_587, BgL_arg1332z00_541);
							}}
							{	/* Read/jvm.scm 15 */
								int BgL_auxz00_544;

								BgL_auxz00_544 = (int) ((BgL_iz00_539 - 1L));
								{
									long BgL_iz00_592;

									BgL_iz00_592 = (long) (BgL_auxz00_544);
									BgL_iz00_539 = BgL_iz00_592;
									goto BgL_loopz00_540;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzread_jvmz00(void)
	{
		{	/* Read/jvm.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzread_jvmz00(void)
	{
		{	/* Read/jvm.scm 15 */
			return (BGl_za2jvmzd2markza2zd2zzread_jvmz00 =
				CNST_TABLE_REF(0), BUNSPEC);
		}

	}



/* jvm-class-sans-directory */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmzd2classzd2sanszd2directoryzd2zzread_jvmz00(obj_t BgL_filez00_3)
	{
		{	/* Read/jvm.scm 33 */
			if (STRINGP(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00))
				{	/* Read/jvm.scm 36 */
					long BgL_ldz00_127;
					long BgL_lfz00_128;

					BgL_ldz00_127 =
						STRING_LENGTH(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00);
					BgL_lfz00_128 = STRING_LENGTH(BgL_filez00_3);
					{	/* Read/jvm.scm 38 */
						bool_t BgL_test1366z00_600;

						if ((BgL_lfz00_128 < (BgL_ldz00_127 + 1L)))
							{	/* Read/jvm.scm 38 */
								BgL_test1366z00_600 = ((bool_t) 1);
							}
						else
							{	/* Read/jvm.scm 38 */
								if (bigloo_strncmp(BgL_filez00_3,
										BGl_za2jvmzd2directoryza2zd2zzengine_paramz00,
										BgL_ldz00_127))
									{	/* Read/jvm.scm 39 */
										if (
											(STRING_REF(BgL_filez00_3, BgL_ldz00_127) ==
												(unsigned char) (FILE_SEPARATOR)))
											{	/* Read/jvm.scm 40 */
												BgL_test1366z00_600 = ((bool_t) 0);
											}
										else
											{	/* Read/jvm.scm 40 */
												BgL_test1366z00_600 = ((bool_t) 1);
											}
									}
								else
									{	/* Read/jvm.scm 39 */
										BgL_test1366z00_600 = ((bool_t) 1);
									}
							}
						if (BgL_test1366z00_600)
							{	/* Read/jvm.scm 38 */
								return BgL_filez00_3;
							}
						else
							{	/* Read/jvm.scm 38 */
								return
									c_substring(BgL_filez00_3,
									(1L + BgL_ldz00_127), BgL_lfz00_128);
							}
					}
				}
			else
				{	/* Read/jvm.scm 34 */
					return BgL_filez00_3;
				}
		}

	}



/* &jvm-class-sans-directory */
	obj_t BGl_z62jvmzd2classzd2sanszd2directoryzb0zzread_jvmz00(obj_t
		BgL_envz00_520, obj_t BgL_filez00_521)
	{
		{	/* Read/jvm.scm 33 */
			return
				BGl_jvmzd2classzd2sanszd2directoryzd2zzread_jvmz00(BgL_filez00_521);
		}

	}



/* jvm-class-with-directory */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmzd2classzd2withzd2directoryzd2zzread_jvmz00(obj_t BgL_classz00_4)
	{
		{	/* Read/jvm.scm 47 */
			if (STRINGP(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00))
				{	/* Read/jvm.scm 51 */
					bool_t BgL_test1373z00_615;

					{	/* Read/jvm.scm 51 */
						long BgL_l1z00_344;

						BgL_l1z00_344 = STRING_LENGTH(BgL_classz00_4);
						if ((BgL_l1z00_344 == 0L))
							{	/* Read/jvm.scm 51 */
								int BgL_arg1282z00_347;

								{	/* Read/jvm.scm 51 */
									char *BgL_auxz00_621;
									char *BgL_tmpz00_619;

									BgL_auxz00_621 =
										BSTRING_TO_STRING(BGl_string1309z00zzread_jvmz00);
									BgL_tmpz00_619 = BSTRING_TO_STRING(BgL_classz00_4);
									BgL_arg1282z00_347 =
										memcmp(BgL_tmpz00_619, BgL_auxz00_621, BgL_l1z00_344);
								}
								BgL_test1373z00_615 = ((long) (BgL_arg1282z00_347) == 0L);
							}
						else
							{	/* Read/jvm.scm 51 */
								BgL_test1373z00_615 = ((bool_t) 0);
							}
					}
					if (BgL_test1373z00_615)
						{	/* Read/jvm.scm 51 */
							return BGl_za2jvmzd2directoryza2zd2zzengine_paramz00;
						}
					else
						{	/* Read/jvm.scm 51 */
							return
								BGl_makezd2filezd2namez00zz__osz00
								(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00, BgL_classz00_4);
						}
				}
			else
				{	/* Read/jvm.scm 49 */
					return BgL_classz00_4;
				}
		}

	}



/* &jvm-class-with-directory */
	obj_t BGl_z62jvmzd2classzd2withzd2directoryzb0zzread_jvmz00(obj_t
		BgL_envz00_522, obj_t BgL_classz00_523)
	{
		{	/* Read/jvm.scm 47 */
			return
				BGl_jvmzd2classzd2withzd2directoryzd2zzread_jvmz00(BgL_classz00_523);
		}

	}



/* add-qualified-type! */
	BGL_EXPORTED_DEF obj_t BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(obj_t
		BgL_modulez00_5, obj_t BgL_qtypez00_6, obj_t BgL_identz00_7)
	{
		{	/* Read/jvm.scm 64 */
			{	/* Read/jvm.scm 65 */
				obj_t BgL_bcz00_151;

				BgL_bcz00_151 = BGl_thezd2backendzd2zzbackend_backendz00();
				{	/* Read/jvm.scm 66 */
					bool_t BgL_test1383z00_629;

					{	/* Read/jvm.scm 66 */
						bool_t BgL_test1384z00_630;

						{	/* Read/jvm.scm 66 */
							obj_t BgL_classz00_353;

							BgL_classz00_353 = BGl_backendz00zzbackend_backendz00;
							if (BGL_OBJECTP(BgL_bcz00_151))
								{	/* Read/jvm.scm 66 */
									BgL_objectz00_bglt BgL_arg1807z00_355;

									BgL_arg1807z00_355 = (BgL_objectz00_bglt) (BgL_bcz00_151);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Read/jvm.scm 66 */
											long BgL_idxz00_361;

											BgL_idxz00_361 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_355);
											BgL_test1384z00_630 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_361 + 1L)) == BgL_classz00_353);
										}
									else
										{	/* Read/jvm.scm 66 */
											bool_t BgL_res1306z00_386;

											{	/* Read/jvm.scm 66 */
												obj_t BgL_oclassz00_369;

												{	/* Read/jvm.scm 66 */
													obj_t BgL_arg1815z00_377;
													long BgL_arg1816z00_378;

													BgL_arg1815z00_377 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Read/jvm.scm 66 */
														long BgL_arg1817z00_379;

														BgL_arg1817z00_379 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_355);
														BgL_arg1816z00_378 =
															(BgL_arg1817z00_379 - OBJECT_TYPE);
													}
													BgL_oclassz00_369 =
														VECTOR_REF(BgL_arg1815z00_377, BgL_arg1816z00_378);
												}
												{	/* Read/jvm.scm 66 */
													bool_t BgL__ortest_1115z00_370;

													BgL__ortest_1115z00_370 =
														(BgL_classz00_353 == BgL_oclassz00_369);
													if (BgL__ortest_1115z00_370)
														{	/* Read/jvm.scm 66 */
															BgL_res1306z00_386 = BgL__ortest_1115z00_370;
														}
													else
														{	/* Read/jvm.scm 66 */
															long BgL_odepthz00_371;

															{	/* Read/jvm.scm 66 */
																obj_t BgL_arg1804z00_372;

																BgL_arg1804z00_372 = (BgL_oclassz00_369);
																BgL_odepthz00_371 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_372);
															}
															if ((1L < BgL_odepthz00_371))
																{	/* Read/jvm.scm 66 */
																	obj_t BgL_arg1802z00_374;

																	{	/* Read/jvm.scm 66 */
																		obj_t BgL_arg1803z00_375;

																		BgL_arg1803z00_375 = (BgL_oclassz00_369);
																		BgL_arg1802z00_374 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_375, 1L);
																	}
																	BgL_res1306z00_386 =
																		(BgL_arg1802z00_374 == BgL_classz00_353);
																}
															else
																{	/* Read/jvm.scm 66 */
																	BgL_res1306z00_386 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1384z00_630 = BgL_res1306z00_386;
										}
								}
							else
								{	/* Read/jvm.scm 66 */
									BgL_test1384z00_630 = ((bool_t) 0);
								}
						}
						if (BgL_test1384z00_630)
							{	/* Read/jvm.scm 67 */
								bool_t BgL_test1389z00_653;

								{	/* Read/jvm.scm 67 */
									long BgL_l1z00_389;

									BgL_l1z00_389 = STRING_LENGTH(BgL_qtypez00_6);
									if ((BgL_l1z00_389 == 0L))
										{	/* Read/jvm.scm 67 */
											int BgL_arg1282z00_392;

											{	/* Read/jvm.scm 67 */
												char *BgL_auxz00_659;
												char *BgL_tmpz00_657;

												BgL_auxz00_659 =
													BSTRING_TO_STRING(BGl_string1309z00zzread_jvmz00);
												BgL_tmpz00_657 = BSTRING_TO_STRING(BgL_qtypez00_6);
												BgL_arg1282z00_392 =
													memcmp(BgL_tmpz00_657, BgL_auxz00_659, BgL_l1z00_389);
											}
											BgL_test1389z00_653 = ((long) (BgL_arg1282z00_392) == 0L);
										}
									else
										{	/* Read/jvm.scm 67 */
											BgL_test1389z00_653 = ((bool_t) 0);
										}
								}
								if (BgL_test1389z00_653)
									{	/* Read/jvm.scm 68 */
										obj_t BgL_arg1126z00_170;

										BgL_arg1126z00_170 =
											BGl_thezd2backendzd2zzbackend_backendz00();
										BgL_test1383z00_629 =
											(((BgL_backendz00_bglt) COBJECT(
													((BgL_backendz00_bglt) BgL_arg1126z00_170)))->
											BgL_qualifiedzd2typeszd2);
									}
								else
									{	/* Read/jvm.scm 67 */
										BgL_test1383z00_629 = ((bool_t) 0);
									}
							}
						else
							{	/* Read/jvm.scm 66 */
								BgL_test1383z00_629 = ((bool_t) 0);
							}
					}
					if (BgL_test1383z00_629)
						{	/* Read/jvm.scm 72 */
							obj_t BgL_arg1092z00_156;

							{	/* Read/jvm.scm 72 */
								bool_t BgL_test1391z00_667;

								if (PAIRP(BgL_identz00_7))
									{	/* Read/jvm.scm 72 */
										obj_t BgL_tmpz00_670;

										BgL_tmpz00_670 = CAR(BgL_identz00_7);
										BgL_test1391z00_667 = SYMBOLP(BgL_tmpz00_670);
									}
								else
									{	/* Read/jvm.scm 72 */
										BgL_test1391z00_667 = ((bool_t) 0);
									}
								if (BgL_test1391z00_667)
									{	/* Read/jvm.scm 74 */
										obj_t BgL_arg1122z00_164;

										{	/* Read/jvm.scm 74 */
											obj_t BgL_arg1123z00_165;

											BgL_arg1123z00_165 = CAR(BgL_identz00_7);
											{	/* Read/jvm.scm 74 */
												obj_t BgL_arg1455z00_402;

												BgL_arg1455z00_402 =
													SYMBOL_TO_STRING(((obj_t) BgL_arg1123z00_165));
												BgL_arg1122z00_164 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_402);
											}
										}
										BgL_arg1092z00_156 =
											string_append_3(BGl_string1310z00zzread_jvmz00,
											BgL_arg1122z00_164, BGl_string1311z00zzread_jvmz00);
									}
								else
									{	/* Read/jvm.scm 72 */
										BgL_arg1092z00_156 = BGl_string1309z00zzread_jvmz00;
									}
							}
							{	/* Read/jvm.scm 69 */
								obj_t BgL_list1093z00_157;

								{	/* Read/jvm.scm 69 */
									obj_t BgL_arg1097z00_158;

									{	/* Read/jvm.scm 69 */
										obj_t BgL_arg1102z00_159;

										{	/* Read/jvm.scm 69 */
											obj_t BgL_arg1103z00_160;

											BgL_arg1103z00_160 =
												MAKE_YOUNG_PAIR(BgL_arg1092z00_156, BNIL);
											BgL_arg1102z00_159 =
												MAKE_YOUNG_PAIR(BgL_modulez00_5, BgL_arg1103z00_160);
										}
										BgL_arg1097z00_158 =
											MAKE_YOUNG_PAIR(BGl_string1312z00zzread_jvmz00,
											BgL_arg1102z00_159);
									}
									BgL_list1093z00_157 =
										MAKE_YOUNG_PAIR(BGl_string1313z00zzread_jvmz00,
										BgL_arg1097z00_158);
								}
								BGl_warningz00zz__errorz00(BgL_list1093z00_157);
							}
						}
					else
						{	/* Read/jvm.scm 66 */
							BFALSE;
						}
				}
				{	/* Read/jvm.scm 77 */
					obj_t BgL_bz00_171;

					BgL_bz00_171 =
						BGl_getpropz00zz__r4_symbols_6_4z00(BgL_modulez00_5,
						CNST_TABLE_REF(0));
					if (CBOOL(BgL_bz00_171))
						{	/* Read/jvm.scm 80 */
							bool_t BgL_test1394z00_687;

							{	/* Read/jvm.scm 80 */
								bool_t BgL_test1395z00_688;

								{	/* Read/jvm.scm 80 */
									obj_t BgL_classz00_403;

									BgL_classz00_403 = BGl_backendz00zzbackend_backendz00;
									if (BGL_OBJECTP(BgL_bcz00_151))
										{	/* Read/jvm.scm 80 */
											BgL_objectz00_bglt BgL_arg1807z00_405;

											BgL_arg1807z00_405 = (BgL_objectz00_bglt) (BgL_bcz00_151);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Read/jvm.scm 80 */
													long BgL_idxz00_411;

													BgL_idxz00_411 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_405);
													BgL_test1395z00_688 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_411 + 1L)) == BgL_classz00_403);
												}
											else
												{	/* Read/jvm.scm 80 */
													bool_t BgL_res1307z00_436;

													{	/* Read/jvm.scm 80 */
														obj_t BgL_oclassz00_419;

														{	/* Read/jvm.scm 80 */
															obj_t BgL_arg1815z00_427;
															long BgL_arg1816z00_428;

															BgL_arg1815z00_427 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Read/jvm.scm 80 */
																long BgL_arg1817z00_429;

																BgL_arg1817z00_429 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_405);
																BgL_arg1816z00_428 =
																	(BgL_arg1817z00_429 - OBJECT_TYPE);
															}
															BgL_oclassz00_419 =
																VECTOR_REF(BgL_arg1815z00_427,
																BgL_arg1816z00_428);
														}
														{	/* Read/jvm.scm 80 */
															bool_t BgL__ortest_1115z00_420;

															BgL__ortest_1115z00_420 =
																(BgL_classz00_403 == BgL_oclassz00_419);
															if (BgL__ortest_1115z00_420)
																{	/* Read/jvm.scm 80 */
																	BgL_res1307z00_436 = BgL__ortest_1115z00_420;
																}
															else
																{	/* Read/jvm.scm 80 */
																	long BgL_odepthz00_421;

																	{	/* Read/jvm.scm 80 */
																		obj_t BgL_arg1804z00_422;

																		BgL_arg1804z00_422 = (BgL_oclassz00_419);
																		BgL_odepthz00_421 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_422);
																	}
																	if ((1L < BgL_odepthz00_421))
																		{	/* Read/jvm.scm 80 */
																			obj_t BgL_arg1802z00_424;

																			{	/* Read/jvm.scm 80 */
																				obj_t BgL_arg1803z00_425;

																				BgL_arg1803z00_425 =
																					(BgL_oclassz00_419);
																				BgL_arg1802z00_424 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_425, 1L);
																			}
																			BgL_res1307z00_436 =
																				(BgL_arg1802z00_424 ==
																				BgL_classz00_403);
																		}
																	else
																		{	/* Read/jvm.scm 80 */
																			BgL_res1307z00_436 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1395z00_688 = BgL_res1307z00_436;
												}
										}
									else
										{	/* Read/jvm.scm 80 */
											BgL_test1395z00_688 = ((bool_t) 0);
										}
								}
								if (BgL_test1395z00_688)
									{	/* Read/jvm.scm 80 */
										if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_bz00_171,
												BgL_qtypez00_6))
											{	/* Read/jvm.scm 81 */
												BgL_test1394z00_687 = ((bool_t) 0);
											}
										else
											{	/* Read/jvm.scm 82 */
												obj_t BgL_arg1164z00_196;

												BgL_arg1164z00_196 =
													BGl_thezd2backendzd2zzbackend_backendz00();
												BgL_test1394z00_687 =
													(((BgL_backendz00_bglt) COBJECT(
															((BgL_backendz00_bglt) BgL_arg1164z00_196)))->
													BgL_qualifiedzd2typeszd2);
											}
									}
								else
									{	/* Read/jvm.scm 80 */
										BgL_test1394z00_687 = ((bool_t) 0);
									}
							}
							if (BgL_test1394z00_687)
								{	/* Read/jvm.scm 80 */
									BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_modulez00_5,
										CNST_TABLE_REF(0), BgL_qtypez00_6);
									{	/* Read/jvm.scm 86 */
										obj_t BgL_arg1131z00_176;

										{	/* Read/jvm.scm 86 */
											bool_t BgL_test1401z00_718;

											if (PAIRP(BgL_identz00_7))
												{	/* Read/jvm.scm 86 */
													obj_t BgL_tmpz00_721;

													BgL_tmpz00_721 = CAR(BgL_identz00_7);
													BgL_test1401z00_718 = SYMBOLP(BgL_tmpz00_721);
												}
											else
												{	/* Read/jvm.scm 86 */
													BgL_test1401z00_718 = ((bool_t) 0);
												}
											if (BgL_test1401z00_718)
												{	/* Read/jvm.scm 88 */
													obj_t BgL_arg1154z00_189;

													{	/* Read/jvm.scm 88 */
														obj_t BgL_arg1157z00_190;

														BgL_arg1157z00_190 = CAR(BgL_identz00_7);
														{	/* Read/jvm.scm 88 */
															obj_t BgL_arg1455z00_441;

															BgL_arg1455z00_441 =
																SYMBOL_TO_STRING(((obj_t) BgL_arg1157z00_190));
															BgL_arg1154z00_189 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_441);
														}
													}
													BgL_arg1131z00_176 =
														string_append(BGl_string1314z00zzread_jvmz00,
														BgL_arg1154z00_189);
												}
											else
												{	/* Read/jvm.scm 86 */
													BgL_arg1131z00_176 = BGl_string1309z00zzread_jvmz00;
												}
										}
										{	/* Read/jvm.scm 84 */
											obj_t BgL_list1132z00_177;

											{	/* Read/jvm.scm 84 */
												obj_t BgL_arg1137z00_178;

												{	/* Read/jvm.scm 84 */
													obj_t BgL_arg1138z00_179;

													{	/* Read/jvm.scm 84 */
														obj_t BgL_arg1140z00_180;

														{	/* Read/jvm.scm 84 */
															obj_t BgL_arg1141z00_181;

															{	/* Read/jvm.scm 84 */
																obj_t BgL_arg1142z00_182;

																{	/* Read/jvm.scm 84 */
																	obj_t BgL_arg1143z00_183;

																	{	/* Read/jvm.scm 84 */
																		obj_t BgL_arg1145z00_184;

																		{	/* Read/jvm.scm 84 */
																			obj_t BgL_arg1148z00_185;

																			BgL_arg1148z00_185 =
																				MAKE_YOUNG_PAIR
																				(BGl_string1315z00zzread_jvmz00, BNIL);
																			BgL_arg1145z00_184 =
																				MAKE_YOUNG_PAIR(BgL_qtypez00_6,
																				BgL_arg1148z00_185);
																		}
																		BgL_arg1143z00_183 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1316z00zzread_jvmz00,
																			BgL_arg1145z00_184);
																	}
																	BgL_arg1142z00_182 =
																		MAKE_YOUNG_PAIR(BgL_bz00_171,
																		BgL_arg1143z00_183);
																}
																BgL_arg1141z00_181 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1317z00zzread_jvmz00,
																	BgL_arg1142z00_182);
															}
															BgL_arg1140z00_180 =
																MAKE_YOUNG_PAIR(BgL_arg1131z00_176,
																BgL_arg1141z00_181);
														}
														BgL_arg1138z00_179 =
															MAKE_YOUNG_PAIR(BgL_modulez00_5,
															BgL_arg1140z00_180);
													}
													BgL_arg1137z00_178 =
														MAKE_YOUNG_PAIR(BGl_string1318z00zzread_jvmz00,
														BgL_arg1138z00_179);
												}
												BgL_list1132z00_177 =
													MAKE_YOUNG_PAIR(BGl_string1313z00zzread_jvmz00,
													BgL_arg1137z00_178);
											}
											BGl_warningz00zz__errorz00(BgL_list1132z00_177);
										}
									}
									{	/* Read/jvm.scm 93 */
										obj_t BgL_arg1162z00_193;

										{	/* Read/jvm.scm 93 */
											obj_t BgL_tmpz00_739;

											BgL_tmpz00_739 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_arg1162z00_193 =
												BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_739);
										}
										return
											BGl_dumpzd2tracezd2stackz00zz__errorz00
											(BgL_arg1162z00_193, BINT(10L));
									}
								}
							else
								{	/* Read/jvm.scm 80 */
									return BFALSE;
								}
						}
					else
						{	/* Read/jvm.scm 78 */
							return
								BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_modulez00_5,
								CNST_TABLE_REF(0), BgL_qtypez00_6);
						}
				}
			}
		}

	}



/* &add-qualified-type! */
	obj_t BGl_z62addzd2qualifiedzd2typez12z70zzread_jvmz00(obj_t BgL_envz00_524,
		obj_t BgL_modulez00_525, obj_t BgL_qtypez00_526, obj_t BgL_identz00_527)
	{
		{	/* Read/jvm.scm 64 */
			return
				BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(BgL_modulez00_525,
				BgL_qtypez00_526, BgL_identz00_527);
		}

	}



/* read-jfile */
	BGL_EXPORTED_DEF obj_t BGl_readzd2jfilezd2zzread_jvmz00(void)
	{
		{	/* Read/jvm.scm 98 */
			{
				obj_t BgL_namez00_202;

				if (STRINGP(BGl_za2qualifiedzd2typezd2fileza2z00zzengine_paramz00))
					{	/* Read/jvm.scm 109 */
						if (fexists(BSTRING_TO_STRING
								(BGl_za2qualifiedzd2typezd2fileza2z00zzengine_paramz00)))
							{	/* Read/jvm.scm 113 */
								BgL_namez00_202 =
									BGl_za2qualifiedzd2typezd2fileza2z00zzengine_paramz00;
							BgL_zc3z04anonymousza31169ze3z87_203:
								{	/* Read/jvm.scm 100 */
									obj_t BgL_portz00_204;

									{	/* Read/jvm.scm 100 */

										BgL_portz00_204 =
											BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
											(BgL_namez00_202, BTRUE, BINT(5000000L));
									}
									{	/* Read/jvm.scm 101 */
										obj_t BgL_list1170z00_205;

										{	/* Read/jvm.scm 101 */
											obj_t BgL_arg1171z00_206;

											{	/* Read/jvm.scm 101 */
												obj_t BgL_arg1172z00_207;

												{	/* Read/jvm.scm 101 */
													obj_t BgL_arg1182z00_208;

													BgL_arg1182z00_208 =
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
													BgL_arg1172z00_207 =
														MAKE_YOUNG_PAIR(BGl_string1319z00zzread_jvmz00,
														BgL_arg1182z00_208);
												}
												BgL_arg1171z00_206 =
													MAKE_YOUNG_PAIR(BgL_namez00_202, BgL_arg1172z00_207);
											}
											BgL_list1170z00_205 =
												MAKE_YOUNG_PAIR(BGl_string1320z00zzread_jvmz00,
												BgL_arg1171z00_206);
										}
										BGl_verbosez00zztools_speekz00(BINT(2L),
											BgL_list1170z00_205);
									}
									if (INPUT_PORTP(BgL_portz00_204))
										{	/* Read/jvm.scm 104 */
											obj_t BgL_exitd1049z00_210;

											BgL_exitd1049z00_210 = BGL_EXITD_TOP_AS_OBJ();
											{	/* Read/jvm.scm 106 */
												obj_t BgL_zc3z04anonymousza31184ze3z87_528;

												BgL_zc3z04anonymousza31184ze3z87_528 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31184ze3ze5zzread_jvmz00,
													(int) (0L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31184ze3z87_528,
													(int) (0L), BgL_portz00_204);
												{	/* Read/jvm.scm 104 */
													obj_t BgL_arg1828z00_444;

													{	/* Read/jvm.scm 104 */
														obj_t BgL_arg1829z00_445;

														BgL_arg1829z00_445 =
															BGL_EXITD_PROTECT(BgL_exitd1049z00_210);
														BgL_arg1828z00_444 =
															MAKE_YOUNG_PAIR
															(BgL_zc3z04anonymousza31184ze3z87_528,
															BgL_arg1829z00_445);
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1049z00_210,
														BgL_arg1828z00_444);
													BUNSPEC;
												}
												{	/* Read/jvm.scm 105 */
													obj_t BgL_tmp1051z00_212;

													BgL_tmp1051z00_212 =
														BGl_dozd2readzd2jfilez00zzread_jvmz00
														(BgL_portz00_204, BgL_namez00_202);
													{	/* Read/jvm.scm 104 */
														bool_t BgL_test1406z00_773;

														{	/* Read/jvm.scm 104 */
															obj_t BgL_arg1827z00_447;

															BgL_arg1827z00_447 =
																BGL_EXITD_PROTECT(BgL_exitd1049z00_210);
															BgL_test1406z00_773 = PAIRP(BgL_arg1827z00_447);
														}
														if (BgL_test1406z00_773)
															{	/* Read/jvm.scm 104 */
																obj_t BgL_arg1825z00_448;

																{	/* Read/jvm.scm 104 */
																	obj_t BgL_arg1826z00_449;

																	BgL_arg1826z00_449 =
																		BGL_EXITD_PROTECT(BgL_exitd1049z00_210);
																	BgL_arg1825z00_448 =
																		CDR(((obj_t) BgL_arg1826z00_449));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1049z00_210,
																	BgL_arg1825z00_448);
																BUNSPEC;
															}
														else
															{	/* Read/jvm.scm 104 */
																BFALSE;
															}
													}
													bgl_close_input_port(BgL_portz00_204);
													return BgL_tmp1051z00_212;
												}
											}
										}
									else
										{	/* Read/jvm.scm 102 */
											return
												BGl_userzd2errorzd2zztools_errorz00(CNST_TABLE_REF(1),
												BGl_string1321z00zzread_jvmz00, BgL_namez00_202, BNIL);
										}
								}
							}
						else
							{	/* Read/jvm.scm 113 */
								return
									BGl_userzd2errorzd2zztools_errorz00(CNST_TABLE_REF(1),
									BGl_string1322z00zzread_jvmz00,
									BGl_za2qualifiedzd2typezd2fileza2z00zzengine_paramz00, BNIL);
							}
					}
				else
					{	/* Read/jvm.scm 109 */
						if (fexists(BSTRING_TO_STRING
								(BGl_za2qualifiedzd2typezd2filezd2defaultza2zd2zzengine_paramz00)))
							{
								obj_t BgL_namez00_788;

								BgL_namez00_788 =
									BGl_za2qualifiedzd2typezd2filezd2defaultza2zd2zzengine_paramz00;
								BgL_namez00_202 = BgL_namez00_788;
								goto BgL_zc3z04anonymousza31169ze3z87_203;
							}
						else
							{	/* Read/jvm.scm 110 */
								return CNST_TABLE_REF(2);
							}
					}
			}
		}

	}



/* &read-jfile */
	obj_t BGl_z62readzd2jfilezb0zzread_jvmz00(obj_t BgL_envz00_529)
	{
		{	/* Read/jvm.scm 98 */
			return BGl_readzd2jfilezd2zzread_jvmz00();
		}

	}



/* &<@anonymous:1184> */
	obj_t BGl_z62zc3z04anonymousza31184ze3ze5zzread_jvmz00(obj_t BgL_envz00_530)
	{
		{	/* Read/jvm.scm 104 */
			{	/* Read/jvm.scm 106 */
				obj_t BgL_portz00_531;

				BgL_portz00_531 = PROCEDURE_REF(BgL_envz00_530, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_portz00_531));
			}
		}

	}



/* do-read-jfile */
	obj_t BGl_dozd2readzd2jfilez00zzread_jvmz00(obj_t BgL_portz00_8,
		obj_t BgL_jfnamez00_9)
	{
		{	/* Read/jvm.scm 121 */
			{	/* Read/jvm.scm 122 */
				obj_t BgL_objz00_220;

				BgL_objz00_220 = BGl_readz00zz__readerz00(BgL_portz00_8, BTRUE);
				{	/* Read/jvm.scm 122 */
					obj_t BgL_eofz00_221;

					{	/* Read/jvm.scm 123 */

						{	/* Read/jvm.scm 123 */

							BgL_eofz00_221 = BGl_readz00zz__readerz00(BgL_portz00_8, BFALSE);
						}
					}
					{	/* Read/jvm.scm 123 */

						if (EOF_OBJECTP(BgL_objz00_220))
							{	/* Read/jvm.scm 125 */
								return
									BGl_userzd2errorzd2zztools_errorz00(CNST_TABLE_REF(1),
									BGl_string1323z00zzread_jvmz00, BgL_objz00_220, BNIL);
							}
						else
							{	/* Read/jvm.scm 125 */
								if (EOF_OBJECTP(BgL_eofz00_221))
									{
										obj_t BgL_objz00_226;

										BgL_objz00_226 = BgL_objz00_220;
									BgL_zc3z04anonymousza31189ze3z87_227:
										if (NULLP(BgL_objz00_226))
											{	/* Read/jvm.scm 131 */
												return CNST_TABLE_REF(2);
											}
										else
											{

												{	/* Read/jvm.scm 133 */
													obj_t BgL_ezd2103zd2_233;

													BgL_ezd2103zd2_233 = CAR(((obj_t) BgL_objz00_226));
													if (PAIRP(BgL_ezd2103zd2_233))
														{	/* Read/jvm.scm 133 */
															obj_t BgL_carzd2108zd2_235;
															obj_t BgL_cdrzd2109zd2_236;

															BgL_carzd2108zd2_235 = CAR(BgL_ezd2103zd2_233);
															BgL_cdrzd2109zd2_236 = CDR(BgL_ezd2103zd2_233);
															if (SYMBOLP(BgL_carzd2108zd2_235))
																{	/* Read/jvm.scm 133 */
																	if (PAIRP(BgL_cdrzd2109zd2_236))
																		{	/* Read/jvm.scm 133 */
																			obj_t BgL_carzd2114zd2_239;

																			BgL_carzd2114zd2_239 =
																				CAR(BgL_cdrzd2109zd2_236);
																			if (STRINGP(BgL_carzd2114zd2_239))
																				{	/* Read/jvm.scm 133 */
																					if (NULLP(CDR(BgL_cdrzd2109zd2_236)))
																						{	/* Read/jvm.scm 133 */
																							BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00
																								(BgL_carzd2108zd2_235,
																								BgL_carzd2114zd2_239, BNIL);
																							{	/* Read/jvm.scm 136 */
																								obj_t BgL_arg1200z00_465;

																								BgL_arg1200z00_465 =
																									CDR(((obj_t) BgL_objz00_226));
																								{
																									obj_t BgL_objz00_825;

																									BgL_objz00_825 =
																										BgL_arg1200z00_465;
																									BgL_objz00_226 =
																										BgL_objz00_825;
																									goto
																										BgL_zc3z04anonymousza31189ze3z87_227;
																								}
																							}
																						}
																					else
																						{	/* Read/jvm.scm 133 */
																						BgL_tagzd2102zd2_232:
																							{	/* Read/jvm.scm 140 */
																								obj_t BgL_arg1201z00_246;

																								BgL_arg1201z00_246 =
																									CAR(((obj_t) BgL_objz00_226));
																								return
																									BGl_userzd2errorzd2zztools_errorz00
																									(CNST_TABLE_REF(1),
																									BGl_string1323z00zzread_jvmz00,
																									BgL_arg1201z00_246, BNIL);
																							}
																						}
																				}
																			else
																				{	/* Read/jvm.scm 133 */
																					goto BgL_tagzd2102zd2_232;
																				}
																		}
																	else
																		{	/* Read/jvm.scm 133 */
																			goto BgL_tagzd2102zd2_232;
																		}
																}
															else
																{	/* Read/jvm.scm 133 */
																	goto BgL_tagzd2102zd2_232;
																}
														}
													else
														{	/* Read/jvm.scm 133 */
															goto BgL_tagzd2102zd2_232;
														}
												}
											}
									}
								else
									{	/* Read/jvm.scm 127 */
										return
											BGl_userzd2errorzd2zztools_errorz00(CNST_TABLE_REF(1),
											BGl_string1323z00zzread_jvmz00, BgL_eofz00_221, BNIL);
									}
							}
					}
				}
			}
		}

	}



/* add-current-module-qualified-type-name! */
	obj_t
		BGl_addzd2currentzd2modulezd2qualifiedzd2typezd2namez12zc0zzread_jvmz00
		(void)
	{
		{	/* Read/jvm.scm 145 */
			{	/* Read/jvm.scm 147 */
				obj_t BgL_qtypez00_252;

				BgL_qtypez00_252 =
					BGl_getpropz00zz__r4_symbols_6_4z00
					(BGl_za2moduleza2z00zzmodule_modulez00, CNST_TABLE_REF(0));
				if (STRINGP(BgL_qtypez00_252))
					{	/* Read/jvm.scm 148 */
						return BFALSE;
					}
				else
					{	/* Read/jvm.scm 153 */
						bool_t BgL_test1419z00_836;

						if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
							{	/* Read/jvm.scm 153 */
								BgL_test1419z00_836 =
									(BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(3));
							}
						else
							{	/* Read/jvm.scm 153 */
								BgL_test1419z00_836 = ((bool_t) 1);
							}
						if (BgL_test1419z00_836)
							{	/* Read/jvm.scm 156 */
								obj_t BgL_qtz00_256;

								{	/* Read/jvm.scm 156 */
									bool_t BgL_test1421z00_841;

									{	/* Read/jvm.scm 156 */
										obj_t BgL_tmpz00_842;

										BgL_tmpz00_842 =
											CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
										BgL_test1421z00_841 = STRINGP(BgL_tmpz00_842);
									}
									if (BgL_test1421z00_841)
										{	/* Read/jvm.scm 157 */
											obj_t BgL_uqtypez00_260;

											BgL_uqtypez00_260 =
												BGl_prefixz00zz__osz00(BGl_basenamez00zz__osz00(CAR
													(BGl_za2srczd2filesza2zd2zzengine_paramz00)));
											if (BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00
												(BgL_uqtypez00_260))
												{	/* Read/jvm.scm 158 */
													BgL_qtz00_256 = bigloo_mangle(BgL_uqtypez00_260);
												}
											else
												{	/* Read/jvm.scm 158 */
													BgL_qtz00_256 = BgL_uqtypez00_260;
												}
										}
									else
										{	/* Read/jvm.scm 156 */
											BgL_qtz00_256 = BGl_string1324z00zzread_jvmz00;
										}
								}
								BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00
									(BGl_za2moduleza2z00zzmodule_modulez00, BgL_qtz00_256, BNIL);
								return BgL_qtz00_256;
							}
						else
							{	/* Read/jvm.scm 165 */
								obj_t BgL_qtz00_265;

								BgL_qtz00_265 =
									BGl_prefixz00zz__osz00(BGl_za2destza2z00zzengine_paramz00);
								BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00
									(BGl_za2moduleza2z00zzmodule_modulez00, BgL_qtz00_265, BNIL);
								return BgL_qtz00_265;
							}
					}
			}
		}

	}



/* module->qualified-type */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t
		BgL_modulez00_10)
	{
		{	/* Read/jvm.scm 175 */
			{	/* Read/jvm.scm 176 */
				obj_t BgL_bz00_268;

				BgL_bz00_268 =
					BGl_getpropz00zz__r4_symbols_6_4z00(BgL_modulez00_10,
					CNST_TABLE_REF(0));
				if (STRINGP(BgL_bz00_268))
					{	/* Read/jvm.scm 178 */
						return BgL_bz00_268;
					}
				else
					{	/* Read/jvm.scm 178 */
						if ((BgL_modulez00_10 == BGl_za2moduleza2z00zzmodule_modulez00))
							{	/* Read/jvm.scm 180 */
								return
									BGl_addzd2currentzd2modulezd2qualifiedzd2typezd2namez12zc0zzread_jvmz00
									();
							}
						else
							{	/* Read/jvm.scm 183 */
								obj_t BgL_abasez00_270;

								{	/* Read/jvm.scm 183 */
									obj_t BgL_l1056z00_287;

									BgL_l1056z00_287 =
										BGl_za2accesszd2filesza2zd2zzengine_paramz00;
									if (NULLP(BgL_l1056z00_287))
										{	/* Read/jvm.scm 183 */
											BgL_abasez00_270 = BNIL;
										}
									else
										{	/* Read/jvm.scm 183 */
											obj_t BgL_head1058z00_289;

											{	/* Read/jvm.scm 183 */
												obj_t BgL_arg1244z00_301;

												{	/* Read/jvm.scm 183 */
													obj_t BgL_arg1248z00_302;

													BgL_arg1248z00_302 = CAR(((obj_t) BgL_l1056z00_287));
													BgL_arg1244z00_301 =
														BGl_dirnamez00zz__osz00(BgL_arg1248z00_302);
												}
												BgL_head1058z00_289 =
													MAKE_YOUNG_PAIR(BgL_arg1244z00_301, BNIL);
											}
											{	/* Read/jvm.scm 183 */
												obj_t BgL_g1061z00_290;

												BgL_g1061z00_290 = CDR(((obj_t) BgL_l1056z00_287));
												{
													obj_t BgL_l1056z00_292;
													obj_t BgL_tail1059z00_293;

													BgL_l1056z00_292 = BgL_g1061z00_290;
													BgL_tail1059z00_293 = BgL_head1058z00_289;
												BgL_zc3z04anonymousza31236ze3z87_294:
													if (NULLP(BgL_l1056z00_292))
														{	/* Read/jvm.scm 183 */
															BgL_abasez00_270 = BgL_head1058z00_289;
														}
													else
														{	/* Read/jvm.scm 183 */
															obj_t BgL_newtail1060z00_296;

															{	/* Read/jvm.scm 183 */
																obj_t BgL_arg1239z00_298;

																{	/* Read/jvm.scm 183 */
																	obj_t BgL_arg1242z00_299;

																	BgL_arg1242z00_299 =
																		CAR(((obj_t) BgL_l1056z00_292));
																	BgL_arg1239z00_298 =
																		BGl_dirnamez00zz__osz00(BgL_arg1242z00_299);
																}
																BgL_newtail1060z00_296 =
																	MAKE_YOUNG_PAIR(BgL_arg1239z00_298, BNIL);
															}
															SET_CDR(BgL_tail1059z00_293,
																BgL_newtail1060z00_296);
															{	/* Read/jvm.scm 183 */
																obj_t BgL_arg1238z00_297;

																BgL_arg1238z00_297 =
																	CDR(((obj_t) BgL_l1056z00_292));
																{
																	obj_t BgL_tail1059z00_879;
																	obj_t BgL_l1056z00_878;

																	BgL_l1056z00_878 = BgL_arg1238z00_297;
																	BgL_tail1059z00_879 = BgL_newtail1060z00_296;
																	BgL_tail1059z00_293 = BgL_tail1059z00_879;
																	BgL_l1056z00_292 = BgL_l1056z00_878;
																	goto BgL_zc3z04anonymousza31236ze3z87_294;
																}
															}
														}
												}
											}
										}
								}
								{	/* Read/jvm.scm 183 */
									obj_t BgL_filesz00_271;

									{	/* Read/jvm.scm 184 */
										obj_t BgL_fun1233z00_286;

										BgL_fun1233z00_286 =
											BGl_bigloozd2modulezd2resolverz00zz__modulez00();
										BgL_filesz00_271 =
											BGL_PROCEDURE_CALL3(BgL_fun1233z00_286, BgL_modulez00_10,
											BNIL, BgL_abasez00_270);
									}
									{	/* Read/jvm.scm 184 */
										obj_t BgL_defaultz00_272;

										if (PAIRP(BgL_filesz00_271))
											{	/* Read/jvm.scm 185 */
												BgL_defaultz00_272 =
													BGl_prefixz00zz__osz00(BGl_basenamez00zz__osz00(CAR
														(BgL_filesz00_271)));
											}
										else
											{	/* Read/jvm.scm 187 */
												obj_t BgL_arg1455z00_477;

												BgL_arg1455z00_477 = SYMBOL_TO_STRING(BgL_modulez00_10);
												BgL_defaultz00_272 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_477);
											}
										{	/* Read/jvm.scm 185 */

											{	/* Read/jvm.scm 188 */
												bool_t BgL_test1428z00_894;

												{	/* Read/jvm.scm 188 */
													obj_t BgL_arg1228z00_281;

													BgL_arg1228z00_281 =
														BGl_thezd2backendzd2zzbackend_backendz00();
													BgL_test1428z00_894 =
														(((BgL_backendz00_bglt) COBJECT(
																((BgL_backendz00_bglt) BgL_arg1228z00_281)))->
														BgL_qualifiedzd2typeszd2);
												}
												if (BgL_test1428z00_894)
													{	/* Read/jvm.scm 191 */
														obj_t BgL_arg1221z00_275;

														{	/* Read/jvm.scm 191 */
															obj_t BgL_arg1227z00_280;

															{	/* Read/jvm.scm 191 */
																obj_t BgL_arg1455z00_480;

																BgL_arg1455z00_480 =
																	SYMBOL_TO_STRING(BgL_modulez00_10);
																BgL_arg1227z00_280 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_480);
															}
															BgL_arg1221z00_275 =
																string_append_3(BGl_string1325z00zzread_jvmz00,
																BgL_arg1227z00_280,
																BGl_string1326z00zzread_jvmz00);
														}
														{	/* Read/jvm.scm 189 */
															obj_t BgL_list1222z00_276;

															{	/* Read/jvm.scm 189 */
																obj_t BgL_arg1223z00_277;

																{	/* Read/jvm.scm 189 */
																	obj_t BgL_arg1225z00_278;

																	{	/* Read/jvm.scm 189 */
																		obj_t BgL_arg1226z00_279;

																		BgL_arg1226z00_279 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1327z00zzread_jvmz00, BNIL);
																		BgL_arg1225z00_278 =
																			MAKE_YOUNG_PAIR(BgL_defaultz00_272,
																			BgL_arg1226z00_279);
																	}
																	BgL_arg1223z00_277 =
																		MAKE_YOUNG_PAIR
																		(BGl_string1328z00zzread_jvmz00,
																		BgL_arg1225z00_278);
																}
																BgL_list1222z00_276 =
																	MAKE_YOUNG_PAIR(BgL_arg1221z00_275,
																	BgL_arg1223z00_277);
															}
															BGl_warningz00zz__errorz00(BgL_list1222z00_276);
														}
													}
												else
													{	/* Read/jvm.scm 188 */
														BFALSE;
													}
											}
											BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00
												(BgL_modulez00_10, BgL_defaultz00_272, BNIL);
											return BgL_defaultz00_272;
										}
									}
								}
							}
					}
			}
		}

	}



/* &module->qualified-type */
	obj_t BGl_z62modulezd2ze3qualifiedzd2typez81zzread_jvmz00(obj_t
		BgL_envz00_532, obj_t BgL_modulez00_533)
	{
		{	/* Read/jvm.scm 175 */
			return
				BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(BgL_modulez00_533);
		}

	}



/* source->qualified-type */
	BGL_EXPORTED_DEF obj_t BGl_sourcezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t
		BgL_filez00_11)
	{
		{	/* Read/jvm.scm 201 */
			if (fexists(BSTRING_TO_STRING(BgL_filez00_11)))
				{	/* Read/jvm.scm 202 */
					return
						BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00
						(BgL_filez00_11, BGl_proc1329z00zzread_jvmz00);
				}
			else
				{	/* Read/jvm.scm 202 */
					return BFALSE;
				}
		}

	}



/* &source->qualified-type */
	obj_t BGl_z62sourcezd2ze3qualifiedzd2typez81zzread_jvmz00(obj_t
		BgL_envz00_535, obj_t BgL_filez00_536)
	{
		{	/* Read/jvm.scm 201 */
			return BGl_sourcezd2ze3qualifiedzd2typeze3zzread_jvmz00(BgL_filez00_536);
		}

	}



/* &<@anonymous:1253> */
	obj_t BGl_z62zc3z04anonymousza31253ze3ze5zzread_jvmz00(obj_t BgL_envz00_537)
	{
		{	/* Read/jvm.scm 204 */
			{	/* Read/jvm.scm 205 */
				obj_t BgL_ezd2120zd2_549;

				{	/* Read/jvm.scm 205 */
					obj_t BgL_arg1304z00_550;

					{	/* Read/jvm.scm 205 */
						obj_t BgL_tmpz00_913;

						BgL_tmpz00_913 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1304z00_550 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_913);
					}
					{	/* Read/jvm.scm 205 */

						{	/* Read/jvm.scm 205 */

							BgL_ezd2120zd2_549 =
								BGl_readz00zz__readerz00(BgL_arg1304z00_550, BFALSE);
						}
					}
				}
				if (PAIRP(BgL_ezd2120zd2_549))
					{	/* Read/jvm.scm 204 */
						obj_t BgL_cdrzd2124zd2_551;

						BgL_cdrzd2124zd2_551 = CDR(BgL_ezd2120zd2_549);
						if ((CAR(BgL_ezd2120zd2_549) == CNST_TABLE_REF(4)))
							{	/* Read/jvm.scm 204 */
								if (PAIRP(BgL_cdrzd2124zd2_551))
									{	/* Read/jvm.scm 204 */
										return
											BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(CAR
											(BgL_cdrzd2124zd2_551));
									}
								else
									{	/* Read/jvm.scm 204 */
										return BFALSE;
									}
							}
						else
							{	/* Read/jvm.scm 204 */
								return BFALSE;
							}
					}
				else
					{	/* Read/jvm.scm 204 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzread_jvmz00(void)
	{
		{	/* Read/jvm.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzread_jvmz00(void)
	{
		{	/* Read/jvm.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzread_jvmz00(void)
	{
		{	/* Read/jvm.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzread_jvmz00(void)
	{
		{	/* Read/jvm.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1330z00zzread_jvmz00));
			BGl_modulezd2initializa7ationz75zzengine_enginez00(373986149L,
				BSTRING_TO_STRING(BGl_string1330z00zzread_jvmz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1330z00zzread_jvmz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1330z00zzread_jvmz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1330z00zzread_jvmz00));
			BGl_modulezd2initializa7ationz75zzinit_mainz00(288050968L,
				BSTRING_TO_STRING(BGl_string1330z00zzread_jvmz00));
			return
				BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1330z00zzread_jvmz00));
		}

	}

#ifdef __cplusplus
}
#endif
