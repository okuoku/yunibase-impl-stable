/*===========================================================================*/
/*   (Read/reader.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Read/reader.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_READ_READER_TYPE_DEFINITIONS
#define BGL_READ_READER_TYPE_DEFINITIONS
#endif													// BGL_READ_READER_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzread_readerz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_compilerzd2readzd2srcz00zzread_readerz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzread_readerz00(void);
	static obj_t BGl_objectzd2initzd2zzread_readerz00(void);
	BGL_IMPORT obj_t string_to_obj(obj_t, obj_t, obj_t);
	static obj_t BGl_getzd2compilerzd2readerz00zzread_readerz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62compilerzd2readzb0zzread_readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzread_readerz00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62compilerzd2readzd2srcz62zzread_readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_bigloozd2loadzd2readerz00zz__paramz00(void);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzread_readerz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__intextz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzread_readerz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzread_readerz00(void);
	BGL_EXPORTED_DECL obj_t BGl_compilerzd2readzd2zzread_readerz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzread_readerz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzread_readerz00(void);
	extern obj_t BGl_za2readerza2z00zzengine_paramz00;
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_compilerzd2readzd2envz00zzread_readerz00,
		BgL_bgl_za762compilerza7d2re1027z00, va_generic_entry,
		BGl_z62compilerzd2readzb0zzread_readerz00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_compilerzd2readzd2srczd2envzd2zzread_readerz00,
		BgL_bgl_za762compilerza7d2re1028z00, va_generic_entry,
		BGl_z62compilerzd2readzd2srcz62zzread_readerz00, BUNSPEC, -1);
	BGL_IMPORT obj_t BGl_readzd2envzd2zz__readerz00;
	   
		 
		DEFINE_STRING(BGl_string1022z00zzread_readerz00,
		BgL_bgl_string1022za700za7za7r1029za7, "", 0);
	      DEFINE_STRING(BGl_string1023z00zzread_readerz00,
		BgL_bgl_string1023za700za7za7r1030za7, "Illegal intern value", 20);
	      DEFINE_STRING(BGl_string1024z00zzread_readerz00,
		BgL_bgl_string1024za700za7za7r1031za7, "read_reader", 11);
	      DEFINE_STRING(BGl_string1025z00zzread_readerz00,
		BgL_bgl_string1025za700za7za7r1032za7, "intern-src intern ", 18);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzread_readerz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzread_readerz00(long
		BgL_checksumz00_46, char *BgL_fromz00_47)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzread_readerz00))
				{
					BGl_requirezd2initializa7ationz75zzread_readerz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzread_readerz00();
					BGl_libraryzd2moduleszd2initz00zzread_readerz00();
					BGl_cnstzd2initzd2zzread_readerz00();
					BGl_importedzd2moduleszd2initz00zzread_readerz00();
					return BGl_methodzd2initzd2zzread_readerz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzread_readerz00(void)
	{
		{	/* Read/reader.scm 16 */
			BGl_modulezd2initializa7ationz75zz__intextz00(0L, "read_reader");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "read_reader");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "read_reader");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "read_reader");
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "read_reader");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "read_reader");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L, "read_reader");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzread_readerz00(void)
	{
		{	/* Read/reader.scm 16 */
			{	/* Read/reader.scm 16 */
				obj_t BgL_cportz00_35;

				{	/* Read/reader.scm 16 */
					obj_t BgL_stringz00_42;

					BgL_stringz00_42 = BGl_string1025z00zzread_readerz00;
					{	/* Read/reader.scm 16 */
						obj_t BgL_startz00_43;

						BgL_startz00_43 = BINT(0L);
						{	/* Read/reader.scm 16 */
							obj_t BgL_endz00_44;

							BgL_endz00_44 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_42)));
							{	/* Read/reader.scm 16 */

								BgL_cportz00_35 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_42, BgL_startz00_43, BgL_endz00_44);
				}}}}
				{
					long BgL_iz00_36;

					BgL_iz00_36 = 1L;
				BgL_loopz00_37:
					if ((BgL_iz00_36 == -1L))
						{	/* Read/reader.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Read/reader.scm 16 */
							{	/* Read/reader.scm 16 */
								obj_t BgL_arg1026z00_38;

								{	/* Read/reader.scm 16 */

									{	/* Read/reader.scm 16 */
										obj_t BgL_locationz00_40;

										BgL_locationz00_40 = BBOOL(((bool_t) 0));
										{	/* Read/reader.scm 16 */

											BgL_arg1026z00_38 =
												BGl_readz00zz__readerz00(BgL_cportz00_35,
												BgL_locationz00_40);
										}
									}
								}
								{	/* Read/reader.scm 16 */
									int BgL_tmpz00_72;

									BgL_tmpz00_72 = (int) (BgL_iz00_36);
									CNST_TABLE_SET(BgL_tmpz00_72, BgL_arg1026z00_38);
							}}
							{	/* Read/reader.scm 16 */
								int BgL_auxz00_41;

								BgL_auxz00_41 = (int) ((BgL_iz00_36 - 1L));
								{
									long BgL_iz00_77;

									BgL_iz00_77 = (long) (BgL_auxz00_41);
									BgL_iz00_36 = BgL_iz00_77;
									goto BgL_loopz00_37;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzread_readerz00(void)
	{
		{	/* Read/reader.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* get-compiler-reader */
	obj_t BGl_getzd2compilerzd2readerz00zzread_readerz00(void)
	{
		{	/* Read/reader.scm 24 */
			{	/* Read/reader.scm 25 */
				obj_t BgL__ortest_1012z00_13;

				BgL__ortest_1012z00_13 = BGl_bigloozd2loadzd2readerz00zz__paramz00();
				if (CBOOL(BgL__ortest_1012z00_13))
					{	/* Read/reader.scm 25 */
						return BgL__ortest_1012z00_13;
					}
				else
					{	/* Read/reader.scm 25 */
						return BGl_readzd2envzd2zz__readerz00;
					}
			}
		}

	}



/* compiler-read */
	BGL_EXPORTED_DEF obj_t BGl_compilerzd2readzd2zzread_readerz00(obj_t
		BgL_argsz00_3)
	{
		{	/* Read/reader.scm 30 */
			{	/* Read/reader.scm 31 */
				obj_t BgL_readz00_14;

				BgL_readz00_14 = BGl_getzd2compilerzd2readerz00zzread_readerz00();
				{	/* Read/reader.scm 31 */
					obj_t BgL_valuez00_15;

					BgL_valuez00_15 = apply(BgL_readz00_14, BgL_argsz00_3);
					{	/* Read/reader.scm 32 */

						if ((BGl_za2readerza2z00zzengine_paramz00 == CNST_TABLE_REF(0)))
							{	/* Read/reader.scm 33 */
								if (EOF_OBJECTP(BgL_valuez00_15))
									{	/* Read/reader.scm 35 */
										return BgL_valuez00_15;
									}
								else
									{	/* Read/reader.scm 35 */
										if (STRINGP(BgL_valuez00_15))
											{	/* Read/reader.scm 38 */

												return string_to_obj(BgL_valuez00_15, BFALSE, BFALSE);
											}
										else
											{	/* Read/reader.scm 37 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string1022z00zzread_readerz00,
													BGl_string1023z00zzread_readerz00, BgL_valuez00_15);
											}
									}
							}
						else
							{	/* Read/reader.scm 33 */
								return BgL_valuez00_15;
							}
					}
				}
			}
		}

	}



/* &compiler-read */
	obj_t BGl_z62compilerzd2readzb0zzread_readerz00(obj_t BgL_envz00_31,
		obj_t BgL_argsz00_32)
	{
		{	/* Read/reader.scm 30 */
			return BGl_compilerzd2readzd2zzread_readerz00(BgL_argsz00_32);
		}

	}



/* compiler-read-src */
	BGL_EXPORTED_DEF obj_t BGl_compilerzd2readzd2srcz00zzread_readerz00(obj_t
		BgL_argsz00_4)
	{
		{	/* Read/reader.scm 46 */
			{	/* Read/reader.scm 47 */
				obj_t BgL_readz00_21;

				BgL_readz00_21 = BGl_getzd2compilerzd2readerz00zzread_readerz00();
				{	/* Read/reader.scm 47 */
					obj_t BgL_valuez00_22;

					BgL_valuez00_22 = apply(BgL_readz00_21, BgL_argsz00_4);
					{	/* Read/reader.scm 48 */

						if ((BGl_za2readerza2z00zzengine_paramz00 == CNST_TABLE_REF(1)))
							{	/* Read/reader.scm 49 */
								if (EOF_OBJECTP(BgL_valuez00_22))
									{	/* Read/reader.scm 51 */
										return BgL_valuez00_22;
									}
								else
									{	/* Read/reader.scm 51 */
										if (STRINGP(BgL_valuez00_22))
											{	/* Read/reader.scm 54 */

												return string_to_obj(BgL_valuez00_22, BFALSE, BFALSE);
											}
										else
											{	/* Read/reader.scm 53 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string1022z00zzread_readerz00,
													BGl_string1023z00zzread_readerz00, BgL_valuez00_22);
											}
									}
							}
						else
							{	/* Read/reader.scm 49 */
								return BgL_valuez00_22;
							}
					}
				}
			}
		}

	}



/* &compiler-read-src */
	obj_t BGl_z62compilerzd2readzd2srcz62zzread_readerz00(obj_t BgL_envz00_33,
		obj_t BgL_argsz00_34)
	{
		{	/* Read/reader.scm 46 */
			return BGl_compilerzd2readzd2srcz00zzread_readerz00(BgL_argsz00_34);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzread_readerz00(void)
	{
		{	/* Read/reader.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzread_readerz00(void)
	{
		{	/* Read/reader.scm 16 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzread_readerz00(void)
	{
		{	/* Read/reader.scm 16 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzread_readerz00(void)
	{
		{	/* Read/reader.scm 16 */
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1024z00zzread_readerz00));
		}

	}

#ifdef __cplusplus
}
#endif
