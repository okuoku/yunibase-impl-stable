/*===========================================================================*/
/*   (Read/load.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Read/load.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_READ_LOAD_TYPE_DEFINITIONS
#define BGL_READ_LOAD_TYPE_DEFINITIONS
#endif													// BGL_READ_LOAD_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzread_loadz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zzread_loadz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT obj_t BGl_readerzd2resetz12zc0zz__readerz00(void);
	static obj_t BGl_objectzd2initzd2zzread_loadz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzread_loadz00(void);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31093ze3ze5zzread_loadz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_loadzd2modulezd2zzread_loadz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzread_loadz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT obj_t BGl_loadqz00zz__evalz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzread_loadz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzread_loadz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzread_loadz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzread_loadz00(void);
	BGL_IMPORT obj_t BGl_za2loadzd2pathza2zd2zz__evalz00;
	static obj_t BGl_z62loadzd2modulezb0zzread_loadz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1105z00zzread_loadz00,
		BgL_bgl_string1105za700za7za7r1116za7, "]", 1);
	      DEFINE_STRING(BGl_string1106z00zzread_loadz00,
		BgL_bgl_string1106za700za7za7r1117za7, "      [reading loaded module ", 29);
	      DEFINE_STRING(BGl_string1107z00zzread_loadz00,
		BgL_bgl_string1107za700za7za7r1118za7, " vs ", 4);
	      DEFINE_STRING(BGl_string1108z00zzread_loadz00,
		BgL_bgl_string1108za700za7za7r1119za7, "conflict in module's name: ", 27);
	      DEFINE_STRING(BGl_string1109z00zzread_loadz00,
		BgL_bgl_string1109za700za7za7r1120za7, "load-module", 11);
	      DEFINE_STRING(BGl_string1110z00zzread_loadz00,
		BgL_bgl_string1110za700za7za7r1121za7, "Illegal module declaration", 26);
	      DEFINE_STRING(BGl_string1111z00zzread_loadz00,
		BgL_bgl_string1111za700za7za7r1122za7, "Can't open such file", 20);
	      DEFINE_STRING(BGl_string1112z00zzread_loadz00,
		BgL_bgl_string1112za700za7za7r1123za7, "Can't find file", 15);
	      DEFINE_STRING(BGl_string1113z00zzread_loadz00,
		BgL_bgl_string1113za700za7za7r1124za7, "read_load", 9);
	      DEFINE_STRING(BGl_string1114z00zzread_loadz00,
		BgL_bgl_string1114za700za7za7r1125za7, "module ", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_loadzd2modulezd2envz00zzread_loadz00,
		BgL_bgl_za762loadza7d2module1126z00, BGl_z62loadzd2modulezb0zzread_loadz00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzread_loadz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzread_loadz00(long
		BgL_checksumz00_166, char *BgL_fromz00_167)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzread_loadz00))
				{
					BGl_requirezd2initializa7ationz75zzread_loadz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzread_loadz00();
					BGl_libraryzd2moduleszd2initz00zzread_loadz00();
					BGl_cnstzd2initzd2zzread_loadz00();
					BGl_importedzd2moduleszd2initz00zzread_loadz00();
					return BGl_methodzd2initzd2zzread_loadz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzread_loadz00(void)
	{
		{	/* Read/load.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "read_load");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "read_load");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "read_load");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "read_load");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "read_load");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "read_load");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "read_load");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"read_load");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzread_loadz00(void)
	{
		{	/* Read/load.scm 15 */
			{	/* Read/load.scm 15 */
				obj_t BgL_cportz00_155;

				{	/* Read/load.scm 15 */
					obj_t BgL_stringz00_162;

					BgL_stringz00_162 = BGl_string1114z00zzread_loadz00;
					{	/* Read/load.scm 15 */
						obj_t BgL_startz00_163;

						BgL_startz00_163 = BINT(0L);
						{	/* Read/load.scm 15 */
							obj_t BgL_endz00_164;

							BgL_endz00_164 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_162)));
							{	/* Read/load.scm 15 */

								BgL_cportz00_155 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_162, BgL_startz00_163, BgL_endz00_164);
				}}}}
				{
					long BgL_iz00_156;

					BgL_iz00_156 = 0L;
				BgL_loopz00_157:
					if ((BgL_iz00_156 == -1L))
						{	/* Read/load.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Read/load.scm 15 */
							{	/* Read/load.scm 15 */
								obj_t BgL_arg1115z00_158;

								{	/* Read/load.scm 15 */

									{	/* Read/load.scm 15 */
										obj_t BgL_locationz00_160;

										BgL_locationz00_160 = BBOOL(((bool_t) 0));
										{	/* Read/load.scm 15 */

											BgL_arg1115z00_158 =
												BGl_readz00zz__readerz00(BgL_cportz00_155,
												BgL_locationz00_160);
										}
									}
								}
								{	/* Read/load.scm 15 */
									int BgL_tmpz00_193;

									BgL_tmpz00_193 = (int) (BgL_iz00_156);
									CNST_TABLE_SET(BgL_tmpz00_193, BgL_arg1115z00_158);
							}}
							{	/* Read/load.scm 15 */
								int BgL_auxz00_161;

								BgL_auxz00_161 = (int) ((BgL_iz00_156 - 1L));
								{
									long BgL_iz00_198;

									BgL_iz00_198 = (long) (BgL_auxz00_161);
									BgL_iz00_156 = BgL_iz00_198;
									goto BgL_loopz00_157;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzread_loadz00(void)
	{
		{	/* Read/load.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* load-module */
	BGL_EXPORTED_DEF obj_t BGl_loadzd2modulezd2zzread_loadz00(obj_t
		BgL_modulez00_3, obj_t BgL_fnamesz00_4)
	{
		{	/* Read/load.scm 26 */
			{	/* Read/load.scm 27 */
				obj_t BgL_list1064z00_72;

				{	/* Read/load.scm 27 */
					obj_t BgL_arg1065z00_73;

					{	/* Read/load.scm 27 */
						obj_t BgL_arg1066z00_74;

						{	/* Read/load.scm 27 */
							obj_t BgL_arg1068z00_75;

							BgL_arg1068z00_75 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1066z00_74 =
								MAKE_YOUNG_PAIR(BGl_string1105z00zzread_loadz00,
								BgL_arg1068z00_75);
						}
						BgL_arg1065z00_73 =
							MAKE_YOUNG_PAIR(BgL_modulez00_3, BgL_arg1066z00_74);
					}
					BgL_list1064z00_72 =
						MAKE_YOUNG_PAIR(BGl_string1106z00zzread_loadz00, BgL_arg1065z00_73);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1064z00_72);
			}
			{	/* Read/load.scm 28 */
				obj_t BgL_filez00_76;

				BgL_filez00_76 = CAR(BgL_fnamesz00_4);
				{	/* Read/load.scm 28 */
					obj_t BgL_fnamez00_77;

					BgL_fnamez00_77 =
						BGl_findzd2filezf2pathz20zz__osz00(BgL_filez00_76,
						BGl_za2loadzd2pathza2zd2zz__evalz00);
					{	/* Read/load.scm 29 */

						if (STRINGP(BgL_fnamez00_77))
							{	/* Read/load.scm 32 */
								obj_t BgL_portz00_79;

								{	/* Read/load.scm 32 */

									BgL_portz00_79 =
										BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
										(BgL_fnamez00_77, BTRUE, BINT(5000000L));
								}
								BGl_readerzd2resetz12zc0zz__readerz00();
								if (INPUT_PORTP(BgL_portz00_79))
									{	/* Read/load.scm 36 */
										obj_t BgL_exitd1052z00_81;

										BgL_exitd1052z00_81 = BGL_EXITD_TOP_AS_OBJ();
										{	/* Read/load.scm 52 */
											obj_t BgL_zc3z04anonymousza31093ze3z87_149;

											BgL_zc3z04anonymousza31093ze3z87_149 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31093ze3ze5zzread_loadz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31093ze3z87_149,
												(int) (0L), BgL_portz00_79);
											{	/* Read/load.scm 36 */
												obj_t BgL_arg1828z00_126;

												{	/* Read/load.scm 36 */
													obj_t BgL_arg1829z00_127;

													BgL_arg1829z00_127 =
														BGL_EXITD_PROTECT(BgL_exitd1052z00_81);
													BgL_arg1828z00_126 =
														MAKE_YOUNG_PAIR
														(BgL_zc3z04anonymousza31093ze3z87_149,
														BgL_arg1829z00_127);
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1052z00_81,
													BgL_arg1828z00_126);
												BUNSPEC;
											}
											{	/* Read/load.scm 36 */
												obj_t BgL_tmp1054z00_83;

												{	/* Read/load.scm 36 */
													obj_t BgL_declz00_84;

													{	/* Read/load.scm 36 */

														{	/* Read/load.scm 36 */

															BgL_declz00_84 =
																BGl_readz00zz__readerz00(BgL_portz00_79,
																BFALSE);
													}}
													{	/* Read/load.scm 37 */
														bool_t BgL_test1131z00_227;

														if (PAIRP(BgL_declz00_84))
															{	/* Read/load.scm 37 */
																BgL_test1131z00_227 =
																	(CAR(BgL_declz00_84) == CNST_TABLE_REF(0));
															}
														else
															{	/* Read/load.scm 37 */
																BgL_test1131z00_227 = ((bool_t) 0);
															}
														if (BgL_test1131z00_227)
															{	/* Read/load.scm 37 */
																if (
																	(CAR(CDR(BgL_declz00_84)) == BgL_modulez00_3))
																	{	/* Read/load.scm 44 */
																		BgL_tmp1054z00_83 = BFALSE;
																	}
																else
																	{	/* Read/load.scm 49 */
																		obj_t BgL_arg1078z00_90;

																		{	/* Read/load.scm 49 */
																			obj_t BgL_arg1080z00_92;
																			obj_t BgL_arg1082z00_93;

																			{	/* Read/load.scm 49 */
																				obj_t BgL_arg1455z00_134;

																				BgL_arg1455z00_134 =
																					SYMBOL_TO_STRING(BgL_modulez00_3);
																				BgL_arg1080z00_92 =
																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																					(BgL_arg1455z00_134);
																			}
																			{	/* Read/load.scm 50 */
																				obj_t BgL_arg1088z00_98;

																				BgL_arg1088z00_98 =
																					CAR(CDR(BgL_declz00_84));
																				{	/* Read/load.scm 50 */
																					obj_t BgL_arg1455z00_140;

																					BgL_arg1455z00_140 =
																						SYMBOL_TO_STRING(
																						((obj_t) BgL_arg1088z00_98));
																					BgL_arg1082z00_93 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1455z00_140);
																				}
																			}
																			{	/* Read/load.scm 47 */
																				obj_t BgL_list1083z00_94;

																				{	/* Read/load.scm 47 */
																					obj_t BgL_arg1084z00_95;

																					{	/* Read/load.scm 47 */
																						obj_t BgL_arg1085z00_96;

																						{	/* Read/load.scm 47 */
																							obj_t BgL_arg1087z00_97;

																							BgL_arg1087z00_97 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1082z00_93, BNIL);
																							BgL_arg1085z00_96 =
																								MAKE_YOUNG_PAIR
																								(BGl_string1107z00zzread_loadz00,
																								BgL_arg1087z00_97);
																						}
																						BgL_arg1084z00_95 =
																							MAKE_YOUNG_PAIR(BgL_arg1080z00_92,
																							BgL_arg1085z00_96);
																					}
																					BgL_list1083z00_94 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1108z00zzread_loadz00,
																						BgL_arg1084z00_95);
																				}
																				BgL_arg1078z00_90 =
																					BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																					(BgL_list1083z00_94);
																			}
																		}
																		BgL_tmp1054z00_83 =
																			BGl_userzd2errorzd2zztools_errorz00
																			(BGl_string1109z00zzread_loadz00,
																			BgL_arg1078z00_90, BgL_declz00_84, BNIL);
																	}
															}
														else
															{	/* Read/load.scm 37 */
																BgL_tmp1054z00_83 =
																	BGl_userzd2errorzd2zztools_errorz00
																	(BGl_string1109z00zzread_loadz00,
																	BGl_string1110z00zzread_loadz00,
																	BgL_declz00_84, BNIL);
															}
													}
												}
												{	/* Read/load.scm 36 */
													bool_t BgL_test1134z00_251;

													{	/* Read/load.scm 36 */
														obj_t BgL_arg1827z00_142;

														BgL_arg1827z00_142 =
															BGL_EXITD_PROTECT(BgL_exitd1052z00_81);
														BgL_test1134z00_251 = PAIRP(BgL_arg1827z00_142);
													}
													if (BgL_test1134z00_251)
														{	/* Read/load.scm 36 */
															obj_t BgL_arg1825z00_143;

															{	/* Read/load.scm 36 */
																obj_t BgL_arg1826z00_144;

																BgL_arg1826z00_144 =
																	BGL_EXITD_PROTECT(BgL_exitd1052z00_81);
																BgL_arg1825z00_143 =
																	CDR(((obj_t) BgL_arg1826z00_144));
															}
															BGL_EXITD_PROTECT_SET(BgL_exitd1052z00_81,
																BgL_arg1825z00_143);
															BUNSPEC;
														}
													else
														{	/* Read/load.scm 36 */
															BFALSE;
														}
												}
												bgl_close_input_port(BgL_portz00_79);
												BgL_tmp1054z00_83;
											}
										}
									}
								else
									{	/* Read/load.scm 34 */
										BGl_userzd2errorzd2zztools_errorz00
											(BGl_string1109z00zzread_loadz00,
											BGl_string1111z00zzread_loadz00, BgL_filez00_76, BNIL);
									}
							}
						else
							{	/* Read/load.scm 30 */
								BGl_userzd2errorzd2zztools_errorz00
									(BGl_string1109z00zzread_loadz00,
									BGl_string1112z00zzread_loadz00, BgL_filez00_76, BNIL);
							}
					}
				}
			}
			{
				obj_t BgL_l1058z00_113;

				{	/* Read/load.scm 53 */
					bool_t BgL_tmpz00_261;

					BgL_l1058z00_113 = BgL_fnamesz00_4;
				BgL_zc3z04anonymousza31096ze3z87_114:
					if (PAIRP(BgL_l1058z00_113))
						{	/* Read/load.scm 53 */
							{	/* Read/load.scm 53 */
								obj_t BgL_arg1102z00_116;

								BgL_arg1102z00_116 = CAR(BgL_l1058z00_113);
								{	/* Read/load.scm 53 */
									obj_t BgL_envz00_118;

									BgL_envz00_118 = BGl_defaultzd2environmentzd2zz__evalz00();
									{	/* Read/load.scm 53 */

										BGl_loadqz00zz__evalz00(BgL_arg1102z00_116, BgL_envz00_118);
									}
								}
							}
							{
								obj_t BgL_l1058z00_267;

								BgL_l1058z00_267 = CDR(BgL_l1058z00_113);
								BgL_l1058z00_113 = BgL_l1058z00_267;
								goto BgL_zc3z04anonymousza31096ze3z87_114;
							}
						}
					else
						{	/* Read/load.scm 53 */
							BgL_tmpz00_261 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_261);
				}
			}
		}

	}



/* &load-module */
	obj_t BGl_z62loadzd2modulezb0zzread_loadz00(obj_t BgL_envz00_150,
		obj_t BgL_modulez00_151, obj_t BgL_fnamesz00_152)
	{
		{	/* Read/load.scm 26 */
			return
				BGl_loadzd2modulezd2zzread_loadz00(BgL_modulez00_151,
				BgL_fnamesz00_152);
		}

	}



/* &<@anonymous:1093> */
	obj_t BGl_z62zc3z04anonymousza31093ze3ze5zzread_loadz00(obj_t BgL_envz00_153)
	{
		{	/* Read/load.scm 36 */
			{	/* Read/load.scm 52 */
				obj_t BgL_portz00_154;

				BgL_portz00_154 = PROCEDURE_REF(BgL_envz00_153, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_portz00_154));
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzread_loadz00(void)
	{
		{	/* Read/load.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzread_loadz00(void)
	{
		{	/* Read/load.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzread_loadz00(void)
	{
		{	/* Read/load.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzread_loadz00(void)
	{
		{	/* Read/load.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1113z00zzread_loadz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1113z00zzread_loadz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1113z00zzread_loadz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1113z00zzread_loadz00));
			return
				BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1113z00zzread_loadz00));
		}

	}

#ifdef __cplusplus
}
#endif
