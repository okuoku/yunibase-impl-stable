/*===========================================================================*/
/*   (Read/inline.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Read/inline.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_READ_INLINE_TYPE_DEFINITIONS
#define BGL_READ_INLINE_TYPE_DEFINITIONS
#endif													// BGL_READ_INLINE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_za2macrozd2definitionsza2zd2zzread_inlinez00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zzread_inlinez00 = BUNSPEC;
	static obj_t BGl_lookzd2forzf2expz20zzread_inlinez00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzread_inlinez00(void);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzread_inlinez00(void);
	static obj_t BGl_objectzd2initzd2zzread_inlinez00(void);
	static obj_t BGl_z62zc3z04anonymousza31313ze3ze5zzread_inlinez00(obj_t);
	static obj_t BGl_z62inlinezd2definitionzd2queuez62zzread_inlinez00(obj_t);
	static obj_t BGl_lookzd2forzd2definitionsz00zzread_inlinez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_openzd2inlinezd2filez00zzread_inlinez00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzread_inlinez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzread_inlinez00(void);
	BGL_EXPORTED_DECL obj_t BGl_inlinezd2finaliza7erz75zzread_inlinez00(void);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62inlinezd2finaliza7erz17zzread_inlinez00(obj_t);
	BGL_IMPORT obj_t bgl_remq_bang(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_lookzd2forzd2inlineszd2andzd2macrosz00zzread_inlinez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzread_inlinez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_importzd2macrozd2finaliza7erza7zzread_inlinez00(void);
	static obj_t BGl_z62zc3z04anonymousza31195ze3ze5zzread_inlinez00(obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zzread_inlinez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzread_inlinez00(void);
	extern obj_t BGl_compilerzd2readzd2zzread_readerz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzread_inlinez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzread_inlinez00(void);
	static obj_t BGl_za2inlinezd2definitionsza2zd2zzread_inlinez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_inlinezd2definitionzd2queuez00zzread_inlinez00(void);
	BGL_IMPORT obj_t BGl_za2loadzd2pathza2zd2zz__evalz00;
	static obj_t
		BGl_z62lookzd2forzd2inlineszd2andzd2macrosz62zzread_inlinez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	static obj_t BGl_loopze70ze7zzread_inlinez00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_compilezd2srfizf3z21zz__expander_srfi0z00(obj_t);
	static obj_t BGl_z62importzd2macrozd2finaliza7erzc5zzread_inlinez00(obj_t);
	static obj_t __cnst[15];


	   
		 
		DEFINE_STRING(BGl_string1600z00zzread_inlinez00,
		BgL_bgl_string1600za700za7za7r1610za7, "Can't find macro definition(s)",
		30);
	      DEFINE_STRING(BGl_string1601z00zzread_inlinez00,
		BgL_bgl_string1601za700za7za7r1611za7, "Can't find syntax definition(s)",
		31);
	      DEFINE_STRING(BGl_string1602z00zzread_inlinez00,
		BgL_bgl_string1602za700za7za7r1612za7, "Can't find expander definition(s)",
		33);
	      DEFINE_STRING(BGl_string1603z00zzread_inlinez00,
		BgL_bgl_string1603za700za7za7r1613za7,
		"Can't find inline/generic definition(s)", 39);
	      DEFINE_STRING(BGl_string1604z00zzread_inlinez00,
		BgL_bgl_string1604za700za7za7r1614za7, "Can't find file", 15);
	      DEFINE_STRING(BGl_string1605z00zzread_inlinez00,
		BgL_bgl_string1605za700za7za7r1615za7, "read_inline", 11);
	      DEFINE_STRING(BGl_string1606z00zzread_inlinez00,
		BgL_bgl_string1606za700za7za7r1616za7,
		"imported-macros unit imported-inlines cond-expand define-expander define-syntax define-macro define-inline bigloo-compile else @ sifun done begin inline ",
		153);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inlinezd2finaliza7erzd2envza7zzread_inlinez00,
		BgL_bgl_za762inlineza7d2fina1617z00,
		BGl_z62inlinezd2finaliza7erz17zzread_inlinez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1597z00zzread_inlinez00,
		BgL_bgl_string1597za700za7za7r1618za7, "Cannot open file for input", 26);
	      DEFINE_STRING(BGl_string1598z00zzread_inlinez00,
		BgL_bgl_string1598za700za7za7r1619za7, "Cannot find source file", 23);
	      DEFINE_STRING(BGl_string1599z00zzread_inlinez00,
		BgL_bgl_string1599za700za7za7r1620za7, "import", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inlinezd2definitionzd2queuezd2envzd2zzread_inlinez00,
		BgL_bgl_za762inlineza7d2defi1621z00,
		BGl_z62inlinezd2definitionzd2queuez62zzread_inlinez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_lookzd2forzd2inlineszd2andzd2macroszd2envzd2zzread_inlinez00,
		BgL_bgl_za762lookza7d2forza7d21622za7,
		BGl_z62lookzd2forzd2inlineszd2andzd2macrosz62zzread_inlinez00, 0L, BUNSPEC,
		8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_importzd2macrozd2finaliza7erzd2envz75zzread_inlinez00,
		BgL_bgl_za762importza7d2macr1623z00,
		BGl_z62importzd2macrozd2finaliza7erzc5zzread_inlinez00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2macrozd2definitionsza2zd2zzread_inlinez00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzread_inlinez00));
		   
			 ADD_ROOT((void *) (&BGl_za2inlinezd2definitionsza2zd2zzread_inlinez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzread_inlinez00(long
		BgL_checksumz00_1016, char *BgL_fromz00_1017)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzread_inlinez00))
				{
					BGl_requirezd2initializa7ationz75zzread_inlinez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzread_inlinez00();
					BGl_libraryzd2moduleszd2initz00zzread_inlinez00();
					BGl_cnstzd2initzd2zzread_inlinez00();
					BGl_importedzd2moduleszd2initz00zzread_inlinez00();
					return BGl_toplevelzd2initzd2zzread_inlinez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"read_inline");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"read_inline");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "read_inline");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "read_inline");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 15 */
			{	/* Read/inline.scm 15 */
				obj_t BgL_cportz00_1005;

				{	/* Read/inline.scm 15 */
					obj_t BgL_stringz00_1012;

					BgL_stringz00_1012 = BGl_string1606z00zzread_inlinez00;
					{	/* Read/inline.scm 15 */
						obj_t BgL_startz00_1013;

						BgL_startz00_1013 = BINT(0L);
						{	/* Read/inline.scm 15 */
							obj_t BgL_endz00_1014;

							BgL_endz00_1014 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1012)));
							{	/* Read/inline.scm 15 */

								BgL_cportz00_1005 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1012, BgL_startz00_1013, BgL_endz00_1014);
				}}}}
				{
					long BgL_iz00_1006;

					BgL_iz00_1006 = 14L;
				BgL_loopz00_1007:
					if ((BgL_iz00_1006 == -1L))
						{	/* Read/inline.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Read/inline.scm 15 */
							{	/* Read/inline.scm 15 */
								obj_t BgL_arg1609z00_1008;

								{	/* Read/inline.scm 15 */

									{	/* Read/inline.scm 15 */
										obj_t BgL_locationz00_1010;

										BgL_locationz00_1010 = BBOOL(((bool_t) 0));
										{	/* Read/inline.scm 15 */

											BgL_arg1609z00_1008 =
												BGl_readz00zz__readerz00(BgL_cportz00_1005,
												BgL_locationz00_1010);
										}
									}
								}
								{	/* Read/inline.scm 15 */
									int BgL_tmpz00_1048;

									BgL_tmpz00_1048 = (int) (BgL_iz00_1006);
									CNST_TABLE_SET(BgL_tmpz00_1048, BgL_arg1609z00_1008);
							}}
							{	/* Read/inline.scm 15 */
								int BgL_auxz00_1011;

								BgL_auxz00_1011 = (int) ((BgL_iz00_1006 - 1L));
								{
									long BgL_iz00_1053;

									BgL_iz00_1053 = (long) (BgL_auxz00_1011);
									BgL_iz00_1006 = BgL_iz00_1053;
									goto BgL_loopz00_1007;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 15 */
			BGl_za2inlinezd2definitionsza2zd2zzread_inlinez00 = BNIL;
			return (BGl_za2macrozd2definitionsza2zd2zzread_inlinez00 = BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzread_inlinez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_480;

				BgL_headz00_480 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_481;
					obj_t BgL_tailz00_482;

					BgL_prevz00_481 = BgL_headz00_480;
					BgL_tailz00_482 = BgL_l1z00_1;
				BgL_loopz00_483:
					if (PAIRP(BgL_tailz00_482))
						{
							obj_t BgL_newzd2prevzd2_485;

							BgL_newzd2prevzd2_485 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_482), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_481, BgL_newzd2prevzd2_485);
							{
								obj_t BgL_tailz00_1063;
								obj_t BgL_prevz00_1062;

								BgL_prevz00_1062 = BgL_newzd2prevzd2_485;
								BgL_tailz00_1063 = CDR(BgL_tailz00_482);
								BgL_tailz00_482 = BgL_tailz00_1063;
								BgL_prevz00_481 = BgL_prevz00_1062;
								goto BgL_loopz00_483;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_480);
				}
			}
		}

	}



/* open-inline-file */
	obj_t BGl_openzd2inlinezd2filez00zzread_inlinez00(obj_t BgL_filez00_25)
	{
		{	/* Read/inline.scm 37 */
			{	/* Read/inline.scm 38 */
				obj_t BgL_pathz00_502;

				BgL_pathz00_502 =
					BGl_findzd2filezf2pathz20zz__osz00(BgL_filez00_25,
					BGl_za2loadzd2pathza2zd2zz__evalz00);
				if (STRINGP(BgL_pathz00_502))
					{	/* Read/inline.scm 40 */
						obj_t BgL_portz00_504;

						{	/* Read/inline.scm 40 */

							BgL_portz00_504 =
								BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_pathz00_502, BTRUE, BINT(5000000L));
						}
						if (INPUT_PORTP(BgL_portz00_504))
							{	/* Read/inline.scm 41 */
								return BgL_portz00_504;
							}
						else
							{	/* Read/inline.scm 41 */
								return
									BGl_userzd2errorzd2zztools_errorz00(CNST_TABLE_REF(0),
									BGl_string1597z00zzread_inlinez00, BgL_pathz00_502, BNIL);
							}
					}
				else
					{	/* Read/inline.scm 39 */
						return
							BGl_userzd2errorzd2zztools_errorz00(CNST_TABLE_REF(0),
							BGl_string1598z00zzread_inlinez00, BgL_filez00_25, BNIL);
					}
			}
		}

	}



/* look-for-inlines-and-macros */
	BGL_EXPORTED_DEF obj_t
		BGl_lookzd2forzd2inlineszd2andzd2macrosz00zzread_inlinez00(obj_t
		BgL_inlinesz00_26, obj_t BgL_macrosz00_27, obj_t BgL_syntaxesz00_28,
		obj_t BgL_expandersz00_29, obj_t BgL_codez00_30, obj_t BgL_expsz00_31,
		obj_t BgL_fnamesz00_32, obj_t BgL_modulez00_33)
	{
		{	/* Read/inline.scm 53 */
			{	/* Read/inline.scm 55 */
				obj_t BgL_portz00_511;

				{	/* Read/inline.scm 55 */
					obj_t BgL_arg1196z00_520;

					BgL_arg1196z00_520 = CAR(((obj_t) BgL_fnamesz00_32));
					BgL_portz00_511 =
						BGl_openzd2inlinezd2filez00zzread_inlinez00(BgL_arg1196z00_520);
				}
				{	/* Read/inline.scm 56 */
					obj_t BgL_exitd1064z00_512;

					BgL_exitd1064z00_512 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Read/inline.scm 63 */
						obj_t BgL_zc3z04anonymousza31195ze3z87_985;

						BgL_zc3z04anonymousza31195ze3z87_985 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31195ze3ze5zzread_inlinez00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31195ze3z87_985, (int) (0L),
							BgL_portz00_511);
						{	/* Read/inline.scm 56 */
							obj_t BgL_arg1828z00_876;

							{	/* Read/inline.scm 56 */
								obj_t BgL_arg1829z00_877;

								BgL_arg1829z00_877 = BGL_EXITD_PROTECT(BgL_exitd1064z00_512);
								BgL_arg1828z00_876 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31195ze3z87_985,
									BgL_arg1829z00_877);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1064z00_512, BgL_arg1828z00_876);
							BUNSPEC;
						}
						{	/* Read/inline.scm 59 */
							obj_t BgL_tmp1066z00_514;

							{	/* Read/inline.scm 59 */
								obj_t BgL_list1192z00_515;

								{	/* Read/inline.scm 59 */
									obj_t BgL_arg1193z00_516;

									BgL_arg1193z00_516 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
									BgL_list1192z00_515 =
										MAKE_YOUNG_PAIR(BgL_portz00_511, BgL_arg1193z00_516);
								}
								BGl_compilerzd2readzd2zzread_readerz00(BgL_list1192z00_515);
							}
							{	/* Read/inline.scm 62 */
								obj_t BgL_arg1194z00_517;

								BgL_arg1194z00_517 = CDR(((obj_t) BgL_fnamesz00_32));
								BgL_tmp1066z00_514 =
									BGl_lookzd2forzd2definitionsz00zzread_inlinez00
									(BgL_inlinesz00_26, BgL_macrosz00_27, BgL_syntaxesz00_28,
									BgL_expandersz00_29, BgL_codez00_30, BgL_expsz00_31,
									BgL_arg1194z00_517, BgL_modulez00_33, BgL_portz00_511);
							}
							{	/* Read/inline.scm 56 */
								bool_t BgL_test1630z00_1095;

								{	/* Read/inline.scm 56 */
									obj_t BgL_arg1827z00_880;

									BgL_arg1827z00_880 = BGL_EXITD_PROTECT(BgL_exitd1064z00_512);
									BgL_test1630z00_1095 = PAIRP(BgL_arg1827z00_880);
								}
								if (BgL_test1630z00_1095)
									{	/* Read/inline.scm 56 */
										obj_t BgL_arg1825z00_881;

										{	/* Read/inline.scm 56 */
											obj_t BgL_arg1826z00_882;

											BgL_arg1826z00_882 =
												BGL_EXITD_PROTECT(BgL_exitd1064z00_512);
											BgL_arg1825z00_881 = CDR(((obj_t) BgL_arg1826z00_882));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1064z00_512,
											BgL_arg1825z00_881);
										BUNSPEC;
									}
								else
									{	/* Read/inline.scm 56 */
										BFALSE;
									}
							}
							bgl_close_input_port(((obj_t) BgL_portz00_511));
							return BgL_tmp1066z00_514;
						}
					}
				}
			}
		}

	}



/* &look-for-inlines-and-macros */
	obj_t BGl_z62lookzd2forzd2inlineszd2andzd2macrosz62zzread_inlinez00(obj_t
		BgL_envz00_986, obj_t BgL_inlinesz00_987, obj_t BgL_macrosz00_988,
		obj_t BgL_syntaxesz00_989, obj_t BgL_expandersz00_990,
		obj_t BgL_codez00_991, obj_t BgL_expsz00_992, obj_t BgL_fnamesz00_993,
		obj_t BgL_modulez00_994)
	{
		{	/* Read/inline.scm 53 */
			return
				BGl_lookzd2forzd2inlineszd2andzd2macrosz00zzread_inlinez00
				(BgL_inlinesz00_987, BgL_macrosz00_988, BgL_syntaxesz00_989,
				BgL_expandersz00_990, BgL_codez00_991, BgL_expsz00_992,
				BgL_fnamesz00_993, BgL_modulez00_994);
		}

	}



/* &<@anonymous:1195> */
	obj_t BGl_z62zc3z04anonymousza31195ze3ze5zzread_inlinez00(obj_t
		BgL_envz00_995)
	{
		{	/* Read/inline.scm 56 */
			{	/* Read/inline.scm 63 */
				obj_t BgL_portz00_996;

				BgL_portz00_996 = PROCEDURE_REF(BgL_envz00_995, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_portz00_996));
			}
		}

	}



/* look-for-definitions */
	obj_t BGl_lookzd2forzd2definitionsz00zzread_inlinez00(obj_t BgL_inlinesz00_34,
		obj_t BgL_macrosz00_35, obj_t BgL_syntaxesz00_36, obj_t BgL_expandersz00_37,
		obj_t BgL_codez00_38, obj_t BgL_expsz00_39, obj_t BgL_fnamesz00_40,
		obj_t BgL_modulez00_41, obj_t BgL_portz00_42)
	{
		{	/* Read/inline.scm 68 */
			{	/* Read/inline.scm 70 */
				obj_t BgL_g1068z00_521;

				{	/* Read/inline.scm 74 */
					obj_t BgL_arg1321z00_639;

					BgL_arg1321z00_639 =
						BGl_appendzd221011zd2zzread_inlinez00(BgL_expsz00_39,
						BgL_codez00_38);
					BgL_g1068z00_521 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BgL_arg1321z00_639);
				}
				return
					BGl_loopze70ze7zzread_inlinez00(BgL_modulez00_41, BgL_inlinesz00_34,
					BgL_macrosz00_35, BgL_syntaxesz00_36, BgL_expandersz00_37,
					BgL_g1068z00_521, BgL_fnamesz00_40, BgL_portz00_42);
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzread_inlinez00(obj_t BgL_modulez00_1003,
		obj_t BgL_inlinesz00_523, obj_t BgL_macrosz00_524,
		obj_t BgL_syntaxesz00_525, obj_t BgL_expandersz00_526, obj_t BgL_expz00_527,
		obj_t BgL_fnamesz00_528, obj_t BgL_portz00_529)
	{
		{	/* Read/inline.scm 70 */
		BGl_loopze70ze7zzread_inlinez00:
			{	/* Read/inline.scm 77 */
				obj_t BgL_inlinesz00_531;

				BgL_inlinesz00_531 =
					BGl_lookzd2forzf2expz20zzread_inlinez00(BgL_inlinesz00_523,
					BgL_macrosz00_524, BgL_syntaxesz00_525, BgL_expandersz00_526,
					BgL_expz00_527, BgL_modulez00_1003);
				{	/* Read/inline.scm 78 */
					obj_t BgL_macrosz00_532;
					obj_t BgL_syntaxesz00_533;
					obj_t BgL_expandersz00_534;

					{	/* Read/inline.scm 80 */
						obj_t BgL_tmpz00_885;

						{	/* Read/inline.scm 80 */
							int BgL_tmpz00_1114;

							BgL_tmpz00_1114 = (int) (1L);
							BgL_tmpz00_885 = BGL_MVALUES_VAL(BgL_tmpz00_1114);
						}
						{	/* Read/inline.scm 80 */
							int BgL_tmpz00_1117;

							BgL_tmpz00_1117 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_1117, BUNSPEC);
						}
						BgL_macrosz00_532 = BgL_tmpz00_885;
					}
					{	/* Read/inline.scm 80 */
						obj_t BgL_tmpz00_886;

						{	/* Read/inline.scm 80 */
							int BgL_tmpz00_1120;

							BgL_tmpz00_1120 = (int) (2L);
							BgL_tmpz00_886 = BGL_MVALUES_VAL(BgL_tmpz00_1120);
						}
						{	/* Read/inline.scm 80 */
							int BgL_tmpz00_1123;

							BgL_tmpz00_1123 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_1123, BUNSPEC);
						}
						BgL_syntaxesz00_533 = BgL_tmpz00_886;
					}
					{	/* Read/inline.scm 80 */
						obj_t BgL_tmpz00_887;

						{	/* Read/inline.scm 80 */
							int BgL_tmpz00_1126;

							BgL_tmpz00_1126 = (int) (3L);
							BgL_tmpz00_887 = BGL_MVALUES_VAL(BgL_tmpz00_1126);
						}
						{	/* Read/inline.scm 80 */
							int BgL_tmpz00_1129;

							BgL_tmpz00_1129 = (int) (3L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_1129, BUNSPEC);
						}
						BgL_expandersz00_534 = BgL_tmpz00_887;
					}
					{	/* Read/inline.scm 80 */
						bool_t BgL_test1631z00_1132;

						if (NULLP(BgL_inlinesz00_531))
							{	/* Read/inline.scm 80 */
								if (NULLP(BgL_macrosz00_532))
									{	/* Read/inline.scm 80 */
										if (NULLP(BgL_syntaxesz00_533))
											{	/* Read/inline.scm 81 */
												BgL_test1631z00_1132 = NULLP(BgL_expandersz00_534);
											}
										else
											{	/* Read/inline.scm 81 */
												BgL_test1631z00_1132 = ((bool_t) 0);
											}
									}
								else
									{	/* Read/inline.scm 80 */
										BgL_test1631z00_1132 = ((bool_t) 0);
									}
							}
						else
							{	/* Read/inline.scm 80 */
								BgL_test1631z00_1132 = ((bool_t) 0);
							}
						if (BgL_test1631z00_1132)
							{	/* Read/inline.scm 80 */
								return CNST_TABLE_REF(2);
							}
						else
							{	/* Read/inline.scm 80 */
								if (EOF_OBJECTP(BgL_expz00_527))
									{	/* Read/inline.scm 83 */
										if (NULLP(BgL_fnamesz00_528))
											{	/* Read/inline.scm 84 */
												if (PAIRP(BgL_macrosz00_532))
													{	/* Read/inline.scm 89 */
														obj_t BgL_arg1206z00_542;

														{	/* Read/inline.scm 89 */
															obj_t BgL_head1103z00_546;

															BgL_head1103z00_546 =
																MAKE_YOUNG_PAIR(CAR(CAR(BgL_macrosz00_532)),
																BNIL);
															{	/* Read/inline.scm 89 */
																obj_t BgL_g1106z00_547;

																BgL_g1106z00_547 = CDR(BgL_macrosz00_532);
																{
																	obj_t BgL_l1101z00_549;
																	obj_t BgL_tail1104z00_550;

																	BgL_l1101z00_549 = BgL_g1106z00_547;
																	BgL_tail1104z00_550 = BgL_head1103z00_546;
																BgL_zc3z04anonymousza31209ze3z87_551:
																	if (NULLP(BgL_l1101z00_549))
																		{	/* Read/inline.scm 89 */
																			BgL_arg1206z00_542 = BgL_head1103z00_546;
																		}
																	else
																		{	/* Read/inline.scm 89 */
																			obj_t BgL_newtail1105z00_553;

																			{	/* Read/inline.scm 89 */
																				obj_t BgL_arg1215z00_555;

																				{	/* Read/inline.scm 89 */
																					obj_t BgL_pairz00_892;

																					BgL_pairz00_892 =
																						CAR(((obj_t) BgL_l1101z00_549));
																					BgL_arg1215z00_555 =
																						CAR(BgL_pairz00_892);
																				}
																				BgL_newtail1105z00_553 =
																					MAKE_YOUNG_PAIR(BgL_arg1215z00_555,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1104z00_550,
																				BgL_newtail1105z00_553);
																			{	/* Read/inline.scm 89 */
																				obj_t BgL_arg1212z00_554;

																				BgL_arg1212z00_554 =
																					CDR(((obj_t) BgL_l1101z00_549));
																				{
																					obj_t BgL_tail1104z00_1161;
																					obj_t BgL_l1101z00_1160;

																					BgL_l1101z00_1160 =
																						BgL_arg1212z00_554;
																					BgL_tail1104z00_1161 =
																						BgL_newtail1105z00_553;
																					BgL_tail1104z00_550 =
																						BgL_tail1104z00_1161;
																					BgL_l1101z00_549 = BgL_l1101z00_1160;
																					goto
																						BgL_zc3z04anonymousza31209ze3z87_551;
																				}
																			}
																		}
																}
															}
														}
														return
															BGl_userzd2errorzd2zztools_errorz00
															(BGl_string1599z00zzread_inlinez00,
															BGl_string1600z00zzread_inlinez00,
															BgL_arg1206z00_542, BNIL);
													}
												else
													{	/* Read/inline.scm 86 */
														if (PAIRP(BgL_syntaxesz00_533))
															{	/* Read/inline.scm 93 */
																obj_t BgL_arg1221z00_561;

																{	/* Read/inline.scm 93 */
																	obj_t BgL_head1109z00_565;

																	BgL_head1109z00_565 =
																		MAKE_YOUNG_PAIR(CAR(CAR
																			(BgL_syntaxesz00_533)), BNIL);
																	{	/* Read/inline.scm 93 */
																		obj_t BgL_g1112z00_566;

																		BgL_g1112z00_566 = CDR(BgL_syntaxesz00_533);
																		{
																			obj_t BgL_l1107z00_568;
																			obj_t BgL_tail1110z00_569;

																			BgL_l1107z00_568 = BgL_g1112z00_566;
																			BgL_tail1110z00_569 = BgL_head1109z00_565;
																		BgL_zc3z04anonymousza31224ze3z87_570:
																			if (NULLP(BgL_l1107z00_568))
																				{	/* Read/inline.scm 93 */
																					BgL_arg1221z00_561 =
																						BgL_head1109z00_565;
																				}
																			else
																				{	/* Read/inline.scm 93 */
																					obj_t BgL_newtail1111z00_572;

																					{	/* Read/inline.scm 93 */
																						obj_t BgL_arg1227z00_574;

																						{	/* Read/inline.scm 93 */
																							obj_t BgL_pairz00_899;

																							BgL_pairz00_899 =
																								CAR(((obj_t) BgL_l1107z00_568));
																							BgL_arg1227z00_574 =
																								CAR(BgL_pairz00_899);
																						}
																						BgL_newtail1111z00_572 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1227z00_574, BNIL);
																					}
																					SET_CDR(BgL_tail1110z00_569,
																						BgL_newtail1111z00_572);
																					{	/* Read/inline.scm 93 */
																						obj_t BgL_arg1226z00_573;

																						BgL_arg1226z00_573 =
																							CDR(((obj_t) BgL_l1107z00_568));
																						{
																							obj_t BgL_tail1110z00_1179;
																							obj_t BgL_l1107z00_1178;

																							BgL_l1107z00_1178 =
																								BgL_arg1226z00_573;
																							BgL_tail1110z00_1179 =
																								BgL_newtail1111z00_572;
																							BgL_tail1110z00_569 =
																								BgL_tail1110z00_1179;
																							BgL_l1107z00_568 =
																								BgL_l1107z00_1178;
																							goto
																								BgL_zc3z04anonymousza31224ze3z87_570;
																						}
																					}
																				}
																		}
																	}
																}
																return
																	BGl_userzd2errorzd2zztools_errorz00
																	(BGl_string1599z00zzread_inlinez00,
																	BGl_string1601z00zzread_inlinez00,
																	BgL_arg1221z00_561, BNIL);
															}
														else
															{	/* Read/inline.scm 90 */
																if (PAIRP(BgL_expandersz00_534))
																	{	/* Read/inline.scm 97 */
																		obj_t BgL_arg1232z00_580;

																		{	/* Read/inline.scm 97 */
																			obj_t BgL_head1115z00_584;

																			BgL_head1115z00_584 =
																				MAKE_YOUNG_PAIR(CAR(CAR
																					(BgL_expandersz00_534)), BNIL);
																			{	/* Read/inline.scm 97 */
																				obj_t BgL_g1118z00_585;

																				BgL_g1118z00_585 =
																					CDR(BgL_expandersz00_534);
																				{
																					obj_t BgL_l1113z00_587;
																					obj_t BgL_tail1116z00_588;

																					BgL_l1113z00_587 = BgL_g1118z00_585;
																					BgL_tail1116z00_588 =
																						BgL_head1115z00_584;
																				BgL_zc3z04anonymousza31236ze3z87_589:
																					if (NULLP(BgL_l1113z00_587))
																						{	/* Read/inline.scm 97 */
																							BgL_arg1232z00_580 =
																								BgL_head1115z00_584;
																						}
																					else
																						{	/* Read/inline.scm 97 */
																							obj_t BgL_newtail1117z00_591;

																							{	/* Read/inline.scm 97 */
																								obj_t BgL_arg1239z00_593;

																								{	/* Read/inline.scm 97 */
																									obj_t BgL_pairz00_906;

																									BgL_pairz00_906 =
																										CAR(
																										((obj_t) BgL_l1113z00_587));
																									BgL_arg1239z00_593 =
																										CAR(BgL_pairz00_906);
																								}
																								BgL_newtail1117z00_591 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1239z00_593, BNIL);
																							}
																							SET_CDR(BgL_tail1116z00_588,
																								BgL_newtail1117z00_591);
																							{	/* Read/inline.scm 97 */
																								obj_t BgL_arg1238z00_592;

																								BgL_arg1238z00_592 =
																									CDR(
																									((obj_t) BgL_l1113z00_587));
																								{
																									obj_t BgL_tail1116z00_1197;
																									obj_t BgL_l1113z00_1196;

																									BgL_l1113z00_1196 =
																										BgL_arg1238z00_592;
																									BgL_tail1116z00_1197 =
																										BgL_newtail1117z00_591;
																									BgL_tail1116z00_588 =
																										BgL_tail1116z00_1197;
																									BgL_l1113z00_587 =
																										BgL_l1113z00_1196;
																									goto
																										BgL_zc3z04anonymousza31236ze3z87_589;
																								}
																							}
																						}
																				}
																			}
																		}
																		return
																			BGl_userzd2errorzd2zztools_errorz00
																			(BGl_string1599z00zzread_inlinez00,
																			BGl_string1602z00zzread_inlinez00,
																			BgL_arg1232z00_580, BNIL);
																	}
																else
																	{	/* Read/inline.scm 101 */
																		obj_t BgL_arg1249z00_598;

																		if (NULLP(BgL_inlinesz00_531))
																			{	/* Read/inline.scm 101 */
																				BgL_arg1249z00_598 = BNIL;
																			}
																		else
																			{	/* Read/inline.scm 101 */
																				obj_t BgL_head1121z00_602;

																				{	/* Read/inline.scm 101 */
																					obj_t BgL_arg1304z00_614;

																					{	/* Read/inline.scm 101 */
																						obj_t BgL_pairz00_910;

																						BgL_pairz00_910 =
																							CAR(((obj_t) BgL_inlinesz00_531));
																						BgL_arg1304z00_614 =
																							CAR(BgL_pairz00_910);
																					}
																					BgL_head1121z00_602 =
																						MAKE_YOUNG_PAIR(BgL_arg1304z00_614,
																						BNIL);
																				}
																				{	/* Read/inline.scm 101 */
																					obj_t BgL_g1124z00_603;

																					BgL_g1124z00_603 =
																						CDR(((obj_t) BgL_inlinesz00_531));
																					{
																						obj_t BgL_l1119z00_605;
																						obj_t BgL_tail1122z00_606;

																						BgL_l1119z00_605 = BgL_g1124z00_603;
																						BgL_tail1122z00_606 =
																							BgL_head1121z00_602;
																					BgL_zc3z04anonymousza31252ze3z87_607:
																						if (NULLP(BgL_l1119z00_605))
																							{	/* Read/inline.scm 101 */
																								BgL_arg1249z00_598 =
																									BgL_head1121z00_602;
																							}
																						else
																							{	/* Read/inline.scm 101 */
																								obj_t BgL_newtail1123z00_609;

																								{	/* Read/inline.scm 101 */
																									obj_t BgL_arg1272z00_611;

																									{	/* Read/inline.scm 101 */
																										obj_t BgL_pairz00_913;

																										BgL_pairz00_913 =
																											CAR(
																											((obj_t)
																												BgL_l1119z00_605));
																										BgL_arg1272z00_611 =
																											CAR(BgL_pairz00_913);
																									}
																									BgL_newtail1123z00_609 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1272z00_611, BNIL);
																								}
																								SET_CDR(BgL_tail1122z00_606,
																									BgL_newtail1123z00_609);
																								{	/* Read/inline.scm 101 */
																									obj_t BgL_arg1268z00_610;

																									BgL_arg1268z00_610 =
																										CDR(
																										((obj_t) BgL_l1119z00_605));
																									{
																										obj_t BgL_tail1122z00_1217;
																										obj_t BgL_l1119z00_1216;

																										BgL_l1119z00_1216 =
																											BgL_arg1268z00_610;
																										BgL_tail1122z00_1217 =
																											BgL_newtail1123z00_609;
																										BgL_tail1122z00_606 =
																											BgL_tail1122z00_1217;
																										BgL_l1119z00_605 =
																											BgL_l1119z00_1216;
																										goto
																											BgL_zc3z04anonymousza31252ze3z87_607;
																									}
																								}
																							}
																					}
																				}
																			}
																		return
																			BGl_userzd2errorzd2zztools_errorz00
																			(BGl_string1599z00zzread_inlinez00,
																			BGl_string1603z00zzread_inlinez00,
																			BgL_arg1249z00_598, BNIL);
																	}
															}
													}
											}
										else
											{	/* Read/inline.scm 102 */
												obj_t BgL_fnamez00_616;

												{	/* Read/inline.scm 102 */
													obj_t BgL_arg1317z00_631;

													BgL_arg1317z00_631 = CAR(((obj_t) BgL_fnamesz00_528));
													BgL_fnamez00_616 =
														BGl_findzd2filezf2pathz20zz__osz00
														(BgL_arg1317z00_631,
														BGl_za2loadzd2pathza2zd2zz__evalz00);
												}
												if (STRINGP(BgL_fnamez00_616))
													{	/* Read/inline.scm 105 */
														obj_t BgL_portz00_618;

														BgL_portz00_618 =
															BGl_openzd2inlinezd2filez00zzread_inlinez00
															(BgL_fnamez00_616);
														{	/* Read/inline.scm 106 */
															obj_t BgL_exitd1069z00_619;

															BgL_exitd1069z00_619 = BGL_EXITD_TOP_AS_OBJ();
															{	/* Read/inline.scm 114 */
																obj_t BgL_zc3z04anonymousza31313ze3z87_997;

																BgL_zc3z04anonymousza31313ze3z87_997 =
																	MAKE_FX_PROCEDURE
																	(BGl_z62zc3z04anonymousza31313ze3ze5zzread_inlinez00,
																	(int) (0L), (int) (1L));
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31313ze3z87_997,
																	(int) (0L), BgL_portz00_618);
																{	/* Read/inline.scm 106 */
																	obj_t BgL_arg1828z00_918;

																	{	/* Read/inline.scm 106 */
																		obj_t BgL_arg1829z00_919;

																		BgL_arg1829z00_919 =
																			BGL_EXITD_PROTECT(BgL_exitd1069z00_619);
																		BgL_arg1828z00_918 =
																			MAKE_YOUNG_PAIR
																			(BgL_zc3z04anonymousza31313ze3z87_997,
																			BgL_arg1829z00_919);
																	}
																	BGL_EXITD_PROTECT_SET(BgL_exitd1069z00_619,
																		BgL_arg1828z00_918);
																	BUNSPEC;
																}
																{	/* Read/inline.scm 111 */
																	obj_t BgL_tmp1071z00_621;

																	{	/* Read/inline.scm 111 */
																		obj_t BgL_arg1308z00_622;
																		obj_t BgL_arg1310z00_623;

																		{	/* Read/inline.scm 111 */
																			obj_t BgL_list1311z00_624;

																			{	/* Read/inline.scm 111 */
																				obj_t BgL_arg1312z00_625;

																				BgL_arg1312z00_625 =
																					MAKE_YOUNG_PAIR(BTRUE, BNIL);
																				BgL_list1311z00_624 =
																					MAKE_YOUNG_PAIR(BgL_portz00_618,
																					BgL_arg1312z00_625);
																			}
																			BgL_arg1308z00_622 =
																				BGl_compilerzd2readzd2zzread_readerz00
																				(BgL_list1311z00_624);
																		}
																		BgL_arg1310z00_623 =
																			CDR(((obj_t) BgL_fnamesz00_528));
																		BgL_tmp1071z00_621 =
																			BGl_loopze70ze7zzread_inlinez00
																			(BgL_modulez00_1003, BgL_inlinesz00_531,
																			BgL_macrosz00_532, BgL_syntaxesz00_533,
																			BgL_expandersz00_534, BgL_arg1308z00_622,
																			BgL_arg1310z00_623, BgL_portz00_618);
																	}
																	{	/* Read/inline.scm 106 */
																		bool_t BgL_test1646z00_1240;

																		{	/* Read/inline.scm 106 */
																			obj_t BgL_arg1827z00_922;

																			BgL_arg1827z00_922 =
																				BGL_EXITD_PROTECT(BgL_exitd1069z00_619);
																			BgL_test1646z00_1240 =
																				PAIRP(BgL_arg1827z00_922);
																		}
																		if (BgL_test1646z00_1240)
																			{	/* Read/inline.scm 106 */
																				obj_t BgL_arg1825z00_923;

																				{	/* Read/inline.scm 106 */
																					obj_t BgL_arg1826z00_924;

																					BgL_arg1826z00_924 =
																						BGL_EXITD_PROTECT
																						(BgL_exitd1069z00_619);
																					BgL_arg1825z00_923 =
																						CDR(((obj_t) BgL_arg1826z00_924));
																				}
																				BGL_EXITD_PROTECT_SET
																					(BgL_exitd1069z00_619,
																					BgL_arg1825z00_923);
																				BUNSPEC;
																			}
																		else
																			{	/* Read/inline.scm 106 */
																				BFALSE;
																			}
																	}
																	BGl_z62zc3z04anonymousza31313ze3ze5zzread_inlinez00
																		(BgL_zc3z04anonymousza31313ze3z87_997);
																	return BgL_tmp1071z00_621;
																}
															}
														}
													}
												else
													{	/* Read/inline.scm 104 */
														obj_t BgL_arg1315z00_629;

														BgL_arg1315z00_629 =
															CAR(((obj_t) BgL_fnamesz00_528));
														return
															BGl_userzd2errorzd2zztools_errorz00
															(BGl_string1599z00zzread_inlinez00,
															BGl_string1604z00zzread_inlinez00,
															BgL_arg1315z00_629, BNIL);
													}
											}
									}
								else
									{	/* Read/inline.scm 121 */
										obj_t BgL_arg1318z00_632;

										{	/* Read/inline.scm 121 */
											obj_t BgL_list1319z00_633;

											{	/* Read/inline.scm 121 */
												obj_t BgL_arg1320z00_634;

												BgL_arg1320z00_634 = MAKE_YOUNG_PAIR(BTRUE, BNIL);
												BgL_list1319z00_633 =
													MAKE_YOUNG_PAIR(BgL_portz00_529, BgL_arg1320z00_634);
											}
											BgL_arg1318z00_632 =
												BGl_compilerzd2readzd2zzread_readerz00
												(BgL_list1319z00_633);
										}
										{
											obj_t BgL_expz00_1258;
											obj_t BgL_expandersz00_1257;
											obj_t BgL_syntaxesz00_1256;
											obj_t BgL_macrosz00_1255;
											obj_t BgL_inlinesz00_1254;

											BgL_inlinesz00_1254 = BgL_inlinesz00_531;
											BgL_macrosz00_1255 = BgL_macrosz00_532;
											BgL_syntaxesz00_1256 = BgL_syntaxesz00_533;
											BgL_expandersz00_1257 = BgL_expandersz00_534;
											BgL_expz00_1258 = BgL_arg1318z00_632;
											BgL_expz00_527 = BgL_expz00_1258;
											BgL_expandersz00_526 = BgL_expandersz00_1257;
											BgL_syntaxesz00_525 = BgL_syntaxesz00_1256;
											BgL_macrosz00_524 = BgL_macrosz00_1255;
											BgL_inlinesz00_523 = BgL_inlinesz00_1254;
											goto BGl_loopze70ze7zzread_inlinez00;
										}
									}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1313> */
	obj_t BGl_z62zc3z04anonymousza31313ze3ze5zzread_inlinez00(obj_t
		BgL_envz00_998)
	{
		{	/* Read/inline.scm 106 */
			{	/* Read/inline.scm 114 */
				obj_t BgL_portz00_999;

				BgL_portz00_999 = PROCEDURE_REF(BgL_envz00_998, (int) (0L));
				if (INPUT_PORTP(BgL_portz00_999))
					{	/* Read/inline.scm 114 */
						return bgl_close_input_port(BgL_portz00_999);
					}
				else
					{	/* Read/inline.scm 114 */
						return BFALSE;
					}
			}
		}

	}



/* look-for/exp */
	obj_t BGl_lookzd2forzf2expz20zzread_inlinez00(obj_t BgL_inlinesz00_43,
		obj_t BgL_macrosz00_44, obj_t BgL_syntaxesz00_45, obj_t BgL_expandersz00_46,
		obj_t BgL_expz00_47, obj_t BgL_modulez00_48)
	{
		{	/* Read/inline.scm 128 */
			{	/* Read/inline.scm 129 */
				bool_t BgL_test1648z00_1264;

				if (NULLP(BgL_inlinesz00_43))
					{	/* Read/inline.scm 129 */
						if (NULLP(BgL_macrosz00_44))
							{	/* Read/inline.scm 129 */
								if (NULLP(BgL_syntaxesz00_45))
									{	/* Read/inline.scm 129 */
										BgL_test1648z00_1264 = NULLP(BgL_expandersz00_46);
									}
								else
									{	/* Read/inline.scm 129 */
										BgL_test1648z00_1264 = ((bool_t) 0);
									}
							}
						else
							{	/* Read/inline.scm 129 */
								BgL_test1648z00_1264 = ((bool_t) 0);
							}
					}
				else
					{	/* Read/inline.scm 129 */
						BgL_test1648z00_1264 = ((bool_t) 0);
					}
				if (BgL_test1648z00_1264)
					{	/* Read/inline.scm 129 */
						{	/* Read/inline.scm 130 */
							int BgL_tmpz00_1272;

							BgL_tmpz00_1272 = (int) (4L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1272);
						}
						{	/* Read/inline.scm 130 */
							int BgL_tmpz00_1275;

							BgL_tmpz00_1275 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_1275, BNIL);
						}
						{	/* Read/inline.scm 130 */
							int BgL_tmpz00_1278;

							BgL_tmpz00_1278 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_1278, BNIL);
						}
						{	/* Read/inline.scm 130 */
							int BgL_tmpz00_1281;

							BgL_tmpz00_1281 = (int) (3L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_1281, BNIL);
						}
						return BNIL;
					}
				else
					{
						obj_t BgL_expza2za2_660;
						obj_t BgL_expza2za2_658;
						obj_t BgL_idz00_656;
						obj_t BgL_idz00_654;
						obj_t BgL_idz00_652;
						obj_t BgL_protoz00_648;
						obj_t BgL_namez00_649;
						obj_t BgL_bodyz00_650;

						if (PAIRP(BgL_expz00_47))
							{	/* Read/inline.scm 131 */
								obj_t BgL_cdrzd2376zd2_665;

								BgL_cdrzd2376zd2_665 = CDR(((obj_t) BgL_expz00_47));
								if ((CAR(((obj_t) BgL_expz00_47)) == CNST_TABLE_REF(7)))
									{	/* Read/inline.scm 131 */
										if (PAIRP(BgL_cdrzd2376zd2_665))
											{	/* Read/inline.scm 131 */
												obj_t BgL_carzd2380zd2_669;

												BgL_carzd2380zd2_669 = CAR(BgL_cdrzd2376zd2_665);
												if (PAIRP(BgL_carzd2380zd2_669))
													{	/* Read/inline.scm 131 */
														BgL_protoz00_648 = BgL_carzd2380zd2_669;
														BgL_namez00_649 = CAR(BgL_carzd2380zd2_669);
														BgL_bodyz00_650 = CDR(BgL_cdrzd2376zd2_665);
														{	/* Read/inline.scm 133 */
															obj_t BgL_idz00_705;

															BgL_idz00_705 =
																BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																(BgL_namez00_649,
																BGl_findzd2locationzd2zztools_locationz00
																(BgL_expz00_47));
															{	/* Read/inline.scm 133 */
																obj_t BgL_cellz00_706;

																BgL_cellz00_706 =
																	BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																	(BgL_idz00_705, BgL_inlinesz00_43);
																{	/* Read/inline.scm 134 */
																	obj_t BgL_inlz00_707;

																	{	/* Read/inline.scm 135 */
																		bool_t BgL_test1657z00_1301;

																		if (PAIRP(BgL_cellz00_706))
																			{	/* Read/inline.scm 135 */
																				BgL_test1657z00_1301 =
																					(CDR(BgL_cellz00_706) ==
																					CNST_TABLE_REF(3));
																			}
																		else
																			{	/* Read/inline.scm 135 */
																				BgL_test1657z00_1301 = ((bool_t) 0);
																			}
																		if (BgL_test1657z00_1301)
																			{	/* Read/inline.scm 135 */
																				{	/* Read/inline.scm 137 */
																					obj_t BgL_arg1421z00_715;

																					{	/* Read/inline.scm 137 */
																						obj_t BgL_arg1422z00_716;

																						{	/* Read/inline.scm 137 */
																							obj_t BgL_arg1434z00_717;

																							BgL_arg1434z00_717 =
																								MAKE_YOUNG_PAIR
																								(BgL_modulez00_48, BNIL);
																							BgL_arg1422z00_716 =
																								MAKE_YOUNG_PAIR(BgL_idz00_705,
																								BgL_arg1434z00_717);
																						}
																						BgL_arg1421z00_715 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																							BgL_arg1422z00_716);
																					}
																					SET_CAR(BgL_protoz00_648,
																						BgL_arg1421z00_715);
																				}
																				BGl_za2inlinezd2definitionsza2zd2zzread_inlinez00
																					=
																					MAKE_YOUNG_PAIR(BgL_expz00_47,
																					BGl_za2inlinezd2definitionsza2zd2zzread_inlinez00);
																				BgL_inlz00_707 =
																					bgl_remq_bang(BgL_cellz00_706,
																					BgL_inlinesz00_43);
																			}
																		else
																			{	/* Read/inline.scm 135 */
																				BgL_inlz00_707 = BgL_inlinesz00_43;
																			}
																	}
																	{	/* Read/inline.scm 135 */

																		{	/* Read/inline.scm 142 */
																			int BgL_tmpz00_1314;

																			BgL_tmpz00_1314 = (int) (4L);
																			BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1314);
																		}
																		{	/* Read/inline.scm 142 */
																			int BgL_tmpz00_1317;

																			BgL_tmpz00_1317 = (int) (1L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_1317,
																				BgL_macrosz00_44);
																		}
																		{	/* Read/inline.scm 142 */
																			int BgL_tmpz00_1320;

																			BgL_tmpz00_1320 = (int) (2L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_1320,
																				BgL_syntaxesz00_45);
																		}
																		{	/* Read/inline.scm 142 */
																			int BgL_tmpz00_1323;

																			BgL_tmpz00_1323 = (int) (3L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_1323,
																				BgL_expandersz00_46);
																		}
																		return BgL_inlz00_707;
																	}
																}
															}
														}
													}
												else
													{	/* Read/inline.scm 131 */
													BgL_tagzd2365zd2_662:
														{	/* Read/inline.scm 208 */
															int BgL_tmpz00_1328;

															BgL_tmpz00_1328 = (int) (4L);
															BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1328);
														}
														{	/* Read/inline.scm 208 */
															int BgL_tmpz00_1331;

															BgL_tmpz00_1331 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_1331,
																BgL_macrosz00_44);
														}
														{	/* Read/inline.scm 208 */
															int BgL_tmpz00_1334;

															BgL_tmpz00_1334 = (int) (2L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_1334,
																BgL_syntaxesz00_45);
														}
														{	/* Read/inline.scm 208 */
															int BgL_tmpz00_1337;

															BgL_tmpz00_1337 = (int) (3L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_1337,
																BgL_expandersz00_46);
														}
														return BgL_inlinesz00_43;
													}
											}
										else
											{	/* Read/inline.scm 131 */
												goto BgL_tagzd2365zd2_662;
											}
									}
								else
									{	/* Read/inline.scm 131 */
										if ((CAR(((obj_t) BgL_expz00_47)) == CNST_TABLE_REF(8)))
											{	/* Read/inline.scm 131 */
												if (PAIRP(BgL_cdrzd2376zd2_665))
													{	/* Read/inline.scm 131 */
														obj_t BgL_carzd2423zd2_677;

														BgL_carzd2423zd2_677 = CAR(BgL_cdrzd2376zd2_665);
														if (PAIRP(BgL_carzd2423zd2_677))
															{	/* Read/inline.scm 131 */
																obj_t BgL_carzd2426zd2_679;

																BgL_carzd2426zd2_679 =
																	CAR(BgL_carzd2423zd2_677);
																if (SYMBOLP(BgL_carzd2426zd2_679))
																	{	/* Read/inline.scm 131 */
																		BgL_idz00_652 = BgL_carzd2426zd2_679;
																		{	/* Read/inline.scm 144 */
																			obj_t BgL_cellz00_721;

																			BgL_cellz00_721 =
																				BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_idz00_652, BgL_macrosz00_44);
																			{	/* Read/inline.scm 144 */
																				obj_t BgL_macz00_722;

																				if (PAIRP(BgL_cellz00_721))
																					{	/* Read/inline.scm 145 */
																						BGl_za2macrozd2definitionsza2zd2zzread_inlinez00
																							=
																							MAKE_YOUNG_PAIR(BgL_expz00_47,
																							BGl_za2macrozd2definitionsza2zd2zzread_inlinez00);
																						BgL_macz00_722 =
																							bgl_remq_bang(BgL_cellz00_721,
																							BgL_macrosz00_44);
																					}
																				else
																					{	/* Read/inline.scm 145 */
																						BgL_macz00_722 = BgL_macrosz00_44;
																					}
																				{	/* Read/inline.scm 145 */

																					{	/* Read/inline.scm 151 */
																						int BgL_tmpz00_1358;

																						BgL_tmpz00_1358 = (int) (4L);
																						BGL_MVALUES_NUMBER_SET
																							(BgL_tmpz00_1358);
																					}
																					{	/* Read/inline.scm 151 */
																						int BgL_tmpz00_1361;

																						BgL_tmpz00_1361 = (int) (1L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_1361,
																							BgL_macz00_722);
																					}
																					{	/* Read/inline.scm 151 */
																						int BgL_tmpz00_1364;

																						BgL_tmpz00_1364 = (int) (2L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_1364,
																							BgL_syntaxesz00_45);
																					}
																					{	/* Read/inline.scm 151 */
																						int BgL_tmpz00_1367;

																						BgL_tmpz00_1367 = (int) (3L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_1367,
																							BgL_expandersz00_46);
																					}
																					return BgL_inlinesz00_43;
																				}
																			}
																		}
																	}
																else
																	{	/* Read/inline.scm 131 */
																		goto BgL_tagzd2365zd2_662;
																	}
															}
														else
															{	/* Read/inline.scm 131 */
																goto BgL_tagzd2365zd2_662;
															}
													}
												else
													{	/* Read/inline.scm 131 */
														goto BgL_tagzd2365zd2_662;
													}
											}
										else
											{	/* Read/inline.scm 131 */
												obj_t BgL_cdrzd2464zd2_681;

												BgL_cdrzd2464zd2_681 = CDR(((obj_t) BgL_expz00_47));
												if ((CAR(((obj_t) BgL_expz00_47)) == CNST_TABLE_REF(9)))
													{	/* Read/inline.scm 131 */
														if (PAIRP(BgL_cdrzd2464zd2_681))
															{	/* Read/inline.scm 131 */
																obj_t BgL_carzd2466zd2_685;

																BgL_carzd2466zd2_685 =
																	CAR(BgL_cdrzd2464zd2_681);
																if (SYMBOLP(BgL_carzd2466zd2_685))
																	{	/* Read/inline.scm 131 */
																		BgL_idz00_654 = BgL_carzd2466zd2_685;
																		{	/* Read/inline.scm 153 */
																			obj_t BgL_cellz00_728;

																			BgL_cellz00_728 =
																				BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_idz00_654, BgL_syntaxesz00_45);
																			{	/* Read/inline.scm 153 */
																				obj_t BgL_synz00_729;

																				if (PAIRP(BgL_cellz00_728))
																					{	/* Read/inline.scm 154 */
																						BGl_za2macrozd2definitionsza2zd2zzread_inlinez00
																							=
																							MAKE_YOUNG_PAIR(BgL_expz00_47,
																							BGl_za2macrozd2definitionsza2zd2zzread_inlinez00);
																						BgL_synz00_729 =
																							bgl_remq_bang(BgL_cellz00_728,
																							BgL_syntaxesz00_45);
																					}
																				else
																					{	/* Read/inline.scm 154 */
																						BgL_synz00_729 = BgL_syntaxesz00_45;
																					}
																				{	/* Read/inline.scm 154 */

																					{	/* Read/inline.scm 160 */
																						int BgL_tmpz00_1387;

																						BgL_tmpz00_1387 = (int) (4L);
																						BGL_MVALUES_NUMBER_SET
																							(BgL_tmpz00_1387);
																					}
																					{	/* Read/inline.scm 160 */
																						int BgL_tmpz00_1390;

																						BgL_tmpz00_1390 = (int) (1L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_1390,
																							BgL_macrosz00_44);
																					}
																					{	/* Read/inline.scm 160 */
																						int BgL_tmpz00_1393;

																						BgL_tmpz00_1393 = (int) (2L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_1393,
																							BgL_synz00_729);
																					}
																					{	/* Read/inline.scm 160 */
																						int BgL_tmpz00_1396;

																						BgL_tmpz00_1396 = (int) (3L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_1396,
																							BgL_expandersz00_46);
																					}
																					return BgL_inlinesz00_43;
																				}
																			}
																		}
																	}
																else
																	{	/* Read/inline.scm 131 */
																		goto BgL_tagzd2365zd2_662;
																	}
															}
														else
															{	/* Read/inline.scm 131 */
																goto BgL_tagzd2365zd2_662;
															}
													}
												else
													{	/* Read/inline.scm 131 */
														if (
															(CAR(
																	((obj_t) BgL_expz00_47)) ==
																CNST_TABLE_REF(10)))
															{	/* Read/inline.scm 131 */
																if (PAIRP(BgL_cdrzd2464zd2_681))
																	{	/* Read/inline.scm 131 */
																		obj_t BgL_carzd2490zd2_691;

																		BgL_carzd2490zd2_691 =
																			CAR(BgL_cdrzd2464zd2_681);
																		if (SYMBOLP(BgL_carzd2490zd2_691))
																			{	/* Read/inline.scm 131 */
																				BgL_idz00_656 = BgL_carzd2490zd2_691;
																				{	/* Read/inline.scm 162 */
																					obj_t BgL_cellz00_735;

																					BgL_cellz00_735 =
																						BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																						(BgL_idz00_656,
																						BgL_expandersz00_46);
																					{	/* Read/inline.scm 162 */
																						obj_t BgL_expdz00_736;

																						if (PAIRP(BgL_cellz00_735))
																							{	/* Read/inline.scm 163 */
																								BGl_za2macrozd2definitionsza2zd2zzread_inlinez00
																									=
																									MAKE_YOUNG_PAIR(BgL_expz00_47,
																									BGl_za2macrozd2definitionsza2zd2zzread_inlinez00);
																								BgL_expdz00_736 =
																									bgl_remq_bang(BgL_cellz00_735,
																									BgL_expandersz00_46);
																							}
																						else
																							{	/* Read/inline.scm 163 */
																								BgL_expdz00_736 =
																									BgL_expandersz00_46;
																							}
																						{	/* Read/inline.scm 163 */

																							{	/* Read/inline.scm 169 */
																								int BgL_tmpz00_1414;

																								BgL_tmpz00_1414 = (int) (4L);
																								BGL_MVALUES_NUMBER_SET
																									(BgL_tmpz00_1414);
																							}
																							{	/* Read/inline.scm 169 */
																								int BgL_tmpz00_1417;

																								BgL_tmpz00_1417 = (int) (1L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_1417,
																									BgL_macrosz00_44);
																							}
																							{	/* Read/inline.scm 169 */
																								int BgL_tmpz00_1420;

																								BgL_tmpz00_1420 = (int) (2L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_1420,
																									BgL_syntaxesz00_45);
																							}
																							{	/* Read/inline.scm 169 */
																								int BgL_tmpz00_1423;

																								BgL_tmpz00_1423 = (int) (3L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_1423,
																									BgL_expdz00_736);
																							}
																							return BgL_inlinesz00_43;
																						}
																					}
																				}
																			}
																		else
																			{	/* Read/inline.scm 131 */
																				goto BgL_tagzd2365zd2_662;
																			}
																	}
																else
																	{	/* Read/inline.scm 131 */
																		goto BgL_tagzd2365zd2_662;
																	}
															}
														else
															{	/* Read/inline.scm 131 */
																if (
																	(CAR(
																			((obj_t) BgL_expz00_47)) ==
																		CNST_TABLE_REF(1)))
																	{	/* Read/inline.scm 131 */
																		obj_t BgL_arg1361z00_695;

																		BgL_arg1361z00_695 =
																			CDR(((obj_t) BgL_expz00_47));
																		BgL_expza2za2_658 = BgL_arg1361z00_695;
																		{
																			obj_t BgL_inlinesz00_743;
																			obj_t BgL_macrosz00_744;
																			obj_t BgL_syntaxesz00_745;
																			obj_t BgL_expandersz00_746;
																			obj_t BgL_expza2za2_747;

																			BgL_inlinesz00_743 = BgL_inlinesz00_43;
																			BgL_macrosz00_744 = BgL_macrosz00_44;
																			BgL_syntaxesz00_745 = BgL_syntaxesz00_45;
																			BgL_expandersz00_746 =
																				BgL_expandersz00_46;
																			BgL_expza2za2_747 = BgL_expza2za2_658;
																		BgL_zc3z04anonymousza31452ze3z87_748:
																			if (NULLP(BgL_expza2za2_747))
																				{	/* Read/inline.scm 176 */
																					{	/* Read/inline.scm 177 */
																						int BgL_tmpz00_1435;

																						BgL_tmpz00_1435 = (int) (4L);
																						BGL_MVALUES_NUMBER_SET
																							(BgL_tmpz00_1435);
																					}
																					{	/* Read/inline.scm 177 */
																						int BgL_tmpz00_1438;

																						BgL_tmpz00_1438 = (int) (1L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_1438,
																							BgL_macrosz00_744);
																					}
																					{	/* Read/inline.scm 177 */
																						int BgL_tmpz00_1441;

																						BgL_tmpz00_1441 = (int) (2L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_1441,
																							BgL_syntaxesz00_745);
																					}
																					{	/* Read/inline.scm 177 */
																						int BgL_tmpz00_1444;

																						BgL_tmpz00_1444 = (int) (3L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_1444,
																							BgL_expandersz00_746);
																					}
																					return BgL_inlinesz00_743;
																				}
																			else
																				{	/* Read/inline.scm 178 */
																					obj_t BgL_inlinesz00_754;

																					{	/* Read/inline.scm 179 */
																						obj_t BgL_arg1473z00_759;

																						BgL_arg1473z00_759 =
																							CAR(((obj_t) BgL_expza2za2_747));
																						BgL_inlinesz00_754 =
																							BGl_lookzd2forzf2expz20zzread_inlinez00
																							(BgL_inlinesz00_743,
																							BgL_macrosz00_744,
																							BgL_syntaxesz00_745,
																							BgL_expandersz00_746,
																							BgL_arg1473z00_759,
																							BgL_modulez00_48);
																					}
																					{	/* Read/inline.scm 179 */
																						obj_t BgL_macrosz00_755;
																						obj_t BgL_syntaxesz00_756;
																						obj_t BgL_expandersz00_757;

																						{	/* Read/inline.scm 180 */
																							obj_t BgL_tmpz00_930;

																							{	/* Read/inline.scm 180 */
																								int BgL_tmpz00_1450;

																								BgL_tmpz00_1450 = (int) (1L);
																								BgL_tmpz00_930 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_1450);
																							}
																							{	/* Read/inline.scm 180 */
																								int BgL_tmpz00_1453;

																								BgL_tmpz00_1453 = (int) (1L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_1453, BUNSPEC);
																							}
																							BgL_macrosz00_755 =
																								BgL_tmpz00_930;
																						}
																						{	/* Read/inline.scm 180 */
																							obj_t BgL_tmpz00_931;

																							{	/* Read/inline.scm 180 */
																								int BgL_tmpz00_1456;

																								BgL_tmpz00_1456 = (int) (2L);
																								BgL_tmpz00_931 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_1456);
																							}
																							{	/* Read/inline.scm 180 */
																								int BgL_tmpz00_1459;

																								BgL_tmpz00_1459 = (int) (2L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_1459, BUNSPEC);
																							}
																							BgL_syntaxesz00_756 =
																								BgL_tmpz00_931;
																						}
																						{	/* Read/inline.scm 180 */
																							obj_t BgL_tmpz00_932;

																							{	/* Read/inline.scm 180 */
																								int BgL_tmpz00_1462;

																								BgL_tmpz00_1462 = (int) (3L);
																								BgL_tmpz00_932 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_1462);
																							}
																							{	/* Read/inline.scm 180 */
																								int BgL_tmpz00_1465;

																								BgL_tmpz00_1465 = (int) (3L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_1465, BUNSPEC);
																							}
																							BgL_expandersz00_757 =
																								BgL_tmpz00_932;
																						}
																						{	/* Read/inline.scm 180 */
																							obj_t BgL_arg1472z00_758;

																							BgL_arg1472z00_758 =
																								CDR(
																								((obj_t) BgL_expza2za2_747));
																							{
																								obj_t BgL_expza2za2_1474;
																								obj_t BgL_expandersz00_1473;
																								obj_t BgL_syntaxesz00_1472;
																								obj_t BgL_macrosz00_1471;
																								obj_t BgL_inlinesz00_1470;

																								BgL_inlinesz00_1470 =
																									BgL_inlinesz00_754;
																								BgL_macrosz00_1471 =
																									BgL_macrosz00_755;
																								BgL_syntaxesz00_1472 =
																									BgL_syntaxesz00_756;
																								BgL_expandersz00_1473 =
																									BgL_expandersz00_757;
																								BgL_expza2za2_1474 =
																									BgL_arg1472z00_758;
																								BgL_expza2za2_747 =
																									BgL_expza2za2_1474;
																								BgL_expandersz00_746 =
																									BgL_expandersz00_1473;
																								BgL_syntaxesz00_745 =
																									BgL_syntaxesz00_1472;
																								BgL_macrosz00_744 =
																									BgL_macrosz00_1471;
																								BgL_inlinesz00_743 =
																									BgL_inlinesz00_1470;
																								goto
																									BgL_zc3z04anonymousza31452ze3z87_748;
																							}
																						}
																					}
																				}
																		}
																	}
																else
																	{	/* Read/inline.scm 131 */
																		if (
																			(CAR(
																					((obj_t) BgL_expz00_47)) ==
																				CNST_TABLE_REF(11)))
																			{	/* Read/inline.scm 131 */
																				obj_t BgL_arg1367z00_698;

																				BgL_arg1367z00_698 =
																					CDR(((obj_t) BgL_expz00_47));
																				BgL_expza2za2_660 = BgL_arg1367z00_698;
																				{	/* Read/inline.scm 186 */
																					obj_t BgL_g1073z00_761;

																					{
																						obj_t BgL_expza2za2_782;

																						BgL_expza2za2_782 =
																							BgL_expza2za2_660;
																					BgL_zc3z04anonymousza31490ze3z87_783:
																						if (NULLP(BgL_expza2za2_782))
																							{	/* Read/inline.scm 191 */
																								BgL_g1073z00_761 = BNIL;
																							}
																						else
																							{
																								obj_t BgL_clausez00_785;
																								obj_t BgL_rz00_786;

																								{	/* Read/inline.scm 193 */
																									obj_t BgL_ezd2528zd2_791;

																									BgL_ezd2528zd2_791 =
																										CAR(
																										((obj_t)
																											BgL_expza2za2_782));
																									if (PAIRP(BgL_ezd2528zd2_791))
																										{	/* Read/inline.scm 193 */
																											obj_t
																												BgL_carzd2534zd2_793;
																											BgL_carzd2534zd2_793 =
																												CAR(BgL_ezd2528zd2_791);
																											if (SYMBOLP
																												(BgL_carzd2534zd2_793))
																												{	/* Read/inline.scm 193 */
																													BgL_clausez00_785 =
																														BgL_carzd2534zd2_793;
																													BgL_rz00_786 =
																														CDR
																														(BgL_ezd2528zd2_791);
																													{	/* Read/inline.scm 195 */
																														bool_t
																															BgL_test1681z00_1491;
																														if (BGl_compilezd2srfizf3z21zz__expander_srfi0z00(BgL_clausez00_785))
																															{	/* Read/inline.scm 195 */
																																BgL_test1681z00_1491
																																	=
																																	((bool_t) 1);
																															}
																														else
																															{	/* Read/inline.scm 195 */
																																BgL_test1681z00_1491
																																	=
																																	(BgL_clausez00_785
																																	==
																																	CNST_TABLE_REF
																																	(5));
																															}
																														if (BgL_test1681z00_1491)
																															{	/* Read/inline.scm 195 */
																																BgL_g1073z00_761
																																	=
																																	BgL_rz00_786;
																															}
																														else
																															{	/* Read/inline.scm 197 */
																																obj_t
																																	BgL_arg1535z00_802;
																																BgL_arg1535z00_802
																																	=
																																	CDR(((obj_t)
																																		BgL_expza2za2_782));
																																{
																																	obj_t
																																		BgL_expza2za2_1498;
																																	BgL_expza2za2_1498
																																		=
																																		BgL_arg1535z00_802;
																																	BgL_expza2za2_782
																																		=
																																		BgL_expza2za2_1498;
																																	goto
																																		BgL_zc3z04anonymousza31490ze3z87_783;
																																}
																															}
																													}
																												}
																											else
																												{	/* Read/inline.scm 193 */
																													if (
																														(BgL_carzd2534zd2_793
																															==
																															CNST_TABLE_REF
																															(6)))
																														{	/* Read/inline.scm 193 */
																															BgL_g1073z00_761 =
																																CDR
																																(BgL_ezd2528zd2_791);
																														}
																													else
																														{	/* Read/inline.scm 193 */
																														BgL_tagzd2527zd2_790:
																															{	/* Read/inline.scm 201 */
																																obj_t
																																	BgL_arg1540z00_804;
																																BgL_arg1540z00_804
																																	=
																																	CDR(((obj_t)
																																		BgL_expza2za2_782));
																																{
																																	obj_t
																																		BgL_expza2za2_1506;
																																	BgL_expza2za2_1506
																																		=
																																		BgL_arg1540z00_804;
																																	BgL_expza2za2_782
																																		=
																																		BgL_expza2za2_1506;
																																	goto
																																		BgL_zc3z04anonymousza31490ze3z87_783;
																																}
																															}
																														}
																												}
																										}
																									else
																										{	/* Read/inline.scm 193 */
																											goto BgL_tagzd2527zd2_790;
																										}
																								}
																							}
																					}
																					{
																						obj_t BgL_inlinesz00_763;
																						obj_t BgL_macrosz00_764;
																						obj_t BgL_syntaxesz00_765;
																						obj_t BgL_expandersz00_766;
																						obj_t BgL_expza2za2_767;

																						BgL_inlinesz00_763 =
																							BgL_inlinesz00_43;
																						BgL_macrosz00_764 =
																							BgL_macrosz00_44;
																						BgL_syntaxesz00_765 =
																							BgL_syntaxesz00_45;
																						BgL_expandersz00_766 =
																							BgL_expandersz00_46;
																						BgL_expza2za2_767 =
																							BgL_g1073z00_761;
																					BgL_zc3z04anonymousza31474ze3z87_768:
																						if (NULLP(BgL_expza2za2_767))
																							{	/* Read/inline.scm 202 */
																								{	/* Read/inline.scm 203 */
																									int BgL_tmpz00_1509;

																									BgL_tmpz00_1509 = (int) (4L);
																									BGL_MVALUES_NUMBER_SET
																										(BgL_tmpz00_1509);
																								}
																								{	/* Read/inline.scm 203 */
																									int BgL_tmpz00_1512;

																									BgL_tmpz00_1512 = (int) (1L);
																									BGL_MVALUES_VAL_SET
																										(BgL_tmpz00_1512,
																										BgL_macrosz00_764);
																								}
																								{	/* Read/inline.scm 203 */
																									int BgL_tmpz00_1515;

																									BgL_tmpz00_1515 = (int) (2L);
																									BGL_MVALUES_VAL_SET
																										(BgL_tmpz00_1515,
																										BgL_syntaxesz00_765);
																								}
																								{	/* Read/inline.scm 203 */
																									int BgL_tmpz00_1518;

																									BgL_tmpz00_1518 = (int) (3L);
																									BGL_MVALUES_VAL_SET
																										(BgL_tmpz00_1518,
																										BgL_expandersz00_766);
																								}
																								return BgL_inlinesz00_763;
																							}
																						else
																							{	/* Read/inline.scm 204 */
																								obj_t BgL_inlinesz00_774;

																								{	/* Read/inline.scm 205 */
																									obj_t BgL_arg1489z00_779;

																									BgL_arg1489z00_779 =
																										CAR(
																										((obj_t)
																											BgL_expza2za2_767));
																									BgL_inlinesz00_774 =
																										BGl_lookzd2forzf2expz20zzread_inlinez00
																										(BgL_inlinesz00_763,
																										BgL_macrosz00_764,
																										BgL_syntaxesz00_765,
																										BgL_expandersz00_766,
																										BgL_arg1489z00_779,
																										BgL_modulez00_48);
																								}
																								{	/* Read/inline.scm 205 */
																									obj_t BgL_macrosz00_775;
																									obj_t BgL_syntaxesz00_776;
																									obj_t BgL_expandersz00_777;

																									{	/* Read/inline.scm 206 */
																										obj_t BgL_tmpz00_942;

																										{	/* Read/inline.scm 206 */
																											int BgL_tmpz00_1524;

																											BgL_tmpz00_1524 =
																												(int) (1L);
																											BgL_tmpz00_942 =
																												BGL_MVALUES_VAL
																												(BgL_tmpz00_1524);
																										}
																										{	/* Read/inline.scm 206 */
																											int BgL_tmpz00_1527;

																											BgL_tmpz00_1527 =
																												(int) (1L);
																											BGL_MVALUES_VAL_SET
																												(BgL_tmpz00_1527,
																												BUNSPEC);
																										}
																										BgL_macrosz00_775 =
																											BgL_tmpz00_942;
																									}
																									{	/* Read/inline.scm 206 */
																										obj_t BgL_tmpz00_943;

																										{	/* Read/inline.scm 206 */
																											int BgL_tmpz00_1530;

																											BgL_tmpz00_1530 =
																												(int) (2L);
																											BgL_tmpz00_943 =
																												BGL_MVALUES_VAL
																												(BgL_tmpz00_1530);
																										}
																										{	/* Read/inline.scm 206 */
																											int BgL_tmpz00_1533;

																											BgL_tmpz00_1533 =
																												(int) (2L);
																											BGL_MVALUES_VAL_SET
																												(BgL_tmpz00_1533,
																												BUNSPEC);
																										}
																										BgL_syntaxesz00_776 =
																											BgL_tmpz00_943;
																									}
																									{	/* Read/inline.scm 206 */
																										obj_t BgL_tmpz00_944;

																										{	/* Read/inline.scm 206 */
																											int BgL_tmpz00_1536;

																											BgL_tmpz00_1536 =
																												(int) (3L);
																											BgL_tmpz00_944 =
																												BGL_MVALUES_VAL
																												(BgL_tmpz00_1536);
																										}
																										{	/* Read/inline.scm 206 */
																											int BgL_tmpz00_1539;

																											BgL_tmpz00_1539 =
																												(int) (3L);
																											BGL_MVALUES_VAL_SET
																												(BgL_tmpz00_1539,
																												BUNSPEC);
																										}
																										BgL_expandersz00_777 =
																											BgL_tmpz00_944;
																									}
																									{	/* Read/inline.scm 206 */
																										obj_t BgL_arg1485z00_778;

																										BgL_arg1485z00_778 =
																											CDR(
																											((obj_t)
																												BgL_expza2za2_767));
																										{
																											obj_t BgL_expza2za2_1548;
																											obj_t
																												BgL_expandersz00_1547;
																											obj_t
																												BgL_syntaxesz00_1546;
																											obj_t BgL_macrosz00_1545;
																											obj_t BgL_inlinesz00_1544;

																											BgL_inlinesz00_1544 =
																												BgL_inlinesz00_774;
																											BgL_macrosz00_1545 =
																												BgL_macrosz00_775;
																											BgL_syntaxesz00_1546 =
																												BgL_syntaxesz00_776;
																											BgL_expandersz00_1547 =
																												BgL_expandersz00_777;
																											BgL_expza2za2_1548 =
																												BgL_arg1485z00_778;
																											BgL_expza2za2_767 =
																												BgL_expza2za2_1548;
																											BgL_expandersz00_766 =
																												BgL_expandersz00_1547;
																											BgL_syntaxesz00_765 =
																												BgL_syntaxesz00_1546;
																											BgL_macrosz00_764 =
																												BgL_macrosz00_1545;
																											BgL_inlinesz00_763 =
																												BgL_inlinesz00_1544;
																											goto
																												BgL_zc3z04anonymousza31474ze3z87_768;
																										}
																									}
																								}
																							}
																					}
																				}
																			}
																		else
																			{	/* Read/inline.scm 131 */
																				goto BgL_tagzd2365zd2_662;
																			}
																	}
															}
													}
											}
									}
							}
						else
							{	/* Read/inline.scm 131 */
								goto BgL_tagzd2365zd2_662;
							}
					}
			}
		}

	}



/* inline-definition-queue */
	BGL_EXPORTED_DEF obj_t
		BGl_inlinezd2definitionzd2queuez00zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 218 */
			return BGl_za2inlinezd2definitionsza2zd2zzread_inlinez00;
		}

	}



/* &inline-definition-queue */
	obj_t BGl_z62inlinezd2definitionzd2queuez62zzread_inlinez00(obj_t
		BgL_envz00_1000)
	{
		{	/* Read/inline.scm 218 */
			return BGl_inlinezd2definitionzd2queuez00zzread_inlinez00();
		}

	}



/* inline-finalizer */
	BGL_EXPORTED_DEF obj_t BGl_inlinezd2finaliza7erz75zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 224 */
			if (NULLP(BGl_za2inlinezd2definitionsza2zd2zzread_inlinez00))
				{	/* Read/inline.scm 225 */
					return BNIL;
				}
			else
				{	/* Read/inline.scm 227 */
					obj_t BgL_arg1544z00_814;

					{	/* Read/inline.scm 227 */
						obj_t BgL_idz00_966;
						obj_t BgL_sexpza2za2_967;

						BgL_idz00_966 = CNST_TABLE_REF(12);
						BgL_sexpza2za2_967 =
							BGl_za2inlinezd2definitionsza2zd2zzread_inlinez00;
						{	/* Read/inline.scm 227 */
							obj_t BgL_newz00_968;

							BgL_newz00_968 = create_struct(CNST_TABLE_REF(13), (int) (5L));
							{	/* Read/inline.scm 227 */
								int BgL_tmpz00_1556;

								BgL_tmpz00_1556 = (int) (4L);
								STRUCT_SET(BgL_newz00_968, BgL_tmpz00_1556, BFALSE);
							}
							{	/* Read/inline.scm 227 */
								int BgL_tmpz00_1559;

								BgL_tmpz00_1559 = (int) (3L);
								STRUCT_SET(BgL_newz00_968, BgL_tmpz00_1559, BTRUE);
							}
							{	/* Read/inline.scm 227 */
								int BgL_tmpz00_1562;

								BgL_tmpz00_1562 = (int) (2L);
								STRUCT_SET(BgL_newz00_968, BgL_tmpz00_1562, BgL_sexpza2za2_967);
							}
							{	/* Read/inline.scm 227 */
								obj_t BgL_auxz00_1567;
								int BgL_tmpz00_1565;

								BgL_auxz00_1567 = BINT(0L);
								BgL_tmpz00_1565 = (int) (1L);
								STRUCT_SET(BgL_newz00_968, BgL_tmpz00_1565, BgL_auxz00_1567);
							}
							{	/* Read/inline.scm 227 */
								int BgL_tmpz00_1570;

								BgL_tmpz00_1570 = (int) (0L);
								STRUCT_SET(BgL_newz00_968, BgL_tmpz00_1570, BgL_idz00_966);
							}
							BgL_arg1544z00_814 = BgL_newz00_968;
					}}
					{	/* Read/inline.scm 227 */
						obj_t BgL_list1545z00_815;

						BgL_list1545z00_815 = MAKE_YOUNG_PAIR(BgL_arg1544z00_814, BNIL);
						return BgL_list1545z00_815;
					}
				}
		}

	}



/* &inline-finalizer */
	obj_t BGl_z62inlinezd2finaliza7erz17zzread_inlinez00(obj_t BgL_envz00_1001)
	{
		{	/* Read/inline.scm 224 */
			return BGl_inlinezd2finaliza7erz75zzread_inlinez00();
		}

	}



/* import-macro-finalizer */
	BGL_EXPORTED_DEF obj_t
		BGl_importzd2macrozd2finaliza7erza7zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 237 */
			if (NULLP(BGl_za2macrozd2definitionsza2zd2zzread_inlinez00))
				{	/* Read/inline.scm 238 */
					return BNIL;
				}
			else
				{	/* Read/inline.scm 240 */
					obj_t BgL_arg1552z00_817;

					{	/* Read/inline.scm 240 */
						obj_t BgL_idz00_976;
						obj_t BgL_sexpza2za2_977;

						BgL_idz00_976 = CNST_TABLE_REF(14);
						BgL_sexpza2za2_977 =
							BGl_za2macrozd2definitionsza2zd2zzread_inlinez00;
						{	/* Read/inline.scm 240 */
							obj_t BgL_newz00_978;

							BgL_newz00_978 = create_struct(CNST_TABLE_REF(13), (int) (5L));
							{	/* Read/inline.scm 240 */
								int BgL_tmpz00_1581;

								BgL_tmpz00_1581 = (int) (4L);
								STRUCT_SET(BgL_newz00_978, BgL_tmpz00_1581, BFALSE);
							}
							{	/* Read/inline.scm 240 */
								int BgL_tmpz00_1584;

								BgL_tmpz00_1584 = (int) (3L);
								STRUCT_SET(BgL_newz00_978, BgL_tmpz00_1584, BTRUE);
							}
							{	/* Read/inline.scm 240 */
								int BgL_tmpz00_1587;

								BgL_tmpz00_1587 = (int) (2L);
								STRUCT_SET(BgL_newz00_978, BgL_tmpz00_1587, BgL_sexpza2za2_977);
							}
							{	/* Read/inline.scm 240 */
								obj_t BgL_auxz00_1592;
								int BgL_tmpz00_1590;

								BgL_auxz00_1592 = BINT(0L);
								BgL_tmpz00_1590 = (int) (1L);
								STRUCT_SET(BgL_newz00_978, BgL_tmpz00_1590, BgL_auxz00_1592);
							}
							{	/* Read/inline.scm 240 */
								int BgL_tmpz00_1595;

								BgL_tmpz00_1595 = (int) (0L);
								STRUCT_SET(BgL_newz00_978, BgL_tmpz00_1595, BgL_idz00_976);
							}
							BgL_arg1552z00_817 = BgL_newz00_978;
					}}
					{	/* Read/inline.scm 240 */
						obj_t BgL_list1553z00_818;

						BgL_list1553z00_818 = MAKE_YOUNG_PAIR(BgL_arg1552z00_817, BNIL);
						return BgL_list1553z00_818;
					}
				}
		}

	}



/* &import-macro-finalizer */
	obj_t BGl_z62importzd2macrozd2finaliza7erzc5zzread_inlinez00(obj_t
		BgL_envz00_1002)
	{
		{	/* Read/inline.scm 237 */
			return BGl_importzd2macrozd2finaliza7erza7zzread_inlinez00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzread_inlinez00(void)
	{
		{	/* Read/inline.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			BGl_modulezd2initializa7ationz75zzread_readerz00(95801752L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1605z00zzread_inlinez00));
		}

	}

#ifdef __cplusplus
}
#endif
