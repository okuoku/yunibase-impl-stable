/*===========================================================================*/
/*   (Read/src.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Read/src.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_READ_SRC_TYPE_DEFINITIONS
#define BGL_READ_SRC_TYPE_DEFINITIONS

/* object type definitions */

#endif													// BGL_READ_SRC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62zc3z04anonymousza31030ze3ze5zzread_srcz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzread_srcz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_raisez00zz__errorz00(obj_t);
	static obj_t BGl_readzd2modulezd2clausez00zzread_srcz00(void);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzread_srcz00(void);
	extern obj_t BGl_compilerzd2readzd2srcz00zzread_readerz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzread_srcz00(void);
	BGL_IMPORT obj_t BGl_readerzd2resetz12zc0zz__readerz00(void);
	static obj_t BGl_z62zc3z04anonymousza31036ze3ze5zzread_srcz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzread_srcz00(void);
	extern obj_t BGl_hellozd2worldzd2zzengine_enginez00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t BGl_za2bigloozd2interpreterza2zd2zz__readerz00;
	static obj_t BGl_z62readzd2handlerzb0zzread_srcz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzread_srcz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzread_srcz00(void);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_za2portza2z00zzread_srcz00 = BUNSPEC;
	static obj_t BGl_z62readzd2srczb0zzread_srcz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_openzd2srczd2filez00zzread_srcz00(obj_t);
	static obj_t BGl_readzd2srczf2portz20zzread_srcz00(void);
	BGL_IMPORT obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzread_srcz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_enginez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_z62errorz62zz__objectz00;
	static obj_t BGl_cnstzd2initzd2zzread_srcz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzread_srcz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzread_srcz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzread_srcz00(void);
	BGL_IMPORT obj_t BGl_za2loadzd2pathza2zd2zz__evalz00;
	BGL_EXPORTED_DECL obj_t BGl_readzd2srczd2zzread_srcz00(void);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	static obj_t BGl_closezd2srczd2portz00zzread_srcz00(void);
	BGL_IMPORT obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t, obj_t);
	extern obj_t BGl_compilerzd2exitzd2zzinit_mainz00(obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_readzd2handlerzd2envz00zzread_srcz00,
		BgL_bgl_za762readza7d2handle1060z00, BGl_z62readzd2handlerzb0zzread_srcz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_readzd2srczd2envz00zzread_srcz00,
		BgL_bgl_za762readza7d2srcza7b01061za7, BGl_z62readzd2srczb0zzread_srcz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1055z00zzread_srcz00,
		BgL_bgl_string1055za700za7za7r1062za7, "Can't open such file", 20);
	      DEFINE_STRING(BGl_string1056z00zzread_srcz00,
		BgL_bgl_string1056za700za7za7r1063za7, "Can't find such file", 20);
	      DEFINE_STRING(BGl_string1057z00zzread_srcz00,
		BgL_bgl_string1057za700za7za7r1064za7, "read_src", 8);
	      DEFINE_STRING(BGl_string1058z00zzread_srcz00,
		BgL_bgl_string1058za700za7za7r1065za7, "src-file->memory ", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1054z00zzread_srcz00,
		BgL_bgl_za762za7c3za704anonymo1066za7,
		BGl_z62zc3z04anonymousza31030ze3ze5zzread_srcz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzread_srcz00));
		     ADD_ROOT((void *) (&BGl_za2portza2z00zzread_srcz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzread_srcz00(long
		BgL_checksumz00_162, char *BgL_fromz00_163)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzread_srcz00))
				{
					BGl_requirezd2initializa7ationz75zzread_srcz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzread_srcz00();
					BGl_libraryzd2moduleszd2initz00zzread_srcz00();
					BGl_cnstzd2initzd2zzread_srcz00();
					BGl_importedzd2moduleszd2initz00zzread_srcz00();
					return BGl_toplevelzd2initzd2zzread_srcz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzread_srcz00(void)
	{
		{	/* Read/src.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"read_src");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "read_src");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"read_src");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzread_srcz00(void)
	{
		{	/* Read/src.scm 15 */
			{	/* Read/src.scm 15 */
				obj_t BgL_cportz00_124;

				{	/* Read/src.scm 15 */
					obj_t BgL_stringz00_131;

					BgL_stringz00_131 = BGl_string1058z00zzread_srcz00;
					{	/* Read/src.scm 15 */
						obj_t BgL_startz00_132;

						BgL_startz00_132 = BINT(0L);
						{	/* Read/src.scm 15 */
							obj_t BgL_endz00_133;

							BgL_endz00_133 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_131)));
							{	/* Read/src.scm 15 */

								BgL_cportz00_124 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_131, BgL_startz00_132, BgL_endz00_133);
				}}}}
				{
					long BgL_iz00_125;

					BgL_iz00_125 = 0L;
				BgL_loopz00_126:
					if ((BgL_iz00_125 == -1L))
						{	/* Read/src.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Read/src.scm 15 */
							{	/* Read/src.scm 15 */
								obj_t BgL_arg1059z00_127;

								{	/* Read/src.scm 15 */

									{	/* Read/src.scm 15 */
										obj_t BgL_locationz00_129;

										BgL_locationz00_129 = BBOOL(((bool_t) 0));
										{	/* Read/src.scm 15 */

											BgL_arg1059z00_127 =
												BGl_readz00zz__readerz00(BgL_cportz00_124,
												BgL_locationz00_129);
										}
									}
								}
								{	/* Read/src.scm 15 */
									int BgL_tmpz00_193;

									BgL_tmpz00_193 = (int) (BgL_iz00_125);
									CNST_TABLE_SET(BgL_tmpz00_193, BgL_arg1059z00_127);
							}}
							{	/* Read/src.scm 15 */
								int BgL_auxz00_130;

								BgL_auxz00_130 = (int) ((BgL_iz00_125 - 1L));
								{
									long BgL_iz00_198;

									BgL_iz00_198 = (long) (BgL_auxz00_130);
									BgL_iz00_125 = BgL_iz00_198;
									goto BgL_loopz00_126;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzread_srcz00(void)
	{
		{	/* Read/src.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzread_srcz00(void)
	{
		{	/* Read/src.scm 15 */
			return (BGl_za2portza2z00zzread_srcz00 = BFALSE, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzread_srcz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_6;

				BgL_headz00_6 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_7;
					obj_t BgL_tailz00_8;

					BgL_prevz00_7 = BgL_headz00_6;
					BgL_tailz00_8 = BgL_l1z00_1;
				BgL_loopz00_9:
					if (PAIRP(BgL_tailz00_8))
						{
							obj_t BgL_newzd2prevzd2_11;

							BgL_newzd2prevzd2_11 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_8), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_7, BgL_newzd2prevzd2_11);
							{
								obj_t BgL_tailz00_208;
								obj_t BgL_prevz00_207;

								BgL_prevz00_207 = BgL_newzd2prevzd2_11;
								BgL_tailz00_208 = CDR(BgL_tailz00_8);
								BgL_tailz00_8 = BgL_tailz00_208;
								BgL_prevz00_7 = BgL_prevz00_207;
								goto BgL_loopz00_9;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_6);
				}
			}
		}

	}



/* read-src */
	BGL_EXPORTED_DEF obj_t BGl_readzd2srczd2zzread_srcz00(void)
	{
		{	/* Read/src.scm 31 */
			{	/* Read/src.scm 35 */
				obj_t BgL_modz00_14;

				BgL_modz00_14 = BGl_readzd2modulezd2clausez00zzread_srcz00();
				if (PAIRP(BgL_modz00_14))
					{	/* Read/src.scm 38 */
						obj_t BgL_g1012z00_16;
						obj_t BgL_g1013z00_17;

						BgL_g1012z00_16 = BGl_readzd2srczf2portz20zzread_srcz00();
						BgL_g1013z00_17 = CDR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
						{
							obj_t BgL_srcz00_19;
							obj_t BgL_sfilesz00_20;

							BgL_srcz00_19 = BgL_g1012z00_16;
							BgL_sfilesz00_20 = BgL_g1013z00_17;
						BgL_zc3z04anonymousza31020ze3z87_21:
							if (NULLP(BgL_sfilesz00_20))
								{	/* Read/src.scm 40 */
									return
										MAKE_YOUNG_PAIR(BgL_modz00_14,
										bgl_reverse_bang(BgL_srcz00_19));
								}
							else
								{	/* Read/src.scm 42 */
									obj_t BgL_arg1023z00_24;
									obj_t BgL_arg1024z00_25;

									{	/* Read/src.scm 42 */
										obj_t BgL_arg1025z00_26;

										{	/* Read/src.scm 42 */
											obj_t BgL_arg1026z00_27;

											BgL_arg1026z00_27 = CAR(((obj_t) BgL_sfilesz00_20));
											BGl_openzd2srczd2filez00zzread_srcz00(BgL_arg1026z00_27);
											BgL_arg1025z00_26 =
												BGl_readzd2srczf2portz20zzread_srcz00();
										}
										BgL_arg1023z00_24 =
											BGl_appendzd221011zd2zzread_srcz00(BgL_arg1025z00_26,
											BgL_srcz00_19);
									}
									BgL_arg1024z00_25 = CDR(((obj_t) BgL_sfilesz00_20));
									{
										obj_t BgL_sfilesz00_228;
										obj_t BgL_srcz00_227;

										BgL_srcz00_227 = BgL_arg1023z00_24;
										BgL_sfilesz00_228 = BgL_arg1024z00_25;
										BgL_sfilesz00_20 = BgL_sfilesz00_228;
										BgL_srcz00_19 = BgL_srcz00_227;
										goto BgL_zc3z04anonymousza31020ze3z87_21;
									}
								}
						}
					}
				else
					{	/* Read/src.scm 36 */
						return BgL_modz00_14;
					}
			}
		}

	}



/* &read-src */
	obj_t BGl_z62readzd2srczb0zzread_srcz00(obj_t BgL_envz00_116)
	{
		{	/* Read/src.scm 31 */
			return BGl_readzd2srczd2zzread_srcz00();
		}

	}



/* &read-handler */
	obj_t BGl_z62readzd2handlerzb0zzread_srcz00(obj_t BgL_envz00_117,
		obj_t BgL_ez00_118)
	{
		{	/* Read/src.scm 48 */
			{	/* Read/src.scm 49 */
				bool_t BgL_test1072z00_230;

				{	/* Read/src.scm 49 */
					obj_t BgL_classz00_135;

					BgL_classz00_135 = BGl_z62errorz62zz__objectz00;
					if (BGL_OBJECTP(BgL_ez00_118))
						{	/* Read/src.scm 49 */
							BgL_objectz00_bglt BgL_arg1807z00_136;

							BgL_arg1807z00_136 = (BgL_objectz00_bglt) (BgL_ez00_118);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Read/src.scm 49 */
									long BgL_idxz00_137;

									BgL_idxz00_137 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_136);
									BgL_test1072z00_230 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_137 + 3L)) == BgL_classz00_135);
								}
							else
								{	/* Read/src.scm 49 */
									bool_t BgL_res1053z00_140;

									{	/* Read/src.scm 49 */
										obj_t BgL_oclassz00_141;

										{	/* Read/src.scm 49 */
											obj_t BgL_arg1815z00_142;
											long BgL_arg1816z00_143;

											BgL_arg1815z00_142 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Read/src.scm 49 */
												long BgL_arg1817z00_144;

												BgL_arg1817z00_144 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_136);
												BgL_arg1816z00_143 = (BgL_arg1817z00_144 - OBJECT_TYPE);
											}
											BgL_oclassz00_141 =
												VECTOR_REF(BgL_arg1815z00_142, BgL_arg1816z00_143);
										}
										{	/* Read/src.scm 49 */
											bool_t BgL__ortest_1115z00_145;

											BgL__ortest_1115z00_145 =
												(BgL_classz00_135 == BgL_oclassz00_141);
											if (BgL__ortest_1115z00_145)
												{	/* Read/src.scm 49 */
													BgL_res1053z00_140 = BgL__ortest_1115z00_145;
												}
											else
												{	/* Read/src.scm 49 */
													long BgL_odepthz00_146;

													{	/* Read/src.scm 49 */
														obj_t BgL_arg1804z00_147;

														BgL_arg1804z00_147 = (BgL_oclassz00_141);
														BgL_odepthz00_146 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_147);
													}
													if ((3L < BgL_odepthz00_146))
														{	/* Read/src.scm 49 */
															obj_t BgL_arg1802z00_148;

															{	/* Read/src.scm 49 */
																obj_t BgL_arg1803z00_149;

																BgL_arg1803z00_149 = (BgL_oclassz00_141);
																BgL_arg1802z00_148 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_149,
																	3L);
															}
															BgL_res1053z00_140 =
																(BgL_arg1802z00_148 == BgL_classz00_135);
														}
													else
														{	/* Read/src.scm 49 */
															BgL_res1053z00_140 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1072z00_230 = BgL_res1053z00_140;
								}
						}
					else
						{	/* Read/src.scm 49 */
							BgL_test1072z00_230 = ((bool_t) 0);
						}
				}
				if (BgL_test1072z00_230)
					{	/* Read/src.scm 49 */
						BGl_hellozd2worldzd2zzengine_enginez00();
						BGl_errorzd2notifyzd2zz__errorz00(BgL_ez00_118);
						BGl_closezd2srczd2portz00zzread_srcz00();
						return BGl_compilerzd2exitzd2zzinit_mainz00(BINT(3L));
					}
				else
					{	/* Read/src.scm 49 */
						return BGl_raisez00zz__errorz00(BgL_ez00_118);
					}
			}
		}

	}



/* read-module-clause */
	obj_t BGl_readzd2modulezd2clausez00zzread_srcz00(void)
	{
		{	/* Read/src.scm 63 */
			BGl_openzd2srczd2filez00zzread_srcz00(CAR
				(BGl_za2srczd2filesza2zd2zzengine_paramz00));
			{	/* Read/src.scm 65 */
				obj_t BgL_modz00_31;

				BgL_modz00_31 =
					BGl_withzd2exceptionzd2handlerz00zz__errorz00
					(BGl_readzd2handlerzd2envz00zzread_srcz00,
					BGl_proc1054z00zzread_srcz00);
				if (CBOOL(BGl_za2bigloozd2interpreterza2zd2zz__readerz00))
					{	/* Read/src.scm 69 */
						return BFALSE;
					}
				else
					{	/* Read/src.scm 69 */
						return BgL_modz00_31;
					}
			}
		}

	}



/* &<@anonymous:1030> */
	obj_t BGl_z62zc3z04anonymousza31030ze3ze5zzread_srcz00(obj_t BgL_envz00_120)
	{
		{	/* Read/src.scm 67 */
			{	/* Read/src.scm 68 */
				obj_t BgL_list1031z00_150;

				{	/* Read/src.scm 68 */
					obj_t BgL_arg1033z00_151;

					BgL_arg1033z00_151 = MAKE_YOUNG_PAIR(BTRUE, BNIL);
					BgL_list1031z00_150 =
						MAKE_YOUNG_PAIR(BGl_za2portza2z00zzread_srcz00, BgL_arg1033z00_151);
				}
				return
					BGl_compilerzd2readzd2srcz00zzread_readerz00(BgL_list1031z00_150);
			}
		}

	}



/* read-src/port */
	obj_t BGl_readzd2srczf2portz20zzread_srcz00(void)
	{
		{	/* Read/src.scm 78 */
			{	/* Read/src.scm 79 */
				obj_t BgL_portz00_37;

				BgL_portz00_37 = BGl_za2portza2z00zzread_srcz00;
				{	/* Read/src.scm 83 */
					obj_t BgL_zc3z04anonymousza31036ze3z87_121;

					BgL_zc3z04anonymousza31036ze3z87_121 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31036ze3ze5zzread_srcz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31036ze3z87_121,
						(int) (0L), BgL_portz00_37);
					return
						BGl_withzd2exceptionzd2handlerz00zz__errorz00
						(BGl_readzd2handlerzd2envz00zzread_srcz00,
						BgL_zc3z04anonymousza31036ze3z87_121);
				}
			}
		}

	}



/* &<@anonymous:1036> */
	obj_t BGl_z62zc3z04anonymousza31036ze3ze5zzread_srcz00(obj_t BgL_envz00_122)
	{
		{	/* Read/src.scm 82 */
			{	/* Read/src.scm 83 */
				obj_t BgL_portz00_123;

				BgL_portz00_123 = PROCEDURE_REF(BgL_envz00_122, (int) (0L));
				{	/* Read/src.scm 83 */
					obj_t BgL_g1014z00_152;

					{	/* Read/src.scm 83 */
						obj_t BgL_list1043z00_153;

						{	/* Read/src.scm 83 */
							obj_t BgL_arg1044z00_154;

							BgL_arg1044z00_154 = MAKE_YOUNG_PAIR(BTRUE, BNIL);
							BgL_list1043z00_153 =
								MAKE_YOUNG_PAIR(BgL_portz00_123, BgL_arg1044z00_154);
						}
						BgL_g1014z00_152 =
							BGl_compilerzd2readzd2srcz00zzread_readerz00(BgL_list1043z00_153);
					}
					{
						obj_t BgL_rz00_156;
						obj_t BgL_accz00_157;

						BgL_rz00_156 = BgL_g1014z00_152;
						BgL_accz00_157 = BNIL;
					BgL_loopz00_155:
						if (EOF_OBJECTP(BgL_rz00_156))
							{	/* Read/src.scm 85 */
								BGl_closezd2srczd2portz00zzread_srcz00();
								return BgL_accz00_157;
							}
						else
							{	/* Read/src.scm 89 */
								obj_t BgL_arg1039z00_158;
								obj_t BgL_arg1040z00_159;

								{	/* Read/src.scm 89 */
									obj_t BgL_list1041z00_160;

									{	/* Read/src.scm 89 */
										obj_t BgL_arg1042z00_161;

										BgL_arg1042z00_161 = MAKE_YOUNG_PAIR(BTRUE, BNIL);
										BgL_list1041z00_160 =
											MAKE_YOUNG_PAIR(BgL_portz00_123, BgL_arg1042z00_161);
									}
									BgL_arg1039z00_158 =
										BGl_compilerzd2readzd2srcz00zzread_readerz00
										(BgL_list1041z00_160);
								}
								BgL_arg1040z00_159 =
									MAKE_YOUNG_PAIR(BgL_rz00_156, BgL_accz00_157);
								{
									obj_t BgL_accz00_286;
									obj_t BgL_rz00_285;

									BgL_rz00_285 = BgL_arg1039z00_158;
									BgL_accz00_286 = BgL_arg1040z00_159;
									BgL_accz00_157 = BgL_accz00_286;
									BgL_rz00_156 = BgL_rz00_285;
									goto BgL_loopz00_155;
								}
							}
					}
				}
			}
		}

	}



/* open-src-file */
	obj_t BGl_openzd2srczd2filez00zzread_srcz00(obj_t BgL_sfilez00_5)
	{
		{	/* Read/src.scm 107 */
			BGl_readerzd2resetz12zc0zz__readerz00();
			if (STRINGP(BgL_sfilez00_5))
				{	/* Read/src.scm 111 */
					obj_t BgL_foundz00_56;

					BgL_foundz00_56 =
						BGl_findzd2filezf2pathz20zz__osz00(BgL_sfilez00_5,
						BGl_za2loadzd2pathza2zd2zz__evalz00);
					if (CBOOL(BgL_foundz00_56))
						{	/* Read/src.scm 113 */
							obj_t BgL_portz00_57;

							{	/* Read/src.scm 113 */

								BgL_portz00_57 =
									BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
									(BgL_foundz00_56, BTRUE, BINT(5000000L));
							}
							if (INPUT_PORTP(BgL_portz00_57))
								{	/* Read/src.scm 114 */
									BGl_readerzd2resetz12zc0zz__readerz00();
									return (BGl_za2portza2z00zzread_srcz00 =
										BgL_portz00_57, BUNSPEC);
								}
							else
								{	/* Read/src.scm 114 */
									return (BGl_za2portza2z00zzread_srcz00 =
										BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
											BGl_string1055z00zzread_srcz00, BgL_foundz00_56),
										BUNSPEC);
								}
						}
					else
						{	/* Read/src.scm 112 */
							return (BGl_za2portza2z00zzread_srcz00 =
								BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
									BGl_string1056z00zzread_srcz00, BgL_sfilez00_5), BUNSPEC);
						}
				}
			else
				{	/* Read/src.scm 124 */
					obj_t BgL_tmpz00_302;

					BgL_tmpz00_302 = BGL_CURRENT_DYNAMIC_ENV();
					return (BGl_za2portza2z00zzread_srcz00 =
						BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_302), BUNSPEC);
				}
		}

	}



/* close-src-port */
	obj_t BGl_closezd2srczd2portz00zzread_srcz00(void)
	{
		{	/* Read/src.scm 129 */
			{	/* Read/src.scm 130 */
				bool_t BgL_test1082z00_305;

				if (INPUT_PORTP(BGl_za2portza2z00zzread_srcz00))
					{	/* Read/src.scm 130 */
						bool_t BgL_test1084z00_308;

						{	/* Read/src.scm 130 */
							obj_t BgL_arg1052z00_69;

							{	/* Read/src.scm 130 */
								obj_t BgL_tmpz00_309;

								BgL_tmpz00_309 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg1052z00_69 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_309);
							}
							BgL_test1084z00_308 =
								(BGl_za2portza2z00zzread_srcz00 == BgL_arg1052z00_69);
						}
						if (BgL_test1084z00_308)
							{	/* Read/src.scm 130 */
								BgL_test1082z00_305 = ((bool_t) 0);
							}
						else
							{	/* Read/src.scm 130 */
								BgL_test1082z00_305 = ((bool_t) 1);
							}
					}
				else
					{	/* Read/src.scm 130 */
						BgL_test1082z00_305 = ((bool_t) 0);
					}
				if (BgL_test1082z00_305)
					{	/* Read/src.scm 130 */
						return bgl_close_input_port(BGl_za2portza2z00zzread_srcz00);
					}
				else
					{	/* Read/src.scm 130 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzread_srcz00(void)
	{
		{	/* Read/src.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzread_srcz00(void)
	{
		{	/* Read/src.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzread_srcz00(void)
	{
		{	/* Read/src.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzread_srcz00(void)
	{
		{	/* Read/src.scm 15 */
			BGl_modulezd2initializa7ationz75zzread_readerz00(95801752L,
				BSTRING_TO_STRING(BGl_string1057z00zzread_srcz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1057z00zzread_srcz00));
			BGl_modulezd2initializa7ationz75zzengine_enginez00(373986149L,
				BSTRING_TO_STRING(BGl_string1057z00zzread_srcz00));
			return
				BGl_modulezd2initializa7ationz75zzinit_mainz00(288050968L,
				BSTRING_TO_STRING(BGl_string1057z00zzread_srcz00));
		}

	}

#ifdef __cplusplus
}
#endif
