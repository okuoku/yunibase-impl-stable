/*===========================================================================*/
/*   (Trace/walk.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Trace/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TRACE_WALK_TYPE_DEFINITIONS
#define BGL_TRACE_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_TRACE_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62findzd2lastzd2nodez62zztrace_walkz00(obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2sync1299z62zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztrace_walkz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	extern bool_t BGl_isloopzf3zf3zztrace_isloopz00(BgL_letzd2funzd2_bglt);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_tracezd2idzd2zztrace_walkz00(obj_t);
	static obj_t BGl_z62findzd2lastzd2nodezd2appzd2l1273z62zztrace_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static bool_t BGl_toplevelzd2tracezd2nodeza2z12zb0zztrace_walkz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zztrace_walkz00(void);
	static obj_t BGl_z62findzd2lastzd2nodezd2setzd2e1291z62zztrace_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62tracezd2nodezd2app1301z62zztrace_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zztrace_walkz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_z62findzd2lastzd2nodezd2exter1277zb0zztrace_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62findzd2lastzd2nodezd2funca1275zb0zztrace_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2condition1313z62zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_findzd2lastzd2nodez00zztrace_walkz00(BgL_nodez00_bglt);
	static obj_t BGl_objectzd2initzd2zztrace_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2sequence1297z62zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_tracezd2walkz12zc0zztrace_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2letzd2fun1319zb0zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static bool_t BGl_tracezd2nodeza2z12z62zztrace_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62findzd2lastzd2nodezd2condi1281zb0zztrace_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztrace_walkz00(void);
	static obj_t BGl_tracezd2funz12zc0zztrace_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2appzd2ly1303zb0zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_letzd2varzd2_bglt
		BGl_makezd2tracedzd2nodez00zztrace_walkz00(BgL_nodez00_bglt,
		BgL_typez00_bglt, obj_t, obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2jumpzd2exzd2i1325z62zztrace_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_tracezd2nodezd2zztrace_walkz00(BgL_nodez00_bglt,
		obj_t, obj_t);
	static obj_t BGl_z62tracezd2walkz12za2zztrace_walkz00(obj_t, obj_t);
	static obj_t BGl_z62findzd2lastzd2nodezd2letzd2f1289z62zztrace_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62findzd2lastzd2nodezd2letzd2v1287z62zztrace_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2extern1307z62zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2setzd2exzd2it1323z62zztrace_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62tracezd2nodezb0zztrace_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2switch1317z62zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62findzd2lastzd2nodezd2app1271zb0zztrace_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	static obj_t BGl_z62findzd2lastzd2nodezd2jumpzd21293z62zztrace_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62toplevelzd2tracezd2nodez62zztrace_walkz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_fqnameze70ze7zztrace_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62toplevelzd2tracezd2nodezd21335zb0zztrace_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62toplevelzd2tracezd2nodezd21337zb0zztrace_walkz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztrace_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztrace_isloopz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_letz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62toplevelzd2tracezd2nodezd21340zb0zztrace_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2funcall1305z62zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62findzd2lastzd2node1264z62zztrace_walkz00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2letzd2var1321zb0zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zztrace_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztrace_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztrace_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2cast1309z62zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_findzd2lastzd2sexpz00zztrace_walkz00(BgL_nodez00_bglt);
	static obj_t BGl_gczd2rootszd2initz00zztrace_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2makezd2box1327zb0zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2boxzd2ref1329zb0zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62findzd2lastzd2nodezd2setq1279zb0zztrace_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2boxzd2setz121331za2zztrace_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62toplevelzd2tracezd2node1332z62zztrace_walkz00(obj_t,
		obj_t);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62findzd2lastzd2nodezd2sync1269zb0zztrace_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2setq1311z62zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62findzd2lastzd2nodezd2fail1283zb0zztrace_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_toplevelzd2tracezd2nodez00zztrace_walkz00(BgL_nodez00_bglt);
	static obj_t BGl_z62findzd2lastzd2nodezd2seque1267zb0zztrace_walkz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2fail1315z62zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern bool_t BGl_userzd2symbolzf3z21zzast_identz00(obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static obj_t BGl_z62tracezd2node1294zb0zztrace_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00;
	static obj_t BGl_z62findzd2lastzd2nodezd2switc1285zb0zztrace_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[21];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2112z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2151za7,
		BGl_z62findzd2lastzd2nodezd2sync1269zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2113z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2152za7,
		BGl_z62findzd2lastzd2nodezd2app1271zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2114z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2153za7,
		BGl_z62findzd2lastzd2nodezd2appzd2l1273z62zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2115z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2154za7,
		BGl_z62findzd2lastzd2nodezd2funca1275zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2116z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2155za7,
		BGl_z62findzd2lastzd2nodezd2exter1277zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2117z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2156za7,
		BGl_z62findzd2lastzd2nodezd2setq1279zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2118z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2157za7,
		BGl_z62findzd2lastzd2nodezd2condi1281zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2119z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2158za7,
		BGl_z62findzd2lastzd2nodezd2fail1283zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2126z00zztrace_walkz00,
		BgL_bgl_string2126za700za7za7t2159za7, "trace-node", 10);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_toplevelzd2tracezd2nodezd2envzd2zztrace_walkz00,
		BgL_bgl_za762toplevelza7d2tr2160z00,
		BGl_z62toplevelzd2tracezd2nodez62zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2120z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2161za7,
		BGl_z62findzd2lastzd2nodezd2switc1285zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2121z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2162za7,
		BGl_z62findzd2lastzd2nodezd2letzd2v1287z62zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2122z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2163za7,
		BGl_z62findzd2lastzd2nodezd2letzd2f1289z62zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2123z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2164za7,
		BGl_z62findzd2lastzd2nodezd2setzd2e1291z62zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2124z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2165za7,
		BGl_z62findzd2lastzd2nodezd2jumpzd21293z62zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2125z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72166za7,
		BGl_z62tracezd2nodezd2sequence1297z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72167za7,
		BGl_z62tracezd2nodezd2sync1299z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2128z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72168za7,
		BGl_z62tracezd2nodezd2app1301z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2129z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72169za7,
		BGl_z62tracezd2nodezd2appzd2ly1303zb0zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2130z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72170za7,
		BGl_z62tracezd2nodezd2funcall1305z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72171za7,
		BGl_z62tracezd2nodezd2extern1307z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72172za7,
		BGl_z62tracezd2nodezd2cast1309z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72173za7,
		BGl_z62tracezd2nodezd2setq1311z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2134z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72174za7,
		BGl_z62tracezd2nodezd2condition1313z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2135z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72175za7,
		BGl_z62tracezd2nodezd2fail1315z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2136z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72176za7,
		BGl_z62tracezd2nodezd2switch1317z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2137z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72177za7,
		BGl_z62tracezd2nodezd2letzd2fun1319zb0zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2138z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72178za7,
		BGl_z62tracezd2nodezd2letzd2var1321zb0zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2145z00zztrace_walkz00,
		BgL_bgl_string2145za700za7za7t2179za7, "toplevel-trace-node", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2139z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72180za7,
		BGl_z62tracezd2nodezd2setzd2exzd2it1323z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2148z00zztrace_walkz00,
		BgL_bgl_string2148za700za7za7t2181za7, "trace_walk", 10);
	      DEFINE_STRING(BGl_string2149z00zztrace_walkz00,
		BgL_bgl_string2149za700za7za7t2182za7,
		"|:| value let $env-pop-trace $env-push-trace current-dynamic-env dynamic-env quote at location env loc name aux generic-init method-init no-trace @ imported-modules-init toplevel-init pass-started ",
		197);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2140z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72183za7,
		BGl_z62tracezd2nodezd2jumpzd2exzd2i1325z62zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2141z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72184za7,
		BGl_z62tracezd2nodezd2makezd2box1327zb0zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2142z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72185za7,
		BGl_z62tracezd2nodezd2boxzd2ref1329zb0zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2143z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72186za7,
		BGl_z62tracezd2nodezd2boxzd2setz121331za2zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zztrace_walkz00,
		BgL_bgl_za762toplevelza7d2tr2187z00,
		BGl_z62toplevelzd2tracezd2nodezd21335zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zztrace_walkz00,
		BgL_bgl_za762toplevelza7d2tr2188z00,
		BGl_z62toplevelzd2tracezd2nodezd21337zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zztrace_walkz00,
		BgL_bgl_za762toplevelza7d2tr2189z00,
		BGl_z62toplevelzd2tracezd2nodezd21340zb0zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2094z00zztrace_walkz00,
		BgL_bgl_string2094za700za7za7t2190za7, "Trace", 5);
	      DEFINE_STRING(BGl_string2095z00zztrace_walkz00,
		BgL_bgl_string2095za700za7za7t2191za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2096z00zztrace_walkz00,
		BgL_bgl_string2096za700za7za7t2192za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2097z00zztrace_walkz00,
		BgL_bgl_string2097za700za7za7t2193za7, " error", 6);
	      DEFINE_STRING(BGl_string2098z00zztrace_walkz00,
		BgL_bgl_string2098za700za7za7t2194za7, "s", 1);
	      DEFINE_STRING(BGl_string2099z00zztrace_walkz00,
		BgL_bgl_string2099za700za7za7t2195za7, "", 0);
	      DEFINE_STATIC_BGL_GENERIC(BGl_tracezd2nodezd2envz00zztrace_walkz00,
		BgL_bgl_za762traceza7d2nodeza72196za7,
		BGl_z62tracezd2nodezb0zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2walkz12zd2envz12zztrace_walkz00,
		BgL_bgl_za762traceza7d2walkza72197za7,
		BGl_z62tracezd2walkz12za2zztrace_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2198za7,
		BGl_z62findzd2lastzd2nodez62zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2100z00zztrace_walkz00,
		BgL_bgl_string2100za700za7za7t2199za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string2101z00zztrace_walkz00,
		BgL_bgl_string2101za700za7za7t2200za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string2102z00zztrace_walkz00,
		BgL_bgl_string2102za700za7za7t2201za7, "%toplevel@", 10);
	      DEFINE_STRING(BGl_string2103z00zztrace_walkz00,
		BgL_bgl_string2103za700za7za7t2202za7, "%import@", 8);
	      DEFINE_STRING(BGl_string2105z00zztrace_walkz00,
		BgL_bgl_string2105za700za7za7t2203za7, "find-last-node1264", 18);
	      DEFINE_STRING(BGl_string2107z00zztrace_walkz00,
		BgL_bgl_string2107za700za7za7t2204za7, "trace-node1294", 14);
	      DEFINE_STRING(BGl_string2109z00zztrace_walkz00,
		BgL_bgl_string2109za700za7za7t2205za7, "toplevel-trace-node1332", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2104z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2206za7,
		BGl_z62findzd2lastzd2node1264z62zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2111z00zztrace_walkz00,
		BgL_bgl_string2111za700za7za7t2207za7, "find-last-node", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2106z00zztrace_walkz00,
		BgL_bgl_za762traceza7d2node12208z00,
		BGl_z62tracezd2node1294zb0zztrace_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2108z00zztrace_walkz00,
		BgL_bgl_za762toplevelza7d2tr2209z00,
		BGl_z62toplevelzd2tracezd2node1332z62zztrace_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2110z00zztrace_walkz00,
		BgL_bgl_za762findza7d2lastza7d2210za7,
		BGl_z62findzd2lastzd2nodezd2seque1267zb0zztrace_walkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztrace_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztrace_walkz00(long
		BgL_checksumz00_3595, char *BgL_fromz00_3596)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztrace_walkz00))
				{
					BGl_requirezd2initializa7ationz75zztrace_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztrace_walkz00();
					BGl_libraryzd2moduleszd2initz00zztrace_walkz00();
					BGl_cnstzd2initzd2zztrace_walkz00();
					BGl_importedzd2moduleszd2initz00zztrace_walkz00();
					BGl_genericzd2initzd2zztrace_walkz00();
					BGl_methodzd2initzd2zztrace_walkz00();
					return BGl_toplevelzd2initzd2zztrace_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztrace_walkz00(void)
	{
		{	/* Trace/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"trace_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"trace_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"trace_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "trace_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "trace_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztrace_walkz00(void)
	{
		{	/* Trace/walk.scm 15 */
			{	/* Trace/walk.scm 15 */
				obj_t BgL_cportz00_3409;

				{	/* Trace/walk.scm 15 */
					obj_t BgL_stringz00_3416;

					BgL_stringz00_3416 = BGl_string2149z00zztrace_walkz00;
					{	/* Trace/walk.scm 15 */
						obj_t BgL_startz00_3417;

						BgL_startz00_3417 = BINT(0L);
						{	/* Trace/walk.scm 15 */
							obj_t BgL_endz00_3418;

							BgL_endz00_3418 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3416)));
							{	/* Trace/walk.scm 15 */

								BgL_cportz00_3409 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3416, BgL_startz00_3417, BgL_endz00_3418);
				}}}}
				{
					long BgL_iz00_3410;

					BgL_iz00_3410 = 20L;
				BgL_loopz00_3411:
					if ((BgL_iz00_3410 == -1L))
						{	/* Trace/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Trace/walk.scm 15 */
							{	/* Trace/walk.scm 15 */
								obj_t BgL_arg2150z00_3412;

								{	/* Trace/walk.scm 15 */

									{	/* Trace/walk.scm 15 */
										obj_t BgL_locationz00_3414;

										BgL_locationz00_3414 = BBOOL(((bool_t) 0));
										{	/* Trace/walk.scm 15 */

											BgL_arg2150z00_3412 =
												BGl_readz00zz__readerz00(BgL_cportz00_3409,
												BgL_locationz00_3414);
										}
									}
								}
								{	/* Trace/walk.scm 15 */
									int BgL_tmpz00_3631;

									BgL_tmpz00_3631 = (int) (BgL_iz00_3410);
									CNST_TABLE_SET(BgL_tmpz00_3631, BgL_arg2150z00_3412);
							}}
							{	/* Trace/walk.scm 15 */
								int BgL_auxz00_3415;

								BgL_auxz00_3415 = (int) ((BgL_iz00_3410 - 1L));
								{
									long BgL_iz00_3636;

									BgL_iz00_3636 = (long) (BgL_auxz00_3415);
									BgL_iz00_3410 = BgL_iz00_3636;
									goto BgL_loopz00_3411;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztrace_walkz00(void)
	{
		{	/* Trace/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztrace_walkz00(void)
	{
		{	/* Trace/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* trace-walk! */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2walkz12zc0zztrace_walkz00(obj_t
		BgL_globalsz00_17)
	{
		{	/* Trace/walk.scm 39 */
			{	/* Trace/walk.scm 40 */
				obj_t BgL_list1365z00_1575;

				{	/* Trace/walk.scm 40 */
					obj_t BgL_arg1367z00_1576;

					{	/* Trace/walk.scm 40 */
						obj_t BgL_arg1370z00_1577;

						BgL_arg1370z00_1577 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1367z00_1576 =
							MAKE_YOUNG_PAIR(BGl_string2094z00zztrace_walkz00,
							BgL_arg1370z00_1577);
					}
					BgL_list1365z00_1575 =
						MAKE_YOUNG_PAIR(BGl_string2095z00zztrace_walkz00,
						BgL_arg1367z00_1576);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1365z00_1575);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string2094z00zztrace_walkz00;
			{	/* Trace/walk.scm 40 */
				obj_t BgL_g1108z00_1578;

				BgL_g1108z00_1578 = BNIL;
				{
					obj_t BgL_hooksz00_1581;
					obj_t BgL_hnamesz00_1582;

					BgL_hooksz00_1581 = BgL_g1108z00_1578;
					BgL_hnamesz00_1582 = BNIL;
				BgL_zc3z04anonymousza31371ze3z87_1583:
					if (NULLP(BgL_hooksz00_1581))
						{	/* Trace/walk.scm 40 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Trace/walk.scm 40 */
							bool_t BgL_test2214z00_3649;

							{	/* Trace/walk.scm 40 */
								obj_t BgL_fun1423z00_1590;

								BgL_fun1423z00_1590 = CAR(((obj_t) BgL_hooksz00_1581));
								BgL_test2214z00_3649 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1423z00_1590));
							}
							if (BgL_test2214z00_3649)
								{	/* Trace/walk.scm 40 */
									obj_t BgL_arg1408z00_1587;
									obj_t BgL_arg1410z00_1588;

									BgL_arg1408z00_1587 = CDR(((obj_t) BgL_hooksz00_1581));
									BgL_arg1410z00_1588 = CDR(((obj_t) BgL_hnamesz00_1582));
									{
										obj_t BgL_hnamesz00_3661;
										obj_t BgL_hooksz00_3660;

										BgL_hooksz00_3660 = BgL_arg1408z00_1587;
										BgL_hnamesz00_3661 = BgL_arg1410z00_1588;
										BgL_hnamesz00_1582 = BgL_hnamesz00_3661;
										BgL_hooksz00_1581 = BgL_hooksz00_3660;
										goto BgL_zc3z04anonymousza31371ze3z87_1583;
									}
								}
							else
								{	/* Trace/walk.scm 40 */
									obj_t BgL_arg1421z00_1589;

									BgL_arg1421z00_1589 = CAR(((obj_t) BgL_hnamesz00_1582));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2094z00zztrace_walkz00,
										BGl_string2096z00zztrace_walkz00, BgL_arg1421z00_1589);
								}
						}
				}
			}
			{	/* Trace/walk.scm 46 */
				obj_t BgL_idz00_1593;

				BgL_idz00_1593 = CNST_TABLE_REF(1);
				{	/* Trace/walk.scm 46 */
					obj_t BgL_gloz00_1594;

					{	/* Trace/walk.scm 47 */
						obj_t BgL_list1435z00_1598;

						BgL_list1435z00_1598 =
							MAKE_YOUNG_PAIR(BGl_za2moduleza2z00zzmodule_modulez00, BNIL);
						BgL_gloz00_1594 =
							BGl_findzd2globalzd2zzast_envz00(BgL_idz00_1593,
							BgL_list1435z00_1598);
					}
					{	/* Trace/walk.scm 47 */

						{	/* Trace/walk.scm 48 */
							bool_t BgL_test2215z00_3668;

							{	/* Trace/walk.scm 48 */
								obj_t BgL_classz00_2346;

								BgL_classz00_2346 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gloz00_1594))
									{	/* Trace/walk.scm 48 */
										BgL_objectz00_bglt BgL_arg1807z00_2348;

										BgL_arg1807z00_2348 =
											(BgL_objectz00_bglt) (BgL_gloz00_1594);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Trace/walk.scm 48 */
												long BgL_idxz00_2354;

												BgL_idxz00_2354 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2348);
												BgL_test2215z00_3668 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2354 + 2L)) == BgL_classz00_2346);
											}
										else
											{	/* Trace/walk.scm 48 */
												bool_t BgL_res2055z00_2379;

												{	/* Trace/walk.scm 48 */
													obj_t BgL_oclassz00_2362;

													{	/* Trace/walk.scm 48 */
														obj_t BgL_arg1815z00_2370;
														long BgL_arg1816z00_2371;

														BgL_arg1815z00_2370 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Trace/walk.scm 48 */
															long BgL_arg1817z00_2372;

															BgL_arg1817z00_2372 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2348);
															BgL_arg1816z00_2371 =
																(BgL_arg1817z00_2372 - OBJECT_TYPE);
														}
														BgL_oclassz00_2362 =
															VECTOR_REF(BgL_arg1815z00_2370,
															BgL_arg1816z00_2371);
													}
													{	/* Trace/walk.scm 48 */
														bool_t BgL__ortest_1115z00_2363;

														BgL__ortest_1115z00_2363 =
															(BgL_classz00_2346 == BgL_oclassz00_2362);
														if (BgL__ortest_1115z00_2363)
															{	/* Trace/walk.scm 48 */
																BgL_res2055z00_2379 = BgL__ortest_1115z00_2363;
															}
														else
															{	/* Trace/walk.scm 48 */
																long BgL_odepthz00_2364;

																{	/* Trace/walk.scm 48 */
																	obj_t BgL_arg1804z00_2365;

																	BgL_arg1804z00_2365 = (BgL_oclassz00_2362);
																	BgL_odepthz00_2364 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2365);
																}
																if ((2L < BgL_odepthz00_2364))
																	{	/* Trace/walk.scm 48 */
																		obj_t BgL_arg1802z00_2367;

																		{	/* Trace/walk.scm 48 */
																			obj_t BgL_arg1803z00_2368;

																			BgL_arg1803z00_2368 =
																				(BgL_oclassz00_2362);
																			BgL_arg1802z00_2367 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2368, 2L);
																		}
																		BgL_res2055z00_2379 =
																			(BgL_arg1802z00_2367 ==
																			BgL_classz00_2346);
																	}
																else
																	{	/* Trace/walk.scm 48 */
																		BgL_res2055z00_2379 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2215z00_3668 = BgL_res2055z00_2379;
											}
									}
								else
									{	/* Trace/walk.scm 48 */
										BgL_test2215z00_3668 = ((bool_t) 0);
									}
							}
							if (BgL_test2215z00_3668)
								{	/* Trace/walk.scm 49 */
									BgL_sfunz00_bglt BgL_i1110z00_1596;

									BgL_i1110z00_1596 =
										((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_gloz00_1594))))->
											BgL_valuez00));
									{
										obj_t BgL_auxz00_3695;

										{	/* Trace/walk.scm 50 */
											obj_t BgL_arg1434z00_1597;

											BgL_arg1434z00_1597 =
												(((BgL_sfunz00_bglt) COBJECT(BgL_i1110z00_1596))->
												BgL_bodyz00);
											BgL_auxz00_3695 =
												((obj_t)
												BGl_toplevelzd2tracezd2nodez00zztrace_walkz00((
														(BgL_nodez00_bglt) BgL_arg1434z00_1597)));
										}
										((((BgL_sfunz00_bglt) COBJECT(BgL_i1110z00_1596))->
												BgL_bodyz00) = ((obj_t) BgL_auxz00_3695), BUNSPEC);
									}
								}
							else
								{	/* Trace/walk.scm 48 */
									BFALSE;
								}
						}
					}
				}
			}
			{
				obj_t BgL_l1252z00_1600;

				BgL_l1252z00_1600 = BgL_globalsz00_17;
			BgL_zc3z04anonymousza31436ze3z87_1601:
				if (PAIRP(BgL_l1252z00_1600))
					{	/* Trace/walk.scm 52 */
						{	/* Trace/walk.scm 52 */
							obj_t BgL_vz00_1603;

							BgL_vz00_1603 = CAR(BgL_l1252z00_1600);
							BGl_tracezd2funz12zc0zztrace_walkz00(BgL_vz00_1603, BNIL,
								BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00);
						}
						{
							obj_t BgL_l1252z00_3705;

							BgL_l1252z00_3705 = CDR(BgL_l1252z00_1600);
							BgL_l1252z00_1600 = BgL_l1252z00_3705;
							goto BgL_zc3z04anonymousza31436ze3z87_1601;
						}
					}
				else
					{	/* Trace/walk.scm 52 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Trace/walk.scm 53 */
					{	/* Trace/walk.scm 53 */
						obj_t BgL_port1254z00_1608;

						{	/* Trace/walk.scm 53 */
							obj_t BgL_tmpz00_3710;

							BgL_tmpz00_3710 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1254z00_1608 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3710);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1254z00_1608);
						bgl_display_string(BGl_string2097z00zztrace_walkz00,
							BgL_port1254z00_1608);
						{	/* Trace/walk.scm 53 */
							obj_t BgL_arg1453z00_1609;

							{	/* Trace/walk.scm 53 */
								bool_t BgL_test2222z00_3715;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Trace/walk.scm 53 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Trace/walk.scm 53 */
												BgL_test2222z00_3715 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Trace/walk.scm 53 */
												BgL_test2222z00_3715 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Trace/walk.scm 53 */
										BgL_test2222z00_3715 = ((bool_t) 0);
									}
								if (BgL_test2222z00_3715)
									{	/* Trace/walk.scm 53 */
										BgL_arg1453z00_1609 = BGl_string2098z00zztrace_walkz00;
									}
								else
									{	/* Trace/walk.scm 53 */
										BgL_arg1453z00_1609 = BGl_string2099z00zztrace_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1453z00_1609, BgL_port1254z00_1608);
						}
						bgl_display_string(BGl_string2100z00zztrace_walkz00,
							BgL_port1254z00_1608);
						bgl_display_char(((unsigned char) 10), BgL_port1254z00_1608);
					}
					{	/* Trace/walk.scm 53 */
						obj_t BgL_list1457z00_1613;

						BgL_list1457z00_1613 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1457z00_1613);
					}
				}
			else
				{	/* Trace/walk.scm 53 */
					obj_t BgL_g1111z00_1614;

					BgL_g1111z00_1614 = BNIL;
					{
						obj_t BgL_hooksz00_1617;
						obj_t BgL_hnamesz00_1618;

						BgL_hooksz00_1617 = BgL_g1111z00_1614;
						BgL_hnamesz00_1618 = BNIL;
					BgL_zc3z04anonymousza31458ze3z87_1619:
						if (NULLP(BgL_hooksz00_1617))
							{	/* Trace/walk.scm 53 */
								return BgL_globalsz00_17;
							}
						else
							{	/* Trace/walk.scm 53 */
								bool_t BgL_test2226z00_3732;

								{	/* Trace/walk.scm 53 */
									obj_t BgL_fun1487z00_1626;

									BgL_fun1487z00_1626 = CAR(((obj_t) BgL_hooksz00_1617));
									BgL_test2226z00_3732 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1487z00_1626));
								}
								if (BgL_test2226z00_3732)
									{	/* Trace/walk.scm 53 */
										obj_t BgL_arg1472z00_1623;
										obj_t BgL_arg1473z00_1624;

										BgL_arg1472z00_1623 = CDR(((obj_t) BgL_hooksz00_1617));
										BgL_arg1473z00_1624 = CDR(((obj_t) BgL_hnamesz00_1618));
										{
											obj_t BgL_hnamesz00_3744;
											obj_t BgL_hooksz00_3743;

											BgL_hooksz00_3743 = BgL_arg1472z00_1623;
											BgL_hnamesz00_3744 = BgL_arg1473z00_1624;
											BgL_hnamesz00_1618 = BgL_hnamesz00_3744;
											BgL_hooksz00_1617 = BgL_hooksz00_3743;
											goto BgL_zc3z04anonymousza31458ze3z87_1619;
										}
									}
								else
									{	/* Trace/walk.scm 53 */
										obj_t BgL_arg1485z00_1625;

										BgL_arg1485z00_1625 = CAR(((obj_t) BgL_hnamesz00_1618));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string2101z00zztrace_walkz00, BgL_arg1485z00_1625);
									}
							}
					}
				}
		}

	}



/* &trace-walk! */
	obj_t BGl_z62tracezd2walkz12za2zztrace_walkz00(obj_t BgL_envz00_3246,
		obj_t BgL_globalsz00_3247)
	{
		{	/* Trace/walk.scm 39 */
			return BGl_tracezd2walkz12zc0zztrace_walkz00(BgL_globalsz00_3247);
		}

	}



/* trace-id */
	obj_t BGl_tracezd2idzd2zztrace_walkz00(obj_t BgL_vz00_18)
	{
		{	/* Trace/walk.scm 64 */
			{	/* Trace/walk.scm 66 */
				bool_t BgL_test2227z00_3749;

				{	/* Trace/walk.scm 66 */
					bool_t BgL_test2228z00_3750;

					{	/* Trace/walk.scm 66 */
						obj_t BgL_classz00_2396;

						BgL_classz00_2396 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_vz00_18))
							{	/* Trace/walk.scm 66 */
								BgL_objectz00_bglt BgL_arg1807z00_2398;

								BgL_arg1807z00_2398 = (BgL_objectz00_bglt) (BgL_vz00_18);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Trace/walk.scm 66 */
										long BgL_idxz00_2404;

										BgL_idxz00_2404 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2398);
										BgL_test2228z00_3750 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2404 + 2L)) == BgL_classz00_2396);
									}
								else
									{	/* Trace/walk.scm 66 */
										bool_t BgL_res2057z00_2429;

										{	/* Trace/walk.scm 66 */
											obj_t BgL_oclassz00_2412;

											{	/* Trace/walk.scm 66 */
												obj_t BgL_arg1815z00_2420;
												long BgL_arg1816z00_2421;

												BgL_arg1815z00_2420 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Trace/walk.scm 66 */
													long BgL_arg1817z00_2422;

													BgL_arg1817z00_2422 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2398);
													BgL_arg1816z00_2421 =
														(BgL_arg1817z00_2422 - OBJECT_TYPE);
												}
												BgL_oclassz00_2412 =
													VECTOR_REF(BgL_arg1815z00_2420, BgL_arg1816z00_2421);
											}
											{	/* Trace/walk.scm 66 */
												bool_t BgL__ortest_1115z00_2413;

												BgL__ortest_1115z00_2413 =
													(BgL_classz00_2396 == BgL_oclassz00_2412);
												if (BgL__ortest_1115z00_2413)
													{	/* Trace/walk.scm 66 */
														BgL_res2057z00_2429 = BgL__ortest_1115z00_2413;
													}
												else
													{	/* Trace/walk.scm 66 */
														long BgL_odepthz00_2414;

														{	/* Trace/walk.scm 66 */
															obj_t BgL_arg1804z00_2415;

															BgL_arg1804z00_2415 = (BgL_oclassz00_2412);
															BgL_odepthz00_2414 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2415);
														}
														if ((2L < BgL_odepthz00_2414))
															{	/* Trace/walk.scm 66 */
																obj_t BgL_arg1802z00_2417;

																{	/* Trace/walk.scm 66 */
																	obj_t BgL_arg1803z00_2418;

																	BgL_arg1803z00_2418 = (BgL_oclassz00_2412);
																	BgL_arg1802z00_2417 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2418,
																		2L);
																}
																BgL_res2057z00_2429 =
																	(BgL_arg1802z00_2417 == BgL_classz00_2396);
															}
														else
															{	/* Trace/walk.scm 66 */
																BgL_res2057z00_2429 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2228z00_3750 = BgL_res2057z00_2429;
									}
							}
						else
							{	/* Trace/walk.scm 66 */
								BgL_test2228z00_3750 = ((bool_t) 0);
							}
					}
					if (BgL_test2228z00_3750)
						{	/* Trace/walk.scm 66 */
							BgL_test2227z00_3749 =
								(
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_vz00_18))))->BgL_idz00) ==
								CNST_TABLE_REF(1));
						}
					else
						{	/* Trace/walk.scm 66 */
							BgL_test2227z00_3749 = ((bool_t) 0);
						}
				}
				if (BgL_test2227z00_3749)
					{	/* Trace/walk.scm 67 */
						obj_t BgL_arg1509z00_1632;

						{	/* Trace/walk.scm 67 */
							obj_t BgL_arg1513z00_1633;
							obj_t BgL_arg1514z00_1634;

							{	/* Trace/walk.scm 67 */
								obj_t BgL_arg1516z00_1635;

								BgL_arg1516z00_1635 =
									bstring_to_symbol(BGl_string2102z00zztrace_walkz00);
								{	/* Trace/walk.scm 67 */
									obj_t BgL_arg1455z00_2433;

									BgL_arg1455z00_2433 = SYMBOL_TO_STRING(BgL_arg1516z00_1635);
									BgL_arg1513z00_1633 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2433);
								}
							}
							{	/* Trace/walk.scm 67 */
								obj_t BgL_arg1535z00_1636;

								BgL_arg1535z00_1636 =
									(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_vz00_18)))->BgL_modulez00);
								{	/* Trace/walk.scm 67 */
									obj_t BgL_arg1455z00_2436;

									BgL_arg1455z00_2436 = SYMBOL_TO_STRING(BgL_arg1535z00_1636);
									BgL_arg1514z00_1634 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2436);
								}
							}
							BgL_arg1509z00_1632 =
								string_append(BgL_arg1513z00_1633, BgL_arg1514z00_1634);
						}
						return bstring_to_symbol(BgL_arg1509z00_1632);
					}
				else
					{	/* Trace/walk.scm 68 */
						bool_t BgL_test2233z00_3787;

						{	/* Trace/walk.scm 68 */
							bool_t BgL_test2234z00_3788;

							{	/* Trace/walk.scm 68 */
								obj_t BgL_classz00_2438;

								BgL_classz00_2438 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_vz00_18))
									{	/* Trace/walk.scm 68 */
										BgL_objectz00_bglt BgL_arg1807z00_2440;

										BgL_arg1807z00_2440 = (BgL_objectz00_bglt) (BgL_vz00_18);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Trace/walk.scm 68 */
												long BgL_idxz00_2446;

												BgL_idxz00_2446 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2440);
												BgL_test2234z00_3788 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2446 + 2L)) == BgL_classz00_2438);
											}
										else
											{	/* Trace/walk.scm 68 */
												bool_t BgL_res2058z00_2471;

												{	/* Trace/walk.scm 68 */
													obj_t BgL_oclassz00_2454;

													{	/* Trace/walk.scm 68 */
														obj_t BgL_arg1815z00_2462;
														long BgL_arg1816z00_2463;

														BgL_arg1815z00_2462 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Trace/walk.scm 68 */
															long BgL_arg1817z00_2464;

															BgL_arg1817z00_2464 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2440);
															BgL_arg1816z00_2463 =
																(BgL_arg1817z00_2464 - OBJECT_TYPE);
														}
														BgL_oclassz00_2454 =
															VECTOR_REF(BgL_arg1815z00_2462,
															BgL_arg1816z00_2463);
													}
													{	/* Trace/walk.scm 68 */
														bool_t BgL__ortest_1115z00_2455;

														BgL__ortest_1115z00_2455 =
															(BgL_classz00_2438 == BgL_oclassz00_2454);
														if (BgL__ortest_1115z00_2455)
															{	/* Trace/walk.scm 68 */
																BgL_res2058z00_2471 = BgL__ortest_1115z00_2455;
															}
														else
															{	/* Trace/walk.scm 68 */
																long BgL_odepthz00_2456;

																{	/* Trace/walk.scm 68 */
																	obj_t BgL_arg1804z00_2457;

																	BgL_arg1804z00_2457 = (BgL_oclassz00_2454);
																	BgL_odepthz00_2456 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2457);
																}
																if ((2L < BgL_odepthz00_2456))
																	{	/* Trace/walk.scm 68 */
																		obj_t BgL_arg1802z00_2459;

																		{	/* Trace/walk.scm 68 */
																			obj_t BgL_arg1803z00_2460;

																			BgL_arg1803z00_2460 =
																				(BgL_oclassz00_2454);
																			BgL_arg1802z00_2459 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2460, 2L);
																		}
																		BgL_res2058z00_2471 =
																			(BgL_arg1802z00_2459 ==
																			BgL_classz00_2438);
																	}
																else
																	{	/* Trace/walk.scm 68 */
																		BgL_res2058z00_2471 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2234z00_3788 = BgL_res2058z00_2471;
											}
									}
								else
									{	/* Trace/walk.scm 68 */
										BgL_test2234z00_3788 = ((bool_t) 0);
									}
							}
							if (BgL_test2234z00_3788)
								{	/* Trace/walk.scm 68 */
									BgL_test2233z00_3787 =
										(
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_vz00_18))))->BgL_idz00) ==
										CNST_TABLE_REF(2));
								}
							else
								{	/* Trace/walk.scm 68 */
									BgL_test2233z00_3787 = ((bool_t) 0);
								}
						}
						if (BgL_test2233z00_3787)
							{	/* Trace/walk.scm 69 */
								obj_t BgL_arg1544z00_1640;

								{	/* Trace/walk.scm 69 */
									obj_t BgL_arg1546z00_1641;
									obj_t BgL_arg1552z00_1642;

									{	/* Trace/walk.scm 69 */
										obj_t BgL_arg1553z00_1643;

										BgL_arg1553z00_1643 =
											bstring_to_symbol(BGl_string2103z00zztrace_walkz00);
										{	/* Trace/walk.scm 69 */
											obj_t BgL_arg1455z00_2475;

											BgL_arg1455z00_2475 =
												SYMBOL_TO_STRING(BgL_arg1553z00_1643);
											BgL_arg1546z00_1641 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2475);
										}
									}
									{	/* Trace/walk.scm 69 */
										obj_t BgL_arg1559z00_1644;

										BgL_arg1559z00_1644 =
											(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_vz00_18)))->BgL_modulez00);
										{	/* Trace/walk.scm 69 */
											obj_t BgL_arg1455z00_2478;

											BgL_arg1455z00_2478 =
												SYMBOL_TO_STRING(BgL_arg1559z00_1644);
											BgL_arg1552z00_1642 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2478);
										}
									}
									BgL_arg1544z00_1640 =
										string_append(BgL_arg1546z00_1641, BgL_arg1552z00_1642);
								}
								return bstring_to_symbol(BgL_arg1544z00_1640);
							}
						else
							{	/* Trace/walk.scm 70 */
								bool_t BgL_test2239z00_3825;

								{	/* Trace/walk.scm 70 */
									obj_t BgL_classz00_2480;

									BgL_classz00_2480 = BGl_globalz00zzast_varz00;
									if (BGL_OBJECTP(BgL_vz00_18))
										{	/* Trace/walk.scm 70 */
											BgL_objectz00_bglt BgL_arg1807z00_2482;

											BgL_arg1807z00_2482 = (BgL_objectz00_bglt) (BgL_vz00_18);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Trace/walk.scm 70 */
													long BgL_idxz00_2488;

													BgL_idxz00_2488 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2482);
													BgL_test2239z00_3825 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2488 + 2L)) == BgL_classz00_2480);
												}
											else
												{	/* Trace/walk.scm 70 */
													bool_t BgL_res2059z00_2513;

													{	/* Trace/walk.scm 70 */
														obj_t BgL_oclassz00_2496;

														{	/* Trace/walk.scm 70 */
															obj_t BgL_arg1815z00_2504;
															long BgL_arg1816z00_2505;

															BgL_arg1815z00_2504 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Trace/walk.scm 70 */
																long BgL_arg1817z00_2506;

																BgL_arg1817z00_2506 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2482);
																BgL_arg1816z00_2505 =
																	(BgL_arg1817z00_2506 - OBJECT_TYPE);
															}
															BgL_oclassz00_2496 =
																VECTOR_REF(BgL_arg1815z00_2504,
																BgL_arg1816z00_2505);
														}
														{	/* Trace/walk.scm 70 */
															bool_t BgL__ortest_1115z00_2497;

															BgL__ortest_1115z00_2497 =
																(BgL_classz00_2480 == BgL_oclassz00_2496);
															if (BgL__ortest_1115z00_2497)
																{	/* Trace/walk.scm 70 */
																	BgL_res2059z00_2513 =
																		BgL__ortest_1115z00_2497;
																}
															else
																{	/* Trace/walk.scm 70 */
																	long BgL_odepthz00_2498;

																	{	/* Trace/walk.scm 70 */
																		obj_t BgL_arg1804z00_2499;

																		BgL_arg1804z00_2499 = (BgL_oclassz00_2496);
																		BgL_odepthz00_2498 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2499);
																	}
																	if ((2L < BgL_odepthz00_2498))
																		{	/* Trace/walk.scm 70 */
																			obj_t BgL_arg1802z00_2501;

																			{	/* Trace/walk.scm 70 */
																				obj_t BgL_arg1803z00_2502;

																				BgL_arg1803z00_2502 =
																					(BgL_oclassz00_2496);
																				BgL_arg1802z00_2501 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2502, 2L);
																			}
																			BgL_res2059z00_2513 =
																				(BgL_arg1802z00_2501 ==
																				BgL_classz00_2480);
																		}
																	else
																		{	/* Trace/walk.scm 70 */
																			BgL_res2059z00_2513 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2239z00_3825 = BgL_res2059z00_2513;
												}
										}
									else
										{	/* Trace/walk.scm 70 */
											BgL_test2239z00_3825 = ((bool_t) 0);
										}
								}
								if (BgL_test2239z00_3825)
									{	/* Trace/walk.scm 71 */
										obj_t BgL_arg1564z00_1646;
										obj_t BgL_arg1565z00_1647;

										BgL_arg1564z00_1646 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_vz00_18))))->BgL_idz00);
										BgL_arg1565z00_1647 =
											(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_vz00_18)))->BgL_modulez00);
										{	/* Trace/walk.scm 71 */
											obj_t BgL_list1566z00_1648;

											{	/* Trace/walk.scm 71 */
												obj_t BgL_arg1571z00_1649;

												{	/* Trace/walk.scm 71 */
													obj_t BgL_arg1573z00_1650;

													BgL_arg1573z00_1650 =
														MAKE_YOUNG_PAIR(BgL_arg1565z00_1647, BNIL);
													BgL_arg1571z00_1649 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
														BgL_arg1573z00_1650);
												}
												BgL_list1566z00_1648 =
													MAKE_YOUNG_PAIR(BgL_arg1564z00_1646,
													BgL_arg1571z00_1649);
											}
											BGL_TAIL return
												BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
												(BgL_list1566z00_1648);
										}
									}
								else
									{	/* Trace/walk.scm 70 */
										return
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_vz00_18)))->BgL_idz00);
									}
							}
					}
			}
		}

	}



/* trace-fun! */
	obj_t BGl_tracezd2funz12zc0zztrace_walkz00(obj_t BgL_varz00_19,
		obj_t BgL_stackz00_20, obj_t BgL_levelz00_21)
	{
		{	/* Trace/walk.scm 81 */
			{	/* Trace/walk.scm 82 */
				BgL_valuez00_bglt BgL_funz00_1655;

				BgL_funz00_1655 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_19)))->BgL_valuez00);
				{	/* Trace/walk.scm 82 */
					obj_t BgL_bodyz00_1656;

					BgL_bodyz00_1656 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1655)))->BgL_bodyz00);
					{	/* Trace/walk.scm 83 */
						obj_t BgL_llocz00_1657;

						BgL_llocz00_1657 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1655)))->BgL_locz00);
						{	/* Trace/walk.scm 84 */

							{	/* Trace/walk.scm 85 */
								bool_t BgL_test2244z00_3866;

								if (CBOOL(
										(((BgL_funz00_bglt) COBJECT(
													((BgL_funz00_bglt) BgL_funz00_1655)))->
											BgL_predicatezd2ofzd2)))
									{	/* Trace/walk.scm 85 */
										BgL_test2244z00_3866 = ((bool_t) 0);
									}
								else
									{	/* Trace/walk.scm 85 */
										if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(CNST_TABLE_REF(4),
													(((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
																	BgL_funz00_1655)))->BgL_propertyz00))))
											{	/* Trace/walk.scm 86 */
												BgL_test2244z00_3866 = ((bool_t) 0);
											}
										else
											{	/* Trace/walk.scm 87 */
												obj_t BgL_arg1646z00_1701;

												BgL_arg1646z00_1701 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_varz00_19)))->
													BgL_idz00);
												BgL_test2244z00_3866 =
													BGl_userzd2symbolzf3z21zzast_identz00
													(BgL_arg1646z00_1701);
											}
									}
								if (BgL_test2244z00_3866)
									{	/* Trace/walk.scm 85 */
										BGl_enterzd2functionzd2zztools_errorz00
											(BGl_tracezd2idzd2zztrace_walkz00(BgL_varz00_19));
										{	/* Trace/walk.scm 89 */
											obj_t BgL_bdz00_1665;

											{	/* Trace/walk.scm 89 */
												bool_t BgL_test2247z00_3882;

												if (((long) CINT(BgL_levelz00_21) > 1L))
													{	/* Trace/walk.scm 89 */
														BgL_test2247z00_3882 = ((bool_t) 1);
													}
												else
													{	/* Trace/walk.scm 90 */
														bool_t BgL_test2249z00_3886;

														{	/* Trace/walk.scm 90 */
															obj_t BgL_classz00_2524;

															BgL_classz00_2524 = BGl_globalz00zzast_varz00;
															if (BGL_OBJECTP(BgL_varz00_19))
																{	/* Trace/walk.scm 90 */
																	BgL_objectz00_bglt BgL_arg1807z00_2526;

																	BgL_arg1807z00_2526 =
																		(BgL_objectz00_bglt) (BgL_varz00_19);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Trace/walk.scm 90 */
																			long BgL_idxz00_2532;

																			BgL_idxz00_2532 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2526);
																			BgL_test2249z00_3886 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2532 + 2L)) ==
																				BgL_classz00_2524);
																		}
																	else
																		{	/* Trace/walk.scm 90 */
																			bool_t BgL_res2060z00_2557;

																			{	/* Trace/walk.scm 90 */
																				obj_t BgL_oclassz00_2540;

																				{	/* Trace/walk.scm 90 */
																					obj_t BgL_arg1815z00_2548;
																					long BgL_arg1816z00_2549;

																					BgL_arg1815z00_2548 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Trace/walk.scm 90 */
																						long BgL_arg1817z00_2550;

																						BgL_arg1817z00_2550 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2526);
																						BgL_arg1816z00_2549 =
																							(BgL_arg1817z00_2550 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2540 =
																						VECTOR_REF(BgL_arg1815z00_2548,
																						BgL_arg1816z00_2549);
																				}
																				{	/* Trace/walk.scm 90 */
																					bool_t BgL__ortest_1115z00_2541;

																					BgL__ortest_1115z00_2541 =
																						(BgL_classz00_2524 ==
																						BgL_oclassz00_2540);
																					if (BgL__ortest_1115z00_2541)
																						{	/* Trace/walk.scm 90 */
																							BgL_res2060z00_2557 =
																								BgL__ortest_1115z00_2541;
																						}
																					else
																						{	/* Trace/walk.scm 90 */
																							long BgL_odepthz00_2542;

																							{	/* Trace/walk.scm 90 */
																								obj_t BgL_arg1804z00_2543;

																								BgL_arg1804z00_2543 =
																									(BgL_oclassz00_2540);
																								BgL_odepthz00_2542 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2543);
																							}
																							if ((2L < BgL_odepthz00_2542))
																								{	/* Trace/walk.scm 90 */
																									obj_t BgL_arg1802z00_2545;

																									{	/* Trace/walk.scm 90 */
																										obj_t BgL_arg1803z00_2546;

																										BgL_arg1803z00_2546 =
																											(BgL_oclassz00_2540);
																										BgL_arg1802z00_2545 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2546, 2L);
																									}
																									BgL_res2060z00_2557 =
																										(BgL_arg1802z00_2545 ==
																										BgL_classz00_2524);
																								}
																							else
																								{	/* Trace/walk.scm 90 */
																									BgL_res2060z00_2557 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2249z00_3886 =
																				BgL_res2060z00_2557;
																		}
																}
															else
																{	/* Trace/walk.scm 90 */
																	BgL_test2249z00_3886 = ((bool_t) 0);
																}
														}
														if (BgL_test2249z00_3886)
															{	/* Trace/walk.scm 91 */
																bool_t BgL__ortest_1114z00_1694;

																BgL__ortest_1114z00_1694 =
																	(
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_varz00_19))))->BgL_idz00) ==
																	CNST_TABLE_REF(1));
																if (BgL__ortest_1114z00_1694)
																	{	/* Trace/walk.scm 91 */
																		BgL_test2247z00_3882 =
																			BgL__ortest_1114z00_1694;
																	}
																else
																	{	/* Trace/walk.scm 92 */
																		bool_t BgL__ortest_1116z00_1695;

																		BgL__ortest_1116z00_1695 =
																			(
																			(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_globalz00_bglt)
																								BgL_varz00_19))))->BgL_idz00) ==
																			CNST_TABLE_REF(5));
																		if (BgL__ortest_1116z00_1695)
																			{	/* Trace/walk.scm 92 */
																				BgL_test2247z00_3882 =
																					BgL__ortest_1116z00_1695;
																			}
																		else
																			{	/* Trace/walk.scm 92 */
																				BgL_test2247z00_3882 =
																					(
																					(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_globalz00_bglt)
																										BgL_varz00_19))))->
																						BgL_idz00) == CNST_TABLE_REF(6));
																			}
																	}
															}
														else
															{	/* Trace/walk.scm 90 */
																BgL_test2247z00_3882 = ((bool_t) 0);
															}
													}
												if (BgL_test2247z00_3882)
													{	/* Trace/walk.scm 101 */
														bool_t BgL_test2256z00_3926;

														{	/* Trace/walk.scm 101 */
															bool_t BgL_test2257z00_3927;

															{	/* Trace/walk.scm 101 */
																obj_t BgL_classz00_2561;

																BgL_classz00_2561 = BGl_localz00zzast_varz00;
																if (BGL_OBJECTP(BgL_varz00_19))
																	{	/* Trace/walk.scm 101 */
																		BgL_objectz00_bglt BgL_arg1807z00_2563;

																		BgL_arg1807z00_2563 =
																			(BgL_objectz00_bglt) (BgL_varz00_19);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Trace/walk.scm 101 */
																				long BgL_idxz00_2569;

																				BgL_idxz00_2569 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2563);
																				BgL_test2257z00_3927 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2569 + 2L)) ==
																					BgL_classz00_2561);
																			}
																		else
																			{	/* Trace/walk.scm 101 */
																				bool_t BgL_res2061z00_2594;

																				{	/* Trace/walk.scm 101 */
																					obj_t BgL_oclassz00_2577;

																					{	/* Trace/walk.scm 101 */
																						obj_t BgL_arg1815z00_2585;
																						long BgL_arg1816z00_2586;

																						BgL_arg1815z00_2585 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Trace/walk.scm 101 */
																							long BgL_arg1817z00_2587;

																							BgL_arg1817z00_2587 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2563);
																							BgL_arg1816z00_2586 =
																								(BgL_arg1817z00_2587 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2577 =
																							VECTOR_REF(BgL_arg1815z00_2585,
																							BgL_arg1816z00_2586);
																					}
																					{	/* Trace/walk.scm 101 */
																						bool_t BgL__ortest_1115z00_2578;

																						BgL__ortest_1115z00_2578 =
																							(BgL_classz00_2561 ==
																							BgL_oclassz00_2577);
																						if (BgL__ortest_1115z00_2578)
																							{	/* Trace/walk.scm 101 */
																								BgL_res2061z00_2594 =
																									BgL__ortest_1115z00_2578;
																							}
																						else
																							{	/* Trace/walk.scm 101 */
																								long BgL_odepthz00_2579;

																								{	/* Trace/walk.scm 101 */
																									obj_t BgL_arg1804z00_2580;

																									BgL_arg1804z00_2580 =
																										(BgL_oclassz00_2577);
																									BgL_odepthz00_2579 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2580);
																								}
																								if ((2L < BgL_odepthz00_2579))
																									{	/* Trace/walk.scm 101 */
																										obj_t BgL_arg1802z00_2582;

																										{	/* Trace/walk.scm 101 */
																											obj_t BgL_arg1803z00_2583;

																											BgL_arg1803z00_2583 =
																												(BgL_oclassz00_2577);
																											BgL_arg1802z00_2582 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2583,
																												2L);
																										}
																										BgL_res2061z00_2594 =
																											(BgL_arg1802z00_2582 ==
																											BgL_classz00_2561);
																									}
																								else
																									{	/* Trace/walk.scm 101 */
																										BgL_res2061z00_2594 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2257z00_3927 =
																					BgL_res2061z00_2594;
																			}
																	}
																else
																	{	/* Trace/walk.scm 101 */
																		BgL_test2257z00_3927 = ((bool_t) 0);
																	}
															}
															if (BgL_test2257z00_3927)
																{	/* Trace/walk.scm 101 */
																	BgL_test2256z00_3926 = ((bool_t) 1);
																}
															else
																{	/* Trace/walk.scm 101 */
																	if (
																		((((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_globalz00_bglt)
																								BgL_varz00_19))))->BgL_idz00) ==
																			CNST_TABLE_REF(5)))
																		{	/* Trace/walk.scm 102 */
																			BgL_test2256z00_3926 = ((bool_t) 1);
																		}
																	else
																		{	/* Trace/walk.scm 102 */
																			BgL_test2256z00_3926 =
																				(
																				(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_varz00_19))))->
																					BgL_idz00) == CNST_TABLE_REF(6));
																		}
																}
														}
														if (BgL_test2256z00_3926)
															{	/* Trace/walk.scm 104 */
																long BgL_arg1615z00_1685;

																BgL_arg1615z00_1685 =
																	((long) CINT(BgL_levelz00_21) - 1L);
																BgL_bdz00_1665 =
																	((obj_t)
																	BGl_tracezd2nodezd2zztrace_walkz00(
																		((BgL_nodez00_bglt) BgL_bodyz00_1656),
																		BgL_stackz00_20,
																		BINT(BgL_arg1615z00_1685)));
															}
														else
															{	/* Trace/walk.scm 105 */
																obj_t BgL_arg1616z00_1686;
																long BgL_arg1625z00_1687;

																BgL_arg1616z00_1686 =
																	MAKE_YOUNG_PAIR(BgL_varz00_19,
																	BgL_stackz00_20);
																BgL_arg1625z00_1687 =
																	((long) CINT(BgL_levelz00_21) - 1L);
																BgL_bdz00_1665 =
																	((obj_t)
																	BGl_tracezd2nodezd2zztrace_walkz00((
																			(BgL_nodez00_bglt) BgL_bodyz00_1656),
																		BgL_arg1616z00_1686,
																		BINT(BgL_arg1625z00_1687)));
													}}
												else
													{	/* Trace/walk.scm 89 */
														BgL_bdz00_1665 = BgL_bodyz00_1656;
													}
											}
											{	/* Trace/walk.scm 89 */
												BgL_typez00_bglt BgL_tz00_1666;

												{	/* Trace/walk.scm 107 */
													BgL_typez00_bglt BgL_arg1594z00_1669;
													BgL_typez00_bglt BgL_arg1595z00_1670;

													BgL_arg1594z00_1669 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_bodyz00_1656)))->
														BgL_typez00);
													BgL_arg1595z00_1670 =
														(((BgL_variablez00_bglt)
															COBJECT(((BgL_variablez00_bglt) BgL_varz00_19)))->
														BgL_typez00);
													BgL_tz00_1666 =
														BGl_strictzd2nodezd2typez00zzast_nodez00
														(BgL_arg1594z00_1669, BgL_arg1595z00_1670);
												}
												{	/* Trace/walk.scm 107 */
													obj_t BgL_idz00_1667;

													BgL_idz00_1667 =
														BGl_tracezd2idzd2zztrace_walkz00(BgL_varz00_19);
													{	/* Trace/walk.scm 108 */
														BgL_letzd2varzd2_bglt BgL_nbodyz00_1668;

														BgL_nbodyz00_1668 =
															BGl_makezd2tracedzd2nodez00zztrace_walkz00(
															((BgL_nodez00_bglt) BgL_bdz00_1665),
															BgL_tz00_1666, BgL_idz00_1667, BgL_llocz00_1657,
															BgL_stackz00_20);
														{	/* Trace/walk.scm 109 */

															((((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt) BgL_funz00_1655)))->
																	BgL_bodyz00) =
																((obj_t) ((obj_t) BgL_nbodyz00_1668)), BUNSPEC);
															return BGl_leavezd2functionzd2zztools_errorz00();
														}
													}
												}
											}
										}
									}
								else
									{	/* Trace/walk.scm 85 */
										return BFALSE;
									}
							}
						}
					}
				}
			}
		}

	}



/* find-last-sexp */
	obj_t BGl_findzd2lastzd2sexpz00zztrace_walkz00(BgL_nodez00_bglt
		BgL_sexpz00_22)
	{
		{	/* Trace/walk.scm 120 */
			{
				obj_t BgL_sexpz00_1704;
				obj_t BgL_resz00_1705;

				BgL_sexpz00_1704 = ((obj_t) BgL_sexpz00_22);
				BgL_resz00_1705 = ((obj_t) BgL_sexpz00_22);
			BgL_zc3z04anonymousza31651ze3z87_1706:
				if (PAIRP(BgL_sexpz00_1704))
					{	/* Trace/walk.scm 126 */
						bool_t BgL_test2264z00_3988;

						{	/* Trace/walk.scm 126 */
							obj_t BgL_tmpz00_3989;

							BgL_tmpz00_3989 = CDR(BgL_sexpz00_1704);
							BgL_test2264z00_3988 = PAIRP(BgL_tmpz00_3989);
						}
						if (BgL_test2264z00_3988)
							{
								obj_t BgL_resz00_3994;
								obj_t BgL_sexpz00_3992;

								BgL_sexpz00_3992 =
									BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
									(BgL_sexpz00_1704);
								BgL_resz00_3994 = BgL_sexpz00_1704;
								BgL_resz00_1705 = BgL_resz00_3994;
								BgL_sexpz00_1704 = BgL_sexpz00_3992;
								goto BgL_zc3z04anonymousza31651ze3z87_1706;
							}
						else
							{
								obj_t BgL_resz00_3997;
								obj_t BgL_sexpz00_3995;

								BgL_sexpz00_3995 = CAR(BgL_sexpz00_1704);
								BgL_resz00_3997 = BgL_sexpz00_1704;
								BgL_resz00_1705 = BgL_resz00_3997;
								BgL_sexpz00_1704 = BgL_sexpz00_3995;
								goto BgL_zc3z04anonymousza31651ze3z87_1706;
							}
					}
				else
					{	/* Trace/walk.scm 124 */
						return BgL_resz00_1705;
					}
			}
		}

	}



/* make-traced-node */
	BgL_letzd2varzd2_bglt
		BGl_makezd2tracedzd2nodez00zztrace_walkz00(BgL_nodez00_bglt BgL_nodez00_38,
		BgL_typez00_bglt BgL_typez00_39, obj_t BgL_symbolz00_40,
		obj_t BgL_llocz00_41, obj_t BgL_stackz00_42)
	{
		{	/* Trace/walk.scm 243 */
			{	/* Trace/walk.scm 250 */
				obj_t BgL_locz00_1715;

				BgL_locz00_1715 =
					(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_38))->BgL_locz00);
				{	/* Trace/walk.scm 250 */
					obj_t BgL_auxz00_1716;

					BgL_auxz00_1716 =
						BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
						(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(7)));
					{	/* Trace/walk.scm 251 */
						obj_t BgL_tauxz00_1717;

						BgL_tauxz00_1717 =
							BGl_makezd2typedzd2identz00zzast_identz00(BgL_auxz00_1716,
							(((BgL_typez00_bglt) COBJECT(BgL_typez00_39))->BgL_idz00));
						{	/* Trace/walk.scm 252 */
							obj_t BgL_tmp1z00_1718;

							BgL_tmp1z00_1718 =
								BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
								(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(8)));
							{	/* Trace/walk.scm 253 */
								obj_t BgL_tmp2z00_1719;

								BgL_tmp2z00_1719 =
									BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
									(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(9)));
								{	/* Trace/walk.scm 254 */
									obj_t BgL_tmp3z00_1720;

									BgL_tmp3z00_1720 =
										BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
										(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(10)));
									{	/* Trace/walk.scm 255 */
										obj_t BgL_symz00_1721;

										{	/* Trace/walk.scm 256 */
											bool_t BgL_test2265z00_4015;

											if (PAIRP(BgL_stackz00_42))
												{	/* Trace/walk.scm 256 */
													obj_t BgL_arg1770z00_1772;

													BgL_arg1770z00_1772 = CAR(BgL_stackz00_42);
													{	/* Trace/walk.scm 256 */
														obj_t BgL_classz00_2609;

														BgL_classz00_2609 = BGl_variablez00zzast_varz00;
														if (BGL_OBJECTP(BgL_arg1770z00_1772))
															{	/* Trace/walk.scm 256 */
																BgL_objectz00_bglt BgL_arg1807z00_2611;

																BgL_arg1807z00_2611 =
																	(BgL_objectz00_bglt) (BgL_arg1770z00_1772);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Trace/walk.scm 256 */
																		long BgL_idxz00_2617;

																		BgL_idxz00_2617 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2611);
																		BgL_test2265z00_4015 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2617 + 1L)) ==
																			BgL_classz00_2609);
																	}
																else
																	{	/* Trace/walk.scm 256 */
																		bool_t BgL_res2062z00_2642;

																		{	/* Trace/walk.scm 256 */
																			obj_t BgL_oclassz00_2625;

																			{	/* Trace/walk.scm 256 */
																				obj_t BgL_arg1815z00_2633;
																				long BgL_arg1816z00_2634;

																				BgL_arg1815z00_2633 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Trace/walk.scm 256 */
																					long BgL_arg1817z00_2635;

																					BgL_arg1817z00_2635 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2611);
																					BgL_arg1816z00_2634 =
																						(BgL_arg1817z00_2635 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2625 =
																					VECTOR_REF(BgL_arg1815z00_2633,
																					BgL_arg1816z00_2634);
																			}
																			{	/* Trace/walk.scm 256 */
																				bool_t BgL__ortest_1115z00_2626;

																				BgL__ortest_1115z00_2626 =
																					(BgL_classz00_2609 ==
																					BgL_oclassz00_2625);
																				if (BgL__ortest_1115z00_2626)
																					{	/* Trace/walk.scm 256 */
																						BgL_res2062z00_2642 =
																							BgL__ortest_1115z00_2626;
																					}
																				else
																					{	/* Trace/walk.scm 256 */
																						long BgL_odepthz00_2627;

																						{	/* Trace/walk.scm 256 */
																							obj_t BgL_arg1804z00_2628;

																							BgL_arg1804z00_2628 =
																								(BgL_oclassz00_2625);
																							BgL_odepthz00_2627 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2628);
																						}
																						if ((1L < BgL_odepthz00_2627))
																							{	/* Trace/walk.scm 256 */
																								obj_t BgL_arg1802z00_2630;

																								{	/* Trace/walk.scm 256 */
																									obj_t BgL_arg1803z00_2631;

																									BgL_arg1803z00_2631 =
																										(BgL_oclassz00_2625);
																									BgL_arg1802z00_2630 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2631, 1L);
																								}
																								BgL_res2062z00_2642 =
																									(BgL_arg1802z00_2630 ==
																									BgL_classz00_2609);
																							}
																						else
																							{	/* Trace/walk.scm 256 */
																								BgL_res2062z00_2642 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2265z00_4015 = BgL_res2062z00_2642;
																	}
															}
														else
															{	/* Trace/walk.scm 256 */
																BgL_test2265z00_4015 = ((bool_t) 0);
															}
													}
												}
											else
												{	/* Trace/walk.scm 256 */
													BgL_test2265z00_4015 = ((bool_t) 0);
												}
											if (BgL_test2265z00_4015)
												{	/* Trace/walk.scm 256 */
													BgL_symz00_1721 =
														BGl_fqnameze70ze7zztrace_walkz00(BgL_symbolz00_40,
														BgL_stackz00_42);
												}
											else
												{	/* Trace/walk.scm 256 */
													BgL_symz00_1721 = BgL_symbolz00_40;
												}
										}
										{	/* Trace/walk.scm 256 */
											obj_t BgL_lz00_1722;

											{	/* Trace/walk.scm 260 */
												bool_t BgL_test2271z00_4042;

												if (STRUCTP(BgL_locz00_1715))
													{	/* Trace/walk.scm 260 */
														BgL_test2271z00_4042 =
															(STRUCT_KEY(BgL_locz00_1715) ==
															CNST_TABLE_REF(11));
													}
												else
													{	/* Trace/walk.scm 260 */
														BgL_test2271z00_4042 = ((bool_t) 0);
													}
												if (BgL_test2271z00_4042)
													{	/* Trace/walk.scm 261 */
														obj_t BgL_arg1755z00_1764;

														{	/* Trace/walk.scm 261 */
															obj_t BgL_arg1761z00_1765;
															obj_t BgL_arg1762z00_1766;

															BgL_arg1761z00_1765 =
																BGl_locationzd2fullzd2fnamez00zztools_locationz00
																(BgL_locz00_1715);
															BgL_arg1762z00_1766 =
																MAKE_YOUNG_PAIR(STRUCT_REF(BgL_locz00_1715,
																	(int) (1L)), BNIL);
															BgL_arg1755z00_1764 =
																MAKE_YOUNG_PAIR(BgL_arg1761z00_1765,
																BgL_arg1762z00_1766);
														}
														BgL_lz00_1722 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
															BgL_arg1755z00_1764);
													}
												else
													{	/* Trace/walk.scm 260 */
														BgL_lz00_1722 = BFALSE;
													}
											}
											{	/* Trace/walk.scm 260 */
												obj_t BgL_expz00_1723;

												{	/* Trace/walk.scm 262 */
													obj_t BgL_arg1688z00_1727;

													{	/* Trace/walk.scm 262 */
														obj_t BgL_arg1689z00_1728;
														obj_t BgL_arg1691z00_1729;

														{	/* Trace/walk.scm 262 */
															obj_t BgL_arg1692z00_1730;
															obj_t BgL_arg1699z00_1731;

															{	/* Trace/walk.scm 262 */
																obj_t BgL_arg1700z00_1732;

																{	/* Trace/walk.scm 262 */
																	obj_t BgL_arg1701z00_1733;

																	{	/* Trace/walk.scm 262 */
																		obj_t BgL_arg1702z00_1734;

																		BgL_arg1702z00_1734 =
																			MAKE_YOUNG_PAIR(BgL_symz00_1721, BNIL);
																		BgL_arg1701z00_1733 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																			BgL_arg1702z00_1734);
																	}
																	BgL_arg1700z00_1732 =
																		MAKE_YOUNG_PAIR(BgL_arg1701z00_1733, BNIL);
																}
																BgL_arg1692z00_1730 =
																	MAKE_YOUNG_PAIR(BgL_tmp1z00_1718,
																	BgL_arg1700z00_1732);
															}
															{	/* Trace/walk.scm 263 */
																obj_t BgL_arg1703z00_1735;
																obj_t BgL_arg1705z00_1736;

																{	/* Trace/walk.scm 263 */
																	obj_t BgL_arg1708z00_1737;

																	{	/* Trace/walk.scm 263 */
																		obj_t BgL_arg1709z00_1738;

																		{	/* Trace/walk.scm 263 */
																			obj_t BgL_arg1710z00_1739;

																			BgL_arg1710z00_1739 =
																				MAKE_YOUNG_PAIR(BgL_lz00_1722, BNIL);
																			BgL_arg1709z00_1738 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																				BgL_arg1710z00_1739);
																		}
																		BgL_arg1708z00_1737 =
																			MAKE_YOUNG_PAIR(BgL_arg1709z00_1738,
																			BNIL);
																	}
																	BgL_arg1703z00_1735 =
																		MAKE_YOUNG_PAIR(BgL_tmp2z00_1719,
																		BgL_arg1708z00_1737);
																}
																{	/* Trace/walk.scm 264 */
																	obj_t BgL_arg1711z00_1740;

																	{	/* Trace/walk.scm 264 */
																		obj_t BgL_arg1714z00_1741;
																		obj_t BgL_arg1717z00_1742;

																		BgL_arg1714z00_1741 =
																			BGl_makezd2typedzd2identz00zzast_identz00
																			(BgL_tmp3z00_1720, CNST_TABLE_REF(14));
																		{	/* Trace/walk.scm 264 */
																			obj_t BgL_arg1718z00_1743;

																			BgL_arg1718z00_1743 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																				BNIL);
																			BgL_arg1717z00_1742 =
																				MAKE_YOUNG_PAIR(BgL_arg1718z00_1743,
																				BNIL);
																		}
																		BgL_arg1711z00_1740 =
																			MAKE_YOUNG_PAIR(BgL_arg1714z00_1741,
																			BgL_arg1717z00_1742);
																	}
																	BgL_arg1705z00_1736 =
																		MAKE_YOUNG_PAIR(BgL_arg1711z00_1740, BNIL);
																}
																BgL_arg1699z00_1731 =
																	MAKE_YOUNG_PAIR(BgL_arg1703z00_1735,
																	BgL_arg1705z00_1736);
															}
															BgL_arg1689z00_1728 =
																MAKE_YOUNG_PAIR(BgL_arg1692z00_1730,
																BgL_arg1699z00_1731);
														}
														{	/* Trace/walk.scm 266 */
															obj_t BgL_arg1720z00_1744;

															{	/* Trace/walk.scm 266 */
																obj_t BgL_arg1722z00_1745;

																{	/* Trace/walk.scm 266 */
																	obj_t BgL_arg1724z00_1746;

																	{	/* Trace/walk.scm 266 */
																		obj_t BgL_arg1733z00_1747;
																		obj_t BgL_arg1734z00_1748;

																		{	/* Trace/walk.scm 266 */
																			obj_t BgL_arg1735z00_1749;

																			{	/* Trace/walk.scm 266 */
																				obj_t BgL_arg1736z00_1750;

																				{	/* Trace/walk.scm 266 */
																					obj_t BgL_arg1737z00_1751;

																					BgL_arg1737z00_1751 =
																						MAKE_YOUNG_PAIR(BgL_tmp2z00_1719,
																						BNIL);
																					BgL_arg1736z00_1750 =
																						MAKE_YOUNG_PAIR(BgL_tmp1z00_1718,
																						BgL_arg1737z00_1751);
																				}
																				BgL_arg1735z00_1749 =
																					MAKE_YOUNG_PAIR(BgL_tmp3z00_1720,
																					BgL_arg1736z00_1750);
																			}
																			BgL_arg1733z00_1747 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																				BgL_arg1735z00_1749);
																		}
																		{	/* Trace/walk.scm 267 */
																			obj_t BgL_arg1738z00_1752;

																			{	/* Trace/walk.scm 267 */
																				obj_t BgL_arg1739z00_1753;

																				{	/* Trace/walk.scm 267 */
																					obj_t BgL_arg1740z00_1754;
																					obj_t BgL_arg1746z00_1755;

																					{	/* Trace/walk.scm 267 */
																						obj_t BgL_arg1747z00_1756;

																						{	/* Trace/walk.scm 267 */
																							obj_t BgL_arg1748z00_1757;

																							BgL_arg1748z00_1757 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_nodez00_38), BNIL);
																							BgL_arg1747z00_1756 =
																								MAKE_YOUNG_PAIR
																								(BgL_tauxz00_1717,
																								BgL_arg1748z00_1757);
																						}
																						BgL_arg1740z00_1754 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1747z00_1756, BNIL);
																					}
																					{	/* Trace/walk.scm 268 */
																						obj_t BgL_arg1749z00_1758;
																						obj_t BgL_arg1750z00_1759;

																						{	/* Trace/walk.scm 268 */
																							bool_t BgL_test2273z00_4083;

																							if (STRUCTP(BgL_llocz00_41))
																								{	/* Trace/walk.scm 268 */
																									BgL_test2273z00_4083 =
																										(STRUCT_KEY(BgL_llocz00_41)
																										== CNST_TABLE_REF(11));
																								}
																							else
																								{	/* Trace/walk.scm 268 */
																									BgL_test2273z00_4083 =
																										((bool_t) 0);
																								}
																							if (BgL_test2273z00_4083)
																								{	/* Trace/walk.scm 270 */
																									obj_t BgL_arg1752z00_1761;

																									{	/* Trace/walk.scm 270 */
																										obj_t BgL_res2065z00_2652;

																										BgL_res2065z00_2652 =
																											MAKE_YOUNG_EPAIR
																											(BgL_tmp3z00_1720, BNIL,
																											BgL_llocz00_41);
																										BgL_arg1752z00_1761 =
																											BgL_res2065z00_2652;
																									}
																									{	/* Trace/walk.scm 269 */
																										obj_t BgL_res2066z00_2654;

																										{	/* Trace/walk.scm 269 */
																											obj_t BgL_obj1z00_2653;

																											BgL_obj1z00_2653 =
																												CNST_TABLE_REF(17);
																											BgL_res2066z00_2654 =
																												MAKE_YOUNG_EPAIR
																												(BgL_obj1z00_2653,
																												BgL_arg1752z00_1761,
																												BgL_llocz00_41);
																										}
																										BgL_arg1749z00_1758 =
																											BgL_res2066z00_2654;
																									}
																								}
																							else
																								{	/* Trace/walk.scm 272 */
																									obj_t BgL_arg1753z00_1762;

																									BgL_arg1753z00_1762 =
																										MAKE_YOUNG_PAIR
																										(BgL_tmp3z00_1720, BNIL);
																									BgL_arg1749z00_1758 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(17),
																										BgL_arg1753z00_1762);
																								}
																						}
																						BgL_arg1750z00_1759 =
																							MAKE_YOUNG_PAIR(BgL_auxz00_1716,
																							BNIL);
																						BgL_arg1746z00_1755 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1749z00_1758,
																							BgL_arg1750z00_1759);
																					}
																					BgL_arg1739z00_1753 =
																						MAKE_YOUNG_PAIR(BgL_arg1740z00_1754,
																						BgL_arg1746z00_1755);
																				}
																				BgL_arg1738z00_1752 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																					BgL_arg1739z00_1753);
																			}
																			BgL_arg1734z00_1748 =
																				MAKE_YOUNG_PAIR(BgL_arg1738z00_1752,
																				BNIL);
																		}
																		BgL_arg1724z00_1746 =
																			MAKE_YOUNG_PAIR(BgL_arg1733z00_1747,
																			BgL_arg1734z00_1748);
																	}
																	BgL_arg1722z00_1745 =
																		MAKE_YOUNG_PAIR(BNIL, BgL_arg1724z00_1746);
																}
																BgL_arg1720z00_1744 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																	BgL_arg1722z00_1745);
															}
															BgL_arg1691z00_1729 =
																MAKE_YOUNG_PAIR(BgL_arg1720z00_1744, BNIL);
														}
														BgL_arg1688z00_1727 =
															MAKE_YOUNG_PAIR(BgL_arg1689z00_1728,
															BgL_arg1691z00_1729);
													}
													BgL_expz00_1723 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
														BgL_arg1688z00_1727);
												}
												{	/* Trace/walk.scm 262 */
													BgL_nodez00_bglt BgL_nnodez00_1724;

													BgL_nnodez00_1724 =
														BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_expz00_1723,
														BNIL, BgL_locz00_1715, CNST_TABLE_REF(19));
													{	/* Trace/walk.scm 274 */

														{	/* Trace/walk.scm 275 */
															bool_t BgL_arg1678z00_1725;

															{	/* Trace/walk.scm 275 */
																obj_t BgL_arg1681z00_1726;

																BgL_arg1681z00_1726 =
																	BGl_thezd2backendzd2zzbackend_backendz00();
																BgL_arg1678z00_1725 =
																	(((BgL_backendz00_bglt) COBJECT(
																			((BgL_backendz00_bglt)
																				BgL_arg1681z00_1726)))->
																	BgL_removezd2emptyzd2letz00);
															}
															((((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_nnodez00_1724)))->
																	BgL_removablezf3zf3) =
																((bool_t) BgL_arg1678z00_1725), BUNSPEC);
														}
														return ((BgL_letzd2varzd2_bglt) BgL_nnodez00_1724);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* fqname~0 */
	obj_t BGl_fqnameze70ze7zztrace_walkz00(obj_t BgL_symz00_1778,
		obj_t BgL_stackz00_1779)
	{
		{	/* Trace/walk.scm 248 */
			if (NULLP(BgL_stackz00_1779))
				{	/* Trace/walk.scm 246 */
					return BgL_symz00_1778;
				}
			else
				{	/* Trace/walk.scm 248 */
					obj_t BgL_arg1805z00_1782;

					{	/* Trace/walk.scm 248 */
						obj_t BgL_arg1820z00_1786;
						obj_t BgL_arg1822z00_1787;

						{	/* Trace/walk.scm 248 */
							obj_t BgL_arg1823z00_1788;

							BgL_arg1823z00_1788 = CAR(((obj_t) BgL_stackz00_1779));
							BgL_arg1820z00_1786 =
								BGl_tracezd2idzd2zztrace_walkz00(BgL_arg1823z00_1788);
						}
						BgL_arg1822z00_1787 = CDR(((obj_t) BgL_stackz00_1779));
						BgL_arg1805z00_1782 =
							BGl_fqnameze70ze7zztrace_walkz00(BgL_arg1820z00_1786,
							BgL_arg1822z00_1787);
					}
					{	/* Trace/walk.scm 248 */
						obj_t BgL_list1806z00_1783;

						{	/* Trace/walk.scm 248 */
							obj_t BgL_arg1808z00_1784;

							{	/* Trace/walk.scm 248 */
								obj_t BgL_arg1812z00_1785;

								BgL_arg1812z00_1785 =
									MAKE_YOUNG_PAIR(BgL_arg1805z00_1782, BNIL);
								BgL_arg1808z00_1784 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg1812z00_1785);
							}
							BgL_list1806z00_1783 =
								MAKE_YOUNG_PAIR(BgL_symz00_1778, BgL_arg1808z00_1784);
						}
						return
							BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1806z00_1783);
					}
				}
		}

	}



/* trace-node*! */
	bool_t BGl_tracezd2nodeza2z12z62zztrace_walkz00(obj_t BgL_nodeza2za2_100,
		obj_t BgL_stackz00_101, obj_t BgL_levelz00_102)
	{
		{	/* Trace/walk.scm 448 */
		BGl_tracezd2nodeza2z12z62zztrace_walkz00:
			if (NULLP(BgL_nodeza2za2_100))
				{	/* Trace/walk.scm 449 */
					return ((bool_t) 0);
				}
			else
				{	/* Trace/walk.scm 449 */
					{	/* Trace/walk.scm 450 */
						BgL_nodez00_bglt BgL_arg1831z00_1791;

						{	/* Trace/walk.scm 450 */
							obj_t BgL_arg1832z00_1792;

							BgL_arg1832z00_1792 = CAR(((obj_t) BgL_nodeza2za2_100));
							BgL_arg1831z00_1791 =
								BGl_tracezd2nodezd2zztrace_walkz00(
								((BgL_nodez00_bglt) BgL_arg1832z00_1792), BgL_stackz00_101,
								BgL_levelz00_102);
						}
						{	/* Trace/walk.scm 450 */
							obj_t BgL_auxz00_4138;
							obj_t BgL_tmpz00_4136;

							BgL_auxz00_4138 = ((obj_t) BgL_arg1831z00_1791);
							BgL_tmpz00_4136 = ((obj_t) BgL_nodeza2za2_100);
							SET_CAR(BgL_tmpz00_4136, BgL_auxz00_4138);
						}
					}
					{	/* Trace/walk.scm 451 */
						obj_t BgL_arg1833z00_1793;

						BgL_arg1833z00_1793 = CDR(((obj_t) BgL_nodeza2za2_100));
						{
							obj_t BgL_nodeza2za2_4143;

							BgL_nodeza2za2_4143 = BgL_arg1833z00_1793;
							BgL_nodeza2za2_100 = BgL_nodeza2za2_4143;
							goto BGl_tracezd2nodeza2z12z62zztrace_walkz00;
						}
					}
				}
		}

	}



/* toplevel-trace-node*! */
	bool_t BGl_toplevelzd2tracezd2nodeza2z12zb0zztrace_walkz00(obj_t
		BgL_nodeza2za2_107)
	{
		{	/* Trace/walk.scm 499 */
		BGl_toplevelzd2tracezd2nodeza2z12zb0zztrace_walkz00:
			if (NULLP(BgL_nodeza2za2_107))
				{	/* Trace/walk.scm 500 */
					return ((bool_t) 0);
				}
			else
				{	/* Trace/walk.scm 500 */
					{	/* Trace/walk.scm 501 */
						BgL_nodez00_bglt BgL_arg1835z00_1795;

						{	/* Trace/walk.scm 501 */
							obj_t BgL_arg1836z00_1796;

							BgL_arg1836z00_1796 = CAR(((obj_t) BgL_nodeza2za2_107));
							BgL_arg1835z00_1795 =
								BGl_toplevelzd2tracezd2nodez00zztrace_walkz00(
								((BgL_nodez00_bglt) BgL_arg1836z00_1796));
						}
						{	/* Trace/walk.scm 501 */
							obj_t BgL_auxz00_4152;
							obj_t BgL_tmpz00_4150;

							BgL_auxz00_4152 = ((obj_t) BgL_arg1835z00_1795);
							BgL_tmpz00_4150 = ((obj_t) BgL_nodeza2za2_107);
							SET_CAR(BgL_tmpz00_4150, BgL_auxz00_4152);
						}
					}
					{	/* Trace/walk.scm 502 */
						obj_t BgL_arg1837z00_1797;

						BgL_arg1837z00_1797 = CDR(((obj_t) BgL_nodeza2za2_107));
						{
							obj_t BgL_nodeza2za2_4157;

							BgL_nodeza2za2_4157 = BgL_arg1837z00_1797;
							BgL_nodeza2za2_107 = BgL_nodeza2za2_4157;
							goto BGl_toplevelzd2tracezd2nodeza2z12zb0zztrace_walkz00;
						}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztrace_walkz00(void)
	{
		{	/* Trace/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztrace_walkz00(void)
	{
		{	/* Trace/walk.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_proc2104z00zztrace_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2105z00zztrace_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00,
				BGl_proc2106z00zztrace_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2107z00zztrace_walkz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_toplevelzd2tracezd2nodezd2envzd2zztrace_walkz00,
				BGl_proc2108z00zztrace_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2109z00zztrace_walkz00);
		}

	}



/* &toplevel-trace-node1332 */
	obj_t BGl_z62toplevelzd2tracezd2node1332z62zztrace_walkz00(obj_t
		BgL_envz00_3251, obj_t BgL_nodez00_3252)
	{
		{	/* Trace/walk.scm 456 */
			return ((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3252));
		}

	}



/* &trace-node1294 */
	obj_t BGl_z62tracezd2node1294zb0zztrace_walkz00(obj_t BgL_envz00_3253,
		obj_t BgL_nodez00_3254, obj_t BgL_stackz00_3255, obj_t BgL_levelz00_3256)
	{
		{	/* Trace/walk.scm 281 */
			return ((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3254));
		}

	}



/* &find-last-node1264 */
	obj_t BGl_z62findzd2lastzd2node1264z62zztrace_walkz00(obj_t BgL_envz00_3257,
		obj_t BgL_nodez00_3258)
	{
		{	/* Trace/walk.scm 137 */
			return ((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3258));
		}

	}



/* find-last-node */
	obj_t BGl_findzd2lastzd2nodez00zztrace_walkz00(BgL_nodez00_bglt
		BgL_nodez00_23)
	{
		{	/* Trace/walk.scm 137 */
			{	/* Trace/walk.scm 137 */
				obj_t BgL_method1265z00_1812;

				{	/* Trace/walk.scm 137 */
					obj_t BgL_res2071z00_2694;

					{	/* Trace/walk.scm 137 */
						long BgL_objzd2classzd2numz00_2665;

						BgL_objzd2classzd2numz00_2665 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_23));
						{	/* Trace/walk.scm 137 */
							obj_t BgL_arg1811z00_2666;

							BgL_arg1811z00_2666 =
								PROCEDURE_REF(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
								(int) (1L));
							{	/* Trace/walk.scm 137 */
								int BgL_offsetz00_2669;

								BgL_offsetz00_2669 = (int) (BgL_objzd2classzd2numz00_2665);
								{	/* Trace/walk.scm 137 */
									long BgL_offsetz00_2670;

									BgL_offsetz00_2670 =
										((long) (BgL_offsetz00_2669) - OBJECT_TYPE);
									{	/* Trace/walk.scm 137 */
										long BgL_modz00_2671;

										BgL_modz00_2671 =
											(BgL_offsetz00_2670 >> (int) ((long) ((int) (4L))));
										{	/* Trace/walk.scm 137 */
											long BgL_restz00_2673;

											BgL_restz00_2673 =
												(BgL_offsetz00_2670 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Trace/walk.scm 137 */

												{	/* Trace/walk.scm 137 */
													obj_t BgL_bucketz00_2675;

													BgL_bucketz00_2675 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2666), BgL_modz00_2671);
													BgL_res2071z00_2694 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2675), BgL_restz00_2673);
					}}}}}}}}
					BgL_method1265z00_1812 = BgL_res2071z00_2694;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1265z00_1812, ((obj_t) BgL_nodez00_23));
			}
		}

	}



/* &find-last-node */
	obj_t BGl_z62findzd2lastzd2nodez62zztrace_walkz00(obj_t BgL_envz00_3259,
		obj_t BgL_nodez00_3260)
	{
		{	/* Trace/walk.scm 137 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3260));
		}

	}



/* trace-node */
	BgL_nodez00_bglt BGl_tracezd2nodezd2zztrace_walkz00(BgL_nodez00_bglt
		BgL_nodez00_43, obj_t BgL_stackz00_44, obj_t BgL_levelz00_45)
	{
		{	/* Trace/walk.scm 281 */
			{	/* Trace/walk.scm 281 */
				obj_t BgL_method1295z00_1813;

				{	/* Trace/walk.scm 281 */
					obj_t BgL_res2076z00_2725;

					{	/* Trace/walk.scm 281 */
						long BgL_objzd2classzd2numz00_2696;

						BgL_objzd2classzd2numz00_2696 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_43));
						{	/* Trace/walk.scm 281 */
							obj_t BgL_arg1811z00_2697;

							BgL_arg1811z00_2697 =
								PROCEDURE_REF(BGl_tracezd2nodezd2envz00zztrace_walkz00,
								(int) (1L));
							{	/* Trace/walk.scm 281 */
								int BgL_offsetz00_2700;

								BgL_offsetz00_2700 = (int) (BgL_objzd2classzd2numz00_2696);
								{	/* Trace/walk.scm 281 */
									long BgL_offsetz00_2701;

									BgL_offsetz00_2701 =
										((long) (BgL_offsetz00_2700) - OBJECT_TYPE);
									{	/* Trace/walk.scm 281 */
										long BgL_modz00_2702;

										BgL_modz00_2702 =
											(BgL_offsetz00_2701 >> (int) ((long) ((int) (4L))));
										{	/* Trace/walk.scm 281 */
											long BgL_restz00_2704;

											BgL_restz00_2704 =
												(BgL_offsetz00_2701 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Trace/walk.scm 281 */

												{	/* Trace/walk.scm 281 */
													obj_t BgL_bucketz00_2706;

													BgL_bucketz00_2706 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2697), BgL_modz00_2702);
													BgL_res2076z00_2725 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2706), BgL_restz00_2704);
					}}}}}}}}
					BgL_method1295z00_1813 = BgL_res2076z00_2725;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL3(BgL_method1295z00_1813,
						((obj_t) BgL_nodez00_43), BgL_stackz00_44, BgL_levelz00_45));
			}
		}

	}



/* &trace-node */
	BgL_nodez00_bglt BGl_z62tracezd2nodezb0zztrace_walkz00(obj_t BgL_envz00_3261,
		obj_t BgL_nodez00_3262, obj_t BgL_stackz00_3263, obj_t BgL_levelz00_3264)
	{
		{	/* Trace/walk.scm 281 */
			return
				BGl_tracezd2nodezd2zztrace_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3262), BgL_stackz00_3263,
				BgL_levelz00_3264);
		}

	}



/* toplevel-trace-node */
	BgL_nodez00_bglt
		BGl_toplevelzd2tracezd2nodez00zztrace_walkz00(BgL_nodez00_bglt
		BgL_nodez00_103)
	{
		{	/* Trace/walk.scm 456 */
			{	/* Trace/walk.scm 456 */
				obj_t BgL_method1333z00_1814;

				{	/* Trace/walk.scm 456 */
					obj_t BgL_res2081z00_2756;

					{	/* Trace/walk.scm 456 */
						long BgL_objzd2classzd2numz00_2727;

						BgL_objzd2classzd2numz00_2727 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_103));
						{	/* Trace/walk.scm 456 */
							obj_t BgL_arg1811z00_2728;

							BgL_arg1811z00_2728 =
								PROCEDURE_REF
								(BGl_toplevelzd2tracezd2nodezd2envzd2zztrace_walkz00,
								(int) (1L));
							{	/* Trace/walk.scm 456 */
								int BgL_offsetz00_2731;

								BgL_offsetz00_2731 = (int) (BgL_objzd2classzd2numz00_2727);
								{	/* Trace/walk.scm 456 */
									long BgL_offsetz00_2732;

									BgL_offsetz00_2732 =
										((long) (BgL_offsetz00_2731) - OBJECT_TYPE);
									{	/* Trace/walk.scm 456 */
										long BgL_modz00_2733;

										BgL_modz00_2733 =
											(BgL_offsetz00_2732 >> (int) ((long) ((int) (4L))));
										{	/* Trace/walk.scm 456 */
											long BgL_restz00_2735;

											BgL_restz00_2735 =
												(BgL_offsetz00_2732 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Trace/walk.scm 456 */

												{	/* Trace/walk.scm 456 */
													obj_t BgL_bucketz00_2737;

													BgL_bucketz00_2737 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2728), BgL_modz00_2733);
													BgL_res2081z00_2756 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2737), BgL_restz00_2735);
					}}}}}}}}
					BgL_method1333z00_1814 = BgL_res2081z00_2756;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1333z00_1814,
						((obj_t) BgL_nodez00_103)));
			}
		}

	}



/* &toplevel-trace-node */
	BgL_nodez00_bglt BGl_z62toplevelzd2tracezd2nodez62zztrace_walkz00(obj_t
		BgL_envz00_3265, obj_t BgL_nodez00_3266)
	{
		{	/* Trace/walk.scm 456 */
			return
				BGl_toplevelzd2tracezd2nodez00zztrace_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3266));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztrace_walkz00(void)
	{
		{	/* Trace/walk.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2110z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_syncz00zzast_nodez00, BGl_proc2112z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_appz00zzast_nodez00, BGl_proc2113z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2114z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_funcallz00zzast_nodez00, BGl_proc2115z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_externz00zzast_nodez00, BGl_proc2116z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_setqz00zzast_nodez00, BGl_proc2117z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2118z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_failz00zzast_nodez00, BGl_proc2119z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_switchz00zzast_nodez00, BGl_proc2120z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2121z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2122z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2123z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2lastzd2nodezd2envzd2zztrace_walkz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2124z00zztrace_walkz00,
				BGl_string2111z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc2125z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc2127z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2128z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc2129z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc2130z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc2131z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc2132z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc2133z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2134z00zztrace_walkz00,
				BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc2135z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc2136z00zztrace_walkz00, BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2137z00zztrace_walkz00,
				BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2138z00zztrace_walkz00,
				BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2139z00zztrace_walkz00,
				BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2140z00zztrace_walkz00,
				BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2141z00zztrace_walkz00,
				BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2142z00zztrace_walkz00,
				BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tracezd2nodezd2envz00zztrace_walkz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2143z00zztrace_walkz00,
				BGl_string2126z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_toplevelzd2tracezd2nodezd2envzd2zztrace_walkz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2144z00zztrace_walkz00,
				BGl_string2145z00zztrace_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_toplevelzd2tracezd2nodezd2envzd2zztrace_walkz00,
				BGl_syncz00zzast_nodez00, BGl_proc2146z00zztrace_walkz00,
				BGl_string2145z00zztrace_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_toplevelzd2tracezd2nodezd2envzd2zztrace_walkz00,
				BGl_setqz00zzast_nodez00, BGl_proc2147z00zztrace_walkz00,
				BGl_string2145z00zztrace_walkz00);
		}

	}



/* &toplevel-trace-node-1340 */
	BgL_nodez00_bglt BGl_z62toplevelzd2tracezd2nodezd21340zb0zztrace_walkz00(obj_t
		BgL_envz00_3302, obj_t BgL_nodez00_3303)
	{
		{	/* Trace/walk.scm 483 */
			{	/* Trace/walk.scm 485 */
				BgL_varz00_bglt BgL_i1135z00_3424;

				BgL_i1135z00_3424 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_3303)))->BgL_varz00);
				{	/* Trace/walk.scm 486 */
					bool_t BgL_test2278z00_4304;

					{	/* Trace/walk.scm 486 */
						bool_t BgL_test2279z00_4305;

						{	/* Trace/walk.scm 486 */
							BgL_variablez00_bglt BgL_arg1991z00_3425;

							BgL_arg1991z00_3425 =
								(((BgL_varz00_bglt) COBJECT(BgL_i1135z00_3424))->
								BgL_variablez00);
							{	/* Trace/walk.scm 486 */
								obj_t BgL_classz00_3426;

								BgL_classz00_3426 = BGl_globalz00zzast_varz00;
								{	/* Trace/walk.scm 486 */
									BgL_objectz00_bglt BgL_arg1807z00_3427;

									{	/* Trace/walk.scm 486 */
										obj_t BgL_tmpz00_4307;

										BgL_tmpz00_4307 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1991z00_3425));
										BgL_arg1807z00_3427 =
											(BgL_objectz00_bglt) (BgL_tmpz00_4307);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Trace/walk.scm 486 */
											long BgL_idxz00_3428;

											BgL_idxz00_3428 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3427);
											BgL_test2279z00_4305 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3428 + 2L)) == BgL_classz00_3426);
										}
									else
										{	/* Trace/walk.scm 486 */
											bool_t BgL_res2082z00_3431;

											{	/* Trace/walk.scm 486 */
												obj_t BgL_oclassz00_3432;

												{	/* Trace/walk.scm 486 */
													obj_t BgL_arg1815z00_3433;
													long BgL_arg1816z00_3434;

													BgL_arg1815z00_3433 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Trace/walk.scm 486 */
														long BgL_arg1817z00_3435;

														BgL_arg1817z00_3435 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3427);
														BgL_arg1816z00_3434 =
															(BgL_arg1817z00_3435 - OBJECT_TYPE);
													}
													BgL_oclassz00_3432 =
														VECTOR_REF(BgL_arg1815z00_3433,
														BgL_arg1816z00_3434);
												}
												{	/* Trace/walk.scm 486 */
													bool_t BgL__ortest_1115z00_3436;

													BgL__ortest_1115z00_3436 =
														(BgL_classz00_3426 == BgL_oclassz00_3432);
													if (BgL__ortest_1115z00_3436)
														{	/* Trace/walk.scm 486 */
															BgL_res2082z00_3431 = BgL__ortest_1115z00_3436;
														}
													else
														{	/* Trace/walk.scm 486 */
															long BgL_odepthz00_3437;

															{	/* Trace/walk.scm 486 */
																obj_t BgL_arg1804z00_3438;

																BgL_arg1804z00_3438 = (BgL_oclassz00_3432);
																BgL_odepthz00_3437 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3438);
															}
															if ((2L < BgL_odepthz00_3437))
																{	/* Trace/walk.scm 486 */
																	obj_t BgL_arg1802z00_3439;

																	{	/* Trace/walk.scm 486 */
																		obj_t BgL_arg1803z00_3440;

																		BgL_arg1803z00_3440 = (BgL_oclassz00_3432);
																		BgL_arg1802z00_3439 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3440, 2L);
																	}
																	BgL_res2082z00_3431 =
																		(BgL_arg1802z00_3439 == BgL_classz00_3426);
																}
															else
																{	/* Trace/walk.scm 486 */
																	BgL_res2082z00_3431 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2279z00_4305 = BgL_res2082z00_3431;
										}
								}
							}
						}
						if (BgL_test2279z00_4305)
							{	/* Trace/walk.scm 487 */
								bool_t BgL_test2283z00_4330;

								{	/* Trace/walk.scm 487 */
									bool_t BgL_test2284z00_4331;

									{	/* Trace/walk.scm 487 */
										BgL_nodez00_bglt BgL_arg1990z00_3441;

										BgL_arg1990z00_3441 =
											(((BgL_setqz00_bglt) COBJECT(
													((BgL_setqz00_bglt) BgL_nodez00_3303)))->
											BgL_valuez00);
										{	/* Trace/walk.scm 487 */
											obj_t BgL_classz00_3442;

											BgL_classz00_3442 = BGl_atomz00zzast_nodez00;
											{	/* Trace/walk.scm 487 */
												BgL_objectz00_bglt BgL_arg1807z00_3443;

												{	/* Trace/walk.scm 487 */
													obj_t BgL_tmpz00_4334;

													BgL_tmpz00_4334 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1990z00_3441));
													BgL_arg1807z00_3443 =
														(BgL_objectz00_bglt) (BgL_tmpz00_4334);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Trace/walk.scm 487 */
														long BgL_idxz00_3444;

														BgL_idxz00_3444 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3443);
														BgL_test2284z00_4331 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3444 + 2L)) == BgL_classz00_3442);
													}
												else
													{	/* Trace/walk.scm 487 */
														bool_t BgL_res2083z00_3447;

														{	/* Trace/walk.scm 487 */
															obj_t BgL_oclassz00_3448;

															{	/* Trace/walk.scm 487 */
																obj_t BgL_arg1815z00_3449;
																long BgL_arg1816z00_3450;

																BgL_arg1815z00_3449 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Trace/walk.scm 487 */
																	long BgL_arg1817z00_3451;

																	BgL_arg1817z00_3451 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3443);
																	BgL_arg1816z00_3450 =
																		(BgL_arg1817z00_3451 - OBJECT_TYPE);
																}
																BgL_oclassz00_3448 =
																	VECTOR_REF(BgL_arg1815z00_3449,
																	BgL_arg1816z00_3450);
															}
															{	/* Trace/walk.scm 487 */
																bool_t BgL__ortest_1115z00_3452;

																BgL__ortest_1115z00_3452 =
																	(BgL_classz00_3442 == BgL_oclassz00_3448);
																if (BgL__ortest_1115z00_3452)
																	{	/* Trace/walk.scm 487 */
																		BgL_res2083z00_3447 =
																			BgL__ortest_1115z00_3452;
																	}
																else
																	{	/* Trace/walk.scm 487 */
																		long BgL_odepthz00_3453;

																		{	/* Trace/walk.scm 487 */
																			obj_t BgL_arg1804z00_3454;

																			BgL_arg1804z00_3454 =
																				(BgL_oclassz00_3448);
																			BgL_odepthz00_3453 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3454);
																		}
																		if ((2L < BgL_odepthz00_3453))
																			{	/* Trace/walk.scm 487 */
																				obj_t BgL_arg1802z00_3455;

																				{	/* Trace/walk.scm 487 */
																					obj_t BgL_arg1803z00_3456;

																					BgL_arg1803z00_3456 =
																						(BgL_oclassz00_3448);
																					BgL_arg1802z00_3455 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3456, 2L);
																				}
																				BgL_res2083z00_3447 =
																					(BgL_arg1802z00_3455 ==
																					BgL_classz00_3442);
																			}
																		else
																			{	/* Trace/walk.scm 487 */
																				BgL_res2083z00_3447 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2284z00_4331 = BgL_res2083z00_3447;
													}
											}
										}
									}
									if (BgL_test2284z00_4331)
										{	/* Trace/walk.scm 487 */
											BgL_test2283z00_4330 = ((bool_t) 1);
										}
									else
										{	/* Trace/walk.scm 488 */
											bool_t BgL_test2288z00_4357;

											{	/* Trace/walk.scm 488 */
												BgL_nodez00_bglt BgL_arg1989z00_3457;

												BgL_arg1989z00_3457 =
													(((BgL_setqz00_bglt) COBJECT(
															((BgL_setqz00_bglt) BgL_nodez00_3303)))->
													BgL_valuez00);
												{	/* Trace/walk.scm 488 */
													obj_t BgL_classz00_3458;

													BgL_classz00_3458 = BGl_varz00zzast_nodez00;
													{	/* Trace/walk.scm 488 */
														BgL_objectz00_bglt BgL_arg1807z00_3459;

														{	/* Trace/walk.scm 488 */
															obj_t BgL_tmpz00_4360;

															BgL_tmpz00_4360 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg1989z00_3457));
															BgL_arg1807z00_3459 =
																(BgL_objectz00_bglt) (BgL_tmpz00_4360);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Trace/walk.scm 488 */
																long BgL_idxz00_3460;

																BgL_idxz00_3460 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3459);
																BgL_test2288z00_4357 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3460 + 2L)) ==
																	BgL_classz00_3458);
															}
														else
															{	/* Trace/walk.scm 488 */
																bool_t BgL_res2084z00_3463;

																{	/* Trace/walk.scm 488 */
																	obj_t BgL_oclassz00_3464;

																	{	/* Trace/walk.scm 488 */
																		obj_t BgL_arg1815z00_3465;
																		long BgL_arg1816z00_3466;

																		BgL_arg1815z00_3465 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Trace/walk.scm 488 */
																			long BgL_arg1817z00_3467;

																			BgL_arg1817z00_3467 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3459);
																			BgL_arg1816z00_3466 =
																				(BgL_arg1817z00_3467 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3464 =
																			VECTOR_REF(BgL_arg1815z00_3465,
																			BgL_arg1816z00_3466);
																	}
																	{	/* Trace/walk.scm 488 */
																		bool_t BgL__ortest_1115z00_3468;

																		BgL__ortest_1115z00_3468 =
																			(BgL_classz00_3458 == BgL_oclassz00_3464);
																		if (BgL__ortest_1115z00_3468)
																			{	/* Trace/walk.scm 488 */
																				BgL_res2084z00_3463 =
																					BgL__ortest_1115z00_3468;
																			}
																		else
																			{	/* Trace/walk.scm 488 */
																				long BgL_odepthz00_3469;

																				{	/* Trace/walk.scm 488 */
																					obj_t BgL_arg1804z00_3470;

																					BgL_arg1804z00_3470 =
																						(BgL_oclassz00_3464);
																					BgL_odepthz00_3469 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3470);
																				}
																				if ((2L < BgL_odepthz00_3469))
																					{	/* Trace/walk.scm 488 */
																						obj_t BgL_arg1802z00_3471;

																						{	/* Trace/walk.scm 488 */
																							obj_t BgL_arg1803z00_3472;

																							BgL_arg1803z00_3472 =
																								(BgL_oclassz00_3464);
																							BgL_arg1802z00_3471 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3472, 2L);
																						}
																						BgL_res2084z00_3463 =
																							(BgL_arg1802z00_3471 ==
																							BgL_classz00_3458);
																					}
																				else
																					{	/* Trace/walk.scm 488 */
																						BgL_res2084z00_3463 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2288z00_4357 = BgL_res2084z00_3463;
															}
													}
												}
											}
											if (BgL_test2288z00_4357)
												{	/* Trace/walk.scm 488 */
													BgL_test2283z00_4330 = ((bool_t) 1);
												}
											else
												{	/* Trace/walk.scm 489 */
													bool_t BgL_test2292z00_4383;

													{	/* Trace/walk.scm 489 */
														BgL_nodez00_bglt BgL_arg1988z00_3473;

														BgL_arg1988z00_3473 =
															(((BgL_setqz00_bglt) COBJECT(
																	((BgL_setqz00_bglt) BgL_nodez00_3303)))->
															BgL_valuez00);
														{	/* Trace/walk.scm 489 */
															obj_t BgL_classz00_3474;

															BgL_classz00_3474 = BGl_kwotez00zzast_nodez00;
															{	/* Trace/walk.scm 489 */
																BgL_objectz00_bglt BgL_arg1807z00_3475;

																{	/* Trace/walk.scm 489 */
																	obj_t BgL_tmpz00_4386;

																	BgL_tmpz00_4386 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1988z00_3473));
																	BgL_arg1807z00_3475 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_4386);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Trace/walk.scm 489 */
																		long BgL_idxz00_3476;

																		BgL_idxz00_3476 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3475);
																		BgL_test2292z00_4383 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3476 + 2L)) ==
																			BgL_classz00_3474);
																	}
																else
																	{	/* Trace/walk.scm 489 */
																		bool_t BgL_res2085z00_3479;

																		{	/* Trace/walk.scm 489 */
																			obj_t BgL_oclassz00_3480;

																			{	/* Trace/walk.scm 489 */
																				obj_t BgL_arg1815z00_3481;
																				long BgL_arg1816z00_3482;

																				BgL_arg1815z00_3481 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Trace/walk.scm 489 */
																					long BgL_arg1817z00_3483;

																					BgL_arg1817z00_3483 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3475);
																					BgL_arg1816z00_3482 =
																						(BgL_arg1817z00_3483 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3480 =
																					VECTOR_REF(BgL_arg1815z00_3481,
																					BgL_arg1816z00_3482);
																			}
																			{	/* Trace/walk.scm 489 */
																				bool_t BgL__ortest_1115z00_3484;

																				BgL__ortest_1115z00_3484 =
																					(BgL_classz00_3474 ==
																					BgL_oclassz00_3480);
																				if (BgL__ortest_1115z00_3484)
																					{	/* Trace/walk.scm 489 */
																						BgL_res2085z00_3479 =
																							BgL__ortest_1115z00_3484;
																					}
																				else
																					{	/* Trace/walk.scm 489 */
																						long BgL_odepthz00_3485;

																						{	/* Trace/walk.scm 489 */
																							obj_t BgL_arg1804z00_3486;

																							BgL_arg1804z00_3486 =
																								(BgL_oclassz00_3480);
																							BgL_odepthz00_3485 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3486);
																						}
																						if ((2L < BgL_odepthz00_3485))
																							{	/* Trace/walk.scm 489 */
																								obj_t BgL_arg1802z00_3487;

																								{	/* Trace/walk.scm 489 */
																									obj_t BgL_arg1803z00_3488;

																									BgL_arg1803z00_3488 =
																										(BgL_oclassz00_3480);
																									BgL_arg1802z00_3487 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3488, 2L);
																								}
																								BgL_res2085z00_3479 =
																									(BgL_arg1802z00_3487 ==
																									BgL_classz00_3474);
																							}
																						else
																							{	/* Trace/walk.scm 489 */
																								BgL_res2085z00_3479 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2292z00_4383 = BgL_res2085z00_3479;
																	}
															}
														}
													}
													if (BgL_test2292z00_4383)
														{	/* Trace/walk.scm 489 */
															BgL_test2283z00_4330 = ((bool_t) 1);
														}
													else
														{	/* Trace/walk.scm 490 */
															BgL_nodez00_bglt BgL_arg1987z00_3489;

															BgL_arg1987z00_3489 =
																(((BgL_setqz00_bglt) COBJECT(
																		((BgL_setqz00_bglt) BgL_nodez00_3303)))->
																BgL_valuez00);
															{	/* Trace/walk.scm 490 */
																obj_t BgL_classz00_3490;

																BgL_classz00_3490 = BGl_pragmaz00zzast_nodez00;
																{	/* Trace/walk.scm 490 */
																	BgL_objectz00_bglt BgL_arg1807z00_3491;

																	{	/* Trace/walk.scm 490 */
																		obj_t BgL_tmpz00_4411;

																		BgL_tmpz00_4411 =
																			((obj_t)
																			((BgL_objectz00_bglt)
																				BgL_arg1987z00_3489));
																		BgL_arg1807z00_3491 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_4411);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Trace/walk.scm 490 */
																			long BgL_idxz00_3492;

																			BgL_idxz00_3492 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3491);
																			BgL_test2283z00_4330 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3492 + 4L)) ==
																				BgL_classz00_3490);
																		}
																	else
																		{	/* Trace/walk.scm 490 */
																			bool_t BgL_res2086z00_3495;

																			{	/* Trace/walk.scm 490 */
																				obj_t BgL_oclassz00_3496;

																				{	/* Trace/walk.scm 490 */
																					obj_t BgL_arg1815z00_3497;
																					long BgL_arg1816z00_3498;

																					BgL_arg1815z00_3497 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Trace/walk.scm 490 */
																						long BgL_arg1817z00_3499;

																						BgL_arg1817z00_3499 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3491);
																						BgL_arg1816z00_3498 =
																							(BgL_arg1817z00_3499 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3496 =
																						VECTOR_REF(BgL_arg1815z00_3497,
																						BgL_arg1816z00_3498);
																				}
																				{	/* Trace/walk.scm 490 */
																					bool_t BgL__ortest_1115z00_3500;

																					BgL__ortest_1115z00_3500 =
																						(BgL_classz00_3490 ==
																						BgL_oclassz00_3496);
																					if (BgL__ortest_1115z00_3500)
																						{	/* Trace/walk.scm 490 */
																							BgL_res2086z00_3495 =
																								BgL__ortest_1115z00_3500;
																						}
																					else
																						{	/* Trace/walk.scm 490 */
																							long BgL_odepthz00_3501;

																							{	/* Trace/walk.scm 490 */
																								obj_t BgL_arg1804z00_3502;

																								BgL_arg1804z00_3502 =
																									(BgL_oclassz00_3496);
																								BgL_odepthz00_3501 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3502);
																							}
																							if ((4L < BgL_odepthz00_3501))
																								{	/* Trace/walk.scm 490 */
																									obj_t BgL_arg1802z00_3503;

																									{	/* Trace/walk.scm 490 */
																										obj_t BgL_arg1803z00_3504;

																										BgL_arg1803z00_3504 =
																											(BgL_oclassz00_3496);
																										BgL_arg1802z00_3503 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3504, 4L);
																									}
																									BgL_res2086z00_3495 =
																										(BgL_arg1802z00_3503 ==
																										BgL_classz00_3490);
																								}
																							else
																								{	/* Trace/walk.scm 490 */
																									BgL_res2086z00_3495 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2283z00_4330 =
																				BgL_res2086z00_3495;
																		}
																}
															}
														}
												}
										}
								}
								if (BgL_test2283z00_4330)
									{	/* Trace/walk.scm 487 */
										BgL_test2278z00_4304 = ((bool_t) 0);
									}
								else
									{	/* Trace/walk.scm 487 */
										BgL_test2278z00_4304 = ((bool_t) 1);
									}
							}
						else
							{	/* Trace/walk.scm 486 */
								BgL_test2278z00_4304 = ((bool_t) 0);
							}
					}
					if (BgL_test2278z00_4304)
						{	/* Trace/walk.scm 491 */
							BgL_typez00_bglt BgL_tz00_3505;

							{	/* Trace/walk.scm 491 */
								BgL_typez00_bglt BgL_arg1983z00_3506;
								BgL_typez00_bglt BgL_arg1984z00_3507;

								BgL_arg1983z00_3506 =
									(((BgL_nodez00_bglt) COBJECT(
											(((BgL_setqz00_bglt) COBJECT(
														((BgL_setqz00_bglt) BgL_nodez00_3303)))->
												BgL_valuez00)))->BgL_typez00);
								BgL_arg1984z00_3507 =
									(((BgL_variablez00_bglt)
										COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) ((
															(BgL_varz00_bglt) COBJECT(BgL_i1135z00_3424))->
														BgL_variablez00)))))->BgL_typez00);
								BgL_tz00_3505 =
									BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg1983z00_3506,
									BgL_arg1984z00_3507);
							}
							{	/* Trace/walk.scm 491 */
								BgL_letzd2varzd2_bglt BgL_tracez00_3508;

								{	/* Trace/walk.scm 492 */
									BgL_nodez00_bglt BgL_arg1979z00_3509;
									obj_t BgL_arg1980z00_3510;
									obj_t BgL_arg1981z00_3511;

									BgL_arg1979z00_3509 =
										(((BgL_setqz00_bglt) COBJECT(
												((BgL_setqz00_bglt) BgL_nodez00_3303)))->BgL_valuez00);
									{	/* Trace/walk.scm 492 */
										BgL_variablez00_bglt BgL_arg1982z00_3512;

										BgL_arg1982z00_3512 =
											(((BgL_varz00_bglt) COBJECT(BgL_i1135z00_3424))->
											BgL_variablez00);
										BgL_arg1980z00_3510 =
											BGl_tracezd2idzd2zztrace_walkz00(((obj_t)
												BgL_arg1982z00_3512));
									}
									BgL_arg1981z00_3511 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_setqz00_bglt) BgL_nodez00_3303))))->BgL_locz00);
									BgL_tracez00_3508 =
										BGl_makezd2tracedzd2nodez00zztrace_walkz00
										(BgL_arg1979z00_3509, BgL_tz00_3505, BgL_arg1980z00_3510,
										BgL_arg1981z00_3511, BNIL);
								}
								{	/* Trace/walk.scm 492 */

									((((BgL_setqz00_bglt) COBJECT(
													((BgL_setqz00_bglt) BgL_nodez00_3303)))->
											BgL_valuez00) =
										((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_tracez00_3508)),
										BUNSPEC);
								}
							}
						}
					else
						{	/* Trace/walk.scm 486 */
							BFALSE;
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_3303));
		}

	}



/* &toplevel-trace-node-1337 */
	BgL_nodez00_bglt BGl_z62toplevelzd2tracezd2nodezd21337zb0zztrace_walkz00(obj_t
		BgL_envz00_3304, obj_t BgL_nodez00_3305)
	{
		{	/* Trace/walk.scm 469 */
			((((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_3305)))->BgL_mutexz00) =
				((BgL_nodez00_bglt)
					BGl_toplevelzd2tracezd2nodez00zztrace_walkz00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3305)))->
							BgL_mutexz00))), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3305)))->
					BgL_prelockz00) =
				((BgL_nodez00_bglt)
					BGl_toplevelzd2tracezd2nodez00zztrace_walkz00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3305)))->
							BgL_prelockz00))), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3305)))->
					BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_toplevelzd2tracezd2nodez00zztrace_walkz00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3305)))->BgL_bodyz00))),
				BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_3305));
		}

	}



/* &toplevel-trace-node-1335 */
	BgL_nodez00_bglt BGl_z62toplevelzd2tracezd2nodezd21335zb0zztrace_walkz00(obj_t
		BgL_envz00_3306, obj_t BgL_nodez00_3307)
	{
		{	/* Trace/walk.scm 462 */
			BGl_toplevelzd2tracezd2nodeza2z12zb0zztrace_walkz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_3307)))->BgL_nodesz00));
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_3307));
		}

	}



/* &trace-node-box-set!1331 */
	BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2boxzd2setz121331za2zztrace_walkz00(obj_t
		BgL_envz00_3308, obj_t BgL_nodez00_3309, obj_t BgL_stackz00_3310,
		obj_t BgL_levelz00_3311)
	{
		{	/* Trace/walk.scm 439 */
			{
				BgL_varz00_bglt BgL_auxz00_4478;

				{	/* Trace/walk.scm 441 */
					BgL_varz00_bglt BgL_arg1951z00_3516;

					BgL_arg1951z00_3516 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3309)))->BgL_varz00);
					BgL_auxz00_4478 =
						((BgL_varz00_bglt)
						BGl_tracezd2nodezd2zztrace_walkz00(
							((BgL_nodez00_bglt) BgL_arg1951z00_3516), BgL_stackz00_3310,
							BgL_levelz00_3311));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3309)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_4478), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4486;

				{	/* Trace/walk.scm 442 */
					BgL_nodez00_bglt BgL_arg1952z00_3517;

					BgL_arg1952z00_3517 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3309)))->BgL_valuez00);
					BgL_auxz00_4486 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1952z00_3517,
						BgL_stackz00_3310, BgL_levelz00_3311);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3309)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4486), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3309));
		}

	}



/* &trace-node-box-ref1329 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2boxzd2ref1329zb0zztrace_walkz00(obj_t
		BgL_envz00_3312, obj_t BgL_nodez00_3313, obj_t BgL_stackz00_3314,
		obj_t BgL_levelz00_3315)
	{
		{	/* Trace/walk.scm 432 */
			{	/* Trace/walk.scm 433 */
				BgL_nodez00_bglt BgL_arg1949z00_3519;

				{	/* Trace/walk.scm 433 */
					BgL_varz00_bglt BgL_arg1950z00_3520;

					BgL_arg1950z00_3520 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3313)))->BgL_varz00);
					BgL_arg1949z00_3519 =
						BGl_tracezd2nodezd2zztrace_walkz00(
						((BgL_nodez00_bglt) BgL_arg1950z00_3520), BgL_stackz00_3314,
						BgL_levelz00_3315);
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3313)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1949z00_3519)), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_3313));
		}

	}



/* &trace-node-make-box1327 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2makezd2box1327zb0zztrace_walkz00(obj_t
		BgL_envz00_3316, obj_t BgL_nodez00_3317, obj_t BgL_stackz00_3318,
		obj_t BgL_levelz00_3319)
	{
		{	/* Trace/walk.scm 425 */
			((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_3317)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_tracezd2nodezd2zztrace_walkz00((((BgL_makezd2boxzd2_bglt)
								COBJECT(((BgL_makezd2boxzd2_bglt) BgL_nodez00_3317)))->
							BgL_valuez00), BgL_stackz00_3318, BgL_levelz00_3319)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_3317));
		}

	}



/* &trace-node-jump-ex-i1325 */
	BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2jumpzd2exzd2i1325z62zztrace_walkz00(obj_t
		BgL_envz00_3320, obj_t BgL_nodez00_3321, obj_t BgL_stackz00_3322,
		obj_t BgL_levelz00_3323)
	{
		{	/* Trace/walk.scm 416 */
			{
				BgL_nodez00_bglt BgL_auxz00_4510;

				{	/* Trace/walk.scm 418 */
					BgL_nodez00_bglt BgL_arg1945z00_3523;

					BgL_arg1945z00_3523 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3321)))->BgL_exitz00);
					BgL_auxz00_4510 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1945z00_3523,
						BgL_stackz00_3322, BgL_levelz00_3323);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3321)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_4510), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4516;

				{	/* Trace/walk.scm 419 */
					BgL_nodez00_bglt BgL_arg1946z00_3524;

					BgL_arg1946z00_3524 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3321)))->
						BgL_valuez00);
					BgL_auxz00_4516 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1946z00_3524,
						BgL_stackz00_3322, BgL_levelz00_3323);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3321)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_4516), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3321));
		}

	}



/* &trace-node-set-ex-it1323 */
	BgL_nodez00_bglt
		BGl_z62tracezd2nodezd2setzd2exzd2it1323z62zztrace_walkz00(obj_t
		BgL_envz00_3324, obj_t BgL_nodez00_3325, obj_t BgL_stackz00_3326,
		obj_t BgL_levelz00_3327)
	{
		{	/* Trace/walk.scm 408 */
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3325)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_tracezd2nodezd2zztrace_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3325)))->
							BgL_bodyz00), BgL_stackz00_3326, BgL_levelz00_3327)), BUNSPEC);
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
								BgL_nodez00_3325)))->BgL_onexitz00) =
				((BgL_nodez00_bglt)
					BGl_tracezd2nodezd2zztrace_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3325)))->
							BgL_onexitz00), BgL_stackz00_3326, BgL_levelz00_3327)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
					BgL_nodez00_3325));
		}

	}



/* &trace-node-let-var1321 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2letzd2var1321zb0zztrace_walkz00(obj_t
		BgL_envz00_3328, obj_t BgL_nodez00_3329, obj_t BgL_stackz00_3330,
		obj_t BgL_levelz00_3331)
	{
		{	/* Trace/walk.scm 397 */
			{	/* Trace/walk.scm 399 */
				obj_t BgL_g1263z00_3527;

				BgL_g1263z00_3527 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_3329)))->BgL_bindingsz00);
				{
					obj_t BgL_l1261z00_3529;

					BgL_l1261z00_3529 = BgL_g1263z00_3527;
				BgL_zc3z04anonymousza31935ze3z87_3528:
					if (PAIRP(BgL_l1261z00_3529))
						{	/* Trace/walk.scm 399 */
							{	/* Trace/walk.scm 400 */
								obj_t BgL_bindingz00_3530;

								BgL_bindingz00_3530 = CAR(BgL_l1261z00_3529);
								{	/* Trace/walk.scm 400 */
									BgL_nodez00_bglt BgL_arg1937z00_3531;

									{	/* Trace/walk.scm 400 */
										obj_t BgL_arg1938z00_3532;

										BgL_arg1938z00_3532 = CDR(((obj_t) BgL_bindingz00_3530));
										BgL_arg1937z00_3531 =
											BGl_tracezd2nodezd2zztrace_walkz00(
											((BgL_nodez00_bglt) BgL_arg1938z00_3532),
											BgL_stackz00_3330, BgL_levelz00_3331);
									}
									{	/* Trace/walk.scm 400 */
										obj_t BgL_auxz00_4547;
										obj_t BgL_tmpz00_4545;

										BgL_auxz00_4547 = ((obj_t) BgL_arg1937z00_3531);
										BgL_tmpz00_4545 = ((obj_t) BgL_bindingz00_3530);
										SET_CDR(BgL_tmpz00_4545, BgL_auxz00_4547);
									}
								}
							}
							{
								obj_t BgL_l1261z00_4550;

								BgL_l1261z00_4550 = CDR(BgL_l1261z00_3529);
								BgL_l1261z00_3529 = BgL_l1261z00_4550;
								goto BgL_zc3z04anonymousza31935ze3z87_3528;
							}
						}
					else
						{	/* Trace/walk.scm 399 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4552;

				{	/* Trace/walk.scm 402 */
					BgL_nodez00_bglt BgL_arg1940z00_3533;

					BgL_arg1940z00_3533 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_3329)))->BgL_bodyz00);
					BgL_auxz00_4552 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1940z00_3533,
						BgL_stackz00_3330, BgL_levelz00_3331);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_3329)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4552), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_3329));
		}

	}



/* &trace-node-let-fun1319 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2letzd2fun1319zb0zztrace_walkz00(obj_t
		BgL_envz00_3332, obj_t BgL_nodez00_3333, obj_t BgL_stackz00_3334,
		obj_t BgL_levelz00_3335)
	{
		{	/* Trace/walk.scm 381 */
			if (BGl_isloopzf3zf3zztrace_isloopz00(
					((BgL_letzd2funzd2_bglt) BgL_nodez00_3333)))
				{	/* Trace/walk.scm 382 */
					{	/* Trace/walk.scm 384 */
						obj_t BgL_varz00_3535;

						BgL_varz00_3535 =
							CAR(
							(((BgL_letzd2funzd2_bglt) COBJECT(
										((BgL_letzd2funzd2_bglt) BgL_nodez00_3333)))->
								BgL_localsz00));
						{	/* Trace/walk.scm 384 */
							BgL_typez00_bglt BgL_tz00_3536;

							{	/* Trace/walk.scm 385 */
								BgL_typez00_bglt BgL_arg1927z00_3537;
								BgL_typez00_bglt BgL_arg1928z00_3538;

								BgL_arg1927z00_3537 =
									(((BgL_nodez00_bglt) COBJECT(
											(((BgL_letzd2funzd2_bglt) COBJECT(
														((BgL_letzd2funzd2_bglt) BgL_nodez00_3333)))->
												BgL_bodyz00)))->BgL_typez00);
								BgL_arg1928z00_3538 =
									(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
												BgL_varz00_3535)))->BgL_typez00);
								BgL_tz00_3536 =
									BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg1927z00_3537,
									BgL_arg1928z00_3538);
							}
							{	/* Trace/walk.scm 385 */
								obj_t BgL_idz00_3539;

								BgL_idz00_3539 =
									BGl_tracezd2idzd2zztrace_walkz00(BgL_varz00_3535);
								{	/* Trace/walk.scm 386 */

									{
										BgL_nodez00_bglt BgL_auxz00_4573;

										{	/* Trace/walk.scm 387 */
											BgL_nodez00_bglt BgL_arg1925z00_3540;
											obj_t BgL_arg1926z00_3541;

											BgL_arg1925z00_3540 =
												(((BgL_letzd2funzd2_bglt) COBJECT(
														((BgL_letzd2funzd2_bglt) BgL_nodez00_3333)))->
												BgL_bodyz00);
											BgL_arg1926z00_3541 =
												(((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
																BgL_nodez00_3333))))->BgL_locz00);
											BgL_auxz00_4573 =
												((BgL_nodez00_bglt)
												BGl_makezd2tracedzd2nodez00zztrace_walkz00
												(BgL_arg1925z00_3540, BgL_tz00_3536, BgL_idz00_3539,
													BgL_arg1926z00_3541, BNIL));
										}
										((((BgL_letzd2funzd2_bglt) COBJECT(
														((BgL_letzd2funzd2_bglt) BgL_nodez00_3333)))->
												BgL_bodyz00) =
											((BgL_nodez00_bglt) BgL_auxz00_4573), BUNSPEC);
									}
								}
							}
						}
					}
					return
						((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_3333));
				}
			else
				{	/* Trace/walk.scm 382 */
					{	/* Trace/walk.scm 390 */
						obj_t BgL_g1260z00_3542;

						BgL_g1260z00_3542 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_3333)))->BgL_localsz00);
						{
							obj_t BgL_l1258z00_3544;

							BgL_l1258z00_3544 = BgL_g1260z00_3542;
						BgL_zc3z04anonymousza31931ze3z87_3543:
							if (PAIRP(BgL_l1258z00_3544))
								{	/* Trace/walk.scm 390 */
									BGl_tracezd2funz12zc0zztrace_walkz00(CAR(BgL_l1258z00_3544),
										BgL_stackz00_3334, BgL_levelz00_3335);
									{
										obj_t BgL_l1258z00_4591;

										BgL_l1258z00_4591 = CDR(BgL_l1258z00_3544);
										BgL_l1258z00_3544 = BgL_l1258z00_4591;
										goto BgL_zc3z04anonymousza31931ze3z87_3543;
									}
								}
							else
								{	/* Trace/walk.scm 390 */
									((bool_t) 1);
								}
						}
					}
					{
						BgL_nodez00_bglt BgL_auxz00_4593;

						{	/* Trace/walk.scm 391 */
							BgL_nodez00_bglt BgL_arg1934z00_3545;

							BgL_arg1934z00_3545 =
								(((BgL_letzd2funzd2_bglt) COBJECT(
										((BgL_letzd2funzd2_bglt) BgL_nodez00_3333)))->BgL_bodyz00);
							BgL_auxz00_4593 =
								BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1934z00_3545,
								BgL_stackz00_3334, BgL_levelz00_3335);
						}
						((((BgL_letzd2funzd2_bglt) COBJECT(
										((BgL_letzd2funzd2_bglt) BgL_nodez00_3333)))->BgL_bodyz00) =
							((BgL_nodez00_bglt) BgL_auxz00_4593), BUNSPEC);
					}
					return
						((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_3333));
				}
		}

	}



/* &trace-node-switch1317 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2switch1317z62zztrace_walkz00(obj_t
		BgL_envz00_3336, obj_t BgL_nodez00_3337, obj_t BgL_stackz00_3338,
		obj_t BgL_levelz00_3339)
	{
		{	/* Trace/walk.scm 370 */
			{
				BgL_nodez00_bglt BgL_auxz00_4601;

				{	/* Trace/walk.scm 372 */
					BgL_nodez00_bglt BgL_arg1916z00_3547;

					BgL_arg1916z00_3547 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_3337)))->BgL_testz00);
					BgL_auxz00_4601 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1916z00_3547,
						BgL_stackz00_3338, BgL_levelz00_3339);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_3337)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4601), BUNSPEC);
			}
			{	/* Trace/walk.scm 373 */
				obj_t BgL_g1257z00_3548;

				BgL_g1257z00_3548 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_3337)))->BgL_clausesz00);
				{
					obj_t BgL_l1255z00_3550;

					BgL_l1255z00_3550 = BgL_g1257z00_3548;
				BgL_zc3z04anonymousza31917ze3z87_3549:
					if (PAIRP(BgL_l1255z00_3550))
						{	/* Trace/walk.scm 373 */
							{	/* Trace/walk.scm 374 */
								obj_t BgL_clausez00_3551;

								BgL_clausez00_3551 = CAR(BgL_l1255z00_3550);
								{	/* Trace/walk.scm 374 */
									BgL_nodez00_bglt BgL_arg1919z00_3552;

									{	/* Trace/walk.scm 374 */
										obj_t BgL_arg1920z00_3553;

										BgL_arg1920z00_3553 = CDR(((obj_t) BgL_clausez00_3551));
										BgL_arg1919z00_3552 =
											BGl_tracezd2nodezd2zztrace_walkz00(
											((BgL_nodez00_bglt) BgL_arg1920z00_3553),
											BgL_stackz00_3338, BgL_levelz00_3339);
									}
									{	/* Trace/walk.scm 374 */
										obj_t BgL_auxz00_4618;
										obj_t BgL_tmpz00_4616;

										BgL_auxz00_4618 = ((obj_t) BgL_arg1919z00_3552);
										BgL_tmpz00_4616 = ((obj_t) BgL_clausez00_3551);
										SET_CDR(BgL_tmpz00_4616, BgL_auxz00_4618);
									}
								}
							}
							{
								obj_t BgL_l1255z00_4621;

								BgL_l1255z00_4621 = CDR(BgL_l1255z00_3550);
								BgL_l1255z00_3550 = BgL_l1255z00_4621;
								goto BgL_zc3z04anonymousza31917ze3z87_3549;
							}
						}
					else
						{	/* Trace/walk.scm 373 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_3337));
		}

	}



/* &trace-node-fail1315 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2fail1315z62zztrace_walkz00(obj_t
		BgL_envz00_3340, obj_t BgL_nodez00_3341, obj_t BgL_stackz00_3342,
		obj_t BgL_levelz00_3343)
	{
		{	/* Trace/walk.scm 360 */
			{
				BgL_nodez00_bglt BgL_auxz00_4625;

				{	/* Trace/walk.scm 362 */
					BgL_nodez00_bglt BgL_arg1912z00_3555;

					BgL_arg1912z00_3555 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3341)))->BgL_procz00);
					BgL_auxz00_4625 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1912z00_3555,
						BgL_stackz00_3342, BgL_levelz00_3343);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3341)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4625), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4631;

				{	/* Trace/walk.scm 363 */
					BgL_nodez00_bglt BgL_arg1913z00_3556;

					BgL_arg1913z00_3556 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3341)))->BgL_msgz00);
					BgL_auxz00_4631 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1913z00_3556,
						BgL_stackz00_3342, BgL_levelz00_3343);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3341)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4631), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4637;

				{	/* Trace/walk.scm 364 */
					BgL_nodez00_bglt BgL_arg1914z00_3557;

					BgL_arg1914z00_3557 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3341)))->BgL_objz00);
					BgL_auxz00_4637 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1914z00_3557,
						BgL_stackz00_3342, BgL_levelz00_3343);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3341)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4637), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_3341));
		}

	}



/* &trace-node-condition1313 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2condition1313z62zztrace_walkz00(obj_t
		BgL_envz00_3344, obj_t BgL_nodez00_3345, obj_t BgL_stackz00_3346,
		obj_t BgL_levelz00_3347)
	{
		{	/* Trace/walk.scm 350 */
			{
				BgL_nodez00_bglt BgL_auxz00_4645;

				{	/* Trace/walk.scm 352 */
					BgL_nodez00_bglt BgL_arg1906z00_3559;

					BgL_arg1906z00_3559 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3345)))->BgL_testz00);
					BgL_auxz00_4645 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1906z00_3559,
						BgL_stackz00_3346, BgL_levelz00_3347);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3345)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4645), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4651;

				{	/* Trace/walk.scm 353 */
					BgL_nodez00_bglt BgL_arg1910z00_3560;

					BgL_arg1910z00_3560 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3345)))->BgL_truez00);
					BgL_auxz00_4651 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1910z00_3560,
						BgL_stackz00_3346, BgL_levelz00_3347);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3345)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4651), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4657;

				{	/* Trace/walk.scm 354 */
					BgL_nodez00_bglt BgL_arg1911z00_3561;

					BgL_arg1911z00_3561 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3345)))->BgL_falsez00);
					BgL_auxz00_4657 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1911z00_3561,
						BgL_stackz00_3346, BgL_levelz00_3347);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3345)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4657), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_3345));
		}

	}



/* &trace-node-setq1311 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2setq1311z62zztrace_walkz00(obj_t
		BgL_envz00_3348, obj_t BgL_nodez00_3349, obj_t BgL_stackz00_3350,
		obj_t BgL_levelz00_3351)
	{
		{	/* Trace/walk.scm 343 */
			((((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_3349)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_tracezd2nodezd2zztrace_walkz00((((BgL_setqz00_bglt)
								COBJECT(((BgL_setqz00_bglt) BgL_nodez00_3349)))->BgL_valuez00),
						BgL_stackz00_3350, BgL_levelz00_3351)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_3349));
		}

	}



/* &trace-node-cast1309 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2cast1309z62zztrace_walkz00(obj_t
		BgL_envz00_3352, obj_t BgL_nodez00_3353, obj_t BgL_stackz00_3354,
		obj_t BgL_levelz00_3355)
	{
		{	/* Trace/walk.scm 336 */
			((((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_3353)))->BgL_argz00) =
				((BgL_nodez00_bglt)
					BGl_tracezd2nodezd2zztrace_walkz00((((BgL_castz00_bglt)
								COBJECT(((BgL_castz00_bglt) BgL_nodez00_3353)))->BgL_argz00),
						BgL_stackz00_3354, BgL_levelz00_3355)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_3353));
		}

	}



/* &trace-node-extern1307 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2extern1307z62zztrace_walkz00(obj_t
		BgL_envz00_3356, obj_t BgL_nodez00_3357, obj_t BgL_stackz00_3358,
		obj_t BgL_levelz00_3359)
	{
		{	/* Trace/walk.scm 329 */
			BGl_tracezd2nodeza2z12z62zztrace_walkz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_3357)))->BgL_exprza2za2),
				BgL_stackz00_3358, BgL_levelz00_3359);
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_3357));
		}

	}



/* &trace-node-funcall1305 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2funcall1305z62zztrace_walkz00(obj_t
		BgL_envz00_3360, obj_t BgL_nodez00_3361, obj_t BgL_stackz00_3362,
		obj_t BgL_levelz00_3363)
	{
		{	/* Trace/walk.scm 320 */
			{
				BgL_nodez00_bglt BgL_auxz00_4684;

				{	/* Trace/walk.scm 322 */
					BgL_nodez00_bglt BgL_arg1897z00_3566;

					BgL_arg1897z00_3566 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3361)))->BgL_funz00);
					BgL_auxz00_4684 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1897z00_3566,
						BgL_stackz00_3362, BgL_levelz00_3363);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3361)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4684), BUNSPEC);
			}
			BGl_tracezd2nodeza2z12z62zztrace_walkz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_3361)))->BgL_argsz00),
				BgL_stackz00_3362, BgL_levelz00_3363);
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_3361));
		}

	}



/* &trace-node-app-ly1303 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2appzd2ly1303zb0zztrace_walkz00(obj_t
		BgL_envz00_3364, obj_t BgL_nodez00_3365, obj_t BgL_stackz00_3366,
		obj_t BgL_levelz00_3367)
	{
		{	/* Trace/walk.scm 311 */
			{
				BgL_nodez00_bglt BgL_auxz00_4695;

				{	/* Trace/walk.scm 313 */
					BgL_nodez00_bglt BgL_arg1894z00_3568;

					BgL_arg1894z00_3568 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3365)))->BgL_funz00);
					BgL_auxz00_4695 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1894z00_3568,
						BgL_stackz00_3366, BgL_levelz00_3367);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3365)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4695), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4701;

				{	/* Trace/walk.scm 314 */
					BgL_nodez00_bglt BgL_arg1896z00_3569;

					BgL_arg1896z00_3569 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3365)))->BgL_argz00);
					BgL_auxz00_4701 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1896z00_3569,
						BgL_stackz00_3366, BgL_levelz00_3367);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3365)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4701), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_3365));
		}

	}



/* &trace-node-app1301 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2app1301z62zztrace_walkz00(obj_t
		BgL_envz00_3368, obj_t BgL_nodez00_3369, obj_t BgL_stackz00_3370,
		obj_t BgL_levelz00_3371)
	{
		{	/* Trace/walk.scm 304 */
			BGl_tracezd2nodeza2z12z62zztrace_walkz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_3369)))->BgL_argsz00),
				BgL_stackz00_3370, BgL_levelz00_3371);
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3369));
		}

	}



/* &trace-node-sync1299 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2sync1299z62zztrace_walkz00(obj_t
		BgL_envz00_3372, obj_t BgL_nodez00_3373, obj_t BgL_stackz00_3374,
		obj_t BgL_levelz00_3375)
	{
		{	/* Trace/walk.scm 294 */
			{
				BgL_nodez00_bglt BgL_auxz00_4714;

				{	/* Trace/walk.scm 296 */
					BgL_nodez00_bglt BgL_arg1890z00_3572;

					BgL_arg1890z00_3572 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3373)))->BgL_mutexz00);
					BgL_auxz00_4714 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1890z00_3572,
						BgL_stackz00_3374, BgL_levelz00_3375);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3373)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4714), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4720;

				{	/* Trace/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg1891z00_3573;

					BgL_arg1891z00_3573 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3373)))->BgL_prelockz00);
					BgL_auxz00_4720 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1891z00_3573,
						BgL_stackz00_3374, BgL_levelz00_3375);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3373)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4720), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4726;

				{	/* Trace/walk.scm 298 */
					BgL_nodez00_bglt BgL_arg1892z00_3574;

					BgL_arg1892z00_3574 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3373)))->BgL_bodyz00);
					BgL_auxz00_4726 =
						BGl_tracezd2nodezd2zztrace_walkz00(BgL_arg1892z00_3574,
						BgL_stackz00_3374, BgL_levelz00_3375);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3373)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4726), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_3373));
		}

	}



/* &trace-node-sequence1297 */
	BgL_nodez00_bglt BGl_z62tracezd2nodezd2sequence1297z62zztrace_walkz00(obj_t
		BgL_envz00_3376, obj_t BgL_nodez00_3377, obj_t BgL_stackz00_3378,
		obj_t BgL_levelz00_3379)
	{
		{	/* Trace/walk.scm 287 */
			BGl_tracezd2nodeza2z12z62zztrace_walkz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_3377)))->BgL_nodesz00),
				BgL_stackz00_3378, BgL_levelz00_3379);
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_3377));
		}

	}



/* &find-last-node-jump-1293 */
	obj_t BGl_z62findzd2lastzd2nodezd2jumpzd21293z62zztrace_walkz00(obj_t
		BgL_envz00_3380, obj_t BgL_nodez00_3381)
	{
		{	/* Trace/walk.scm 237 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3381)))->BgL_valuez00));
		}

	}



/* &find-last-node-set-e1291 */
	obj_t BGl_z62findzd2lastzd2nodezd2setzd2e1291z62zztrace_walkz00(obj_t
		BgL_envz00_3382, obj_t BgL_nodez00_3383)
	{
		{	/* Trace/walk.scm 231 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3383)))->BgL_bodyz00));
		}

	}



/* &find-last-node-let-f1289 */
	obj_t BGl_z62findzd2lastzd2nodezd2letzd2f1289z62zztrace_walkz00(obj_t
		BgL_envz00_3384, obj_t BgL_nodez00_3385)
	{
		{	/* Trace/walk.scm 225 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_3385)))->BgL_bodyz00));
		}

	}



/* &find-last-node-let-v1287 */
	obj_t BGl_z62findzd2lastzd2nodezd2letzd2v1287z62zztrace_walkz00(obj_t
		BgL_envz00_3386, obj_t BgL_nodez00_3387)
	{
		{	/* Trace/walk.scm 219 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_3387)))->BgL_bodyz00));
		}

	}



/* &find-last-node-switc1285 */
	obj_t BGl_z62findzd2lastzd2nodezd2switc1285zb0zztrace_walkz00(obj_t
		BgL_envz00_3388, obj_t BgL_nodez00_3389)
	{
		{	/* Trace/walk.scm 210 */
			{	/* Trace/walk.scm 212 */
				bool_t BgL_test2303z00_4751;

				{	/* Trace/walk.scm 212 */
					obj_t BgL_tmpz00_4752;

					BgL_tmpz00_4752 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_3389)))->BgL_clausesz00);
					BgL_test2303z00_4751 = PAIRP(BgL_tmpz00_4752);
				}
				if (BgL_test2303z00_4751)
					{	/* Trace/walk.scm 213 */
						obj_t BgL_arg1878z00_3581;

						BgL_arg1878z00_3581 =
							CDR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
								(((BgL_switchz00_bglt) COBJECT(
											((BgL_switchz00_bglt) BgL_nodez00_3389)))->
									BgL_clausesz00)));
						return BGl_findzd2lastzd2nodez00zztrace_walkz00(((BgL_nodez00_bglt)
								BgL_arg1878z00_3581));
					}
				else
					{	/* Trace/walk.scm 212 */
						return
							BGl_findzd2lastzd2sexpz00zztrace_walkz00(
							(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_3389)))->BgL_testz00));
					}
			}
		}

	}



/* &find-last-node-fail1283 */
	obj_t BGl_z62findzd2lastzd2nodezd2fail1283zb0zztrace_walkz00(obj_t
		BgL_envz00_3390, obj_t BgL_nodez00_3391)
	{
		{	/* Trace/walk.scm 204 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_3391)))->BgL_objz00));
		}

	}



/* &find-last-node-condi1281 */
	obj_t BGl_z62findzd2lastzd2nodezd2condi1281zb0zztrace_walkz00(obj_t
		BgL_envz00_3392, obj_t BgL_nodez00_3393)
	{
		{	/* Trace/walk.scm 198 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_3393)))->BgL_falsez00));
		}

	}



/* &find-last-node-setq1279 */
	obj_t BGl_z62findzd2lastzd2nodezd2setq1279zb0zztrace_walkz00(obj_t
		BgL_envz00_3394, obj_t BgL_nodez00_3395)
	{
		{	/* Trace/walk.scm 192 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_3395)))->BgL_valuez00));
		}

	}



/* &find-last-node-exter1277 */
	obj_t BGl_z62findzd2lastzd2nodezd2exter1277zb0zztrace_walkz00(obj_t
		BgL_envz00_3396, obj_t BgL_nodez00_3397)
	{
		{	/* Trace/walk.scm 183 */
			if (NULLP(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_3397)))->BgL_exprza2za2)))
				{	/* Trace/walk.scm 185 */
					return ((obj_t) ((BgL_externz00_bglt) BgL_nodez00_3397));
				}
			else
				{	/* Trace/walk.scm 186 */
					obj_t BgL_arg1868z00_3586;

					BgL_arg1868z00_3586 =
						CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
							(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt) BgL_nodez00_3397)))->
								BgL_exprza2za2)));
					return BGl_findzd2lastzd2nodez00zztrace_walkz00(((BgL_nodez00_bglt)
							BgL_arg1868z00_3586));
				}
		}

	}



/* &find-last-node-funca1275 */
	obj_t BGl_z62findzd2lastzd2nodezd2funca1275zb0zztrace_walkz00(obj_t
		BgL_envz00_3398, obj_t BgL_nodez00_3399)
	{
		{	/* Trace/walk.scm 174 */
			{	/* Trace/walk.scm 176 */
				bool_t BgL_test2305z00_4786;

				{	/* Trace/walk.scm 176 */
					obj_t BgL_tmpz00_4787;

					BgL_tmpz00_4787 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3399)))->BgL_argsz00);
					BgL_test2305z00_4786 = PAIRP(BgL_tmpz00_4787);
				}
				if (BgL_test2305z00_4786)
					{	/* Trace/walk.scm 177 */
						obj_t BgL_arg1859z00_3588;

						BgL_arg1859z00_3588 =
							CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
								(((BgL_funcallz00_bglt) COBJECT(
											((BgL_funcallz00_bglt) BgL_nodez00_3399)))->
									BgL_argsz00)));
						return BGl_findzd2lastzd2nodez00zztrace_walkz00(((BgL_nodez00_bglt)
								BgL_arg1859z00_3588));
					}
				else
					{	/* Trace/walk.scm 176 */
						return
							BGl_findzd2lastzd2nodez00zztrace_walkz00(
							(((BgL_funcallz00_bglt) COBJECT(
										((BgL_funcallz00_bglt) BgL_nodez00_3399)))->BgL_funz00));
					}
			}
		}

	}



/* &find-last-node-app-l1273 */
	obj_t BGl_z62findzd2lastzd2nodezd2appzd2l1273z62zztrace_walkz00(obj_t
		BgL_envz00_3400, obj_t BgL_nodez00_3401)
	{
		{	/* Trace/walk.scm 168 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_3401)))->BgL_argz00));
		}

	}



/* &find-last-node-app1271 */
	obj_t BGl_z62findzd2lastzd2nodezd2app1271zb0zztrace_walkz00(obj_t
		BgL_envz00_3402, obj_t BgL_nodez00_3403)
	{
		{	/* Trace/walk.scm 159 */
			{	/* Trace/walk.scm 161 */
				bool_t BgL_test2306z00_4803;

				{	/* Trace/walk.scm 161 */
					obj_t BgL_tmpz00_4804;

					BgL_tmpz00_4804 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_3403)))->BgL_argsz00);
					BgL_test2306z00_4803 = PAIRP(BgL_tmpz00_4804);
				}
				if (BgL_test2306z00_4803)
					{	/* Trace/walk.scm 162 */
						obj_t BgL_arg1851z00_3591;

						BgL_arg1851z00_3591 =
							CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_3403)))->BgL_argsz00)));
						return
							BGl_findzd2lastzd2nodez00zztrace_walkz00(
							((BgL_nodez00_bglt) BgL_arg1851z00_3591));
					}
				else
					{	/* Trace/walk.scm 161 */
						return ((obj_t) ((BgL_appz00_bglt) BgL_nodez00_3403));
					}
			}
		}

	}



/* &find-last-node-sync1269 */
	obj_t BGl_z62findzd2lastzd2nodezd2sync1269zb0zztrace_walkz00(obj_t
		BgL_envz00_3404, obj_t BgL_nodez00_3405)
	{
		{	/* Trace/walk.scm 152 */
			return
				BGl_findzd2lastzd2nodez00zztrace_walkz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_3405)))->BgL_bodyz00));
		}

	}



/* &find-last-node-seque1267 */
	obj_t BGl_z62findzd2lastzd2nodezd2seque1267zb0zztrace_walkz00(obj_t
		BgL_envz00_3406, obj_t BgL_nodez00_3407)
	{
		{	/* Trace/walk.scm 143 */
			if (NULLP(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_3407)))->BgL_nodesz00)))
				{	/* Trace/walk.scm 145 */
					return ((obj_t) ((BgL_sequencez00_bglt) BgL_nodez00_3407));
				}
			else
				{	/* Trace/walk.scm 146 */
					obj_t BgL_arg1844z00_3594;

					BgL_arg1844z00_3594 =
						CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
							(((BgL_sequencez00_bglt) COBJECT(
										((BgL_sequencez00_bglt) BgL_nodez00_3407)))->
								BgL_nodesz00)));
					return BGl_findzd2lastzd2nodez00zztrace_walkz00(((BgL_nodez00_bglt)
							BgL_arg1844z00_3594));
				}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztrace_walkz00(void)
	{
		{	/* Trace/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zzast_letz00(469204197L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			BGl_modulezd2initializa7ationz75zztrace_isloopz00(332486922L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2148z00zztrace_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
