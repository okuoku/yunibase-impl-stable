/*===========================================================================*/
/*   (Trace/isloop.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Trace/isloop.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TRACE_ISLOOP_TYPE_DEFINITIONS
#define BGL_TRACE_ISLOOP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;


#endif													// BGL_TRACE_ISLOOP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_tailzf3zf3zztrace_isloopz00(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	static obj_t BGl_z62tailzf3zd2funcall1276z43zztrace_isloopz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztrace_isloopz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_isloopzf3zf3zztrace_isloopz00(BgL_letzd2funzd2_bglt);
	static obj_t BGl_z62tailzf3zd2letzd2var1285z91zztrace_isloopz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zztrace_isloopz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62tailzf3z91zztrace_isloopz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zztrace_isloopz00(void);
	static obj_t BGl_objectzd2initzd2zztrace_isloopz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62tailzf31263z91zztrace_isloopz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zztrace_isloopz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62tailzf3zd2conditional1281z43zztrace_isloopz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62tailzf3zd2setq1279z43zztrace_isloopz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailzf3zd2var1270z43zztrace_isloopz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62isloopzf3z91zztrace_isloopz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zztrace_isloopz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_letz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62tailzf3zd2kwote1268z43zztrace_isloopz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailzf3zd2sequence1272z43zztrace_isloopz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	static obj_t BGl_libraryzd2moduleszd2initz00zztrace_isloopz00(void);
	static obj_t BGl_z62tailzf3zd2letzd2fun1283z91zztrace_isloopz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailzf3zd2app1274z43zztrace_isloopz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zztrace_isloopz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztrace_isloopz00(void);
	static obj_t BGl_z62tailzf3zd2switch1287z43zztrace_isloopz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailzf3zd2extern1289z43zztrace_isloopz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62tailzf3zd2literal1266z43zztrace_isloopz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t *__cnst;


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_tailzf3zd2envz21zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za791za7za7t1777za7,
		BGl_z62tailzf3z91zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1762z00zztrace_isloopz00,
		BgL_bgl_string1762za700za7za7t1778za7, "tail?1263", 9);
	      DEFINE_STRING(BGl_string1764z00zztrace_isloopz00,
		BgL_bgl_string1764za700za7za7t1779za7, "tail?", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1761z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f31263za791780za7,
		BGl_z62tailzf31263z91zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1763z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2lit1781za7,
		BGl_z62tailzf3zd2literal1266z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1765z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2kwo1782za7,
		BGl_z62tailzf3zd2kwote1268z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1766z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2var1783za7,
		BGl_z62tailzf3zd2var1270z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1767z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2seq1784za7,
		BGl_z62tailzf3zd2sequence1272z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1768z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2app1785za7,
		BGl_z62tailzf3zd2app1274z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1769z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2fun1786za7,
		BGl_z62tailzf3zd2funcall1276z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1776z00zztrace_isloopz00,
		BgL_bgl_string1776za700za7za7t1787za7, "trace_isloop", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1770z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2set1788za7,
		BGl_z62tailzf3zd2setq1279z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1771z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2con1789za7,
		BGl_z62tailzf3zd2conditional1281z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1772z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2let1790za7,
		BGl_z62tailzf3zd2letzd2fun1283z91zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1773z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2let1791za7,
		BGl_z62tailzf3zd2letzd2var1285z91zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1774z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2swi1792za7,
		BGl_z62tailzf3zd2switch1287z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1775z00zztrace_isloopz00,
		BgL_bgl_za762tailza7f3za7d2ext1793za7,
		BGl_z62tailzf3zd2extern1289z43zztrace_isloopz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_isloopzf3zd2envz21zztrace_isloopz00,
		BgL_bgl_za762isloopza7f3za791za71794z00,
		BGl_z62isloopzf3z91zztrace_isloopz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztrace_isloopz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztrace_isloopz00(long
		BgL_checksumz00_2241, char *BgL_fromz00_2242)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztrace_isloopz00))
				{
					BGl_requirezd2initializa7ationz75zztrace_isloopz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztrace_isloopz00();
					BGl_libraryzd2moduleszd2initz00zztrace_isloopz00();
					BGl_importedzd2moduleszd2initz00zztrace_isloopz00();
					BGl_genericzd2initzd2zztrace_isloopz00();
					BGl_methodzd2initzd2zztrace_isloopz00();
					return BGl_toplevelzd2initzd2zztrace_isloopz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztrace_isloopz00(void)
	{
		{	/* Trace/isloop.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "trace_isloop");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "trace_isloop");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"trace_isloop");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "trace_isloop");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"trace_isloop");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"trace_isloop");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "trace_isloop");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "trace_isloop");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztrace_isloopz00(void)
	{
		{	/* Trace/isloop.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztrace_isloopz00(void)
	{
		{	/* Trace/isloop.scm 15 */
			return BUNSPEC;
		}

	}



/* isloop? */
	BGL_EXPORTED_DEF bool_t
		BGl_isloopzf3zf3zztrace_isloopz00(BgL_letzd2funzd2_bglt BgL_nodez00_17)
	{
		{	/* Trace/isloop.scm 38 */
			{	/* Trace/isloop.scm 40 */
				bool_t BgL_test1796z00_2261;

				{	/* Trace/isloop.scm 40 */
					bool_t BgL_test1797z00_2262;

					{	/* Trace/isloop.scm 40 */
						obj_t BgL_tmpz00_2263;

						BgL_tmpz00_2263 =
							(((BgL_letzd2funzd2_bglt) COBJECT(BgL_nodez00_17))->
							BgL_localsz00);
						BgL_test1797z00_2262 = PAIRP(BgL_tmpz00_2263);
					}
					if (BgL_test1797z00_2262)
						{	/* Trace/isloop.scm 40 */
							BgL_test1796z00_2261 =
								NULLP(CDR(
									(((BgL_letzd2funzd2_bglt) COBJECT(BgL_nodez00_17))->
										BgL_localsz00)));
						}
					else
						{	/* Trace/isloop.scm 40 */
							BgL_test1796z00_2261 = ((bool_t) 0);
						}
				}
				if (BgL_test1796z00_2261)
					{	/* Trace/isloop.scm 41 */
						bool_t BgL_test1800z00_2269;

						{	/* Trace/isloop.scm 41 */
							BgL_nodez00_bglt BgL_arg1346z00_1547;

							BgL_arg1346z00_1547 =
								(((BgL_letzd2funzd2_bglt) COBJECT(BgL_nodez00_17))->
								BgL_bodyz00);
							{	/* Trace/isloop.scm 41 */
								obj_t BgL_classz00_1950;

								BgL_classz00_1950 = BGl_appz00zzast_nodez00;
								{	/* Trace/isloop.scm 41 */
									BgL_objectz00_bglt BgL_arg1807z00_1952;

									{	/* Trace/isloop.scm 41 */
										obj_t BgL_tmpz00_2271;

										BgL_tmpz00_2271 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1346z00_1547));
										BgL_arg1807z00_1952 =
											(BgL_objectz00_bglt) (BgL_tmpz00_2271);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Trace/isloop.scm 41 */
											long BgL_idxz00_1958;

											BgL_idxz00_1958 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1952);
											BgL_test1800z00_2269 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_1958 + 3L)) == BgL_classz00_1950);
										}
									else
										{	/* Trace/isloop.scm 41 */
											bool_t BgL_res1754z00_1983;

											{	/* Trace/isloop.scm 41 */
												obj_t BgL_oclassz00_1966;

												{	/* Trace/isloop.scm 41 */
													obj_t BgL_arg1815z00_1974;
													long BgL_arg1816z00_1975;

													BgL_arg1815z00_1974 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Trace/isloop.scm 41 */
														long BgL_arg1817z00_1976;

														BgL_arg1817z00_1976 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1952);
														BgL_arg1816z00_1975 =
															(BgL_arg1817z00_1976 - OBJECT_TYPE);
													}
													BgL_oclassz00_1966 =
														VECTOR_REF(BgL_arg1815z00_1974,
														BgL_arg1816z00_1975);
												}
												{	/* Trace/isloop.scm 41 */
													bool_t BgL__ortest_1115z00_1967;

													BgL__ortest_1115z00_1967 =
														(BgL_classz00_1950 == BgL_oclassz00_1966);
													if (BgL__ortest_1115z00_1967)
														{	/* Trace/isloop.scm 41 */
															BgL_res1754z00_1983 = BgL__ortest_1115z00_1967;
														}
													else
														{	/* Trace/isloop.scm 41 */
															long BgL_odepthz00_1968;

															{	/* Trace/isloop.scm 41 */
																obj_t BgL_arg1804z00_1969;

																BgL_arg1804z00_1969 = (BgL_oclassz00_1966);
																BgL_odepthz00_1968 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_1969);
															}
															if ((3L < BgL_odepthz00_1968))
																{	/* Trace/isloop.scm 41 */
																	obj_t BgL_arg1802z00_1971;

																	{	/* Trace/isloop.scm 41 */
																		obj_t BgL_arg1803z00_1972;

																		BgL_arg1803z00_1972 = (BgL_oclassz00_1966);
																		BgL_arg1802z00_1971 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_1972, 3L);
																	}
																	BgL_res1754z00_1983 =
																		(BgL_arg1802z00_1971 == BgL_classz00_1950);
																}
															else
																{	/* Trace/isloop.scm 41 */
																	BgL_res1754z00_1983 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1800z00_2269 = BgL_res1754z00_1983;
										}
								}
							}
						}
						if (BgL_test1800z00_2269)
							{	/* Trace/isloop.scm 42 */
								bool_t BgL_test1804z00_2294;

								{	/* Trace/isloop.scm 42 */
									BgL_nodez00_bglt BgL_arg1340z00_1544;
									obj_t BgL_arg1342z00_1545;

									BgL_arg1340z00_1544 =
										(((BgL_letzd2funzd2_bglt) COBJECT(BgL_nodez00_17))->
										BgL_bodyz00);
									BgL_arg1342z00_1545 =
										CAR((((BgL_letzd2funzd2_bglt) COBJECT(BgL_nodez00_17))->
											BgL_localsz00));
									BgL_test1804z00_2294 =
										CBOOL(BGl_tailzf3zf3zztrace_isloopz00(BgL_arg1340z00_1544,
											((BgL_variablez00_bglt) BgL_arg1342z00_1545), BTRUE));
								}
								if (BgL_test1804z00_2294)
									{	/* Trace/isloop.scm 43 */
										BgL_valuez00_bglt BgL_funz00_1537;

										{
											BgL_variablez00_bglt BgL_auxz00_2301;

											{
												BgL_localz00_bglt BgL_auxz00_2302;

												{	/* Trace/isloop.scm 43 */
													obj_t BgL_pairz00_1985;

													BgL_pairz00_1985 =
														(((BgL_letzd2funzd2_bglt) COBJECT(BgL_nodez00_17))->
														BgL_localsz00);
													BgL_auxz00_2302 =
														((BgL_localz00_bglt) CAR(BgL_pairz00_1985));
												}
												BgL_auxz00_2301 =
													((BgL_variablez00_bglt) BgL_auxz00_2302);
											}
											BgL_funz00_1537 =
												(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_2301))->
												BgL_valuez00);
										}
										{	/* Trace/isloop.scm 45 */
											obj_t BgL_arg1331z00_1539;
											obj_t BgL_arg1332z00_1540;

											BgL_arg1331z00_1539 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_1537)))->
												BgL_bodyz00);
											BgL_arg1332z00_1540 =
												CAR((((BgL_letzd2funzd2_bglt) COBJECT(BgL_nodez00_17))->
													BgL_localsz00));
											return
												CBOOL(BGl_tailzf3zf3zztrace_isloopz00((
														(BgL_nodez00_bglt) BgL_arg1331z00_1539),
													((BgL_variablez00_bglt) BgL_arg1332z00_1540), BTRUE));
										}
									}
								else
									{	/* Trace/isloop.scm 42 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Trace/isloop.scm 41 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Trace/isloop.scm 40 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &isloop? */
	obj_t BGl_z62isloopzf3z91zztrace_isloopz00(obj_t BgL_envz00_2079,
		obj_t BgL_nodez00_2080)
	{
		{	/* Trace/isloop.scm 38 */
			return
				BBOOL(BGl_isloopzf3zf3zztrace_isloopz00(
					((BgL_letzd2funzd2_bglt) BgL_nodez00_2080)));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztrace_isloopz00(void)
	{
		{	/* Trace/isloop.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztrace_isloopz00(void)
	{
		{	/* Trace/isloop.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00,
				BGl_proc1761z00zztrace_isloopz00, BGl_nodez00zzast_nodez00,
				BGl_string1762z00zztrace_isloopz00);
		}

	}



/* &tail?1263 */
	obj_t BGl_z62tailzf31263z91zztrace_isloopz00(obj_t BgL_envz00_2082,
		obj_t BgL_nodez00_2083, obj_t BgL_funz00_2084, obj_t BgL_tailpz00_2085)
	{
		{	/* Trace/isloop.scm 50 */
			return BBOOL(((bool_t) 0));
		}

	}



/* tail? */
	obj_t BGl_tailzf3zf3zztrace_isloopz00(BgL_nodez00_bglt BgL_nodez00_18,
		BgL_variablez00_bglt BgL_funz00_19, obj_t BgL_tailpz00_20)
	{
		{	/* Trace/isloop.scm 50 */
			{	/* Trace/isloop.scm 50 */
				obj_t BgL_method1264z00_1558;

				{	/* Trace/isloop.scm 50 */
					obj_t BgL_res1759z00_2018;

					{	/* Trace/isloop.scm 50 */
						long BgL_objzd2classzd2numz00_1989;

						BgL_objzd2classzd2numz00_1989 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_18));
						{	/* Trace/isloop.scm 50 */
							obj_t BgL_arg1811z00_1990;

							BgL_arg1811z00_1990 =
								PROCEDURE_REF(BGl_tailzf3zd2envz21zztrace_isloopz00,
								(int) (1L));
							{	/* Trace/isloop.scm 50 */
								int BgL_offsetz00_1993;

								BgL_offsetz00_1993 = (int) (BgL_objzd2classzd2numz00_1989);
								{	/* Trace/isloop.scm 50 */
									long BgL_offsetz00_1994;

									BgL_offsetz00_1994 =
										((long) (BgL_offsetz00_1993) - OBJECT_TYPE);
									{	/* Trace/isloop.scm 50 */
										long BgL_modz00_1995;

										BgL_modz00_1995 =
											(BgL_offsetz00_1994 >> (int) ((long) ((int) (4L))));
										{	/* Trace/isloop.scm 50 */
											long BgL_restz00_1997;

											BgL_restz00_1997 =
												(BgL_offsetz00_1994 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Trace/isloop.scm 50 */

												{	/* Trace/isloop.scm 50 */
													obj_t BgL_bucketz00_1999;

													BgL_bucketz00_1999 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1990), BgL_modz00_1995);
													BgL_res1759z00_2018 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1999), BgL_restz00_1997);
					}}}}}}}}
					BgL_method1264z00_1558 = BgL_res1759z00_2018;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1264z00_1558,
					((obj_t) BgL_nodez00_18), ((obj_t) BgL_funz00_19), BgL_tailpz00_20);
			}
		}

	}



/* &tail? */
	obj_t BGl_z62tailzf3z91zztrace_isloopz00(obj_t BgL_envz00_2086,
		obj_t BgL_nodez00_2087, obj_t BgL_funz00_2088, obj_t BgL_tailpz00_2089)
	{
		{	/* Trace/isloop.scm 50 */
			return
				BGl_tailzf3zf3zztrace_isloopz00(
				((BgL_nodez00_bglt) BgL_nodez00_2087),
				((BgL_variablez00_bglt) BgL_funz00_2088), BgL_tailpz00_2089);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztrace_isloopz00(void)
	{
		{	/* Trace/isloop.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_literalz00zzast_nodez00,
				BGl_proc1763z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_kwotez00zzast_nodez00,
				BGl_proc1765z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_varz00zzast_nodez00,
				BGl_proc1766z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_sequencez00zzast_nodez00,
				BGl_proc1767z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_appz00zzast_nodez00,
				BGl_proc1768z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_funcallz00zzast_nodez00,
				BGl_proc1769z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_setqz00zzast_nodez00,
				BGl_proc1770z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc1771z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc1772z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1773z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_switchz00zzast_nodez00,
				BGl_proc1774z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailzf3zd2envz21zztrace_isloopz00, BGl_externz00zzast_nodez00,
				BGl_proc1775z00zztrace_isloopz00, BGl_string1764z00zztrace_isloopz00);
		}

	}



/* &tail?-extern1289 */
	obj_t BGl_z62tailzf3zd2extern1289z43zztrace_isloopz00(obj_t BgL_envz00_2102,
		obj_t BgL_nodez00_2103, obj_t BgL_funz00_2104, obj_t BgL_tailpz00_2105)
	{
		{	/* Trace/isloop.scm 150 */
			{	/* Trace/isloop.scm 151 */
				bool_t BgL_tmpz00_2369;

				{	/* Trace/isloop.scm 152 */
					obj_t BgL_g1261z00_2154;

					BgL_g1261z00_2154 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2103)))->BgL_exprza2za2);
					{
						obj_t BgL_l1259z00_2156;

						BgL_l1259z00_2156 = BgL_g1261z00_2154;
					BgL_zc3z04anonymousza31607ze3z87_2155:
						if (NULLP(BgL_l1259z00_2156))
							{	/* Trace/isloop.scm 152 */
								BgL_tmpz00_2369 = ((bool_t) 1);
							}
						else
							{	/* Trace/isloop.scm 152 */
								obj_t BgL_nvz00_2157;

								{	/* Trace/isloop.scm 152 */
									obj_t BgL_ez00_2158;

									BgL_ez00_2158 = CAR(((obj_t) BgL_l1259z00_2156));
									BgL_nvz00_2157 =
										BGl_tailzf3zf3zztrace_isloopz00(
										((BgL_nodez00_bglt) BgL_ez00_2158),
										((BgL_variablez00_bglt) BgL_funz00_2104), BFALSE);
								}
								if (CBOOL(BgL_nvz00_2157))
									{	/* Trace/isloop.scm 152 */
										obj_t BgL_arg1609z00_2159;

										BgL_arg1609z00_2159 = CDR(((obj_t) BgL_l1259z00_2156));
										{
											obj_t BgL_l1259z00_2383;

											BgL_l1259z00_2383 = BgL_arg1609z00_2159;
											BgL_l1259z00_2156 = BgL_l1259z00_2383;
											goto BgL_zc3z04anonymousza31607ze3z87_2155;
										}
									}
								else
									{	/* Trace/isloop.scm 152 */
										BgL_tmpz00_2369 = ((bool_t) 0);
									}
							}
					}
				}
				return BBOOL(BgL_tmpz00_2369);
			}
		}

	}



/* &tail?-switch1287 */
	obj_t BGl_z62tailzf3zd2switch1287z43zztrace_isloopz00(obj_t BgL_envz00_2106,
		obj_t BgL_nodez00_2107, obj_t BgL_funz00_2108, obj_t BgL_tailpz00_2109)
	{
		{	/* Trace/isloop.scm 142 */
			{	/* Trace/isloop.scm 143 */
				bool_t BgL_tmpz00_2385;

				{	/* Trace/isloop.scm 144 */
					bool_t BgL_test1810z00_2386;

					{	/* Trace/isloop.scm 144 */
						BgL_nodez00_bglt BgL_arg1606z00_2161;

						BgL_arg1606z00_2161 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_2107)))->BgL_testz00);
						BgL_test1810z00_2386 =
							CBOOL(BGl_tailzf3zf3zztrace_isloopz00(BgL_arg1606z00_2161,
								((BgL_variablez00_bglt) BgL_funz00_2108), BFALSE));
					}
					if (BgL_test1810z00_2386)
						{	/* Trace/isloop.scm 145 */
							obj_t BgL_g1257z00_2162;

							BgL_g1257z00_2162 =
								(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_2107)))->BgL_clausesz00);
							{
								obj_t BgL_l1255z00_2164;

								BgL_l1255z00_2164 = BgL_g1257z00_2162;
							BgL_zc3z04anonymousza31596ze3z87_2163:
								if (NULLP(BgL_l1255z00_2164))
									{	/* Trace/isloop.scm 145 */
										BgL_tmpz00_2385 = ((bool_t) 1);
									}
								else
									{	/* Trace/isloop.scm 145 */
										obj_t BgL_nvz00_2165;

										{	/* Trace/isloop.scm 145 */
											obj_t BgL_cz00_2166;

											BgL_cz00_2166 = CAR(((obj_t) BgL_l1255z00_2164));
											{	/* Trace/isloop.scm 145 */
												obj_t BgL_arg1605z00_2167;

												BgL_arg1605z00_2167 = CDR(((obj_t) BgL_cz00_2166));
												BgL_nvz00_2165 =
													BGl_tailzf3zf3zztrace_isloopz00(
													((BgL_nodez00_bglt) BgL_arg1605z00_2167),
													((BgL_variablez00_bglt) BgL_funz00_2108),
													BgL_tailpz00_2109);
											}
										}
										if (CBOOL(BgL_nvz00_2165))
											{	/* Trace/isloop.scm 145 */
												obj_t BgL_arg1602z00_2168;

												BgL_arg1602z00_2168 = CDR(((obj_t) BgL_l1255z00_2164));
												{
													obj_t BgL_l1255z00_2407;

													BgL_l1255z00_2407 = BgL_arg1602z00_2168;
													BgL_l1255z00_2164 = BgL_l1255z00_2407;
													goto BgL_zc3z04anonymousza31596ze3z87_2163;
												}
											}
										else
											{	/* Trace/isloop.scm 145 */
												BgL_tmpz00_2385 = ((bool_t) 0);
											}
									}
							}
						}
					else
						{	/* Trace/isloop.scm 144 */
							BgL_tmpz00_2385 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_2385);
			}
		}

	}



/* &tail?-let-var1285 */
	obj_t BGl_z62tailzf3zd2letzd2var1285z91zztrace_isloopz00(obj_t
		BgL_envz00_2110, obj_t BgL_nodez00_2111, obj_t BgL_funz00_2112,
		obj_t BgL_tailpz00_2113)
	{
		{	/* Trace/isloop.scm 134 */
			{	/* Trace/isloop.scm 136 */
				bool_t BgL_test1813z00_2409;

				{	/* Trace/isloop.scm 136 */
					obj_t BgL_g1253z00_2170;

					BgL_g1253z00_2170 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2111)))->BgL_bindingsz00);
					{
						obj_t BgL_l1251z00_2172;

						BgL_l1251z00_2172 = BgL_g1253z00_2170;
					BgL_zc3z04anonymousza31590ze3z87_2171:
						if (NULLP(BgL_l1251z00_2172))
							{	/* Trace/isloop.scm 136 */
								BgL_test1813z00_2409 = ((bool_t) 1);
							}
						else
							{	/* Trace/isloop.scm 136 */
								obj_t BgL_nvz00_2173;

								{	/* Trace/isloop.scm 136 */
									obj_t BgL_bz00_2174;

									BgL_bz00_2174 = CAR(((obj_t) BgL_l1251z00_2172));
									{	/* Trace/isloop.scm 136 */
										obj_t BgL_arg1593z00_2175;

										BgL_arg1593z00_2175 = CDR(((obj_t) BgL_bz00_2174));
										BgL_nvz00_2173 =
											BGl_tailzf3zf3zztrace_isloopz00(
											((BgL_nodez00_bglt) BgL_arg1593z00_2175),
											((BgL_variablez00_bglt) BgL_funz00_2112), BFALSE);
									}
								}
								if (CBOOL(BgL_nvz00_2173))
									{	/* Trace/isloop.scm 136 */
										obj_t BgL_arg1591z00_2176;

										BgL_arg1591z00_2176 = CDR(((obj_t) BgL_l1251z00_2172));
										{
											obj_t BgL_l1251z00_2425;

											BgL_l1251z00_2425 = BgL_arg1591z00_2176;
											BgL_l1251z00_2172 = BgL_l1251z00_2425;
											goto BgL_zc3z04anonymousza31590ze3z87_2171;
										}
									}
								else
									{	/* Trace/isloop.scm 136 */
										BgL_test1813z00_2409 = ((bool_t) 0);
									}
							}
					}
				}
				if (BgL_test1813z00_2409)
					{	/* Trace/isloop.scm 137 */
						BgL_nodez00_bglt BgL_arg1589z00_2177;

						BgL_arg1589z00_2177 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2111)))->BgL_bodyz00);
						return
							BGl_tailzf3zf3zztrace_isloopz00(BgL_arg1589z00_2177,
							((BgL_variablez00_bglt) BgL_funz00_2112), BgL_tailpz00_2113);
					}
				else
					{	/* Trace/isloop.scm 136 */
						return BFALSE;
					}
			}
		}

	}



/* &tail?-let-fun1283 */
	obj_t BGl_z62tailzf3zd2letzd2fun1283z91zztrace_isloopz00(obj_t
		BgL_envz00_2114, obj_t BgL_nodez00_2115, obj_t BgL_funz00_2116,
		obj_t BgL_tailpz00_2117)
	{
		{	/* Trace/isloop.scm 120 */
			{	/* Trace/isloop.scm 121 */
				bool_t BgL_tmpz00_2430;

				{	/* Trace/isloop.scm 122 */
					bool_t BgL_test1816z00_2431;

					{	/* Trace/isloop.scm 122 */
						BgL_nodez00_bglt BgL_arg1573z00_2179;

						BgL_arg1573z00_2179 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_2115)))->BgL_bodyz00);
						BgL_test1816z00_2431 =
							CBOOL(BGl_tailzf3zf3zztrace_isloopz00(BgL_arg1573z00_2179,
								((BgL_variablez00_bglt) BgL_funz00_2116), BgL_tailpz00_2117));
					}
					if (BgL_test1816z00_2431)
						{	/* Trace/isloop.scm 122 */
							if (BGl_isloopzf3zf3zztrace_isloopz00(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_2115)))
								{	/* Trace/isloop.scm 124 */
									BgL_sfunz00_bglt BgL_i1117z00_2180;

									{
										BgL_valuez00_bglt BgL_auxz00_2440;

										{
											BgL_variablez00_bglt BgL_auxz00_2441;

											{
												BgL_localz00_bglt BgL_auxz00_2442;

												{	/* Trace/isloop.scm 124 */
													obj_t BgL_pairz00_2181;

													BgL_pairz00_2181 =
														(((BgL_letzd2funzd2_bglt) COBJECT(
																((BgL_letzd2funzd2_bglt) BgL_nodez00_2115)))->
														BgL_localsz00);
													BgL_auxz00_2442 =
														((BgL_localz00_bglt) CAR(BgL_pairz00_2181));
												}
												BgL_auxz00_2441 =
													((BgL_variablez00_bglt) BgL_auxz00_2442);
											}
											BgL_auxz00_2440 =
												(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_2441))->
												BgL_valuez00);
										}
										BgL_i1117z00_2180 = ((BgL_sfunz00_bglt) BgL_auxz00_2440);
									}
									{	/* Trace/isloop.scm 125 */
										bool_t BgL_test1818z00_2450;

										{	/* Trace/isloop.scm 125 */
											obj_t BgL_arg1564z00_2182;

											BgL_arg1564z00_2182 =
												(((BgL_sfunz00_bglt) COBJECT(BgL_i1117z00_2180))->
												BgL_bodyz00);
											BgL_test1818z00_2450 =
												CBOOL(BGl_tailzf3zf3zztrace_isloopz00((
														(BgL_nodez00_bglt) BgL_arg1564z00_2182),
													((BgL_variablez00_bglt) BgL_funz00_2116),
													BgL_tailpz00_2117));
										}
										if (BgL_test1818z00_2450)
											{	/* Trace/isloop.scm 126 */
												obj_t BgL_g1249z00_2183;

												BgL_g1249z00_2183 =
													(((BgL_letzd2funzd2_bglt) COBJECT(
															((BgL_letzd2funzd2_bglt) BgL_nodez00_2115)))->
													BgL_localsz00);
												{
													obj_t BgL_l1247z00_2185;

													BgL_l1247z00_2185 = BgL_g1249z00_2183;
												BgL_zc3z04anonymousza31555ze3z87_2184:
													if (NULLP(BgL_l1247z00_2185))
														{	/* Trace/isloop.scm 126 */
															BgL_tmpz00_2430 = ((bool_t) 1);
														}
													else
														{	/* Trace/isloop.scm 127 */
															obj_t BgL_nvz00_2186;

															{	/* Trace/isloop.scm 127 */
																obj_t BgL_lz00_2187;

																BgL_lz00_2187 =
																	CAR(((obj_t) BgL_l1247z00_2185));
																{	/* Trace/isloop.scm 127 */
																	BgL_sfunz00_bglt BgL_i1118z00_2188;

																	BgL_i1118z00_2188 =
																		((BgL_sfunz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_lz00_2187))))->BgL_valuez00));
																	{	/* Trace/isloop.scm 128 */
																		obj_t BgL_arg1561z00_2189;

																		BgL_arg1561z00_2189 =
																			(((BgL_sfunz00_bglt)
																				COBJECT(BgL_i1118z00_2188))->
																			BgL_bodyz00);
																		BgL_nvz00_2186 =
																			BGl_tailzf3zf3zztrace_isloopz00((
																				(BgL_nodez00_bglt) BgL_arg1561z00_2189),
																			((BgL_variablez00_bglt) BgL_funz00_2116),
																			BFALSE);
																	}
																}
															}
															if (CBOOL(BgL_nvz00_2186))
																{	/* Trace/isloop.scm 126 */
																	obj_t BgL_arg1559z00_2190;

																	BgL_arg1559z00_2190 =
																		CDR(((obj_t) BgL_l1247z00_2185));
																	{
																		obj_t BgL_l1247z00_2474;

																		BgL_l1247z00_2474 = BgL_arg1559z00_2190;
																		BgL_l1247z00_2185 = BgL_l1247z00_2474;
																		goto BgL_zc3z04anonymousza31555ze3z87_2184;
																	}
																}
															else
																{	/* Trace/isloop.scm 126 */
																	BgL_tmpz00_2430 = ((bool_t) 0);
																}
														}
												}
											}
										else
											{	/* Trace/isloop.scm 125 */
												BgL_tmpz00_2430 = ((bool_t) 0);
											}
									}
								}
							else
								{	/* Trace/isloop.scm 123 */
									BgL_tmpz00_2430 = ((bool_t) 0);
								}
						}
					else
						{	/* Trace/isloop.scm 122 */
							BgL_tmpz00_2430 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_2430);
			}
		}

	}



/* &tail?-conditional1281 */
	obj_t BGl_z62tailzf3zd2conditional1281z43zztrace_isloopz00(obj_t
		BgL_envz00_2118, obj_t BgL_nodez00_2119, obj_t BgL_funz00_2120,
		obj_t BgL_tailpz00_2121)
	{
		{	/* Trace/isloop.scm 112 */
			{	/* Trace/isloop.scm 114 */
				bool_t BgL_test1822z00_2476;

				{	/* Trace/isloop.scm 114 */
					BgL_nodez00_bglt BgL_arg1540z00_2192;

					BgL_arg1540z00_2192 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2119)))->BgL_testz00);
					BgL_test1822z00_2476 =
						CBOOL(BGl_tailzf3zf3zztrace_isloopz00(BgL_arg1540z00_2192,
							((BgL_variablez00_bglt) BgL_funz00_2120), BFALSE));
				}
				if (BgL_test1822z00_2476)
					{	/* Trace/isloop.scm 115 */
						obj_t BgL__andtest_1115z00_2193;

						{	/* Trace/isloop.scm 115 */
							BgL_nodez00_bglt BgL_arg1535z00_2194;

							BgL_arg1535z00_2194 =
								(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_2119)))->
								BgL_truez00);
							BgL__andtest_1115z00_2193 =
								BGl_tailzf3zf3zztrace_isloopz00(BgL_arg1535z00_2194,
								((BgL_variablez00_bglt) BgL_funz00_2120), BgL_tailpz00_2121);
						}
						if (CBOOL(BgL__andtest_1115z00_2193))
							{	/* Trace/isloop.scm 115 */
								BgL_nodez00_bglt BgL_arg1516z00_2195;

								BgL_arg1516z00_2195 =
									(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_2119)))->
									BgL_falsez00);
								return BGl_tailzf3zf3zztrace_isloopz00(BgL_arg1516z00_2195,
									((BgL_variablez00_bglt) BgL_funz00_2120), BgL_tailpz00_2121);
							}
						else
							{	/* Trace/isloop.scm 115 */
								return BFALSE;
							}
					}
				else
					{	/* Trace/isloop.scm 114 */
						return BFALSE;
					}
			}
		}

	}



/* &tail?-setq1279 */
	obj_t BGl_z62tailzf3zd2setq1279z43zztrace_isloopz00(obj_t BgL_envz00_2122,
		obj_t BgL_nodez00_2123, obj_t BgL_funz00_2124, obj_t BgL_tailpz00_2125)
	{
		{	/* Trace/isloop.scm 105 */
			{	/* Trace/isloop.scm 107 */
				BgL_nodez00_bglt BgL_arg1509z00_2197;

				BgL_arg1509z00_2197 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2123)))->BgL_valuez00);
				return
					BGl_tailzf3zf3zztrace_isloopz00(BgL_arg1509z00_2197,
					((BgL_variablez00_bglt) BgL_funz00_2124), BFALSE);
			}
		}

	}



/* &tail?-funcall1276 */
	obj_t BGl_z62tailzf3zd2funcall1276z43zztrace_isloopz00(obj_t BgL_envz00_2126,
		obj_t BgL_nodez00_2127, obj_t BgL_funz00_2128, obj_t BgL_tailpz00_2129)
	{
		{	/* Trace/isloop.scm 97 */
			{	/* Trace/isloop.scm 99 */
				bool_t BgL_test1825z00_2496;

				{	/* Trace/isloop.scm 99 */
					obj_t BgL_g1245z00_2199;

					BgL_g1245z00_2199 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2127)))->BgL_argsz00);
					{
						obj_t BgL_l1243z00_2201;

						BgL_l1243z00_2201 = BgL_g1245z00_2199;
					BgL_zc3z04anonymousza31490ze3z87_2200:
						if (NULLP(BgL_l1243z00_2201))
							{	/* Trace/isloop.scm 99 */
								BgL_test1825z00_2496 = ((bool_t) 1);
							}
						else
							{	/* Trace/isloop.scm 99 */
								obj_t BgL_nvz00_2202;

								{	/* Trace/isloop.scm 99 */
									obj_t BgL_argz00_2203;

									BgL_argz00_2203 = CAR(((obj_t) BgL_l1243z00_2201));
									BgL_nvz00_2202 =
										BGl_tailzf3zf3zztrace_isloopz00(
										((BgL_nodez00_bglt) BgL_argz00_2203),
										((BgL_variablez00_bglt) BgL_funz00_2128), BFALSE);
								}
								if (CBOOL(BgL_nvz00_2202))
									{	/* Trace/isloop.scm 99 */
										obj_t BgL_arg1502z00_2204;

										BgL_arg1502z00_2204 = CDR(((obj_t) BgL_l1243z00_2201));
										{
											obj_t BgL_l1243z00_2510;

											BgL_l1243z00_2510 = BgL_arg1502z00_2204;
											BgL_l1243z00_2201 = BgL_l1243z00_2510;
											goto BgL_zc3z04anonymousza31490ze3z87_2200;
										}
									}
								else
									{	/* Trace/isloop.scm 99 */
										BgL_test1825z00_2496 = ((bool_t) 0);
									}
							}
					}
				}
				if (BgL_test1825z00_2496)
					{	/* Trace/isloop.scm 100 */
						BgL_nodez00_bglt BgL_arg1489z00_2205;

						BgL_arg1489z00_2205 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_2127)))->BgL_funz00);
						return
							BGl_tailzf3zf3zztrace_isloopz00(BgL_arg1489z00_2205,
							((BgL_variablez00_bglt) BgL_funz00_2128), BFALSE);
					}
				else
					{	/* Trace/isloop.scm 99 */
						return BFALSE;
					}
			}
		}

	}



/* &tail?-app1274 */
	obj_t BGl_z62tailzf3zd2app1274z43zztrace_isloopz00(obj_t BgL_envz00_2130,
		obj_t BgL_nodez00_2131, obj_t BgL_funz00_2132, obj_t BgL_tailpz00_2133)
	{
		{	/* Trace/isloop.scm 86 */
			{	/* Trace/isloop.scm 88 */
				bool_t BgL_test1828z00_2515;

				{	/* Trace/isloop.scm 88 */
					obj_t BgL_g1241z00_2207;

					BgL_g1241z00_2207 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2131)))->BgL_argsz00);
					{
						obj_t BgL_l1239z00_2209;

						BgL_l1239z00_2209 = BgL_g1241z00_2207;
					BgL_zc3z04anonymousza31454ze3z87_2208:
						if (NULLP(BgL_l1239z00_2209))
							{	/* Trace/isloop.scm 88 */
								BgL_test1828z00_2515 = ((bool_t) 1);
							}
						else
							{	/* Trace/isloop.scm 88 */
								obj_t BgL_nvz00_2210;

								{	/* Trace/isloop.scm 88 */
									obj_t BgL_argz00_2211;

									BgL_argz00_2211 = CAR(((obj_t) BgL_l1239z00_2209));
									BgL_nvz00_2210 =
										BGl_tailzf3zf3zztrace_isloopz00(
										((BgL_nodez00_bglt) BgL_argz00_2211),
										((BgL_variablez00_bglt) BgL_funz00_2132), BFALSE);
								}
								if (CBOOL(BgL_nvz00_2210))
									{	/* Trace/isloop.scm 88 */
										obj_t BgL_arg1472z00_2212;

										BgL_arg1472z00_2212 = CDR(((obj_t) BgL_l1239z00_2209));
										{
											obj_t BgL_l1239z00_2529;

											BgL_l1239z00_2529 = BgL_arg1472z00_2212;
											BgL_l1239z00_2209 = BgL_l1239z00_2529;
											goto BgL_zc3z04anonymousza31454ze3z87_2208;
										}
									}
								else
									{	/* Trace/isloop.scm 88 */
										BgL_test1828z00_2515 = ((bool_t) 0);
									}
							}
					}
				}
				if (BgL_test1828z00_2515)
					{	/* Trace/isloop.scm 89 */
						bool_t BgL_test1832z00_2530;

						{	/* Trace/isloop.scm 89 */
							BgL_varz00_bglt BgL_arg1453z00_2213;

							BgL_arg1453z00_2213 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2131)))->BgL_funz00);
							{	/* Trace/isloop.scm 89 */
								obj_t BgL_classz00_2214;

								BgL_classz00_2214 = BGl_varz00zzast_nodez00;
								{	/* Trace/isloop.scm 89 */
									BgL_objectz00_bglt BgL_arg1807z00_2215;

									{	/* Trace/isloop.scm 89 */
										obj_t BgL_tmpz00_2533;

										BgL_tmpz00_2533 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1453z00_2213));
										BgL_arg1807z00_2215 =
											(BgL_objectz00_bglt) (BgL_tmpz00_2533);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Trace/isloop.scm 89 */
											long BgL_idxz00_2216;

											BgL_idxz00_2216 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2215);
											BgL_test1832z00_2530 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2216 + 2L)) == BgL_classz00_2214);
										}
									else
										{	/* Trace/isloop.scm 89 */
											bool_t BgL_res1760z00_2219;

											{	/* Trace/isloop.scm 89 */
												obj_t BgL_oclassz00_2220;

												{	/* Trace/isloop.scm 89 */
													obj_t BgL_arg1815z00_2221;
													long BgL_arg1816z00_2222;

													BgL_arg1815z00_2221 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Trace/isloop.scm 89 */
														long BgL_arg1817z00_2223;

														BgL_arg1817z00_2223 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2215);
														BgL_arg1816z00_2222 =
															(BgL_arg1817z00_2223 - OBJECT_TYPE);
													}
													BgL_oclassz00_2220 =
														VECTOR_REF(BgL_arg1815z00_2221,
														BgL_arg1816z00_2222);
												}
												{	/* Trace/isloop.scm 89 */
													bool_t BgL__ortest_1115z00_2224;

													BgL__ortest_1115z00_2224 =
														(BgL_classz00_2214 == BgL_oclassz00_2220);
													if (BgL__ortest_1115z00_2224)
														{	/* Trace/isloop.scm 89 */
															BgL_res1760z00_2219 = BgL__ortest_1115z00_2224;
														}
													else
														{	/* Trace/isloop.scm 89 */
															long BgL_odepthz00_2225;

															{	/* Trace/isloop.scm 89 */
																obj_t BgL_arg1804z00_2226;

																BgL_arg1804z00_2226 = (BgL_oclassz00_2220);
																BgL_odepthz00_2225 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2226);
															}
															if ((2L < BgL_odepthz00_2225))
																{	/* Trace/isloop.scm 89 */
																	obj_t BgL_arg1802z00_2227;

																	{	/* Trace/isloop.scm 89 */
																		obj_t BgL_arg1803z00_2228;

																		BgL_arg1803z00_2228 = (BgL_oclassz00_2220);
																		BgL_arg1802z00_2227 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2228, 2L);
																	}
																	BgL_res1760z00_2219 =
																		(BgL_arg1802z00_2227 == BgL_classz00_2214);
																}
															else
																{	/* Trace/isloop.scm 89 */
																	BgL_res1760z00_2219 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1832z00_2530 = BgL_res1760z00_2219;
										}
								}
							}
						}
						if (BgL_test1832z00_2530)
							{	/* Trace/isloop.scm 91 */
								bool_t BgL__ortest_1111z00_2229;

								if (
									(((obj_t)
											(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_2131)))->
												BgL_funz00)) == BgL_funz00_2132))
									{	/* Trace/isloop.scm 91 */
										BgL__ortest_1111z00_2229 = ((bool_t) 0);
									}
								else
									{	/* Trace/isloop.scm 91 */
										BgL__ortest_1111z00_2229 = ((bool_t) 1);
									}
								if (BgL__ortest_1111z00_2229)
									{	/* Trace/isloop.scm 91 */
										return BBOOL(BgL__ortest_1111z00_2229);
									}
								else
									{	/* Trace/isloop.scm 91 */
										return BgL_tailpz00_2133;
									}
							}
						else
							{	/* Trace/isloop.scm 92 */
								BgL_varz00_bglt BgL_arg1448z00_2230;

								BgL_arg1448z00_2230 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_2131)))->BgL_funz00);
								return
									BGl_tailzf3zf3zztrace_isloopz00(
									((BgL_nodez00_bglt) BgL_arg1448z00_2230),
									((BgL_variablez00_bglt) BgL_funz00_2132), BFALSE);
							}
					}
				else
					{	/* Trace/isloop.scm 88 */
						return BFALSE;
					}
			}
		}

	}



/* &tail?-sequence1272 */
	obj_t BGl_z62tailzf3zd2sequence1272z43zztrace_isloopz00(obj_t BgL_envz00_2134,
		obj_t BgL_nodez00_2135, obj_t BgL_funz00_2136, obj_t BgL_tailpz00_2137)
	{
		{	/* Trace/isloop.scm 75 */
			{
				obj_t BgL_nodesz00_2233;

				BgL_nodesz00_2233 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2135)))->BgL_nodesz00);
			BgL_loopz00_2232:
				if (NULLP(BgL_nodesz00_2233))
					{	/* Trace/isloop.scm 79 */
						return BTRUE;
					}
				else
					{	/* Trace/isloop.scm 79 */
						if (NULLP(CDR(((obj_t) BgL_nodesz00_2233))))
							{	/* Trace/isloop.scm 80 */
								obj_t BgL_arg1375z00_2234;

								BgL_arg1375z00_2234 = CAR(((obj_t) BgL_nodesz00_2233));
								return
									BGl_tailzf3zf3zztrace_isloopz00(
									((BgL_nodez00_bglt) BgL_arg1375z00_2234),
									((BgL_variablez00_bglt) BgL_funz00_2136), BTRUE);
							}
						else
							{	/* Trace/isloop.scm 81 */
								obj_t BgL__andtest_1108z00_2235;

								{	/* Trace/isloop.scm 81 */
									obj_t BgL_arg1377z00_2236;

									BgL_arg1377z00_2236 = CAR(((obj_t) BgL_nodesz00_2233));
									BgL__andtest_1108z00_2235 =
										BGl_tailzf3zf3zztrace_isloopz00(
										((BgL_nodez00_bglt) BgL_arg1377z00_2236),
										((BgL_variablez00_bglt) BgL_funz00_2136), BFALSE);
								}
								if (CBOOL(BgL__andtest_1108z00_2235))
									{	/* Trace/isloop.scm 81 */
										obj_t BgL_arg1376z00_2237;

										BgL_arg1376z00_2237 = CDR(((obj_t) BgL_nodesz00_2233));
										{
											obj_t BgL_nodesz00_2588;

											BgL_nodesz00_2588 = BgL_arg1376z00_2237;
											BgL_nodesz00_2233 = BgL_nodesz00_2588;
											goto BgL_loopz00_2232;
										}
									}
								else
									{	/* Trace/isloop.scm 81 */
										return BFALSE;
									}
							}
					}
			}
		}

	}



/* &tail?-var1270 */
	obj_t BGl_z62tailzf3zd2var1270z43zztrace_isloopz00(obj_t BgL_envz00_2138,
		obj_t BgL_nodez00_2139, obj_t BgL_funz00_2140, obj_t BgL_tailpz00_2141)
	{
		{	/* Trace/isloop.scm 68 */
			{	/* Trace/isloop.scm 70 */
				bool_t BgL_tmpz00_2591;

				if (
					(((obj_t)
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_2139)))->BgL_variablez00)) ==
						BgL_funz00_2140))
					{	/* Trace/isloop.scm 70 */
						BgL_tmpz00_2591 = ((bool_t) 0);
					}
				else
					{	/* Trace/isloop.scm 70 */
						BgL_tmpz00_2591 = ((bool_t) 1);
					}
				return BBOOL(BgL_tmpz00_2591);
			}
		}

	}



/* &tail?-kwote1268 */
	obj_t BGl_z62tailzf3zd2kwote1268z43zztrace_isloopz00(obj_t BgL_envz00_2142,
		obj_t BgL_nodez00_2143, obj_t BgL_funz00_2144, obj_t BgL_tailpz00_2145)
	{
		{	/* Trace/isloop.scm 62 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &tail?-literal1266 */
	obj_t BGl_z62tailzf3zd2literal1266z43zztrace_isloopz00(obj_t BgL_envz00_2146,
		obj_t BgL_nodez00_2147, obj_t BgL_funz00_2148, obj_t BgL_tailpz00_2149)
	{
		{	/* Trace/isloop.scm 56 */
			return BBOOL(((bool_t) 1));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztrace_isloopz00(void)
	{
		{	/* Trace/isloop.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zzast_letz00(469204197L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1776z00zztrace_isloopz00));
		}

	}

#ifdef __cplusplus
}
#endif
