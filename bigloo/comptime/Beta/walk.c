/*===========================================================================*/
/*   (Beta/walk.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Beta/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BETA_WALK_TYPE_DEFINITIONS
#define BGL_BETA_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_BETA_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzbeta_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2condition1307z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2extern1301z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2switch1311z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzbeta_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2boxzd2setz121317zb0zzbeta_walkz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2makezd2box1313za2zzbeta_walkz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_betazd2walkz12zc0zzbeta_walkz00(obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzbeta_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2appzd2ly1297za2zzbeta_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzbeta_walkz00(void);
	static obj_t BGl_nodezd2betaza2z12z62zzbeta_walkz00(obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_nodezd2betaz12zc0zzbeta_walkz00(BgL_nodez00_bglt,
		obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62betazd2walkz12za2zzbeta_walkz00(obj_t, obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2letzd2fun1319za2zzbeta_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62nodezd2betaz12za2zzbeta_walkz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbeta_walkz00(void);
	static obj_t BGl_z62nodezd2betaz121288za2zzbeta_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2jumpzd2exzd2i1325z70zzbeta_walkz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	BGL_IMPORT obj_t bgl_typeof(obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2cast1303z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2setzd2exzd2it1323z70zzbeta_walkz00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2setq1305z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2fail1309z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static bool_t BGl_betazd2globalsz12zc0zzbeta_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzbeta_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2app1295z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2sequence1291z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzbeta_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbeta_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2letzd2var1321za2zzbeta_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzbeta_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzbeta_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2boxzd2ref1315za2zzbeta_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2sync1293z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2funcall1299z70zzbeta_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	extern obj_t BGl_patchz00zzast_nodez00;
	static obj_t __cnst[3];


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza711984za7,
		BGl_z62nodezd2betaz12za2zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_betazd2walkz12zd2envz12zzbeta_walkz00,
		BgL_bgl_za762betaza7d2walkza711985za7,
		BGl_z62betazd2walkz12za2zzbeta_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1951z00zzbeta_walkz00,
		BgL_bgl_string1951za700za7za7b1986za7, "Constant Beta", 13);
	      DEFINE_STRING(BGl_string1952z00zzbeta_walkz00,
		BgL_bgl_string1952za700za7za7b1987za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1953z00zzbeta_walkz00,
		BgL_bgl_string1953za700za7za7b1988za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1954z00zzbeta_walkz00,
		BgL_bgl_string1954za700za7za7b1989za7, " error", 6);
	      DEFINE_STRING(BGl_string1955z00zzbeta_walkz00,
		BgL_bgl_string1955za700za7za7b1990za7, "s", 1);
	      DEFINE_STRING(BGl_string1956z00zzbeta_walkz00,
		BgL_bgl_string1956za700za7za7b1991za7, "", 0);
	      DEFINE_STRING(BGl_string1957z00zzbeta_walkz00,
		BgL_bgl_string1957za700za7za7b1992za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1958z00zzbeta_walkz00,
		BgL_bgl_string1958za700za7za7b1993za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1960z00zzbeta_walkz00,
		BgL_bgl_string1960za700za7za7b1994za7, "node-beta!1288", 14);
	      DEFINE_STRING(BGl_string1962z00zzbeta_walkz00,
		BgL_bgl_string1962za700za7za7b1995za7, "node-beta!", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1959z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza711996za7,
		BGl_z62nodezd2betaz121288za2zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1961z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza711997za7,
		BGl_z62nodezd2betaz12zd2sequence1291z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1963z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza711998za7,
		BGl_z62nodezd2betaz12zd2sync1293z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1964z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza711999za7,
		BGl_z62nodezd2betaz12zd2app1295z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1965z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712000za7,
		BGl_z62nodezd2betaz12zd2appzd2ly1297za2zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1966z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712001za7,
		BGl_z62nodezd2betaz12zd2funcall1299z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1967z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712002za7,
		BGl_z62nodezd2betaz12zd2extern1301z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1968z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712003za7,
		BGl_z62nodezd2betaz12zd2cast1303z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1969z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712004za7,
		BGl_z62nodezd2betaz12zd2setq1305z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1970z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712005za7,
		BGl_z62nodezd2betaz12zd2condition1307z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1971z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712006za7,
		BGl_z62nodezd2betaz12zd2fail1309z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1972z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712007za7,
		BGl_z62nodezd2betaz12zd2switch1311z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1973z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712008za7,
		BGl_z62nodezd2betaz12zd2makezd2box1313za2zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1980z00zzbeta_walkz00,
		BgL_bgl_string1980za700za7za7b2009za7, "wrong node", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1974z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712010za7,
		BGl_z62nodezd2betaz12zd2boxzd2ref1315za2zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1981z00zzbeta_walkz00,
		BgL_bgl_string1981za700za7za7b2011za7, "beta_walk", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1975z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712012za7,
		BGl_z62nodezd2betaz12zd2boxzd2setz121317zb0zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1982z00zzbeta_walkz00,
		BgL_bgl_string1982za700za7za7b2013za7, "read beta pass-started ", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1976z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712014za7,
		BGl_z62nodezd2betaz12zd2letzd2fun1319za2zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1977z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712015za7,
		BGl_z62nodezd2betaz12zd2letzd2var1321za2zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1978z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712016za7,
		BGl_z62nodezd2betaz12zd2setzd2exzd2it1323z70zzbeta_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1979z00zzbeta_walkz00,
		BgL_bgl_za762nodeza7d2betaza712017za7,
		BGl_z62nodezd2betaz12zd2jumpzd2exzd2i1325z70zzbeta_walkz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzbeta_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzbeta_walkz00(long
		BgL_checksumz00_3156, char *BgL_fromz00_3157)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbeta_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzbeta_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbeta_walkz00();
					BGl_libraryzd2moduleszd2initz00zzbeta_walkz00();
					BGl_cnstzd2initzd2zzbeta_walkz00();
					BGl_importedzd2moduleszd2initz00zzbeta_walkz00();
					BGl_genericzd2initzd2zzbeta_walkz00();
					BGl_methodzd2initzd2zzbeta_walkz00();
					return BGl_toplevelzd2initzd2zzbeta_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbeta_walkz00(void)
	{
		{	/* Beta/walk.scm 18 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"beta_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "beta_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "beta_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbeta_walkz00(void)
	{
		{	/* Beta/walk.scm 18 */
			{	/* Beta/walk.scm 18 */
				obj_t BgL_cportz00_2875;

				{	/* Beta/walk.scm 18 */
					obj_t BgL_stringz00_2882;

					BgL_stringz00_2882 = BGl_string1982z00zzbeta_walkz00;
					{	/* Beta/walk.scm 18 */
						obj_t BgL_startz00_2883;

						BgL_startz00_2883 = BINT(0L);
						{	/* Beta/walk.scm 18 */
							obj_t BgL_endz00_2884;

							BgL_endz00_2884 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2882)));
							{	/* Beta/walk.scm 18 */

								BgL_cportz00_2875 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2882, BgL_startz00_2883, BgL_endz00_2884);
				}}}}
				{
					long BgL_iz00_2876;

					BgL_iz00_2876 = 2L;
				BgL_loopz00_2877:
					if ((BgL_iz00_2876 == -1L))
						{	/* Beta/walk.scm 18 */
							return BUNSPEC;
						}
					else
						{	/* Beta/walk.scm 18 */
							{	/* Beta/walk.scm 18 */
								obj_t BgL_arg1983z00_2878;

								{	/* Beta/walk.scm 18 */

									{	/* Beta/walk.scm 18 */
										obj_t BgL_locationz00_2880;

										BgL_locationz00_2880 = BBOOL(((bool_t) 0));
										{	/* Beta/walk.scm 18 */

											BgL_arg1983z00_2878 =
												BGl_readz00zz__readerz00(BgL_cportz00_2875,
												BgL_locationz00_2880);
										}
									}
								}
								{	/* Beta/walk.scm 18 */
									int BgL_tmpz00_3193;

									BgL_tmpz00_3193 = (int) (BgL_iz00_2876);
									CNST_TABLE_SET(BgL_tmpz00_3193, BgL_arg1983z00_2878);
							}}
							{	/* Beta/walk.scm 18 */
								int BgL_auxz00_2881;

								BgL_auxz00_2881 = (int) ((BgL_iz00_2876 - 1L));
								{
									long BgL_iz00_3198;

									BgL_iz00_3198 = (long) (BgL_auxz00_2881);
									BgL_iz00_2876 = BgL_iz00_3198;
									goto BgL_loopz00_2877;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbeta_walkz00(void)
	{
		{	/* Beta/walk.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbeta_walkz00(void)
	{
		{	/* Beta/walk.scm 18 */
			return BUNSPEC;
		}

	}



/* beta-walk! */
	BGL_EXPORTED_DEF obj_t BGl_betazd2walkz12zc0zzbeta_walkz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Beta/walk.scm 35 */
			{	/* Beta/walk.scm 36 */
				obj_t BgL_list1329z00_1394;

				{	/* Beta/walk.scm 36 */
					obj_t BgL_arg1331z00_1395;

					{	/* Beta/walk.scm 36 */
						obj_t BgL_arg1332z00_1396;

						BgL_arg1332z00_1396 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1331z00_1395 =
							MAKE_YOUNG_PAIR(BGl_string1951z00zzbeta_walkz00,
							BgL_arg1332z00_1396);
					}
					BgL_list1329z00_1394 =
						MAKE_YOUNG_PAIR(BGl_string1952z00zzbeta_walkz00,
						BgL_arg1331z00_1395);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1329z00_1394);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1951z00zzbeta_walkz00;
			{	/* Beta/walk.scm 36 */
				obj_t BgL_g1110z00_1397;

				BgL_g1110z00_1397 = BNIL;
				{
					obj_t BgL_hooksz00_1400;
					obj_t BgL_hnamesz00_1401;

					BgL_hooksz00_1400 = BgL_g1110z00_1397;
					BgL_hnamesz00_1401 = BNIL;
				BgL_zc3z04anonymousza31333ze3z87_1402:
					if (NULLP(BgL_hooksz00_1400))
						{	/* Beta/walk.scm 36 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Beta/walk.scm 36 */
							bool_t BgL_test2021z00_3211;

							{	/* Beta/walk.scm 36 */
								obj_t BgL_fun1343z00_1409;

								BgL_fun1343z00_1409 = CAR(((obj_t) BgL_hooksz00_1400));
								BgL_test2021z00_3211 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1343z00_1409));
							}
							if (BgL_test2021z00_3211)
								{	/* Beta/walk.scm 36 */
									obj_t BgL_arg1339z00_1406;
									obj_t BgL_arg1340z00_1407;

									BgL_arg1339z00_1406 = CDR(((obj_t) BgL_hooksz00_1400));
									BgL_arg1340z00_1407 = CDR(((obj_t) BgL_hnamesz00_1401));
									{
										obj_t BgL_hnamesz00_3223;
										obj_t BgL_hooksz00_3222;

										BgL_hooksz00_3222 = BgL_arg1339z00_1406;
										BgL_hnamesz00_3223 = BgL_arg1340z00_1407;
										BgL_hnamesz00_1401 = BgL_hnamesz00_3223;
										BgL_hooksz00_1400 = BgL_hooksz00_3222;
										goto BgL_zc3z04anonymousza31333ze3z87_1402;
									}
								}
							else
								{	/* Beta/walk.scm 36 */
									obj_t BgL_arg1342z00_1408;

									BgL_arg1342z00_1408 = CAR(((obj_t) BgL_hnamesz00_1401));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1951z00zzbeta_walkz00,
										BGl_string1953z00zzbeta_walkz00, BgL_arg1342z00_1408);
								}
						}
				}
			}
			BGl_betazd2globalsz12zc0zzbeta_walkz00(BgL_globalsz00_3);
			{	/* Beta/walk.scm 38 */
				obj_t BgL_valuez00_1412;

				BgL_valuez00_1412 =
					BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(1),
					BgL_globalsz00_3);
				if (((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
						0L))
					{	/* Beta/walk.scm 38 */
						{	/* Beta/walk.scm 38 */
							obj_t BgL_port1273z00_1414;

							{	/* Beta/walk.scm 38 */
								obj_t BgL_tmpz00_3233;

								BgL_tmpz00_3233 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_port1273z00_1414 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3233);
							}
							bgl_display_obj
								(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
								BgL_port1273z00_1414);
							bgl_display_string(BGl_string1954z00zzbeta_walkz00,
								BgL_port1273z00_1414);
							{	/* Beta/walk.scm 38 */
								obj_t BgL_arg1346z00_1415;

								{	/* Beta/walk.scm 38 */
									bool_t BgL_test2023z00_3238;

									if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
										(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
										{	/* Beta/walk.scm 38 */
											if (INTEGERP
												(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
												{	/* Beta/walk.scm 38 */
													BgL_test2023z00_3238 =
														(
														(long)
														CINT
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
														> 1L);
												}
											else
												{	/* Beta/walk.scm 38 */
													BgL_test2023z00_3238 =
														BGl_2ze3ze3zz__r4_numbers_6_5z00
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
														BINT(1L));
												}
										}
									else
										{	/* Beta/walk.scm 38 */
											BgL_test2023z00_3238 = ((bool_t) 0);
										}
									if (BgL_test2023z00_3238)
										{	/* Beta/walk.scm 38 */
											BgL_arg1346z00_1415 = BGl_string1955z00zzbeta_walkz00;
										}
									else
										{	/* Beta/walk.scm 38 */
											BgL_arg1346z00_1415 = BGl_string1956z00zzbeta_walkz00;
										}
								}
								bgl_display_obj(BgL_arg1346z00_1415, BgL_port1273z00_1414);
							}
							bgl_display_string(BGl_string1957z00zzbeta_walkz00,
								BgL_port1273z00_1414);
							bgl_display_char(((unsigned char) 10), BgL_port1273z00_1414);
						}
						{	/* Beta/walk.scm 38 */
							obj_t BgL_list1349z00_1419;

							BgL_list1349z00_1419 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
							return BGl_exitz00zz__errorz00(BgL_list1349z00_1419);
						}
					}
				else
					{	/* Beta/walk.scm 38 */
						obj_t BgL_g1112z00_1420;

						BgL_g1112z00_1420 = BNIL;
						{
							obj_t BgL_hooksz00_1423;
							obj_t BgL_hnamesz00_1424;

							BgL_hooksz00_1423 = BgL_g1112z00_1420;
							BgL_hnamesz00_1424 = BNIL;
						BgL_zc3z04anonymousza31350ze3z87_1425:
							if (NULLP(BgL_hooksz00_1423))
								{	/* Beta/walk.scm 38 */
									return BgL_valuez00_1412;
								}
							else
								{	/* Beta/walk.scm 38 */
									bool_t BgL_test2027z00_3255;

									{	/* Beta/walk.scm 38 */
										obj_t BgL_fun1370z00_1432;

										BgL_fun1370z00_1432 = CAR(((obj_t) BgL_hooksz00_1423));
										BgL_test2027z00_3255 =
											CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1370z00_1432));
									}
									if (BgL_test2027z00_3255)
										{	/* Beta/walk.scm 38 */
											obj_t BgL_arg1361z00_1429;
											obj_t BgL_arg1364z00_1430;

											BgL_arg1361z00_1429 = CDR(((obj_t) BgL_hooksz00_1423));
											BgL_arg1364z00_1430 = CDR(((obj_t) BgL_hnamesz00_1424));
											{
												obj_t BgL_hnamesz00_3267;
												obj_t BgL_hooksz00_3266;

												BgL_hooksz00_3266 = BgL_arg1361z00_1429;
												BgL_hnamesz00_3267 = BgL_arg1364z00_1430;
												BgL_hnamesz00_1424 = BgL_hnamesz00_3267;
												BgL_hooksz00_1423 = BgL_hooksz00_3266;
												goto BgL_zc3z04anonymousza31350ze3z87_1425;
											}
										}
									else
										{	/* Beta/walk.scm 38 */
											obj_t BgL_arg1367z00_1431;

											BgL_arg1367z00_1431 = CAR(((obj_t) BgL_hnamesz00_1424));
											return
												BGl_internalzd2errorzd2zztools_errorz00
												(BGl_za2currentzd2passza2zd2zzengine_passz00,
												BGl_string1958z00zzbeta_walkz00, BgL_arg1367z00_1431);
										}
								}
						}
					}
			}
		}

	}



/* &beta-walk! */
	obj_t BGl_z62betazd2walkz12za2zzbeta_walkz00(obj_t BgL_envz00_2794,
		obj_t BgL_globalsz00_2795)
	{
		{	/* Beta/walk.scm 35 */
			return BGl_betazd2walkz12zc0zzbeta_walkz00(BgL_globalsz00_2795);
		}

	}



/* beta-globals! */
	bool_t BGl_betazd2globalsz12zc0zzbeta_walkz00(obj_t BgL_globalsz00_4)
	{
		{	/* Beta/walk.scm 43 */
			{
				obj_t BgL_l1274z00_1436;

				BgL_l1274z00_1436 = BgL_globalsz00_4;
			BgL_zc3z04anonymousza31372ze3z87_1437:
				if (PAIRP(BgL_l1274z00_1436))
					{	/* Beta/walk.scm 44 */
						{	/* Beta/walk.scm 45 */
							obj_t BgL_globalz00_1439;

							BgL_globalz00_1439 = CAR(BgL_l1274z00_1436);
							{	/* Beta/walk.scm 45 */
								BgL_valuez00_bglt BgL_sfunz00_1440;

								BgL_sfunz00_1440 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1439))))->
									BgL_valuez00);
								{	/* Beta/walk.scm 46 */
									BgL_nodez00_bglt BgL_arg1375z00_1441;

									{	/* Beta/walk.scm 46 */
										obj_t BgL_arg1376z00_1442;

										BgL_arg1376z00_1442 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_sfunz00_1440)))->BgL_bodyz00);
										BgL_arg1375z00_1441 =
											BGl_nodezd2betaz12zc0zzbeta_walkz00(
											((BgL_nodez00_bglt) BgL_arg1376z00_1442), BNIL);
									}
									((((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_sfunz00_1440)))->
											BgL_bodyz00) =
										((obj_t) ((obj_t) BgL_arg1375z00_1441)), BUNSPEC);
								}
							}
						}
						{
							obj_t BgL_l1274z00_3285;

							BgL_l1274z00_3285 = CDR(BgL_l1274z00_1436);
							BgL_l1274z00_1436 = BgL_l1274z00_3285;
							goto BgL_zc3z04anonymousza31372ze3z87_1437;
						}
					}
				else
					{	/* Beta/walk.scm 44 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* node-beta*! */
	obj_t BGl_nodezd2betaza2z12z62zzbeta_walkz00(obj_t BgL_nodeza2za2_43,
		obj_t BgL_stackz00_44)
	{
		{	/* Beta/walk.scm 257 */
			{
				obj_t BgL_l1286z00_1447;

				BgL_l1286z00_1447 = BgL_nodeza2za2_43;
			BgL_zc3z04anonymousza31378ze3z87_1448:
				if (NULLP(BgL_l1286z00_1447))
					{	/* Beta/walk.scm 258 */
						BgL_nodeza2za2_43;
					}
				else
					{	/* Beta/walk.scm 258 */
						{	/* Beta/walk.scm 258 */
							BgL_nodez00_bglt BgL_arg1408z00_1450;

							{	/* Beta/walk.scm 258 */
								obj_t BgL_nodez00_1451;

								BgL_nodez00_1451 = CAR(((obj_t) BgL_l1286z00_1447));
								BgL_arg1408z00_1450 =
									BGl_nodezd2betaz12zc0zzbeta_walkz00(
									((BgL_nodez00_bglt) BgL_nodez00_1451), BgL_stackz00_44);
							}
							{	/* Beta/walk.scm 258 */
								obj_t BgL_auxz00_3295;
								obj_t BgL_tmpz00_3293;

								BgL_auxz00_3295 = ((obj_t) BgL_arg1408z00_1450);
								BgL_tmpz00_3293 = ((obj_t) BgL_l1286z00_1447);
								SET_CAR(BgL_tmpz00_3293, BgL_auxz00_3295);
							}
						}
						{	/* Beta/walk.scm 258 */
							obj_t BgL_arg1410z00_1452;

							BgL_arg1410z00_1452 = CDR(((obj_t) BgL_l1286z00_1447));
							{
								obj_t BgL_l1286z00_3300;

								BgL_l1286z00_3300 = BgL_arg1410z00_1452;
								BgL_l1286z00_1447 = BgL_l1286z00_3300;
								goto BgL_zc3z04anonymousza31378ze3z87_1448;
							}
						}
					}
			}
			return BgL_nodeza2za2_43;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbeta_walkz00(void)
	{
		{	/* Beta/walk.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbeta_walkz00(void)
	{
		{	/* Beta/walk.scm 18 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_proc1959z00zzbeta_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string1960z00zzbeta_walkz00);
		}

	}



/* &node-beta!1288 */
	obj_t BGl_z62nodezd2betaz121288za2zzbeta_walkz00(obj_t BgL_envz00_2797,
		obj_t BgL_nodez00_2798, obj_t BgL_stackz00_2799)
	{
		{	/* Beta/walk.scm 52 */
			return ((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2798));
		}

	}



/* node-beta! */
	BgL_nodez00_bglt BGl_nodezd2betaz12zc0zzbeta_walkz00(BgL_nodez00_bglt
		BgL_nodez00_5, obj_t BgL_stackz00_6)
	{
		{	/* Beta/walk.scm 52 */
			{	/* Beta/walk.scm 52 */
				obj_t BgL_method1289z00_1459;

				{	/* Beta/walk.scm 52 */
					obj_t BgL_res1932z00_2030;

					{	/* Beta/walk.scm 52 */
						long BgL_objzd2classzd2numz00_2001;

						BgL_objzd2classzd2numz00_2001 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_5));
						{	/* Beta/walk.scm 52 */
							obj_t BgL_arg1811z00_2002;

							BgL_arg1811z00_2002 =
								PROCEDURE_REF(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
								(int) (1L));
							{	/* Beta/walk.scm 52 */
								int BgL_offsetz00_2005;

								BgL_offsetz00_2005 = (int) (BgL_objzd2classzd2numz00_2001);
								{	/* Beta/walk.scm 52 */
									long BgL_offsetz00_2006;

									BgL_offsetz00_2006 =
										((long) (BgL_offsetz00_2005) - OBJECT_TYPE);
									{	/* Beta/walk.scm 52 */
										long BgL_modz00_2007;

										BgL_modz00_2007 =
											(BgL_offsetz00_2006 >> (int) ((long) ((int) (4L))));
										{	/* Beta/walk.scm 52 */
											long BgL_restz00_2009;

											BgL_restz00_2009 =
												(BgL_offsetz00_2006 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Beta/walk.scm 52 */

												{	/* Beta/walk.scm 52 */
													obj_t BgL_bucketz00_2011;

													BgL_bucketz00_2011 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2002), BgL_modz00_2007);
													BgL_res1932z00_2030 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2011), BgL_restz00_2009);
					}}}}}}}}
					BgL_method1289z00_1459 = BgL_res1932z00_2030;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1289z00_1459,
						((obj_t) BgL_nodez00_5), BgL_stackz00_6));
			}
		}

	}



/* &node-beta! */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12za2zzbeta_walkz00(obj_t BgL_envz00_2800,
		obj_t BgL_nodez00_2801, obj_t BgL_stackz00_2802)
	{
		{	/* Beta/walk.scm 52 */
			return
				BGl_nodezd2betaz12zc0zzbeta_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_2801), BgL_stackz00_2802);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbeta_walkz00(void)
	{
		{	/* Beta/walk.scm 18 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1961z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc1963z00zzbeta_walkz00, BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc1964z00zzbeta_walkz00, BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1965z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc1966z00zzbeta_walkz00, BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc1967z00zzbeta_walkz00, BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc1968z00zzbeta_walkz00, BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc1969z00zzbeta_walkz00, BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1970z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc1971z00zzbeta_walkz00, BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc1972z00zzbeta_walkz00, BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1973z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1974z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1975z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1976z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1977z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1978z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2betaz12zd2envz12zzbeta_walkz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1979z00zzbeta_walkz00,
				BGl_string1962z00zzbeta_walkz00);
		}

	}



/* &node-beta!-jump-ex-i1325 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2jumpzd2exzd2i1325z70zzbeta_walkz00(obj_t
		BgL_envz00_2821, obj_t BgL_nodez00_2822, obj_t BgL_stackz00_2823)
	{
		{	/* Beta/walk.scm 249 */
			((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2822)))->BgL_exitz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_jumpzd2exzd2itz00_bglt)
								COBJECT(((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2822)))->
							BgL_exitz00), BgL_stackz00_2823)), BUNSPEC);
			((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
								BgL_nodez00_2822)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_jumpzd2exzd2itz00_bglt)
								COBJECT(((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2822)))->
							BgL_valuez00), BgL_stackz00_2823)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt)
					BgL_nodez00_2822));
		}

	}



/* &node-beta!-set-ex-it1323 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2setzd2exzd2it1323z70zzbeta_walkz00(obj_t
		BgL_envz00_2824, obj_t BgL_nodez00_2825, obj_t BgL_stackz00_2826)
	{
		{	/* Beta/walk.scm 241 */
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2825)))->BgL_onexitz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2825)))->
							BgL_onexitz00), BgL_stackz00_2826)), BUNSPEC);
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
								BgL_nodez00_2825)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2825)))->
							BgL_bodyz00), BgL_stackz00_2826)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
					BgL_nodez00_2825));
		}

	}



/* &node-beta!-let-var1321 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2letzd2var1321za2zzbeta_walkz00(obj_t
		BgL_envz00_2827, obj_t BgL_nodez00_2828, obj_t BgL_stackz00_2829)
	{
		{	/* Beta/walk.scm 202 */
			{
				BgL_variablez00_bglt BgL_xz00_2891;
				BgL_variablez00_bglt BgL_yz00_2892;

				{
					obj_t BgL_bindingsz00_2899;
					obj_t BgL_newzd2stackzd2_2900;

					{	/* Beta/walk.scm 212 */
						obj_t BgL_arg1720z00_2963;

						BgL_arg1720z00_2963 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2828)))->
							BgL_bindingsz00);
						{
							BgL_letzd2varzd2_bglt BgL_auxz00_3382;

							BgL_bindingsz00_2899 = BgL_arg1720z00_2963;
							BgL_newzd2stackzd2_2900 = BgL_stackz00_2829;
						BgL_loopz00_2898:
							if (NULLP(BgL_bindingsz00_2899))
								{	/* Beta/walk.scm 214 */
									{
										BgL_nodez00_bglt BgL_auxz00_3385;

										{	/* Beta/walk.scm 216 */
											BgL_nodez00_bglt BgL_arg1724z00_2901;

											BgL_arg1724z00_2901 =
												(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_2828)))->
												BgL_bodyz00);
											BgL_auxz00_3385 =
												BGl_nodezd2betaz12zc0zzbeta_walkz00(BgL_arg1724z00_2901,
												BgL_newzd2stackzd2_2900);
										}
										((((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_2828)))->
												BgL_bodyz00) =
											((BgL_nodez00_bglt) BgL_auxz00_3385), BUNSPEC);
									}
									BgL_auxz00_3382 = ((BgL_letzd2varzd2_bglt) BgL_nodez00_2828);
								}
							else
								{	/* Beta/walk.scm 218 */
									obj_t BgL_bindingz00_2902;

									BgL_bindingz00_2902 = CAR(((obj_t) BgL_bindingsz00_2899));
									{	/* Beta/walk.scm 218 */
										obj_t BgL_varz00_2903;

										BgL_varz00_2903 = CAR(((obj_t) BgL_bindingz00_2902));
										{	/* Beta/walk.scm 219 */
											BgL_nodez00_bglt BgL_valz00_2904;

											{	/* Beta/walk.scm 220 */
												obj_t BgL_arg1842z00_2905;

												BgL_arg1842z00_2905 =
													CDR(((obj_t) BgL_bindingz00_2902));
												BgL_valz00_2904 =
													BGl_nodezd2betaz12zc0zzbeta_walkz00(
													((BgL_nodez00_bglt) BgL_arg1842z00_2905),
													BgL_stackz00_2829);
											}
											{	/* Beta/walk.scm 220 */

												{	/* Beta/walk.scm 221 */
													bool_t BgL_test2031z00_3400;

													if (
														(((BgL_letzd2varzd2_bglt) COBJECT(
																	((BgL_letzd2varzd2_bglt) BgL_nodez00_2828)))->
															BgL_removablezf3zf3))
														{	/* Beta/walk.scm 222 */
															bool_t BgL_test2033z00_3404;

															{	/* Beta/walk.scm 222 */
																bool_t BgL_test2034z00_3405;

																{	/* Beta/walk.scm 222 */
																	bool_t BgL_test2035z00_3406;

																	{	/* Beta/walk.scm 222 */
																		obj_t BgL_arg1840z00_2906;

																		BgL_arg1840z00_2906 =
																			CDR(((obj_t) BgL_bindingz00_2902));
																		{	/* Beta/walk.scm 222 */
																			obj_t BgL_classz00_2907;

																			BgL_classz00_2907 =
																				BGl_atomz00zzast_nodez00;
																			if (BGL_OBJECTP(BgL_arg1840z00_2906))
																				{	/* Beta/walk.scm 222 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_2908;
																					BgL_arg1807z00_2908 =
																						(BgL_objectz00_bglt)
																						(BgL_arg1840z00_2906);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Beta/walk.scm 222 */
																							long BgL_idxz00_2909;

																							BgL_idxz00_2909 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_2908);
																							BgL_test2035z00_3406 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_2909 + 2L)) ==
																								BgL_classz00_2907);
																						}
																					else
																						{	/* Beta/walk.scm 222 */
																							bool_t BgL_res1941z00_2912;

																							{	/* Beta/walk.scm 222 */
																								obj_t BgL_oclassz00_2913;

																								{	/* Beta/walk.scm 222 */
																									obj_t BgL_arg1815z00_2914;
																									long BgL_arg1816z00_2915;

																									BgL_arg1815z00_2914 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Beta/walk.scm 222 */
																										long BgL_arg1817z00_2916;

																										BgL_arg1817z00_2916 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_2908);
																										BgL_arg1816z00_2915 =
																											(BgL_arg1817z00_2916 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_2913 =
																										VECTOR_REF
																										(BgL_arg1815z00_2914,
																										BgL_arg1816z00_2915);
																								}
																								{	/* Beta/walk.scm 222 */
																									bool_t
																										BgL__ortest_1115z00_2917;
																									BgL__ortest_1115z00_2917 =
																										(BgL_classz00_2907 ==
																										BgL_oclassz00_2913);
																									if (BgL__ortest_1115z00_2917)
																										{	/* Beta/walk.scm 222 */
																											BgL_res1941z00_2912 =
																												BgL__ortest_1115z00_2917;
																										}
																									else
																										{	/* Beta/walk.scm 222 */
																											long BgL_odepthz00_2918;

																											{	/* Beta/walk.scm 222 */
																												obj_t
																													BgL_arg1804z00_2919;
																												BgL_arg1804z00_2919 =
																													(BgL_oclassz00_2913);
																												BgL_odepthz00_2918 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_2919);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_2918))
																												{	/* Beta/walk.scm 222 */
																													obj_t
																														BgL_arg1802z00_2920;
																													{	/* Beta/walk.scm 222 */
																														obj_t
																															BgL_arg1803z00_2921;
																														BgL_arg1803z00_2921
																															=
																															(BgL_oclassz00_2913);
																														BgL_arg1802z00_2920
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_2921,
																															2L);
																													}
																													BgL_res1941z00_2912 =
																														(BgL_arg1802z00_2920
																														==
																														BgL_classz00_2907);
																												}
																											else
																												{	/* Beta/walk.scm 222 */
																													BgL_res1941z00_2912 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2035z00_3406 =
																								BgL_res1941z00_2912;
																						}
																				}
																			else
																				{	/* Beta/walk.scm 222 */
																					BgL_test2035z00_3406 = ((bool_t) 0);
																				}
																		}
																	}
																	if (BgL_test2035z00_3406)
																		{	/* Beta/walk.scm 223 */
																			obj_t BgL_valz00_2922;

																			BgL_valz00_2922 =
																				(((BgL_atomz00_bglt) COBJECT(
																						((BgL_atomz00_bglt)
																							CDR(
																								((obj_t)
																									BgL_bindingz00_2902)))))->
																				BgL_valuez00);
																			{	/* Beta/walk.scm 224 */
																				bool_t BgL__ortest_1150z00_2923;

																				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_valz00_2922))
																					{	/* Beta/walk.scm 224 */
																						if (BIGNUMP(BgL_valz00_2922))
																							{	/* Beta/walk.scm 224 */
																								BgL__ortest_1150z00_2923 =
																									((bool_t) 0);
																							}
																						else
																							{	/* Beta/walk.scm 224 */
																								BgL__ortest_1150z00_2923 =
																									((bool_t) 1);
																							}
																					}
																				else
																					{	/* Beta/walk.scm 224 */
																						BgL__ortest_1150z00_2923 =
																							((bool_t) 0);
																					}
																				if (BgL__ortest_1150z00_2923)
																					{	/* Beta/walk.scm 224 */
																						BgL_test2034z00_3405 =
																							BgL__ortest_1150z00_2923;
																					}
																				else
																					{	/* Beta/walk.scm 225 */
																						bool_t BgL__ortest_1151z00_2924;

																						BgL__ortest_1151z00_2924 =
																							BOOLEANP(BgL_valz00_2922);
																						if (BgL__ortest_1151z00_2924)
																							{	/* Beta/walk.scm 225 */
																								BgL_test2034z00_3405 =
																									BgL__ortest_1151z00_2924;
																							}
																						else
																							{	/* Beta/walk.scm 226 */
																								bool_t BgL__ortest_1152z00_2925;

																								BgL__ortest_1152z00_2925 =
																									CHARP(BgL_valz00_2922);
																								if (BgL__ortest_1152z00_2925)
																									{	/* Beta/walk.scm 226 */
																										BgL_test2034z00_3405 =
																											BgL__ortest_1152z00_2925;
																									}
																								else
																									{	/* Beta/walk.scm 227 */
																										bool_t
																											BgL__ortest_1153z00_2926;
																										BgL__ortest_1153z00_2926 =
																											SYMBOLP(BgL_valz00_2922);
																										if (BgL__ortest_1153z00_2926)
																											{	/* Beta/walk.scm 227 */
																												BgL_test2034z00_3405 =
																													BgL__ortest_1153z00_2926;
																											}
																										else
																											{	/* Beta/walk.scm 228 */
																												bool_t
																													BgL__ortest_1154z00_2927;
																												BgL__ortest_1154z00_2927
																													=
																													KEYWORDP
																													(BgL_valz00_2922);
																												if (BgL__ortest_1154z00_2927)
																													{	/* Beta/walk.scm 228 */
																														BgL_test2034z00_3405
																															=
																															BgL__ortest_1154z00_2927;
																													}
																												else
																													{	/* Beta/walk.scm 228 */
																														BgL_test2034z00_3405
																															=
																															CNSTP
																															(BgL_valz00_2922);
																													}
																											}
																									}
																							}
																					}
																			}
																		}
																	else
																		{	/* Beta/walk.scm 222 */
																			BgL_test2034z00_3405 = ((bool_t) 0);
																		}
																}
																if (BgL_test2034z00_3405)
																	{	/* Beta/walk.scm 222 */
																		BgL_test2033z00_3404 = ((bool_t) 1);
																	}
																else
																	{	/* Beta/walk.scm 230 */
																		bool_t BgL_test2047z00_3449;

																		{	/* Beta/walk.scm 230 */
																			obj_t BgL_arg1838z00_2928;

																			BgL_arg1838z00_2928 =
																				CDR(((obj_t) BgL_bindingz00_2902));
																			{	/* Beta/walk.scm 230 */
																				obj_t BgL_classz00_2929;

																				BgL_classz00_2929 =
																					BGl_varz00zzast_nodez00;
																				if (BGL_OBJECTP(BgL_arg1838z00_2928))
																					{	/* Beta/walk.scm 230 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_2930;
																						BgL_arg1807z00_2930 =
																							(BgL_objectz00_bglt)
																							(BgL_arg1838z00_2928);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Beta/walk.scm 230 */
																								long BgL_idxz00_2931;

																								BgL_idxz00_2931 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_2930);
																								BgL_test2047z00_3449 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2931 + 2L)) ==
																									BgL_classz00_2929);
																							}
																						else
																							{	/* Beta/walk.scm 230 */
																								bool_t BgL_res1942z00_2934;

																								{	/* Beta/walk.scm 230 */
																									obj_t BgL_oclassz00_2935;

																									{	/* Beta/walk.scm 230 */
																										obj_t BgL_arg1815z00_2936;
																										long BgL_arg1816z00_2937;

																										BgL_arg1815z00_2936 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Beta/walk.scm 230 */
																											long BgL_arg1817z00_2938;

																											BgL_arg1817z00_2938 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_2930);
																											BgL_arg1816z00_2937 =
																												(BgL_arg1817z00_2938 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2935 =
																											VECTOR_REF
																											(BgL_arg1815z00_2936,
																											BgL_arg1816z00_2937);
																									}
																									{	/* Beta/walk.scm 230 */
																										bool_t
																											BgL__ortest_1115z00_2939;
																										BgL__ortest_1115z00_2939 =
																											(BgL_classz00_2929 ==
																											BgL_oclassz00_2935);
																										if (BgL__ortest_1115z00_2939)
																											{	/* Beta/walk.scm 230 */
																												BgL_res1942z00_2934 =
																													BgL__ortest_1115z00_2939;
																											}
																										else
																											{	/* Beta/walk.scm 230 */
																												long BgL_odepthz00_2940;

																												{	/* Beta/walk.scm 230 */
																													obj_t
																														BgL_arg1804z00_2941;
																													BgL_arg1804z00_2941 =
																														(BgL_oclassz00_2935);
																													BgL_odepthz00_2940 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2941);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_2940))
																													{	/* Beta/walk.scm 230 */
																														obj_t
																															BgL_arg1802z00_2942;
																														{	/* Beta/walk.scm 230 */
																															obj_t
																																BgL_arg1803z00_2943;
																															BgL_arg1803z00_2943
																																=
																																(BgL_oclassz00_2935);
																															BgL_arg1802z00_2942
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2943,
																																2L);
																														}
																														BgL_res1942z00_2934
																															=
																															(BgL_arg1802z00_2942
																															==
																															BgL_classz00_2929);
																													}
																												else
																													{	/* Beta/walk.scm 230 */
																														BgL_res1942z00_2934
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2047z00_3449 =
																									BgL_res1942z00_2934;
																							}
																					}
																				else
																					{	/* Beta/walk.scm 230 */
																						BgL_test2047z00_3449 = ((bool_t) 0);
																					}
																			}
																		}
																		if (BgL_test2047z00_3449)
																			{	/* Beta/walk.scm 231 */
																				bool_t BgL_test2052z00_3474;

																				{	/* Beta/walk.scm 231 */
																					BgL_variablez00_bglt
																						BgL_arg1836z00_2944;
																					BgL_arg1836z00_2944 =
																						(((BgL_varz00_bglt)
																							COBJECT(((BgL_varz00_bglt)
																									CDR(((obj_t)
																											BgL_bindingz00_2902)))))->
																						BgL_variablez00);
																					{	/* Beta/walk.scm 231 */
																						obj_t BgL_classz00_2945;

																						BgL_classz00_2945 =
																							BGl_localz00zzast_varz00;
																						{	/* Beta/walk.scm 231 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_2946;
																							{	/* Beta/walk.scm 231 */
																								obj_t BgL_tmpz00_3479;

																								BgL_tmpz00_3479 =
																									((obj_t)
																									((BgL_objectz00_bglt)
																										BgL_arg1836z00_2944));
																								BgL_arg1807z00_2946 =
																									(BgL_objectz00_bglt)
																									(BgL_tmpz00_3479);
																							}
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Beta/walk.scm 231 */
																									long BgL_idxz00_2947;

																									BgL_idxz00_2947 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_2946);
																									BgL_test2052z00_3474 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_2947 + 2L)) ==
																										BgL_classz00_2945);
																								}
																							else
																								{	/* Beta/walk.scm 231 */
																									bool_t BgL_res1943z00_2950;

																									{	/* Beta/walk.scm 231 */
																										obj_t BgL_oclassz00_2951;

																										{	/* Beta/walk.scm 231 */
																											obj_t BgL_arg1815z00_2952;
																											long BgL_arg1816z00_2953;

																											BgL_arg1815z00_2952 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Beta/walk.scm 231 */
																												long
																													BgL_arg1817z00_2954;
																												BgL_arg1817z00_2954 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_2946);
																												BgL_arg1816z00_2953 =
																													(BgL_arg1817z00_2954 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_2951 =
																												VECTOR_REF
																												(BgL_arg1815z00_2952,
																												BgL_arg1816z00_2953);
																										}
																										{	/* Beta/walk.scm 231 */
																											bool_t
																												BgL__ortest_1115z00_2955;
																											BgL__ortest_1115z00_2955 =
																												(BgL_classz00_2945 ==
																												BgL_oclassz00_2951);
																											if (BgL__ortest_1115z00_2955)
																												{	/* Beta/walk.scm 231 */
																													BgL_res1943z00_2950 =
																														BgL__ortest_1115z00_2955;
																												}
																											else
																												{	/* Beta/walk.scm 231 */
																													long
																														BgL_odepthz00_2956;
																													{	/* Beta/walk.scm 231 */
																														obj_t
																															BgL_arg1804z00_2957;
																														BgL_arg1804z00_2957
																															=
																															(BgL_oclassz00_2951);
																														BgL_odepthz00_2956 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_2957);
																													}
																													if (
																														(2L <
																															BgL_odepthz00_2956))
																														{	/* Beta/walk.scm 231 */
																															obj_t
																																BgL_arg1802z00_2958;
																															{	/* Beta/walk.scm 231 */
																																obj_t
																																	BgL_arg1803z00_2959;
																																BgL_arg1803z00_2959
																																	=
																																	(BgL_oclassz00_2951);
																																BgL_arg1802z00_2958
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_2959,
																																	2L);
																															}
																															BgL_res1943z00_2950
																																=
																																(BgL_arg1802z00_2958
																																==
																																BgL_classz00_2945);
																														}
																													else
																														{	/* Beta/walk.scm 231 */
																															BgL_res1943z00_2950
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test2052z00_3474 =
																										BgL_res1943z00_2950;
																								}
																						}
																					}
																				}
																				if (BgL_test2052z00_3474)
																					{	/* Beta/walk.scm 231 */
																						if (
																							((((BgL_variablez00_bglt) COBJECT(
																											((BgL_variablez00_bglt)
																												((BgL_localz00_bglt)
																													(((BgL_varz00_bglt)
																															COBJECT((
																																	(BgL_varz00_bglt)
																																	CDR(((obj_t)
																																			BgL_bindingz00_2902)))))->
																														BgL_variablez00)))))->
																									BgL_accessz00) ==
																								CNST_TABLE_REF(2)))
																							{	/* Beta/walk.scm 232 */
																								BgL_xz00_2891 =
																									((BgL_variablez00_bglt)
																									BgL_varz00_2903);
																								BgL_yz00_2892 =
																									(((BgL_varz00_bglt)
																										COBJECT(((BgL_varz00_bglt)
																												CDR(((obj_t)
																														BgL_bindingz00_2902)))))->
																									BgL_variablez00);
																								{	/* Beta/walk.scm 207 */
																									bool_t
																										BgL__ortest_1146z00_2893;
																									BgL__ortest_1146z00_2893 =
																										(((obj_t) ((
																													(BgL_variablez00_bglt)
																													COBJECT
																													(BgL_xz00_2891))->
																												BgL_typez00)) ==
																										((obj_t) ((
																													(BgL_variablez00_bglt)
																													COBJECT
																													(BgL_yz00_2892))->
																												BgL_typez00)));
																									if (BgL__ortest_1146z00_2893)
																										{	/* Beta/walk.scm 207 */
																											BgL_test2033z00_3404 =
																												BgL__ortest_1146z00_2893;
																										}
																									else
																										{	/* Beta/walk.scm 208 */
																											bool_t
																												BgL__ortest_1147z00_2894;
																											{	/* Beta/walk.scm 208 */
																												BgL_typez00_bglt
																													BgL_arg1846z00_2895;
																												BgL_arg1846z00_2895 =
																													(((BgL_variablez00_bglt) COBJECT(BgL_xz00_2891))->BgL_typez00);
																												BgL__ortest_1147z00_2894
																													=
																													(((obj_t)
																														BgL_arg1846z00_2895)
																													==
																													BGl_za2_za2z00zztype_cachez00);
																											}
																											if (BgL__ortest_1147z00_2894)
																												{	/* Beta/walk.scm 208 */
																													BgL_test2033z00_3404 =
																														BgL__ortest_1147z00_2894;
																												}
																											else
																												{	/* Beta/walk.scm 209 */
																													bool_t
																														BgL_test2059z00_3522;
																													{	/* Beta/walk.scm 209 */
																														BgL_typez00_bglt
																															BgL_arg1845z00_2896;
																														BgL_arg1845z00_2896
																															=
																															(((BgL_variablez00_bglt) COBJECT(BgL_xz00_2891))->BgL_typez00);
																														BgL_test2059z00_3522
																															=
																															(((obj_t)
																																BgL_arg1845z00_2896)
																															==
																															BGl_za2longza2z00zztype_cachez00);
																													}
																													if (BgL_test2059z00_3522)
																														{	/* Beta/walk.scm 209 */
																															BgL_typez00_bglt
																																BgL_arg1844z00_2897;
																															BgL_arg1844z00_2897
																																=
																																(((BgL_variablez00_bglt) COBJECT(BgL_yz00_2892))->BgL_typez00);
																															BgL_test2033z00_3404
																																=
																																(((obj_t)
																																	BgL_arg1844z00_2897)
																																==
																																BGl_za2bintza2z00zztype_cachez00);
																														}
																													else
																														{	/* Beta/walk.scm 209 */
																															BgL_test2033z00_3404
																																= ((bool_t) 0);
																														}
																												}
																										}
																								}
																							}
																						else
																							{	/* Beta/walk.scm 232 */
																								BgL_test2033z00_3404 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Beta/walk.scm 231 */
																						BgL_test2033z00_3404 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Beta/walk.scm 230 */
																				BgL_test2033z00_3404 = ((bool_t) 0);
																			}
																	}
															}
															if (BgL_test2033z00_3404)
																{	/* Beta/walk.scm 222 */
																	BgL_test2031z00_3400 =
																		(
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							CAR(
																								((obj_t)
																									BgL_bindingz00_2902))))))->
																			BgL_accessz00) == CNST_TABLE_REF(2));
																}
															else
																{	/* Beta/walk.scm 222 */
																	BgL_test2031z00_3400 = ((bool_t) 0);
																}
														}
													else
														{	/* Beta/walk.scm 221 */
															BgL_test2031z00_3400 = ((bool_t) 0);
														}
													if (BgL_test2031z00_3400)
														{	/* Beta/walk.scm 235 */
															obj_t BgL_arg1808z00_2960;
															obj_t BgL_arg1812z00_2961;

															BgL_arg1808z00_2960 =
																CDR(((obj_t) BgL_bindingsz00_2899));
															BgL_arg1812z00_2961 =
																MAKE_YOUNG_PAIR(BgL_bindingz00_2902,
																BgL_newzd2stackzd2_2900);
															{
																obj_t BgL_newzd2stackzd2_3545;
																obj_t BgL_bindingsz00_3544;

																BgL_bindingsz00_3544 = BgL_arg1808z00_2960;
																BgL_newzd2stackzd2_3545 = BgL_arg1812z00_2961;
																BgL_newzd2stackzd2_2900 =
																	BgL_newzd2stackzd2_3545;
																BgL_bindingsz00_2899 = BgL_bindingsz00_3544;
																goto BgL_loopz00_2898;
															}
														}
													else
														{	/* Beta/walk.scm 236 */
															obj_t BgL_arg1820z00_2962;

															BgL_arg1820z00_2962 =
																CDR(((obj_t) BgL_bindingsz00_2899));
															{
																obj_t BgL_bindingsz00_3548;

																BgL_bindingsz00_3548 = BgL_arg1820z00_2962;
																BgL_bindingsz00_2899 = BgL_bindingsz00_3548;
																goto BgL_loopz00_2898;
															}
														}
												}
											}
										}
									}
								}
							return ((BgL_nodez00_bglt) BgL_auxz00_3382);
						}
					}
				}
			}
		}

	}



/* &node-beta!-let-fun1319 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2letzd2fun1319za2zzbeta_walkz00(obj_t
		BgL_envz00_2830, obj_t BgL_nodez00_2831, obj_t BgL_stackz00_2832)
	{
		{	/* Beta/walk.scm 190 */
			{
				BgL_nodez00_bglt BgL_auxz00_3550;

				{	/* Beta/walk.scm 192 */
					BgL_nodez00_bglt BgL_arg1709z00_2965;

					BgL_arg1709z00_2965 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2831)))->BgL_bodyz00);
					BgL_auxz00_3550 =
						BGl_nodezd2betaz12zc0zzbeta_walkz00(BgL_arg1709z00_2965,
						BgL_stackz00_2832);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2831)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3550), BUNSPEC);
			}
			{	/* Beta/walk.scm 193 */
				obj_t BgL_g1284z00_2966;

				BgL_g1284z00_2966 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2831)))->BgL_localsz00);
				{
					obj_t BgL_l1282z00_2968;

					BgL_l1282z00_2968 = BgL_g1284z00_2966;
				BgL_zc3z04anonymousza31710ze3z87_2967:
					if (PAIRP(BgL_l1282z00_2968))
						{	/* Beta/walk.scm 193 */
							{	/* Beta/walk.scm 194 */
								obj_t BgL_localz00_2969;

								BgL_localz00_2969 = CAR(BgL_l1282z00_2968);
								{	/* Beta/walk.scm 194 */
									BgL_valuez00_bglt BgL_funz00_2970;

									BgL_funz00_2970 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2969))))->
										BgL_valuez00);
									{	/* Beta/walk.scm 195 */
										BgL_nodez00_bglt BgL_arg1714z00_2971;

										{	/* Beta/walk.scm 195 */
											obj_t BgL_arg1717z00_2972;

											BgL_arg1717z00_2972 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2970)))->
												BgL_bodyz00);
											BgL_arg1714z00_2971 =
												BGl_nodezd2betaz12zc0zzbeta_walkz00(((BgL_nodez00_bglt)
													BgL_arg1717z00_2972), BgL_stackz00_2832);
										}
										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2970)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_arg1714z00_2971)), BUNSPEC);
									}
								}
							}
							{
								obj_t BgL_l1282z00_3571;

								BgL_l1282z00_3571 = CDR(BgL_l1282z00_2968);
								BgL_l1282z00_2968 = BgL_l1282z00_3571;
								goto BgL_zc3z04anonymousza31710ze3z87_2967;
							}
						}
					else
						{	/* Beta/walk.scm 193 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2831));
		}

	}



/* &node-beta!-box-set!1317 */
	BgL_nodez00_bglt
		BGl_z62nodezd2betaz12zd2boxzd2setz121317zb0zzbeta_walkz00(obj_t
		BgL_envz00_2833, obj_t BgL_nodez00_2834, obj_t BgL_stackz00_2835)
	{
		{	/* Beta/walk.scm 182 */
			{	/* Beta/walk.scm 183 */
				BgL_nodez00_bglt BgL_arg1702z00_2974;

				{	/* Beta/walk.scm 183 */
					BgL_varz00_bglt BgL_arg1703z00_2975;

					BgL_arg1703z00_2975 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2834)))->BgL_varz00);
					BgL_arg1702z00_2974 =
						BGl_nodezd2betaz12zc0zzbeta_walkz00(
						((BgL_nodez00_bglt) BgL_arg1703z00_2975), BgL_stackz00_2835);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2834)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1702z00_2974)), BUNSPEC);
			}
			((((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2834)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_boxzd2setz12zc0_bglt)
								COBJECT(((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2834)))->
							BgL_valuez00), BgL_stackz00_2835)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2834));
		}

	}



/* &node-beta!-box-ref1315 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2boxzd2ref1315za2zzbeta_walkz00(obj_t
		BgL_envz00_2836, obj_t BgL_nodez00_2837, obj_t BgL_stackz00_2838)
	{
		{	/* Beta/walk.scm 175 */
			{	/* Beta/walk.scm 176 */
				BgL_nodez00_bglt BgL_arg1700z00_2977;

				{	/* Beta/walk.scm 176 */
					BgL_varz00_bglt BgL_arg1701z00_2978;

					BgL_arg1701z00_2978 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2837)))->BgL_varz00);
					BgL_arg1700z00_2977 =
						BGl_nodezd2betaz12zc0zzbeta_walkz00(
						((BgL_nodez00_bglt) BgL_arg1701z00_2978), BgL_stackz00_2838);
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2837)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1700z00_2977)), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2837));
		}

	}



/* &node-beta!-make-box1313 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2makezd2box1313za2zzbeta_walkz00(obj_t
		BgL_envz00_2839, obj_t BgL_nodez00_2840, obj_t BgL_stackz00_2841)
	{
		{	/* Beta/walk.scm 168 */
			((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2840)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_makezd2boxzd2_bglt)
								COBJECT(((BgL_makezd2boxzd2_bglt) BgL_nodez00_2840)))->
							BgL_valuez00), BgL_stackz00_2841)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2840));
		}

	}



/* &node-beta!-switch1311 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2switch1311z70zzbeta_walkz00(obj_t
		BgL_envz00_2842, obj_t BgL_nodez00_2843, obj_t BgL_stackz00_2844)
	{
		{	/* Beta/walk.scm 158 */
			((((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2843)))->BgL_testz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_switchz00_bglt)
								COBJECT(((BgL_switchz00_bglt) BgL_nodez00_2843)))->BgL_testz00),
						BgL_stackz00_2844)), BUNSPEC);
			{	/* Beta/walk.scm 160 */
				obj_t BgL_g1281z00_2981;

				BgL_g1281z00_2981 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2843)))->BgL_clausesz00);
				{
					obj_t BgL_l1279z00_2983;

					BgL_l1279z00_2983 = BgL_g1281z00_2981;
				BgL_zc3z04anonymousza31682ze3z87_2982:
					if (PAIRP(BgL_l1279z00_2983))
						{	/* Beta/walk.scm 162 */
							{	/* Beta/walk.scm 161 */
								obj_t BgL_clausez00_2984;

								BgL_clausez00_2984 = CAR(BgL_l1279z00_2983);
								{	/* Beta/walk.scm 161 */
									BgL_nodez00_bglt BgL_arg1688z00_2985;

									{	/* Beta/walk.scm 161 */
										obj_t BgL_arg1689z00_2986;

										BgL_arg1689z00_2986 = CDR(((obj_t) BgL_clausez00_2984));
										BgL_arg1688z00_2985 =
											BGl_nodezd2betaz12zc0zzbeta_walkz00(
											((BgL_nodez00_bglt) BgL_arg1689z00_2986),
											BgL_stackz00_2844);
									}
									{	/* Beta/walk.scm 161 */
										obj_t BgL_auxz00_3621;
										obj_t BgL_tmpz00_3619;

										BgL_auxz00_3621 = ((obj_t) BgL_arg1688z00_2985);
										BgL_tmpz00_3619 = ((obj_t) BgL_clausez00_2984);
										SET_CDR(BgL_tmpz00_3619, BgL_auxz00_3621);
									}
								}
							}
							{
								obj_t BgL_l1279z00_3624;

								BgL_l1279z00_3624 = CDR(BgL_l1279z00_2983);
								BgL_l1279z00_2983 = BgL_l1279z00_3624;
								goto BgL_zc3z04anonymousza31682ze3z87_2982;
							}
						}
					else
						{	/* Beta/walk.scm 162 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2843));
		}

	}



/* &node-beta!-fail1309 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2fail1309z70zzbeta_walkz00(obj_t
		BgL_envz00_2845, obj_t BgL_nodez00_2846, obj_t BgL_stackz00_2847)
	{
		{	/* Beta/walk.scm 149 */
			((((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2846)))->BgL_procz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_failz00_bglt)
								COBJECT(((BgL_failz00_bglt) BgL_nodez00_2846)))->BgL_procz00),
						BgL_stackz00_2847)), BUNSPEC);
			((((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt) BgL_nodez00_2846)))->
					BgL_msgz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_failz00_bglt)
								COBJECT(((BgL_failz00_bglt) BgL_nodez00_2846)))->BgL_msgz00),
						BgL_stackz00_2847)), BUNSPEC);
			((((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt) BgL_nodez00_2846)))->
					BgL_objz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_failz00_bglt)
								COBJECT(((BgL_failz00_bglt) BgL_nodez00_2846)))->BgL_objz00),
						BgL_stackz00_2847)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2846));
		}

	}



/* &node-beta!-condition1307 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2condition1307z70zzbeta_walkz00(obj_t
		BgL_envz00_2848, obj_t BgL_nodez00_2849, obj_t BgL_stackz00_2850)
	{
		{	/* Beta/walk.scm 140 */
			((((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2849)))->BgL_testz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_conditionalz00_bglt)
								COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_2849)))->
							BgL_testz00), BgL_stackz00_2850)), BUNSPEC);
			((((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
								BgL_nodez00_2849)))->BgL_truez00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_conditionalz00_bglt)
								COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_2849)))->
							BgL_truez00), BgL_stackz00_2850)), BUNSPEC);
			((((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
								BgL_nodez00_2849)))->BgL_falsez00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_conditionalz00_bglt)
								COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_2849)))->
							BgL_falsez00), BgL_stackz00_2850)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2849));
		}

	}



/* &node-beta!-setq1305 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2setq1305z70zzbeta_walkz00(obj_t
		BgL_envz00_2851, obj_t BgL_nodez00_2852, obj_t BgL_stackz00_2853)
	{
		{	/* Beta/walk.scm 133 */
			((((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2852)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_setqz00_bglt)
								COBJECT(((BgL_setqz00_bglt) BgL_nodez00_2852)))->BgL_valuez00),
						BgL_stackz00_2853)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2852));
		}

	}



/* &node-beta!-cast1303 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2cast1303z70zzbeta_walkz00(obj_t
		BgL_envz00_2854, obj_t BgL_nodez00_2855, obj_t BgL_stackz00_2856)
	{
		{	/* Beta/walk.scm 126 */
			((((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2855)))->BgL_argz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_castz00_bglt)
								COBJECT(((BgL_castz00_bglt) BgL_nodez00_2855)))->BgL_argz00),
						BgL_stackz00_2856)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2855));
		}

	}



/* &node-beta!-extern1301 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2extern1301z70zzbeta_walkz00(obj_t
		BgL_envz00_2857, obj_t BgL_nodez00_2858, obj_t BgL_stackz00_2859)
	{
		{	/* Beta/walk.scm 119 */
			BGl_nodezd2betaza2z12z62zzbeta_walkz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2858)))->BgL_exprza2za2),
				BgL_stackz00_2859);
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2858));
		}

	}



/* &node-beta!-funcall1299 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2funcall1299z70zzbeta_walkz00(obj_t
		BgL_envz00_2860, obj_t BgL_nodez00_2861, obj_t BgL_stackz00_2862)
	{
		{	/* Beta/walk.scm 111 */
			((((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2861)))->BgL_funz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_funcallz00_bglt)
								COBJECT(((BgL_funcallz00_bglt) BgL_nodez00_2861)))->BgL_funz00),
						BgL_stackz00_2862)), BUNSPEC);
			BGl_nodezd2betaza2z12z62zzbeta_walkz00((((BgL_funcallz00_bglt)
						COBJECT(((BgL_funcallz00_bglt) BgL_nodez00_2861)))->BgL_argsz00),
				BgL_stackz00_2862);
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2861));
		}

	}



/* &node-beta!-app-ly1297 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2appzd2ly1297za2zzbeta_walkz00(obj_t
		BgL_envz00_2863, obj_t BgL_nodez00_2864, obj_t BgL_stackz00_2865)
	{
		{	/* Beta/walk.scm 103 */
			((((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2864)))->BgL_funz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_appzd2lyzd2_bglt)
								COBJECT(((BgL_appzd2lyzd2_bglt) BgL_nodez00_2864)))->
							BgL_funz00), BgL_stackz00_2865)), BUNSPEC);
			((((BgL_appzd2lyzd2_bglt) COBJECT(((BgL_appzd2lyzd2_bglt)
								BgL_nodez00_2864)))->BgL_argz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_appzd2lyzd2_bglt)
								COBJECT(((BgL_appzd2lyzd2_bglt) BgL_nodez00_2864)))->
							BgL_argz00), BgL_stackz00_2865)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2864));
		}

	}



/* &node-beta!-app1295 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2app1295z70zzbeta_walkz00(obj_t
		BgL_envz00_2866, obj_t BgL_nodez00_2867, obj_t BgL_stackz00_2868)
	{
		{	/* Beta/walk.scm 74 */
			{	/* Beta/walk.scm 75 */
				BgL_nodez00_bglt BgL_arg1473z00_2995;

				{	/* Beta/walk.scm 75 */
					BgL_varz00_bglt BgL_arg1485z00_2996;

					BgL_arg1485z00_2996 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2867)))->BgL_funz00);
					BgL_arg1473z00_2995 =
						BGl_nodezd2betaz12zc0zzbeta_walkz00(
						((BgL_nodez00_bglt) BgL_arg1485z00_2996), BgL_stackz00_2868);
				}
				((((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2867)))->BgL_funz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1473z00_2995)), BUNSPEC);
			}
			{	/* Beta/walk.scm 76 */
				obj_t BgL_l01278z00_2997;

				BgL_l01278z00_2997 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2867)))->BgL_argsz00);
				{
					obj_t BgL_l1277z00_2999;

					BgL_l1277z00_2999 = BgL_l01278z00_2997;
				BgL_zc3z04anonymousza31486ze3z87_2998:
					if (NULLP(BgL_l1277z00_2999))
						{	/* Beta/walk.scm 97 */
							BgL_l01278z00_2997;
						}
					else
						{	/* Beta/walk.scm 97 */
							{	/* Beta/walk.scm 77 */
								obj_t BgL_arg1489z00_3000;

								{	/* Beta/walk.scm 77 */
									obj_t BgL_nz00_3001;

									BgL_nz00_3001 = CAR(((obj_t) BgL_l1277z00_2999));
									{	/* Beta/walk.scm 77 */
										bool_t BgL_test2063z00_3716;

										{	/* Beta/walk.scm 77 */
											bool_t BgL_test2064z00_3717;

											{	/* Beta/walk.scm 77 */
												obj_t BgL_classz00_3002;

												BgL_classz00_3002 = BGl_varz00zzast_nodez00;
												if (BGL_OBJECTP(BgL_nz00_3001))
													{	/* Beta/walk.scm 77 */
														BgL_objectz00_bglt BgL_arg1807z00_3003;

														BgL_arg1807z00_3003 =
															(BgL_objectz00_bglt) (BgL_nz00_3001);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Beta/walk.scm 77 */
																long BgL_idxz00_3004;

																BgL_idxz00_3004 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3003);
																BgL_test2064z00_3717 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3004 + 2L)) ==
																	BgL_classz00_3002);
															}
														else
															{	/* Beta/walk.scm 77 */
																bool_t BgL_res1933z00_3007;

																{	/* Beta/walk.scm 77 */
																	obj_t BgL_oclassz00_3008;

																	{	/* Beta/walk.scm 77 */
																		obj_t BgL_arg1815z00_3009;
																		long BgL_arg1816z00_3010;

																		BgL_arg1815z00_3009 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Beta/walk.scm 77 */
																			long BgL_arg1817z00_3011;

																			BgL_arg1817z00_3011 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3003);
																			BgL_arg1816z00_3010 =
																				(BgL_arg1817z00_3011 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3008 =
																			VECTOR_REF(BgL_arg1815z00_3009,
																			BgL_arg1816z00_3010);
																	}
																	{	/* Beta/walk.scm 77 */
																		bool_t BgL__ortest_1115z00_3012;

																		BgL__ortest_1115z00_3012 =
																			(BgL_classz00_3002 == BgL_oclassz00_3008);
																		if (BgL__ortest_1115z00_3012)
																			{	/* Beta/walk.scm 77 */
																				BgL_res1933z00_3007 =
																					BgL__ortest_1115z00_3012;
																			}
																		else
																			{	/* Beta/walk.scm 77 */
																				long BgL_odepthz00_3013;

																				{	/* Beta/walk.scm 77 */
																					obj_t BgL_arg1804z00_3014;

																					BgL_arg1804z00_3014 =
																						(BgL_oclassz00_3008);
																					BgL_odepthz00_3013 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3014);
																				}
																				if ((2L < BgL_odepthz00_3013))
																					{	/* Beta/walk.scm 77 */
																						obj_t BgL_arg1802z00_3015;

																						{	/* Beta/walk.scm 77 */
																							obj_t BgL_arg1803z00_3016;

																							BgL_arg1803z00_3016 =
																								(BgL_oclassz00_3008);
																							BgL_arg1802z00_3015 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3016, 2L);
																						}
																						BgL_res1933z00_3007 =
																							(BgL_arg1802z00_3015 ==
																							BgL_classz00_3002);
																					}
																				else
																					{	/* Beta/walk.scm 77 */
																						BgL_res1933z00_3007 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2064z00_3717 = BgL_res1933z00_3007;
															}
													}
												else
													{	/* Beta/walk.scm 77 */
														BgL_test2064z00_3717 = ((bool_t) 0);
													}
											}
											if (BgL_test2064z00_3717)
												{	/* Beta/walk.scm 77 */
													BgL_variablez00_bglt BgL_arg1589z00_3017;

													BgL_arg1589z00_3017 =
														(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nz00_3001)))->
														BgL_variablez00);
													{	/* Beta/walk.scm 77 */
														obj_t BgL_classz00_3018;

														BgL_classz00_3018 = BGl_localz00zzast_varz00;
														{	/* Beta/walk.scm 77 */
															BgL_objectz00_bglt BgL_arg1807z00_3019;

															{	/* Beta/walk.scm 77 */
																obj_t BgL_tmpz00_3742;

																BgL_tmpz00_3742 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_arg1589z00_3017));
																BgL_arg1807z00_3019 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_3742);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Beta/walk.scm 77 */
																	long BgL_idxz00_3020;

																	BgL_idxz00_3020 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3019);
																	BgL_test2063z00_3716 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3020 + 2L)) ==
																		BgL_classz00_3018);
																}
															else
																{	/* Beta/walk.scm 77 */
																	bool_t BgL_res1934z00_3023;

																	{	/* Beta/walk.scm 77 */
																		obj_t BgL_oclassz00_3024;

																		{	/* Beta/walk.scm 77 */
																			obj_t BgL_arg1815z00_3025;
																			long BgL_arg1816z00_3026;

																			BgL_arg1815z00_3025 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Beta/walk.scm 77 */
																				long BgL_arg1817z00_3027;

																				BgL_arg1817z00_3027 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3019);
																				BgL_arg1816z00_3026 =
																					(BgL_arg1817z00_3027 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3024 =
																				VECTOR_REF(BgL_arg1815z00_3025,
																				BgL_arg1816z00_3026);
																		}
																		{	/* Beta/walk.scm 77 */
																			bool_t BgL__ortest_1115z00_3028;

																			BgL__ortest_1115z00_3028 =
																				(BgL_classz00_3018 ==
																				BgL_oclassz00_3024);
																			if (BgL__ortest_1115z00_3028)
																				{	/* Beta/walk.scm 77 */
																					BgL_res1934z00_3023 =
																						BgL__ortest_1115z00_3028;
																				}
																			else
																				{	/* Beta/walk.scm 77 */
																					long BgL_odepthz00_3029;

																					{	/* Beta/walk.scm 77 */
																						obj_t BgL_arg1804z00_3030;

																						BgL_arg1804z00_3030 =
																							(BgL_oclassz00_3024);
																						BgL_odepthz00_3029 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3030);
																					}
																					if ((2L < BgL_odepthz00_3029))
																						{	/* Beta/walk.scm 77 */
																							obj_t BgL_arg1802z00_3031;

																							{	/* Beta/walk.scm 77 */
																								obj_t BgL_arg1803z00_3032;

																								BgL_arg1803z00_3032 =
																									(BgL_oclassz00_3024);
																								BgL_arg1802z00_3031 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3032, 2L);
																							}
																							BgL_res1934z00_3023 =
																								(BgL_arg1802z00_3031 ==
																								BgL_classz00_3018);
																						}
																					else
																						{	/* Beta/walk.scm 77 */
																							BgL_res1934z00_3023 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2063z00_3716 = BgL_res1934z00_3023;
																}
														}
													}
												}
											else
												{	/* Beta/walk.scm 77 */
													BgL_test2063z00_3716 = ((bool_t) 0);
												}
										}
										if (BgL_test2063z00_3716)
											{
												obj_t BgL_nz00_3034;

												BgL_nz00_3034 = BgL_nz00_3001;
											BgL_loopz00_3033:
												{	/* Beta/walk.scm 79 */
													obj_t BgL_redz00_3035;

													BgL_redz00_3035 =
														BGl_assqz00zz__r4_pairs_and_lists_6_3z00(
														((obj_t)
															(((BgL_varz00_bglt) COBJECT(
																		((BgL_varz00_bglt) BgL_nz00_3034)))->
																BgL_variablez00)), BgL_stackz00_2868);
													if (PAIRP(BgL_redz00_3035))
														{	/* Beta/walk.scm 83 */
															bool_t BgL_test2073z00_3771;

															{	/* Beta/walk.scm 83 */
																obj_t BgL_arg1584z00_3036;

																BgL_arg1584z00_3036 = CDR(BgL_redz00_3035);
																{	/* Beta/walk.scm 83 */
																	obj_t BgL_classz00_3037;

																	BgL_classz00_3037 =
																		BGl_literalz00zzast_nodez00;
																	if (BGL_OBJECTP(BgL_arg1584z00_3036))
																		{	/* Beta/walk.scm 83 */
																			BgL_objectz00_bglt BgL_arg1807z00_3038;

																			BgL_arg1807z00_3038 =
																				(BgL_objectz00_bglt)
																				(BgL_arg1584z00_3036);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Beta/walk.scm 83 */
																					long BgL_idxz00_3039;

																					BgL_idxz00_3039 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_3038);
																					BgL_test2073z00_3771 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_3039 + 3L)) ==
																						BgL_classz00_3037);
																				}
																			else
																				{	/* Beta/walk.scm 83 */
																					bool_t BgL_res1935z00_3042;

																					{	/* Beta/walk.scm 83 */
																						obj_t BgL_oclassz00_3043;

																						{	/* Beta/walk.scm 83 */
																							obj_t BgL_arg1815z00_3044;
																							long BgL_arg1816z00_3045;

																							BgL_arg1815z00_3044 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Beta/walk.scm 83 */
																								long BgL_arg1817z00_3046;

																								BgL_arg1817z00_3046 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_3038);
																								BgL_arg1816z00_3045 =
																									(BgL_arg1817z00_3046 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_3043 =
																								VECTOR_REF(BgL_arg1815z00_3044,
																								BgL_arg1816z00_3045);
																						}
																						{	/* Beta/walk.scm 83 */
																							bool_t BgL__ortest_1115z00_3047;

																							BgL__ortest_1115z00_3047 =
																								(BgL_classz00_3037 ==
																								BgL_oclassz00_3043);
																							if (BgL__ortest_1115z00_3047)
																								{	/* Beta/walk.scm 83 */
																									BgL_res1935z00_3042 =
																										BgL__ortest_1115z00_3047;
																								}
																							else
																								{	/* Beta/walk.scm 83 */
																									long BgL_odepthz00_3048;

																									{	/* Beta/walk.scm 83 */
																										obj_t BgL_arg1804z00_3049;

																										BgL_arg1804z00_3049 =
																											(BgL_oclassz00_3043);
																										BgL_odepthz00_3048 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_3049);
																									}
																									if ((3L < BgL_odepthz00_3048))
																										{	/* Beta/walk.scm 83 */
																											obj_t BgL_arg1802z00_3050;

																											{	/* Beta/walk.scm 83 */
																												obj_t
																													BgL_arg1803z00_3051;
																												BgL_arg1803z00_3051 =
																													(BgL_oclassz00_3043);
																												BgL_arg1802z00_3050 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_3051,
																													3L);
																											}
																											BgL_res1935z00_3042 =
																												(BgL_arg1802z00_3050 ==
																												BgL_classz00_3037);
																										}
																									else
																										{	/* Beta/walk.scm 83 */
																											BgL_res1935z00_3042 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2073z00_3771 =
																						BgL_res1935z00_3042;
																				}
																		}
																	else
																		{	/* Beta/walk.scm 83 */
																			BgL_test2073z00_3771 = ((bool_t) 0);
																		}
																}
															}
															if (BgL_test2073z00_3771)
																{	/* Beta/walk.scm 84 */
																	BgL_nodez00_bglt BgL_duplicated1116z00_3052;
																	BgL_literalz00_bglt BgL_new1114z00_3053;

																	BgL_duplicated1116z00_3052 =
																		((BgL_nodez00_bglt) CDR(BgL_redz00_3035));
																	{	/* Beta/walk.scm 84 */
																		BgL_literalz00_bglt BgL_new1118z00_3054;

																		BgL_new1118z00_3054 =
																			((BgL_literalz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_literalz00_bgl))));
																		{	/* Beta/walk.scm 84 */
																			long BgL_arg1513z00_3055;

																			BgL_arg1513z00_3055 =
																				BGL_CLASS_NUM
																				(BGl_literalz00zzast_nodez00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1118z00_3054),
																				BgL_arg1513z00_3055);
																		}
																		{	/* Beta/walk.scm 84 */
																			BgL_objectz00_bglt BgL_tmpz00_3801;

																			BgL_tmpz00_3801 =
																				((BgL_objectz00_bglt)
																				BgL_new1118z00_3054);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3801,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1118z00_3054);
																		BgL_new1114z00_3053 = BgL_new1118z00_3054;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1114z00_3053)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(BgL_duplicated1116z00_3052))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1114z00_3053)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																					COBJECT(BgL_duplicated1116z00_3052))->
																				BgL_typez00)), BUNSPEC);
																	((((BgL_atomz00_bglt)
																				COBJECT(((BgL_atomz00_bglt)
																						BgL_new1114z00_3053)))->
																			BgL_valuez00) =
																		((obj_t) (((BgL_atomz00_bglt)
																					COBJECT(((BgL_atomz00_bglt)
																							BgL_duplicated1116z00_3052)))->
																				BgL_valuez00)), BUNSPEC);
																	BgL_arg1489z00_3000 =
																		((obj_t) BgL_new1114z00_3053);
																}
															else
																{	/* Beta/walk.scm 85 */
																	bool_t BgL_test2078z00_3816;

																	{	/* Beta/walk.scm 85 */
																		obj_t BgL_arg1576z00_3056;

																		BgL_arg1576z00_3056 = CDR(BgL_redz00_3035);
																		{	/* Beta/walk.scm 85 */
																			obj_t BgL_classz00_3057;

																			BgL_classz00_3057 =
																				BGl_closurez00zzast_nodez00;
																			if (BGL_OBJECTP(BgL_arg1576z00_3056))
																				{	/* Beta/walk.scm 85 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_3058;
																					BgL_arg1807z00_3058 =
																						(BgL_objectz00_bglt)
																						(BgL_arg1576z00_3056);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Beta/walk.scm 85 */
																							long BgL_idxz00_3059;

																							BgL_idxz00_3059 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_3058);
																							BgL_test2078z00_3816 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_3059 + 3L)) ==
																								BgL_classz00_3057);
																						}
																					else
																						{	/* Beta/walk.scm 85 */
																							bool_t BgL_res1936z00_3062;

																							{	/* Beta/walk.scm 85 */
																								obj_t BgL_oclassz00_3063;

																								{	/* Beta/walk.scm 85 */
																									obj_t BgL_arg1815z00_3064;
																									long BgL_arg1816z00_3065;

																									BgL_arg1815z00_3064 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Beta/walk.scm 85 */
																										long BgL_arg1817z00_3066;

																										BgL_arg1817z00_3066 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_3058);
																										BgL_arg1816z00_3065 =
																											(BgL_arg1817z00_3066 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_3063 =
																										VECTOR_REF
																										(BgL_arg1815z00_3064,
																										BgL_arg1816z00_3065);
																								}
																								{	/* Beta/walk.scm 85 */
																									bool_t
																										BgL__ortest_1115z00_3067;
																									BgL__ortest_1115z00_3067 =
																										(BgL_classz00_3057 ==
																										BgL_oclassz00_3063);
																									if (BgL__ortest_1115z00_3067)
																										{	/* Beta/walk.scm 85 */
																											BgL_res1936z00_3062 =
																												BgL__ortest_1115z00_3067;
																										}
																									else
																										{	/* Beta/walk.scm 85 */
																											long BgL_odepthz00_3068;

																											{	/* Beta/walk.scm 85 */
																												obj_t
																													BgL_arg1804z00_3069;
																												BgL_arg1804z00_3069 =
																													(BgL_oclassz00_3063);
																												BgL_odepthz00_3068 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_3069);
																											}
																											if (
																												(3L <
																													BgL_odepthz00_3068))
																												{	/* Beta/walk.scm 85 */
																													obj_t
																														BgL_arg1802z00_3070;
																													{	/* Beta/walk.scm 85 */
																														obj_t
																															BgL_arg1803z00_3071;
																														BgL_arg1803z00_3071
																															=
																															(BgL_oclassz00_3063);
																														BgL_arg1802z00_3070
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_3071,
																															3L);
																													}
																													BgL_res1936z00_3062 =
																														(BgL_arg1802z00_3070
																														==
																														BgL_classz00_3057);
																												}
																											else
																												{	/* Beta/walk.scm 85 */
																													BgL_res1936z00_3062 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2078z00_3816 =
																								BgL_res1936z00_3062;
																						}
																				}
																			else
																				{	/* Beta/walk.scm 85 */
																					BgL_test2078z00_3816 = ((bool_t) 0);
																				}
																		}
																	}
																	if (BgL_test2078z00_3816)
																		{	/* Beta/walk.scm 86 */
																			BgL_nodez00_bglt
																				BgL_duplicated1121z00_3072;
																			BgL_closurez00_bglt BgL_new1119z00_3073;

																			BgL_duplicated1121z00_3072 =
																				((BgL_nodez00_bglt)
																				CDR(BgL_redz00_3035));
																			{	/* Beta/walk.scm 86 */
																				BgL_closurez00_bglt BgL_new1123z00_3074;

																				BgL_new1123z00_3074 =
																					((BgL_closurez00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_closurez00_bgl))));
																				{	/* Beta/walk.scm 86 */
																					long BgL_arg1516z00_3075;

																					BgL_arg1516z00_3075 =
																						BGL_CLASS_NUM
																						(BGl_closurez00zzast_nodez00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1123z00_3074),
																						BgL_arg1516z00_3075);
																				}
																				{	/* Beta/walk.scm 86 */
																					BgL_objectz00_bglt BgL_tmpz00_3846;

																					BgL_tmpz00_3846 =
																						((BgL_objectz00_bglt)
																						BgL_new1123z00_3074);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_3846, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1123z00_3074);
																				BgL_new1119z00_3073 =
																					BgL_new1123z00_3074;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1119z00_3073)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT
																							(BgL_duplicated1121z00_3072))->
																						BgL_locz00)), BUNSPEC);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_new1119z00_3073)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																							COBJECT
																							(BgL_duplicated1121z00_3072))->
																						BgL_typez00)), BUNSPEC);
																			((((BgL_varz00_bglt)
																						COBJECT(((BgL_varz00_bglt)
																								BgL_new1119z00_3073)))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) ((
																							(BgL_varz00_bglt)
																							COBJECT(((BgL_varz00_bglt)
																									BgL_duplicated1121z00_3072)))->
																						BgL_variablez00)), BUNSPEC);
																			BgL_arg1489z00_3000 =
																				((obj_t) BgL_new1119z00_3073);
																		}
																	else
																		{	/* Beta/walk.scm 87 */
																			bool_t BgL_test2083z00_3861;

																			{	/* Beta/walk.scm 87 */
																				obj_t BgL_arg1575z00_3076;

																				BgL_arg1575z00_3076 =
																					CDR(BgL_redz00_3035);
																				{	/* Beta/walk.scm 87 */
																					obj_t BgL_classz00_3077;

																					BgL_classz00_3077 =
																						BGl_refz00zzast_nodez00;
																					if (BGL_OBJECTP(BgL_arg1575z00_3076))
																						{	/* Beta/walk.scm 87 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_3078;
																							BgL_arg1807z00_3078 =
																								(BgL_objectz00_bglt)
																								(BgL_arg1575z00_3076);
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Beta/walk.scm 87 */
																									long BgL_idxz00_3079;

																									BgL_idxz00_3079 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_3078);
																									BgL_test2083z00_3861 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_3079 + 3L)) ==
																										BgL_classz00_3077);
																								}
																							else
																								{	/* Beta/walk.scm 87 */
																									bool_t BgL_res1937z00_3082;

																									{	/* Beta/walk.scm 87 */
																										obj_t BgL_oclassz00_3083;

																										{	/* Beta/walk.scm 87 */
																											obj_t BgL_arg1815z00_3084;
																											long BgL_arg1816z00_3085;

																											BgL_arg1815z00_3084 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Beta/walk.scm 87 */
																												long
																													BgL_arg1817z00_3086;
																												BgL_arg1817z00_3086 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_3078);
																												BgL_arg1816z00_3085 =
																													(BgL_arg1817z00_3086 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_3083 =
																												VECTOR_REF
																												(BgL_arg1815z00_3084,
																												BgL_arg1816z00_3085);
																										}
																										{	/* Beta/walk.scm 87 */
																											bool_t
																												BgL__ortest_1115z00_3087;
																											BgL__ortest_1115z00_3087 =
																												(BgL_classz00_3077 ==
																												BgL_oclassz00_3083);
																											if (BgL__ortest_1115z00_3087)
																												{	/* Beta/walk.scm 87 */
																													BgL_res1937z00_3082 =
																														BgL__ortest_1115z00_3087;
																												}
																											else
																												{	/* Beta/walk.scm 87 */
																													long
																														BgL_odepthz00_3088;
																													{	/* Beta/walk.scm 87 */
																														obj_t
																															BgL_arg1804z00_3089;
																														BgL_arg1804z00_3089
																															=
																															(BgL_oclassz00_3083);
																														BgL_odepthz00_3088 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_3089);
																													}
																													if (
																														(3L <
																															BgL_odepthz00_3088))
																														{	/* Beta/walk.scm 87 */
																															obj_t
																																BgL_arg1802z00_3090;
																															{	/* Beta/walk.scm 87 */
																																obj_t
																																	BgL_arg1803z00_3091;
																																BgL_arg1803z00_3091
																																	=
																																	(BgL_oclassz00_3083);
																																BgL_arg1802z00_3090
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_3091,
																																	3L);
																															}
																															BgL_res1937z00_3082
																																=
																																(BgL_arg1802z00_3090
																																==
																																BgL_classz00_3077);
																														}
																													else
																														{	/* Beta/walk.scm 87 */
																															BgL_res1937z00_3082
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test2083z00_3861 =
																										BgL_res1937z00_3082;
																								}
																						}
																					else
																						{	/* Beta/walk.scm 87 */
																							BgL_test2083z00_3861 =
																								((bool_t) 0);
																						}
																				}
																			}
																			if (BgL_test2083z00_3861)
																				{	/* Beta/walk.scm 88 */
																					obj_t BgL_mz00_3092;

																					BgL_mz00_3092 = CDR(BgL_redz00_3035);
																					{	/* Beta/walk.scm 89 */
																						bool_t BgL_test2088z00_3886;

																						{	/* Beta/walk.scm 89 */
																							bool_t BgL_test2089z00_3887;

																							{	/* Beta/walk.scm 89 */
																								obj_t BgL_classz00_3093;

																								BgL_classz00_3093 =
																									BGl_varz00zzast_nodez00;
																								if (BGL_OBJECTP(BgL_mz00_3092))
																									{	/* Beta/walk.scm 89 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_3094;
																										BgL_arg1807z00_3094 =
																											(BgL_objectz00_bglt)
																											(BgL_mz00_3092);
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Beta/walk.scm 89 */
																												long BgL_idxz00_3095;

																												BgL_idxz00_3095 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_3094);
																												BgL_test2089z00_3887 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_3095 +
																															2L)) ==
																													BgL_classz00_3093);
																											}
																										else
																											{	/* Beta/walk.scm 89 */
																												bool_t
																													BgL_res1938z00_3098;
																												{	/* Beta/walk.scm 89 */
																													obj_t
																														BgL_oclassz00_3099;
																													{	/* Beta/walk.scm 89 */
																														obj_t
																															BgL_arg1815z00_3100;
																														long
																															BgL_arg1816z00_3101;
																														BgL_arg1815z00_3100
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Beta/walk.scm 89 */
																															long
																																BgL_arg1817z00_3102;
																															BgL_arg1817z00_3102
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_3094);
																															BgL_arg1816z00_3101
																																=
																																(BgL_arg1817z00_3102
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_3099 =
																															VECTOR_REF
																															(BgL_arg1815z00_3100,
																															BgL_arg1816z00_3101);
																													}
																													{	/* Beta/walk.scm 89 */
																														bool_t
																															BgL__ortest_1115z00_3103;
																														BgL__ortest_1115z00_3103
																															=
																															(BgL_classz00_3093
																															==
																															BgL_oclassz00_3099);
																														if (BgL__ortest_1115z00_3103)
																															{	/* Beta/walk.scm 89 */
																																BgL_res1938z00_3098
																																	=
																																	BgL__ortest_1115z00_3103;
																															}
																														else
																															{	/* Beta/walk.scm 89 */
																																long
																																	BgL_odepthz00_3104;
																																{	/* Beta/walk.scm 89 */
																																	obj_t
																																		BgL_arg1804z00_3105;
																																	BgL_arg1804z00_3105
																																		=
																																		(BgL_oclassz00_3099);
																																	BgL_odepthz00_3104
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_3105);
																																}
																																if (
																																	(2L <
																																		BgL_odepthz00_3104))
																																	{	/* Beta/walk.scm 89 */
																																		obj_t
																																			BgL_arg1802z00_3106;
																																		{	/* Beta/walk.scm 89 */
																																			obj_t
																																				BgL_arg1803z00_3107;
																																			BgL_arg1803z00_3107
																																				=
																																				(BgL_oclassz00_3099);
																																			BgL_arg1802z00_3106
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_3107,
																																				2L);
																																		}
																																		BgL_res1938z00_3098
																																			=
																																			(BgL_arg1802z00_3106
																																			==
																																			BgL_classz00_3093);
																																	}
																																else
																																	{	/* Beta/walk.scm 89 */
																																		BgL_res1938z00_3098
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test2089z00_3887 =
																													BgL_res1938z00_3098;
																											}
																									}
																								else
																									{	/* Beta/walk.scm 89 */
																										BgL_test2089z00_3887 =
																											((bool_t) 0);
																									}
																							}
																							if (BgL_test2089z00_3887)
																								{	/* Beta/walk.scm 89 */
																									BgL_variablez00_bglt
																										BgL_arg1553z00_3108;
																									BgL_arg1553z00_3108 =
																										(((BgL_varz00_bglt)
																											COBJECT(((BgL_varz00_bglt)
																													BgL_mz00_3092)))->
																										BgL_variablez00);
																									{	/* Beta/walk.scm 89 */
																										obj_t BgL_classz00_3109;

																										BgL_classz00_3109 =
																											BGl_localz00zzast_varz00;
																										{	/* Beta/walk.scm 89 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_3110;
																											{	/* Beta/walk.scm 89 */
																												obj_t BgL_tmpz00_3912;

																												BgL_tmpz00_3912 =
																													((obj_t)
																													((BgL_objectz00_bglt)
																														BgL_arg1553z00_3108));
																												BgL_arg1807z00_3110 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_3912);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Beta/walk.scm 89 */
																													long BgL_idxz00_3111;

																													BgL_idxz00_3111 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_3110);
																													BgL_test2088z00_3886 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_3111 +
																																2L)) ==
																														BgL_classz00_3109);
																												}
																											else
																												{	/* Beta/walk.scm 89 */
																													bool_t
																														BgL_res1939z00_3114;
																													{	/* Beta/walk.scm 89 */
																														obj_t
																															BgL_oclassz00_3115;
																														{	/* Beta/walk.scm 89 */
																															obj_t
																																BgL_arg1815z00_3116;
																															long
																																BgL_arg1816z00_3117;
																															BgL_arg1815z00_3116
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Beta/walk.scm 89 */
																																long
																																	BgL_arg1817z00_3118;
																																BgL_arg1817z00_3118
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_3110);
																																BgL_arg1816z00_3117
																																	=
																																	(BgL_arg1817z00_3118
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_3115
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_3116,
																																BgL_arg1816z00_3117);
																														}
																														{	/* Beta/walk.scm 89 */
																															bool_t
																																BgL__ortest_1115z00_3119;
																															BgL__ortest_1115z00_3119
																																=
																																(BgL_classz00_3109
																																==
																																BgL_oclassz00_3115);
																															if (BgL__ortest_1115z00_3119)
																																{	/* Beta/walk.scm 89 */
																																	BgL_res1939z00_3114
																																		=
																																		BgL__ortest_1115z00_3119;
																																}
																															else
																																{	/* Beta/walk.scm 89 */
																																	long
																																		BgL_odepthz00_3120;
																																	{	/* Beta/walk.scm 89 */
																																		obj_t
																																			BgL_arg1804z00_3121;
																																		BgL_arg1804z00_3121
																																			=
																																			(BgL_oclassz00_3115);
																																		BgL_odepthz00_3120
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_3121);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_3120))
																																		{	/* Beta/walk.scm 89 */
																																			obj_t
																																				BgL_arg1802z00_3122;
																																			{	/* Beta/walk.scm 89 */
																																				obj_t
																																					BgL_arg1803z00_3123;
																																				BgL_arg1803z00_3123
																																					=
																																					(BgL_oclassz00_3115);
																																				BgL_arg1802z00_3122
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_3123,
																																					2L);
																																			}
																																			BgL_res1939z00_3114
																																				=
																																				(BgL_arg1802z00_3122
																																				==
																																				BgL_classz00_3109);
																																		}
																																	else
																																		{	/* Beta/walk.scm 89 */
																																			BgL_res1939z00_3114
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test2088z00_3886 =
																														BgL_res1939z00_3114;
																												}
																										}
																									}
																								}
																							else
																								{	/* Beta/walk.scm 89 */
																									BgL_test2088z00_3886 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test2088z00_3886)
																							{	/* Beta/walk.scm 90 */
																								BgL_refz00_bglt
																									BgL_arg1544z00_3124;
																								{	/* Beta/walk.scm 90 */
																									BgL_nodez00_bglt
																										BgL_duplicated1126z00_3125;
																									BgL_refz00_bglt
																										BgL_new1124z00_3126;
																									BgL_duplicated1126z00_3125 =
																										((BgL_nodez00_bglt)
																										CDR(BgL_redz00_3035));
																									{	/* Beta/walk.scm 90 */
																										BgL_refz00_bglt
																											BgL_new1129z00_3127;
																										BgL_new1129z00_3127 =
																											((BgL_refz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_refz00_bgl))));
																										{	/* Beta/walk.scm 90 */
																											long BgL_arg1546z00_3128;

																											BgL_arg1546z00_3128 =
																												BGL_CLASS_NUM
																												(BGl_refz00zzast_nodez00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt)
																													BgL_new1129z00_3127),
																												BgL_arg1546z00_3128);
																										}
																										{	/* Beta/walk.scm 90 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_3941;
																											BgL_tmpz00_3941 =
																												((BgL_objectz00_bglt)
																												BgL_new1129z00_3127);
																											BGL_OBJECT_WIDENING_SET
																												(BgL_tmpz00_3941,
																												BFALSE);
																										}
																										((BgL_objectz00_bglt)
																											BgL_new1129z00_3127);
																										BgL_new1124z00_3126 =
																											BgL_new1129z00_3127;
																									}
																									((((BgL_nodez00_bglt) COBJECT(
																													((BgL_nodez00_bglt)
																														BgL_new1124z00_3126)))->
																											BgL_locz00) =
																										((obj_t) ((
																													(BgL_nodez00_bglt)
																													COBJECT
																													(BgL_duplicated1126z00_3125))->
																												BgL_locz00)), BUNSPEC);
																									((((BgL_nodez00_bglt)
																												COBJECT((
																														(BgL_nodez00_bglt)
																														BgL_new1124z00_3126)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt) ((
																													(BgL_nodez00_bglt)
																													COBJECT
																													(BgL_duplicated1126z00_3125))->
																												BgL_typez00)), BUNSPEC);
																									((((BgL_varz00_bglt)
																												COBJECT((
																														(BgL_varz00_bglt)
																														BgL_new1124z00_3126)))->
																											BgL_variablez00) =
																										((BgL_variablez00_bglt) ((
																													(BgL_varz00_bglt)
																													COBJECT((
																															(BgL_varz00_bglt)
																															BgL_duplicated1126z00_3125)))->
																												BgL_variablez00)),
																										BUNSPEC);
																									BgL_arg1544z00_3124 =
																										BgL_new1124z00_3126;
																								}
																								{
																									obj_t BgL_nz00_3955;

																									BgL_nz00_3955 =
																										((obj_t)
																										BgL_arg1544z00_3124);
																									BgL_nz00_3034 = BgL_nz00_3955;
																									goto BgL_loopz00_3033;
																								}
																							}
																						else
																							{	/* Beta/walk.scm 91 */
																								BgL_nodez00_bglt
																									BgL_duplicated1132z00_3129;
																								BgL_refz00_bglt
																									BgL_new1130z00_3130;
																								BgL_duplicated1132z00_3129 =
																									((BgL_nodez00_bglt)
																									CDR(BgL_redz00_3035));
																								{	/* Beta/walk.scm 91 */
																									BgL_refz00_bglt
																										BgL_new1134z00_3131;
																									BgL_new1134z00_3131 =
																										((BgL_refz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_refz00_bgl))));
																									{	/* Beta/walk.scm 91 */
																										long BgL_arg1552z00_3132;

																										BgL_arg1552z00_3132 =
																											BGL_CLASS_NUM
																											(BGl_refz00zzast_nodez00);
																										BGL_OBJECT_CLASS_NUM_SET((
																												(BgL_objectz00_bglt)
																												BgL_new1134z00_3131),
																											BgL_arg1552z00_3132);
																									}
																									{	/* Beta/walk.scm 91 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_3963;
																										BgL_tmpz00_3963 =
																											((BgL_objectz00_bglt)
																											BgL_new1134z00_3131);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_3963, BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_new1134z00_3131);
																									BgL_new1130z00_3130 =
																										BgL_new1134z00_3131;
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1130z00_3130)))->
																										BgL_locz00) =
																									((obj_t) (((BgL_nodez00_bglt)
																												COBJECT
																												(BgL_duplicated1132z00_3129))->
																											BgL_locz00)), BUNSPEC);
																								((((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_new1130z00_3130)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt) ((
																												(BgL_nodez00_bglt)
																												COBJECT
																												(BgL_duplicated1132z00_3129))->
																											BgL_typez00)), BUNSPEC);
																								((((BgL_varz00_bglt)
																											COBJECT(((BgL_varz00_bglt)
																													BgL_new1130z00_3130)))->
																										BgL_variablez00) =
																									((BgL_variablez00_bglt) ((
																												(BgL_varz00_bglt)
																												COBJECT((
																														(BgL_varz00_bglt)
																														BgL_duplicated1132z00_3129)))->
																											BgL_variablez00)),
																									BUNSPEC);
																								BgL_arg1489z00_3000 =
																									((obj_t) BgL_new1130z00_3130);
																				}}}
																			else
																				{	/* Beta/walk.scm 92 */
																					bool_t BgL_test2097z00_3978;

																					{	/* Beta/walk.scm 92 */
																						obj_t BgL_arg1573z00_3133;

																						BgL_arg1573z00_3133 =
																							CDR(BgL_redz00_3035);
																						{	/* Beta/walk.scm 92 */
																							obj_t BgL_classz00_3134;

																							BgL_classz00_3134 =
																								BGl_patchz00zzast_nodez00;
																							if (BGL_OBJECTP
																								(BgL_arg1573z00_3133))
																								{	/* Beta/walk.scm 92 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_3135;
																									BgL_arg1807z00_3135 =
																										(BgL_objectz00_bglt)
																										(BgL_arg1573z00_3133);
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Beta/walk.scm 92 */
																											long BgL_idxz00_3136;

																											BgL_idxz00_3136 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_3135);
																											BgL_test2097z00_3978 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_3136 +
																														3L)) ==
																												BgL_classz00_3134);
																										}
																									else
																										{	/* Beta/walk.scm 92 */
																											bool_t
																												BgL_res1940z00_3139;
																											{	/* Beta/walk.scm 92 */
																												obj_t
																													BgL_oclassz00_3140;
																												{	/* Beta/walk.scm 92 */
																													obj_t
																														BgL_arg1815z00_3141;
																													long
																														BgL_arg1816z00_3142;
																													BgL_arg1815z00_3141 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Beta/walk.scm 92 */
																														long
																															BgL_arg1817z00_3143;
																														BgL_arg1817z00_3143
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_3135);
																														BgL_arg1816z00_3142
																															=
																															(BgL_arg1817z00_3143
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_3140 =
																														VECTOR_REF
																														(BgL_arg1815z00_3141,
																														BgL_arg1816z00_3142);
																												}
																												{	/* Beta/walk.scm 92 */
																													bool_t
																														BgL__ortest_1115z00_3144;
																													BgL__ortest_1115z00_3144
																														=
																														(BgL_classz00_3134
																														==
																														BgL_oclassz00_3140);
																													if (BgL__ortest_1115z00_3144)
																														{	/* Beta/walk.scm 92 */
																															BgL_res1940z00_3139
																																=
																																BgL__ortest_1115z00_3144;
																														}
																													else
																														{	/* Beta/walk.scm 92 */
																															long
																																BgL_odepthz00_3145;
																															{	/* Beta/walk.scm 92 */
																																obj_t
																																	BgL_arg1804z00_3146;
																																BgL_arg1804z00_3146
																																	=
																																	(BgL_oclassz00_3140);
																																BgL_odepthz00_3145
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_3146);
																															}
																															if (
																																(3L <
																																	BgL_odepthz00_3145))
																																{	/* Beta/walk.scm 92 */
																																	obj_t
																																		BgL_arg1802z00_3147;
																																	{	/* Beta/walk.scm 92 */
																																		obj_t
																																			BgL_arg1803z00_3148;
																																		BgL_arg1803z00_3148
																																			=
																																			(BgL_oclassz00_3140);
																																		BgL_arg1802z00_3147
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_3148,
																																			3L);
																																	}
																																	BgL_res1940z00_3139
																																		=
																																		(BgL_arg1802z00_3147
																																		==
																																		BgL_classz00_3134);
																																}
																															else
																																{	/* Beta/walk.scm 92 */
																																	BgL_res1940z00_3139
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2097z00_3978 =
																												BgL_res1940z00_3139;
																										}
																								}
																							else
																								{	/* Beta/walk.scm 92 */
																									BgL_test2097z00_3978 =
																										((bool_t) 0);
																								}
																						}
																					}
																					if (BgL_test2097z00_3978)
																						{	/* Beta/walk.scm 93 */
																							BgL_nodez00_bglt
																								BgL_duplicated1137z00_3149;
																							BgL_patchz00_bglt
																								BgL_new1135z00_3150;
																							BgL_duplicated1137z00_3149 =
																								((BgL_nodez00_bglt)
																								CDR(BgL_redz00_3035));
																							{	/* Beta/walk.scm 93 */
																								BgL_patchz00_bglt
																									BgL_new1142z00_3151;
																								BgL_new1142z00_3151 =
																									((BgL_patchz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_patchz00_bgl))));
																								{	/* Beta/walk.scm 93 */
																									long BgL_arg1564z00_3152;

																									BgL_arg1564z00_3152 =
																										BGL_CLASS_NUM
																										(BGl_patchz00zzast_nodez00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1142z00_3151),
																										BgL_arg1564z00_3152);
																								}
																								{	/* Beta/walk.scm 93 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_4008;
																									BgL_tmpz00_4008 =
																										((BgL_objectz00_bglt)
																										BgL_new1142z00_3151);
																									BGL_OBJECT_WIDENING_SET
																										(BgL_tmpz00_4008, BFALSE);
																								}
																								((BgL_objectz00_bglt)
																									BgL_new1142z00_3151);
																								BgL_new1135z00_3150 =
																									BgL_new1142z00_3151;
																							}
																							((((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												BgL_new1135z00_3150)))->
																									BgL_locz00) =
																								((obj_t) (((BgL_nodez00_bglt)
																											COBJECT
																											(BgL_duplicated1137z00_3149))->
																										BgL_locz00)), BUNSPEC);
																							((((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												BgL_new1135z00_3150)))->
																									BgL_typez00) =
																								((BgL_typez00_bglt) ((
																											(BgL_nodez00_bglt)
																											COBJECT
																											(BgL_duplicated1137z00_3149))->
																										BgL_typez00)), BUNSPEC);
																							((((BgL_atomz00_bglt)
																										COBJECT(((BgL_atomz00_bglt)
																												BgL_new1135z00_3150)))->
																									BgL_valuez00) =
																								((obj_t) (((BgL_atomz00_bglt)
																											COBJECT((
																													(BgL_atomz00_bglt)
																													BgL_duplicated1137z00_3149)))->
																										BgL_valuez00)), BUNSPEC);
																							((((BgL_patchz00_bglt)
																										COBJECT
																										(BgL_new1135z00_3150))->
																									BgL_refz00) =
																								((BgL_varz00_bglt) ((
																											(BgL_patchz00_bglt)
																											COBJECT((
																													(BgL_patchz00_bglt)
																													BgL_duplicated1137z00_3149)))->
																										BgL_refz00)), BUNSPEC);
																							((((BgL_patchz00_bglt)
																										COBJECT
																										(BgL_new1135z00_3150))->
																									BgL_indexz00) =
																								((long) (((BgL_patchz00_bglt)
																											COBJECT((
																													(BgL_patchz00_bglt)
																													BgL_duplicated1137z00_3149)))->
																										BgL_indexz00)), BUNSPEC);
																							((((BgL_patchz00_bglt)
																										COBJECT
																										(BgL_new1135z00_3150))->
																									BgL_patchidz00) =
																								((obj_t) (((BgL_patchz00_bglt)
																											COBJECT((
																													(BgL_patchz00_bglt)
																													BgL_duplicated1137z00_3149)))->
																										BgL_patchidz00)), BUNSPEC);
																							BgL_arg1489z00_3000 =
																								((obj_t) BgL_new1135z00_3150);
																						}
																					else
																						{	/* Beta/walk.scm 92 */
																							BgL_arg1489z00_3000 =
																								BGl_errorz00zz__errorz00
																								(BGl_string1962z00zzbeta_walkz00,
																								BGl_string1980z00zzbeta_walkz00,
																								bgl_typeof(CDR
																									(BgL_redz00_3035)));
																						}
																				}
																		}
																}
														}
													else
														{	/* Beta/walk.scm 81 */
															BgL_arg1489z00_3000 = BgL_nz00_3034;
														}
												}
											}
										else
											{	/* Beta/walk.scm 77 */
												BgL_arg1489z00_3000 =
													((obj_t)
													BGl_nodezd2betaz12zc0zzbeta_walkz00(
														((BgL_nodez00_bglt) BgL_nz00_3001),
														BgL_stackz00_2868));
											}
									}
								}
								{	/* Beta/walk.scm 97 */
									obj_t BgL_tmpz00_4038;

									BgL_tmpz00_4038 = ((obj_t) BgL_l1277z00_2999);
									SET_CAR(BgL_tmpz00_4038, BgL_arg1489z00_3000);
								}
							}
							{	/* Beta/walk.scm 97 */
								obj_t BgL_arg1591z00_3153;

								BgL_arg1591z00_3153 = CDR(((obj_t) BgL_l1277z00_2999));
								{
									obj_t BgL_l1277z00_4043;

									BgL_l1277z00_4043 = BgL_arg1591z00_3153;
									BgL_l1277z00_2999 = BgL_l1277z00_4043;
									goto BgL_zc3z04anonymousza31486ze3z87_2998;
								}
							}
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2867));
		}

	}



/* &node-beta!-sync1293 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2sync1293z70zzbeta_walkz00(obj_t
		BgL_envz00_2869, obj_t BgL_nodez00_2870, obj_t BgL_stackz00_2871)
	{
		{	/* Beta/walk.scm 65 */
			((((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2870)))->BgL_mutexz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2870)))->BgL_mutexz00),
						BgL_stackz00_2871)), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2870)))->
					BgL_prelockz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2870)))->
							BgL_prelockz00), BgL_stackz00_2871)), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2870)))->
					BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_nodezd2betaz12zc0zzbeta_walkz00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2870)))->BgL_bodyz00),
						BgL_stackz00_2871)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2870));
		}

	}



/* &node-beta!-sequence1291 */
	BgL_nodez00_bglt BGl_z62nodezd2betaz12zd2sequence1291z70zzbeta_walkz00(obj_t
		BgL_envz00_2872, obj_t BgL_nodez00_2873, obj_t BgL_stackz00_2874)
	{
		{	/* Beta/walk.scm 58 */
			BGl_nodezd2betaza2z12z62zzbeta_walkz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2873)))->BgL_nodesz00),
				BgL_stackz00_2874);
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2873));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbeta_walkz00(void)
	{
		{	/* Beta/walk.scm 18 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
			return
				BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1981z00zzbeta_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
