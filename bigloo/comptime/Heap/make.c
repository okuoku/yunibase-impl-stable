/*===========================================================================*/
/*   (Heap/make.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Heap/make.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_HEAP_MAKE_TYPE_DEFINITIONS
#define BGL_HEAP_MAKE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_HEAP_MAKE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzheap_makez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31202ze3ze5zzheap_makez00(obj_t, obj_t);
	extern obj_t BGl_za2cczd2optionsza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_makezd2addzd2heapz00zzheap_makez00(void);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t output_obj(obj_t, obj_t);
	extern obj_t BGl_getzd2tenvzd2zztype_envz00(void);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzheap_makez00(void);
	static obj_t BGl_objectzd2initzd2zzheap_makez00(void);
	BGL_IMPORT obj_t BGl_setzd2objzd2stringzd2modez12zc0zz__intextz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62preparezd2globalsz12za2zzheap_makez00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzheap_makez00(void);
	extern obj_t BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00;
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_z62zc3z04anonymousza31147ze3ze5zzheap_makez00(obj_t, obj_t);
	static obj_t BGl_z62makezd2heapzb0zzheap_makez00(obj_t);
	extern obj_t BGl_za2heapzd2libraryza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t close_binary_port(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62preparezd2additionalzd2globalsz12z70zzheap_makez00(obj_t);
	extern obj_t BGl_unbindzd2globalz12zc0zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_za2bigloozd2versionza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_makezd2heapzd2zzheap_makez00(void);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzheap_makez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzheap_restorez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__intextz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_za2heapzd2nameza2zd2zzengine_paramz00;
	extern obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00;
	static obj_t
		BGl_z62checkzd2additionalzd2heapzd2libraryzb0zzheap_makez00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzheap_makez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzheap_makez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzheap_makez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzheap_makez00(void);
	static obj_t BGl_z62makezd2addzd2heapz62zzheap_makez00(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_za2additionalzd2heapzd2nameza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00;
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_getzd2genvzd2zzast_envz00(void);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[9];


	   
		 
		DEFINE_STRING(BGl_string1241z00zzheap_makez00,
		BgL_bgl_string1241za700za7za7h1269za7, "Heap", 4);
	      DEFINE_STRING(BGl_string1242z00zzheap_makez00,
		BgL_bgl_string1242za700za7za7h1270za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1243z00zzheap_makez00,
		BgL_bgl_string1243za700za7za7h1271za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1244z00zzheap_makez00,
		BgL_bgl_string1244za700za7za7h1272za7, "make-heap", 9);
	      DEFINE_STRING(BGl_string1245z00zzheap_makez00,
		BgL_bgl_string1245za700za7za7h1273za7, "Can't open output port", 22);
	      DEFINE_STRING(BGl_string1246z00zzheap_makez00,
		BgL_bgl_string1246za700za7za7h1274za7, "Illegal heap's name", 19);
	      DEFINE_STRING(BGl_string1248z00zzheap_makez00,
		BgL_bgl_string1248za700za7za7h1275za7, "Library heap", 12);
	      DEFINE_STRING(BGl_string1249z00zzheap_makez00,
		BgL_bgl_string1249za700za7za7h1276za7, "make-addd-heap", 14);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_preparezd2additionalzd2globalsz12zd2envzc0zzheap_makez00,
		BgL_bgl_za762prepareza7d2add1277z00,
		BGl_z62preparezd2additionalzd2globalsz12z70zzheap_makez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1250z00zzheap_makez00,
		BgL_bgl_string1250za700za7za7h1278za7, "make-add-heap", 13);
	      DEFINE_STRING(BGl_string1251z00zzheap_makez00,
		BgL_bgl_string1251za700za7za7h1279za7,
		"Illegal reserved identifier for additional heap library (see `-heap-library' compiler option)",
		93);
	      DEFINE_STRING(BGl_string1252z00zzheap_makez00,
		BgL_bgl_string1252za700za7za7h1280za7,
		"Missing additional heap library identifier (see `-heap-library' compiler option)",
		80);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1247z00zzheap_makez00,
		BgL_bgl_za762za7c3za704anonymo1281za7,
		BGl_z62zc3z04anonymousza31147ze3ze5zzheap_makez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1254z00zzheap_makez00,
		BgL_bgl_string1254za700za7za7h1282za7, "heap_make", 9);
	      DEFINE_STRING(BGl_string1255z00zzheap_makez00,
		BgL_bgl_string1255za700za7za7h1283za7,
		"make-add-heap bigloo (check-additional-heap-library prepare-additional-globals!) import export static pair pass-started (prepare-globals!) ",
		139);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1253z00zzheap_makez00,
		BgL_bgl_za762za7c3za704anonymo1284za7,
		BGl_z62zc3z04anonymousza31202ze3ze5zzheap_makez00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_checkzd2additionalzd2heapzd2libraryzd2envz00zzheap_makez00,
		BgL_bgl_za762checkza7d2addit1285z00,
		BGl_z62checkzd2additionalzd2heapzd2libraryzb0zzheap_makez00, 0L, BUNSPEC,
		0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_preparezd2globalsz12zd2envz12zzheap_makez00,
		BgL_bgl_za762prepareza7d2glo1286z00,
		BGl_z62preparezd2globalsz12za2zzheap_makez00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2heapzd2envz00zzheap_makez00,
		BgL_bgl_za762makeza7d2heapza7b1287za7, BGl_z62makezd2heapzb0zzheap_makez00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2addzd2heapzd2envzd2zzheap_makez00,
		BgL_bgl_za762makeza7d2addza7d21288za7,
		BGl_z62makezd2addzd2heapz62zzheap_makez00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzheap_makez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzheap_makez00(long
		BgL_checksumz00_823, char *BgL_fromz00_824)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzheap_makez00))
				{
					BGl_requirezd2initializa7ationz75zzheap_makez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzheap_makez00();
					BGl_libraryzd2moduleszd2initz00zzheap_makez00();
					BGl_cnstzd2initzd2zzheap_makez00();
					BGl_importedzd2moduleszd2initz00zzheap_makez00();
					return BGl_methodzd2initzd2zzheap_makez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzheap_makez00(void)
	{
		{	/* Heap/make.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__intextz00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__binaryz00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"heap_make");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "heap_make");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"heap_make");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzheap_makez00(void)
	{
		{	/* Heap/make.scm 15 */
			{	/* Heap/make.scm 15 */
				obj_t BgL_cportz00_792;

				{	/* Heap/make.scm 15 */
					obj_t BgL_stringz00_799;

					BgL_stringz00_799 = BGl_string1255z00zzheap_makez00;
					{	/* Heap/make.scm 15 */
						obj_t BgL_startz00_800;

						BgL_startz00_800 = BINT(0L);
						{	/* Heap/make.scm 15 */
							obj_t BgL_endz00_801;

							BgL_endz00_801 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_799)));
							{	/* Heap/make.scm 15 */

								BgL_cportz00_792 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_799, BgL_startz00_800, BgL_endz00_801);
				}}}}
				{
					long BgL_iz00_793;

					BgL_iz00_793 = 8L;
				BgL_loopz00_794:
					if ((BgL_iz00_793 == -1L))
						{	/* Heap/make.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Heap/make.scm 15 */
							{	/* Heap/make.scm 15 */
								obj_t BgL_arg1268z00_795;

								{	/* Heap/make.scm 15 */

									{	/* Heap/make.scm 15 */
										obj_t BgL_locationz00_797;

										BgL_locationz00_797 = BBOOL(((bool_t) 0));
										{	/* Heap/make.scm 15 */

											BgL_arg1268z00_795 =
												BGl_readz00zz__readerz00(BgL_cportz00_792,
												BgL_locationz00_797);
										}
									}
								}
								{	/* Heap/make.scm 15 */
									int BgL_tmpz00_854;

									BgL_tmpz00_854 = (int) (BgL_iz00_793);
									CNST_TABLE_SET(BgL_tmpz00_854, BgL_arg1268z00_795);
							}}
							{	/* Heap/make.scm 15 */
								int BgL_auxz00_798;

								BgL_auxz00_798 = (int) ((BgL_iz00_793 - 1L));
								{
									long BgL_iz00_859;

									BgL_iz00_859 = (long) (BgL_auxz00_798);
									BgL_iz00_793 = BgL_iz00_859;
									goto BgL_loopz00_794;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzheap_makez00(void)
	{
		{	/* Heap/make.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* make-heap */
	BGL_EXPORTED_DEF obj_t BGl_makezd2heapzd2zzheap_makez00(void)
	{
		{	/* Heap/make.scm 35 */
			{	/* Heap/make.scm 36 */
				obj_t BgL_list1116z00_548;

				{	/* Heap/make.scm 36 */
					obj_t BgL_arg1122z00_549;

					{	/* Heap/make.scm 36 */
						obj_t BgL_arg1123z00_550;

						BgL_arg1123z00_550 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1122z00_549 =
							MAKE_YOUNG_PAIR(BGl_string1241z00zzheap_makez00,
							BgL_arg1123z00_550);
					}
					BgL_list1116z00_548 =
						MAKE_YOUNG_PAIR(BGl_string1242z00zzheap_makez00,
						BgL_arg1122z00_549);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1116z00_548);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1241z00zzheap_makez00;
			{	/* Heap/make.scm 36 */
				obj_t BgL_g1070z00_551;
				obj_t BgL_g1071z00_552;

				{	/* Heap/make.scm 36 */
					obj_t BgL_list1134z00_565;

					BgL_list1134z00_565 =
						MAKE_YOUNG_PAIR(BGl_preparezd2globalsz12zd2envz12zzheap_makez00,
						BNIL);
					BgL_g1070z00_551 = BgL_list1134z00_565;
				}
				BgL_g1071z00_552 = CNST_TABLE_REF(0);
				{
					obj_t BgL_hooksz00_554;
					obj_t BgL_hnamesz00_555;

					BgL_hooksz00_554 = BgL_g1070z00_551;
					BgL_hnamesz00_555 = BgL_g1071z00_552;
				BgL_zc3z04anonymousza31124ze3z87_556:
					if (NULLP(BgL_hooksz00_554))
						{	/* Heap/make.scm 36 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Heap/make.scm 36 */
							bool_t BgL_test1292z00_874;

							{	/* Heap/make.scm 36 */
								obj_t BgL_fun1133z00_563;

								BgL_fun1133z00_563 = CAR(((obj_t) BgL_hooksz00_554));
								BgL_test1292z00_874 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1133z00_563));
							}
							if (BgL_test1292z00_874)
								{	/* Heap/make.scm 36 */
									obj_t BgL_arg1129z00_560;
									obj_t BgL_arg1131z00_561;

									BgL_arg1129z00_560 = CDR(((obj_t) BgL_hooksz00_554));
									BgL_arg1131z00_561 = CDR(((obj_t) BgL_hnamesz00_555));
									{
										obj_t BgL_hnamesz00_886;
										obj_t BgL_hooksz00_885;

										BgL_hooksz00_885 = BgL_arg1129z00_560;
										BgL_hnamesz00_886 = BgL_arg1131z00_561;
										BgL_hnamesz00_555 = BgL_hnamesz00_886;
										BgL_hooksz00_554 = BgL_hooksz00_885;
										goto BgL_zc3z04anonymousza31124ze3z87_556;
									}
								}
							else
								{	/* Heap/make.scm 36 */
									obj_t BgL_arg1132z00_562;

									BgL_arg1132z00_562 = CAR(((obj_t) BgL_hnamesz00_555));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1241z00zzheap_makez00,
										BGl_string1243z00zzheap_makez00, BgL_arg1132z00_562);
								}
						}
				}
			}
			BGl_setzd2objzd2stringzd2modez12zc0zz__intextz00(CNST_TABLE_REF(2));
			if (STRINGP(BGl_za2heapzd2nameza2zd2zzengine_paramz00))
				{	/* Heap/make.scm 40 */
					obj_t BgL_hnamez00_567;

					{	/* Heap/make.scm 40 */
						obj_t BgL_arg1143z00_576;

						BgL_arg1143z00_576 = CAR(BGl_za2libzd2dirza2zd2zzengine_paramz00);
						BgL_hnamez00_567 =
							BGl_makezd2filezd2namez00zz__osz00(BgL_arg1143z00_576,
							BGl_za2heapzd2nameza2zd2zzengine_paramz00);
					}
					{	/* Heap/make.scm 41 */
						obj_t BgL_portz00_568;

						BgL_portz00_568 =
							BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00
							(BgL_hnamez00_567);
						if (BINARY_PORTP(BgL_portz00_568))
							{	/* Heap/make.scm 42 */
								{	/* Heap/make.scm 46 */
									obj_t BgL_arg1137z00_570;

									{	/* Heap/make.scm 46 */
										obj_t BgL_v1105z00_571;

										BgL_v1105z00_571 = create_vector(5L);
										{	/* Heap/make.scm 46 */
											obj_t BgL_arg1138z00_572;

											{	/* Heap/make.scm 46 */
												obj_t BgL_arg1140z00_573;

												BgL_arg1140z00_573 =
													BGl_thezd2backendzd2zzbackend_backendz00();
												BgL_arg1138z00_572 =
													(((BgL_backendz00_bglt) COBJECT(
															((BgL_backendz00_bglt) BgL_arg1140z00_573)))->
													BgL_languagez00);
											}
											VECTOR_SET(BgL_v1105z00_571, 0L, BgL_arg1138z00_572);
										}
										VECTOR_SET(BgL_v1105z00_571, 1L,
											BGl_za2bigloozd2versionza2zd2zzengine_paramz00);
										VECTOR_SET(BgL_v1105z00_571, 2L,
											BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00);
										VECTOR_SET(BgL_v1105z00_571, 3L,
											BGl_getzd2genvzd2zzast_envz00());
										VECTOR_SET(BgL_v1105z00_571, 4L,
											BGl_getzd2tenvzd2zztype_envz00());
										BgL_arg1137z00_570 = BgL_v1105z00_571;
									}
									output_obj(BgL_portz00_568, BgL_arg1137z00_570);
								}
								return close_binary_port(BgL_portz00_568);
							}
						else
							{	/* Heap/make.scm 42 */
								return
									BGl_errorz00zz__errorz00(BGl_string1244z00zzheap_makez00,
									BGl_string1245z00zzheap_makez00, BgL_hnamez00_567);
							}
					}
				}
			else
				{	/* Heap/make.scm 38 */
					return
						BGl_userzd2errorzd2zztools_errorz00(BGl_string1244z00zzheap_makez00,
						BGl_string1246z00zzheap_makez00,
						BGl_za2heapzd2nameza2zd2zzengine_paramz00, BNIL);
				}
		}

	}



/* &make-heap */
	obj_t BGl_z62makezd2heapzb0zzheap_makez00(obj_t BgL_envz00_781)
	{
		{	/* Heap/make.scm 35 */
			return BGl_makezd2heapzd2zzheap_makez00();
		}

	}



/* &prepare-globals! */
	obj_t BGl_z62preparezd2globalsz12za2zzheap_makez00(obj_t BgL_envz00_782)
	{
		{	/* Heap/make.scm 60 */
			{	/* Heap/make.scm 75 */
				bool_t BgL_tmpz00_915;

				BGl_forzd2eachzd2globalz12z12zzast_envz00(BGl_proc1247z00zzheap_makez00,
					BNIL);
				BgL_tmpz00_915 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_915);
			}
		}

	}



/* &<@anonymous:1147> */
	obj_t BGl_z62zc3z04anonymousza31147ze3ze5zzheap_makez00(obj_t BgL_envz00_784,
		obj_t BgL_gz00_785)
	{
		{	/* Heap/make.scm 61 */
			if (
				((((BgL_globalz00_bglt) COBJECT(
								((BgL_globalz00_bglt) BgL_gz00_785)))->BgL_importz00) ==
					CNST_TABLE_REF(3)))
				{	/* Heap/make.scm 65 */
					obj_t BgL_arg1152z00_803;
					obj_t BgL_arg1153z00_804;

					BgL_arg1152z00_803 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_gz00_785))))->BgL_idz00);
					BgL_arg1153z00_804 =
						(((BgL_globalz00_bglt) COBJECT(
								((BgL_globalz00_bglt) BgL_gz00_785)))->BgL_modulez00);
					BGl_unbindzd2globalz12zc0zzast_envz00(BgL_arg1152z00_803,
						BgL_arg1153z00_804);
				}
			else
				{	/* Heap/make.scm 64 */
					if (
						((((BgL_globalz00_bglt) COBJECT(
										((BgL_globalz00_bglt) BgL_gz00_785)))->BgL_importz00) ==
							CNST_TABLE_REF(4)))
						{	/* Heap/make.scm 67 */
							obj_t BgL_vz00_805;

							BgL_vz00_805 = CNST_TABLE_REF(5);
							((((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_gz00_785)))->BgL_importz00) =
								((obj_t) BgL_vz00_805), BUNSPEC);
						}
					else
						{	/* Heap/make.scm 66 */
							BUNSPEC;
						}
				}
			{	/* Heap/make.scm 72 */
				bool_t BgL_test1301z00_937;

				{	/* Heap/make.scm 72 */
					obj_t BgL_tmpz00_938;

					{	/* Heap/make.scm 72 */
						BgL_objectz00_bglt BgL_tmpz00_939;

						BgL_tmpz00_939 = ((BgL_objectz00_bglt) BgL_gz00_785);
						BgL_tmpz00_938 = BGL_OBJECT_WIDENING(BgL_tmpz00_939);
					}
					BgL_test1301z00_937 = CBOOL(BgL_tmpz00_938);
				}
				if (BgL_test1301z00_937)
					{	/* Heap/make.scm 72 */
						{	/* Heap/make.scm 72 */
							long BgL_arg1162z00_806;

							{	/* Heap/make.scm 72 */
								obj_t BgL_arg1164z00_807;

								{	/* Heap/make.scm 72 */
									obj_t BgL_arg1166z00_808;

									{	/* Heap/make.scm 72 */
										obj_t BgL_arg1815z00_809;
										long BgL_arg1816z00_810;

										BgL_arg1815z00_809 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Heap/make.scm 72 */
											long BgL_arg1817z00_811;

											BgL_arg1817z00_811 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt) BgL_gz00_785));
											BgL_arg1816z00_810 = (BgL_arg1817z00_811 - OBJECT_TYPE);
										}
										BgL_arg1166z00_808 =
											VECTOR_REF(BgL_arg1815z00_809, BgL_arg1816z00_810);
									}
									BgL_arg1164z00_807 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1166z00_808);
								}
								{	/* Heap/make.scm 72 */
									obj_t BgL_tmpz00_949;

									BgL_tmpz00_949 = ((obj_t) BgL_arg1164z00_807);
									BgL_arg1162z00_806 = BGL_CLASS_NUM(BgL_tmpz00_949);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_gz00_785), BgL_arg1162z00_806);
						}
						{	/* Heap/make.scm 72 */
							BgL_objectz00_bglt BgL_tmpz00_954;

							BgL_tmpz00_954 = ((BgL_objectz00_bglt) BgL_gz00_785);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_954, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_gz00_785);
						BgL_gz00_785;
					}
				else
					{	/* Heap/make.scm 72 */
						BFALSE;
					}
			}
			((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_gz00_785))))->BgL_occurrencez00) =
				((long) 0L), BUNSPEC);
			{	/* Heap/make.scm 75 */
				obj_t BgL_vz00_812;

				BgL_vz00_812 = BGl_za2heapzd2libraryza2zd2zzengine_paramz00;
				return
					((((BgL_globalz00_bglt) COBJECT(
								((BgL_globalz00_bglt) BgL_gz00_785)))->BgL_libraryz00) =
					((obj_t) BgL_vz00_812), BUNSPEC);
			}
		}

	}



/* make-add-heap */
	BGL_EXPORTED_DEF obj_t BGl_makezd2addzd2heapz00zzheap_makez00(void)
	{
		{	/* Heap/make.scm 84 */
			{	/* Heap/make.scm 85 */
				obj_t BgL_list1167z00_596;

				{	/* Heap/make.scm 85 */
					obj_t BgL_arg1171z00_597;

					{	/* Heap/make.scm 85 */
						obj_t BgL_arg1172z00_598;

						BgL_arg1172z00_598 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1171z00_597 =
							MAKE_YOUNG_PAIR(BGl_string1248z00zzheap_makez00,
							BgL_arg1172z00_598);
					}
					BgL_list1167z00_596 =
						MAKE_YOUNG_PAIR(BGl_string1242z00zzheap_makez00,
						BgL_arg1171z00_597);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1167z00_596);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1248z00zzheap_makez00;
			{	/* Heap/make.scm 85 */
				obj_t BgL_g1073z00_599;
				obj_t BgL_g1074z00_600;

				{	/* Heap/make.scm 85 */
					obj_t BgL_list1189z00_613;

					{	/* Heap/make.scm 85 */
						obj_t BgL_arg1190z00_614;

						BgL_arg1190z00_614 =
							MAKE_YOUNG_PAIR
							(BGl_preparezd2additionalzd2globalsz12zd2envzc0zzheap_makez00,
							BNIL);
						BgL_list1189z00_613 =
							MAKE_YOUNG_PAIR
							(BGl_checkzd2additionalzd2heapzd2libraryzd2envz00zzheap_makez00,
							BgL_arg1190z00_614);
					}
					BgL_g1073z00_599 = BgL_list1189z00_613;
				}
				BgL_g1074z00_600 = CNST_TABLE_REF(6);
				{
					obj_t BgL_hooksz00_602;
					obj_t BgL_hnamesz00_603;

					BgL_hooksz00_602 = BgL_g1073z00_599;
					BgL_hnamesz00_603 = BgL_g1074z00_600;
				BgL_zc3z04anonymousza31173ze3z87_604:
					if (NULLP(BgL_hooksz00_602))
						{	/* Heap/make.scm 85 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Heap/make.scm 85 */
							bool_t BgL_test1307z00_976;

							{	/* Heap/make.scm 85 */
								obj_t BgL_fun1188z00_611;

								BgL_fun1188z00_611 = CAR(((obj_t) BgL_hooksz00_602));
								BgL_test1307z00_976 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1188z00_611));
							}
							if (BgL_test1307z00_976)
								{	/* Heap/make.scm 85 */
									obj_t BgL_arg1182z00_608;
									obj_t BgL_arg1183z00_609;

									BgL_arg1182z00_608 = CDR(((obj_t) BgL_hooksz00_602));
									BgL_arg1183z00_609 = CDR(((obj_t) BgL_hnamesz00_603));
									{
										obj_t BgL_hnamesz00_988;
										obj_t BgL_hooksz00_987;

										BgL_hooksz00_987 = BgL_arg1182z00_608;
										BgL_hnamesz00_988 = BgL_arg1183z00_609;
										BgL_hnamesz00_603 = BgL_hnamesz00_988;
										BgL_hooksz00_602 = BgL_hooksz00_987;
										goto BgL_zc3z04anonymousza31173ze3z87_604;
									}
								}
							else
								{	/* Heap/make.scm 85 */
									obj_t BgL_arg1187z00_610;

									BgL_arg1187z00_610 = CAR(((obj_t) BgL_hnamesz00_603));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1248z00zzheap_makez00,
										BGl_string1243z00zzheap_makez00, BgL_arg1187z00_610);
								}
						}
				}
			}
			{	/* Heap/make.scm 88 */
				obj_t BgL_genvz00_615;
				obj_t BgL_tenvz00_616;
				obj_t BgL_langz00_617;

				BgL_genvz00_615 = BGl_getzd2genvzd2zzast_envz00();
				BgL_tenvz00_616 = BGl_getzd2tenvzd2zztype_envz00();
				{	/* Heap/make.scm 90 */
					obj_t BgL_arg1196z00_625;

					BgL_arg1196z00_625 = BGl_thezd2backendzd2zzbackend_backendz00();
					BgL_langz00_617 =
						(((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_arg1196z00_625)))->BgL_languagez00);
				}
				BGl_setzd2objzd2stringzd2modez12zc0zz__intextz00(CNST_TABLE_REF(2));
				if (STRINGP(BGl_za2additionalzd2heapzd2nameza2z00zzengine_paramz00))
					{	/* Heap/make.scm 96 */
						obj_t BgL_hnamez00_619;

						BgL_hnamez00_619 =
							BGl_za2additionalzd2heapzd2nameza2z00zzengine_paramz00;
						{	/* Heap/make.scm 97 */
							obj_t BgL_portz00_620;

							BgL_portz00_620 =
								BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00
								(BgL_hnamez00_619);
							if (BINARY_PORTP(BgL_portz00_620))
								{	/* Heap/make.scm 98 */
									{	/* Heap/make.scm 101 */
										obj_t BgL_arg1193z00_622;

										{	/* Heap/make.scm 101 */
											obj_t BgL_v1106z00_623;

											BgL_v1106z00_623 = create_vector(7L);
											VECTOR_SET(BgL_v1106z00_623, 0L, BgL_langz00_617);
											VECTOR_SET(BgL_v1106z00_623, 1L,
												BGl_za2bigloozd2versionza2zd2zzengine_paramz00);
											VECTOR_SET(BgL_v1106z00_623, 2L,
												BGl_za2bigloozd2specificzd2versionza2z00zzengine_paramz00);
											VECTOR_SET(BgL_v1106z00_623, 3L, BgL_genvz00_615);
											VECTOR_SET(BgL_v1106z00_623, 4L, BgL_tenvz00_616);
											VECTOR_SET(BgL_v1106z00_623, 5L,
												BGl_za2additionalzd2includezd2foreignza2z00zzengine_paramz00);
											VECTOR_SET(BgL_v1106z00_623, 6L,
												BGl_za2cczd2optionsza2zd2zzengine_paramz00);
											BgL_arg1193z00_622 = BgL_v1106z00_623;
										}
										output_obj(BgL_portz00_620, BgL_arg1193z00_622);
									}
									return close_binary_port(BgL_portz00_620);
								}
							else
								{	/* Heap/make.scm 98 */
									return
										BGl_errorz00zz__errorz00(BGl_string1249z00zzheap_makez00,
										BGl_string1245z00zzheap_makez00, BgL_hnamez00_619);
								}
						}
					}
				else
					{	/* Heap/make.scm 92 */
						return
							BGl_userzd2errorzd2zztools_errorz00
							(BGl_string1250z00zzheap_makez00, BGl_string1246z00zzheap_makez00,
							BGl_za2additionalzd2heapzd2nameza2z00zzengine_paramz00, BNIL);
					}
			}
		}

	}



/* &make-add-heap */
	obj_t BGl_z62makezd2addzd2heapz62zzheap_makez00(obj_t BgL_envz00_786)
	{
		{	/* Heap/make.scm 84 */
			return BGl_makezd2addzd2heapz00zzheap_makez00();
		}

	}



/* &check-additional-heap-library */
	obj_t BGl_z62checkzd2additionalzd2heapzd2libraryzb0zzheap_makez00(obj_t
		BgL_envz00_788)
	{
		{	/* Heap/make.scm 113 */
			if ((BGl_za2heapzd2libraryza2zd2zzengine_paramz00 == CNST_TABLE_REF(7)))
				{	/* Heap/make.scm 115 */
					return
						BGl_userzd2errorzd2zztools_errorz00(CNST_TABLE_REF(8),
						BGl_string1251z00zzheap_makez00,
						BGl_za2heapzd2libraryza2zd2zzengine_paramz00, BNIL);
				}
			else
				{	/* Heap/make.scm 115 */
					if (SYMBOLP(BGl_za2heapzd2libraryza2zd2zzengine_paramz00))
						{	/* Heap/make.scm 119 */
							return BTRUE;
						}
					else
						{	/* Heap/make.scm 119 */
							return
								BGl_userzd2errorzd2zztools_errorz00(CNST_TABLE_REF(8),
								BGl_string1252z00zzheap_makez00,
								BGl_za2heapzd2libraryza2zd2zzengine_paramz00, BNIL);
						}
				}
		}

	}



/* &prepare-additional-globals! */
	obj_t BGl_z62preparezd2additionalzd2globalsz12z70zzheap_makez00(obj_t
		BgL_envz00_787)
	{
		{	/* Heap/make.scm 133 */
			{	/* Heap/make.scm 149 */
				bool_t BgL_tmpz00_1026;

				BGl_forzd2eachzd2globalz12z12zzast_envz00(BGl_proc1253z00zzheap_makez00,
					BNIL);
				BgL_tmpz00_1026 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_1026);
			}
		}

	}



/* &<@anonymous:1202> */
	obj_t BGl_z62zc3z04anonymousza31202ze3ze5zzheap_makez00(obj_t BgL_envz00_790,
		obj_t BgL_gz00_791)
	{
		{	/* Heap/make.scm 134 */
			{	/* Heap/make.scm 137 */
				bool_t BgL_test1313z00_1029;

				if (
					((((BgL_globalz00_bglt) COBJECT(
									((BgL_globalz00_bglt) BgL_gz00_791)))->BgL_importz00) ==
						CNST_TABLE_REF(3)))
					{	/* Heap/make.scm 137 */
						BgL_test1313z00_1029 = ((bool_t) 1);
					}
				else
					{	/* Heap/make.scm 137 */
						BgL_test1313z00_1029 =
							CBOOL(
							(((BgL_globalz00_bglt) COBJECT(
										((BgL_globalz00_bglt) BgL_gz00_791)))->BgL_libraryz00));
					}
				if (BgL_test1313z00_1029)
					{	/* Heap/make.scm 139 */
						obj_t BgL_arg1208z00_813;
						obj_t BgL_arg1209z00_814;

						BgL_arg1208z00_813 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_gz00_791))))->BgL_idz00);
						BgL_arg1209z00_814 =
							(((BgL_globalz00_bglt) COBJECT(
									((BgL_globalz00_bglt) BgL_gz00_791)))->BgL_modulez00);
						BGl_unbindzd2globalz12zc0zzast_envz00(BgL_arg1208z00_813,
							BgL_arg1209z00_814);
					}
				else
					{	/* Heap/make.scm 137 */
						if (
							((((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_gz00_791)))->BgL_importz00) ==
								CNST_TABLE_REF(4)))
							{	/* Heap/make.scm 141 */
								obj_t BgL_vz00_815;

								BgL_vz00_815 = CNST_TABLE_REF(5);
								((((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt) BgL_gz00_791)))->BgL_importz00) =
									((obj_t) BgL_vz00_815), BUNSPEC);
							}
						else
							{	/* Heap/make.scm 140 */
								BUNSPEC;
							}
					}
			}
			{	/* Heap/make.scm 146 */
				bool_t BgL_test1316z00_1052;

				{	/* Heap/make.scm 146 */
					obj_t BgL_tmpz00_1053;

					{	/* Heap/make.scm 146 */
						BgL_objectz00_bglt BgL_tmpz00_1054;

						BgL_tmpz00_1054 = ((BgL_objectz00_bglt) BgL_gz00_791);
						BgL_tmpz00_1053 = BGL_OBJECT_WIDENING(BgL_tmpz00_1054);
					}
					BgL_test1316z00_1052 = CBOOL(BgL_tmpz00_1053);
				}
				if (BgL_test1316z00_1052)
					{	/* Heap/make.scm 146 */
						{	/* Heap/make.scm 146 */
							long BgL_arg1218z00_816;

							{	/* Heap/make.scm 146 */
								obj_t BgL_arg1219z00_817;

								{	/* Heap/make.scm 146 */
									obj_t BgL_arg1220z00_818;

									{	/* Heap/make.scm 146 */
										obj_t BgL_arg1815z00_819;
										long BgL_arg1816z00_820;

										BgL_arg1815z00_819 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Heap/make.scm 146 */
											long BgL_arg1817z00_821;

											BgL_arg1817z00_821 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt) BgL_gz00_791));
											BgL_arg1816z00_820 = (BgL_arg1817z00_821 - OBJECT_TYPE);
										}
										BgL_arg1220z00_818 =
											VECTOR_REF(BgL_arg1815z00_819, BgL_arg1816z00_820);
									}
									BgL_arg1219z00_817 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1220z00_818);
								}
								{	/* Heap/make.scm 146 */
									obj_t BgL_tmpz00_1064;

									BgL_tmpz00_1064 = ((obj_t) BgL_arg1219z00_817);
									BgL_arg1218z00_816 = BGL_CLASS_NUM(BgL_tmpz00_1064);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_gz00_791), BgL_arg1218z00_816);
						}
						{	/* Heap/make.scm 146 */
							BgL_objectz00_bglt BgL_tmpz00_1069;

							BgL_tmpz00_1069 = ((BgL_objectz00_bglt) BgL_gz00_791);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1069, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_gz00_791);
						BgL_gz00_791;
					}
				else
					{	/* Heap/make.scm 146 */
						BFALSE;
					}
			}
			((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_gz00_791))))->BgL_occurrencez00) =
				((long) 0L), BUNSPEC);
			{	/* Heap/make.scm 149 */
				obj_t BgL_vz00_822;

				BgL_vz00_822 = BGl_za2heapzd2libraryza2zd2zzengine_paramz00;
				return
					((((BgL_globalz00_bglt) COBJECT(
								((BgL_globalz00_bglt) BgL_gz00_791)))->BgL_libraryz00) =
					((obj_t) BgL_vz00_822), BUNSPEC);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzheap_makez00(void)
	{
		{	/* Heap/make.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzheap_makez00(void)
	{
		{	/* Heap/make.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzheap_makez00(void)
	{
		{	/* Heap/make.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzheap_makez00(void)
	{
		{	/* Heap/make.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zzforeign_libraryz00(419430976L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zzheap_restorez00(147989063L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
			return
				BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1254z00zzheap_makez00));
		}

	}

#ifdef __cplusplus
}
#endif
